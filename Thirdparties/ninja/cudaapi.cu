#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cusparse_v2.h>
#include <cusolverSp.h>
#include "cudakernel.h"

typedef size_t devptr_t;
extern "C" void gpureset_()
{
  cudaDeviceReset();
}
const char* cublasGetError(cublasStatus_t error)
{
  switch (error)
    {
    case CUBLAS_STATUS_SUCCESS:
      return "CUBLAS_STATUS_SUCCESS";

    case CUBLAS_STATUS_NOT_INITIALIZED:
      return "CUBLAS_STATUS_NOT_INITIALIZED";

    case CUBLAS_STATUS_ALLOC_FAILED:
      return "CUBLAS_STATUS_ALLOC_FAILED";

    case CUBLAS_STATUS_INVALID_VALUE:
      return "CUBLAS_STATUS_INVALID_VALUE";

    case CUBLAS_STATUS_ARCH_MISMATCH:
      return "CUBLAS_STATUS_ARCH_MISMATCH";

    case CUBLAS_STATUS_MAPPING_ERROR:
      return "CUBLAS_STATUS_MAPPING_ERROR";

    case CUBLAS_STATUS_EXECUTION_FAILED:
      return "CUBLAS_STATUS_EXECUTION_FAILED";

    case CUBLAS_STATUS_INTERNAL_ERROR:
      return "CUBLAS_STATUS_INTERNAL_ERROR";
    }

  return "<unknown>";
}
extern "C" void gpumemgetinfo_(devptr_t *free,devptr_t *total)
{
  size_t fr,tot;
  cudaMemGetInfo(&fr,&tot);
  *free = fr;
  *total = tot;
}

extern "C" void gpumemset_(devptr_t* mem,int *val,int* count)
{
  cudaError_t err;
  err = cudaMemset((void*)(*mem),*val,*count);
    if(err != cudaSuccess)
      {
	printf("CUDA memset error %s",cudaGetErrorString(err));exit(1);
      }
    return;
}

extern "C" void cudaddot_(devptr_t *han,const int *n, const devptr_t *devPtrx, const int *incx,const devptr_t *devPtry, const int *incy,double *out,devptr_t *caller)
{
  cublasStatus_t stat;
  double result;
  double *x = (double *)(*devPtrx);
  double *y = (double *)(*devPtry);
  stat = cublasDdot ((cublasHandle_t)*han,*n, x, *incx, y, *incy,&result);
  if(stat != CUBLAS_STATUS_SUCCESS)
    {
      printf("Failed DDOT %s called by %s\n",cublasGetError(stat),(char*)caller);exit(1);
    }
  *out = result;
  return;
}
extern "C" void cudadaxpy_(devptr_t *han,const int *n,const double *alpha,const devptr_t *devPtrx, const int *incx,const devptr_t *devPtry, const int *incy)
{
  double co = *alpha;
  cublasStatus_t stat;
  double *x = (double *)(*devPtrx);
  double *y = (double *)(*devPtry);
  stat = cublasDaxpy ((cublasHandle_t)(*han),*n,&co, x, *incx, y, *incy);
  if(stat != CUBLAS_STATUS_SUCCESS)
    {
      printf("Failed AXPY\n");exit(1);
    }
  return;
}
extern "C" void cudadcopy_(devptr_t *han,const int *n, const devptr_t *devPtrx, const int *incx,const devptr_t *devPtry, const int *incy)
{
  cublasStatus_t stat;
  double *x = (double *)(*devPtrx);
  double *y = (double *)(*devPtry);
  stat = cublasDcopy((cublasHandle_t)*han,*n, x, *incx, y, *incy);
  if(stat != CUBLAS_STATUS_SUCCESS)
    {
      printf("Failed DCOPY\n");exit(1);
    }
  return;
}
extern "C" void cudadscal_(devptr_t *han,const int *n,double *alpha,devptr_t *devPtry, const int *incy)
{
  double al = *alpha;
  cublasStatus_t stat;
  double *y = (double *)(*devPtry);
  stat = cublasDscal ((cublasHandle_t)*han,*n,&al, y, *incy);
  if(stat != CUBLAS_STATUS_SUCCESS)
    {
      printf("Failed DSCAl\n");exit(1);
    }
  return;
}
extern "C" void cudadnrm2_(devptr_t *han,const int *n,devptr_t *devPtry, const int *incy,double *out)
{
  cublasStatus_t stat;
  double *y = (double *)(*devPtry);
  double result;
  
  stat = cublasDnrm2 ((cublasHandle_t)*han,*n,y, *incy,&result);
  if(stat != CUBLAS_STATUS_SUCCESS)
    {
      printf("Failed DNRM2\n");exit(1);
    }
  *out = result;
  return;
}
extern "C" void  cublashandle_(devptr_t *han)
{
  cublasStatus_t stat;
  cublasHandle_t hand=0;
  stat = cublasCreate(&hand);
  if(stat != CUBLAS_STATUS_SUCCESS)
    {
      printf("FAiled handle CUBLAS\n");exit(1);
    }
  *han  = (devptr_t)hand;
  return;
}
extern "C" void gpumalloc_(devptr_t *pt,int *N,devptr_t *caller)
{
  void *x;
  cudaError_t err;
  err = cudaMalloc(&x,*N);
    if(err != cudaSuccess)
      {
	printf("CUDA malloc error %s called by %s",cudaGetErrorString(err),(char*)caller);exit(1);
      }
    *pt = (devptr_t)x;
    return;
}

extern "C" void memcpytogpu_ (devptr_t *dst,devptr_t *src,int *N,devptr_t *caller)
{
  cudaError_t err;
  err = cudaMemcpy((void*)(*dst),(void*)src,*N,cudaMemcpyHostToDevice);
    if(err != cudaSuccess)
      {
	printf("CUDA memcpy  error to GPU %s called by %s",cudaGetErrorString(err),(char*)caller);exit(1);
      }
    return;
}
extern "C" void memcpyfromgpu_ (devptr_t *dst,devptr_t *src,int *N,devptr_t* caller)
{
  cudaError_t err;
  err = cudaMemcpy((void*)dst,(void*)*src,*N,cudaMemcpyDeviceToHost);
    if(err != cudaSuccess)
      {
	printf("CUDA memcpy error from GPU %s called by %s",cudaGetErrorString(err),(char*)caller);exit(1);
      }
    return;
}

extern "C" void cusparsehandle_(devptr_t *handle)
{
  cusparseHandle_t thandle = 0; 
  cusparseStatus_t stat = cusparseCreate(&thandle);
  if(stat != CUSPARSE_STATUS_SUCCESS)
    {
      printf("CUDA CUSPARSE create handle error ");exit(1);
    }
  *handle  = (devptr_t)thandle;
  return;
}

extern "C" void cusparse_mat_descr_create_(devptr_t *descrA)
{
  cusparseMatDescr_t tdescrA = 0;
  cusparseStatus_t stat = cusparseCreateMatDescr(&tdescrA);
  if(stat != CUSPARSE_STATUS_SUCCESS)
    {
      printf("CUDA matrix desc create error ");exit(1);
    }
  *descrA  = (devptr_t)tdescrA;
  return;
}

extern "C" void  cusparse_set_mat_base_(devptr_t *descrA, int *base)
{
  cusparseStatus_t stat = cusparseSetMatIndexBase((cusparseMatDescr_t)(*descrA),(cusparseIndexBase_t)(*base));
  if(stat != CUSPARSE_STATUS_SUCCESS)
    {
      printf("CUDA mat base set  error ");exit(1);
    }
  return;
}
extern "C" void  cusparse_set_mat_type_(devptr_t *descrA, int *base)
{
  cusparseStatus_t stat = cusparseSetMatType((cusparseMatDescr_t)(*descrA), (cusparseMatrixType_t)(*base));
  if(stat != CUSPARSE_STATUS_SUCCESS)
    {
      printf("CUDA mat base set  error ");exit(1);
    }
  return;
}
extern "C" void cudadbsrmv_(devptr_t *handle,int *dir,int *transA,int *mb,int *nb,int *nnzb,double *alpha,devptr_t *descrA,devptr_t *bsrValA,devptr_t *bsrRowPtrA,devptr_t *bsrColIndA,int *blockdim,devptr_t *x,double *beta, devptr_t *y)
{
  const double al = *alpha;
  const double bl = *beta;
  
  double *rhs = (double *)(*x);
  double *unk = (double *)(*y);
  double *A = (double *)(*bsrValA);
  int *r = (int *)(*bsrRowPtrA);
  int *c = (int *)(*bsrColIndA);
  cusparseStatus_t stat;
  stat = cusparseDbsrmv((cusparseHandle_t)(*handle),
			(cusparseDirection_t)(*dir),
			(cusparseOperation_t)(*transA),
			*mb, 
			*nb, 
			*nnzb,
			&al,
			(cusparseMatDescr_t)(*descrA), 
			A, 
			r, 
			c, 
			*blockdim,
			rhs,
			&bl, 
			unk);
  if(stat != CUSPARSE_STATUS_SUCCESS)
    {
      printf("CUDA mat base set  error ");exit(1);
    }
  return;
}

extern "C" void  gpustreamcreate_(devptr_t *str)
{
  cudaError_t stat;
  cudaStream_t s;
  stat = cudaStreamCreate(&s);
  if(stat != cudaSuccess)
    {
      printf("FAiled create cuda stream\n");exit(1);
    }
  *str  = (devptr_t)s;
  return;
}

extern "C" void memcpytogpuasync_ (devptr_t *dst,devptr_t *src,int *N,devptr_t *str)
{
  cudaError_t err;
  err = cudaMemcpyAsync((void*)(*dst),(void*)src,*N,cudaMemcpyHostToDevice,(cudaStream_t) (*str));
    if(err != cudaSuccess)
      {
	printf("CUDA memcpy  error %s",cudaGetErrorString(err));exit(1);
      }
    return;
}
extern "C" void memcpyfromgpuasync_ (devptr_t *dst,devptr_t *src,int *N,devptr_t *str)
{
  cudaError_t err;
  err = cudaMemcpyAsync((void*)dst,(void*)*src,*N,cudaMemcpyDeviceToHost,(cudaStream_t) (*str));
    if(err != cudaSuccess)
      {
	printf("CUDA memcpy error %s",cudaGetErrorString(err));exit(1);
      }
    return;
}

extern "C" void cublasattachstream_(devptr_t *han,devptr_t *str)
{
  cublasStatus_t stat;
  stat = cublasSetStream((cublasHandle_t)(*han),(cudaStream_t)(*str));
  if(stat != CUBLAS_STATUS_SUCCESS)
    {
      printf("FAiled handle CUBLAS attach stream\n");exit(1);
    }
}

extern "C" void cusparseattachstream_(devptr_t *han,devptr_t *str)
{
  cusparseStatus_t stat;
  stat = cusparseSetStream((cusparseHandle_t)(*han),(cudaStream_t)(*str));
  if(stat != CUSPARSE_STATUS_SUCCESS)
    {
      printf("FAiled handle sparse attach stream\n");exit(1);
    }
}

extern "C" void  gpustreamsync_(devptr_t *str)
{
  cudaError_t stat;
  stat = cudaStreamSynchronize((cudaStream_t)(*str));
  if(stat != cudaSuccess)
    {
      printf("FAiled to sync cuda stream\n");exit(1);
    }
  return;
}

extern "C" void  gpusync_()
{
  cudaError_t stat;
  stat = cudaDeviceSynchronize();
  if(stat != cudaSuccess)
    {
      printf("FAiled to sync GPU  %s\n",cudaGetErrorString(stat));exit(1);
    }
  return;
}

extern "C" void cusolversphandle_(devptr_t *han)
{
  cusolverStatus_t stat;
  cusolverSpHandle_t hand=0;
  stat = cusolverSpCreate(&hand);
  if(stat != CUSOLVER_STATUS_SUCCESS)
    {
      printf("FAiled handle CUSOLVER\n");exit(1);
    }
  *han  = (devptr_t)hand;
  return;
}

extern "C" void cusolverspattachstream_(devptr_t *han,devptr_t *str)
{
  cusolverStatus_t stat;
  stat = cusolverSpSetStream((cusolverSpHandle_t)(*han),(cudaStream_t)(*str));
  if(stat != CUSOLVER_STATUS_SUCCESS)
    {
      printf("FAiled handle CUSOLVER attach stream\n");exit(1);
    }
  return;
}

extern "C" void cusolverspdcsrlsvchol_(devptr_t* han,int *m,int *nnz,devptr_t* desc,devptr_t* val,devptr_t* row,devptr_t* col,devptr_t* rhs,double *tol,int *reod,devptr_t *unk)
{
  cusolverStatus_t stat;
  int sing;
  stat = cusolverSpDcsrlsvchol((cusolverSpHandle_t) (*han), 
			       *m, 
			       *nnz, 
			       (cusparseMatDescr_t) (*desc), 
			       (double*) (*val), 
			       (int*) (*row), 
			       (int*) (*col), 
			       (double*) (*rhs), 
			       *tol, 
			       *reod, 
			       (double*)(*unk), 
			       &sing);
  if(stat != CUSOLVER_STATUS_SUCCESS)
    {
      printf("FAiled CUSOLVER cholesky direct \n");exit(1);
    }
  if( sing != -1)
    {
      printf("Singularity observerd..value of singularity ---> %d\n",sing);
    }
  return;
}
