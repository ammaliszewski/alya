subroutine GPUDEFLCG(ND,V,NGRP,NSKYL,NZGRO,iagro,jagro,maxiter,amatr,ia,ja,bb,xx,lgrp)
  use gpuvardeflcg
  use def_solver, only : solve_sol,resin,resfi,resi1,resi2,iters
  use mod_csrdir
  implicit none
  integer*4 :: ND,V,maxiter
  integer*4 :: ia(*),ja(*),lgrp(*),iagro(*),jagro(*)
  real*8 :: amatr(*),bb(*),xx(*)
  integer*4 :: NNZB,N,NV
  integer*4 :: plat,info

  real*8 :: a,b,na,gammaold,r1,gamma,delta,deno
  integer*4 :: NVOWN,off,exlen,NGRP,NSKYL,NZGRO
  real*8 ::alp,bet,nalp
  real*8 :: invb,rnorm,eps
  integer*8 :: free,total,usepergpu,error
  plat = 1
  exlen =  (ND-npoi1) * V
  off   =  npoi1 * V 
  alp   =  1.0_rp
  bet   =  0.0_rp
  nalp  = -alp
  NNZB  =  ia(ND+1) -1
  N     =  NNZB * V * V
  NV    =  ND * V
 ! NGRP =0
  if( ISEQUEN ) then
     NVOWN = NV
  else
     NVOWN = npoi3 * V
  end if

  if(NGRP /= 0) then
     allocate(mu(V*NGRP))
     allocate(murhs(V*NGRP))
     allocate(grpmat(NZGRO*V*V))
  end if
  mu =0.0_rp 
  murhs = 0.0_rp
  grpmat = 0.0_rp

  if(NGRP /= 0 .and. INOTMASTER)  call assgr2(NGRP,ND,NSKYL,V,ia,ja,amatr,grpmat)
  
  call PAR_SUM(NZGRO*V*V,grpmat,'IN MY CODE')
  
  if( INOTMASTER ) then
     
     if (tii == 1) then
!        PRINT *,ND,V,NSKYL,NGRP,NNZB,NZGRO
        call GPUMEMGETINFO(free,total)
        usepergpu = NNZB*4 + (ND+1)*4 + N*8 + 6*NV*8 + NSKYL*8 + NGRP*4 &
             + NZGRO*4 + (NGRP+1)*4
        if(usepergpu > free) then
           Print *,total,free,usepergpu,"Total Free Usepergpu respectively"
           print *,"need more gpus por favor"
           stop
        end if
        call GPUMALLOC(d_col,NNZB*4,"CG col")
        call GPUMALLOC(d_row,(ND+1)*4,"CG row")  
        call GPUMALLOC(d_val,N*8,"CG matrix")
        call GPUMALLOC(d_x,NV*8,"CG unk")
        call GPUMALLOC(d_r,NV*8,"CG resi")
        call GPUMALLOC(d_p,NV*8,"CG pvec")
        call GPUMALLOC(d_u,NV*8,"CD uvec")
        call GPUMALLOC(d_s,NV*8,"CG svec")
        call GPUMALLOC(d_dia,NV*8,"CG dia")        
        call GPUMALLOC(d_grpmat,NZGRO*V*V*8,"CG grpmat")
        call GPUMALLOC(d_grprhs,NGRP*V*8,"CG grprhs")
        call GPUMALLOC(d_grplhs,NGRP*V*8,"CG grplhs")
        call GPUMALLOC(d_lgrp,ND*4,"CG grpmat")
        call GPUMALLOC(d_iagro,(NGRP+1)*4,"CG IAGRO")
        call GPUMALLOC(d_jagro,NZGRO*4,"CG JAGRO")
        allocate(dia(NV),stat=error)
        if(error.ne.0) then
           print *,"error allocating diag memory"
           stop
        end if 
        call CUBLASHANDLE(handlebl)
        call CUSPARSEHANDLE(handlesp)
        call CUSOLVERSPHANDLE(handlesl)
        call CUSPARSE_MAT_DESCR_CREATE(descr)
        call CUSPARSE_SET_MAT_TYPE(descr,0)
        call CUSPARSE_SET_MAT_BASE(descr,1)
        tii = tii + 1
     end if
 
     call GENDIAPRE(amatr,ia,ja,dia,ND,V)     
     call MEMCPYTOGPU(d_col,ja,NNZB*4,"CG col")
     call MEMCPYTOGPU(d_row,ia,(ND+1)*4,"CG row")
     call MEMCPYTOGPU(d_dia,dia,NV*8,"CG dia")
     call MEMCPYTOGPU(d_val,amatr,N*8,"CG matrix")
     call MEMCPYTOGPU(d_x,xx,NV*8,"CG guess")
     call MEMCPYTOGPU(d_r,bb,NV*8,"CG rhs")
     if ( NGRP /=0) then
        call MEMCPYTOGPU(d_lgrp,lgrp,ND*4,"GRP index")
        call MEMCPYTOGPU(d_iagro,iagro,(NGRP+1)*4,"GRP ia index")
        call MEMCPYTOGPU(d_jagro,jagro,NZGRO*4,"GRP ja index")
        !call CSRDEFL(d_val,d_row,d_col,NGRP,ND,V,d_grpmat,d_iagro,d_jagro,d_lgrp,0)
        call MEMCPYTOGPU(d_grpmat,grpmat,NZGRO*V*8,"CG GRPMAT")
     end if
     !
     ! calculate deno and invb
     !
     call DMULBYELEM(d_dia,d_r,d_s,NV,0)
     d_off = d_s + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_s,1,deno)
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_r,1,invb)
  end if
  call PAR_SUM(deno,'IN MY CODE')
  call PAR_SUM(invb,'IN MY CODE')
  deno = sqrt(deno)
  invb = 1.0_rp /sqrt(invb)

  if( INOTMASTER ) then          
     if (plat == 1 .and. NGRP /=0) then    
        nullify(solve_sol(1) % Ildef)
        nullify(solve_sol(1) % Jldef)
        nullify(solve_sol(1) % Lndef)
        nullify(solve_sol(1) % iUdef)
        nullify(solve_sol(1) % jUdef)
        nullify(solve_sol(1) % Undef)                                ! Permutation arrays                                                                   
        nullify(solve_sol(1) % invpRdef)
        nullify(solve_sol(1) % invpCdef)
        call CSR_LU_Factorization(&                                  ! CSR LU Factorization                                                                 
             NGRP,V,iagro,jagro,grpmat,&
             solve_sol(1) % ILdef,solve_sol(1) % JLdef,&
             solve_sol(1) % LNdef,solve_sol(1) % IUdef,&
             solve_sol(1) % JUdef,solve_sol(1) % UNdef,info)
        if( info /= 0 ) call runend('DEFLCG: SINGULAR MATRIX')
     end if
   
     !
     ! Matrix multiply
     !
     call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
          d_val,d_row,d_col,V,d_x,bet,d_s)                                     ! d_s = A d_x
     d_off = d_s + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")                             ! Copy interface d_x from device to xx on CPU
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')  ! Exchange
     call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")                               ! Copy xx to device d_s
     call CUDADCOPY(handlebl,NV,d_r,1,d_p,1)                                   ! Copy d_p = d_r (d_r is b)
     call CUDADAXPY(handlebl,NV,nalp,d_s,1,d_p,1)                              ! d_p = d_p - d_s
  end if
      
!!!----------do here initial deflation new residual vector----
  if(NGRP /= 0 ) then
     if( INOTMASTER ) then          
        call GPUMEMSET(d_grprhs,0,NGRP*V*8)
        call CUDAWTVEC(d_grprhs,d_p,d_lgrp,ND,V,0)
        call MEMCPYFROMGPU(murhs,d_grprhs,NGRP*V*8,"small vec")
     end if
     call PAR_SUM(NGRP*V,murhs,'IN MY CODE')
     if( INOTMASTER ) then
        call CSR_LUsol(                                          &
             NGRP                    , V                       , &
             solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
             solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
             solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
             solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
             murhs                   , mu                      )   
     end if
  end if
  
  if ( INOTMASTER ) then
     if (NGRP /= 0 ) then
        call MEMCPYTOGPU(d_grplhs,mu,NGRP*V*8,"small vec")
        call CUDAWVEC(d_p,d_grplhs,d_lgrp,ND,V,0)
        call CUDADAXPY(handlebl,NV,alp,d_p,1,d_x,1)
     end if
     
     call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
          d_val,d_row,d_col,V,d_x,bet,d_s)
     d_off = d_s + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
     
     call CUDADAXPY(handlebl,NV,nalp,d_s,1,d_r,1)
     
!!!----------end deflation----
     
     call DMULBYELEM(d_dia,d_r,d_p,NV,0)
     d_off = d_p + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
     call CUDADCOPY(handlebl,NV,d_p,1,d_u,1)
!!!----preconditioning deflation-------------------------
  end if
  if (NGRP /=0 ) then
     if( INOTMASTER ) then
        call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
             d_val,d_row,d_col,V,d_p,bet,d_s)
        d_off = d_s + off*8
        call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
        call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
        call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
        
        call GPUMEMSET(d_grprhs,0,NGRP*V*8)
        call CUDAWTVEC(d_grprhs,d_s,d_lgrp,ND,V,0)
        
        call MEMCPYFROMGPU(murhs,d_grprhs,NGRP*V*8,"small vec")
     end if
     call PAR_SUM(NGRP*V,murhs,'IN MY CODE')
     if( INOTMASTER ) then
        call CSR_LUsol(                                          &
             NGRP                   , V                        , &
             solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
             solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
             solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
             solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
             murhs                   , mu                      )
     end if
  end if
  
  if ( INOTMASTER ) then
     if (NGRP /= 0 ) then
        call MEMCPYTOGPU(d_grplhs,mu,NGRP*V*8,"small vec")
        
        call CUDAWVEC(d_s,d_grplhs,d_lgrp,ND,V,0)
        
        call CUDADAXPY(handlebl,NV,nalp,d_s,1,d_p,1)
     end if
!!!----end deflation------------------------------------- 
     
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_u,1,gamma)
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_r,1,rnorm)
  end if
  call PAR_SUM(rnorm,'IN MY CODE')  
  call PAR_SUM(gamma,'IN MY CODE')
  rnorm = sqrt(rnorm)
  r1 = sqrt(gamma)
  iters = 1
  !-------------------ADAPTIVE RESIDUAL--------------------
  solve_sol(1) % reni2 = rnorm            !non-normalized residual
  solve_sol(1) % resi2 = rnorm * invb     !normalized residual
  solve_sol(1) % resin = r1 / deno     !preconditioned residual
  resin = solve_sol(1) % resin
  if( solve_sol(1) % kfl_adres == 1 ) then
     eps = max( solve_sol(1) % resin * solve_sol(1) % adres , solve_sol(1) % solmi )
  else
     eps = solve_sol(1) % solco
  end if
  tolsta = eps*deno

  do while (r1 > tolsta .and. iters <= maxiter)   
     if( INOTMASTER ) then
        call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
             d_val,d_row,d_col,V,d_p,bet,d_s)
        d_off = d_s + off*8
        call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
        call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')
        call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
     
        !
        ! dot = d_Ax . d_p
        !   
        call CUDADDOT(handlebl,NVOWN,d_s,1,d_p,1,delta)
     end if
     
     call PAR_SUM(delta,'IN MY CODE')
     a  = gamma/delta
     gammaold = gamma
     if( INOTMASTER ) then
        !    PRINT *,dot,a
        ! 
        ! d_x = d_x + a * d_p 
        !
        call CUDADAXPY(handlebl,NV,a,d_p,1,d_x,1)
        !
        ! d_r = d_r + na * d_s
        !
        na = -1.0_rp * a
        call CUDADAXPY(handlebl,NV,na,d_s,1,d_r,1)
        !
        ! ui+1 = M-1 ri+1
        !
        call DMULBYELEM(d_dia,d_r,d_u,NV,0)
        d_off = d_u + off*8
        call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
        call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
        call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")

        call CUDADDOT(handlebl,NVOWN,d_r,1,d_u,1,gamma)
     end if
     call PAR_SUM(gamma,'IN MY CODE')     
     b = gamma/gammaold
!!!-----------------------Start deflation----------------------------
     
     if ( NGRP /=0 ) then
        if ( INOTMASTER ) then
           call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
                d_val,d_row,d_col,V,d_u,bet,d_s)
           d_off = d_s + off*8
           call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
           call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
           call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
           
           call GPUMEMSET(d_grprhs,0,NGRP*V*8)
           call CUDAWTVEC(d_grprhs,d_s,d_lgrp,ND,V,0)
                   
           call MEMCPYFROMGPU(murhs,d_grprhs,NGRP*V*8,"small vec")
        end if
        call PAR_SUM(NGRP*V,murhs,'IN MY CODE')
        if ( INOTMASTER ) then
           call CSR_LUsol(                                          &
                NGRP                   , V                        , &
                solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
                solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
                solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
                solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
                murhs                   , mu                      )
        end if
     end if
     
     if ( INOTMASTER ) then
        if (NGRP /= 0 ) then
           call MEMCPYTOGPU(d_grplhs,mu,NGRP*V*8,"small vec")
           call CUDAWVEC(d_s,d_grplhs,d_lgrp,ND,V,0)
!!!-----------------------End deflation------------------------------ 
        end if
        call CUDADSCAL(handlebl,NV,b,d_p,1)
        call CUDADAXPY(handlebl,NV,alp,d_u,1,d_p,1)
        if (NGRP /=0 ) then
           call CUDADAXPY(handlebl,NV,nalp,d_s,1,d_p,1)     
        end if
     end if
     resi2 = resi1
     r1 = sqrt(gamma)
     resi1 = r1 / deno
!     if( INOTSLAVE ) then
!        print*,'residual=',r1,gamma,tolsta
!     end if
     iters = iters + 1
  enddo
  !------SUbmitting outputs--------------
  resfi = r1 / deno
  iters = iters -1 
!  if( solve_sol(1) % kfl_cvgso == 1 ) then
!     if ( INOTSLAVE ) write(solve_sol(1) % lun_cvgso,100) iters,resfi
!  end if

!  if ( INOTSLAVE ) pRINT *,'iterations and rsidual ---> ',k,r1
  if(INOTMASTER ) call MEMCPYFROMGPU(xx,d_x,NV*8,"CG unk")

100 format(i7,2(1x,e12.6))

  if(NGRP /= 0) then
     deallocate(mu,murhs,grpmat)
  end if


end subroutine GPUDEFLCG
