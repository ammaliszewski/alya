subroutine GPUCG(ND,V,maxiter,amatr,ia,ja,bb,xx)
  use gpuvarcg
  use def_solver, only : resin,resfi,resi1,resi2,iters,solve_sol
  implicit none
  integer*4 :: ND,V,maxiter
  integer*4 :: ia(*),ja(*)
  real*8 :: amatr(*),bb(*),xx(*)
  integer*4 :: NNZB,N,NV
  
  real*8 :: a,b,na,gammaold,r1,gamma,delta,deno
  integer*4 :: NVOWN,off,exlen
  real*8 ::alp,bet,nalp
  real*8 :: invb,rnorm,eps
  integer*8 :: free,total,usepergpu,error
  exlen = (ND-npoi1) * V
  off   =  npoi1 * V 
  alp   =  1.0_rp
  bet   =  0.0_rp
  nalp  = -1*alp
  NNZB  = ia(ND+1) -1
  N     = NNZB * V * V
  NV    = ND * V
  if( ISEQUEN ) then
     NVOWN = NV
  else
     NVOWN = npoi3 * V
  end if
  
  if( INOTMASTER ) then

     if (ti == 1) then
!        PRINT *,ND,V
        call GPUMEMGETINFO(free,total)
        usepergpu = NNZB*4 + (ND+1)*4 + N*8 + 6*NV*8
        if(usepergpu > free) then
           Print *,total,free,usepergpu,"Total Free Usepergpu respectively"
           print *,"need more gpus por favor"
           stop
        end if
        call GPUMALLOC(d_col,NNZB*4,"CG col")
        call GPUMALLOC(d_row,(ND+1)*4,"CG row")  
        call GPUMALLOC(d_val,N*8,"CG matrix")
        call GPUMALLOC(d_x,NV*8,"CG unk")
        call GPUMALLOC(d_r,NV*8,"CG resi")
        call GPUMALLOC(d_p,NV*8,"CG pvec")
        call GPUMALLOC(d_u,NV*8,"CD uvec")
        call GPUMALLOC(d_s,NV*8,"CG svec")
        call GPUMALLOC(d_dia,NV*8,"CG dia")
        allocate(dia(NV),stat=error)
        if(error.ne.0) then
           print *,"error allocating diag memory"
           stop
        end if

        call CUBLASHANDLE(handlebl)
        call CUSPARSEHANDLE(handlesp)
        call CUSPARSE_MAT_DESCR_CREATE(descr)
        call CUSPARSE_SET_MAT_TYPE(descr,0)
        call CUSPARSE_SET_MAT_BASE(descr,1)
        ti = ti + 1
     end if

     call GENDIAPRE(amatr,ia,ja,dia,ND,V)     
     call MEMCPYTOGPU(d_col,ja,NNZB*4,"CG col")
     call MEMCPYTOGPU(d_row,ia,(ND+1)*4,"CG row")
     call MEMCPYTOGPU(d_dia,dia,NV*8,"CG dia")
     call MEMCPYTOGPU(d_val,amatr,N*8,"CG matrix")
     call MEMCPYTOGPU(d_x,xx,NV*8,"CG guess")
     call MEMCPYTOGPU(d_r,bb,NV*8,"CG rhs")
     !
     ! calculate deno and invb
     !
     call DMULBYELEM(d_dia,d_r,d_s,NV,0)
     d_off = d_s + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_s,1,deno)
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_r,1,invb)
  end if
  call PAR_SUM(deno,'IN MY CODE')
  call PAR_SUM(invb,'IN MY CODE')
  deno = sqrt(deno)
  invb = 1/sqrt(invb)
  
  if( INOTMASTER ) then
     !
     ! Matrix multiply
     !

     call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
          d_val,d_row,d_col,V,d_x,bet,d_s)
     d_off = d_s + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")


     !start conjugate gradient

     call CUDADAXPY(handlebl,NV,nalp,d_s,1,d_r,1)
     call DMULBYELEM(d_dia,d_r,d_p,NV,0)
     d_off = d_p + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
     call CUDADCOPY(handlebl,NV,d_p,1,d_u,1)
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_u,1,gamma)
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_r,1,rnorm)
  end if
  
  call PAR_SUM(gamma,'IN MY CODE')
  call PAR_SUM(rnorm,'IN MY CODE')
  
  rnorm = sqrt(rnorm)
  r1 = sqrt(gamma)
  iters = 1
  !-------------------ADAPTIVE RESIDUAL--------------------
  solve_sol(1) % reni2 = rnorm            !non-normalized residual
  solve_sol(1) % resi2 = rnorm * invb     !normalized residual
  solve_sol(1) % resin = r1 / deno     !preconditioned residual
  resin = solve_sol(1) % resin
  if( solve_sol(1) % kfl_adres == 1 ) then
     eps = max( solve_sol(1) % resin * solve_sol(1) % adres , solve_sol(1) % solmi )
  else
     eps = solve_sol(1) % solco
  end if
  tolsta = eps*deno

  do while (r1 > tolsta .and. iters <= maxiter)   
     if( INOTMASTER ) then
        call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
             d_val,d_row,d_col,V,d_p,bet,d_s)
        d_off = d_s + off*8
        call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
        call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')
        call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")
     
        !
        ! dot = d_Ax . d_p
        !   
        call CUDADDOT(handlebl,NVOWN,d_s,1,d_p,1,delta)
     end if
     
     call PAR_SUM(delta,'IN MY CODE')
     a  = gamma/delta
     gammaold = gamma
     if( INOTMASTER ) then
        !    PRINT *,dot,a
        ! 
        ! d_x = d_x + a * d_p 
        !
        call CUDADAXPY(handlebl,NV,a,d_p,1,d_x,1)
        !
        ! d_r = d_r + na * d_s
        !
        na = -1.0_rp * a
        call CUDADAXPY(handlebl,NV,na,d_s,1,d_r,1)
        !
        ! ui+1 = M-1 ri+1
        !
        call DMULBYELEM(d_dia,d_r,d_u,NV,0)
        d_off = d_u + off*8
        call MEMCPYFROMGPU(xx,d_off,exlen*8,"bdr ex")
        call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
        call MEMCPYTOGPU(d_off,xx,exlen*8,"bdr ex")

        call CUDADDOT(handlebl,NVOWN,d_r,1,d_u,1,gamma)
     end if
     call PAR_SUM(gamma,'IN MY CODE')     
     b = gamma/gammaold
     if( INOTMASTER ) then
        call CUDADSCAL(handlebl,NV,b,d_p,1)
        call CUDADAXPY(handlebl,NV,alp,d_u,1,d_p,1)
     end if
     !r1 = gamma
     resi2 = resi1
     
     r1 = sqrt(gamma)
     resi1 = r1/deno
     !     if( INOTSLAVE ) then
     !       print*,'residual=',r1,gamma,tolsta
     !    end if
     iters = iters + 1
  enddo
  !------SUbmitting outputs--------------
  resfi = resi1
  iters = iters -1 
!  if( solve_sol(1) % kfl_cvgso == 1 ) then
!     if ( INOTSLAVE ) write(solve_sol(1) % lun_cvgso,100) iters,resfi
!  end if
!  PRINT *,"Iterations and residual CG",iters,(r1/tolsta)
  if(INOTMASTER ) call MEMCPYFROMGPU(xx,d_x,NV*8,"CG unk")

100 format(i7,2(1x,e12.6))

end subroutine GPUCG
