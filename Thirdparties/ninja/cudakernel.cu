#include <stdio.h>
#include <stdlib.h>
#include "cudakernel.h"

__global__ void genbdrrowidxker(int *x,int *y,int len,int val)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  if(tx<len)
    {
      y[tx]=x[tx]-val;
    }
  return;
}

__global__ void dmulbyelemker(double *x,double *y,double *out,int N)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  if(tx<N)
    {
      out[tx]=y[tx]*x[tx];
    }
  return;
}

__global__ void linearcombo(double *x,double *v,double *y,int m,int n)
{
  extern __shared__ double temp[];
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  double out;
  if(tx<m)
    {
      out = x[tx];
      for(int i=threadIdx.x; i < n; i += blockDim.x) 
	{
	  temp[i] = y[ i ];
	}
      for(int i=0;i<n;i++)
	{
	  out += v[tx + i*m]*temp[i];
	}
      x[tx] = out;
    }
  return;
}

__global__ void linearcombostatic(double *x,double *v,double *y,double *mm,int m,int n)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  double out;
  if(tx<m)
    {
      out = 0;
      for(int i=0;i<n;i++)
	{
	  out += v[tx + i*m]*y[i];
	}
      x[tx] = x[tx] + mm[tx]*out;
    }
  return;
}

__global__ void dbsrmvkernel(double *matrix,int *row,int *col,double *rhs,double *unk,int m,int n,int dim)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  int blkst;
  int blkend;
  int blkoff;
  int colid;
  double temp;
  double raux;
  if (tx < m)
    {
      blkst = row[tx] - 1;
      blkend = row[tx+1] - 1;
      blkoff = blkst*dim*dim;
      for(int k=blkst;k<blkend;k++)
	{
	  colid = col[k]-1;
	  for(int i=0;i<dim;i++)
	    {
	      temp=0;
	      raux = rhs[colid*dim + i];
	      for(int j=0;j<dim;j++)
		{
		  temp += matrix[blkoff + j*dim + i]*raux;
		}
	      unk[colid*dim + i] = temp;
	    }
	}
    }
  return;
}
__global__ void csrdeflkernel(double *matrix,int *row,int *col,int NGRP,int ND,int V,double *grpmatrix,int *iagro,int *jagro,int *lgrp)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  int igr,jgr,ty;
  int tmp1;
  if(tx < ND)
    {
      if(lgrp[tx] >= 0)
	{
	  igr = lgrp[tx]-1;
	  for(int i = row[tx]-1;i<row[tx+1]-1;i++)
	    {
	      ty = col[i] -1;
	      if( lgrp[ty] >= 0 )
		{
		  jgr = lgrp[ty]-1;
		  tmp1 = iagro[igr]-1;
		  while ( jagro[tmp1]-1 != jgr )
		    {
		      tmp1++;
		    }
		  for(int j=0;j<V*V;j++)
		    {
		      grpmatrix[tmp1*V*V +  j ] += matrix[ i*V*V +j ];
		    }
		}
	    }
	}
    }
  return;
}

__device__ __forceinline__ double atomicAdd(double* address, double val)
{
  unsigned long long int* address_as_ull = (unsigned long long int*) address;
  unsigned long long int old = *address_as_ull, assumed;
  do {
    assumed = old;
    old = atomicCAS(address_as_ull, assumed, __double_as_longlong(val + __longlong_as_double(assumed)));
  } while (assumed != old);
  return __longlong_as_double(old);
}

__global__ void wtveckernel(double *small,double *big,int *lgrp,int ND,int V)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  int lgp;
  if(tx < ND)
    {
      lgp = lgrp[tx] - 1;
      if(lgp >= 0)
	{
	  for(int j=0;j<V;j++)
	    {
	      //small[lgp*V + j] += big[tx*V +j]; 
	      atomicAdd( &small[lgp*V+j] , big[tx*V+j]);
	    }
	}
    }
}
__global__ void wveckernel(double *big,double *small,int *lgrp,int ND,int V)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  int lgp;
  if(tx < ND)
    {
      lgp = lgrp[tx] - 1;
      if(lgp >= 0)
	{
	  for(int j=0;j<V;j++)
	    {
	      big[tx*V + j] = small[lgp*V +j]; 
	    }
	}
      else
	{
	  for(int j=0;j<V;j++)
	    {
	      big[tx*V + j] = 0.0; 
	    }
	}
    }
}
