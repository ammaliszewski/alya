#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cudakernel.h"
#define threads 128
#define devptr_t size_t


extern "C" void genbdrrowidx_(devptr_t *ia,devptr_t *iaa,int *npoi,int *ND,int *val,devptr_t *str)
{
  cudaError_t stat;
  int N = *ND;
  int off = *npoi;
  int *x = (int *)(*ia + off*4);
  int *y = (int *)(*iaa);
  genbdrrowidxker<<<(int)((N-off+1)/threads)+1,threads>>>(x,y,N-off+1,*val);
  stat = cudaGetLastError();
  if(stat != cudaSuccess)
    {
      printf("FAiled launch BDR rowidx kernel %s\n",cudaGetErrorString(stat));exit(1);
    }

  return;
}

extern "C" void dmulbyelem_(devptr_t *xx,devptr_t *yy,devptr_t *out,int *len,devptr_t *str)
{
  cudaError_t stat;
  double *y = (double *)(*yy);
  double *x = (double *)(*xx);
  double *z = (double *)(*out);
  int N = *len;
  //if( *str == 0)
  //{
      dmulbyelemker<<<(int)(N/threads)+1,threads>>>(x,y,z,N);
      //}
      //else
      //{
      //dmulbyelemker<<<(int)(N/threads)+1,threads,0,(cudaStream_t)(*str)>>>(x,y,z,N);
      //}
  stat = cudaGetLastError();
  if(stat != cudaSuccess)
    {
      printf("FAiled launch dscal by element kernel %s\n",cudaGetErrorString(stat));exit(1);
    }
  return;
}


extern "C" void linearcombogpu_(devptr_t *X,devptr_t *V,devptr_t *Y,devptr_t *MM,int *M,int *N,devptr_t *str)
{
  cudaError_t stat;
  double *y = (double *)(*Y);
  double *x = (double *)(*X);
  double *v = (double *)(*V);
  double *mm = (double *)(*MM);
  int n = *N;
  int m = *M;
  //  if( *str == 0)
  //  {
  //  linearcombo<<<(int)(m/threads)+1,threads,n*sizeof(double)>>>(x,v,y,m,n);
  linearcombostatic<<<(int)(m/threads)+1,threads>>>(x,v,y,mm,m,n);
      //  }
      // else
      // {
      //  linearcombo<<<(int)(m/threads)+1,threads,0,(cudaStream_t)(*str)>>>(x,v,y,m,n);
      // }
  stat = cudaGetLastError();
  if(stat != cudaSuccess)
    {
      printf("FAiled launch linear combo kernel %s\n",cudaGetErrorString(stat));
      exit(1);
    }
  return;
}


extern "C" void dbsrmv_(int *mb,int *nb,int *nnzb,devptr_t *bsrValA,devptr_t *bsrRowPtrA,devptr_t *bsrColIndA,int *blockdim,devptr_t *x,devptr_t *y,devptr_t *str)
{
  double *rhs = (double *)(*x);
  double *unk = (double *)(*y);
  double *A = (double *)(*bsrValA);
  int *r = (int *)(*bsrRowPtrA);
  int *c = (int *)(*bsrColIndA);
  cudaError_t stat;
  //  if( *str == 0)
  //  {
  dbsrmvkernel<<<(int)(*mb/threads)+1,threads>>>(A,r,c,rhs,unk,*mb,*nb,*blockdim);
  //  }
  // else
  // {
  //  linearcombo<<<(int)(m/threads)+1,threads,0,(cudaStream_t)(*str)>>>(x,v,y,m,n);
  // }
  stat = cudaGetLastError();
  if(stat != cudaSuccess)
    {
      printf("FAiled launch linear combo kernel %s\n",cudaGetErrorString(stat));
      exit(1);
    }
  return;
}

extern "C" void csrdefl_(devptr_t *mat,devptr_t *row,devptr_t *col,int *NGRP,int *ND,int *V,devptr_t *grpmatrix,devptr_t *iagro,devptr_t* jagro,devptr_t *LGRP,devptr_t *str)
{
  double *A = (double*)(*mat);
  int *R = (int*)(*row);
  int *C = (int*)(*col);
  int *lgrp = (int*)(*LGRP);
  int *ig = (int*)(*iagro);
  int *jg = (int*)(*jagro);
  double *GPM = (double*)(*grpmatrix);
  cudaError_t stat;
  csrdeflkernel<<<(int)(*ND/threads)+1,threads>>>(A,R,C,*NGRP,*ND,*V,GPM,ig,jg,lgrp);
  stat = cudaGetLastError();
  if(stat != cudaSuccess)
    {
      printf("FAiled launch CSR DEFL kernel %s\n",cudaGetErrorString(stat));
      exit(1);
    }
  return;
}

extern "C" void cudawtvec_(devptr_t *SMALL,devptr_t *BIG,devptr_t* LGRP,int *ND,int *V,devptr_t* str)
{
  double *s = (double*)(*SMALL);
  double *b = (double*)(*BIG);
  int *lgrp = (int*)(*LGRP);
  cudaError_t stat;
  wtveckernel<<<(int)(*ND/threads)+1,threads>>>(s,b,lgrp,*ND,*V);
  stat = cudaGetLastError();
  if(stat != cudaSuccess)
    {
      printf("FAiled launch WTVEC kernel %s\n",cudaGetErrorString(stat));
      exit(1);
    }
  return;
}

extern "C" void cudawvec_(devptr_t *BIG,devptr_t *SMALL,devptr_t* LGRP,int *ND,int *V,devptr_t* str)
{
  double *s = (double*)(*SMALL);
  double *b = (double*)(*BIG);
  int *lgrp = (int*)(*LGRP);
  cudaError_t stat;
  wveckernel<<<(int)(*ND/threads)+1,threads>>>(b,s,lgrp,*ND,*V);
  stat = cudaGetLastError();
  if(stat != cudaSuccess)
    {
      printf("FAiled launch WVEC kernel %s\n",cudaGetErrorString(stat));
      exit(1);
    }
  return;
}
