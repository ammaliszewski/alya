subroutine gendiapre(amatr,ia,ja,dia,ND,V)
  implicit none
  integer*4 :: ia(*),ja(*),ND,V
  real*8 :: amatr(V,V,*),dia(*)
  integer*4 :: i,j,k
  !$OMP PARALLEL  DO                                     &
  !$OMP SCHEDULE ( STATIC )                              &
  !$OMP DEFAULT  ( NONE )                                &
  !$OMP SHARED   ( amatr, ia, ja, ND,V,dia )  &
  !$OMP PRIVATE  ( i, j,k)           
  do i=1,ND
     do j=ia(i),ia(i+1)-1
        if(ja(j)==i) then
           do k=1,V
              dia((i-1)*V + k) = 1/amatr(k,k,j)
           end do
        end if
     end do
  end do
  !$OMP END PARALLEL DO
end subroutine gendiapre

subroutine mulbyelemoff(npoi1,len,x,y,z)
  implicit none
  integer*4 :: npoi1,len
  real*8 :: x(*),y(*),z(*)
  integer*4 :: i
  !$OMP PARALLEL  DO                                     &
  !$OMP SCHEDULE ( STATIC )                              &
  !$OMP DEFAULT  ( NONE )                                &
  !$OMP SHARED   ( amatr, ia, ja, ND,V,dia )  &
  !$OMP PRIVATE  ( i, j,k)           
  do i=1+npoi1,len
     z(i) = x(i)*y(i)
  end do
  !$OMP END PARALLEL DO
end subroutine mulbyelemoff


subroutine dbsrmvoff(npoi1,nbnodes,nbvar,an,ja,ia,xx,yy) 

  use def_kintyp, only             :  ip,rp
  implicit none
  integer(ip), intent(in)          :: nbnodes,nbvar,npoi1
  real(rp),    intent(in)          :: an(nbvar,nbvar,*)
  integer(ip), intent(in)          :: ja(*),ia(*)
  real(rp),    intent(in)          :: xx(nbvar,*)
  real(rp),    intent(out), target :: yy(nbvar,*)
  integer(ip)                      :: ii,jj,kk,ll,col
  real(rp)                         :: raux,raux1,raux2,raux3


  if( nbvar == 1 ) then
     !
     ! NBVAR=1
     !
     !$OMP PARALLEL  DO                                     &
     !$OMP SCHEDULE ( STATIC )                              &
     !$OMP DEFAULT  ( NONE )                                &
     !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
     !$OMP PRIVATE  ( col, ii, jj, raux )            
     !
     do ii = npoi1+1,nbnodes
        yy(1,ii-npoi1) = 0.0_rp
        do jj   = ia(ii),ia(ii+1)-1
           col  = ja(jj)
           raux = xx(1,col)
           yy(1,ii-npoi1) = yy(1,ii-npoi1) +an(1,1,jj) * raux
        end do
     end do
     !$OMP END PARALLEL DO
  else if( nbvar == 2 ) then
     !
     ! NBVAR=2
     !
     !$OMP PARALLEL  DO                                     &
     !$OMP SCHEDULE ( STATIC )                              &
     !$OMP DEFAULT  ( NONE )                                &
     !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
     !$OMP PRIVATE  ( col, ii, jj, raux1, raux2 )    
     !
     do ii = npoi1+1,nbnodes
        yy(1,ii-npoi1) = 0.0_rp
        yy(2,ii-npoi1) = 0.0_rp
        do jj       = ia(ii),ia(ii+1)-1
           col      = ja(jj)
           raux1    = xx(1,col)
           raux2    = xx(2,col)
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(1,1,jj) * raux1
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(2,1,jj) * raux2
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(1,2,jj) * raux1
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(2,2,jj) * raux2
        end do
     end do
     !$OMP END PARALLEL DO
  else if( nbvar == 3 ) then
     !
     ! NBVAR=3
     !
     !$OMP PARALLEL  DO                                       &
     !$OMP SCHEDULE ( STATIC )                                &
     !$OMP DEFAULT  ( NONE )                                  &
     !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )    &
     !$OMP PRIVATE  ( col, ii, jj, raux1, raux2, raux3 )    
     !
     do ii = npoi1+1,nbnodes
        yy(1,ii-npoi1) = 0.0_rp
        yy(2,ii-npoi1) = 0.0_rp
        yy(3,ii-npoi1) = 0.0_rp
        do jj       = ia(ii),ia(ii+1)-1
           col      = ja(jj)
           raux1    = xx(1,col)
           raux2    = xx(2,col)
           raux3    = xx(3,col)
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(1,1,jj) * raux1
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(2,1,jj) * raux2
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(3,1,jj) * raux3
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(1,2,jj) * raux1
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(2,2,jj) * raux2
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(3,2,jj) * raux3
           yy(3,ii-npoi1) = yy(3,ii-npoi1) + an(1,3,jj) * raux1
           yy(3,ii-npoi1) = yy(3,ii-npoi1) + an(2,3,jj) * raux2
           yy(3,ii-npoi1) = yy(3,ii-npoi1) + an(3,3,jj) * raux3
        end do
        
     end do
     !$OMP END PARALLEL DO
  else if( nbvar == 4 ) then
     !
     ! NBVAR=4
     !
     !$OMP PARALLEL  DO                                     &
     !$OMP SCHEDULE ( STATIC )                              &
     !$OMP DEFAULT  ( NONE )                                &
     !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
     !$OMP PRIVATE  ( col, ii, jj, raux )            
     !
     do ii = npoi1+1,nbnodes
        yy(1,ii-npoi1) = 0.0_rp
        yy(2,ii-npoi1) = 0.0_rp
        yy(3,ii-npoi1) = 0.0_rp
        yy(4,ii-npoi1) = 0.0_rp
        do jj       = ia(ii),ia(ii+1)-1
           col      = ja(jj)
           raux     = xx(1,col)
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(1,1,jj) * raux
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(1,2,jj) * raux
           yy(3,ii-npoi1) = yy(3,ii-npoi1) + an(1,3,jj) * raux
           yy(4,ii-npoi1) = yy(4,ii-npoi1) + an(1,4,jj) * raux
           raux     = xx(2,col)
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(2,1,jj) * raux
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(2,2,jj) * raux
           yy(3,ii-npoi1) = yy(3,ii-npoi1) + an(2,3,jj) * raux
           yy(4,ii-npoi1) = yy(4,ii-npoi1) + an(2,4,jj) * raux
           raux     = xx(3,col)
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(3,1,jj) * raux
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(3,2,jj) * raux
           yy(3,ii-npoi1) = yy(3,ii-npoi1) + an(3,3,jj) * raux
           yy(4,ii-npoi1) = yy(4,ii-npoi1) + an(3,4,jj) * raux
           raux     = xx(4,col)
           yy(1,ii-npoi1) = yy(1,ii-npoi1) + an(4,1,jj) * raux
           yy(2,ii-npoi1) = yy(2,ii-npoi1) + an(4,2,jj) * raux
           yy(3,ii-npoi1) = yy(3,ii-npoi1) + an(4,3,jj) * raux
           yy(4,ii-npoi1) = yy(4,ii-npoi1) + an(4,4,jj) * raux
        end do
        
     end do
     !$OMP END PARALLEL DO
  else
     !
     ! NBVAR = whatever
     !
     !$OMP PARALLEL  DO                                            &
     !$OMP SCHEDULE ( STATIC )                                     &
     !$OMP DEFAULT  ( NONE )                                       &
     !$OMP SHARED   ( an, ia, ja, nbnodes, nbvar, npoi1, xx, yy )  &
     !$OMP PRIVATE  ( col, ii, jj, kk, ll, raux )           
     !
     do ii = npoi1+1,nbnodes
        do kk = 1,nbvar
           yy(kk,ii-npoi1) = 0.0_rp
        end do
        do jj  = ia(ii),ia(ii+1)-1
           col = ja(jj)
           do ll = 1,nbvar
              raux = xx(ll,col)
              do kk = 1,nbvar
                 yy(kk,ii-npoi1) = yy(kk,ii-npoi1) + an(ll,kk,jj) * raux
              end do
           end do
        end do
     end do
     !$OMP END PARALLEL DO
  end if
  
end subroutine dbsrmvoff

subroutine copybdr(npoi1,ND,V,x,y)
  implicit none
  real*8 :: x(*),y(*)
  integer*4 :: npoi1,ND,V
  integer*4 :: i,j
     !$OMP PARALLEL  DO                                            &
     !$OMP SCHEDULE ( STATIC )                                     &
     !$OMP DEFAULT  ( NONE )                                       &
     !$OMP SHARED   ( x, y )  &
     !$OMP PRIVATE  ( i,ND,npoi1,V)           
  do i=npoi1+1,ND
     do j=1,V
        y((i-npoi1-1)*V + j) = x((i-1)*V + j)
     end do
  end do
  !$OMP END PARALLEL DO
end subroutine copybdr
subroutine copybdrback(npoi1,ND,V,x,y)
  implicit none
  real*8 :: x(*),y(*)
  integer*4 :: npoi1,ND,V
  integer*4 :: i,j
     !$OMP PARALLEL  DO                                            &
     !$OMP SCHEDULE ( STATIC )                                     &
     !$OMP DEFAULT  ( NONE )                                       &
     !$OMP SHARED   ( x, y )  &
     !$OMP PRIVATE  ( i,ND,npoi1,V)           
  do i=npoi1+1,ND
     do j=1,V
        y((i-1)*V + j) = x((i-npoi1-1)*V + j)
     end do
  end do
  !$OMP END PARALLEL DO
end subroutine copybdrback

subroutine ddotcpu(N,x,y,ans)
  implicit none
  real*8 :: x(*),y(*),ans

  integer*4 :: N
  integer*4 :: i
  ans=0
  !$OMP PARALLEL  DO  REDUCTION(+:ans)                                          &
  !$OMP PRIVATE  ( i)
  !$OMP DEFAULT (SHARED)
  do i=1,N
     ans = ans + (x(i)*y(i))
  end do
  !$OMP END PARALLEL DO
end subroutine ddotcpu

subroutine daxpycpu(N,x,y,alpha)
  implicit none
  real*8 :: x(*),y(*),alpha

  integer*4 :: N
  integer*4 :: i

  
  !$OMP PARALLEL  DO                                            &
  !$OMP SCHEDULE ( STATIC )                                     &
  !$OMP DEFAULT  ( NONE )                                       &
  !$OMP SHARED   ( x, y,alpha,N )  &
  !$OMP PRIVATE  ( i)           
  do i=1,N
     y(i) = alpha*x(i) + y(i)
  end do
  !$OMP END PARALLEL DO
end subroutine daxpycpu

subroutine dscalcpu(N,x,alpha)
  implicit none
  real*8 :: x(*),alpha

  integer*4 :: N
  integer*4 :: i

  
  !$OMP PARALLEL  DO                                            &
  !$OMP SCHEDULE ( STATIC )                                     &
  !$OMP DEFAULT  ( NONE )                                       &
  !$OMP SHARED   ( x,alpha,N )  &
  !$OMP PRIVATE  ( i)           
  do i=1,N
     x(i) = alpha*x(i) 
  end do
  !$OMP END PARALLEL DO
end subroutine dscalcpu

