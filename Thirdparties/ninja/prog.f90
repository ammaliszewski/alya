PROGRAM P

  implicit none
  REAL*8,DIMENSION(40) :: A,AAA
  REAL*8,DIMENSION(8) :: B
  integer*4,DIMENSION(3) ::iaa,jaa
  integer*4,DIMENSION(6) ::ia
  integer*4,DIMENSION(10) ::ja
  real*8,DIMENSION(10) ::xx,yy
  real*8,DIMENSION(6) ::bb
  real*8,DIMENSION(2) ::xx_b
  real*8 :: alp,bet,r1
  real*8 :: dia(6)  
  real*8,dimension(2,2,10) :: arr 
  
  integer*4 ::i,j
  
  REAL*8,DIMENSION(16) :: agm
  REAL*8,DIMENSION(4) :: bgm
  integer*4,DIMENSION(3) ::iagm
  integer*4,DIMENSION(4) ::jagm
  real*8,DIMENSION(4) ::xgm

  A = (/1,2,0,0,3,4,2,1,1,1,2,0,0,0,1,2,0,1,0,1,1,1,2,0,1,1,1,1,1,2,2,1,1,0,0,1,0,0,0,1/)
  AAA = (/1,0,2,0,3,2,4,1,1,2,1,0,0,1,0,2,0,0,1,1,1,2,1,0,1,1,1,1,1,2,2,1,1,0,0,1,0,0,0,1/)
  B = (/0,1,0,1,1,1,2,0/)
  ja = (/1,2,2,3,1,3,3,1,2,3/)
  ia = (/1,3,5,7,8,11/)

  agm = (/2,3,3,4,4,5,5,6,4,5,5,6,6,7,7,8/)
  iagm=(/1,3,5/)
  jagm=(/1,2,1,2/)
  bgm = (/1,2,3,4/)
  xgm = (/1,0,0,0/)

  call GPUGMRES(2,2,100,2,agm,iagm,jagm,bgm,xgm)

  
END PROGRAM P
