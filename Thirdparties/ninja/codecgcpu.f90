MODULE cgcpu
  use def_kintyp,         only : ip,rp
  use def_master,         only : INOTMASTER,IMASTER,INOTSLAVE,ISEQUEN
  use def_master,         only : NPOIN_TYPE
  use def_master,         only : npoi1,npoi2,npoi3
  use mod_communications, only : PAR_SUM
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  IMPLICIT NONE
  real*8,allocatable :: p(:),u(:),s(:),exe(:)
  real*8 :: tol = 1.0e-12_rp
  INTEGER :: ti = 1
  real*8,allocatable :: dia(:)
END MODULE cgcpu

subroutine GPUCGCPU(ND,V,maxiter,amatr,ia,ja,bb,xx)
  use cgcpu
  implicit none
  integer*4 :: ND,V,maxiter
  integer*4 :: ia(*),ja(*)
  real*8 :: amatr(*),bb(*),xx(*)
  integer*4 :: NNZB,N,NV
  real*8 :: a,b,na,gammaold,r1,gamma,delta,deno,res,start,finish
  integer*4 :: k=1,NVOWN,off,exlen,error
  real*8 ::alp,bet,nalp

  call cpu_time(start)
  exlen = (ND-npoi1) * V
  off   =  npoi1 * V
  alp   =  1.0_rp
  bet   =  0.0_rp
  nalp  = -1*alp
  NNZB  = ia(ND+1) -1
  N     = NNZB * V * V
  NV    = ND * V
  if( ISEQUEN ) then
     NVOWN = NV
  else
     NVOWN = npoi3 * V
  end if

  if( INOTMASTER ) then

     if (ti == 1) then
        allocate(exe(NV),stat=error)
        allocate(p(NV),stat=error)
        allocate(u(NV),stat=error)
        allocate(s(NV),stat=error)
        allocate(dia(NV),stat=error)
        if(error.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        call GENDIAPRE(amatr,ia,ja,dia,ND,V)
        ti = ti + 1
     end if

     call MULBYELEMOFF(0,NV,dia,bb,s)
     call COPYBDR(npoi1,ND,V,s,exe)
     call PAR_INTERFACE_NODE_EXCHANGE(V,exe,'SUMI','IN MY CODE','SYNCHRONOUS')
     call COPYBDRBACK(npoi1,ND,V,exe,s)
     call DDOTCPU(NVOWN,s,bb,deno)
  end if
  call PAR_SUM(deno,'IN MY CODE')
  deno = sqrt(deno)

  if( INOTMASTER ) then

     call DBSRMVOFF(0,ND,V,amatr,ja,ia,xx,s)
     call COPYBDR(npoi1,ND,V,s,exe)
     call PAR_INTERFACE_NODE_EXCHANGE(V,exe,'SUMI','IN MY CODE','SYNCHRONOUS')
     call COPYBDRBACK(npoi1,ND,V,exe,s)
     call DAXPYCPU(NV,s,bb,nalp)
     call MULBYELEMOFF(0,NV,dia,bb,p)
     call COPYBDR(npoi1,ND,V,p,exe)
     call PAR_INTERFACE_NODE_EXCHANGE(V,exe,'SUMI','IN MY CODE','SYNCHRONOUS')
     call COPYBDRBACK(npoi1,ND,V,exe,p)
     call COPYBDR(0,ND,V,p,u)
     call DDOTCPU(NVOWN,bb,u,gamma)
  end if

  call PAR_SUM(gamma,'IN MY CODE')
  r1 = gamma/deno
  k = 1
  Print *,r1

  !  do while (r1 > tol .and. k <= maxiter)
  do while (k <= 100)
     if( INOTMASTER ) then
        call DBSRMVOFF(0,ND,V,amatr,ja,ia,p,s)
        call COPYBDR(npoi1,ND,V,s,exe)
        call PAR_INTERFACE_NODE_EXCHANGE(V,exe,'SUMI','IN MY CODE','SYNCHRONOUS')
        call COPYBDRBACK(npoi1,ND,V,exe,s)

        call DDOTCPU(NVOWN,s,p,delta)
     end if

     call PAR_SUM(delta,'IN MY CODE')
     a  = gamma/delta
     gammaold = gamma
     if( INOTMASTER ) then
        call DAXPYCPU(NV,p,xx,a)
        na = -1.0_rp * a
        call DAXPYCPU(NV,s,bb,na)
        call MULBYELEMOFF(0,NV,dia,bb,u)
        call COPYBDR(npoi1,ND,V,u,exe)
        call PAR_INTERFACE_NODE_EXCHANGE(V,exe,'SUMI','IN MY CODE','SYNCHRONOUS')
        call COPYBDRBACK(npoi1,ND,V,exe,u)

        call DDOTCPU(NVOWN,bb,u,gamma)

     end if
     call PAR_SUM(gamma,'IN MY CODE')

     b = gamma/gammaold
     if( INOTMASTER ) then

        call DSCALCPU(NV,p,b)
        call DAXPYCPU(NV,u,p,alp)
     end if

     r1 = gamma/deno

!     if( INOTSLAVE ) then
!        print*,'residual=',r1
!     end if
     k = k + 1
  enddo
  call cpu_time(finish)
  if ( INOTMASTER ) then
     print *,"Time = ",finish-start,"seconds "
  end if
  !  if ( INOTSLAVE ) then
  !     PRINT *,'Iterations and residual',k,r1
!  end if
  
end subroutine GPUCGCPU

