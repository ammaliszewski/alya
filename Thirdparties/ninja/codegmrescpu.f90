MODULE gmrescpu
  use def_kintyp,         only : ip,rp
  use def_master,         only : INOTMASTER,IMASTER,INOTSLAVE,ISEQUEN
  use def_master,         only : NPOIN_TYPE
  use def_master,         only : npoi1,npoi2,npoi3
  use mod_communications, only : PAR_SUM
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  IMPLICIT NONE
  real*8 :: tol = 1.0e-8
  INTEGER :: tiii = 1
  real*8, dimension(:), allocatable :: dia, cs,sn, s, y,forawhile,w,r,Ax,Krylov
  real*8, dimension(:,:), allocatable :: H
END MODULE gmrescpu
subroutine GPUGMRES(ND,V,maxiter,restart,amatr,ia,ja,bb,xx)
  use gmrescpu
  implicit none
  integer*4 :: ND,V,maxiter,restart
  integer*4 :: ia(*),ja(*)
  real*8 :: amatr(*),bb(*),xx(*)
  integer*4 :: NNZB,N,NV,exlen,off,NVOWN
  real*8 :: bnrm2,alp,bet,nalp,error,temp
  integer*4 :: k=1,res=1,j=1
  integer*4 :: allocerror

  Interface
     subroutine uppertrisolve(H,y,s,N)
       implicit none
       integer*4 :: N
       real*8 :: H(:,:),y(:),s(:)
     end subroutine uppertrisolve
  end Interface

  exlen = (ND-npoi1)*V
  off = npoi1 * V
  alp = 1.0
  bet = 0.0
  nalp = -1.0
  NNZB = ia(ND+1) -1
  N = NNZB * V * V
  NV = ND * V
  if( ISEQUEN ) then
     NVOWN = NV
  else
     NVOWN = npoi3 * V
  end if

!!!!!!!! INITIALIZE -----------------
  if(INOTMASTER) then
     if (tiii == 1) then
        Print *,N,ND,V,NNZB,restart
        allocate(w(NV),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        allocate(r(NV),stat=allocerror)
        allocate(Ax(NV),stat=allocerror)
        allocate(Krylov(NV*(restart+1)),stat=allocerror)
        tiii = tiii + 1
        allocate(dia(NV),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        allocate(cs(restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        cs = 0
        allocate(s(restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        s = 0
        allocate(y(restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        y = 0
        allocate(sn(restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        sn = 0
        
        allocate(H(restart+1,restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        H = 0

     end if
     call GENDIAPRE(amatr,ia,ja,dia,ND,V)     
     
!!!!!!!!!!!!INITIAL CALCULATIONS---------------------
     
     call DDOTCPU(NVOWN,bb,bb,bnrm2)
  end if
  call PAR_SUM(bnrm2,'IN MY CODE')
  bnrm2 = sqrt(bnrm2);
  if(bnrm2 == 0.0) then
     bnrm2 =1.0
  end if
  if ( INOTMASTER ) then
     call CUDADCOPY(handlebl,NV,d_b,1,d_r,1)
     call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
          d_val,d_row,d_col,V,d_x,bet,d_Ax)
     d_off = d_Ax + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8)
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8)
     call CUDADAXPY(handlebl,NV,nalp,d_Ax,1,d_r,1)
     call DMULBYELEM(d_r,d_M,d_r,NV,0)
     d_off = d_r + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8)
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8)
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_r,1,error,"Initial error ddot")
  end if
  call PAR_SUM(error,'IN MY CODE')
  error = sqrt(error) 
  temp = 1/error
  if ( INOTMASTER ) then
     call CUDADSCAL(handlebl,NV,temp,d_r,1)
     call CUDADCOPY(handlebl,NV,d_r,1,d_Krylov,1)
     s(1) = error
  end if
!!!!!---------------Starting iterations-------------------------------
  do while(k<=maxiter .and. error > tol)
     
     !!-------------KRylov iterations--------------------
     do res=1,restart
        if ( INOTMASTER ) then
           call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
                d_val,d_row,d_col,V,d_Krylov + (res-1)*NV*8,bet,d_Ax)
           d_off = d_Ax + off*8
           call MEMCPYFROMGPU(xx,d_off,exlen*8)
           call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
           call MEMCPYTOGPU(d_off,xx,exlen*8)
           call DMULBYELEM(d_Ax,d_M,d_w,NV,0)
           d_off = d_w + off*8
           call MEMCPYFROMGPU(xx,d_off,exlen*8)
           call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
           call MEMCPYTOGPU(d_off,xx,exlen*8)
        endif
!!!-------Global Re-orthogonalization----------------------------------
        
        do j=1,res
           if ( INOTMASTER ) then 
              call CUDADDOT(handlebl,NVOWN,d_w,1,d_Krylov + (j-1)*NV*8 ,1,temp,"Ortho ddot")
           end if
           call PAR_SUM(temp,'IN MY CODE')
           if ( INOTMASTER ) then
              H(j,res) = temp
              temp = -1*temp
              call CUDADAXPY(handlebl,NV,temp,d_Krylov + (j-1)*NV*8,1,d_w,1)
           end if
        end do

        if ( INOTMASTER ) then
           call CUDADDOT(handlebl,NVOWN,d_w,1,d_w,1,temp,"ortho ddot end")
        end if
        call PAR_SUM(temp,'IN MY CODE')
        temp = sqrt(temp)
        if ( INOTMASTER ) then
           
           H(res+1,res) = temp
           temp = 1/temp
           call CUDADSCAL(handlebl,NV,temp,d_w,1)
           call CUDADCOPY(handlebl,NV,d_w,1,d_Krylov + res*NV*8,1)
           
           !!------Givens Rotations------------------------------------
           do j=1,res-1
              temp = cs(j)*H(j,res) + sn(j)*H(j+1,res)
              H(j+1,res) = -1*sn(j)*H(j,res) + cs(j)*H(j+1,res)
              H(j,res) = temp
           end do
           call GIVENSROT(cs(res),sn(res),H(res,res),H(res+1,res))
           temp = cs(res)*s(res)
           s(res+1) = -1*sn(res)*s(res)
           s(res) = temp
           H(res,res) = cs(res)*H(res,res) + sn(res)*H(res+1,res)
           H(res+1,res) = 0.0
        end if
     end do
     
     !!---------upper triangle solve and linear combination------
     if ( INOTMASTER ) then
        call UPPERTRISOLVE(H,y,s,restart)
        call MEMCPYTOGPU(d_y,y,restart*8)
        call LINEARCOMBOGPU(d_x,d_Krylov,d_y,NV,restart,0)
        call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
             d_val,d_row,d_col,V,d_x,bet,d_Ax)
        d_off = d_Ax + off*8
        call MEMCPYFROMGPU(xx,d_off,exlen*8)
        call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
        call MEMCPYTOGPU(d_off,xx,exlen*8)
        call CUDADCOPY(handlebl,NV,d_b,1,d_r,1)
        call CUDADAXPY(handlebl,NV,nalp,d_Ax,1,d_r,1)
        call DMULBYELEM(d_r,d_M,d_r,NV,0)
        d_off = d_r + off*8
        call MEMCPYFROMGPU(xx,d_off,exlen*8)
        call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
        call MEMCPYTOGPU(d_off,xx,exlen*8)
        call CUDADDOT(handlebl,NVOWN,d_r,1,d_r,1,error,"Final error ddot")     
     end if
     call PAR_SUM(error,'IN MY CODE')
     if ( INOTMASTER ) then
        temp = 1/sqrt(error)
        call CUDADSCAL(handlebl,NV,temp,d_r,1)
        call CUDADCOPY(handlebl,NV,d_r,1,d_Krylov,1)
        s(1) = sqrt(error)
     end if
     error = sqrt(error)/bnrm2
     if ( INOTSLAVE) then
        print *,'ITERATION   ',k,'===>>>>   error  ',error
     end if
     k = k+1
  end do
  if ( INOTMASTER) then
     call MEMCPYFROMGPU(xx,d_x,NV*8)
  end if
end subroutine GPUGMRES

subroutine givensrot(c,s,a,b)
  implicit none
  real*8 :: c,s,a,b
  real*8 :: temp
  if (b== 0.0) then
     c = 1.0
     s = 0.0
  else if(abs(b) > abs(a)) then
     temp = a/b
     s = 1.0 / sqrt(1.0 + temp*temp)
     c = temp*s
  else
     temp = b/a
     c = 1.0 / sqrt(1.0 + temp*temp)
     s = temp*c
  end if
end subroutine givensrot

subroutine uppertrisolve(H,y,s,N)
  implicit none
  integer*4 :: N,i,j
  real*8 :: H(:,:),y(:),s(:)
  y(N) = s(N) / H(N,N)
  do i=N-1,1,-1
     do j=N,i+1,-1
        s(i) = s(i) - H(i,j)*y(j)
     end do
     y(i) = s(i) / H(i,i)
  end do
end subroutine uppertrisolve
