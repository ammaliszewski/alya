subroutine GPUGMRES(ND,V,maxiter,restart,amatr,ia,ja,bb,xx)
  use gpuvargmres
  use def_kintyp, only : ip,rp
  use def_solver, only : resin,resfi,resi1,resi2,iters,solve_sol
  implicit none
  integer*4 :: ND,V,maxiter,restart
  integer*4 :: ia(*),ja(*)
  real*8 :: amatr(*),bb(*),xx(*)
  
  integer*4 :: NNZB,N,NV,exlen,off,NVOWN
  real*8 :: bnrm2,alp,bet,nalp,error,temp,rnorm,eps
  integer*4 :: res=1,j=1
  integer*4 :: allocerror

  Interface
     subroutine uppertrisolve(H,y,s,N)
       use def_kintyp, only : ip,rp
       implicit none
       integer*4 :: N
       real*8 :: H(:,:),y(:),s(:)
     end subroutine uppertrisolve
  end Interface

  exlen = (ND-npoi1)*V
  off = npoi1 * V
  alp = 1.0_rp
  bet = 0.0_rp
  nalp = -1.0_rp
  NNZB = ia(ND+1) -1
  N = NNZB * V * V
  NV = ND * V
  if( ISEQUEN ) then
     NVOWN = NV
  else
     NVOWN = npoi3 * V
  end if
  
!!!!!!!! INITIALIZE -----------------
  if(INOTMASTER) then
     if (tiii == 1) then
        !Print *,ND,V,restart
        call GPUMALLOC(d_col,NNZB*4)
        call GPUMALLOC(d_row,(ND+1)*4)  
        call GPUMALLOC(d_val,N*8)
        call GPUMALLOC(d_x,NV*8)
        call GPUMALLOC(d_w,NV*8)
        call GPUMALLOC(d_r,NV*8)
        call GPUMALLOC(d_Ax,NV*8)
        call GPUMALLOC(d_b,NV*8)
        call GPUMALLOC(d_Krylov,NV*(restart+1)*8)
        call GPUMALLOC(d_y, restart*8)
        call GPUMALLOC(d_M,NV*8)        
        call CUBLASHANDLE(handlebl)
        call CUSPARSEHANDLE(handlesp)
        call CUSPARSE_MAT_DESCR_CREATE(descr)
        call CUSPARSE_SET_MAT_TYPE(descr,0)
        call CUSPARSE_SET_MAT_BASE(descr,1)
        tiii = tiii + 1
        allocate(dia(NV),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        allocate(cs(restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        cs = 0.0_rp
        allocate(s(restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        s = 0.0_rp
        allocate(y(restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        y = 0.0_rp
        allocate(sn(restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        sn = 0.0_rp
        
        allocate(H(restart+1,restart),stat=allocerror)
        if(allocerror.ne.0) then
           print *,'error allocating diag memory'
           stop
        end if
        H = 0
     end if
     call GENDIAPRE(amatr,ia,ja,dia,ND,V)     
     call MEMCPYTOGPU(d_col,ja,NNZB*4,"GMRES col")
     call MEMCPYTOGPU(d_row,ia,(ND+1)*4,"GMRES row")     
     call MEMCPYTOGPU(d_val,amatr,N*8,"GMRES matrix")
     call MEMCPYTOGPU(d_x,xx,NV*8,"GMRES unk")
     call MEMCPYTOGPU(d_b,bb,NV*8,"GMRES rhs")
     call MEMCPYTOGPU(d_M,dia,NV*8,"GMRES dia")
!!!!!!!!!!!!INITIAL CALCULATIONS---------------------
     
     call CUDADDOT(handlebl,NVOWN,d_b,1,d_b,1,bnrm2,"RHS DDOT")
  end if
  call PAR_SUM(bnrm2,'IN MY CODE')
  bnrm2 = sqrt(bnrm2);
  if(bnrm2 == 0.0_rp) then
     bnrm2 =1.0_rp
  end if
  if ( INOTMASTER ) then
     call CUDADCOPY(handlebl,NV,d_b,1,d_r,1)
     call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
          d_val,d_row,d_col,V,d_x,bet,d_Ax)
!     call DBSRMV(ND,ND,NNZB,d_val,d_row,d_col,V,d_x,d_Ax,0)
     d_off = d_Ax + off*8
     call MEMCPYFROMGPU(xx,d_off,exlen*8)
     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
     call MEMCPYTOGPU(d_off,xx,exlen*8)
     call CUDADAXPY(handlebl,NV,nalp,d_Ax,1,d_r,1)
!     call DMULBYELEM(d_r,d_M,d_r,NV,0)
!     d_off = d_r + off*8
!     call MEMCPYFROMGPU(xx,d_off,exlen*8)
!     call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
!     call MEMCPYTOGPU(d_off,xx,exlen*8)
     call CUDADDOT(handlebl,NVOWN,d_r,1,d_r,1,rnorm,"Initial error ddot")
  end if
  call PAR_SUM(rnorm,'IN MY CODE')
  rnorm = sqrt(rnorm)
  error = rnorm 
  temp = 1.0_rp/error
  if ( INOTMASTER ) then
     call CUDADSCAL(handlebl,NV,temp,d_r,1)
     call CUDADCOPY(handlebl,NV,d_r,1,d_Krylov,1)
     s(1) = error
  end if
  !-------------------ADAPTIVE RESIDUAL--------------------
  solve_sol(1) % reni2 = rnorm            !non-normalized residual
  solve_sol(1) % resi2 = rnorm  / bnrm2     !normalized residual
  solve_sol(1) % resin = rnorm / bnrm2     !preconditioned residual
  resin = solve_sol(1) % resin
  if( solve_sol(1) % kfl_adres == 1 ) then
     eps = max( solve_sol(1) % resin * solve_sol(1) % adres , solve_sol(1) % solmi )
  else
     eps = solve_sol(1) % solco
  end if
  tolsta = eps*bnrm2
  iters = 1
!!!!!---------------Starting iterations-------------------------------
  do while(iters<=maxiter .and. error > tolsta)
     !!-------------KRylov iterations--------------------
     do res=1,restart
        if ( INOTMASTER ) then
           call DMULBYELEM(d_Krylov + (res-1)*NV*8,d_M,d_Ax,NV,0)
           d_off = d_Ax + off*8
           call MEMCPYFROMGPU(xx,d_off,exlen*8)
           call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
           call MEMCPYTOGPU(d_off,xx,exlen*8)                   
!           call DBSRMV(ND,ND,NNZB,d_val,d_row,d_col,V,d_Ax,d_w,0)
           call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
                d_val,d_row,d_col,V,d_Ax,bet,d_w)
           d_off = d_w + off*8
           call MEMCPYFROMGPU(xx,d_off,exlen*8)
           call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
           call MEMCPYTOGPU(d_off,xx,exlen*8)
        endif
!!!-------Global Re-orthogonalization----------------------------------
        
        do j=1,res
           if ( INOTMASTER ) then 
              call CUDADDOT(handlebl,NVOWN,d_w,1,d_Krylov + (j-1)*NV*8 ,1,temp,"Ortho ddot")
           end if
           call PAR_SUM(temp,'IN MY CODE')
           if ( INOTMASTER ) then
              H(j,res) = temp
              temp = -1*temp
              call CUDADAXPY(handlebl,NV,temp,d_Krylov + (j-1)*NV*8,1,d_w,1)
           end if
        end do

        if ( INOTMASTER ) then
           call CUDADDOT(handlebl,NVOWN,d_w,1,d_w,1,temp,"ortho ddot end")
        end if
        call PAR_SUM(temp,'IN MY CODE')
        temp = sqrt(temp)
        if ( INOTMASTER ) then
           
           H(res+1,res) = temp
           temp = 1.0_rp/temp
           call CUDADSCAL(handlebl,NV,temp,d_w,1)
           call CUDADCOPY(handlebl,NV,d_w,1,d_Krylov + res*NV*8,1)
           
           !!------Givens Rotations------------------------------------
           do j=1,res-1
              temp = cs(j)*H(j,res) + sn(j)*H(j+1,res)
              H(j+1,res) = -sn(j)*H(j,res) + cs(j)*H(j+1,res)
              H(j,res) = temp
           end do
           call GIVENSROT2(cs(res),sn(res),H(res,res),H(res+1,res))
           temp = cs(res)*s(res)
           s(res+1) = -sn(res)*s(res)
           s(res) = temp
           H(res,res) = cs(res)*H(res,res) + sn(res)*H(res+1,res)
           H(res+1,res) = 0.0_rp
        end if
     end do
     
     !!---------upper triangle solve and linear combination------
     if ( INOTMASTER ) then
        call UPPERTRISOLVE(H,y,s,restart)
        call MEMCPYTOGPU(d_y,y,restart*8)
        call LINEARCOMBOGPU(d_x,d_Krylov,d_y,d_M,NV,restart,0)
!!!----update new vector and residual-------------------
!        call DBSRMV(ND,ND,NNZB,d_val,d_row,d_col,V,d_x,d_Ax,0)
        call CUDADBSRMV(handlesp,0,0,ND,ND,NNZB,alp,descr,&
             d_val,d_row,d_col,V,d_x,bet,d_Ax)
        d_off = d_Ax + off*8
        call MEMCPYFROMGPU(xx,d_off,exlen*8)
        call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
        call MEMCPYTOGPU(d_off,xx,exlen*8)
        call CUDADCOPY(handlebl,NV,d_b,1,d_r,1)
        call CUDADAXPY(handlebl,NV,nalp,d_Ax,1,d_r,1)
        !call DMULBYELEM(d_r,d_M,d_r,NV,0)
        !d_off = d_r + off*8
        !call MEMCPYFROMGPU(xx,d_off,exlen*8)
        !call PAR_INTERFACE_NODE_EXCHANGE(V,xx,'SUMI','IN MY CODE','SYNCHRONOUS')     
        !call MEMCPYTOGPU(d_off,xx,exlen*8)
        call CUDADDOT(handlebl,NVOWN,d_r,1,d_r,1,error,"Final error ddot")     
     end if
     call PAR_SUM(error,'IN MY CODE')
     error = sqrt(error)
     resi2 = resi1
     resi1 = error / bnrm2
     if ( INOTMASTER ) then
        temp = 1.0_rp/error
        call CUDADSCAL(handlebl,NV,temp,d_r,1)
        call CUDADCOPY(handlebl,NV,d_r,1,d_Krylov,1)
        s(1) = error
     end if
!     error = sqrt(error)
!     if ( INOTSLAVE) then
!        print *,'ITERATION   ',iters,'===>>>>   error  ',error,tolsta
!     end if
     iters = iters + 1
  end do
!  print *,'ITERATION and residual GMRES   ',k,error
  if ( INOTMASTER) then
     call MEMCPYFROMGPU(xx,d_x,NV*8,"GMRES unk")
  end if
  !------SUbmitting outputs--------------
  resfi = error / bnrm2
  iters = iters -1 
!  if( solve_sol(1) % kfl_cvgso == 1 ) then
!     if ( INOTSLAVE ) write(solve_sol(1) % lun_cvgso,100) iters,resfi
!  end if

100 format(i7,2(1x,e12.6))

end subroutine GPUGMRES

subroutine givensrot(c,s,a,b)
  use def_kintyp, only : ip,rp
  implicit none
  real*8 :: c,s,a,b
  real*8 :: temp
  if (b== 0.0) then
     c = 1.0_rp
     s = 0.0_rp
  else if(abs(b) > abs(a)) then
     temp = a/b
     s = 1.0_rp / sqrt(1.0_rp + temp*temp)
     c = temp*s
  else
     temp = b/a
     c = 1.0_rp / sqrt(1.0_rp + temp*temp)
     s = temp*c
  end if
end subroutine givensrot

subroutine uppertrisolve(H,y,s,N)
  use def_kintyp, only : ip,rp
  implicit none
  integer*4 :: N,i,j
  real*8 :: H(:,:),y(:),s(:)
  y(N) = s(N) / H(N,N)
  do i=N-1,1,-1
     do j=N,i+1,-1
        s(i) = s(i) - H(i,j)*y(j)
     end do
     y(i) = s(i) / H(i,i)
  end do
end subroutine uppertrisolve

subroutine givensrot2(c,s,a,b)
  use def_kintyp, only : ip,rp
 implicit none
  real*8 :: c,s,a,b
  real*8 :: temp
  temp = a*a + b*b
  temp = 1.0_rp/sqrt(temp)
  c = a * temp
  s = b * temp
end subroutine givensrot2
