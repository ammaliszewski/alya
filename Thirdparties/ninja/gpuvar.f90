MODULE GPUVARCG
  use def_kintyp,         only : ip,rp
  use def_master,         only : INOTMASTER,IMASTER,INOTSLAVE,ISEQUEN
  use def_master,         only : NPOIN_TYPE
  use def_master,         only : npoi1,npoi2,npoi3
  use mod_communications, only : PAR_SUM
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  IMPLICIT NONE
  integer*8 :: d_val,d_x,d_r,d_p,d_u,d_s,d_off,d_dia
  integer*8 :: d_col,d_row,descr,handlesp,handlebl
  INTEGER :: ti = 1
  real*8 :: tolsta = 1.0e-8_rp
  real*8,allocatable :: dia(:)
END MODULE GPUVARCG

MODULE GPUVARGMRES
  use def_kintyp,         only : ip,rp
  use def_master,         only : INOTMASTER,IMASTER,INOTSLAVE,ISEQUEN
  use def_master,         only : NPOIN_TYPE
  use def_master,         only : npoi1,npoi2,npoi3
  use mod_communications, only : PAR_SUM
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  IMPLICIT NONE
  integer*8 :: d_val,d_x,d_b,d_Krylov,d_y,d_M,d_r,d_Ax,d_w,d_off
  integer*8 :: d_col,d_row,descr,handlesp,handlebl,strcomp,strcopy
  INTEGER :: tiii = 1
  real*8 :: tolsta = 1.0e-8_rp
  real*8, dimension(:), allocatable :: dia, cs,sn, s, y,forawhile
  real*8, dimension(:,:), allocatable :: H
END MODULE GPUVARGMRES

MODULE GPUVARDEFLCG
  use def_kintyp,         only : ip,rp
  use def_master,         only : INOTMASTER,IMASTER,INOTSLAVE,ISEQUEN
  use def_master,         only : NPOIN_TYPE
  use def_master,         only : npoi1,npoi2,npoi3
  use mod_communications, only : PAR_SUM
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  IMPLICIT NONE
  integer*8 :: d_val,d_x,d_r,d_p,d_u,d_s,d_off,d_dia,d_grpmat,d_lgrp,d_iagro,d_jagro,d_grplhs,d_grprhs
  integer*8 :: d_col,d_row,descr,handlesp,handlebl,handlesl
  INTEGER :: tii = 1
  real*8 :: tolsta = 1.0e-8_rp
  real*8,allocatable :: dia(:),grpmat(:),vecallred(:),mu(:),murhs(:)
END MODULE GPUVARDEFLCG
