#ifndef _CUDA_KERNELS_
#define _CUDA_KERNELS_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define threads 128

#endif


__global__ void genbdrrowidxker(int *x,int *y,int len,int val);
__global__ void dmulbyelemker(double *x,double *y,double *out,int N);
__global__ void linearcombo(double *x,double *v,double *y,int m,int n);
__global__ void linearcombostatic(double *x,double *v,double *y,double *mm,int m,int n);
__global__ void dbsrmvkernel(double *matrix,int *row,int *col,double *rhs,double *unk,int m,int n,int dim);
__global__ void csrdeflkernel(double *matrix,int *row,int *col,int NGRP,int ND,int V,double *grpmatrix,int *iagro,int *jagro,int *lgrp);
__global__ void wtveckernel(double *small,double *big,int *lgrp,int ND,int V);
__global__ void wveckernel(double *big,double *small,int *lgrp,int ND,int V);
