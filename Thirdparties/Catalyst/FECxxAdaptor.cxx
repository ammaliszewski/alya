
// Adaptor for getting Fortran simulation code into ParaView CoProcessor.

// CoProcessor specific headers
#include "vtkCPDataDescription.h"
#include "vtkCPInputDataDescription.h"
#include "vtkCPProcessor.h"
#include "vtkCPPythonScriptPipeline.h"
#include "vtkSmartPointer.h"
#include "vtkDoubleArray.h"
#include "vtkPointData.h"
#include "vtkImageData.h"

// Fortran specific header
#include "vtkCPPythonAdaptorAPI.h"



#include "vtkCellData.h"
#include "vtkCellType.h"
#include "vtkCPAdaptorAPI.h"
#include "vtkFloatArray.h"
#include "vtkNew.h"
#include "vtkPoints.h"
#include "vtkUnstructuredGrid.h"

//#include <cstdint>
                                   


namespace
{
  vtkSmartPointer<vtkUnstructuredGrid> VTKGrid;

void BuildVTKGrid(unsigned int numberOfPoints, 
		  double* pointsData,
		  unsigned int numberOfCells,
		  unsigned int* cellsTypes,
		  unsigned int icount,
		  uint64_t* cellsData)

{   
    // create the points information
    vtkNew<vtkDoubleArray> pointArray;
    pointArray->SetNumberOfComponents(3);
    pointArray->SetArray(pointsData, static_cast<vtkIdType>(numberOfPoints*3), 1);

    vtkNew<vtkPoints> points;
    points->SetData(pointArray.GetPointer());

    VTKGrid=vtkSmartPointer<vtkUnstructuredGrid>::New();
    VTKGrid->SetPoints(points.GetPointer());
    
    // create the cells (hexa)
    VTKGrid->Allocate(static_cast<vtkIdType>(numberOfCells*9));
    for(unsigned int cell=0;cell<numberOfCells;cell++)
      {
	//unsigned int* cellPoints = cellsData+8*cell;
      uint64_t* cellPoints = cellsData+8*cell;
      vtkIdType tmp[8] = {cellPoints[0], cellPoints[1], cellPoints[2], cellPoints[3],
                          cellPoints[4], cellPoints[5], cellPoints[6], cellPoints[7]};
      VTKGrid->InsertNextCell(VTK_HEXAHEDRON, 8, tmp);
      }
      
  }
}

extern "C" void catalystcoprocess_(unsigned int* numberOfPoints, 
				   double* pointsData,
				   unsigned int* numberOfCells, 
				   unsigned int* cellsTypes,
				   unsigned int* icount,
				   uint64_t* cellsData, 
				   double* time,
				   unsigned int* timeStep)
{
  /*
  cout << "numberOfPoints = " <<  *numberOfPoints << endl; 
  for(unsigned int i=0;i<*numberOfPoints;i++)
    {
      cout << "pointsData[I] = " << i << "||||" <<  pointsData[i] << endl;
    }

  cout << "numberOfCells = " <<  *numberOfCells << endl; 
  for(unsigned int i=0;i<*numberOfCells;i++)
    {
      cout << " cellsTypes[i]= " << i << "||||" <<  cellsTypes[i] << endl;
    }
  
  cout << "icount = " <<  *icount << endl; 
  for(unsigned int i=0;i<*icount;i++)
    {
      cout << " cellsData[i]= " << i << "||||" <<  cellsData[i] << endl;
    }
  */
  vtkCPProcessor* processor = vtkCPAdaptorAPI::GetCoProcessor();
  vtkCPDataDescription* dataDescription = vtkCPAdaptorAPI::GetCoProcessorData();
  

  if(processor == NULL || dataDescription == NULL)
    {
    cerr << "ERROR: Catalyst not properly initialized.\n";
    return;
    }

  dataDescription->AddInput("input");
  dataDescription->SetTimeData(*time, *timeStep);
  //if(lastTimeStep == true)
  //{
    // assume that we want to all the pipelines to execute if it
    // is the last time step.
    //dataDescription->ForceOutputOn();
    //}
  if(processor->RequestDataDescription(dataDescription) != 0)
    {
  
      BuildVTKGrid(*numberOfPoints, pointsData, *numberOfCells,
		   cellsTypes, *icount, cellsData);
   
    dataDescription->GetInputDescriptionByName("input")->SetGrid(VTKGrid);
    processor->CoProcess(dataDescription);
    }
 
}
