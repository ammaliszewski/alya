module tcp
  use iso_c_binding
  implicit none
  public
  interface tcp_adaptor
    module procedure testcoprocessor
    end interface
    contains

      subroutine testcoprocessor(itste,ttime,npoin,coord,nelem,lnods,ltype)  
        use iso_c_binding
        use def_kintyp
        !use def_domain

        implicit none
        integer(ip), intent(in)                    :: itste,nelem,npoin
        real(rp),    intent(in)                    :: ttime
        real(rp),    dimension(:,:), intent (in)   :: coord
        integer(ip), dimension(:,:), intent (in)        :: lnods
        integer(ip), dimension(:),   intent (in)        :: ltype
        integer(ip)                                     :: flag
        !--------------------------------------------------------- 
        integer(ip)             :: inode,ielem
        integer(ip)             :: offset,istat,icount,pnode
        !-----------------------------------------------
        !ojo 
        !lnods_tmp and offset_tmp had to be integer double precision 8 bytes
        !ltype_tmp had to be integer simple precison 2 bytes
        !  
        !----------------------------------------------- 
        integer(8), pointer       :: lnods_tmp(:),offset_tmp(:)
        integer(4), pointer       :: ltype_tmp(:)
        !---------------------------------------------------------

        nullify(lnods_tmp)
        nullify(ltype_tmp)
        nullify(offset_tmp)

        offset=0
        icount=0
        do ielem=1,nelem
           if       ( ltype(ielem) == 30 ) then ! TETRA
              icount=icount+4+1
           else if  ( ltype(ielem) == 37 ) then ! HEXA
              icount=icount+8+1
           else if  ( ltype(ielem) == 32 ) then ! PYRA
              icount=icount+5+1
           else if  ( ltype(ielem) == 34 ) then ! PENTA   
              icount=icount+6+1
           endif
        enddo
        !
        ! allocate memory temporary
        !
        allocate (lnods_tmp(icount),stat=istat)
        allocate (offset_tmp(nelem),stat=istat)
        allocate (ltype_tmp(nelem),stat=istat)
        !
        ! CONNEC
        !
        icount=0
        do ielem=1,nelem
           if       ( ltype(ielem) == 30 ) then ! TETRA
              pnode = 4
              icount=icount+1
              lnods_tmp(icount)=pnode  
              do inode=1,pnode
                 icount=icount+1
                 lnods_tmp(icount)=lnods(inode,ielem)-1  
              end do
              offset = offset + pnode
              offset_tmp(ielem) = offset
              ltype_tmp(ielem) = 10
           else if  ( ltype(ielem) == 37 ) then ! HEXA
              pnode = 8
              icount=icount+1
              lnods_tmp(icount)=pnode
              do inode=1,pnode
                 icount=icount+1
                 lnods_tmp(icount)=lnods(inode,ielem)-1  
              end do
              offset = offset + pnode
              offset_tmp(ielem) = offset
              ltype_tmp(ielem) = 12
           else if  ( ltype(ielem) == 32 ) then ! PYRA
              pnode = 5
              icount=icount+1
              lnods_tmp(icount)=pnode
              do inode=1,pnode
                 icount=icount+1
                 lnods_tmp(icount)=lnods(inode,ielem)-1  
              end do
              offset = offset + pnode
              offset_tmp(ielem) = offset
              ltype_tmp(ielem) = 14
           else if  ( ltype(ielem) == 34 ) then ! PENTA
              pnode = 6
              icount=icount+1
              lnods_tmp(icount)=pnode
              do inode=1,pnode
                 icount=icount+1
                 lnods_tmp(icount)=lnods(inode,ielem)-1  
              end do
              offset = offset + pnode
              offset_tmp(ielem) = offset
              ltype_tmp(ielem) = 13
           end if
        enddo
        !--------------------------------------------------------
        !write(*,*)'nelem=',nelem
        !write(*,*)'npoin=',npoin
        !write(*,*)'icount=',icount
        !write(*,*)'size(lype_tmp)=',size(ltype_tmp)
        !write(*,*)'lype_tmp=',ltype_tmp
        !write(*,*)'size(lnods_tmp)=',size(lnods_tmp)
        !write(*,*)'lnods_tmp=',lnods_tmp
        !write(*,*)'size(offset_tmp)=',size(offset_tmp)
        !write(*,*)'offset_tmp=',offset_tmp
        !----------------------------------------
        
        call requestdatadescription(itste,ttime,flag)
        if (flag .ne. 0) then


           call catalystcoprocess(npoin, coord, nelem, ltype_tmp, icount, lnods_tmp, ttime, itste)
           call coprocess()
        end if                         
        
        !--------------------------------------------------------
        deallocate(lnods_tmp)
        deallocate(offset_tmp)
        deallocate(ltype_tmp)

        return

      end subroutine testcoprocessor
end module tcp

