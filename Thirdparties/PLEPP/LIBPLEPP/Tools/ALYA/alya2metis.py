import numpy as np 
import argparse
import sys 

 
#------------------------------------------------------------------------------#
parser = argparse.ArgumentParser()
#
#parser.add_argument('-X', action='append', dest='XXX',
#                    default=[], type=str, nargs="+",
#                    help='gcc|intel',
#                    )
#
parser.add_argument('-F', action='store', dest='F_ELEMENTS',
                    default=[''], type=str, nargs=1,
                    help='ALYA ELEMENTS',
                    )

Results = parser.parse_args()
Fname01   = Results.F_ELEMENTS[0]

if(Fname01==''):
   parser.print_help()
   print "EXIT!!\n"
   sys.exit(1)  
else: 
  print "FILE: '%s' " % (Fname01)
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
Fin = open(Fname01, 'r')
lines = Fin.readlines() 
Fin.close() 

import os
for i, line in enumerate(lines):
#  line = line.rstrip(os.linesep)
#  line = line.strip()  
  line = line.replace("  ", "")

  if( line.find("ELEMENTS") != -1 ):  
    line = line.replace('#', '')
    line = "#" + line  
  else: 
    line = line.split(" ") 
    line = " ".join( line[1:] )

  lines[i] = line 

lines.pop(0)
lines.pop( )

n_lines = len(lines)
print "n_lines: '%d' " % n_lines
lines.insert(0, "%d \n" % n_lines )

fileName, fileExtension = os.path.splitext(Fname01)
Fname02 = fileName + ".metis"
Fout = open(Fname02, "w")
Fout.writelines(lines)
Fout.close()

print "FILE: '%s' " % (Fname02)


#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
print "\n\n\t METIS_PATH/mpmetis %s n_partitions \n\n" % ( Fname02  )


#------------------------------------------------------------------------------#
print "OK!"  
