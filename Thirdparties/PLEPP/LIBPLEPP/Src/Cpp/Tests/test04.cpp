/*
  2016JAN29. BSC, BARCELONA, SPAIN, Migue Zavala 

  TETRAS, PARALLEL

  SEE: 
    MESH PARTITON: 
      alya2metis.py 

    PARTITION VISILAZATION:  
      pv_load_property01.py 
 
*/
#include <iostream> // cin, cout, endl, cerr
#include <vector>   // vector
#include <map>
#include <mpi.h>
#include <omp.h>
#include <cmath>
#include <assert.h>     /* assert */
#include <algorithm>    // std::fill
#include "commdom.hpp"
#include "read_file.hpp"


using namespace std;

string IAM   = "PLEPP";
string NAMEi = "MESH01"; 
string NAMEj = "MESH02"; 

int main(int argc, char** argv)
{
  //--------------------------------------------------------------------||---//
  int idx = -1;
  int init_col=-1;

  string  name_argv = "";
  if(argc==2) name_argv = argv[1];

  string         namei  = ":(";
  string         namej  = ":(";
  string         dicti  = ":(";

  map<string, vector<int>    >            ArrayI;
  map<string, vector<double> >            ArrayD;

  map<string,string>                      Files;
  typedef map<string,string>::iterator    FilesIt;

  map<string,MPI_Comm>                    Commij;
  typedef map<string,MPI_Comm>::iterator  It;

  if( name_argv == NAMEi )
  {
    namei         = name_argv; 
    namej         = NAMEj; 
    Commij[NAMEj] = MPI_COMM_NULL;
    // 
    string PATH = "/home/bsc21/bsc21704/z2016/REPOSITORY/COMMDOM/PLE_2016ENE25/LIBPLEPP20/Data/DiskBrake01/TETRAS/MESH01/";
    Files["types"]        = PATH +"Mesh01_TYPES.dat";       // 
    Files["connectivity"] = PATH +"Mesh01_ELEMENTS.dat";
    Files["points"]       = PATH +"Mesh01_COORDINATES.dat"; // 
    init_col = 1; 
  }

  if( name_argv == NAMEj )
  {
    namei         = name_argv; 
    namej         = NAMEi; 
    Commij[NAMEi] = MPI_COMM_NULL;
    // 
    string PATH = "/home/bsc21/bsc21704/z2016/REPOSITORY/COMMDOM/PLE_2016ENE25/LIBPLEPP20/Data/DiskBrake01/TETRAS/MESH02/";
    Files["types"]        = PATH +"Mesh02_TYPES.dat";       // 
    Files["connectivity"] = PATH +"Mesh02_ELEMENTS.dat";
    Files["points"]       = PATH +"Mesh02_COORDINATES.dat"; // 
    Files["metis"]        = PATH +"Mesh02_ELEMENTS.metis.epart.3";
    init_col = 1;                                            // NOTE: 1==ALYA TYPE 
  }
 
  vector<read_log_file> DATA( Files.size() );

  //--------------------------------------------------------------| PLEPP |---//
  MPI_Init(NULL, NULL);
  MPI_Comm PLEPP_COMM_WORLD = MPI_COMM_WORLD;

  int  app_id = -1;
  int  n_apps = -1;
  MPI_Comm  local_comm;

  // CommDom
  CommDom  CD = CommDom();
  CD.init();
  CD.set_app_type(IAM);
  CD.set_world_comm(PLEPP_COMM_WORLD);
  CD.set_app_name(namei); 
  CD.name_to_id(&app_id, &n_apps, &local_comm);
  CD.__create_interaction__();
  CD.create_commij(local_comm);
  
  int local_rank = -1;
  int local_size = -1;
  MPI_Comm_rank(local_comm, &local_rank);
  MPI_Comm_size(local_comm, &local_size);

  //---------------------------------------------------------------------||---//
  MPI_Barrier(PLEPP_COMM_WORLD);

  int n_cells = -1; 
  for(FilesIt it=Files.begin(); it!=Files.end(); it++)
  {
    string name = it->first; 
    string path = it->second;   

    read_log_file DATA; 
    DATA.set_name( path  );
    DATA.run();
    
    vector< vector<double> > vdata( DATA.get_vdata() );

    if(name=="points")
    {
      ArrayD[name] = vector<double>(0);
      for(int i=0,k=0; i<vdata.size(); i++)
      {
        for(int j=init_col; j<vdata[i].size(); j++) ArrayD[name].push_back( vdata[i][j] );        
      }
    }
    else if(name=="metis")
    {
      ArrayI[name] = vector<int>(0);
      for(int i=0,k=0; i<vdata.size(); i++)
      {
        for(int j=0; j<vdata[i].size(); j++) ArrayI[name].push_back( (int)vdata[i][j] );
      }
    }
    else
    {
      ArrayI[name] = vector<int>(0);  
      for(int i=0,k=0; i<vdata.size(); i++) 
      {
        for(int j=init_col; j<vdata[i].size(); j++) ArrayI[name].push_back( (int)vdata[i][j] ); 
      }
    }

    DATA.end();
  }

  vector<int>        types( ArrayI.find("types")->second );
  vector<int> connectivity;
  vector<double>    points( ArrayD.find("points")->second );

  //---------------------------------------------------------------------||---//
  if(false)
  {
    read_log_file DATA;
    DATA.set_name( Files.find("connectivity")->second );
    DATA.run();
    DATA.end();
    vector< vector<double> > aux( DATA.get_vdata() );

    for(int i=0; i<aux.size(); i++)
    {
      for(int j=init_col; j<aux[i].size(); j++)
      {
        connectivity.push_back( (int)aux[i][j]  );
      }
    }
    aux.clear(); 

  }

  //---------------------------------------------------------------------||---//
  if(ArrayI.find("metis") != ArrayI.end())
  {
    vector<int> gpartition( ArrayI.find("metis")->second );

    map<int, vector<int> >            parts;   
    map<int, vector<int> >::iterator  it;

    for(int i=0; i<gpartition.size(); i++)
    { 
      int key = gpartition[i]; 
      it = parts.find(key);
      if(it != parts.end())  parts[key].push_back(i);   
      else                   parts[key] = vector<int>(1,i);  
    } 

    vector<int>    lpartition( parts[local_rank] ); 
    cout<<"local_rank:"<< local_rank <<" parts:"<< lpartition.size() <<" "<< lpartition[0] ;
    cout<<"\n";

    read_log_file DATA;
    DATA.set_name( Files.find("connectivity")->second );
    DATA.run();
    DATA.end();
    vector< vector<double> > aux( DATA.get_vdata() );

    vector<int> _types( lpartition.size(), -1);
    vector<int> _connectivity;
    for(int i=0; i<lpartition.size(); i++)
    {
      int key   = lpartition[i];
      _types[i] = types[key];  

      for(int j=init_col; j<aux[key].size(); j++)
      {
        _connectivity.push_back( (int)aux[key][j]  );
      }
    }
    aux.clear();

    types.clear(); 
    types = _types; 
   _types.clear(); 

    connectivity.clear(); 
    connectivity = _connectivity; 
   _connectivity.clear(); 

  }
  else 
  {
    read_log_file DATA;
    DATA.set_name( Files.find("connectivity")->second );
    DATA.run();
    DATA.end();
    vector< vector<double> > aux( DATA.get_vdata() );

    for(int i=0; i<aux.size(); i++)
    {
      for(int j=init_col; j<aux[i].size(); j++)
      {
        connectivity.push_back( (int)aux[i][j]  );
      }
    }
    aux.clear();
  }
 
  //-----------------------------------------------------------| LOCATION |---//
  MPI_Barrier(PLEPP_COMM_WORLD);

  MPI_Comm  commij = MPI_COMM_NULL;
  if( (CD.__get_app_name__() == namei)&&(CD.__get_friends__(namej) == 1) ) commij = CD.get_mpi_commij(namej);
  if( (CD.__get_app_name__() == namej)&&(CD.__get_friends__(namei) == 1) ) commij = CD.get_mpi_commij(namei); 

    if(commij != MPI_COMM_NULL)
    {
      CD.locator_create2(local_comm, commij, 1e-3);

      int            n_vertices_i = 0;  
      double* vertex_coords_i_ptr = NULL; 
             n_vertices_i = points.size()/3; 
      vertex_coords_i_ptr = points.data(); 

      int       n_elements_i = types.size();   
      int*  vertex_num_i_ptr = connectivity.data(); 
      int* vertex_type_i_ptr = types.data(); 

      int            n_vertices_j = 0;  
      double* vertex_coords_j_ptr = NULL;  

      if(local_rank==0)
      {
               n_vertices_j = points.size()/3;
        vertex_coords_j_ptr = points.data();
      }

      CD.locator_set_cs_mesh(n_vertices_i,
                             n_elements_i,
                             vertex_coords_i_ptr,
                             vertex_num_i_ptr,
                             vertex_type_i_ptr, 
                             n_vertices_j,
                             vertex_coords_j_ptr); 

      CD.save_dist_coords(0, local_comm);

      int n_recv = CD.get_n_interior();
      int n_send = CD.get_n_dist_points();

      //CD.locator_destroy();
    }

  //--------------------------------------------------------------------||---//
  MPI_Barrier(PLEPP_COMM_WORLD);
  MPI_Finalize();

  return 0;
} // main


//=======================================================================||===//
//=======================================================================||===//
