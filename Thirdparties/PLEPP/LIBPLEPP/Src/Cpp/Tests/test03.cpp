/*
  2016JAN22. BSC, BARCELONA, SPAIN 
  Migue Zavala 

  FROM:
    test04.cpp 

  TETRAS, SERIAL 
*/
#include <iostream> // cin, cout, endl, cerr
#include <vector>   // vector
#include <map>
#include <mpi.h>
#include <omp.h>
#include <cmath>
#include <assert.h>     /* assert */
#include <algorithm>    // std::fill
#include "commdom.hpp"
#include "read_file.hpp"


using namespace std;

string IAM   = "PLEPP";
string NAMEi = "MESH01"; 
string NAMEj = "MESH02"; 

int main(int argc, char** argv)
{
  int idx = -1; 
  int init_col=-1; 

  string  name_argv = "";
  if(argc==2) name_argv = argv[1];

  //--------------------------------------------------------------------||---//

  string         namei  = ":(";
  string         namej  = ":(";
  string         dicti  = ":(";

  map<string, vector<int>    >            ArrayI;
  map<string, vector<double> >            ArrayD;

  map<string,string>                      Files;
  typedef map<string,string>::iterator    FilesIt;

  map<string,MPI_Comm>                    Commij;
  typedef map<string,MPI_Comm>::iterator  It;

  if( name_argv == NAMEi )
  {
    namei         = name_argv; 
    namej         = NAMEj; 
    Commij[NAMEj] = MPI_COMM_NULL;
    // 
    string PATH = "/home/bsc21/bsc21704/z2016/REPOSITORY/COMMDOM/PLE_2016ENE25/LIBPLEPP20/Data/SLIDING01/MESH01/"; 
    Files["types"]        = PATH +"Mesh01_TYPES.dat";       // 8040  
    Files["connectivity"] = PATH +"Mesh01_ELEMENTS.dat";
    Files["points"]       = PATH +"Mesh01_COORDINATES.dat"; // 4184 
    init_col = 1; 
  }

  if( name_argv == NAMEj )
  {
    namei         = name_argv; 
    namej         = NAMEi; 
    Commij[NAMEi] = MPI_COMM_NULL;
    // 
    string PATH = "/home/bsc21/bsc21704/z2016/REPOSITORY/COMMDOM/PLE_2016ENE25/LIBPLEPP20/Data/SLIDING01/MESH01/";  
    Files["types"]        = PATH +"A_TYPES.dat";       // 2022 
    Files["connectivity"] = PATH +"A_ELEMENTS.dat"; 
    Files["points"]       = PATH +"A_COORDINATES.dat"; // 1071  
    init_col = 1;                                            // NOTE: 1==ALYA TYPE 
  }
 
  vector<read_log_file> DATA( Files.size() );

  //--------------------------------------------------------------| PLEPP |---//
  MPI_Init(NULL, NULL);
  MPI_Comm PLEPP_COMM_WORLD = MPI_COMM_WORLD;

  int  app_id = -1;
  int  n_apps = -1;
  MPI_Comm  local_comm;

  // CommDom
  CommDom  CD = CommDom();
  CD.init();
  CD.set_app_type(IAM);
  CD.set_world_comm(PLEPP_COMM_WORLD);
  CD.set_app_name(namei);
  CD.name_to_id(&app_id, &n_apps, &local_comm);
  CD.__create_interaction__();
  CD.create_commij(local_comm);

  int local_rank = -1;
  MPI_Comm_rank(local_comm, &local_rank);

  //---------------------------------------------------------------------||---//
  MPI_Barrier(PLEPP_COMM_WORLD);

  int n_cells = -1; 
  for(FilesIt it=Files.begin(); it!=Files.end(); it++)
  {
    string name = it->first; 
    string path = it->second;   

    read_log_file DATA; 
    DATA.set_name( path  );
    DATA.run();
    
    vector< vector<double> > vdata( DATA.get_vdata() );

    if(name=="points")
    {
      ArrayD[name] = vector<double>(0);
      for(int i=0,k=0; i<vdata.size(); i++)
      {
        for(int j=init_col; j<vdata[i].size(); j++) ArrayD[name].push_back( vdata[i][j] );        
      }
    }
    else
    {
      ArrayI[name] = vector<int>(0);  
      for(int i=0,k=0; i<vdata.size(); i++) 
      {
        for(int j=init_col; j<vdata[i].size(); j++) ArrayI[name].push_back( (int)vdata[i][j] ); 
      }
//      if(name=="types") n_cells = vdata.size();  
    }

    DATA.end();
  }

  vector<int>        types( ArrayI.find("types")->second );
  vector<int> connectivity;
  vector<int>      offsets;
  vector<int>        faces;                                            //faces.data() == NULL 
  vector<int>  faceoffsets; 
  vector<double>    points( ArrayD.find("points")->second );

  //---------------------------------------------------------------------||---//

  //---------------------------------------------------------------------||---//
  if(ArrayI.find("faces") != ArrayI.end()) 
  {
    connectivity = vector<int>( ArrayI.find("connectivity")->second );
         offsets = vector<int>( ArrayI.find("offsets")->second );
           faces = vector<int>( ArrayI.find("faces")->second );
     faceoffsets = vector<int>( ArrayI.find("faceoffsets")->second );
  }
  else
  {
    read_log_file DATA;
    DATA.set_name( Files.find("connectivity")->second );
    DATA.run();
    DATA.end();
    vector< vector<double> > aux( DATA.get_vdata() ); 

    offsets    = vector<int>( aux.size()+1,-1); 
    offsets[0] = 0; 
    for(int i=0; i<aux.size(); i++)
    {
      offsets[i+1] = offsets[i] + aux[i].size() - init_col; 
      for(int j=init_col; j<aux[i].size(); j++)
      {
        connectivity.push_back( (int)aux[i][j]  ); 
      }
    } 
  }

  cout<<" n_cells:"<< types.size() ;
  cout<<" n_pts:"  << points.size()/3 <<" ";
  cout<<"\n";
//  cout<<" "<<  <<" ";
  //---------------------------------------------------------------------||---//

  //-----------------------------------------------------------| LOCATION |---//
  MPI_Barrier(PLEPP_COMM_WORLD);

  MPI_Comm  commij = MPI_COMM_NULL;
  if( (CD.__get_app_name__() == namei)&&(CD.__get_friends__(namej) == 1) ) commij = CD.get_mpi_commij(namej);
  if( (CD.__get_app_name__() == namej)&&(CD.__get_friends__(namei) == 1) ) commij = CD.get_mpi_commij(namei); 

    if(commij != MPI_COMM_NULL)
    {
      CD.locator_create2(local_comm, commij, 1e-3);

      int n_vertices_i = points.size()/3; 
      double* vertex_coords_i_ptr = points.data(); 

      int n_elements_i = types.size();   
      int* vertex_num_i_ptr  = connectivity.data(); 
      int* vertex_type_i_ptr = types.data(); 

      int n_vertices_j = points.size()/3;
      double* vertex_coords_j_ptr = points.data(); 

      CD.locator_set_cs_mesh(n_vertices_i,
                             n_elements_i,
                             vertex_coords_i_ptr,
                             vertex_num_i_ptr,
                             vertex_type_i_ptr, 
                             n_vertices_j,
                             vertex_coords_j_ptr); 

      CD.save_dist_coords(0, local_comm);

      int n_recv = CD.get_n_interior();
      int n_send = CD.get_n_dist_points();

      //CD.locator_destroy();
    }

  //--------------------------------------------------------------------||---//
  MPI_Barrier(PLEPP_COMM_WORLD);
  MPI_Finalize();

  return 0;
} // main


//=======================================================================||===//
//=======================================================================||===//
