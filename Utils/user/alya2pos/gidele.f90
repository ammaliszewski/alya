subroutine gidele(&
     mnode,npoin,nelem,lunit,lexis,ltype,lnods,coord,&
     gisca,ieles,ipois,ndime,kfl_markm,title) 

  use def_kintyp, only          :  ip,rp,cenam,nnode,cetop
  implicit none
  integer(ip),    intent(in)    :: mnode,npoin,nelem,lunit,kfl_markm,ndime
  integer(ip),    intent(in)    :: lnods(mnode,*),lexis(*),ltype(*),gisca(*)
  integer(ip),    intent(inout) :: ieles,ipois
  real(rp),       intent(in)    :: coord(ndime,*)
  character(150), intent(in)    :: title
  integer(ip)                   :: idime,ipoin,inode,ielem,pnode,ielty
  integer(ip)                   :: ifirs,ipoty
  integer(4)                    :: elnum
  character(150)                :: dumml
 
  ifirs = 0
  ipoty = 0

  do ielty = 1,100

     if( lexis(ielty) /= 0 ) then

        dumml=adjustl(trim(title)//'_'//cenam(ielty))
        !
        ! Header
        !
        write(lunit,1)&
             adjustl(trim(dumml)),max(2_ip,ndime),&
             adjustl(trim(cetop(ielty))),nnode(ielty)
        !    
        ! Coordinates
        !
        if( ifirs == 0 .and. npoin > 0 ) then
           ifirs = 1
           write(lunit,2)'coordinates'
           if(ndime==1) then
              do ipoin=1,npoin
                 write(lunit,3) ipoin+ipois,coord(1,ipoin),0.0_rp
              end do
           else
              do ipoin=1,npoin
                write(lunit,3) ipoin+ipois,(coord(idime,ipoin),idime=1,ndime)
              end do
           end if
           write(lunit,2)'end coordinates'        
        end if
        !         
        ! Connectivity
        !
        write(lunit,2)'elements'
        if( kfl_markm == 3 ) then
           do ielem=1,nelem
              if(abs(ltype(ielem))==ielty) then
                 if( ltype(ielem) < 0 ) then
                    write(lunit,4) ielem+ieles,&
                         (lnods(inode,ielem)+ipois,inode=1,nnode(ielty)),1000+gisca(ielem)                    
                 else
                    write(lunit,4) ielem+ieles,&
                         (lnods(inode,ielem)+ipois,inode=1,nnode(ielty)),gisca(ielem)
                 end if
              end if
           end do           
        else 
           do ielem=1,nelem
              if(abs(ltype(ielem))==ielty) then
                 write(lunit,4) ielem+ieles,&
                      (lnods(inode,ielem)+ipois,inode=1,nnode(ielty)),gisca(ielem)
              end if
           end do
        end if
        write(lunit,2)'end elements'

     end if

  end do

  ieles=ieles+nelem

1 format('MESH ',a,' dimension ',i1,' Elemtype ',a,' Nnode ',i2)
2 format(a)
3 format(i9, 3(1x,e16.8e3))
4 format(i9,50(1x,i9))

end subroutine gidele
