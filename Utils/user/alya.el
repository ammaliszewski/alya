;; $Id: alya.el,v 1.2 2007/01/23
;; 
;; Simple mode for Alya input files
;;
;; Author: G. Houzeaux
;;         based on the tochnog template done by Martin L�thi, tn@tnoo.net
;;         and Miguel A. Pasenau de Riera, miguel@cimne.upc.es
;;
;;
;; Version 0.1, 2007.01.23
;;
;; 
;; Add to your .emacs
;;
;; (load-library "alya")
;; (load-alya-modes)
;;
;; or
;; 
;; (autoload 'alya-dat-mode "alya" "Alya mode" t)
;; (setq auto-mode-alist (append '(("\\.dat$" . alya-dat-mode)) auto-mode-alist))
;;
;; (autoload 'alya-fix-mode "alya" "Alya .fix mode" t)
;; (setq auto-mode-alist (append '(("\\.fix$" . alya-fix-mode)) auto-mode-alist))
;; 
;;
;; (-*- alya -*-)
;;

(defun fortran-fontify-string (limit)
  (let ((match (match-string 1)))
    (cond ((string= "'" match)
	   (re-search-forward "\\([^'\n]*'?\\)" limit))
	  ((string= "\"" match)
	   (re-search-forward "\\([^\"\n]*\"?\\)" limit)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; For Alya fixity file (boundary conditions)
;;

(defvar alya-fix-mode-hook nil)

;; all Alya keywords
(defvar alya-fix-font-lock-keywords
  (list

   '("END_VALUES"                 . font-lock-function-name-face)
   '("END_FUNCTIONS"              . font-lock-function-name-face)
   '("END_FUNCTION"               . font-lock-function-name-face)
   '("END_ON_NODES"               . font-lock-function-name-face)
   '("END_ON_BOUNDARIES"          . font-lock-function-name-face)
   '("VALUES"                     . font-lock-function-name-face)
   '("FUNCTIONS"                  . font-lock-function-name-face)
   '("FUNCTION"                   . font-lock-function-name-face)
   '("ON_NODES"                   . font-lock-function-name-face)
   '("ON_BOUNDARIES"              . font-lock-function-name-face)
   
   )
  )

(defvar alya-comment-prefix "(** "
  "*The comment `alya-insert-comment' inserts."
  )


;;;###autoload
(defun alya-fix-mode ()
  "Major mode for editing Alya input files."
  (interactive)

  ;; set up local variables
  (kill-all-local-variables)
  (make-local-variable 'font-lock-defaults)
  (make-local-variable 'paragraph-separate)
  (make-local-variable 'paragraph-start)
  (make-local-variable 'require-final-newline)
  (make-local-variable 'comment-start)
  (make-local-variable 'comment-end)
  (make-local-variable 'comment-start-skip)
  (make-local-variable 'comment-column)
  (make-local-variable 'comment-indent-function)
  (make-local-variable 'indent-region-function)
  (make-local-variable 'indent-line-function)
  (make-local-variable 'add-log-current-defun-function)

  (setq font-lock-defaults '(alya-fix-font-lock-keywords nil t))
  (setq major-mode          'alya-fix-mode
	mode-name               "Alya .fix"
	font-lock-defaults      '(alya-fix-font-lock-keywords nil t)
	paragraph-separate      "^[ \t]*$"
	paragraph-start         "^[ \t]*$"
	require-final-newline   t
	comment-start           ""
	comment-end             ""
	comment-start-skip      ""
	comment-column          40
	comment-indent-function 'alya-comment-indent-function
	indent-region-function  'alya-indent-region
	indent-line-function    'alya-indent-line
	)

  ;; Run the mode hook.
  (if alya-fix-mode-hook
      (run-hooks 'alya-fix-mode-hook))
  )

(provide 'alya-fix-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Data and fixity
;;

(defvar alya-dat-mode-hook nil)
(defvar alya-dat-font-lock-keywords
  (list

;;
;; All fonts:
;; ----------
;; font-lock-string-face         rose
;; font-lock-comment-face        dark red
;; font-lock-variable-name-face  yellow
;; font-lock-type-face           light green
;; font-lock-function-name-face  light blue
;; font-lock-defaults            white
;;

;;
;; GENERAL
;;
   '("$-"                        . font-lock-comment-face)
   '("-"                         . font-lock-comment-face)
   '("ALYA:"                     . font-lock-comment-face)
   '("INCLUDE"                   . font-lock-variable-name-face)
   '("BAR02"                     . font-lock-variable-name-face)
   '("BAR03"                     . font-lock-variable-name-face)
   '("BAR04"                     . font-lock-variable-name-face) 
   '("TRI03"                     . font-lock-variable-name-face) 
   '("TRI06"                     . font-lock-variable-name-face) 
   '("QUA04"                     . font-lock-variable-name-face) 
   '("QUA08"                     . font-lock-variable-name-face) 
   '("QUA09"                     . font-lock-variable-name-face) 
   '("QUA16"                     . font-lock-variable-name-face) 
   '("TET04"                     . font-lock-variable-name-face) 
   '("TET10"                     . font-lock-variable-name-face) 
   '("PYR05"                     . font-lock-variable-name-face) 
   '("PYR14"                     . font-lock-variable-name-face) 
   '("PEN06"                     . font-lock-variable-name-face) 
   '("PEN15"                     . font-lock-variable-name-face) 
   '("PEN18"                     . font-lock-variable-name-face) 
   '("HEX08"                     . font-lock-variable-name-face) 
   '("HEX20"                     . font-lock-variable-name-face) 
   '("HEX27"                     . font-lock-variable-name-face) 
   '("HEX64"                     . font-lock-variable-name-face) 
;;
;; IMPORTANT WORDS
;;
   '("BIN"                       . font-lock-variable-name-face)
   '("BINARY"                    . font-lock-variable-name-face)
   '("ASCII"                     . font-lock-variable-name-face)
;;
;; MUST BE BEFORE BECAUSE THEY INCLUDE COLORED WORD
;;
   '("END_SPACE_&_TIME_FUNCTIONS" . font-lock-type-face)
   '("END_TIME_FUNCTIONS"         . font-lock-type-face)
   '("END_SPACE_FUNCTIONS"        . font-lock-type-face)
   '("SPACE_FUNCTIONS"            . font-lock-type-face)
   '("UNKNOWN_BOUNDARIES"         . font-lock-type-face)
   '("END_FUNCTIONS"              . font-lock-type-face)
   '("END_NODES"                  . font-lock-type-face)
   '("END_ON_NODES"               . font-lock-type-face)
   '("END_ON_BOUNDARIES"          . font-lock-type-face)
   '("END_GEOMETRICAL_CONDITIONS" . font-lock-type-face)
   '("END_SKEW_SYSTEMS"           . font-lock-type-face)
   '("END_MATERIALS"              . font-lock-comment-face)
   '("END_MATERIAL"               . font-lock-comment-face)
   '("SPACE_&_TIME_FUNCTIONS"     . font-lock-type-face)
   '("TIME_FUNCTIONS"             . font-lock-type-face)
   '("FUNCTIONS"                  . font-lock-type-face)
   '("ON_NODES"                   . font-lock-type-face)
   '("ON_BOUNDARIES"              . font-lock-type-face)
   '("GEOMETRICAL_CONDITIONS"     . font-lock-type-face)
   '("PERIODIC_NODES"             . font-lock-type-face)
   '("END_NODES_PER_ELEMENT"      . font-lock-type-face)
   '("NODES_PER_ELEMENT"          . font-lock-type-face)
   '("END_BOUNDARIES"             . font-lock-type-face)
;;
;; NASTIN
;;
   '("TAU_STRATEGY"              . font-lock-defaults)
   '("SCHUR_COMPLEMENT"          . font-lock-variable-name-face)
   '("RICHARDSON"                . font-lock-variable-name-face)
   '("ORTHOMIN"                  . font-lock-variable-name-face)
   '("MOMENTUM_PRESERVING"       . font-lock-variable-name-face)
   '("CONTINUITY_PRESERVING"     . font-lock-variable-name-face)
   '("END_MOMENTUM"              . font-lock-type-face)
   '("END_HYDROSTATIC_PRESSURE"  . font-lock-type-face)
   '("END_CONTINUITY"            . font-lock-type-face)
   '("END_ALGORITHM"             . font-lock-type-face)
   '("MOMENTUM"                  . font-lock-type-face)
   '("HYDROSTATIC_PRESSURE"      . font-lock-type-face)
   '("CONTINUITY"                . font-lock-type-face)
   '("ALGORITHM"                 . font-lock-type-face)
;;
;; DOMAIN
;;
   '("NODAL_POINTS"              . font-lock-type-face)
   '("SPACE_DIMENSIONS"          . font-lock-type-face)
   '("TYPES_OF"                  . font-lock-type-face)
   '("TYPES_OF_ELEMENTS"         . font-lock-type-face)
   '("NODES"                     . font-lock-type-face) 
   '("BOUNDARIES"                . font-lock-type-face)
   '("SKEW_SYSTEMS"              . font-lock-type-face)
   '("MATERIALS"                 . font-lock-comment-face)
   '("MATERIAL"                  . font-lock-comment-face)
   '("INTEGRATION_RULE"          . font-lock-type-face)
   '("END_IMMERSED_BOUNDARY"     . font-lock-type-face)
   '("IMMERSED_BOUNDARY"         . font-lock-type-face)
   '("DOMAIN_INTEGRATION_POINTS" . font-lock-type-face)
   '("SCALE"                     . font-lock-type-face)
   '("OUTPUT_MESH_DATA"          . font-lock-type-face)
   '("OUTPUT:"                   . font-lock-type-face)
   '("END_ELSEST"                . font-lock-type-face)
   '("ELSEST"                    . font-lock-type-face)
   '("PERIODICITY_STRATEGY"      . font-lock-type-face)
   '("END_DIMENSIONS"            . font-lock-function-name-face)
   '("END_STRATEGY"              . font-lock-function-name-face)
   '("DIMENSIONS"                . font-lock-function-name-face)
   '("STRATEGY"                  . font-lock-function-name-face)
   '("END_SETS"                  . font-lock-function-name-face)
   '("SETS"                      . font-lock-function-name-face)
;;
;; GEOMETRY
;;
   '("END_GEOMETRY"              . font-lock-function-name-face)
   '("END_TYPES_OF_ELEMENTS"     . font-lock-type-face)
   '("END_PERIODICITY"           . font-lock-type-face)
   '("END_ELEMENTS"              . font-lock-type-face)
   '("END_COORDINATES"           . font-lock-type-face)
   '("GEOMETRY"                  . font-lock-function-name-face)
   '("ELEMENTS"                  . font-lock-type-face)
   '("TYPES_OF_ELEMENTS"         . font-lock-type-face)
   '("BOUNDARIES"                . font-lock-type-face)
   '("COORDINATES"               . font-lock-type-face)
   '("ELEMENTS"                  . font-lock-type-face)
   '("PERIODICITY"               . font-lock-type-face)
;;
;; RUN DATA
;;
   '("ON_LAST_MESH"              . font-lock-variable-name-face)
   '("ON_ORIGINAL_MESH"          . font-lock-variable-name-face)
   '("GRAVITY"                   . font-lock-variable-name-face)
   '("DENSITY"                   . font-lock-variable-name-face)
   '("VISCOSITY"                 . font-lock-variable-name-face)
   '("DIVISION"                  . font-lock-variable-name-face)
   '("ROUGHNESS"                 . font-lock-variable-name-face)
   '("U_REFERENCE"               . font-lock-variable-name-face)
   '("H_REFERENCE"               . font-lock-variable-name-face)
   '("K_REFERENCE"               . font-lock-variable-name-face)
   '("US_REFERENCE"              . font-lock-variable-name-face)
   '("WALL_LAW"                  . font-lock-variable-name-face)
   '("END_ALGEBRAIC_SOLVER"      . font-lock-variable-name-face)
   '("ALGEBRAIC_SOLVER"          . font-lock-variable-name-face)
   '("PRECONDITIONING"           . font-lock-variable-name-face)
   '("PRECONDITIONER"            . font-lock-variable-name-face)
   '("EXTENDED_GRAPH"            . font-lock-variable-name-face)

   '("RUN_TYPE"                  . font-lock-type-face)
   '("LATEX_INFO_FILE"           . font-lock-type-face)
   '("CUSTOMER"                  . font-lock-type-face)
   '("POSTPROCESS"               . font-lock-type-face)
   '("OUTPUT_FORMAT"             . font-lock-type-face)
   '("LIVE_INFORMATION"          . font-lock-type-face)
   '("LOG_FILE"                  . font-lock-type-face)
   '("LIVE_INFO"                 . font-lock-type-face)
   '("LOT_OF_MEMORY"             . font-lock-type-face)
   '("MEMORY"                    . font-lock-type-face)
   '("TIME_COUPLING"             . font-lock-type-face)
   '("TIME_INTERVAL"             . font-lock-type-face)
   '("TIME_STEP_SIZE"            . font-lock-type-face)
   '("NUMBER_OF_STEPS"           . font-lock-type-face)
   '("MAXIMUM_NUMBER_GLOBAL"     . font-lock-type-face)
   '("END_BLOCK_ITERATION"       . font-lock-function-name-face)
   '("END_RUN_DATA"              . font-lock-function-name-face)
   '("END_PROBLEM_DATA"          . font-lock-function-name-face)
   '("BLOCK_ITERATION"           . font-lock-function-name-face)
   '("RUN_DATA"                  . font-lock-function-name-face)
   '("PROBLEM_DATA"              . font-lock-function-name-face)
   '("END_WITNESS_POINTS"        . font-lock-type-face)
   '("WITNESS_POINTS"            . font-lock-type-face)
   '("END_FILTERS"               . font-lock-type-face)
   '("FILTERS"                   . font-lock-type-face)
   '("END_LAGRANGIAN_PARTICLES"  . font-lock-type-face)
   '("LAGRANGIAN_PARTICLES"      . font-lock-type-face)
   '("END_MESH"                  . font-lock-type-face)
   '("MESH"                      . font-lock-type-face)
;;
;; RUN DATA: MODULES AND SERVICES
;;
   '("END_NASTIN_MODULE"         . font-lock-comment-face)
   '("END_TEMPER_MODULE"         . font-lock-comment-face)
   '("END_CODIRE_MODULE"         . font-lock-comment-face)
   '("END_TURBUL_MODULE"         . font-lock-comment-face)
   '("END_EXMEDI_MODULE"         . font-lock-comment-face)
   '("END_NASTAL_MODULE"         . font-lock-comment-face)
   '("END_ALEFOR_MODULE"         . font-lock-comment-face)
   '("END_LATBOL_MODULE"         . font-lock-comment-face)
   '("END_APELME_MODULE"         . font-lock-comment-face)
   '("END_SOLIDZ_MODULE"         . font-lock-comment-face)
   '("END_GOTITA_MODULE"         . font-lock-comment-face)
   '("END_WAVEQU_MODULE"         . font-lock-comment-face)
   '("END_LEVELS_MODULE"         . font-lock-comment-face)
   '("END_QUANTY_MODULE"         . font-lock-comment-face)
   '("END_PARTIS_MODULE"         . font-lock-comment-face)
   '("END_CHEMIC_MODULE"         . font-lock-comment-face)
   '("END_HELMOZ_MODULE"         . font-lock-comment-face)
   '("END_IMMBOU_MODULE"         . font-lock-comment-face)
   '("END_RADIAT_MODULE"         . font-lock-comment-face)
   '("END_POROUS_MODULE"         . font-lock-comment-face)

   '("NASTIN_MODULE"             . font-lock-comment-face)
   '("TEMPER_MODULE"             . font-lock-comment-face)
   '("CODIRE_MODULE"             . font-lock-comment-face)
   '("TURBUL_MODULE"             . font-lock-comment-face)
   '("EXMEDI_MODULE"             . font-lock-comment-face)
   '("NASTAL_MODULE"             . font-lock-comment-face)
   '("ALEFOR_MODULE"             . font-lock-comment-face)
   '("LATBOL_MODULE"             . font-lock-comment-face)
   '("APELME_MODULE"             . font-lock-comment-face)
   '("SOLIDZ_MODULE"             . font-lock-comment-face)
   '("GOTITA_MODULE"             . font-lock-comment-face)
   '("WAVEQU_MODULE"             . font-lock-comment-face)
   '("LEVELS_MODULE"             . font-lock-comment-face)
   '("QUANTY_MODULE"             . font-lock-comment-face)
   '("PARTIS_MODULE"             . font-lock-comment-face)
   '("CHEMIC_MODULE"             . font-lock-comment-face)
   '("HELMOZ_MODULE"             . font-lock-comment-face)
   '("IMMBOU_MODULE"             . font-lock-comment-face)
   '("RADIAT_MODULE"             . font-lock-comment-face)
   '("POROUS_MODULE"             . font-lock-comment-face)
  
   '("NASTIN"                    . font-lock-variable-name-face)
   '("TEMPER"                    . font-lock-variable-name-face)
   '("CODIRE"                    . font-lock-variable-name-face)
   '("TURBUL"                    . font-lock-variable-name-face)
   '("EXMEDI"                    . font-lock-variable-name-face)
   '("NASTAL"                    . font-lock-variable-name-face)
   '("ALEFOR"                    . font-lock-variable-name-face)
   '("LATBOL"                    . font-lock-variable-name-face)
   '("APELME"                    . font-lock-variable-name-face)
   '("SOLIDZ"                    . font-lock-variable-name-face)
   '("GOTITA"                    . font-lock-variable-name-face)
   '("WAVEQU"                    . font-lock-variable-name-face)
   '("LEVELS"                    . font-lock-variable-name-face)
   '("QUANTY"                    . font-lock-variable-name-face)
   '("PARTIS"                    . font-lock-variable-name-face)
   '("CHEMIC"                    . font-lock-variable-name-face)
   '("HELMOZ"                    . font-lock-variable-name-face)
   '("IMMBOU"                    . font-lock-variable-name-face)
   '("RADIAT"                    . font-lock-variable-name-face)
   '("POROUS"                    . font-lock-variable-name-face)

   '("END_HDFPOS_SERVICE"        . font-lock-comment-face)
   '("END_PARALL_SERVICE"        . font-lock-comment-face)
   '("END_DODEME_SERVICE"        . font-lock-comment-face)
   '("HDFPOS_SERVICE"            . font-lock-comment-face)
   '("PARALL_SERVICE"            . font-lock-comment-face)
   '("DODEME_SERVICE"            . font-lock-comment-face)

   '("POSTPROCESS"               . font-lock-type-face)
   '("OUTPUT_FILE"               . font-lock-type-face)
   '("TASK"                      . font-lock-type-face)
   '("PARTITION_TYPE"            . font-lock-type-face)
   '("COMMUNICATIONS"            . font-lock-type-face)
   '("FILE_HIERARCHY"            . font-lock-type-face)
   '("VIRTUAL_FILE"              . font-lock-type-face)

;;
;; MODULES
;;
   '("END_PHYSICAL_PROBLEM"      . font-lock-function-name-face)
   '("END_PROBLEM_DEFINITION"    . font-lock-type-face)
   '("END_PROPERTIES"            . font-lock-type-face)
   '("END_NUMERICAL_TREATMENT"   . font-lock-function-name-face)
   '("END_SUBGRID_SCALE_MODELING". font-lock-type-face)
   '("END_TIME_PROBLEM"          . font-lock-type-face)
   '("END_INNER_ITERATIONS"      . font-lock-type-face)
   '("END_OTHERS"                . font-lock-type-face)

   '("END_OUTPUT_&_POST_PROCESS" . font-lock-function-name-face)
   '("END_OUTPUT_&_POSTPROCESS"  . font-lock-function-name-face)
   '("END_BOUNDARY_CONDITIONS"   . font-lock-function-name-face)

   '("END_ELEMENT_SET"           . font-lock-type-face)
   '("END_BOUNDARY_SET"          . font-lock-type-face)
   '("END_NODE_SET"              . font-lock-type-face)
   '("END_CODES"                 . font-lock-type-face)
   '("END_PARAMETERS"            . font-lock-type-face)

   '("PHYSICAL_PROBLEM"          . font-lock-function-name-face)
   '("PROBLEM_DEFINITION"        . font-lock-type-face)
   '("PROPERTIES"                . font-lock-type-face)
   '("NUMERICAL_TREATMENT"       . font-lock-function-name-face)
   '("SUBGRID_SCALE_MODELING"    . font-lock-type-face)
   '("TIME_PROBLEM"              . font-lock-type-face)
   '("INNER_ITERATIONS"          . font-lock-type-face)
   '("OTHERS"                    . font-lock-type-face)

   '("OUTPUT_&_POST_PROCESS"     . font-lock-function-name-face)
   '("OUTPUT_&_POSTPROCESS"      . font-lock-function-name-face)
   '("BOUNDARY_CONDITIONS"       . font-lock-function-name-face)

   '("ELEMENT_SET"               . font-lock-type-face)
   '("BOUNDARY_SET"              . font-lock-type-face)
   '("NODE_SET"                  . font-lock-type-face)
   '("CODES"                     . font-lock-type-face)
   '("PARAMETERS"                . font-lock-type-face)
;;
;; CHEMIC
;;
   '("END_REACTIONS"             . font-lock-variable-name-face)
   '("END_CLASSES"               . font-lock-variable-name-face)
   '("END_ODES"                  . font-lock-variable-name-face)
   '("ODES"                      . font-lock-variable-name-face)
   '("CLASSES"                   . font-lock-variable-name-face)
   '("REACTIONS"                 . font-lock-variable-name-face)

   )
  )

(defvar alya-comment-prefix "(** "
  "*The comment `alya-insert-comment' inserts."
  )


;;;###autoload
(defun alya-dat-mode ()
  "Major mode for editing Alya input files."
  (interactive)
  ;; set up local variables
  (kill-all-local-variables)
  (make-local-variable 'font-lock-defaults)
  (make-local-variable 'paragraph-separate)
  (make-local-variable 'paragraph-start)
  (make-local-variable 'require-final-newline)
  (make-local-variable 'comment-start)
  (make-local-variable 'comment-end)
  (make-local-variable 'comment-start-skip)
  (make-local-variable 'comment-column)
  (make-local-variable 'comment-indent-function)
  (make-local-variable 'indent-region-function)
  (make-local-variable 'indent-line-function)
  (make-local-variable 'add-log-current-defun-function)
  ;;
  (setq font-lock-defaults '(alya-dat-font-lock-keywords nil t))
  ;;(set-syntax-table alya-dat-mode-syntax-table)
  (setq major-mode          'alya-dat-mode
	mode-name               "Alya .dat"
	font-lock-defaults      '(alya-dat-font-lock-keywords nil t)
	paragraph-separate      "^[ \t]*$"
	paragraph-start         "^[ \t]*$"
	require-final-newline   t
	comment-start           ""
	comment-end             ""
	comment-start-skip      ""
	comment-column          40
	comment-indent-function 'alya-comment-indent-function
	indent-region-function  'alya-indent-region
	indent-line-function    'alya-indent-line
	)

  ;; Run the mode hook.
  (if alya-dat-mode-hook
      (run-hooks 'alya-dat-mode-hook))
  )

(provide 'alya-dat-mode)

(defun load-alya-modes ()
  ;; para que al cargar un .dat/.fix se cargue el modo Alya.  
  (autoload 'alya-fix-mode "alya" "Alya .fix mode" t)
  (setq auto-mode-alist (append '(("\\.fix$" . alya-fix-mode)) auto-mode-alist))
  
  (autoload 'alya-dat-mode "alya" "Alya .dat mode" t)
  (setq auto-mode-alist (append '(("\\.dat$" . alya-dat-mode)) auto-mode-alist))
)
