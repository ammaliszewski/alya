program alya_particule

  implicit none
  integer(4)            :: one4=1,two4=2
  integer(4)            :: i,ii,jj,kk,j,nline,modu
  real(8)               :: xcoord,ycoord,zcoord,xvelo,yvelo,zvelo,time,r
  integer(4)            :: subdom,part,max_part,max_particule,part_id,part_type
  character(150)        :: fil_name,name
  character(4)          :: cpart,num
  integer(4), pointer   :: family(:)
  logical               :: dir_e

  nline=0
  part=0

  call GETARG(one4,name)
  call GETARG(two4,num)
  
  fil_name    = trim(name)//'.pts.res'

  if(len(trim(name))==0)then
     write(6,*) &
          '--| Usage: alya-particule [name] [num]'
     write(6,*) '--|'
     write(6,*) '--|'
     write(6,*) '--| Try again motherfucker !'
     write(6,*) '--|'
     stop
  end if

  write(6,*) '--|'
  write(6,*) '--| Alya-particule converter '
  write(6,*) '--|'
  

  read (num,'(I4)') modu

  if(len(trim(num))==0)then
     modu=1
     write(6,*) '--|'
     write(6,*) '--| You are processing all the particules !!!'
     write(6,*) '--|'
  end if
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
  open(unit=10,file=fil_name,status='old')
  do 
     read(10,*,end=106)
     nline= nline+1
  end do
106 continue
  close(10)
  nline=nline-13
  write(6,*) '--|'
  write(6,*) '--| nline = ',nline
  write(6,*) '--|'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  open(unit=10,file=fil_name,status='old')
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  do ii =1,nline
     read (10,*) time,part_id,xcoord,ycoord,zcoord,xvelo,yvelo,zvelo,part_type,subdom,r,r,r,r,r
     max_part=max(part_id,part)
     part=max_part
  end do
  close (10)
  write(6,*) '--|'
  write(6,*) '--| max_part = ',max_part
  write(6,*) '--|'

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  allocate (family(max_part))
  do i=1,max_part
     family(i) = 0
  enddo

  open(unit=10,file=fil_name,status='old')
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  do ii =1,nline
     read (10,*) time,part_id,xcoord,ycoord,zcoord,xvelo,yvelo,zvelo,part_type,subdom,r,r,r,r,r
     if (time <= 0.100000E-02 .and. zcoord  >= 0.00672873) then
        !write(*,*)" TYPE 1 ",part_id,family(part_id)
        if (family(part_id) == 0) family(part_id) = 1  
     else
        !write(*,*)" TYPE 2 ",part_id,family(part_id)
        if (family(part_id) == 0) family(part_id) = 2
     end if
  end do
  close (10)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  inquire(file='paraview_particule.csv', exist=dir_e)
  if ( dir_e ) then
     !write(*,*)"exist file"
  else
     open (12, file="paraview_particule.csv", status="new")
     close(12)
  end if
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  open(12, file="paraview_particule.csv", status="old")
  open(unit=10,file=fil_name,status='old')
  write(12,*)'time,part_id,xcoord,ycoord,zcoord,xvelo,yvelo,zvelo,part_type,subdom,family'
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  read(10,*)
  do ii =1,nline
     read (10,*) time,part_id,xcoord,ycoord,zcoord,xvelo,yvelo,zvelo,part_type,subdom,r,r,r,r,r
     if (mod(part_id,modu)==0) write(12,83) time,part_id,xcoord,ycoord,zcoord,xvelo,yvelo,zvelo,part_type,subdom,family(part_id)
  end do
  close (10)
  close (12)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

83 format (e12.6,',',i5,',',e12.6,',',e12.6,',',e12.6,',',e12.6,',',e12.6,',',e12.6,',',i1,',',i4',',i1)
  deallocate (family)

end program alya_particule
 
