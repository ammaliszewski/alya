   subroutine Postpr
  ! This routines writes 
  ! 1) The obtained time step profiles to a file 
  ! 2) Global variables after each time step
     ! 3) Point evolution variables after each time step
     ! And results to a given lenght
     
     use def_master
     implicit none
     integer :: kpoin
     real(8) :: richf, grvel(2), prodt, prodm
     real(8) :: tstar,  gpnut, facto, grtem, htflx, stres, lenmo, htpbl, stre0
     real(8) :: velcu(2,mcuts), keycu(mcuts), epscu(mcuts), temcu(mcuts)
     character*20 :: label, file





     ! set file names 
     ! profiles
     ! cuts
     ! global variables

     !
     ! Write global evolution variables
     !
     gpnut =  cmu*keyva(1,1)*keyva(1,1)/epsil(1,1)
     ! qwall positive in stable atm, when floor is cold (heat flux going out)
     hflx0 =  rhocp*gpnut*(tempe(2,1)- tempe(1,1))/(coord(2)-coord(1))/sigte
     tstar =  hflx0/(rhocp*ustar)
     lenmo = ustar*ustar*teref/(kar*gravi*tstar)
     stre0 = ustar*ustar*densi
! looks for Height top boundary layer
     do ipoin = 2, npoin
        gpnut = densi*cmu*keyva(ipoin,1)*keyva(ipoin,1)/epsil(ipoin,1)       
        grvel(1) =(veloc(ipoin,1,1)-veloc(ipoin-1,1,1)) /(coord(ipoin)-coord(ipoin-1))
        grvel(2) =(veloc(ipoin,2,1)-veloc(ipoin-1,2,1)) /(coord(ipoin)-coord(ipoin-1))              
        stres = gpnut*sqrt(grvel(1)*grvel(1)+grvel(2)*grvel(2))
        kpoin = ipoin
        if (stres.lt.0.05*stre0) exit
     end do
     ! height top bound layer
     htpbl =coord(kpoin)

     write (lun_globa,'(20(e17.10,2x))')  ctime , ustar, hflx0, tstar, tewal, lenmy, lenmo, htpbl
     !
     ! Write cut evolution
     !
     do icuts =1, ncuts      
        ! values at coord icuts
        ipoin = ielec(icuts)
        jpoin = ipoin +1
        facto = (cutpr(icuts)-coord(ipoin))/(coord(jpoin)-coord(ipoin))
        ! interpolate values
        velcu(1:2,icuts) = veloc(ipoin,1:2,1) + facto*(veloc(jpoin,1:2,1)-veloc(ipoin,1:2,1))    
        keycu(icuts) = keyva(ipoin,1) + facto*(keyva(jpoin,1)-keyva(ipoin,1))
        epscu(icuts) = epsil(ipoin,1) + facto*(epsil(jpoin,1)-epsil(ipoin,1))
        temcu(icuts) = tempe(ipoin,1) + facto*(tempe(jpoin,1)-tempe(ipoin,1))
        write(lun_cutre(icuts),'(10(e17.10,2x))') ctime, velcu(1,icuts), velcu(2,icuts), &
             keycu(icuts), epscu(icuts), temcu(icuts)
     end do

     !
     ! Write profiles at some time steps
     !      

     if (mod(istep, stepr)==0) then

        write(label,'(f6.2)') ctime/3600.0 - mod(ctime/3600,1.0)*0.4 ! time in hours         
        file = 'plotprofile_'//trim(adjustl(label))  ! plotprof_hh.hh
        open(lun_timre, FILE=file,status='unknown')
        write(lun_timre,'(a)') '#1: coord          2:velocx          3:velocy          &
             4: modvel          5:key            6:eps             7: mutur         &
             8: mixlen         9:temp            10:rich_flux      11: prodm        &
12: prodt         13:htflx          14:stress '
        
        do ipoin = 2, npoin
           gpnut = cmu*keyva(ipoin,1)*keyva(ipoin,1)/epsil(ipoin,1)


           ! production distribution
           !            prodm =0.0d0
           !            prodt =0.0d0
           !            richf = 0.0d0
           !            htflx =0.0d0
           !            if (ipoin.gt.1) then
           grvel(1) =(veloc(ipoin,1,1)-veloc(ipoin-1,1,1)) /(coord(ipoin)-coord(ipoin-1))
           grvel(2) =(veloc(ipoin,2,1)-veloc(ipoin-1,2,1)) /(coord(ipoin)-coord(ipoin-1))      
           grtem =(tempe(ipoin,1)-tempe(ipoin-1,1)) /(coord(ipoin)-coord(ipoin-1))          
           prodm  =densi*gpnut*(grvel(1)*grvel(1)+grvel(2)*grvel(2)) 
           sigte =0.74d0
           if (grtem.lt.0.0d0) then ! unstable
              richf  = gravi/teref*grtem/(sigte*(grvel(1)*grvel(1)+grvel(2)*grvel(2)))
              sigte = 0.74d0*(1.0d0-15.d0*richf)**(-0.25d0)
              richf  = richf*0.74d0/sigte
              sigte = 0.74d0*(1.0d0-15.d0*richf)**(-0.25d0)
           end if
           ! thermal production
           prodt  = -densi*gravi/teref*gpnut*grtem/sigte
           ! richardson flux
           Richf  = prodt/prodm         

           htflx = -rhocp*gpnut/sigte*grtem
           stres = densi*gpnut*sqrt(grvel(1)*grvel(1)+grvel(2)*grvel(2))
          


           write (lun_timre, '(20(e17.10, x))') coord(ipoin),veloc(ipoin,1,1),veloc(ipoin,2,1), &
                sqrt(veloc(ipoin,1,1)*veloc(ipoin,1,1)+ veloc(ipoin,2,1)*veloc(ipoin,2,1)),  &
                keyva(ipoin,1), epsil(ipoin,1), gpnut, &
                ((cmu*keyva(ipoin,1)*keyva(ipoin,1))**(0.75d0))/epsil(ipoin,1), tempe(ipoin,1) , &
                Richf, prodm, prodt,htflx, stres


        end do
        close (lun_timre)
     end if


   end subroutine Postpr
