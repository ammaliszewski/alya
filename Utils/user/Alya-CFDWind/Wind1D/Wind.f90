      program Wind
!-----------------------------------------------------------------------
!****f* master/zephyr
! NAME
!    Zephyr
! DESCRIPTION
!    This is the main routine of the program. It controls the main flow 
!    of the program by calling routines that drive specific tasks 
! USES
!    Turnon
!    Begste
!    Doiter
!    Gencon
!    Errest
!    Newmsh
!    Endste
!    Turnof
!***
!-----------------------------------------------------------------------
        use      def_master
        implicit none
        integer :: kfl_gotim
            
        call Reapro  ! Reads program data
        call Turnon  ! Turns on problem
        kfl_gotim = 1
        istep = 0
        ctime = 0.0d0
        if (kfl_trtem) ctime = 16.0*3600.0 !initial time for thermal transient
        time: do while (kfl_gotim==1)  ! Advances in time
           call Begste
           iiter = 0  ! Global iteration
           do while(iiter.lt.miite)   
              iiter = iiter +1
              call Doiter ! solves all variables
           end do
           ! Updates unknowns
           do ipoin = 1, npoin
              veloc(ipoin, 1,2)=veloc(ipoin, 1,1)
              veloc(ipoin, 2,2)=veloc(ipoin, 2,1)
              keyva(ipoin,2)=keyva(ipoin,1)
              epsil(ipoin,2)=epsil(ipoin,1)
              tempe(ipoin,2)=tempe(ipoin,1)
           end do         
           ! postprocess time step results only for transient problems
           if (kfl_trtem) call postpr
           if (istep.eq.nstep) kfl_gotim =0     
        end do time
        call Turnof  ! Writes restart and postprocess info

     end program Wind
     
