  subroutine readinp
!********************************************************
!*
!*    Reads the properties from the input file
!*
!********************************************************
  use KindType
  use InpOut
  use def_Master
  implicit none
!
  integer(ip)           :: istat,ivoid(1)
  real   (rp)           :: rvoid(mcuts)
  character(len=s_mess) :: message,cvoid

 ! initializations 
  dtinv  = 1.0d0
  kfl_thcou = .false.
  kfl_trtem = .false.
  kfl_canop = .false.
  lmoni =0.0_rp
  ztsbl = 0.0_rp
  tewal = teref
  ncuts = 0
  kfl_canmo =0 ! Sogachev's canopy model (default)
  kfl_candi =0 ! uniform canopy distribution (default)

!
!*** Writes to the log file
!
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Reading input data file...',/)
!
!*** Reads the GENERAL BLOCK
! 

  call get_input_int(finp,'GENERAL','model',ivoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  kfl_model = ivoid(1)
!
  call get_input_int(finp,'GENERAL','nstep',ivoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  nstep = ivoid(1)
!
  call get_input_int(finp,'GENERAL','miite',ivoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  miite = ivoid(1)
 

  dtinv = 2.0d0 ! default time step
  call get_input_rea(finp,'GENERAL','timst',rvoid,1,istat,message) 
  
  if(istat.ne.0) call wriwar(message) 

  if (istat.eq.0)   dtinv = rvoid(1)
  dtinv = 1.0d0/dtinv

!
!*** Reads the PHYSICAL BLOCK
!

  call get_input_rea(finp,'PHYSICAL','fcori',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  fcori = rvoid(1)

  call get_input_rea(finp,'PHYSICAL','vegeo',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  vegeo = rvoid(1)

  call get_input_rea(finp,'PHYSICAL','ustar',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  ustar = rvoid(1)

  call get_input_rea(finp,'PHYSICAL','l_max',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  l_max = rvoid(1)
  if (abs(fcori).lt.1.0d-9) then  
     l_max=1.0d50
  elseif (abs(l_max).lt.0.01) then
     l_max = 0.00027*vegeo/fcori
  end if
  print *, 'l_max=', l_max

  call get_input_rea(finp,'PHYSICAL','rough',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  rough = rvoid(1)
!
!*** Reads the MESH BLOCK
!

  call get_input_rea(finp,'MESH','ztop',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  length = rvoid(1)

  call get_input_rea(finp,'MESH','hele1',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  dz1 = rvoid(1)

  call get_input_int(finp,'GENERAL','nelem',ivoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  nelem = ivoid(1)
!
!*** Reads the BOUNDARY BLOCK
!
  call get_input_int(finp,'BOUNDARY','bouco',ivoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  kfl_bouco_vel = ivoid(1)

!
!*** Reads the NUMERICAL BLOCK
!

  call get_input_rea(finp,'NUMERICAL','dwall',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  dwall = rvoid(1)

  call get_input_rea(finp,'NUMERICAL','toler',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  toler(1) = rvoid(1)
  toler(2) = toler(1)
  toler(3) = toler(1)*0.01d0
  toler(4) = toler(3)
  toler(5) = toler(3) 

  call get_input_rea(finp,'NUMERICAL','mitve',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  maxit(1) = rvoid(1)
  maxit(2) = maxit(1)

  call get_input_rea(finp,'NUMERICAL','mittu',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  maxit(3) = rvoid(1)
  maxit(4) = maxit(3)


!
!*** Reads the Thermal coupling block
!

  call get_input_int(finp,'THERMAL','thmod',ivoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  kfl_thmod = ivoid(1) ! thermal model
  
  if (kfl_thmod.ne.0) then
     call get_input_rea(finp,'THERMAL','heatfl',rvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     hflx0 = rvoid(1) ! heat flux over the ground
     if (abs(hflx0).gt.0.01) kfl_thcou = .true. 
     if (abs(hflx0).lt.0.01) kfl_thmod = 0

     call get_input_rea(finp,'THERMAL','height',rvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     ztsbl = rvoid(1) ! z top sbl
     if (kfl_thmod.ne.1) ztsbl =1.0d50
     
     
     call get_input_rea(finp,'THERMAL','molen',rvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     lmoni = rvoid(1) !  Monin - Obukhov length (positive, stabla, negative: unstable)

     call get_input_cha(finp,'THERMAL','trans',cvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     if (TRIM(cvoid).eq.'YES'.or.TRIM(cvoid).eq.'yes'.or.&
          TRIM(cvoid).eq.'On'.or.TRIM(cvoid).eq.'on') &
          kfl_trtem =.true.
     
 ! defines MO function parameters
     alpha_mo = 16.0d0  
     beta_mo  = 5.00d0
     expon_mo = - 0.25d0
  end if
  !
  !*** Reads the Postprocess block
  !  
  if (kfl_trtem) then
     ! Postproces at some time steps
     call get_input_rea(finp,'POSTPROCESS','steps',rvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     stepr = rvoid(1)
     iiter = 0

     !Tracking point (height)
     call get_input_rea(finp,'POSTPROCESS','cutheight',rvoid,mcuts,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)

     cutpr(1:mcuts) = rvoid(1:mcuts)
     print *, 'rvoid', cutpr
     do icuts =1, mcuts
        if (cutpr(icuts).le.dwall.or.cutpr(icuts).gt.length) &
             exit
     end do
     ncuts = icuts -1
     print *, 'ncuts', ncuts
  end if
 
  if (kfl_thmod.eq.4.and..not.kfl_trtem) call runend('READINP: ERROR CALCULATION SHOULD BE TRANSIENT')

  !
  !*** Reads the canopy coupling block
  !

  call get_input_cha(finp,'CANOPY','canopy',cvoid,1,istat,message)
!  if(istat.gt.0) call wriwar(message)
!  if(istat.lt.0) call runend(message)
  if (TRIM(cvoid).eq.'YES'.or.TRIM(cvoid).eq.'yes'.or.TRIM(cvoid).eq.'On'.or.TRIM(cvoid).eq.'on') &
       kfl_canop =.true.

  if (kfl_canop) then
     call get_input_rea(finp,'CANOPY','cd',rvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     cdcan = rvoid(1) ! drag coeff

     call get_input_rea(finp,'CANOPY','LAD',rvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     LAD = rvoid(1) ! Leaf area density
     

     call get_input_rea(finp,'CANOPY','height',rvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     heica = rvoid(1) ! Height of the canopy 


     call get_input_cha(finp,'CANOPY','model',cvoid,1,istat,message)
!     if(istat.gt.0) call wriwar(message)
!     if(istat.lt.0) call runend(message)
     if (TRIM(cvoid).eq.'SVENS') then
        kfl_canmo =1
        print *, 'CANOPY MODEL=SVENS'
     else if (TRIM(cvoid).eq.'SANZ') then
        kfl_canmo =2
        print *, 'CANOPY MODEL=SANZ'
     else if (TRIM(cvoid).eq.'GREEN') then
        kfl_canmo =3
        print *, 'CANOPY MODEL=GREEN'
     else if (TRIM(cvoid).eq.'LOPES') then
        kfl_canmo =4
        print *, 'CANOPY MODEL=LOPES'
     end if


     call get_input_cha(finp,'CANOPY','distribution',cvoid,1,istat,message)
     !     if(istat.gt.0) call wriwar(message)
     !     if(istat.lt.0) call runend(message)
     if (TRIM(cvoid).eq.'LALIC') then ! from Lalic and mihailovic
        kfl_candi =1
     else if (TRIM(cvoid).eq.'TABLE') then  ! Table distr. (Lopes da Costa phd-thesis)
        kfl_candi =2
     end if

     if (kfl_candi==2) then
        call get_input_rea(finp,'CANOPY','LAI',rvoid,1,istat,message)        
        if(istat.gt.0) call wriwar(message)
        if(istat.lt.0) call runend(message)
        LAI = rvoid(1) ! Leaf area index
     end if
   

     
     
  end if

  

  return
end subroutine readinp
