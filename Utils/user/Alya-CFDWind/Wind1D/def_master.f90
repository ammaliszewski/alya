   module def_master
     !------------------------------------------------------------------------
     !    
     ! Heading for the master routines. This module contains the global varibles
     ! of the code.
     !
     !------------------------------------------------------------------------
     
!     real*8, parameter :: kar = 0.410d0
     !
     ! Logical units
     !
     integer, parameter :: &
          lun_rstar = 121 , lun_postp= 122, lun_globa = 123, lun_timre = 124,  mcuts = 10
     real, parameter :: &
          pi = 3.14159265358979323846d0,  &
!          rhocp = 1234.5    ! rho 1.229 cp =1004.5          
          rhocp = 1232.9,  & ! rho 1.225 cp =1006.43 (Allinot-Masson)
          gravi = 9.81d0,  & ! gravity acceleration
          teref = 283.15d0,& ! reference temperature
          keyam = 1.0d-4,  & ! ambient key 
          epsam = 7.208d-8   ! ambient eps
        

     integer ::  lun_conve(5),  lun_cutre(mcuts)
     data  lun_conve  /101, 102, 103, 104, 105/
     data  lun_cutre  /111, 112, 113, 114, 115, 116,117, 118, 119, 120/ !cut results
     
     integer :: igaus, inode, ielem,  itime, ipoin, iiter, &
          kfl_goite, izdom, jnode, jpoin, istep, icuts, &
          kfl_close, & ! close integration rule
          kfl_order, & ! finite element interpolation order
          kfl_model, & ! k eps model: 0:Limited length,1: RNG,2: Realizable
          kfl_thmod ! thermal coupling model 1: Apsley and Castro, 2: Alinot-Mason 3:Crespo

     real*8,pointer  ::  &    ! GLOBAL
          unkno(:),      &    ! unknown to the solver
          veloc(:,:,:),  &    ! Velocity  NSTINC
          tempe(:,:),  &    ! potential temperature
          keyva(:,:),    &    ! unkno: turbulent kinetic energy
          epsil(:,:),    &    ! unkno: turbulent dissipation
          veloc_ini(:,:),&    ! unkno: turbulent dissipation
          keyva_ini(:),  &    ! unkno: turbulent dissipation
          epsil_ini(:),  &    ! unkno: turbulent dissipation
          amatr(:),      &    ! solver matrix
          avect(:),      &
          diago(:),      &
          cvect(:),      &
          rhsid(:),      &
          shape(:,:),    & 
          deriv(:,:),    & 
          deri2(:,:),    & 
          coord(:),      &
          weigp(:),      &
          posgp(:)


     real*8  ::  &    ! GLOBAL
          dtinv, ugeos(2), vegeo, ustar, rough, densi,     &
          dwall, toler(5), c1, c2, &
          cmu, sigka, sigep, fcori, l_max, ctime, length, kar, dz1, cmu0, &
          hflx0,ztsbl, sigte, lmoni, alpha_mo, beta_mo, expon_mo, a(6,4), &
          tewal, &   ! wall temp, for transient temp problem, it should be a transient function
          cutpr(mcuts), &  ! cuts to be ploted
          lenmy, &         ! length mellor yamada
          tetop, &            ! temperature at top
          cdcan, LAD,  heica, LAI, ustar_Can


     integer*4, pointer  :: &
          lnods(:,:),       &
          ia(:),            &
          ja(:)
          
     integer*4 ::       &
          nzdom,        &     !3*(npoin-2) + 4 = 3*npoin -2  
          npoin,        &     !number of nodes
          nelem,        &     !number of element s
          ngaus,        &     !number of integration points (usually 2)       
          nnode,        &     !number of nodes per element (usually 2)
          nstep,        &     !maximum number of time steps of the run
          kfl_bouco_vel,&     !kind of veloc boundary condition
          maxit(4),     & 
          miite,        &
          ncuts,        &     ! number od heiught cuts to postprocess
          stepr,        &     ! profiles each stepr steps
          ielec(mcuts), &     ! element to who icut belongs   
          kfl_canmo,    &     ! canopy model
          kfl_candi           ! canopy distribution
     logical ::   &
          kfl_thcou, kfl_trtem, kfl_canop
        




    end module def_master
