subroutine Begste
  !-----------------------------------------------------------------------
  !****f* doiter
  ! NAME 
  !    doiter
  ! DESCRIPTION
  !    This routine sets some variable before solving a time step
  !    equations.
  ! USES
  !
  !
  ! USED BY
  ! Wind
  !***
  !-----------------------------------------------------------------------

  use      def_master
  implicit none
  real :: htime

  istep = istep +1
  
  if (istep.gt.1.and.dtinv.gt.3.0e-4.and..not.kfl_trtem) then
     !              dtinv=max(dtinv*0.7d0,2.0e-4)
!     dtinv=dtinv*0.94d0
     dtinv=max (dtinv*0.94d0,2.0e-3 )     
  end if
  
  ctime = ctime +1.0d0/dtinv
!  print *, 'ctime',ctime 
  
  if (kfl_trtem) then     
     htime =ctime/3600.0d0 !Current time in hurs
!     htime = 16.0d0
     print *, htime
     !     if (ctime.lt.1000) tewal = teref - 0.0001d0*ctime
     if (htime.lt.17.4d0) then
        tewal = -10.0d0 -25.0d0*cos(0.22d0*htime+0.2d0)
     else if (htime.lt.30d0) then
        tewal = -0.54d0*htime +15.2d0
     else if (htime.lt.41.9d0) then
        tewal = -7.0d0 -25.0d0*cos(0.21d0*htime+1.8d0)
     else if (htime.lt.53.3d0) then
        tewal = -0.37d0*htime +18.0d0
     else if (htime.lt.65.6d0) then
        tewal = -4.0d0 -25.0d0*cos(0.22d0*htime+2.5d0)
     else
        tewal = 4.4d0
     end if
     tewal = tewal + 273.15d0 

  end if
  

end subroutine begste
