subroutine turnon
  ! This routines reads initial and boundary condition

  use def_master
  implicit none

  real(8)    ::  z,  ustar1, dz, distz, rageo, tnume, tdeno,  sqkey, gpcod
  integer    ::  flag,kpoin
  character*20 :: label, file
  
  if  (kfl_model==0) then !CFDW2
! Original Jones-Launder , 1972 
     kar =0.41d0
     cmu=0.09d0
     c1= 1.44d0
     c2=1.92d0
     sigka=1.0d0
     sigep=1.11d0  

!Crespo et al  ! From Crespo et al (Used in Alya)
     kar =0.41d0
     cmu=0.0333d0
     c1= 1.176d0
     c2= 1.92d0
     sigka= 1.0d0
     sigep= kar*kar/(c2-c1)/sqrt(cmu)    

!  Detering 
!     kar = 0.4
!     cmu=0.0256d0
!     c1= 1.13d0
!     c2=1.90d0
!     sigka=0.7407d0
!     sigep=1.2987d0  

! Sogachev
!     kar =0.4d0
!     cmu=0.09d0
!     c1= 1.52d0
!     c2= 1.833d0
!     sigka= 1.0d0
!     sigep= kar*kar/(c2-c1)/sqrt(cmu)    

! Koblitz
!     kar =0.4d0
!     cmu= 0.03d0
!     c1=  1.52d0
!     c2=  1.833d0
!     sigep= kar*kar/(c2-c1)/sqrt(cmu)
!     sigka= sigep
     if (kfl_canmo == 2) then ! SANZ model
!        Atmospheric (Lopes da Costa)
        kar =0.40d0
        cmu= 0.0333d0
        c1=  1.44d0
        c2=  1.92d0
        sigka = 1.0d0
        sigep = kar*kar/(c2-c1)/sqrt(cmu)
     end if
!     
  else if (kfl_model==1) then ! RNG
     kar =0.41d0
     cmu=0.085d0
     c1= 1.42d0
     c2=1.68d0
     sigka=0.7179d0
     sigep=0.7179d0  
  else if (kfl_model==2) then ! REALIZABLE
     kar =0.41d0
     cmu=0.09d0
     c1= 0.0d0
     c2=1.9d0
     sigka=1.0d0
     sigep=1.2d0  
  else

     write(*,*) 'ERROR, NO CFWIND1 NO CFWIND2'
     stop
  end if
  
  if (kfl_thmod ==2 ) then !Alinot and Masson table for C3
     ! stable and z/L < 1/3 
     a(1,1) = 4.181d0
     a(2,1) = 33.994d0 
     a(3,1) = -442.398d0 
     a(4,1) = 2368.12d0 
     a(5,1) = -6043.544d0 
     a(6,1) = 5970.776d0 

     ! stable and z/L > 1/3 
     a(1,2) =5.225d0
     a(2,2) =-5.269d0
     a(3,2) =5.115d0 
     a(4,2) =-2.406d0
     a(5,2) =0.435d0 
     a(6,2) =0.000d0 
     ! unstable and z/L < 1/4 
     a(1,3) =-0.0609d0 
     a(2,3) =-33.672d0 
     a(3,3) =-546.880d0
     a(4,3) =-3234.06d0 
     a(5,3) =-9490.792d0
     a(6,3) =-11163.202d0 
     ! unstable and z/L > 1/4 
     a(1,4) =1.765d0
     a(2,4) =17.1346d0
     a(3,4) =19.165d0
     a(4,4) =11.912d0
     a(5,4) =3.821d0
     a(6,4) =0.492d0

  end if
  sigte = 0.9d0
  if (kfl_thmod==2 ) sigte =1.0d0
  cmu0=cmu

  ! velocity components on top (geostrophic)
  ugeos(1) =  vegeo
  ugeos(2) =  0.0d0 ! geostrophic paralel to x direction 

  !  GAUSS POINTS AND WEIGHTS
  if (kfl_order==2) then  
     ngaus= 3
     nnode= 3
     npoin = 2*nelem +1
  end if
  allocate(weigp(ngaus))
  allocate(posgp(ngaus))
  if (ngaus==2.and.kfl_close==1) then
     write(*,*) 'closed integration rule'
     posgp(1)=-1.0d0
     posgp(2)= 1.0d0
     weigp(1)= 1.0d0
     weigp(2)= 1.0d0
  else if (ngaus==2) then
     posgp(1)=-0.577350269189626d0
     posgp(2)= 0.577350269189626d0
     weigp(1)= 1.0d0
     weigp(2)= 1.0d0
  else if(ngaus==3) then
     posgp(1)=-0.774596669241483d0
     posgp(2)= 0.0d0
     posgp(3)= 0.774596669241483d0
     weigp(1)= 0.555555555555556d0
     weigp(2)= 0.888888888888889d0
     weigp(3)= 0.555555555555556d0
  else if(ngaus==4)  then
     posgp(1)=-0.861136311594053d0
     posgp(2)=-0.339981043584856d0
     posgp(3)= 0.339981043584856d0
     posgp(4)= 0.861136311594053d0
     weigp(1)= 0.347854845137454d0
     weigp(2)= 0.652145154862546d0
     weigp(3)= 0.652145154862546d0
     weigp(4)= 0.347854845137454d0
  end if
  ! Definition of shape functions
  allocate(shape(nnode, ngaus))
  do igaus=1, ngaus
     z= posgp(igaus)
     shape(1,igaus)= -0.5d0*(z-1.0d0)
     shape(2,igaus)=  0.5d0*(z+1.0d0)
  end do
  if (kfl_order==2) then
     allocate(deriv(nnode, ngaus))
     allocate(deri2(nnode, ngaus))
     do igaus=1, ngaus
        z= posgp(igaus)
        shape(1,igaus)=  0.5d0*z*(z-1.0d0)            !  1    3    2
        shape(2,igaus)=  0.5d0*z*(z+1.0d0)
        shape(3,igaus)= -(z+1.0d0)*(z-1.0d0)
        deriv(1,igaus)= z - 0.5d0
        deriv(2,igaus)= z + 0.5d0
        deriv(3,igaus)=-2.0d0*z
        deri2(1,igaus)= 1.0d0
        deri2(2,igaus)= 1.0d0
        deri2(3,igaus)= -2.0d0
     end do
  end if
  

  !
  ! Allocate structures
  !
  allocate (lnods(nnode,nelem))
  allocate (unkno(npoin))
  allocate (rhsid(npoin))
  allocate (coord(npoin))
  allocate (avect(npoin))
  allocate (diago(npoin))
  allocate (cvect(npoin))
  allocate (veloc(npoin, 2, 2))
  allocate (keyva(npoin, 2))
  allocate (epsil(npoin, 2))
  allocate (veloc_ini(npoin, 2))
  allocate (keyva_ini(npoin))
  allocate (epsil_ini(npoin)) 
  allocate (ia(npoin+1))
!  if (kfl_trtem) 
  allocate (tempe(npoin,2))
  !
  !
  !  
  if (kfl_order==1) then 
     nzdom = 3*npoin -2  
     allocate (ja(nzdom))
     allocate(amatr(nzdom))
     ! conectivity matrix
     do ielem = 1, nelem 
        do inode = 1, nnode
           lnods(inode, ielem) = ielem +inode -1      
        end do
     end do
     ! matrix index (csr)

     ia(1) = 1  
     ja(1) = 1
     ja(2) = 2
     izdom = 3

     do ipoin = 2, npoin -1
        ia(ipoin)= izdom
        ja(izdom)= ipoin -1
        ja(izdom+1)= ipoin
        ja(izdom+2)= ipoin +1 
        izdom =izdom + 3
     end do
     ia(npoin)= izdom
     ja(izdom)= npoin -1
     ja(izdom+1)= npoin 
     nzdom =izdom + 1
!     write(*,*)'nzdom=', nzdom
     ia(npoin+1) = nzdom +1
  else if (kfl_order==2) then ! second order conectivity matrix

     nzdom =5*(nelem-1) + 3*(nelem+2)
     allocate (ja(nzdom))
     allocate(amatr(nzdom))
     ! conectivity matrix
     ipoin =1
     do ielem =1, nelem
        lnods(1,ielem)= ipoin
        lnods(2,ielem)= ipoin +2
        lnods(3,ielem)= ipoin +1
        ipoin = ipoin +2
     end do
     
     ! matrix index (csr)

     ia(1) = 1  
     ja(1) = 1
     ja(2) = 2
     ja(3) = 3
     izdom = 4

     do ipoin = 2, npoin -1
        
        ia(ipoin)= izdom
        if (mod(ipoin,2)==0) then !even
           ja(izdom)= ipoin -1
           ja(izdom+1)= ipoin
           ja(izdom+2)= ipoin +1 
           izdom =izdom + 3
        else  !odd
           ja(izdom)  = ipoin -2
           ja(izdom+1)= ipoin -1
           ja(izdom+2)= ipoin  
           ja(izdom+3)= ipoin +1
           ja(izdom+4)= ipoin +2
           izdom =izdom + 5
        end if
     end do
     ! last row
     ia(npoin)= izdom
     ja(izdom)= npoin -2
     ja(izdom+1)= npoin  -1
     ja(izdom+2)= npoin 
     nzdom =izdom + 2
     write(*,*)'nelem=', nelem
     write(*,*)'npoin=', npoin
     write(*,*)'nzdom=', nzdom
     ia(npoin+1) = nzdom +1

  else
     write(*,*) 'ERROR: KFL_ORDER SHOULD BE ONE OR TWO'
  end if


  !
  !Coordinates and Initial conditions
  !


  ustar1 = vegeo*kar/log(1.0d0+length/rough)    
 ! wall distance and first element length
!  dwall = 5.0*rough
!  dz1  = 5.0*rough! *0.1d0
  dz = dz1 
  rageo = 1.01d0
  ielem = int(nelem*1.0)
  do while ((rageo**(ielem)-1.0d0)/(rageo-1.0d0).lt.length/dz1) 
     rageo = rageo + 0.002d0
  end do
  write (*,*) 'geometrical ratio (r+2)=', rageo+0.02
  flag=0
  do ipoin=1, npoin
     if (ipoin.eq.1) z=0.0d0
     if (ipoin.eq.2) z= dz
     if (ipoin.ne.1.and.ipoin.ne.2) then        
        if (z.lt.6.0*dz1) then 
           dz = dz*(rageo+0.02d0) !max(dz*1.5d0, 2.0d0*c/dfloat(nez))
        else if (z.lt.40.0*dz1) then 
           dz = dz*(rageo+0.02d0) !max(dz*1.5d0, 2.0d0*c/dfloat(nez))
        else
           dz=  dz*(rageo+0.02d0)
        end if
        distz= (length-z)/dfloat(npoin-ipoin+1)
!        write (*,*) 'z=',z
        if (dz.gt.distz.or.flag==1) then
           
           
           if (flag==0) write (*,*)  'uniform length factor=', (z+distz)/length, 'at z= ', z+distz, 'ipoin=', ipoin,'unilength=',distz, dz
!           write (*,*) 'distz',distz
           dz = distz      
           flag=1
        end if
        z = z + dz
     end if
     !Vertical coordinate
     coord(ipoin)= z +dwall
     !initial condition
     veloc_ini(ipoin,1) = ustar1/kar*log(1.0d0+(z+dwall)/rough)
     veloc_ini(ipoin,2) = 0.0d0
     keyva_ini(ipoin) = ustar*ustar/sqrt(cmu)
!     lm =kar*(z+rough)*l_max/(kar*(z+rough)+l_max)
!     epsil_ini(ipoin) =  ((cmu*keyva_ini(ipoin)*keyva_ini(ipoin))**(0.75d0))/lm
     epsil_ini(ipoin) = ustar*ustar*ustar/kar/(z+dwall+rough)
!     initial temp for transient temp case     
  end do
  if (flag==0) then
     write(*,*) 'ERROR constructing mesh, correct input parameters \\ bad length, factor=', z/length, 'rageo', rageo
     stop
  end if
!  write(*,*) 'length factor=', z/length, 'rageo', rageo

  !
  ! Initialize unknown
  !  
  do ipoin =1, npoin
     veloc(ipoin, 1, 1) = veloc_ini(ipoin,1)
     veloc(ipoin, 2, 1) = veloc_ini(ipoin,2)
     keyva(ipoin, 1) =  keyva_ini(ipoin) 
     epsil(ipoin, 1) =  epsil_ini(ipoin) 
     veloc(ipoin, 1,2)=veloc(ipoin, 1,1)
     veloc(ipoin, 2,2)=veloc(ipoin, 2,1)
     keyva(ipoin,2)=keyva(ipoin,1)
     epsil(ipoin,2)=epsil(ipoin,1)     
  end do
  if (kfl_trtem) then  ! Initial solution temper
     do ipoin = 1, npoin
!        tempe(ipoin,1) = teref + coord(ipoin)/100.0d0
        !GABLS2
        if (coord(ipoin).lt.200.0) then
           tempe(ipoin,1) = 288.0 -2.0*coord(ipoin)/200.0
        else if (coord(ipoin).lt.850.0) then
           tempe(ipoin,1) = 286.0
        else if (coord(ipoin).lt.900.0) then
           tempe(ipoin,1) = 286.0 +2.0*(coord(ipoin)-850.0)/50.0
        else if (coord(ipoin).lt.1000.0) then
           tempe(ipoin,1) = 288.0 +4.0*(coord(ipoin)-900.0)/100.0
        else if (coord(ipoin).lt.2000.0) then
           tempe(ipoin,1) = 292.0 +8.0*(coord(ipoin)-1000.0)/1000.0
        else if (coord(ipoin).lt.3500.0) then
           tempe(ipoin,1) = 300.0 +10.0*(coord(ipoin)-2000.0)/1500.0
        else if (coord(ipoin).lt.4000.0) then
           tempe(ipoin,1) = 310.0 +2.0*(coord(ipoin)-3500.0)/500.0
        else
           tempe(ipoin,1) = 312.0 +5.0*(coord(ipoin)-4000.0)/2000.0
        end if
        tempe(ipoin,1) = tempe(ipoin,1) - 3.92
        tempe(ipoin,2) = tempe(ipoin,1)
     end do
     ! Fixed temperature at top
     tetop = tempe(npoin,1)
     ! calculates initial Mellor-Yosida length
     tnume = 0.0d0
     tdeno = 0.0d0
     do ielem =1, nelem
        ipoin = ielem
        jpoin = ielem + 1
        dz = coord(jpoin)-coord(ipoin)
        sqkey = 0.50d0*(sqrt(keyva(ipoin,1))+sqrt(keyva(jpoin,1)))
        gpcod = coord(ipoin) +0.50*dz
        tnume = tnume + gpcod*sqkey*dz
        tdeno = tdeno + sqkey*dz
     end do
!     Mellor-Yosida length
     lenmy = 0.075*tnume/tdeno
  end if
   

  !
  ! open files
  !
  open(lun_conve(1), FILE='Wind2.ns1.cvg',status='unknown')
  open(lun_conve(2), FILE='Wind2.ns2.cvg',status='unknown')
  open(lun_conve(3), FILE='Wind2.tur1.cvg',status='unknown')
  open(lun_conve(4), FILE='Wind2.tur2.cvg',status='unknown')
  if (kfl_trtem) open(lun_conve(5), FILE='Wind2.tem.cvg',status='unknown')

  open(lun_rstar, FILE='Wind2.rst', status='unknown')
  write(lun_conve(1),101)
  write(lun_conve(2),102)
  write(lun_conve(3),201)
  write(lun_conve(4),202)
  if (kfl_trtem)  write(lun_conve(5),301)
  !
  ! CUTS AND TRANSIENT FILES
  !
  do icuts = 1, ncuts        
     write(label,'(f5.2)') cutpr(icuts)
     file = 'plotcut_'//trim(adjustl(label))  ! plotcut_zz.zz
     open(lun_cutre(icuts), FILE=file, status='unknown')
     write(lun_cutre(icuts),'(a)')  '#1: time          2:velocx          3:velocy      &
          4:key     5:eps     6:temp '          

     ! INTERPOLATION VALUES
     ipoin =1
     jpoin =npoin
     kpoin =npoin/2
     do while((jpoin-ipoin).gt.1)
        if(cutpr(icuts).lt.coord(kpoin)) then
           jpoin = kpoin
        else
           ipoin = kpoin
        end if
        kpoin =(ipoin + jpoin)/2   
     end do
     ielec(icuts) = ipoin ! element to which icuts belongs

  end do
  if (kfl_trtem) then
     open(lun_globa, FILE='plotglobal', status='unknown' )
     write(lun_globa,'(a)')  '#1: time          2:ustar          3:heatfl       4:tstar    5: tewal    6: maxle  7: MOlength  8: htpbl'
  end if



  !
  ! Formats
  !
101 format('$ ','       Time','     Global','      Inner',&
       &      '       Current','   x- Velocity','  x - Velocity'/,&          
       & '$ ','       step','  iteration','  iteration',&
       &      '          time','      residual', '   norm')

201 format('$ ','       Time','     Global','      Inner',&
       &      '       Current','      kinetic_tur',' kinet'/,&          
       & '$ ','       step','  iteration','  iteration',&
       &      '          time','      residual','      residual', '   norm')

102 format('$ ','       Time','     Global','      Inner',&
       &      '       Current','   y- Velocity','   y - Veloc'/,&          
       & '$ ','       step','  iteration','  iteration',&
       &      '          time','      residual', '   norm')

202 format('$ ','       Time','     Global','      Inner',&
       &      '       Current','      epsil_tur','   epsil'/,&          
       & '$ ','       step','  iteration','  iteration',&
       &      '          time','      residual', '   norm')

301 format('$ ','       Time','     Global','      Inner',&
       &      '       Current','     Temper','   Temper'/,&          
       & '$ ','       step','  iteration','  iteration',&
       &      '          time','      residual', '   norm')
end subroutine turnon
