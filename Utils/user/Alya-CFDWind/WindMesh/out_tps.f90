       subroutine out_alya_tps
!**************************************************************
!*
!*     This routine writes the tracking points along profiles
!*     or fixed locations
!*
!**************************************************************
      use InpOut
      use Master
      implicit none
!
      integer(ip) :: itps,iwit
!
!*** Writes output files
!
     fwit = TRIM(path)//'Alya/'//TRIM(name)//'.wit'
     ftps = TRIM(path)//'Alya/'//TRIM(name)//'.tps'
     open(90,FILE=TRIM(fwit),status='unknown')
     open(91,FILE=TRIM(ftps),status='unknown')
!
     write(90,10) nwit
 10  format('WITNESS_POINTS, NUMBER=',i5)
!
     do itps = 1,ntps
        if(tps(itps)%exists) then  
!
           write(91,11) TRIM(tps(itps)%type),TRIM(tps(itps)%name),TRIM(tps(itps)%attr), & 
                        TRIM(utmzone),tps(itps)%nwit
 11        format('TYPE: ',a,/,&
                  'NAME: ',a,/,&
                  'ATTRIBUTE: ',a,/,&
                  'COORDINATES: UTM ',a,/,&
                  'NTPS: ',i5)
!
           do iwit = 1,tps(itps)%nwit
              write(90,20) tps(itps)%coord(1,iwit),tps(itps)%coord(2,iwit),tps(itps)%coord(3,iwit)
              write(91,20) tps(itps)%coord(1,iwit),tps(itps)%coord(2,iwit),tps(itps)%coord(3,iwit)
 20           format(3(e14.7,1x))
           end do
        end if
     end do
     write(90,30) 'END_WITNESS_POINTS'
 30  format(a)
!
     close(90)
     close(91)
!
      return
      end subroutine out_alya_tps
!
!
!
       subroutine out_foam_tps
!**************************************************************
!*
!*     This routine writes the tracking points along profiles
!*     or fixed locations
!*
!**************************************************************
      use InpOut
      use Master
      implicit none
!
      integer(ip) :: itps,iwit
!
!*** Writes output file
!
     ftps = TRIM(path)//'OpenFoam/'//'tracking_points'
     open(91,FILE=TRIM(ftps),status='unknown')
!
     do itps = 1,ntps
        if(tps(itps)%exists) then  
           do iwit = 1,tps(itps)%nwit
              write(91,20) tps(itps)%coord(1,iwit),tps(itps)%coord(2,iwit),tps(itps)%coord(3,iwit)
 20           format(3(1x,e15.8))
           end do
        end if
     end do
!
     close(91)
!
      return
      end subroutine out_foam_tps
