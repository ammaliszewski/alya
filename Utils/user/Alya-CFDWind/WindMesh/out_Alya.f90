   subroutine out_alya_3d(problemname,path,x,y,z,zo2d,lnods,lnodb,lelmb,my_zone_boun,&
     npoin2d,nz,nnode,nelem,nnodb,nboun)
  !*************************************************
  !*
  !*     Output for alya in 3d using 
  !*     x(npoin2d,nz)
  !*     y(npoin2d,nz)
  !*     z(npoin2d,nz) 
  !*
  !*************************************************
  use KindType
  use Master, only : zo_def
  implicit none
  !
  character(len=s_file) :: problemname,path
  integer(ip)           :: npoin2d,nz,nnode,nelem,nnodb,nboun
  integer(ip)           :: lnods(nnode,nelem),lnodb(nnodb,nboun),lelmb(nboun),my_zone_boun(nboun)
  real(rp)              :: x(npoin2d,nz),y(npoin2d,nz),z(npoin2d,nz),zo2d(npoin2d)
  !
  character(len=s_file) :: fname
  integer(ip) :: ipoin2d,iz,ielem,ipoin,inode,iboun,npoin
  real   (rp) :: zo_boun
  !
  !*** 1. Geometry (mesh) file 
  !
  fname = TRIM(path)//'.WindMesh.geo'
  open(90,file=TRIM(fname),status='unknown')
  !
  !*** Writes nodal connectivities
  !
  write(90,'(a)') 'ELEMENTS'
  do ielem = 1,nelem
     write(90,'(9(i8,1x))') ielem,(lnods(inode,ielem),inode=1,nnode)
  end do
  write(90,'(a)') 'END_ELEMENTS'
  !
  !*** Writes coordinates
  !
  write(90,'(a)') 'COORDINATES'
  ipoin = 0_ip
  do iz = 1,nz
     do ipoin2d = 1,npoin2d
        ipoin = ipoin + 1
        write(90,'(i8,3(1x,e14.7))') ipoin,x(ipoin2d,iz),y(ipoin2d,iz),z(ipoin2d,iz)
     end do
  end do
  write(90,'(a)') 'END_COORDINATES'
  !
  !*** Writes boundaries nodal connectivities
  !
  write(90,'(a)') 'BOUNDARIES, ELEMENTS'
  do iboun = 1,nboun
     write(90,'(6(i8,1x))') iboun,(lnodb(inode,iboun),inode=nnodb,1,-1),lelmb(iboun)     ! Note the order, opposite to OF. Rotation inwards
  end do
  write(90,'(a)') 'END_BOUNDARIES'
  !
  !*** Writes skew systems
  !
  write(90,'(a)') 'SKEW_SYSTEMS'
  write(90,'(a)') 'END_SKEW_SYSTEMS'
  close(90)
  !
  !*** 2. Boundary conditions (fix) file. Codes on boundaries
  !
  fname = TRIM(path)//'.fix'
  open(90,file=TRIM(fname),status='unknown')
  !
  write(90,'(a)') 'ON_BOUNDARIES'
  do iboun = 1,nboun
     write(90,'(2(i8,1x))') iboun,my_zone_boun(iboun)
  end do
  write(90,'(a)') 'END_ON_BOUNDARIES'
  close(90)
  !
  !*** 3. Roughness (zo) file at surface
  !
  fname = TRIM(path)//'.zo.dat'
  open(90,file=TRIM(fname),status='unknown')
  !
  write(90,'(a)') 'FIELD, NUMBER=1'
  write(90,'(a)') 'FIELD=1, DIMENSION=1, NODES'
  !
  ipoin = 0
  do iz = 1,1
     do ipoin2d = 1,npoin2d
        ipoin = ipoin + 1
        write(90,'(i8,1x,f10.5)') ipoin, zo2d(ipoin2d) 
     end do
  end do
  !
  write(90,'(a)') 'END_FIELD'
  write(90,'(a)') 'END_FIELD'
  close(90)
  !
  !*** 4. Geo file
  !
  fname = TRIM(path)//'.WindMesh.dom.dat' 
  open(90,file=TRIM(fname),status='unknown')
  !
  npoin = npoin2d*nz
  write(90,100) npoin,nelem, nboun, & 
                TRIM(problemname)//'.geo', &
                TRIM(problemname)//'.zo.dat', &
                TRIM(problemname)//'.fix', &
                './AlyaFix/'//TRIM(problemname)//'.nsi.ini', &   
                './AlyaFix/'//TRIM(problemname)//'.tur1.ini', &   
                './AlyaFix/'//TRIM(problemname)//'.tur2.ini'     
  !
100 format( &
       '$------------------------------------------------------------',/, &
       'DIMENSIONS',/,&
       '  NODAL_POINTS  ',i8,/,&
       '  ELEMENTS      ',i8,/,&
       '  SPACE_DIMENSIONS   3',/,&
       '  TYPES_OF_ELEMENTS  HEX08',/,&
       '  BOUNDARIES    ',i8,/,&
       'END_DIMENSIONS',/,&
       '$------------------------------------------------------------',/,&
       'STRATEGY',/,&
       '  INTEGRATION_RULE           Closed',/,&  
       '  DOMAIN_INTEGRATION_POINTS  8',/,&
       'END_STRATEGY',/,&
       '$-------------------------------------------------------------',/,&
       'GEOMETRY',/,&
       '  GROUPS = 500',/,&
       '  TYPES, ALL=HEX08',/,&
       '  END_TYPES',/,&
       '  INCLUDE ',a,/,&
       '  INCLUDE ',a,/,&
       'END_GEOMETRY',/,&  
       '$-------------------------------------------------------------',/,&
       'SETS',/,&
       'END_SETS',/,&
       '$-------------------------------------------------------------',/,&
       'BOUNDARY_CONDITIONS',/,&
       '  INCLUDE ',a,/,&  
       '  GEOMETRICAL_CONDITIONS',/,&
       '     FREESTREAM   1,2,3,4,6',/,&
       '$     SYMMETRY     6',/,&
       '     WALL_LAW     5',/,&
       '$     ANGLE, GEO_ANGLE = 150.0',/,&
       '     CRITERION_FREESTREAM : VALUE_FUNCTION =  1 toler 90.0',/,&
       '  END_GEOMETRICAL_CONDITIONS',/,&
       '  VALUES, FUNCTION=1, DIMENSION=3',/,&
       '     INCLUDE ',a,/,&
       '  END_VALUES',/,&
       '  VALUES, FUNCTION=2, DIMENSION=1',/,&
       '     INCLUDE ',a,/,&
       '  END_VALUES',/,&
       '  VALUES, FUNCTION=3, DIMENSION=1',/,&
       '     INCLUDE ',a,/,&
       '  END_VALUES',/,&
       'END_BOUNDARY_CONDITIONS',/,&
       '$-------------------------------------------------------------')
  !
  close(90)
  !
  !*** 5. File for imposing boundary and initial conditions (CFDWind 1 and 2)
  !
  fname = TRIM(path)//'.WindMesh.bcs' 
  open(90,file=TRIM(fname),status='unknown')
  !
  !***    For the initial condition, a single value of zo is considered in the whole
  !***    domain. This is to avoid: i) initial discontinuities, ii) different prescription
  !***    of velocity at the top of the computational domain
  !
  zo_boun = zo_def  ! default value is given to all nodes in the buffer zone
  !
  write(90,'(i8)') npoin2d*nz
  do iz = 1,nz
     do ipoin2d = 1,npoin2d
        write(90,'(2(1x,e14.7))') zo2d(ipoin2d), z(ipoin2d,iz)-z(ipoin2d,1)
!        write(90,'(2(1x,e14.7))') zo_boun, z(ipoin2d,iz)-z(ipoin2d,1)
     end do
  end do
  close(90)
  !
  return
end subroutine out_alya_3d
!
!
!

subroutine out_alya_3d_arrays(problemname,path,x,y,z,zo2d,lnods,lnodb,lelmb,my_zone_boun,npoin2d,nz,nnode,nelem,nnodb,nboun)
  !*************************************************
  !*
  !*     Output for alya in 3d (hexahedras)
  !*
  !*************************************************
  use KindType
  implicit none
  !
  character(len=s_file) :: problemname,path
  integer(ip)           :: npoin2d,nz,nnode,nelem,nnodb,nboun
  integer(ip)           :: lnods(nnode,nelem),lnodb(nnodb,nboun),lelmb(nboun),my_zone_boun(nboun)
  real(rp)              :: x(npoin2d),y(npoin2d),z(npoin2d,nz),zo2d(npoin2d)
  !
  character(len=s_file) :: fname
  integer(ip) :: ipoin2d,iz,ielem,ipoin,inode,iboun,npoin
  !
  !*** 1. Geometry (mesh) file 
  !
  fname = TRIM(path)//'.geo'
  open(90,file=TRIM(fname),status='unknown')
  !
  !*** Writes nodal connectivities
  !
  write(90,'(a)') 'ELEMENTS'
  do ielem = 1,nelem
     write(90,'(9(i8,1x))') ielem,(lnods(inode,ielem),inode=1,nnode)
  end do
  write(90,'(a)') 'END_ELEMENTS'
  !
  !*** Writes coordinates
  !
  write(90,'(a)') 'COORDINATES'
  ipoin = 0
  do iz = 1,nz
     do ipoin2d = 1,npoin2d
        ipoin = ipoin + 1
        write(90,'(i8,2(1x,f11.1),1x,f8.1)') ipoin,x(ipoin2d),y(ipoin2d),z(ipoin2d,iz)
     end do
  end do
  write(90,'(a)') 'END_COORDINATES'
  !
  !*** Writes boundaries nodal connectivities
  !
  write(90,'(a)') 'BOUNDARIES, ELEMENTS'
  do iboun = 1,nboun
     write(90,'(6(i8,1x))') iboun,(lnodb(inode,iboun),inode=nnodb,1,-1),lelmb(iboun)     ! Note the order, opposite to OF. Rotation inwards
  end do
  write(90,'(a)') 'END_BOUNDARIES'
  !
  !*** Writes skew systems
  !
  write(90,'(a)') 'SKEW_SYSTEMS'
  write(90,'(a)') 'END_SKEW_SYSTEMS'
  close(90)
  !
  !*** 2. Boundary conditions (fix) file. Codes on boundaries
  !
  fname = TRIM(path)//'.fix'
  open(90,file=TRIM(fname),status='unknown')
  !
  write(90,'(a)') 'ON_BOUNDARIES'
  do iboun = 1,nboun
     write(90,'(2(i8,1x))') iboun,my_zone_boun(iboun)
  end do
  write(90,'(a)') 'END_ON_BOUNDARIES'
  close(90)
  !
  !*** 3. Roughness (zo) file at surface
  !
  fname = TRIM(path)//'.zo.dat'
  open(90,file=TRIM(fname),status='unknown')
  !
  write(90,'(a)') 'FIELD, NUMBER=1'
  write(90,'(a)') 'FIELD=1, DIMENSION=1, NODES'
  !
  ipoin = 0
  do iz = 1,1
     do ipoin2d = 1,npoin2d
        ipoin = ipoin + 1
        write(90,'(i8,1x,f10.5)') ipoin, zo2d(ipoin2d) 
     end do
  end do
  !
  write(90,'(a)') 'END_FIELD'
  write(90,'(a)') 'END_FIELD'
  close(90)
  !
  !*** 4. Geo file
  !
  fname = TRIM(path)//'.dom.dat' 
  open(90,file=TRIM(fname),status='unknown')
  !
  npoin = npoin2d*nz
  write(90,100) npoin,nelem, nboun, & 
       TRIM(problemname)//'.geo',TRIM(problemname)//'.zo.dat', &
       TRIM(problemname)//'.fix'
  !
100 format( &
       '$------------------------------------------------------------',/, &
       'DIMENSIONS',/,&
       '  NODAL_POINTS  ',i8,/,&
       '  ELEMENTS      ',i8,/,&
       '  SPACE_DIMENSIONS   3',/,&
       '  TYPES_OF_ELEMENTS  HEX08',/,&
       '  BOUNDARIES    ',i8,/,&
       'END_DIMENSIONS',/,&
       '$------------------------------------------------------------',/,&
       'STRATEGY',/,&
       '  INTEGRATION_RULE           Closed',/,&  
       '  DOMAIN_INTEGRATION_POINTS  8',/,&
       'END_STRATEGY',/,&
       '$-------------------------------------------------------------',/,&
       'GEOMETRY',/,&
       '  GROUPS = 500',/,&
       '  TYPES, ALL=HEX08',/,&
       '  END_TYPES',/,&
       '  INCLUDE ',a,/,&
       '  INCLUDE ',a,/,&
       'END_GEOMETRY',/,&  
       '$-------------------------------------------------------------',/,&
       'SETS',/,&
       'END_SETS',/,&
       '$-------------------------------------------------------------',/,&
       'BOUNDARY_CONDITIONS',/,&
       '  INCLUDE ',a,/,&  
       '  GEOMETRICAL_CONDITIONS',/,&
       '     FREESTREAM   1,2,3,4,6',/,&
       '     WALL_LAW     5',/,&
       '     ANGLE, GEO_ANGLE = 150.0',/,&
       '  END_GEOMETRICAL_CONDITIONS',/,&
       'END_BOUNDARY_CONDITIONS',/,&
       '$-------------------------------------------------------------')
  !
  close(90)
  !
  return
end subroutine out_alya_3d_arrays


