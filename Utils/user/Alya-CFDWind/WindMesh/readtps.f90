       subroutine readtps_list
!**************************************************************
!*
!*     This routine reads the tracking points along profiles
!*     or fixed locations from the input file
!*
!**************************************************************
      use InpOut
      use Master
      use Elsest_mod
      implicit none
!
      character(len=s_long) :: card
      character(len=s_long) :: words(nwormax)
      logical               :: line1found,line2found
      integer(ip)           :: nword,npar,itps
      real(rp)              :: param(nparmax)
!
      logical                :: go_on
      integer(ip), parameter :: pnode = 4
      integer(ip), parameter :: ndime = 2
      integer(ip)            :: ielem,ifoun,inode,iz,icell,niter,iiter,iwit
      integer(ip)            :: ix_nobuff,iy_nobuff,ix,iy,ipoin
      real   (rp)            :: dist,distmin
      real   (rp) :: toler,lmini,lmaxi,elcod(ndime,pnode),coglo(3),coloc(3),shapf(pnode),deriv(ndime,pnode), & 
                     xjacm(ndime,ndime),xjaci(ndime,ndime),zo
      real   (rp) :: l,dz,alpha,alphamin,alphamax
      real   (rp) :: eta (nz)
!
!***  Search the block and counts the number of structures (profiles)
!
      ntps = 0
!
      open(90,FILE=TRIM(finp),STATUS='old')
      line1found  = .false.
      line2found  = .false.
      do while(.not.line2found)
        do while(.not.line1found) 
           read(90,'(a256)',END=102) card
           call sdecode(card,words,param,nword,npar)
           call upcase(words(1))
           if(TRIM(words(1)).eq.'WITNESS_POINTS_LIST') line1found = .true.
        end do
        read(90,'(a256)',END=103) card
        call sdecode(card,words,param,nword,npar)
        call upcase(words(1))
        if(TRIM(words(1)).eq.'END_WITNESS_POINTS_LIST') line2found = .true.
        if(.not.line2found) ntps = ntps + 1
      end do
!
 102  if(.not.line1found) then
        close(90)
        call runend('WITNESS_POINTS_LIST not found in input file')
      end if
 103  if(.not.line2found) then
        close(90)
        call runend('END_WITNESS_POINTS_LIST not found in input file')
      end if
      if(ntps.eq.0) return
!
!***  allocates structures (except for coordinates) and reads
!
      allocate(tps(ntps))
!
      rewind(90)
      line1found  = .false.
      do while(.not.line1found)
         read(90,'(a256)',END=102) card
         call sdecode(card,words,param,nword,npar)
         call upcase(words(1))
         if(TRIM(words(1)).eq.'WITNESS_POINTS_LIST') line1found = .true.
      end do
      do itps = 1,ntps
          read(90,'(a256)',END=102) card
          words(3) = ' '                                ! Optional. Initialize to void
          call sdecode(card,words,param,nword,npar)
          tps(itps)%type   = TRIM(words(1))
          tps(itps)%name   = TRIM(words(2))
          tps(itps)%attr   = TRIM(words(3))
          tps(itps)%exists = .true.
          if(TRIM(tps(itps)%type).eq.'xy') then
            tps(itps)%xtps = param(1)
            tps(itps)%ytps = param(2)
            tps(itps)%ztps = -99.
          else if(TRIM(tps(itps)%type).eq.'xz') then
            tps(itps)%xtps = param(1)
            tps(itps)%ytps = -99.
            tps(itps)%ztps = param(2)
          else if(TRIM(tps(itps)%type).eq.'yz') then
            tps(itps)%xtps = -99.
            tps(itps)%ytps = param(1)
            tps(itps)%ztps = param(2)
          else if(TRIM(tps(itps)%type).eq.'xyz') then
            tps(itps)%xtps = param(1)
            tps(itps)%ytps = param(2)
            tps(itps)%ztps = param(3)
          else
            tps(itps)%exists = .false.
          end if
     end do
      close(90)
!
!*** Builds the profiles
!
     do itps = 1,ntps
        if(tps(itps)%exists) then
!
        if(TRIM(tps(itps)%type).eq.'xy') then
!
!       1. Vertical profile.
! 
!          Find the surface element of the point to get
!          the vertical coordinate (z at surface)
!
           toler = 1d-4
           lmini = -toler         ! tolerance for local coordinate
           lmaxi =  1.0_rp+toler
           zo    = 0.0_rp
           coglo(1) = tps(itps)%xtps
           coglo(2) = tps(itps)%ytps
           ielem = 0
           ifoun = 0
           go_on = .true.

           do while(go_on)
              ielem = ielem + 1
!
              elcod(1,1) = x2d(lnods2d(1,ielem))
              elcod(2,1) = y2d(lnods2d(1,ielem))
              elcod(1,2) = x2d(lnods2d(2,ielem))
              elcod(2,2) = y2d(lnods2d(2,ielem))
              elcod(1,3) = x2d(lnods2d(3,ielem))
              elcod(2,3) = y2d(lnods2d(3,ielem))
              elcod(1,4) = x2d(lnods2d(4,ielem))
              elcod(2,4) = y2d(lnods2d(4,ielem))           
!
              call elsest_elq1p1(pnode,lmini,lmaxi,elcod,coglo,coloc,&
                                 shapf,deriv,ifoun,xjacm,xjaci)
              if(ifoun.eq.1) then
                 go_on = .false.    
                 do inode = 1,pnode
                    shapf(inode) = max(0.0_rp,shapf(inode))
                    shapf(inode) = min(1.0_rp,shapf(inode))
                    zo = zo + shapf(inode)*z2d(lnods2d(inode,ielem))
                end do
              end if
              if(ielem.eq.nelem2d) go_on = .false.
           end do  
!
!          Vertical distribution
!
           tps(itps)%nwit = nz
           allocate(tps(itps)%coord(3,tps(itps)%nwit))
           tps(itps)%coord(1,1:tps(itps)%nwit) = tps(itps)%xtps    
           tps(itps)%coord(2,1:tps(itps)%nwit) = tps(itps)%ytps    
!
           dz = 1.0_rp/ncellz
           eta(1) = 0.0_rp         !Build the logical (uniform) mesh in (0,1)
           do icell = 1,ncellz
              iz = icell + 1
              eta(iz) = eta(iz-1) + dz
           end do
           eta(nz) = 1.0_rp 
! 
           if(TRIM(dz_distribution).eq.'GEOMETRIC') then    ! case 1 geometric distribution
              niter = 50
              toler = 1d-4*dz0
              l     = ztop - zo
              dz    = l/ncellz
              alphamin = 0.0
              alphamax = 100.0
              go_on = .true.
              iiter = 0
              do while(go_on)
                 iiter = iiter + 1
                 alpha = 0.5*(alphamin + alphamax) 
                 dz    = l*(exp(alpha*eta(2))-1.0)/(exp(alpha)-1.0)
                 if((dz-dz0).gt.toler) then
                    alphamin = alpha
                 else if((dz-dz0).lt.toler) then
                    alphamax = alpha
                 else
                    go_on = .false.
                 end if
                 if(iiter.gt.niter) go_on=.false.
             end do
! 
             tps(itps)%coord(3,1) = zo
             do icell = 1,ncellz 
                iz = icell + 1
                tps(itps)%coord(3,iz) = zo + l*(exp(alpha*eta(iz))-1.0)/(exp(alpha)-1.0)
             end do
!
          else                  ! case2: linear 
!   
              l  = ztop - zo
              dz = l/ncellz
              tps(itps)%coord(3,1) = zo
              do icell = 1,ncellz 
                 iz = icell + 1
                 tps(itps)%coord(3,iz) = zo + dz
              end do
          end if
!
        else if(TRIM(tps(itps)%type).eq.'xz') then
!
!       2. Profile along y
!
           tps(itps)%nwit = ny_nobuff
           allocate(tps(itps)%coord(3,tps(itps)%nwit))
           tps(itps)%coord(1,1:tps(itps)%nwit) = tps(itps)%xtps 
!
!          Find the closest surface point
!
           distmin = 1d9
           ix = 1
           do ix_nobuff = 1,nx_nobuff
              ipoin = nobuff_2d_local(ix_nobuff,1)    ! global poin
              dist  = (tps(itps)%xtps-x2d(ipoin))*(tps(itps)%xtps-x2d(ipoin)) 
              if(dist.lt.distmin) then 
                 distmin = dist
                 ix = ix_nobuff
              end if
           end do
!
          do iy_nobuff = 1,ny_nobuff 
             ipoin = nobuff_2d_local(ix,iy_nobuff)    ! global poin
             tps(itps)%coord(2,iy_nobuff) = y2d(ipoin)
             tps(itps)%coord(3,iy_nobuff) = z2d(ipoin) + tps(itps)%ztps
          end do
!
        else if(TRIM(tps(itps)%type).eq.'yz') then
!
!       3. Profile along x
!
           tps(itps)%nwit = nx_nobuff
           allocate(tps(itps)%coord(3,tps(itps)%nwit))
           tps(itps)%coord(2,1:tps(itps)%nwit) = tps(itps)%ytps    
!
!          Find the closest surface point
!
           distmin = 1d9
           iy = 1
           do iy_nobuff = 1,ny_nobuff
              ipoin = nobuff_2d_local(1,iy_nobuff)    ! global poin
              dist  = (tps(itps)%ytps-y2d(ipoin))*(tps(itps)%ytps-y2d(ipoin)) 
              if(dist.lt.distmin) then 
                 distmin = dist
                 iy = iy_nobuff
              end if
           end do
!
          do ix_nobuff = 1,nx_nobuff 
             ipoin = nobuff_2d_local(ix_nobuff,iy)    ! global poin
             tps(itps)%coord(1,ix_nobuff) = x2d(ipoin)
             tps(itps)%coord(3,ix_nobuff) = z2d(ipoin) + tps(itps)%ztps
          end do
!
        else if(TRIM(tps(itps)%type).eq.'xyz') then
!
!          4. Single point
!
           tps(itps)%nwit = 1
           allocate(tps(itps)%coord(3,tps(itps)%nwit))
!
!          Find the surface element of the point to get
!          the vertical coordinate (z at surface)
!
           toler = 1d-4
           lmini = -toler         ! tolerance for local coordinate
           lmaxi =  1.0_rp+toler
           zo    = 0.0_rp
           coglo(1) = tps(itps)%xtps
           coglo(2) = tps(itps)%ytps
           ielem = 0
           ifoun = 0
           go_on = .true.
           do while(go_on)
              ielem = ielem + 1
!
              elcod(1,1) = x2d(lnods2d(1,ielem))
              elcod(2,1) = y2d(lnods2d(1,ielem))
              elcod(1,2) = x2d(lnods2d(2,ielem))
              elcod(2,2) = y2d(lnods2d(2,ielem))
              elcod(1,3) = x2d(lnods2d(3,ielem))
              elcod(2,3) = y2d(lnods2d(3,ielem))
              elcod(1,4) = x2d(lnods2d(4,ielem))
              elcod(2,4) = y2d(lnods2d(4,ielem))           
!
              call elsest_elq1p1(pnode,lmini,lmaxi,elcod,coglo,coloc,&
                                 shapf,deriv,ifoun,xjacm,xjaci)
              if(ifoun.eq.1) then
                 go_on = .false.    
                 do inode = 1,pnode
                    shapf(inode) = max(0.0_rp,shapf(inode))
                    shapf(inode) = min(1.0_rp,shapf(inode))
                    zo = zo + shapf(inode)*z2d(lnods2d(inode,ielem))
                end do
              end if
              if(ielem.eq.nelem2d) go_on = .false.
           end do  
!
           tps(itps)%coord(1,1) = tps(itps)%xtps         
           tps(itps)%coord(2,1) = tps(itps)%ytps       
           tps(itps)%coord(3,1) = zo + tps(itps)%ztps
!
        end if   ! if(tps(itps)%type
        end if   ! if(tps(itps)%exists)
     end do
!
!*** Total number of witness points
!
     nwit = 0
     do itps = 1,ntps
        if(tps(itps)%exists) nwit = nwit + tps(itps)%nwit 
    end do          
!
    return
    end subroutine readtps_list
!
!
!
      subroutine readtps
!**************************************************************
!*
!*     This routine reads the tracking points file
!*
!**************************************************************
      use InpOut
      use Master
      implicit none
!
      logical               :: linefound
      character(len=s_long) :: card
      character(len=s_long) :: words(nwormax)
      integer(ip)           :: nword,npar,iwit,idime
      real(rp)              :: param(nparmax)
!
!***  Initializations
!
      ntps = 1
      allocate(tps(ntps))
      tps(1)%exists = .true.
      tps(1)%type   = 'point'
      tps(1)%name   = 'point'
      tps(1)%attr   = 'point'
!
      linefound = .false.    
      open(90,FILE=TRIM(fwit),STATUS='old')
      read(90,'(a256)',END=102) card
      call sdecode(card,words,param,nword,npar)  ! WITNESS_POINTS, NUMBER= xxx
      nwit = param(1)
      tps(1)%nwit = nwit
      linefound = .true.
 102  if(.not.linefound) then
        close(90)
        return
      end if
!
      allocate(tps(1)%coord(3,tps(1)%nwit))
!
      do iwit = 1,nwit    
         read(90,*) (tps(1)%coord(idime,iwit),idime=1,3)
      end do
!  
      close(90)
      return
      end subroutine readtps
    
