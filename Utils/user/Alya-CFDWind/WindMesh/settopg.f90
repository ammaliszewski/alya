     subroutine settopg
!*************************************************************
!*  
!*    This subroutine interpolates the topography.
!*
!*************************************************************
     use Master
     use InpOut
     use Delauney
     use Elsest_mod
     implicit none
!
     integer(ip) :: ipoin,ifoun,ibopo,jpoin,inode
     real(rp)    :: z0,z1,d,d0,d1,xp,yp,x0,y0,x1,y1
!
     integer(ip) :: iiter,i
     real(rp)    :: lambda_smooth,mu_smooth,z_baric
!
     real   (rp) :: coglo(2), coloc(2)               !  Elsest variables (triangles; nnode=3, ndime=2)
     real   (rp) :: shapf(3), deriv(2,3), relse(20)
     integer(ip) :: ielse(10), ltopo,  ielem
!
!*** No real interpolation needed (defalt value asigned)
!
     if(n_top.eq.0) return
!
!*** 1. Build first the background interpolation mesh (Delauney triangulation by now)
!
     write(lulog,11)
     if(out_screen) write(*,11)
 11  format('---> Constructing the Delauney triangulation for topography...',/)
!
     node_num_z = n_top                                ! Numer of nodes 
     allocate(node_xy_z(2,node_num_z))                 ! coordinates
     allocate(triangle_node_z(3,2*node_num_z))         ! Conectivities. Overdimension to 2*node_num (instead of triangle_num)
     node_xy_z(1,1:node_num_z) = x_top(1:node_num_z)
     node_xy_z(2,1:node_num_z) = y_top(1:node_num_z)
     call set_delauney(node_xy_z,triangle_node_z,node_num_z,triangle_num_z)
!
     allocate(bbox_elem_z(triangle_num_z,nbbox_z,nbbox_z))
     allocate(bbox_min_z(2,nbbox_z,nbbox_z))
     allocate(bbox_max_z(2,nbbox_z,nbbox_z))
     call set_delauney_bboxes(node_xy_z,triangle_node_z,bbox_elem_z,bbox_min_z,bbox_max_z,node_num_z,triangle_num_z,nbbox_z)
!
!*** 2. Initialize Elsest variaables
!
     write(lulog,12)
     if(out_screen) write(*,12)
 12  format('---> Interpolating topography topography...',/)
!
     ielse(1)      = 80          ! number of search boxes in x
     ielse(2)      = 80          ! number of search boxes in y
     ielse(3)      = 10          ! nz
     ielse(4)      = 1           ! data format (0=type,1=linked list)
     ielse(5)      = 1           ! Maximum number of possible meshes
     ielse(6)      = 1           ! Not used
     ielse(7)      = 0           ! Output unit (do not)
     ielse(8)      = 0           ! Search strategy (0=bin)
!
     relse(1)      = 1e-4_rp     ! Tolerance for iteration 1d-4
     relse(3)      =-1e12_rp     ! Patch bounding boxs
     relse(4)      = 1e12_rp                                   
     relse(5)      =-1e12_rp
     relse(6)      = 1e12_rp
     relse(7)      =-1e12_rp
     relse(8)      = 1e12_rp
!
     ltopo = 1_ip  ! triange 2d
!
!*** 2. Loop over 2D mesh points excluding the buffer zone, where no interpolation is needed
!
     do ipoin = 1,npoin2d
        if(my_zone(ipoin).ne.0) then  ! skip the buffer zone
!
           coglo(1) = x2d(ipoin)
           coglo(2) = y2d(ipoin)
!
           call elsest(2_ip,ielse,3_ip,2_ip,node_num_z,triangle_num_z, &   
                triangle_node_z,ltopo,node_xy_z,coglo,relse,    &
                ielem,shapf,deriv,coloc)
           ifoun = ielem
           if(ifoun.le.0) then 
              call runend('Elsest: point not found. Check the bounds of the topography zone') 
           else
!
!***         Interpolate
!
             z2d(ipoin) = 0.0_rp
             do inode = 1,3
                if(shapf(inode).lt.-relse(1).or.shapf(inode).gt.1.0+relse(1)) call runend('settopg:bad shape')
!                shapf(inode) = max(0.0_rp,shapf(inode))
!                shapf(inode) = min(1.0_rp,shapf(inode))
                z2d(ipoin) = z2d(ipoin) + shapf(inode)*z_top(triangle_node_z(inode,ielem))
             end do   
           end if        
        end if
      end do
!
!***  3. Calculate z0, the default height of the (external) buffer zone.
!***     This is the minimum height value of the transition zone perimeter.
!
     if(elevation_buffer.lt.(-10.0) ) then    ! -99.
        if(1.eq.1) then                       !  This is the minimum height value of the transition zone perimeter.
           z0 = 1d9
           do ibopo = 1,tran%nbopo
             if( z2d(tran%ibopo(ibopo)).lt.z0 ) then
                 z0 = z2d(tran%ibopo(ibopo))
             end if
           end do
        else                                   !  This is the mean height value of the transition zone perimeter.
           z0 = 0
           do ibopo = 1,tran%nbopo
              z0 = z0 + z2d(tran%ibopo(ibopo))
           end do
           z0 = z0/tran%nbopo
        end if
     else
        z0 = elevation_buffer
     end if
!
!*** 4. Initialization of the buffer zone height
!
     do ipoin = 1,npoin2d
        if(my_zone(ipoin).eq.0) then   ! buffer zone 
           z2d(ipoin) = z0
        end if
     end do!
     write(lulog,10) z0
     if(out_screen) write(*,10) z0
 10  format('Height of the buffer zone z = ',f9.3)
!
!***  5. Assign topography to the buffer zone (only to the inner shell)
!
     if(smooth_buffer) then
    
      do ipoin = 1,bufe%npoin
         if(bufe%myshell(ipoin).eq.1) then   ! inner shell 
!
         xp = bufe%x2d(ipoin)
         yp = bufe%y2d(ipoin)
!
!***     Find the closer point in the boundary of the transition zone
!
         d1 = 1d9
         do ibopo = 1,tran%nbopo
            x1 = x2d(tran%ibopo(ibopo))
            y1 = y2d(tran%ibopo(ibopo))
            d  = sqrt( (x1-xp)*(x1-xp) + (y1-yp)*(y1-yp) )
            if( d.lt.d1 ) then
               d1  = d
               z1  = z2d(tran%ibopo(ibopo))
            end if
         end do
!
!***     Find the closer point in the outer shell 
!
         d0 = 1d9
         do jpoin = 1,bufe%npoin
            if(bufe%myshell(jpoin).eq.2) then
              x0 = bufe%x2d(jpoin)
              y0 = bufe%y2d(jpoin)
              d  = sqrt( (x0-xp)*(x0-xp) + (y0-yp)*(y0-yp) )
              if( d.lt.d0 ) then
               d0  = d
              end if
            end if
         end do         
!
!***    Interpolates height
!
        z2d(abs(bufe%lpoty(ipoin))) = (d0*z1+d1*z0)/(d1+d0)  ! interpolate height
!
        end if   ! inner buffer shell
      end do 
!
      else   ! do not smooth buffer. Elevation already fixed
!
        continue
!
      end if
!
!***  6. Smooth topography
!
      if(smooth_topography) then
!
!        Initializations (default value)
!
         lambda_smooth = 0.6307_rp
         mu_smooth     = 1.0_rp/(0.1_rp - 1.0_rp/lambda_smooth)
! 
!        1. First loop over smoothing iterations in the whole domain
!
            do iiter = 1,niter_smooth               
!
!           Forward laplacian
!
               do ipoin = 1,npoin2d
                  if(lpoin2d(9,ipoin).eq.8) then    ! inner point (8 neighbours)
                     !
                     z_baric = 0.0_rp
                     do i = 1,8
                        z_baric = z_baric + z2d(lpoin2d(i,ipoin))
                     end do
                     z_baric = z_baric/8.0_rp 
                     !
                     z2d(ipoin) = z2d(ipoin) + lambda_smooth*(z_baric-z2d(ipoin))
                     !
                  end if
               end do
!
!           Backwards laplacian
!
               do ipoin = 1,npoin2d
                  if(lpoin2d(9,ipoin).eq.8) then    ! inner point (8 neighbours)
                     !
                     z_baric = 0.0_rp
                     do i = 1,8
                        z_baric = z_baric + z2d(lpoin2d(i,ipoin))
                     end do
                     z_baric = z_baric/8.0_rp 
                     !
                     z2d(ipoin) = z2d(ipoin) + mu_smooth*(z_baric-z2d(ipoin))
                     !
                 end if
              end do
           end do  ! iiter = 1,niter_smooth    
!
!         2. Second, another round for buffer and transition zones only
!
          do iiter = 1,2*niter_smooth               
!
!           Forward laplacian
!
            do ipoin = 1,npoin2d
               if(lpoin2d(9,ipoin).eq.8.and.  &   ! inner point (8 neighbours)
                  my_zone(ipoin)  .ne.2) then     ! buffer and transition
               !
               z_baric = 0.0_rp
               do i = 1,8
                  z_baric = z_baric + z2d(lpoin2d(i,ipoin))
               end do
               z_baric = z_baric/8.0_rp 
               !
               z2d(ipoin) = z2d(ipoin) + lambda_smooth*(z_baric-z2d(ipoin))
               !
               end if
            end do
!
!           Backwards laplacian
!
            do ipoin = 1,npoin2d
               if(lpoin2d(9,ipoin).eq.8.and.  &   ! inner point (8 neighbours)
                  my_zone(ipoin)  .ne.2) then     ! buffer and transition
               !
               z_baric = 0.0_rp
               do i = 1,8
                  z_baric = z_baric + z2d(lpoin2d(i,ipoin))
               end do
               z_baric = z_baric/8.0_rp 
               !
               z2d(ipoin) = z2d(ipoin) + mu_smooth*(z_baric-z2d(ipoin))
               !
               end if
            end do
          end do  ! iiter = 1,niter_smooth    
!
      end if    ! smooth_topography
!
!***  Finally, redefine ztop (top domain height) so that is the value relative to the maxval height of the domain
!
      ztop = ztop + maxval(z2d(1:npoin2d))
!
      return
      end subroutine settopg
