 
    subroutine out_gid_3d(fname,x,y,z,lnods,my_zone,npoin2d,nz,nnode,nelem)
!*************************************************
!*
!*     Output for GiD in 3d using
!*     x(npoin2d,nz)
!*     y(npoin2d,nz)
!*     z(npoin2d,nz)
!*
!*************************************************
     use KindType
     implicit none
!
     character(len=s_file) :: fname
     integer(ip)           :: npoin2d,nz,nnode,nelem
     integer(ip)           :: lnods(nnode,nelem),my_zone(nelem)
     real(rp)              :: x(npoin2d,nz),y(npoin2d,nz),z(npoin2d,nz)
!
     integer(ip) :: ipoin2d,ix,iy,iz,ielem,ipoin,inode
!
!***
!
     open(90,file=TRIM(fname),status='unknown')
!
!*** header
!    
     if(nnode.eq.8) then
         write(90,1) '    ',3,'Hexahedra',8
     else
        call runend(' out_gid_3d: incorrect number of nodes')
     end if
!
!*** coordinates
!
     write(90,2)'coordinates'
     ipoin = 0_ip
     do iz = 1,nz
        do ipoin2d = 1,npoin2d
           ipoin = ipoin + 1
           write(90,'(i8,3(1x,E14.7))') ipoin,x(ipoin2d,iz),y(ipoin2d,iz),z(ipoin2d,iz)
        end do
     end do

     write(90,2)'end coordinates'
     !
     !*** elements
     !
     write(90,2)'elements'
    do ielem = 1,nelem
       write(90,4) ielem,(lnods(inode,ielem),inode=1,nnode),my_zone(ielem)
    end do       
    write(90,2)'end elements'
!
 1  format('MESH ',a,' dimension ',i1,' Elemtype ',a,' Nnode ',i2)
 2  format(a)
 3  format(i9, 3(1x,e16.8e3))
 4  format(i9,50(1x,i9))
!
    return  
   end subroutine out_gid_3d
!
!
!
   subroutine out_gid_3d_arrays(fname,x,y,z,lnods,my_zone,npoin2d,nz,nnode,nelem)
!*************************************************
!*
!*     Output for GiD in 3d (hexahedras)
!*
!*************************************************
     use KindType
     implicit none
!
     character(len=s_file) :: fname
     integer(ip)           :: npoin2d,nz,nnode,nelem
     integer(ip)           :: lnods(nnode,nelem),my_zone(nelem)
     real(rp)              :: x(npoin2d),y(npoin2d),z(npoin2d,nz)
!
     integer(ip) :: ipoin2d,iz,ielem,ipoin,inode
!
!***
!
     open(90,file=TRIM(fname),status='unknown')
!
!*** header
!    
     if(nnode.eq.8) then
         write(90,1) '    ',3,'Hexahedra',8
     else
        call runend(' out_gid_3d: incorrect number of nodes')
     end if
!
!*** coordinates
!
    write(90,2)'coordinates'
    ipoin = 0
    do iz = 1,nz
       do ipoin2d = 1,npoin2d
          ipoin = ipoin + 1
          write(90,3) ipoin,x(ipoin2d),y(ipoin2d),z(ipoin2d,iz)
       end do
    end do
    write(90,2)'end coordinates'
!
!*** elements
!
    write(90,2)'elements'
    do ielem = 1,nelem
       write(90,4) ielem,(lnods(inode,ielem),inode=1,nnode),my_zone(ielem)
    end do       
    write(90,2)'end elements'
!
 1  format('MESH ',a,' dimension ',i1,' Elemtype ',a,' Nnode ',i2)
 2  format(a)
 3  format(i9, 3(1x,e16.8e3))
 4  format(i9,50(1x,i9))
!
    return  
   end subroutine out_gid_3d_arrays
!
!
!
    subroutine out_gid_2d(fname,x,y,z,lnods,my_zone,nnode,npoin,nelem)
!*************************************************
!*
!*     Output for GiD in 2d (quadrilaterals)
!*
!*************************************************
     use KindType
     implicit none
!
     character(len=s_file) :: fname
     integer(ip)           :: nnode,npoin,nelem
     integer(ip)           :: lnods(nnode,nelem),my_zone(nelem)
     real(rp)              :: x(npoin),y(npoin),z(npoin)
!
     integer(ip) ::ielem,ipoin,inode
!
!*** Opens file
!
     open(91,file=TRIM(fname),status='unknown')
!
!*** header
!    
     if(nnode.eq.3) then
         write(91,1) '    ',3,'Triangle',3
     else if(nnode.eq.4) then
         write(91,1) '    ',3,'Quadrilateral',4
     else
        call runend(' out_gid_2d: incorrect number of nodes')
     end if
!
!*** coordinates
!
    write(91,2)'coordinates'
    do ipoin = 1,npoin
       write(91,3) ipoin,x(ipoin),y(ipoin),z(ipoin)
    end do
    write(91,2)'end coordinates'
!
!*** elements
!
    write(91,2)'elements'
    do ielem = 1,nelem
       write(91,4) ielem,(lnods(inode,ielem),inode=1,nnode),my_zone(ielem)
    end do       
    write(91,2)'end elements'
!
 1  format('MESH ',a,' dimension ',i1,' Elemtype ',a,' Nnode ',i2)
 2  format(a)
 3  format(i9, 3(1x,e16.8))
 4  format(i9,50(1x,i9))
!
    return  
   end subroutine out_gid_2d
!
!
!
   subroutine out_gid_2d_t(fname,coord,lnods,my_zone,npoin,nnode,nelem)
!*************************************************
!*
!*     Output for GiD in 2D
!*
!*************************************************
     use KindType
     implicit none
!
     character(len=s_file) :: fname
     integer(ip)           :: npoin,nnode,nelem
     integer(ip)           :: lnods(nnode,nelem),my_zone(nelem)
     real(rp)              :: coord(3,npoin)
!
     integer(ip) :: ielem,ipoin,inode
!
!***
!
     open(91,file=TRIM(fname),status='unknown')
!
!*** header
!    
     if(nnode.eq.3) then
         write(91,1) '    ',3,'Triangle',3
     else if(nnode.eq.4) then
         write(91,1) '    ',3,'Quadrilateral',4
     else
        call runend(' out_gid_2d_t: incorrect number of nodes')
     end if
!
!*** coordinates
!
    write(91,2)'coordinates'
    do ipoin = 1,npoin
          write(91,3) ipoin,coord(1,ipoin),coord(2,ipoin),coord(3,ipoin)
    end do
    write(91,2)'end coordinates'
!
!*** elements
!
    write(91,2)'elements'
    do ielem = 1,nelem
       write(91,4) ielem,(lnods(inode,ielem),inode=1,nnode),my_zone(ielem)
    end do       
    write(91,2)'end elements'
!
 1  format('MESH ',a,' dimension ',i1,' Elemtype ',a,' Nnode ',i2)
 2  format(a)
 3  format(i9, 3(1x,e16.8e3))
 4  format(i9,50(1x,i9))
!
    return  
   end subroutine out_gid_2d_t
