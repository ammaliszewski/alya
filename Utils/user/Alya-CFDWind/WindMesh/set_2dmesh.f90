   subroutine set_2dmesh
  !***************************************************
  !*
  !*   Builds the 2D (surface) mesh, except for the
  !*   vertical coordinate. The global mesh is obtained
  !*   merging meshes for the different zones (build
  !*   first independently). The FINAL output is
  !* 
  !***************************************************
  use Master 
  use InpOut
  implicit none
  !
  logical                  :: origin,found,go_on
  integer(ip)              :: ix,iy,ipoin,jpoin,ielem,iboun,i,ipos,neigh,j
   real(rp)                :: xg,yg,xp1,yp1,cos_r,sin_r
  !
  !*** Writes to the log file
  !
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Building the 2D composed surface mesh with 3 differentiates zones...',/)
 !
  npoin2d = farm%npoin + tran%npoin - farm%nbopo + bufe%npoin - tran%nbopo
  nelem2d = farm%nelem + tran%nelem + bufe%nelem
  nboun2d = bufe%nboun 
  !
  allocate(my_zone       (  npoin2d))
  allocate(my_zone_elem2d(  nelem2d))
  allocate(lnods2d       (4,nelem2d))
  allocate(lnodb2d       (2,nboun2d))
  allocate(lface2d       (4,nelem2d))
  allocate(lcode2d       (4,nelem2d))
  allocate(lpoin2d       (9,npoin2d))
  !
  allocate(x2d      (npoin2d))
  allocate(y2d      (npoin2d))     
  allocate(z2d      (npoin2d))
  allocate(zo2d     (npoin2d))
  allocate(zo2d_elem(nelem2d))
  !
  z2d      (1:npoin2d) = 0.0_rp    ! default value
  zo2d     (1:npoin2d) = zo_def    ! default value
  zo2d_elem(1:nelem2d) = zo_def    ! default value
  !
  !*** Global mesh: 
  !      x2d(npoin2d)
  !      y2d(npoin2d)
  !      my_zone(npoin2d)
  !
  ipoin = 0
  do i = 1,farm%npoin 
     ipoin = ipoin + 1
     x2d(ipoin) = farm%x2d(i)
     y2d(ipoin) = farm%y2d(i)
     !
     my_zone(ipoin) = 2
  end do
  !
  do i = 1,tran%npoin 
     if(tran%lpoty(i).gt.0) then   ! exclude inner boundary points
        ipoin = ipoin + 1
        x2d(ipoin) = tran%x2d(i)
        y2d(ipoin) = tran%y2d(i)
        !
        my_zone(ipoin) = 1
     end if
  end do
  !
  do i = 1,bufe%npoin 
     if(bufe%lpoty(i).gt.0) then   ! exclude inner boundary points
        ipoin = ipoin + 1
        x2d(ipoin) = bufe%x2d(i)
        y2d(ipoin) = bufe%y2d(i)
        !
        my_zone(ipoin) = 0
     end if
  end do
  !
  !*** Global mesh:
  !      lnods2d(4,nelem2d) 
  !      lface2d(4,nelem2d)
  !      lcode2d(4,nelem2d)
  !      my_zone_elem2d(nelem2d)
  !
  ielem = 0
  do i = 1,farm%nelem
     ielem = ielem + 1
     lnods2d(1:4,ielem) = farm%lnods2d(1:4,i)
     lface2d(1:4,ielem) = farm%lface2d(1:4,i)
     lcode2d(1:4,ielem) = farm%lcode2d(1:4,i)
     !
     my_zone_elem2d(ielem) = 2
  end do
  !
  do i = 1,tran%nelem
     ielem = ielem + 1
     !
     lnods2d(1:4,ielem) = abs(tran%lpoty(tran%lnods2d(1:4,i)))
     lface2d(1:4,ielem) = tran%lface2d(1:4,i)
     lcode2d(1:4,ielem) = tran%lcode2d(1:4,i)
     !
     my_zone_elem2d(ielem) = 1
  end do
  !
  do i = 1,bufe%nelem
     ielem = ielem + 1
     !
     lnods2d(1:4,ielem) = abs(bufe%lpoty(bufe%lnods2d(1:4,i)))
     lface2d(1:4,ielem) = bufe%lface2d(1:4,i)
     lcode2d(1:4,ielem) = bufe%lcode2d(1:4,i)
     !
     my_zone_elem2d(ielem) = 0
  end do
  !
  !*** Global mesh:
  !      lnodb2d(2,nboun2d)
  !
  do iboun = 1,bufe%nboun
     lnodb2d(1:2,iboun) = abs(bufe%lpoty(bufe%lnodb2d(1:2,iboun)))
  end do
  !
  !*** Global mesh:
  !      lpoin2d(9,npooin2d)
  !   
  do ipoin = 1,npoin2d
     lpoin2d(1:9,ipoin) = 0
     !
     do ielem = 1,nelem2d
        found = .false.
        do i = 1,4
           if(lnods2d(i,ielem).eq.ipoin) found = .true.
        end do
        if(found) then
           do i = 1,4                    ! candidates (icluding ipoint itself)
              jpoin = lnods2d(i,ielem)
              if (ipoin.ne.jpoin) then
                  ipos  =  0
                  neigh =  1 
                  do j = 1,8                                         ! span over all possible neighbours
                     if( lpoin2d(j,ipoin).eq.jpoin)     neigh = -1   ! already  counter for
                     if((lpoin2d(j,ipoin).eq.0).and. &       
                        (ipos                  .eq.0) ) ipos  = j    ! position
                  end do
                  if(neigh.eq.1) then 
                     lpoin2d(ipos,ipoin) = jpoin
                     lpoin2d(9   ,ipoin) = ipos                      ! number of neighbours counted so far
                  end if
              end if
           end do
        end if      ! not found
     end do         ! ielem = 1,nelem2d
  end do            ! ipoin = 1,npoin2d
  !
  !*** If necessary, rotate the mesh coordinates
  !
  if(rotate_mesh) then
     !
     write(lulog,4)
     if(out_screen) write(*,4)
  4  format(/,'---> Rotating the 2D surface mesh...',/)
     !
     !***  Determine the center of gravity of the farm zone (where the rotation axiz along z 
     !***  is assumed).
     !
     xg = farm%xi + 0.5*farm%nelem_x1*farm%cell_size_x
     yg = farm%yi + 0.5*farm%nelem_y1*farm%cell_size_y
     !
     !*** Translation of origin, rotation along z, and re-traslation
     ! 
     cos_r = cos(-rotation_angle)  ! rotate incident
     sin_r = sin(-rotation_angle)
     !
     do i = 1,npoin2d
        xp1 = x2d(i) - xg
        yp1 = y2d(i) - yg
        x2d(i) = cos_r*xp1 - sin_r*yp1
        y2d(i) = sin_r*xp1 + cos_r*yp1
        x2d(i) = x2d(i) + xg
        y2d(i) = y2d(i) + yg
     end do
     !
     !*** Rotate and store the zone limits
     !
     farm%xv(1) = farm%xi
     farm%yv(1) = farm%yi
     farm%xv(2) = farm%xf
     farm%yv(2) = farm%yi
     farm%xv(3) = farm%xf
     farm%yv(3) = farm%yf
     farm%xv(4) = farm%xi
     farm%yv(4) = farm%yf
     do i=1,4
        xp1 = farm%xv(i) - xg
        yp1 = farm%yv(i) - yg   
        farm%xv(i) = xg + cos_r*xp1 - sin_r*yp1
        farm%yv(i) = yg + sin_r*xp1 + cos_r*yp1
     end do
     !
     tran%xv(1) = tran%xi
     tran%yv(1) = tran%yi
     tran%xv(2) = tran%xf
     tran%yv(2) = tran%yi
     tran%xv(3) = tran%xf
     tran%yv(3) = tran%yf
     tran%xv(4) = tran%xi
     tran%yv(4) = tran%yf
     do i=1,4
        xp1 = tran%xv(i) - xg
        yp1 = tran%yv(i) - yg   
        tran%xv(i) = xg + cos_r*xp1 - sin_r*yp1
        tran%yv(i) = yg + sin_r*xp1 + cos_r*yp1
     end do
  !
  else     ! do not rotate_mesh
  !
     farm%xv(1) = farm%xi
     farm%yv(1) = farm%yi
     farm%xv(2) = farm%xf
     farm%yv(2) = farm%yi
     farm%xv(3) = farm%xf
     farm%yv(3) = farm%yf
     farm%xv(4) = farm%xi
     farm%yv(4) = farm%yf
     !
     tran%xv(1) = tran%xi
     tran%yv(1) = tran%yi
     tran%xv(2) = tran%xf
     tran%yv(2) = tran%yi
     tran%xv(3) = tran%xf
     tran%yv(3) = tran%yf
     tran%xv(4) = tran%xi
     tran%yv(4) = tran%yf
  !
  end if  
  !
  return
  end subroutine set_2dmesh
