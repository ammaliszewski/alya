     subroutine checktopg
!***************************************************
!*
!*   Checks the quality of the mesh interpolation by
!*   computing the 2d mesh elevation of the topography
!*   points
!*
!***************************************************
     use Master 
     use InpOut
     implicit none
!
     logical     :: found
     integer(ip) :: i_top,npts,ielem
     real   (rp) :: xtopo,ytopo,ztopo,zmesh,s,t,err,meanerr,maxerr,minerr
     real   (rp) :: xmin,xmax,ymin,ymax
     real(rp)    :: sm,tm,sp,tp,shape(4)
!
!*** No real check needed 
!
     if(n_zo.eq.0) return
!
!*** Writes to the log file
!
      write(lulog,1)
      if(out_screen) write(*,1)
  1   format(/,'---> Cheking the quality of the topography interpolation...',/, &
               '     (only for topography points inside the farm and transition zones)')
!
!*** Loop over topography points
!
     npts = 0
     meanerr = 0.0_rp
     maxerr  = 0.0_rp
     minerr  = 1d20
     do i_top = 1, n_top
!
!***    Check if the topography point lays in the transition zone
!
        xtopo = x_top(i_top)
        ytopo = y_top(i_top)
        ztopo = z_top(i_top)
        if( xtopo.ge.tran%xi.and.xtopo.le.tran%xf.and. & 
            ytopo.ge.tran%yi.and.ytopo.le.tran%yf) then
!
          npts = npts + 1
!
!***      Gets the element and position of (x_topo,y_topo) to interpolate
!
          found = .false.
          ielem = 0
          do while(.not.found)
             ielem = ielem + 1
             if(my_zone_elem2d(ielem).ne.0) then  ! skip the buffer zone
!
                xmin = minval(x2d(lnods2d(1:4,ielem)))
                xmax = maxval(x2d(lnods2d(1:4,ielem)))
                ymin = minval(y2d(lnods2d(1:4,ielem)))
                ymax = maxval(y2d(lnods2d(1:4,ielem)))
!
                if( xtopo.ge.xmin.and.xtopo.le.xmax.and. & 
                    ytopo.ge.ymin.and.ytopo.le.ymax) then    
!
!***                Element found
!
                    found = .true.
                    s = (xtopo-xmin)/(xmax-xmin)   ! parameter s in (0,1)
                    s = 2.0*s-1.0                  ! parameter s in (-1,1)    
                    t = (ytopo-ymin)/(ymax-ymin)   ! parameter t in (0,1)
                    t = 2.0*t-1.0                  ! parameter s in (-1,1)
!
!***                Interpolates (bilinear)
!
                    sm = 0.5*(1.0-s)
                    tm = 0.5*(1.0-t)
                    sp = 0.5*(1.0+s)
                    tp = 0.5*(1.0+t)
                    shape(1) = sm*tm
                    shape(2) = sp*tm
                    shape(3) = sp*tp
                    shape(4) = sm*tp
!
                    zmesh = shape(1)*z2d(lnods2d(1,ielem)) +  &
                            shape(2)*z2d(lnods2d(2,ielem)) +  &
                            shape(3)*z2d(lnods2d(3,ielem)) +  &
                            shape(4)*z2d(lnods2d(4,ielem))
!
!***                Errrors
!
                    if(ztopo.gt.1d-3) then 
                       err = 100*abs(ztopo-zmesh)/ztopo
                       if(err.gt.maxerr) maxerr = err
                       if(err.lt.minerr) minerr = err
                       meanerr = meanerr + err
                    end if
                end if   ! element found
             end if  
             if((ielem.eq.nelem2d).and.(.not.found)) call runend('Checktopg: element not found')       
         end do
!
       end if
     end do   ! topography points
!
     write(lulog,10) npts,maxerr,minerr,meanerr/npts
 10  format(/,&
            'Number of points = ',i11         ,/, &
            'Maximum error    = ',f11.3,' (%)',/, &
            'Minimum error    = ',f11.3,' (%)',/, &
            'Mean error       = ',f11.3,' (%)')
!
     return
     end subroutine checktopg
