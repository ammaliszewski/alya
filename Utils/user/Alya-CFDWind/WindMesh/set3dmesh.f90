  subroutine set3dmesh
  !***************************************************
  !*
  !*   Builds the 3d mesh from the 2d surface mesh. 
  !*   Outputs are:
  !*
  !*   nz
  !*   npoin
  !*   nelem
  !*   nboun
  !*   nface
  !*
  !*   z3d  (npoin2d,nz)  
  !*   x3d  (npoin2d,nz) 
  !*   y3d  (npoin2d,nz)
  !*   lnods(8,nelem)
  !*   lnodb(4,nboun)
  !*   lelmb(nboun)
  !*   my_zone_elem3d(nelem)
  !*   my_zone_boun  (nboun)
  !*
  !***************************************************
  use KindType 
  use Master
  use InpOut
  implicit none
  !
  integer(ip) :: ipoin,ipoin2d,iz,ielem,ielem2d,iboun,kboun,i,icell
  integer(ip) :: iiter,niter,nzbl,jz
  real   (rp) :: l,dz,alpha,alphamin,alphamax,toler, rageo, z, distz
  real(rp)    :: lambda_smooth,mu_smooth,x_baric,y_baric
  real, allocatable, dimension(:) :: eta
  !
  !*** Writes to the log file
  !
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Building the 3d mesh...',/)
  !
  !*** Initializes and allocates
  !
  nz    = ncellz + 1
  npoin = npoin2d*nz
  nelem = nelem2d*ncellz
  nboun = 2*nelem2d + bufe%nbopo*ncellz
  !
  allocate(x3d(npoin2d,nz))
  allocate(y3d(npoin2d,nz))
  allocate(z3d(npoin2d,nz))
  allocate(eta(nz))
  allocate(lnods(8,nelem ))
  allocate(lnodb(4,nboun ))
  allocate(lelmb(nboun))  
  allocate(my_zone_boun(nboun))
  allocate(my_zone_elem3d(nelem))
  !
  !*** Initial x-y coordinates of the 3D grid
  !
  do iz = 1,nz
  do ipoin2d = 1,npoin2d
     x3d(ipoin2d,iz) = x2d(ipoin2d)
     y3d(ipoin2d,iz) = y2d(ipoin2d)
  end do
  end do
  !
  !*** Checks
  !
  if(ztop.le.maxval(z2d(:))) call runend('set3dmesh: value of ztop is less than the topography maximum')
  !
  !*** Build the logical (uniform) mesh in (0,1)
  !
  dz = 1.0_rp/ncellz
  eta(1) = 0.0_rp
  do icell = 1,ncellz
     iz = icell + 1
     eta(iz) = eta(iz-1) + dz
  end do
  eta(nz) = 1.0_rp 
  !
  !*** Loop over surface points to find the vertical coordinates.
  !*** Computes z3d(npoin2d,nz)
  !
  if (TRIM(dz_distribution).eq.'GEOMETRIC') then 
     l     =  ztop 
     dz0   =  min(dz0,l/ncellz)  ! this avoids possible user incosistent size and number of cells

     do ipoin = 1,npoin2d
        l  = ztop - z2d(ipoin) 
        !
        ! geometric ratio
        !
        rageo = 1.02d0
        do while ((rageo**(ncellz)-1)/(rageo-1.0d0).lt.l/dz0) 
           rageo = rageo + 0.002d0
        end do
        !
        dz = dz0      
        z3d(ipoin,1) = z2d(ipoin)
        z            = z2d(ipoin) + dz0
        z3d(ipoin,2) = z

        do iz =3, nz
           dz    = dz*(rageo+0.02d0)
           distz = (ztop -z)/dfloat(nz-iz+1)
           if (dz.gt.distz) then
              do jz = iz, nz
                 z3d(ipoin,jz) = z + distz*dfloat(jz-iz+1)
              end do
              exit 
           end if
           z = z + dz
           z3d(ipoin,iz) = z
        end do
     end do
  !
  else   ! default case: constant spacing
     do ipoin = 1,npoin2d
        l  = ztop - z2d(ipoin) 
        dz = l/ncellz
        ! 
        !       Loop over vertical cells
        !
        z3d(ipoin,1) = z2d(ipoin)
        do icell = 1,ncellz
           iz = icell + 1
           z3d(ipoin,iz) = z3d(ipoin,iz-1) + dz
        end do
     end do
     !
  end if
  !
  !***    lnods(8,nelem)
  !***    lnodb(4,nboun)
  !***    lelmb(nboun)
  !***    my_zone_boun(nboun)
  !***    my_zone_elem3d(nelem)
  !
  ielem = 0
  iboun = 0
  do iz = 1,ncellz
     do ielem2d = 1,nelem2d
        !
        ielem = ielem + 1
        my_zone_elem3d(ielem) = 0  ! inner element
        !
        lnods(1,ielem) = (iz-1)*npoin2d + lnods2d(1,ielem2d)
        lnods(2,ielem) = (iz-1)*npoin2d + lnods2d(2,ielem2d)
        lnods(3,ielem) = (iz-1)*npoin2d + lnods2d(3,ielem2d)
        lnods(4,ielem) = (iz-1)*npoin2d + lnods2d(4,ielem2d)
        !
        lnods(5,ielem) = (iz  )*npoin2d + lnods2d(1,ielem2d)
        lnods(6,ielem) = (iz  )*npoin2d + lnods2d(2,ielem2d)
        lnods(7,ielem) = (iz  )*npoin2d + lnods2d(3,ielem2d)
        lnods(8,ielem) = (iz  )*npoin2d + lnods2d(4,ielem2d)
        !
        ! Sense of rotation so that the normal points outwards. This is 
        ! a requirement of OpenFoam. Note that Alya does the opposite (this
        ! is accounted for later, when writting results in Alya format)
        !
        if(iz.eq.1) then                   ! Surface (code 5)
           iboun = iboun + 1
           my_zone_elem3d(ielem) = 5
           my_zone_boun  (iboun) = 5
           lelmb(iboun)          = ielem
           !
           lnodb(1,iboun) = (iz-1)*npoin2d + lnods2d(1,ielem2d)
           lnodb(2,iboun) = (iz-1)*npoin2d + lnods2d(4,ielem2d)      
           lnodb(3,iboun) = (iz-1)*npoin2d + lnods2d(3,ielem2d)      
           lnodb(4,iboun) = (iz-1)*npoin2d + lnods2d(2,ielem2d)
        end if
        if(iz.eq.ncellz) then              ! Top (code 6)
           iboun = iboun + 1
           my_zone_elem3d(ielem) = 6
           my_zone_boun  (iboun) = 6
           lelmb(iboun)          = ielem
           !
           lnodb(1,iboun) = (iz  )*npoin2d + lnods2d(2,ielem2d)
           lnodb(2,iboun) = (iz  )*npoin2d + lnods2d(3,ielem2d)           
           lnodb(3,iboun) = (iz  )*npoin2d + lnods2d(4,ielem2d)
           lnodb(4,iboun) = (iz  )*npoin2d + lnods2d(1,ielem2d)
        end if
        do i = 1,4
           kboun = lface2d(i,ielem2d)
           if(kboun.gt.0) then
              iboun = iboun + 1
              my_zone_elem3d(ielem) = lcode2d(i,ielem2d)
              my_zone_boun  (iboun) = lcode2d(i,ielem2d)
              lelmb(iboun)          = ielem        
              !
              lnodb(1,iboun) = (iz-1)*npoin2d + lnodb2d(1,kboun)
              lnodb(2,iboun) = (iz-1)*npoin2d + lnodb2d(2,kboun)
              lnodb(3,iboun) = (iz  )*npoin2d + lnodb2d(2,kboun)
              lnodb(4,iboun) = (iz  )*npoin2d + lnodb2d(1,kboun)
           end if
        end do
     end do
  end do
  !
  !*** Outputs the results
  !
   write(lulog,11) npoin,nelem,nboun
11 format('Number of nodes                 = ',i9,/, &
          'Number of hexahedra elements    = ',i9,/, &
          'Number of outer boundaries      = ',i9,/)
  !
  return
  end subroutine set3dmesh
