module Kdtree
  use KindType, only : ip,rp
  implicit none
!
  type netyp
     integer(ip)          :: nelem             ! Number of elements for a node
     integer(ip), pointer :: eleme(:)          ! Ids of elements
  end type netyp
!
  
contains
!
  subroutine setkdtree(&
       itask,ndime,nnode,mnodb,npoib,nboun,fabox,bobox,coord,lnodb,&
       sabox,blink,stru2,ldist,lnele)
    !-----------------------------------------------------------------------
    !****f* domain/kdtree
    ! NAME
    !    kdtree
    ! DESCRIPTION
    !    Skd-tree construction. If ITASK=1 memory is allocated for the 
    !    structure at the beginning and for the search at the end
    ! INPUT
    !    ITASK ... If=1 FABOX is computed here
    !    NBOUN ... Number of boundaries
    !    FABOX ... Bounding box of each boundary
    !    BOBOX ... Complete boundaing box
    !    IF ITASK = 1 
    !      COORD ... 
    !      LNODB ... 
    !      LTYPB ... 
    ! OUTPUT
    !    SABOX ... Bounding box kd-tree structure
    !    BLINK ... Index of tree
    ! USED BY
    !    nepoib
    !***
    !----------------------------------------------------------------------- 
    implicit none
    integer(ip), intent(in)             :: itask
    integer(ip), intent(in)             :: ndime
    integer(ip), intent(in)             :: nnode
    integer(ip), intent(in)             :: mnodb
    integer(ip), intent(in)             :: npoib
    integer(ip), intent(in)             :: nboun
    integer(ip), intent(in)             :: lnodb(mnodb,nboun)
    integer(ip), pointer                :: stru2(:)

    type(netyp), intent(inout), pointer :: lnele(:)

    real(rp),    intent(in)             :: coord(ndime,npoib)
    real(rp),    intent(out),   pointer :: sabox(:,:,:)
    integer(ip), intent(out),   pointer :: blink(:)
    real(rp),    intent(inout), pointer :: fabox(:,:,:)
    real(rp),    intent(inout), pointer :: ldist(:)
    real(rp),    intent(inout)          :: bobox(3,2)

    integer(ip), pointer                :: perm(:)
    integer(ip), pointer                :: struc(:,:)
    real(rp),    pointer                :: vect3(:)
    real(rp),    pointer                :: centr(:,:)
    integer(ip)                         :: idime,ipoib,iboun,inodb
    integer(ip)                         :: indst,next,curr,imini,imaxi
    integer(ip)                         :: ladim,irang,krang,idim1
    integer(ip)                         :: jface,nfac2,imedi
    integer(4)                          :: istat
    real(rp)                            :: toler,dista

    if( itask == 2 ) then
       !
       ! Deallocate kd-tree structure memory
       !      
       do ipoib = 1,npoib
          deallocate( lnele(ipoib)%eleme )
       end do
       deallocate( lnele )
       deallocate( stru2 )
       deallocate( ldist )
       deallocate( fabox )
       deallocate( sabox )
       deallocate( blink )
       
    else if( itask == 1 ) then
       !
       ! Construct FABOX and BOBOX
       !
       allocate( fabox(2,ndime,max(1,nboun)) )
       allocate( sabox(2,ndime,max(1,2*nboun-1)) )
       allocate( blink(max(1,2*nboun-1)) )
       allocate( lnele(npoib) )
       !
       ! Allocate memory for search
       !
       allocate( stru2(2*nboun-1),stat=istat)
       allocate( ldist(2*nboun-1),stat=istat)
       !
       ! Construct LNELE. List of connected elements for each node 
       !
       do ipoib = 1,npoib
          lnele(ipoib)%nelem = 0_ip
       end do
      do iboun = 1,nboun
          do inodb = 1,mnodb          
             ipoib = lnodb(inodb,iboun)    
             lnele(ipoib)%nelem = lnele(ipoib)%nelem + 1_ip
          end do
       end do
       do ipoib = 1,npoib
          allocate( lnele(ipoib)%eleme(lnele(ipoib)%nelem) )
       end do
       do ipoib = 1,npoib
          lnele(ipoib)%nelem = 0_ip
       end do
       do iboun = 1,nboun
          do inodb = 1,mnodb          
             ipoib = lnodb(inodb,iboun)       
             lnele(ipoib)%nelem = lnele(ipoib)%nelem + 1_ip     
             lnele(ipoib)%eleme(lnele(ipoib)%nelem) = iboun
          end do
       end do

    end if

    if( itask /= 2 ) then
       
       toler = 1.0e-3_rp
       do iboun = 1,nboun
          do idime = 1,ndime
             fabox(1,idime,iboun) =  1.0e12_rp
             fabox(2,idime,iboun) = -1.0e12_rp
             do inodb = 1,nnode ! nnode(ltypb(iboun))
                ipoib = lnodb(inodb,iboun)
                fabox(1,idime,iboun) = min( fabox(1,idime,iboun) , coord(idime,ipoib) )
                fabox(2,idime,iboun) = max( fabox(2,idime,iboun) , coord(idime,ipoib) )
             end do
             dista = fabox(2,idime,iboun) - fabox(1,idime,iboun)
             if (dista < 1.0e-3) then
                fabox(1,idime,iboun) = fabox(1,idime,iboun) - 1.0e-3_rp
                fabox(2,idime,iboun) = fabox(2,idime,iboun) + 1.0e-3_rp
             else
                fabox(1,idime,iboun) = fabox(1,idime,iboun) - abs(dista*toler)
                fabox(2,idime,iboun) = fabox(2,idime,iboun) + abs(dista*toler)
             end if
          end do
       end do
       do idime = 1,ndime
          bobox(idime,1) =  1.0e12_rp
          bobox(idime,2) = -1.0e12_rp
       end do
       do iboun = 1,nboun
          do idime = 1,ndime
             bobox(idime,1) = min( bobox(idime,1) , fabox(1,idime,iboun) )
             bobox(idime,2) = max( bobox(idime,2) , fabox(2,idime,iboun) )
          end do
       end do
       !
       ! Allocate memory
       !
       allocate( perm(nboun),        stat = istat )
       allocate( centr(ndime,nboun), stat = istat )
       allocate( struc(2*nboun-1,3), stat = istat )

      !
       ! Determine the centroid values for each boundig box of a face
       !
       do iboun = 1,nboun
          do idime = 1,ndime
             centr(idime,iboun) = ( fabox(2,idime,iboun) + fabox(1,idime,iboun) ) * 0.5_rp
          end do
          perm(iboun) = iboun
       end do
       !
       ! Store the total bounding box 
       !
       do idime = 1,ndime
          sabox(1,idime,1) = bobox(idime,1)
          sabox(2,idime,1) = bobox(idime,2)
       end do

       indst      = 1_ip
       struc(1,1) = 1_ip
       struc(1,2) = 1_ip
       struc(1,3) = nboun
       next       = 2_ip
       !
       ! Tree strucure construction 
       !
       do while( indst > 0_ip )

          curr  = struc(indst,1)
          imini = struc(indst,2)
          imaxi = struc(indst,3)
          indst = indst - 1_ip    

          if( imaxi == imini ) then
             blink(curr) = -perm(imaxi)
          else
             allocate( vect3(imaxi-imini+1),stat=istat)
             imedi = int ( real(imaxi+imini) * 0.5_rp )
             !
             ! Choose the largest dimension of the current bounding box, sabox(curr)
             !
             ladim = 1_ip
             if ( (sabox(2,2,curr)-sabox(1,2,curr)) > (sabox(2,1,curr)-sabox(1,1,curr)) ) ladim = 2
             if ( (sabox(2,ndime,curr)-sabox(1,ndime,curr)) > (sabox(2,2,curr)-sabox(1,2,curr)) ) ladim = ndime
             !
             ! Reorder perm(imini:imaxi) with the centroid values
             !
             krang =  imaxi - imini + 1_ip
             irang =  0
             do idim1 = imini,imaxi
                irang = irang + 1
                vect3(irang) = centr(ladim,perm(idim1))
             end do
             !
             ! Sort VECT2 using VECT3 in increasing order
             !
             call heapsortri(2_ip,krang,vect3,perm(imini:))
             !
             ! The two children of node curr are locate at blink(curr) y blink(curr+1)
             ! 
             blink(curr) = next
             !
             ! Determine the total bounding box for each children 
             !
             sabox(1,1,next)     =  1.0e6_rp
             sabox(2,1,next)     = -1.0e6_rp 
             sabox(1,2,next)     =  1.0e6_rp
             sabox(2,2,next)     = -1.0e6_rp 
             sabox(1,ndime,next) =  1.0e6_rp
             sabox(2,ndime,next) = -1.0e6_rp 
             nfac2 = imedi - imini + 1_ip
             do iboun = 1,nfac2
                jface = perm(imini+iboun-1)
                sabox(1,1,next)     = min( fabox(1,1,jface) , sabox(1,1,next) ) 
                sabox(2,1,next)     = max( fabox(2,1,jface) , sabox(2,1,next) ) 
                sabox(1,2,next)     = min( fabox(1,2,jface) , sabox(1,2,next) ) 
                sabox(2,2,next)     = max( fabox(2,2,jface) , sabox(2,2,next) ) 
                sabox(1,ndime,next) = min( fabox(1,ndime,jface) , sabox(1,ndime,next) ) 
                sabox(2,ndime,next) = max( fabox(2,ndime,jface) , sabox(2,ndime,next) ) 
             end do

             sabox(1,1,next+1)     =  1.0e6_rp
             sabox(2,1,next+1)     = -1.0e6_rp
             sabox(1,2,next+1)     =  1.0e6_rp
             sabox(2,2,next+1)     = -1.0e6_rp
             sabox(1,ndime,next+1) =  1.0e6_rp
             sabox(2,ndime,next+1) = -1.0e6_rp

             nfac2 = imaxi-imedi
             do iboun = 1,nfac2
                jface = perm(imedi+iboun) 
                sabox(1,1,next+1)     = min( fabox(1,1,jface) , sabox(1,1,next+1) )               
                sabox(2,1,next+1)     = max( fabox(2,1,jface) , sabox(2,1,next+1) ) 
                sabox(1,2,next+1)     = min( fabox(1,2,jface) , sabox(1,2,next+1) )               
                sabox(2,2,next+1)     = max( fabox(2,2,jface) , sabox(2,2,next+1) ) 
                sabox(1,ndime,next+1) = min( fabox(1,ndime,jface) , sabox(1,ndime,next+1) )               
                sabox(2,ndime,next+1) = max( fabox(2,ndime,jface) , sabox(2,ndime,next+1) ) 
             end do
             !
             ! Store the children of current element of the stack (struc)
             !
             indst          = indst + 1_ip
             struc(indst,1) = next
             struc(indst,2) = imini
             struc(indst,3) = imedi                
             indst          = indst + 1_ip
             struc(indst,1) = next  + 1_ip
             struc(indst,2) = imedi + 1_ip
             struc(indst,3) = imaxi             
             next           = next  + 2_ip        

             deallocate( vect3,stat=istat)
          end if

       end do

       deallocate( centr, stat = istat )
       deallocate( perm,  stat = istat )
       deallocate( struc, stat = istat )

    end if

  end subroutine setkdtree

  subroutine dpopar(&
       itask,ndime,nnode,xcoor,sabox,blink,npoib,mnodb,nboun,chkdi,&
       lnodb,coord,lnele,fabox,stru2,ldist,dista,norma,proje,&
       bound)
    !-----------------------------------------------------------------------
    ! NAME
    !    nepoib
    ! DESCRIPTION
    !    Shortest signed distance from a given point to the particle
    !    INPUT
    !       XCOOR ... Test point coordinates
    !       SABOX ... Tree stru2ture
    !       BLINK ... Tree stru2ture
    !       NBOUN ... Number of boundaries
    !       LTYPB ... Types of boundaries
    !       LNODB ... Boundary connectivity
    !       COORD ... Boundary node coordinates
    !       CHKDI ... Distance use to check the boundary box of the faces in the particle 
    !                 for find the shortest distance (use chkdi = 1.0e10_rp in general)
    !    OUTPUT
    !       DISTA ... is less than 0 is point is inside the particle
    !                 is more than 0 is point is outside the particle
    !       NORMA ... Normal to the plane that contains the projection of the point
    !       PROJE ... Nearest projection point to the particle surface
    !       
    ! USED BY
    !    inouib
    !----------------------------------------------------------------------- 
    implicit none
    integer(ip), intent(in)             :: itask
    integer(ip), intent(in)             :: ndime
    integer(ip), intent(in)             :: nnode
    integer(ip), intent(in)             :: npoib
    integer(ip), intent(in)             :: mnodb
    integer(ip), intent(in)             :: nboun
    integer(ip), intent(in)             :: lnodb(mnodb,nboun)
    real(rp),    intent(in)             :: xcoor(ndime)
    real(rp),    intent(in)             :: sabox(2,ndime,2*nboun-1)
    integer(ip), intent(in)             :: blink(2*nboun-1)
    real(rp),    intent(in)             :: fabox(2,ndime,nboun)
    real(rp),    intent(in)             :: chkdi
    real(rp),    intent(in)             :: coord(ndime,npoib)
    type(netyp), intent(inout)          :: lnele(npoib)
    real(rp),    intent(out)            :: dista
    real(rp),    intent(out)            :: norma(ndime)
    real(rp),    intent(out)            :: proje(ndime)
    integer(ip), intent(out)            :: bound
    integer(ip), intent(inout), pointer :: stru2(:)
    real(rp),    intent(inout), pointer :: ldist(:)
    integer(ip)                         :: idime,ipoib,indst,inodb,curr,ifoun
    integer(ip)                         :: pblty,pnodb,iboun,jboun,kboun,ilist
    integer(ip)                         :: jlist,node1,node2
    real(rp)                            :: temdi,dismi1,dismi2,temp,tmpfa,facto
    real(rp)                            :: tenor(3),propo(3),toler,pladi,numer
    real(rp)                            :: bocod(ndime,mnodb),bari1,bari2,denom

    toler = 1.0e-6_rp    

    dista = 0.0_rp
    do idime = 1,ndime
       temp  = max(xcoor(idime)-sabox(1,idime,1), sabox(2,idime,1)-xcoor(idime))
       dista = dista + temp*temp
    end do
    bound = 0_ip
    if ( chkdi*chkdi < dista ) then       
       dista = chkdi*chkdi       
    end if

    indst        = 1_ip
    stru2(indst) = 1_ip

    dismi1 = 0.0_rp
    do idime = 1,ndime
       temp  = max(0.0_rp,sabox(1,idime,1)-xcoor(idime))  
       temp  = max(temp,xcoor(idime)-sabox(2,idime,1))  
       dismi1 = dismi1 + temp * temp
    end do
    ldist(1) = dismi1
    !
    ! Assemble a list of candidate patches by traversing skd-tree
    !
    do while( indst > 0_ip )
       curr  = stru2(indst)      
       indst = indst - 1_ip
       !
       ! Minimum distance
       !       
       if ( ldist(curr) < dista ) then
          !
          ! If currnode is a leaf in the tree stru2ture
          !      
          if( blink(curr) < 0_ip ) then
             !
             ! Find the closest projection to the particle 
             !
             iboun = -blink(curr)
!             pblty =  ltypb(iboun)
             pnodb =  nnode  ! nnode(pblty)        
             !
             !norma: Exterior normal           
             !
             call extbou(itask,ndime,pnodb,lnodb(1,iboun),coord,tenor)

             do inodb = 1,pnodb
                ipoib = lnodb(inodb,iboun)            
                bocod(1    ,inodb) = coord(1     ,ipoib)
                bocod(2    ,inodb) = coord(2     ,ipoib)
                bocod(ndime,inodb) = coord(ndime,ipoib)                
             end do

             call dpofac(ndime,pnodb,xcoor,bocod,fabox(1,1,iboun),tenor,temdi,tmpfa,propo) 

             if( temdi < dista ) then               
                dista = temdi
                bound = iboun
                facto = tmpfa

                norma(1)     = tenor(1)
                norma(2)     = tenor(2)
                norma(ndime) = tenor(ndime)

                proje(1)     = propo(1)
                proje(2)     = propo(2)
                proje(ndime) = propo(ndime)             
             end if

          else                         

             dismi1 = 0.0_rp
             dismi2 = 0.0_rp
             do idime = 1,ndime
                temp = 0.0_rp
                temp  = max(0.0_rp,sabox(1,idime,blink(curr))-xcoor(idime))  
                temp  = max(temp,xcoor(idime)-sabox(2,idime,blink(curr)))  
                dismi1 = dismi1 + temp * temp

                temp = 0.0_rp
                temp  = max(0.0_rp,sabox(1,idime,blink(curr)+1_ip)-xcoor(idime))  
                temp  = max(temp,xcoor(idime)-sabox(2,idime,blink(curr)+1_ip) )  
                dismi2 = dismi2 + temp * temp
             end do
             ldist(blink(curr))    = dismi1             
             ldist(blink(curr)+1)  = dismi2

             indst          = indst + 2_ip
             if (dismi1 > dismi2) then               
                stru2(indst-1) = blink(curr)
                stru2(indst)   = blink(curr) + 1_ip
             else
                stru2(indst-1) = blink(curr) + 1_ip
                stru2(indst)   = blink(curr)                              
             end if

          end if
       end if
    end do

    if (bound /= 0_ip) then
       !
       ! Determine if the projection point is near to a node or a segment of the face
       !
       iboun = bound
!       pblty = ltypb(iboun)
       pnodb = nnode ! nnode(pblty)        

       do inodb = 1,pnodb
          ipoib = lnodb(inodb,iboun)            
          bocod(1    ,inodb) = coord(1    ,ipoib)
          bocod(2    ,inodb) = coord(2    ,ipoib)
          bocod(ndime,inodb) = coord(ndime,ipoib)                
       end do

       if( ndime == 3 ) then
          !
          ! 3D
          !
          call instr2(ndime,pnodb,proje,bocod,ifoun,bari1,bari2)

          node1 = 0_ip
          node2 = 0_ip
          !
          ! Check if the projection point is near to a node
          !
          if ( bari1 > 1.0_rp-toler ) then
             node1 = lnodb(3,iboun)       
          else if ( bari2 > 1.0_rp-toler ) then
             node1 = lnodb(2,iboun)       
          else if ( 1.0_rp-bari1-bari2  > 1.0_rp-toler ) then
             node1 = lnodb(1,iboun)       
             !
             ! Check if the projection point is near to a node
             !
          else if ( bari1 < toler ) then
             node1 = lnodb(1,iboun)     
             node2 = lnodb(2,iboun)     
          else if ( bari2 < toler ) then
             node1 = lnodb(1,iboun)     
             node2 = lnodb(3,iboun)     
          else if ( 1.0_rp-bari1-bari2 < toler ) then
             node1 = lnodb(2,iboun)     
             node2 = lnodb(3,iboun)     
          end if
          !
          ! It is near to a segmnent
          !
          if ( node2 /= 0_ip ) then
             norma(1)     = 0.0_rp
             norma(2)     = 0.0_rp
             norma(ndime) = 0.0_rp
             !
             ! Sum all the exterior normals that use the segment
             !       
             do ilist = 1,lnele(node1)%nelem
                do jlist = 1,lnele(node2)%nelem
                   jboun = lnele(node1)%eleme(ilist)
                   kboun = lnele(node2)%eleme(jlist)
                   if (jboun == kboun) then
                      call extbou(itask,ndime,pnodb,lnodb(1,jboun),coord,tenor)
                      do idime = 1,ndime  
                         norma(idime) = norma(idime) + tenor(idime)
                      end do
                   end if
                end  do
             end  do
             !
             ! Determine if the point are really inside or outside the particle
             !       
             pladi = 0.0_rp
             do idime = 1,ndime
                pladi = pladi + norma(idime) * ( xcoor(idime) - bocod(idime,1) )
             end do
             facto = sign(1.0_rp,pladi)
             !
             ! It is near to a node
             !
          else if ( node1 /= 0_ip ) then

             norma(1)     = 0.0_rp
             norma(2)     = 0.0_rp
             norma(ndime) = 0.0_rp
             !
             ! Sum all the exterior normals that use the node 
             !       
             do ilist = 1,lnele(node1)%nelem
                jboun = lnele(node1)%eleme(ilist)
                call extbou(itask,ndime,pnodb,lnodb(1,jboun),coord,tenor)
                do idime = 1,ndime  
                   norma(idime) = norma(idime) + tenor(idime)
                end  do
             end  do
             !
             ! Determine if the point are really inside or outside the particle
             !       
             pladi = 0.0_rp
             do idime = 1,ndime
                pladi = pladi + norma(idime) * ( xcoor(idime) - bocod(idime,1) )
             end do
             facto = sign(1.0_rp,pladi)             
          end if

       else
          !
          ! 2D
          !
          node1 = 0_ip
          numer = 0.0_rp
          denom = 0.0_rp
          do idime = 1,ndime
             numer = numer + (bocod(idime,2) - bocod(idime,1)) * (xcoor(idime)   - bocod(idime,1))
             denom = denom + (bocod(idime,2) - bocod(idime,1)) * (bocod(idime,2) - bocod(idime,1))
          end do
          bari1 = numer / denom
          if( bari1 < toler ) then
             node1 = lnodb(1,iboun)
          else if( bari1 > 1.0_rp - toler ) then
             node1 = lnodb(2,iboun)
          end if

          if ( node1 /= 0_ip ) then
             norma(1)     = 0.0_rp
             norma(2)     = 0.0_rp
             !
             ! Sum all the exterior normals that use the node 
             !       
             do ilist = 1,lnele(node1)%nelem
                jboun = lnele(node1)%eleme(ilist)
                call extbou(itask,ndime,pnodb,lnodb(1,jboun),coord,tenor)
                do idime = 1,ndime  
                   norma(idime) = norma(idime) + tenor(idime)
                end  do
             end  do
             !
             ! Determine if the point are really inside or outside the particle
             !       
             pladi = 0.0_rp
             do idime = 1,ndime
                pladi = pladi + norma(idime) * ( xcoor(idime) - bocod(idime,1) )
             end do
             facto = sign(1.0_rp,pladi)             
          end if

       end if
       !
       ! Signed distamce
       !           
       dista = facto * sqrt(dista)

    else
       dista = 1.0e10_rp
    end if

  end subroutine dpopar

subroutine extbou(itask,ndime,pnodb,lnodb,coord,bouno)
  !-----------------------------------------------------------------------
  !****f* extbou/extbou
  ! NAME
  !    extbou
  ! DESCRIPTION
  !    This routines computes the exterior normal to the IB 
  ! USED BY
  !    domain
  !***
  !-----------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: itask,pnodb,ndime
  integer(ip), intent(in)  :: lnodb(pnodb)
  real(rp),    intent(in)  :: coord(ndime,*)
  real(rp),    intent(out) :: bouno(ndime)
  integer(ip)              :: p1,p2,p3
  real(rp)                 :: xfact,vec(3,3),tmp(3)

  if( pnodb == 2 ) then

     if (itask == 1) then
        p1 = lnodb(1)
        p2 = lnodb(2)
     else
        p1 = lnodb(2)
        p2 = lnodb(1)
     end if

     vec(1,1) = coord(1,p2) - coord(1,p1)
     vec(2,1) = coord(2,p2) - coord(2,p1)
     bouno(1) =  vec(2,1)
     bouno(2) = -vec(1,1)

  else if( pnodb == 3 ) then

     p1 = lnodb(1)
     if (itask == 1) then
        p2 = lnodb(2)
        p3 = lnodb(3)     
     else
        p2 = lnodb(3)
        p3 = lnodb(2)     
     end if
     call nortri(p1,p2,p3,coord,vec,ndime)
     bouno(1) = 0.5_rp*vec(1,3)
     bouno(2) = 0.5_rp*vec(2,3)
     bouno(3) = 0.5_rp*vec(3,3)

  else if( pnodb == 4 ) then

     p1 = lnodb(1)
     if (itask == 1) then
        p2 = lnodb(2)
        p3 = lnodb(3)     
     else
        p2 = lnodb(3)
        p3 = lnodb(2)     
     end if

     call nortri(p1,p2,p3,coord,vec,ndime)
     bouno(1) = 0.5_rp*vec(1,3)
     bouno(2) = 0.5_rp*vec(2,3)
     bouno(3) = 0.5_rp*vec(3,3)
     if (itask == 1) then
        p2 = lnodb(3)
        p3 = lnodb(4)     
     else
        p2 = lnodb(4)
        p3 = lnodb(3)     
     end if
     call nortri(p1,p2,p3,coord,vec,ndime)
     bouno(1) = bouno(1)+0.5_rp*vec(1,3)
     bouno(2) = bouno(2)+0.5_rp*vec(2,3)
     bouno(3) = bouno(3)+0.5_rp*vec(3,3)

  else

     call runend('EXTBOU: COULD NOT COMPUTE EXTERIOR NORMAL')

  end if

  call vecuni(ndime,bouno,xfact)

end subroutine extbou
!
!
!
subroutine dpofac(&
     ndime,pnodb,xcoor,bocod,fabox,norma,dista,factor,proje)
  !-----------------------------------------------------------------------
  ! NAME
  !    pofadi
  ! DESCRIPTION
  !    Minimun distance between a point and a face
  !    point: point
  !    bocod: triangle xcoorinates
  !    norma: nornmalized exterior normal to the triangle
  !    dista: minimum distance between the point and the triangle
  !    proje: nearest triangle point projection  
  ! USED BY
  !    shdiib
  !***
  !----------------------------------------------------------------------- 
  implicit   none
  integer(ip),   intent(in)      :: pnodb
  integer(ip),   intent(in)      :: ndime
  real(rp),      intent(in)      :: xcoor(ndime),norma(ndime)
  real(rp),      intent(in)      :: bocod(ndime,*)
  real(rp),      intent(in)      :: fabox(2,ndime)
  real(rp),      intent(out)     :: dista
  real(rp),      intent(out)     :: factor
  real(rp),      intent(out)     :: proje(ndime)
  integer(ip)                    :: idime,ifoun
  real(rp)                       :: pladi,plapo(ndime)
  real(rp)                       :: dist1,dist2,dist3,dist4
  real(rp)                       :: proj1(3),proj2(3),proj3(3),proj4(3)
  real(rp)                       :: bari1,bari2
  !
  ! Distance between the point and the plane formed by the face
  ! Dot product between the exterior normal and the vector build from the first triangle vertex
  ! to the point  
  !
  pladi = 0.0_rp
  do idime = 1,ndime
     pladi = pladi + norma(idime) * ( xcoor(idime) - bocod(idime,1) )
  end do
  !
  ! Point projection on the plane
  !  

  plapo(1)     = xcoor(1)     - pladi*norma(1)
  plapo(2)     = xcoor(2)     - pladi*norma(2)
  plapo(ndime) = xcoor(ndime) - pladi*norma(ndime)



  if ( pladi < 0.0 ) then
     factor = -1.0_rp
  else
     factor =  1.0_rp
  end if
  pladi = pladi * pladi
  ifoun = 0
 
  if( ndime == 3 ) then
     !
     ! Check if we are in bounding bpx
     !        
     if( plapo(1) >= fabox(1,1) .and. plapo(1) <= fabox(2,1) ) then
        if( plapo(2) >= fabox(1,2) .and. plapo(2) <= fabox(2,2) ) then
           if( plapo(3) >= fabox(1,3) .and. plapo(3) <= fabox(2,3) ) then
              !
              ! Determine if the projection point on plane is inside the triangle
              !
              call instr2(ndime,pnodb,plapo,bocod,ifoun,bari1,bari2)
           end if
        end if
     end if
     !
     ! The projection point is inside the triangle
     !
     if( ifoun == 1 ) then                                
        dista    = pladi        
        proje(1) = plapo(1)
        proje(2) = plapo(2)
        proje(3) = plapo(3)
     else
        if( pnodb == 4 ) then
           call dposeg(ndime,xcoor,bocod(1,1),bocod(1,2),dist1,proj1,ifoun)
           call dposeg(ndime,xcoor,bocod(1,2),bocod(1,4),dist2,proj2,ifoun)
           call dposeg(ndime,xcoor,bocod(1,3),bocod(1,4),dist3,proj3,ifoun)         
           call dposeg(ndime,xcoor,bocod(1,1),bocod(1,4),dist4,proj4,ifoun)         

           if( dist1 < dist2 .and. dist1 < dist3 .and. dist1 < dist4 ) then              
              dista = dist1 
              proje(1) = proj1(1)
              proje(2) = proj1(2)
              proje(3) = proj1(3)
           else if( dist2 < dist1 .and. dist2 < dist3 .and. dist2 < dist4 ) then        
              dista = dist2
              proje(1) = proj2(1)
              proje(2) = proj2(2)
              proje(3) = proj2(3)
           else if( dist3 < dist1 .and. dist3 < dist2 .and. dist3 < dist4 ) then        
              dista = dist3 
              proje(1) = proj3(1)
              proje(2) = proj3(2)
              proje(3) = proj3(3)              
           else
              dista = dist4
              proje(1) = proj4(1)
              proje(2) = proj4(2)
              proje(3) = proj4(3)
           end if

        else
           call dposeg(ndime,xcoor,bocod(1,1),bocod(1,2),dist1,proj1,ifoun)
           call dposeg(ndime,xcoor,bocod(1,2),bocod(1,3),dist2,proj2,ifoun)
           call dposeg(ndime,xcoor,bocod(1,1),bocod(1,3),dist3,proj3,ifoun)           
           if( dist1 < dist2 .and. dist1 < dist3 ) then              
              dista = dist1
              proje(1) = proj1(1)
              proje(2) = proj1(2)
              proje(3) = proj1(3)
           else if( dist2 < dist1 .and. dist2 < dist3 ) then
              dista = dist2 
              proje(1) = proj2(1)
              proje(2) = proj2(2)
              proje(3) = proj2(3)
           else
              dista = dist3 
              proje(1) = proj3(1)
              proje(2) = proj3(2)
              proje(3) = proj3(3)
           end if
        end if
     end if
  else if( ndime == 2 ) then
     call dposeg(ndime,xcoor,bocod(1,1),bocod(1,2),dist1,proje,ifoun)           
     dista = dist1 
  end if

end subroutine dpofac
!
!
!
subroutine dposeg(ndime,xcoor,coor1,coor2,dista,proje,ifoun)
  !-----------------------------------------------------------------------
  ! NAME
  !    segdis
  ! DESCRIPTION
  !    Minimun distance between a point and a segment
  !    xcoor : point xcoorinates 
  !    coor1,coor2 : defines the segment
  !    ndime: dimension
  !    dista: distance
  !    proje: projection of the point on the segment
  !    ifoun = 1 the projection point is inside the segment
  !    ifoun = 0 the projection point is outside the segment
  ! USED BY
  !    pofadi
  !***
  !----------------------------------------------------------------------- 
  implicit none
  integer(ip),   intent(out)     :: ifoun
  integer(ip),   intent(in)      :: ndime
  real(rp),      intent(in)      :: xcoor(ndime),coor1(ndime),coor2(ndime)
  real(rp),      intent(out)     :: dista,proje(ndime)
  integer(ip)                    :: idime  
  real(rp)                       :: numer,denom,dsegm

  numer = 0.0_rp
  denom = 0.0_rp
  do idime = 1,ndime
      numer = numer + (coor2(idime) - coor1(idime)) * (xcoor(idime) - coor1(idime))
      denom = denom + (coor2(idime) - coor1(idime)) * (coor2(idime) - coor1(idime))
  end do  

  dsegm = numer / denom
  
  if( dsegm < 0.0_rp ) then     
     ifoun        = 0_ip
     proje(1)     = coor1(1)
     proje(2)     = coor1(2)
     proje(ndime) = coor1(ndime)
  else if ( dsegm >= 0.0_rp .and. dsegm <= 1.0_rp ) then
     ifoun        = 1_ip
     proje(1)     = coor1(1)     + dsegm * (coor2(1)     - coor1(1))
     proje(2)     = coor1(2)     + dsegm * (coor2(2)     - coor1(2))
     proje(ndime) = coor1(ndime) + dsegm * (coor2(ndime) - coor1(ndime))
  else
     ifoun        = 0_ip
     proje(1)     = coor2(1)
     proje(2)     = coor2(2)
     proje(ndime) = coor2(ndime)
  end if

  dista = 0.0_rp
  do idime = 1,ndime
     dista = dista + (xcoor(idime) - proje(idime))*(xcoor(idime) - proje(idime))
  end do

end subroutine dposeg
!
!
!
subroutine vecuni(n,v,mod)
!-----------------------------------------------------------------------
!
! This routine computes the length of vector V and converts it to  
! a unit one 
!
!-----------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: n
  real(rp), intent(out)   :: v(n),mod
  integer(ip)             :: i

  mod=0.0_rp
  do i=1,n
     mod=mod + v(i)*v(i)
  end do
  mod=sqrt(mod)
  if(mod>1.0e-8) v=v/mod
 
end subroutine vecuni
!
!
!
subroutine nortri(p1,p2,p3,coord,vec,ndime)
!-----------------------------------------------------------------------
!****f* Domain/nortri
! NAME
!    nortri
! DESCRIPTION
!    This routine computes the boundary normals
! USES
! USED BY
!    bounor
!***
!-----------------------------------------------------------------------
!  use      def_kintyp
  implicit none
  integer(ip), intent(in)  :: p1,p2,p3,ndime
  real(rp),    intent(in)  :: coord(ndime,*)
  real(rp),    intent(out) :: vec(3,3)

  vec(1,1) = coord(1,p2) - coord(1,p1)
  vec(2,1) = coord(2,p2) - coord(2,p1)
  vec(3,1) = coord(3,p2) - coord(3,p1)
  vec(1,2) = coord(1,p3) - coord(1,p1)
  vec(2,2) = coord(2,p3) - coord(2,p1)
  vec(3,2) = coord(3,p3) - coord(3,p1)
  call vecpro(vec(1,1),vec(1,2),vec(1,3),ndime)

end subroutine nortri
!
!
!
subroutine vecpro(v1,v2,v3,n)

!-----------------------------------------------------------------------
!
! Two and three-dimensional vectorial product of two vectors  v3 = v1 x v2.
! The same pointer as for v1 or v2 may be used for v3. If N = 2, it is
!  assumed that v1 = (0,0,v1_3) and v2 = (v2_1,v2_2,0).      
!
!-----------------------------------------------------------------------
!  use      def_kintyp
  implicit none
  integer(ip), intent(in)  :: n
  real(rp),    intent(in)  :: v2(n),v1(3)
  real(rp),    intent(out) :: v3(n)
  real(rp)                 :: c1,c2,c3

  if(n==2) then
     c1=-v1(3)*v2(2)
     c2= v1(3)*v2(1)
     v3(1)=c1
     v3(2)=c2
  else if(n==3) then
     c1=v1(2)*v2(3)-v1(3)*v2(2)
     c2=v1(3)*v2(1)-v1(1)*v2(3)
     c3=v1(1)*v2(2)-v1(2)*v2(1)
     v3(1)=c1
     v3(2)=c2
     v3(3)=c3
  end if
  
end subroutine vecpro
!
!
!
subroutine instr2(ndime,pnodb,point,facoo,ifoun,bari1,bari2)
  !-----------------------------------------------------------------------
  ! NAME
  !    insiib
  ! DESCRIPTION
  !    Determine if a point is inside a triangle using the same side technique
  !    point: point
  !    facoo(dimension,vertices): triangle coordinates
  !    ifoun: If is equal to 1, the point is inside the triangle, 0 otherwise
  ! USED BY
  !    pofadi,faliib
  !***
  !----------------------------------------------------------------------- 
  implicit   none
  integer(ip),   intent(in)     :: ndime
  integer(ip),   intent(out)    :: ifoun
  integer(ip),   intent(in)     :: pnodb
  real(rp),      intent(in)     :: point(ndime)
  real(rp),      intent(in)     :: facoo(ndime,*)
  real(rp),      intent(out)    :: bari1,bari2
  real(rp)                      :: v0(3),v1(3),v2(3)
  real(rp)                      :: dot00,dot01,dot02,dot11,dot12
  real(rp)                      :: invDenom
  !
  ! 3D
  !
  v0(1) = facoo(1,3) - facoo(1,1)
  v0(2) = facoo(2,3) - facoo(2,1)
  v0(3) = facoo(3,3) - facoo(3,1)

  v1(1) = facoo(1,2) - facoo(1,1)
  v1(2) = facoo(2,2) - facoo(2,1)
  v1(3) = facoo(3,2) - facoo(3,1)

  v2(1) = point(1)   - facoo(1,1)
  v2(2) = point(2)   - facoo(2,1)
  v2(3) = point(3)   - facoo(3,1)

  dot00 = v0(1)*v0(1) + v0(2)*v0(2) + v0(3)*v0(3)
  dot01 = v0(1)*v1(1) + v0(2)*v1(2) + v0(3)*v1(3)
  dot02 = v0(1)*v2(1) + v0(2)*v2(2) + v0(3)*v2(3)
  dot11 = v1(1)*v1(1) + v1(2)*v1(2) + v1(3)*v1(3)
  dot12 = v1(1)*v2(1) + v1(2)*v2(2) + v1(3)*v2(3)  
  !
  ! Compute barycentric coordinates
  !
  invDenom = 1.0_rp / (dot00 * dot11 - dot01 * dot01)
  bari1 = (dot11 * dot02 - dot01 * dot12) * invDenom
  bari2 = (dot00 * dot12 - dot01 * dot02) * invDenom
  !
  ! Check
  !
  if( bari1 >= 0.0_rp .and. bari2 >= 0.0_rp .and. bari1 + bari2 <= 1.0_rp ) then
     ifoun = 1
  else
     ifoun = 0
  end if

end subroutine instr2
!
!
!
subroutine heapsortri(itask,nrows,rvin,ivou)
  !------------------------------------------------------------------------
  !****f* mathru/heapsortri
  ! NAME
  !    heapsorti2
  ! DESCRIPTION
  !    Quick sorting of an integer IVOU using a real rVIN. 
  !    The element in rvin are sorting in:
  !    ITASK = 1 ... Decreasing value, i.e., rvin(1) > rvin(2) > ...
  !    ITASK = 2 ... Increasing value, i.e., rvin(1) < rvin(2) < ...
  ! INPUT
  !    ITASK ... 1,2 for decreasing, increasing order
  !    NROWS ... Size of RVIN
  !    RVIN .... Array to be ordered (input)
  !    IVOU .... Array to be ordered (output)
  ! OUTPUT
  !    RVIN .... Ordered array
  !    IVOU .... Ordered array
  ! USED BY
  !    
  !***
  !------------------------------------------------------------------------ 
  implicit none
  integer(ip), intent(in)    :: itask,nrows
  real(rp),    intent(inout) :: rvin(:)
  integer(ip), intent(inout) :: ivou(:) 
  integer(ip)                :: len, ir, ii, jj, jaux
  real(rp)                   :: raux

  select case(itask)

  case(1)
     !
     ! Decreasing order
     !
     if(nrows<2) then
        return
     end if

     len = (nrows/2) + 1
     ir  = nrows

100  continue

     if (len>1) then
        len = len - 1
        raux = rvin(len)
        jaux = ivou(len)
     else
        raux = rvin(ir)
        rvin(ir) = rvin(1)

        jaux = ivou(ir)
        ivou(ir) = ivou(1)

        ir = ir - 1

        if (ir==1) then
           rvin(1) = raux
           ivou(1) = jaux
           return
        endif
     end if

     ii = len
     jj = len + len

200  if (jj<=ir) then
        if (jj<ir) then
           if ( rvin(jj)>rvin(jj+1) ) then
              jj = jj + 1
           endif
        endif

        if (raux>rvin(jj) ) then
           rvin(ii) = rvin(jj)
           ivou(ii) = ivou(jj)

           ii = jj
           jj = jj + jj
        else
           jj = ir + 1
        endif

        goto 200
     end if

     rvin(ii) = raux
     ivou(ii) = jaux

     goto 100

  case(2)
     !
     ! Increasing order
     !
     if(nrows<2) then
        return
     end if

     len = (nrows/2) + 1
     ir  = nrows

300  continue

     if (len>1) then
        len = len - 1
        raux = rvin(len)
        jaux = ivou(len)
     else
        raux = rvin(ir)
        rvin(ir) = rvin(1)
        jaux = ivou(ir)
        ivou(ir) = ivou(1)

        ir = ir - 1

        if (ir==1) then
           rvin(1) = raux
           ivou(1) = jaux
           return
        endif
     end if

     ii = len
     jj = len + len

400  if (jj<=ir) then
        if (jj<ir) then
           if ( rvin(jj)<rvin(jj+1) ) then
              jj = jj + 1
           endif
        endif

        if (raux<rvin(jj) ) then
           rvin(ii) = rvin(jj)
           ivou(ii) = ivou(jj)

           ii = jj
           jj = jj + jj
        else
           jj = ir + 1
        endif

        goto 400
     end if

     rvin(ii) = raux
     ivou(ii) = jaux

     goto 300

  case(3)

     if(nrows<2) then
        return
     end if

     do jj=2,nrows
        raux=rvin(jj)
        jaux=ivou(jj)
        do ii=jj-1,1,-1
           if(rvin(ii)<=raux) exit
           rvin(ii+1)=rvin(ii)
           ivou(ii+1)=ivou(ii)
        end do
        rvin(ii+1)=raux
        ivou(ii+1)=jaux
     end do

  end select

end subroutine heapsortri
!
!
!
end module Kdtree
