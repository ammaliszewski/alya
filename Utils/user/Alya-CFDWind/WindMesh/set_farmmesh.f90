   subroutine set_farmmesh
  !***************************************************
  !*
  !*   Builds the 2D (surface) farm mesh
  !*
  !***************************************************
  use Master 
  use InpOut
  implicit none
  !
  integer(ip)              :: nx,ny
  integer(ip)              :: ix,iy,ipoin,ibopo,ielem
  !
  !*** Writes to the log file
  !
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Building the 2D surface farm mesh...',/)
  !
  !***  nx*ny regular mesh
  !
  farm%nelem_x1 = INT((farm%xf - farm%xi)/farm%cell_size_x) 
  farm%nelem_y1 = INT((farm%yf - farm%yi)/farm%cell_size_y) 
  nx = farm%nelem_x1 + 1
  ny = farm%nelem_y1 + 1
  !
  farm%npoin = nx * ny
  farm%nelem = (nx-1)*(ny-1)
  farm%nbopo = 2*nx + 2*ny - 4            ! interface nodes
  farm%nboun = 2*nx + 2*ny                ! interface elements
  !
  allocate(farm%lnods2d(4,farm%nelem))
  allocate(farm%lface2d(4,farm%nelem))    ! = 0 inner face; iboun otherwise
  allocate(farm%lcode2d(4,farm%nelem))    ! = 0 inner face; icode (N,W,S,E) otherwise
  allocate(farm%lpoty  (  farm%npoin))    ! = ipoin in global numeration.  < 0 for inner boundary; > 0 otherwise   
  allocate(farm%ibopo  (  farm%nbopo))    ! global numeration of the external node ibopo
  allocate(farm%x2d    (  farm%npoin))
  allocate(farm%y2d    (  farm%npoin))
  !
  !***  Farm: coodinates, lpoty, ibopo
  !
  ipoin = 0
  ibopo = 0
  do iy = 1,ny
  do ix = 1,nx
     ipoin = ipoin + 1
     farm%x2d(ipoin) = farm%xi + (ix-1) * farm%cell_size_x
     farm%y2d(ipoin) = farm%yi + (iy-1) * farm%cell_size_y
     farm%lpoty(ipoin) = ipoin
     if(iy.eq.1) then               ! External boundary                         
        ibopo = ibopo + 1
        farm%ibopo(ibopo) = ipoin
     else if(ix.eq.1) then                                   
        ibopo = ibopo + 1
        farm%ibopo(ibopo) = ipoin
     else if(ix.eq.nx) then                               
        ibopo = ibopo + 1
        farm%ibopo(ibopo) = ipoin
     else if(iy.eq.ny) then 
        ibopo = ibopo + 1                            
        farm%ibopo(ibopo) = ipoin
     end if 
  end do
  end do
  !
  !*** Farm: lface,lcode
  !
  farm%lface2d(:,:) = 0    ! always inner
  farm%lcode2d(:,:) = 0    ! always inner
  !
  !***  Farm: lnods
  !
  ielem = 0    
  do iy = 1,ny-1
  do ix = 1,nx-1
     ielem = ielem + 1
     farm%lnods2d(1,ielem) = (iy-1)*nx + ix
     farm%lnods2d(2,ielem) = (iy-1)*nx + ix + 1
     farm%lnods2d(3,ielem) = (iy  )*nx + ix + 1
     farm%lnods2d(4,ielem) = (iy  )*nx + ix
  end do
  end do  
  !
  return
  end subroutine set_farmmesh
