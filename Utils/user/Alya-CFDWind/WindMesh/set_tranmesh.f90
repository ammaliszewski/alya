   subroutine set_tranmesh
  !***************************************************
  !*
  !*   Builds the 2D (surface) transition mesh
  !*
  !***************************************************
  use Master 
  use InpOut
  implicit none
  !
  logical               :: go_on,found
  integer(ip)           :: nx,ny,nr,na,npoin_shell
  integer(ip)           :: ix,iy,ipoin,ibopo,kbopo,jbopo,ielem,i,iiter,niter,i1,i2
  integer(ip)           :: ipos,neigh,j,jpoin
  real(rp)              :: dx,dy,x,y
  real(rp)              :: lambda_smooth,mu_smooth,x_baric,y_baric
  integer(ip), allocatable :: inode(:,:) 
  real(rp)   , allocatable :: work(:)
  real(rp)   , allocatable :: distx(:),disty(:)
  !
  !*** Writes to the log file
  !
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Building the 2D surface transition mesh...',/)
  !
  !*** Transition zone (finds the number of elements iteratively given the sizes of the end-members)
  !
  !  zone x1  (Left)
  !
  tran%nelem_x1 = INT((farm%xi - tran%xi)/min(bufe%cell_size_x,farm%cell_size_x))   ! maximum value. Overdimension to allocate
  allocate(tran%cell_size_x1(tran%nelem_x1))
  allocate(work(tran%nelem_x1))
  !
  tran%nelem_x1 = 1 
  go_on = .true.
  x = farm%xi       ! max coordinate value
  do while(go_on)       
     tran%cell_size_x1(tran%nelem_x1) = farm%cell_size_x + (bufe%cell_size_x-farm%cell_size_x)*(x-farm%xi)/(tran%xi - farm%xi)  
     x = x - tran%cell_size_x1(tran%nelem_x1)
     if(x.lt.(tran%xi+bufe%cell_size_x)) then 
        go_on = .false.
        tran%cell_size_x1(tran%nelem_x1) =  tran%cell_size_x1(tran%nelem_x1) + (x-tran%xi) 
     else
        tran%nelem_x1 = tran%nelem_x1 + 1    
     end if
  end do
  do i = 1,tran%nelem_x1
     work(i) = tran%cell_size_x1(tran%nelem_x1+1-i)
  end do
  tran%cell_size_x1(1:tran%nelem_x1) = work(1:tran%nelem_x1)
  deallocate(work)
  !
  !  zone x2  (Right)
  ! 
  tran%nelem_x2 = INT((tran%xf - farm%xf)/min(bufe%cell_size_x,farm%cell_size_x))  ! maximum value. Overdimension to allocate
  allocate(tran%cell_size_x2(tran%nelem_x2))
  !
  tran%nelem_x2 = 1 
  go_on = .true.
  x = farm%xf       ! min coordinate value
  do while(go_on)       
     tran%cell_size_x2(tran%nelem_x2) = farm%cell_size_x + (bufe%cell_size_x-farm%cell_size_x)*(x-farm%xf)/(tran%xf-farm%xf)
     x = x + tran%cell_size_x2(tran%nelem_x2)
     if(x.gt.(tran%xf-bufe%cell_size_x)) then 
        go_on = .false.
        tran%cell_size_x2(tran%nelem_x2) =  tran%cell_size_x2(tran%nelem_x2) - (x-tran%xf)  
     else
        tran%nelem_x2 = tran%nelem_x2 + 1    
     end if
  end do
  !
  ! zone y1 (Bottom)
  !
  tran%nelem_y1 = INT((farm%yi - tran%yi)/min(bufe%cell_size_y,farm%cell_size_y) )   ! maximum value. Overdimension to allocate
  allocate(tran%cell_size_y1(tran%nelem_y1))
  allocate(work(tran%nelem_y1))
  !
  tran%nelem_y1 = 1 
  go_on = .true.
  y = farm%yi       ! max coordinate value
  do while(go_on)       
     tran%cell_size_y1(tran%nelem_y1) = farm%cell_size_y + (bufe%cell_size_y-farm%cell_size_y)*(y-farm%yi)/(tran%yi - farm%yi)  
     y = y - tran%cell_size_y1(tran%nelem_y1)
     if(y.lt.(tran%yi+bufe%cell_size_y)) then 
        go_on = .false.
        tran%cell_size_y1(tran%nelem_y1) =  tran%cell_size_y1(tran%nelem_y1) + (y-tran%yi) 
     else
        tran%nelem_y1 = tran%nelem_y1 + 1    
     end if
  end do
  do i = 1,tran%nelem_y1
     work(i) = tran%cell_size_y1(tran%nelem_y1+1-i)
  end do
  tran%cell_size_y1(1:tran%nelem_y1) = work(1:tran%nelem_y1)
  deallocate(work)
  !
  !  zone y2  (Top)
  ! 
  tran%nelem_y2 = INT((tran%yf - farm%yf)/min(bufe%cell_size_y,farm%cell_size_y))   ! maximum value. Overdimension to allocate
  allocate(tran%cell_size_y2(tran%nelem_y2))
  !
  tran%nelem_y2 = 1 
  go_on = .true.
  y = farm%yf       ! min coordinate value
  do while(go_on)       
     tran%cell_size_y2(tran%nelem_y2) = farm%cell_size_y + (bufe%cell_size_y-farm%cell_size_y)*(y-farm%yf)/(tran%yf-farm%yf)
     y = y + tran%cell_size_y2(tran%nelem_y2)
     if(y.gt.(tran%yf-bufe%cell_size_y)) then 
        go_on = .false.
        tran%cell_size_y2(tran%nelem_y2) =  tran%cell_size_y2(tran%nelem_y2) - (y-tran%yf)  
     else
        tran%nelem_y2 = tran%nelem_y2 + 1    
     end if
  end do
  !
  !*** Mesh the transition zone  
  !
  nx = tran%nelem_x1 + farm%nelem_x1 + tran%nelem_x2 + 1 
  ny = tran%nelem_y1 + farm%nelem_y1 + tran%nelem_y2 + 1
  !
  allocate(distx(nx))
  allocate(disty(ny))
  allocate(inode(nx,ny))
  inode(:,:) = 0
  !
  !*** Determine the 1D coordinates
  !
  ipoin = 1
  distx(1) = tran%xi
  do ix = 1,tran%nelem_x1 
     ipoin = ipoin + 1
     distx(ipoin) = distx(ipoin-1) + tran%cell_size_x1(ix)
  end do
  distx(ipoin) = farm%xi    ! Impose (float point errors exist otherwise)
  !
  do ix = 1,farm%nelem_x1 
     ipoin = ipoin + 1
     distx(ipoin) = distx(ipoin-1) + farm%cell_size_x
  end do
  distx(ipoin) = farm%xf    ! Impose (float point errors exist otherwise)
  !
  do ix = 1,tran%nelem_x2
     ipoin = ipoin + 1
     distx(ipoin) = distx(ipoin-1) + tran%cell_size_x2(ix)
  end do
  distx(ipoin) = tran%xf    ! Impose (float point errors exist otherwise)
  !
  ipoin = 1
  disty(1) = tran%yi
  do iy = 1,tran%nelem_y1 
     ipoin = ipoin + 1
     disty(ipoin) = disty(ipoin-1) + tran%cell_size_y1(iy)
  end do
  disty(ipoin) = farm%yi   ! Impose (float point errors exist otherwise)
  !
  do iy = 1,farm%nelem_y1 
     ipoin = ipoin + 1
     disty(ipoin) = disty(ipoin-1) + farm%cell_size_y
  end do
  disty(ipoin) = farm%yf   ! Impose (float point errors exist otherwise)
  !
  do iy = 1,tran%nelem_y2
     ipoin = ipoin + 1
     disty(ipoin) = disty(ipoin-1) + tran%cell_size_y2(iy)
  end do
  disty(ipoin) = tran%yf   ! Impose (float point errors exist otherwise)
  !
  !*** Determine the number of points of the transition zone and its location
  !
  ipoin = 0
  do iy = 1,ny
  do ix = 1,nx
     if( (distx(ix).gt.farm%xi).and.(distx(ix).lt.farm%xf).and. &
         (disty(iy).gt.farm%yi).and.(disty(iy).lt.farm%yf) ) then
        continue
     else
        ipoin = ipoin + 1
        inode(ix,iy) = ipoin
     end if
  end do
  end do
  !
  !*** Determine the number of elements of the transition zone
  !
  ielem = 0
  do iy = 1,ny-1
  do ix = 1,nx-1
     if( ((distx(ix  ).ge.farm%xi).and.(distx(ix  ).le.farm%xf).and.(disty(iy  ).ge.farm%yi).and.(disty(iy  ).le.farm%yf)).and. &
         ((distx(ix+1).ge.farm%xi).and.(distx(ix+1).le.farm%xf).and.(disty(iy  ).ge.farm%yi).and.(disty(iy  ).le.farm%yf)).and. &
         ((distx(ix+1).ge.farm%xi).and.(distx(ix+1).le.farm%xf).and.(disty(iy+1).ge.farm%yi).and.(disty(iy+1).le.farm%yf)).and. &
         ((distx(ix  ).ge.farm%xi).and.(distx(ix  ).le.farm%xf).and.(disty(iy+1).ge.farm%yi).and.(disty(iy+1).le.farm%yf)) ) then
        continue
     else
        ielem = ielem + 1
     end if
  end do
  end do
  !
  !*** Allocates
  !
  tran%npoin = ipoin
  tran%nelem = ielem
  tran%nbopo = 2*nx + 2*ny - 4           ! interface nodes
  allocate(tran%lnods2d(4,tran%nelem))
  allocate(tran%lface2d(4,tran%nelem))
  allocate(tran%lcode2d(4,tran%nelem))
  allocate(tran%lpoty  (  tran%npoin))
  allocate(tran%ibopo  (  tran%nbopo))
  allocate(tran%lpoin2d(9,tran%npoin))
  allocate(tran%myshell(  tran%npoin))
  allocate(tran%x2d    (  tran%npoin))
  allocate(tran%y2d    (  tran%npoin))
  !
  !***  Transition: lface2d,lcode2d
  !
  tran%lface2d(:,:) = 0   ! always inner
  tran%lcode2d(:,:) = 0   ! always inner
  !
  !*** Transition: coodinates, lpoty, ibopo
  !
  ipoin = 0
  ibopo = 0
  kbopo = 0
  do iy = 1,ny
  do ix = 1,nx
     if( (distx(ix).gt.farm%xi).and.(distx(ix).lt.farm%xf).and. &
         (disty(iy).gt.farm%yi).and.(disty(iy).lt.farm%yf) ) then
        continue
     else
        ipoin = ipoin + 1
        tran%x2d(ipoin) = distx(ix)
        tran%y2d(ipoin) = disty(iy)
        tran%lpoty(ipoin) = farm%npoin + ipoin - kbopo
        if(iy.eq.1) then                                    ! External boundary                  
           ibopo = ibopo + 1
           tran%ibopo(ibopo) =  farm%npoin + ipoin - kbopo  ! global numeration of external boundary point
        else if(ix.eq.1) then                                   
           ibopo = ibopo + 1
           tran%ibopo(ibopo) =  farm%npoin + ipoin - kbopo
        else if(ix.eq.nx) then                               
           ibopo = ibopo + 1
           tran%ibopo(ibopo) =  farm%npoin + ipoin - kbopo
        else if(iy.eq.ny) then   
           ibopo = ibopo + 1
           tran%ibopo(ibopo) =  farm%npoin + ipoin - kbopo
        end if 
        !
        if( ( iy.eq.(tran%nelem_y1+1)).and.&
             (ix.ge.(tran%nelem_x1+1)).and.&
             (ix.le.(tran%nelem_x1+farm%nelem_x1+1)) )  then   !  internal boundary (<0)    
           kbopo = kbopo + 1
           tran%lpoty(ipoin) = -farm%ibopo(kbopo)
        else if( (iy.eq.(tran%nelem_y1+farm%nelem_y1+1)).and.&
             (ix.ge.(tran%nelem_x1+1)).and.  & 
             (ix.le.(tran%nelem_x1+farm%nelem_x1+1)) ) then 
           kbopo = kbopo + 1
           tran%lpoty(ipoin) = -farm%ibopo(kbopo)
        else if( (ix.eq.(tran%nelem_x1+1)).and.&
             (iy.ge.(tran%nelem_y1+1)).and.&
             (iy.le.(tran%nelem_y1+farm%nelem_y1+1)) ) then                     
           kbopo = kbopo + 1
           tran%lpoty(ipoin) = -farm%ibopo(kbopo)
        else if( (ix.eq.(tran%nelem_x1+farm%nelem_x1+1)).and.&
             (iy.ge.(tran%nelem_y1+1)).and.  & 
             (iy.le.(tran%nelem_y1+farm%nelem_y1+1)) ) then  
           kbopo = kbopo + 1
           tran%lpoty(ipoin) = -farm%ibopo(kbopo)
        end if 
        !
     end if
  end do
  end do
  !
  !*** Transition: lnods 
  !
  ielem = 0
  do iy = 1,ny-1
  do ix = 1,nx-1
     if( ((distx(ix  ).ge.farm%xi).and.(distx(ix  ).le.farm%xf).and.(disty(iy  ).ge.farm%yi).and.(disty(iy  ).le.farm%yf)).and. &
         ((distx(ix+1).ge.farm%xi).and.(distx(ix+1).le.farm%xf).and.(disty(iy  ).ge.farm%yi).and.(disty(iy  ).le.farm%yf)).and. &
         ((distx(ix+1).ge.farm%xi).and.(distx(ix+1).le.farm%xf).and.(disty(iy+1).ge.farm%yi).and.(disty(iy+1).le.farm%yf)).and. &
         ((distx(ix  ).ge.farm%xi).and.(distx(ix  ).le.farm%xf).and.(disty(iy+1).ge.farm%yi).and.(disty(iy+1).le.farm%yf)) ) then
       continue
     else
        ielem = ielem + 1
        tran%lnods2d(1,ielem) = inode(ix  ,iy  )
        tran%lnods2d(2,ielem) = inode(ix+1,iy  )
        tran%lnods2d(3,ielem) = inode(ix+1,iy+1)
        tran%lnods2d(4,ielem) = inode(ix  ,iy+1)
     end if
  end do
  end do
  !
  !*** Computes the mapping of the no buffer zone (i.e. farm + transition). This is
  !*** needed later when orthogonality of the 3D mesh is imposed.
  !
  nx_nobuff = nx
  ny_nobuff = ny 
  allocate(nobuff_2d_local(nx_nobuff,ny_nobuff))
  !
  ipoin = 0      
  do iy = 1,ny
  do ix = 1,nx
     if( (distx(ix).gt.farm%xi).and.(distx(ix).lt.farm%xf).and. &
         (disty(iy).gt.farm%yi).and.(disty(iy).lt.farm%yf) ) then
        !                   farm zone
        nobuff_2d_local(ix,iy) = -1 
     else
        !                   transition zone
        ipoin = ipoin + 1
        nobuff_2d_local(ix,iy) = tran%lpoty(ipoin)
     end if
  end do
  end do
  ipoin = 0      ! second  fill the farm (not in the previous loop because of overlap)
  do iy = 1,ny
  do ix = 1,nx 
     if( nobuff_2d_local(ix,iy).lt.0) then
        ipoin = ipoin + 1
        nobuff_2d_local(ix,iy) = farm%lpoty(ipoin)
     end if
  end do
  end do
  !
  !*** Transition: myshell
  !
  tran%myshell(1:tran%npoin) = 2 ! outer shell is default
  npoin_shell = 4                ! do not smooth transition in this shell
  ipoin = 0
  do iy = 1,ny
  do ix = 1,nx
     if( (distx(ix).gt.farm%xi).and.(distx(ix).lt.farm%xf).and. &
         (disty(iy).gt.farm%yi).and.(disty(iy).lt.farm%yf) ) then
        continue
     else
        ipoin = ipoin + 1
        if(ix.ge.(tran%nelem_x1-npoin_shell+1).and. & 
           ix.le.(tran%nelem_x1+farm%nelem_x1+npoin_shell+1).and. &
           iy.ge.(tran%nelem_y1-npoin_shell+1).and. & 
           iy.le.(tran%nelem_y1+farm%nelem_y1+npoin_shell+1)) then
           tran%myshell(ipoin) = 1  ! inner shell
        end if
     end if
  end do
  end do
  !
  !*** Transition: lpoin2d
  !   
  do ipoin = 1,tran%npoin
     tran%lpoin2d(1:9,ipoin) = 0
     !
     do ielem = 1,tran%nelem
        found = .false.
        do i = 1,4
           if(tran%lnods2d(i,ielem).eq.ipoin) found = .true.
        end do
        if(found) then
           do i = 1,4                    ! candidates (icluding ipoint itself)
              jpoin = tran%lnods2d(i,ielem)
              if (ipoin.ne.jpoin) then
                  ipos  =  0
                  neigh =  1 
                  do j = 1,8                                          ! span over all possible neighbours
                     if( tran%lpoin2d(j,ipoin).eq.jpoin) neigh = -1   ! already  counter for
                     if((tran%lpoin2d(j,ipoin).eq.0).and. &       
                        (ipos                 .eq.0) )   ipos  = j    ! position
                  end do
                  if(neigh.eq.1) then 
                     tran%lpoin2d(ipos,ipoin) = jpoin
                     tran%lpoin2d(9   ,ipoin) = ipos                  ! number of neighbours counted so far
                  end if
              end if
           end do
        end if      ! not found
     end do         ! ielem = 1,tran%nelem
  end do            ! ipoin = 1,tran%npoin
  !
  !*** Make transition outer borders uniform...
  !
  if (1.eq.0) then
     dx = (tran%xf-tran%xi)/(nx-1)
     dy = (tran%yf-tran%yi)/(ny-1)
     !
     ipoin = 0
     do iy = 1,ny
        do ix = 1,nx
           if( (distx(ix).gt.farm%xi).and.(distx(ix).lt.farm%xf).and. &
                (disty(iy).gt.farm%yi).and.(disty(iy).lt.farm%yf) ) then
              continue
           else
              ipoin = ipoin + 1
              if(ix.eq.1.or.ix.eq.nx) tran%y2d(ipoin) = tran%yi + (iy-1)*dy
              if(iy.eq.1.or.iy.eq.ny) tran%x2d(ipoin) = tran%xi + (ix-1)*dx
           end if
        end do
     end do
     !
     do ix = 1,nx
        distx(ix) = tran%xi + (ix-1)*dx
     end do
     do iy = 1,ny
        disty(iy) = tran%yi + (iy-1)*dy
     end do
  end if
  !
  !*** Laplacian smoothing of coordinates (if necessary)
  !
  if(tran%laplacian_mesh_smoothing) then
     lambda_smooth = 0.6307_rp
     mu_smooth     = 1.0_rp/(0.1_rp - 1.0_rp/lambda_smooth)
     niter         = 0.2*(nx+ny)  ! mumber of smoothing iterations
  else 
     niter = 0
  end if
!
  do iiter = 1,niter               
!
!    Forward laplacian
!
     do ipoin = 1,tran%npoin
         if(tran%myshell(  ipoin).eq.2.and. &
            tran%lpoin2d(9,ipoin).eq.8) then    ! inner point (8 neighbours)
            !
            x_baric = 0.0_rp
            y_baric = 0.0_rp
            do i = 1,8
               x_baric = x_baric + tran%x2d(tran%lpoin2d(i,ipoin))
               y_baric = y_baric + tran%y2d(tran%lpoin2d(i,ipoin))
            end do
            x_baric = x_baric/8.0_rp 
            y_baric = y_baric/8.0_rp 
            tran%x2d(ipoin) = tran%x2d(ipoin) + lambda_smooth*(x_baric-tran%x2d(ipoin))
            tran%y2d(ipoin) = tran%y2d(ipoin) + lambda_smooth*(y_baric-tran%y2d(ipoin))
            !
          end if
     end do
!
!    Backwards laplacian
!
     do ipoin = 1,tran%npoin
         if(tran%myshell(  ipoin).eq.2.and. &
            tran%lpoin2d(9,ipoin).eq.8) then    ! inner point (8 neighbours)
           !
           x_baric = 0.0_rp
           y_baric = 0.0_rp
           do i = 1,8
              x_baric = x_baric + tran%x2d(tran%lpoin2d(i,ipoin))
              y_baric = y_baric + tran%y2d(tran%lpoin2d(i,ipoin))
           end do
            x_baric = x_baric/8.0_rp 
            y_baric = y_baric/8.0_rp 
            tran%x2d(ipoin) = tran%x2d(ipoin) + mu_smooth*(x_baric-tran%x2d(ipoin))
            tran%y2d(ipoin) = tran%y2d(ipoin) + mu_smooth*(y_baric-tran%y2d(ipoin))
            !
         end if
     end do
  end do
  !
  !*** Release memory
  ! 
  allocate(tran%x1d(nx))
  allocate(tran%y1d(ny))
  allocate(tran%inode(nx,ny))
  tran%x1d(:)     = distx(:)      ! save for later use when meshing buffer
  tran%y1d(:)     = disty(:)
  tran%inode(:,:) = inode(:,:)
  !
  deallocate(distx)
  deallocate(disty)
  deallocate(inode)
  !
  return
  end subroutine set_tranmesh
