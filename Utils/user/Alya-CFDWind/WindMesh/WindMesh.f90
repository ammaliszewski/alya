     program WindMesh
!*********************************************************************
!*
!*   PROGRAM: WindMesh
!*            Generates a structured mesh from a topography file
!*            considering 3 different zones (inner, transition and buffer)
!*   VERSION: 4.1
!*   CALL   : WindMesh.x name (task)  
!*
!*            task = check_bounds_only
!*            task = all
!*
!**********************************************************************
     use InpOut
     use Master
     implicit none
!
!*** Gets problemname and task from the call arguments
!
     call GETARG(1,name)    
     call GETARG(2,task)  
!
     if(TRIM(name).eq.'') then
       stop 'Specify the problemname'
     endif       
!
!*** Gets filename and the working path 
!
     finp = TRIM(name)//'.WindMesh.inp'
     call get_input_path(finp,path)
!
!*** Open log file
!
     flog = TRIM(path)//TRIM(name)//'.WindMesh.log' 
     call openinp 
!
!*** Reads input file
!
     call readinp
!
!*** Reads topography and roughness (map format only)
!
     SELECT CASE(topo_format)
     case('NONE')
        continue
     case('MAP')  
        call readmap
     case default
        call runend('Topography file format not found') 
     END SELECT
!
!*** Builds the 2D mesh (on a plane)
!
     call set_farmmesh
     call set_tranmesh
     call set_bufemesh
     call set_2dmesh
!
!*** Writes and outputs the GoogleEarth kmz script
!
     fout = TRIM(path)//TRIM(name)//'.domain.kml' 
     call out_google(fout,name)
!
!*** For task = check_bounds_only  stop the run
!
     if(TRIM(task).eq.'check_bounds_only') call runend('OK')
!
!*** Interpolates the topography onto the 2D mesh to build a 3D surface
!
     call settopg
!
!*** Interpolates roughness onto the 2D mesh
!
     SELECT CASE(rou_format)
     case('NONE')
        continue
     case('MAP')  
        call setroug    ! zo already read. Just interpolate contours
     case default
        call runend('Rougness file format not found') 
     END SELECT
!
!*** Checks 2D mesh interpolation quality (non rotated mesh only)
!    arnau: this is time consuming and shold be done using elsest    
!     call checktopg
!
!*** Writes the 2D mesh (for visualization purposes only)
!
     if(out_gid) then 
        fout = TRIM(path)//'Gid/'//TRIM(name)//'.WindMesh.farm-topo.msh'  
        call out_gid_2d(fout,x2d,y2d,z2d,lnods2d,my_zone_elem2d,4_ip,farm%npoin,farm%nelem)
        fout = TRIM(path)//'Gid/'//TRIM(name)//'.WindMesh.tran-topo.msh'  
        call out_gid_2d(fout,x2d,y2d,z2d,lnods2d,my_zone_elem2d,4_ip,farm%npoin+tran%npoin,farm%nelem+tran%nelem)
        !
        fout = TRIM(path)//'Gid/'//TRIM(name)//'.WindMesh.surface-topo.msh'  
        call out_gid_2d(fout,x2d,y2d,z2d,lnods2d,my_zone_elem2d,4_ip,npoin2d,nelem2d)
        fout = TRIM(path)//'Gid/'//TRIM(name)//'.WindMesh.surface-zo.msh'  
        call out_gid_2d(fout,x2d,y2d,1d3*zo2d,lnods2d,my_zone_elem2d,4_ip,npoin2d,nelem2d) ! 1d3 vertical exageration
     end if
!
!*** Builds 3D mesh
!
     call set3dmesh
!
!*** Smooths the 3D mesh
!    arnau: skip 3D elliptic by now. Coefficients should be tested
!     call elliptic_solver
!
!*** Builds and outputs 2D cuts (for visualization purposes only)
!
     call setcuts
     if(out_gid) then 
        fout = TRIM(path)//'Gid/'//TRIM(name)//'.WindMesh.cutx-Farm.msh' 
        call out_gid_2d(fout,cutx_farm%x,cutx_farm%y,cutx_farm%z,cutx_farm%lnods,cutx_farm%zone, & 
                        4_ip,cutx_farm%npoin,cutx_farm%nelem)
        fout = TRIM(path)//'Gid/'//TRIM(name)//'.WindMesh.cuty-Farm.msh' 
        call out_gid_2d(fout,cuty_farm%x,cuty_farm%y,cuty_farm%z,cuty_farm%lnods,cuty_farm%zone, &
                        4_ip,cuty_farm%npoin,cuty_farm%nelem)
     end if
!
!*** Outputs 3d mesh (for visualization purposes only)
!
     if(out_gid) then 
        fout = TRIM(path)//'Gid/'//TRIM(name)//'.WindMesh.msh' 
        call out_gid_3d(fout,x3d,y3d,z3d,lnods,my_zone_elem3d,npoin2d,nz,8_ip,nelem)
     end if
!
!*** Outputs 3d mesh and boundary conditions for Alya and OF
!
!     if(out_foam) then
!       fout = TRIM(path)//'OpenFoam/'     
!       call out_OFoam_3d(fout,x3d,y3d,z3d,zo2d,zo2d_elem,lnodf,cello,celln,npoin2d,nelem2d,&
!            nz,8_ip,nelem,npoin,nface,nneig)
!     end if
!
     if(out_alya) then
        write(lulog,1)
        if(out_screen) write(*,1)
1       format(/,'---> Output of results for Alya...')
        fout = TRIM(path)//'Alya/'//TRIM(name)
        call out_alya_3d(name,fout,x3d,y3d,z3d,zo2d,lnods,lnodb,lelmb,my_zone_boun,&
             npoin2d,nz,8_ip,nelem,4_ip,nboun)
     end if
!
!*** If necessary outputs tracking profiles and points
!
     if(witness_points) then
        write(lulog,2)
        if(out_screen) write(*,2)
  2     format(/,'---> Computing witness points...')
        !
        if(TRIM(fwit).eq.'NONE'.or.TRIM(fwit).eq.'none') then
           call readtps_list
        else
           call readtps    
        end if
        if(out_alya) call out_alya_tps
        if(out_foam) call out_foam_tps
     end if
!
!*** Normal end
!
     call runend('OK')
!
     end program WindMesh

