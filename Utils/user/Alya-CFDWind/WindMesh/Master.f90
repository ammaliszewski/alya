!***************************************************************
!*
!*              Module for master operations
!* 
!***************************************************************
 MODULE Master
   use KindType
   IMPLICIT NONE
   SAVE
!
!***  General
!
      logical               :: check   = .true.
      character(len=3)      :: version = '4.1'
      character(len=s_mess) :: task    = ' '
!     
!***  UTM coordinate system
!
      character(len=3 ) :: utmzone  = ' '
      character(len=25) :: utmdatum = 'ED_50'    ! default value
      integer(ip)       :: idatum
      real(rp)          :: utm_x_ref,utm_y_ref
!
!***  2D Mesh structures. Three zones are defined and meshed indepently: farm, transition and buffer
!
      type mesh2d
         logical     :: laplacian_mesh_smoothing = .false.
         integer(ip) :: npoin
         integer(ip) :: nelem
         integer(ip) :: nbopo
         integer(ip) :: nboun
         integer(ip) :: nelem_x1
         integer(ip) :: nelem_y1
         integer(ip) :: nelem_x2
         integer(ip) :: nelem_y2
         !
         real(rp)    :: xi
         real(rp)    :: xf
         real(rp)    :: yi
         real(rp)    :: yf
         real(rp)    :: xv(4),yv(4)
         real(rp)    :: cell_size_x
         real(rp)    :: cell_size_y
         !
         integer(ip), allocatable :: lnods2d(:,:)       ! lnods2d(4,nelem) 
         integer(ip), allocatable :: lnodb2d(:,:)       ! lnodb2d(2,nboun)
         integer(ip), allocatable :: lface2d(:,:)       ! lface2d(4,nelem) 
         integer(ip), allocatable :: lcode2d(:,:)       ! lcode2d(4,nelem) 
         integer(ip), allocatable :: lpoin2d(:,:)       ! lpoin2d(9,npoin) 
         integer(ip), allocatable :: lpoty(:)           ! lpoty(npoin)
         integer(ip), allocatable :: ibopo(:)           ! ibopo(nbopo)
         integer(ip), allocatable :: myshell(:)         ! myshell(npoin)
         integer(ip), allocatable :: inode(:,:)
         real(rp)   , allocatable :: x2d(:)
         real(rp)   , allocatable :: y2d(:)
         real(rp)   , allocatable :: x1d(:)
         real(rp)   , allocatable :: y1d(:)
         real(rp)   , allocatable :: cell_size_x1(:)    ! cell_size_x1(nelem_x1)
         real(rp)   , allocatable :: cell_size_x2(:)    ! cell_size_x2(nelem_x2)
         real(rp)   , allocatable :: cell_size_y1(:)    ! cell_size_y1(nelem_y1)
         real(rp)   , allocatable :: cell_size_y2(:)    ! cell_size_21(nelem_y2)
      end type mesh2d
      type(mesh2d) farm,tran,bufe,surf
!  
!***  Variables related to the global mesh 3D (1D variables only in z direction)
!
      character(len=25) :: dz_distribution
      integer(ip)       :: ncellz,nz,ncellbl,npoinbl
      integer(ip)       :: npoin2d,nelem2d,nboun2d
      integer(ip)       :: npoin  ,nelem  ,nboun
      real(rp)          :: ztop,dz0   
!
      integer(ip),allocatable :: my_zone(:)        ! my_zone     (npoin2d)
      integer(ip),allocatable :: my_zone_elem2d(:) ! my_zone_elem(nelem2d)
      integer(ip),allocatable :: my_zone_elem3d(:) ! my_zone_elem(nelem  )
      integer(ip),allocatable :: my_zone_boun(:)   ! my_zone_boun(nboun)
!
      real(rp), allocatable :: x2d(:)       ! x2d (npoin2d)
      real(rp), allocatable :: y2d(:)       ! y2d (npoin2d)
      real(rp), allocatable :: z2d(:)       ! z2d (npoin2d)
      
      real(rp), allocatable :: x3d(:,:)     ! x3d (npoin2d,nz)
      real(rp), allocatable :: y3d(:,:)     ! y3d (npoin2d,nz)
      real(rp), allocatable :: z3d(:,:)     ! z3d (npoin2d,nz)
!
      real(rp), allocatable :: zo2d(:)      ! zo2d     (npoin2d)
      real(rp), allocatable :: zo2d_elem(:) ! zo2d_elem(nelem2d)
!
      integer(ip), allocatable :: lnods2d(:,:)  ! lnods2d(4,nelem2d)
      integer(ip), allocatable :: lnodb2d(:,:)  ! lnodb2d(2,nboun2d)
      integer(ip), allocatable :: lpoin2d(:,:)  ! lpoin2d(9,npoin2d)
      integer(ip), allocatable :: lface2d(:,:)  ! lface2d(4,nelem2d) 
      integer(ip), allocatable :: lcode2d(:,:)  ! lcode2d(4,nelem2d) 

      integer(ip), allocatable :: lnods  (:,:)  ! lnods  (8,nelem  )
      integer(ip), allocatable :: lnodb  (:,:)  ! lnodb  (4,nboun  )
      integer(ip), allocatable :: lelmb  (:)    ! lelmb  (  nboun  )
!
!***  Topography and roughness
!
      character(len=20) :: roughness_contours,roughness_criterion
!
      integer(ip) :: n_top     = 0
      integer(ip) :: n_zo      = 0
      integer(ip) :: ncurva_zo = 0
      real   (rp) :: zo_def             ! default contour value
!
      real(rp), allocatable :: x_top(:),y_top(:),z_top(:)
      real(rp), allocatable :: x_zo (:),y_zo (:)
!
      logical     :: smooth_buffer
      logical     :: rotate_mesh
      logical     :: smooth_topography
      integer(ip) :: niter_smooth
      real(rp)    :: elevation_buffer
      real(rp)    :: rotation_angle
!
!***  Variables for rougness (zo)
!
      type curva
        logical       :: clockwise                ! Sense of rotation
        integer(ip)   :: ndime                    ! Dimension
        integer(ip)   :: nnode                    ! surface elements
        integer(ip)   :: mnodb                    ! Number of boundary nodes
        integer(ip)   :: npoib                    ! Number of points in a contour
        integer(ip)   :: nboun                    ! Number of boundaries
        real   (rp)   :: value_right
        real   (rp)   :: value_left
        integer(ip), allocatable :: lnodb(:,:)    ! connectivities lnodb(mnodb,nboun)
        real   (rp), allocatable :: coord(:,:)    ! Coordinates of the points coord(ndime,npoib)
!
        integer(ip), pointer :: blink(:)
        integer(ip), pointer :: stru2(:)
        real(rp),    pointer :: ldist(:)
        real(rp),    pointer :: fabox(:,:,:)
        real(rp),    pointer :: sabox(:,:,:)
      end type curva
!
      type(curva), allocatable :: curva_zo(:) 
!
!***  Elliptic smoothing
!
      integer(ip) :: nx_nobuff,ny_nobuff,nz_nobuff
      integer(ip), allocatable :: nobuff_2d_local(:,:)    !  nobuff_2d_local(nx_nobuff,ny_nobuff)
!
!*** Mesh cuts (structures)
!
    type mesh_cut
      integer(ip) :: npoin2d
      integer(ip) :: npoin
      integer(ip) :: nelem
      integer(ip), allocatable :: ipoin(:)
      integer(ip), allocatable :: lnods(:,:)
      integer(ip), allocatable :: zone (:)
      real   (rp), allocatable :: x(:)
      real   (rp), allocatable :: y(:)
      real   (rp), allocatable :: z(:)
    end type mesh_cut
    type(mesh_cut) :: cutx_farm,cuty_farm,cutx_tran1,cuty_tran1
!
!
!*** Tracking points (structures)
!
     type profile
       logical    :: exists
       character(s_name)       :: type,name,attr
       integer(ip)             :: nwit
       real   (rp) :: xtps
       real   (rp) :: ytps
       real   (rp) :: ztps
       real   (rp), allocatable :: coord(:,:) ! coord(3,nwit)
     end type profile
!
     type(profile), allocatable :: tps(:)    ! tps(npts)
     logical     :: witness_points = .false. 
     integer(ip) :: ntps,nwit
!
!***  Delauney mesh for topography (z)
!
      integer(ip) :: node_num_z                ! number of nodes
      integer(ip) :: triangle_num_z            ! number of triangles
      integer(ip) :: nbbox_z = 3               ! number of bounding boxes
!
      integer(ip), allocatable :: triangle_node_z(:,:)       ! nodal conectivities      triangle_node_z(3,triangle_num_z)
      real   (rp), allocatable :: node_xy_z(:,:)             ! coordinates              node_xy_z(2,node_num_z)
!
      integer(ip), allocatable :: bbox_elem_z(:,:,:)         ! bbox_elem_z(triangle_num_z,nbbox_z,nbbox_z))
      real   (rp), allocatable :: bbox_min_z (:,:,:)         ! bbox_min_z (2,nbbox_z,nbbox_z))
      real   (rp), allocatable :: bbox_max_z (:,:,:)         ! bbox_max_ z(2,nbbox_z,nbbox_z))
!  
!
!
END MODULE Master
