!******************************************************************************************
!*
!*  Module for Delauney triangulation based on 
!*  GEOMPACK - a software package for the generation of meshes using geometric algorithms, 
!*  Advances in Engineering Software,
!*  Volume 13, pages 325-331, 1991.
!* 
!******************************************************************************************
 MODULE Delauney
   use KindType
   use InpOut
   IMPLICIT NONE
   SAVE
!
CONTAINS
!
!
!
     subroutine set_delauney(node_xy,triangle_node,node_num,triangle_num)
!***************************************************************
!*
!*   Builds a Delauney trinagulation from a set of discrete
!*   points. 
!*
!*   INPUTS:
!*      node_num
!*      node_xy(2,node_num)
!*
!*      
!*   OUTPUTS:
!*      triangle_num
!*      triangle_node(3,2*node_num)
!* 
!***************************************************************
     implicit none
!
     integer(ip) :: node_num,triangle_num
     integer(ip) :: triangle_node(3,2*node_num)
     real   (rp) :: node_xy(2,node_num)
!
     integer(ip), allocatable :: triangle_neighbor(:,:)   ! triangle neighbor list   triangle_neighbor(3,triangle_num)
!
!*** Initialization
!
     allocate(triangle_neighbor(3,2*node_num))   ! overdimension to 2*node_num instead of triangle_num ( = 2*NODE_NUM - NB - 2)
!
!*** Calls the driver
!
     call dtris2 ( node_num, node_xy, triangle_num, triangle_node, triangle_neighbor )
!
!*** Release memory
!
     deallocate(triangle_neighbor)
!
     return
     end subroutine set_delauney
!
!
!
    subroutine set_delauney_bboxes(coord,lnods,bbox_elem,bbox_min,bbox_max,npoin,nelem,nbbox)
!***************************************************************
!* 
!*  Build the bounding boxes. This is done in order to decrease
!*  the search and interpolation times later.
!*
!*  OUTPUT
!*    bbox_elem(nelem,nbbox,nbbox)  where   bbox_elem(ielem,nbbox,nbbox) = 0  ielem does not belong  to (ibbox,jbbox) 
!*                                          bbox_elem(ielem,nbbox,nbbox) = 1  ielem          belongs to (ibbox,jbbox) 
!*    bbox_min(2,nbbox,nbbox)  xmin and ymin coordinates of (ibbox,jbbox) 
!*    bbox_max(2,nbbox,nbbox)  xmax and ymax coordinates of (ibbox,jbbox) 
!*
!***************************************************************
     implicit none
!
     integer(ip) :: npoin,nelem,nbbox
     integer(ip) :: lnods(3,nelem),bbox_elem(nelem,nbbox,nbbox)
     real   (rp) :: coord(2,npoin),bbox_min(2,nbbox,nbbox),bbox_max(2,nbbox,nbbox)
!
     integer(ip) :: ibbox,jbbox,ielem
     real   (rp) :: xmin,xmax,ymin,ymax,dx,dy
!
!*** Determinates the coordinates of the nbbox*nbbox bounding boxes
!    
     xmin = minval(coord(1,1:npoin))
     xmax = maxval(coord(1,1:npoin))
     ymin = minval(coord(2,1:npoin))
     ymax = maxval(coord(2,1:npoin))
     dx = (xmax - xmin)/nbbox
     dy = (ymax - ymin)/nbbox
!
     do ibbox = 1,nbbox
        bbox_min(1,ibbox,1:nbbox) = xmin + (ibbox-1)*dx
        bbox_max(1,ibbox,1:nbbox) = xmin + (ibbox  )*dx
     end do
     do jbbox = 1,nbbox
        bbox_min(2,1:nbbox,jbbox) = ymin + (jbbox-1)*dy
        bbox_max(2,1:nbbox,jbbox) = ymin + (jbbox  )*dy
     end do
!
!*** Fill the bounding boxes
!
     do ielem = 1,nelem
!
!***    Element bounding box
!
        xmin = min(coord(1,lnods(1,ielem)),coord(1,lnods(2,ielem)),coord(1,lnods(3,ielem)))
        xmax = max(coord(1,lnods(1,ielem)),coord(1,lnods(2,ielem)),coord(1,lnods(3,ielem)))
        ymin = min(coord(2,lnods(1,ielem)),coord(2,lnods(2,ielem)),coord(2,lnods(3,ielem)))
        ymax = max(coord(2,lnods(1,ielem)),coord(2,lnods(2,ielem)),coord(2,lnods(3,ielem)))
!
!***    Finds if ielem is in the bounding box. It assumes that, if an element delongs to 
!***    more than one bbox, these are contiguous.
!
        do jbbox = 1,nbbox
        do ibbox = 1,nbbox
           if( (xmin.ge.bbox_min(1,ibbox,jbbox)).and.(xmin.le.bbox_max(1,ibbox,jbbox)).and. &
               (ymin.ge.bbox_min(2,ibbox,jbbox)).and.(ymin.le.bbox_max(2,ibbox,jbbox)) ) then
               bbox_elem(ielem,ibbox,jbbox) = 1
           else if( (xmax.ge.bbox_min(1,ibbox,jbbox)).and.(xmax.le.bbox_max(1,ibbox,jbbox)).and. &
                    (ymin.ge.bbox_min(2,ibbox,jbbox)).and.(ymin.le.bbox_max(2,ibbox,jbbox)) ) then
               bbox_elem(ielem,ibbox,jbbox) = 1            
           else if( (xmax.ge.bbox_min(1,ibbox,jbbox)).and.(xmax.le.bbox_max(1,ibbox,jbbox)).and. &
                    (ymax.ge.bbox_min(2,ibbox,jbbox)).and.(ymax.le.bbox_max(2,ibbox,jbbox)) ) then
               bbox_elem(ielem,ibbox,jbbox) = 1        
           else if( (xmin.ge.bbox_min(1,ibbox,jbbox)).and.(xmin.le.bbox_max(1,ibbox,jbbox)).and. &
                    (ymax.ge.bbox_min(2,ibbox,jbbox)).and.(ymax.le.bbox_max(2,ibbox,jbbox)) ) then
               bbox_elem(ielem,ibbox,jbbox) = 1            
           else
               bbox_elem(ielem,ibbox,jbbox) = 0
           end if
        end do
        end do
!
     end do
! 
    return
    end subroutine set_delauney_bboxes
!
!
!
subroutine dtris2 ( node_num, node_xy, triangle_num, triangle_node, &
  triangle_neighbor )

!*****************************************************************************80
!
!! DTRIS2 constructs a Delaunay triangulation of 2D vertices.
!
!  Discussion:
!
!    The routine constructs the Delaunay triangulation of a set of 2D vertices
!    using an incremental approach and diagonal edge swaps.  Vertices are
!    first sorted in lexicographically increasing (X,Y) order, and
!    then are inserted one at a time from outside the convex hull.
!
!  Modified:
!
!    25 August 2001
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) NODE_NUM, the number of vertices.
!
!    Input/output, real ( kind = 8 ) NODE_XY(2,NODE_NUM), the coordinates
!    of the vertices.  On output, the vertices have been sorted into
!    dictionary order.
!
!    Output, integer ( kind = 4 ) TRIANGLE_NUM, the number of triangles in
!    the triangulation;  TRIANGLE_NUM is equal to 2*NODE_NUM - NB - 2, where
!    NB is the number of boundary vertices.
!
!    Output, integer ( kind = 4 ) TRIANGLE_NODE(3,TRIANGLE_NUM), the nodes
!    that make up each triangle.  The elements are indices of P.  The vertices
!    of the triangles are in counter clockwise order.
!
!    Output, integer ( kind = 4 ) TRIANGLE_NEIGHBOR(3,TRIANGLE_NUM),
!    the triangle neighbor list.  Positive elements are indices of TIL;
!    negative elements are used for links of a counter clockwise linked list
!    of boundary edges; LINK = -(3*I + J-1) where I, J = triangle, edge index;
!    TRIANGLE_NEIGHBOR(J,I) refers to the neighbor along edge from vertex J
!    to J+1 (mod 3).
!
  implicit none

  integer ( ip ) node_num

  real    ( rp ) cmax
  integer ( ip ) e
  integer ( ip ) i
  integer ( ip ) ierr
  integer ( ip ) indx(node_num)
  integer ( ip ) j
  integer ( ip ) k
  integer ( ip ) l
  integer ( ip ) ledg
  integer ( ip ) lr
  integer ( ip ) lrline
  integer ( ip ) ltri
  integer ( ip ) m
  integer ( ip ) m1
  integer ( ip ) m2
  integer ( ip ) n
  real    ( rp ) node_xy(2,node_num)
  integer ( ip ) redg
  integer ( ip ) rtri
  integer ( ip ) stack(node_num)
  integer ( ip ) t
  real    ( rp ) tol
  integer ( ip ) top
  integer ( ip ) triangle_neighbor(3,node_num*2)
  integer ( ip ) triangle_num
  integer ( ip ) triangle_node(3,node_num*2)

  tol = 100.0D+00 * epsilon ( tol )
  ierr = 0
!
!  Sort the vertices by increasing (x,y).
!
  call r82vec_sort_heap_index_a ( node_num, node_xy, indx )

  call r82vec_permute ( node_num, node_xy, indx )
!
!  Make sure that the data points are "reasonably" distinct.
!
  m1 = 1

  do i = 2, node_num

    m = m1
    m1 = i

    k = 0

    do j = 1, 2

      cmax = max ( abs ( node_xy(j,m) ), abs ( node_xy(j,m1) ) )

      if ( tol * ( cmax + 1.0D+00 ) &
           < abs ( node_xy(j,m) - node_xy(j,m1) ) ) then
        k = j
        exit
      end if

    end do

    if ( k == 0 ) then
      write ( lulog, '(a)' ) ' '
      write ( lulog, '(a)' ) 'DTRIS2 - Fatal error!'
      write ( lulog, '(a,i8)' ) '  Fails for dulicated point number I = ', i
      write ( lulog, '(a,i8)' ) '  M = ', m
      write ( lulog, '(a,i8)' ) '  M1 = ', m1
      write ( lulog, '(a,2g14.6)' ) '  X,Y(M)  = ', node_xy(1:2,m)
      write ( lulog, '(a,2g14.6)' ) '  X,Y(M1) = ', node_xy(1:2,m1)
      call runend('DTRIS2 - Fatal error, duplicated point!')
      ierr = 224
      return
    end if

  end do
!
!  Starting from points M1 and M2, search for a third point M that
!  makes a "healthy" triangle (M1,M2,M)
!
  m1 = 1
  m2 = 2
  j = 3

  do
 
    if ( node_num < j ) then
      write ( lulog, '(a)' ) ' '
      write ( lulog, '(a)' ) 'DTRIS2 - Fatal error!'
      ierr = 225
      return
    end if

    m = j

    lr = lrline ( node_xy(1,m), node_xy(2,m), node_xy(1,m1), &
      node_xy(2,m1), node_xy(1,m2), node_xy(2,m2), 0.0D+00 )

    if ( lr /= 0 ) then
      exit
    end if

    j = j + 1

  end do
!
!  Set up the triangle information for (M1,M2,M), and for any other
!  triangles you created because points were collinear with M1, M2.
!
  triangle_num = j - 2

  if ( lr == -1 ) then

    triangle_node(1,1) = m1
    triangle_node(2,1) = m2
    triangle_node(3,1) = m
    triangle_neighbor(3,1) = -3

    do i = 2, triangle_num

      m1 = m2
      m2 = i+1
      triangle_node(1,i) = m1
      triangle_node(2,i) = m2
      triangle_node(3,i) = m
      triangle_neighbor(1,i-1) = -3 * i
      triangle_neighbor(2,i-1) = i
      triangle_neighbor(3,i) = i - 1

    end do

    triangle_neighbor(1,triangle_num) = -3 * triangle_num - 1
    triangle_neighbor(2,triangle_num) = -5
    ledg = 2
    ltri = triangle_num

  else

    triangle_node(1,1) = m2
    triangle_node(2,1) = m1
    triangle_node(3,1) = m
    triangle_neighbor(1,1) = -4

    do i = 2, triangle_num
      m1 = m2
      m2 = i+1
      triangle_node(1,i) = m2
      triangle_node(2,i) = m1
      triangle_node(3,i) = m
      triangle_neighbor(3,i-1) = i
      triangle_neighbor(1,i) = -3 * i - 3
      triangle_neighbor(2,i) = i - 1
    end do

    triangle_neighbor(3,triangle_num) = -3 * triangle_num
    triangle_neighbor(2,1) = -3 * triangle_num - 2
    ledg = 2
    ltri = 1

  end if
!
!  Insert the vertices one at a time from outside the convex hull,
!  determine visible boundary edges, and apply diagonal edge swaps until
!  Delaunay triangulation of vertices (so far) is obtained.
!
  top = 0

  do i = j+1, node_num

    m = i
    m1 = triangle_node(ledg,ltri)

    if ( ledg <= 2 ) then
      m2 = triangle_node(ledg+1,ltri)
    else
      m2 = triangle_node(1,ltri)
    end if

    lr = lrline ( node_xy(1,m), node_xy(2,m), node_xy(1,m1), &
      node_xy(2,m1), node_xy(1,m2), node_xy(2,m2), 0.0D+00 )

    if ( 0 < lr ) then
      rtri = ltri
      redg = ledg
      ltri = 0
    else
      l = -triangle_neighbor(ledg,ltri)
      rtri = l / 3
      redg = mod(l,3) + 1
    end if

    call vbedg ( node_xy(1,m), node_xy(2,m), node_num, node_xy, triangle_num, &
      triangle_node, triangle_neighbor, ltri, ledg, rtri, redg )

    n = triangle_num + 1
    l = -triangle_neighbor(ledg,ltri)

    do

      t = l / 3
      e = mod ( l, 3 ) + 1
      l = -triangle_neighbor(e,t)
      m2 = triangle_node(e,t)

      if ( e <= 2 ) then
        m1 = triangle_node(e+1,t)
      else
        m1 = triangle_node(1,t)
      end if

      triangle_num = triangle_num + 1
      triangle_neighbor(e,t) = triangle_num
      triangle_node(1,triangle_num) = m1
      triangle_node(2,triangle_num) = m2
      triangle_node(3,triangle_num) = m
      triangle_neighbor(1,triangle_num) = t
      triangle_neighbor(2,triangle_num) = triangle_num - 1
      triangle_neighbor(3,triangle_num) = triangle_num + 1
      top = top + 1

      if ( node_num < top ) then
        ierr = 8
        write ( lulog, '(a)' ) ' '
        write ( lulog, '(a)' ) 'DTRIS2 - Fatal error!'
        write ( lulog, '(a)' ) '  Stack overflow.'
        return
      end if

      stack(top) = triangle_num

      if ( t == rtri .and. e == redg ) then
        exit
      end if

    end do

    triangle_neighbor(ledg,ltri) = -3 * n - 1
    triangle_neighbor(2,n) = -3 * triangle_num - 2
    triangle_neighbor(3,triangle_num) = -l
    ltri = n
    ledg = 2

    call swapec ( m, top, ltri, ledg, node_num, node_xy, triangle_num, &
      triangle_node, triangle_neighbor, stack, ierr )

    if ( ierr /= 0 ) then
      write ( lulog, '(a)' ) ' '
      write ( lulog, '(a)' ) 'DTRIS2 - Fatal error!'
      write ( lulog, '(a)' ) '  Error return from SWAPEC.'
      call runend('DTRIS2 - Fatal error!')
    end if

  end do
!
!  Now account for the sorting that we did.
!
  do i = 1, 3
    do j = 1, triangle_num
      triangle_node(i,j) = indx ( triangle_node(i,j) )
    end do
  end do

  call perm_inv ( node_num, indx )

  call r82vec_permute ( node_num, node_xy, indx )

  return
end subroutine
!
!
!
subroutine r82vec_permute ( n, a, p )

!*****************************************************************************80
!
!! R82VEC_PERMUTE permutes an R82VEC in place.
!
!  Discussion:
!
!    This routine permutes an array of real "objects", but the same
!    logic can be used to permute an array of objects of any arithmetic
!    type, or an array of objects of any complexity.  The only temporary
!    storage required is enough to store a single object.  The number
!    of data movements made is N + the number of cycles of order 2 or more,
!    which is never more than N + N/2.
!
!  Example:
!
!    Input:
!
!      N = 5
!      P = (   2,    4,    5,    1,    3 )
!      A = ( 1.0,  2.0,  3.0,  4.0,  5.0 )
!          (11.0, 22.0, 33.0, 44.0, 55.0 )
!
!    Output:
!
!      A    = (  2.0,  4.0,  5.0,  1.0,  3.0 )
!             ( 22.0, 44.0, 55.0, 11.0, 33.0 ).
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    08 December 2004
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, integer ( ip ) N, the number of objects.
!
!    Input/output, real ( rp ) A(2,N), the array to be permuted.
!
!    Input, integer ( ip ) P(N), the permutation.  P(I) = J means
!    that the I-th element of the output array should be the J-th
!    element of the input array.  P must be a legal permutation
!    of the integer ( ip )s from 1 to N, otherwise the algorithm will
!    fail catastrophically.
!
  implicit none

  integer ( ip ) n

  real    ( rp ) a(2,n)
  real    ( rp ) a_temp(2)
  integer ( ip ) ierror
  integer ( ip ) iget
  integer ( ip ) iput
  integer ( ip ) istart
  integer ( ip ) p(n)

  call perm_check ( n, p, ierror )

  if ( ierror /= 0 ) then
    write ( lulog, '(a)' ) ' '
    write ( lulog, '(a)' ) 'R82VEC_PERMUTE - Fatal error!'
    write ( lulog, '(a)' ) '  The input array does not represent'
    write ( lulog, '(a)' ) '  a proper permutation.  In particular, the'
    write ( lulog, '(a,i8)' ) '  array is missing the value ', ierror
    call runend('R82VEC_PERMUTE - Fatal error!')
  end if
!
!  Search for the next element of the permutation that has not been used.
!
  do istart = 1, n

    if ( p(istart) < 0 ) then

      cycle

    else if ( p(istart) == istart ) then

      p(istart) = -p(istart)
      cycle

    else

      a_temp(1:2) = a(1:2,istart)
      iget = istart
!
!  Copy the new value into the vacated entry.
!
      do

        iput = iget
        iget = p(iget)

        p(iput) = -p(iput)

        if ( iget < 1 .or. n < iget ) then
          write ( lulog, '(a)' ) ' '
          write ( lulog, '(a)' ) 'R82VEC_PERMUTE - Fatal error!'
          call runend('R82VEC_PERMUTE - Fatal error!')
        end if

        if ( iget == istart ) then
          a(1:2,iput) = a_temp(1:2)
          exit
        end if

        a(1:2,iput) = a(1:2,iget)

      end do

    end if

  end do
!
!  Restore the signs of the entries.
!
  p(1:n) = -p(1:n)

  return
end subroutine
!
!
!
subroutine r82vec_sort_heap_index_a ( n, a, indx )

!*****************************************************************************80
!
!! R82VEC_SORT_HEAP_INDEX_A does an indexed heap ascending sort of an R82VEC.
!
!  Discussion:
!
!    The sorting is not actually carried out.  Rather an index array is
!    created which defines the sorting.  This array may be used to sort
!    or index the array, or to sort or index related arrays keyed on the
!    original array.
!
!    Once the index array is computed, the sorting can be carried out
!    "implicitly:
!
!      A(1:2,INDX(I)), I = 1 to N is sorted,
!
!    or explicitly, by the call
!
!      call R82VEC_PERMUTE ( N, A, INDX )
!
!    after which A(1:2,I), I = 1 to N is sorted.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    08 December 2004
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, integer ( ip ) N, the number of entries in the array.
!
!    Input, real ( rp ) A(2,N), an array to be index-sorted.
!
!    Output, integer ( ip ) INDX(N), the sort index.  The
!    I-th element of the sorted array is A(1:2,INDX(I)).
!
  implicit none

  integer ( ip ) n

  real    ( rp ) a(2,n)
  real    ( rp ) aval(2)
  integer ( ip ) i
  integer ( ip ) indx(n)
  integer ( ip ) indxt
  integer ( ip ) ir
  integer ( ip ) j
  integer ( ip ) l

  if ( n < 1 ) then
    return
  end if

  do i = 1, n
    indx(i) = i
  end do

  if ( n == 1 ) then
    return
  end if

  l = n / 2 + 1
  ir = n

  do

    if ( 1 < l ) then

      l = l - 1
      indxt = indx(l)
      aval(1:2) = a(1:2,indxt)

    else

      indxt = indx(ir)
      aval(1:2) = a(1:2,indxt)
      indx(ir) = indx(1)
      ir = ir - 1

      if ( ir == 1 ) then
        indx(1) = indxt
        exit
      end if

    end if

    i = l
    j = l + l

    do while ( j <= ir )

      if ( j < ir ) then
        if (   a(1,indx(j)) <  a(1,indx(j+1)) .or. &
             ( a(1,indx(j)) == a(1,indx(j+1)) .and. &
               a(2,indx(j)) <  a(2,indx(j+1)) ) ) then
          j = j + 1
        end if
      end if

      if (   aval(1) <  a(1,indx(j)) .or. &
           ( aval(1) == a(1,indx(j)) .and. &
             aval(2) <  a(2,indx(j)) ) ) then
        indx(i) = indx(j)
        i = j
        j = j + j
      else
        j = ir + 1
      end if

    end do

    indx(i) = indxt

  end do

  return
end subroutine
!
!
!
subroutine perm_check ( n, p, ierror )

!*****************************************************************************80
!
!! PERM_CHECK checks that a vector represents a permutation.
!
!  Discussion:
!
!    The routine verifies that each of the values from 1
!    to N occurs among the N entries of the permutation.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    01 February 2001
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, integer ( ip ) N, the number of entries.
!
!    Input, integer ( ip ) P(N), the array to check.
!
!    Output, integer ( ip ) IERROR, error flag.
!    0, the array represents a permutation.
!    nonzero, the array does not represent a permutation.  The smallest
!    missing value is equal to IERROR.
!
  implicit none

  integer ( ip ) n

  integer ( ip ) ierror
  integer ( ip ) ifind
  integer ( ip ) iseek
  integer ( ip ) p(n)

  ierror = 0

  do iseek = 1, n

    ierror = iseek

    do ifind = 1, n
      if ( p(ifind) == iseek ) then
        ierror = 0
        exit
      end if
    end do

    if ( ierror /= 0 ) then
      return
    end if

  end do

  return
end subroutine
!
!
!
subroutine perm_inv ( n, p )

!*****************************************************************************80
!
!! PERM_INV inverts a permutation "in place".
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    25 July 2000
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, integer ( ip ) N, the number of objects being permuted.
!
!    Input/output, integer ( ip ) P(N), the permutation, in standard
!    index form.  On output, P describes the inverse permutation
!
  implicit none

  integer ( ip ) n

  integer ( ip ) i
  integer ( ip ) i0
  integer ( ip ) i1
  integer ( ip ) i2
  integer ( ip ) ierror
  integer ( ip ) is
  integer ( ip ) p(n)

  if ( n <= 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'PERM_INV - Fatal error!'
    write ( *, '(a,i8)' ) '  Input value of N = ', n
    call runend('PERM_INV - Fatal error!')
  end if

  call perm_check ( n, p, ierror )

  if ( ierror /= 0 ) then
    write ( lulog, '(a)' ) ' '
    write ( lulog, '(a)' ) 'PERM_INV - Fatal error!'
    write ( lulog, '(a)' ) '  The input array does not represent'
    write ( lulog, '(a)' ) '  a proper permutation.  In particular, the'
    write ( lulog, '(a,i8)' ) '  array is missing the value ', ierror
    call runend('PERM_INV - Fatal error!')
  end if

  is = 1

  do i = 1, n

    i1 = p(i)

    do while ( i < i1 )
      i2 = p(i1)
      p(i1) = -i2
      i1 = i2
    end do

    is = -sign ( 1, p(i) )
    p(i) = sign ( p(i), is )

  end do

  do i = 1, n

    i1 = -p(i)

    if ( 0 <= i1 ) then

      i0 = i

      do

        i2 = p(i1)
        p(i1) = i0

        if ( i2 < 0 ) then
          exit
        end if

        i0 = i1
        i1 = i2

      end do

    end if

  end do

  return
end subroutine
!
!
!
subroutine vbedg ( x, y, node_num, node_xy, triangle_num, triangle_node, &
  triangle_neighbor, ltri, ledg, rtri, redg )

!*****************************************************************************80
!
!! VBEDG determines which boundary edges are visible to a point.
!
!  Discussion:
!
!    The point (X,Y) is assumed to be outside the convex hull of the
!    region covered by the 2D triangulation.
!
!  Modified:
!
!    25 August 2001
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, real ( rp ) X, Y, the coordinates of a point outside the
!    convex hull of the current triangulation.
!
!    Input, integer ( ip ) NODE_NUM, the number of points.
!
!    Input, real ( rp ) NODE_XY(2,NODE_NUM), the coordinates of the
!    vertices.
!
!    Input, integer ( ip ) TRIANGLE_NUM, the number of triangles.
!
!    Input, integer ( ip ) TRIANGLE_NODE(3,TRIANGLE_NUM), the
!    triangle incidence list.
!
!    Input, integer ( ip ) TRIANGLE_NEIGHBOR(3,TRIANGLE_NUM), the
!    triangle neighbor list; negative values are used for links of a
!    counterclockwise linked list of boundary edges;
!      LINK = -(3*I + J-1) where I, J = triangle, edge index.
!
!    Input/output, integer ( ip ) LTRI, LEDG.  If LTRI /= 0 then these
!    values are assumed to be already computed and are not changed, else they
!    are updated.  On output, LTRI is the index of boundary triangle to the
!    left of the leftmost boundary triangle visible from (X,Y), and LEDG is
!    the boundary edge of triangle LTRI to the left of the leftmost boundary
!    edge visible from (X,Y).  1 <= LEDG <= 3.
!
!    Input/output, integer ( ip ) RTRI.  On input, the index of the
!    boundary triangle to begin the search at.  On output, the index of the
!    rightmost boundary triangle visible from (X,Y).
!
!    Input/output, integer ( ip ) REDG, the edge of triangle RTRI that
!    is visible from (X,Y).  1 <= REDG <= 3.
!
  implicit none

  integer ( ip ), parameter :: dim_num = 2
  integer ( ip ) node_num
  integer ( ip ) triangle_num

  integer ( ip ) a
  integer ( ip ) b
  integer ( ip ) e
  integer ( ip ) i4_wrap
  integer ( ip ) l
  logical              ldone
  integer ( ip ) ledg
  integer ( ip ) lr
  integer ( ip ) lrline
  integer ( ip ) ltri
  real    ( rp ) node_xy(2,node_num)
  integer ( ip ) redg
  integer ( ip ) rtri
  integer ( ip ) t
  integer ( ip ) triangle_neighbor(3,triangle_num)
  integer ( ip ) triangle_node(3,triangle_num)
  real    ( rp ) x
  real    ( rp ) y
!
!  Find the rightmost visible boundary edge using links, then possibly
!  leftmost visible boundary edge using triangle neighbor information.
!
  if ( ltri == 0 ) then
    ldone = .false.
    ltri = rtri
    ledg = redg
  else
    ldone = .true.
  end if

  do

    l = -triangle_neighbor(redg,rtri)
    t = l / 3
    e = mod ( l, 3 ) + 1
    a = triangle_node(e,t)

    if ( e <= 2 ) then
      b = triangle_node(e+1,t)
    else
      b = triangle_node(1,t)
    end if

    lr = lrline ( x, y, node_xy(1,a), node_xy(2,a), node_xy(1,b), &
      node_xy(2,b), 0.0D+00 )

    if ( lr <= 0 ) then
      exit
    end if

    rtri = t
    redg = e

  end do

  if ( ldone ) then
    return
  end if

  t = ltri
  e = ledg

  do

    b = triangle_node(e,t)
    e = i4_wrap ( e-1, 1, 3 )

    do while ( 0 < triangle_neighbor(e,t) )

      t = triangle_neighbor(e,t)

      if ( triangle_node(1,t) == b ) then
        e = 3
      else if ( triangle_node(2,t) == b ) then
        e = 1
      else
        e = 2
      end if

    end do

    a = triangle_node(e,t)

    lr = lrline ( x, y, node_xy(1,a), node_xy(2,a), node_xy(1,b), &
      node_xy(2,b), 0.0D+00 )

    if ( lr <= 0 ) then
      exit
    end if

  end do

  ltri = t
  ledg = e

  return
end subroutine
!
!
!
subroutine swapec ( i, top, btri, bedg, node_num, node_xy, triangle_num, &
  triangle_node, triangle_neighbor, stack, ierr )

!*****************************************************************************80
!
!! SWAPEC swaps diagonal edges until all triangles are Delaunay.
!
!  Discussion:
!
!    The routine swaps diagonal edges in a 2D triangulation, based on
!    the empty circumcircle criterion, until all triangles are Delaunay,
!    given that I is the index of the new vertex added to the triangulation.
!
!  Modified:
!
!    14 July 2001
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, integer ( ip ) I, the index of the new vertex.
!
!    Input/output, integer ( ip ) TOP, the index of the top of the stack.
!    On output, TOP is zero.
!
!    Input/output, integer ( ip ) BTRI, BEDG; on input, if positive, are the
!    triangle and edge indices of a boundary edge whose updated indices
!    must be recorded.  On output, these may be updated because of swaps.
!
!    Input, integer ( ip ) NODE_NUM, the number of points.
!
!    Input, real ( rp ) NODE_XY(2,NODE_NUM), the coordinates of
!    the points.
!
!    Input, integer ( ip ) TRIANGLE_NUM, the number of triangles.
!
!    Input/output, integer ( ip ) TRIANGLE_NODE(3,TRIANGLE_NUM), the triangle
!    incidence list.  May be updated on output because of swaps.
!
!    Input/output, integer ( ip ) TRIANGLE_NEIGHBOR(3,TRIANGLE_NUM),
!    the triangle neighbor list; negative values are used for links of the
!    counter-clockwise linked list of boundary edges;  May be updated on output
!    because of swaps.
!    LINK = -(3*I + J-1) where I, J = triangle, edge index.
!
!    Workspace, integer ( ip ) STACK(MAXST); on input, entries 1 through TOP
!    contain the indices of initial triangles (involving vertex I)
!    put in stack; the edges opposite I should be in interior;  entries
!    TOP+1 through MAXST are used as a stack.
!
!    Output, integer ( ip ) IERR is set to 8 for abnormal return.
!
  implicit none

  integer ( ip ) node_num
  integer ( ip ) triangle_num

  integer ( ip ) a
  integer ( ip ) b
  integer ( ip ) bedg
  integer ( ip ) btri
  integer ( ip ) c
  integer ( ip ) diaedg
  integer ( ip ) e
  integer ( ip ) ee
  integer ( ip ) em1
  integer ( ip ) ep1
  integer ( ip ) f
  integer ( ip ) fm1
  integer ( ip ) fp1
  integer ( ip ) i
  integer ( ip ) ierr
  integer ( ip ) i4_wrap
  integer ( ip ) l
  real    ( rp ) node_xy(2,node_num)
  integer ( ip ) r
  integer ( ip ) s
  integer ( ip ) stack(node_num)
  integer ( ip ) swap
  integer ( ip ) t
  integer ( ip ) top
  integer ( ip ) triangle_neighbor(3,triangle_num)
  integer ( ip ) triangle_node(3,triangle_num)
  integer ( ip ) tt
  integer ( ip ) u
  real    ( rp ) x
  real    ( rp ) y
!
!  Determine whether triangles in stack are Delaunay, and swap
!  diagonal edge of convex quadrilateral if not.
!
  x = node_xy(1,i)
  y = node_xy(2,i)

  do

    if ( top <= 0 ) then
      exit
    end if

    t = stack(top)
    top = top - 1

    if ( triangle_node(1,t) == i ) then
      e = 2
      b = triangle_node(3,t)
    else if ( triangle_node(2,t) == i ) then
      e = 3
      b = triangle_node(1,t)
    else
      e = 1
      b = triangle_node(2,t)
    end if

    a = triangle_node(e,t)
    u = triangle_neighbor(e,t)

    if ( triangle_neighbor(1,u) == t ) then
      f = 1
      c = triangle_node(3,u)
    else if ( triangle_neighbor(2,u) == t ) then
      f = 2
      c = triangle_node(1,u)
    else
      f = 3
      c = triangle_node(2,u)
    end if

    swap = diaedg ( x, y, node_xy(1,a), node_xy(2,a), node_xy(1,c), &
      node_xy(2,c), node_xy(1,b), node_xy(2,b) )

    if ( swap == 1 ) then

      em1 = i4_wrap ( e - 1, 1, 3 )
      ep1 = i4_wrap ( e + 1, 1, 3 )
      fm1 = i4_wrap ( f - 1, 1, 3 )
      fp1 = i4_wrap ( f + 1, 1, 3 )

      triangle_node(ep1,t) = c
      triangle_node(fp1,u) = i
      r = triangle_neighbor(ep1,t)
      s = triangle_neighbor(fp1,u)
      triangle_neighbor(ep1,t) = u
      triangle_neighbor(fp1,u) = t
      triangle_neighbor(e,t) = s
      triangle_neighbor(f,u) = r

      if ( 0 < triangle_neighbor(fm1,u) ) then
        top = top + 1
        stack(top) = u
      end if

      if ( 0 < s ) then

        if ( triangle_neighbor(1,s) == u ) then
          triangle_neighbor(1,s) = t
        else if ( triangle_neighbor(2,s) == u ) then
          triangle_neighbor(2,s) = t
        else
          triangle_neighbor(3,s) = t
        end if

        top = top + 1

        if ( node_num < top ) then
          ierr = 8
          return
        end if

        stack(top) = t

      else

        if ( u == btri .and. fp1 == bedg ) then
          btri = t
          bedg = e
        end if

        l = - ( 3 * t + e - 1 )
        tt = t
        ee = em1

        do while ( 0 < triangle_neighbor(ee,tt) )

          tt = triangle_neighbor(ee,tt)

          if ( triangle_node(1,tt) == a ) then
            ee = 3
          else if ( triangle_node(2,tt) == a ) then
            ee = 1
          else
            ee = 2
          end if

        end do

        triangle_neighbor(ee,tt) = l

      end if

      if ( 0 < r ) then

        if ( triangle_neighbor(1,r) == t ) then
          triangle_neighbor(1,r) = u
        else if ( triangle_neighbor(2,r) == t ) then
          triangle_neighbor(2,r) = u
        else
          triangle_neighbor(3,r) = u
        end if

      else

        if ( t == btri .and. ep1 == bedg ) then
          btri = u
          bedg = f
        end if

        l = - ( 3 * u + f - 1 )
        tt = u
        ee = fm1

        do while ( 0 < triangle_neighbor(ee,tt) )

          tt = triangle_neighbor(ee,tt)

          if ( triangle_node(1,tt) == b ) then
            ee = 3
          else if ( triangle_node(2,tt) == b ) then
            ee = 1
          else
            ee = 2
          end if

        end do

        triangle_neighbor(ee,tt) = l

      end if

    end if

  end do

  return
end subroutine
!
!
!
 END MODULE Delauney      
!
!
!

function lrline ( xu, yu, xv1, yv1, xv2, yv2, dv )

!*****************************************************************************80
!
!! LRLINE determines if a point is left of, right or, or on a directed line.
!
!  Discussion:
!
!    The directed line is parallel to, and at a signed distance DV from
!    a directed base line from (XV1,YV1) to (XV2,YV2).
!
!  Modified:
!
!    14 July 2001
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, real ( rp ) XU, YU, the coordinates of the point whose
!    position relative to the directed line is to be determined.
!
!    Input, real ( rp ) XV1, YV1, XV2, YV2, the coordinates of two points
!    that determine the directed base line.
!
!    Input, real ( rp ) DV, the signed distance of the directed line
!    from the directed base line through the points (XV1,YV1) and (XV2,YV2).
!    DV is positive for a line to the left of the base line.
!
!    Output, integer ( ip ) LRLINE, the result:
!    +1, the point is to the right of the directed line;
!     0, the point is on the directed line;
!    -1, the point is to the left of the directed line.
!
use KindType 
 implicit none

  real    ( rp ) dv
  real    ( rp ) dx
  real    ( rp ) dxu
  real    ( rp ) dy
  real    ( rp ) dyu
  integer ( ip ) lrline
  real    ( rp ) t
  real    ( rp ) tol
  real    ( rp ) tolabs
  real    ( rp ) xu
  real    ( rp ) xv1
  real    ( rp ) xv2
  real    ( rp ) yu
  real    ( rp ) yv1
  real    ( rp ) yv2

  tol = 100.0D+00 * epsilon ( tol )

  dx = xv2 - xv1
  dy = yv2 - yv1
  dxu = xu - xv1
  dyu = yu - yv1

  tolabs = tol * max ( abs ( dx ), abs ( dy ), abs ( dxu ), &
    abs ( dyu ), abs ( dv ) )

  t = dy * dxu - dx * dyu !+ dv * sqrt ( dx * dx + dy * dy )

  if ( tolabs < t ) then
    lrline = 1
  else if ( -tolabs <= t ) then
    lrline = 0
  else
    lrline = -1
  end if

  return
end function
!
!
!
function i4_wrap ( ival, ilo, ihi )

!*****************************************************************************80
!
!! I4_WRAP forces an I4 to lie between given limits by wrapping.
!
!  Example:
!
!    ILO = 4, IHI = 8
!
!    I  I4_WRAP
!
!    -2     8
!    -1     4
!     0     5
!     1     6
!     2     7
!     3     8
!     4     4
!     5     5
!     6     6
!     7     7
!     8     8
!     9     4
!    10     5
!    11     6
!    12     7
!    13     8
!    14     4
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    19 August 2003
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, integer ( ip ) IVAL, a value.
!
!    Input, integer ( ip ) ILO, IHI, the desired bounds for the value.
!
!    Output, integer ( ip ) I4_WRAP, a "wrapped" version of the value.
!
use KindType
  implicit none

  integer ( ip ) i4_modp
  integer ( ip ) i4_wrap
  integer ( ip ) ihi
  integer ( ip ) ilo
  integer ( ip ) ival
  integer ( ip ) jhi
  integer ( ip ) jlo
  integer ( ip ) wide

  jlo = min ( ilo, ihi )
  jhi = max ( ilo, ihi )

  wide = jhi - jlo + 1

  if ( wide == 1 ) then
    i4_wrap = jlo
  else
    i4_wrap = jlo + i4_modp ( ival - jlo, wide )
  end if

  return
end function
!
!
!
function diaedg ( x0, y0, x1, y1, x2, y2, x3, y3 )

!*****************************************************************************80
!
!! DIAEDG chooses a diagonal edge.
!
!  Discussion:
!
!    The routine determines whether 0--2 or 1--3 is the diagonal edge
!    that should be chosen, based on the circumcircle criterion, where
!    (X0,Y0), (X1,Y1), (X2,Y2), (X3,Y3) are the vertices of a simple
!    quadrilateral in counterclockwise order.
!
!  Modified:
!
!    19 February 2001
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, real ( rp ) X0, Y0, X1, Y1, X2, Y2, X3, Y3, the
!    coordinates of the vertices of a quadrilateral, given in
!    counter clockwise order.
!
!    Output, integer ( ip ) DIAEDG, chooses a diagonal:
!    +1, if diagonal edge 02 is chosen;
!    -1, if diagonal edge 13 is chosen;
!     0, if the four vertices are cocircular.
!
use KindType 
 implicit none

  real    ( rp ) ca
  real    ( rp ) cb
  integer ( ip ) diaedg
  real    ( rp ) dx10
  real    ( rp ) dx12
  real    ( rp ) dx30
  real    ( rp ) dx32
  real    ( rp ) dy10
  real    ( rp ) dy12
  real    ( rp ) dy30
  real    ( rp ) dy32
  real    ( rp ) s
  real    ( rp ) tol
  real    ( rp ) tola
  real    ( rp ) tolb
  real    ( rp ) x0
  real    ( rp ) x1
  real    ( rp ) x2
  real    ( rp ) x3
  real    ( rp ) y0
  real    ( rp ) y1
  real    ( rp ) y2
  real    ( rp ) y3

  tol = 100.0D+00 * epsilon ( tol )

  dx10 = x1 - x0
  dy10 = y1 - y0
  dx12 = x1 - x2
  dy12 = y1 - y2
  dx30 = x3 - x0
  dy30 = y3 - y0
  dx32 = x3 - x2
  dy32 = y3 - y2

  tola = tol * max ( abs ( dx10 ), abs ( dy10 ), abs ( dx30 ), abs ( dy30 ) )
  tolb = tol * max ( abs ( dx12 ), abs ( dy12 ), abs ( dx32 ), abs ( dy32 ) )

  ca = dx10 * dx30 + dy10 * dy30
  cb = dx12 * dx32 + dy12 * dy32

  if ( tola < ca .and. tolb < cb ) then

    diaedg = -1

  else if ( ca < -tola .and. cb < -tolb ) then

    diaedg = 1

  else

    tola = max ( tola, tolb )
    s = ( dx10 * dy30 - dx30 * dy10 ) * cb + ( dx32 * dy12 - dx12 * dy32 ) * ca

    if ( tola < s ) then
      diaedg = -1
    else if ( s < -tola ) then
      diaedg = 1
    else
      diaedg = 0
    end if

  end if

  return
end function
!
!
!
function i4_modp ( i, j )

!*****************************************************************************80
!
!! I4_MODP returns the nonnegative remainder of integer ( ip ) division.
!
!  Discussion:
!
!    The MOD function computes a result with the same sign as the
!    quantity being divided.  Thus, suppose you had an angle A,
!    and you wanted to ensure that it was between 0 and 360.
!    Then mod(A,360) would do, if A was positive, but if A
!    was negative, your result would be between -360 and 0.
!
!    On the other hand, I4_MODP(A,360) is between 0 and 360, always.
!
!    If
!      NREM = I4_MODP ( I, J )
!      NMULT = ( I - NREM ) / J
!    then
!      I = J * NMULT + NREM
!    where NREM is always nonnegative.
!
!  Example:
!
!        I     J     MOD  I4_MODP    Factorization
!
!      107    50       7       7    107 =  2 *  50 + 7
!      107   -50       7       7    107 = -2 * -50 + 7
!     -107    50      -7      43   -107 = -3 *  50 + 43
!     -107   -50      -7      43   -107 =  3 * -50 + 43
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    02 March 1999
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, integer ( ip ) I, the number to be divided.
!
!    Input, integer ( ip ) J, the number that divides I.
!
!    Output, integer ( ip ) I4_MODP, the nonnegative remainder
!    when I is divided by J.
!
use KindType
  implicit none

  integer ( ip ) i
  integer ( ip ) i4_modp
  integer ( ip ) j

  if ( j == 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'I4_MODP - Fatal error!'
    write ( *, '(a,i8)' ) '  I4_MODP ( I, J ) called with J = ', j
    call runend('I4_MODP - Fatal error!')
  end if

  i4_modp = mod ( i, j )

  if ( i4_modp < 0 ) then
    i4_modp = i4_modp + abs ( j )
  end if

  return
end function

