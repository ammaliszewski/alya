   subroutine set_bufemesh
  !***************************************************
  !*
  !*   Builds the 2D (surface) transition mesh
  !*
  !***************************************************
  use Master 
  use InpOut
  implicit none
  !
  logical                  :: origin,found
  integer(ip)              :: nr,na
  integer(ip)              :: ipoin2d,ir,ia,ipoin,ielem,i,j,ipos,neigh,jpoin
  integer(ip)              :: ibopo,kbopo,jbopo,iboun,iiter,niter
  real(rp)                 :: xo,yo,x1,y1,R1,a1,Rc,xn,yn,Rn,an
  real(rp)                 :: lambda_smooth,mu_smooth,x_baric,y_baric
  !
  !*** Writes to the log file
  !
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Building the 2D surface buffer mesh...',/)
  !
  !*** Allocates
  !
  bufe%nelem_x1 = INT((tran%xi - bufe%xi)/bufe%cell_size_x) 
  bufe%nelem_x2 = INT((bufe%xf - tran%xf)/bufe%cell_size_x) 
  bufe%nelem_y1 = INT((tran%yi - bufe%yi)/bufe%cell_size_y) 
  bufe%nelem_y2 = INT((bufe%yf - tran%yf)/bufe%cell_size_y) 
  !
  nr = bufe%nelem_x1 + 1                                       ! number of points along radial  direction
  na = tran%nelem_x1 + farm%nelem_x1 + tran%nelem_x2 + 1 + &   ! number of points along azimuth directon
       tran%nelem_y1 + farm%nelem_y1 + tran%nelem_y2 - 1 + &
       tran%nelem_x1 + farm%nelem_x1 + tran%nelem_x2 + 1 + &
       tran%nelem_y1 + farm%nelem_y1 + tran%nelem_y2 - 1 
  !
  bufe%npoin = nr * na
  bufe%nelem = (nr-1)*(na ) 
  bufe%nboun = na
  bufe%nbopo = na        
  !  
  allocate(bufe%lnods2d(4,bufe%nelem))
  allocate(bufe%lnodb2d(2,bufe%nboun))
  allocate(bufe%lface2d(4,bufe%nelem))
  allocate(bufe%lcode2d(4,bufe%nelem))
  allocate(bufe%lpoty  (  bufe%npoin))
  allocate(bufe%lpoin2d(9,bufe%npoin))
  allocate(bufe%ibopo  (  bufe%nbopo))
  allocate(bufe%myshell(  bufe%npoin))
  allocate(bufe%x2d    (  bufe%npoin))
  allocate(bufe%y2d    (  bufe%npoin))
  !
  bufe%lface2d(:,:) = 0
  bufe%lcode2d(:,:) = 0
  !
  !*** Parameters of the circle
  !
  xo = 0.5*(bufe%xi + bufe%xf)   ! x center of mass
  yo = 0.5*(bufe%yi + bufe%yf)   ! y center of mass
  Rc = bufe%xf - xo              ! radius of the outer circle
  Rc = 1.2*Rc                    ! (multiply by a certain factor)
  !
  !*** Buffer: coodinates, lpoty, ibopo, myshell
  !
  ipoin = 0
  ibopo = 0
  kbopo = 0
  do ir = 1,nr   
     !                            
     ! bottom
     !
     do ia = 1,tran%nelem_x1 + farm%nelem_x1 + tran%nelem_x2 + 1   
        ipoin = ipoin + 1
        !
        x1 = tran%x1d(ia) - xo          !  Boundary point
        y1 = tran%y1d(1 ) - yo       
        call xy2ra(x1,y1,R1,a1,origin)  !  Get polar coordinates (R1,a1) of the point (x1,y1)
        Rn = R1 + (ir-1)*(Rc-R1)/(nr-1) !  Polar coordinates of the point (xn,yn)
        an = a1
        !
        bufe%x2d(ipoin) = xo + Rn*cos(an)            !  Cartesian coordinates of the point (xn,yn)
        bufe%y2d(ipoin) = yo + Rn*sin(an)
        !
        if(ir.eq.1) then
          kbopo = kbopo + 1
          bufe%lpoty(ipoin) = -tran%lpoty(tran%inode(ia,1))       
        else
          bufe%lpoty(ipoin) =  farm%npoin + tran%npoin - farm%nbopo + ipoin - kbopo
        end if
        !
        if(ir.eq.nr) then
          ibopo = ibopo + 1
          bufe%ibopo(ibopo) =  farm%npoin + tran%npoin - farm%nbopo + ipoin - kbopo  ! global numeration of external boundary point
        end if
        !
        if(ir.ge.nr/2) then
          bufe%myshell(ipoin) = 2
        else
          bufe%myshell(ipoin) = 1
        end if
        !
     end do
     !
     !  Right
     !
     do ia = 2,tran%nelem_y1 + farm%nelem_y1 + tran%nelem_y2   
        ipoin = ipoin + 1
        !
        x1 = tran%x1d(tran%nelem_x1 + farm%nelem_x1 + tran%nelem_x2 +1) - xo  !  Boundary point
        y1 = tran%y1d(ia) - yo       
        call xy2ra(x1,y1,R1,a1,origin)  !  Get polar coordinates (R1,a1) of the point (x1,y1)
        Rn = R1 + (ir-1)*(Rc-R1)/(nr-1) !  Polar coordinates of the point (xn,yn)
        an = a1
        !
        bufe%x2d(ipoin) = xo + Rn*cos(an)            !  Cartesian coordinates of the point (xn,yn)
        bufe%y2d(ipoin) = yo + Rn*sin(an)
        !
        if(ir.eq.1) then
          kbopo = kbopo + 1
          bufe%lpoty(ipoin) = -tran%lpoty(tran%inode(tran%nelem_x1 + farm%nelem_x1 + tran%nelem_x2 + 1,ia))  
        else
          bufe%lpoty(ipoin) =  farm%npoin + tran%npoin - farm%nbopo + ipoin - kbopo
        end if
        !
        if(ir.eq.nr) then
          ibopo = ibopo + 1
          bufe%ibopo(ibopo) =  farm%npoin + tran%npoin - farm%nbopo + ipoin - kbopo  ! global numeration of external boundary point
        end if
        !
        if(ir.ge.nr/2) then
          bufe%myshell(ipoin) = 2
        else
          bufe%myshell(ipoin) = 1
        end if
        !
     end do
     !                            
     ! Top
     !
     do ia = tran%nelem_x1 + farm%nelem_x1 + tran%nelem_x2 + 1,1,-1 
        ipoin = ipoin + 1
        !
        x1 = tran%x1d(ia) - xo          !  Boundary point
        y1 = tran%y1d(tran%nelem_y1 + farm%nelem_y1 + tran%nelem_y2 + 1 ) - yo       
        call xy2ra(x1,y1,R1,a1,origin)  !  Get polar coordinates (R1,a1) of the point (x1,y1)
        Rn = R1 + (ir-1)*(Rc-R1)/(nr-1) !  Polar coordinates of the point (xn,yn)
        an = a1
        !
        bufe%x2d(ipoin) = xo + Rn*cos(an)            !  Cartesian coordinates of the point (xn,yn)
        bufe%y2d(ipoin) = yo + Rn*sin(an)
        !
        if(ir.eq.1) then
          kbopo = kbopo + 1
          bufe%lpoty(ipoin) = -tran%lpoty(tran%inode(ia,tran%nelem_y1 + farm%nelem_y1 + tran%nelem_y2 +1))  
        else
          bufe%lpoty(ipoin) =  farm%npoin + tran%npoin - farm%nbopo + ipoin - kbopo
        end if 
        !
        if(ir.eq.nr) then
          ibopo = ibopo + 1
          bufe%ibopo(ibopo) =  farm%npoin + tran%npoin - farm%nbopo + ipoin - kbopo  ! global numeration of external boundary point
        end if
        !
        if(ir.ge.nr/2) then
          bufe%myshell(ipoin) = 2
        else
          bufe%myshell(ipoin) = 1
        end if
     end do
     !
     !  Left
     !
     do ia = tran%nelem_y1 + farm%nelem_y1 + tran%nelem_y2,2,-1   
        ipoin = ipoin + 1
        !
        x1 = tran%x1d(1 ) - xo          !  Boundary point
        y1 = tran%y1d(ia) - yo       
        call xy2ra(x1,y1,R1,a1,origin)  !  Get polar coordinates (R1,a1) of the point (x1,y1)
        Rn = R1 + (ir-1)*(Rc-R1)/(nr-1) !  Polar coordinates of the point (xn,yn)
        an = a1
        !
        bufe%x2d(ipoin) = xo + Rn*cos(an)            !  Cartesian coordinates of the point (xn,yn)
        bufe%y2d(ipoin) = yo + Rn*sin(an)
        !
        if(ir.eq.1) then
          kbopo = kbopo + 1
          bufe%lpoty(ipoin) = -tran%lpoty(tran%inode(1,ia))      
        else
          bufe%lpoty(ipoin) =  farm%npoin + tran%npoin - farm%nbopo + ipoin - kbopo
        end if
        !
        if(ir.eq.nr) then
          ibopo = ibopo + 1
          bufe%ibopo(ibopo) =  farm%npoin + tran%npoin - farm%nbopo + ipoin - kbopo  ! global numeration of external boundary point
        end if
        !
        if(ir.ge.nr/2) then
          bufe%myshell(ipoin) = 2
        else
          bufe%myshell(ipoin) = 1
        end if
        !
     end do

   end do ! ir = 1,nr 
  !
  !*** Buffer: lnods2d(4,nelem)
  !***         lnodb2d(2,nboun)
  !***         lface2d(4,nelem) = 0 inner face, iboun for outer face
  !***         lcode2d(4,nelem) = 0 inner face, icode for outer face
  !
  ielem = 0     
  iboun = 0
  do ir = 1,nr-1
     do ia = 1,na
        ielem = ielem + 1
        if(ia.ne.na) then
          bufe%lnods2d(1,ielem) = (ir  )*na + ia
          bufe%lnods2d(2,ielem) = (ir  )*na + ia + 1
          bufe%lnods2d(3,ielem) = (ir-1)*na + ia + 1
          bufe%lnods2d(4,ielem) = (ir-1)*na + ia 
       else
          bufe%lnods2d(1,ielem) = (ir  )*na + ia
          bufe%lnods2d(2,ielem) = (ir  )*na + 1 
          bufe%lnods2d(3,ielem) = (ir-1)*na + 1 
          bufe%lnods2d(4,ielem) = (ir-1)*na + ia       
       end if
       !
       if(ir.eq.(nr-1)) then
         iboun = iboun + 1
         if(ia.ne.na) then
            bufe%lnodb2d(1,iboun) = (ir  )*na + ia
            bufe%lnodb2d(2,iboun) = (ir  )*na + ia + 1
         else
            bufe%lnodb2d(1,iboun) = (ir  )*na + ia
            bufe%lnodb2d(2,iboun) = (ir  )*na + 1
         end if
         bufe%lface2d(1,ielem) = iboun
         bufe%lcode2d(1,ielem) = 1                 !  External face (code 1)
       end if
     end do
  end do  
  !
  !*** Buffer: lpoin2d
  !   
  do ipoin = 1,bufe%npoin
     bufe%lpoin2d(1:9,ipoin) = 0
     !
     do ielem = 1,bufe%nelem
        found = .false.
        do i = 1,4
           if(bufe%lnods2d(i,ielem).eq.ipoin) found = .true.
        end do
        if(found) then
           do i = 1,4                    ! candidates (icluding ipoint itself)
              jpoin = bufe%lnods2d(i,ielem)
              if (ipoin.ne.jpoin) then
                  ipos  =  0
                  neigh =  1 
                  do j = 1,8                                          ! span over all possible neighbours
                     if( bufe%lpoin2d(j,ipoin).eq.jpoin) neigh = -1   ! already  counter for
                     if((bufe%lpoin2d(j,ipoin).eq.0).and. &       
                        (ipos                 .eq.0) )   ipos  = j    ! position
                  end do
                  if(neigh.eq.1) then 
                     bufe%lpoin2d(ipos,ipoin) = jpoin
                     bufe%lpoin2d(9   ,ipoin) = ipos                  ! number of neighbours counted so far
                  end if
              end if
           end do
        end if      ! not found
     end do         ! ielem = 1,bufe%nelem
  end do            ! ipoin = 1,bufe%npoin
  !
  !*** Laplacian smoothing of coordinates (if necessary)
  !
  if(bufe%laplacian_mesh_smoothing) then
     lambda_smooth = 0.6307_rp
     mu_smooth     = 1.0_rp/(0.1_rp - 1.0_rp/lambda_smooth)
     niter         = 0.2*(na+nr)  ! mumber of smoothing iterations
  else 
     niter = 0
  end if
!
  do iiter = 1,niter               
!
!    Forward laplacian
!
     do ipoin = 1,bufe%npoin
         if(bufe%lpoin2d(9,ipoin).eq.8) then    ! inner point (8 neighbours)
            !
            x_baric = 0.0_rp
            y_baric = 0.0_rp
            do i = 1,8
               x_baric = x_baric + bufe%x2d(bufe%lpoin2d(i,ipoin))
               y_baric = y_baric + bufe%y2d(bufe%lpoin2d(i,ipoin))
            end do
            x_baric = x_baric/8.0_rp 
            y_baric = y_baric/8.0_rp 
            bufe%x2d(ipoin) = bufe%x2d(ipoin) + lambda_smooth*(x_baric-bufe%x2d(ipoin))
            bufe%y2d(ipoin) = bufe%y2d(ipoin) + lambda_smooth*(y_baric-bufe%y2d(ipoin))
            !
          end if
     end do
!
!    Backwards laplacian
!
     do ipoin = 1,bufe%npoin
        if(bufe%lpoin2d(9,ipoin).eq.8) then    ! inner point (8 neighbours)
           !
           x_baric = 0.0_rp
           y_baric = 0.0_rp
           do i = 1,8
              x_baric = x_baric + bufe%x2d(bufe%lpoin2d(i,ipoin))
              y_baric = y_baric + bufe%y2d(bufe%lpoin2d(i,ipoin))
           end do
            x_baric = x_baric/8.0_rp 
            y_baric = y_baric/8.0_rp 
            bufe%x2d(ipoin) = bufe%x2d(ipoin) + mu_smooth*(x_baric-bufe%x2d(ipoin))
            bufe%y2d(ipoin) = bufe%y2d(ipoin) + mu_smooth*(y_baric-bufe%y2d(ipoin))
            !
         end if
     end do
  end do
  !
  return
  end subroutine set_bufemesh
!
!
!
     subroutine xy2ra(x,y,r,a,origin)
!**********************************************************
!*  
!*   Cartesian to polar conversion
!*
!**********************************************************
    use KindType
    implicit none
!
    logical  :: origin
    real(rp) :: x,y,r,a
!
    real(rp), parameter :: epsilon = 1d-6
    real(rp), parameter :: twopi   = 8.0d0*atan(1.0d0)
!
    r = sqrt(x*x+y*y)
    if(r.gt.epsilon) then
       origin = .false.
       a      = atan2(y,x)          ! angle in radians (-pi,pi)
       if(a.lt.0d0) a = a + twopi   ! anlge in radians (0,2*pi)
    else
       origin = .true.
       a      = 0d0
    end if
    return
    end subroutine xy2ra
