      subroutine out_google(fout,name)
!*************************************************
!*
!*    Writes the kml script to visualize the
!*    domain in GoogleEarth
!*
!*************************************************
     use KindType
     use Master
     use Coordinates
     use InpOut, only: topo_format
     implicit none
!
     character(len=s_file) :: fout,name,line
     integer(ip) :: istat,iboun,i
     real   (rp) :: lon,lat,x,y
!
!*** Mesh kml file
!
     open(90,file=TRIM(fout),status='unknown')
     write(90,10) TRIM(name),npoin2d,nelem2d,nboun2d
 10  format(&
     '<?xml version="1.0" encoding="UTF-8"?>',/,&
     '<kml xmlns="http://www.opengis.net/kml/2.2">',/,&
     '<Document>',/,&
     '<name>',a,' Mesh</name>',/,&
     '<Snippet maxLines="3">',/,&
     '<![CDATA[',/,&
      '<ul>',/,&
     '<li>Number of surface nodes:<b>',i9,'</b></li>',/,&
     '<li>Number of surface elements:<b>',i9,'</b></li>',/,&
     '<li>Number of surface boundaries:<b>',i6,'</b></li>',/,&
      '</ul>',/,&
     ']]>',/,&
     '</Snippet>',/,&
!
!*** Define styles. Color in format aabbggrr, aa=alfa (de 00 a ff); bb=azul (de 00 a ff); gg=verde (de 00 a ff); rr=rojo (de 00 a ff)
!
!                                     Farm
     '<Style id="StyleFarm">',/,&
     '<LineStyle>',/,&
     '<width>1.5</width>',/,&
     '<color>7d32CD32</color>',/,&   
     '</LineStyle>',/,&
     '<PolyStyle>',/,&
     '<color>7d32CD32</color>',/,&
     '</PolyStyle>',/,&
     '</Style>',/,&
!                                     Transition
     '<Style id="StyleTransition">',/,&
     '<LineStyle>',/,&
     '<width>1.5</width>',/,&
     '<color>7dFFA500</color>',/,&
     '</LineStyle>',/,&
     '<PolyStyle>',/,&
     '<color>7dFFA500</color>',/,&
     '</PolyStyle>',/,&
     '</Style>',/,&
!                                     Buffer
     '<Style id="StyleBuffer">',/,&
     '<LineStyle>',/,&
     '<width>1.5</width>',/,&
     '<color>7d0000ff</color>',/,&
     '</LineStyle>',/,&
     '<PolyStyle>',/,&
     '<color>7d0000ff</color>',/,&
     '</PolyStyle>',/,&
     '</Style>',/,&
!                                     Map file
     '<Style id="StyleMap">',/,&
     '<LineStyle>',/,&
     '<width>1.5</width>',/,&
     '<color>50FFFFFF</color>',/,&
     '</LineStyle>',/,&
     '<PolyStyle>',/,&
     '<color>50FFFFFF</color>',/,&
     '</PolyStyle>',/,&
     '</Style>')
!
!     '<Folder>',/,&
!     '<name>Mesh zones</name>',/,&
!     '<open>1</open>')
!
!*** Map file coverage
!
     if(topo_format.ne.'NONE') then
     write(90,20) 'Map file',n_top,'#StyleMap'
 20  format(&
     '<Placemark>',/,&
     '<name>',a,'</name>',/,&
     '<Snippet maxLines="1">',/,&
     '<![CDATA[',/,&
      '<ul>',/,&
     '<li>Number points:<b>',i8,'</b></li>',/,&
     '</ul>',/,&
     ']]>',/,&
     '</Snippet>',/,&
     '<styleUrl>',a,'</styleUrl>',/,&
     '<Polygon>',/,&
     '<extrude>0</extrude>',/,&
     '<tessellate>1</tessellate>',/,&
     '<altitudeMode>clampToGround</altitudeMode>',/,&
     '<outerBoundaryIs>',/,&
     '<LinearRing>',/,&
     '<coordinates>')
!
      x = utm_x_ref + minval(x_top)
      y = utm_y_ref + minval(y_top)
      call utm2ll(x,y,utmzone,lon,lat,2,istat)             ! Map file limits
      if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat) 
      write(line,21) lon,lat,',0'
21    format(' ',f10.5,',',f10.5,a)
      call remove_str_spaces(line)
      write(90,'(a)') TRIM(line)
!
      x = utm_x_ref + maxval(x_top)
      y = utm_y_ref + minval(y_top)
      call utm2ll(x,y,utmzone,lon,lat,2,istat)   
      if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat)       
      write(line,21) lon,lat,',0'
      call remove_str_spaces(line)
      write(90,'(a)') TRIM(line)
!
      x = utm_x_ref + maxval(x_top)
      y = utm_y_ref + maxval(y_top)
      call utm2ll(x,y,utmzone,lon,lat,2,istat)    
      if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat)         
      write(line,21) lon,lat,',0'
      call remove_str_spaces(line)
      write(90,'(a)') TRIM(line)
!
      x = utm_x_ref + minval(x_top)
      y = utm_y_ref + maxval(y_top)
      call utm2ll(x,y,utmzone,lon,lat,2,istat) 
      if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat)           
      write(line,21) lon,lat,',0'
      call remove_str_spaces(line)
      write(90,'(a)') TRIM(line)
!
      write(90,22)
 22   format(&
      '</coordinates>',/,&
      '</LinearRing>',/,&
      '</outerBoundaryIs>',/,&
      '</Polygon>',/,&
      '</Placemark>')
     end if
!
!*** Farm zone 
!
     write(90,23) 'Farm zone',farm%npoin,farm%nelem,INT(farm%cell_size_x),'#StyleFarm' 
 23  format(&
     '<Placemark>',/,&
     '<name>',a,'</name>',/,&
     '<Snippet maxLines="3">',/,&
     '<![CDATA[',/,&
      '<ul>',/,&
     '<li>Number of surface nodes:<b>',i6,'</b></li>',/,&
     '<li>Number of surface elements:<b>',i6,'</b></li>',/,&
     '<li>Cell size:<b>',i6,'m </b></li>',/,&
     '</ul>',/,&
     ']]>',/,&
     '</Snippet>',/,&
     '<styleUrl>',a,'</styleUrl>',/,&
     '<Polygon>',/,&
     '<extrude>0</extrude>',/,&
     '<tessellate>1</tessellate>',/,&
     '<altitudeMode>clampToGround</altitudeMode>',/,&
     '<outerBoundaryIs>',/,&
     '<LinearRing>',/,&
     '<coordinates>')
!
      do i = 1,4                                      ! farm limits
         x = utm_x_ref + farm%xv(i)
         y = utm_y_ref + farm%yv(i)
         call utm2ll(x,y,utmzone,lon,lat,2,istat)      
         if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat)         
         write(line,21) lon,lat,',0'
         call remove_str_spaces(line)
         write(90,'(a)') TRIM(line)
      end do
!
      write(90,24)
 24   format(&
      '</coordinates>',/,&
      '</LinearRing>',/,&
      '</outerBoundaryIs>',/,&
      '</Polygon>',/,&
      '</Placemark>')
!
!***  Transition
!     
      write(90,30) 'Transition zone',tran%npoin,tran%nelem,'#StyleTransition'
 30  format(&
     '<Placemark>',/,&
     '<name>',a,'</name>',/,&
     '<Snippet maxLines="3">',/,&
     '<![CDATA[',/,&
     '<ul>',/,&
     '<li>Number of surface nodes:<b>',i6,'</b></li>',/,&
     '<li>Number of surface elements:<b>',i6,'</b></li>',/,&
     '<li>Cell size:<b> variable </b></li>',/,&
     '</ul>',/,&
     ']]>',/,&
     '</Snippet>',/,&
     '<styleUrl>',a,'</styleUrl>',/,&
     '<Polygon>',/,&
     '<extrude>0</extrude>',/,&
     '<tessellate>1</tessellate>',/,&
     '<altitudeMode>clampToGround</altitudeMode>',/,&
     '<innerBoundaryIs>',/,&
     '<LinearRing>',/,&
     '<coordinates>')
!
      do i = 1,4                                      ! transition inner limits
         x = utm_x_ref + farm%xv(i)
         y = utm_y_ref + farm%yv(i)
         call utm2ll(x,y,utmzone,lon,lat,2,istat)   
         if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat)            
         write(line,21) lon,lat,',0'
         call remove_str_spaces(line)
         write(90,'(a)') TRIM(line)
      end do
!
      write(90,32)
 32   format(&
      '</coordinates>',/,&
      '</LinearRing>',/,&
      '</innerBoundaryIs>',/,&
      '<outerBoundaryIs>',/,&
      '<LinearRing>',/,&
      '<coordinates>')
!
      do i = 1,4                                      ! transition outer limits
         x = utm_x_ref + tran%xv(i)
         y = utm_y_ref + tran%yv(i)
         call utm2ll(x,y,utmzone,lon,lat,2,istat)    
         if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat)           
         write(line,21) lon,lat,',0'
         call remove_str_spaces(line)
         write(90,'(a)') TRIM(line)
      end do
!
      write(90,33)
 33   format(&
      '</coordinates>',/,&
      '</LinearRing>',/,&
      '</outerBoundaryIs>',/,&
      '</Polygon>',/,&
      '</Placemark>')
!
!***  Buffer
!   
      write(90,40) 'Buffer zone',bufe%npoin,bufe%nelem,INT(bufe%cell_size_x),'#StyleBuffer'
 40  format(&
     '<Placemark>',/,&
     '<name>',a,'</name>',/,&
     '<Snippet maxLines="3">',/,&
     '<![CDATA[',/,&
      '<ul>',/,&
     '<li>Number of surface nodes:<b>',i6,'</b></li>',/,&
     '<li>Number of surface elements:<b>',i6,'</b></li>',/,&
     '<li>Cell size:<b>',i6,'m </b></li>',/,&
     '</ul>',/,&
     ']]>',/,&
     '</Snippet>',/,&
     '<styleUrl>',a,'</styleUrl>',/,&
     '<Polygon>',/,&
     '<extrude>0</extrude>',/,&
     '<tessellate>1</tessellate>',/,&
     '<altitudeMode>clampToGround</altitudeMode>',/,&
     '<innerBoundaryIs>',/,&
     '<LinearRing>',/,&
     '<coordinates>')
!
      do i = 1,4                                      ! buffer inner limits
         x = utm_x_ref + tran%xv(i)
         y = utm_y_ref + tran%yv(i)
         call utm2ll(x,y,utmzone,lon,lat,2,istat) 
         if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat)              
         write(line,21) lon,lat,',0'
         call remove_str_spaces(line)
         write(90,'(a)') TRIM(line)
      end do
!
      write(90,42)             ! buffer outer limits (cilindrical)
 42   format(&
      '</coordinates>',/,&
      '</LinearRing>',/,&
      '</innerBoundaryIs>',/,&
      '<outerBoundaryIs>',/,&
      '<LinearRing>',/,&
      '<coordinates>')
!
      do iboun = 1,bufe%nboun
          x = utm_x_ref + x2d(lnodb2d(1,iboun))
          y = utm_y_ref + y2d(lnodb2d(1,iboun))
          call utm2ll(x,y,utmzone,lon,lat,2,istat)     
          if(idatum.ne.2) call ll2datum_WGS_84(lon,lat,idatum,istat)        
          write(line,21) lon,lat,',0'
          call remove_str_spaces(line)
          write(90,'(a)') TRIM(line)
      end do
!
      write(90,43)
 43   format(&
      '</coordinates>',/,&
      '</LinearRing>',/,&
      '</outerBoundaryIs>',/,&
      '</Polygon>',/,&
      '</Placemark>')
!
!***  End
!
     write(90,100) 
100  format(& 
!     '</Folder>',/,&
     '</Document>',/,&
     '</kml>')
!
     close(90)
     return
     end subroutine out_google
!
!
!
     subroutine remove_str_spaces(str1)
     use KindType
     implicit none
     character(len=s_file) :: str1,str2
     integer(ip) :: ls1,ls2,i
     str2(1:len_trim(str2)) = ' '
     ls1 = len_trim(str1)
     ls2 = 0
     do i = 1,ls1
        if(str1(i:i).ne.' ') then
           ls2 = ls2 + 1
           str2(ls2:ls2) = str1(i:i)
        endif
     enddo
     str1=str2
     return
     end subroutine remove_str_spaces
