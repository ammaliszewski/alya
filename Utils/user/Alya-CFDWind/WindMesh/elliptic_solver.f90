   subroutine elliptic_solver
  !***************************************************
  !*
  !*   This routine build an elliptic grid as the
  !*   solution of an elliptic system of equations
  !*   for the grid coordinates x,y,z
  !*
  !* Written  by: Simone Marras, Sept. 13, 2011
  !* Modified by: Arnau Folch, Apr 2013
  !* 
  !***************************************************
  use Master 
  use InpOut
  implicit none
  !
  logical     :: top_neumann
  integer(ip) :: ix,iy,iz,ipoin2d,iter
  integer(ip) :: niter_external,nzbl
  real(rp)    :: tol,err,error,Q0,lambda,dz
  !
  real   (rp), allocatable :: x3d_local(:,:,:),y3d_local(:,:,:),z3d_local(:,:,:)
  !
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Starting 3D mesh elliptic smoothing...')
  ! 
  !*** Initializations
  !
  top_neumann    = .true.
  !
  nz_nobuff = nz
  nzbl = 3                                         ! skip smoothing in the nzbl bottom layers (boundary layer)
  niter_external = int(0.001*nx_nobuff*ny_nobuff)  ! numer of smoothing iterations
  error = 1e-5_rp                                  ! Gauss-Sidel solver error
  Q0 = 0.25_rp                                     ! Q0 < 0 to repel lines from the ground; Q0 > 0 to attract lines to the ground; Q0 = 0 NO CLUSTERING
  if(TRIM(dz_distribution).eq.'GEOMETRIC') then
     lambda = 1.0_rp
  else
     lambda = 0.0_rp
  end if
  !
  !*** Allocates
  !
  allocate(x3d_local(nx_nobuff,ny_nobuff,nz_nobuff))
  allocate(y3d_local(nx_nobuff,ny_nobuff,nz_nobuff))
  allocate(z3d_local(nx_nobuff,ny_nobuff,nz_nobuff)) 
  !
  !*** Store initial grid to x3d, y3d, z3d
  !
  do iz = 1,nz_nobuff
  do iy = 1,ny_nobuff
  do ix = 1,nx_nobuff         
     ipoin2d = nobuff_2d_local(ix,iy)
     !
     x3d_local(ix,iy,iz) = x3d(ipoin2d,iz)
     y3d_local(ix,iy,iz) = y3d(ipoin2d,iz)
     z3d_local(ix,iy,iz) = z3d(ipoin2d,iz)
     !
  end do
  end do
  end do
  !
  !*** Global cycle:
  !
  tol = error
  err = 0.0_rp
  extrenal_iteration: do iter = 1,niter_external
        !
        !*** Solve the system by means of Gauss-Seidel iterations:
        !
        call gauss_seidel_TTM(iter, nzbl, Q0, lambda, x3d_local, y3d_local, z3d_local, err)
        !
        if(top_neumann) then
           do iy = 2,ny_nobuff-1
           do ix = 2,nx_nobuff-1
              x3d_local(ix,iy,nz_nobuff) = x3d_local(ix,iy,nz_nobuff-1)
              y3d_local(ix,iy,nz_nobuff) = y3d_local(ix,iy,nz_nobuff-1)
           end do
           end do
        end if
        !
        !*** Print the error and current iteration to log file
        !
        write(lulog,10) iter, err
        if(out_screen) write(*,10) iter, err
10      format(' Elliptic iterations = ',i9,' Elliptic solver error = ',f7.4,/)
        !
        !*** check error
        !   
        if(err < tol ) exit
        !
  end do extrenal_iteration
  !
  !*** Store back to the global variables:
  !
  do iz = nzbl,nz_nobuff
  do iy = 2,ny_nobuff-1
  do ix = 2,nx_nobuff-1
     ipoin2d = nobuff_2d_local(ix,iy) 
     !     
     x3d(ipoin2d,iz) = x3d_local(ix,iy,iz)
     y3d(ipoin2d,iz) = y3d_local(ix,iy,iz)
     z3d(ipoin2d,iz) = z3d_local(ix,iy,iz)
     !
  end do
  end do
  end do
  !
  !*** Deallocates
  !
  deallocate(x3d_local)
  deallocate(y3d_local)
  deallocate(z3d_local)
  !
  return
  end subroutine elliptic_solver
  !
  !
  !
  subroutine gauss_seidel_TTM(iter, nzbl, Q0, lambda, x, y, z, err)
  use Master
  implicit none
  !*
  !* 1 iteration of Gauss-Seidel:
  !*
  integer(ip):: iter, nzbl
  real(rp)   :: Q0, lambda
  integer(ip):: i, j, k, ipoin2d, ipoin3d
  real(rp)   :: xtemp, ytemp, ztemp, rhs
  real(rp)   :: g11, g22, g33, g12, g13, g23, Jac,Jinv
  real(rp)   :: x(nx_nobuff,ny_nobuff,nz_nobuff), y(nx_nobuff,ny_nobuff,nz_nobuff), z(nx_nobuff,ny_nobuff,nz_nobuff)

  real(rp)   :: P, Q, R
  real(rp)   :: err, tol
  real(rp)   :: dz

  real(rp)   :: e_x,e_y,e_z
  real(rp)   :: n_x,n_y,n_z
  real(rp)   :: c_x,c_y,c_z

  real(rp)   :: x_e, x_n, x_c
  real(rp)   :: y_e, y_n, y_c
  real(rp)   :: z_e, z_n, z_c
  
  real(rp)   :: x_en, x_ec, x_nc
  real(rp)   :: y_en, y_ec, y_nc
  real(rp)   :: z_en, z_ec, z_nc
  !
  dz = 1.0_rp/ncellz   
  !
  !*** Solve the Thompso--Thames-Mastin system using Gauss-Seidel
  !
  do k=nzbl+1,nz_nobuff-1
     do j=2,ny_nobuff-1
        do i=2,nx_nobuff-1

           !First derivatives:
           x_e = 0.5_rp*(x(i+1,j,k) - x(i-1,j,k))
           x_n = 0.5_rp*(x(i,j+1,k) - x(i,j-1,k))
           x_c = 0.5_rp*(x(i,j,k+1) - x(i,j,k-1))
           
           y_e = 0.5_rp*(y(i+1,j,k) - y(i-1,j,k))
           y_n = 0.5_rp*(y(i,j+1,k) - y(i,j-1,k))
           y_c = 0.5_rp*(y(i,j,k+1) - y(i,j,k-1))

           z_e = 0.5_rp*(z(i+1,j,k) - z(i-1,j,k))
           z_n = 0.5_rp*(z(i,j+1,k) - z(i,j-1,k))
           z_c = 0.5_rp*(z(i,j,k+1) - z(i,j,k-1))

           !Crossed second derivatives:
           x_en = 0.25_rp*( x(i+1,j+1,k) - x(i-1,j+1,k) + x(i-1,j-1,k) - x(i+1,j-1,k) )
           x_ec = 0.25_rp*( x(i+1,j,k+1) - x(i-1,j,k+1) + x(i-1,j,k-1) - x(i+1,j,k-1) )
           x_nc = 0.25_rp*( x(i,j+1,k+1) - x(i,j-1,k+1) + x(i,j-1,k-1) - x(i,j+1,k-1) )

           y_en = 0.25_rp*( y(i+1,j+1,k) - y(i-1,j+1,k) + y(i-1,j-1,k) - y(i+1,j-1,k) )
           y_ec = 0.25_rp*( y(i+1,j,k+1) - y(i-1,j,k+1) + y(i-1,j,k-1) - y(i+1,j,k-1) )
           y_nc = 0.25_rp*( y(i,j+1,k+1) - y(i,j-1,k+1) + y(i,j-1,k-1) - y(i,j+1,k-1) )

           z_en = 0.25_rp*( z(i+1,j+1,k) - z(i-1,j+1,k) + z(i-1,j-1,k) - z(i+1,j-1,k) )
           z_ec = 0.25_rp*( z(i+1,j,k+1) - z(i-1,j,k+1) + z(i-1,j,k-1) - z(i+1,j,k-1) )
           z_nc = 0.25_rp*( z(i,j+1,k+1) - z(i,j-1,k+1) + z(i,j-1,k-1) - z(i,j+1,k-1) )

           !Jacobian term:
           Jac = x_e*y_n*z_c + x_n*y_c*z_e + x_c*y_e*z_n - x_e*y_c*z_n - x_n*y_e*z_c - x_c*y_n*z_e
           Jinv = 1.0_rp/J
           
           !Back derivatives:
           e_x = Jinv*(y_n*z_c - y_c*z_n)
           e_y = Jinv*(x_c*z_n - x_n*z_c)
           e_z = Jinv*(x_n*y_c - x_c*y_n)

           n_x = Jinv*(y_c*z_e - y_e*z_c)
           n_y = Jinv*(x_e*z_c - x_c*z_e)
           n_z = Jinv*(x_c*y_e - x_e*y_c)

           c_x = Jinv*(y_e*z_n - y_n*z_e)
           c_y = Jinv*(x_n*z_e - x_e*z_n)
           c_z = Jinv*(x_e*y_n - x_n*y_e)

           !Metric tensor G (components of -):
           g11 = e_x*e_x + e_y*e_y + e_z*e_z
           g22 = n_x*n_x + n_y*n_y + n_z*n_z
           g33 = c_x*c_x + n_y*c_y + c_z*c_z
           g12 = e_x*n_x + e_y*n_y + e_z*n_z
           g13 = e_x*c_x + e_y*c_y + e_z*c_z
           g23 = n_x*c_x + n_y*c_y + n_z*c_z
           

           !Compute the control functions P and Q:
           call compute_PQ( k, dz, Q0, lambda, P, Q, R)
           
           !X:
           rhs = &
                g11*(x(i-1,j,k) + x(i+1,j,k)) + &
                g22*(x(i,j-1,k) + x(i,j+1,k)) + &
                g33*(x(i,j,k-1) + x(i,j,k+1)) + &
                2.0_rp*(g12*x_en + g13*x_ec + g23*x_nc)
           
           xtemp = 0.5_rp*rhs/(g11 + g22 + g33)

           !Y:
           rhs = &
                g11*(y(i-1,j,k) + y(i+1,j,k)) + &
                g22*(y(i,j-1,k) + y(i,j+1,k)) + &
                g33*(y(i,j,k-1) + y(i,j,k+1)) + &
                2.0_rp*(g12*y_en + g13*y_ec + g23*y_nc)
           
           ytemp = 0.5_rp*rhs/(g11 + g22 + g33)

           !Z:
           rhs = &
                g11*(z(i-1,j,k) + z(i+1,j,k)) + &
                g22*(z(i,j-1,k) + z(i,j+1,k)) + &
                g33*(z(i,j,k-1) + z(i,j,k+1)) + &
                2.0_rp*(g12*z_en + g13*z_ec + g23*z_nc) - &
                g33*z_c*R
           
           ztemp = 0.5_rp*rhs/(g11 + g22 + g33)
           
           !Adjust error at current iteration:
           err = err +			             &
		(x(i,j,k)-xtemp)*(x(i,j,k)-xtemp) +  &
		(y(i,j,k)-ytemp)*(y(i,j,k)-ytemp) +  &
		(z(i,j,k)-ztemp)*(z(i,j,k)-ztemp)
           
           x(i,j,k) = xtemp
           y(i,j,k) = ytemp
           z(i,j,k) = ztemp
                    
        end do
     end do
  end do
  !
  err = sqrt( err/((nx_nobuff-2)*(ny_nobuff-2)*(nz_nobuff-2)))
  !
  return
  end subroutine gauss_seidel_TTM
!
!
!

subroutine compute_PQ( k, dz, Q0, lambda, P, Q, R)
!--------------------------------------------------------
! compute_PQ()
!
! This routine constructs the control functions P,Q
! for the right hand side term of the elliptic system.
! They are built according to TTM and Knupp's book.
!--------------------------------------------------------
  use KindType 
  implicit none
  !
  integer(ip)  :: k
  real(rp)     :: dz,Q0,lambda
  real(rp)     :: sgnr, deta, eta
  real(rp)     :: etas

  !Return variables:
  real(rp)     :: P,Q,R

  etas =  0.0_rp     ! eta line of attraction: etas = 0 is the ground */
  P    = 0.0_rp
  Q    = 0.0_rp
  
  !Compute R
  eta = (k-1)*dz
  deta = eta - etas
  !
  if (deta >= 0.0_rp) sgnr =  1.0_rp
  if (deta < 0.0_rp)  sgnr = -1.0_rp
  !
  R = Q0*sgnr*exp( -lambda*abs(deta) )
  !
  return
  end subroutine compute_PQ

