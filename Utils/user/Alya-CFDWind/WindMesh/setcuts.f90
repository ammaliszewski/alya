   subroutine setcuts
  !***************************************************
  !*
  !*   Builds 2D cuts
  !*
  !***************************************************
  use Master 
  use InpOut
  implicit none
  !
  integer(ip)  :: nx,ny
  integer(ip)  :: ix,iy,ipoin,iz,i,ielem
  !
  !*** Writes to the log file
  !
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Building 2D mesh cuts...',/)
  !
  !*** 1) Farm cuts
  !
  !
  cutx_farm%npoin2d = farm%nelem_y1 + 1
  cutx_farm%npoin   = (cutx_farm%npoin2d    )*nz
  cutx_farm%nelem   = (cutx_farm%npoin2d - 1)*(nz-1)
  allocate(cutx_farm%ipoin(  cutx_farm%npoin2d))
  allocate(cutx_farm%lnods(4,cutx_farm%nelem  ))
  allocate(cutx_farm%zone (  cutx_farm%nelem  ))
  allocate(cutx_farm%x    (  cutx_farm%npoin  ))
  allocate(cutx_farm%y    (  cutx_farm%npoin  ))
  allocate(cutx_farm%z    (  cutx_farm%npoin  ))
  !
  cuty_farm%npoin2d = farm%nelem_x1 + 1
  cuty_farm%npoin   = (cuty_farm%npoin2d    )*nz
  cuty_farm%nelem   = (cuty_farm%npoin2d - 1)*(nz-1)
  allocate(cuty_farm%ipoin(  cuty_farm%npoin2d))
  allocate(cuty_farm%lnods(4,cuty_farm%nelem  ))
  allocate(cuty_farm%zone (  cuty_farm%nelem  ))
  allocate(cuty_farm%x    (  cuty_farm%npoin  ))
  allocate(cuty_farm%y    (  cuty_farm%npoin  ))
  allocate(cuty_farm%z    (  cuty_farm%npoin  ))
  !
  !*** ipoin list
  !
  ipoin = 0
  nx = farm%nelem_x1 + 1
  ny = farm%nelem_y1 + 1
  do iy = 1,ny
  do ix = 1,nx
     ipoin = ipoin + 1
     if(ix.eq.(nx/2)) cutx_farm%ipoin(iy) = ipoin
     if(iy.eq.(ny/2)) cuty_farm%ipoin(ix) = ipoin
  end do
  end do
  !
  !*** coordinates
  !
  ipoin = 0
  do iz = 1,nz
  do i  = 1,cutx_farm%npoin2d
     ipoin = ipoin + 1
     cutx_farm%x(ipoin) = x3d(cutx_farm%ipoin(i),iz)
     cutx_farm%y(ipoin) = y3d(cutx_farm%ipoin(i),iz)
     cutx_farm%z(ipoin) = z3d(cutx_farm%ipoin(i),iz)
  end do
  end do
  !
  ipoin = 0
  do iz = 1,nz
  do i  = 1,cuty_farm%npoin2d
     ipoin = ipoin + 1
     cuty_farm%x(ipoin) = x3d(cuty_farm%ipoin(i),iz)
     cuty_farm%y(ipoin) = y3d(cuty_farm%ipoin(i),iz)
     cuty_farm%z(ipoin) = z3d(cuty_farm%ipoin(i),iz)
  end do
  end do
  !
  !*** lnods and zone
  !
  ielem = 0
  do iz = 1,nz-1
     do i = 1,cutx_farm%npoin2d-1
        ielem = ielem + 1
        cutx_farm%zone(   ielem) = 4  
        cutx_farm%lnods(1,ielem) = (iz-1)*cutx_farm%npoin2d + i
        cutx_farm%lnods(2,ielem) = (iz-1)*cutx_farm%npoin2d + i+1
        cutx_farm%lnods(3,ielem) = (iz  )*cutx_farm%npoin2d + i+1  
        cutx_farm%lnods(4,ielem) = (iz  )*cutx_farm%npoin2d + i
    end do
  end do
  !
  ielem = 0
  do iz = 1,nz-1
     do i = 1,cuty_farm%npoin2d-1
        ielem = ielem + 1
        cuty_farm%zone(   ielem) = 4  
        cuty_farm%lnods(1,ielem) = (iz-1)*cuty_farm%npoin2d + i
        cuty_farm%lnods(2,ielem) = (iz-1)*cuty_farm%npoin2d + i+1
        cuty_farm%lnods(3,ielem) = (iz  )*cuty_farm%npoin2d + i+1  
        cuty_farm%lnods(4,ielem) = (iz  )*cuty_farm%npoin2d + i
    end do
  end do
  !
  return
  end subroutine setcuts
