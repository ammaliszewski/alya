       subroutine readmap
!********************************************************
!*
!*    Reads topography in map format
!*
!********************************************************
      use KindType
      use InpOut
      use Master
      implicit none
!
      integer(ip) :: ncol,nword,npar
      integer(ip) :: i,j,i_top,i_zo,ipos,j_top,j_zo,n_dup_z,n_dup_zo
      real   (rp) :: rvoid,ztopo,zo,zo_left,zo_right 
!
      logical :: clockwise
      character(len=s_name) :: card, words(nwormax)      
      real(rp)              :: param(nparmax)
!
!*** Writes to the log file
!
      write(lulog,1)
      if(out_screen) write(*,1)
  1   format(/,'---> Reading the topography file in MAP format...',/)
!
!***  Start reading
!
      open(lutop,file=TRIM(ftop),status='unknown')
!
!***  First of all get:
!***    The total number of topography points (n_top)
!***    The total number of roughness points  (n_zo )
!***    The total number of rougness curves   (ncurva_zo)
!***  Previous read is needed in order to allocate memory first
!
      read(lutop,*) card   ! 4 lines header
      do i = 1,3
         read(lutop,*) card
      end do
!
      do while(0.ne.1) 
         read(lutop,'(a256)',END=10) card              ! header
         call sdecode(card,words,param,nword,npar)
!
         if(npar.eq.2) then   
           ! 
           ! This is an elevation contour line
           !
            ztopo = param(1)
            i_top = INT(param(2))
            n_top = n_top + i_top
!
            read(lutop,'(a256)',END=10) card              ! get the number of columns is the block
            call sdecode(card,words,param,nword,npar)
            ncol = npar/2
            backspace(lutop)
!
            do i = 1,i_top/ncol    
              read(lutop,*) rvoid  
            end do
            if(mod(i_top,ncol).ne.0) read(lutop,*) rvoid  
!
         else if(npar.eq.3) then
           !
           ! This is a zo contour line
           ! 
           zo        = param(1) 
           i_zo      = INT(param(3))
           ncurva_zo = ncurva_zo + 1
           n_zo      = n_zo + i_zo
!
            read(lutop,'(a256)',END=10) card              ! get the number of columns is the block
            call sdecode(card,words,param,nword,npar)
            ncol = npar/2
            backspace(lutop)
!
            do i = 1,i_zo/ncol    
              read(lutop,*) rvoid  
            end do
            if(mod(i_zo,ncol).ne.0) read(lutop,*) rvoid 
!
         else if(npar.eq.4) then
           ! 
           ! This is a mixed elevation and contour contour line
           !
            ztopo = param(1)
            zo    = param(2)
            i_top = INT(param(4))
            i_zo  = INT(param(4))
            n_top = n_top + i_top
            ncurva_zo = ncurva_zo + 1
            n_zo  = n_zo + i_zo
!
            read(lutop,'(a256)',END=10) card              ! get the number of columns is the block
            call sdecode(card,words,param,nword,npar)
            ncol = npar/2
            backspace(lutop)
!
            do i = 1,i_top/ncol    
              read(lutop,*) rvoid  
            end do
            if(mod(i_top,ncol).ne.0) read(lutop,*) rvoid  
!
         else
!
           call runend('Incorrect number of parameters in the map file')
!
         end if
!
      end do
!
  10  continue
!
!***  Allocates memory
!
      allocate(x_top(n_top))
      allocate(y_top(n_top))
      allocate(z_top(n_top))
!
      allocate(curva_zo(ncurva_zo))
      allocate(x_zo(n_zo))
      allocate(y_zo(n_zo))
!
!***  Reads again the file
!
      rewind(lutop)
      read(lutop,*) card
      do i = 1,3
         read(lutop,*) card
      end do
!  
      j_top = 0 
      j_zo  = 0
      ncurva_zo = 0
!
      do while(0.ne.1) 
         read(lutop,'(a256)',END=20) card              ! header
         call sdecode(card,words,param,nword,npar)
!
         if(npar.eq.2) then   
           ! 
           ! This is an elevation contour line
           !
            ztopo = param(1)
            i_top = INT(param(2))
!
            read(lutop,'(a256)',END=20) card              ! get the number of columns is the block
            call sdecode(card,words,param,nword,npar)
            ncol = npar/2
            backspace(lutop)
!
            do i = 1,i_top/ncol    
              read(lutop,*) (x_top(j_top+j),y_top(j_top+j),j=1,ncol)  
              z_top(j_top+1:j_top+ncol) = ztopo
              j_top = j_top + ncol 
            end do
            if(mod(i_top,ncol).ne.0) then
              read(lutop,*) (x_top(j_top+j),y_top(j_top+j),j=1,mod(i_top,ncol)) 
              z_top(j_top+1:j_top+mod(i_top,ncol)) = ztopo
              j_top = j_top + mod(i_top,ncol)
            end if
!
         else if(npar.eq.3.or.npar.eq.4) then
           if(npar.eq.3) then
           !
           ! This is a zo contour line
           ! 
           zo_left  = param(1)         ! left  line value
           zo_right = param(2)         ! right line value
           i_zo = INT(param(3))        ! number of points
           ncurva_zo = ncurva_zo + 1
           ipos = j_zo                 ! inedx
!
            read(lutop,'(a256)',END=20) card              ! get the number of columns is the block
            call sdecode(card,words,param,nword,npar)
            ncol = npar/2
            backspace(lutop)
!
            do i = 1,i_zo/ncol    
              read(lutop,*) (x_zo(j_zo+j),y_zo(j_zo+j),j=1,ncol)  
              j_zo = j_zo + ncol 
            end do
            if(mod(i_zo,ncol).ne.0) then
              read(lutop,*) (x_zo(j_zo+j),y_zo(j_zo+j),j=1,mod(i_zo,ncol)) 
              j_zo = j_zo + mod(i_zo,ncol)
            end if
!
          else   
            ! 
            ! This is a mixed elevation and zo contour line
            !
            ztopo = param(1)
            zo_left  = param(2)         ! left  line value
            zo_right = param(3)         ! right line value
            i_top = INT(param(4))
            i_zo  = INT(param(4)) 
            ncurva_zo = ncurva_zo + 1
            ipos = j_zo                 ! inedx
!
            read(lutop,'(a256)',END=20) card              ! get the number of columns is the block
            call sdecode(card,words,param,nword,npar)
            ncol = npar/2
            backspace(lutop)
!
            do i = 1,i_top/ncol    
              read(lutop,*) (x_top(j_top+j),y_top(j_top+j),j=1,ncol)  
              do j=1,ncol
                 x_zo(j_zo+j) = x_top(j_top+j)
                 y_zo(j_zo+j) = y_top(j_top+j)
              end do
              z_top(j_top+1:j_top+ncol) = ztopo
              j_top = j_top + ncol 
              j_zo  = j_zo  + ncol 
            end do
            if(mod(i_top,ncol).ne.0) then
              read(lutop,*) (x_top(j_top+j),y_top(j_top+j),j=1,mod(i_top,ncol)) 
              do j=1,mod(i_top,ncol)
                 x_zo(j_zo+j) = x_top(j_top+j)
                 y_zo(j_zo+j) = y_top(j_top+j)
              end do               
              z_top(j_top+1:j_top+mod(i_top,ncol)) = ztopo
              j_top = j_top + mod(i_top,ncol)
              j_zo  = j_zo  + mod(i_zo ,ncol)
            end if
           end if   ! npar=4
!
!***       Builds the structure curve
!
            curva_zo(ncurva_zo)%value_right = zo_right 
            curva_zo(ncurva_zo)%value_left  = zo_left 
            curva_zo(ncurva_zo)%ndime = 2_ip
            curva_zo(ncurva_zo)%nnode = 2_ip
            curva_zo(ncurva_zo)%mnodb = 2_ip
            curva_zo(ncurva_zo)%npoib = i_zo !-1   ! last point is repeated and closes the curve. Not considered
            curva_zo(ncurva_zo)%nboun = i_zo !-1
            allocate(curva_zo(ncurva_zo)%lnodb( curva_zo(ncurva_zo)%mnodb, curva_zo(ncurva_zo)%nboun) )
            allocate(curva_zo(ncurva_zo)%coord( curva_zo(ncurva_zo)%ndime, curva_zo(ncurva_zo)%npoib) )
!
!***        Coordinates of the points
!
            do i = 1,curva_zo(ncurva_zo)%npoib
               curva_zo(ncurva_zo)%coord(1,i) = x_zo(ipos+i)
               curva_zo(ncurva_zo)%coord(2,i) = y_zo(ipos+i)
            end do
!
!***        Sense of rotation. Read from the input file. It is assumed that the sense
!***        is the same in all the contours of the file     
!
            if(TRIM(roughness_contours).eq.'clockwise') then
               curva_zo(ncurva_zo)%clockwise = .true.
            else
               curva_zo(ncurva_zo)%clockwise = .false.
            end if
!
!***        Nodal conectivities. Conectivities are build always anticlockwise because of requirements
!***        of the routine kdtree, used later to find if a point lays inside the curve 
!
           if(curva_zo(ncurva_zo)%clockwise) then
                j = 0
                do i = curva_zo(ncurva_zo)%nboun-1,1,-1
                   j = j +1
                   curva_zo(ncurva_zo)%lnodb(1,j) = i + 1 
                   curva_zo(ncurva_zo)%lnodb(2,j) = i 
                end do
                curva_zo(ncurva_zo)%lnodb(1,curva_zo(ncurva_zo)%nboun) = 1
                curva_zo(ncurva_zo)%lnodb(2,curva_zo(ncurva_zo)%nboun) = curva_zo(ncurva_zo)%nboun  
            else
                do i = 1,curva_zo(ncurva_zo)%nboun - 1
                   curva_zo(ncurva_zo)%lnodb(1,i) = i
                   curva_zo(ncurva_zo)%lnodb(2,i) = i + 1
                end do
                curva_zo(ncurva_zo)%lnodb(1,curva_zo(ncurva_zo)%nboun) = curva_zo(ncurva_zo)%nboun
                curva_zo(ncurva_zo)%lnodb(2,curva_zo(ncurva_zo)%nboun) = 1
            end if
!
        end if   ! npar
!
      end do
!
  20  continue
!
!***  Closes
!
      close(lutop)
!
!***  Writes information
! 
      write(lulog,110) n_top, & 
                       minval(x_top),maxval(x_top), & 
                       minval(y_top),maxval(y_top), & 
                       minval(z_top),maxval(z_top)
 110  format('Topography points',/, &
             'npoin  = ',4x,i8,/, &
             'xmin   = ',f12.1,/, &
             'xmax   = ',f12.1,/, &
             'ymin   = ',f12.1,/, &
             'ymax   = ',f12.1,/, &
             'zmin   = ',f12.1,/, &
             'zmax   = ',f12.1)
!
      if(n_zo.eq.0) then
        write(lulog,120) n_zo
 120    format('Roughness points'  ,/, &
               'np    = ',4x,i8)
      else
        write(lulog,121) n_zo, ncurva_zo, & 
                       minval(x_zo),maxval(x_zo), & 
                       minval(y_zo),maxval(y_zo)
 121    format('Roughness points'  ,/, &
               'npoin     = ',4x,i8   ,/, &
               'n contour = ',4x,i8   ,/, &
               'xmin      = ',f12.1,/, &
               'xmax      = ',f12.1,/, &
               'ymin      = ',f12.1,/, &
               'ymax      = ',f12.1)
      end if
!
!***  Checks that topography covers the transition zone (it is not necessary to cover the buffer zone)
!
      if(check) then
        if(minval(x_top).gt.tran%xi) call runend('readmap: transition zone not in topograpghy file for xmin')
        if(minval(y_top).gt.tran%yi) call runend('readmap: transition zone not in topograpghy file for ymin')
        if(maxval(x_top).lt.tran%xf) call runend('readmap: transition zone not in topograpghy file for xmax')
        if(maxval(y_top).lt.tran%yf) call runend('readmap: transition zone not in topograpghy file for ymax')
      end if
!
!***  Checks if there are repeated points for z. Duplicated points with same coordinates cause problems in the
!***  interpolation algorithms and mush be eliminated
!
      write(lulog,102)
      if(out_screen) write(*,102)
  102 format(/,'---> Checking for duplicated points in the topography file...',/)
!
      call check_duplicate_pts(x_top,y_top,z_top,n_top,n_dup_z)
      write(lulog,103) n_dup_z

  103 format('Number z  of duplicated points (eliminated)  ',i6)
!
      return
      end subroutine readmap
!
!
!
      subroutine check_duplicate_pts(x_top,y_top,z_top,n_top,n_dup)
!******************************************************************
!*
!*    This routine checks (and remove) the number of duplicated
!*    points n_dup.
!*    Note that this tolerance must be lowr than the one used to
!*    to duplicate lines of zo
!*
!*****************************************************************
      use KindType
      use Math
      implicit none
!
      integer(ip) :: n_top,n_dup
      real(rp)    :: x_top(n_top), y_top(n_top), z_top(n_top)
!
      logical     :: go_on
      integer(ip) :: i,j,id,id1,id2,ii, k,l
      real(rp)    :: dtol
      integer(ip), allocatable  :: id_nei(:)
      real(rp)   , allocatable  :: dist(:), auxve(:,:)
!
!***  Initializations
!

      dtol = 0.001              !  Tolerance distance (in m)
      n_dup = 0                 !  Number of duplicate points
      allocate(dist(n_top))
      allocate(id_nei(n_top))
      allocate(auxve(n_top,4))
!
!***  Computes the distance and initializes id_nei
!
 
      do i = 1,n_top
         dist(i) = sqrt((x_top(1)-x_top(i))*(x_top(1)-x_top(i)) + (y_top(1)-y_top(i))*(y_top(1)-y_top(i)))
         id_nei(i) = i
      end do 

!
!***  Order points by increasing distance to point 1
!
      call rank_array (dist,id_nei,n_top) 
!
!***  Checks candidates to duplicity. If a duplicated point is found it is cancelled
!
! orders array      
      do i =1, n_top
         k= id_nei(i) 
         auxve(i,1) =  x_top(k)
         auxve(i,2) =  y_top(k)
         auxve(i,3) =  z_top(k)        
         auxve(i,4) =  dist(k)        
      end do
      do i =1, n_top     
         x_top(i) = auxve(i,1)
         y_top(i) = auxve(i,2)
         z_top(i) = auxve(i,3)
         dist(i) =  auxve(i,4)
      end do

      go_on = .true.
      i = 0  
      do while(go_on) 

         i   = i + 1
         id1 = i
         id2 = i+1
         !
         if(dist (id2)-dist(id1).lt.dtol) then
            if( abs(x_top(id1)-x_top(id2)).lt.dtol .and. &
                 abs(y_top(id1)-y_top(id2)).lt.dtol ) then
               !
               !            I am a duplicated point
               !             
       
               id_nei(n_dup+1) = id2 ! stores duplicated point
               !
               n_dup = n_dup + 1              
               n_top = n_top - 1
 
            end if
         end if

         if(i.eq.n_top+n_dup-1) go_on = .false.
        
      end do

      ! eliminates duplicate points from array
      j = 0 
         
      do i =1, n_top 
         if (id_nei(j+1).eq.(i+j))  then
            j = j+1
            if (id_nei(j+1).eq.(i+j))  then
               j = j+1
               if (id_nei(j+1).eq.(i+j))  then
                  j = j+1         
               end if
            end if
         end if          

         x_top(i) = x_top(i+j)
         y_top(i) = y_top(i+j)
         z_top(i) = z_top(i+j)         
      end do       
          
!
     deallocate(dist)
     deallocate(id_nei)
     deallocate(auxve)

!
     return
     end subroutine check_duplicate_pts
         
