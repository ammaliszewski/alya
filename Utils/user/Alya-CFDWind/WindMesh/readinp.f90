  subroutine readinp
  !********************************************************
  !*
  !*    Reads the properties from the input file
  !*
  !********************************************************
  use KindType
  use InpOut
  use Master
  use Coordinates
  implicit none
  !
  integer(ip)           :: istat,ivoid(1),i
  real   (rp)           :: rvoid(1)
  character(len=s_mess) :: message,cvoid
  !
  real(rp), parameter   :: pi = 4.0_rp*atan(1.0_rp)
  !
  !*** Writes to the log file
  !
  write(lulog,1)
  if(out_screen) write(*,1)
1 format(/,'---> Reading input data file...',/)
  !
  !***  Read the COORDINATES block
  !
  call get_input_cha(finp,'COORDINATES','utm_zone',utmzone,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  !
  call get_input_cha(finp,'COORDINATES','utm_datum',utmdatum,1,istat,message)
  if(istat.ne.0) then 
     utmdatum = 'ED_50'
  end if
  !
  if(TRIM(utmdatum).eq.'CLARKE_1866') then
     idatum = 0
  else if(TRIM(utmdatum).eq.'GRS_80') then
     idatum = 1
  else if(TRIM(utmdatum).eq.'WGS_84') then
     idatum = 2
  else if(TRIM(utmdatum).eq.'NAD_83') then
     idatum = 3
  else if(TRIM(utmdatum).eq.'AIRY_1830') then
     idatum = 4
  else if(TRIM(utmdatum).eq.'INTERNATIONAL_1924') then
     idatum = 5
  else if(TRIM(utmdatum).eq.'ED_50') then
     idatum = 5
  else
     idatum = 2
  end if
  !
  call get_input_rea(finp,'COORDINATES','utm_x_ref',rvoid,1,istat,message)
  if(istat.ne.0) then
    utm_x_ref = 0.0
  else
    utm_x_ref = rvoid(1)
  end if
  !
  call get_input_rea(finp,'COORDINATES','utm_y_ref',rvoid,1,istat,message)
  if(istat.ne.0) then
    utm_y_ref = 0.0
  else
    utm_y_ref = rvoid(1)
  end if
  !
  !***  Coordinates of the farm (Inner) zone
  !
  call get_input_rea(finp,'FARM_ZONE','xmin',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  farm%xi = rvoid(1)
  !
  call get_input_rea(finp,'FARM_ZONE','xmax',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  farm%xf = rvoid(1)
  !
  call get_input_rea(finp,'FARM_ZONE','ymin',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  farm%yi = rvoid(1)
  !
  call get_input_rea(finp,'FARM_ZONE','ymax',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  farm%yf = rvoid(1)
  !
  call get_input_rea(finp,'FARM_ZONE','cell_size_x',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  farm%cell_size_x = rvoid(1)
  !
  call get_input_rea(finp,'FARM_ZONE','cell_size_y',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  farm%cell_size_y = rvoid(1)
  !
  !***  Transition zone (uniform size)
  !
  call get_input_rea(finp,'TRANSITION_ZONE','size_left_(m)',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  tran%xi = rvoid(1)
  !
  call get_input_rea(finp,'TRANSITION_ZONE','size_right_(m)',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  tran%xf = rvoid(1)
  !
  call get_input_rea(finp,'TRANSITION_ZONE','size_bottom_(m)',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  tran%yi = rvoid(1)
  !
  call get_input_rea(finp,'TRANSITION_ZONE','size_top_(m)',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  tran%yf = rvoid(1)
  !
  call get_input_rea(finp,'TRANSITION_ZONE','laplacian_mesh_smoothing',rvoid,1,istat,message)
  if(istat.eq.0) then
     if(TRIM(cvoid).eq.'YES'.or.TRIM(cvoid).eq.'yes') tran%laplacian_mesh_smoothing = .true.
  end if
  !
  tran%cell_size_x = -1    ! to be calculated later
  tran%cell_size_y = -1
  !
  !***  Buffer zone (circular, uniform size)
  !
  call get_input_rea(finp,'BUFFER_ZONE','size_(m)',rvoid,1,istat,message)
  if(istat.ne.0) then
     call get_input_rea(finp,'BUFFER_ZONE','size_left_(m)',rvoid,1,istat,message)   ! Keep compatibility with previos input file versions 3.0
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
  end if
  bufe%xi = rvoid(1)
  bufe%xf = rvoid(1)
  bufe%yi = rvoid(1)
  bufe%yf = rvoid(1)
  !
  call get_input_rea(finp,'BUFFER_ZONE','cell_size',rvoid,1,istat,message)
  if(istat.ne.0) then
     call get_input_rea(finp,'BUFFER_ZONE','cell_size_x',rvoid,1,istat,message) ! Keep compatibility with previos input file versions 3.0
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
  end if
  bufe%cell_size_x = rvoid(1)
  bufe%cell_size_y = rvoid(1)
  !
  call get_input_rea(finp,'BUFFER_ZONE','laplacian_mesh_smoothing',rvoid,1,istat,message)
  if(istat.eq.0) then
     if(TRIM(cvoid).eq.'YES'.or.TRIM(cvoid).eq.'yes') bufe%laplacian_mesh_smoothing = .true.
  end if
  !
  !***  Vertical
  !
  call get_input_rea(finp,'VERTICAL','ztop',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  ztop = rvoid(1) 
  !
  call get_input_rea(finp,'VERTICAL','first_cell_size',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  dz0 = rvoid(1) 
  !
  call get_input_cha(finp,'VERTICAL','cell_distribution',dz_distribution,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  do i = 1,LEN_TRIM(dz_distribution)
     call upcase(dz_distribution(i:i))
  end do
  !
  call get_input_rea(finp,'VERTICAL','number_of_cells',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  ncellz = INT(rvoid(1))
  dz0    = min(dz0,ztop/ncellz) ! this avoids possible user incosistent size and number of cells
  !
  !***  Topography
  !
  call get_input_cha(finp,'TOPOGRAPHY','topo_file',ftop,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  !
  call get_input_cha(finp,'TOPOGRAPHY','format_topo_file',topo_format,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  do i = 1,LEN_TRIM(topo_format)
     call upcase(topo_format(i:i))
  end do
  !
  if(TRIM(ftop).eq.'none'.or.TRIM(ftop).eq.'NONE') then    ! No topography file provided
     ftop        = 'NONE'
     topo_format = 'NONE'
  end if
  !
  smooth_topography = .false.
  call get_input_cha(finp,'TOPOGRAPHY','smooth_topography',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'YES') smooth_topography = .true.
  !
  niter_smooth = 1
  if(smooth_topography) then
     call get_input_rea(finp,'TOPOGRAPHY','number_of_iterations_smooth',rvoid,1,istat,message)
     if(istat.eq.0) niter_smooth = INT(rvoid(1))
  end if
  !
  rotate_mesh = .false.
  call get_input_cha(finp,'TOPOGRAPHY','rotate_mesh',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'YES') rotate_mesh = .true.
  !
  rotation_angle = 0.0_rp
  if(rotate_mesh) then
     call get_input_rea(finp,'TOPOGRAPHY','rotation_angle',rvoid,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
     rotation_angle = rvoid(1)
     rotation_angle = rotation_angle*pi/180.0_rp   ! convert to radians
  end if
  !
  !***  Roughness
  !
  call get_input_rea(finp,'ROUGHNESS','default_value',rvoid,1,istat,message)
  if(istat.gt.0) call wriwar(message)
  if(istat.lt.0) call runend(message)
  zo_def = rvoid(1)
  !
  call get_input_cha(finp,'ROUGHNESS','roughness_contours',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'CLOCKWISE') then
     roughness_contours = 'clockwise'
  else if(istat.eq.0.and.TRIM(cvoid).eq.'ANTICLOCKWISE') then
     roughness_contours = 'anticlockwise'
  else
     roughness_contours = 'clockwise'
  end if
  !
  call get_input_cha(finp,'ROUGHNESS','roughness_criterion',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'OUT-IN') then
     roughness_criterion = 'out-in'
  else if(istat.eq.0.and.TRIM(cvoid).eq.'LEFT-RIGHT') then
     roughness_criterion = 'left-right'
  else
     roughness_criterion = 'out-in'
  end if
  !
  call get_input_cha(finp,'ROUGHNESS','roughness_file',frou,1,istat,message)
  if(istat.ne.0) frou = 'NONE'
  !
  call get_input_cha(finp,'ROUGHNESS','format_roughness_file',rou_format,1,istat,message)
  if(istat.ne.0) then 
     rou_format = topo_format  ! default
  else
    do i = 1,LEN_TRIM(rou_format)
       call upcase(rou_format(i:i))
    end do
  end if
  !
  !***  Buffer
  !
  smooth_buffer = .true.
  call get_input_cha(finp,'BUFFER','smooth_buffer',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'NO') smooth_buffer = .false.  
  !
  call get_input_rea(finp,'BUFFER','elevation_buffer',rvoid,1,istat,message)
  if(istat.ne.0) then
     elevation_buffer = -99.
  else
     elevation_buffer = rvoid(1)
  endif
  !
  !***  witness points
  !
  call get_input_cha(finp,'WITNESS','witness_points',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'YES') witness_points = .true.
  !
  if(witness_points) then
     call get_input_cha(finp,'WITNESS','witness_points_file',fwit,1,istat,message)
     if(istat.gt.0) call wriwar(message)
     if(istat.lt.0) call runend(message)
  end if
  !
  !***  Output
  !
  call get_input_cha(finp,'OUTPUT','format_GiD',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'YES') out_gid = .true.
  !
  call get_input_cha(finp,'OUTPUT','format_vtk',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'YES') out_vtk = .true.
  !
  call get_input_cha(finp,'OUTPUT','format_alya',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'YES') out_alya = .true.
  !
  call get_input_cha(finp,'OUTPUT','format_open_foam',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'YES') out_foam = .true.
  !
  call get_input_cha(finp,'OUTPUT','format_stl',cvoid,1,istat,message)
  do i = 1,LEN_TRIM(cvoid)
     call upcase(cvoid(i:i))
  end do
  if(istat.eq.0.and.TRIM(cvoid).eq.'YES') out_stl = .true.
  !
  !***  Creates the required directories (if needed). Previous contents
  !***  are removed
  !
  if(out_gid ) call system('mkdir ' // trim(path) // 'Gid')
  if(out_vtk ) call system('mkdir ' // trim(path) // 'vtk')      
  if(out_alya) call system('mkdir ' // trim(path) // 'Alya')   
  if(out_foam) call system('mkdir ' // trim(path) // 'OpenFoam')  
  if(out_stl ) call system('mkdir ' // trim(path) // 'stl')   
  !
  !*** Calculates the coordinates of the zones (lecture gives the size)
  !
  ! Transition left
  !
  if( tran%xi .le. 0  ) then
     tran%xi = farm%xi
  else
     tran%xi = farm%xi - tran%xi
  end if
  !
  ! Transition right
  !
  if( tran%xf .le. 0  ) then
     tran%xf = farm%xf
  else
     tran%xf = farm%xf + tran%xf
  end if
  !
  ! Buffer left
  !
  if( bufe%xi .le. 0  ) then
     bufe%xi = tran%xi
  else
     bufe%xi = tran%xi - bufe%xi
  end if
  !
  ! Buffer right
  ! 
  if( bufe%xf .le. 0  ) then
     bufe%xf = tran%xf
  else
     bufe%xf = tran%xf + bufe%xf
  end if
  !
  ! Transition bottom
  !
  if( tran%yi .le. 0  ) then
     tran%yi = farm%yi
  else
     tran%yi = farm%yi - tran%yi
  end if
  !
  ! Transition top
  !
  if( tran%yf .le. 0  ) then
     tran%yf = farm%yf
  else
     tran%yf = farm%yf + tran%yf
  end if
  !
  ! Buffer bottom
  !
  if( bufe%yi .le. 0  ) then
     bufe%yi = tran%yi
  else
     bufe%yi = tran%yi - bufe%yi
  end if
  !
  ! Buffer top
  !
  if( bufe%yf .le. 0  ) then
     bufe%yf = tran%yf
  else
     bufe%yf = tran%yf + bufe%yf
  end if
  !
  !*** Performs some basic checks after reading
  !
  if(check) then
     !
     if(bufe%xi.ge.bufe%xf) call runend('readinp: xmin > xmax in the buffer zone')
     if(bufe%yi.ge.bufe%yf) call runend('readinp: ymin > ymax in the buffer zone')
     if(tran%xi.ge.tran%xf) call runend('readinp: xmin > xmax in the transition zone')
     if(tran%yi.ge.tran%yf) call runend('readinp: ymin > ymax in the transition zone')
     if(farm%xi.ge.farm%xf) call runend('readinp: xmin > xmax in the farm zone')
     if(farm%yi.ge.farm%yf) call runend('readinp: ymin > ymax in the farm zone')
     !
     if(farm%xi.le.tran%xi) call runend('readinp: farm zone is not inside the transition zone for xmin')
     if(farm%xf.ge.tran%xf) call runend('readinp: farm zone is not inside the transition zone for xmax')
     if(farm%yi.le.tran%yi) call runend('readinp: farm zone is not inside the transition zone for ymin')
     if(farm%yf.ge.tran%yf) call runend('readinp: farm zone is not inside the transition zone for ymax')
     !
     if(tran%xi.le.bufe%xi) call runend('readinp: transition zone is not inside the buffer zone for xmin')
     if(tran%xf.ge.bufe%xf) call runend('readinp: transition zone is not inside the buffer zone for xmax')
     if(tran%yi.le.bufe%yi) call runend('readinp: transition zone is not inside the buffer zone for ymin')
     if(tran%yf.ge.bufe%yf) call runend('readinp: transition zone is not inside the buffer zone for ymax')
     !
  end if
 !
 !*** Writes to the log file
 !
   write(lulog,10)  farm%xi,farm%xf,farm%yi,farm%yf,farm%cell_size_x,farm%cell_size_y  
10 format(&
       '----',/, &
       'MESH',/, &
       '----',/, &
       '  FARM_ZONE'       ,/, &
       '   xmin = ',f12.1,/, &
       '   xmax = ',f12.1,/, &
       '   ymin = ',f12.1,/, &
       '   ymax = ',f12.1,/, &
       '   cell_size_x = ',f9.1,/, & 
       '   cell_size_y = ',f9.1)

  !
   write(lulog,20)  tran%xi,tran%xf,tran%yi,tran%yf
20 format(' TRANSITION_ZONE'  ,/, &
       '   xmin = ',f12.1,/, &
       '   xmax = ',f12.1,/, &
       '   ymin = ',f12.1,/, &
       '   ymax = ',f12.1,/, &
       '   cell_size_x  = variable',/, & 
       '   cell_size_y  = variable' ) 
  !
   write(lulog,30) bufe%xi,bufe%xf,bufe%yi,bufe%yf,bufe%cell_size_x,bufe%cell_size_y 
30 format(' BUFFER_ZONE'       ,/, &
       '   xmin = ',f12.1,/, &
       '   xmax = ',f12.1,/, &
       '   ymin = ',f12.1,/, &
       '   ymax = ',f12.1,/, &
       '   cell_size_x  = ',f9.1,/, & 
       '   cell_size_y  = ',f9.1)
  !
   write(lulog,31) ztop,TRIM(dz_distribution),dz0,ncellz
31 format(' VERTICAL',/, &
       '   ztop = ',f9.2,/, & 
       '   cell_distribution = ',a,/, &
       '   first_cell_size = ',f9.4,/,&
       '   number_of_cells = ',i6,/)
   !
    write(lulog,40)  TRIM(ftop),TRIM(topo_format),zo_def 
40 format(&
       '--------',/, &
       'GEOMETRY',/, &
       '--------',/, &
       ' TOPOGRAPHY'                    ,/, &
       '   topography file        =  ',a,/, &
       '   topography file format =  ',a,/, &
       ' ROUGHNESS',/,&
       '   default value          =  ',f8.3)
 !
  if(smooth_buffer) then
      write(lulog,50) 
50    format(' BUFFER',/, &
             '   smooth_buffer         =  yes  ')
  else
      write(lulog,500) 
500   format(' BUFFER',/, &
             '   smooth_buffer         =  no  ')
   end if 
  !
   write(lulog,51)  elevation_buffer
51    format('   elevation_buffer      = ',f8.1)
  !
   write(lulog,60) 
60 format(' ',/)
  !
  return
  end subroutine readinp
