     subroutine setroug
!*************************************************************
!*  
!*    This subroutine interpolates surface roughness
!*
!*************************************************************
     use Master
     use InpOut
     use Kdtree
     implicit none
!
     integer(ip) :: ifoun,icurva_zo,ipoin,ielem
!
     type(netyp), pointer :: lnele(:)
     integer(ip)          :: bound
     real(rp)             :: bobox(3,2),xcoor(2),dista,norma(3),proje(3),chkdi 
!
     real(rp),allocatable :: dist_zo     (:)
     real(rp),allocatable :: dist_zo_elem(:)
!
!*** No real interpolation needed (defalt value asigned)
!
     if(n_zo.eq.0) return
!
!*** Writes to the log file
!
     write(lulog,1)
     if(out_screen) write(*,1)
  1  format(/,'---> Interpolating roughness to the surface mesh...')
! 
!*** Initializations
!
     chkdi   = 1d10
!
     allocate(dist_zo(npoin2d))
     dist_zo = -chkdi                   ! negative values
     allocate(dist_zo_elem(nelem2d))
     dist_zo_elem = -chkdi
!
!*** Loop over all the zo curves
!
     do icurva_zo = 1,ncurva_zo
!
!***    Initializes kd-tree structure forthe ith-zo curve
!
        call setkdtree(&
            1_ip,curva_zo(icurva_zo)%ndime,curva_zo(icurva_zo)%nnode,curva_zo(icurva_zo)%mnodb,curva_zo(icurva_zo)%npoib, & 
                 curva_zo(icurva_zo)%nboun,curva_zo(icurva_zo)%fabox,bobox                    ,curva_zo(icurva_zo)%coord, &
                 curva_zo(icurva_zo)%lnodb,curva_zo(icurva_zo)%sabox,curva_zo(icurva_zo)%blink,curva_zo(icurva_zo)%stru2, &
                 curva_zo(icurva_zo)%ldist,lnele)
!
!***    Loop over the mesh points. The buffer zone is included. If no points are found to interpolate, 
!***    the default value is assigned 
!
        do ipoin = 1,npoin2d
! 
           xcoor(1) = x2d(ipoin)
           xcoor(2) = y2d(ipoin)
!
           call dpopar(&
           1_ip,curva_zo(icurva_zo)%ndime,curva_zo(icurva_zo)%nnode,xcoor,curva_zo(icurva_zo)%sabox,curva_zo(icurva_zo)%blink, &
                curva_zo(icurva_zo)%npoib,curva_zo(icurva_zo)%mnodb      ,curva_zo(icurva_zo)%nboun,chkdi, &
                curva_zo(icurva_zo)%lnodb,curva_zo(icurva_zo)%coord,lnele,curva_zo(icurva_zo)%fabox,curva_zo(icurva_zo)%stru2, &
                curva_zo(icurva_zo)%ldist,dista,norma,proje,bound)
!
           if(dista.lt.0) then 
!
!***          Point inside the line. Find the closest (less negative, i.e. greater) value
!
              if(dista.gt.dist_zo(ipoin)) then
                 dist_zo(ipoin) = dista
                 if(TRIM(roughness_criterion).eq.'out-in') then
                    zo2d(ipoin) = curva_zo(icurva_zo)%value_right     ! inner value always
                 else
                   if(curva_zo(icurva_zo)%clockwise) then
                      zo2d(ipoin) = curva_zo(icurva_zo)%value_right
                   else
                     zo2d(ipoin) = curva_zo(icurva_zo)%value_left
                   end if
                 end if
              end if
           end if
!
       end do
!
     end do ! icurva_zo = 1,ncurva_zo
!
!***   Remove the buffer points from the interpolation
!***   This should change in the future when Alya allows for
!***   variable z_o at inflow
!
     do ipoin = 1,npoin2d
        if(my_zone(ipoin).eq.0)  zo2d(ipoin) = zo_def
     end do
     do ielem = 1,nelem2d
        if(my_zone_elem2d(ielem).eq.0) zo2d_elem(ielem) = zo_def
     end do
!
!*** Release memory
!
     deallocate(dist_zo)
     deallocate(dist_zo_elem)
!
     return
     end subroutine setroug
