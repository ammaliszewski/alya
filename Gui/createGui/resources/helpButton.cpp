void <<className>>::on_<<lineInputName>>HelpButton_clicked()
{
    QMessageBox::information(this,"<<lineInputName>>", <<helpText>>);
}
