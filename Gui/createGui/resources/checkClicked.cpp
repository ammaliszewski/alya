void <<className>>::on_<<lineInputName>>CheckBox_toggled(bool checked)
{
    if (checked) {
        ui-><<frameName>>->show();
    }
    else {
        ui-><<frameName>>->hide();
    }

}

