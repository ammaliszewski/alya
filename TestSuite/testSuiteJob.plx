#!/usr/bin/perl

#usage modules
use FindBin;                 # locate this script
use lib "$FindBin::Bin/libraries";  # use the parent directory
use File::Copy;
use File::Path;
use File::Find;
use File::Compare;
use File::Copy::Recursive qw(dirmove);
use File::Copy::Recursive qw(dircopy);
use XML::Simple;
use Data::Dumper;
use POSIX;
use ALYA::ExecFactory;
use ALYA::Reports;
use Archive::Tar;
use Net::SMTP::SSL;
use MIME::Lite;

#global variables
$basePath = "modules/";
my $reportDir = "rep";
$alyaExec = "~/alya/Executables/unix/Alya.x";
$testId = "";
$columnCompared = 6;

#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------


#----------------------------------------------------------------------
#--Function configTestSuite
#--Description:
#----Creates the testSuite xml configuration file from the testSuiteJob xml configuration
#--Parameters:
#----xml: pointer to the utility to create an xml from a perl hash
#----data: Pointer to a var that contains the testSuiteJob xml configuration
#----filePath: The path where the testSuite configuration file has to be written
sub configureTestSuite {
        local($xml, $data, $filePath) = @_;

        my $config = {};
	$config->{testSuite} = {};
	$config->{testSuite}->{common}->{testSuiteName} = "ALYA";
	$config->{testSuite}->{common}->{executable} = "../../../../../Executables/unix/Alya.x";
	$config->{testSuite}->{common}->{alya2pos} = "../../../../../Utils/user/alya2pos/alya2pos.x";
	$execModule = $data->{execModule};
	if (!$execModule) {
		$execModule = "LocalExec";
	}
	$config->{testSuite}->{common}->{execModule} = $execModule;
	$config->{testSuite}->{modules}->{module} = [];
        my $modules = $config->{testSuite}->{modules};

	$modulesTest = $data->{configure}->{module};
	@tests = $modulesTest;
	if (ref $modulesTest eq 'ARRAY') {
		@tests = @{$modulesTest};
	}	
	#creates the configure command
	foreach $test (@tests) {
		$module = {};
                $module->{moduleName} = $test;
                push(@{$modules->{module}}, $module);
	}

		
	$xml->XMLout(
	    $config,
	    KeepRoot => 1,
	    NoAttr => 1,
	    OutputFile => $filePath,
	);

}


#----------------------------------------------------------------------
#--Function checkOutDone
#--Description:
#----Returns true if checkout to alya svn repos was successfull and false otherwise
#--Parameters:
#----svnName: The name of the folder where the checkout was done
sub checkOutDone {
        $checkOutDone = 0;
	local($svnName) = @_;

	if (-d "$svnName/Executables" && -d "$svnName/Sources" && -d "$svnName/TestSuite" && -d "$svnName/Thirdparties") {
		$checkOutDone = 1;
	}

	return $checkOutDone;
}


#----------------------------------------------------------------------
#--Function checkMetisCompilation
#--Description:
#----Returns true if metis compilation was successfull and false otherwise
#--Parameters:
#----metisPath: The path where metis was compilated
sub checkMetisCompilation {
	local($metisPath) = @_;
        $metisCompiled = 0;

	if (-f "$metisPath/pmetis") {
		$metisCompiled = 1;
	}

	return $metisCompiled;
}


#----------------------------------------------------------------------
#--Function configureDone
#--Description:
#----Returns true if the configure operation has succesfully terminated, false otherwise
#--Parameters:
#----confPath: The path where configure was done
sub configureDone {
	local($confPath) = @_;
        $configureDone = 0;
	
	print "CONF PATH:" . "$confPath/makefile\n";
	if (-f $confPath . "makefile") {
		$configureDone = 1;
	}

	return $configureDone;
}


#----------------------------------------------------------------------
#--Function makeDone
#--Description:
#----returns true if the make Alya operation was done succesfully, false otherwise
#--Parameters:
#----makePath: The path where make was done
sub makeDone {
	local($makePath) = @_;
        $makeDone = 0;

	if (-f $makePath . "Alya.x") {
		$makeDone = 1;
	}

	return $makeDone;
}


#----------------------------------------------------------------------
#--Function alya2posDone
#--Description:
#----returns true if the alya2pos compilation was successfully done, false otherwise
#--Parameters:
#----alya2posPath: The path were alya2pos was compiled
sub alya2posDone {
	local($alya2posPath) = @_;
        $alya2posDone = 0;

	if (-f "$alya2posPath/alya2pos.x") {
		$alya2posDone = 1;
	}

	return $alya2posDone;
}


#----------------------------------------------------------------------
#--Function testSuiteDone
#--Description:
#----Returns true if the testSuite execution was successfull, false otherwise
#--Parameters:
#----testSuitePath: The path were the testSuite was launched
sub testSuiteDone {
	local($testSuitePath) = @_;
        $testSuiteDone = 0;

	if (-f "$testSuitePath/index.html") {
		$testSuiteDone = 1;
	}

	return $testSuiteDone;
}


#----------------------------------------------------------------------
#--Function tarReport
#--Description:
#----creates a tar.gz file
#--Parameters:
#----dirName: The folder name that has to be compressed
#----archive: The name of the tar.gz file to generate
sub tarReport {
	local($dirName, $archive) = @_;

	# Create inventory of files & directories
	#my @inventory = ();
	#print "DIR:" . $dir;
	#find (sub { push @inventory, $File::Find::name }, $dir);

	# Create a new tar object
	#my $tar = Archive::Tar->new();
	#print "Inventori:" . Dumper($inventory);
	#$tar->add_files( @inventory );

	# Write compressed tar file
	#$tar->write( $archive, 9 );
	$result = `tar -cvzf $archive $dirName`;
}


#----------------------------------------------------------------------
#--Function sendEmail
#--Description:
#----sends the general report email to the people specified in the email xml configuration file
#--Parameters:
#----mailConf: Pointer to a var that contins the email configuration, the from and the to
#----subject: The email subject
#----svnName: The name of the report to send, with this var the function tries to send a .tar.gz file with the report
#----message: the email message
#----sendOutput: Boolean var, if true the testSuiteJob output is send as an attached file
sub sendEmail {
	local($mailConf, $subject, $svnName, $message, $sendOutput) = @_;
	$fileName = "$svnName.tar.gz";
	$path = "$svnName.tar.gz";

	#Services
	$mailsTo = $mailConf->{mailTo};
	@emailsTo = $mailsTo;
	if (ref $mailsTo eq 'ARRAY') {
		@emailsTo = @{$mailsTo};
	}	
	#creates the configure command
	foreach $mailTo (@emailsTo) {
		print "SENDING EMAIL:" . $mailTo . "\n";
		$msg = MIME::Lite->new(
				 From    => $mailConf->{mailFrom},
				 To      => $mailTo,
				 Subject => $subject,
				 Type    =>'multipart/mixed'
				 );
		$msg->attach(Type     =>'TEXT',
				 Data     =>$message
				 );
		if (-f $path) {
			$msg->attach(Type     =>'application/x-gzip',
				 Path     => $path,
				 Filename => $fileName,
				 Disposition => 'attachment'
				 );
		}
		
		#if ($sendOutput) {
		#	$result = `tar -cvzf output.tar.gz output.out`;
		#	$msg->attach(Type     =>'application/x-gzip',
		#		 Path     => 'output.tar.gz',
		#		 Filename => 'output.tar.gz',
		#		 Disposition => 'attachment'
		#		 );
		#}

		my $smtps = Net::SMTP::SSL->new($mailConf->{mailServer}, 
				               Port => $mailConf->{mailPort},
					       Debug => 1
				               ) or print "$!\n"; 
		# authenticate
		defined ($smtps->auth($mailConf->{mailUser}, $mailConf->{mailPassword}))
		    or die "Can't authenticate: $!\n";
		# send preliminary data
		$smtps->mail("$mailConf->{mailFrom}\n");
		$smtps->to("$mailTo\n");
		$smtps->data();
		#send header
		$smtps->datasend($msg->as_string());
		$smtps->dataend();
		$smtps->quit;
	}
}

#----------------------------------------------------------------------
#--Function senOneEmail
#--Description:
#----Sends only one email to the specified email direction, message and subject
#--Parameters:
#----mailConf: Pointer to a var that contains the email configuration.
#----mailTo: the email direction
#----subject: The email subject
#----message: the email message
sub sendOneEmail {
	local($mailConf, $mailTo, $subject, $message) = @_;

	print "SENDING EMAIL:" . $mailTo . "\n";
	$msg = MIME::Lite->new(
			 From    => $mailConf->{mailFrom},
			 To      => $mailTo,
			 Subject => $subject,
			 Type    =>'multipart/mixed'
			 );
	$msg->attach(Type     =>'TEXT',
		     Data     =>$message
	             );
	
	my $smtps = Net::SMTP::SSL->new($mailConf->{mailServer}, 
				        Port => $mailConf->{mailPort},
					Debug => 1
				        ) or print "$!\n"; 
	# authenticate
	defined ($smtps->auth($mailConf->{mailUser}, $mailConf->{mailPassword}))
	    or die "Can't authenticate: $!\n";
	# send preliminary data
	$smtps->mail("$mailConf->{mailFrom}\n");
	$smtps->to("$mailTo\n");
	$smtps->data();
	#send header
	$smtps->datasend($msg->as_string());
	$smtps->dataend();
	$smtps->quit;
}


#----------------------------------------------------------------------
#--Function managersEmail
#--Description:
#----Sends emails to the managers according to the manager's xml configuration data
#--Parameters:
#----mail: email configuration data
#----managers: managers configuration data: relationship between modules and manager
#----subject: The email subject
#----svnName: The name of the testSuiteJob report folder
#----tSuiteName: The name of the testSuite report folder
#----reportFile: The name of the testSuiteJob result xml file
sub managersEmail {
	local($mail, $managers, $subject, $svnName, $tSuiteName, $reportFile) = @_;
	
	#get each testSuite manager
	$managersList = $managers->{manager};
	@managerList = $managersList;
	if (ref $managersList eq 'ARRAY') {
		@managerList = @{$managersList};
	}	

	#put all the managerTest in one hash
	my $managerTests = {};
	foreach $manager (@managerList) {
		$managerTestsList = $manager->{managerTest};
		@managerTestList = $managerTestsList;
		if (ref $managerTestsList eq 'ARRAY') {
			@managerTestList = @{$managerTestsList};
		}
		@{$managerTests}{@managerTestList}=();
	}	
	print "Manager Tests:" . Dumper($managerTests) . "\n";
	#for each manager creates its summary report
	foreach $manager (@managerList) {
		print "manager name:" . $manager->{managerName} . "\n";
		$summary = ALYA::Reports::doEmailReportManager("$svnName/TestSuite/$tSuiteName", "testSuiteResult.xml", $reportFile, $manager, $managerTests);
		#The summary is only generated if the manager has one of its modules with erros.
		if ($summary) {
			print "sumariing manager name:" . $manager->{managerMail} . "\n";
			sendOneEmail($mail, $manager->{managerMail}, "[AlyaTestSuite][manager]$svnName", $summary);
		}
	}

}


#----------------------------------------------------------------------
#--Function sshCopy
#--Description:
#----Function used to copy the report to a remote machine using ssh
#--Parameters:
#----sshConf:Pointer to the ssh configuration xml data.
sub sshCopy {
	local($sshConf) = @_;
	if ($sshConf) {
		if (-d $reportDir) {
		   #$output = `ssh $sshConf->{sshUser}@$sshConf->{sshHost} rm -R $sshConf->{sshFolder}/report_*`;
		   $output = `scp -r $reportDir/* $sshConf->{sshUser}\@$sshConf->{sshHost}:$sshConf->{sshFolder}`;
		}
	}
}


#----------------------------------------------------------------------
#--Function saveError
#--Description:
#----Saves to a file the 30 last lines of a given output
#--Parameters:
#----path:The path where to save the file
#----test:The name of the file to save
#----stdout: The content to save into the file
sub saveError {
	local($path, $test, $stdout) = @_;

		
	my @lines = split(/\n/, $stdout);
	my $numLines = scalar (@lines);
	my $finalLines = join("\n", @lines[($numLines-30) .. ($numLines-1)]);

	#save to disc the output
	open FILE, ">$path/$test.txt" or die $!;
	print FILE $finalLines;
	close FILE;

	return "$test.txt";

}


#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

#xml data
$xml = new XML::Simple;
$data = $xml->XMLin("testSuiteJob.xml");
$svnURL = $data->{svnURL};
$password = $data->{password};

#report xml initialization
my $svnName = ALYA::Reports::generateReportName();
my $report = {};
$report->{testSuiteJobResult} = {};
$report->{testSuiteJobResult}->{moduleCompilation}->{moduleCompilationResult} = [];
$report->{testSuiteJobResult}->{serviceCompilation}->{serviceCompilationResult} = [];
$moduleCompilation = $report->{testSuiteJobResult}->{moduleCompilation};
$serviceCompilation = $report->{testSuiteJobResult}->{serviceCompilation};
rmtree($svnName );
mkdir($svnName );
rmtree($reportDir );
mkdir($reportDir );

$errors = "No errors";

$execution = ALYA::ExecFactory->instantiate("LocalExec");

#1.checkout from svn
print "Checkout ALYA from repository\n";
$output = $execution->execCommand("svn co $svnURL --password $password $svnName" , 120);
if (!checkOutDone($svnName)) {
	$errors = "svn checkout failed";
}

#2.compile metis
if ($errors eq "No errors") {

	if ($data->{metisPath}) {
		print "Compile metis: $svnName/$data->{metisPath} \n";
		chdir "$svnName/$data->{metisPath}";
		$output = $execution->execCommand("make" , 120);
		chdir "../../../";		
	}

	if (!checkMetisCompilation("$svnName/$data->{metisPath}")) {
		$errors = "Metis not compiled correctly";	
	}

}

#3.configure and make each module and service one by one and log the result
if ($errors eq "No errors") {
	#exec the configure
	chdir "$svnName/Executables/unix";

	#Modules
	$modulesTest = $data->{configure}->{module};
	@tests = $modulesTest;
	if (ref $modulesTest eq 'ARRAY') {
		@tests = @{$modulesTest};
	}	
	#creates the configure command and executes make for each module
	foreach $test (@tests) {
		$confCommand = "./configure ";
		#log
		$result = {};
		$result->{moduleName} = $test;
		push(@{$moduleCompilation->{moduleCompilationResult}}, $result);
	
		#clean
		$stdout = $execution->execCommand("make clean" , 3600);
		#configure
		$confCommand = $confCommand . $test . " ";
		if ($data->{configure}->{file}) {
			$confCommand = $confCommand . "-f=" . $data->{configure}->{file};
		}
		$output = $execution->execCommand($confCommand , 120);
		if (!configureDone("")) {
			$result->{passed} = "false";
			$result->{error} = "configure not executed correctly";
		}
		else {
			#.make			
			$stdout = $execution->execCommand("make" , 3600);
			if (!makeDone("")) {
				$result->{passed} = "false";
				$result->{error} = "Alya compilation errors";
				$result->{file} = saveError("../../../$reportDir", $test, $stdout);
			}
			else {
				$result->{passed} = "true";
			}	
		}
	}
	#Services
	$modulesTest = $data->{configure}->{service};
	@tests = $modulesTest;
	if (ref $modulesTest eq 'ARRAY') {
		@tests = @{$modulesTest};
	}	
	#creates the configure command and executes make for each service
	foreach $test (@tests) {
		$confCommand = "./configure ";
		#log
		$result = {};
		$result->{serviceName} = $test;
		push(@{$serviceCompilation->{serviceCompilationResult}}, $result);
	
		#clean
		$stdout = $execution->execCommand("make clean" , 3600);
		#configure
		$confCommand = $confCommand . $test . " ";
		if ($data->{configure}->{file}) {
			$confCommand = $confCommand . "-f=" . $data->{configure}->{file};
		}
		print "ConfCommand:" . $confCommand . "\n";
		$output = $execution->execCommand($confCommand , 120);
		if (!configureDone("")) {
			$result->{passed} = "false";
			$result->{error} = "configure not executed correctly";
		}
		else {
			#.make			
			$stdout = $execution->execCommand("make" , 3600);
			if (!makeDone("")) {
				$result->{passed} = "false";
				$result->{error} = "Alya compilation errors";
				$result->{file} = saveError("../../../$reportDir", $test, $stdout);
			}
			else {
				$result->{passed} = "true";
			}	
		}
	}
}


#4.exec the configure only with modules and services that passed the compilation
if ($errors eq "No errors") {
        #clean
        $stdout = $execution->execCommand("make clean" , 3600);
	$confCommand = "./configure ";
	#Modules
	$modulesCompResult = $moduleCompilation->{moduleCompilationResult};
	@modCompResults = $modulesCompResult;
	if (ref $modulesCompResult eq 'ARRAY') {
		@modCompResults = @{$modulesCompResult};
	}	
	#creates the configure command
	foreach $modCompResult (@modCompResults) {
		if ($modCompResult->{passed} eq "true") {
			$confCommand = $confCommand . $modCompResult->{moduleName} . " ";
		}		
	}
	#Services
	$servicesCompResult = $serviceCompilation->{serviceCompilationResult};
	@serCompResults = $servicesCompResult;
	if (ref $servicesCompResult eq 'ARRAY') {
		@serCompResults = @{$servicesCompResult};
	}	
	#creates the configure command
	foreach $serCompResult (@serCompResults) {
		if ($serCompResult->{passed} eq "true") {
			$confCommand = $confCommand . $serCompResult->{serviceName} . " ";
		}	}
	if ($data->{configure}->{file}) {
		$confCommand = $confCommand . "-f=" . $data->{configure}->{file};
	}
	$output = $execution->execCommand($confCommand , 120, "../../../");

	if (!configureDone("")) {
		$errors = "configure not executed correctly";
		chdir "../../../";
	}
}

#5.make
if ($errors eq "No errors") {
	#log
	$result = {};
	$result->{moduleName} = "All";
	push(@{$moduleCompilation->{moduleCompilationResult}}, $result);

	#exec make
	$output = $execution->execCommand("make" , 3600, "../../../");
	if (!makeDone("")) {
		$errors = "Alya compilation errors";
		chdir "../../../";
		$result->{passed} = "false";
		$result->{error} = "Alya compilation errors";
		$result->{file} = saveError($reportDir, $test, $output);		
	}
	else {
		$result->{passed} = "true";
	}	
}

#6.compile alya2pos
if ($errors eq "No errors") {
	
	$output = `chmod +x alya2pos-compile.sh`;
	$output = $execution->execCommand("./alya2pos-compile.sh" , 3600);	
	chdir "../../../";
	if (!alya2posDone("$svnName/Utils/user/alya2pos")) {
		#$errors = "Alya2pos compilation errors";
		$report->{testSuiteJobResult}->{alya2posCompilation} = {};
		$report->{testSuiteJobResult}->{alya2posCompilation}->{passed} = "false";
		$report->{testSuiteJobResult}->{alya2posCompilation}->{error} = "Alya2pos compilation errors";
		$report->{testSuiteJobResult}->{alya2posCompilation}->{file} = saveError($reportDir, "alya2pos", $output);
	}	
}

#7.exec the testSuite
#Obtain testSuiteName
my $tSuiteName = ALYA::Reports::generateReportName();
if ($errors eq "No errors") {
	configureTestSuite($xml, $data, "$svnName/TestSuite/testSuite.xml");
	chdir "$svnName/TestSuite";
	$output = `chmod +x testSuite.plx`;
	$output = $execution->execCommand("./testSuite.plx" , 28800);
	chdir "../../";
	if (!testSuiteDone("$svnName/TestSuite/$tSuiteName")) {
		$errors = "TestSuite execution problems";
	}
}

#8.create the global report, joins compilation and testSuite report
#write the report
$xml->XMLout(
	$report,
    	KeepRoot => 1,
	NoAttr => 1,
	OutputFile => $reportDir . "/testSuiteJobResult.xml",
);

#Copy the entire testSuite report folder into reportdir
dircopy("$svnName/TestSuite/$tSuiteName", $reportDir);
ALYA::Reports::doGlobalReport("testSuiteResult.xml", $reportDir, "testSuiteJobResult.xml", $data->{sshReport});

#9.tar the report
tarReport($reportDir, "$svnName.tar.gz");

#10.Send the result
$message = "Execution errors:" . $errors . "\n";
$sendOutput = 0;
if ($errors eq "No errors") {	
	#Summarize the testSuite report
	$summary = ALYA::Reports::doEmailReport("$svnName/TestSuite/$tSuiteName", "testSuiteResult.xml", $reportDir."/testSuiteJobResult.xml", $data->{sshReport});
	$message = $summary;
}
else {
	$message = $message . "Output:\n";
	$message = $message . $output;
	$sendOutput = 1;
}

#10.1.General email
sendEmail($data->{mail}, "[AlyaTestSuite]$svnName", $svnName, $message, $sendOutput);
#10.2.Managers email
managersEmail($data->{mail}, $data->{managers}, "[AlyaTestSuite]$svnName", $svnName, $tSuiteName, $reportDir."/testSuiteJobResult.xml");

#11.sshcopy
sshCopy($data->{sshReport});

#12.remove the folders
#rmtree($svnName );

exit 0
