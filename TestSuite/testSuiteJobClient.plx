#!/usr/bin/perl

#usage modules
use FindBin;                 # locate this script
use lib "$FindBin::Bin/libraries";  # use the parent directory
use File::Copy;
use File::Path;
use File::Find;
use File::Compare;
use File::Copy::Recursive qw(dirmove);
use XML::Simple;
use Data::Dumper;
use POSIX;
use ALYA::Reports;
use ALYA::Utils;
use ALYA::Datawarehouse;
use Archive::Tar;
use ALYA::ExecFactory;
use Getopt::Std;


#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------



#----------------------------------------------------------------------
#--Function tarReport
#--Description:
#----creates a tar.gz file
#--Parameters:
#----dirName: The folder name that has to be compressed
#----archive: The name of the tar.gz file to generate
sub tarReport {
	local($dirName, $archive) = @_;
	$result = `tar -cvzf $archive $dirName`;
}

sub getSVNRevision {
	local($coOutput) = @_;
	$revision;
	if($coOutput =~ m/Revisión obtenida: (\d+)/) {
		$revision = $1;
	}
	elsif ($coOutput =~ m/Checked out revision (\d+)/) {
		$revision = $1;
	}

	return $revision;
}

#----------------------------------------------------------------------
#--Function saveError
#--Description:
#----Saves to a file the 30 last lines of a given output
#--Parameters:
#----path:The path where to save the file
#----test:The name of the file to save
#----stdout: The content to save into the file
sub saveError {
	local($path, $test, $stdout) = @_;

		
	my @lines = split(/\n/, $stdout);
	my $numLines = scalar (@lines);
	my $finalLines = join("\n", @lines[($numLines-30) .. ($numLines-1)]);

	#save to disc the output
	open FILE, ">$path/$test.txt" or die $!;
	print FILE $finalLines;
	close FILE;

	return "$test.txt";

}

#----------------------------------------------------------------------
#--Function generateReportName
#--Description:
#----Genereates the report file name, that includes the current time, the module name and the test name.
#--Parameters:
#----moduleName: The module's name that it's tested
#----testName: The test name.
sub generateReportName {
	#get local time in format: MMM_DD_YYYY
	$now_string = strftime "%b_%d_%M_%Y", localtime;

	#Compound the file name
	$fileName = "report_" . $now_string;

	return $fileName;
}
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

#argumentos opcionales -b(bigExecution)
getopts('b');
our($opt_b);
my $bigExecutionArg = $opt_b || "0";

#xml data
my $xml = new XML::Simple;
my $data = $xml->XMLin("testSuiteJob.xml");
my $host = $data->{host};
my $user = $data->{user};
my $bashJob = $data->{job};
if ($bigExecutionArg) {
	$bashJob = $data->{jobBig};
}
my $reportDir = $data->{reportDir};
my $reportFile = $data->{reportFile};
my $reportName = $data->{reportName};
my $execModule = $data->{execModule};
$svnURL = $data->{svnURL};
my $localSvnURL = $data->{localSvnURL};
$password = $data->{password};
my $svnRevision;


#$output = `ssh $user\@$host bqueues`;
#$repName = generateReportName();
#saveError("bqueues_reports", $test, $stdout);

if ($host ne "localhost") {
	
	$execution = ALYA::ExecFactory->instantiate($execModule);

	#1.copy configuration file to remote machine
	$output = `scp testSuiteJob.xml $user\@$host:/gpfs/projects/bsc21/WORK-TONI/alya/TestSuite 2>&1`;
	#check if there is no connection
	my $connection = 1;
	if ($output =~ m/Connection timed out/) {
		$connection = 0;
		print "Connection error:" . $output . "\n";
	}
	if ($connection) {
		#1.checkout from svn
		print "Checkout ALYA from repository\n";
		$output = `ssh $user\@$host rm -r -f /gpfs/projects/bsc21/WORK-TONI/alya/TestSuite/Report`;
		$output = `ssh $user\@$host mkdir /gpfs/projects/bsc21/WORK-TONI/alya/TestSuite/Report`;
		$output = `ssh $user\@$host svn co $svnURL --password $password /gpfs/projects/bsc21/WORK-TONI/alya/TestSuite/Report`;
		$svnRevision = getSVNRevision($output);
		#1.launch the job on the remote server
		$execution->execServerJob($user, $host, $bashJob);
		#2.get the report
		$execOutput = `scp $user\@$host:$reportDir/$reportFile .`;
	}
}
else {
	#1.checkout from svn
	print "Checkout ALYA from repository\n";
	$reportFolder = "Report";
    rmtree($reportFolder);
    mkdir($reportFolder);
	$output = `svn co $svnURL --password $password $reportFolder`;
	$svnRevision = getSVNRevision($output);	
    print "Start server execution\n";
	$execOutput = `./$bashJob > output.out`;
}

print "Svn revision:" . $svnRevision . "\n";
print "Start report file processing\n";
my $svnName = ALYA::Reports::generateReportName();
if (-f $reportFile) {
        print "Start untar report file\n";
	#5.untar de report
	$execOutput = `tar -xvzf $reportFile`;
	print "Untar output:$execOutput\n";
	if (-d $reportName) {
		#Enter in the directory
		chdir $reportName;
		#load errors
		open FILE, "errors.txt" or die "Couldn't open file: $!";
  		my $errors = do { local $/; <FILE> };
		close FILE;
		chomp($errors);
		#load output
		open FILE, "output.txt" or die "Couldn't open file: $!";
		my $output = do { local $/; <FILE> };
		close FILE;
		$message = "Execution errors:" . $errors . "\n";
		$sendOutput = 0;
		chdir "..";		
		print "errors:" . $errors . "#\n";
		if ($errors eq "No errors") {
			ALYA::Reports::doHTMLReport($reportName, "testSuiteResult.xml");
		}
		ALYA::Reports::doGlobalReport("testSuiteResult.xml", $reportName, "testSuiteJobResult.xml", $data->{sshReport}, $svnRevision, $data->{metaInfo});
		tarReport($reportName, "$svnName.tar.gz");
		if ($errors eq "No errors") {	
			#Summarize the testSuite report
			$summary = ALYA::Reports::doEmailReport($reportName, "testSuiteResult.xml", $reportName."/testSuiteJobResult.xml", "globalResult.xml", $data->{sshReport});
			$message = $summary;
		}
		else {
			$message = $message . "Output:\n";
			$message = $message . $output;
			$sendOutput = 1;
		}
		#9.4.General email
		ALYA::Utils::sendEmail($data->{mail}, "[AlyaTestSuite]$svnName", "$svnName.tar.gz", $message, $sendOutput);
		#9.5.Managers email
		ALYA::Utils::managersEmail($data->{mail}, $data->{managers}, "[AlyaTestSuite][manager]$svnName", $svnName, $reportName, $reportName."/testSuiteJobResult.xml");
		#9.6.Errors email
		ALYA::Utils::errorsEmail($data->{mail},"[AlyaTestSuite][error]$svnName", $svnName, $reportName, $reportName."/globalResult.xml", $svnRevision, $localSvnURL);
		
		#9.7.Store de report in the log folder, depending if its big o small test
		$execOutput = `mv $svnName.tar.gz log/`;
		print "Move report to log output:$execOutput\n";	
		
		#9.8.Store the report into the datawarehouse data base
		ALYA::Datawarehouse::resultsToBBDD ($reportName, "testSuiteResult.xml", $reportName."/testSuiteJobResult.xml", "globalResult.xml", $bigExecutionArg);
		
	}

} else {
	print "File report.tar.gz not found\n";
}

#9.6.sshcopy
ALYA::Utils::sshCopy($data->{sshReport}, $reportName);
rmtree($reportName);
unlink($reportFile);
unlink($svnName);

exit 0
