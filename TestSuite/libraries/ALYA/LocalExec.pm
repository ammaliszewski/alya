package ALYA::LocalExec;

#---------------------------------------------------------------------
#--Package that creates the html reports from the xml result----------
#---------------------------------------------------------------------

#usage modules
use FindBin;                 # locate this script
use lib "$FindBin::Bin/.";  # use the parent directory
use File::Copy;
use File::Path;
use File::Compare;
use File::Copy::Recursive qw(dircopy);
use XML::Simple;
use Data::Dumper;
use POSIX;

#global variables
$execOutput = "";

sub new { 
  my $class = shift; 
  my $self = { }; 
  bless $self, $class;
  return $self;
}

#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------


#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
sub execTest {
	local($self, $basePath, $origPath, $alyaExec, $testId, $timeout, $executionTest) = @_;
	my $execOutput;
	if (!$timeout) {
		$timeout = 0;
	}
	
	my $openMPThreads = $executionTest->{openMPThreads};
	my $mpiProcesses = $executionTest->{mpiProcesses};
	my $bigExecution = $executionTest->{bigExecution};
	my $couplingsTest = $executionTest->{coupling};
	if (!$openMPThreads) {
		$openMPThreads = 1;
	}
	
	chdir $basePath . "/tmp";
	print "Start execution with $mpiProcesses processors\n";
	eval {
		local $SIG{ALRM} = sub {die "alarm\n"};
		alarm $timeout;
		$execOutput =  ``;
		
		#CREATE THE COMMAND EXEC LINE
		my $commandExec = "";
		if ($couplingsTest) {
			#Iterate over the coupling modules to create the command line executable
			my @couplings = $couplingsTest;
			if (ref $couplingsTest eq 'ARRAY') {
				@couplings = @{$couplingsTest};
			}
			my $coupLength = @couplings;
			my $idx = 1;
			$commandExec = "mpirun ";
			foreach $couplingTest (@couplings) {
				my $problemName = $couplingTest->{problemName};
				my $problemParams = $couplingTest->{problemParams};
				$commandExec = $commandExec . "-np $mpiProcesses $alyaExec $problemName $problemParams";
				if ($idx < $coupLength) {
					$commandExec = $commandExec . " : ";
				}
				$idx = $idx + 1;
			}
		}
		else {
			$commandExec = "mpirun -np $mpiProcesses $alyaExec $testId";
		}
		print "CommandExec:" . $commandExec;
		#RUN THE COMMAND
		$execOutput =  `export OMP_NUM_THREADS=$openMPThreads; $commandExec >>test.out 2>&1`;
		open FILE, "test.out" or die "Couldn't open file: $!";
  		my $testOut = do { local $/; <FILE> };
		close FILE;
		$execOutput = $execOutput . "\n" . $testOut;
		print $execOutput;
		alarm 0;
	};

	if ($@) {
		die unless $@ eq "alarm\n";
		#kill the process
		system 'killall', 'mpirun';
		$execOutput = "timed out, it takes more than " . $timeout . " seconds\n" . $execOutput;
		open FILE, "test.out" or die "Couldn't open file: $!";
  		my $testOut = do { local $/; <FILE> };
		close FILE;
		$execOutput = $execOutput . "\n" . $testOut;
	}
	else {
		print "didn't time out\n";
	}
	chdir $origPath;
	return $execOutput;
}

sub execWithOpen {
	local($self, $basePath, $origPath, $command, $timeout) = @_;
	my $execOutput = "";
	if (!$timeout) {
		$timeout = 0;
	}
	chdir $basePath . "/tmp";
	eval {
		local $SIG{ALRM} = sub {die "alarm\n"};
		alarm $timeout;
		$pid = open(WRITEME, "| $command");
		print WRITEME "\n";
		close(WRITEME);
		alarm 0;
	};

	if ($@) {
		die unless $@ eq "alarm\n";
		#kill the process
		system 'kill', '-9' , $pid;
		$execOutput = "timed out, it takes more than " . $timeout . " seconds\n";
	}
	else {
		print "didn't time out\n";
	}
	chdir $origPath;

	return $execOutput;
}

sub execCommand {
	local($self, $command, $timeout) = @_;
	print "Executing command:" . $command . "\n";
	my $execOutput = "";
	if (!$timeout) {
		$timeout = 0;
	}
	$pid = "";
	eval {
		local $SIG{ALRM} = sub {die "alarm\n"};
		alarm $timeout;
		#$execOutput = `$command 2>error.out`;
		open FILE, ">>output.out" or die $!;
		$pid = open(COMM, "$command 2>&1 |");
		while ( defined(my $line = <COMM> ) )
		{
			chomp($line);
			print FILE $line . "\n";
		}
		close(COMM);
		close FILE;
		alarm 0;
		print $execOutput;
	};
	
	if ($@) {
		die unless $@ eq "alarm\n";		
		#kill the process
		system 'kill', '-9', $pid;
		close(COMM);
		close FILE;
		$execOutput = "timed out, it takes more than " . $timeout . " seconds\n";
	}
	else {
		print "didn't time out\n";
	}

	#loads the output into the var
	open FILE, "<output.out";
	$file_contents = do { local $/; <FILE> };
	close FILE;
	$execOutput = $execOutput . $file_contents;
	unlink "output.out";
	return $execOutput;
}


1;
