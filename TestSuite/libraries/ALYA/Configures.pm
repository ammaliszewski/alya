package ALYA::Configures;

#--------------------------------------------------------------------------------
#--Package that contains functions to create the distinct alya configures--------
#--------------------------------------------------------------------------------

#usage modules
use FindBin;                 # locate this script
use lib "$FindBin::Bin/.";  # use the parent directory
use File::Copy;
use File::Path;
use File::Compare;
use File::Copy::Recursive qw(dircopy);
use XML::Simple;
use Data::Dumper;
use POSIX;


#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------
my $response = "";
my $configOriginal = "";

sub checkChange {
	local($config, $line, $compilationID) = @_;
	
	if ($configOriginal eq $config) {
			$response = $response . "ERROR AL CREAR CONFIGURE $compilationId, no se ha modificado linea $line\n";
	}
	else {
			$configOriginal = $config;
	}		
}

#----------------------------------------------------------------------
#--Function createConfigure
#--Description:
#----create the specific configure
#--Parameters:
#----compilationId: Configure Identification
sub createConfigure {
	local($compilationId) = @_;
	
	#removes the actual configuration
	unlink "config.in";
	
	#load the base configure
    open FILE, "<configure.in/config_ifort.in" or die $!;
	my $config = do { local $/; <FILE> };
	close FILE;
	
	$configOriginal = $config;
	$response = "";
	
	#Modifies the base configure depending on the compilationId
	#--ENTEROS 8--------
	if ($compilationId eq "enteros8") {
		$config =~ s/\#CSALYA   := \$\(CSALYA\) -i8 -DI8 -m64/CSALYA   := \$\(CSALYA\) -i8 -DI8 -m64/g;
		checkChange($config,1,$compilationId);		
	#--MPI OFF---------	
    } elsif ($compilationId eq "mpi_off") {
		$config =~ s/F77      = mpif90/F77     = ifort/g;
		checkChange($config,1,$compilationId);
		
		$config =~ s/F90      = mpif90/F90     = ifort/g;
		checkChange($config,2,$compilationId);
		
		$config =~ s/FCOCC    = mpicc -c/FCOCC   = gcc -c/g;
		checkChange($config,3,$compilationId);
		
		$config =~ s/FOPT      = -O1/\#FOPT      = -O1/g;
		checkChange($config,4,$compilationId);
		
		$config =~ s/CSALYA     := \$\(CSALYA\) -DMETIS/\#CSALYA     := \$\(CSALYA\) -DMETIS/g;
		checkChange($config,5,$compilationId);
		
		$config =~ s/\#CSALYA    := \$\(CSALYA\) -DMPI_OFF/CSALYA    := \$\(CSALYA\) -DMPI_OFF/g;
		checkChange($config,6,$compilationId);
	#--OPENMP--	
    }  elsif ($compilationId eq "openmp") {
		$config =~ s/\#CSALYA    := \$\(CSALYA\) -openmp/CSALYA    := \$\(CSALYA\) -openmp/g;
		checkChange($config,1,$compilationId);
		
		$config =~ s/\#EXTRALIB   := \$\(EXTRALIB\) -openmp/EXTRALIB   := \$\(EXTRALIB\) -openmp/g;
		checkChange($config,2,$compilationId);
	#--CHECK--
    } elsif ($compilationId eq "check") {
		$config =~ s/\#CSALYA   := \$\(CSALYA\) -ftrapuv -check all/CSALYA   := \$\(CSALYA\) -ftrapuv -check all/g;
		checkChange($config,1,$compilationId);
		$config =~ s/FOPT      = -O1/\#FOPT      = -O1/g;
		checkChange($config,2,$compilationId);
	}  elsif ($compilationId eq "libple_cht") {
		
    } elsif ($compilationId eq "libple_cnt") {
		
    } elsif ($compilationId eq "iberdrola") {
		
    } elsif ($compilationId eq "standard") {
		
    } else {
		$response = $response . "ERROR, this compilationId:$compilationId is not defined\n";
	}
    
    #save the actual configure to a file
	open FILE, ">config.in" or die $!;
	print FILE $config;
	close FILE;
	
	return $response;
	
}



1;
