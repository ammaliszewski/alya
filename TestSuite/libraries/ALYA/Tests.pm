package ALYA::Tests;

#---------------------------------------------------------------------
#--Package that make one test for a given module----------------------
#---------------------------------------------------------------------

#usage modules
use FindBin;                 # locate this script
use lib "$FindBin::Bin/.";  # use the parent directory
use File::Copy;
use File::Path;
use File::Compare;
use XML::Simple;
use Data::Dumper;
use Text::Diff;
use POSIX;
use ALYA::ExecFactory;

#global variables
$basePath = "modules/";
$alyaExec = "~/alya/Executables/unix/Alya.x";
$testId = "";
$columnCompared = 6;
$reportDir = "report";

#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------

#----------------------------------------------------------------------
#--Function compareFiles
#--Description:
#----Makes comparisons between files and creates reports about the results
#--Parameters:
#----comparison: Array that contains the comparisons list to do and his properties
#----reportName: Then name of the report to generate
sub compareFiles {
	local($comparison, $reportName, $result, $processors) = @_;

	#compare each file
	@comparFile = $comparison;
	if (ref $comparison eq 'ARRAY') {
		@comparFile = @{$comparison};
	}	
	#iterate over each comparison
	foreach $compar (@comparFile) {
		my $process = 1;
		
		#for coupling check if this file if for this part of the coupling or not
		my $fromCouplingProblem = $compar->{fromProblem};
		if ($fromCouplingProblem) {
			if ($fromCouplingProblem ne $testId) {
				$process = 0;
			}
		}
		
		if ($process==1) {
			#log
			$compareLog = {};
			$compareLog->{fileName} = $testId . $compar->{file};
			push(@{$result->{file}}, $compareLog);
			#obtain files to compare
			if (-d $basePath . "/base/". $processors ."p") {
				$ficheroBase = $basePath . "/base/". $processors ."p/" . $testId . $compar->{file};
			}
			else {
				$ficheroBase = $basePath . "/base/1p/" . $testId . $compar->{file};
			}

			$ficheroComp = $basePath . "/tmp/" . $testId . $compar->{file};	

			#report the result
			if ($compar->{type} eq "diff") {
				compareByDiff($ficheroBase, $ficheroComp, $compareLog, $processors, $testId, $compar);
			}
			elsif ($compar->{type} eq "value") {
				compareByValue($ficheroBase, $ficheroComp, $compareLog, $processors, $testId, $compar);
			} 
	    }          		
	}
	          	
}

#----------------------------------------------------------------------
#--Function compareFilesByValue
#--Description:
#----Makes comparisons between files and creates reports about the results
#--Parameters:
#----comparison: Array that contains the comparisons list to do and his properties
#----reportName: Then name of the report to generate
sub compareByDiff {
	local($ficheroBase, $ficheroComp, $compareLog, $processors, $testId, $compar) = @_;
	
	if (compare($ficheroBase,$ficheroComp) == 0) {
		$compareLog->{passed} = true;
	}
	else {
		$compareLog->{passed} = false;
		#check if file exists
		unless (-e $ficheroComp) {
			$compareLog->{error} = "File have not been generated";
		}
		else {
			#make the diff between the files
			my $diff = diff $ficheroBase, $ficheroComp, { STYLE => "Context", CONTEXT=> "0" };
			$compareLog->{diff} = $diff;
			#save the file into the report folder
			copy($ficheroComp , $basePath . "/".$reportDir."/" . $processors . "p_".$testId .$compar->{file})
			    or die "copy failed: $!";
		}
	}
}

#----------------------------------------------------------------------
#--Function compareFilesByValue
#--Description:
#----Makes comparisons between files and creates reports about the results
#--Parameters:
#----comparison: Array that contains the comparisons list to do and his properties
#----reportName: Then name of the report to generate
sub compareByValue {
	local($ficheroBase, $ficheroComp, $compareLog, $processors, $testId, $compar) = @_;


	my $section = $compar->{section};
	my $columnToCompare = $compar->{column};
	my $compareListBase = [];
	my $compareListTest = [];

	$compareLog->{section} = $section;

    #----------------FILL SECTION-------------------------------------------
	#fill the comparison structures with the comparison items from the files
	#-----------------------------------------------------------------------
	if ($section eq "FINITE ELEMENT ERRORS") {
		#search for w(n,n) at start line
		#for each line we get each number and make the comparison
		open my $base, $ficheroBase or die "Could not open $ficheroBase: $!";
		while( my $baseLine = <$base>)  { 
			if ($baseLine =~ /^\s*W\(/s) {
				#get each number of the line
				my @compareLine = split(/\s+/,$baseLine);
				my $compRef = [];
				foreach $line (@compareLine) {
					if ($line =~ /^-*\d/s) {
						push(@{$compRef},$line);
					}
				}
				push(@{$compareListBase}, $compRef);
			}
		}

		open my $base, $ficheroComp;
		while( my $baseLine = <$base>)  {    
			if ($baseLine =~ /^\s*W\(/s) {
				#get each number of the line
				my @compareLine = split(/\s+/,$baseLine);
				my $compRef = [];
				foreach $line (@compareLine) {
					if ($line =~ /^-*\d/s) {
						push(@{$compRef},$line);
					}
				}
				push(@{$compareListTest}, $compRef);
			}
		}
		
	}
	elsif ($section eq "ALYA Witness set results") {
		#obtain the last line, get all the columns and compare his values
		open my $base, $ficheroBase or die "Could not open $ficheroBase: $!";
		my $lastBaseLine;
		while( my $baseLine = <$base>)  {
			$lastBaseLine = $baseLine;
		}
		print "LastBaseLine:$lastBaseLine\n";
		#get each number of the line
		my @compareLine = split(/\s+/,$lastBaseLine);
		my $compRef = [];
		my $idx = 0;
		foreach $line (@compareLine) {
			if ($line =~ /^-*\d/s) {				
				if (!$columnToCompare || $columnToCompare==$idx) {
					push(@{$compRef},$line);
				}
				$idx = $idx + 1;
			}
		}
		push(@{$compareListBase}, $compRef);
		
		open my $base, $ficheroComp;
		my $lastCompLine;
		while( my $baseLine = <$base>)  {    
			$lastCompLine = $baseLine
		}
		print "LastCompLine:$lastCompLine\n";
		#get each number of the line
		my @compareCompLine = split(/\s+/,$lastCompLine);
		my $compCompRef = [];
		$idx = 0;
		foreach $line (@compareCompLine) {
			if ($line =~ /^-*\d/s) {				
				if (!$columnToCompare || $columnToCompare==$idx) {
					push(@{$compCompRef},$line);
				}
				$idx = $idx + 1;
			}
		}
		push(@{$compareListTest}, $compCompRef);
		print Dumper($compareListBase);
		print Dumper($compareListTest);
	}
	elsif ($section eq "ensi coordinates") {
		#search for w(n,n) at start line
		#for each line we get each number and make the comparison
		open my $base, $ficheroBase or die "Could not open $ficheroBase: $!";
		while( my $baseLine = <$base>)  { 
			if ($baseLine =~ m/^\s*-?\d+\.\d+/) {
				my $line = $baseLine;
				$line =~ s/^\s+//;
				$line =~ s/\s+$//;
				my $compRef = [];				
				push(@{$compRef},$line);
				push(@{$compareListBase}, $compRef);
			}
		}

		open my $base, $ficheroComp;
		while( my $baseLine = <$base>)  {
			if ($baseLine =~ m/^\s*-?\d+\.\d+/) {
				my $line = $baseLine;
				$line =~ s/^\s+//;
				$line =~ s/\s+$//;
				my $compRef = [];				
				push(@{$compRef},$line);
				push(@{$compareListTest}, $compRef);
			}

		}
		
	}
	elsif ($section eq "ComponentNames PRESS") {
		#search for w(n,n) at start line
		#for each line we get each number and make the comparison
		open my $base, $ficheroBase or die "Could not open $ficheroBase: $!";
		my $beginComp = 0;
		while( my $baseLine = <$base>)  {			
			if ($baseLine =~ m/^ComponentNames PRESS/) {
				$beginComp = 1;
			}
			if ($beginComp == 1 && $baseLine =~ m/^\s+\d+\s+(\d+\.\d+)/) {
				my $line = $baseLine;
				$line =~ s/^\s+\d+\s+//;
				$line =~ s/\n//;
				my $compRef = [];				
				push(@{$compRef},$line);
				push(@{$compareListBase}, $compRef);
			}
		}
		$beginComp = 0;
		open my $base, $ficheroComp;
		while( my $baseLine = <$base>)  {
			if ($baseLine =~ m/^ComponentNames PRESS/) {
				$beginComp = 1;
			}
			if ($beginComp == 1 && $baseLine =~ m/^\s+\d+\s+(\d+\.\d+)/) {
				my $line = $baseLine;
				$line =~ s/^\s+\d+\s+//;
				$line =~ s/\n//;
				my $compRef = [];				
				push(@{$compRef},$line);
				push(@{$compareListTest}, $compRef);
			}
		}		
	}
   elsif ($section eq "particles") {
	   print "PARTICLES COMPARISON\n";
		#Particles pts.res cvg file format
		# --| ALYA Lagrangian postprocess  
        # --| Columns displayed:
        # --|  1. Current time          2. Particle number        3. x            
		# --|  4. y                     5. z                      6. vx           
		# --|  7. vy                    8. vz                     9. Type number  
		# --| 10. Subdomain            11. ax                    12. ay           
		# --| 13. az                   14. Cd                    15. Re           
		# --| 16. vfx                  17. vfy                   18. vfz          
		# --| 19. adx                  20. ady                   21. adz          
		# --| 22. aex                  23. aey                   24. aez          
		# --| 25. agx                  26. agy                   27. agz          
		# --| 28+ physical properties
		#We check the first 6 columns
		my $compareListBaseHash = {};
		open my $base, $ficheroBase or die "Could not open $ficheroBase: $!";
		my $first = 1;
		my $firstKey = "";
		while( my $baseLine = <$base>)  {
			if ($baseLine =~ m/^\s*([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([0-9]+)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)/) {
				my $line = $baseLine;
				my $compRef = [];																
				$line =~ /^\s*([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([0-9]+)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)/;
				push(@{$compRef},$1,$3,$4,$6,$8,$10,$12,$14);
				my $auxKey = $1 . "|" . $3;
				#insert the values into hash to eliminate duplicates
				$compareListBaseHash->{$auxKey} = $compRef;
				#push(@{$compareListBase}, $compRef);
			}
		}
		
		my $compareListTestHash = {};
		open my $base, $ficheroComp;
		while( my $baseLine = <$base>)  {			
			if ($baseLine =~ m/^\s*([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([0-9]+)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)/) {
				my $line = $baseLine;
				my $compRef = [];																
				$line =~ /^\s*([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([0-9]+)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)/;
				push(@{$compRef},$1,$3,$4,$6,$8,$10,$12,$14);
				my $auxKey = $1 . "|" . $3;
				#insert the values into hash to eliminate duplicates
				$compareListTestHash->{$auxKey} = $compRef;
				#push(@{$compareListTest}, $compRef);
			}
		}
		#Load the comparisons into list correcting the order, of avoid problem file orders.
		while( my ($k, $v) = each %$compareListBaseHash ) {			
			push(@{$compareListBase}, $v);			
			my $v2 = $compareListTestHash-> {$k};
			#if ($v2) {
				push(@{$compareListTest}, $v2);
			#}			
		}	
		
	}
    #----------------END FILL SECTION---------------------------------------
	#fill the comparison structures with the comparison items from the files
	#-----------------------------------------------------------------------
	#print Dumper($compareListBase);
	#print Dumper($compareListTest);

    #----------------COMPARE SECTION----------------------------------------
	#fill the comparison structures with the comparison items from the files
	#-----------------------------------------------------------------------
	
	#check if have the same number of elements
	if (scalar @{$compareListBase} != scalar @{$compareListTest}) {
		$compareLog->{error} = "File doesn't have the same number of lines, base:" . scalar @{$compareListBase} .
                                       " lines, test:" . scalar @{$compareListTest} . " lines";
	}
	else {
		my $line = 0;
		my $comparisonsCount = 0;
		$compareLog->{diffValue} = [];
		foreach $comparLine (@{$compareListBase}) {
			if ($comparisonsCount < 30) {
				my $column = 0;
				foreach $lineToCompare (@{$comparLine}) {
					my $original = $lineToCompare;					
					my $obtained = $compareListTest->[$line][$column];
					#print "OBTAINED:$obtained\n";
					$difference = $original - $obtained;
					#print "DIFFERENCE:$difference\n";
					if (isDifferent($difference,$compar->{decimals} )) {
						$diffValue = {};
						if ($columnToCompare) {
							$diffValue->{column} = $columnToCompare;
						}
						else {
							$diffValue->{column} = $column;
						}					
						$diffValue->{original} = $original;
						$diffValue->{obtained} = $obtained;
						push(@{$compareLog->{diffValue}},$diffValue);
						$comparisonsCount = $comparisonsCount + 1;
					}	
					$column = $column + 1;
				}
				$line = $line + 1;
		    }	
		}
		if (scalar @{$compareLog->{diffValue}} > 0) {
			$compareLog->{passed} = "false";
		}
		else {
			$compareLog->{passed} = "true";
		}		
	}		
}

sub isDifferent {
	local($difference, $decimals) = @_;
	$isDifferent = 0;
	$differ = $decimals - $difference;
	if (abs($difference) > $decimals) {
		$isDifferent = 1;
	}
	else {
		$isDifferent = 0;
	}
	return $isDifferent;
}

#Check if the current test have to be runned
#if true then returns the alya execution command
#if false returns 0
sub haveToExecTest {
	local($compilationId, $alyaExec, $basePath, $origPath, $common, $bigExecution) = @_;
	$haveToExec = 0;
	
	#check if alya executable id it is present and enter executable name into $haveToExec
	if (!$compilationId) {
		$haveToExec = $alyaExec;
	}
	else {
		my $alyaExecAux = $alyaExec;
		my $newExecName = "Alya_$compilationId.x";
		$alyaExecAux =~ s/Alya.x/$newExecName/g;
		print "AlyaExecAux:$alyaExecAux\n";
		chdir $basePath . "/tmp";
		if (-f $alyaExecAux) {
			$haveToExec = $alyaExecAux;
		}		
		chdir $origPath;
	}
	
	#if bigExecution and normalExecution PARAMETERS

	if ($haveToExec) {
		$withBigExec = $common->{bigExecution};

		if ($withBigExec eq 'true') {
			if ($bigExecution ne 'true') {
				$haveToExec = 0;
			}
		}
		else {
			if ($bigExecution eq 'true') {
				$haveToExec = 0;
			}					
		}
	}

	return $haveToExec;
}

sub avg {
  @_ or return 0;
  my $sum = 0;
  $sum += $_ foreach @_;
  return $sum/@_;
}

#----------------------------------------------------------------------
#--Function getPerformanceData
#--Description:
#----Makes comparisons between files and creates reports about the results
#--Parameters:
#----comparison: Array that contains the comparisons list to do and his properties
#----reportName: Then name of the report to generate
sub getPerformanceData {
	local($moduleName, $testName) = @_;

	#xml data
	my $xmlSimple = new XML::Simple;
	my $moduleData = $xmlSimple->XMLin("modules/" . $moduleName . ".xml");
		
	
	#log
	$result->{performance} = {};
	
	#Number of tests
	$cvgFileList = $moduleData->{cvgdata}->{cvgfile};

	@cvgFiles = $cvgFileList;
	if (ref $cvgFileList eq 'ARRAY') {
		@cvgFiles = @{$cvgFileList};
	}
	
	#iterate over each test and calculate the cpu mean time
	my $mean = 0;
	
	foreach $cvgFile (@cvgFiles) {
		my $name = $cvgFile->{cvgfilename};
		if ($name ne '') {
			my $cputimecol = $cvgFile->{cputimecol};
			my $headlen = $cvgFile->{headlen};
			my @cvgData;
			
			$name = $basePath . "/tmp/" . $testId . "." . $name . ".cvg";
			#----Parseo fichero
			open( $l, "<$name" ) || print "Error : $!";
			my @lines = <$l>;
			close( $l );
			my $lineIdx = 0;
			my @values;
			foreach my $line ( @lines ) {
			  $lineIdx = $lineIdx + 1;
			  # Skipping if the line is part of the header
			  next if ( $lineIdx <= $headlen );
			  
			  @cvgData = split( /\s+/, $line );
			  #print "Line:" . $line . "\n";
			  #print "Split:" . join(", ", @cvgData). "\n";
			  #print "Cputimecol:" . $cputimecol. "\n";
			  #print "Cputime Data:" . $cvgData[$cputimecol]. "\n";
			  push(@values, $cvgData[$cputimecol]);
			}
			#----Fin Parseo fichero
			$mean = $mean + avg(@values);
		}
	}
	
	$result->{performance}->{CPUTimeAtEachStep} = $mean;
	          	
}

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
sub doTests {
	local($common, $moduleName, $testName, $serialArg) = @_;
        $basePath = "modules/";
	#xml data
	$xml = new XML::Simple;
	$data = $xml->XMLin("modules/" . $moduleName . "/" . $testName . ".xml");

	$execModule = $common->{execModule};
	if (!$execModule) {
		$execModule = "LocalExec";
	}
	$execution = ALYA::ExecFactory->instantiate($execModule);


	#global initializations
	$basePath = $basePath . $moduleName . "/" . $data->{id};
	$alyaExec = $common->{executable};
	$alya2posExec = $common->{alya2pos};
	$testId = $data->{id};
	rmtree($basePath . "/tmp");

        #report xml initialization
        rmtree( $basePath . "/" . $reportDir );
        mkdir( $basePath . "/" . $reportDir);
	
	my $report = {};
	$report->{testResult} = {};
        $report->{testResult}->{name} = $data->{id};
        $report->{testResult}->{module} = $moduleName;
        $report->{testResult}->{services} = $data->{services};

	$report->{testResult}->{results}->{result} = [];
        my $results = $report->{testResult}->{results};

	#Number of tests
	$executionsTest = $data->{executions}->{execution};
	if ($serialArg) {		
		$executionsTest = {};
	        $executionsTest->{mpiProcesses} = 1;
		print Dumper($executionsTest);
	}
	@executions = $executionsTest;
	if (ref $executionsTest eq 'ARRAY') {
		@executions = @{$executionsTest};
	}
	#iterate over each test
	foreach $executionTest (@executions) {
		my $compilationId = $executionTest->{compilationId};
		print "CompilationId:$compilationId\n";
		my $openMPThreads = $executionTest->{openMPThreads};
		my $mpiProcesses = $executionTest->{mpiProcesses};
		my $bigExecution = $executionTest->{bigExecution};
		if (!$openMPThreads) {
			$openMPThreads = 1;
		}
		#--------------------------
		#Check if we have to execute the test
		#1.create tmp dir
		mkdir( $basePath . "/tmp");
		my $executable = haveToExecTest($compilationId, $alyaExec, $basePath, "../../../../", $common, $bigExecution);
		if ($executable) {
			#log
			$result = {};
		        $result->{mpiProcesses} = $mpiProcesses;
			$result->{openMPThreads} = $openMPThreads;
			if ($compilationId) {
				$result->{compilationId} = $compilationId;
			}
            $result->{file} = [];
		    push(@{$results->{result}}, $result);

			#exec locally
			#2.copy input files
			print "Base path:$basePath\n";
			opendir DH, $basePath or die "Couldn't open the current directory: $!";
			while ($_ = readdir(DH)) {
				next if $_ eq "." or $_ eq ".." or -d $_;
				if (!($_ eq "base") && !($_ eq $reportDir) && !($_ eq "tmp")) {
					copy($basePath . "/" . $_ , $basePath . "/tmp" . "/" . $_)
					    or die "copy failed: $!";		
				}
			}
			
			#--modify DIVISION parameter of the ker.dat input file--
			my $division = $executionTest->{division};
			if (!$division) {
				$division = "0";
			}
			#Open the .ker.dat input file
			my $kerdatFile = $basePath . "/tmp" . "/" . $testId . ".ker.dat";
			open FILE, "<$kerdatFile";
			my $kerdat = do { local $/; <FILE> };
			close FILE;
			#modify the division parameter
			$kerdat =~ s/<<division>>/$division/;
			#save the modified .ker,dat file
			open FILE, ">$kerdatFile" or die $!;
			print FILE $kerdat;
			close FILE;
			
			#--Auxilary execution to test the RESTART--
			my $restart = $executionTest->{restart};
			if ($restart eq 'true') {
				#Save the current .dat file in other name
				my $datFile = $basePath . "/tmp" . "/" . $testId . ".dat";
				my $datAuxFile = $basePath . "/tmp" . "/" . $testId . "_Aux.dat";
				move($datFile,$datAuxFile);
				
				#Save .dat file to create the restart file
				$datFile = $basePath . "/tmp" . "/" . $testId . "_restart1.dat";
				$datAuxFile = $basePath . "/tmp" . "/" . $testId . ".dat";
				move($datFile,$datAuxFile);
				
			    #Exec the job
			    $execOutput = $execution->execTest($basePath, "../../../../", $executable, $testId, $data->{timeout}, $executionTest);
			    
			    #Save .dat file to continue from the restart file
				$datFile = $basePath . "/tmp" . "/" . $testId . "_restart2.dat";
				$datAuxFile = $basePath . "/tmp" . "/" . $testId . ".dat";
				move($datFile,$datAuxFile);
				
			}

			#Exec the job
			$execOutput = $execution->execTest($basePath, "../../../../", $executable, $testId, $data->{timeout}, $executionTest);

           if ($restart eq 'true') {
				#Restart the origina .dat file, for the next tests
				my $datFile = $basePath . "/tmp" . "/" . $testId . "_Aux.dat";
				my $datAuxFile = $basePath . "/tmp" . "/" . $testId . ".dat";
				move($datFile,$datAuxFile);				
			}

			#4.check the execution finalization
			print "Exec output:" . $execOutput;
			my $execOutputAux = $execOutput;
			$execOutputAux =~ s/\s+//g;
			if ($execOutputAux =~ m/ALYAFINISHEDNORMALLY/ or $execOutputAux =~ m/ALYACALCULATIONSCORRECT/) {	
			   print "finished correctlyyyyyyyyyyyyyyyyyyyyy\n";
			   
			   #If we have couplings we need to do one alya2pos for each problem
               my $couplingsTest = $executionTest->{coupling};
               #--------------WITH COUPLING-------------------
               if ($couplingsTest) {
                 #Iterate over the coupling modules to create the command line executable
                 my @couplings = $couplingsTest;
                 if (ref $couplingsTest eq 'ARRAY') {
                   @couplings = @{$couplingsTest};
                 }
                 foreach $couplingTest (@couplings) {
                   my $problemName = $couplingTest->{problemName};
                   if ("true" eq $data->{comparisons}->{withAlya2pos}) {
                     $execution->execWithOpen($basePath, "../../../../",  "$alya2posExec " . $problemName, 600);
			       }
                   $testId = $problemName;
                   #compare the files and create the report
                   compareFiles($data->{comparisons}->{comparison}, $reportNameFile, $result, $mpiProcesses);			   
                   #get perfomance data
                   getPerformanceData($moduleName, $testId);
                 }
                 $testId = $data->{id};
               }
               
               #--------------WITHOUT COUPLING-------------------
               #if we not have couplings we need have one problem to test
               else {
			     #if it is required executes alya2pos			   
			     if ("true" eq $data->{comparisons}->{withAlya2pos}) {
                   $execution->execWithOpen($basePath, "../../../../",  "$alya2posExec " . $testId, 600); 
                 }
                   
                 #compare the files and create the report
                 compareFiles($data->{comparisons}->{comparison}, $reportNameFile, $result, $mpiProcesses);
			   
                 #get perfomance data
                 print "antes write report A!!!!!!!!!!!!!!!!!!!!!!!!";
                 getPerformanceData($moduleName, $testName);
                 print "antes write report B!!!!!!!!!!!!!!!!!!!!!!!!";
 								     				  
               }			   
			}
			else {
			   print "finished errorssssssssssssssssssssss\n";
			   #log
			   $result->{file} = [];
			   $compareLog = {};
			   $compareLog->{fileName} = "";
			   $execOutput =~ s/[\x00-\x08]//g;
			   print $execOutput;
			   $compareLog->{error} = "Alya execution finished with errors:\n" . $execOutput;
		           $compareLog->{passed} = "false";
			   push(@{$result->{file}}, $compareLog);
			}
		}
		#5.delete temporal files
		#$files_deleted = rmtree($basePath . "/tmp");	


	}

	#write the report
	$xml->XMLout(
	    $report,
	    KeepRoot => 1,
	    NoAttr => 1,
	    OutputFile => $basePath . "/".$reportDir."/" . $data->{id} . ".xml",
	); 

	return $reportNameFile;
}

1;
