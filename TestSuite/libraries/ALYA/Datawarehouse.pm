package ALYA::Datawarehouse;

#-----------------------------------------------------------------------------------------
#--Package that stores the results into the datawarehouse system, based on mysl data base
#-----------------------------------------------------------------------------------------

#usage modules
use FindBin;                 # locate this script
use lib "$FindBin::Bin/.";  # use the parent directory
use File::Copy;
use File::Path;
use File::Compare;
use File::Copy::Recursive qw(dircopy);
use XML::Simple;
use Data::Dumper;
use POSIX;
#xsl test
use XML::XSLT;
#bbdd
use DBI;

#global variables
my $basePath = "modules/";
my $reportDir = "report";
#----BBDD CONNECTION DATA---
my $driver = "mysql"; 
my $database = "alya_testsuite";
my $host = "bsccase02.bsc.es";
my $dsn = "DBI:$driver:database=$database;host=$host";
my $userid = "aartigue";
my $password = "thiat8Ee";
my $bigExecutionArg = 0;
#----END BBDD CONNECTION DATA---

#global variables


#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------

#----------------------------------------------------------------------
#--Function generateReportName
#--Description:
#----Genereates the report file name, that includes the current time, the module name and the test name.
#--Parameters:
#----moduleName: The module's name that it's tested
#----testName: The test name.
sub generateReportName {
	#local($moduleName, $testName) = @_;
	#get local time in format: MMM_DD_YYYY
	my $now_string = strftime "%b_%d_%Y", localtime;

	#Compound the file name
	my $fileName = "report_" . $now_string;
	#opendir RH, $basePath . $reportDir or die "Couldn't open the current directory: $!";
        #check if this filename exists
        #$i = 0;
	#while ($_ = readdir(RH)) {
	#	next if $_ eq "." or $_ eq ".." or -d $_;
	#	if ($_ =~ /$fileName/) {
	#		$i++;
	#	}
	#}
        #if filename exists add an aditional identifier to the name
	#if ($i > 0) {
	#	$fileName .= "_" . ($i + 1);
	#}
        #add the extension
	#$fileName .= ".txt";

	return $fileName;
}


sub alignNameSpaces {
	local($name, $numSpaces) = @_;
	
	my $spaces = "";
	#alineacion, todos empiezan en la columna 11
	my $nameSize = length $name;
	for ($count = $nameSize; $count <= $numSpaces; $count++) {
		$spaces = $spaces . " ";
	}
	
	return $spaces;
	
}

sub getTest_id_dim {
	local($test_name, $module_name, $dn, $uid, $passw) = @_;
	
	my $test_id = "";
	
	my $dbh = DBI->connect($dn, $uid, $passw ) or die $DBI::errstr;
    my $sth = $dbh->prepare("SELECT test_id FROM test_simulation_dim WHERE test_name = ?");
    $sth->execute( $test_name ) or die $DBI::errstr;
    my $numRows = $sth->rows;
    if ($numRows == 1) {
		#TEST FOUND
		my @row = $sth->fetchrow_array();
		($test_id) = @row;
	}elsif ($numRows == 0) {
		#NO TEST FOUND, WE NEED TO ADD IT
		
		#---Get the module_id----
		my $module_id = "";
		my $sth2 = $dbh->prepare("SELECT module_id FROM module_simulation_dim WHERE module_name = ?");
		$sth2->execute( $module_name ) or die $DBI::errstr;
		my $numRows2 = $sth2->rows;
		if ($numRows2 == 1) {
			#MODULE FOUND
			my @row2 = $sth2->fetchrow_array();
			($module_id) = @row2;
		}elsif ($numRows == 0) {
			#MODULE NOT FOUND, WE HAVE TO ADD IT
			$sth2->finish();
			my $sth3 = $dbh->prepare("INSERT INTO module_simulation_dim (module_name,manager_id) VALUES (?,?)");
			$sth3->execute($module_name,1) or die $DBI::errstr;
            $sth3->finish();
            #$dbh->commit or die $DBI::errstr;            
            $sth2 = $dbh->prepare("SELECT module_id FROM module_simulation_dim WHERE module_name = ?");
            $sth2->execute( $module_name ) or die $DBI::errstr;
            my @row2 = $sth2->fetchrow_array();
			($module_id) = @row2;			
		}else {
			die "ERROR MODULO CON DOS IDENTIFICADORES";
		}
		$sth2->finish();
		
		#---Insert the test---
		$sth2 = $dbh->prepare("INSERT INTO test_simulation_dim (test_name,module_id) VALUES (?,?)");
		$sth2->execute($test_name,$module_id) or die $DBI::errstr;
        $sth2->finish();
        #$dbh->commit or die $DBI::errstr;
        $sth->finish();
        $sth = $dbh->prepare("SELECT test_id FROM test_simulation_dim WHERE test_name = ?");
        $sth->execute( $test_name ) or die $DBI::errstr;
        my @row2 = $sth->fetchrow_array();
	    ($test_id) = @row2;		         
		
	}
	else {
		die "ERROR TEST CON DOS IDENTIFICADORES";
	}
	
	$sth->finish();
	$dbh->disconnect;
	
	return $test_id;
	
}

sub getCompilation_id_dim {
	local($compilation_name, $dn, $uid, $passw) = @_;
	
	my $compilation_id = "";
	
	if ($compilation_name eq "") {
		#The default is 1 for non compilation name.
		return "1";
	}
	
	my $dbh = DBI->connect($dn, $uid, $passw ) or die $DBI::errstr;
    my $sth = $dbh->prepare("SELECT compilation_id FROM compilation_dim WHERE compilation_name = ?");
    $sth->execute( $compilation_name ) or die $DBI::errstr;
    my $numRows = $sth->rows;
    if ($numRows == 1) {
		#TEST FOUND
		my @row = $sth->fetchrow_array();
		($compilation_id) = @row;
	}elsif ($numRows == 0) {
		#NO TEST FOUND, WE NEED TO ADD IT
		
		#---Insert the test---
		my $sth2 = $dbh->prepare("INSERT INTO compilation_dim (compilation_name) VALUES (?)");
		$sth2->execute($compilation_name) or die $DBI::errstr;
        $sth2->finish();
        #$dbh->commit or die $DBI::errstr;
        $sth->finish();
        $sth = $dbh->prepare("SELECT compilation_id FROM compilation_dim WHERE compilation_name = ?");
        $sth->execute( $compilation_name ) or die $DBI::errstr;
        my @row2 = $sth->fetchrow_array();
	    ($compilation_id) = @row2;		
	}
	else {
		die "ERROR COMPILATION CON DOS IDENTIFICADORES";
	}
	
	$sth->finish();
	$dbh->disconnect;
	
	return $compilation_id;
	
}

#----------------------------------------------------------------------
#--Function doEmailReport
#--Description:
#----Creates a basic summary of the testSuite and testSuiteJob results in text format to send to the general email.
#--Parameters:
#----folderName: Folder name where the testSuite results are alocated
#----xmlFile: Name of the main xml testSuite result file
#----jobResultXmlFile: Path to the xml testSuiteJob result
#----sshReport: Data of the ssh configuration information, used to create a link to the remote report
#INSERT INTO test_simulation_fact
#(date_id,test_id,compilation_id,mpiprocs,openmpthreads,bigexecution,pass,timeout,cputime,svnversion,errortext)
#VALUES
#(,,,,,,,,,,);
sub resultsToBBDD {
	local($folderName, $xmlFile, $jobResultXmlFile, $globalResultXmlFile, $bigExecution) = @_;
	
    #Get svnRevision from global result xml file
	my $xml = new XML::Simple;
	my $data = $xml->XMLin($folderName . "/" .$globalResultXmlFile);
	my $svnVersion = $data->{information}->{svnRevision};
	$bigExecutionArg = $bigExecution;
	
	#Get date_id
	my $date_id;
    my $now_string = strftime "%Y-%m-%d", localtime;
	my $dbh = DBI->connect($dsn, $userid, $password ) or die $DBI::errstr;
    my $sth = $dbh->prepare("SELECT date_id FROM time_dim WHERE date = ?");
    $sth->execute( $now_string ) or die $DBI::errstr;
    while (my @row = $sth->fetchrow_array()) {
      ($date_id) = @row;
    }
    $sth->finish();
    $dbh->disconnect;        

	#xml data
	$data = $xml->XMLin($jobResultXmlFile);

=pod
	#$outputReport = $outputReport . "\n------------Compilation-----------------------------------\n";
	$outputReport = $outputReport . "\n------------Modules and Services Compilation--------------\n";
	#Modules
	$outputReport = $outputReport . "---Modules\n";
	$testCompResult = $data->{moduleCompilation}->{moduleCompilationResult};
	@testCompResultsList = $testCompResult;
	if (ref $testCompResult eq 'ARRAY') {
		@testCompResultsList = @{$testCompResult};
	}	
	foreach $testCompResult (@testCompResultsList) {
		$passed = "FAIL";
		if ($testCompResult->{passed} eq "true") {
			$passed = "OK"
		}
		my $spaces = alignNameSpaces($testCompResult->{moduleName}, 10);
		$outputReport = $outputReport . "------" . $testCompResult->{moduleName} . $spaces . "-->" . $passed ."\n";
	}

	#Services
	$outputReport = $outputReport . "---Services\n";
	$servicesCompResult = $data->{serviceCompilation}->{serviceCompilationResult};
	@serCompResults = $servicesCompResult;
	if (ref $servicesCompResult eq 'ARRAY') {
		@serCompResults = @{$servicesCompResult};
	}	
	foreach $serCompResult (@serCompResults) {
		$passed = "FAIL";
		if ($serCompResult->{passed} eq "true") {
			$passed = "OK"
		}
		my $spaces = alignNameSpaces($serCompResult->{serviceName}, 10);
		$outputReport = $outputReport . "------" . $serCompResult->{serviceName} . $spaces . "-->" . $passed ."\n";
	}


	$outputReport = $outputReport . "\n------------Configuration Compilation Tests---------------\n";
	#compilation tests
	$outputReport = $outputReport . "---Compilations\n";
	$testCompilationResult = $data->{testCompilation}->{testCompilationResult};
	@testCompilationResultList = $testCompilationResult;
	if (ref $testCompilationResult eq 'ARRAY') {
		@testCompilationResultList = @{$testCompilationResult};
	}	
	foreach $testCompilationResult (@testCompilationResultList) {
		$passed = "FAIL";
		if ($testCompilationResult->{passed} eq "true") {
			$passed = "OK"
		}
		my $spaces = alignNameSpaces($testCompilationResult->{compilationDescription},40);
		$outputReport = $outputReport . "------" . $testCompilationResult->{compilationDescription} . $spaces . "-->" . $passed ."\n";
	}
=cut

	#iterate over the sub reports
	$testResults = "";
	#xml data
	$data = $xml->XMLin($folderName . "/" .$xmlFile);

	$resultsReport = $data->{moduleResults}->{moduleResult};
	@resultsRep = $resultsReport;
	if (ref $resultsReport eq 'ARRAY') {
		@resultsRep = @{$resultsReport};
	}
	my $testPassed = 1;
	$subTestResult = "";
	#iterate over each test
	foreach $resRep (@resultsRep) {
		#check if there is an error
		if ($resRep->{error}) {
			$testPassed = 0;
		}
		if ($resRep->{passed} eq 'false') {
			$testPassed = 0;
		}
		#------------------------------
		#iterate over each subtest	
		$subData = $xml->XMLin($folderName . "/" . $resRep->{moduleName} . "/" . $resRep->{moduleName} . ".xml" );		
		#Number of tests results
		$resultsTest = $subData->{testResults}->{testResult};
		@resultTest = $resultsTest;
		if (ref $resultsTest eq 'ARRAY') {
			@resultTest = @{$resultsTest};
		}

		#iterate over each test
		foreach $resTest (@resultTest) {			
			my $errAux = $resTest->{error};
			my $subPassed = "OK";
			#check if there is an error
			if ($resTest->{passed} eq 'false') {
				$subPassed = "FAIL";
			}
			if ($resTest->{error}) {
				$subPassed = "FAIL";
				$errorMsg = $resTest->{error};
				if ($errorMsg =~ m/timed out, it takes more than/g) {
					$subPassed = "TIMEOUT";
				}
			}
			
			#get test_id for the data base
			my $test_id = getTest_id_dim ($resTest->{testName},$resRep->{moduleName}, $dsn, $userid, $password);						
			
			my $spaces = alignNameSpaces($resTest->{testName},30);
			
			#------------------------------
			#iterate over each subtest	
			#$subPassed = '';
			$subsubData = $xml->XMLin($folderName . "/" . $resRep->{moduleName} . "/" . $resTest->{testName} . "/". $resTest->{testName} . ".xml" );		
			#Number of tests results
			$resultsSubTest = $subsubData->{results}->{result};
			@resultSubTest = $resultsSubTest;
			if (ref $resultsSubTest eq 'ARRAY') {
				@resultSubTest = @{$resultsSubTest};
			}

			#iterate over each test
			foreach $resSubTest (@resultSubTest) {
				
				#Get the compilation_id from database
				my $compilation_id = getCompilation_id_dim ($resSubTest->{compilationId}, $dsn, $userid, $password);
				
				#Get the mpiprocs
				my $mpiprocs = $resSubTest->{mpiProcesses};
				
				#Get the openmpthreads
				my $openmpthreads = $resSubTest->{openMPThreads};
				
				#Get big execution from $bigExecutionArg global var			
				
				#GET cputime
				my $cputime = $resSubTest->{performance}->{CPUTimeAtEachStep};
							
				$resultsSubSubTest = $resSubTest->{file};
				@resultSubSubTest = $resultsSubSubTest;
				if (ref $resultsSubSubTest eq 'ARRAY') {
					@resultSubSubTest = @{$resultsSubSubTest};
				}
				#iterate over each test
				my $subsubPassed = "OK";
				my $errortext = "";
				my $fileDetails = {};
				$fileDetails->{fileDetails} = {};
				$fileDetails->{fileDetails}->{file} = [];
				foreach $resSubSubTest (@resultSubSubTest) {
					push(@{$fileDetails->{fileDetails}->{file}},$resSubSubTest);			
					my $errSubSubAux = $resSubSubTest->{error};
					my $subsubsubPassed = "OK";
					#check if there is an error
					if ($resSubSubTest->{passed} eq 'false') {
						$subsubsubPassed = "FAIL";
					}
					if ($resSubSubTest->{error}) {
						$subsubsubPassed = "FAIL";
						$errorSubSubMsg = $resSubSubTest->{error};
						$errortext = $errortext . $errorSubSubMsg;
						if ($errorSubSubMsg =~ m/timed out, it takes more than/g) {
							$subsubsubPassed = "TIMEOUT";
						}
					}
					if ($subsubsubPassed ne 'OK') {
						$subsubPassed = $subsubsubPassed;
					}
				}
				
				my $pass = 1;
				my $timeout = 0;
				if ($subsubPassed ne 'OK') {
						$pass = 0;
						if ($subsubPassed eq 'TIMEOUT') {
							$timeout = 1;
						}
				}
				
				#FILE DETALILS
				my $fileDetails = $xml->XMLout($fileDetails,KeepRoot => 1,NoAttr => 1, NoIndent => 1);
				
				#DO THE INSERTION OF ALL THE DATA				
			    $dbh = DBI->connect($dsn, $userid, $password ) or die $DBI::errstr;
                $sth = $dbh->prepare("INSERT INTO test_simulation_fact(date_id,test_id,compilation_id,mpiprocs,openmpthreads,bigexecution,pass,timeout,cputime,svnversion,errortext,fileDetails) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                $sth->execute($date_id,$test_id,$compilation_id,$mpiprocs,$openmpthreads,$bigExecutionArg,$pass,$timeout,$cputime,$svnVersion,$errortext,$fileDetails) or die $DBI::errstr;
                $sth->finish();
                $dbh->disconnect;
				
				$subPassed = $subPassed . ' ' . $subsubPassed;	
			}
		}		
	}
	
	$testResult = "";
	if ($testPassed) {
		$testResult = "------------ALL TESTS PASSED SUCCESSFULLY------------------\n";
	}
	else {
		$testResult = "------------SOME TESTS HAVE NOT PASSED------------------\n";
	}

	$outputReport = $outputReport . $testResult;
	$outputReport = $outputReport . "-----------------------------------------------------------\n\n";
	$outputReport = $outputReport . $subTestResult;

	#copy the css styles and images
	return $outputReport;
	

}

1;
