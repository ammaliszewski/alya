package ALYA::Modules;

#---------------------------------------------------------------------
#--Package that executes all tests associated to one module-----------
#---------------------------------------------------------------------

#usage modules
use FindBin;                 # locate this script
use lib "$FindBin::Bin/.";  # use the parent directory
use File::Copy;
use File::Path;
use File::Compare;
use File::Copy::Recursive qw(dirmove);
use XML::Simple;
use ALYA::Tests;
use Data::Dumper;
use POSIX;

#global variables
my $basePath = "modules/";
my $reportDir = "report";

#global variables


#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------


#----------------------------------------------------------------------
#--Function checkIfTestPassed
#--Description:
#----Iterates over all the test results and determines if the test has passed
#--Parameters:
#----testId: The name of the test to check
#----result: The xml report variable where to write if test has passed or not
sub checkIfTestPassed {

local($testId, $result) = @_;
	my $xml = new XML::Simple;
	#load the report xml generated, and check if the module passes the test
	$testReport = $xml->XMLin($basePath . "/" . $testId . "/" . $reportDir . "/" . $testId . ".xml");
	my $testPassed = 1;
	my $testTimedOut = 0;
	
	#Number of tests results
	$resultsReport = $testReport->{results}->{result};
	@resultsRep = $resultsReport;
	if (ref $resultsReport eq 'ARRAY') {
		@resultsRep = @{$resultsReport};
	}

	my $resultsRepSize = 0;
	#iterate over each test
	OUTER_LOOP:
	foreach $resRep (@resultsRep) {
		if ($resRep) {
			$resultsRepSize = $resultsRepSize + 1;
		}
		#check if there is an error
		if ($repRep->{error}) {
			$testPassed = 0;
			last;
		}
		else {
			#iterate over each file			
			$filesReport = $resRep->{file};
			@filesRep = $filesReport;
			if (ref $filesReport eq 'ARRAY') {
				@filesRep = @{$filesReport};
			}
	
			#iterate over each test
			foreach $fileRep (@filesRep) {
				if ($fileRep->{error}) {
					$testPassed = 0;
					#check if timeout
					$errorMsg = $fileRep->{error};
					if ($errorMsg =~ m/timed out, it takes more than/g) {
						$testTimedOut = 1;
					}
					last OUTER_LOOP;
				}
				if ($fileRep->{passed} eq 'false') {
					$testPassed = 0;
					last OUTER_LOOP;
				}	
			}
		}
			
	}
	if ($testPassed) {
		$result->{passed} = "true";
	}
	else {
		$result->{passed} = "false";
		if ($testTimedOut) {
			$result->{error} = "timed out, it takes more than max seconds";
		}
	}
	
	return $resultsRepSize;
}

#----------------------------------------------------------------------
#--Function checkIfTestPassed
#--Description:
#----Iterates over all the test results and determines if the test has passed
#--Parameters:
#----testId: The name of the test to check
#----result: The xml report variable where to write if test has passed or not
sub moveReport {

local($testId) = @_;
	rmtree( $basePath . "/" . $reportDir . "/" . $testId);
        mkdir( $basePath . "/" . $reportDir. "/" . $testId);
	dirmove($basePath . "/" . $testId . "/" . $reportDir , $basePath . "/" . $reportDir. "/" . $testId) or die("$!\n");

	#opendir DH, $basePath . "/" . $testId . "/" . $reportDir or die "Couldn't open the current directory: $!";
	#while ($_ = readdir(DH)) {
	#	next if $_ eq "." or $_ eq ".." or -d $_;
	#	copy($basePath . "/" . $testId . "/" . $reportDir . "/" . $_, 
	#	     $basePath . "/" . $reportDir. "/" . $testId . "/" . $_)
	#		    or die "copy failed: $!";		
	#}
	#rmtree( $basePath . "/" . $testId . "/" . $reportDir);
}

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
sub doModules {
	local($common, $moduleName, $testArg, $serialArg) = @_;
        $basePath = "modules/";

	#xml data
	$xml = new XML::Simple;
	$data = $xml->XMLin($basePath . $moduleName . ".xml");

	$basePath = $basePath . $moduleName;

        #report xml initialization
        rmtree( $basePath . "/" . $reportDir );
        mkdir( $basePath . "/" . $reportDir );
	
	my $report = {};
	$report->{moduleResult} = {};
        $report->{moduleResult}->{name} = $moduleName;
	$report->{moduleResult}->{testResults}->{testResult} = [];
        my $results = $report->{moduleResult}->{testResults};

	#Number of tests
	$modulesTest = $data->{tests}->{test};
	if ($testArg) {		
		$modulesTest = {};
	        $modulesTest->{testName} = $testArg;
	}
	@tests = $modulesTest;
	if (ref $modulesTest eq 'ARRAY') {
		@tests = @{$modulesTest};
	}
	
	#iterate over each test
	foreach $test (@tests) {

		#log
		$result = {};
                $result->{testName} = $test->{testName};
		$result->{description} = $test->{description};                

		#1.Exec the test
		print "Start execution of test $test->{testName}\n";
		ALYA::Tests::doTests($common, $moduleName , $test->{testName}, $serialArg);
		
		#2.Checks if test passed
		my $resultsRepSize = checkIfTestPassed($test->{testName} , $result);
		
		#3.Push only test result only if also one execution is done
		if ($resultsRepSize > 0) {
			push(@{$results->{testResult}}, $result);
		}
		
		#4.move the report test to the module
		moveReport($test->{testName});
	}

	#write the report
	$xml->XMLout(
	    $report,
	    KeepRoot => 1,
	    NoAttr => 1,
	    OutputFile => $basePath . "/" . $reportDir . "/" . $moduleName . ".xml",
	);
}

1;
