#!/bin/sh
source /opt/intel/bin/compilervars.sh intel64
export OMPI_FC=ifort
export SVN_EDITOR=vi
export PATH=/opt/intel/composer_xe_2011_sp1.7.256/bin/intel64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/opt/intel/composer_xe_2011_sp1.7.256/mpirt/bin/intel64
export PATH=/home/tartigues/apps/doxygen/bin:$PATH
export LD_LIBRARY_PATH=/opt/intel/composer_xe_2011_sp1.7.256/compiler/lib/intel64:/opt/intel/composer_xe_2011_sp1.7.256/ipp/../compiler/lib/intel64:/opt/intel/composer_xe_2011_sp1.7.256/ipp/lib/intel64:/opt/intel/composer_xe_2011_sp1.7.256/compiler/lib/intel64:/opt/intel/composer_xe_2011_sp1.7.256/mkl/lib/intel64:/opt/intel/composer_xe_2011_sp1.7.256/tbb/lib/intel64//cc4.1.0_libc2.4_kernel2.6.16.21:/opt/intel/composer_xe_2011_sp1.7.256/debugger/lib/intel64:/opt/intel/composer_xe_2011_sp1.7.256/mpirt/lib/intel64
cd ~/alya/TestSuite
./testSuiteJobClient.plx > output.out 2>&1
./alyaStatsJob.plx > stats.out 2>&1
./alyaDocumentationJob.plx > alyadoc.out 2>&1
