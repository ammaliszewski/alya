#!/usr/bin/perl

#usage modules
use FindBin;                # locate this script
use lib "$FindBin::Bin/libraries";  # use the parent directory
use File::Copy;
use File::Path;
use File::Compare;
use File::Copy::Recursive qw(dirmove);
use XML::Simple;
use ALYA::Modules;
use Data::Dumper;
use POSIX;
use Getopt::Std;

#global variables
$basePath = "modules/";
my $reportDir = "report";
$alyaExec = "~/alya/Executables/unix/Alya.x";
$testId = "";
$columnCompared = 6;

#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------



#----------------------------------------------------------------------
#--Function checkIfTestPassed
#--Description:
#----Iterates over all the test results and determines if the test has passed
#--Parameters:
#----testId: The name of the test to check
#----result: The xml report variable where to write if test has passed or not
sub checkIfTestPassed {

local($moduleId, $result) = @_;

	my $xml = new XML::Simple;
	#load the report xml generated, and check if the module passes the test
	$testReport = $xml->XMLin($basePath . $moduleId . "/" . $reportDir . "/" . $moduleId . ".xml");
	my $testPassed = 1;
	#Number of tests results
	$resultsReport = $testReport->{testResults}->{testResult};
	@resultsRep = $resultsReport;
	if (ref $resultsReport eq 'ARRAY') {
		@resultsRep = @{$resultsReport};
	}

	my $resultsRepSize = 0;
	#iterate over each test
	foreach $resRep (@resultsRep) {		
		if ($resRep) {
			$resultsRepSize = $resultsRepSize + 1;
		}
		#check if there is an error
		if ($resRep->{error}) {
			$testPassed = 0;
			last;
		}
		if ($resRep->{passed} eq 'false') {
			$testPassed = 0;
			last;
		}					
	}
	if ($testPassed) {
		$result->{passed} = "true";
	}
	else {
		$result->{passed} = "false";
	}
	
	return $resultsRepSize;
}

#----------------------------------------------------------------------
#--Function checkIfTestPassed
#--Description:
#----Iterates over all the test results and determines if the test has passed
#--Parameters:
#----testId: The name of the test to check
#----result: The xml report variable where to write if test has passed or not
sub moveReport {

local($moduleId, $reportName) = @_;

	rmtree( $reportName . "/" . $moduleId);
        mkdir(  $reportName. "/" . $moduleId);

	dirmove($basePath . $moduleId . "/" . $reportDir , $reportName. "/" . $moduleId) or die("$!\n");

}

#----------------------------------------------------------------------
#--Function generateReportName
#--Description:
#----Genereates the report file name, that includes the current time, the module name and the test name.
#--Parameters:
#----moduleName: The module's name that it's tested
#----testName: The test name.
sub generateReportName {
	#get local time in format: MMM_DD_YYYY
	$now_string = strftime "%b_%d_%Y", localtime;
	#Compound the file name
	$fileName = "report_" . $now_string;
	return $fileName;
}

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

#argumentos opcionales -m[modulo] -t[test] -s(ejecutar en serie) -b(bigExecution)
getopts('m:t:s:b');
our($opt_m, $opt_t, $opt_s, $opt_b);

my $moduleArg = $opt_m || "0";
my $testArg = $opt_t || "0";
my $serialArg = $opt_s || "0";
my $bigExecutionArg = $opt_b || "0";

#xml data
$xml = new XML::Simple;
$data = $xml->XMLin("testSuite.xml");

#report xml initialization
my $reportName = generateReportName();
rmtree($reportName );
mkdir($reportName );

my $report = {};
$report->{testSuiteResult} = {};
#$report->{testSuiteResult}->{name} = $moduleName;
$report->{testSuiteResult}->{moduleResults}->{moduleResult} = [];
my $results = $report->{testSuiteResult}->{moduleResults};

#Number of tests
$modulesTest = $data->{modules}->{module};
if ($moduleArg) {
	$modulesTest = {};
        $modulesTest->{moduleName} = $moduleArg;
}
@modules = $modulesTest;
if (ref $modulesTest eq 'ARRAY') {
	@modules = @{$modulesTest};
}

#bigExecutionParams
if ($bigExecutionArg) {
	$data->{common}->{bigExecution} = "true";
}

#iterate over each test
foreach $module (@modules) {
	#check if xml module exists
	$moduleXML = $basePath . $module->{moduleName} . ".xml";
	if (-f $moduleXML) {
		#log
		$result = {};
		$result->{moduleName} = $module->{moduleName};
		$result->{description} = $module->{description};		

		#1.Exec the test
		print "Start execution of module $module->{moduleName}\n";
		ALYA::Modules::doModules($data->{common}, $module->{moduleName}, $testArg, $serialArg);

		#2.Checks if test passed
		my $resultsRepSize = checkIfTestPassed($module->{moduleName} , $result);
		
		#3.add to the result only a module with tests
		if ($resultsRepSize > 0) {
			push(@{$results->{moduleResult}}, $result);
		}
		
		#4.move the report module to the testsuite
		moveReport($module->{moduleName}, $reportName);
	}	
}

#write the report
$xml->XMLout(
    $report,
    KeepRoot => 1,
    NoAttr => 1,
    OutputFile => $reportName . "/testSuiteResult.xml",
);

#Create the html output report
#ALYA::Reports::doReport("testSuite.xsl", $reportName . "/testSuiteResult.xml", $reportName . "/index.html");
if (!$data->{common}->{doReport} || $data->{common}->{doReport} eq "true") {
	require ALYA::Reports; 
	ALYA::Reports::doHTMLReport($reportName, "testSuiteResult.xml");
	print ALYA::Reports::doResumeReport($reportName, "testSuiteResult.xml");
}


exit 0
