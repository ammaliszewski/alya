#!/usr/bin/perl


#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

#-------------------------------------------
#--ALYA DOCUMENTATION
#-------------------------------------------

#Parameters
my $docFolder = "Documents/doxygen/html";
my $caseServer = "case\@bsccase02.bsc.es";
my $docServerFolder = "/srv/www/htdocs/case/alya";

#Update current Alya
chdir "..";
my $output = `svn update`;
print $output;

#Reconfigure
chdir "Executables/unix";
$output = `./reconfigure`;
print $output;

#print doxygen path
$output = `which doxygen`;
print "EL DOXYGEN USADO ES:$output";

#make docs
$output = `make docs`;
print $output;

chdir "../../Documents/doxygen/html";

#tar the html doc
$output = `tar -cvzf html.tar.gz *`;
print $output;


#copy html docs to the server
$output = `scp -r html.tar.gz $caseServer:$docServerFolder`;
print $output;

#extract html docs tar in the server
$output = `ssh $caseServer 'cd $docServerFolder; tar -xvzf html.tar.gz'`;
print $output;

#remove html docs tar in the server
$output = `ssh $caseServer 'cd $docServerFolder; rm -f html.tar.gz'`;
print $output;

#remove local tar server
$output = `rm -f html.tar.gz`;
print $output;

#-------------------------------------------
#--ALYA BIBLIOGRAPHY
#-------------------------------------------
print "-----------BiblioCase-----------";

chdir "../../../../biblioCase/";

#Update current BiblioCase
my $output = `svn update`;
print $output;

#Generate html index doc
$output = `./bibtex2html.sh`;
print $output;

#copy html docs to the server
#tar the html doc
$output = `tar -cvzf biblio.tar.gz index.html Author/ Biblio/ Category/ Icons/ Keyword/ Year/`;
$output = `ssh $caseServer 'mkdir $docServerFolder/overview/bibliography'`;
$output = `scp -r biblio.tar.gz $caseServer:$docServerFolder/overview/bibliography/`;
#extract html docs tar in the server
$output = `ssh $caseServer 'cd $docServerFolder/overview/bibliography/; tar -xvzf biblio.tar.gz'`;
#Rename the index.html to bibliography.html
$output = `ssh $caseServer 'mv $docServerFolder/overview/bibliography/index.html $docServerFolder/overview/bibliography/bibliography.html'`;
#remove html docs tar in the server
$output = `ssh $caseServer 'cd $docServerFolder/overview/bibliography/; rm -f biblio.tar.gz'`;
#remove local tar server
$output = `rm -f biblio.tar.gz`;


print $output;

exit 0
