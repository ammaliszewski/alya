ALYA_PATH=/home/bsc21/bsc21704/z2015/REPOSITORY/ALYA_2015FEB18/
ALYA_PATH=/home/bsc21/bsc21704/z2015/REPOSITORY/ALYA_2015May25
ALYA_PATH=/home/bsc21/bsc21704/z2015/REPOSITORY/ALYA_2015Jun02
ALYA_PATH=/home/bsc21/bsc21704/z2015/REPOSITORY/ALYA_2015Jun03
ALYA_PATH=/home/bsc21/bsc21704/z2015/REPOSITORY/ALYA_2015Jun08

ALYAi=1 
ALYAj=1 

time mpirun -np $ALYAi $ALYA_PATH/Executables/unix/Alya.x Mesh01 : -np $ALYAj $ALYA_PATH/Executables/unix/Alya.x Mesh02

# 
# |---------------------------------------------------------------------------|---------------------------------------------------------------------------|
#  
# +solver-interface                                                                
# |_data:vector name="Forces"                                                  
# |_data:vector name="Displacements"                                             
# |_data:vector name="DisplacementDeltas"                                         
# 
# +mesh "WetSurface"                                                            
# |_use-data    name="Displacements"                                                  
# |_use-data    name="DisplacementDeltas"                                             
# |_use-data    name="Forces"                                                              
#
#
# +participant "NASTIN"                                                         
# |_use-mesh  "WetSurface" from="SOLIDZ"                                       
# |_write-data name="Forces"                mesh="WetSurface"                
# |_read-data  name="DisplacementDeltas"    mesh="WetSurface"                 
# |_mapping    direction="write"            mesh="WetSurface"                
# |_mapping    direction="read"             mesh="WetSurface"                
#
#
# +participant "SOLIDZ"                                                              
# |_use-mesh   name="WetSurface"         provide="yes"                         
# |_write-data name="Displacements"         mesh="WetSurface"                  
# |_write-data name="DisplacementDeltas"    mesh="WetSurface"                  
# |_read-data  name="Forces"                mesh="WetSurface"                  
# 
# +communication:sockets                                                         
# |_              from  = "NASTIN"                                             
# |_                to  = "SOLIDZ"                                            
# |_               port = "51235"                                               
# |_ exchange-directory = "../"                                                  
#
# +communication:mpi                                             
# |_              from  = "NASTIN"                                           
# |_                to  = "SOLIDZ"                                            
# |_ exchange-directory = "../"                                                 
#
#
# +coupling-scheme:serial-implicit                                                
# |_participants first="NASTIN" second="SOLIDZ"                                   
# |_exchange data="Forces"                  mesh="WetSurface" from="NASTIN"      
# |_exchange data="Displacements"           mesh="WetSurface" from="SOLIDZ"    
# |_exchange data="DisplacementDeltas"      mesh="WetSurface" from="SOLIDZ"     
#
# 
# +relative-convergence-measure                                                   
# |_data="Displacements"                    mesh="WetSurface" limit="1e-3"      
# +relative-convergence-measure                                                    
# |_data="Forces"                           mesh="WetSurface" limit="1e-3"          
#
# |---------------------------------------------------------------------------|---------------------------------------------------------------------------|

