# state file generated using paraview version 4.3.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Line Chart View'
lineChartView1 = CreateView('XYChartView')
lineChartView1.ViewSize = [487, 275]
lineChartView1.LeftAxisUseCustomRange = 1
lineChartView1.LeftAxisRangeMinimum = -10000.0
lineChartView1.LeftAxisRangeMaximum = 300000.0
lineChartView1.BottomAxisRangeMinimum = -2.0
lineChartView1.BottomAxisRangeMaximum = 2.0

# Create a new 'Line Chart View'
lineChartView2 = CreateView('XYChartView')
lineChartView2.ViewSize = [487, 274]
lineChartView2.LeftAxisUseCustomRange = 1
lineChartView2.LeftAxisRangeMinimum = -0.01
lineChartView2.LeftAxisRangeMaximum = 0.12
lineChartView2.BottomAxisRangeMinimum = -2.0
lineChartView2.BottomAxisRangeMaximum = 2.0

# Create a new 'Render View'
renderView2 = CreateView('RenderView')
renderView2.ViewSize = [488, 579]
renderView2.CenterOfRotation = [0.0012431144714355469, 0.0, 0.10000000149011612]
renderView2.StereoType = 0
renderView2.CameraPosition = [-3.8263585977050822, -1.3199491871417108, 1.268204177333519]
renderView2.CameraFocalPoint = [0.0012431144714355432, -6.324909841776758e-19, 0.10000000149011608]
renderView2.CameraViewUp = [0.24970223946703615, 0.12566519923599104, 0.9601338705129226]
renderView2.CameraParallelScale = 2.828876675490891
renderView2.Background = [0.32, 0.34, 0.43]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'EnSight Reader'
enSightReader1 = EnSightReader(CaseFileName='Small01.ensi.case')
enSightReader1.PointArrays = ['DISPL', 'FIXNO', 'RESID', 'NSIGM', 'TSIGM']

# create a new 'Warp By Vector'
warpByVector1 = WarpByVector(Input=enSightReader1)
warpByVector1.Vectors = ['POINTS', 'DISPL']

# create a new 'EnSight Reader'
enSightReader2 = EnSightReader(CaseFileName='Big01.ensi.case')
enSightReader2.PointArrays = ['DISPL', 'FIXNO', 'RESID', 'NSIGM', 'TSIGM']

# create a new 'Calculator'
calculator2 = Calculator(Input=enSightReader2)
calculator2.ResultArrayName = 'ResultX'
calculator2.Function = 'coordsX'

# create a new 'Plot Over Line'
plotOverLine2 = PlotOverLine(Input=calculator2,
    Source='High Resolution Line Source')
plotOverLine2.Tolerance = 2.22044604925031e-16

# init the 'High Resolution Line Source' selected for 'Source'
plotOverLine2.Source.Point1 = [-1.997513771057129, 0.0, 0.1]
plotOverLine2.Source.Point2 = [2.0, 0.0, 0.1]
plotOverLine2.Source.Resolution = 1000

# create a new 'Warp By Vector'
warpByVector2 = WarpByVector(Input=enSightReader2)
warpByVector2.Vectors = ['POINTS', 'DISPL']

# create a new 'Calculator'
calculator1 = Calculator(Input=enSightReader1)
calculator1.ResultArrayName = 'ResultX'
calculator1.Function = 'coordsX'

# create a new 'Plot Over Line'
plotOverLine1 = PlotOverLine(Input=calculator1,
    Source='High Resolution Line Source')
plotOverLine1.Tolerance = 2.22044604925031e-16

# init the 'High Resolution Line Source' selected for 'Source'
plotOverLine1.Source.Point1 = [-0.5, 0.0, 0.1]
plotOverLine1.Source.Point2 = [0.5, 0.0, 0.1]

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'FIXNO'
fIXNOLUT = GetColorTransferFunction('FIXNO')
fIXNOLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.8660254037844386, 0.865003, 0.865003, 0.865003, 1.7320508075688772, 0.705882, 0.0156863, 0.14902]
fIXNOLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'FIXNO'
fIXNOPWF = GetOpacityTransferFunction('FIXNO')
fIXNOPWF.Points = [0.0, 0.0, 0.5, 0.0, 1.7320508075688772, 1.0, 0.5, 0.0]
fIXNOPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'lineChartView1'
# ----------------------------------------------------------------

# show data from plotOverLine1
plotOverLine1Display = Show(plotOverLine1, lineChartView1)
# trace defaults for the display properties.
plotOverLine1Display.CompositeDataSetIndex = [0]
plotOverLine1Display.UseIndexForXAxis = 0
plotOverLine1Display.XArrayName = 'ResultX'
plotOverLine1Display.SeriesVisibility = ['RESID_Magnitude']
plotOverLine1Display.SeriesLabel = ['arc_length', 'arc_length', 'DISPL_X', 'DISPL_X', 'DISPL_Y', 'DISPL_Y', 'DISPL_Z', 'DISPL_Z', 'DISPL_Magnitude', 'DISPL_Magnitude', 'FIXNO_X', 'FIXNO_X', 'FIXNO_Y', 'FIXNO_Y', 'FIXNO_Z', 'FIXNO_Z', 'FIXNO_Magnitude', 'FIXNO_Magnitude', 'NSIGM_X', 'NSIGM_X', 'NSIGM_Y', 'NSIGM_Y', 'NSIGM_Z', 'NSIGM_Z', 'NSIGM_Magnitude', 'NSIGM_Magnitude', 'RESID_X', 'RESID_X', 'RESID_Y', 'RESID_Y', 'RESID_Z', 'RESID_Z', 'RESID_Magnitude', 'RESID_Magnitude', 'ResultX', 'ResultX']
plotOverLine1Display.SeriesColor = ['arc_length', '0', '0', '0', 'DISPL_X', '0.889998', '0.100008', '0.110002', 'DISPL_Y', '0.220005', '0.489998', '0.719997', 'DISPL_Z', '0.300008', '0.689998', '0.289998', 'DISPL_Magnitude', '0.6', '0.310002', '0.639994', 'FIXNO_X', '1', '0.500008', '0', 'FIXNO_Y', '0.650004', '0.340002', '0.160006', 'FIXNO_Z', '0', '0', '0', 'FIXNO_Magnitude', '0.889998', '0.100008', '0.110002', 'NSIGM_X', '0.220005', '0.489998', '0.719997', 'NSIGM_Y', '0.300008', '0.689998', '0.289998', 'NSIGM_Z', '0.6', '0.310002', '0.639994', 'NSIGM_Magnitude', '1', '0.500008', '0', 'RESID_X', '0.650004', '0.340002', '0.160006', 'RESID_Y', '0', '0', '0', 'RESID_Z', '0.889998', '0.100008', '0.110002', 'RESID_Magnitude', '0.220005', '0.489998', '0.719997', 'ResultX', '0.300008', '0.689998', '0.289998']
plotOverLine1Display.SeriesPlotCorner = ['DISPL_Magnitude', '0', 'DISPL_X', '0', 'DISPL_Y', '0', 'DISPL_Z', '0', 'FIXNO_Magnitude', '0', 'FIXNO_X', '0', 'FIXNO_Y', '0', 'FIXNO_Z', '0', 'NSIGM_Magnitude', '0', 'NSIGM_X', '0', 'NSIGM_Y', '0', 'NSIGM_Z', '0', 'RESID_Magnitude', '0', 'RESID_X', '0', 'RESID_Y', '0', 'RESID_Z', '0', 'ResultX', '0', 'arc_length', '0']
plotOverLine1Display.SeriesLineStyle = ['DISPL_Magnitude', '1', 'DISPL_X', '1', 'DISPL_Y', '1', 'DISPL_Z', '1', 'FIXNO_Magnitude', '1', 'FIXNO_X', '1', 'FIXNO_Y', '1', 'FIXNO_Z', '1', 'NSIGM_Magnitude', '1', 'NSIGM_X', '1', 'NSIGM_Y', '1', 'NSIGM_Z', '1', 'RESID_Magnitude', '0', 'RESID_X', '1', 'RESID_Y', '1', 'RESID_Z', '1', 'ResultX', '1', 'arc_length', '1']
plotOverLine1Display.SeriesLineThickness = ['DISPL_Magnitude', '2', 'DISPL_X', '2', 'DISPL_Y', '2', 'DISPL_Z', '2', 'FIXNO_Magnitude', '2', 'FIXNO_X', '2', 'FIXNO_Y', '2', 'FIXNO_Z', '2', 'NSIGM_Magnitude', '2', 'NSIGM_X', '2', 'NSIGM_Y', '2', 'NSIGM_Z', '2', 'RESID_Magnitude', '2', 'RESID_X', '2', 'RESID_Y', '2', 'RESID_Z', '2', 'ResultX', '2', 'arc_length', '2']
plotOverLine1Display.SeriesMarkerStyle = ['DISPL_Magnitude', '0', 'DISPL_X', '0', 'DISPL_Y', '0', 'DISPL_Z', '0', 'FIXNO_Magnitude', '0', 'FIXNO_X', '0', 'FIXNO_Y', '0', 'FIXNO_Z', '0', 'NSIGM_Magnitude', '0', 'NSIGM_X', '0', 'NSIGM_Y', '0', 'NSIGM_Z', '0', 'RESID_Magnitude', '1', 'RESID_X', '0', 'RESID_Y', '0', 'RESID_Z', '0', 'ResultX', '0', 'arc_length', '0']

# show data from plotOverLine2
plotOverLine2Display = Show(plotOverLine2, lineChartView1)
# trace defaults for the display properties.
plotOverLine2Display.CompositeDataSetIndex = [0]
plotOverLine2Display.UseIndexForXAxis = 0
plotOverLine2Display.XArrayName = 'ResultX'
plotOverLine2Display.SeriesVisibility = ['RESID_Magnitude']
plotOverLine2Display.SeriesLabel = ['arc_length', 'arc_length', 'DISPL_X', 'DISPL_X', 'DISPL_Y', 'DISPL_Y', 'DISPL_Z', 'DISPL_Z', 'DISPL_Magnitude', 'DISPL_Magnitude', 'FIXNO_X', 'FIXNO_X', 'FIXNO_Y', 'FIXNO_Y', 'FIXNO_Z', 'FIXNO_Z', 'FIXNO_Magnitude', 'FIXNO_Magnitude', 'NSIGM_X', 'NSIGM_X', 'NSIGM_Y', 'NSIGM_Y', 'NSIGM_Z', 'NSIGM_Z', 'NSIGM_Magnitude', 'NSIGM_Magnitude', 'RESID_X', 'RESID_X', 'RESID_Y', 'RESID_Y', 'RESID_Z', 'RESID_Z', 'RESID_Magnitude', 'RESID_Magnitude', 'ResultX', 'ResultX']
plotOverLine2Display.SeriesColor = ['arc_length', '0', '0', '0', 'DISPL_X', '0.889998', '0.100008', '0.110002', 'DISPL_Y', '0.220005', '0.489998', '0.719997', 'DISPL_Z', '0.300008', '0.689998', '0.289998', 'DISPL_Magnitude', '0.6', '0.310002', '0.639994', 'FIXNO_X', '1', '0.500008', '0', 'FIXNO_Y', '0.650004', '0.340002', '0.160006', 'FIXNO_Z', '0', '0', '0', 'FIXNO_Magnitude', '0.889998', '0.100008', '0.110002', 'NSIGM_X', '0.220005', '0.489998', '0.719997', 'NSIGM_Y', '0.300008', '0.689998', '0.289998', 'NSIGM_Z', '0.6', '0.310002', '0.639994', 'NSIGM_Magnitude', '1', '0.500008', '0', 'RESID_X', '0.650004', '0.340002', '0.160006', 'RESID_Y', '0', '0', '0', 'RESID_Z', '0.889998', '0.100008', '0.110002', 'RESID_Magnitude', '0.220005', '0.489998', '0.719997', 'ResultX', '0.300008', '0.689998', '0.289998']
plotOverLine2Display.SeriesPlotCorner = ['DISPL_Magnitude', '0', 'DISPL_X', '0', 'DISPL_Y', '0', 'DISPL_Z', '0', 'FIXNO_Magnitude', '0', 'FIXNO_X', '0', 'FIXNO_Y', '0', 'FIXNO_Z', '0', 'NSIGM_Magnitude', '0', 'NSIGM_X', '0', 'NSIGM_Y', '0', 'NSIGM_Z', '0', 'RESID_Magnitude', '0', 'RESID_X', '0', 'RESID_Y', '0', 'RESID_Z', '0', 'ResultX', '0', 'arc_length', '0']
plotOverLine2Display.SeriesLineStyle = ['DISPL_Magnitude', '1', 'DISPL_X', '1', 'DISPL_Y', '1', 'DISPL_Z', '1', 'FIXNO_Magnitude', '1', 'FIXNO_X', '1', 'FIXNO_Y', '1', 'FIXNO_Z', '1', 'NSIGM_Magnitude', '1', 'NSIGM_X', '1', 'NSIGM_Y', '1', 'NSIGM_Z', '1', 'RESID_Magnitude', '1', 'RESID_X', '1', 'RESID_Y', '1', 'RESID_Z', '1', 'ResultX', '1', 'arc_length', '1']
plotOverLine2Display.SeriesLineThickness = ['DISPL_Magnitude', '2', 'DISPL_X', '2', 'DISPL_Y', '2', 'DISPL_Z', '2', 'FIXNO_Magnitude', '2', 'FIXNO_X', '2', 'FIXNO_Y', '2', 'FIXNO_Z', '2', 'NSIGM_Magnitude', '2', 'NSIGM_X', '2', 'NSIGM_Y', '2', 'NSIGM_Z', '2', 'RESID_Magnitude', '2', 'RESID_X', '2', 'RESID_Y', '2', 'RESID_Z', '2', 'ResultX', '2', 'arc_length', '2']
plotOverLine2Display.SeriesMarkerStyle = ['DISPL_Magnitude', '0', 'DISPL_X', '0', 'DISPL_Y', '0', 'DISPL_Z', '0', 'FIXNO_Magnitude', '0', 'FIXNO_X', '0', 'FIXNO_Y', '0', 'FIXNO_Z', '0', 'NSIGM_Magnitude', '0', 'NSIGM_X', '0', 'NSIGM_Y', '0', 'NSIGM_Z', '0', 'RESID_Magnitude', '0', 'RESID_X', '0', 'RESID_Y', '0', 'RESID_Z', '0', 'ResultX', '0', 'arc_length', '0']

# ----------------------------------------------------------------
# setup the visualization in view 'lineChartView2'
# ----------------------------------------------------------------

# show data from plotOverLine2
plotOverLine2Display_1 = Show(plotOverLine2, lineChartView2)
# trace defaults for the display properties.
plotOverLine2Display_1.CompositeDataSetIndex = [0]
plotOverLine2Display_1.UseIndexForXAxis = 0
plotOverLine2Display_1.XArrayName = 'ResultX'
plotOverLine2Display_1.SeriesVisibility = ['DISPL_Magnitude']
plotOverLine2Display_1.SeriesLabel = ['arc_length', 'arc_length', 'DISPL_X', 'DISPL_X', 'DISPL_Y', 'DISPL_Y', 'DISPL_Z', 'DISPL_Z', 'DISPL_Magnitude', 'DISPL_Magnitude', 'FIXNO_X', 'FIXNO_X', 'FIXNO_Y', 'FIXNO_Y', 'FIXNO_Z', 'FIXNO_Z', 'FIXNO_Magnitude', 'FIXNO_Magnitude', 'NSIGM_X', 'NSIGM_X', 'NSIGM_Y', 'NSIGM_Y', 'NSIGM_Z', 'NSIGM_Z', 'NSIGM_Magnitude', 'NSIGM_Magnitude', 'RESID_X', 'RESID_X', 'RESID_Y', 'RESID_Y', 'RESID_Z', 'RESID_Z', 'RESID_Magnitude', 'RESID_Magnitude', 'ResultX', 'ResultX']
plotOverLine2Display_1.SeriesColor = ['arc_length', '0', '0', '0', 'DISPL_X', '0.889998', '0.100008', '0.110002', 'DISPL_Y', '0.220005', '0.489998', '0.719997', 'DISPL_Z', '0.300008', '0.689998', '0.289998', 'DISPL_Magnitude', '0.6', '0.310002', '0.639994', 'FIXNO_X', '1', '0.500008', '0', 'FIXNO_Y', '0.650004', '0.340002', '0.160006', 'FIXNO_Z', '0', '0', '0', 'FIXNO_Magnitude', '0.889998', '0.100008', '0.110002', 'NSIGM_X', '0.220005', '0.489998', '0.719997', 'NSIGM_Y', '0.300008', '0.689998', '0.289998', 'NSIGM_Z', '0.6', '0.310002', '0.639994', 'NSIGM_Magnitude', '1', '0.500008', '0', 'RESID_X', '0.650004', '0.340002', '0.160006', 'RESID_Y', '0', '0', '0', 'RESID_Z', '0.889998', '0.100008', '0.110002', 'RESID_Magnitude', '0.220005', '0.489998', '0.719997', 'ResultX', '0.300008', '0.689998', '0.289998']
plotOverLine2Display_1.SeriesPlotCorner = ['DISPL_Magnitude', '0', 'DISPL_X', '0', 'DISPL_Y', '0', 'DISPL_Z', '0', 'FIXNO_Magnitude', '0', 'FIXNO_X', '0', 'FIXNO_Y', '0', 'FIXNO_Z', '0', 'NSIGM_Magnitude', '0', 'NSIGM_X', '0', 'NSIGM_Y', '0', 'NSIGM_Z', '0', 'RESID_Magnitude', '0', 'RESID_X', '0', 'RESID_Y', '0', 'RESID_Z', '0', 'ResultX', '0', 'arc_length', '0']
plotOverLine2Display_1.SeriesLineStyle = ['DISPL_Magnitude', '1', 'DISPL_X', '1', 'DISPL_Y', '1', 'DISPL_Z', '1', 'FIXNO_Magnitude', '1', 'FIXNO_X', '1', 'FIXNO_Y', '1', 'FIXNO_Z', '1', 'NSIGM_Magnitude', '1', 'NSIGM_X', '1', 'NSIGM_Y', '1', 'NSIGM_Z', '1', 'RESID_Magnitude', '1', 'RESID_X', '1', 'RESID_Y', '1', 'RESID_Z', '1', 'ResultX', '1', 'arc_length', '1']
plotOverLine2Display_1.SeriesLineThickness = ['DISPL_Magnitude', '2', 'DISPL_X', '2', 'DISPL_Y', '2', 'DISPL_Z', '2', 'FIXNO_Magnitude', '2', 'FIXNO_X', '2', 'FIXNO_Y', '2', 'FIXNO_Z', '2', 'NSIGM_Magnitude', '2', 'NSIGM_X', '2', 'NSIGM_Y', '2', 'NSIGM_Z', '2', 'RESID_Magnitude', '2', 'RESID_X', '2', 'RESID_Y', '2', 'RESID_Z', '2', 'ResultX', '2', 'arc_length', '2']
plotOverLine2Display_1.SeriesMarkerStyle = ['DISPL_Magnitude', '0', 'DISPL_X', '0', 'DISPL_Y', '0', 'DISPL_Z', '0', 'FIXNO_Magnitude', '0', 'FIXNO_X', '0', 'FIXNO_Y', '0', 'FIXNO_Z', '0', 'NSIGM_Magnitude', '0', 'NSIGM_X', '0', 'NSIGM_Y', '0', 'NSIGM_Z', '0', 'RESID_Magnitude', '0', 'RESID_X', '0', 'RESID_Y', '0', 'RESID_Z', '0', 'ResultX', '0', 'arc_length', '0']

# show data from plotOverLine1
plotOverLine1Display_1 = Show(plotOverLine1, lineChartView2)
# trace defaults for the display properties.
plotOverLine1Display_1.CompositeDataSetIndex = [0]
plotOverLine1Display_1.UseIndexForXAxis = 0
plotOverLine1Display_1.XArrayName = 'ResultX'
plotOverLine1Display_1.SeriesVisibility = ['DISPL_Magnitude']
plotOverLine1Display_1.SeriesLabel = ['arc_length', 'arc_length', 'DISPL_X', 'DISPL_X', 'DISPL_Y', 'DISPL_Y', 'DISPL_Z', 'DISPL_Z', 'DISPL_Magnitude', 'DISPL_Magnitude', 'FIXNO_X', 'FIXNO_X', 'FIXNO_Y', 'FIXNO_Y', 'FIXNO_Z', 'FIXNO_Z', 'FIXNO_Magnitude', 'FIXNO_Magnitude', 'NSIGM_X', 'NSIGM_X', 'NSIGM_Y', 'NSIGM_Y', 'NSIGM_Z', 'NSIGM_Z', 'NSIGM_Magnitude', 'NSIGM_Magnitude', 'RESID_X', 'RESID_X', 'RESID_Y', 'RESID_Y', 'RESID_Z', 'RESID_Z', 'RESID_Magnitude', 'RESID_Magnitude', 'ResultX', 'ResultX']
plotOverLine1Display_1.SeriesColor = ['arc_length', '0', '0', '0', 'DISPL_X', '0.889998', '0.100008', '0.110002', 'DISPL_Y', '0.220005', '0.489998', '0.719997', 'DISPL_Z', '0.300008', '0.689998', '0.289998', 'DISPL_Magnitude', '0.6', '0.310002', '0.639994', 'FIXNO_X', '1', '0.500008', '0', 'FIXNO_Y', '0.650004', '0.340002', '0.160006', 'FIXNO_Z', '0', '0', '0', 'FIXNO_Magnitude', '0.889998', '0.100008', '0.110002', 'NSIGM_X', '0.220005', '0.489998', '0.719997', 'NSIGM_Y', '0.300008', '0.689998', '0.289998', 'NSIGM_Z', '0.6', '0.310002', '0.639994', 'NSIGM_Magnitude', '1', '0.500008', '0', 'RESID_X', '0.650004', '0.340002', '0.160006', 'RESID_Y', '0', '0', '0', 'RESID_Z', '0.889998', '0.100008', '0.110002', 'RESID_Magnitude', '0.220005', '0.489998', '0.719997', 'ResultX', '0.300008', '0.689998', '0.289998']
plotOverLine1Display_1.SeriesPlotCorner = ['DISPL_Magnitude', '0', 'DISPL_X', '0', 'DISPL_Y', '0', 'DISPL_Z', '0', 'FIXNO_Magnitude', '0', 'FIXNO_X', '0', 'FIXNO_Y', '0', 'FIXNO_Z', '0', 'NSIGM_Magnitude', '0', 'NSIGM_X', '0', 'NSIGM_Y', '0', 'NSIGM_Z', '0', 'RESID_Magnitude', '0', 'RESID_X', '0', 'RESID_Y', '0', 'RESID_Z', '0', 'ResultX', '0', 'arc_length', '0']
plotOverLine1Display_1.SeriesLineStyle = ['DISPL_Magnitude', '0', 'DISPL_X', '1', 'DISPL_Y', '1', 'DISPL_Z', '1', 'FIXNO_Magnitude', '1', 'FIXNO_X', '1', 'FIXNO_Y', '1', 'FIXNO_Z', '1', 'NSIGM_Magnitude', '1', 'NSIGM_X', '1', 'NSIGM_Y', '1', 'NSIGM_Z', '1', 'RESID_Magnitude', '1', 'RESID_X', '1', 'RESID_Y', '1', 'RESID_Z', '1', 'ResultX', '1', 'arc_length', '1']
plotOverLine1Display_1.SeriesLineThickness = ['DISPL_Magnitude', '2', 'DISPL_X', '2', 'DISPL_Y', '2', 'DISPL_Z', '2', 'FIXNO_Magnitude', '2', 'FIXNO_X', '2', 'FIXNO_Y', '2', 'FIXNO_Z', '2', 'NSIGM_Magnitude', '2', 'NSIGM_X', '2', 'NSIGM_Y', '2', 'NSIGM_Z', '2', 'RESID_Magnitude', '2', 'RESID_X', '2', 'RESID_Y', '2', 'RESID_Z', '2', 'ResultX', '2', 'arc_length', '2']
plotOverLine1Display_1.SeriesMarkerStyle = ['DISPL_Magnitude', '1', 'DISPL_X', '0', 'DISPL_Y', '0', 'DISPL_Z', '0', 'FIXNO_Magnitude', '0', 'FIXNO_X', '0', 'FIXNO_Y', '0', 'FIXNO_Z', '0', 'NSIGM_Magnitude', '0', 'NSIGM_X', '0', 'NSIGM_Y', '0', 'NSIGM_Z', '0', 'RESID_Magnitude', '0', 'RESID_X', '0', 'RESID_Y', '0', 'RESID_Z', '0', 'ResultX', '0', 'arc_length', '0']

# ----------------------------------------------------------------
# setup the visualization in view 'renderView2'
# ----------------------------------------------------------------

# show data from warpByVector1
warpByVector1Display = Show(warpByVector1, renderView2)
# trace defaults for the display properties.
warpByVector1Display.Representation = 'Wireframe'
warpByVector1Display.ColorArrayName = ['POINTS', 'FIXNO']
warpByVector1Display.LookupTable = fIXNOLUT
warpByVector1Display.ScalarOpacityUnitDistance = 822.9413526909449

# show data from warpByVector2
warpByVector2Display = Show(warpByVector2, renderView2)
# trace defaults for the display properties.
warpByVector2Display.Representation = 'Surface With Edges'
warpByVector2Display.ColorArrayName = ['POINTS', 'FIXNO']
warpByVector2Display.LookupTable = fIXNOLUT
warpByVector2Display.ScalarOpacityUnitDistance = 0.38952313966791796
