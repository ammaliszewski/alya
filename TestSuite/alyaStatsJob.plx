#!/usr/bin/perl


#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

#Parameters
my $docFolder = "log";
my $caseServer = "case\@bsccase02.bsc.es";
my $docServerFolder = "/srv/www/htdocs/case/tests";

#Run stats
my $output = `python stats.py 90`;
print $output;


chdir "log";
#copy stats report to the server
$output = `scp -r report.tar.gz $caseServer:$docServerFolder`;
print $output;

#extract html docs tar in the server
$output = `ssh $caseServer 'cd $docServerFolder; tar -xvzf report.tar.gz'`;
print $output;

#remove html docs tar in the server
$output = `ssh $caseServer 'cd $docServerFolder; rm -f report.tar.gz'`;
print $output;

#remove local tar server
$output = `rm -f report.tar.gz`;
print $output;

exit 0
