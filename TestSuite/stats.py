#!/usr/bin/env python3

import os
#import numpy as np
#import math
import sys
import tarfile
import xml.etree.ElementTree as ET
from datetime import datetime , timedelta
import shutil
from ordereddict import OrderedDict
from lxml import etree as xsltParser


# Each module .cvg is formatted different:
# module (file extension)
# cputime (column to read)
# headlen (rows to ignore at the beginning of the file)
# Add more as necesary

##################################################
# ALYA TIMESTEP STATISTICS GENERATOR
##################################################
numDays = int(sys.argv[1])
today = datetime.now()
reportFolder = 'report'
os.chdir("log")
if (os.path.isdir(reportFolder)):
	shutil.rmtree(reportFolder)
os.makedirs(reportFolder)
modulesList = OrderedDict()
datesDict = OrderedDict()

#-----------------------------------------------------
#1.parse testSuite xml into modulesList and datesDict
#-----------------------------------------------------
for idx in range(numDays - 1, -1 , -1):
	dateIdx = today - timedelta(days=idx)
	#print 'la fecha a tratar es:' + str(dateIdx)
	
	#prepare the modulesDict
	modulesDict = OrderedDict()	
	
	#get report name
	reportName = dateIdx.strftime('report_%b_%d_%Y.tar.gz')
	if os.path.isfile(reportName):
		#Untar the corresponding report
		tar = tarfile.open(reportName)
		tar.extractall()
		tar.close()
		
		#go into the report
		os.chdir("rep")
		
		#parse de report
		try:
			tsReport = ET.parse('testSuiteResult.xml')	
			for moduleResult in tsReport.findall('moduleResults/moduleResult'):
				moduleName = moduleResult.find('moduleName').text
				#prepare the seriesDict
				seriesDict = OrderedDict()
				
				#open the modulename report
				moduleReport = ET.parse(moduleName + '/' + moduleName + '.xml')
				for testResult in moduleReport.findall('testResults/testResult'):
					testName = testResult.find('testName').text
					#open the test details
					testReport = ET.parse(moduleName + '/' + testName + '/' + testName + '.xml')
					for result in testReport.findall('results/result'):
						mpiProcesses = result.find('mpiProcesses').text
						try:
							compilationId = result.find('compilationId').text
						except AttributeError:
							compilationId = "";
						cputime = 0
						if (result.find('performance/CPUTimeAtEachStep') != None):
							cputime = result.find('performance/CPUTimeAtEachStep').text				
						seriesDict[testName + '_' + mpiProcesses + 'mpi' + '_' + compilationId] = cputime
						
				#update modulesDict		
				modulesDict[moduleName] = seriesDict
				modulesList[moduleName] = moduleName
		except IOError:
			print "Oops!  testSuiteResult.xml in " + reportName + " doesn't exists"
		
		#update datesdict
		datesDict[dateIdx.strftime('%d_%b_%Y')] = modulesDict
		os.chdir("../")	
		shutil.rmtree('rep')


#print "Start creating charts"

#------------------------------------
#2.Create google charts html report
#------------------------------------

#format:
#['Date', 'Test1-1p', 'Test2-2p'],
#['2004',  1000,      400],
#['2005',  1170,      460],
#['2006',  660,       1120],
#['2007',  1030,      540]

#Fore each module creates a module_name.html report
for moduleKey, moduleValue in modulesList.items():
	#print "ModuleName:" + moduleKey
	
	#load the template into string
	chart = ''
	with open ("chartTemplate.html", "r") as myfile:
		chart=myfile.read()

	#replace the title
	chart = chart.replace('<<title>>', moduleKey);
	
	#Determines the series
	seriesDict = OrderedDict()
	for dateIdx, modulesDict in datesDict.items():
		if moduleKey in modulesDict:
			#print "DateIdx:" + dateIdx
			seriesDictAux = modulesDict[moduleKey]
			for serieIdx, serieValue in seriesDictAux.items():
				seriesDict[serieIdx] = serieIdx
				#print "SerieIdx:" + serieIdx + " ,serieValue:" + str(serieValue)
			
	#With the series creates the data header
	data = "['Date'"
	for serieIdx, serieValue in seriesDict.items():
		data = data + ", '" + serieIdx + "'"
	data = data + "],"
	
	#creates each date line
	totalDates = len (datesDict)
	idx = 1
	for dateIdx, modulesDict in datesDict.items():
		if moduleKey in modulesDict:
			data = data + "['" + dateIdx + "'"
			seriesDictMod = modulesDict[moduleKey]		
			#creates series values
			for serieIdx, serieValue in seriesDict.items():
				#series values
				if serieIdx in seriesDictMod:
					value = seriesDictMod[serieIdx]
					if value == 0:
						data = data + ", null"
					else:
						data = data + ", " + str(value)
				else:
					data = data + ", null"
			data = data + "]"
			if idx < totalDates:
				data = data + ","
		idx = idx + 1
		
	chart = chart.replace('<<data>>', data);
	#save the report	
	text_file = open(reportFolder + "/" + moduleKey + ".html", "w")
	text_file.write(chart)
	text_file.close()

#Now creates a the main index.html report page that
#contains links to the other module google charts

#load the template into string
indexHTML = ''
with open ("indexTemplate.html", "r") as myfile:
	indexHTML=myfile.read().replace('\n', '')
moduleData = ''
for moduleKey, moduleValue in modulesList.items():
	moduleData = moduleData + '<tr class="version odd open ">'
	moduleData = moduleData + '<td class="name" align="center">' + moduleKey + '</td>'
	moduleData = moduleData + '<td class="buttons"  align="center">'
	moduleData = moduleData + '<a class="icon icon-edit" href="' + moduleKey + '.html">'
	moduleData = moduleData + 'Open</a>'
	moduleData = moduleData + '</td>'
	moduleData = moduleData + '</tr>'
	
indexHTML = indexHTML.replace('<<data>>', moduleData)
totalDates = len (datesDict)
fromDateKey, fromDateValue = datesDict.items()[0]
toDateKey, toDateValue = datesDict.items()[totalDates - 1]
indexHTML = indexHTML.replace('<<from>>', fromDateKey)
indexHTML = indexHTML.replace('<<to>>', toDateKey)
#save the report	
text_file = open(reportFolder + "/index.html", "w")
text_file.write(indexHTML)
text_file.close()

#copy the css style
shutil.copytree("../libraries/libs", reportFolder + "/libs")
#zip the report
#Untar the corresponding report
tar = tarfile.open("report.tar.gz", "w:gz")
tar.add(reportFolder, arcname=os.path.basename(reportFolder))
tar.close()
shutil.rmtree(reportFolder)

##################################################
# END ALYA TIMESTEP STATISTICS GENERATOR
##################################################
