#!/usr/bin/perl

#usage modules
use FindBin;                 # locate this script
use lib "$FindBin::Bin/libraries";  # use the parent directory
use File::Copy;
use File::Path;
use File::Find;
use File::Compare;
use File::Copy::Recursive qw(dirmove);
use File::Copy::Recursive qw(dircopy);
use XML::Simple;
use Data::Dumper;
use POSIX;
use ALYA::ExecFactory;
use ALYA::Configures;
use Getopt::Std;

#global variables
$basePath = "modules/";
my $reportDir = "rep";
$alyaExec = "~/alya/Executables/unix/Alya.x";
$testId = "";
$columnCompared = 6;

#----------------------------------------------------------------------
#-------------------------------functions------------------------------
#----------------------------------------------------------------------


#----------------------------------------------------------------------
#--Function configTestSuite
#--Description:
#----Creates the testSuite xml configuration file from the testSuiteJob xml configuration
#--Parameters:
#----xml: pointer to the utility to create an xml from a perl hash
#----data: Pointer to a var that contains the testSuiteJob xml configuration
#----filePath: The path where the testSuite configuration file has to be written
sub configureTestSuite {
        local($xml, $moduleCompilation, $filePath, $modulesPath, $bigExecutionArg) = @_;

    my $config = {};
	$config->{testSuite} = {};
	$config->{testSuite}->{common}->{testSuiteName} = "ALYA";
	$config->{testSuite}->{common}->{executable} = "../../../../../Executables/unix/Alya.x";
	$config->{testSuite}->{common}->{alya2pos} = "../../../../../Utils/user/alya2pos/alya2pos.x";
	$execModule = $data->{execModule};
	if (!$execModule) {
		$execModule = "LocalExec";
	}
	$config->{testSuite}->{common}->{execModule} = $execModule;
	$config->{testSuite}->{common}->{doReport} = "false";
	if ($bigExecutionArg) {
		$config->{testSuite}->{common}->{bigExecution} = "true";	
	}	
	$config->{testSuite}->{modules}->{module} = [];
    my $modules = $config->{testSuite}->{modules};

=pod
	$modulesTest = $data->{configure}->{module};
	@tests = $modulesTest;
	if (ref $modulesTest eq 'ARRAY') {
		@tests = @{$modulesTest};
	}	
	#creates the configure command
	foreach $test (@tests) {
		$module = {};
                $module->{moduleName} = $test;
                push(@{$modules->{module}}, $module);
	}
=cut
    #list al module test folders
    print "modulesPath path:$modulesPath\n";
    opendir DH, $modulesPath or die "Couldn't open the current directory: $!";
	while ($_ = readdir(DH)) {
		print "DIR: $_\n";
		my $moduleName = $_;
		next if $moduleName eq "." or $moduleName eq ".." or $moduleName eq ".svn";
		if (-d "$modulesPath/$moduleName") {
			print "IS DIR: $_\n";			
			# Check if the module has passed the tests
			my @modulesName = split("-", $moduleName);
			my $passed = "OK";			
			foreach $name (@modulesName) {				
				print "moduleName: $name\n";
				my $found = 0;
				#--Test if this module has passed the compilation
				$testCompResult = $moduleCompilation->{moduleCompilationResult};
				@testCompResultsList = $testCompResult;
				if (ref $testCompResult eq 'ARRAY') {
					@testCompResultsList = @{$testCompResult};
				}			
				foreach $testCompResult (@testCompResultsList) {
					my $modName = $testCompResult->{moduleName};
					print "testCompResult:$modName\n";
					if ($modName eq $name) {
						$found = 1;
						if ($testCompResult->{passed} ne "true") {
							$passed = "FAIL";
							print "FAILED:$name\n";
						}	
					}		
				}
				#Si no se ha encontrado el modulo es que no se ha intentado compilar
				if (!$found) {
					$passed = "FAIL";
				}
			}
			#if module compiled correctly then add his test
			if ($passed eq "OK") {
				my $module = {};
				$module->{moduleName} = $moduleName;
				push(@{$modules->{module}}, $module);
			}
		}				
	}
		
	$xml->XMLout(
	    $config,
	    KeepRoot => 1,
	    NoAttr => 1,
	    OutputFile => $filePath,
	);

}


#----------------------------------------------------------------------
#--Function checkOutDone
#--Description:
#----Returns true if checkout to alya svn repos was successfull and false otherwise
#--Parameters:
#----svnName: The name of the folder where the checkout was done
sub checkOutDone {
        $checkOutDone = 0;
	local($svnName) = @_;

	if (-d "$svnName/Executables" && -d "$svnName/Sources" && -d "$svnName/TestSuite" && -d "$svnName/Thirdparties") {
		$checkOutDone = 1;
	}

	return $checkOutDone;
}


#----------------------------------------------------------------------
#--Function checkMetisCompilation
#--Description:
#----Returns true if metis compilation was successfull and false otherwise
#--Parameters:
#----metisPath: The path where metis was compilated
sub checkMetisCompilation {
	local($metisPath) = @_;
        $metisCompiled = 0;

	if (-f "$metisPath/pmetis") {
		$metisCompiled = 1;
	}

	return $metisCompiled;
}


#----------------------------------------------------------------------
#--Function configureDone
#--Description:
#----Returns true if the configure operation has succesfully terminated, false otherwise
#--Parameters:
#----confPath: The path where configure was done
sub configureDone {
	local($confPath) = @_;
        $configureDone = 0;
	
	print "CONF PATH:" . "$confPath/makefile\n";
	if (-f $confPath . "makefile") {
		$configureDone = 1;
	}

	return $configureDone;
}


#----------------------------------------------------------------------
#--Function makeDone
#--Description:
#----returns true if the make Alya operation was done succesfully, false otherwise
#--Parameters:
#----makePath: The path where make was done
sub makeDone {
	local($makePath) = @_;
        $makeDone = 0;

	if (-f $makePath . "Alya.x") {
		$makeDone = 1;
	}

	return $makeDone;
}


#----------------------------------------------------------------------
#--Function alya2posDone
#--Description:
#----returns true if the alya2pos compilation was successfully done, false otherwise
#--Parameters:
#----alya2posPath: The path were alya2pos was compiled
sub alya2posDone {
	local($alya2posPath) = @_;
        $alya2posDone = 0;

	if (-f "$alya2posPath/alya2pos.x") {
		$alya2posDone = 1;
	}

	return $alya2posDone;
}


#----------------------------------------------------------------------
#--Function testSuiteDone
#--Description:
#----Returns true if the testSuite execution was successfull, false otherwise
#--Parameters:
#----testSuitePath: The path were the testSuite was launched
sub testSuiteDone {
	local($testSuitePath) = @_;
        $testSuiteDone = 0;

	if (-f "$testSuitePath/testSuiteResult.xml") {
		$testSuiteDone = 1;
	}

	return $testSuiteDone;
}


#----------------------------------------------------------------------
#--Function tarReport
#--Description:
#----creates a tar.gz file
#--Parameters:
#----dirName: The folder name that has to be compressed
#----archive: The name of the tar.gz file to generate
sub tarReport {
	local($dirName, $archive) = @_;

	# Create inventory of files & directories
	#my @inventory = ();
	#print "DIR:" . $dir;
	#find (sub { push @inventory, $File::Find::name }, $dir);

	# Create a new tar object
	#my $tar = Archive::Tar->new();
	#print "Inventori:" . Dumper($inventory);
	#$tar->add_files( @inventory );

	# Write compressed tar file
	#$tar->write( $archive, 9 );
	$result = `tar -cvzf $archive $dirName`;
	print "Result:" . $result . "\n";
}


#----------------------------------------------------------------------
#--Function saveError
#--Description:
#----Saves to a file the 30 last lines of a given output
#--Parameters:
#----path:The path where to save the file
#----test:The name of the file to save
#----stdout: The content to save into the file
sub saveError {
	local($path, $test, $stdout) = @_;

		
	my @lines = split(/\n/, $stdout);
	my $numLines = scalar (@lines);
	my $finalLines = join("\n", @lines[($numLines-30) .. ($numLines-1)]);

	#save to disc the output
	open FILE, ">$path/$test.txt" or die $!;
	print FILE $finalLines;
	close FILE;

	return "$test.txt";

}

#----------------------------------------------------------------------
#--Function generateReportName
#--Description:
#----Genereates the report file name, that includes the current time, the module name and the test name.
#--Parameters:
#----moduleName: The module's name that it's tested
#----testName: The test name.
sub generateReportName {
	#get local time in format: MMM_DD_YYYY
	$now_string = strftime "%b_%d_%Y", localtime;

	#Compound the file name
	$fileName = "report_" . $now_string;

	return $fileName;
}

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#--------------------------------MAIN-----------------------------------------------------------
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
print "Server begins\n";

#argumentos opcionales -b(bigExecution)
getopts('b');
our($opt_b);
my $bigExecutionArg = $opt_b || "0";

#xml data
$xml = new XML::Simple;
$data = $xml->XMLin("testSuiteJob.xml");

#report xml initialization
my $svnName = "Report";
my $report = {};
$report->{testSuiteJobResult} = {};
$report->{testSuiteJobResult}->{moduleCompilation}->{moduleCompilationResult} = [];
$report->{testSuiteJobResult}->{serviceCompilation}->{serviceCompilationResult} = [];
$moduleCompilation = $report->{testSuiteJobResult}->{moduleCompilation};
$serviceCompilation = $report->{testSuiteJobResult}->{serviceCompilation};
rmtree($reportDir );
mkdir($reportDir );

$errors = "No errors";

$execution = ALYA::ExecFactory->instantiate("LocalExec");

if (!checkOutDone($svnName)) {
	$errors = "svn checkout failed";
}

print "Server begins2\n";
#2.1.compile metis
if ($errors eq "No errors") {

	if ($data->{metisPath}) {
		print "Compile metis: $svnName/$data->{metisPath} \n";
		chdir "$svnName/$data->{metisPath}";
		$output = $execution->execCommand("make" , 120);
		chdir "../../../";		
	}

	if (!checkMetisCompilation("$svnName/$data->{metisPath}")) {
		$errors = "Metis not compiled correctly";	
	}

}

#2.2.compile libple

if ($errors eq "No errors") {
	chdir "$svnName/Executables/unix";
	my $confResult = ALYA::Configures::createConfigure("standard");
    if ($confResult) {
	   print "Error trying to do standard configure creation:" . $confResult . "\n";
    }
    #2.2.1 configure to create makefile
	my $confCommand = "./configure -x";
    my $output = $execution->execCommand($confCommand , 120);
	if (configureDone("")) {
		$stdout = $execution->execCommand("make libple" , 180);
		print "libple: " . $stdout;
		$stdout = $execution->execCommand("make libplepp" , 180);
		print "libplepp:" . $stdout;
	}	
	chdir "../../../";
}

#3.configure and make each module and service one by one and log the result
if ($errors eq "No errors") {
	#exec the configure
	chdir "$svnName/Executables/unix";

	#Modules
	$modulesTest = $data->{configure}->{module};
	@tests = $modulesTest;
	if (ref $modulesTest eq 'ARRAY') {
		@tests = @{$modulesTest};
	}	
	#creates the configure command and executes make for each module
	foreach $test (@tests) {
		my $confCommand = "./configure ";
		#log
		$result = {};
		$result->{moduleName} = $test;
		push(@{$moduleCompilation->{moduleCompilationResult}}, $result);
	
		#clean
		$stdout = $execution->execCommand("make clean" , 3600);
		#configure
		$confCommand = $confCommand . $test . " -x";
		$output = $execution->execCommand($confCommand , 120);
		if (!configureDone("")) {
			$result->{passed} = "false";
			$result->{error} = "configure not executed correctly";
		}
		else {
			#.make			
			$stdout = $execution->execCommand("make -j 8" , 3600);
			if (!makeDone("")) {
				$result->{passed} = "false";
				$result->{error} = "Alya compilation errors";
				$result->{file} = saveError("../../../$reportDir", $test, $stdout);
			}
			else {
				$result->{passed} = "true";
			}	
		}
	}
	#Services
	$modulesTest = $data->{configure}->{service};
	@tests = $modulesTest;
	if (ref $modulesTest eq 'ARRAY') {
		@tests = @{$modulesTest};
	}	
	#creates the configure command and executes make for each service
	foreach $test (@tests) {
		$confCommand = "./configure ";
		#log
		$result = {};
		$result->{serviceName} = $test;
		push(@{$serviceCompilation->{serviceCompilationResult}}, $result);
	
		#clean
		$stdout = $execution->execCommand("make clean" , 3600);
		#configure
		$confCommand = $confCommand . $test . " -x";
		print "ConfCommand:" . $confCommand . "\n";
		$output = $execution->execCommand($confCommand , 120);
		if (!configureDone("")) {
			$result->{passed} = "false";
			$result->{error} = "configure not executed correctly";
		}
		else {
			#.make			
			$stdout = $execution->execCommand("make -j 8" , 3600);
			if (!makeDone("")) {
				$result->{passed} = "false";
				$result->{error} = "Alya compilation errors";
				$result->{file} = saveError("../../../$reportDir", $test, $stdout);
			}
			else {
				$result->{passed} = "true";
			}	
		}
	}
}

#4.creates the configure line for the modules and services that passed the compilation
my $configureBase;
if ($errors eq "No errors") {
	$confCommand = "./configure ";
	#Modules
	$modulesCompResult = $moduleCompilation->{moduleCompilationResult};
	@modCompResults = $modulesCompResult;
	if (ref $modulesCompResult eq 'ARRAY') {
		@modCompResults = @{$modulesCompResult};
	}	
	#creates the configure command
	foreach $modCompResult (@modCompResults) {
		if ($modCompResult->{passed} eq "true") {
			$confCommand = $confCommand . $modCompResult->{moduleName} . " ";
		}		
	}
	#Services
	$servicesCompResult = $serviceCompilation->{serviceCompilationResult};
	@serCompResults = $servicesCompResult;
	if (ref $servicesCompResult eq 'ARRAY') {
		@serCompResults = @{$servicesCompResult};
	}	
	#creates the configure command
	foreach $serCompResult (@serCompResults) {
		if ($serCompResult->{passed} eq "true") {
			$confCommand = $confCommand . $serCompResult->{serviceName} . " ";
		}	
	}
	$configureBase = $confCommand;
}

#5.exec the compilations tests with the basis configure
if ($errors eq "No errors" && $data->{testCompilations}->{compilation}) {
	print "Starting TestCompilations:\n";
	#report
	$report->{testSuiteJobResult}->{testCompilation}->{testCompilationResult} = [];
	$testCompilation = $report->{testSuiteJobResult}->{testCompilation};
	my $compilations = $data->{testCompilations}->{compilation};
	@compilationsList = $compilations;
	if (ref $compilations eq 'ARRAY') {
		@compilationsList = @{$compilations};
	}	
	#creates the configure command
	my $compIdx = 0;
	foreach $compilation (@compilationsList) {
		print "Starting compilation:" . $compilation->{compilationDescription} . "\n";
		#log
		$result = {};
		$result->{compilationDescription} = $compilation->{compilationDescription};
		$result->{compilationId} = $compilation->{compilationId};
		my $compilationId = $compilation->{compilationId};
		push(@{$testCompilation->{testCompilationResult}}, $result);

		#clean
        $stdout = $execution->execCommand("make clean" , 3600);
        
        #¡¡¡---TEMPORAL----FOR IBERDROLA----!!!
		if ($compilationId eq "iberdrola") {
			#open the nsi_turbul.f90 file
			open FILE, "<../../Sources/modules/nastin/nsi_turbul.f90";
			my $nsi_turbul_in = do { local $/; <FILE> };
			close FILE;
			#modify the file
			$nsi_turbul_in =~ s/!!!#define ransopenint/#define ransopenint/;
			$nsi_turbul_in =~ s/usa def_turbul/use def_turbul/;
			#save the changes
			open FILE, ">../../Sources/modules/nastin/nsi_turbul.f90";
			print FILE $nsi_turbul_in;
			close FILE;	
		}
        #¡¡¡---FIN TEMPORAL----FOR IBERDROLA----!!!
        
		#configure
		$confResult = 0;
		$confResult = ALYA::Configures::createConfigure($compilation->{compilationId});
		if ($confResult) {
			$result->{passed} = "false";
			$result->{error} = "createConfigure not executed correctly: $confResult";
		}
		else {
			my $confCommand = $configureBase . " -x";
			#----------------------------------------------------
			$output = $execution->execCommand($confCommand , 120);
			if (!configureDone("")) {
				$result->{passed} = "false";
				$result->{error} = "configure not executed correctly";
			}
			else {
				#.make
				my $makeParameters = $compilation->{compilationParams};
				$stdout = $execution->execCommand("make ". $makeParameters . " -j 8" , 3600);
				if (!makeDone("")) {
					$result->{passed} = "false";
					$result->{error} = "Alya compilation errors";
					$result->{file} = saveError("../../../$reportDir", "compilation$compIdx", $stdout);
				}
				else {
					$result->{passed} = "true";
					#rename of the alya exec file to Alya_compilationId.x
					$stdout = $execution->execCommand("mv Alya.x Alya_$compilationId.x" , 30);
				}	
			}						
		}
				
		#¡¡¡---TEMPORAL----FOR IBERDROLA----!!!
		
		if ($compilationId eq "iberdrola") {
			#open the nsi_turbul.f90 file
			open FILE, "<../../Sources/modules/nastin/nsi_turbul.f90";
			my $nsi_turbul_in = do { local $/; <FILE> };
			close FILE;
			#modify the file
			$nsi_turbul_in =~ s/#define ransopenint/!!!#define ransopenint/;
			$nsi_turbul_in =~ s/use def_turbul/usa def_turbul/;
			#save the changes
			open FILE, ">../../Sources/modules/nastin/nsi_turbul.f90";
			print FILE $nsi_turbul_in;
			close FILE;
		}
        #¡¡¡---FIN TEMPORAL----FOR IBERDROLA----!!!
		#----------------------------------------------------		
	}	
}

#6.exec the configure only with modules and services that passed the compilation
if ($errors eq "No errors") {
        #clean
        $stdout = $execution->execCommand("make clean" , 3600);
	$confCommand = "./configure ";
	#Modules
	$modulesCompResult = $moduleCompilation->{moduleCompilationResult};
	@modCompResults = $modulesCompResult;
	if (ref $modulesCompResult eq 'ARRAY') {
		@modCompResults = @{$modulesCompResult};
	}	
	#creates the configure command
	foreach $modCompResult (@modCompResults) {
		if ($modCompResult->{passed} eq "true") {
			$confCommand = $confCommand . $modCompResult->{moduleName} . " ";
		}		
	}
	#Services
	$servicesCompResult = $serviceCompilation->{serviceCompilationResult};
	@serCompResults = $servicesCompResult;
	if (ref $servicesCompResult eq 'ARRAY') {
		@serCompResults = @{$servicesCompResult};
	}	
	#creates the configure command
	foreach $serCompResult (@serCompResults) {
		if ($serCompResult->{passed} eq "true") {
			$confCommand = $confCommand . $serCompResult->{serviceName} . " ";
		}	
	}
	
    $confResult = 0;
	$confResult = ALYA::Configures::createConfigure("standard");		
	if ($confResult) {
		$errors = "configure not executed correctly:$confResult";
		chdir "../../../";
	}
	else {
		$confCommand = $confCommand . "-x";
		$output = $execution->execCommand($confCommand , 120, "../../../");

		if (!configureDone("")) {
			$errors = "configure not executed correctly";
			chdir "../../../";
		}
	}
	
	
}

#7.make
if ($errors eq "No errors") {
	#log
	$result = {};
	$result->{moduleName} = "All";
	push(@{$moduleCompilation->{moduleCompilationResult}}, $result);

	#exec make
	$output = $execution->execCommand("make -j 8" , 3600, "../../../");
	if (!makeDone("")) {
		$errors = "Alya compilation errors";
		chdir "../../../";
		$result->{passed} = "false";
		$result->{error} = "Alya compilation errors";
		$result->{file} = saveError($reportDir, $test, $output);		
	}
	else {
		$result->{passed} = "true";
	}	
}

#8.compile alya2pos
if ($errors eq "No errors") {
	
	$output = `chmod +x alya2pos-compile.sh`;
	$output = $execution->execCommand("./alya2pos-compile.sh" , 3600);	
	chdir "../../../";
	if (!alya2posDone("$svnName/Utils/user/alya2pos")) {
		#$errors = "Alya2pos compilation errors";
		$report->{testSuiteJobResult}->{alya2posCompilation} = {};
		$report->{testSuiteJobResult}->{alya2posCompilation}->{passed} = "false";
		$report->{testSuiteJobResult}->{alya2posCompilation}->{error} = "Alya2pos compilation errors";
		$report->{testSuiteJobResult}->{alya2posCompilation}->{file} = saveError($reportDir, "alya2pos", $output);
	}	
}



#9.exec the testSuite
#Obtain testSuiteName
my $tSuiteName = generateReportName();
if ($errors eq "No errors") {
	configureTestSuite($xml, $moduleCompilation, "$svnName/TestSuite/testSuite.xml", "$svnName/TestSuite/modules", $bigExecutionArg);
	chdir "$svnName/TestSuite";
	$output = `chmod +x testSuite.plx`;
	$output = $execution->execCommand("./testSuite.plx" , 28800);
	chdir "../../";
	if (!testSuiteDone("$svnName/TestSuite/$tSuiteName")) {
		$errors = "TestSuite execution problems";
	}
}

#10.write the report
$xml->XMLout(
	$report,
    	KeepRoot => 1,
	NoAttr => 1,
	OutputFile => $reportDir . "/testSuiteJobResult.xml",
);

#zip result
#10.1.copy the errors to the report directory
open FILE, ">>$reportDir/errors.txt" or die $!;	
print FILE $errors . "\n";
close FILE;
#10.2.copy the output to the report directory
open FILE, ">>$reportDir/output.txt" or die $!;	
print FILE $output . "\n";
close FILE;
#Copy the entire testSuite report folder into reportdir
dircopy("$svnName/TestSuite/$tSuiteName", $reportDir);
#10.3.tar the report
tarReport($reportDir, "report.tar.gz");

#11.remove the folders
print "Antes TEST SUITE FINISHED\n";
#rmtree($svnName);
#rmtree($reportDir);
print "TEST SUITE FINISHED\n";

exit 0
