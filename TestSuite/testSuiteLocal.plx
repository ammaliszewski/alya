#!/usr/bin/perl
# configuration
use FindBin;                # locate this script
use lib "$FindBin::Bin/libraries";  # use the parent directory
use File::Copy;
use File::Path;
use File::Compare;
use File::Copy qw(move);
use File::Find;
use Data::Dumper;
use XML::Simple;
use POSIX;

#----------------------------------------------------------------------
#--Function configTestSuite
#--Description:
#----Creates the testSuite xml configuration file from the testSuiteJob xml configuration
#--Parameters:
#----xml: pointer to the utility to create an xml from a perl hash
#----data: Pointer to a var that contains the testSuiteJob xml configuration
#----filePath: The path where the testSuite configuration file has to be written
sub configureTestSuite {
        local($moduleName, $xml) = @_;

        my $config = {};
	$config->{testSuite} = {};
	$config->{testSuite}->{common}->{testSuiteName} = "ALYA";
	$config->{testSuite}->{common}->{executable} = "../../../../../Executables/unix/Alya.x";
	$config->{testSuite}->{common}->{alya2pos} = "../../../../../Utils/user/alya2pos/alya2pos.x";
	$config->{testSuite}->{common}->{execModule} = "LocalExec";
	$config->{testSuite}->{common}->{doReport} = "true";
	$config->{testSuite}->{modules}->{module} = [];
        my $modules = $config->{testSuite}->{modules};
	my $module = {};
        my $module->{moduleName} = $moduleName;
        push(@{$modules->{module}}, $module);

	$filePath = "testSuite.xml";
	$xml->XMLout(
	    $config,
	    KeepRoot => 1,
	    NoAttr => 1,
	    OutputFile => $filePath,
	);

}

#------------------------------------------------
#---------------main-----------------------------
#------------------------------------------------

#-----Get the arguments------
my $numArgs = $#ARGV + 1;
my $moduleName;
if ($numArgs < 1) {
	print "The usage is: ./testSuiteLocal.plx module_name\n";
	exit(0);
}
else {
	$moduleName = $ARGV[0];
}
$xml = new XML::Simple;

configureTestSuite($moduleName, $xml);
$output = `chmod +x testSuite.plx`;
$output = `./testSuite.plx`;
print $output . "\n";

exit(0);
