$-------------------------------------------------------------------
PHYSICAL_PROBLEM
  PROPERTIES
    MATERIAL: 1
       DENSITY:       *GenData(KER_Densi_values1), VALUE=*GenData(KER_Densi_values1)
       VISCOSITY:     *GenData(KER_Visco_values1), VALUE=*GenData(KER_Visco_values1)
       CONDUCTIVITY:  *GenData(KER_Condu_values1), VALUE=*GenData(KER_Condu_values1)
       SPECIFIC_HEAT: *GenData(KER_Sphea_values1), VALUE=*GenData(KER_Sphea_values1)
    END_MATERIAL
  END_PROPERTIES
END_PHYSICAL_PROBLEM
$-------------------------------------------------------------------
NUMERICAL_TREATMENT 
  ELSEST
    STRATEGY:    *GenData(KER_ELSEST_Strategy)
*if(strcasecmp(GenData(KER_ELSEST_Strategy),"Bin")!=0)
    NUMBER_BINS= *GenData(KER_Number_bins)
*else
    MAXIMUM=     *GenData(KER_Max_nodes)
*endif
  END_ELSEST
  MESH
    MULTIPLICATION= *GenData(KER_Multiplication)
  END_MESH
END_NUMERICAL_TREATMENT  
$-------------------------------------------------------------------
OUTPUT_&_POST_PROCESS  
  *GenData(KER_Postprocess)
*if( GenData(KER_Post_frequency,int) != -2 )
   STEPS = GenData(KER_Post_frequency,int)
*endif
END_OUTPUT_&_POST_PROCESS  
$-------------------------------------------------------------------
