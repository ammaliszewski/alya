*if(ndime==2)
*set elems(Triangle)  
*add elems(Quadrilateral)
*else
*set elems(Tetrahedra)  
*add elems(Hexahedra)
*add elems(Prism)
*add elems(Pyramid)
*endif
*set var ielem=0
*\------------------------------------------------------------------------
*\
*\ NODES PER ELEMENTS
*\ 
*\------------------------------------------------------------------------
  NODES_PER_ELEMENT
*loop elems
*set var ielem=ielem+1
   *ielem *ElemsNnode
*end
  END_NODES_PER_ELEMENT
*\------------------------------------------------------------------------
*\
*\ ELEMENTS
*\ 
*\------------------------------------------------------------------------
  ELEMENTS
*set var ielem=0
*loop elems
*set var ielem=ielem+1
*set var kelem=elemsNum 
  *ielem *elemsConec  
*end
  END_ELEMENTS  
*\------------------------------------------------------------------------
*\
*\ COODRINATES
*\ 
*\------------------------------------------------------------------------
*set var ipoin=0
*realformat "%14.6e"
  COORDINATES  
*loop nodes
*set var ipoin=ipoin+1
   *ipoin *NodesCoord   
*end
  END_COORDINATES
*\------------------------------------------------------------------------
*\
*\ BOUNDARIES
*\ 
*\------------------------------------------------------------------------
  BOUNDARIES, ELEMENT
*set var iboun=0
*if(ndime==2)
*set Cond DOMAIN_boundary_2D       *elems
*loop elems *onlyincond *canrepeat
*set var entit=cond(1,int)
*set var iboun=iboun+1
*format "%i %i %i %i %i %i %i %i %i %i %i %i "
*iboun *globalnodes  *ElemsNum
*end elems
*else
*set Cond DOMAIN_boundary_3D       *elems
*loop elems *onlyincond *canrepeat
*set var entit=cond(1,int)
*set var iboun=iboun+1
*format "%i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i "
*iboun *globalnodes  *ElemsNum
*end elems
*endif
  END_BOUNDARIES
*\------------------------------------------------------------------------
*\
*\ SKEW SYSTEMS
*\ 
*\------------------------------------------------------------------------
*\*if(strcasecmp(GenData(NASTIN_Module),"Off")!=0)
*\*Set Cond VELOCITY_surface *nodes
*\*Add Cond VELOCITY_line *nodes
*\*Add Cond VELOCITY_point *nodes
*\  SKEW_SYSTEMS 
*\*set var nlocalaxis = 0
*\*loop nodes *OnlyInCond
*\*if(strcasecmp(cond(3),"LOCAL-GID")==0) 
*\*set var nlocalaxis = 1
*\*endif
*\*end 
*\*if(nlocalaxis!=0) 
*\*realformat "%8.4f"
*\*intformat "%6i" 
*\*loop LocalAxes
*\ *LocalAxesNum *LocalAxesDef  
*\*end
*\*endif
*\  END_SKEW_SYSTEMS  
*\*endif
*\------------------------------------------------------------------------
*\
*\ SUBDOMAINS
*\ 
*\------------------------------------------------------------------------
*if(GenData(Number_subdomains,int)!=1)
  SUBDOMAINS
*if(ndime==3)
*Set Cond SUBDOMAINS_3D *elems
*loop elems *OnlyInCond
*Elemsnum *cond(1)
*end elems
*else
*Set Cond SUBDOMAINS_2D *elems
*loop elems *OnlyInCond
*Elemsnum *cond(1)
*end elems
*endif
  END_SUBDOMAINS
*endif

*\------------------------------------------------------------------------
*\
*\ FIELDS
*\ 
*\------------------------------------------------------------------------
*set var field=0
*set var field1=0
*set var field2=0
*set var field3=0
*set var ndim1=0
*set var ndim2=0
*set var ndim3=0
*if(ndime==2)
*Set Cond FIELD_ELEMENT_1_2D *elems
*else
*Set Cond FIELD_ELEMENT_1_3D *elems
*endif
*loop elems *onlyincond
*set var field1=field1+1
*set var field=field+1
*set var ndim1=cond(1,int)
*end
*if(ndime==2)
*Set Cond FIELD_ELEMENT_2_2D *elems
*else
*Set Cond FIELD_ELEMENT_2_3D *elems
*endif
*loop elems *onlyincond
*set var field2=field2+1
*set var field=field+1
*set var ndim2=cond(1,int)
*end
*if(ndime==2)
*Set Cond FIELD_ELEMENT_2_2D *elems
*else
*Set Cond FIELD_ELEMENT_2_3D *elems
*endif
*loop elems *onlyincond
*set var field3=field3+1
*set var field=field+1
*set var ndim3=cond(1,int)
*end
*if(field>0)
  FIELDS
*if(field1>0)
*Set Cond FIELD_ELEMENT_1_2D *elems
*Add Cond FIELD_ELEMENT_1_3D *elems
    FIELD=1, DIMENSION=*ndim1
*loop elems *onlyincond
    *Elemsnum *cond(2)
*end
    END_FIELD
*endif
*if(field2>0)
*Set Cond FIELD_ELEMENT_2_2D *elems
*Add Cond FIELD_ELEMENT_2_3D *elems
    FIELD=2, DIMENSION=*ndim2
*loop elems *onlyincond
    *Elemsnum *cond(2)
*end
    END_FIELD
*endif
*if(field3>0)
*Set Cond FIELD_ELEMENT_3_2D *elems
*Add Cond FIELD_ELEMENT_3_3D *elems
    FIELD=3, DIMENSION=*ndim3
*loop elems *onlyincond
    *Elemsnum *cond(2)
*end
    END_FIELD
*endif
  END_FIELDS
*endif
*\------------------------------------------------------------------------
*\
*\ MATERIAL
*\ 
*\------------------------------------------------------------------------
*if(nmats!=0) 
  MATERIALS
*loop elems
   *elemsNum *ElemsMat
*end
  END_MATERIALS
*endif 
*\------------------------------------------------------------------------
*\
*\ MATERIALS
*\ 
*\------------------------------------------------------------------------
  MATERIALS, NUMBER=*GenData(Number_materials), DEFAULT=1
*if(ndime==2)
*set Cond MATERIAL_2D       *elems
*else
*set Cond MATERIAL_3D       *elems
*endif
*set var ielem=0
*loop elems *onlyincond *NoCanRepeat
*set var entit=cond(1,int)
*set var ielem=ielem+1
*set var kelem=elemsNum
   *kelem *entit
*end
  END_MATERIALS
*\------------------------------------------------------------------------
*\
*\ ZONES
*\ 
*\------------------------------------------------------------------------
*if(GenData(Number_zones,int)!=1)  
ZONES
*if(ndime==2)
*set Cond ZONE1_2D       *elems
*else
*set Cond ZONE1_3D       *elems
*endif
  ZONE, NUMBER=1
*loop elems *onlyincond *NoCanRepeat
   *elemsNum 
*end
  END_ZONE
*if(GenData(Number_zones,int)>1)  
*if(ndime==2)
*set Cond ZONE2_2D       *elems
*else
*set Cond ZONE2_3D       *elems
*endif
  ZONE, NUMBER=2
*loop elems *onlyincond *NoCanRepeat
   *elemsNum 
*end
  END_ZONE
*endif 
*if(GenData(Number_zones,int)==3)  
*if(ndime==2)
*set Cond ZONE3_2D       *elems
*else
*set Cond ZONE3_3D       *elems
*endif
  ZONE, NUMBER=3
*loop elems *onlyincond *NoCanRepeat
   *elemsNum 
*end
  END_ZONE
*endif
END_ZONES
*endif
*\------------------------------------------------------------------------
*\
*\ CHARACTERISTICS
*\ 
*\------------------------------------------------------------------------
  CHARACTERISTICS
*set var kflcontact=3
*set var epsilon=0.000000001
*set var ielem=0
*if(ndime==2)
*#
*#  2D problem, check only for quadris
*#
*set elems(Quadrilateral)
*loop elems
*set var ielem=ielem+1
*set var kelem=elemsNum 
*if((NodesCoord(1,2)>NodesCoord(4,2) - epsilon) && (NodesCoord(1,2)<NodesCoord(4,2) + epsilon))
*if((NodesCoord(1,1)>NodesCoord(4,1) - epsilon) && (NodesCoord(1,1)<NodesCoord(4,1) + epsilon))
*#  contact found
  *kelem *kflcontact 
*endif
*endif
*end
*else
*#
*#  2D problem, check only for hexas and prisms
*#
*set elems(Hexahedra)
*loop elems
*set var ielem=ielem+1
*set var kelem=elemsNum 
*if((NodesCoord(1,3)>NodesCoord(5,3) - epsilon) && (NodesCoord(1,3)<NodesCoord(5,3) + epsilon))
*if((NodesCoord(1,2)>NodesCoord(5,2) - epsilon) && (NodesCoord(1,2)<NodesCoord(5,2) + epsilon))
*if((NodesCoord(1,1)>NodesCoord(5,1) - epsilon) && (NodesCoord(1,1)<NodesCoord(5,1) + epsilon))
*#  contact found
  *kelem *kflcontact 
*endif
*endif
*endif
*end
*set elems(Prism)
*loop elems
*set var ielem=ielem+1
*set var kelem=elemsNum 
*if((NodesCoord(1,3)>NodesCoord(4,3) - epsilon) && (NodesCoord(1,3)<NodesCoord(4,3) + epsilon))
*if((NodesCoord(1,2)>NodesCoord(4,2) - epsilon) && (NodesCoord(1,2)<NodesCoord(4,2) + epsilon))
*if((NodesCoord(1,1)>NodesCoord(4,1) - epsilon) && (NodesCoord(1,1)<NodesCoord(4,1) + epsilon))
*#  contact found
  *kelem *kflcontact 
*endif
*endif
*endif
*end
*endif
  END_CHARACTERISTICS
