#! /bin/bash
#
#@ job_name         = humanHF
#@ output           = human1.out
#@ error            = human1.err
#@ initialdir       = .
#@ total_tasks      = 24
#@ cpus_per_task    = 12
#@ wall_clock_limit = 08:00:00
#@ queue

echo '--| Starting Make process at : ' `date`
time srun /gpfs/projects/bsc21/WORK-JAZMIN/SVN/Alya/Executables/unix/Alya.x human
echo '--| End Make at              : ' `date`
