subroutine ada_elmgra
!-----------------------------------------------------------------------
!****f* adapti/ada_elmgra
! NAME 
!    ada_elmgra
! DESCRIPTION
!    This routine computes element graph when it is not already computed
!    by any other module
! USES
!
! USED BY
!
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_adapti

  implicit none

  !!!if (medge == 0) call elmgra(2)


end subroutine ada_elmgra
