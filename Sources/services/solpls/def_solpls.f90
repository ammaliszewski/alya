module def_solpls
  !-----------------------------------------------------------------------
  !****f* solpls/def_solpls
  ! NAME
  !   def_sopls
  ! DESCRIPTION
  !   This module is the header of the solpls service (LPS solver)
  !***
  !-----------------------------------------------------------------------
  use def_kintyp

  type spars_pls
     !
     ! d.o.f. ..... a%dof
     ! n nodes .... a%ndx
     ! index ...... a%idx(1:ndx)
     ! position ... a%pos(1:nzz)
     ! matrix ..... a%val(1:dof*nnz)
     !
     integer(ip)          :: nnz,ndx,dof
     integer(ip), pointer :: idx(:)
     integer(ip), pointer :: pos(:)
     real(rp),    pointer :: val(:)
  end type spars_pls

  type(spars_pls)         :: &
       Aii,                  &
       Aib,                  &
       Abb,                  &
       Abi,                  &
       Att

  integer(ip),    pointer :: &
       letyp_pls(:)

end module def_solpls
