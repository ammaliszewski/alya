subroutine pls_driver()
  !-----------------------------------------------------------------------
  !****f* solpls/pls_driver
  ! NAME 
  !    pls_driver
  ! DESCRIPTION
  !    This routine is the bridge for PLS solver
  ! USES
  !
  ! USED BY
  !
  !***
  !-----------------------------------------------------------------------
  use def_master, only :  amatr,rhsid,unkno,kfl_paral
  use def_domain, only :  c_dom,r_dom,npoin
  use def_solpls
  implicit none
  integer(ip)          :: gindex(4,0:1),nbvois,ordcomm,proc2block
  real(rp)             :: anIB,jaIB,iaIB
  real(rp)             :: anBI,jaBI,iaBI,anBB,jaBB,iaBB
  real(rp)             :: b_B,x_B

  logical(lg)          :: lparam(5)
  integer(ip)          :: iparam(17)
  real(rp)             :: dparam(4)

  gindex(1,0) = 1 ! nbvar (ver scal_sys_d.F)
  gindex(2,0) = 0
  gindex(3,0) = npoin
  gindex(4,0) = 0 
  gindex(1,1) = npoin
  gindex(2,1) = 0

  lparam(1) = .false.
  lparam(2) = .false.
  lparam(3) = .false.
  lparam(4) = .false.
  lparam(5) = .false.

  iparam(1) = 1
  iparam(2) = 1
  iparam(3) = 0
  iparam(4) = 0
  iparam(5) = 1
  iparam(6) = 1
  iparam(7) = 100
  iparam(8) = 1
  iparam(9) = 0
  iparam(10)= 0
  iparam(11)= 10
  iparam(12)= 1
  iparam(13)= 0
  iparam(14)= 0
  iparam(15)= 0
  iparam(16)= npoin
  iparam(17)= 1

  dparam(1) = 1.0e-6
  dparam(2) = 0.0
  dparam(3) = 1.0e-6
  dparam(4) = 1.0

  !if(kfl_paral==-1) then
  !   call pls_solver(&
  !        lparam, iparam, dparam,&
  !        gindex, nbvois, ordcomm, proc2block,&
  !        amatr, c_dom, r_dom, anIB, jaIB, iaIB,&
  !        anBI, jaBI, iaBI, anBB, jaBB, iaBB,&
  !        rhsid, b_B, unkno, x_B )
  !end if

end subroutine pls_driver
