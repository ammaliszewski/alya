subroutine pls_domgra(itask)
  !-----------------------------------------------------------------------
  !****f* Domain/pls_domgra
  ! NAME
  !    pls_domgra
  ! DESCRIPTION
  !    This routine allocates memory for the coefficients of 
  !    the mesh graph.
  ! OUTPUT
  !    ITASK = 1 ... Aii (lni x lni)
  !          = 2 ... Aib (lni x gnb)
  !          = 3 ... Abi (gnb x lni)
  !          = 4 ... Abb (gnb x gnb)
  !          = 5 ... Att (gnb x gnb)
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use mod_memchk
  implicit none
  integer(ip), intent(in)  :: itask
  integer(ip)              :: istat,ipoin,ielem,jelem,igrap
  integer(ip)              :: izdom,ncoef,nlelp,nrows_pls
  integer(ip)              :: lsize
  integer(ip), allocatable :: lista(:)
  logical(lg), allocatable :: touch(:)
  !
  ! Number of rows
  !
  if(itask<=2) then
     nrows_pls=lni
  else
     nrows_pls=gnb
  end if
  !
  ! Compute node-element connectivity (if not already computed)
  !
  allocate(nepoi_pls(nrows_pls),stat=istat)
  call memchk(zero,istat,memor_dom,'NEPOI_PLS','connpo',nepoi_pls)
  !
  ! Aii and Aib
  !
  if(itask<=2) then
     do ielem=1,nelem
        do inode=1,nnode(ltype(ielem))
           ipoin=lnods(inode,ielem)
           if(ipoin<=lni)& 
                nepoi_pls(ipoin)=nepoi_pls(ipoin)+1
        end do
     end do
  else
     do ielem=1,nelem
        do inode=1,nnode(ltype(ielem))
           ipoin=lnods(inode,ielem)
           if(ipoin<=lni)& 
                nepoi_pls(ipoin)=nepoi_pls(ipoin)+1
        end do
     end do     
  end if
  !
  ! Allocate memory for PELPO_PLS and compute it
  !
  allocate(pelpo_pls(npoin+1),stat=istat)
  call memchk(zero,istat,memor_dom,'PELPO_PLS','connpo',pelpo_pls)
  pelpo_pls(1)=1
  do ipoin=1,npoin
     pelpo_pls(ipoin+1)=pelpo_pls(ipoin)+nepoi_pls(ipoin)
  end do
  !
  ! Allocate memory for LELPO and construct the list
  !
  nlelp=pelpo_pls(npoin+1)
  allocate(lelpo(nlelp),stat=istat)
  call memchk(zero,istat,memor_dom,'LELPO','connpo',lelpo)
  mpopo=0
  do ielem=1,nelem
     mpopo = mpopo + nnode(ltype(ielem))*nnode(ltype(ielem))
     do inode=1,nnode(ltype(ielem))
        ipoin=lnods(inode,ielem)
        lelpo(pelpo_pls(ipoin))=ielem
        pelpo_pls(ipoin)=pelpo_pls(ipoin)+1
     end do
  end do
  !
  ! Recompute PELPO_PLS and maximum number of element neighbors MEPOI
  !
  pelpo_pls(1)=1
  mepoi=-1
  do ipoin=1,npoin
     pelpo_pls(ipoin+1)=pelpo_pls(ipoin)+nepoi_pls(ipoin)
     mepoi=max(mepoi,nepoi_pls(ipoin))
  end do
  !
  ! Deallocate memory for temporary node/element connectivity
  !
  call memchk(two,istat,memor_dom,'NEPOI_PLS','connpo',nepoi_pls)
  deallocate(nepoi_pls,stat=istat)
  if(istat/=0) call memerr(two,'NEPOI_PLS','connpo',0_ip)  
  !
  ! Compute node-node graph
  !
  igrap=1
  if(igrap==1) then
     !
     ! Strategy 1: slow but does not require lots of memory
     !
     nzdom = 0
     allocate(touch(nelem*mnode),stat=istat)
     call memchk(zero,istat,memor_dom,'TOUCH','connpo',touch)
     do ipoin = 1,npoin
        nlelp          = pelpo_pls(ipoin+1)-pelpo_pls(ipoin)
        ncoef          = nlelp*mnode
        touch(1:ncoef) = .false.
        call nzecof(&
             nlelp,nnode,nelem,nelty,mnode,ncoef,&
             nzdom,lelpo(pelpo_pls(ipoin)),lnods,ltype,touch)
     end do
     !
     ! Increment NZDOM due to domain decomposition
     !
     nzdom_dod=nzdom
     call Dodeme(4_ip)
     nzdom=nzdom_dod
     !
     ! Construct the array of indexes
     !
     allocate(r_dom(npoin+1),stat=istat)
     call memchk(zero,istat,memor_dom,'R_DOM','connpo',r_dom)
     allocate(c_dom(nzdom),stat=istat)
     call memchk(zero,istat,memor_dom,'C_DOM','connpo',c_dom)
     izdom = 1
     do ipoin=1,npoin
        nlelp          = pelpo_pls(ipoin+1)-pelpo_pls(ipoin)
        ncoef          = nlelp*mnode
        touch(1:ncoef) = .false.
        call arrind(&
             nlelp,nnode,nelem,npoin,nelty,mnode,ncoef,&
             nzdom,lelpo(pelpo_pls(ipoin)),lnods,ltype,touch,&
             r_dom,c_dom,izdom,ipoin)
        !
        ! Add nodal connections to graph C_DOM
        !
        nzdom_dod=izdom
        ipoin_dod=ipoin
        call Dodeme(5_ip)
        izdom=nzdom_dod
     end do
     r_dom(npoin+1) = nzdom+1
     call memchk(two,istat,memor_dom,'TOUCH','connpo',touch)
     deallocate(touch,stat=istat)
     if(istat/=0) call memerr(two,'TOUCH','connpo',0_ip) 

  else
     !
     ! Strategy 2: quick but requires lots of memory
     ! 
     allocate(lista(mpopo),stat=istat)
     call memchk(zero,istat,memor_dom,'LISTA','pls_domgra',lista)     
     allocate(r_dom(npoin+1),stat=istat)
     call memchk(zero,istat,memor_dom,'R_DOM','pls_domgra',r_dom)
     !
     ! Construct the array of indexes
     !     
     r_dom(1) = 1
     do ipoin=1, npoin
        lsize = 0
        do ielem= pelpo_pls(ipoin), pelpo_pls(ipoin+1)-1
           jelem = lelpo(ielem)
           call mergli( lista(r_dom(ipoin)), lsize, lnods(1,jelem), &
                nnode(ltype(jelem)), mone )
        enddo
        nzdom_dod=0
        call Dodeme(6_ip)
        lsize=lsize+nzdom_dod
        r_dom(ipoin+1) = r_dom(ipoin) + lsize
     enddo
     nzdom = r_dom(npoin+1)-1
     allocate(c_dom(nzdom),stat=istat)
     call memchk(zero,istat,memor_dom,'C_DOM','pls_domgra',c_dom)     
     do ipoin=1, nzdom
        c_dom(ipoin) = lista(ipoin)
     enddo
     call Dodeme(7_ip)
     call memchk(two,istat,memor_dom,'LISTA','pls_domgra',lista)
     deallocate(lista,stat=istat)
     if(istat/=0) call memerr(two,'LISTA','pls_domgra',0_ip)
  end if

end subroutine pls_domgra
