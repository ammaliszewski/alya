subroutine pls_csrase()
  !----------------------------------------------------------------------
  !****f* solpls/pls_csrase
  ! NAME
  !    pls_csrase
  ! DESCRIPTION
  !    Assemble matrix for PLS solver
  !    - Sequential version:
  !    - Parallel version:
  !      Interior elements are computed apart to avoid conditionals 
  !      inside the loops
  ! INPUT
  ! OUTPUT
  ! USED BY
  !***
  !----------------------------------------------------------------------
  use def_solpls
  use def_elmope, only :  elmat
  use def_solver, only :  pnode_pls,ielem_pls,solve_sol
  use def_master, only :  npoi1,npoi2,npoi3,gni,lnbin
  use def_domain, only :  lnods
  implicit none 
  integer(ip)          :: inode,jnode,ipoin,jpoin,kpoin,lpoin,ipos

  if(letyp_pls(ielem_pls)==0) then
     !
     ! Interior element
     !
     do inode=1,pnode_pls
        ipoin=lnods(inode,ielem_pls)
        do jnode=1,pnode_pls
           jpoin=lnods(jnode,ielem_pls)
           ipos=Aii%idx(ipoin)
           kpoin=Aii%pos(ipos)
           do while(kpoin/=jpoin)
              kpoin=Aii%pos(ipos)
              ipos=ipos+1
           end do
           Aii%val(ipos)=elmat(inode,jnode)
        end do
     end do

  else
     !
     ! Boundary element
     !
     do inode=1,pnode_pls
        ipoin=lnods(inode,ielem_pls)
        if(ipoin<=npoi1) then
           !
           ! AI...
           !
           do jnode=1,pnode_pls
              jpoin=lnods(jnode,ielem_pls)

              if(jpoin<=npoi1) then
                 !
                 ! AII
                 !
                 ipos=Aii%idx(ipoin)
                 kpoin=Aii%pos(ipos)
                 do while(kpoin/=jpoin)
                    kpoin=Aii%pos(ipos)
                    ipos=ipos+1
                 end do
                 Aii%val(ipos)=elmat(inode,jnode)

              else
                 !
                 ! AIB
                 !
                 ipos=Aib%idx(ipoin)
                 kpoin=Aib%pos(ipos)
                 jpoin=lnbin(jpoin)-gni
                 do while(kpoin/=jpoin)
                    kpoin=Aib%pos(ipos)
                    ipos=ipos+1
                 end do
                 Aib%val(ipos)=elmat(inode,jnode)

              end if

           end do

        else
           !
           ! AB...
           !
           lpoin=lnbin(ipoin)-gni
           do jnode=1,pnode_pls
              jpoin=lnods(jnode,ielem_pls)

              if(jpoin<=npoi1) then
                 !
                 ! ABI
                 !
                 ipos=Abi%idx(lpoin)
                 kpoin=Abi%pos(ipos)
                 do while(kpoin/=jpoin)
                    kpoin=Abi%pos(ipos)
                    ipos=ipos+1
                 end do
                 Abi%val(ipos)=elmat(inode,jnode)

              else if(ipoin>=npoi2.and.ipoin<=npoi3) then
                 !
                 ! ABB
                 !
                 ipos=Abb%idx(lpoin)
                 kpoin=Abb%pos(ipos)
                 jpoin=lnbin(jpoin)-gni
                 do while(kpoin/=jpoin)
                    kpoin=Abb%pos(ipos)
                    ipos=ipos+1
                 end do
                 Abb%val(ipos)=elmat(inode,jnode)

              else 
                 !
                 ! ATT
                 !
                 ipos=Att%idx(lpoin)
                 kpoin=Att%pos(ipos)
                 jpoin=lnbin(jpoin)-gni
                 do while(kpoin/=jpoin)
                    kpoin=Att%pos(ipos)
                    ipos=ipos+1
                 end do
                 Att%val(ipos)=elmat(inode,jnode)

              end if

           end do

        end if

     end do

  end if

end subroutine pls_csrase
