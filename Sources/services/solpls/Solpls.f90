subroutine Solpls(order)
  !-----------------------------------------------------------------------
  !****f* solpls/Solpls
  ! NAME 
  !    Solpls
  ! DESCRIPTION
  !    This routine is the bridge for Solpls service
  ! USES
  !
  ! USED BY
  !
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_solpls
  use def_domain
  use mod_postpr
  implicit none
  integer(ip), intent(in) :: order
  real(rp)                :: time1,time2

  servi=8

  call cputim(time1)

  select case(order)

  case(10)
     !
     ! Graphs
     !
     call pls_graphs()

  case(20)
     !
     ! Element type
     !
     call pls_elmtyp()

  case(30)
     !
     ! Assembly
     !
     call pls_csrase()

  case(40)
     !
     ! Solution
     !
     call pls_driver()

  end select

  call cputim(time2)
  cpu_servi(1,servi) =  cpu_servi(1,servi) + time2-time1

end subroutine Solpls
