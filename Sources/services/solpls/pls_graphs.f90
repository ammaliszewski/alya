subroutine pls_graphs()
  !-----------------------------------------------------------------------
  !****f* solpls/pls_graphs
  ! NAME
  !   pls_grpahs
  ! DESCRIPTION
  !   This routine computes the graph of the 5 matrices
  !   Aii, Aib, Abi, Abb and Atmp
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_master
  use def_domain
  use def_solpls
  implicit none
  integer(ip)         :: ipoin,jpoin,izdom,iprev,inext,kpoin
  integer(ip), target :: ii(1)
  !
  ! Local to global boundary numbering LBNIN
  !
  call Parall(16_ip)

  if(kfl_paral>0) then
     !
     ! Allocate memory for indices
     !
     Aii%ndx=lni
     Aib%ndx=lni
     Abi%ndx=gnb
     Abb%ndx=gnb
     Att%ndx=gnb
     call pls_memory(1_ip)
     !
     ! Initialization
     !
     Aii%nnz=0
     Aib%nnz=0
     Abi%nnz=0
     Abb%nnz=0
     Att%nnz=0
     Aii%dof=mxdof
     Aib%dof=mxdof
     Abi%dof=mxdof
     Abb%dof=mxdof
     Att%dof=mxdof
     !
     ! Compute NNZ and IDX: Internal nodes
     !
     Aii%idx(1)=1
     Aib%idx(1)=1
     do ipoin=1,npoi1
        do izdom=r_dom(ipoin),r_dom(ipoin+1)-1
           jpoin=c_dom(ipoin)
           if(jpoin<=lni) then
              !
              ! Contribution to AII
              !
              Aii%nnz=Aii%nnz+1
           else
              !
              ! Contribution to AIB
              !
              Aib%nnz=Aib%nnz+1
           end if
        end do
        Aii%idx(ipoin+1)=Aii%nnz+1
        Aib%idx(ipoin+1)=Aib%nnz+1
     end do
     !
     ! Compute NNZ and IDX: Boundary nodes
     !
     do kpoin=1,gnb
        Abi%idx(kpoin)=0        
        Att%idx(kpoin)=0        
     end do
     do ipoin=npoi1+1,npoin
        kpoin=lnbin(ipoin)-gni
        do izdom=r_dom(ipoin),r_dom(ipoin+1)-1
           jpoin=c_dom(ipoin)
           if(jpoin<=lni) then
              !
              ! Contribution to ABI
              !
              Abi%idx(kpoin)=Abi%idx(kpoin)+1
           else if(ipoin<npoi2.or.ipoin>npoi3) then
              !
              ! Contribution to ATT
              !
              Att%idx(kpoin)=Att%idx(kpoin)+1
           end if
        end do
     end do
   
     Abi%idx(1)=1
     iprev=Abi%idx(1)
     do kpoin=1,gnb
        inext=Abi%idx(kpoin+1)
        Abi%idx(kpoin+1)=inext+iprev
        iprev=inext
     end do
     Abi%nnz=Abi%idx(gnb+1)-1

     Att%idx(1)=1
     iprev=Att%idx(1)
     do kpoin=1,gnb
        inext=Att%idx(kpoin+1)
        Att%idx(kpoin+1)=inext+iprev
        iprev=inext
     end do
     Att%nnz=Att%idx(gnb+1)-1
     !
     ! Allocate memory for POS
     !
     call pls_memory(2_ip)
     !
     ! Compute positions: Internal nodes
     !
     Aii%nnz=0
     Aib%nnz=0
     do ipoin=1,lni
        do izdom=r_dom(ipoin),r_dom(ipoin+1)-1
           jpoin=c_dom(ipoin)
           if(jpoin<=lni) then
              !
              ! Contribution to AII
              !
              Aii%nnz=Aii%nnz+1
              Aii%pos(Aii%nnz)=jpoin
           else 
              !
              ! Contribution to AIB
              !
              Aib%nnz=Aib%nnz+1
              Aib%pos(Aib%nnz)=lnbin(jpoin)-gni
           end if
        end do
     end do
     !
     ! Compute positions: Boundary nodes
     !
     Abi%nnz=0
     Att%nnz=0
     do ipoin=npoi1+1,npoin
        do izdom=r_dom(ipoin),r_dom(ipoin+1)-1
           jpoin=c_dom(ipoin)
           if(jpoin<=lni) then
              !
              ! Contribution to ABI
              !
              Abi%nnz=Abi%nnz+1
              Abi%pos(Abi%nnz)=jpoin
           else if(ipoin<npoi2.or.ipoin>npoi3) then
              !
              ! Contribution to ATT
              !
              Att%nnz=Att%nnz+1
              Att%pos(Att%nnz)=lnbin(jpoin)-gni              
           end if
        end do
     end do

  end if
  !
  ! Matrix ABB computed by master
  !
  if(kfl_paral==0) then
     call Parall(17_ip)

  else if(kfl_paral>0) then
     !
     ! Receive ABB%IDX
     !
     npari         =  gnb+1
     parin         => Abb%idx
     call Parall(18_ip)
     !
     ! Allocate memory for ABB%POS
     !
     Abb%nnz       =  Abb%idx(gnb+1)
     call pls_memory(4_ip)
     !
     ! Receive ABB%POS
     !
     npari         =  Abb%nnz 
     parin         => Abb%pos
     call Parall(18_ip)

  end if

end subroutine pls_graphs
