subroutine pls_elmtyp()
  !-----------------------------------------------------------------------
  !****f* solpls/pls_elmtyp
  ! NAME
  !    pls_grpahs
  ! DESCRIPTION
  !    Define element type for the parallelization
  !    LETYP(IELEM) = 0 ... Interior element
  !    LETYP(IELEM) = 1 ... Boundary element
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_master
  use def_domain
  use def_solpls
  use mod_memchk
  implicit none
  integer(ip) :: ipoin,ielem,inode,pnode

  if(kfl_paral>0) then
     call pls_memory(6_ip)
     do ielem=1,nelem
        inode=0
        pnode=nnode(ltype(ielem))
        do while(inode<pnode)
           inode=inode+1
           if(lnods(inode,ielem)>npoi1) inode=pnode+1
        end do
        if(inode==pnode+1) letyp_pls(ielem)=1
     end do
  end if

end subroutine pls_elmtyp
