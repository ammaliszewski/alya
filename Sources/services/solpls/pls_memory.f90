subroutine pls_memory(itask)
  !-------------------------------------------------------------------------------
  !****f* solpls/pls_memory
  ! NAME
  !    pls_memory
  ! DESCRIPTION
  !    Allocate memory for PLS solver 
  ! INPUT
  ! OUTPUT
  ! USED BY
  !    pls_create_graph_arrays
  !***
  !-------------------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master
  use def_solpls
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: itask
  integer(4)              :: istat

  select case(itask)

  case(1)
     !
     ! Allocate memory for indices of submatrices
     !
     allocate(Aii%idx(Aii%ndx),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'AII%IDX','pls_memory',Aii%idx)
     allocate(Aib%idx(Aib%ndx),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'AIB%IDX','pls_memory',Aib%idx)
     allocate(Abi%idx(Abi%ndx),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ABI%IDX','pls_memory',Abi%idx)
     allocate(Abb%idx(Abb%ndx),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ABB%IDX','pls_memory',Abb%idx)
     allocate(Att%idx(Att%ndx),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ATT%IDX','pls_memory',Att%idx)

  case(2)
     !
     ! Allocate memory for position
     !
     allocate(Aii%pos(Aii%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'AII%POS','pls_memory',Aii%pos)
     allocate(Aib%pos(Aib%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'AIB%POS','pls_memory',Aib%pos)
     allocate(Abi%pos(Abi%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ABI%POS','pls_memory',Abi%pos)
     allocate(Abb%pos(Abb%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ABB%POS','pls_memory',Abb%pos)
     allocate(Att%pos(Att%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ATT%POS','pls_memory',Att%pos)
     
  case(3)
     !
     ! Allocate memory for values of AII, AIB and ABI
     !
     allocate(Aii%val(Aii%dof*Aii%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'AII%POS','pls_memory',Aii%pos)
     allocate(Aib%val(Aib%dof*Aib%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'AIB%POS','pls_memory',Aib%pos)
     allocate(Abi%val(Abi%dof*Abi%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ABI%POS','pls_memory',Abi%pos)

  case(4)
     !
     ! Allocate memory for values of ABB
     !
     allocate(Abb%val(Abb%dof*Abb%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ABB%POS','pls_memory',Abb%pos)

  case(5)
     !
     ! Allocate memory for values of ATT
     !
     allocate(Att%val(Att%dof*Att%nnz),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'ATT%POS','pls_memory',Att%pos)
   
  case(6)
     !
     ! Element types LETYP_PLS
     !
     allocate(letyp_pls(nelem),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LETYP_PLS','par_partit',letyp_pls)
  
  end select

end subroutine pls_memory

