subroutine par_reapro()
  !------------------------------------------------------------------------
  !****f* Parall/par_reapro
  ! NAME
  !    par_reapro
  ! DESCRIPTION
  !    This routine reads service data
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_parall
  use def_inpout
  use mod_parall, only : par_omp_granularity, par_omp_coloring_alg
#ifdef PARMETIS  
  use mod_par_partit_paral, only: kfl_paral_parmes, kfl_paral_proc_node
#endif
  implicit none

  if( IMASTER .or. ISEQUEN ) then

     npart_par           = nproc_par-1    ! Number of subdomains
     npart               = npart_par
     kfl_ascii_par       = 0              ! Restart file format
     kfl_bytes_par       = ip             ! Integer bytes for restart files
     kfl_parti_par       = 2              ! Partition type (nodes/faces)
     kfl_outpu_par       = 0              ! Output of files
     kfl_postp_par       = 1              ! Postprocess type (master=1, slave=0)
     kfl_fileh_par       = 0              ! No file hierarchy
     kfl_prefi_par       = 0              ! If prefix to add to restart files
     kfl_filio_par       = 0              ! Do not open and close files in preprocess
     kfl_weigh_par       = 0              ! Weight on Gauss points
     kfl_virfi_par       = 0              ! No virtual files
     kfl_async           = 1              ! Asynchronous (by default)
     nsire_par           = nproc_par+1    ! Number of simultaneous readings
     nzone_par           = 1              ! Number of METIS zones
     lzone_par           = 1              ! List of METIS zones
     wpref_par           = ''             ! Prefix
     rmbyt_par           = 1              ! Max number of Gb for virtual files
     par_omp_granularity = 10             ! Granularity
     par_omp_coloring_alg  = 0        ! Default openmp coloring algorithm
     !
     ! Reach the section
     !      
     rewind(lisda)
     do while(words(1)/='RUNDA')
        call ecoute('PAR_REAPRO')
     end do
     do while(words(1)/='ENDRU')
        call ecoute('PAR_REAPRO')
     end do
     do while(words(1)/='PROBL')
        call ecoute('PAR_REAPRO')
     end do
     !
     ! Read data
     !      
     do while(words(1)/='ENDPR')
        call ecoute('PAR_REAPRO')
        if(words(1)==naser(servi)(1:5)) then 
           if(exists('ON   ')) then
              kfl_servi(servi)=1
              do while(words(1)/='END'//naser(servi)(1:2))

                  if( words(1) == 'OUTPU' ) then
                    !
                    ! Output done by master or slave
                    !
                    if(words(2)=='YES  ' .or. words(2) == 'SLAVE' ) then
                       kfl_outpu_par=1         ! Slaves
                    else
                       kfl_outpu_par=0         ! Only master
                    end if

                 else if( words(1) == 'POSTP' ) then
                    !
                    ! Postprocess
                    !
                    if(words(2)=='MASTE' ) then
                       kfl_postp_par=1
                    else
                       kfl_postp_par=0
                    end if

                 else if( words(1) == 'TASK ' ) then
                    !
                    ! Task: all/pre or post
                    !
                    if( words(2)=='ONLYP' .or. words(2)=='PREPR' ) then

                       kfl_ptask=0
                       call vocabu(-1_ip,0_ip,0_ip) 
                       npart_par = getint('SUBDO',1_ip,'#Number of subdomains')
                       npart     = npart_par
                       if( exists('ASCII') .or. exists('FORMA') ) kfl_ascii_par = 1
                       if( exists('BINAR') .or. exists('UNFOR') ) kfl_ascii_par = 0
                       if( exists('FOURB') .or. exists('4BYTE') ) kfl_bytes_par = 4
                       if( exists('EIGHT') .or. exists('8BYTE') ) kfl_bytes_par = 8

                    else if(words(2)=='PARTI' ) then

                       kfl_ptask=1
                       call vocabu(-1_ip,0_ip,0_ip) 

                    else if(words(2)=='READP' ) then

                       kfl_ptask=2
                       call vocabu(-1_ip,0_ip,0_ip) 
                       if(exists('SIMUL')) then
                          nsire_par=getint('SIMUL',nproc_par+1_ip,'#Number of simultaneous readings')
                       end if
                       if( exists('ASCII') .or. exists('FORMA') ) kfl_ascii_par = 1
                       if( exists('BINAR') .or. exists('UNFOR') ) kfl_ascii_par = 0    

                    end if
                    if(npart_par<2) call runend('WRONG NUMBER OF SUBDOMAINS')

                 else if( words(1) == 'PARTI' ) then
                    !
                    ! Partitioning type
                    !
                    if(words(2)=='NODES' ) then
                       kfl_parti_par=1
                    else if(words(2)=='FACES' ) then
                       kfl_parti_par=2
                    end if

                 else if( words(1) == 'FILEH' ) then
                    !
                    ! File hierarchy
                    !
                    if(words(2)=='NONE ' ) then
                       kfl_fileh_par=0
                    else if(words(2)=='YES  '.or.words(2)=='ONELE'.or.words(2)=='ON   ' ) then
                       kfl_fileh_par=1
                    else if(words(2)=='TWOLE' ) then
                       kfl_fileh_par=2
                    end if

                 else if( words(1) == 'FILEP' ) then
                    !
                    ! File prefix
                    !                    
                    if(words(2)=='NO   '.or.words(2)=='OFF  '.or.words(2)=='NONE ' ) then
                       kfl_prefi_par=0
                    else
                       kfl_prefi_par=1
                       wpref_par=trim(wname)
                    end if

                 else if( words(1) == 'FILEO' ) then
                    !
                    ! Open/Close files
                    !                    
                    if(words(2)=='YES  '.or.words(2)=='ON   ' ) then
                       kfl_filio_par=1
                    else
                       kfl_filio_par=0
                    end if

                 else if( words(1) == 'WEIGH' ) then
                    !
                    ! Weight for graph
                    !
                    if(words(2)=='GAUSS' ) then
                       kfl_weigh_par=0
                    else
                       kfl_weigh_par=1
                    end if

                 else if( words(1) == 'VIRTU' ) then
                    !
                    ! Virtual file
                    !
                    if(words(2)=='ON   ' ) then
                       kfl_virfi_par = 1
                       kfl_ascii_par = 0
                       rmbyt_par     = getrea('MAXIM',1.0_rp,'#MAXIMUM NUMBER OF GB FOR VIRTUAL FILES')
                    else
                       kfl_virfi_par = 0
                    end if

                 else if( words(1) == 'COMMU' ) then
                    if( words(2) == 'SYNCH' ) then
                       kfl_async = 0
                    else if( words(2) == 'ASYNC' ) then
                       kfl_async = 1
                    end if

                 else if( words(1) == 'ZONES' ) then
                    !
                    ! METIS partitions zone-wise
                    !
                    nzone_par          = nnpar
                    lzone_par(1:nnpar) = int(param(1:nnpar),ip)
#ifdef PARMETIS               
                 else if( words(1) == 'PARME' ) then
                    !
                    ! PARMETIS partitions, number of partitioning workers
                    !                                        
                    kfl_paral_parmes = int(param(1))

                 else if( words(1) == 'PROCN' ) then
                    !
                    ! PARMETIS partitions, partitioning workers distribution
                    !
                    kfl_paral_proc_node = int(param(1))
#endif

                 else if( words(1) == 'OPENM' ) then
                    !
                    ! OpenMP stuffs
                    !
                    call ecoute('PAR_REAPRO')
                    do while( words(1) /= 'ENDOP' )
                       if( words(1) == 'GRANU' ) then
                          par_omp_granularity = getint('GRANU',10_ip,'#Granularity of OpenMP blocks')
                       else if ( words(1) == 'COLOR' ) then
                          if(words(2)=='MINCO') then
                            par_omp_coloring_alg = 1
                          else
                            par_omp_coloring_alg = 0
                          end if                          
                       end if
                       call ecoute('PAR_REAPRO')
                    end do
                 end if
                 call ecoute('PAR_REAPRO')
              end do
           end if
        end if
     end do
     !
     ! Check errors and warnings
     !
     if(IMASTER.and.kfl_servi(servi)/=1) &
          call runend('MPI WAS INITIATED AND PARALL SERVICE IS OFF')
     if(IMASTER.and.npart_par<=1) &
          call runend('WRONG NUMBER OF SUBDOMAINS TO USE PARALL SERVICE')
     if(kfl_ptask==0.and.kfl_filio_par==0.and.npart_par>1000) then
        call livinf(10000_ip,'OPEN AND CLOSE ONCE RESTART FILES MAY LEAD TO PROBLEMS',0_ip)
     end if
     if( kfl_ptask == 2 ) kfl_virfi_par = 0

  end if

end subroutine par_reapro
 
