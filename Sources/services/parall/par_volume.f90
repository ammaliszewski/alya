subroutine par_volume()
  !-----------------------------------------------------------------------
  !****f* Parall/par_volume
  ! NAME
  !    par_volume
  ! DESCRIPTION
  !    This routine exchange data 
  ! USES
  ! USED BY
  !    Reapro
  !***
  !-----------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      def_parall
  implicit none
  integer(ip)         :: indice_dom,kfl_ptask_old
  integer(ip), target :: ielem_par(2)
  real(rp),    target :: vomin_par(1),vomax_par(1),vodom_par(1)

  if(IPARALL.and.kfl_ptask/=0) then
     !
     ! Always communicate via MPI
     !
     kfl_ptask_old =  kfl_ptask
     kfl_ptask     =  1
     call vocabu(-1_ip,0_ip,0_ip) 
     !
     ! Reduce min, max and sum of element volumes 
     !
     nparr        =  1
     vomin_par(1) =  vomin
     parre        => vomin_par
     call par_operat(1_ip)
     nparr        =  1
     vomax_par(1) =  vomax
     parre        => vomax_par
     call par_operat(2_ip)
     nparr        =  1
     vodom_par(1) =  vodom
     parre        => vodom_par
     call par_operat(3_ip) 

     if(ISLAVE) then
        !
        ! Slaves check if they are the voted one!
        !
        if(vomin==vomin_par(1)) then
           ielem_par(1) = elmin
        else
           ielem_par(1) = 0
        end if
        if(vomax==vomax_par(1)) then
           ielem_par(2) = elmax
        else
           ielem_par(2) = 0
        end if
        kfl_desti_par =  0
        npari         =  2
        parin         => ielem_par
        call par_sendin()
     else
        !
        ! Master looks for voted elements (with largest global numbering)
        !
        elmin=0
        elmax=0
        indice_dom=0
        do kfl_desti_par=1,npart_par
           npari =  2
           parin => ielem_par
           call par_receiv()
           if(ielem_par(1)/=0) then
              ielem_par(1) = leinv_par(indice_dom+ielem_par(1))
              if(ielem_par(1)>elmin) elmin=ielem_par(1)
           end if
           if(ielem_par(2)/=0) then
              ielem_par(2) = leinv_par(indice_dom+ielem_par(2))
              if(ielem_par(2)>elmax) elmax=ielem_par(2)
           end if
           indice_dom=indice_dom+nelem_par(kfl_desti_par)
        end do
     end if

     if(IMASTER) then
        !
        ! Actualize min, max and average volume
        !
        vomin = vomin_par(1)
        vomax = vomax_par(1)
        vodom = vodom_par(1)
        voave = vodom/nelem
     end if

     kfl_ptask = kfl_ptask_old
     call vocabu(-1_ip,0_ip,0_ip) 
     
  end if

end subroutine par_volume
