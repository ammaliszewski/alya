subroutine par_senbcs()
  !------------------------------------------------------------------------
  !****f* Parall/par_senbcs
  ! NAME
  !    par_senbcs
  ! DESCRIPTION
  !    Send boundary conditions to slaves  
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_parall
  use def_domain
  use def_master
  use mod_memchk
  implicit none  

  strin = 'par_senbcs'
  strre = 'par_senbcs'
  strch = 'par_senbcs'

  !----------------------------------------------------------------------
  !
  ! Codes
  !
  !----------------------------------------------------------------------
  !
  ! Slaves allocate memory
  !
  if( ISLAVE ) then
     nboun_2 = nboun
     call membcs(1_ip)
     call membcs(2_ip)
  end if
  !
  ! Gather code arrays
  !
  if( kfl_icodn > 0 ) call par_parari('GAT',NPOIN_TYPE,mcono*npoin,kfl_codno)
  if( kfl_icodb > 0 ) call par_parari('GAT',NBOUN_TYPE,nboun,kfl_codbo)
  !
  ! Master deallocates memory
  !
  if( IMASTER ) then
     call membcs(-1_ip)
     call membcs(-2_ip)
  end if

  !----------------------------------------------------------------------
  !
  ! Node values
  !
  !----------------------------------------------------------------------

  if( nvcod > 0 ) then
     if( ISLAVE ) call membcs(8_ip)
     !
     ! Cannot use a broadcast because in read_and_run mode master
     ! wants to read from file... and master does not get in this subroutine
     !
     if( IMASTER ) then
        do kfl_desti_par = 1,npart_par
           call par_parari('SND',0_ip,2*mvcod,lvcod)
        end do
     else
        kfl_desti_par = 0
        call par_parari('RCV',0_ip,2*mvcod,lvcod)
     end if

     do nvcod = 1,mvcod
        if( lvcod(1,nvcod) /= 0 ) then
           !
           ! This code has been defined
           !
           if( ISLAVE ) then
              call membcs(9_ip)
           end if
           call par_pararr('GAT',NPOIN_TYPE,lvcod(2,nvcod)*npoin,bvcod(nvcod)%a)
           if( IMASTER ) call membcs(-9_ip)
        end if
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! Boundary values
  !
  !----------------------------------------------------------------------

  if( nvcob > 0 ) then

     if( ISLAVE ) call membcs(10_ip)

     if( IMASTER ) then
        do kfl_desti_par = 1,npart_par
           call par_parari('SND',0_ip,2*mvcob,lvcob)
        end do
     else
        kfl_desti_par = 0
        call par_parari('RCV',0_ip,2*mvcob,lvcob)
     end if

     do nvcob = 1,mvcob
        if( lvcob(1,nvcob) /= 0 ) then
           !
           ! This code has been defined
           !
           if( ISLAVE ) then
              call membcs(11_ip)
           end if
           call par_pararr('GAT',NBOUN_TYPE,lvcob(2,nvcob)*nboun,bvcob(nvcob)%a)

           if( IMASTER ) call membcs(-11_ip)
        end if
     end do
  end if

end subroutine par_senbcs
