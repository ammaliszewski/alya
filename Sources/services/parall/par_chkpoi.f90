subroutine par_chkpoi(ichec)
  !------------------------------------------------------------------------
  !****f* Parall/par_chkpoi
  ! NAME
  !    par_chkpoi
  ! DESCRIPTION
  !    Checkpoint. Retunrs ICHEC:
  !    ICHEC = 0 ... MPI REDUCE has performed well
  !
  !    
  ! OUTPUT
  !    ICHEC
  ! USED BY
  !    par_partit
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_parall
  implicit none  
  integer(ip), intent(out) :: ichec
  integer(ip), target      :: dummi(1)
  !
  ! Checkpoint
  !
  if(kfl_paral>=0) then
     npari    =  1
     dummi(1) =  1
     parin    => dummi
     call par_operat(3_ip)
     ichec=nproc_par-dummi(1)-1
  end if

end subroutine par_chkpoi

