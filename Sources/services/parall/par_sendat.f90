subroutine par_sendat(order)
  !-----------------------------------------------------------------------
  !****f* Parall/par_sendat
  ! NAME
  !    par_sendat
  ! DESCRIPTION
  !    This routine exchange data 
  ! USES
  ! USED BY
  !    Reapro
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_parall
  use def_inpout
  use def_solver
  use mod_memchk
  use mod_parall
#ifdef PARMETIS
  use mod_par_partit_paral
#endif
  !use mod_opebcs
  implicit none
  integer(ip), intent(in) :: order
  integer(ip)             :: ii,ir,ic,ji,ki,dummi
  integer(ip)             :: ipass,kfl_ptask_old
  integer(ip)             :: kfl_memor_new,nsbcs,nnbcs
  integer(4)              :: istat

  select case (order)

  case(1_ip)
     !
     ! First communication always performed with MPI and not files
     !
     if( IMASTER ) then
        if( kfl_ptask == 0 .and. nproc_par == 1 ) return
        kfl_ptask_old=kfl_ptask
        kfl_memor_new=kfl_memor
     end if
     kfl_ptask=1
     call vocabu(-1_ip,0_ip,0_ip) 
     !
     ! Exchange data read in Reapro (always using MPI)
     !
     if( IPARALL ) then
        strre='Reapro'
        strin='Reapro'
        strch='Reapro'
        do parii=1,2 
           npari=0
           nparr=0
           nparc=0
           !
           ! Partition (just to check errors)
           !
           call iexcha(npart_par)
           !
           ! Read in rrudat
           !
           call iexcha(current_code)
           call iexcha(kfl_naked)
           call iexcha(kfl_custo)
           call iexcha(kfl_examp)
           call iexcha(kfl_preli)
           call iexcha(kfl_memor_new)
           call iexcha(kfl_timin)
           call iexcha(kfl_lotme)
           call iexcha(kfl_freme)
           call iexcha(kfl_outpu)
           call iexcha(nprit)
           call iexcha(kfl_rstar)
           call iexcha(kfl_rsfil)
           call iexcha(kfl_commu)
           call iexcha(kfl_outfo)
           call iexcha(kfl_ptask_old)
           call iexcha(kfl_vtk)
#ifdef PARMETIS
           call iexcha(kfl_paral_parmes)
           call iexcha(kfl_paral_proc_node) 
#endif        
           call iexcha(par_omp_granularity)
           call iexcha(par_omp_coloring_alg)
           call rexcha(cpu_limit)
           nparc=nparc+132
           if(parii==2.and.IMASTER) parch(1:66)   = title(1:66)
           if(parii==2.and.ISLAVE)  title(1:66)   = parch(1:66)
           if(parii==2.and.IMASTER) parch(67:132) = namda(1:66)
           if(parii==2.and.ISLAVE)  namda(1:66)   = parch(67:132)
           !
           ! Read in readat and 'mod'_reapro of modules 'mod'
           !
           do ji=1,mblok
              call iexcha(micou(ji))
           end do
           call iexcha(nblok)
           call iexcha(kfl_timco) 
           call iexcha(kfl_reset)
           call rexcha(reset_factor)
           call iexcha(kfl_timei)
           call iexcha(kfl_timef)
           call iexcha(mitim)
           call iexcha(mitsm)
           call iexcha(mitrf)
           call iexcha(kfl_algor_msh)
           call iexcha(kfl_error_msh)
           call iexcha(kfl_gover_msh)
           call iexcha(kfl_block_msh)
           call iexcha(kfl_outpu_par)
           call iexcha(kfl_postp_par)
           call iexcha(nzone_par)
           call iexcha(kfl_wwork)
           call iexcha(kfl_lumped)
           call rexcha(timei)
           call rexcha(timef)
           call rexcha(dtime)
           call rexcha(toler_msh)
           do ji=1,mmodu
              call iexcha(kfl_modul(ji))
           end do
           do ji=1,mmodu
              call iexcha(kfl_delay(ji))
           end do
           do ji=1,mmodu
              call iexcha(kfl_conve(ji))
           end do
           do ji=0,mmodu
              call iexcha(kfl_solve(ji))
           end do
           do ji=1,mmodu
              call iexcha(ndela(ji))
           end do
           do ji=1,mmodu
              do ki=1,mblok
                 call iexcha(lmord(ji,ki))
              end do
           end do
           do ji=0,mmodu
              call iexcha(lzone(ji))
           end do
           do ji=1,mserv
              call iexcha(kfl_servi(ji))
           end do
           call fildef(1_ip)
           call posdef(1_ip,dummi)
           !
           ! Allocate memory for the first pass
           !
           if(parii==1) then
              allocate(parin(npari),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,servi),'PARIN','par_sendat',parin)
              allocate(parre(nparr),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,servi),'PARRE','par_sendat',parre)
              if(ISLAVE) call par_broadc()
           end if
        end do
        if(IMASTER) call par_broadc()

        call memchk(two,istat,mem_servi(1:2,servi),'PARIN','par_sendat',parin)
        deallocate(parin,stat=istat)
        if(istat/=0) call memerr(two,'PARIN','par_sendat',0_ip)
        call memchk(two,istat,mem_servi(1:2,servi),'PARRE','par_sendat',parre)
        deallocate(parre,stat=istat)
        if(istat/=0) call memerr(two,'PARRE','ns_sendat',0_ip)

        kfl_ptask = kfl_ptask_old
        kfl_memor = kfl_memor_new
        call vocabu(-1_ip,0_ip,0_ip) 

     end if

  case(2_ip)

    if( IPARALL ) then

        nsbcs = size(lsbcs,1) * size(lsbcs,2)
        nnbcs = size(npbcs)

        strre='readim_reastr_reageo'
        strin='readim_reastr_reageo'
        strch='readim_reastr_reageo'
        do parii=1,2  
           ipass = parii
           npari=0
           nparr=0
           nparc=0
           !
           ! Read in readim
           !
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_autbo
           if(ipass==2.and.ISLAVE)  kfl_autbo    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_divid
           if(ipass==2.and.ISLAVE)  kfl_divid    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = npoin_par(kfl_desti_par)
           if(ipass==2.and.ISLAVE)  npoin        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = ndime
#ifdef NDIMEPAR

#else
           if(ipass==2.and.ISLAVE)  ndime        = parin(npari)
#endif           
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nelem_par(kfl_desti_par)
           if(ipass==2.and.ISLAVE)  nelem        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nboun_par(kfl_desti_par)
           if(ipass==2.and.ISLAVE)  nboun        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nskew_par(kfl_desti_par)
           if(ipass==2.and.ISLAVE)  nskew        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nperi
           if(ipass==2.and.ISLAVE)  nperi        = parin(npari)
           !npari=npari+1                                            CRISTOBAL
           !if(ipass==2.and.IMASTER) parin(npari) = nimbo
           !if(ipass==2.and.ISLAVE)  nimbo     = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nzone
           if(ipass==2.and.ISLAVE)  nzone        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nsubd
           if(ipass==2.and.ISLAVE)  nsubd        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nboib
           if(ipass==2.and.ISLAVE)  nboib        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = npoib
           if(ipass==2.and.ISLAVE)  npoib        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nelem
           if(ipass==2.and.ISLAVE)  nelwh        = parin(npari)
           do ji=1,nelty
              npari=npari+1
              if(ipass==2.and.IMASTER) parin(npari) = lexis(ji)
              if(ipass==2.and.ISLAVE)  lexis(ji)    = parin(npari)
           end do
           !npari=npari+1
           !if(ipass==2.and.IMASTER) parin(npari) = nhang_par(kfl_desti_par)
           !if(ipass==2.and.ISLAVE)  nhang     = parin(npari)
           do ji=1,nelty
              npari=npari+1
              if(ipass==2.and.IMASTER) parin(npari) = lnuty(ji)
              if(ipass==2.and.ISLAVE)  lnuty(ji)    = parin(npari)
           end do
           !
           ! Read in reastr
           !
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_binar
           if(ipass==2.and.ISLAVE)  kfl_binar = parin(npari)
           do ji=1,nelty
              npari=npari+1
              if(ipass==2.and.IMASTER) parin(npari) = lquad(ji)
              if(ipass==2.and.ISLAVE)  lquad(ji) = parin(npari)
           end do
           do ji=1,3
              nparr=nparr+1
              if(ipass==2.and.IMASTER) parre(nparr) = scale(ji)
              if(ipass==2.and.ISLAVE)  scale(ji) = parre(nparr)
           end do
           do ji=1,3
              nparr=nparr+1
              if(ipass==2.and.IMASTER) parre(nparr) = trans(ji)
              if(ipass==2.and.ISLAVE)  trans(ji) = parre(nparr)
           end do
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_creco
           if(ipass==2.and.ISLAVE)  kfl_creco = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_immbo
           if(ipass==2.and.ISLAVE)  kfl_immbo = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_markm
           if(ipass==2.and.ISLAVE)  kfl_markm = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_savda
           if(ipass==2.and.ISLAVE)  kfl_savda = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nfiel
           if(ipass==2.and.ISLAVE)  nfiel = parin(npari)
           !call posdef(1_ip,dummi) ! OJO POST
           !
           ! Read in reageo
           !
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_chege 
           if(ipass==2.and.ISLAVE)  kfl_chege    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_naxis
           if(ipass==2.and.ISLAVE)  kfl_naxis    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_spher
           if(ipass==2.and.ISLAVE)  kfl_spher    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_bouel
           if(ipass==2.and.ISLAVE)  kfl_bouel    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = mperi
           if(ipass==2.and.ISLAVE)  mperi        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nmate
           if(ipass==2.and.ISLAVE)  nmate        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nmatf
           if(ipass==2.and.ISLAVE)  nmatf        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = ngrou_dom
           if(ipass==2.and.ISLAVE)  ngrou_dom    = parin(npari)
           !
           ! Read in reabcs
           !
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_geome
           if(ipass==2.and.ISLAVE)  kfl_geome    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_convx
           if(ipass==2.and.ISLAVE)  kfl_convx    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_frees
           if(ipass==2.and.ISLAVE)  kfl_frees    = parin(npari)
           nparr=nparr+1
           if(ipass==2.and.IMASTER) parre(nparr) = awind
           if(ipass==2.and.ISLAVE)  awind        = parre(nparr)
           nparr=nparr+1
           if(ipass==2.and.IMASTER) parre(nparr) = tolan
           if(ipass==2.and.ISLAVE)  tolan        = parre(nparr)
           nparr=nparr+1
           if(ipass==2.and.IMASTER) parre(nparr) = geoan
           if(ipass==2.and.ISLAVE)  geoan        = parre(nparr)

           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_icodn
           if(ipass==2.and.ISLAVE)  kfl_icodn    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_icodb
           if(ipass==2.and.ISLAVE)  kfl_icodb    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = kfl_extra
           if(ipass==2.and.ISLAVE)  kfl_extra    = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = ncodn
           if(ipass==2.and.ISLAVE)  ncodn        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = ncodb
           if(ipass==2.and.ISLAVE)  ncodb        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = mcono
           if(ipass==2.and.ISLAVE)  mcono        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = mvcod
           if(ipass==2.and.ISLAVE)  mvcod        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nvcod
           if(ipass==2.and.ISLAVE)  nvcod        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = mvcob
           if(ipass==2.and.ISLAVE)  mvcob        = parin(npari)
           npari=npari+1
           if(ipass==2.and.IMASTER) parin(npari) = nvcob
           if(ipass==2.and.ISLAVE)  nvcob        = parin(npari)
           do ii = 1,size(npbcs)
              npari = npari+1
              if(ipass==2.and.IMASTER) parin(npari) = npbcs(ii)
              if(ipass==2.and.ISLAVE)  npbcs(ii)    = parin(npari)
           end do
           do ii = 1,size(lsbcs,2)
              do ji = 1,size(lsbcs,1)
                 npari = npari+1
                 if(ipass==2.and.IMASTER) parin(npari) = lsbcs(ji,ii)
                 if(ipass==2.and.ISLAVE)  lsbcs(ji,ii) = parin(npari)
              end do
           end do

           if( ipass == 1 ) then
              allocate(parin(npari),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,servi),'PARIN','sendat',parin)
              allocate(parre(nparr),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,servi),'PARRE','sendat',parre)
              if( ISLAVE ) call par_receiv()
           end if
        end do

        if( IMASTER ) call par_sendin()

        call memchk(two,istat,mem_servi(1:2,servi),'PARIN','sendat',parin)
        deallocate(parin,stat=istat)
        if(istat/=0) call memerr(two,'PARIN','sendat',0_ip)
        call memchk(two,istat,mem_servi(1:2,servi),'PARRE','sendat',parre)
        deallocate(parre,stat=istat)
        if(istat/=0) call memerr(two,'PARRE','sendat',0_ip)

     end if

  case(3)

     if( IPARALL ) then
        strre='cderda_reaset'
        strin='cderda_reaset'
        strch='cderda_reaset'
        do parii=1,2 
           npari=0
           nparr=0
           nparc=0
           !
           ! Calculated in cderda
           !
           call iexcha(iesta_dom)
           call iexcha(iesto_dom)
           call iexcha(ibsta_dom)
           call iexcha(ibsto_dom)
           call iexcha(ndimb)
           call iexcha(ntens)
           call iexcha(niner)
           call iexcha(mnode)
           call iexcha(mgaus)
           call iexcha(mnoga)
           call iexcha(mnodb)
           call iexcha(mgaub)
           call iexcha(mlapl)
           do ji=1,nelty
              call iexcha(ngaus(ji))
           end do
           do ji=1,nelty
              call iexcha(lrule(ji))
           end do
           do ji=1,nelty
              call iexcha(nface(ji))
           end do
           do ji=1,nelty
              call rexcha(hnatu(ji))
           end do
           !
           ! Immersed boundary: LEXIB, NGAIB, MNOIB, MGAIB, LRUIB, LQUIB
           !
           !call ibmdef(1_ip) ! CRISTOBAL
           !do ji=1,nelty
           !   call iexcha(lexib(ji))
           !end do
           !do ji=1,nelty
           !   call iexcha(ngaib(ji))
           !end do
           !do ji=1,nelty
           !   call iexcha(lruib(ji))
           !end do
           !do ji=1,nelty
           !   call iexcha(lquib(ji))
           !end do
           !call iexcha(mnoib)
           !call iexcha(mgaib)
           !
           ! Read in reaset
           !
           call iexcha(ndidi)
           call iexcha(neset)
           call iexcha(nbset)
           call iexcha(nnset)     ! Re-computed in par_senset
           call iexcha(kfl_neset) 
           do ii = 1,3
              call rexcha(setno(ii))
              do ji = 1,4
                 call rexcha(setpo(ii,ji))
              end do
           end do
           !call iexcha(nwitn) ! Re-computed in par_senset

           if(parii==1) then
              allocate(parin(npari),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,servi),'PARIN','sendat',parin)
              allocate(parre(nparr),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,servi),'PARRE','sendat',parre)
              if(ISLAVE) call par_receiv()
           end if
        end do

        if(IMASTER) call par_sendin()        

        call memchk(two,istat,mem_servi(1:2,servi),'PARIN','sendat',parin)
        deallocate(parin,stat=istat)
        if(istat/=0) call memerr(two,'PARIN','sendat',0_ip)
        call memchk(two,istat,mem_servi(1:2,servi),'PARRE','sendat',parre)
        deallocate(parre,stat=istat)
        if(istat/=0) call memerr(two,'PARRE','sendat',0_ip)

     end if

  case(4)

     if(ISLAVE) call memgeo(one)

  case(5)

     if( kfl_paral >= 0 ) then
        strre='par_partit'
        strin='par_partit'
        strch='par_partit'
        do ipass=1,2
           ii=0
           ir=0
           ic=0
           !
           ! Calculated in partit
           !
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii) = gni
           if(ipass==2.and.ISLAVE)  gni       = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii) = gnb
           if(ipass==2.and.ISLAVE)  gnb       = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii) = ginde_par(3,kfl_desti_par)
           if(ipass==2.and.ISLAVE)  lni       = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii) = ginde_par(4,kfl_desti_par)
           if(ipass==2.and.ISLAVE)  lnb       = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii) = lneig_par(kfl_desti_par)
           if(ipass==2.and.ISLAVE)  nneig     = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii) = slfbo_par(kfl_desti_par)
           if(ipass==2.and.ISLAVE)  slfbo     = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii)   = npoin_total
           if(ipass==2.and.ISLAVE)  npoin_total = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii)   = nelem_total
           if(ipass==2.and.ISLAVE)  nelem_total = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii)   = npoin_total
           if(ipass==2.and.ISLAVE)  npoin_total = parin(ii)
           ii=ii+1
           if(ipass==2.and.IMASTER) parin(ii)   = nboun_total
           if(ipass==2.and.ISLAVE)  nboun_total = parin(ii)
           if(ipass==1) then
              npari = ii
              nparr = ir
              nparc = ic
              allocate(parin(npari),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,servi),'PARIN','sendat',parin)
              allocate(parre(nparr),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,servi),'PARRE','sendat',parre)
              if(ISLAVE) call par_receiv()
           end if
        end do
        if(IMASTER) call par_sendin()

        call memchk(two,istat,mem_servi(1:2,servi),'PARIN','sendat',parin)
        deallocate(parin,stat=istat)
        if(istat/=0) call memerr(two,'PARIN','sendat',0_ip)
        call memchk(two,istat,mem_servi(1:2,servi),'PARRE','sendat',parre)
        deallocate(parre,stat=istat)
        if(istat/=0) call memerr(two,'PARRE','sendat',0_ip)

     end if

  case(6_ip)     
     !
     ! Exchange postprocess structure
     !
     kfl_ptask_old= kfl_ptask
     kfl_ptask    = 1

     do modul = 1,mmodu
        if( kfl_modul(modul) == 1 ) then
           do parii = 1,2 
              npari = 0
              nparr = 0
              nparc = 0
              !
              ! Exchange of nsi_reaphy variables 
              !
              call posdef(1_ip,dummi)
              !
              ! Allocate memory for the first pass
              !
              if(parii==1) then
                 allocate(parin(npari),stat=istat)
                 call memchk(zero,istat,mem_servi(1:2,5),'parin','nsi_sendat',parin)
                 allocate(parre(nparr),stat=istat)
                 call memchk(zero,istat,mem_servi(1:2,5),'parre','nsi_sendat',parre)
                 if( ISLAVE .or. kfl_ptask == 2 ) call par_broadc()
              end if
           end do

           if( IMASTER .and. kfl_ptask /= 2 ) call par_broadc()
           call memchk(two,istat,mem_servi(1:2,5),'parin','nsi_sendat',parin)
           deallocate(parin,stat=istat)
           if(istat/=0) call memerr(two,'parin','nsi_sendat',0_ip)
           call memchk(two,istat,mem_servi(1:2,5),'parre','nsi_sendat',parre)
           deallocate(parre,stat=istat)
           if(istat/=0) call memerr(two,'parre','nsi_sendat',0_ip)

        end if
     end do

     kfl_ptask = kfl_ptask_old

  case(7_ip)     
     !
     ! Boundary conditions
     !
     !call spnbcs(tncod_ker)
     !call spgbcs(tgcod_ker)
     !call spbbcs(tbcod_ker)

     if( IPARALL ) then
        strre='cderda_reaset'
        strin='cderda_reaset'
        strch='cderda_reaset'
        do parii=1,2 
           npari=0
           nparr=0
           nparc=0
           !
           ! Other variables
           !
           call iexcha(kfl_schur)
           call iexcha(kfl_aiipr)

           if(parii==1) then
              allocate(parin(npari),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,5),'parin','nsi_sendat',parin)
              allocate(parre(nparr),stat=istat)
              call memchk(zero,istat,mem_servi(1:2,5),'parre','nsi_sendat',parre)
              if( ISLAVE .or. kfl_ptask == 2 ) call par_broadc()
           end if
        end do

        if( IMASTER .and. kfl_ptask /= 2 ) call par_broadc()
        call memchk(two,istat,mem_servi(1:2,5),'parin','nsi_sendat',parin)
        deallocate(parin,stat=istat)
        if(istat/=0) call memerr(two,'parin','nsi_sendat',0_ip)
        call memchk(two,istat,mem_servi(1:2,5),'parre','nsi_sendat',parre)
        deallocate(parre,stat=istat)
        if(istat/=0) call memerr(two,'parre','nsi_sendat',0_ip)

     end if

  end select

  npari=0
  nparr=0
  nparc=0

end subroutine par_sendat

