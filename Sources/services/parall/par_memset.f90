subroutine par_memset(itask)
  !-----------------------------------------------------------------------
  !****f* Parall/par_openfi
  ! NAME
  !    par_openfi
  ! DESCRIPTION
  !    Allocate memory for node set and witness points treatment
  ! USED BY
  !    par_senset
  !    par_outprt
  !***
  !-----------------------------------------------------------------------
  use  def_parame
  use  def_master
  use  def_domain
  use  def_parall
  use  mod_memchk        
  implicit none
  integer(ip), intent(in) :: itask
  integer(4)              :: istat

  select case(itask)

  case(1)
     !
     ! Node sets
     !
     allocate(nnset_par(npart_par),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'NNSET_PAR','par_senset',nnset_par)
     if(nnset>0) then
        allocate(lnsec_par(nnset,2),stat=istat)
        call memchk(zero,istat,mem_servi(1:2,servi),'LNSEC_PAR','par_senset',lnsec_par)
     end if

  case(2)
     !
     ! Witness points
     !
     allocate(nwitn_par(npart_par),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'NWITN_PAR','par_senset',nwitn_par)
     
  end select

end subroutine par_memset
