subroutine par_sengeo(itask)
  !------------------------------------------------------------------------
  !****f* Parall/par_sengeo
  ! NAME
  !    par_sengeo
  ! DESCRIPTION
  !    Send 
  !    LNINV_LOC:  1->npoin to 1->npoin_total
  !    XLNIN_LOC:  Pointer to LNINV_LOC   
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_parall
  use def_domain
  use def_master
  use mod_memory
  implicit none  
  integer(ip), intent(in) :: itask
  integer(ip)             :: ielem,jelem,inode,jpoin,ipoin,inodb,jboun,kelem
  integer(ip)             :: indice0,indice1,indice2,indice3,indice4,indice5
  integer(ip)             :: ii,jj,iboun_loc,ielem_loc,iposi,iboun,nnodb,itotn
  integer(ip)             :: domai,kperi,iperi,idime,kdime,ifiel,izone
  integer(ip), pointer    :: indice_dom(:)  => null()
  integer(ip), pointer    :: lnods_loc(:)   => null()
  integer(ip), pointer    :: lnodb_loc(:)   => null()
  integer(ip), pointer    :: ltype_loc(:)   => null()
  integer(ip), pointer    :: ltypb_loc(:)   => null()
  integer(ip), pointer    :: lboel_loc(:,:) => null()
  integer(ip), pointer    :: lperi_loc(:,:) => null()
  integer(ip), pointer    :: lnnod_loc(:)   => null()
  integer(ip), pointer    :: lelch_loc(:)   => null()
  integer(ip), pointer    :: lninv_tmp(:)   => null()
  integer(ip), pointer    :: lgrou_loc(:)   => null()
  integer(ip), pointer    :: lnoch_loc(:)   => null()
  integer(ip), pointer    :: lelez_loc(:)   => null()
  integer(ip), pointer    :: lelez_tmp(:,:) => null()
  integer(ip), pointer    :: lboch_loc(:)   => null()
  integer(ip), pointer    :: lesub_loc(:)   => null()
  integer(ip), target     :: pperi(1) 
  real(rp),    pointer    :: coord_loc(:)   => null()
  logical(lg)             :: found

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

  select case( itask ) 

  case( 1_ip )

     if( IMASTER ) then

        !----------------------------------------------------------------
        !
        ! Master
        !
        !----------------------------------------------------------------
        !
        ! Nodal arrays
        !
        call memory_alloca(mem_servi(1:2,servi),'LNOCH_LOC','par_sengeo',lnoch_loc,npoin_total)
        call memory_alloca(mem_servi(1:2,servi),'LNINV_TMP','par_sengeo',lninv_tmp,npoin_total)
        call memory_alloca(mem_servi(1:2,servi),'COORD_LOC','par_sengeo',coord_loc,npoin_total*ndime)

        ii = 0
        jj = 0
        do ipoin = 1, npoin_total
           jj    = jj + 1
           jpoin = lninv_loc(ipoin)
           lninv_tmp(jj) = jpoin
           lnoch_loc(jj) = lnoch(jpoin)
           do idime = 1,ndime
              ii = ii + 1
              coord_loc(ii) = coord(idime,jpoin)
           end do
        end do
        !
        ! Elements arrays
        !
        call memory_alloca(mem_servi(1:2,servi),'LNODS_LOC','par_sengeo',lnods_loc,mnode*nelem)
        call memory_alloca(mem_servi(1:2,servi),'LTYPE_LOC','par_sengeo',ltype_loc,nelem)
        call memory_alloca(mem_servi(1:2,servi),'LNNOD_LOC','par_sengeo',lnnod_loc,nelem)
        call memory_alloca(mem_servi(1:2,servi),'LELCH_LOC','par_sengeo',lelch_loc,nelem)
        call memory_alloca(mem_servi(1:2,servi),'LEINV_LOC','par_sengeo',leinv_loc,nelem)
        call memory_alloca(mem_servi(1:2,servi),'LESUB_LOC','par_sengeo',lesub_loc,nelem)

        do ielem = 1, nelem
           jelem            = leinv_par(ielem)
           domai            = lepar_par(jelem)
           ltype_loc(ielem) = ltype(jelem)
           lnnod_loc(ielem) = lnnod(jelem)
           lelch_loc(ielem) = lelch(jelem)
           leinv_loc(ielem) = jelem
           lesub_loc(ielem) = lesub(jelem)

           do inode = 1, nnode(abs(ltype(jelem)))
              jpoin = lnper_par( lnods(inode,jelem) )
              iposi = ( ielem - 1 ) * mnode + inode
              if( jpoin <= gni ) then 
                 !
                 ! Internal node
                 !
                 lnods_loc(iposi) = jpoin - ginde_par(1,domai) + 1
              else  
                 !
                 ! Boundary node
                 !
                 jpoin = jpoin - gni
                 ii    = badj(jpoin)
                 do while( bdom(ii) /= domai )
                    ii = ii + 1
                    if( ii== badj(jpoin+1) ) &
                         call runend('PAR_SENGEO (LNODB): '&
                         //' GLOBAL NODE '//trim(intost(lnods(inode,ielem)))&
                         //' ('//trim(intost(jpoin+gni))//' IN I/B NUMBERING)'&
                         //' (LOCAL NODE '//trim(intost(inode))&
                         //' OF BOUNDARY '//trim(intost(ielem))//')'&
                         //' DOES NOT BELONG TO DOMAIN '&
                         //trim(intost(domai)))
                 end do
                 lnods_loc(iposi) = bpoin(ii)
              endif
           end do
        end do
        !
        ! Boundary arrays
        !
        call memory_alloca(mem_servi(1:2,servi),'LNODB_LOC','par_sengeo',lnodb_loc,nboun*mnodb)
        call memory_alloca(mem_servi(1:2,servi),'LTYPB_LOC','par_sengeo',ltypb_loc,nboun)
        call memory_alloca(mem_servi(1:2,servi),'LBOCH_LOC','par_sengeo',lboch_loc,nboun)

        do iboun = 1,nboun
           jboun = lbper_par(iboun)
           domai = lbpar_par(iboun)
           ltypb_loc(jboun) = ltypb(iboun)
           lboch_loc(jboun) = lboch(iboun)
           do inodb = 1,nnode(ltypb(iboun))
              jpoin = lnper_par( lnodb(inodb,iboun) )
              iposi = ( jboun - 1 ) * mnodb + inodb
              if( jpoin <= gni ) then
                 !
                 ! Internal node
                 !
                 lnodb_loc(iposi) = jpoin - ginde_par(1,domai) + 1
              else 
                 !
                 ! Boundary node
                 !
                 jpoin = jpoin - gni
                 ii    = badj(jpoin)
                 do while( bdom(ii) /= domai )
                    ii = ii + 1
                    if( ii == badj(jpoin+1) ) &
                         call runend('PAR_SENGEO (LNODB): '&
                         //' GLOBAL NODE '//trim(intost(lnodb(inodb,iboun)))&
                         //' ('//trim(intost(jpoin+gni))//' IN I/B NUMBERING)'&
                         //' (LOCAL NODE '//trim(intost(inodb))&
                         //' OF BOUNDARY '//trim(intost(iboun))//')'&
                         //' DOES NOT BELONG TO DOMAIN '&
                         //trim(intost(domai)))
                 end do
                 lnodb_loc(iposi) = bpoin(ii)
              endif
           end do
        end do
        !
        ! Send data
        !
        nparc   = 0
        indice0 = 1
        indice1 = 1
        indice2 = 1
        indice3 = 1
        indice4 = 1
        indice5 = 1
        do domai= 1, npart_par
           kfl_desti_par = domai
           !
           ! Send LNOCH_LOC
           !
           npari =  npoin_par(domai)
           parin => lnoch_loc(indice0:)
           strin =  'LNOCH_LOC'
           call par_sendin()
           !
           ! Send LNINV_TMP
           !
           npari =  npoin_par(domai)
           parin => lninv_tmp(indice0:)
           strin =  'LNINV_TMP'
           call par_sendin()
           !
           ! Send COORD_LOC
           !
           nparr =  ndime*npoin_par(domai)
           parre => coord_loc(indice1:)
           strre =  'COORD_LOC'
           call par_sendin()
           !
           ! Send LNODS_LOC
           !
           npari =  mnode*nelem_par(domai)
           parin => lnods_loc(indice4:)
           strin =  'LNODS_LOC'
           call par_sendin()
           !
           ! Send LTYPE_LOC
           !
           npari =  nelem_par(domai)
           parin => ltype_loc(indice2:)
           strin =  'LTYPE_LOC'
           call par_sendin()
           !
           ! Send LNNOD_LOC
           !
           npari =  nelem_par(domai)
           parin => lnnod_loc(indice2:)
           strin =  'LNNOD_LOC'
           call par_sendin()
           !
           ! Send LELCH_LOC
           !
           npari =  nelem_par(domai)
           parin => lelch_loc(indice2:)
           strin =  'LELCH_LOC'
           call par_sendin()
           !
           ! Send LEINV_LOC
           !
           npari =  nelem_par(domai)
           parin => leinv_loc(indice2:)
           strin =  'LEINV_LOC'
           call par_sendin()
           !
           ! Send LESUB_LOC
           !
           npari =  nelem_par(domai)
           parin => lesub_loc(indice2:)
           strin =  'LESUB_LOC'
           call par_sendin()

           if( nboun_par(domai) > 0 ) then
              !
              ! Send LNODB_LOC
              !
              npari =  mnodb*nboun_par(domai)
              parin => lnodb_loc(indice5:)
              strin =  'LNODB_LOC'
              call par_sendin()
              !
              ! Send LTYPB_LOC
              !
              npari =  nboun_par(domai)
              parin => ltypb_loc(indice3:)
              strin =  'LTYPB_LOC'
              call par_sendin()
              !
              ! Send LBOCH_LOC
              !
              npari =  nboun_par(domai)
              parin => lboch_loc(indice3:)
              strin =  'LBOCH_LOC'
              call par_sendin()
           end if

           indice0 = indice0 + npoin_par(domai)
           indice1 = indice1 + npoin_par(domai) * ndime
           indice2 = indice2 + nelem_par(domai)
           indice3 = indice3 + nboun_par(domai)
           indice4 = indice4 + nelem_par(domai) * mnode
           indice5 = indice5 + nboun_par(domai) * mnodb

        end do
        !
        ! Deallocate memory
        !
        call memory_deallo(mem_servi(1:2,servi),'LBOCH_LOC','par_sengeo',lboch_loc)
        call memory_deallo(mem_servi(1:2,servi),'LTYPB_LOC','par_sengeo',ltypb_loc)
        call memory_deallo(mem_servi(1:2,servi),'LNODB_LOC','par_sengeo',lnodb_loc)

        call memory_deallo(mem_servi(1:2,servi),'LESUB_LOC','par_sengeo',lesub_loc)
        call memory_deallo(mem_servi(1:2,servi),'LEINV_LOC','par_sengeo',leinv_loc)
        call memory_deallo(mem_servi(1:2,servi),'LELCH_LOC','par_sengeo',lelch_loc)
        call memory_deallo(mem_servi(1:2,servi),'LNNOD_LOC','par_sengeo',lnnod_loc)
        call memory_deallo(mem_servi(1:2,servi),'LTYPE_LOC','par_sengeo',ltype_loc)
        call memory_deallo(mem_servi(1:2,servi),'LNODS_LOC','par_sengeo',lnods_loc)

        call memory_deallo(mem_servi(1:2,servi),'COORD_LOC','par_sengeo',coord_loc)
        call memory_deallo(mem_servi(1:2,servi),'LNINV_TMP','par_sengeo',lninv_tmp)
        call memory_deallo(mem_servi(1:2,servi),'LNOCH_LOC','par_sengeo',lnoch_loc)
        !
        ! Periodicity list
        !
        if( nperi > 0 ) then
           call memory_alloca(mem_servi(1:2,servi),'LPERI_LOC','par_sengeo',lperi_loc,mperi,nperi)
           do iperi = 1,nperi
              lperi_loc(1,iperi) = lperi(1,iperi)
              lperi_loc(2,iperi) = lperi(2,iperi)
           end do
           !call typarr(1_ip,mperi,nperi,lperi_loc,lperi,mem_servi(1:2,servi))
           indice1=0
           do domai=1,npart_par
              kfl_desti_par = domai
              kperi=1
              pperi(1) = 0
              do iperi=1,nperi
                 do ii=1,mperi
                    lperi_loc(ii,iperi)=0
                 end do
              end do
              do iperi=1,nperi
                 jj=0
                 do ii=1,size(lperi,1)
                    ipoin=lperi(ii,iperi)
                    found=.false.
                    jpoin=0
                    do while(jpoin<npoin_par(domai))
                       jpoin=jpoin+1
                       if(lninv_loc(indice1+jpoin)==ipoin) then
                          found=.true.
                          jj=jj+1
                          lperi_loc(jj,kperi)=jpoin
                          jpoin=npoin_par(domai)
                       end if
                    end do
                 end do
                 if( jj >= 2 ) then         ! At least two periodic nodes are in DOMAI
                    kperi = kperi + 1
                    pperi(1) = pperi(1) + 1
                 end if
              end do
              !
              ! Send PPERI (NPERI for slave) to slaves
              !
              npari =  1
              parin => pperi
              strin =  'LPERI'
              call par_sendin()
              !
              ! Send LPERI_LOC to slave
              !
              if( pperi(1) > 0 ) then
                 kdime =  mperi*pperi(1)
                 strin =  'LPERI_LOC'       
                 call par_srinte(1_ip,kdime,lperi_loc)
              end if
              indice1 = indice1 + npoin_par(domai)
           end do

           call memory_deallo(mem_servi(1:2,servi),'LPERI_LOC','par_sengeo',lperi_loc)
           call memgeo(-53_ip)            ! periodic nodes

        end if

        if( ngrou_dom > 0 ) then
           !
           ! Groups
           !
           call memory_alloca(mem_servi(1:2,servi),'LGROU_LOC','par_sengeo', lgrou_loc,npoin_total)
           jj = 0
           do ipoin = 1, npoin_total
              jj    = jj + 1
              jpoin = lninv_loc(ipoin)
              lgrou_loc(jj) = lgrou_dom(jpoin)
           end do
           !
           ! Send data
           !
           nparc   = 0
           indice0 = 1
           do domai= 1, npart_par
              kfl_desti_par = domai
              !
              ! Send LGROU_LOC
              !
              npari =  npoin_par(domai)
              parin => lgrou_loc(indice0:)
              strin =  'LGROU_LOC'
              call par_sendin()           
              indice0 = indice0 + npoin_par(domai)              
           end do
           call memgeo(-27_ip)
           call memory_deallo(mem_servi(1:2,servi),'LGROU_LOC','par_sengeo', lgrou_loc)

        end if
        !
        ! Zones
        !
        call memory_alloca(mem_servi(1:2,servi),'LELEZ_LOC','par_sengeo',lelez_loc,nzone*nelem)
        do izone = 1,nzone
           do kelem = 1,nelez(izone)
              ielem            = lelez(izone) % l(kelem)
              jelem            = leper_par(ielem)
              itotn            = ( jelem - 1 ) * nzone + izone
              lelez_loc(itotn) = 1
           end do
        end do
        nparc   = 0
        indice4 = 1
        do domai = 1,npart_par
           kfl_desti_par =  domai
           npari         =  nzone * nelem_par(domai)
           parin         => lelez_loc(indice4:)
           strin         =  'LELEZ_LOC'
           call par_sendin()
           indice4 = indice4 + nelem_par(domai)*nzone
        end do
        call memory_deallo(mem_servi(1:2,servi),'LELEZ_LOC','par_sengeo',lelez_loc)
        call memgeo(-511_ip)
        call memgeo( -49_ip)
        
     else if( ISLAVE ) then

        !----------------------------------------------------------------
        !
        ! Slaves
        !
        !----------------------------------------------------------------

        call memory_alloca(mem_servi(1:2,servi),'LNINV_LOC','par_sengeo',lninv_loc,npoin)
        call memory_alloca(mem_servi(1:2,servi),'LEINV_LOC','par_sengeo',leinv_loc,nelem)
        !
        ! Receive LNOCH_LOC
        !
        call par_srinte(2_ip,npoin,lnoch)
        !
        ! Receive LNINV_LOC
        !
        call par_srinte(2_ip,npoin,lninv_loc) 
        !
        ! Receive COORD
        !
        call par_srreal(2_ip,ndime*npoin,coord)
        !
        ! Receive LNODS
        !
        call par_srinte(2_ip,mnode*nelem,lnods)
        !
        ! Receive LTYPE
        !
        call par_srinte(2_ip,nelem,ltype)
        !
        ! Receive LNNOD
        !
        call par_srinte(2_ip,nelem,lnnod)
        !
        ! Receive LELCH
        !
        call par_srinte(2_ip,nelem,lelch)
        !
        ! Receive LEINV_LOC
        !
        call par_srinte(2_ip,nelem,leinv_loc) 
        !
        ! Receive LESUB_LOC
        !
        call par_srinte(2_ip,nelem,lesub) 

        if( nboun > 0 ) then
           !
           ! Receive LNODB
           !          
           call par_srinte(2_ip,mnodb*nboun,lnodb)
           !
           ! Receive LTYPB
           !
           call par_srinte(2_ip,nboun,ltypb)
           !
           ! Receive LBOCH
           !
           call par_srinte(2_ip,nboun,lboch)
        end if
        !
        ! Receive NPERI and LPERI_LOC
        !
        if( nperi /= 0 ) then
           npari =  1
           parin => pperi
           call par_receiv()
           nperi =  pperi(1)
           if(nperi>0) then
              call memgeo(53_ip)            ! Zones
              !allocate( lperi(nperi), stat=istat )
              !call memchk( zero, istat, mem_servi(1:2,servi), 'LPERI','par_sengeo', lperi)
              kdime =  mperi*nperi
              call par_srinte(2_ip,kdime,lperi)
              !call typarr(2_ip,mperi,nperi,lperi_loc,lperi,mem_servi(1:2,servi))
              !call memchk( two, istat, mem_servi(1:2,servi), 'LPERI_LOC','par_sengeo', lperi_loc)
              !deallocate( lperi_loc, stat=istat )
              !if(istat/=0) call memerr( two, 'LPERI_LOC', 'par_sengeo',0_ip)
           end if
        end if
        !
        ! Groups
        !
        if( ngrou_dom > 0 ) then
           call memgeo(27_ip)
           call par_srinte(2_ip,npoin,lgrou_dom)
        end if
        !
        ! Zones
        !
        call memory_alloca(mem_servi(1:2,servi),'LELEZ_TMP','par_sengeo',lelez_tmp,nzone,nelem)
        call par_srinte(2_ip,nzone*nelem,lelez_tmp)
        call memgeo(49_ip)        
        do ielem = 1,nelem
           izone = 0
           do while( izone < nzone )
              izone = izone + 1
              if( lelez_tmp(izone,ielem) /= 0 ) nelez(izone) = nelez(izone) + 1
           end do
        end do
        do izone = 1,nzone
           igene = izone
           call memgeo(51_ip)
           nelez(izone) = 0
        end do        
        do ielem = 1,nelem
           do izone = 1,nzone
              if( lelez_tmp(izone,ielem) /= 0 ) then
                 nelez(izone) = nelez(izone) + 1
                 lelez(izone) % l(nelez(izone)) = ielem
              end if
           end do
        end do
        call memory_deallo(mem_servi(1:2,servi),'LELEZ_TMP','par_sengeo',lelez_tmp)

     end if
     !
     ! Materials
     !
     if( nmate >= 1 ) then
        if( ISLAVE ) call memgeo( 34_ip)
        strin =  'LMATE'
        party =  1
        pardi =  1
        parki =  1 
        pari1 => lmate
        call par_gather()
        if( IMASTER ) call memgeo(-34_ip)
     end if

  case( 2_ip )

     if( IMASTER .and. kfl_ptask /= 2 ) then

        call memory_alloca(mem_servi(1:2,servi),'LBOEL_LOC','par_sengeo', lboel_loc,mnodb+1_ip,nboun)
        call memory_alloca(mem_servi(1:2,servi),'INDICE_DOM','par_sengeo',indice_dom,npart_par+1_ip)
        indice_dom(1) = 1
        do domai= 1,npart_par
           indice_dom(domai+1)  = indice_dom(domai) + nelem_par(domai)
        end do

        do iboun = 1, nboun
           iboun_loc = lbper_par(iboun)
           nnodb     = nnode(ltypb(iboun))
           domai     = lbpar_par(iboun)
           do inodb = 1, nnodb
              lboel_loc(inodb,iboun_loc) = lboel(inodb,iboun) 
           end do
           ielem     = lboel(nnodb+1,iboun)
           ielem_loc = leper_par(ielem)-indice_dom(domai)+1
           lboel_loc(nnodb+1,iboun_loc)=ielem_loc
        end do
        nparc   = 0
        nparr   = 0
        indice3 = 1
        do domai= 1, npart_par
           kfl_desti_par = domai
           !
           ! Send LBOEL_LOC
           !
           if( nboun_par(domai) > 0 ) then
              kdime =  (mnodb+1)*nboun_par(domai)
              strin =  'LBOEL_LOC'
              call par_srinte(1_ip,kdime,lboel_loc(1,indice3))
           end if
           indice3 = indice3 + nboun_par(domai)
        end do
        !
        ! Deallocate memory
        !
        call memory_deallo(mem_servi(1:2,servi),'INDICE_DOM','par_sengeo',indice_dom)
        call memory_deallo(mem_servi(1:2,servi),'LBOEL_LOC','par_sengeo', lboel_loc)

     else if(ISLAVE) then
        !
        ! Receive LBOEL
        !
        if( nboun > 0 ) then
           call par_srinte(2_ip,(mnodb+1)*nboun,lboel)
        end if

     end if

  case( 3_ip )

     !----------------------------------------------------------------------
     !
     ! Fields
     !
     !----------------------------------------------------------------------

     if( nfiel > 0 ) then
        if( ISLAVE ) call memgeo(28_ip)
        !
        ! Cannot use a broadcast because in read_and_run mode master
        ! wants to read from file... and master does not get in this subroutine
        !
        if( IMASTER ) then
           do kfl_desti_par = 1,npart_par
              call par_parari('SND',0_ip,4*nfiel,kfl_field)
           end do
        else
           kfl_desti_par = 0
           call par_parari('RCV',0_ip,4*nfiel,kfl_field)
        end if

        do ifiel = 1,nfiel
           if( kfl_field(1,ifiel) /= 0 ) then
              !
              ! This field has been defined
              !
              igene = ifiel
              if( ISLAVE ) then
                 call memgeo(29_ip)
              end if
              if( kfl_field(2,igene) == NPOIN_TYPE ) then
                 call par_pararr('GAT',NPOIN_TYPE,kfl_field(1,ifiel)*kfl_field(4,ifiel)*npoin,xfiel(ifiel) % a)
              else if( kfl_field(2,igene) == NELEM_TYPE ) then
                 call par_pararr('GAT',NELEM_TYPE,kfl_field(1,ifiel)*kfl_field(4,ifiel)*nelem,xfiel(ifiel) % a)
              else
                 call par_pararr('GAT',NBOUN_TYPE,kfl_field(1,ifiel)*kfl_field(4,ifiel)*nboun,xfiel(ifiel) % a)
              end if
              if( IMASTER ) then    ! borrowed from par_senbcs L62
                 do kfl_desti_par = 1,npart_par
                    call par_pararr('SND',0_ip,kfl_field(4,ifiel),time_field(ifiel) % a)
                 end do
                 call memgeo(-29_ip)
              else
                 kfl_desti_par = 0
                 call par_pararr('RCV',0_ip,kfl_field(4,ifiel),time_field(ifiel) % a)
              end if
              
           end if

        end do

     end if

  end select

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine par_sengeo
