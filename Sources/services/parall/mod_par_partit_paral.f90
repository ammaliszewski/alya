module mod_par_partit_paral

use def_kintyp
use mod_parall
  use mod_elmgeo,           only : element_type
  integer(ip)             :: &
       kfl_paral_parmes = 2           ! Number of mesh partition slaves
  integer(ip)             :: &
       kfl_paral_proc_node = 1         ! Cada cuantos slaves hay que coger uno para particionar
                                        ! Ejemplo, kfl_paral_proc_node = 3, cogeríamos los slaves 1, 4, 7, 10, 13...

  !nuevas variables
  integer(ip), target     :: nelem_part, nelem_part_r, nedge_par, pelel_idx, pelel_last, &
                               npoin_part, npoin_part_r, nBoundElem, coordRange, coordOffset
  integer(ip), pointer    :: pelel_loc(:)   => null()
  integer(ip), pointer    :: elel_loc(:)   => null()
  integer(ip), pointer    :: vtxdist(:)   => null()
  integer(ip), pointer    :: wvert_loc(:)   => null()
  integer(ip), pointer    :: wedge_loc(:)   => null()
  integer(ip), pointer    :: recvcounts(:) => null()
  integer(ip), pointer    :: displs(:) => null()  
  integer(ip), pointer :: lnods_part(:,:)   => null()
  integer(ip), pointer :: ltype_part(:)   => null()
  real(rp), pointer :: coord_part(:,:)   => null()
  real(rp), pointer :: coord_part_aux(:,:)   => null()
  integer(ip) :: coord_idx, lnods_idx, coordCount
  integer(ip), pointer :: coord_rv_idx(:)   => null()
  integer(ip), pointer :: coord_rv_rank(:)   => null() 
  real(rp),    pointer :: coord_rv(:,:)      => null()! NPOIN
  real(rp),    pointer :: coord_local_part(:,:)      => null()! NPOIN
  logical(lg),            allocatable :: coord_local_part_lo(:)
  integer(ip), pointer :: coord_local_part_inv(:)   => null()
  integer(ip), pointer :: lnods_local_part(:,:)   => null()
  integer(ip), pointer :: lnods_local(:,:)   => null()
  integer(ip), pointer :: coord_local_inv(:)   => null()
  integer(ip), pointer     :: pelpo_part(:)      => null()
  integer(ip), pointer     :: lelpo_part(:)      => null()
  integer(ip), pointer     :: boundElem_part(:)      => null()
  integer(8)               :: &
       memor_par(2)                  ! Memory counter
  
  !partition faces     
  integer(ip),  pointer :: faces_part(:,:,:) => null()
  integer(ip) :: nelemRecv, npoin_local_recv, npoin_local
  integer(ip), pointer :: elem_loc_recv(:)   => null()
  integer(ip), pointer :: ltype_recv(:)   => null()
  
contains

subroutine par_partit_paral()
  !-------------------------------------------------------------------------------
  !****f* Parall/par_arrays
  ! NAME
  !    par_arrays
  ! DESCRIPTION
  !
  ! INPUT
  !    Element graph
  ! OUTPUT
  !    Partition of the graph
  !    lepar_par
  !    leper_par
  !    leinv_par
  !    lnpar_par
  !    lnper_par
  !    lninv_par
  !    lneig_par
  !    ginde_par 
  !    lcomm_par
  ! USED BY
  !    par_partit
  !***
  !-------------------------------------------------------------------------------
  use def_parame 
  use def_elmtyp
  use def_domain 
  use def_parall
  use mod_parall
  use def_master
  use mod_memory
  use mod_graphs_paral
  implicit none
  integer(ip), pointer    :: wvert_par(:)
  integer(ip)             :: wedge_par(1)   
  integer(ip)             :: isign,ielty
  integer(ip)             :: dummi,ipart,ielem,kelem
  integer(ip)             :: nedge_tmp,kzone,izone,nelem_tmp
  integer(ip)             :: kpart,npart_cur
  integer(ip), pointer    :: ladja_tmp(:)
  integer(ip), pointer    :: padja_tmp(:)
  integer(ip), pointer    :: wvert_tmp(:)
  integer(ip), pointer    :: permr_tmp(:)
  integer(ip), pointer    :: invpr_tmp(:)
  integer(ip), pointer    :: npart_tmp(:)
  integer(ip), pointer    :: lepar_tmp(:)
  real(rp)                :: time0,time1,time2
  !
  ! Nullify pointers
  !
  nullify(ladja_tmp)
  nullify(padja_tmp)
  nullify(wvert_tmp)
  nullify(permr_tmp)
  nullify(invpr_tmp)
  nullify(npart_tmp) 
  nullify(lepar_tmp)
  nullify(wvert_par)

  !----------------------------------------------------------------------
  !
  ! Compute Vertex weights WVERT_PAR = # of Gauss points of the element
  !
  !----------------------------------------------------------------------

  call par_livinf(20_ip,'  ALLOCATE MEMORY...',dummi) 
  call memory_alloca(mem_servi(1:2,servi),'WVERT_PAR','par_prepro',wvert_par,nelem)
  !
  ! If hole elements are taken off, one subdomain can end up with all the hole elements
  ! as they have null weight!
  ! 
  call par_livinf(20_ip,'  SET WEIGHTS... ',dummi)
  if( kfl_weigh_par == 0 ) then
     kfl_weigh_par = 2                  ! 0: No weights, 1: edges, 2: vertices, 3: both
     do ielem = 1,nelem
        ielty = abs(ltype(ielem))
        if( lelch(ielem) == ELHOL ) then
           !wvert_par(ielem) = 0 ! See comment above
           wvert_par(ielem) = ngaus(ielty) 
        else
           wvert_par(ielem) = ngaus(ielty)
        end if
     end do
  else
     kfl_weigh_par = 0                  ! 0: No weights, 1: edges, 2: vertices, 3: both
     do ielem = 1,nelem
        ielty = abs(ltype(ielem))
        wvert_par(ielem) = 1_ip
     end do
  end if
  wedge_par = 0

  !----------------------------------------------------------------------
  !
  ! Compute element graph: PADJA_PAR (PELEL) and LADJA_PAR (LELEL)
  ! using node or face connectivity
  !
  !----------------------------------------------------------------------

  call cputim(time0)
  call par_memory(1_ip)
  call par_memory(8_ip)
  call par_livinf(2_ip,' ',dummi)

  call par_elmgra()

  call par_livinf(7_ip,' ',0_ip)       
  call cputim(time1)
  cpu_paral(3) = time1 - time0

  !----------------------------------------------------------------------
  !
  ! Partition mesh with PARAMETIS
  !
  !----------------------------------------------------------------------

  call par_livinf(3_ip,' ',dummi)

  if( nzone_par == 1 ) then
     !
     ! Only one zone
     !
     !call par_metis(                                             &
     !     1_ip , nelem, nedge , padja_par, ladja_par, wvert_par, &
     !     wedge_par, kfl_weigh_par, npart_par,                   &
     !     lepar_par ,dummi , dummi , dummi , dummi ,             & 
     !     dummi, dummi , nelem , 1_ip , mem_servi(1,servi)       )
     !Envio de datos a los slave encargados de partir la malla
     call par_senmes(wvert_par,wedge_par)
     call par_recpar()

  else
     !
     ! Multiple zones
     !
     call memory_alloca(mem_servi(1:2,servi),'NPART_TMP','par_arrays',npart_tmp,nzone_par)
     nelem_tmp = 0
     do izone = 1,nzone_par
        nelem_tmp = nelem_tmp + nelez(izone)
     end do

     if( nelem_tmp /= nelem ) then
        write(*,*) nelem,nelem_tmp
        call runend('PAR_ARRAYS: WRONG ZONAL-WISE PARTITION')
     end if

     npart_cur = 0
     do kzone = 1,nzone_par
        npart_tmp(kzone) = max(1_ip,int(real(nelez(kzone)* npart_par,rp) / real(nelem,rp) ,ip))
        npart_cur = npart_cur + npart_tmp(kzone)
     end do
     do while( npart_cur /= npart_par )

        if( npart_cur > npart_par ) then
           isign =  1
           dummi =  0
        else
           isign = -1
           dummi = -huge(1_ip)
        end if
        izone = 0
        do kzone = 1,nzone_par
           if( isign * npart_tmp(kzone) > dummi ) then
              dummi = npart_tmp(kzone)
              izone = kzone
           end if
        end do
        npart_tmp(izone) = npart_tmp(izone) - isign
        npart_cur = npart_cur - isign
     end do

     call outfor(63_ip,lun_outpu_par,' ')
     do kzone = 1,nzone_par
        ioutp(1) = lzone_par(kzone)
        ioutp(2) = npart_tmp(kzone)   
        call outfor(64_ip,lun_outpu_par,' ')
     end do

     call memory_alloca(mem_servi(1:2,servi),'PADJA_TMP','par_arrays',padja_tmp,size(padja_par,kind=ip))
     call memory_alloca(mem_servi(1:2,servi),'LADJA_TMP','par_arrays',ladja_tmp,size(ladja_par,kind=ip))
     call memory_alloca(mem_servi(1:2,servi),'PERMR_TMP','par_arrays',permr_tmp,nelem)
     call memory_alloca(mem_servi(1:2,servi),'INVPR_TMP','par_arrays',invpr_tmp,nelem)

     kpart = 0

     do kzone = 1,nzone_par

        izone = lzone_par(kzone)

        if( npart_tmp(kzone) == 1 ) then

           do kelem = 1,nelez(izone)
              ielem            = lelez(izone) % l(kelem)
              lepar_par(ielem) = 1 + kpart
           end do

        else

           call memory_alloca(mem_servi(1:2,servi),'WVERT_TMP','par_arrays',wvert_tmp,nelez(izone))
           call memory_alloca(mem_servi(1:2,servi),'LEPAR_TMP','par_arrays',lepar_tmp,nelez(izone))

           do ielem = 1,nelem+1
              padja_tmp(ielem) = padja_par(ielem)
           end do
           do ielem = 1,size(ladja_par)              
              ladja_tmp(ielem) = ladja_par(ielem)
           end do

           do kelem = 1,nelez(izone)
              ielem            = lelez(izone) % l(kelem)
              permr_tmp(kelem) = ielem
              invpr_tmp(ielem) = kelem
              wvert_tmp(kelem) = wvert_par(ielem)
           end do

           nedge_tmp = nedge
           call graphs_subgra(nelem,nedge_tmp,permr_tmp,invpr_tmp,padja_tmp,ladja_tmp)

           call par_metis(                                                        &
                1_ip , nelez(izone), nedge_tmp , padja_tmp, ladja_tmp, wvert_tmp, &
                wedge_par, kfl_weigh_par, npart_tmp(kzone),                       &
                lepar_tmp ,dummi , dummi , dummi , dummi ,                        & 
                dummi, dummi , nelem , 1_ip , mem_servi(1,servi)                  )

           do kelem = 1,nelez(izone)
              ielem            = lelez(izone) % l(kelem)
              lepar_par(ielem) = lepar_tmp(kelem) + kpart
           end do

           call memory_deallo(mem_servi(1:2,servi),'LEPAR_TMP','par_arrays',lepar_tmp)
           call memory_deallo(mem_servi(1:2,servi),'WVERT_TMP','par_arrays',wvert_tmp)

           do ielem = 1,nelem
              permr_tmp(ielem) = 0
              invpr_tmp(ielem) = 0
           end do

        end if
        do ipart = 1,npart_tmp(kzone)
           lsubz_par(ipart+kpart) = izone
        end do
        kpart = kpart + npart_tmp(kzone)

     end do

     call memory_deallo(mem_servi(1:2,servi),'NPART_TMP','par_arrays',npart_tmp)
     call memory_deallo(mem_servi(1:2,servi),'INVPR_TMP','par_arrays',invpr_tmp)
     call memory_deallo(mem_servi(1:2,servi),'PERMR_TMP','par_arrays',permr_tmp)
     call memory_deallo(mem_servi(1:2,servi),'PADJA_TMP','par_arrays',padja_tmp)
     call memory_deallo(mem_servi(1:2,servi),'LADJA_TMP','par_arrays',ladja_tmp)

     do ielem = 1,nelem
        if( lepar_par(ielem) == 0 ) call runend('PAR_ARRAYS: ELEMENTS ARE MISSING')
     end do

  end if

  call cputim(time2)
  cpu_paral(5) = time2 - time1
  
  !----------------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !----------------------------------------------------------------------

  call memory_deallo(mem_servi(1:2,servi),'WVERT_PAR','par_prepro_D',wvert_par)

end subroutine par_partit_paral

subroutine par_parmetis(&       ! parmetis 4.x
     itask , nelemAux, nedge , vtxdist, xadj, adj, wvert_par, &
     wedge_par, kfl_weigh_par, npart_par,            &
     lepar_par , nbNodInter, xadjSubDom, adjSubDom,  &
     invpR, permR, dsize, nwver , nwedg , mem_servi) 
  !------------------------------------------------------------------------
  !****f* Parall/par_metis5
  ! NAME
  !    par_metis
  ! DESCRIPTION
  !    This routine is the bridge between Alya and PARMETIS
  !    Adapto las llamada a la version 4
  ! INPUT
  !    Element graph
  ! OUTPUT
  ! USED BY
  !    par_graph_arrays 
  !    par_disbou
  !***
  !------------------------------------------------------------------------
  use def_parame
  use mod_memchk 
  use mod_graphs
  use mod_memory
!  use iso_c_binding  ! for c_null_ptr esto ahoar nlo cambie por %VAL(nullvar)
  use iso_c_binding  ! for C_INTPTR_T
  use mod_graphs_paral

  implicit none
  integer(ip), intent(in)    :: itask,nelemAux,npart_par
  integer(ip), intent(in)    :: nedge,dsize,nwedg,nwver,nbNodInter
  integer(ip), intent(in)    :: xadj(*),xadjSubDom(*), vtxdist(*)
  integer(ip), intent(in)    :: adj(*),adjSubDom(*)
  integer(ip), intent(in)    :: wvert_par(*),wedge_par(*)
  integer(ip), intent(in)    :: kfl_weigh_par
  integer(ip), intent(out)   :: lepar_par(*)  
  integer(ip), intent(in)    :: invpR(*),permR(*)  
  integer(8),  intent(inout) :: mem_servi(2)
  integer(ip)                :: edgecut, idx
  integer(ip)                :: optio_par(0:2)
  integer(ip) :: WGTFLAG
  integer(ip) :: NUMFLAG
  integer(ip) :: NCON
  !real(rp)                :: tpwgts(1:npart_par), ubvec(1)
  real(rp), ALLOCATABLE :: TPWGTS(:), UBVEC(:)
  !integer(ip) :: VWGT(:), ADJWGT(:) 
#ifdef MPI_OFF
#else
  include  'mpif.h'
#endif

  integer(C_INTPTR_T) :: nullvar
  nullvar = 0

  !if ( ( wedge_par(1)/=0_ip ) .and. (itask==1_ip) ) then ! notice that I am passing %val(nullvar) in the place that
                                                         ! should be occupied by wedge_par
   ! call runend('par_metis: wedge_par(1)/=0_ip not ready')
  !end if

  !call memory_alloca(mem_servi(1:2,servi),'tpwgts','par_parmetis',TPWGTS, NCON*npart_par)
  NCON = 1_ip
  ALLOCATE (TPWGTS(NCON*npart_par))
  ALLOCATE (UBVEC(NCON))
  optio_par(:) = 0_ip     ! Default options
  UBVEC(:) = 1.05_ip
  WGTFLAG = 0_ip
  NUMFLAG = 1_ip
  !do idx=0,npart_par - 1
  !      tpwgts(idx) = 1.0/npart_par
  !enddo
  TPWGTS(:) = 1.0_rp/real(npart_par,rp)
  !
  ! Linking with METIS compiled with same integers as Alya
  !  
  if( itask == 1_ip ) then
    write (*,*) 'calling parmetis'
#ifdef PARMETIS

    !write(*,*) 'vtxdist:' , sizeof(vtxdist)
    !flush(6)
    !write(*,*) 'xadj:' , size(xadj) , ',' , lbound(xadj), ',' , ubound(xadj)
    !flush(6)
    !write(*,*) 'adj:' , size(adj) , ',' , lbound(adj), ',' , ubound(adj)
    !flush(6)
    !write(*,*) 'npart_par:' , npart_par
    !flush(6)
    !write(*,*) 'TPWGTS:' , size(UBVEC) , ',' , lbound(UBVEC), ',' , ubound(UBVEC)
    !flush(6)
    !write(*,*) 'TPWGTS:' , size(UBVEC) , ',' , lbound(UBVEC), ',' , ubound(UBVEC)
    !flush(6)    
    !write(*,*) 'optio_par:' , size(optio_par) , ',' , lbound(optio_par), ',' , ubound(optio_par)
    !flush(6)  
    !write(*,*) 'lepar_par:' , size(lepar_par) , ',' , lbound(lepar_par), ',' , ubound(lepar_par)
    !flush(6)
    !do while (1==1)
    !   call sleep(10)
    !end do
    call ParMETIS_V3_PartKway(&
             vtxdist, xadj, adj,  %val(nullvar), %val(nullvar), WGTFLAG, &
             NUMFLAG , NCON, npart_par,    &
             TPWGTS, UBVEC, optio_par, edgecut, lepar_par, PARMETIS_COMM )
#endif
    write (*,*) 'end calling parmetis'
  end if

end subroutine par_parmetis

subroutine par_senmes(wvert_par,wedge_par)
  !------------------------------------------------------------------------
  !****f* Parall/par_sengeo
  ! NAME
  !    par_sengeo
  ! DESCRIPTION
  !    Send 
  !    LNINV_LOC:  1->npoin to 1->npoin_total
  !    XLNIN_LOC:  Pointer to LNINV_LOC   
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_parall
  use def_domain
  use def_master
  use mod_memory
  implicit none
  integer(ip), intent(in) ,optional :: wvert_par(nelem)
  integer(ip), intent(in),optional :: wedge_par(*)
  integer(ip)             :: ielem,jelem,inode,jpoin,ipoin,inodb,jboun,kelem
  integer(ip)             :: indice0,indice1,indice2,indice3,indice4,indice5
  integer(ip)             :: ii,jj,iboun_loc,ielem_loc,iposi,iboun,nnodb,itotn
  integer(ip)             :: domai,kperi,iperi,idime,kdime,ifiel,izone
  integer(ip), pointer    :: indice_dom(:)  => null()
  integer(ip), pointer    :: lnods_loc(:)   => null()
  integer(ip), pointer    :: lnodb_loc(:)   => null()
  integer(ip), pointer    :: ltype_loc(:)   => null()
  integer(ip), pointer    :: ltypb_loc(:)   => null()
  integer(ip), pointer    :: lboel_loc(:,:) => null()
  integer(ip), pointer    :: lperi_loc(:,:) => null()
  integer(ip), pointer    :: lnnod_loc(:)   => null()
  integer(ip), pointer    :: lelch_loc(:)   => null()
  integer(ip), pointer    :: lninv_tmp(:)   => null()
  integer(ip), pointer    :: lgrou_loc(:)   => null()
  integer(ip), pointer    :: lnoch_loc(:)   => null()
  integer(ip), pointer    :: lelez_loc(:)   => null()
  integer(ip), pointer    :: lelez_tmp(:,:) => null()
  integer(ip), pointer    :: lboch_loc(:)   => null()
  integer(ip), target     :: pperi(1) 
  real(rp),    pointer    :: coord_loc(:)   => null()
  logical(lg)             :: found
  integer(ip), target     :: vartx(1)
#ifdef EVENT
  call mpitrace_user_function(1)
#endif

     if( IMASTER ) then

        !----------------------------------------------------------------
        !
        ! Master
        !
        !----------------------------------------------------------------
        ! --cosas que hay que enviar
        ! nelem, nedge , padja_par, ladja_par, wvert_par,
        ! wedge_par, kfl_weigh_par, npart_par,lepar_par
        
        ! Alocatar sólo lo necesario
        !calculamos el tamaño de cada trozo
        !call initTest()
        nelem_part = nelem / kfl_paral_parmes
        nelem_part_r = MOD(nelem, kfl_paral_parmes)
        
        !calculamos el vtxdist
        call memory_alloca(mem_servi(1:2,servi),'vtxdist','par_senmes',vtxdist, kfl_paral_parmes +1_ip)
        pelel_idx = 1
        do domai= 1, kfl_paral_parmes +1_ip
          vtxdist(domai) = pelel_idx   
          pelel_idx = pelel_idx + nelem_part
          if (domai == kfl_paral_parmes) then
            pelel_idx = pelel_idx + nelem_part_r
          end if
        end do
        
        !allocatamos espacio para los buffers de envio del resultado
        call memory_alloca(mem_servi(1:2,servi),'recvcounts','par_senmes',recvcounts, kfl_paral_parmes)
        call memory_alloca(mem_servi(1:2,servi),'displs','par_senmes',displs, kfl_paral_parmes)
                
        !allocatamos pelele_loc
        call memory_alloca(mem_servi(1:2,servi),'PELEL_LOC','par_senmes',pelel_loc, nelem_part +1_ip)
        
        pelel_idx = 0
        pelel_last = 0
        !para cada procesador
        do domai= 1, kfl_paral_parmes
           if (domai == kfl_paral_parmes .AND. nelem_part_r /= 0) then
             !recalculate nelem_part and realloc pelel_loc memory
             call memory_deallo(mem_servi(1:2,servi),'PELEL_LOC' ,'par_senmes' , pelel_loc  )
             call memory_alloca(mem_servi(1:2,servi),'PELEL_LOC','par_senmes',pelel_loc, nelem_part +1_ip+ nelem_part_r)
             nelem_part = nelem_part + nelem_part_r
           end if
           recvcounts(domai) = int(nelem_part,4);
           kfl_desti_par = domai
           
           !fill pelel_loc
           do ielem = 1, nelem_part + 1_ip
              pelel_loc(ielem) = padja_par(pelel_idx + ielem) - pelel_last
           end do
           
           !alloca elel_loc
           nedge_par = pelel_loc(nelem_part + 1_ip) - 1        
           call memory_alloca(mem_servi(1:2,servi),'ELEL_LOC','par_senmes',elel_loc, nedge_par)
           
           !fill elel_loc           
           do ielem = 1, nedge_par
              elel_loc(ielem) = ladja_par(pelel_last + ielem)
           end do
                      
           
           !Sending data to the slaves
           npari =  1_ip
           vartx(1) = nelem_part
           parin => vartx
           strin =  'nelem_part'
           call par_sendin()
             
           npari =  1_ip
           vartx(1) = nedge_par
           parin => vartx
           strin =  'nedge_par'
           call par_sendin()
             
           !
           ! Send pelel_loc
           !
           npari =  nelem_part + 1_ip
           parin => pelel_loc
           strin =  'pelel_LOC'
           call par_sendin()
           !
           ! Send elel_loc
           !
           npari =  nedge_par
           parin => elel_loc
           strin =  'elel_LOC'
           call par_sendin()
           !
           ! Send vtxdist
           !
           npari =  kfl_paral_parmes + 1_ip
           parin => vtxdist
           strin =  'vtxdist'
           call par_sendin()
           
           pelel_last = pelel_last + nedge_par
           pelel_idx = pelel_idx + nelem_part
           !deallocate elel_loc
           call memory_deallo(mem_servi(1:2,servi),'ELEL_LOC' ,'mod_par_arrays_paral' , elel_loc  )
        end do
        
        !Calculate the displacements to receive the results.
        displs(1) = 0
        do domai= 2, kfl_paral_parmes
          displs(domai) = displs(domai - 1) + recvcounts(domai - 1)            
        end do

     else if( ISLAVE ) then

        !----------------------------------------------------------------
        !
        ! Slaves
        !
        !----------------------------------------------------------------

        !call memory_alloca(mem_servi(1:2,servi),'LNINV_LOC','par_sengeo',lninv_loc,npoin)
        !call memory_alloca(mem_servi(1:2,servi),'LEINV_LOC','par_sengeo',leinv_loc,nelem)
        !
        ! Receive nelem_part
        !
        npari =  1
        parin => vartx
        call par_receiv()
        nelem_part =  vartx(1)
        !write(*,*) "SLAVE, nelem_part recibido:!" , nelem_part
        
        npari =  1
        parin => vartx
        call par_receiv()
        nedge_par =  vartx(1)
        !write(*,*) "SLAVE, nedge_par recibido:!" , nedge_par
        
        !
        ! Receive PELEL_LOC
        !
        call memory_alloca(mem_servi(1:2,servi),'PELEL_LOC','par_sengeo',pelel_loc,nelem_part + 1_ip)        
        call par_srinte(2_ip,nelem_part + 1_ip,pelel_loc)
        !write(*,*) "SLAVE, pelel_loc recibido:!"
        
        !
        ! Receive ELEL_LOC
        !
        call memory_alloca(mem_servi(1:2,servi), 'ELEL_LOC', 'par_sengeo', elel_loc, nedge_par)        
        call par_srinte(2_ip, nedge_par, elel_loc)
        !write(*,*) "SLAVE, elel_loc recibido:!"
                
        !
        ! Receive vtxdist
        !
        call memory_alloca(mem_servi(1:2,servi), 'vtxdist', 'par_sengeo', vtxdist, kfl_paral_parmes + 1_ip)        
        call par_srinte(2_ip, kfl_paral_parmes + 1_ip, vtxdist)
        !write(*,*) "SLAVE, vtxdist recibido:!"

     end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine par_senmes

subroutine par_partit_paral_slave()
  use def_parame 
  use def_domain 
  use def_parall
  use def_master
  use mod_memory
  use mod_graphs_paral
  use mod_parall, only : PAR_INTEGER

  implicit none
  integer(4)             :: npari4
  integer(4)           :: istat
  integer(ip)             :: dummi, i
  integer(ip), target     :: dummiv(1)
  integer(ip), pointer     :: lepar_par_loc(:)        ! Domain of every element at each processor
  !-----------------------------------
  !--1.Get input parameters
  !-----------------------------------
  call  par_senmes()
  call par_livinf(20_ip,'  ALLOCATE MEMORY...',dummi) 
  call memory_alloca(mem_servi(1:2,servi),'LEPAR_PAR_LOC','par_partit_paral_slave' , lepar_par_loc ,  nelem_part )
  call memory_alloca(mem_servi(1:2,servi),'WVERT_PAR','par_prepro',wvert_loc,nelem_part)
  call memory_alloca(mem_servi(1:2,servi),'WEDGE_LOC','par_prepro',wedge_loc,1_ip)
  
  !write(*,*) '!!!!!!!!!!!!!par_partit_paral_slave:' , kfl_paral , 'nelem_part:' , nelem_part
  
    !do i=1,kfl_paral_parmes + 1_ip
     !   write (*,*) 'Domain:' , i, 'Has elements:' , vtxdist(i)
    !enddo
    
    !do i=vtxdist(kfl_paral), vtxdist(kfl_paral + 1) - 1
    !    write (*,*) 'Domain:' , kfl_paral, 'Has elements:' , pelel_loc(i)
    !enddo
  !-----------------------------------
  !--2.Call parmetis
  !-----------------------------------
  !write(*,*) 'nelem_part:' , nelem_part
  !flush(6)
  !write(*,*) 'nedge_par:' , nedge_par
  !flush(6)
  !write(*,*) 'npart_par:' , nelem_part
  !flush(6)
  !write(*,*) 'nelem:' , nelem
  !flush(6)
  !write(*,*) 'vtxdist:' , size (vtxdist), lbound(vtxdist), ubound(vtxdist)
  !flush(6)
  !write(*,*) 'pelel_loc:' , size (pelel_loc), lbound(pelel_loc), ubound(pelel_loc)
  !flush(6)
  !write(*,*) 'elel_loc:' , size (elel_loc), lbound(elel_loc), ubound(elel_loc)
  !flush(6)
  !write(*,*) 'wvert_loc:' , size (wvert_loc), lbound(wvert_loc), ubound(wvert_loc)
  !flush(6)
  !write(*,*) 'wedge_loc:' , size (wedge_loc), lbound(wedge_loc), ubound(wedge_loc)
  !flush(6)
  !write(*,*) 'kfl_weight_par:' , kfl_weigh_par
  !flush(6)
  !write(*,*) 'lepar_par_loc:' , size (lepar_par_loc), lbound(lepar_par_loc), ubound(lepar_par_loc)
  !flush(6)
  !write(*,*) 'padja_par_VALUES:' , pelel_loc
  !flush(6
  !if (kfl_paral == 3) then
  !write(*,*) 'ladja_par_VALUES:' , elel_loc
  !flush(6)
  !  end if
  !       do while (1==1)
  !            call sleep(10)
  !      end do
  call par_parmetis(                                             &
    1_ip , nelem_part, nedge_par , vtxdist, pelel_loc, elel_loc, wvert_loc, &
    wedge_loc, kfl_weigh_par, npart_par,                   &
    lepar_par_loc ,dummi , [dummi] , [dummi] , [dummi] ,             & 
    [dummi], dummi , nelem , 1_ip , mem_servi(1,servi)       )
    
    !do i=1,nelem_part
    !    write (*,*) 'Element:' , i, 'Is in domain:' , lepar_par_loc(i)
    !enddo
    
  !-----------------------------------
  !--3.Send the results to the master
  !-----------------------------------
  npari =  nelem_part
  parin => lepar_par_loc
  strin =  'lepar_par_loc'
  npari4=int(npari,4)
  parig => dummiv
  call memory_alloca(mem_servi(1:2,servi),'recvcounts','par_senmes',recvcounts, kfl_paral_parmes)
  call memory_alloca(mem_servi(1:2,servi),'displs','par_senmes',displs, kfl_paral_parmes)
#ifdef MPI_OFF
#else
  CALL MPI_GatherV( parin(1:npari) , npari4 , PAR_INTEGER, parig(1:) , recvcounts, displs , &
             PAR_INTEGER , 0_4 , PARMETIS_INTERCOMM , istat)
#endif
  
  npari=0
  write (*,*) 'slave terminated:' , kfl_paral
  call memory_deallo(mem_servi(1:2,servi),'LEPAR_PAR_LOC','par_partit_paral_slave' , lepar_par_loc )
  call memory_deallo(mem_servi(1:2,servi),'recvcounts','par_partit_paral_slave' , recvcounts )
  call memory_deallo(mem_servi(1:2,servi),'displs','par_partit_paral_slave' , displs )
end subroutine par_partit_paral_slave


subroutine par_recpar()
  !------------------------------------------------------------------------
  !****f* Parall/par_sengeo
  ! NAME
  !    par_sengeo
  ! DESCRIPTION
  !    Send 
  !    LNINV_LOC:  1->npoin to 1->npoin_total
  !    XLNIN_LOC:  Pointer to LNINV_LOC   
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_parall
  use mod_parall, only : PAR_INTEGER
  use def_domain
  use def_master
  use mod_memory
  
  implicit none
#ifdef MPI_OFF
#else
  include  'mpif.h'
#endif
  integer(ip)             :: ielem,jelem,inode,jpoin,ipoin,inodb,jboun,kelem
  integer(ip)             :: indice0,indice1,indice2,indice3,indice4,indice5
  integer(ip)             :: ii,jj,iboun_loc,ielem_loc,iposi,iboun,nnodb,itotn
  integer(ip)             :: domai,kperi,iperi,idime,kdime,ifiel,izone
  integer(ip), pointer    :: indice_dom(:)  => null()
  integer(ip), pointer    :: lnods_loc(:)   => null()
  integer(ip), pointer    :: lnodb_loc(:)   => null()
  integer(ip), pointer    :: ltype_loc(:)   => null()
  integer(ip), pointer    :: ltypb_loc(:)   => null()
  integer(ip), pointer    :: lboel_loc(:,:) => null()
  integer(ip), pointer    :: lperi_loc(:,:) => null()
  integer(ip), pointer    :: lnnod_loc(:)   => null()
  integer(ip), pointer    :: lelch_loc(:)   => null()
  integer(ip), pointer    :: lninv_tmp(:)   => null()
  integer(ip), pointer    :: lgrou_loc(:)   => null()
  integer(ip), pointer    :: lnoch_loc(:)   => null()
  integer(ip), pointer    :: lelez_loc(:)   => null()
  integer(ip), pointer    :: lelez_tmp(:,:) => null()
  integer(ip), pointer    :: lboch_loc(:)   => null()
  integer(ip), target     :: pperi(1) 
  real(rp),    pointer    :: coord_loc(:)   => null()
  logical(lg)             :: found
  integer(ip), target     :: vartx(1)
  integer(4)             :: npari4
  integer(4)           :: istat
#ifdef EVENT
  call mpitrace_user_function(1)
#endif

     if( IMASTER ) then

        write(*,*) "MASTER, esperando a los slaves!"
        npari =  0
        parin => vartx
        parig => lepar_par 
        npari4=int(npari,4)
        
#ifdef MPI_OFF
#else
  !write(*,*)'RECVCOUNT SIZE:' , size(recvcounts), ',DISPLS:' , size(displs), ',LEPAR_PAR', size(lepar_par)
  CALL MPI_Gatherv( parin , npari4 , PAR_INTEGER, parig, recvcounts, displs, &
             PAR_INTEGER , MPI_ROOT , PARMETIS_INTERCOMM , istat)
#endif
        
        npari=0
        
        !do ii=1,nelem
        !   write (*,*) 'Element:' , ii, 'Is in domain:' , lepar_par(ii)
        !enddo
        
        call memory_deallo(mem_servi(1:2,servi),'recvcounts' ,'mod_par_arrays_paral' , recvcounts  )
        call memory_deallo(mem_servi(1:2,servi),'displs' ,'mod_par_arrays_paral' , displs  )

     else if( ISLAVE ) then
                
        !
        ! Receive vtxdist
        !
        call memory_alloca(mem_servi(1:2,servi), 'vtxdist', 'par_sengeo', vtxdist, kfl_paral_parmes + 1_ip)        
        call par_srinte(2_ip, kfl_paral_parmes + 1_ip, vtxdist)

     end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine par_recpar


subroutine initTest()
  use def_parame
  use def_master
  use def_domain
  use mod_memory
  use mod_htable
  use def_parall
  implicit none  
  integer(ip)           :: ielty,ielem,iface,inodb,ilist,kelem
  integer(ip)           :: inode,jelem,jface,jelty,ipoin,kelpo
  integer(ip)           :: pepoi,ielpo,dummi,pnodi,jnode
  integer(ip)           :: melel,pelty
  integer(8)            :: msize
  logical(lg)           :: equal_faces
  type(hash_t)          :: ht
  integer(ip),  pointer :: faces(:,:,:) => null()
  integer(ip),  pointer :: lista(:)     => null()
  
  nelem = 15
  !
  ! Allocate memory for PELEL
  !
  call memgeo(-41_ip)
  call memgeo(41_ip)
  
  nedge = 44
     !
     ! Allocate memoty for adjacancies LELEL
     !
  call memgeo(-43_ip)
  call memgeo(43_ip)
  
  pelel = (/ 1, 3, 6, 9, 12, 14, 17, 21, 25, 29, 32, 34, 37, 40, 43, 45 /)
  lelel = (/ 1, 5, 0, 2, 6, 1, 3, 7, 2, 4, 8, 3, 9, 0, 6, 10, 1, 5, 7, 11, 2, 6, 8, 12, 3, 7, 9, 13, 4, 8, 14, 5, 11, 6, 10, 12, 7, 11, 13, 8, 12, 14, 9, 13 /)
  !
  ! Pointers
  !
  padja_par => pelel  
  ladja_par => lelel

end subroutine initTest


!--El master distribuye lnods y coords a los slaves encargados de partir la malla
subroutine par_dist_mesh()

use def_domain
use def_master
use def_parall
use mod_memory
implicit none
integer(ip)             :: indice0, indice1, i, j, kdime, indiceCoord0, indiceCoord1, domai
integer(ip), target     :: vartx(1)
integer(ip), pointer :: lnods_loc(:,:)   => null()
integer(ip), pointer :: ltype_loc(:)   => null()
integer(ip), pointer :: lexis_loc(:)   => null()
real(rp), pointer :: coord_loc(:,:)   => null()

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

     if( IMASTER ) then
       
       ! Alocatar sólo lo necesario
       
       !calculamos el tamaño de cada trozo
       nelem_part = nelem / kfl_paral_parmes
       nelem_part_r = MOD(nelem, kfl_paral_parmes)
       npoin_part = npoin / kfl_paral_parmes
       npoin_part_r = MOD(npoin, kfl_paral_parmes)
       
        !Calculate the displacements to receive the results.
        call memory_alloca(mem_servi(1:2,servi),'recvcounts','par_senmes',recvcounts, kfl_paral_parmes)
        call memory_alloca(mem_servi(1:2,servi),'displs','par_senmes',displs, kfl_paral_parmes)
        
       indice0 = 1
       indiceCoord0 = 1
       !enviamos a cada slave su trozo
       do i=0,kfl_paral_parmes - 1
         !calculo del slave al que hay que enviar esta parte
         kfl_desti_par = (1 + (kfl_paral_proc_node * i))
         
         !Miramos si es el ultimo slave para darle lo que queda, el resto de elementos      
         if ( i == kfl_paral_parmes - 1 ) then
            nelem_part = nelem_part + nelem_part_r
            npoin_part = npoin_part + npoin_part_r
         end if
         
         !calculamos array donde recibiremos respuesta
         recvcounts(i+1) = int(nelem_part,4);
         
         !enviamos el mnode que hemos calculado en cderda
         npari =  1_ip
         vartx(1) = mnode
         parin => vartx
         strin =  'mnode'
         call par_sendin()
         
         !enviamos el nelem que hemos leido de disco
         npari =  1_ip
         vartx(1) = nelem
         !write(*,*)'Enviamos NELEM:' , nelem
         parin => vartx
         strin =  'nelem'
         call par_sendin()
         
         !enviamos el npoin que hemos leido de disco
         npari =  1_ip
         vartx(1) = npoin
         parin => vartx
         strin =  'npoin'
         call par_sendin()
         
         !enviamos el ndime que hemos leido de disco
         npari =  1_ip
         vartx(1) = ndime
         parin => vartx
         strin =  'ndime'
         call par_sendin()
         
         !enviamos el mnodb que hemos calculado en cderda
         npari =  1_ip
         vartx(1) = mnodb
         parin => vartx
         strin =  'mnodb'
         call par_sendin()
         
         !enviamos el mnodb que hemos calculado en cderda
         npari =  1_ip
         vartx(1) = iesta_dom
         parin => vartx
         strin =  'iesta_dom'
         call par_sendin()
         
         !enviamos el mnodb que hemos calculado en cderda
         npari =  1_ip
         vartx(1) = iesto_dom
         parin => vartx
         strin =  'iesto_dom'
         call par_sendin()
         
         !
         ! Send LEXIS
         !
         !lexis_loc => lexis(:)
         kdime =  nelty
         strin =  'LEXIS_LOC'       
         call par_srinte(1_ip,kdime,lexis)
         
         !
         ! Send LTYPE
         !
         indice1 = indice0 + nelem_part - 1
         
         ltype_loc => ltype(indice0:indice1)
         kdime =  nelem_part
         strin =  'LTYPE_LOC'       
         call par_srinte(1_ip,kdime,ltype_loc)
         
         !
         ! Send LNODS
         !
         lnods_loc => lnods(:,indice0:indice1)
         kdime =  mnode*nelem_part
         strin =  'LNODS_LOC'       
         call par_srinte(1_ip,kdime,lnods_loc)
         
         indice0 = indice0 + nelem_part
         
         !
         ! Send COORD
         !
         indiceCoord1 = indiceCoord0 + npoin_part - 1
         coord_loc => coord(:,indiceCoord0:indiceCoord1)
         kdime =  ndime*npoin_part
         strin =  'coord_LOC'       
         call par_srinte(1_ip,kdime,coord_loc)
         indiceCoord0 = indiceCoord0 + npoin_part
         
         !TEMPORAL LE ENVIAMOS TODAS LAS COORD, PARA PODER PINTAR Y CHEQUEAR         
         !coord_loc => coord(:,1:npoin)
         !kdime =  ndime*npoin
         !strin =  'coord_LOC'       
         !call par_srinte(1_ip,kdime,coord_loc)
       end do
           

        displs(1) = 0
        do domai= 2, kfl_paral_parmes
          displs(domai) = displs(domai - 1) + recvcounts(domai - 1)            
        end do   
        
        !alocatamos memoria para lepar_par
        call par_memory(1_ip)
        call par_memory(8_ip)

     else if( IPARSLAVE ) then

        !----------------------------------------------------------------
        !
        ! Slaves
        !
        !----------------------------------------------------------------
        !-----variables basicas del master que recibo mnode, nelem
        !el mnode me lo envía el master, lo ha calculado sólo él en cderda,
        npari =  1
        parin => vartx
        call par_receiv()
        mnode =  vartx(1)
        
        !el nelem me lo envía el master, lo ha calculado sólo él en cderda,
        npari =  1
        parin => vartx
        call par_receiv()
        nelem =  vartx(1)
        !write(*,*)'Recibimos NELEM:' , nelem
        
        !el npoin me lo envía el master, lo ha calculado sólo él en cderda,
        npari =  1
        parin => vartx
        call par_receiv()
        npoin =  vartx(1)
        
        !el ndime me lo envía el master, lo ha calculado sólo él en cderda,
        npari =  1
        parin => vartx
        call par_receiv()
#ifdef NDIMEPAR

#else
        ndime =  vartx(1)
#endif                 
        
        !el mnodb me lo envía el master, lo ha calculado sólo él en cderda,
        npari =  1
        parin => vartx
        call par_receiv()
        mnodb =  vartx(1)
        
        !el iesta_dom me lo envía el master, lo ha calculado sólo él en cderda,
        npari =  1
        parin => vartx
        call par_receiv()
        iesta_dom =  vartx(1)
        
        !el iesto_dom me lo envía el master, lo ha calculado sólo él en cderda,
        npari =  1
        parin => vartx
        call par_receiv()
        iesto_dom =  vartx(1)
        
        
        !
        ! Receive LEXIS IN LEXIS
        !
        !call memory_alloca(memor_par,'LNODS_PART','memgeo' , lnods_part ,  mnode   , nelem_part )
        !allocate(lexis(nelty))
        call par_srinte(2_ip,nelty, lexis)
        
        !calculo del numero de elementos que tengo que recibir
        nelem_part = nelem / kfl_paral_parmes
        nelem_part_r = MOD(nelem, kfl_paral_parmes)
        !calculo del numero de coordenadas que tengo que recibir
        npoin_part = npoin / kfl_paral_parmes
        npoin_part_r = MOD(npoin, kfl_paral_parmes)
        !calculo del coord_idx, indice global de la coordenada leida
        coord_idx = 1 + (npoin_part * (iproc_part_par) )
        
        !si se trata del ultimo slave le sumamos el resto
        if ((iproc_part_par + 1) == kfl_paral_parmes) then
            nelem_part = nelem_part + nelem_part_r
            npoin_part = npoin_part + npoin_part_r
        end if        
        
        !write(*,*) 'mnode received:' , mnode, ' nelem_part:', nelem_part
        
        !
        ! Receive LTYPE IN LTYPE_PART
        !
        !call memory_alloca(memor_par,'LNODS_PART','memgeo' , lnods_part ,  mnode   , nelem_part )
        allocate(ltype_part(nelem_part))
        call par_srinte(2_ip,nelem_part, ltype_part)
        !write(kfl_paral,*) 'lnods received:', NEW_LINE('A') , lnods_part
        !flush(10)
        
        
        !
        ! Receive LNODS IN LNODS_PART
        !
        !call memory_alloca(memor_par,'LNODS_PART','memgeo' , lnods_part ,  mnode   , nelem_part )
        allocate(lnods_part(mnode, nelem_part))
        call par_srinte(2_ip,mnode*nelem_part, lnods_part)
        !write(kfl_paral,*) 'lnods received:', NEW_LINE('A') , lnods_part
        !flush(10)
        
        !
        ! Receive COORD IN COORD_PART
        !
        !call memory_alloca(memor_par,'LNODS_PART','memgeo' , lnods_part ,  mnode   , nelem_part )
        allocate(coord_part(ndime, npoin_part))
        call par_srinte(2_ip,ndime*npoin_part, coord_part)
        

        ! TEMPORAL RECIBIMOS TODO COORD PARA TEMAS DE TESTEO
        !allocate(coord_part_aux(ndime, npoin))
        !call par_srinte(2_ip,ndime*npoin, coord_part_aux)
        
        !write(*,*) ' NELEM FIN DIST:' , nelem
     end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

!--OUTPUT: LNODS_PART, COORD_PART
end subroutine par_dist_mesh


subroutine par_redist_coord()

use def_domain
use def_master
use def_parall
use mod_memory
implicit none
integer(ip)             :: i, j, coord_idx_max, ielem, inode, ipoin, &
                            ncoord_slave, coord_rv_count, indice0, indice1, &
                            coord_counter, local_ipoin, k, kdime, ipoin_slave, &
                            root_slave, coord_idx_slave, coord_idx_max_slave, &
                            npoin_part_slave
integer(ip), pointer :: coord_needed(:)   => null() 
integer(ip), pointer :: coord_vec(:)   => null() 
integer(ip), pointer :: coordCountVec(:)   => null()
integer(4),  pointer :: my_displs(:) => null()
integer(4)           :: istat
real(rp),    pointer :: coord_tmp(:,:)      ! NPOIN
real(rp), pointer :: coord_loc(:,:)   => null()

!search optimizations
integer(ip)             :: maxCoordVal, minCoordVal
logical, pointer :: coord_local_part_index(:)   => null()
logical :: localIdx
#ifdef EVENT
  call mpitrace_user_function(1)
#endif

   if( IMASTER ) then
       write(*,*) 'START COORDINATES REDISTRIBUTION'

   else if( IPARSLAVE ) then
        
      !1--DETERMINAR COORDENADAS QUE NOS FALTAN --> COORD_NEEDED, coordCount
      !---------------------------------------------------------
      !---------------------------------------------------------
      !---------------------------------------------------------
      coord_idx_max = coord_idx + (npoin_part - 1)
      ncoord_slave = npoin / kfl_paral_parmes
      
      write(*,*) 'coord_idx:', coord_idx , ' ,coord_idx_max:', coord_idx_max
      !--Calculamos una estimación al alza del numero de coordenadas que necesitamos
      !--Estamos contando duplicados 2 veces, esto se podria mejorar con una lista de punteros, por ejemplo
      coordCount = 0
      do ielem=1,nelem_part
         do inode=1,mnode
            ipoin=lnods_part(inode,ielem)
            !Si la coordenada no la ha leido el slave necesitamos pedirla a otro
            if(ipoin < coord_idx .OR. ipoin > coord_idx_max ) then
               coordCount = coordCount + 1
            end if 
         end do
      end do

      !Indice global de coorda, para acelerar la búsqueda
        maxCoordVal = MAXVAL(lnods_part)
      minCoordVal = MINVAL(lnods_part)   
      coordRange = maxCoordVal - minCoordVal + 1_ip
      coordOffset = minCoordVal - 1_ip
      allocate(coord_local_part_index(coordRange))     
      coord_local_part_index = .false.
     
      !--alocatamos y llenamos array con las coordenadas que necesitamos
      write(*,*) 'SLAVE DETERMINAR COORDENADAS 2:' , kfl_paral, ' coordcount:' , coordCount
      !write(*,*) 'SLAVE DETERMINAR COORDENADAS 2:' , kfl_paral, 'nelem_part:' , nelem_part
      !write(*,*) 'SLAVE DETERMINAR COORDENADAS 2:' , kfl_paral, 'mnode:' , mnode
      !write(*,*) 'SLAVE DETERMINAR COORDENADAS 2:' , kfl_paral, 'coord_idx:' , coord_idx
      !write(*,*) 'SLAVE DETERMINAR COORDENADAS 2:' , kfl_paral, 'coord_idx_max:' , coord_idx_max
      allocate(coord_needed(coordCount))
      coordCount = 0 
      do ielem=1,nelem_part
         outer: do inode=1,mnode
            ipoin=lnods_part(inode,ielem)
            !if (kfl_paral == 2) then
            !  write(*,*) 'SLAVE DETERMINAR COORDENADAS 2:' , kfl_paral, 'ielem:' , ielem, 'inode:' , inode, 'ipoin:' , ipoin
            !end if
            if(ipoin < coord_idx .OR. ipoin > coord_idx_max ) then
               !Quitamos los duplicados de la lista, miramos si ya lo tenemos dentro
               localIdx = coord_local_part_index(ipoin - coordOffset)
               if (localIdx .eqv. .false.) then
                 ! New coord needed
                 coordCount = coordCount + 1
                 coord_needed (coordCount) = ipoin
                 coord_local_part_index(ipoin - coordOffset) = .true.
               end if
               
               !do i=1,coordCount
               !   if (ipoin == coord_needed(i)) then
               !      ! Found a match so start looking again
               !      cycle outer
               !   end if
               !end do
               !coordCount = coordCount + 1
               !coord_needed (coordCount) = ipoin 
            end if 
         end do outer
      end do
      deallocate(coord_local_part_index)
      
      !2--ENVIAR COORDENADAS QUE NOS FALTAN ENTRE SLAVES --> coord_vec, coordCountVec
      !---------------------------------------------------------
      !---------------------------------------------------------
      !---------------------------------------------------------
      write(*,*) 'SLAVE ENVIAR COORDENADAS 2:'
#ifdef MPI_OFF
#else
      !--enviamos el numero de coordenadas que necesita cada slave entre sí
      allocate(coordCountVec(kfl_paral_parmes))
      call MPI_ALLGATHER( coordCount, 1_4, PAR_INTEGER, &
                        coordCountVec, 1_4, PAR_INTEGER, PARMETIS_COMM, istat )
                        
   
      !--enviamos el vector con la lista de coordenadas entre si --> COORD_VEC
      allocate(coord_vec(INT(SUM(coordCountVec), 4)))
      allocate( my_displs(size(coordCountVec)))
      my_displs(1) = 0_4
      do i = 2, size(coordCountVec)
         my_displs (i) = my_displs (i - 1) + INT(coordCountVec(i - 1))
      end do

      write(*,*) 'coord_needed:', size(coord_needed)
      !write(*,*) 'coord_vec:', size(coord_vec)
      !write(*,*) 'coordCountVec:', coordCountVec
      !write(*,*) 'my_displs:', size(my_displs)
      !write(*,*) 'coordCount:', coordCount
      !write(*,*) 'coordCountVec values:', coordCountVec
      call MPI_ALLGATHERV( coord_needed(1:coordCount), INT(coordCount, 4), PAR_INTEGER, &
                        coord_vec, INT(coordCountVec, 4), my_displs, PAR_INTEGER, PARMETIS_COMM, istat )
                                              
      !--dealocatamos lo que ya no necesitamos, tenemos la info en coord_vec, en
      deallocate(coord_needed)
      deallocate(my_displs)
      !if (iproc_part_par == 3) then
      !   write(1,*) 'coord_vec:', coord_vec
      !endif
                                                
#endif

      write(*,*) 'SLAVE RECORRER SLAVES E IR ENVIANDO'
      !3--RECORRER SLAVES E IR ENVIANDO Y RECIBIENDO COORDENADAS, SIGUIENDO COORD_VEC
      !---------------------------------------------------------
      !---------------------------------------------------------
      !--------------------------------------------------------- 
      !Alocatamos 2 variables donde iremos guardando las coordenadas que vamos recibiendo,
      !en el orden en que vengan.
      allocate(coord_rv_idx(coordCount))
      allocate(coord_rv(ndime,coordCount))
      coord_rv_count = 0
      indice0 = 1
      indice1 = 0

      do i=0,kfl_paral_parmes - 1
         !calculo del slave que tiene el mando ahora
         root_slave = (1 + (kfl_paral_proc_node * i))
         !calculamos el indice inicial y final siguientes
         indice0 = indice1 + 1
         indice1 = indice1 + coordCountVec(i+1)
          
         if (kfl_paral .NE. root_slave) then
            !Si soy otro slave envio al slave destino las coords que tengo yo y él necesita
            !meto las coordenas a enviar en coord_tmp
            allocate(coord_tmp(ndime, indice1 - indice0 + 1))
            coord_counter = 0
            do j=indice0, indice1
               ipoin = coord_vec(j)
               if (ipoin >= coord_idx .and. ipoin <= coord_idx_max) then
                  coord_counter = coord_counter + 1
                  local_ipoin = ipoin - (coord_idx - 1)
                  coord_tmp(:,coord_counter) = coord_part(:,local_ipoin)
               end if                
            end do
            !si hay coordenadas que enviar las envío al slave que toca
            if (coord_counter > 0) then
               coord_loc => coord_tmp(:,1:coord_counter)
               kdime =  ndime*coord_counter
               strin =  'coord_LOC'
               kfl_desti_par = root_slave
               call par_srinte(1_ip,kdime,coord_loc)               
            end if            
            deallocate(coord_tmp)           
         else
            !Soy slave destino, recibo las coordenas que me faltan del resto,
            !que en esta iteracion les toca enviarmelas
            do j=0,kfl_paral_parmes - 1
               kfl_desti_par = (1 + (kfl_paral_proc_node * j))
               if (kfl_paral .NE. kfl_desti_par) then
                  !recibo del slave kfl_desti_par_aux
                  !calculo que coordenadas me va a enviar el job j o kfl_desti_par_aux
                  coord_counter = 0
                  !calculo que rango de coordenadas tiene este slave, para saber si las tiene
                  npoin_part_slave = npoin / kfl_paral_parmes                  
                  coord_idx_slave = 1 + ((npoin_part_slave) * j)
                  if ((j + 1) == kfl_paral_parmes) then
                     npoin_part_slave = npoin_part_slave + npoin_part_r
                  end if
                  coord_idx_max_slave = coord_idx_slave + (npoin_part_slave - 1)
                  !recorremos coordenadas nuestras y vemos si j las tiene
                  do k=indice0, indice1
                     ipoin = coord_vec(k)
                     ipoin_slave = CEILING(REAL(ipoin) / REAL(ncoord_slave))
                     !si el punto lo tiene el slave j que estamos tratando lo añadimos
                     if (ipoin >= coord_idx_slave .and. ipoin <= coord_idx_max_slave) then
                        coord_counter = coord_counter + 1
                        coord_rv_count = coord_rv_count + 1
                        !if (kfl_paral == 3) then
                        !   write(*,*) 'Soy:', kfl_paral, 'y coord_rv_count de tamaño:', coord_rv_count , ', ipoin:', ipoin , ', indice0', indice0, ', indice1', indice1,'|'
                        !   flush(10)
                        !end if
                        coord_rv_idx(coord_rv_count) = ipoin
                     end if
                  end do
                  if (coord_counter > 0) then
                     !receive from the slave the coordinates
                     !write(*,*) 'coord_tmp:', kfl_desti_par , ', ' , ndime, ', ' , coord_counter        
                     allocate(coord_tmp(ndime, coord_counter))
                     call par_srinte(2_ip,ndime*coord_counter, coord_tmp)
                     !write(*,*) 'Soy:', kfl_paral, 'y he Recibido de:', kfl_desti_par , 'coordenadas:', coord_counter ,'|'
                     !flush(10)
                     !add the coordinates received to coord_rv_idx y coord_rv
                     do k=1, coord_counter                        
                        coord_rv(:,coord_rv_count - coord_counter + k) = coord_tmp(:,k)
                     end do
                     deallocate(coord_tmp)
                  end if
               end if 
            end do
         end if      
      end do
      write(*,*) 'SLAVE FIN REDIST_COORD'
      deallocate(coordCountVec)
      deallocate(coord_vec)      
      !en este punto en coord_rv_idx y coord_rv tenemos las coord que nos faltaban
      !if (kfl_paral == 3) then
      !   write(1,*) 'coord_rv_idx:', coord_rv_idx
      !   flush(10)
      !   write(2,*) 'coord_rv:', coord_rv
      !   flush(10)
      !endif
   end if

#ifdef EVENT
   call mpitrace_user_function(0)
#endif
end subroutine par_redist_coord

subroutine par_local_coord()
use mod_mrgrnk
use mod_unista
use def_domain
use def_master
   implicit none
   integer(ip) :: i, ielem, inode, ipoin, ipoinIdx, &
                   localIdx, coord_idx_max, coordIdx, local_ipoin, &
                   maxCoordVal, minCoordVal
   integer(ip), pointer :: lnods_part_count(:)   => null()
   integer(ip), pointer :: coord_local_part_index(:)   => null()
                   
   coordOffset = 0
   coordRange = 0
   coord_idx_max = coord_idx + (npoin_part - 1)
   !1.--Calculamos el numero de coordenadas locales que tenemos en lnods, quitando los duplicados
   !para cada coordenada global de la malla si la tenemos en lnods_par sumamos +1
   npoin_local = 0
   !outer: do i = 1, npoin
   !   do ielem=1,nelem_part
   !       do inode=1,mnode
   !         ipoin=lnods_part(inode,ielem)
   !         if (ipoin == i) then
   !            npoin_local = npoin_local + 1
   !            cycle outer
   !         end if            
   !      end do
   !   end do      
   !end do outer
   
   !1.--ALTERNATIVA!!! Calculamos el numero de coordenadas locales que tenemos en lnods, quitando los duplicados
   !para cada coordenada global de la malla si la tenemos en lnods_par sumamos +1
   allocate(lnods_part_count(nelem_part*mnode))
   i = 1
   do ielem=1,nelem_part
      do inode=1,mnode
            lnods_part_count(i)=lnods_part(inode,ielem)
            i = i + 1
     end do
   end do      
   call unista(lnods_part_count,npoin_local)
   deallocate(lnods_part_count)
   
   !ordenamos el vector
   allocate(coord_local_part(ndime, npoin_local))
   allocate(coord_local_part_inv(npoin_local))
   allocate(lnods_local_part(mnode, nelem_part))
   coord_local_part_inv = -1
   
   !Ordenamos coord_rv_idx y coord_rv, para así luego buscar con mayor velocidad
   allocate(coord_rv_rank(coordCount))
   call mrgrnk(coord_rv_idx, coord_rv_rank); 
   
   !Indice global de coorda, para acelerar la búsqueda
   maxCoordVal = MAXVAL(lnods_part)
   minCoordVal = MINVAL(lnods_part)   
   coordRange = maxCoordVal - minCoordVal + 1_ip
   coordOffset = minCoordVal - 1_ip
   allocate(coord_local_part_index(coordRange))
   coord_local_part_index = 0
      
   ipoinIdx = 1
   do ielem=1,nelem_part
      do inode=1,mnode
         ipoin=lnods_part(inode,ielem)
         
         !buscamos si ya lo tenemos indexado             
             localIdx = coord_local_part_index(ipoin - coordOffset)
             
         !no está indexado, lo añadimos en coord_local_part y coord_local_part_inv
         if (localIdx == 0) then
            localIdx = ipoinIdx
            coord_local_part_index(ipoin - coordOffset) = localIdx
            !añadimos el coord_local_part_inv
            coord_local_part_inv(localIdx) = ipoin            
            ipoinIdx = ipoinIdx + 1
            
            !añadimos en coord_local_part el valor de la coordenada
            if (ipoin < coord_idx .OR. ipoin > coord_idx_max) then
               !no la hemos leido nosotros, está en las recibidas de otro slave
               !buscamos su valor
               coordIdx = 0
               call array_search_rank(coord_rv_idx, coordCount, coord_rv_rank, ipoin, coordIdx);
               !metemos los valores de la coordenada en coord_local_part
               coord_local_part(:,localIdx) = coord_rv(:,coordIdx)
            else
               !la hemos leido nosotros, la cogemos directamente de coord_part
               local_ipoin = ipoin - (coord_idx - 1)
               coord_local_part(:,localIdx) = coord_part(:,local_ipoin)
            end if
         end if         
         !rellenamos el lnods_local_part con la numeracion local
         lnods_local_part(inode, ielem) = localIdx
      end do
   end do
      
   !Podemos dealocatar varias varibles que ya no usaremos
   deallocate(coord_rv)
   deallocate(coord_rv_idx)
   deallocate(coord_part)
   deallocate(coord_rv_rank)
   deallocate(coord_local_part_index)
   coordOffset = 0
   coordRange = 0
   
   !--OUTPUT: lnods_local_part, coord_local_part, coord_local_part_inv
   !if (kfl_paral == 3) then
         !write(1,*) 'coord_local_part:', coord_local_part
         !flush()
         !write(2,*) 'coord_local_part_inv:', coord_local_part_inv
         !flush()
   !endif
end subroutine par_local_coord

subroutine par_local_coord_alt()
use mod_mrgrnk
use mod_unista
use def_domain
use def_master
   implicit none
   integer(ip) :: i, ielem, inode, ipoin, ipoinIdx, &
                   localIdx, coord_idx_max, coordIdx, local_ipoin, &
                   maxCoordVal, minCoordVal
   integer(ip), pointer :: lnods_part_count(:)   => null()
                   
   coord_idx_max = coord_idx + (npoin_part - 1)
   
   npoin_local = 0
   
   
   !1.--ALTERNATIVA!!! Calculamos el numero de coordenadas locales que tenemos en lnods, quitando los duplicados
   !para cada coordenada global de la malla si la tenemos en lnods_par sumamos +1
   !allocate(lnods_part_count(nelem_part*mnode))
   !i = 1
   !do ielem=1,nelem_part
   !   do inode=1,mnode
   !         lnods_part_count(i)=lnods_part(inode,ielem)
   !         i = i + 1
   !  end do
   !end do      
   !call unista(lnods_part_count,npoin_local)
   !calculamos el maximo y minimo numero, para optimizar tamaño vector
   maxCoordVal = MAXVAL(lnods_part)
   minCoordVal = MINVAL(lnods_part)
   !deallocate(lnods_part_count)   
   coordRange = maxCoordVal - minCoordVal + 1
   coordOffset = minCoordVal - 1
   allocate(coord_local_part(ndime, coordRange))
   allocate(coord_local_part_lo(coordRange))
   coord_local_part_lo = .false.
   allocate(lnods_local_part(mnode, nelem_part))
   coord_local_part = -1
   
   write(*,*) 'maxCoordVal:' , maxCoordVal
   write(*,*) 'minCoordVal:' , minCoordVal
   write(*,*) 'coordRange:' , coordRange
   write(*,*) 'coordOffset:' , coordOffset
   write(*,*) 'maxCoordVal:' , maxCoordVal
   
   !ordenamos el vector
   write(*,*) 'SLAVE Calculamos el numero de coordenadas locales 1:' , kfl_paral, ' npoin_local:' , npoin_local
   
   !Ordenamos coord_rv_idx y coord_rv, para así luego buscar con mayor velocidad
   allocate(coord_rv_rank(coordCount))
   call mrgrnk(coord_rv_idx, coord_rv_rank); 
      
   ipoinIdx = 1
   do ielem=1,nelem_part
      do inode=1,mnode
         ipoin=lnods_part(inode,ielem)            
         !añadimos en coord_local_part el valor de la coordenada
         if (ipoin < coord_idx .OR. ipoin > coord_idx_max) then
            !no la hemos leido nosotros, está en las recibidas de otro slave
            !buscamos su valor
            coordIdx = 0
            call array_search_rank(coord_rv_idx, coordCount, coord_rv_rank, ipoin, coordIdx);
            !metemos los valores de la coordenada en coord_local_part
            coord_local_part(:,ipoin - coordOffset) = coord_rv(:,coordIdx)
            coord_local_part_lo(ipoin - coordOffset) = .true.
         else
               !la hemos leido nosotros, la cogemos directamente de coord_part
               local_ipoin = ipoin - (coord_idx - 1)
               coord_local_part(:,ipoin - coordOffset) = coord_part(:,local_ipoin)
               coord_local_part_lo(ipoin - coordOffset) = .true.
         end if         
         !rellenamos el lnods_local_part con la misma numeracion
         lnods_local_part(inode, ielem) = ipoin
      end do
      !if (kfl_paral == 30) then
      !   write(*,*) 'NELEM_PART TRATADO:' , ielem, 'de:', nelem_part
      !end if
   end do 
   
   write(*,*) 'SLAVE Calculamos el numero de coordenadas locales 2:' , kfl_paral, ' ipoinIdx:' , ipoinIdx
      
   !Podemos dealocatar varias varibles que ya no usaremos
   deallocate(coord_rv)
   deallocate(coord_rv_idx)
   deallocate(coord_part)
   deallocate(coord_rv_rank)
   
   
   !--OUTPUT: lnods_local_part, coord_local_part, coord_local_part_inv
   !if (kfl_paral == 3) then
         !write(1,*) 'coord_local_part:', coord_local_part
         !flush()
         !write(2,*) 'coord_local_part_inv:', coord_local_part_inv
         !flush()
   !endif
end subroutine par_local_coord_alt

subroutine par_calc_bound_elem
   use def_domain
   use def_master
   use mod_memory
   implicit none
   integer(ip)           :: ielty, ielem, iface, inodb, inode, ipoin, ilist, &
                             ielpo, jelem, jelty, jface, pepoi,pnodb,pnodi,pnodj
   integer(ip),  pointer :: lista(:)     => null()
   logical(lg)           :: equal_faces

   ! nface se calcula en elmtyp, el slave también lo ha calculado
   ! mnodb lo envia el master
   ! nelem lo envia el master
   ! nelty se calcula en el def_domain, el slave también lo ha calculado
   
   !creamos el pelpo_part y lelpo_part necesarios para el algoritmo
   call connpo_part()
   
   !
   ! faces_part graph: Allocate memory for faces_part
   !
   call memory_alloca(memor_par,'faces_part','par_elmgra',faces_part,mnodb+1,mface,nelem_part)
   !
   ! Construct and sort faces_part
   !
   !*OMP  PARALLEL DO SCHEDULE (GUIDED)           & 
   !*OMP  DEFAULT (NONE)                          &
   !*OMP  PRIVATE (ielem,ielty,iface,inodb,inode) &
   !*OMP  SHARED  (ltype,faces_part,lnods,nelem) 
   !
   do ielem = 1,nelem_part                                         
      ielty = abs(ltype_part(ielem))
      do iface = 1,element_type(ielty) % number_faces
         pnodb = element_type(ielty) % node_faces(iface)
         do inodb = 1,pnodb
            inode = element_type(ielty) % list_faces(inodb,iface)
            faces_part(inodb,iface,ielem) = lnods_local_part(inode,ielem)
         end do
         call sortin(pnodb,faces_part(1,iface,ielem))
      end do
   end do
   
     !
     ! Compute faces_part
     !
     nBoundElem = 0
     maindo: do ielem = 1,nelem_part                                          ! Compare the faces_part and 
        ielty = abs(ltype_part(ielem))                                ! eliminate the repited faces_part
        do iface=1,element_type(ielty) % number_faces
           ipoin = faces_part(1,iface,ielem)
           if( ipoin /= 0 ) then
              pnodi = element_type(ielty) % node_faces(iface)
              ilist = 1
              pepoi = pelpo_part(ipoin+1)-pelpo_part(ipoin)
              ielpo = pelpo_part(ipoin)-1
              do while( ilist <= pepoi )
                 ielpo = ielpo+1
                 jelem = lelpo_part(ielpo)
                 if( jelem /= ielem ) then
                    jelty = abs(ltype_part(jelem))                              ! eliminate the repited faces_part
                    jface = 0
                    do while( jface /= element_type(jelty) % number_faces )
                       jface = jface + 1
                       if( faces_part(1,jface,jelem) /= 0 ) then
                          pnodj       = element_type(jelty) % node_faces(jface)                          
                          equal_faces = .true.
                          inodb       = 0
                          do while( equal_faces .and. inodb /= pnodj )
                             inodb = inodb + 1
                             if( faces_part(inodb,iface,ielem) /= faces_part(inodb,jface,jelem) ) &
                                  equal_faces = .false.
                          end do
                          if( equal_faces ) then
                             faces_part(1,iface,ielem) = 0                         ! IFACE and JFACE
                             faces_part(1,jface,jelem) = 0                         ! are eliminated
                             faces_part(pnodj+1,iface,ielem) = jelem               ! IFACE and JFACE
                             faces_part(pnodi+1,jface,jelem) = ielem               ! are eliminated
                             nedge = nedge + 2
                             jface = element_type(jelty) % number_faces           ! Exit JFACE do
                             ilist = pepoi                                      ! Exit JELEM do
                          end if
                       end if
                    end do
                 end if
                 ilist = ilist + 1
              end do
           end if
           if( faces_part(1,iface,ielem) /= 0 ) then
              !Si el primer nodo de la cara no es 0, entonces la cara no es compartida, es elemento de contorno
              nBoundElem = nBoundElem + 1
              cycle maindo
           end if
        end do
     end do maindo
     
     call memory_deallo(memor_par,'PELPO_PART','par_calc_bound_elem',pelpo_part)
     call memory_deallo(memor_par,'LELPO_PART','par_calc_bound_elem',lelpo_part)
     
     
     call memory_alloca(memor_par,'boundElem_part','par_calc_bound_elem',boundElem_part,nBoundElem)
     !
     !*OMP  PARALLEL DO SCHEDULE (GUIDED)      & 
     !*OMP  DEFAULT (NONE)                     &
     !*OMP  PRIVATE (ielem,ielty,iface)  &
     !*OMP  SHARED  (ltype,faces_part) 
     !
     nBoundElem = 0
     elemdo: do ielem = 1,nelem_part
        ielty = abs(ltype_part(ielem))
        do iface = 1,element_type(ielty) % number_faces
           if( faces_part(1,iface,ielem) /= 0 ) then
              !Si el primer nodo de la cara no es 0, entonces la cara no es compartida, es elemento de contorno
              !write (*,*) 'ELEMENTOOOOO CON CARA NO COMPARTIDA:' , ielem
              !flush()
              nBoundElem = nBoundElem + 1
              boundElem_part(nBoundElem) = ielem
              cycle elemdo
           end if
        end do
     end do elemdo

end subroutine par_calc_bound_elem

subroutine connpo_part()
  !-----------------------------------------------------------------------
  !****f* Domain/connpo
  ! NAME
  !    connpo
  ! DESCRIPTION
  !    This routine compute the node/element connectivity arrays.
  ! OUTPUT
  !    NEPOI(NPOIN) ............ # of elements connected to nodes
  !    PELPO(NPOIN+1) .......... Pointer to list of element LELPO
  !    LELPO(PELPO(NPOIN+1)) ... List of elements connected to nodes
  ! USED BY
  !    domgra
  !    par_partit
  !***
  !-----------------------------------------------------------------------
   use def_parame
   use def_master
   use def_domain
   use mod_memchk
   implicit none
   integer(ip) :: inode,ipoin,ielem,nlelp,pelty
   integer(4)  :: istat
   integer(ip), pointer     :: lnnod_local_part(:)      ! Element number of nodes
   integer(ip), pointer     :: nepoi_part(:)      ! Element number of nodes
   
   !write(*,*) 'ANTES CONNPO1:' , nnode
   !flush(6)
   
   !
   ! LNNOD_LOCAL_PART
   !
   allocate(lnnod_local_part(nelem_part),stat = istat)
   call memchk(zero,istat,memor_par,'lnood_local_part','connpo_part',lnnod_local_part)
   do ielem = 1,nelem_part
      pelty = ltype_part(ielem)
      lnnod_local_part(ielem) = nnode(pelty)
   end do
   
   !
   ! Allocate memory for NEPOI and compute it
   !
   !npoin = UBOUND(COORD_LOCAL_PART, DIM=2, KIND=ip)
   allocate(nepoi_part(npoin_local),stat = istat)
   call memchk(zero,istat,memor_par,'NEPOI_part','connpo_part',nepoi_part)

   do ielem = 1,nelem_part
      do inode = 1,lnnod_local_part(ielem)
         ipoin = lnods_local_part(inode,ielem)
         nepoi_part(ipoin) = nepoi_part(ipoin) + 1
      end do
   end do
  
  !
  ! Allocate memory for PELPO and compute it
  !
  allocate(pelpo_part(npoin_local+1),stat = istat)
  call memchk(zero,istat,memor_par,'PELPO','connpo',pelpo_part)
  pelpo_part(1) = 1
  do ipoin = 1,npoin_local
     pelpo_part(ipoin+1) = pelpo_part(ipoin) + nepoi_part(ipoin)
  end do
  
  !
  ! Allocate memory for LELPO and construct the list
  !
  nlelp = pelpo_part(npoin_local+1)
  allocate(lelpo_part(nlelp),stat = istat)
  call memchk(zero,istat,memor_par,'LELPO_part','connpo_part',lelpo_part)
  !mpopo = 0
  do ielem = 1,nelem_part
     !mpopo  =  mpopo + lnnod(ielem) * lnnod(ielem)
     do inode = 1,lnnod_local_part(ielem)
        ipoin = lnods_local_part(inode,ielem)
        lelpo_part(pelpo_part(ipoin)) = ielem
        pelpo_part(ipoin) = pelpo_part(ipoin)+1
     end do
  end do
  
  !
  ! Recompute PELPO and maximum number of element neighbors MEPOI
  !
  pelpo_part(1) = 1
  mepoi = -1
  do ipoin = 1,npoin_local
     pelpo_part(ipoin+1) = pelpo_part(ipoin)+nepoi_part(ipoin)
     mepoi = max(mepoi,nepoi_part(ipoin))
  end do
  
  !
  ! Deallocate memory for temporary node/element connectivity
  !
  call memchk(two,istat,memor_par,'NEPOI_part','connpo_part',nepoi_part)
  deallocate(nepoi_part,stat = istat)
  if(istat /= 0 ) call memerr(two,'NEPOI_part','connpo_part',0_ip)

end subroutine connpo_part


subroutine par_part_mesh
  use def_kintyp
  use def_parame
  use def_parall
  use def_domain
  use def_master
  use mod_memory

  integer(ip) :: nelem_part_aux, nelem_part_r_aux
  integer(ip), pointer     :: lepar_par_loc(:)        ! Domain of every element at each processor
  integer(ip), SAVE             :: dummi
  integer(ip), target     :: dummiv(1)
  integer(ip)             :: domai
  integer(4)              :: npari4,istat

  !-----------------------------------
  !--1.calculamos el vtxdist
  !-----------------------------------
  nelem_part_aux = nelem / kfl_paral_parmes
  nelem_part_r_aux = MOD(nelem, kfl_paral_parmes)
  call memory_alloca(memor_par,'vtxdist','par_senmes',vtxdist, kfl_paral_parmes +1_ip)
  pelel_idx = 1
  do domai= 1, kfl_paral_parmes +1_ip
     vtxdist(domai) = pelel_idx   
     pelel_idx = pelel_idx + nelem_part_aux
     if (domai == kfl_paral_parmes) then
        pelel_idx = pelel_idx + nelem_part_r_aux
     end if
  end do

  !padja_par => pelel  
  !ladja_par => lelel
  !nedge_par = padja_par(nelem_part + 1_ip) - 1
  call memory_alloca(memor_par,'WVERT_PAR','par_prepro',wvert_loc,nelem_part)
  call memory_alloca(memor_par,'WEDGE_LOC','par_prepro',wedge_loc,1_ip)
  call memory_alloca(memor_par,'LEPAR_PAR_LOC','par_partit_paral_slave' , lepar_par_loc ,  nelem_part )
  kfl_weigh_par = 0
  nelem = -1

  !-----------------------------------
  !--2.Call parmetis
  !-----------------------------------
  !write(*,*) 'nelem_part:' , nelem_part
  !flush(6)
  !write(*,*) 'nedge_par:' , nedge_par
  !flush(6)
  !write(*,*) 'npart_par:' , nelem_part
  !flush(6)
  !write(*,*) 'nelem:' , nelem
  !flush(6)
  !write(*,*) 'vtxdist:' , size (vtxdist), lbound(vtxdist), ubound(vtxdist)
  !flush(6)
  !write(*,*) 'padja_par:' , size (padja_par), lbound(padja_par), ubound(padja_par)
  !flush(6)
  !write(*,*) 'ladja_par:' , size (ladja_par), lbound(ladja_par), ubound(ladja_par)
  !flush(6)
  !write(*,*) 'wvert_loc:' , size (wvert_loc), lbound(wvert_loc), ubound(wvert_loc)
  !flush(6)
  !write(*,*) 'wedge_loc:' , size (wedge_loc), lbound(wedge_loc), ubound(wedge_loc)
  !flush(6)
  !write(*,*) 'kfl_weight_par:' , kfl_weigh_par
  !flush(6)
  !write(*,*) 'lepar_par_loc:' , size (lepar_par_loc), lbound(lepar_par_loc), ubound(lepar_par_loc)
  !flush(6)
  !write(*,*) 'padja_par_VALUES:' , padja_par
  !flush(6
  !if (kfl_paral == 3) then
  !write(*,*) 'ladja_par_VALUES:' , ladja_par
  !flush(6)
  !  end if
  !   do while (1==1)
  !            call sleep(10)
  !      end do
  call par_parmetis(                                             &
       1_ip , nelem_part, nedge_par , vtxdist, padja_par, ladja_par, wvert_loc, &
       wedge_loc, kfl_weigh_par, npart_par,                   &
       lepar_par_loc ,dummi , [dummi] , [dummi] , [dummi] ,             & 
       [dummi], dummi , nelem , 1_ip , mem_servi(1,servi)       )

  !do i=1,nelem_part
  !    write (*,*) 'Element:' , i, 'Is in domain:' , lepar_par_loc(i)
  !enddo

  !-----------------------------------
  !--3.Send the results to the master
  !-----------------------------------
  npari =  nelem_part
  parin => lepar_par_loc
  strin =  'lepar_par_loc'
  npari4=int(npari,4)
  parig => dummiv
  call memory_alloca(memor_par,'recvcounts','par_senmes',recvcounts, kfl_paral_parmes)
  call memory_alloca(memor_par,'displs','par_senmes',displs, kfl_paral_parmes)
#ifdef MPI_OFF
#else
  CALL MPI_GatherV( parin(1:npari) , npari4 , PAR_INTEGER, parig(1:) , recvcounts, displs , &
       PAR_INTEGER , 0_4 , PARMETIS_INTERCOMM , istat)
#endif

  npari=0
  write (*,*) 'slave terminated:' , kfl_paral
  call memory_deallo(memor_par,'LEPAR_PAR_LOC','par_partit_paral_slave' , lepar_par_loc )
  call memory_deallo(memor_par,'recvcounts','par_partit_paral_slave' , recvcounts )
  call memory_deallo(memor_par,'displs','par_partit_paral_slave' , displs )
  call memory_deallo(memor_par,'elem_loc_recv','par_partit_paral_slave' , elem_loc_recv )
  call memory_deallo(memor_par,'ltype_recv','par_partit_paral_slave' , ltype_recv )
  call memory_deallo(memor_par,'lnods_local','par_partit_paral_slave' , lnods_local )
  call memory_deallo(memor_par,'coord_local_inv','par_partit_paral_slave' , coord_local_inv )
  call memory_deallo(memor_par,'vtxdist','par_partit_paral_slave' , vtxdist )
  call memory_deallo(memor_par,'wvert_loc','par_partit_paral_slave' , wvert_loc )
  call memory_deallo(memor_par,'wedge_loc','par_partit_paral_slave' , wedge_loc )
  call memory_deallo(memor_par,'lepar_par_loc','par_partit_paral_slave' , lepar_par_loc )
  call memory_deallo(memor_par,'padja_par','par_partit_paral_slave' , padja_par )
  !call memory_deallo(memor_par,'ladja_par','par_partit_paral_slave' , ladja_par ) 

end subroutine par_part_mesh

subroutine par_partition_mesh_slave
use def_domain
use def_master
   !Master distributes mesh to slaves
   !--OUTPUT: LNODS_PART, COORD_PART, NELEM_PART, NPOIN_PART, NPOIN, NELEM
   write(*,*) 'SLAVE par_dist_mesh:' , kfl_paral
   call par_dist_mesh()

   !Slaves redistribuite the coordinates
   !--OUTPUT: coord_rv_idx, coord_rv, coordCount
   write(*,*) 'SLAVE par_redist_coord:' , kfl_paral
   call par_redist_coord()
   

   !Slaves calculate a local numeration for coordinates
   !--OUTPUT: LNODS_LOCAL_PART, COORD_LOCAL_PART, COORD_LOCAL_PART_INV, NPOIN_LOCAL
   write(*,*) 'SLAVE par_local_coord:' , kfl_paral
   !if (kfl_run_type == 1) then
      call par_local_coord()
   !else
   !   call par_local_coord_alt()
   !end if

   !call print_vtk_slave()
 
   !Salves calculate the list of elements that are in the boundary
   !--OUTPUT: boundElem_part, nBoundElem
   write(*,*) 'SLAVE par_calc_bound_elem:' , kfl_paral
   call par_calc_bound_elem()

   !Calculates bin structure of the actual domains
   !--OUTPUT: PAR_BIN_SIZE, PAR_BIN_PART
   write(*,*) 'SLAVE par_bin_structure_part:' , kfl_paral
   call par_bin_structure_part()

   !Redistributes elements to create a new lnods
   !--OUTPUT: LNODS_LOCAL, COORD_LOCAL_INV, elem_loc_recv, nelemRecv, nelem_part, npoin_part, ltype_recv
   write(*,*) 'SLAVE par_element_distribution_part:' , kfl_paral
   call par_element_distribution_part()   
   
   !AUXILIAR CHEQUEO DE LNODS_LOCAL
   !call check1()
   
   !calculates PELEL/ELEL local
   !--OUTPUT: LNODS_LOCAL_PART, nelem_part, elem_loc_recv, nelemRecv
   write(*,*) 'SLAVE par_elmgra_part:' , kfl_paral
   call par_elmgra_part()
   
   !Mesh partition
   write(*,*) 'SLAVE par_part_mesh:' , kfl_paral
   call par_part_mesh()
   
   !Deallocates
   deallocate(boundElem_part)
   deallocate(lelel)
        
   mpopo = 0    
end subroutine par_partition_mesh_slave

subroutine check1
!--OUTPUT: LNODS_LOCAL, COORD_LOCAL_INV, elem_loc_recv, nelemRecv, nelem_part, npoin_part
 use def_parall
   use def_master
   use def_domain
   implicit none
   CHARACTER(len=20) :: file_name
   integer, parameter :: out_unit=20
   integer(ip) :: numPoints, ielem, inode, nelem_part_aux,ipoin

   !Imprimimos toda la malla del slave
   !file_name = 'node' // ACHAR(kfl_paral)
   nelem_part_aux = nelem / kfl_paral_parmes
   write(*,*) 'CHECK1111111111, NELEM:' , nelem
   if (1==1) then
      !CHECK DE LNODS_LOCAL LA PARTE MIA HASTA nelem_part
      write(file_name,"(A12,I1,A4)") 'elemRedPARTA' , kfl_paral, '.vtk'
      file_name = trim(file_name)
      open (unit=out_unit,file=file_name,action="write",status="replace")
      write(*,*) 'CHEKEANDOOOOOOOOOOOOOOO'
      write(out_unit,*) '# vtk DataFile Version 3.0'
      write(out_unit,*) 'Mesh nodes!'
      write(out_unit,*) 'ASCII'
      write(out_unit,*) 'DATASET POLYDATA'
      numPoints = mnode * nelem_part
      write(out_unit,*) 'POINTS ' , numPoints , 'float'
      do ielem = 1,nelem_part      
         do inode = 1,mnode
            ipoin = lnods_local(inode,ielem)
            ipoin = coord_local_inv(ipoin)
            write (out_unit,100)  coord_part_aux(:,ipoin)
            100 FORMAT ( '' ,3ES14.4)
         end do
      end do
      !Imprimimos el codigo de elemento
      !POINT_DATA 3
      !FIELD dataName 1
      !arrayName0 1 3 int
      !1
      !2
      !3
      write(out_unit,*)'POINT_DATA ' , numPoints
      write(out_unit,*)'FIELD dataName 1'
      write(out_unit,*)'arrayName0 1 ' , numPoints, ' int'
      do ielem = 1,nelem_part      
         do inode = 1,mnode
            write (out_unit,101)  ielem + (nelem_part_aux * iproc_part_par)
            101 FORMAT ( '' ,I4)
         end do
      end do
      close(out_unit)
      
      !CHECK DE LNODS_LOCAL LA PARTE RECIBIDA DE nelem_part HASTA nelemRecv
      write(file_name,"(A12,I1,A4)") 'elemRedPARTB' , kfl_paral, '.vtk'
      file_name = trim(file_name)
      open (unit=out_unit,file=file_name,action="write",status="replace")
      
      write(out_unit,*) '# vtk DataFile Version 3.0'
      write(out_unit,*) 'Mesh nodes!'
      write(out_unit,*) 'ASCII'
      write(out_unit,*) 'DATASET POLYDATA'
      numPoints = mnode * nelemRecv
      write(out_unit,*) 'POINTS ' , numPoints , 'float'
      do ielem = nelem_part + 1, nelem_part + nelemRecv
         do inode = 1,mnode
            ipoin = lnods_local(inode,ielem)
            ipoin = coord_local_inv(ipoin)
            write (out_unit,100)  coord_part_aux(:,ipoin)
            !100 FORMAT ( '' ,3ES14.4)
         end do
      end do
      !Imprimimos el codigo de elemento
      !POINT_DATA 3
      !FIELD dataName 1
      !arrayName0 1 3 int
      !1
      !2
      !3
      write(out_unit,*)'POINT_DATA ' , numPoints
      write(out_unit,*)'FIELD dataName 1'
      write(out_unit,*)'arrayName0 1 ' , numPoints, ' int'
      do ielem = nelem_part + 1, nelem_part + nelemRecv
         do inode = 1,mnode
            write (out_unit,101)  elem_loc_recv(ielem - nelem_part)
         end do
      end do
      close(out_unit)
   end if

end subroutine check1

subroutine print_vtk_slave()
   use def_parall
   use def_master
   use def_domain
   implicit none
   CHARACTER(len=20) :: file_name
   integer, parameter :: out_unit=20
   integer(ip) :: numPoints, ielem, inode
   integer(ip) :: ipoin

   !Imprimimos toda la malla del slave
   !file_name = 'node' // ACHAR(kfl_paral)
   if (1==1) then
      write(file_name,"(A4,I1,A4)") 'node' , kfl_paral, '.vtk'
      file_name = trim(file_name)
      open (unit=out_unit,file=file_name,action="write",status="replace")
      
      write(out_unit,*)'# vtk DataFile Version 3.0'
      write(out_unit,*)'Mesh nodes!'
      write(out_unit,*)'ASCII'
      write(out_unit,*)'DATASET POLYDATA'
      numPoints = mnode * nelem_part
      write(out_unit,*)'POINTS ' , numPoints , 'float'
      do ielem = 1,nelem_part      
         do inode = 1,mnode
            ipoin = lnods_local_part(inode,ielem)
            write (out_unit,100)  coord_local_part(:,ipoin)
            100 FORMAT ( '' ,3ES14.4)
         end do
      end do
      close(out_unit)
      
      !Imprimimos los nodos de los elementos boundary
      !file_name = 'node' // ACHAR(kfl_paral)
      write(file_name,"(A4,I1,A4)") 'boundElem' , kfl_paral, '.vtk'
      file_name = trim(file_name)
      open (unit=out_unit,file=file_name,action="write",status="replace")
      
      write(out_unit,*) '# vtk DataFile Version 3.0'
      write(out_unit,*) 'Bound Elem!'
      write(out_unit,*) 'ASCII'
      write(out_unit,*) 'DATASET POLYDATA'
      numPoints = mnode * nBoundElem
      write(out_unit,*) 'POINTS ' , numPoints , 'float'
      do ielem = 1,nBoundElem      
         do inode = 1,mnode
            ipoin = lnods_local_part(inode, boundElem_part(ielem))
            write (out_unit,100)  coord_local_part(:,ipoin)
         end do
      end do
      close(out_unit)
   end if
   
   
   write(*,*) 'Elementos de contorno:' , nBoundElem, ' para el slave:' , kfl_paral

end subroutine print_vtk_slave

end module mod_par_partit_paral
