subroutine par_initia_part()
  !------------------------------------------------------------------------
  !****f* Parall/par_initia
  ! NAME
  !    par_initia
  ! DESCRIPTION
  !    This routine initialize MPI and open files
  ! OUTPUT
  !    nproc_par ... number of processes
  !    iproc_par ... my PID
  !    kfl_paral ... =iproc_par: tell Master if the process was initiated
  !                  by MPI
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_parall
  use mod_parall,         only : PAR_COMM_MY_CODE4
  use mod_parall,         only : PAR_COMM_WORLD
  use mod_parall,         only : PAR_MY_CODE_RANK
  use mod_parall,         only : PAR_CODE_SIZE
  use mod_communications, only : PAR_COMM_RANK_AND_SIZE
#ifdef PARMETIS
  use mod_par_partit_paral
#endif
  implicit none
#ifdef MPI_OFF
#else
  !include  'mpif.h'
#endif
  integer(4)  :: istat,iproc_par4,nproc_par4,color
  integer(4)  :: iproc_par4_par,nproc_par4_par
  integer(ip) :: i

#ifdef MPI_OFF
#else

  !init communicator for parmetis
#ifdef PARMETIS
  if(nproc_par>1) then
     !Calculate if I am a partition slave
     IPARSLAVE = .FALSE.
     do i=0,kfl_paral_parmes - 1 
      if (iproc_par == (1 + (kfl_paral_proc_node * i))) then
       !iproc_part_par = i + 1
       IPARSLAVE = .TRUE.  
       exit
      end if
     end do
     
     if( IPARSLAVE ) then
        write(*,*) 'Slave de particion:' , iproc_par
        color = 0_4
     else
        color = 1_4
     end if
     !call PAR_COMM_SPLIT(color,PAR_COMM_MY_CODE4,PARMETIS_COMML,my_new_rank,where)
     CALL MPI_COMM_SPLIT (PAR_COMM_MY_CODE4, color, 0_4, PARMETIS_COMM, istat)
     call PAR_COMM_RANK_AND_SIZE(PARMETIS_COMM,iproc_par4_par,nproc_par4_par)
     iproc_part_par = int(iproc_par4_par,ip)
     
     if (color == 1) then
        CALL MPI_INTERCOMM_CREATE (PARMETIS_COMM, 0_4, PAR_COMM_MY_CODE4, 1_4, 111_4, PARMETIS_INTERCOMM, istat)
     else
        CALL MPI_INTERCOMM_CREATE (PARMETIS_COMM, 0_4, PAR_COMM_MY_CODE4, 0_4, 111_4, PARMETIS_INTERCOMM, istat)
     endif
  endif
#endif

#endif
 
end subroutine par_initia_part
