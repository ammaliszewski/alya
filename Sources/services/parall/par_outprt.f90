subroutine par_outprt()
  !-------------------------------------------------------------------------------
  !****f* Parall/par_outprt
  ! NAME
  !    par_outprt
  ! DESCRIPTION
  !    Output Master's partition: Put here all the things needed by the master 
  !    when doing restart
  ! INPUT
  ! OUTPUT
  !
  ! USED BY
  !    par_partit
  !***
  !-------------------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_parall 
  use def_master
  use mod_memchk
  implicit none
  integer(ip) :: iunit,ii,ji,npart_tmp
  integer(4)  :: istat

  iunit = 1000 + 0
  kfl_desti_par = 0
  npart_tmp = npart_par

  if( PART_AND_WRITE() .or. READ_AND_RUN() ) then

     strin = 'readim_reastr_reageo_cderda'
     strre = 'readim_reastr_reageo_cderda'
     strch = 'readim_reastr_reageo_cderda'
     do parii = 1,2 
        npari = 0
        nparr = 0
        nparc = 0

        call iexcha(npart_tmp)

        !-------------------------------------------------------------------
        !
        !  Read in readim
        !
        !-------------------------------------------------------------------
 
        call iexcha(kfl_autbo)
        call iexcha(npoin)
#ifdef NDIMEPAR

#else
        call iexcha(ndime)
#endif         
        call iexcha(nelem)
        call iexcha(nboun)
        call iexcha(nskew)
        call iexcha(nperi)
        call iexcha(nzone)
        call iexcha(nsubd)

        !-------------------------------------------------------------------
        !
        ! Read in reastr
        !
        !-------------------------------------------------------------------

        do ii=1,nelty
           call iexcha(lexis(ii))
        end do
        do ii=1,nelty
           call iexcha(lquad(ii))
        end do
        do ii=1,nelty
           call iexcha(ngaus(ii))
        end do
        call iexcha(kfl_binar)
        do ii=1,3
           call rexcha(scale(3))
        end do

        !-------------------------------------------------------------------
        !
        ! Read in reageo
        !
        !-------------------------------------------------------------------

        call iexcha(kfl_chege)
        call iexcha(kfl_naxis)
        call iexcha(kfl_spher)
        call iexcha(kfl_bouel)
        call iexcha(nmate)
        call iexcha(nmatf)
        call iexcha(ngrou_dom)

        !-------------------------------------------------------------------
        !
        ! Calculated in cderda
        !
        !-------------------------------------------------------------------

        call iexcha(iesta_dom)
        call iexcha(iesto_dom)
        call iexcha(ibsta_dom)
        call iexcha(ibsto_dom)
        call iexcha(ndimb)
        call iexcha(ntens)
        call iexcha(mnode)
        call iexcha(mgaus)
        call iexcha(mnodb)
        call iexcha(mgaub)
        call iexcha(mlapl)
        do ii=1,nelty
           call iexcha(ngaus(ii))
        end do
        do ii=1,nelty
           call iexcha(lrule(ii))
        end do
        do ii=1,nelty
           call iexcha(nface(ii))
        end do
        do ii=1,nelty
           call rexcha(hnatu(ii))
        end do 

        !-------------------------------------------------------------------
        !
        ! Read in reaset
        !
        !-------------------------------------------------------------------

        call iexcha(ndidi)
        call iexcha(neset)
        call iexcha(nbset)
        call iexcha(nnset)

        !-------------------------------------------------------------------
        !
        ! Read in reabcs
        !
        !-------------------------------------------------------------------

        call iexcha(ncodn)
        call iexcha(ncodb)
        call iexcha(kfl_geome) 
        call iexcha(kfl_extra) 
        call iexcha(kfl_icodn) 
        call iexcha(kfl_icodb) 
        call iexcha(mvcod) 
        call iexcha(mvcob) 
        call iexcha(nvcod) 
        call iexcha(nvcob) 
        !do ii = 1,mvcod
        !   call iexcha(lvcod(1,ii)) 
        !   call iexcha(lvcod(2,ii)) 
        !end do
        !do ii = 1,mvcob
        !   call iexcha(lvcob(1,ii)) 
        !   call iexcha(lvcob(2,ii)) 
        !end do
        !
        ! Send or receive data
        !
        if(parii==1) then
           allocate(parin(npari),stat=istat)
           call memchk(zero,istat,mem_servi(1:2,servi),'PARIN','par_outprt',parin)
           allocate(parre(nparr),stat=istat)
           call memchk(zero,istat,mem_servi(1:2,servi),'PARRE','par_outprt',parre)
           if( READ_AND_RUN() ) call par_receiv()
        end if

     end do

     if( PART_AND_WRITE() ) call par_sendin()

     call memchk(two,istat,mem_servi(1:2,servi),'PARIN','par_outprt',parin)
     deallocate(parin,stat=istat)
     if(istat/=0) call memerr(two,'PARIN','par_outprt',0_ip)
     call memchk(two,istat,mem_servi(1:2,servi),'PARRE','par_outprt',parre)
     deallocate(parre,stat=istat)
     if(istat/=0) call memerr(two,'PARRE','par_outprt',0_ip)
     !
     ! reabcs arrays
     !
     if( nvcod > 0 ) then
        strin =  'LVCOD'
        if( READ_AND_RUN() ) then
           call membcs(8_ip)
           call par_parari('RCV',0_ip,2*mvcod,lvcod)
        else
           call par_parari('SND',0_ip,2*mvcod,lvcod)           
        end if
     end if
     if( nvcob > 0 ) then
        strin =  'LVCOB'
        if( READ_AND_RUN() ) then
           call membcs(10_ip)
           call par_parari('RCV',0_ip,2*mvcob,lvcob)
        else
           call par_parari('SND',0_ip,2*mvcob,lvcob)           
        end if
     end if

     !-------------------------------------------------------------------
     !
     ! READ AND RUN is carried out with wrong number of subdomains
     !
     !-------------------------------------------------------------------

     if( npart_tmp /= npart_par ) call runend('WRONG NUMBER OF SUBDOMAINS')

     !-------------------------------------------------------------------
     !
     ! Sets arrays
     !
     !-------------------------------------------------------------------

     if( nnset > 0 ) then
        if( READ_AND_RUN() ) call memose(3_ip)
        npari =  nnset
        parin => lnsec
        strin =  'LNSEC'
        call par_senprt()
     end if

     if( nbset > 0 ) then
        if( READ_AND_RUN() ) call memose(4_ip)
        npari =  nbset
        parin => lbsec
        strin =  'LBSEC'
        call par_senprt()
     end if

     if( neset > 0 ) then
        if( READ_AND_RUN() ) call memose(5_ip)
        npari =  neset
        parin => lesec
        strin =  'LESEC'
        call par_senprt()
     end if

     !-------------------------------------------------------------------
     !
     ! Partition structure
     !
     !-------------------------------------------------------------------
     !
     ! Allocate memory for partition structure
     !
     if( READ_AND_RUN() ) then
        call par_memory(8_ip)
        call par_memset(1_ip)
        call par_memset(2_ip)
     end if
     !
     ! Subdomain and partition dimensions, and sets
     !
     strin = 'Subdomain_and_partition_dimensions'
     strre = 'Subdomain_and_partition_dimensions'
     strch = 'Subdomain_and_partition_dimensions'
     do parii=1,2 
        npari=0
        nparr=0
        nparc=0
        do ii=1,npart_par
           call iexcha(npoin_par(ii))
        end do
        do ii=1,npart_par
           call iexcha(nelem_par(ii))
        end do
        do ii=1,npart_par
           call iexcha(nboun_par(ii))
        end do
        do ii=1,npart_par
           call iexcha(nskew_par(ii))
        end do
        do ii=1,npart_par
           call iexcha(slfbo_par(ii))
        end do
        call iexcha(npoin_total)
        call iexcha(nelem_total)
        call iexcha(nboun_total)
        call iexcha(gnb)
        call iexcha(gni)
        !
        ! Node Set
        !
        if( nnset > 0 ) then
           do ii=1,npart_par
              call iexcha(nnset_par(ii))
           end do
           do ji=1,2
              do ii=1,nnset
                 call iexcha(lnsec_par(ii,ji))
              end do
           end do
        end if
        !
        ! Send or receive data
        !
        if(parii==1) then
           allocate(parin(npari),stat=istat)
           call memchk(zero,istat,mem_servi(1:2,servi),'PARIN','par_outprt',parin)
           allocate(parre(nparr),stat=istat)
           call memchk(zero,istat,mem_servi(1:2,servi),'PARRE','par_outprt',parre)
           if( READ_AND_RUN() ) call par_receiv()
        end if

     end do

     if( PART_AND_WRITE() ) call par_sendin()

     call memchk(two,istat,mem_servi(1:2,servi),'PARIN','par_outprt',parin)
     deallocate(parin,stat=istat)
     if(istat/=0) call memerr(two,'PARIN','par_outprt',0_ip)
     call memchk(two,istat,mem_servi(1:2,servi),'PARRE','par_outprt',parre)
     deallocate(parre,stat=istat)
     if(istat/=0) call memerr(two,'PARRE','par_outprt',0_ip)

     !-------------------------------------------------------------------
     !
     ! Partition structure arrays
     !
     !-------------------------------------------------------------------

     npari = 0
     nparr = 0
     nparc = 0
     !
     ! GINDE_PAR
     !
     npari =  4*(npart_par+1)
     call par_senprt_int(npari,ginde_par)
     !
     ! LNEIG_PAR
     !
     npari =  npart_par
     parin => lneig_par
     strin =  'LNEIG_PAR'
     call par_senprt()

     if( kfl_outfo /= 50 ) then
        !if( READ_AND_RUN() ) call par_memory(2_ip)
        !
        ! LNINV_LOC: Only needed when master does postprocess
        !
        !npari =  npoin_total
        !parin => lninv_loc
        !strin =  'LNINV_LOC'
        !call par_senprt()
        !
        ! XLNIN_LOC
        !
        !npari =  npart_par+1
        !parin => xlnin_loc
        !strin =  'XLNIN_PAR'
        !call par_senprt()
     end if

  end if

  nullify(parin)
  nullify(parre)

end subroutine par_outprt

subroutine par_senprt
  use def_master
  implicit none
  
  if( PART_AND_WRITE() ) then
     call par_sendin()
  else
     call par_receiv()
  end if

end subroutine par_senprt


