subroutine par_submsh()
  !-----------------------------------------------------------------------
  !****f* domain/par_submsh
  ! NAME
  !    domain
  ! DESCRIPTION
  !    Create edge table
  ! OUTPUT
  !    NNEDG ... Number of edges
  !    LEDGG ... Edge table
  !    LEDGB ... Boundary edge table (when Parall is on)
  ! USED BY
  !    Turnon
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_domain
  use def_master
  use def_parall
  use mod_memchk
  use mod_memory
  use mod_parall, only : PAR_COMM_MY_CODE_ARRAY
  use mod_parall, only : commd,PAR_COMM_MY_CODE4
  use mod_parall, only : PAR_INTEGER
  implicit none 
#ifdef MPI_OFF
#else
  include  'mpif.h'
  integer(4)               :: status(MPI_STATUS_SIZE)
#endif
  integer(ip)              :: iedgg,iedgb,ii,ineig,kk,ifacg
  integer(ip)              :: dom_i,bsize,kpoin,kface,ini
  integer(ip)              :: kedge,ipoin,jpoin,ifacb,ielem,kelem
  integer(ip), pointer     :: ledg1(:)      => null()
  integer(ip), pointer     :: ledg2(:)      => null()
  integer(ip), pointer     :: ledgb_loc(:)  => null()
  integer(ip), pointer     :: lfac1(:)      => null()
  integer(ip), pointer     :: lfac2(:)      => null()
  integer(ip), pointer     :: lfac3(:)      => null()
  integer(ip), pointer     :: lfac4(:)      => null()
  integer(ip), pointer     :: lfacb_loc(:)  => null()
  integer(ip), pointer     :: bound_perm(:) => null()
  integer(ip), pointer     :: permR(:)      => null()
  integer(ip), pointer     :: invpR(:)      => null()
  type(i1p),   pointer     :: bperm(:)      => null()
  integer(4)               :: istat,bsize4
  real(rp)                 :: time0,time1,time2,time3,time4
  real(rp)                 :: time5,time6
  ! Renumbering
  integer(ip)              :: dummi
  integer(ip), pointer     :: perm2(:) => null()
  integer(ip), pointer     :: invp2(:) => null()
  integer(ip), pointer     :: ia(:)    => null()
  integer(ip), pointer     :: ja(:)    => null()

  nullify(ledg2)
  nullify(ledgb)
  nullify(lfac1)
  nullify(lfac2)
  nullify(lfac3)
  nullify(lfac4)
  nullify(lfacb_loc)
  nullify(bound_perm)
  nullify(permR)
  nullify(invpR)
  nullify(bperm)
  nullify(perm2)
  nullify(invp2)
  nullify(ia)
  nullify(ja)
  
  !-------------------------------------------------------------------
  !
  ! List of my own nodes:            LOWNS_PAR
  ! List of my neighbor's own nodes: LOWNR_PAR
  !
  !-------------------------------------------------------------------

  call cputim(time0)
  if( INOTMASTER ) then

     call memory_alloca(mem_servi(1:2,servi),'LOWNS_PAR','par_submsh',lowns_par,commd%bound_dim)
     call memory_alloca(mem_servi(1:2,servi),'LOWNR_PAR','par_submsh',lownr_par,commd%bound_dim)
     do ii = 1,commd % bound_dim
        ipoin = commd % bound_perm(ii)
        if( ipoin >= npoi2 .and. ipoin <= npoi3 ) then
           lowns_par(ii) = 1
        else
           lowns_par(ii) = 0
        end if
     end do

#ifdef MPI_OFF
#else
     do ii = 1, nneig
        dom_i  = commd % neights(ii)
        ini    = commd % bound_size(ii)
        bsize  = commd % bound_size(ii+1) - ini 
        bsize4 = int(bsize,4)
        call MPI_Sendrecv(                 & 
             lowns_par(ini:), bsize4,      &
             PAR_INTEGER,  dom_i, 0_4,     &
             lownr_par(ini:), bsize4,      &
             PAR_INTEGER,  dom_i, 0_4,     &
             PAR_COMM_MY_CODE4, status, istat )
     end do
#endif

  end if

  call cputim(time1)

  !-------------------------------------------------------------------
  !
  ! A. Determine neighbors that share the same edge IEDGG (IEDGB)
  !
  !-------------------------------------------------------------------

  call par_bouedg()
  call cputim(time2)

  call par_boufac()
  call cputim(time3)

  if( INOTMASTER ) then

     call memory_deallo(mem_servi(1:2,servi),'LOWNS_PAR','par_submsh',lowns_par)
     call memory_deallo(mem_servi(1:2,servi),'LOWNR_PAR','par_submsh',lownr_par)

  end if

  if( INOTMASTER ) then

     !-------------------------------------------------------------------
     !
     ! B. Reorder. faces were already ordered in par_boufac
     !
     !-------------------------------------------------------------------

     if( nedgb > 0 ) then
        call memory_alloca(mem_servi(1:2,servi),'LEDGB_LOC','par_submsh',ledgb_loc,nedgb)
        call memory_alloca(mem_servi(1:2,servi),'LEDG1',    'par_submsh',ledg1,    nedgb)
        call memory_alloca(mem_servi(1:2,servi),'LEDG2',    'par_submsh',ledg2,    nedgb)
     end if

     if( nfacb > 0 ) then
        call memory_alloca(mem_servi(1:2,servi),'LFACB_LOC','par_submsh',lfacb_loc,nfacb)
        call memory_alloca(mem_servi(1:2,servi),'LFAC1',    'par_submsh',lfac1,    nfacb)
        call memory_alloca(mem_servi(1:2,servi),'LFAC2',    'par_submsh',lfac2,    nfacb)
        call memory_alloca(mem_servi(1:2,servi),'LFAC3',    'par_submsh',lfac3,    nfacb)
        call memory_alloca(mem_servi(1:2,servi),'LFAC4',    'par_submsh',lfac4,    nfacb)
     end if

     do iedgb = 1,nedgb
        iedgg = ledgb(1,iedgb)
        if( ledgb(2,iedgb) > ledgb(3,iedgb) ) then
           ipoin          = ledgb(2,iedgb)            ! B1: reorder boundary edge
           ledgb(2,iedgb) = ledgb(3,iedgb)            ! B1: reorder boundary edge
           ledgb(3,iedgb) = ipoin
           ipoin          = ledgg(1,iedgg)            ! B2: reorder corresponding global edge
           ledgg(1,iedgg) = ledgg(2,iedgg)            ! B2: reorder corresponding global edge
           ledgg(2,iedgg) = ipoin
        end if
     end do


     call memory_alloca(mem_servi(1:2,servi),'BPERM','par_submsh',bperm,nneig)

     call memgen(1_ip,npoin,0_ip)

     do ineig = 1,nneig

        !----------------------------------------------------------------
        !
        ! C. Fill in new edge node array in common with neighbor INEIG
        !
        !----------------------------------------------------------------

        dom_i = commd%neights(ineig)
        kedge = 0
        do iedgb = 1,nedgb
           do ii = 1,ledgb(4,iedgb)
              if( ledgb(4+ii,iedgb) ==  ineig ) then
                 iedgg            = ledgb(1,iedgb)
                 kedge            = kedge + 1
                 ledgb_loc(kedge) = abs(ledgg(3,iedgg))
                 ledg1(kedge)     = ledgb(2,iedgb)
                 ledg2(kedge)     = ledgb(3,iedgb)
              end if
           end do
        end do

        kface = 0
        do ifacb = 1,nfacb
           if( lfacb(7,ifacb) == ineig ) then
              ifacg            = lfacb(1,ifacb)
              kface            = kface + 1
              lfacb_loc(kface) = abs(lfacg(5,ifacg))
              lfac1(kface)     = lfacb(2,ifacb)
              lfac2(kface)     = lfacb(3,ifacb)
              lfac3(kface)     = lfacb(4,ifacb)
              lfac4(kface)     = lfacb(5,ifacb)
           end if
        end do

        !----------------------------------------------------------------
        !
        ! D. Order edges with INEIG so that the send and receive correspond
        !
        !----------------------------------------------------------------

        !if( nedgb > 0 ) call par_sorti2(kedge,ledg1,ledg2,ledgb_loc)
        if( nedgb > 0 ) call hsort2(2_ip,kedge,ledg1,ledg2,ledgb_loc)
        if( nfacb > 0 ) call par_sorti4(kface,lfac1,lfac2,lfac3,lfac4,lfacb_loc)

        !----------------------------------------------------------------
        !
        ! E. Add edge nodes with INEIG to permutation list
        !
        !----------------------------------------------------------------

        !
        ! E1. Copy old permutation array and reallocate it
        !
        kk = 0
        do ii = commd % bound_size(ineig),commd % bound_size(ineig+1)-1
           kk = kk + 1
           gisca(kk) = commd % bound_perm(ii)
        end do
        bsize = commd % bound_size(ineig+1)-commd % bound_size(ineig) + kedge + kface
        call memory_alloca(mem_servi(1:2,servi),'BPERM % L','par_submsh',bperm(ineig) % l,bsize)
        kk = 0
        do ii = commd % bound_size(ineig),commd % bound_size(ineig+1)-1
           kk = kk + 1
           bperm(ineig) % l(kk) = gisca(kk) 
        end do
        do iedgb = 1,kedge
           kk = kk + 1 
           bperm(ineig) % l(kk) = ledgb_loc(iedgb)
        end do
        do ifacb = 1,kface
           kk = kk + 1 
           bperm(ineig) % l(kk) = lfacb_loc(ifacb)           
        end do
        commd % bound_dim = commd % bound_dim + kedge + kface
     end do
     !
     ! Deallocate memory
     !
     call memory_deallo(mem_servi(1:2,servi),'commd % bound_perm','par_submsh',commd % bound_perm)
     call memory_alloca(mem_servi(1:2,servi),'commd % bound_perm','par_submsh',commd % bound_perm,commd % bound_dim)

     if( nedgb > 0 ) then
        call memory_deallo(mem_servi(1:2,servi),'LEDG2','par_submsh',ledg2)
        call memory_deallo(mem_servi(1:2,servi),'LEDG1','par_submsh',ledg1)
     end if
     if( nfacb > 0 ) then
        call memory_deallo(mem_servi(1:2,servi),'LFAC4','par_submsh',lfac4)
        call memory_deallo(mem_servi(1:2,servi),'LFAC3','par_submsh',lfac3)
        call memory_deallo(mem_servi(1:2,servi),'LFAC2','par_submsh',lfac2)
        call memory_deallo(mem_servi(1:2,servi),'LFAC1','par_submsh',lfac1)
     end if

     commd % bound_size(1) = 1
     kk = 0
     do ineig = 1,nneig
        bsize = size( bperm(ineig) % l )
        commd % bound_size(ineig+1) = commd % bound_size(ineig) + bsize
        do ii = 1,bsize
           kk = kk + 1
           commd % bound_perm(kk) = bperm(ineig) % l(ii)
        end do
        call memory_deallo(mem_servi(1:2,servi),'bperm(ineig)%l','par_submsh', bperm(ineig) % l)
     end do
     call memory_deallo(mem_servi(1:2,servi),'bperm','par_submsh', bperm)
     call memgen(3_ip,npoin,0_ip)

  end if

  call cputim(time4)

  if( INOTMASTER ) then

     !-------------------------------------------------------------------
     !
     ! F. Local renumbering
     !
     !-------------------------------------------------------------------

     call memory_alloca(mem_servi(1:2,servi),'permR','par_submsh',permR,npoin)
     call memory_alloca(mem_servi(1:2,servi),'invpr','par_submsh',invpR,npoin)
     !
     ! PERMR: Identify boundary nodes. Others'=-1, Own=-2
     !
     do ipoin = npoi1+1,npoi2-1
        permR(ipoin) = -1
     end do
     do ipoin = npoi2,npoi3
        permR(ipoin) = -2
     end do

     do ipoin = npoi3+1,npoin_old
        permR(ipoin) = -1
     end do
     do iedgb = 1,nedgb
        iedgg = ledgb(1,iedgb)
        ipoin = ledgg(3,iedgg)
        if( ipoin < 0 ) then
           permR(-ipoin) = -2
        else
           permR( ipoin) = -1
        end if
     end do
     do ifacb = 1,nfacb
        ifacg = lfacb(1,ifacb)
        ipoin = lfacg(5,ifacg)
        if( ipoin < 0 ) then
           permR(-ipoin) = -2
        else
           permR( ipoin) = -1
        end if
     end do
     !
     ! Renumber interior nodes
     ! 
     kpoin = 0
     do ipoin = 1,npoin
        if( permR(ipoin) == 0 ) then
           kpoin = kpoin + 1
           permR(ipoin) = kpoin
           invpR(kpoin) = ipoin
        end if
     end do
     !
     ! Re-renumber interior nodes using METIS
     !      
     if( 1 == 1 .and. kpoin > 0 ) then

        call memory_alloca(mem_servi(1:2,servi),'ia','par_submsh',ia,kpoin+1_ip)

        call par_intgra(kpoin,permR,ia)         ! Graph of interior nodes
        ja => gisca

        call memory_alloca(mem_servi(1:2,servi),'invp2','par_submsh',invp2,kpoin)
        call memory_alloca(mem_servi(1:2,servi),'perm2','par_submsh',perm2,kpoin)

        call par_metis(&
             2_ip , dummi , dummi , dummi , dummi , dummi , &
             dummi , dummi , dummi , dummi , kpoin, ia, &
             ja, invp2, perm2, dummi , dummi , dummi , &
             mem_servi(1:2,servi) )

        kpoin = 0
        do ipoin = 1,npoin
           if( permR(ipoin) > 0 ) then
              kpoin        = kpoin + 1
              jpoin        = perm2(kpoin)
              permR(ipoin) = jpoin
              invpR(jpoin) = ipoin
           end if
        end do

        call memory_deallo(mem_servi(1:2,servi),'invp2','par_submsh',invp2)
        call memory_deallo(mem_servi(1:2,servi),'perm2','par_submsh',perm2)

        call memgen(3_ip,1_ip,0_ip)        ! Was allocated in par_intgra()
        call memory_deallo(mem_servi(1:2,servi),'ia','par_submsh',ia)

     end if
     !
     ! Renumber own boundary nodes
     !
     npoi1 = kpoin 
     npoi2 = kpoin + 1
     do ipoin = 1,npoin
        if( permR(ipoin) == -2 ) then
           kpoin = kpoin + 1
           permR(ipoin) = kpoin ! NEW => OLD
           invpR(kpoin) = ipoin ! OLD => NEW
        end if
     end do
     npoi3 = kpoin
     !
     ! Renumber others' boundary nodes
     !
     do ipoin = 1,npoin
        if( permR(ipoin) == -1 ) then
           kpoin = kpoin + 1
           permR(ipoin) = kpoin
           invpR(kpoin) = ipoin
        end if
     end do
     !
     ! Reorder arrays
     !
     lrenn => permR
     call renpoi()
     !
     ! Reorder communication array
     !
     call memory_alloca(mem_servi(1:2,servi),'bound_perm','par_submsh',bound_perm,commd % bound_dim)
     do ii = 1,commd % bound_dim         
        bound_perm(ii) = commd % bound_perm(ii)
     end do
     do ii = 1,commd % bound_dim 
        commd % bound_perm(ii) = lrenn( bound_perm(ii) )
     end do
     !
     ! LNLEV: Save permutation with original mesh
     !
     do ipoin = 1,npoin_old
        invpR(ipoin) = lnlev(ipoin)
     end do
     do ipoin = 1,npoin
        lnlev(ipoin) = 0
     end do
     do ipoin = 1,npoin_old
        kpoin = permR(ipoin) 
        lnlev(kpoin) = invpR(ipoin)
     end do
     !
     ! Deallocate memory
     !
     call memory_deallo(mem_servi(1:2,servi),'bound_perm','par_submsh',bound_perm)
     call memory_deallo(mem_servi(1:2,servi),'permR','par_submsh',permR)
     call memory_deallo(mem_servi(1:2,servi),'invpR','par_submsh',invpR)

  end if

  call cputim(time5)

  !-------------------------------------------------------------------
  !
  ! G. LNINV_LOC: Uniquely renumber the nodes. 
  !    LEINV_LOC: Uniquely renumber the element.
  !    This is useful for:
  !    - Postprocess
  !    - To call the division recursively
  !
  !-------------------------------------------------------------------

  call memgen(1_ip,npart_par+1,0_ip)

  call par_algath()

  npoin_total = 0
  nelem_total = 0
  nboun_total = 0

  if( IMASTER ) then
     do kfl_desti_par = 1,npart_par
        npoin_par(kfl_desti_par) = npoia_tot(kfl_desti_par+1)
        nelem_par(kfl_desti_par) = nelem_tot(kfl_desti_par+1)
        nboun_par(kfl_desti_par) = nboun_tot(kfl_desti_par+1)
        npoin_total              = npoin_total + npoin_par(kfl_desti_par)
        nelem_total              = nelem_total + nelem_par(kfl_desti_par)
        nboun_total              = nboun_total + nboun_par(kfl_desti_par)
     end do
  end if
  !
  ! Send complete mesh informaiton to slave
  !
  call par_parari('BCT',0_ip,1_ip,npoin_total)
  !
  ! LNINV_LOC: nodal global and unique numbering
  !
  do ii = 1,npart_par+1
     gisca(ii) = npoin_tot(ii)
  end do
  do ii = 2,npart_par+1
     gisca(ii) = gisca(ii-1) + gisca(ii)  
  end do

  if( INOTMASTER ) then

     call memory_deallo(mem_servi(1:2,servi),'LNINV_LOC','par_submsh',lninv_loc)
     call memory_alloca(mem_servi(1:2,servi),'LNINV',    'par_submsh',lninv_loc,npoin)

     do ipoin = 1,npoin
        lninv_loc(ipoin) = 0
     end do
     kpoin = gisca(kfl_paral)
     do ipoin = 1,npoi1
        kpoin = kpoin + 1
        lninv_loc(ipoin) = kpoin
     end do
     do ipoin = npoi2,npoi3
        kpoin = kpoin + 1
        lninv_loc(ipoin) = kpoin
     end do

     party=3;pardi=1;parki=1
     pari1 => lninv_loc
     call par_slexch()

  end if
  !
  ! LEINV_LOC: element global and unique numbering
  !
  do ii = 1,npart_par+1
     gisca(ii) = nelem_tot(ii)
  end do
  do ii = 2,npart_par+1
     gisca(ii) = gisca(ii-1) + gisca(ii)  
  end do

  if( INOTMASTER ) then

     call memory_deallo(mem_servi(1:2,servi),'LEINV',    'par_submsh',leinv_loc)
     call memory_alloca(mem_servi(1:2,servi),'LEINV',    'par_submsh',leinv_loc,nelem)

     kelem = gisca(kfl_paral)
     do ielem = 1,nelem
        kelem = kelem + 1
        leinv_loc(ielem) = kelem
     end do

  end if

  call memgen(3_ip,npart_par+1,0_ip)

  !----------------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !----------------------------------------------------------------------

  if( INOTMASTER ) then

     if( nedgb > 0 ) then
        call memory_deallo(mem_servi(1:2,servi),'LEDGB_LOC','par_submsh',ledgb_loc)
        call memory_deallo(mem_servi(1:2,servi),'LEDGB',    'par_submsh',ledgb)
     end if

     if( nfacb > 0 ) then
        call memory_deallo(mem_servi(1:2,servi),'LFACB_LOC','par_submsh',lfacb_loc)
        call memory_deallo(mem_servi(1:2,servi),'LFACB',    'par_submsh',lfacb)
     end if

  end if

  call cputim(time6)

  cpu_other(4) = cpu_other(4) + time1 - time0 
  cpu_other(5) = cpu_other(5) + time2 - time1 
  cpu_other(6) = cpu_other(6) + time3 - time2 
  cpu_other(7) = cpu_other(7) + time4 - time3 
  cpu_other(8) = cpu_other(8) + time5 - time4 
  cpu_other(9) = cpu_other(9) + time6 - time5 

end subroutine par_submsh

subroutine par_sorti2(n,a,b,c)

  !-----------------------------------------------------------------------
  !
  ! Sort a vector c according to a and then b
  !
  !-----------------------------------------------------------------------
  use       def_kintyp
  implicit none
  integer(ip) :: n,i,j,t,u,v
  integer(ip) :: a(n),b(n),c(n)

  do i = 1,n-1
     do j = i+1,n
        if( a(i) > a(j) ) then
           t    = a(i)
           a(i) = a(j) 
           a(j) = t
           u    = b(i)
           b(i) = b(j) 
           b(j) = u
           v    = c(i)
           c(i) = c(j) 
           c(j) = v
        end if
     end do
  end do

  do i = 1,n-1
     do j = i+1,n
        if( a(i) == a(j) ) then
           if( b(i) > b(j) ) then
              t    = a(i)
              a(i) = a(j) 
              a(j) = t
              u    = b(i)
              b(i) = b(j) 
              b(j) = u
              v    = c(i)
              c(i) = c(j) 
              c(j) = v
           end if
        end if
     end do
  end do

end subroutine par_sorti2

subroutine par_sorti4(n,a1,a2,a3,a4,c)

  !-----------------------------------------------------------------------
  !
  ! Sort a vector c according to a and then b
  !
  !-----------------------------------------------------------------------
  use       def_kintyp
  implicit none
  integer(ip) :: n,i,j,t1,t2,t3,t4,v
  integer(ip) :: a1(n),a2(n),a3(n),a4(n),c(n)

  do i = 1,n-1
     do j = i+1,n
        if( a1(i) > a1(j) ) then
           t1    = a1(i)
           a1(i) = a1(j) 
           a1(j) = t1
           t2    = a2(i)
           a2(i) = a2(j) 
           a2(j) = t2
           t3    = a3(i)
           a3(i) = a3(j) 
           a3(j) = t3
           t4    = a4(i)
           a4(i) = a4(j) 
           a4(j) = t4
           v     = c(i)
           c(i)  = c(j) 
           c(j)  = v
        end if
     end do
  end do

  do i = 1,n-1
     do j = i+1,n
        if( a1(i) == a1(j) ) then
           if( a2(i) > a2(j) ) then
              t1    = a1(i)
              a1(i) = a1(j) 
              a1(j) = t1
              t2    = a2(i)
              a2(i) = a2(j) 
              a2(j) = t2
              t3    = a3(i)
              a3(i) = a3(j) 
              a3(j) = t3
              t4    = a4(i)
              a4(i) = a4(j) 
              a4(j) = t4
              v     = c(i)
              c(i)  = c(j) 
              c(j)  = v
           end if
        end if
     end do
  end do

  do i = 1,n-1
     do j = i+1,n
        if( a1(i) == a1(j) .and. a2(i) == a2(j) ) then
           if( a3(i) > a3(j) ) then
              t1    = a1(i)
              a1(i) = a1(j) 
              a1(j) = t1
              t2    = a2(i)
              a2(i) = a2(j) 
              a2(j) = t2
              t3    = a3(i)
              a3(i) = a3(j) 
              a3(j) = t3
              t4    = a4(i)
              a4(i) = a4(j) 
              a4(j) = t4
              v     = c(i)
              c(i)  = c(j) 
              c(j)  = v
           end if
        end if
     end do
  end do

  do i = 1,n-1
     do j = i+1,n
        if( a1(i) == a1(j) .and. a2(i) == a2(j) .and. a3(i) == a3(j) ) then
           if( a4(i) > a4(j) ) then
              t1    = a1(i)
              a1(i) = a1(j) 
              a1(j) = t1
              t2    = a2(i)
              a2(i) = a2(j) 
              a2(j) = t2
              t3    = a3(i)
              a3(i) = a3(j) 
              a3(j) = t3
              t4    = a4(i)
              a4(i) = a4(j) 
              a4(j) = t4
              v     = c(i)
              c(i)  = c(j) 
              c(j)  = v
           end if
        end if
     end do
  end do

end subroutine par_sorti4

subroutine par_caca
  !-----------------------------------------------------------------------
  !****f* Parall/par_caca
  ! NAME
  !    par_caca
  ! DESCRIPTION
  !    This subroutine exchange arrays between master and slaves
  !    commd%bound_perm(jj):      permutation array
  !    loc_sparr1:                my local values
  !    loc_rparr1:                values given by neighbor ii
  !    commd%bound_dim:           size of communication array
  !    nneig:                     number of neighbors that share same group
  !    commd%neights(ii):         number of subdomain ii
  !    commd%bound_size(ii):      where my local arrays sart to exchange with ii
  !    commd%bound_size(ii+1)
  !        -commd%bound_size(ii): number of groups to exchange with ii
  ! USED BY
  !    Parall
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_parame
  use def_parall
  use def_domain
  use mod_memchk  
  use mod_parall, only : PAR_COMM_MY_CODE_ARRAY
  use mod_parall, only : commd,PAR_COMM_MY_CODE4
  use mod_parall, only : PAR_INTEGER

  implicit none
#ifdef MPI_OFF
#else
  include  'mpif.h'
  integer(4)               :: status(MPI_STATUS_SIZE)
#endif
  integer(ip)              :: ipoin,ii,jj,bsize,ini,dom_i,idime,kk
  integer(4)               :: istat,bsize4
  real(rp), allocatable    :: loc_sparr1(:),   loc_rparr1(:)

  if(ISLAVE) then

     !-------------------------------------------------------------
     !
     ! INT(NPOIN)
     !
     !-------------------------------------------------------------

     allocate(loc_sparr1(ndime*commd%bound_dim),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LOC_SPARR1','par_caca',loc_sparr1)

     allocate(loc_rparr1(ndime*commd%bound_dim),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LOC_RPARR1','par_caca',loc_rparr1)

     kk=0
     do jj= 1, commd%bound_dim
        ipoin = commd%bound_perm(jj)
        !if(ipoin>=npoi2.and.ipoin<=npoi3) loc_sparr1(jj) = 1
        do idime = 1,ndime
           kk=kk+1
           loc_sparr1(kk) = coord(idime,ipoin)
        end do
     enddo

     do ii= 1, nneig
        dom_i = commd%neights(ii)

        ini   = commd%bound_size(ii)
        bsize = (commd%bound_size(ii+1) - ini)*ndime

#ifdef MPI_OFF
#else
        bsize4=int(bsize,4)
        call MPI_Sendrecv( loc_sparr1(ini:), bsize4,&
             MPI_DOUBLE_PRECISION,  dom_i, 0_4,     &
             loc_rparr1(ini:), bsize4,              &
             MPI_DOUBLE_PRECISION, dom_i, 0_4,      &
             PAR_COMM_MY_CODE4, status, istat )
#endif
     enddo

     kk=0
     do jj= 1, commd%bound_dim
        ipoin = commd%bound_perm(jj)
        do idime =1,ndime
           kk=kk+1
           if( abs(loc_sparr1(kk)-loc_rparr1(kk))>1.0e-12_rp ) then
              print*,'a=',kfl_paral,jj,ipoin,loc_sparr1(kk),loc_rparr1(kk)
           end if
        end do
        !if( loc_sparr1(jj) == 1 .and. loc_rparr1(jj) == 1 ) then 
        !   print*,kfl_paral,jj,parin(ipoin)
        !end if
     enddo

     call memchk(two,istat,mem_servi(1:2,servi),'LOC_RPARR1','par_caca',loc_rparr1)
     deallocate(loc_rparr1,stat=istat)
     if(istat/=0) call memerr(two,'LOC_RPARR1','par_caca',0_ip)

     call memchk(two,istat,mem_servi(1:2,servi),'LOC_SPARR1','par_caca',loc_sparr1)
     deallocate(loc_sparr1,stat=istat)
     if(istat/=0) call memerr(two,'LOC_SPARR1','par_caca',0_ip)

  end if

end subroutine par_caca

subroutine par_pipi
  !-----------------------------------------------------------------------
  !****f* Parall/par_caca
  ! NAME
  !    par_caca
  ! DESCRIPTION
  ! USED BY
  !    Parall
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_parame
  use def_parall
  use def_domain
  use mod_memchk  
  use mod_parall, only : PAR_COMM_MY_CODE_ARRAY
  use mod_parall, only : commd,PAR_COMM_MY_CODE4
  use mod_parall, only : PAR_INTEGER
  implicit none
#ifdef MPI_OFF
#else
  include  'mpif.h'
  integer(4)               :: status(MPI_STATUS_SIZE)
#endif
  integer(ip)              :: ipoin,ii,jj,bsize,ini,dom_i,kk
  integer(4)               :: istat,bsize4
  integer(ip), allocatable :: loc_sparr1(:),   loc_rparr1(:)

  if(ISLAVE) then

     !-------------------------------------------------------------
     !
     ! INT(NPOIN)
     !
     !-------------------------------------------------------------

     allocate(loc_sparr1(commd%bound_dim),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LOC_SPARR1','par_caca',loc_sparr1)

     allocate(loc_rparr1(commd%bound_dim),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LOC_RPARR1','par_caca',loc_rparr1)

     kk=0
     do jj= 1, commd%bound_dim
        ipoin = commd%bound_perm(jj)
        loc_sparr1(jj) = lninv_loc(ipoin)
        !if(ipoin>=npoi2.and.ipoin<=npoi3) loc_sparr1(jj) = 1
     enddo

     do ii= 1, nneig
        dom_i = commd%neights(ii)

        ini   = commd%bound_size(ii)
        bsize = (commd%bound_size(ii+1) - ini)

#ifdef MPI_OFF
#else
        bsize4=int(bsize,4)
        call MPI_Sendrecv( loc_sparr1(ini:), bsize4,&
             PAR_INTEGER,  dom_i, 0_4,     &
             loc_rparr1(ini:), bsize4,              &
             PAR_INTEGER, dom_i, 0_4,      &
             PAR_COMM_MY_CODE4, status, istat )
#endif
     enddo

     kk=0
     do jj= 1, commd%bound_dim
        ipoin = commd%bound_perm(jj)
        !do idime =1,ndime
        !   kk=kk+1
        !   if( abs(loc_sparr1(kk)-loc_rparr1(kk))<1.0e-12_rp ) then
        !      print*,'a=',kfl_paral,jj,ipoin
        !   end if
        !end do
        if( loc_sparr1(jj) /= loc_rparr1(jj) ) then 
           print*,'AAA=',kfl_paral,jj,ipoin
        end if
     enddo

     call memchk(two,istat,mem_servi(1:2,servi),'LOC_RPARR1','par_caca',loc_rparr1)
     deallocate(loc_rparr1,stat=istat)
     if(istat/=0) call memerr(two,'LOC_RPARR1','par_caca',0_ip)

     call memchk(two,istat,mem_servi(1:2,servi),'LOC_SPARR1','par_caca',loc_sparr1)
     deallocate(loc_sparr1,stat=istat)
     if(istat/=0) call memerr(two,'LOC_SPARR1','par_caca',0_ip)

  end if

end subroutine par_pipi

subroutine par_ownbou()
  !-----------------------------------------------------------------------
  !****f* Parall/par_ownbou
  ! NAME
  !    par_ownbou
  ! DESCRIPTION
  !    This subroutine determines neighbors that share the same edge 
  !    IEDGG (IEDGB)
  ! USED BY
  !    Parall
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_parame
  use def_parall
  use mod_parall, only : PAR_COMM_MY_CODE_ARRAY
  use mod_parall, only : commd,PAR_COMM_MY_CODE4
  use mod_parall, only : PAR_INTEGER
  use def_domain
  use mod_memchk
  implicit none
#ifdef MPI_OFF
#else
  include  'mpif.h'
  integer(4)               :: status(MPI_STATUS_SIZE)
#endif
  integer(ip)              :: ipoin,ii,jj,bsize,ini,dom_i
  integer(4)               :: istat,bsize4
  integer(ip), allocatable :: loc_spari1(:),loc_rpari1(:)
  integer(ip)              :: iedgg,iedgb,knode,ineig,kk
  integer(ip)              :: kowne,myown,jpoin
  integer(ip)              :: ll,kpoin(2),kresu(2)
  type(i2p),   pointer     :: lnown(:)

  if( INOTMASTER ) then

     allocate(loc_spari1(commd%bound_dim),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LOC_SPARI1','par_slexch',loc_spari1)

     allocate(loc_rpari1(commd%bound_dim),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LOC_RPARI1','par_slexch',loc_rpari1)
     !
     ! Send to my neighbors my own nodes
     !
     do jj= 1, commd%bound_dim
        ipoin = commd%bound_perm(jj)
        if( ipoin >= npoi2 .and. ipoin <= npoi3 ) then
           loc_spari1(jj) = 1
        end if
     end do

#ifdef MPI_OFF
#else
     do ii = 1, nneig

        dom_i  = commd%neights(ii)
        ini    = commd%bound_size(ii)
        bsize  = commd%bound_size(ii+1) - ini
        bsize4 = int(bsize,4)

        call MPI_Sendrecv( &
             loc_spari1(ini:), bsize4,     &
             PAR_INTEGER,  dom_i, 0_4,     &
             loc_rpari1(ini:), bsize4,     &
             PAR_INTEGER,  dom_i, 0_4,     &
             PAR_COMM_MY_CODE4, status, istat )
     end do
#endif
     !
     ! List of neighbor's owning
     !
     allocate( lnown(nneig) , stat = istat )
     do ineig = 1, nneig
        bsize  = commd%bound_size(ineig+1) - commd%bound_size(ineig)
        allocate( lnown(ineig) % l(2,bsize) , stat = istat )
        kk = 0
        do jj = commd%bound_size(ineig),commd%bound_size(ineig+1)-1
           kk = kk + 1
           ipoin = commd%bound_perm(jj)
           lnown(ineig) % l(1,kk) = ipoin
           lnown(ineig) % l(2,kk) = loc_rpari1(jj)
        end do
     end do

     call memchk(two,istat,mem_servi(1:2,servi),'LOC_RPARI1','par_slexch',loc_rpari1)
     deallocate(loc_rpari1,stat=istat)
     if(istat/=0) call memerr(two,'LOC_RPARI1','par_slexch',0_ip)

     call memchk(two,istat,mem_servi(1:2,servi),'LOC_SPARI1','par_slexch',loc_spari1)
     deallocate(loc_spari1,stat=istat)
     if(istat/=0) call memerr(two,'LOC_SPARI1','par_slexch',0_ip)     

     do iedgg = 1,nedgg
        if( ledgg(4,iedgg) > 0 ) then
           iedgb          = ledgg(4,iedgg)
           ledgb(1,iedgb) = iedgg
           ipoin          = ledgg(1,iedgg)
           jpoin          = ledgg(2,iedgg)
           ledgb(2,iedgb) = lninv_loc(ipoin)          ! A1: global edge node
           ledgb(3,iedgb) = lninv_loc(jpoin)          ! A1: global edge node
           !
           ! Loop over neighbors 
           !
           do ineig = 1,nneig
              knode = 0
              kowne = 0
              myown = 0
              jj    = commd % bound_size(ineig)
              dom_i = commd % neights(ineig)

              do while( jj <= commd%bound_size(ineig+1)-1 .and. knode < 2 )
                 if( commd % bound_perm(jj) == ipoin .or. commd % bound_perm(jj) == jpoin ) then
                    knode = knode + 1
                    if( commd%bound_perm(jj) >= npoi2 .and. commd%bound_perm(jj) <= npoi3 ) then ! A4. Decide who's the owner
                       kowne = kowne + 1
                       if( commd % bound_perm(jj) == ipoin ) myown = ledgb(2,iedgb)
                       if( commd % bound_perm(jj) == jpoin ) myown = ledgb(3,iedgb)
                    end if
                 end if
                 jj = jj + 1
              end do

              !if(knode>0) then
                 !if(kfl_paral==2.and.dom_i==4 ) print*,'ZOBIIIII=',kfl_paral,knode,lninv_loc(ipoin),lninv_loc(jpoin)
                 !if(kfl_paral==4.and.dom_i==2 ) print*,'ZOBIIIII=',kfl_paral,knode,lninv_loc(ipoin),lninv_loc(jpoin)
              !end if

              if( knode == 2 ) then
                 !
                 ! INEIG shares the same edge IPOIN-JPOIN
                 !
                 if( kowne == 2 ) then        ! It is mine
                    kowne =  -1_ip            
                 else if( kowne == 0 ) then   ! It is not mine
                    kowne =   1_ip            
                 else if( kowne == 1 ) then   ! I cannot decide now
                    kpoin = 0
                    kresu = 0
                    ll    = 0
                    kk    = 0
                    jj    = commd%bound_size(ineig)
                    !
                    ! 1= own boundary node
                    ! 0= it's not my own
                    !
                    ! I have:     0---------1
                    ! INEIG has:  0---------0  (stored in lnown(ineig) % l(2,kk))
                    !         or  1---------0
                    !
                    do while( jj <= commd%bound_size(ineig+1)-1 .and. ll < 2 )
                       kk = kk + 1
                       if( lnown(ineig) % l(1,kk) == ipoin ) then
                          ll = ll + 1
                          kpoin(ll) = ipoin
                          kresu(ll) = lnown(ineig) % l(2,kk)
                       else if( lnown(ineig) % l(1,kk) == jpoin ) then
                          ll = ll + 1
                          kpoin(ll) = jpoin
                          kresu(ll) = lnown(ineig) % l(2,kk)                          
                       end if
                    end do
                    if( kpoin(1) == 0 .and. kpoin(2) == 0 ) then
                       write(*,*) 'ERRRRRRRRRRRRRRRRRRRRRRRRRRRRROR 1'
                    else
                       if( kresu(1) == 0 .and. kresu(2) == 0 ) then
                          kowne = -1
                       else
                          if( kresu(1) /= 0 .and. kresu(2) /= 0 ) then
                             write(*,*) 'ERRRRRRRRRRRRRRRRRRRRRRRRRRRRROR 2'
                          else                             
                             if( myown <= ledgb(2,iedgb) .and. myown <= ledgb(3,iedgb) ) then
                                kowne =  -1_ip
                             else
                                kowne =   1_ip
                             end if
                          end if
                       end if
                    end if
                 end if

                 ii                = ledgb(4,iedgb) + 1
                 ledgb(4,   iedgb) = ii                     ! A2: # subd. that share this edge
                 ledgb(4+ii,iedgb) = ineig                  ! A3: List of subd. that share this edge
                 ledgg(3,   iedgg) = ledgg(3,iedgg) * kowne

              end if

           end do
        end if
     end do
     !
     ! deallocate memory
     !
     do ii = 1, nneig
        deallocate( lnown(ii) % l , stat = istat )
     end do
     deallocate( lnown , stat = istat )

  end if

end subroutine par_ownbou


subroutine par_pipi2()
  !-----------------------------------------------------------------------
  !****f* Parall/par_slexch
  ! NAME
  !    par_slexch
  ! DESCRIPTION
  !    This subroutine exchange arrays between master and slaves
  !    commd%bound_perm(jj):      permutation array
  !    loc_sparr1:                my local values
  !    loc_rparr1:                values given by neighbor ii
  !    commd%bound_dim:           size of communication array
  !    nneig:                     number of neighbors that share same group
  !    commd%neights(ii):         number of subdomain ii
  !    commd%bound_size(ii):      where my local arrays sart to exchange with ii
  !    commd%bound_size(ii+1)
  !        -commd%bound_size(ii): number of groups to exchange with ii
  ! USED BY
  !    Parall
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_parame
  use def_parall
  use mod_parall, only : PAR_COMM_MY_CODE_ARRAY
  use mod_parall, only : commd,PAR_COMM_MY_CODE4
  use mod_parall, only : PAR_INTEGER
  use def_domain
  use mod_memchk
  implicit none
#ifdef MPI_OFF
#else
  include  'mpif.h'
  integer(4)               :: status(MPI_STATUS_SIZE)
#endif
  integer(ip)              :: ipoin,ii,jj,bsize,ini,dom_i
  integer(4)               :: istat,bsize4
  real(rp)                 :: time1,time2
  integer(ip), allocatable :: loc_spari1(:),   loc_rpari1(:)

  call cputim(time1)

  do ii= 1, nneig
     dom_i = commd%neights(ii)
     ini   = commd%bound_size(ii)
     bsize = commd%bound_size(ii+1) - ini
     print*,kfl_paral,dom_i,bsize
  end do
  return

  if(kfl_paral>0) then

     !-------------------------------------------------------------
     !
     ! INT(NPOIN)
     !
     !-------------------------------------------------------------

     allocate(loc_spari1(commd%bound_dim),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LOC_SPARI1','par_slexch',loc_spari1)

     allocate(loc_rpari1(commd%bound_dim),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LOC_RPARI1','par_slexch',loc_rpari1)

     do jj= 1, commd%bound_dim
        ipoin = commd%bound_perm(jj)
        loc_spari1(jj) = pari1(ipoin)
     enddo

     do ii= 1, nneig
        dom_i = commd%neights(ii)

        ini   = commd%bound_size(ii)
        bsize = commd%bound_size(ii+1) - ini

#ifdef MPI_OFF
#else
        bsize4=int(bsize,4)
        call MPI_Sendrecv( loc_spari1(ini:), bsize4,&
             PAR_INTEGER,  dom_i, 0_4,     &
             loc_rpari1(ini:), bsize4,              &
             PAR_INTEGER, dom_i, 0_4,      &
             PAR_COMM_MY_CODE4, status, istat )
#endif
     enddo

     do jj= 1, commd%bound_dim
        ipoin = commd%bound_perm(jj)
        pari1(ipoin) = pari1(ipoin) + loc_rpari1(jj)
     enddo

     call memchk(two,istat,mem_servi(1:2,servi),'LOC_RPARI1','par_slexch',loc_rpari1)
     deallocate(loc_rpari1,stat=istat)
     if(istat/=0) call memerr(two,'LOC_RPARI1','par_slexch',0_ip)

     call memchk(two,istat,mem_servi(1:2,servi),'LOC_SPARI1','par_slexch',loc_spari1)
     deallocate(loc_spari1,stat=istat)
     if(istat/=0) call memerr(two,'LOC_SPARI1','par_slexch',0_ip)


  endif

  call cputim(time2)
  cpu_paral(25)=cpu_paral(25)+time2-time1

end subroutine par_pipi2
