subroutine par_elmgra_part()
  !------------------------------------------------------------------------
  !****f* domain/elmgra
  ! NAME
  !    elmgra
  ! DESCRIPTION
  !    This routine computes the element graph using:
  !    ITASK=1 ... The nodal connectivity
  !             =2 ... The face connectivity
  !
  !    For example, given the following mesh, element 5 will have the
  !    following neighbors:
  !    +---+---+---+
  !    | 1 | 2 | 3 |   ITASK=1: 1,2,3,4,6,7,8,9
  !    +---+---+---+   ITASK=2: 2,4,6,8
  !    | 4 | 5 | 6 |
  !    +---+---+---+
  !    | 7 | 8 | 9 |
  !    +---+---+---+
  !
  !    Working arrays:
  !    NEPOI:      Number of occurencies of a node in connectivity
  !                Node ipoin belongs to nepoi(ipoin) elements
  !    PELPO:      Pointer to node connectivities arrays lelpo
  !    LELPO:      List of node to element connectivities
  !    MELEL:      Maximum number of neighbors=max(nepoi)*mnode
  !
  ! OUTPUT
  !    NEDGE:  Number of edges
  !    PELEL:  Pointer to the lelel array of size 
  !                pelel(nelem+1):
  !                - Adjacencies of ielem starts at pelel(ielem) 
  !                  included
  !                - Adjacencies of ielem ends at pelel(ielem+1)-1 
  !                  included
  !                - pelel does not include the element itself
  !    LELEL:  List of adjacencies.
  !
  !    Example:
  !                 1---2---3---4---5
  !                 |   |   |   |   |
  !                 6---7---8---9--10
  !                 |   |   |   |   |
  !                11--12--13--14--15
  !
  !    nedge:  22
  !    element #:  1  2  3  4  5 ...
  !    pelel:  1  3  8  9 13 ...
  !                |  |  |
  !                |  |  +--------------+
  !                |  |                 |
  !                |  +--+              |
  !                |     |              |
  !    lelel:  2  6  1  3  6  7  8  2  4  7  8  9 ...
  !
  ! USED BY
  !    par_partit
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use mod_memory
  use mod_htable
  use def_parall
  use mod_par_partit_paral, only : faces_part, lnods_local, memor_par, kfl_paral_parmes, ltype_recv
  use mod_par_partit_paral, only : nelem_part, nelemRecv, ltype_part, nedge_par, elel_loc, pelel_loc, elem_loc_recv
  use mod_elmgeo,           only : element_type
  implicit none  
  integer(ip)           :: ielty,ielem,iface,inodb,ilist,kelem
  integer(ip)           :: inode,jelem,jface,jelty,ipoin,kelpo
  integer(ip)           :: pepoi,ielpo,dummi,pnodi,jnode, nelem_part_aux
  integer(ip)           :: melel,pelty,pnodj,pnodb
  integer(8)            :: msize
  logical(lg)           :: equal_faces
  type(hash_t)          :: ht
  integer(ip),  pointer :: faces(:,:,:) 
  integer(ip),  pointer :: lista(:)     
  
  !write(*,*) 'ANTES CONNPO'
  !flush(6)
  
  nullify(faces)
  nullify(lista)

  call connpo_part()
  
  !write(*,*) 'DESPUES CONNPO'
  !flush(6)
    
  !
  ! Allocate memory for PELEL
  !
  call memory_alloca(memor_dom,'PELEL','memgeo' , pelel , nelem_part +1_ip )

  

  !-------------------------------------------------------------------
  !
  ! Graph is based on common faces
  !
  !-------------------------------------------------------------------

  !
  ! Faces graph: Allocate memory for FACES
  !
  
   !
   ! faces_part graph: Allocate memory for faces_part
   !
   call memory_alloca(memor_par,'faces_part','par_elmgra',faces_part,mnodb+1,mface,nelem_part + nelemRecv)
  
   !
   ! Construct and sort faces_part
   do ielem = 1,nelem_part + nelemRecv                                        
      !obtain the elem type
      if (ielem <= nelem_part) then
         !Es elemento que me tocaba a mi
         ielty = abs(ltype_part(ielem))
       else
         !Es elemento que he recibido de otro slave, miro cual eara
         ielty = abs(ltype_recv(ielem - nelem_part))
      end if
      do iface = 1,element_type(ielty) % number_faces
         pnodb = element_type(ielty) % node_faces(iface)
         do inodb = 1,pnodb
            inode = element_type(ielty) % list_faces(inodb,iface)
            faces_part(inodb,iface,ielem) = lnods_local(inode,ielem)
         end do
         call sortin(pnodb,faces_part(1,iface,ielem))
      end do
   end do
   
   
     !
     ! Compute FACES
     !
     nedge = 0
     !write(*,*) 'NEDGE ANTES:' , nedge
     !flush(6)
     do ielem = 1,nelem_part + nelemRecv                         ! Compare the faces and 
        !obtain the elem type
        if (ielem <= nelem_part) then
          !Es elemento que me tocaba a mi
          ielty = abs(ltype_part(ielem))
        else
          !Es elemento que he recibido de otro slave, miro cual eara
          ielty = abs(ltype_recv(ielem - nelem_part))
        end if
        do iface=1,element_type(ielty) % number_faces
           ipoin = faces_part(1,iface,ielem)

           if( ipoin /= 0 ) then
              pnodi = element_type(ielty) % node_faces(iface)
              ilist = 1
              pepoi = pelpo(ipoin+1)-pelpo(ipoin)
              ielpo = pelpo(ipoin)-1
              do while( ilist <= pepoi )
                 ielpo = ielpo+1
                 jelem = lelpo(ielpo)
                 if( jelem /= ielem ) then
                    ! eliminate the repited faces
                    !obtain the elem type
                    if (jelem <= nelem_part) then
                      !Es elemento que me tocaba a mi
                      jelty = abs(ltype_part(jelem))
                    else
                      !Es elemento que he recibido de otro slave, miro cual eara
                      jelty = abs(ltype_recv(jelem - nelem_part))
                    end if
                    jface = 0
                    do while( jface /= element_type(jelty) % number_faces )
                       jface = jface + 1
                       if( faces_part(1,jface,jelem) /= 0 ) then
                          equal_faces = .true.
                          inodb       = 0
                          pnodj       = element_type(jelty) % node_faces(jface)                          
                          do while( equal_faces .and. inodb /= pnodj )
                             inodb = inodb + 1
                             if( faces_part(inodb,iface,ielem) /= faces_part(inodb,jface,jelem) ) &
                                  equal_faces = .false.
                          end do
                          if( equal_faces ) then
                             faces_part(1,iface,ielem) = 0                         ! IFACE and JFACE
                             faces_part(1,jface,jelem) = 0                         ! are eliminated
                             faces_part(pnodj+1,iface,ielem) = jelem               ! IFACE and JFACE
                             faces_part(pnodi+1,jface,jelem) = ielem               ! are eliminated
                             nedge = nedge + 2
                             jface = element_type(jelty) % number_faces            ! Exit JFACE do
                             ilist = pepoi                                         ! Exit JELEM do
                          end if
                       end if
                    end do
                 end if
                 ilist = ilist + 1
              end do
           end if
        end do
     end do
     !write(*,*) 'NEDGE DESPUÉS:' , nedge
     !flush(6)
     !
     ! Allocate memoty for adjacancies LELEL
     !
     call memgeo(43_ip)
     !nedge_par = pelel(nelem_part + 1_ip) - 1
     !write()
     !call memory_alloca(memor_dom,'LELEL','memgeo' , lelel , nedge_par )
     !
     ! Compute PELEL and LELEL
     !
     dummi    = 0
     pelel(1) = 1
     !
     !--*OMP  PARALLEL DO SCHEDULE (GUIDED)      & 
     !--*OMP  DEFAULT (NONE)                     &
     !--*OMP  PRIVATE (dummi,ielem,ielty,iface)  &
     !--*OMP  SHARED  (pelel,ltype,faces,lelel,nelem) 
     !
     do ielem = 1,nelem_part
        !obtain the elem type
        if (ielem <= nelem_part) then
          !Es elemento que me tocaba a mi
          ielty = abs(ltype_part(ielem))
        else
          !Es elemento que he recibido de otro slave, miro cual eara
          ielty = abs(ltype_recv(ielem - nelem_part))
        end if
        do iface = 1,element_type(ielty) % number_faces
           if( faces_part(1,iface,ielem) == 0 ) then
              pnodi          = element_type(ielty) % node_faces(iface)
              dummi          = dummi + 1
              pelel(ielem+1) = pelel(ielem+1) + 1
              lelel(dummi)   = faces_part(pnodi+1,iface,ielem)
           end if
        end do
        pelel(ielem+1) = pelel(ielem) + pelel(ielem+1)
     end do
     !
     ! Deallocate memory of FACES
     !
     call memory_deallo(memor_dom,'FACES','par_elmgra',faces_part)

  
     !
     ! Maximum number of edges in the mesh
     !
     medge=0
     do ielem=1,nelem_part
        if(pelel(ielem+1)-pelel(ielem)>medge) then
           medge=pelel(ielem+1)-pelel(ielem)
           dummi=ielem
        end if
     end do

     nedge_par = pelel(nelem_part + 1_ip) - 1
     nelem_part_aux = nelem / kfl_paral_parmes
     !write(*,*) 'NELEM_PART_AUX:' , nelem_part_aux, 'NELEM_PART:', nelem_part   
     do ielem=1,nedge_par
       jelem = lelel(ielem)
       !convertir jelem local a elemento global
       if (jelem <= nelem_part) then
         !Es elemento que me toca a mi, simple mapeo
         jelem = jelem + (nelem_part_aux * iproc_part_par)
       else
         !Es elemento que he recibido de otro slave, miro cual eara
         jelem = elem_loc_recv(jelem - nelem_part);
         !write(*,*) 'IPROC:' , iproc_part_par , 'ANTES CONVERSION:', lelel(ielem) , 'DESPUES CONVERSION:', jelem
         !flush(6)
       end if
       lelel(ielem) = jelem
     end do     
     padja_par => pelel
     ladja_par => lelel(1:nedge_par)
     
     call memory_deallo(memor_dom,'FACES','par_elmgra',pelpo)
     call memory_deallo(memor_dom,'FACES','par_elmgra',lelpo)
     mpopo = 0
     
end subroutine par_elmgra_part
