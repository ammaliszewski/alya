subroutine par_inidat()
  !------------------------------------------------------------------------
  !****f* Parall/par_inidat
  ! NAME
  !    par_inidat
  ! DESCRIPTION
  !    Send initial data to slaves using MPI
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_parall
  use def_inpout
  implicit none
  integer(ip), target :: kfl_varia_tmp(7)
  integer(ip)         :: kfl_ptask_tmp

  if( ISEQUEN .and. kfl_ptask == 0 ) then
     !
     ! Enable Master to perform preprocess
     !
     kfl_paral=0
     call vocabu(-1_ip,0_ip,0_ip) 

  else if( ( IMASTER .and. kfl_ptask/=0 ) .or. ISLAVE ) then
     !
     ! Exchange type of restart file (ASCII/binary), file hierarchy
     ! and file prefix
     !
     npari            =  7
     kfl_ptask_tmp    =  kfl_ptask
     kfl_ptask        =  1
     call vocabu(-1_ip,0_ip,0_ip) 
     if( IMASTER .and. kfl_ptask /= 0 ) kfl_varia_tmp(1) =  kfl_ascii_par
     kfl_varia_tmp(2) =  kfl_fileh_par
     kfl_varia_tmp(3) =  kfl_prefi_par
     kfl_varia_tmp(4) =  nsire_par
     kfl_varia_tmp(5) =  kfl_outpu
     kfl_varia_tmp(6) =  kfl_async
     kfl_varia_tmp(7) =  npart
     parin            => kfl_varia_tmp
     nparc            =  150
     parch(1:150)     =  wpref_par(1:150)
     call par_broadc()
     kfl_ascii_par    =  kfl_varia_tmp(1)
     kfl_fileh_par    =  kfl_varia_tmp(2)
     kfl_prefi_par    =  kfl_varia_tmp(3)
     nsire_par        =  kfl_varia_tmp(4)
     kfl_outpu        =  kfl_varia_tmp(5)  
     kfl_async        =  kfl_varia_tmp(6) 
     npart            =  kfl_varia_tmp(7) 
     wpref_par        =  parch
     kfl_ptask        =  kfl_ptask_tmp
     call vocabu(-1_ip,0_ip,0_ip) 
  end if

end subroutine par_inidat
 
