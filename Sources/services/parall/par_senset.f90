subroutine par_senset()
  !------------------------------------------------------------------------
  !****f* Parall/par_senset
  ! NAME
  !    par_senset
  ! DESCRIPTION
  !    Send/receive sets and witness points
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use      def_kintyp
  use      def_parame
  use      def_parall
  use      def_domain
  use      def_master
  use      mod_memchk
  implicit none  
  integer(ip)          :: jpoin,ipoin,inset,domai,ii,kpoin
  integer(ip)          :: knset,jelem,jboun,iboun,ielem
  integer(4)           :: istat
  integer(ip), pointer :: lnsec_loc(:)
  integer(ip), target  :: nnset_loc(1)

  if( IMASTER ) then
     
     if( neset > 0 ) then

        !----------------------------------------------------------------
        !
        ! Element sets
        !
        !----------------------------------------------------------------

        call memgen(1_ip,nelem,0_ip)
        do ielem = 1,nelem
           jelem        = leper_par(ielem)
           gisca(jelem) = leset(ielem)
        end do
        do kfl_desti_par = 1,npart_par
           npari =  neset
           parin => lesec
           strin =  'LESEC'
           call par_sendin()
           npari =  nelem_par(kfl_desti_par)
           parin => gisca(leind_par(kfl_desti_par):)
           strin =  'GISCA'
           call par_sendin()
        end do
        call memgen(3_ip,nelem,0_ip)

     end if

     if( nbset > 0 ) then

        !----------------------------------------------------------------
        !
        ! Boundary sets
        !
        !----------------------------------------------------------------

        call memgen(1_ip,nboun,0_ip)
        do iboun = 1,nboun
           jboun        = lbper_par(iboun)
           gisca(jboun) = lbset(iboun)
        end do
        do kfl_desti_par = 1,npart_par
           npari =  nbset
           parin => lbsec
           strin =  'LBSEC'
           call par_sendin()
           npari =  nboun_par(kfl_desti_par)
           parin => gisca(lbind_par(kfl_desti_par):)
           strin =  'LBSET'
           call par_sendin()
        end do
        call memgen(3_ip,nboun,0_ip)

     end if

     if( nnset > 0 ) then

        !----------------------------------------------------------------
        !
        ! Node sets
        !
        !----------------------------------------------------------------

        call par_memset(1_ip)
        call memgen(1_ip,nnset,0_ip)
        allocate(lnsec_loc(nnset),stat=istat)
        call memchk(zero,istat,mem_servi(1:2,servi),'LNSEC_LOC','par_senset',lnsec_loc)
        !
        ! Transform global numbering KPOIN to local numbering JPOIN
        !
        do inset = 1,nnset
           kpoin = lnsec(inset)
           ipoin = lnper_par(kpoin)
           domai = abs(lnpar_par(kpoin))
           if( ipoin <= gni ) then 
              jpoin = ipoin-ginde_par(1,domai)+1
           else  
              ipoin = ipoin-gni
              ii    = badj(ipoin)
              do while( bdom(ii) /= domai )
                 ii = ii+1
              end do
              jpoin = bpoin(ii)
           end if
           gisca(inset) = jpoin
           lnsec_par(inset,1) = domai
        end do
        !
        ! Send LNSEC_LOC
        !
        do domai = 1,npart_par
           knset = 0
           do inset = 1,nnset
              if( lnsec_par(inset,1) == domai ) then
                 knset              = knset+1
                 lnsec_loc(knset)   = gisca(inset)
                 nnset_par(domai)   = nnset_par(domai)+1
                 lnsec_par(inset,2) = knset
              end if
           end do
           kfl_desti_par = domai
           npari =  1
           parin => nnset_par(domai:domai)
           strin =  'NNSET_PAR'
           call par_sendin()        
           if( nnset_par(domai) > 0 ) then
              npari =  nnset_par(domai)
              parin => lnsec_loc
              strin =  'LNSEC_LOC'
              call par_sendin()
           end if
        end do
        !
        ! Deallocate memory
        !
        call memchk(two,istat,mem_servi(1:2,servi),'LNSEC_LOC','par_senset',lnsec_loc)
        deallocate(lnsec_loc,stat=istat)
        if(istat/=0) call memerr(two,'LNSEC_LOC','par_senset',0_ip)
        call memgen(3_ip,nnset,0_ip)

     end if
     
  else if( ISLAVE ) then

     kfl_desti_par = 0

     if( neset > 0 ) then
        !
        ! Element sets
        !
        call memose(1_ip)
        call memose(5_ip)
        npari =  neset
        parin => lesec(:)
        call par_receiv()   
        npari =  nelem
        parin => leset(:)
        call par_receiv()
     end if

     if( nbset > 0 ) then
        !
        ! Boundary sets
        !
        call memose(2_ip)
        call memose(4_ip)
        npari =  nbset
        parin => lbsec(:)
        call par_receiv()        
        npari =  nboun
        parin => lbset(:)
        call par_receiv()
     end if

     if( nnset > 0 ) then
        !
        ! Node sets 
        !     
        npari =  1
        parin => nnset_loc
        call par_receiv()
        nnset =  nnset_loc(1)
        if( nnset > 0) then
           !
           ! Receive LNSEC
           !
           call memose(3_ip)
           npari =  nnset
           parin => lnsec(:)
           call par_receiv()
        end if
     end if

  end if

end subroutine par_senset
