subroutine par_locnum(iglob,iloca)
  !-----------------------------------------------------------------------
  !****f* parall/par_locnum
  ! NAME 
  !    par_locnum
  ! DESCRIPTION
  !    Find the local number ILOCA of a global node IGLOB in all slaves 
  !    If the node does not belong to the slave, return 0.
  ! USES
  !
  ! USED BY
  !
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_parall
  use def_domain
  implicit none
#ifdef MPI_OFF
#else
  include  'mpif.h'
#endif
  integer(ip), intent(in)  :: iglob
  integer(ip), intent(out) :: iloca
  integer(ip)              :: index,jpoin,ipart,ipoin

  if(IMASTER.and.kfl_ptask/=2) then
     call memgen(1_ip,npart_par+1,0_ip)
     gisca(1)=iglob
     index=0
     do ipart=1,npart_par
        ipoin=0
        do while(ipoin<npoin_par(ipart))
           ipoin=ipoin+1
           jpoin=ipoin+index
           if(lninv_loc(jpoin)==iglob) then
              gisca(ipart+1)=ipoin
              ipoin=npoin_par(ipart)
           end if
        end do
        index=index+npoin_par(ipart)
     end do
  end if

  strin = 'PAR_LOCNUM'
  parin => gisca
  npari =  1
  call par_scatt2(iloca)

  if(IMASTER.and.kfl_ptask/=2) call memgen(3_ip,npart_par+1,0_ip)

end subroutine par_locnum
