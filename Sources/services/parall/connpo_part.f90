subroutine connpo_part()
  !-----------------------------------------------------------------------
  !****f* Domain/connpo
  ! NAME
  !    connpo
  ! DESCRIPTION
  !    This routine compute the node/element connectivity arrays.
  ! OUTPUT
  !    NEPOI(NPOIN) ............ # of elements connected to nodes
  !    PELPO(NPOIN+1) .......... Pointer to list of element LELPO
  !    LELPO(PELPO(NPOIN+1)) ... List of elements connected to nodes
  ! USED BY
  !    domgra
  !    par_partit
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use mod_memchk
  use def_elmtyp
  use mod_memory
  use mod_par_partit_paral, only:npoin_local, npoin_local_recv, nelem_part, nelemRecv, lnods_local
  use mod_par_partit_paral, only:ltype_part, memor_par, ltype_recv  
  implicit none
  integer(ip) :: inode,ipoin,ielem,nlelp,pelty
  integer(4)  :: istat
  
  integer(ip), pointer     :: lnnod_local(:)      ! Element number of nodes
  integer(ip), pointer     :: nepoi_part(:)      ! Element number of nodes
  
  !call elmtyp()
  !write(*,*) 'ANTES CONNPO11:' , nnode
  !flush(6)
   !
   ! LNNOD_LOCAL
   !
   !write(*,*) 'antes allocate:' , nelem_part + nelemRecv
   !flush(6)
   allocate(lnnod_local(nelem_part + nelemRecv))
   !call memchk(zero,istat,memor_par,'lnood_local_part','connpo_part',lnnod_local)

   !write(*,*) 'NNODE PELTY:' , pelty
   !flush(6)
   do ielem = 1,nelem_part + nelemRecv
      !obtain the elem type
      if (ielem <= nelem_part) then
         !Es elemento que me tocaba a mi
         pelty = ltype_part(ielem)
       else
         !Es elemento que he recibido de otro slave, miro cual eara
         pelty = ltype_recv(ielem - nelem_part)
      end if
      lnnod_local(ielem) = nnode(pelty)
   end do
  
    !write(*,*) 'ANTES CONNPO2'
    !flush(6)
   
  !
  ! Allocate memory for NEPOI and compute it
  !
  allocate(nepoi(npoin_local + npoin_local_recv),stat = istat)
  call memchk(zero,istat,memor_dom,'NEPOI','connpo',nepoi)
  
   do ielem = 1,nelem_part + nelemRecv
     do inode = 1,lnnod_local(ielem)
        ipoin = lnods_local(inode,ielem)
        nepoi(ipoin) = nepoi(ipoin) + 1
     end do
  end do
  
    !write(*,*) 'ANTES CONNPO3'
    !flush(6)
  
  !
  ! Allocate memory for PELPO and compute it
  !
  allocate(pelpo(npoin_local + npoin_local_recv +1),stat = istat)
  call memchk(zero,istat,memor_dom,'PELPO','connpo',pelpo)
  pelpo(1) = 1
  do ipoin = 1,npoin_local + npoin_local_recv
     pelpo(ipoin+1) = pelpo(ipoin) + nepoi(ipoin)
  end do
  
   !write(*,*) 'ANTES CONNPO4'
   !flush(6)
  
  !
  ! Allocate memory for LELPO and construct the list
  !
  nlelp = pelpo(npoin_local + npoin_local_recv +1)
  allocate(lelpo(nlelp),stat = istat)
  call memchk(zero,istat,memor_dom,'LELPO','connpo',lelpo)
  mpopo = 0
  do ielem = 1,nelem_part + nelemRecv
     mpopo  =  mpopo + lnnod_local(ielem) * lnnod_local(ielem)
     do inode = 1,lnnod_local(ielem)
        ipoin = lnods_local(inode,ielem)
        lelpo(pelpo(ipoin)) = ielem
        pelpo(ipoin) = pelpo(ipoin)+1
     end do
  end do
  
    !write(*,*) 'ANTES CONNPO5'
    !flush(6)
  
  !
  ! Recompute PELPO and maximum number of element neighbors MEPOI
  !
  pelpo(1) = 1
  mepoi = -1
  do ipoin = 1,npoin_local + npoin_local_recv
     pelpo(ipoin+1) = pelpo(ipoin)+nepoi(ipoin)
     mepoi = max(mepoi,nepoi(ipoin))
  end do
  
    !write(*,*) 'ANTES CONNPO6'
    !flush(6)
  !
  ! Deallocate memory for temporary node/element connectivity
  !
  call memchk(two,istat,memor_dom,'NEPOI','connpo',nepoi)
  deallocate(nepoi,stat = istat)
  if(istat /= 0 ) call memerr(two,'NEPOI','connpo',0_ip)
  call memory_deallo(memor_par,'FACES','par_elmgra',lnnod_local)

end subroutine connpo_part
