subroutine par_bougro()
  !-------------------------------------------------------------------------------
  !****f* Parall/par_bougro
  ! NAME
  !    par_bougro
  ! DESCRIPTION
  !    This routine distributes the boundary points between adjacent subdomains
  !    When entering, LNPAR_PAR=0 for boundary nodes
  !                            =subdomain #
  ! INPUT
  !    Element graph
  ! OUTPUT
  !
  ! USED BY
  !    par_create_domain_graph
  !***
  !-------------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_parall
  use mod_memchk
  implicit none
  integer(ip), allocatable :: indice_dom(:), xfrontera(:)
  integer(ip), allocatable :: i_front(:), d_front(:)
  integer(ip), allocatable :: permI(:),invpI(:) 
  integer(ip), allocatable :: permB(:),invpB(:), xadjSubDom(:)
  integer(ip), allocatable :: adjSubDom(:), lnpar_loc(:)
  integer(ip)              :: wvert_loc, wedge_loc, weigh_loc
  integer(ip)              :: nbNodInter, dummi
  integer(ip)              :: ii, jj, kk, dom1, dom2, n_nei, n_nei2
  integer(ip)              :: igrou,  mascara, maxb,  ngrou
  integer(4)               :: istat

  allocate(indice_dom(npart_par),stat=istat)
  call memchk(0_ip,istat,mem_servi(1:2,servi),'indice_dom','par_bougro',indice_dom)

  allocate(xfrontera(npart_par+1),stat=istat)
  call memchk(0_ip,istat,mem_servi(1:2,servi),'xfrontera','par_bougro',xfrontera)

  ngrou = comle(icoml)%ngrou

  do dom1 = 1, npart_par
     indice_dom(dom1) = 0
  enddo

  maxb = 0
  do dom1 = 1, npart_par 
     do dom2 = 1, dom1-1
        kk    = (dom1*(dom1-1))/2 + dom2
        n_nei = comle(icoml)%neighDom(kk)
        if ( n_nei >  maxb ) maxb = n_nei
        if ( n_nei /= 0 ) then
           indice_dom(dom1) = indice_dom(dom1) + n_nei
        endif
     enddo
  enddo

  xfrontera(1) = 1
  do dom1 = 1, npart_par
     xfrontera(dom1+1) = xfrontera(dom1) + indice_dom(dom1)
  enddo

  allocate(i_front(xfrontera(npart_par+1)),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'i_front','par_bougro',i_front)

  allocate(d_front(xfrontera(npart_par+1)),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'d_front','par_bougro',d_front)

  do igrou = 1, ngrou

     if ( lnpar_par(igrou) == 0 ) then
        !
        ! IGROU is a boundary node
        ! 
        do ii = 1, comle(icoml)%ndomi(igrou)
           dom1 = comle(icoml)%domli(ii,igrou)
           do jj = ii+1, comle(icoml)%ndomi(igrou)
              dom2 = comle(icoml)%domli(jj,igrou)
              if ( dom2 > dom1 ) then
                 kk              = xfrontera(dom2)
                 d_front(kk)     = dom1
                 i_front(kk)     = igrou
                 xfrontera(dom2) = kk + 1
              else
                 kk              = xfrontera(dom1)
                 d_front(kk)     = dom2
                 i_front(kk)     = igrou
                 xfrontera(dom1) = kk + 1
              endif
           enddo
        enddo
     endif
  enddo

  xfrontera(1) = 1
  do dom1 = 1, npart_par
     xfrontera(dom1+1) = xfrontera(dom1) + indice_dom(dom1)
  enddo

  allocate(lnpar_loc(ngrou),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'lnpar_loc','par_bougro',lnpar_loc)

  allocate(permI(ngrou),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'permI','par_bougro',permI)

  allocate(invpI(maxb),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'invpI','par_bougro',invpI)

  allocate(permB(ngrou),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'permB','par_bougro',permB)

  allocate(invpB(maxb),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'invpB','par_bougro',invpB)

  allocate(xadjSubDom(maxb+1),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'xadjSubDom','par_bougro',xadjSubDom)

  maxb = (r_dom(ngrou+1)/ngrou)*maxb*2
  allocate( adjSubDom(maxb),stat=istat)
  call memchk(zero,istat,mem_servi(1:2,servi),'adjSubDom','par_bougro',adjSubDom)
  !
  ! Partition graph using METIS
  !
  weigh_loc = 0
  wvert_loc = 0 
  wedge_loc = 0
  mascara = npart_par+1

  do dom1 = 1, npart_par
     do dom2 = 1, dom1-1
        kk = (dom1*(dom1-1))/2 + dom2
        n_nei = comle(icoml)%neighDom(kk)
        if (n_nei/=0) then
           n_nei2 = 0
           do ii = xfrontera(dom1), xfrontera(dom1+1)-1
              if (d_front(ii)==dom2) then
                 igrou = i_front(ii)
                 if ( comle(icoml)%lnpar_par(igrou) == 0 ) then
                    comle(icoml)%lnpar_par(igrou) = mascara
                    n_nei2                        = n_nei2 + 1
                 endif
              endif
           enddo
           if( n_nei2 > 0 ) then
              !   !
              !   !create a subgraph: NEED C_DOM AND R_DOM
              !   !
              !   call par_subgra( &
              !        ngrou , r_dom, c_dom, mascara, comle(icoml)%lnpar_par, &
              !        nbNodInter, nbNodBound, xadjSubDom, adjSubDom,  &
              !        permI, invpI, permB, invpB , adjsize )
              call par_metis(&
                   1_ip , nbNodInter , maxb, xadjSubDom, adjSubDom, &
                   wvert_loc, wedge_loc, weigh_loc,  &
                   2_ip , lnpar_loc , dummi, dummi, &
                   dummi, dummi, dummi, dummi, 1_ip , 1_ip , &
                   mem_servi(1:2,servi) ) 
              
              do ii= 1, nbNodInter
                 igrou = invpI(ii)
                 if (lnpar_loc(ii)==1) then
                    comle(icoml)%lnpar_par(igrou) = -dom1
                 else
                    comle(icoml)%lnpar_par(igrou) = -dom2
                 endif
              enddo
              mascara =  mascara + 1
           endif
        endif
     enddo
  enddo
  !
  ! Deallocate
  !
  call memchk(two,istat,mem_servi(1:2,servi),'xadjSubDom','par_bougro',xadjSubDom)
  deallocate(xadjSubDom,stat=istat)
  if(istat/=0) call memerr(two,'xadjSubDom','par_bougro',0_ip)

  call memchk(two,istat,mem_servi(1:2,servi),'adjSubDom','par_bougro',adjSubDom)
  deallocate(adjSubDom,stat=istat)
  if(istat/=0) call memerr(two,'adjSubDom','par_bougro',0_ip)

  call memchk(two,istat,mem_servi(1:2,servi),'permI','par_bougro',permI)
  deallocate(permI,stat=istat)
  if(istat/=0) call memerr(two,'permI','par_bougro',0_ip)

  call memchk(two,istat,mem_servi(1:2,servi),'invpI','par_bougro',invpI)
  deallocate(invpI,stat=istat)
  if(istat/=0) call memerr(two,'invpI','par_bougro',0_ip)

  call memchk(two,istat,mem_servi(1:2,servi),'permB','par_bougro',permB)
  deallocate(permB,stat=istat)
  if(istat/=0) call memerr(two,'permB','par_bougro',0_ip)

  call memchk(two,istat,mem_servi(1:2,servi),'invpB','par_bougro',invpB)
  deallocate(invpB,stat=istat)
  if(istat/=0) call memerr(two,'invpB','par_bougro',0_ip)

  call memchk(two,istat,mem_servi(1:2,servi),'lnpar_loc','par_bougro',lnpar_loc)
  deallocate(lnpar_loc,stat=istat)
  if(istat/=0) call memerr(two,'lnpar_loc','par_bougro',0_ip)

  call memchk( two, istat, mem_servi(1:2,servi), 'd_front','par_bougro', d_front)
  deallocate( d_front, stat=istat )
  if(istat/=0) call memerr( two, 'd_front', 'par_bougro',0_ip)

  call memchk( two, istat, mem_servi(1:2,servi), 'i_front','par_bougro', i_front)
  deallocate( i_front, stat=istat )
  if(istat/=0) call memerr( two, 'i_front', 'par_bougro',0_ip)

  call memchk( two, istat, mem_servi(1:2,servi), 'xfrontera','par_bougro', xfrontera)
  deallocate( xfrontera, stat=istat )
  if(istat/=0) call memerr( two, 'xfrontera', 'par_bougro',0_ip)

  call memchk( two, istat, mem_servi(1:2,servi), 'indice_dom','par_bougro', indice_dom)
  deallocate( indice_dom, stat=istat )
  if(istat/=0) call memerr( two, 'indice_dom', 'par_bougro',0_ip)

end subroutine par_bougro
