subroutine par_outinf()
  !------------------------------------------------------------------------
  !****f* Parall/par_outinf
  ! NAME
  !    par_outinf
  ! DESCRIPTION
  !    This routine output info of the parallelization
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_domain
  use def_parall
  use mod_parall
  implicit none  
  integer(ip) :: isubd,ksmin(2),ksmax(2),kaver,koutp,nb

  if( IMASTER ) then

     koutp =  0
     koutp = koutp + 1; ioutp(koutp) = npart_par
     koutp = koutp + 1; ioutp(koutp) = npoin_total
     !
     ! NPOIN
     ! 
     ksmin =  huge(1_ip)
     ksmax = -huge(1_ip)
     kaver =  0
     do isubd=1,npart_par
        if(npoin_par(isubd)<ksmin(1)) then
           ksmin(1)=npoin_par(isubd)
           ksmin(2)=isubd
        end if
        if(npoin_par(isubd)>ksmax(1)) then
           ksmax(1)=npoin_par(isubd)
           ksmax(2)=isubd
        end if
        kaver=kaver+npoin_par(isubd)
     end do
     kaver=kaver/npart_par
     koutp=koutp+1;ioutp(koutp)=ksmin(1)
     koutp=koutp+1;ioutp(koutp)=ksmin(2)
     koutp=koutp+1;ioutp(koutp)=ksmax(1)
     koutp=koutp+1;ioutp(koutp)=ksmax(2)
     koutp=koutp+1;ioutp(koutp)=kaver
     !
     ! NELEM
     ! 
     ksmin =  huge(1_ip)
     ksmax = -huge(1_ip)
     kaver =  0
     do isubd=1,npart_par
        if(nelem_par(isubd)<ksmin(1)) then
           ksmin(1)=nelem_par(isubd)
           ksmin(2)=isubd
        end if
        if(nelem_par(isubd)>ksmax(1)) then
           ksmax(1)=nelem_par(isubd)
           ksmax(2)=isubd
        end if
        kaver=kaver+nelem_par(isubd)
     end do
     kaver=kaver/npart_par
     koutp=koutp+1;ioutp(koutp)=ksmin(1)
     koutp=koutp+1;ioutp(koutp)=ksmin(2)
     koutp=koutp+1;ioutp(koutp)=ksmax(1)
     koutp=koutp+1;ioutp(koutp)=ksmax(2)
     koutp=koutp+1;ioutp(koutp)=kaver
     !
     ! NELEW with weight
     ! 
     ksmin =  huge(1_ip)
     ksmax = -huge(1_ip)
     kaver =  0
     do isubd=1,npart_par
        if(nelew_par(isubd)<ksmin(1)) then
           ksmin(1)=nelew_par(isubd)
           ksmin(2)=isubd
        end if
        if(nelew_par(isubd)>ksmax(1)) then
           ksmax(1)=nelew_par(isubd)
           ksmax(2)=isubd
        end if
        kaver=kaver+nelew_par(isubd)
     end do
     kaver=kaver/npart_par
     koutp=koutp+1;ioutp(koutp)=ksmin(1)
     koutp=koutp+1;ioutp(koutp)=ksmin(2)
     koutp=koutp+1;ioutp(koutp)=ksmax(1)
     koutp=koutp+1;ioutp(koutp)=ksmax(2)
     koutp=koutp+1;ioutp(koutp)=kaver
     !
     ! NBOUN
     ! 
     ksmin =  huge(1_ip)
     ksmax = -huge(1_ip)
     kaver =  0
     do isubd=1,npart_par
        if(nboun_par(isubd)<ksmin(1)) then
           ksmin(1)=nboun_par(isubd)
           ksmin(2)=isubd
        end if
        if(nboun_par(isubd)>ksmax(1)) then
           ksmax(1)=nboun_par(isubd)
           ksmax(2)=isubd
        end if
        kaver=kaver+nboun_par(isubd)
     end do
     kaver=kaver/npart_par
     koutp=koutp+1;ioutp(koutp)=ksmin(1)
     koutp=koutp+1;ioutp(koutp)=ksmin(2)
     koutp=koutp+1;ioutp(koutp)=ksmax(1)
     koutp=koutp+1;ioutp(koutp)=ksmax(2)
     koutp=koutp+1;ioutp(koutp)=kaver
     !
     ! NNEIG: number of neighbors
     !
     ksmin =  huge(1_ip)
     ksmax = -huge(1_ip)
     kaver = 0
     do isubd = 1,npart_par
        if(lneig_par(isubd)<ksmin(1)) then
           ksmin(1)=lneig_par(isubd) 
           ksmin(2)=isubd
        end if
        if(lneig_par(isubd)>ksmax(1)) then
           ksmax(1)=lneig_par(isubd)
           ksmax(2)=isubd
        end if
        kaver=kaver+lneig_par(isubd)
     end do
     kaver=kaver/npart_par
     koutp=koutp+1;ioutp(koutp)=ksmin(1)
     koutp=koutp+1;ioutp(koutp)=ksmin(2)
     koutp=koutp+1;ioutp(koutp)=ksmax(1)
     koutp=koutp+1;ioutp(koutp)=ksmax(2)
     koutp=koutp+1;ioutp(koutp)=kaver
     !
     ! NBBOU: Number of boundary nodes 
     !
     ksmin =  huge(1_ip)
     ksmax = -huge(1_ip)
     kaver = 0
     do isubd = 1,npart_par
        nb = npoin_par(isubd)-ginde_par(3,isubd)
        if(nb<ksmin(1)) then
           ksmin(1)=nb
           ksmin(2)=isubd
        end if
        if(nb>ksmax(1)) then
           ksmax(1)=nb
           ksmax(2)=isubd
        end if
        kaver=kaver+nb
     end do
     kaver=kaver/npart_par
     koutp=koutp+1;ioutp(koutp)=ksmin(1)
     koutp=koutp+1;ioutp(koutp)=ksmin(2)
     koutp=koutp+1;ioutp(koutp)=ksmax(1)
     koutp=koutp+1;ioutp(koutp)=ksmax(2)
     koutp=koutp+1;ioutp(koutp)=kaver

     call outfor(38_ip,lun_outpu_par,' ')
     call outfor(38_ip,lun_outpu,    ' ')

  end if

end subroutine par_outinf
