subroutine par_partit()
  !-------------------------------------------------------------------------------
  !****f* Parall/par_arrays
  ! NAME
  !    par_arrays
  ! DESCRIPTION
  !
  ! INPUT
  !    Element graph
  ! OUTPUT
  !    Partition of the graph
  !    lepar_par
  !    leper_par
  !    leinv_par
  !    lnpar_par
  !    lnper_par
  !    lninv_par
  !    lneig_par
  !    ginde_par 
  !    lcomm_par
  ! USED BY
  !    par_partit
  !***
  !-------------------------------------------------------------------------------
  use def_parame 
  use def_elmtyp
  use def_domain 
  use def_parall
  use def_master
  use mod_memory
  use mod_graphs
  use mod_parall
  implicit none
  integer(ip), pointer    :: wvert_par(:)
  integer(ip)             :: wedge_par(1)   
  integer(ip)             :: isign,ielty
  integer(ip)             :: dummi,ipart,ielem,kelem,inode,ipoin
  integer(ip)             :: nedge_tmp,kzone,izone,nelem_tmp
  integer(ip)             :: kpart,npart_cur
  integer(ip), pointer    :: ladja_tmp(:)
  integer(ip), pointer    :: padja_tmp(:)
  integer(ip), pointer    :: wvert_tmp(:)
  integer(ip), pointer    :: permr_tmp(:)
  integer(ip), pointer    :: invpr_tmp(:)
  integer(ip), pointer    :: npart_tmp(:)
  integer(ip), pointer    :: lepar_tmp(:)
  real(rp)                :: time0,time1,time2,dummr
  !
  ! Nullify pointers
  !
  nullify(ladja_tmp)
  nullify(padja_tmp)
  nullify(wvert_tmp)
  nullify(permr_tmp)
  nullify(invpr_tmp)
  nullify(npart_tmp) 
  nullify(lepar_tmp)
  nullify(wvert_par)

  !----------------------------------------------------------------------
  !
  ! Compute Vertex weights WVERT_PAR = # of Gauss points of the element
  !
  !----------------------------------------------------------------------
  !kfl_weigh_par = 5
  !print *, '## -->', kfl_weigh_par
  call par_livinf(20_ip,'  ALLOCATE MEMORY...',dummi) 
  call memory_alloca(mem_servi(1:2,servi),'WVERT_PAR','par_prepro',wvert_par,nelem)
  !
  ! If hole elements are taken off, one subdomain can end up with all the hole elements
  ! as they have null weight!
  ! 
  call par_livinf(20_ip,'  SET WEIGHTS... ',dummi)
  if( kfl_weigh_par == 0 ) then
     kfl_weigh_par = 2                  ! 0: No weights, 1: edges, 2: vertices, 3: both
     do ielem = 1,nelem
        ielty = abs(ltype(ielem))
        if( lelch(ielem) == ELHOL ) then
           !wvert_par(ielem) = 0 ! See comment above
           wvert_par(ielem) = ngaus(ielty) 
        else
           wvert_par(ielem) = ngaus(ielty)
        end if
     end do
   else if(kfl_weigh_par == 5) then
     print *, 'pts mesh partition'
     !kfl_weigh_par = 2
     do ielem = 1,nelem
       ielty = abs(ltype(ielem))
       dummr = 0.0_rp
       print *, '000', lnnod(ielem)
       do inode = 1,lnnod(ielem)
          ipoin = lnods(inode,ielem)
          dummr = dummr + xfiel(1) % a(1,ipoin)
       end do
       dummr = dummr / real(lnnod(ielem),rp)
       wvert_par(ielem) = int((100-1)*(-1/(dummr+1)**10+1)+1,ip)
     end do 
  else
     kfl_weigh_par = 0                  ! 0: No weights, 1: edges, 2: vertices, 3: both
     do ielem = 1,nelem
        ielty = abs(ltype(ielem))
        wvert_par(ielem) = 1_ip
     end do
  end if
  wedge_par = 0

  !----------------------------------------------------------------------
  !
  ! Compute element graph: PADJA_PAR (PELEL) and LADJA_PAR (LELEL)
  ! using node or face connectivity
  !
  !----------------------------------------------------------------------

  call cputim(time0)
  call par_memory(1_ip)
  call par_memory(8_ip)
  call par_livinf(2_ip,' ',dummi)

  call par_elmgra()

  call par_livinf(7_ip,' ',0_ip)       
  call cputim(time1)
  cpu_paral(3) = time1 - time0

  !----------------------------------------------------------------------
  !
  ! Partition mesh with METIS
  !
  !----------------------------------------------------------------------

  call par_livinf(3_ip,' ',dummi)

  if( nzone_par == 1 ) then
     !
     ! Only one zone
     !
     call par_metis(                                             &
          1_ip , nelem, nedge , padja_par, ladja_par, wvert_par, &
          wedge_par, kfl_weigh_par, npart_par,                   &
          lepar_par ,dummi , dummi , dummi , dummi ,             & 
          dummi, dummi , nelem , 1_ip , mem_servi(1,servi)       )

  else
     !
     ! Multiple zones
     !
     call memory_alloca(mem_servi(1:2,servi),'NPART_TMP','par_arrays',npart_tmp,nzone_par)
     nelem_tmp = 0
     do izone = 1,nzone_par
        nelem_tmp = nelem_tmp + nelez(izone)
     end do

     if( nelem_tmp /= nelem ) then
        write(*,*) nelem,nelem_tmp
        call runend('PAR_ARRAYS: WRONG ZONAL-WISE PARTITION')
     end if

     npart_cur = 0
     do kzone = 1,nzone_par
        npart_tmp(kzone) = max(1_ip,int(real(nelez(kzone)* npart_par,rp) / real(nelem,rp) ,ip))
        npart_cur = npart_cur + npart_tmp(kzone)
     end do
     do while( npart_cur /= npart_par )

        if( npart_cur > npart_par ) then
           isign =  1
           dummi =  0
        else
           isign = -1
           dummi = -huge(1_ip)
        end if
        izone = 0
        do kzone = 1,nzone_par
           if( isign * npart_tmp(kzone) > dummi ) then
              dummi = npart_tmp(kzone)
              izone = kzone
           end if
        end do
        npart_tmp(izone) = npart_tmp(izone) - isign
        npart_cur = npart_cur - isign
     end do

     call outfor(63_ip,lun_outpu_par,' ')
     do kzone = 1,nzone_par
        ioutp(1) = lzone_par(kzone)
        ioutp(2) = npart_tmp(kzone)   
        call outfor(64_ip,lun_outpu_par,' ')
     end do

     call memory_alloca(mem_servi(1:2,servi),'PADJA_TMP','par_arrays',padja_tmp,size(padja_par,kind=ip))
     call memory_alloca(mem_servi(1:2,servi),'LADJA_TMP','par_arrays',ladja_tmp,size(ladja_par,kind=ip))
     call memory_alloca(mem_servi(1:2,servi),'PERMR_TMP','par_arrays',permr_tmp,nelem)
     call memory_alloca(mem_servi(1:2,servi),'INVPR_TMP','par_arrays',invpr_tmp,nelem)

     kpart = 0

     do kzone = 1,nzone_par

        izone = lzone_par(kzone)

        if( npart_tmp(kzone) == 1 ) then

           do kelem = 1,nelez(izone)
              ielem            = lelez(izone) % l(kelem)
              lepar_par(ielem) = 1 + kpart
           end do

        else

           call memory_alloca(mem_servi(1:2,servi),'WVERT_TMP','par_arrays',wvert_tmp,nelez(izone))
           call memory_alloca(mem_servi(1:2,servi),'LEPAR_TMP','par_arrays',lepar_tmp,nelez(izone))

           do ielem = 1,nelem+1
              padja_tmp(ielem) = padja_par(ielem)
           end do
           do ielem = 1,size(ladja_par)              
              ladja_tmp(ielem) = ladja_par(ielem)
           end do

           do kelem = 1,nelez(izone)
              ielem            = lelez(izone) % l(kelem)
              permr_tmp(kelem) = ielem
              invpr_tmp(ielem) = kelem
              wvert_tmp(kelem) = wvert_par(ielem)
           end do

           nedge_tmp = nedge
           call graphs_subgra(nelem,nedge_tmp,permr_tmp,invpr_tmp,padja_tmp,ladja_tmp)

           call par_metis(                                                        &
                1_ip , nelez(izone), nedge_tmp , padja_tmp, ladja_tmp, wvert_tmp, &
                wedge_par, kfl_weigh_par, npart_tmp(kzone),                       &
                lepar_tmp ,dummi , dummi , dummi , dummi ,                        & 
                dummi, dummi , nelem , 1_ip , mem_servi(1,servi)                  )

           do kelem = 1,nelez(izone)
              ielem            = lelez(izone) % l(kelem)
              lepar_par(ielem) = lepar_tmp(kelem) + kpart
           end do

           call memory_deallo(mem_servi(1:2,servi),'LEPAR_TMP','par_arrays',lepar_tmp)
           call memory_deallo(mem_servi(1:2,servi),'WVERT_TMP','par_arrays',wvert_tmp)

           do ielem = 1,nelem
              permr_tmp(ielem) = 0
              invpr_tmp(ielem) = 0
           end do

        end if
        do ipart = 1,npart_tmp(kzone)
           lsubz_par(ipart+kpart) = izone
        end do
        kpart = kpart + npart_tmp(kzone)

     end do

     call memory_deallo(mem_servi(1:2,servi),'NPART_TMP','par_arrays',npart_tmp)
     call memory_deallo(mem_servi(1:2,servi),'INVPR_TMP','par_arrays',invpr_tmp)
     call memory_deallo(mem_servi(1:2,servi),'PERMR_TMP','par_arrays',permr_tmp)
     call memory_deallo(mem_servi(1:2,servi),'PADJA_TMP','par_arrays',padja_tmp)
     call memory_deallo(mem_servi(1:2,servi),'LADJA_TMP','par_arrays',ladja_tmp)

     do ielem = 1,nelem
        if( lepar_par(ielem) == 0 ) call runend('PAR_ARRAYS: ELEMENTS ARE MISSING')
     end do

  end if

  call cputim(time2)
  cpu_paral(5) = time2 - time1

  !----------------------------------------------------------------------
  !
  ! Compute some statistics
  !
  !----------------------------------------------------------------------

  do ielem = 1,nelem
     ipart = lepar_par(ielem)
     nelew_par(ipart) = nelew_par(ipart) + wvert_par(ielem)
  end do

  !----------------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !----------------------------------------------------------------------

  call memory_deallo(mem_servi(1:2,servi),'WVERT_PAR','par_prepro_D',wvert_par)

end subroutine par_partit
