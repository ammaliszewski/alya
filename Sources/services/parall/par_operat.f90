subroutine par_operat(itask)
  !------------------------------------------------------------------------
  !****f* Parall/par_operat
  ! NAME
  !    par_operat
  ! DESCRIPTION
  !    This routine operates between slave arrays and broadcast the result
  !    to all slaves and master
  !    ITASK=1 ... Minimum 
  !    ITASK=2 ... Maximum
  !    ITASK=3 ... Sum
  ! OUTPUT
  !    NPARI ..... Integer array
  !    NPARR ..... Real array
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_parall
  use def_master
  use mod_parall, only : PAR_INTEGER
  use mod_parall, only : PAR_COMM_MY_CODE4
  implicit none
#ifdef MPI_OFF
#else
  include  'mpif.h'
#endif
  integer(ip), intent(in) :: itask
  integer(ip)             :: ii 
  integer(4)              :: istat=0,npari4,nparr4,nparx4
  real(rp)                :: time1,time2
  integer(ip), pointer    :: iwa(:)
  real(rp),    pointer    :: rwa(:)
  complex(rp), pointer    :: cwa(:)
  !
  ! Operations on real arrays
  !

  if(nparr>0) then 

#ifdef MPI_OFF
#else
     call cputim(time1)     
     allocate(rwa(nparr), stat=istat)
     if(ISLAVE) then
        do ii=1,nparr
           rwa(ii)=parre(ii)
        end do
     end if
     nparr4=int(nparr,4)
     if(itask==1) then
        !
        ! Minimum
        !
        if(IMASTER) then
           do ii=1,nparr
              rwa(ii)=1e9
           end do
        end if
        call MPI_AllReduce(rwa,parre,nparr4,MPI_DOUBLE_PRECISION,MPI_MIN,PAR_COMM_MY_CODE4,istat) 
     else if(itask==2) then
        !
        ! Maximum
        !
        if(IMASTER) then
           do ii=1,nparr
              rwa(ii)=-1e9
           end do
        end if
        call MPI_AllReduce(rwa,parre,nparr4,MPI_DOUBLE_PRECISION,MPI_MAX,PAR_COMM_MY_CODE4,istat)        
     else if(itask==3) then
        !
        ! Sum
        !
        if(IMASTER) then
           do ii=1,nparr
              rwa(ii)=0.0_rp
           end do
        end if
        call MPI_AllReduce(rwa,parre,nparr4,MPI_DOUBLE_PRECISION,MPI_SUM,PAR_COMM_MY_CODE4,istat)        
     end if
     deallocate(rwa,stat=istat)
     call cputim(time2)
     cpu_paral(22)=cpu_paral(22)+time2-time1
     nparr=0
#endif

  end if
  !
  ! Operations on integer arrays
  !
  if(npari>0) then 

#ifdef MPI_OFF
#else
     call cputim(time1)     
     allocate(iwa(npari), stat=istat)
     if(ISLAVE) then
        do ii=1,npari
           iwa(ii)=parin(ii)
        end do
     end if
     npari4=int(npari,4)
     if(itask==1) then
        !
        ! Minimum
        !
        if(IMASTER) then
           do ii=1,npari
              iwa(ii)=1e9
           end do
        end if
        call MPI_AllReduce(iwa,parin,npari4,PAR_INTEGER,MPI_MIN,PAR_COMM_MY_CODE4,istat) 

     else if(itask==2) then
        !
        ! Maximum
        !
        if(IMASTER) then
           do ii=1,npari
              iwa(ii)=-1e9
           end do
        end if
        call MPI_AllReduce(iwa,parin,npari4,PAR_INTEGER,MPI_MAX,PAR_COMM_MY_CODE4,istat)  

     else if(itask==3) then
        !
        ! Sum
        !
        if(IMASTER) then
           do ii=1,npari
              iwa(ii)=0
           end do
        end if
        call MPI_AllReduce(iwa,parin,npari4,PAR_INTEGER,MPI_SUM,PAR_COMM_MY_CODE4,istat)        
     end if
     deallocate(iwa,stat=istat)
     call cputim(time2)
     cpu_paral(24)=cpu_paral(24)+time2-time1
     npari=0
#endif

  end if

  !
  ! Operations on complex arrays
  !
  if(nparx>0) then 

#ifdef MPI_OFF
#else
     call cputim(time1)     
     allocate(cwa(nparx), stat=istat)
     if(ISLAVE) then
        do ii=1,nparx
           cwa(ii)=parcx(ii)
        end do
     end if
     nparx4=int(nparx,4)
     if(itask==1) then
        !
        ! Minimum
        !
        if(IMASTER) then
           do ii=1,nparx
              cwa(ii)=1e9
           end do
        end if
        call MPI_AllReduce(cwa,parcx,nparx4,MPI_DOUBLE_COMPLEX,MPI_MIN,PAR_COMM_MY_CODE4,istat) 
     else if(itask==2) then
        !
        ! Maximum
        !
        if(IMASTER) then
           do ii=1,nparx
              cwa(ii)=-1e9
           end do
        end if
        call MPI_AllReduce(cwa,parcx,nparx4,MPI_DOUBLE_COMPLEX,MPI_MAX,PAR_COMM_MY_CODE4,istat)        
     else if(itask==3) then
        !
        ! Sum
        !
        if(IMASTER) then
           do ii=1,nparx
              cwa(ii)=(0.0_rp,0.0_rp)
           end do
        end if
        call MPI_AllReduce(cwa,parcx,nparx4,MPI_DOUBLE_COMPLEX,MPI_SUM,PAR_COMM_MY_CODE4,istat)        
     end if
     deallocate(cwa,stat=istat)
     call cputim(time2)
     cpu_paral(22)=cpu_paral(22)+time2-time1
     nparx=0
#endif

  end if
  if(istat/=0) call runend('PARALL: FUNCTION MPI_ALLREDUCE HAS FAILED')

end subroutine par_operat
