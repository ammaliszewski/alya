!----------------------------------------------------------------------
!> @addtogroup Parall
!> @{
!> @file    par_bin_structure_paral.f90
!> @author  Guillaume Houzeaux
!> @date    01/02/2014
!> @brief   Subdomain bin structure
!> @details Temporal adaptation for paralel partitioning. Compute the parallel bin structure
!>          Limit the bin size to 100 in each dimension
!> @} 
!----------------------------------------------------------------------

subroutine par_element_distribution_part()
  use def_kintyp,         only : ip,rp,lg,r1p,r2p,i1p
  use def_master,         only :  IPARALL, kfl_paral
  use def_master,         only :  ioutp,lun_outpu
  use def_domain,         only :  xmima_tot, mnode, nelem
  use def_parall,         only :  iproc_part_par
  use mod_communications, only :  PAR_MIN
  use mod_communications, only :  PAR_MAX
  use mod_communications, only :  PAR_SUM
  use mod_communications, only :  PAR_COMM_RANK_AND_SIZE
  use mod_communications, only :  PAR_ALLGATHERV
  use mod_communications, only :  PAR_ALLGATHER
  use mod_parall,         only :  par_memor
  use mod_parall,         only :  PAR_WORLD_SIZE
  use mod_parall,         only :  PAR_MY_CODE_RANK
  use mod_parall,         only :  par_bin_part
  use mod_parall,         only :  par_bin_boxes
  use mod_parall,         only :  par_bin_comin
  use mod_parall,         only :  par_bin_comax
  use mod_parall,         only :  par_bin_size
  use mod_parall,         only :  par_part_comin
  use mod_parall,         only :  par_part_comax
  use def_domain,         only :  coord,npoin,ndime
  use mod_memory,         only :  memory_alloca
  use mod_maths,          only :  maths_mapping_3d_to_1d
  use mod_maths,          only :  maths_mapping_1d_to_3d_x
  use mod_maths,          only :  maths_mapping_1d_to_3d_y
  use mod_maths,          only :  maths_mapping_1d_to_3d_z
  use mod_elmgeo,         only :  element_type

  use mod_par_partit_paral, only: coord_local_part, kfl_paral_parmes, memor_par, nBoundElem, boundElem_part, ltype_part
  use mod_par_partit_paral, only: faces_part
  use mod_par_partit_paral, only: lnods_local_part, lnods_part, nelem_part, coord_local_part_inv
  use mod_par_partit_paral, only: nelemRecv, elem_loc_recv, lnods_local, coord_local_inv, npoin_local, npoin_local_recv
  use mod_par_partit_paral, only: coord_local_part_lo, coordRange, coordOffset, ltype_recv
  use mod_maths,          only : maths_mapping_coord_to_3d
  use mod_maths,          only : maths_in_box
  use mod_memory,         only : memory_alloca
  use mod_memory,         only : memory_deallo
  use mod_communications, only : PAR_SEND_RECEIVE
  use mod_unista

  implicit none
  integer(ip)                             :: ii,jj,kk,pp,kpart,ipart
  integer(ip)                             :: ielem, cpart, idime
  type(i1p),   pointer                    :: my_boundElem_to_part(:)
  type(i1p),   pointer                    :: my_part_to_boundElem(:)
  integer(ip), pointer                    :: npoin_send(:), nelem_send(:)
  integer(ip), pointer                    :: nelem_recv(:)
  integer(ip)           :: ielty, iface, inodb, inode, ipoin, ilist, &
                             ielpo, jelem, jelty, jface, pepoi
  integer(ip)           :: ksize, desti_rank, maxCoordVal, minCoordVal
  logical(lg), pointer                    :: intersection(:)
  integer(ip), pointer :: lnods_loc(:,:)   => null()
  integer(ip), pointer :: elem_loc(:)   => null()
  integer(ip), pointer :: ltype_loc(:)   => null()
  integer(ip), pointer :: lnods_loc_recv(:,:)   => null()
  integer(ip)           :: idx1, idx2, elemLocal, nelemSend, i, j
  integer(ip)          :: ipoinIdx,  localIdx, nelem_part_aux
  integer(ip), pointer :: lnods_part_count(:)   => null()
  integer(ip), pointer :: coord_local_part_index(:)   => null()
  
  nullify(my_boundElem_to_part)
  nullify(my_part_to_boundElem)
  nullify(npoin_send)
  nullify(nelem_send)
  nullify(nelem_recv)
  nullify(intersection)


  call livinf(0_ip,'PARALL: DISTRIBUTE ELEMENTS FOR PARTITIONS',0_ip)
               !if (iproc_part_par == 1) then
               !write(*,*) ' NELEM VALE:' , nelem
             !end if 
   
   !Recorremos elementos frontera
   !-----------------------------------------------------------------------------
   !-----------------------------------------------------------------------------
   !---1. my_boundElem_to_part
   !-----------------------------------------------------------------------------
   !-----------------------------------------------------------------------------
   call memory_alloca(memor_par,'NPOIN_SEND','par_test',my_boundElem_to_part, nBoundElem,'INITIALIZE')
   call memory_alloca(memor_par,'NPOIN_SEND','par_test',npoin_send,      kfl_paral_parmes,               'INITIALIZE',0_ip)
   call memory_alloca(memor_par,'NPOIN_SEND','par_test',nelem_send,      kfl_paral_parmes,               'INITIALIZE')
   call memory_alloca(memor_par,'NPOIN_SEND','par_test',nelem_recv,      kfl_paral_parmes,               'INITIALIZE')
   call memory_alloca(memor_par,'NPOIN_SEND','par_test',my_part_to_boundElem, kfl_paral_parmes,               'INITIALIZE')
   call memory_alloca(memor_par,'NPOIN_SEND','par_test',intersection,    kfl_paral_parmes,               'INITIALIZE',0_ip)
   
   !write(*,*) 'par_bin_boxes 1:' , par_bin_boxes(1)
   !flush(6)
   !write(*,*) 'par_bin_boxes 2:' , par_bin_boxes(2)
   !flush(6)
   !write(*,*) 'par_bin_boxes 3:' , par_bin_boxes(3)
   !flush(6)
   !write(*,*) 'par_bin_part111:' , par_bin_part(1,1,1)%l
   !flush(6)
   !write(*,*) 'par_bin_part211:' , par_bin_part(2,1,1)%l
   !flush(6)
   !write(*,*) 'par_bin_part121:' , par_bin_part(1,2,1)%l
   
   
   elemdo: do pp = 1,nBoundElem
        !write(*,*) "Elem:" , pp
        !flush(6)
        ielem = boundElem_part(pp)
        !write(*,*) "iElem:" , ielem
        !flush(6)
        ielty = abs(ltype_part(ielem))
        allocate( my_boundElem_to_part(pp) % l(kfl_paral_parmes))
        do ipart = 1, kfl_paral_parmes
          my_boundElem_to_part(pp) % l(ipart) = 0
        end do
        !recorremos las caras del elemento
        do iface = 1,element_type(ielty) % number_faces   
           !write(*,*) "iface:" , iface
           !flush(6)
           !Si la cara es no compartida entonces la procesamos  
           !if( faces_part(1,iface,ielem) /= 0 ) then !OJO ESTO NO SE SI SE CALCULA PARA TODAS LAS CARAS
              !Si el primer nodo de la cara no es 0, entonces la cara no es compartida,
              !recorremos los nodos de la cara
              !npoin_send = 0
              do ipart = 0, kfl_paral_parmes - 1
                 npoin_send(ipart) = 0
              end do
              do inodb = 1,element_type(ielty) % node_faces(iface)
                !write(*,*) "inodb:" , inodb
                !flush(6)
				!obtenemos la posicion del nodo en la cara
                inode = element_type(ielty) % list_faces(inodb,iface) 
                !write(*,*) "inode:" , inode
                !flush(6)
                !obtenemos el numero de nodo
                ipoin = lnods_local_part(inode,ielem)
                !write(*,*) "ipoin:" , ipoin
                !flush(6)
                !----------------------------------------------------------
                !----------------------------------------------------------
                call maths_mapping_coord_to_3d(ndime,par_bin_boxes,par_bin_comin,par_bin_comax,coord_local_part(1:ndime,ipoin - coordOffset),ii,jj,kk)
                if( ii /= 0 ) then 
                  ksize = 0
                  do kpart = 1,par_bin_size(ii,jj,kk)
                    !write(*,*) "kpart:" , kpart
                    !flush(6)
                    ipart = par_bin_part(ii,jj,kk) % l(kpart)
                    !si no soy yo mismo
                    if (ipart /= iproc_part_par) then
                      !write(*,*) "ipart:" , ipart
                      !flush(6)
                      if( maths_in_box(ndime,coord_local_part(1:ndime,ipoin - coordOffset),par_part_comin(1:ndime,ipart),par_part_comax(1:ndime,ipart)) ) then
                        npoin_send(ipart) = npoin_send(ipart) + 1
                      end if
                    end if                    
                  end do
                end if
                !----------------------------------------------------------
                !----------------------------------------------------------                                
              end do
              !!A las particiones que tengan todos los puntos de la cara le enviamos el elemento
              do ipart = 0, kfl_paral_parmes - 1                
                if (npoin_send(ipart) == element_type(ielty) % node_faces(iface) ) then
                   my_boundElem_to_part(pp) % l(ipart + 1) = 1 
                end if
              end do
              
           !end if
        end do
   end do elemdo   
   
   !
   ! Deallocate memory of FACES
   !
   call memory_deallo(memor_par,'FACES','par_calc_bound_elem',faces_part)
   call memory_deallo(memor_par,'par_bin_part','par_calc_bound_elem',par_bin_part)
   call memory_deallo(memor_par,'par_bin_size','par_calc_bound_elem',par_bin_size)
   !do pp = 1,nBoundElem
   !  write(*,*) 'slave:' , kfl_paral, ' Element:', pp , ' partitions:' , my_boundElem_to_part(pp)%l
   !end do
 
   !-----------------------------------------------------------------------------
   !-----------------------------------------------------------------------------
   !---2. my_part_to_boundElem
   !-----------------------------------------------------------------------------
   !-----------------------------------------------------------------------------
   do cpart = 1, kfl_paral_parmes
     do pp = 1, nBoundElem
       if (my_boundElem_to_part(pp)%l(cpart) == 1) then
         nelem_send(cpart) = nelem_send(cpart) + 1
       end if
     end do
   end do
          
    do cpart = 1,kfl_paral_parmes
       if( nelem_send(cpart) > 0 ) then
	     allocate( my_part_to_boundElem(cpart) % l(nelem_send(cpart)) )
	     jelem = 1_ip
	     do pp = 1, nBoundElem
           if (my_boundElem_to_part(pp)%l(cpart) == 1) then
             my_part_to_boundElem(cpart) % l(jelem) = boundElem_part(pp)
             jelem = jelem + 1
           end if
         end do
	   else
         nelem_send(cpart) = 0
       end if
    end do
    
    !do cpart = 1,kfl_paral_parmes
	!	write(*,*) 'slave:' , kfl_paral, ' part:', cpart , ' elements:' , my_part_to_boundElem(cpart) % l
    !end do
    
   !-----------------------------------------------------------------------------
   !-----------------------------------------------------------------------------
   !---3. Bounding box intersection
   !-----------------------------------------------------------------------------
   !-----------------------------------------------------------------------------
    do cpart = 0, kfl_paral_parmes - 1
       intersection(cpart) = .true.
       do idime = 1,ndime
          if(    par_part_comin(idime,iproc_part_par) >= par_part_comax(idime,cpart) .or. &
               & par_part_comin(idime,cpart)       >= par_part_comax(idime,iproc_part_par) ) then
             intersection(cpart) = .false.
          end if
       end do
    end do
    
    !dealocatamos LOS PART_PART_COMIN
    call memory_deallo(memor_par,'NPOIN_SEND'     ,'par_test',par_part_comin)
    call memory_deallo(memor_par,'NPOIN_SEND'     ,'par_test',par_part_comax)
    !do cpart = 0,kfl_paral_parmes - 1
	!	write(*,*) 'slave:' , iproc_part_par, ' part:', cpart , ' intersection:' , intersection(cpart)
    !end do

    ! 
    !
    !-----------------------------------------------------------------------------
    !-----------------------------------------------------------------------------
    !---4. Get how many points I should check if I have them
    !-----------------------------------------------------------------------------
    !-----------------------------------------------------------------------------
    !call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
    !call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
    do cpart = 1,kfl_paral_parmes
       if( intersection(cpart - 1)) then
          !call PAR_SEND_RECEIVE(1_ip,1_ip,npoin_send(cpart:cpart),npoin_recv(cpart:cpart),'IN PARAL PARTITION',cpart,'NON BLOCKING')          
          call PAR_SEND_RECEIVE(1_ip,1_ip,nelem_send(cpart:cpart),nelem_recv(cpart:cpart),'IN PARAL PARTITION',cpart - 1)  
       end if
    end do
    !call PAR_END_NON_BLOCKING_COMM(1_ip)
   
    !-----------------------------------------------------------------------------
    !-----------------------------------------------------------------------------
    !---4. Calculate LNODS_LOC y ELEM_LOC and send the data to LNDOS_LOC_RECV ELEM_LOC_RECV
    !-----------------------------------------------------------------------------
    !-----------------------------------------------------------------------------
    
    nelem_part_aux = nelem / kfl_paral_parmes
    !Total de elementos que vamos a recibir
    nelemRecv = 0
    do cpart = 1,kfl_paral_parmes
       if( intersection(cpart - 1)) then
          nelemRecv = nelemRecv + nelem_recv(cpart)
       end if
    end do
    allocate(lnods_loc_recv(mnode, nelemRecv))
    allocate(elem_loc_recv(nelemRecv))
    allocate(ltype_recv(nelemRecv))
    idx1 = 1
    do cpart = 1,kfl_paral_parmes    
       if( intersection(cpart - 1)) then
          allocate(lnods_loc(mnode, nelem_send(cpart)))
          allocate(elem_loc(nelem_send(cpart)))
          allocate(ltype_loc(nelem_send(cpart)))
          do ielem=1,nelem_send(cpart)
             elemLocal = my_part_to_boundElem(cpart) % l(ielem)
             do inode=1, mnode
               lnods_loc(inode,ielem) = lnods_part(inode,elemLocal)
             end do
             ltype_loc(ielem) = ltype_part(elemLocal)
             !convert local element numbering to global element numbering depending on the rank
             elemLocal = elemLocal + (nelem_part_aux * iproc_part_par)
             elem_loc(ielem) = elemLocal             
          end do

          call PAR_SEND_RECEIVE(mnode * nelem_send(cpart), mnode * nelem_recv(cpart), lnods_loc(1:mnode, 1),lnods_loc_recv(1:mnode, idx1),'IN PARAL PARTITION',cpart - 1)
          call PAR_SEND_RECEIVE(nelem_send(cpart), nelem_recv(cpart), elem_loc, elem_loc_recv(idx1:(idx1+nelem_recv(cpart) - 1)),'IN PARAL PARTITION',cpart - 1)
          call PAR_SEND_RECEIVE(nelem_send(cpart), nelem_recv(cpart), ltype_loc, ltype_recv(idx1:(idx1+nelem_recv(cpart) - 1)),'IN PARAL PARTITION',cpart - 1)
          deallocate(lnods_loc)
          deallocate(elem_loc)
          deallocate(ltype_loc)
          idx1 = idx1 + nelem_recv(cpart)
       end if
    end do
    
    call memory_deallo(memor_par,'NPOIN_SEND'     ,'par_test',my_part_to_boundElem)
    call memory_deallo(memor_par,'NPOIN_SEND'     ,'par_test',my_boundElem_to_part)

    !-----------------------------------------------------------------------------
    !-----------------------------------------------------------------------------
    !---5. Calculate new LNODS_LOCAL COORD_LOCAL_INV
    !-----------------------------------------------------------------------------
    !-----------------------------------------------------------------------------
    !----deallocates auxiliares
    deallocate(lnods_part)
    deallocate(coord_local_part)
    
	allocate(lnods_local(mnode, nelem_part + nelemRecv))
	!PARTE QUE YA TIENE ESTE SLAVE
	do ielem=1, nelem_part
		lnods_local(:,ielem) = lnods_local_part(:,ielem)
	end do
	deallocate(lnods_local_part)
		
	!PARTE NUEVA QUE VIENE
		
	!1.--Calculamos el numero de coordenadas locales que tenemos en lnods_local_part y lnods_loc_recv, quitando los duplicados
	!Calculo de NPOIN_LOCAL_RECV
	!npoin_local_recv = 0
	!outer: do i = 1, npoin
	!   do ielem=1,nelemRecv
	!      do inode=1,mnode
	!        ipoin=lnods_loc_recv(inode,ielem)
	!        if (ipoin == i) then
	!           !--Tenemos el nodo, vemos si ya está en coord_local_part_inv, para no contabilizarlo
	!           localIdx = 0
	!	       do j = 1, npoin_local
	!		      if (coord_local_part_inv(j) .eq. ipoin) then
	!		         localIdx = j
	!                 exit
	!			  end if
	!	       end do
	!	       if (localIdx == 0) then
	!              npoin_local_recv = npoin_local_recv + 1
	!		   end if
	!           cycle outer
	!        end if            
	!     end do
	!  end do      
	!end do outer
		
	!1.--ALTERNATIVA!!! Calculamos el numero de coordenadas locales que tenemos en lnods, quitando los duplicados
	!para cada coordenada global de la malla si la tenemos en lnods_par sumamos +1
	allocate(lnods_part_count(nelemRecv*mnode + nelem_part*mnode))
	i = 1
	do ielem=1,nelem_part
	   do inode=1,mnode
	     lnods_part_count(i)=lnods_local(inode,ielem)
		 i = i + 1
	   end do
	end do
	do ielem=1,nelemRecv
	   do inode=1,mnode
	      lnods_part_count(i)=lnods_loc_recv(inode,ielem)
		  i = i + 1
	   end do
    end do
	   
	maxCoordVal = MAXVAL(lnods_part_count)
	minCoordVal = MINVAL(lnods_part_count)   
	coordRange = maxCoordVal - minCoordVal + 1_ip
	coordOffset = minCoordVal - 1_ip
	call unista(lnods_part_count,npoin_local_recv)	   	   	   
	deallocate(lnods_part_count)
	!Indice global para optimizar busquedas
	allocate(coord_local_part_index(coordRange))
	coord_local_part_index = 0
		   
	!---FIN ALTERNATIVA
		
	allocate(coord_local_inv(npoin_local + npoin_local_recv))
	
	!PARTE QUE YA TIENE ESTE SLAVE
	coord_local_inv = 0
	do inode = 1, npoin_local
		coord_local_inv(inode) = coord_local_part_inv(inode)
	end do
	deallocate(coord_local_part_inv)	
		
	write(*,*) 'SLAVE par_element_distribution_part 7:' , kfl_paral
	ipoinIdx = 1
	do ielem=1,nelemRecv
	   do inode=1,mnode
	      ipoin=lnods_loc_recv(inode,ielem)
          !buscamos si ya lo tenemos indexado
		  localIdx = coord_local_part_index(ipoin - coordOffset)
			 
		  !no está indexado, lo añadimos en coord_local_inv
		  if (localIdx == 0) then
		     localIdx = npoin_local + ipoinIdx
		     coord_local_part_index(ipoin - coordOffset) = localIdx
		     !añadimos el coord_local_part_inv
		     coord_local_inv(localIdx) = ipoin      
		     ipoinIdx = ipoinIdx + 1                        
          end if         
	      !rellenamos el lnods_local_part con la numeracion local
	      lnods_local(inode, nelem_part + ielem) = localIdx
	   end do
    end do

   deallocate(lnods_loc_recv)
   deallocate(npoin_send)
   deallocate(nelem_send)
   deallocate(nelem_recv)
   deallocate(intersection)
   deallocate(coord_local_part_index)
   coordOffset = 0
   coordRange = 0
                    
   !test
   !do ielem=1, nelem_part + nelemRecv
   !   write(*,*) 'LNODS_LOCAL ' , ielem, ':' , lnods_local(:,ielem)
   !end do
end subroutine par_element_distribution_part
