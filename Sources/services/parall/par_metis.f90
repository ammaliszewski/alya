#ifdef V5METIS
 
subroutine par_metis(&       ! metis 5.0.2
     itask , nelem, nedge ,xadj, adj, wvert_par, &
     wedge_par, kfl_weigh_par, npart_par,            &
     lepar_par , nbNodInter, xadjSubDom, adjSubDom,  &
     invpR, permR, dsize, nwver , nwedg , mem_servi) 
  !------------------------------------------------------------------------
  !****f* Parall/par_metis5
  ! NAME
  !    par_metis
  ! DESCRIPTION
  !    This routine is the bridge between Alya and METIS
  !    Adapto las llamada a la version 5
  ! INPUT
  !    Element graph
  ! OUTPUT
  ! USED BY
  !    par_graph_arrays 
  !    par_disbou
  !***
  !------------------------------------------------------------------------
  use def_parame
  use mod_memchk 
  use mod_graphs
!  use iso_c_binding  ! for c_null_ptr esto ahoar nlo cambie por %VAL(nullvar)
  use iso_c_binding  ! for C_INTPTR_T

  implicit none
  integer(ip), intent(in)    :: itask,nelem,npart_par
  integer(ip), intent(in)    :: nedge,dsize,nwedg,nwver,nbNodInter
  integer(ip), intent(in)    :: xadj(*),xadjSubDom(*)
  integer(ip), intent(in)    :: adj(*),adjSubDom(*)
  integer(ip), intent(in)    :: wvert_par(*),wedge_par(*)
  integer(ip), intent(in)    :: kfl_weigh_par
  integer(ip), intent(out)   :: lepar_par(*),invpR(*),permR(*)  
  integer(8),  intent(inout) :: mem_servi(2)
  integer(ip)                :: edgecut
  integer(ip)                :: optio_par(0:39)
  !
  ! METIS 5.0.2 diferent parameters to metis 4 
  !
  INTEGER(ip), PARAMETER :: METIS_OPTION_PTYPE=0
  INTEGER(ip), PARAMETER :: METIS_OPTION_OBJTYPE=1
  INTEGER(ip), PARAMETER :: METIS_OPTION_CTYPE=2
  INTEGER(ip), PARAMETER :: METIS_OPTION_IPTYPE=3
  INTEGER(ip), PARAMETER :: METIS_OPTION_RTYPE=4
  INTEGER(ip), PARAMETER :: METIS_OPTION_DBGLVL=5
  INTEGER(ip), PARAMETER :: METIS_OPTION_NITER=6
  INTEGER(ip), PARAMETER :: METIS_OPTION_NCUTS=7
  INTEGER(ip), PARAMETER :: METIS_OPTION_SEED=8
  INTEGER(ip), PARAMETER :: METIS_OPTION_MINCONN=9
  INTEGER(ip), PARAMETER :: METIS_OPTION_CONTIG=10
  INTEGER(ip), PARAMETER :: METIS_OPTION_COMPRESS=11
  INTEGER(ip), PARAMETER :: METIS_OPTION_CCORDER=12
  INTEGER(ip), PARAMETER :: METIS_OPTION_PFACTOR=13
  INTEGER(ip), PARAMETER :: METIS_OPTION_NSEPS=14
  INTEGER(ip), PARAMETER :: METIS_OPTION_UFACTOR=15
  INTEGER(ip), PARAMETER :: METIS_OPTION_NUMBERING=16
  INTEGER(ip), PARAMETER :: METIS_OPTION_HELP=17
  INTEGER(ip), PARAMETER :: METIS_OPTION_TPWGTS=18
  INTEGER(ip), PARAMETER :: METIS_OPTION_NCOMMON=19
  INTEGER(ip), PARAMETER :: METIS_OPTION_NOOUTPUT=20
  INTEGER(ip), PARAMETER :: METIS_OPTION_BALANCE=21
  INTEGER(ip), PARAMETER :: METIS_OPTION_GTYPE=22
  INTEGER(ip), PARAMETER :: METIS_OPTION_UBVEC=23

!options[METIS OPTION PTYPE] !Specifies the partitioning method. Possible values are:
  INTEGER(ip), PARAMETER :: METIS_PTYPE_RB      = 0   ! Multilevel recursive bisectioning.
  INTEGER(ip), PARAMETER :: METIS_PTYPE_KWAY    = 1   ! Multilevel k-way partitioning. 

!options[METIS OPTION OBJTYPE]  !Specifies the type of objective. Possible values are:
  INTEGER(ip), PARAMETER :: METIS_OBJTYPE_CUT   = 0   ! Edge-cut minimization.
  INTEGER(ip), PARAMETER :: METIS_OBJTYPE_VOL   = 1   ! Total communication volume minimization. 

!options[METIS OPTION CTYPE]
!Specifies the matching scheme to be used during coarsening. Possible values are:
  INTEGER(ip), PARAMETER :: METIS_CTYPE_RM      = 0   ! Random matching.
  INTEGER(ip), PARAMETER :: METIS_CTYPE_SHEM    = 1   ! Sorted heavy-edge matching.

!options[METIS OPTION IPTYPE] Determines the algorithm used during initial partitioning. Possible values are:
  INTEGER(ip), PARAMETER :: METIS_IPTYPE_GROW    = 0 
  INTEGER(ip), PARAMETER :: METIS_IPTYPE_RANDOM  = 1 
  INTEGER(ip), PARAMETER :: METIS_IPTYPE_EDGE    = 2 
  INTEGER(ip), PARAMETER :: METIS_IPTYPE_NODE    = 3 
!Grows a bisection using a greedy strategy. Computes a bisection at random followed by a refinement. Derives a separator from an edge cut. Grow a bisection using a greedy node-based strategy.


!options[METIS OPTION RTYPE] Determines the algorithm used for refinement. Possible values are:
  INTEGER(ip), PARAMETER :: METIS_RTYPE_FM       = 0 
  INTEGER(ip), PARAMETER :: METIS_RTYPE_GREEDY   = 1 
  INTEGER(ip), PARAMETER :: METIS_RTYPE_SEP2SIDED= 2  
  INTEGER(ip), PARAMETER :: METIS_RTYPE_SEP1SIDED= 3 
!FM-based cut refinement. Greedy-based cut and volume refinement. Two-sided node FM refinement. 
!One-sided node FM refinement.

  integer(C_INTPTR_T) :: nullvar
  nullvar = 0

  if ( ( wedge_par(1)/=0_ip ) .and. (itask==1_ip) ) then ! notice that I am passing %val(nullvar) in the place that
                                                         ! should be occupied by wedge_par
    call runend('par_metis: wedge_par(1)/=0_ip not ready')
  end if

  call METIS_SetDefaultOptions(optio_par)
  optio_par(METIS_OPTION_NUMBERING) = 1     ! Start by 1; Fortran 

  !
  ! Linking with METIS compiled with same integers as Alya
  !  
  if( itask == 1_ip ) then

     if(1==2) then
        print*,'sizeof(mem_servi)',sizeof(mem_servi)
        print*,'sizeof(optio_par)',sizeof(optio_par)
        print*,'sizeof(nullvar)',sizeof(nullvar)
        print*,'nelem:',nelem
        print*,'npart_par:',npart_par
     end if

     !IF kfl_weigh_par IS 0 then the wvert_par is nullified
     !DEC$ ATTRIBUTES C,REFERENCE:: metis_partgraphrecursive
     !DEC$ ATTRIBUTES C,REFERENCE:: metis_partgraphkway
     if (kfl_weigh_par == 0) then
        if( npart_par <= 8 ) then           
           call metis_partgraphrecursive( &
             nelem, 1_ip, xadj, adj, %val(nullvar), %val(nullvar), &
             %val(nullvar), npart_par,    &
             %val(nullvar), %val(nullvar), optio_par, edgecut, lepar_par )
        else
           call metis_partgraphkway(&
             nelem, 1_ip, xadj, adj, %val(nullvar), %val(nullvar), &
             %val(nullvar), npart_par,    &
             %val(nullvar), %val(nullvar), optio_par, edgecut, lepar_par )
        end if
     else
        if( npart_par <= 8 ) then
           call metis_partgraphrecursive( &
             nelem, 1_ip, xadj, adj, wvert_par, %val(nullvar), &
             %val(nullvar), npart_par,    &
             %val(nullvar), %val(nullvar), optio_par, edgecut, lepar_par )
        else
           call metis_partgraphkway(&
             nelem, 1_ip, xadj, adj, wvert_par, %val(nullvar), &
             %val(nullvar), npart_par,    &
             %val(nullvar), %val(nullvar), optio_par, edgecut, lepar_par )
        end if
     end if

  else if( itask == 2_ip ) then

     !DEC$ ATTRIBUTES C,REFERENCE:: metis_nodend
     call metis_nodend(&
          nbNodInter, xadjSubDom, adjSubDom, &
          %val(nullvar), optio_par, invpR, permR )

  end if

end subroutine par_metis


#else









































subroutine par_metis(&       ! metis 4
     itask , nelem, nedge ,xadj, adj, wvert_par, &
     wedge_par, kfl_weigh_par, npart_par,            &
     lepar_par , nbNodInter, xadjSubDom, adjSubDom,  &
     invpR, permR, dsize, nwver , nwedg , mem_servi) 
  !------------------------------------------------------------------------
  !****f* Parall/par_metis
  ! NAME
  !    par_metis
  ! DESCRIPTION
  !    This routine is the bridge between Alya and METIS
  ! INPUT
  !    Element graph
  ! OUTPUT
  ! USED BY
  !    par_graph_arrays 
  !    par_disbou
  !***
  !------------------------------------------------------------------------
  use def_parame
  use mod_memchk 
  use mod_graphs
  implicit none
  integer(ip), intent(in)    :: itask,nelem,npart_par
  integer(ip), intent(in)    :: nedge,dsize,nwedg,nwver,nbNodInter
  integer(ip), intent(in)    :: xadj(*),xadjSubDom(*)
  integer(ip), intent(in)    :: adj(*),adjSubDom(*)
  integer(ip), intent(in)    :: wvert_par(*),wedge_par(*)
  integer(ip), intent(in)    :: kfl_weigh_par
  integer(ip), intent(out)   :: lepar_par(*),invpR(*),permR(*)  
  integer(8),  intent(inout) :: mem_servi(2)
  integer(ip)                :: edgecut,ii
  integer(ip)                :: kfl_fortr_par,optio_par(8)
  !
  ! Linking with METIS compiled with same integers as Alya
  !  
  do ii = 1,8
     optio_par(ii) = 0_ip
  end do

  if( itask == 1_ip ) then

     kfl_fortr_par = 1_ip
     optio_par(1)  = 0_ip 
     optio_par(2)  = 3_ip
     optio_par(3)  = 1_ip
     optio_par(4)  = 1_ip
     optio_par(5)  = 0_ip      

     if( npart_par <= 8 ) then
        !DEC$ ATTRIBUTES C,REFERENCE:: metis_partgraphrecursive
        call metis_partgraphrecursive( &
             nelem, xadj, adj, wvert_par, wedge_par,  &
             kfl_weigh_par, kfl_fortr_par, npart_par, &
             optio_par, edgecut, lepar_par )
     else
        !DEC$ ATTRIBUTES C,REFERENCE:: metis_partgraphkway
        call metis_partgraphkway(&
             nelem, xadj, adj, wvert_par, wedge_par,  &
             kfl_weigh_par, kfl_fortr_par, npart_par, &
             optio_par, edgecut, lepar_par )
     end if

  else if( itask == 2_ip ) then

     kfl_fortr_par = 1_ip
     optio_par(1)  = 1_ip
     optio_par(2)  = 3_ip
     optio_par(3)  = 2_ip
     optio_par(4)  = 2_ip
     optio_par(5)  = 0_ip
     optio_par(6)  = 1_ip 
     optio_par(7)  = 0_ip
     optio_par(8)  = 1_ip

     !kfl_fortr_par = 1_ip
     !optio_par(1)  = 1
     !optio_par(2)  = 3
     !optio_par(3)  = 1
     !optio_par(4)  = 1
     !optio_par(5)  = 0
     !optio_par(6)  = 0
     !optio_par(7)  = 60
     !optio_par(8)  = 5 

     !DEC$ ATTRIBUTES C,REFERENCE:: metis_nodend
     call metis_nodend(&
          nbNodInter, xadjSubDom, adjSubDom, kfl_fortr_par, &
          optio_par, invpR, permR )

  end if

end subroutine par_metis

#endif

