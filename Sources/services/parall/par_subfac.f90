subroutine par_subfac()
  !-----------------------------------------------------------------------
  !****f* domain/par_subfac
  ! NAME
  !    domain
  ! DESCRIPTION
  !    Create edge table
  ! OUTPUT
  !    NNEDG ... Number of edges
  !    LEDGG ... Edge table
  !    LEDGB ... Boundary edge table (when Parall is on)
  ! USED BY
  !    Turnon
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_domain
  use def_master
  use def_parall
  use mod_memchk
  use mod_parall, only : PAR_COMM_MY_CODE_ARRAY
  use mod_parall, only : commd,PAR_COMM_MY_CODE4
  use mod_parall, only : PAR_INTEGER
  implicit none 
  integer(ip)              :: iedgg,iedgb,ii,ineig,kk
  integer(ip)              :: dom_i,bsize,kpoin
  integer(ip)              :: kedge,ipoin,jpoin
  integer(ip), allocatable :: ledg1(:),ledg2(:)
  integer(ip), allocatable :: lfacb_loc(:),bound_perm(:)
  integer(ip), pointer     :: permR(:),invpR(:)
  type(i1p),   pointer     :: bperm(:)
  integer(4)               :: istat
  ! Renumbering
  integer(ip)              :: dummi
  integer(ip), pointer     :: perm2(:),invp2(:)
  integer(ip), pointer     :: ia(:),ja(:)
  !
  ! Parall: change 3rd and 4th column by original node numbering
  !
  if( INOTMASTER ) then

     !-------------------------------------------------------------------
     !
     ! A. Determine neighbors that share the same edge IEDGG (IEDGB)
     !
     !-------------------------------------------------------------------

     call par_boufac()

     allocate(lfacb_loc(nedgb),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LFACB_LOC','par_slexch',lfacb_loc)     
     allocate(ledg1(nedgb),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LEDG1','par_slexch',ledg1)
     allocate(ledg2(nedgb),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'LEDG2','par_slexch',ledg2)

     !-------------------------------------------------------------------
     !
     ! B. Reorder
     !
     !-------------------------------------------------------------------

     do iedgb = 1,nedgb
        iedgg = lfacb(1,iedgb)
        if( lfacb(2,iedgb) > lfacb(3,iedgb) ) then
           ipoin          = lfacb(2,iedgb)            ! B1: reorder boundary edge
           lfacb(2,iedgb) = lfacb(3,iedgb)            ! B1: reorder boundary edge
           lfacb(3,iedgb) = ipoin
           ipoin          = lfacg(1,iedgg)            ! B2: reorder corresponding global edge
           lfacg(1,iedgg) = lfacg(2,iedgg)            ! B2: reorder corresponding global edge
           lfacg(2,iedgg) = ipoin
        end if
     end do

     allocate( bperm(nneig) )
     call memgen(1_ip,npoin,0_ip)

     do ineig = 1,nneig

        !----------------------------------------------------------------
        !
        ! C. Fill in new edge node array in common with neighbor INEIG
        !
        !----------------------------------------------------------------

        dom_i = commd%neights(ineig)
        kedge = 0
        do iedgb = 1,nedgb
           do ii = 1,lfacb(6,iedgb)
              if( lfacb(6+ii,iedgb) ==  ineig ) then
                 iedgg = lfacb(1,iedgb)
                 kedge = kedge + 1
                 lfacb_loc(kedge) = abs(lfacg(5,iedgg))
                 ledg1(kedge)     = lfacb(2,iedgb)
                 ledg2(kedge)     = lfacb(3,iedgb)
              end if
           end do
        end do

        !----------------------------------------------------------------
        !
        ! D. Order edges with INEIG so that the send and receive correspond
        !
        !----------------------------------------------------------------

        call par_sorti2(kedge,ledg1,ledg2,lfacb_loc)

        !----------------------------------------------------------------
        !
        ! E. Add edge nodes with INEIG to permutation list
        !
        !----------------------------------------------------------------

        !
        ! E1. Copy old permutation array and reallocate it
        !
        !do ipoin = 1,npoin
        !   gisca(ipoin) = 0 
        !end do
!if( kfl_paral == 2 .and. commd%neights(ineig) == 4 ) print*,'EDGES 2=',kedge
!if( kfl_paral == 4 .and. commd%neights(ineig) == 2 ) print*,'EDGES 4=',kedge

        kk = 0
        do ii = commd % bound_size(ineig),commd % bound_size(ineig+1)-1
           kk = kk + 1
           gisca(kk) = commd % bound_perm(ii)
        end do
        bsize = commd % bound_size(ineig+1)-commd % bound_size(ineig) + kedge
        allocate( bperm(ineig) % l(bsize) )
        kk = 0
        do ii = commd % bound_size(ineig),commd % bound_size(ineig+1)-1
           kk = kk + 1
           bperm(ineig) % l(kk) = gisca(kk) 
        end do
        do iedgb = 1,kedge
           kk = kk + 1 
           bperm(ineig) % l(kk) = lfacb_loc(iedgb)
        end do
        commd % bound_dim = commd % bound_dim + kedge
     end do
     
     call memchk(two,istat,mem_servi(1:2,servi), 'commd % bound_perm', 'par_memory', commd % bound_perm ) 
     deallocate( commd % bound_perm,stat=istat)
     if(istat/=0) call memerr(two,'commd % bound_perm','par_memory',0_ip)
     allocate(   commd % bound_perm(commd % bound_dim) , stat = istat )
     call memchk(zero,istat,mem_servi(1:2,servi),'commd % bound_perm','par_subfac',commd % bound_perm)

     commd % bound_size(1) = 1
     kk = 0
     do ineig = 1,nneig
        bsize = size( bperm(ineig) % l )
        commd % bound_size(ineig+1) = commd % bound_size(ineig) + bsize
        do ii = 1,bsize
           kk = kk + 1
           commd % bound_perm(kk) = bperm(ineig) % l(ii)
        end do
        deallocate( bperm(ineig) % l )
     end do
     deallocate( bperm )
     call memgen(3_ip,npoin,0_ip)

     !do ineig = 1,nneig
     !   write(90+kfl_paral,*) kfl_paral,commd%neights(ineig),commd % bound_size(ineig+1)-commd % bound_size(ineig)
     !end do
     !call runend('POPO')

     !-------------------------------------------------------------------
     !
     ! F. Renumbering
     !
     !-------------------------------------------------------------------

     allocate(permR(npoin),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'permR','par_arrays',permR)

     allocate(invpR(npoin),stat=istat)
     call memchk(zero,istat,mem_servi(1:2,servi),'invpR','par_arrays',invpR)
     !
     ! PERMR: Identify boundary nodes. Others'=-1, Own=-2
     !
     do ipoin = npoi1+1,npoi2-1
        permR(ipoin) = -1
     end do
     do ipoin = npoi2,npoi3
        permR(ipoin) = -2
     end do

     do ipoin = npoi3+1,npoin_old
        permR(ipoin) = -1
     end do
     do iedgb = 1,nedgb
        iedgg = lfacb(1,iedgb)
        ipoin = lfacg(5,iedgg)
        if( ipoin < 0 ) then
           permR(-ipoin) = -2
        else
           permR( ipoin) = -1
        end if
     end do
     !
     ! Renumber interior nodes
     ! 
     kpoin = 0
     do ipoin = 1,npoin
        if( permR(ipoin) == 0 ) then
           kpoin = kpoin + 1
           permR(ipoin) = kpoin
           invpR(kpoin) = ipoin
        end if
     end do
     !
     ! Re-renumber interior nodes using METIS
     !      
     if( 1 == 1 ) then
 
        allocate( ia(kpoin+1) )
 
        call par_intgra(kpoin,permR,ia)         ! Graph of interior nodes
        ja => gisca

        allocate( invp2(kpoin) )
        allocate( perm2(kpoin) )
        call par_metis(&
             2_ip , dummi , dummi , dummi , dummi , dummi , &
             dummi , dummi , dummi , dummi , kpoin, ia, &
             ja, invp2, perm2, dummi , dummi , dummi , &
             mem_servi(1:2,servi) )

        kpoin = 0
        do ipoin = 1,npoin
           if( permR(ipoin) > 0 ) then
              kpoin        = kpoin + 1
              jpoin        = perm2(kpoin)
              permR(ipoin) = jpoin
              invpR(jpoin) = ipoin
           end if
        end do
        
        deallocate( perm2 )        
        deallocate( invp2 )
        call memgen(3_ip,1_ip,0_ip)        ! Was allocated in par_intgra()
        deallocate( ia    )

     end if
     !
     ! Renumber others' boundary nodes
     !
     npoi1 = kpoin 
     do ipoin = 1,npoin
        if( permR(ipoin) == -1 ) then
           kpoin = kpoin + 1
           permR(ipoin) = kpoin
           invpR(kpoin) = ipoin
        end if
     end do
     !
     ! Renumber own boundary nodes
     !
     npoi2 = kpoin + 1
     npoi3 = npoin
     do ipoin = 1,npoin
        if( permR(ipoin) == -2 ) then
           kpoin = kpoin + 1
           permR(ipoin) = kpoin ! NEW => OLD
           invpR(kpoin) = ipoin ! OLD => NEW
        end if
     end do
     !
     ! Reorder arrays
     !
     lrenn => permR
     call renpoi()
     !
     ! Reorder communication array
     !
     allocate( bound_perm(commd % bound_dim) , stat = istat )
     do ii = 1,commd % bound_dim         
        bound_perm(ii) = commd % bound_perm(ii)
     end do
     do ii = 1,commd % bound_dim 
        commd % bound_perm(ii) = lrenn( bound_perm(ii) )
     end do
     !
     ! LNLEV: Save permutation with original mesh
     !
     do ipoin = 1,npoin_old
        invpR(ipoin) = lnlev(ipoin)
     end do
     do ipoin = 1,npoin
        lnlev(ipoin) = 0
     end do
     do ipoin = 1,npoin_old
        kpoin = permR(ipoin) 
        lnlev(kpoin) = invpR(ipoin)
     end do     
     !
     ! Deallocate memory
     !
     deallocate( bound_perm )

     call memchk(two,istat,mem_servi(1:2,servi), 'invpR', 'par_memory', invpR ) 
     deallocate( invpR,stat=istat)
     if(istat/=0) call memerr(two,'INVPR','par_memory',0_ip)
 
     call memchk(two,istat,mem_servi(1:2,servi), 'permR', 'par_memory', permR ) 
     deallocate( permR,stat=istat)
     if(istat/=0) call memerr(two,'PERMR','par_memory',0_ip)

  end if

  !-------------------------------------------------------------------
  !
  ! G. LNINV_LOC: Uniquely renumber the mesh. This is useful for:
  !    - Postprocess
  !    - To call the division recursively
  !
  !-------------------------------------------------------------------

  call memgen(1_ip,npart_par+1,0_ip)
  call par_algath()

  npoin_total = 0
  nelem_total = 0
  nboun_total = 0
  if( IMASTER ) then
     do kfl_desti_par = 1,npart_par
        call par_parari('RCV',0_ip,1_ip,npoin_par(kfl_desti_par))
        call par_parari('RCV',0_ip,1_ip,nelem_par(kfl_desti_par))
        call par_parari('RCV',0_ip,1_ip,nboun_par(kfl_desti_par))
        npoin_total = npoin_total + npoin_par(kfl_desti_par)
        nelem_total = nelem_total + nelem_par(kfl_desti_par)
        nboun_total = nboun_total + nboun_par(kfl_desti_par)
     end do
  else if( ISLAVE ) then
     kfl_desti_par = 0
     call par_parari('SND',0_ip,1_ip,npoin)
     call par_parari('SND',0_ip,1_ip,nelem)
     call par_parari('SND',0_ip,1_ip,nboun)     
  end if

  do ii = 1,npart_par+1
     gisca(ii) = npoin_tot(ii)
  end do
  do ii = 2,npart_par+1
     gisca(ii) = gisca(ii-1) + gisca(ii)  
  end do

  if( INOTMASTER ) then
     deallocate(lninv_loc)
     allocate(lninv_loc(npoin))
     do ipoin = 1,npoin
        lninv_loc(ipoin) = 0
     end do
     kpoin = gisca(kfl_paral)
     do ipoin = 1,npoi1
        kpoin = kpoin + 1
        lninv_loc(ipoin) = kpoin
     end do
     do ipoin = npoi2,npoi3
        kpoin = kpoin + 1
        lninv_loc(ipoin) = kpoin
     end do

     party=3;pardi=1;parki=1
     pari1 => lninv_loc
     call par_slexch()

  end if

  call memgen(3_ip,npart_par+1,0_ip)

  !----------------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !----------------------------------------------------------------------

  if( INOTMASTER ) then
     
     deallocate(ledg2,stat=istat)
     if(istat/=0) call memerr(two,'LEDG2','par_subfac',0_ip)
     call memchk(two,istat,mem_servi(1:2,servi),'LEDG2','par_subfac',ledg2)
     
     deallocate(ledg1,stat=istat)
     if(istat/=0) call memerr(two,'LEDG1','par_subfac',0_ip)
     call memchk(two,istat,mem_servi(1:2,servi),'LEDG1','par_subfac',ledg1)
     
     deallocate(lfacb_loc,stat=istat)
     if(istat/=0) call memerr(two,'LFACB_LOC','par_subfac',0_ip)
     call memchk(two,istat,mem_servi(1:2,servi),'LFACB_LOC','par_subfac',lfacb_loc)
     
     deallocate(lfacb,stat=istat)
     if(istat/=0) call memerr(two,'LFACB','par_subfac',0_ip)
     call memchk(two,istat,mem_servi(1:2,servi),'LFACB','par_subfac',lfacb)

  end if
!  if( INOTMASTER ) then
!  call memgen(0_ip,npoin,0_ip)
!  do ipoin = 1,npoin
!     gesca(ipoin)=real(lnlev(ipoin))
!  end do
!  call possla(2_ip,parin,gesca,parre)
!end if
!call runend('OESOS')
!if(inotmaster) then
!   do jj = 1, commd%bound_dim
!      write(kfl_paral+90,*) commd%bound_perm(jj) 
!   end do
!end if
  !if(inotmaster) then
  !   do ipoin = 1,npoin
  !      if(    ( abs(coord(1,ipoin)-0.2_rp)<1.0e-12_rp .and.  abs(coord(2,ipoin)-0.2_rp)<1.0e-12_rp .and. abs(coord(3,ipoin)-0.2_rp)<1.0e-12_rp ) .or. &
  !           & ( abs(coord(1,ipoin)-0.2_rp)<1.0e-12_rp .and.  abs(coord(2,ipoin)-0.2_rp)<1.0e-12_rp .and. abs(coord(3,ipoin)-0.1_rp)<1.0e-12_rp )) then
  !         print*,'a=',kfl_paral,ipoin
  !         do jj = 1, commd%bound_dim
  !            if( ipoin == commd%bound_perm(jj) ) then 
  !               print*,'b=',kfl_paral,ipoin
  !            end if
  !         end do
  !      end if
  !   end do
  !end if

end subroutine par_subfac
