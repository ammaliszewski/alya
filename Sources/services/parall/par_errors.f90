subroutine par_errors(itask)
!------------------------------------------------------------------------
!****f* Parall/par_errors
! NAME
!    par_errors
! DESCRIPTION
!    Check errors
! OUTPUT
! USED BY
!    Parall
!***
!------------------------------------------------------------------------
  use      def_kintyp
  use      def_domain
  use      def_master
  use      def_parall
  implicit none
  integer(ip), intent(in) :: itask

  if(kfl_paral==0) then

     select case(itask)

     case(1)
        if(rp/=8) then
           call runend('PARALL: ONLY WORKS WITH DOUBLE PRECISION')
        end if
        
        !if(ip/=4) then
        !   call runend('PARALL: ONLY WORKS WITH 4-BYTE INTEGERS')
        !end if
        
        if(kfl_autbo==1) then
           call runend('PARALL: CANNOT USE AUTOMATIC BOUNDARY GENERATION')
        end if
        
        !if(kfl_ptask==2.and.kfl_postp_par==1) then
        !   call runend('PARALL: WHEN DOING RESTART, MASTER CANNOT BE IN CHARGE OF POSTPROCESS')
        !end if
        
     case(2)
        !if(kfl_outfo/=50) then
        !   if(kfl_ptask==2.and.kfl_oumes==1.and.kfl_postp_par==1.and.kfl_paral==0) then
        !      call outfor(2_ip,0_ip,'PARALL: WHEN DOING RESTART, MASTER CANNOT POSTPROCESS THE MESH')
        !      call outfor(2_ip,lun_outpu_par,'PARALL: WHEN DOING RESTART, MASTER CANNOT POSTPROCESS THE MESH')
        !      kfl_oumes=0
        !   end if
        !end if

     end select

  end if

end subroutine par_errors
