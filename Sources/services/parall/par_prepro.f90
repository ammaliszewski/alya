subroutine par_prepro()
  !------------------------------------------------------------------------
  !****f* Parall/par_prepro
  ! NAME
  !    par_prepro
  ! DESCRIPTION
  !    This routine does the following:
  !    Master: parts the graph with METIS and sends partition to slaves
  !    Slaves: receive the partition
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_elmtyp
  use def_master
  use def_domain
  use def_parall
  use mod_memory
#ifdef PARMETIS
  use mod_par_partit_paral
#endif
  implicit none  
  integer(ip)    :: ichec,ipart,iauxi,dummi,color
  real(rp)       :: time1,time2,time3
  character(100) :: messa_integ

  if( IMASTER .and. .not. READ_AND_RUN() ) then

     !-------------------------------------------------------------------
     !
     ! Master
     !
     !-------------------------------------------------------------------

     call cputim(time1)
     call par_livinf(1_ip,' ',dummi)
     !
     ! Prepare arrays needed for efficient reinitialization in cut nodes,  LEFPO & PEFPO in master
     !
     servi = ID_PARALL
     call cputim(time2)
     !
     ! Partition mesh
     !
     !call cpu_time(time1)
#ifdef PARMETIS
         !call par_partit_paral()
         
         call par_dist_mesh()
         call par_recpar()

         !call par_partit()
        !do while (1==1)
		!	call sleep(10)
		!end do
#else
        call par_partit()
#endif
     !call cpu_time(time2)
     !write(*,*) 'PARTITIONING TIME:' , time2-time1
     !
     ! Compute arrays
     !
     call par_arrays()
     !
     ! Send data to slaves
     !
     call par_livinf(6_ip,' ',dummi)

     do kfl_desti_par = 1,npart_par

        iauxi = npart_par / 10_ip
        messa_integ='    S/R  '//trim(intost(kfl_desti_par))//' - TOT '//trim(intost(npart_par))

!!        call par_livinf(20_ip,'toto',dummi)           
        call par_sendat(two)            ! Send/receive data from readim and reastr 
!!        call par_livinf(20_ip,messa_integ,dummi)           
        call par_sendat(three)          ! Send/receive data from cderda and reaset
!!        call par_livinf(20_ip,messa_integ,dummi)           
        call par_sendat(five)           ! Send/receive data from partit
!!        call par_livinf(20_ip,messa_integ,dummi)           

        if (iauxi > 0) then
           if (modulo(kfl_desti_par,iauxi)==0) then
              call par_livinf(20_ip,messa_integ,dummi)           
           end if
        end if
        
     end do

     messa_integ='  SEND/REC  '//trim(intost(npart_par))//' - TOTAL '//trim(intost(npart_par))
     call par_livinf(20_ip,messa_integ,dummi)           

     call par_livinf(20_ip,'     COORD, LTYPE, LNODS, LTYPB, LNODB... ',dummi)
     call par_sengeo(1_ip)              ! Send geometry: COORD, LTYPE, LNODS, LTYPB, LNODB
     call par_livinf(20_ip,'     LBOEL... ',dummi)
     call par_sengeo(2_ip)              ! Send geometry: LBOEL
     call par_livinf(20_ip,'     KFL_FIELD, XFIEL, TIME_FIELD,... ',dummi)
     call par_sengeo(3_ip)              ! Send geometry: KFL_FIELD, XFIEL, TIME_FIELD
     call par_livinf(20_ip,'     SET DATA... ',dummi)
     call par_senset()                  ! Send set data
     call par_livinf(20_ip,'     BC DATA... ',dummi)
     call par_senbcs()
     call par_livinf(20_ip,'     COMMUNICATION ARRAYS... ',dummi)
     call par_sencom()                  ! Send communication arrays
     call par_livinf(20_ip,'  SEND DATA TO SLAVES... DONE.',dummi)
     !
     ! Barrier
     !
     do ipart = 1,nproc_par-1,nsire_par       
        call par_barrie()
     end do
     !
     ! Local ordering of nodes
     !
     npoi1 =  0    
     npoi2 =  0    
     npoi3 = -1   
     npoi4 =  0    
     npoin =  0
     nelem =  0
     !!!!nboun =  0 ! peta en chkcod
     !
     ! Writes in partition file
     !
     call par_livinf(1000_ip,' ',dummi)
     if( PART_AND_WRITE() ) call par_outprt()
     call cputim(time3)
     cpu_paral(4) = time3 - time2

  else if( ISLAVE .and. ( PART_AND_RUN() .or. READ_AND_RUN() ) ) then

     !-------------------------------------------------------------------
     !
     ! Slaves
     !
     !-------------------------------------------------------------------
     
#ifdef PARMETIS     
     !--------------------------------------------------------------------
     !----------------------Slaves partition the mesh---------------------
     if (IPARSLAVE) then !--Para filtrar el numero de slaves que participan
        write(*,*) 'SLAVE EMPIEZA PARTICION MALLA:' , kfl_paral
        call par_partition_mesh_slave()
        !call par_partit_paral_slave()
     end if
     !----------------------Finish Slaves partition the mesh-------------
     !-------------------------------------------------------------------
        !do while (1==1)
		!	call sleep(10)
		!end do
#endif     
     !
     ! Receive (kfl_ptask==1) of read (kfl_ptask==2) data with barrier
     !
     do ipart = 1,nproc_par-1,nsire_par 
        if( kfl_paral >= ipart .and. kfl_paral < ipart+nsire_par ) then
           kfl_desti_par = 0
           call par_sendat(two)     ! Send/receive data from readim and reastr
           call par_sendat(three)   ! Send/receive data from cderda
           call par_sendat(five)    ! Send/receive data from partit
           !
           ! Local ordering of nodes
           !
           npoi1 = lni              ! Interior nodes: ipoin=1,npoi1
           npoi2 = slfbo            ! begin of boundary nodes: ipoin = npoi2,npoi3
           npoi3 = lnb + slfbo - 1  ! End of own boundary nodes
           npoi4 = 0                ! No addition boundary arrays
           !
           ! Continue sending
           !
           call memgeo(1_ip)        ! Allocate memory for geometrical arrays
           call par_sengeo(1_ip)    ! Receive geometry: COORD, LTYPE, LNODS, LTYPB, LNODB, IB...
           call par_sengeo(2_ip)    ! Receive geometry: LBOEL
           call par_sengeo(3_ip)    ! Receive geometry: KFL_FIELD, XFIEL, TIME_FIELD
           call par_senset()        ! Receive set data
           call par_senbcs()        ! Receive boundary conditions
           call par_sencom()        ! Receive communication arrays
        end if
        call par_barrie()
     end do
     !
     ! Checkpoint
     !
     if( READ_AND_RUN() ) call par_chkpoi(ichec)
 
  else if( IMASTER .and. READ_AND_RUN() ) then

     !-------------------------------------------------------------------
     !
     ! Master in read-and-run mode
     !
     !-------------------------------------------------------------------
     if (kfl_modul(id_levels)/=0) call runend('par_prepro: read_and_run not ready with level set')
     !
     ! Open minimum graph to avoid null pointers when calling solvers
     !
     call memory_alloca(mem_servi(1:2,servi),'R_DOM','par_memory',r_dom,1_ip)
     call memory_alloca(mem_servi(1:2,servi),'C_DOM','par_memory',c_dom,1_ip)
     !
     ! Local ordering of nodes
     !
     npoi1 =  0    
     npoi2 =  0    
     npoi3 = -1   
     npoi4 =  0   
     npoin =  0 
     nelem =  0
     !!!!nboun =  0
     !
     ! Barrier
     !
     do ipart = 1,nproc_par-1,nsire_par         
        call par_barrie()
     end do
     !
     ! Communicators
     !
     call par_sencom()
     !
     ! Master reads from partition file
     !
     call par_livinf(6_ip,' ',dummi)
     call par_outprt()
     !
     ! Checkpoint
     !
     call par_chkpoi(ichec)
     call par_livinf(10_ip,' ',ichec)

  end if

end subroutine par_prepro
