subroutine geobin(order)
  !------------------------------------------------------------------------
  !****f* Domain/geobin
  ! NAME
  !    geobin
  ! DESCRIPTION
  !    This routine dumps the geometry in binary format
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      mod_postpr
  implicit none
  integer(ip), intent(in) :: order
  integer(ip)             :: ielem,inode,idime,ipoin,dummi,ihang,khang
  integer(ip)             :: iboun,inodb
  integer(4)              :: itest
  character(20)           :: cfiel

  if( INOTSLAVE ) then

     select case(order)

     case(1_ip)
        !
        ! Write geometry in binary format
        !
        if( .not. READ_AND_RUN() ) then 
           call openfi(7_ip)
           call livinf(39_ip,' ',0_ip)
           itest=123456
           write(lun_binar) itest
           cfiel='DIMEN'
           write(lun_binar) cfiel(1:20),one
           write(lun_binar) nelem,npoin,nboun,npoib,nboib,nhang,mnodb
           cfiel='LTYPE'
           write(lun_binar) cfiel(1:20),one
           write(lun_binar) (ltype(ielem),ielem=1,nelem)
           cfiel='LNNOD'
           write(lun_binar) cfiel(1:20),one
           write(lun_binar) (lnnod(ielem),ielem=1,nelem)
           cfiel='LNODS'
           write(lun_binar) cfiel(1:20),one
           write(lun_binar) ((lnods(inode,ielem),inode=1,nnode(ltype(ielem))),ielem=1,nelem)
           cfiel='COORD'
           write(lun_binar) cfiel(1:20),one
           write(lun_binar) ((coord(idime,ipoin),idime=1,ndime),ipoin=1,npoin)
           if(nboun/=0) then
              cfiel='LTYPB'
              write(lun_binar) cfiel(1:20),one
              write(lun_binar) (ltypb(iboun),iboun=1,nboun)
              cfiel='LNODB'
              write(lun_binar) cfiel(1:20),one
              write(lun_binar) ((lnodb(inodb,iboun),inodb=1,nnode(ltypb(iboun))),iboun=1,nboun)
              cfiel='LBOEL'
              write(lun_binar) cfiel(1:20),one
              write(lun_binar) (lboel(nnode(ltypb(iboun))+1,iboun),iboun=1,nboun)
           end if
           cfiel='END_FILE'
           write(lun_binar) cfiel(1:20),one
           call openfi(8_ip)
        end if

     case(2_ip)
        !
        ! Read geometry in binary format
        !
        call openfi(7_ip)
        call livinf(38_ip,' ',0_ip)
        cfiel='none'
        read(lun_binar) itest
        if( itest /= 123456 ) call runend('WRONG MAGIC NUMBER IN GEOMETRY BINARY FILE') 
        do while(trim(cfiel)/='END_FILE')
           read(lun_binar) cfiel(1:20),dummi
           if(dummi/=0) then
              if(trim(cfiel)=='DIMEN') then
                 read(lun_binar) nelem,npoin,nboun,npoib,nboib,nhang,mnodb
                 if(.not.associated(ltype)) then
                    call memgeo(1_ip)
                    call memgeo(27_ip)
                 end if
              else if(trim(cfiel)=='LTYPE') then
                 call livinf(27_ip,' ',0_ip)
                 read(lun_binar) (ltype(ielem),ielem=1,nelem)
                 call mescek(2_ip)
              else if(trim(cfiel)=='LNNOD') then
                 call livinf(27_ip,' ',0_ip)
                 read(lun_binar) (lnnod(ielem),ielem=1,nelem)
                 call mescek(2_ip)
              else if(trim(cfiel)=='LNODS') then
                 call livinf(28_ip,' ',0_ip)
                 read(lun_binar) ((lnods(inode,ielem),inode=1,nnode(ltype(ielem))),ielem=1,nelem)
                 call mescek(3_ip)
              else if(trim(cfiel)=='COORD') then
                 call livinf(33_ip,' ',0_ip)
                 read(lun_binar) ((coord(idime,ipoin),idime=1,ndime),ipoin=1,npoin)
              else if(trim(cfiel)=='LTYPB') then
                 call livinf(36_ip,' ',0_ip)
                 read(lun_binar) (ltypb(iboun),iboun=1,nboun)
                 call mescek(6_ip)
              else if(trim(cfiel)=='LNODB') then
                 call livinf(35_ip,' ',0_ip)
                 read(lun_binar) ((lnodb(inodb,iboun),inodb=1,nnode(ltypb(iboun))),iboun=1,nboun)
                 call mescek(4_ip)
              else if(trim(cfiel)=='LBOEL') then                 
                 call livinf(37_ip,' ',0_ip) 
                 kfl_bouel=1
                 read(lun_binar) (lboel(nnode(ltypb(iboun))+1,iboun),iboun=1,nboun)
                 call mescek(5_ip)
              end if
           end if
        end do
        call openfi(8_ip)

     end select

  end if

end subroutine geobin
