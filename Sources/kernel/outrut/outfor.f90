subroutine outfor(itask,nunit,messa)
  !------------------------------------------------------------------------
  !****f* outrut/outfor
  ! NAME 
  !    nsi_outfor
  ! DESCRIPTION
  !    Output format for all output files 
  ! USES
  ! USED BY
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_kermod
  use def_domain
  use def_solver
  use mod_parall
  use mod_communications, only : PAR_AVERAGE
  implicit none
  integer(ip),   intent(in) :: itask,nunit
  character*(*), intent(in) :: messa
  integer(ip)               :: jtask,ii,nsolv,isolv
  real(rp)                  :: dummr,load_balance_assembly
  real(rp)                  :: load_balance_solver,load_balance_post
  character(20)             :: me010
  character(100)            :: me100
  character(40)             :: woutl='--------------------------------------'
  character(3)              :: wmpi3,wblas

  if( INOTSLAVE .or. itask <= 0 ) then

     jtask = abs(itask)

     select case (jtask)

     case(0_ip)
        !
        ! End of Alya (runend)
        !        
        write(nunit,1) trim(messa)

     case(1_ip)
        !
        ! Error message
        !
        if( nunit == 0 ) then
           write(lun_outpu,100) trim(messa)
        else
           write(nunit,100) trim(messa)
        end if

     case(2_ip)
        !
        ! Warning message
        !
        if( nunit == 0 ) then
           write(lun_outpu,200) trim(messa)
        else
           write(nunit,200) trim(messa)
        end if

     case(3_ip)
        !
        ! Warning in module MODUL
        !
        write(lun_outpu,300)   'WARNINGS HAVE BEEN FOUND IN '//&
             trim(namod(modul))//' DATA FILE'
        write(lun_outpu,*)
        call flush(lun_outpu)
        write(nunit,*)
        call livinf(50_ip,' ',modul)

     case(4_ip)
        !
        ! Error in module MODUL 
        !
        if( trim(messa) == '1' ) then
           call runend(trim(messa)//' ERROR HAS BEEN FOUND IN '//&
                trim(namod(modul))//' DATA FILE')
        else 
           call runend(trim(messa)//' ERRORS HAVE BEEN FOUND IN '//&
                trim(namod(modul))//' DATA FILE')
        end if

     case(5_ip)
        !
        ! Begin of an inner iteration (Solite)
        !
        if( solve_sol(1) % kfl_solve == 1 ) then
           write(solve_sol(1) % lun_solve,500) ittim,itcou,itinn(modul)
           flush(solve_sol(1) % lun_solve)
        end if

     case(6_ip)
        !
        ! End of run (Turnof)
        !
        write(nunit,600) trim(namod(modul))

     case(7_ip)
        !
        ! Time step (Begste)
        !
        !if( kfl_outpu == 1) write(nunit,700) ittim

     case(8_ip)
        !
        ! Critical time step (*_tistep)
        !
        if( kfl_outpu == 1 ) then
           if( kfl_timco/=2 ) then
              if( ioutp(1)/=0.and.ioutp(2)/=1 ) then
                 if( routp(1)>0.0_rp ) then
                    write(nunit,800) namod(modul),routp(1),dtime/routp(1)
                 else
                    write(nunit,800) namod(modul),routp(1),0.0_rp
                 end if
              end if
           else 
              if( ioutp(1)/=0.and.ioutp(2)/=1)&
                   write(nunit,801) namod(modul),routp(1),routp(2),routp(3)
           end if
        end if

     case(9_ip)
        !
        ! Coupling convergence (*_concou)
        !
        if( kfl_outpu == 1) write(nunit,900) coutp(1),routp(1)

     case(10_ip)
        !
        ! Coupling convergence (Concou)
        !
        if( kfl_outpu == 1) write(nunit,1000) itcou,iblok,routp(1)

     case(11_ip)
        !
        ! Restart run (*_outinf)
        !
        write(nunit,1100) 

     case(12_ip)
        !
        ! Title (*_outinf)
        !
        if( kfl_rstar == 2 ) then
           write(nunit,1100) 
        else
           write(nunit,1200) namod(modul),trim(title)
        end if

     case(13_ip)
        !
        ! Wrong type of element
        !
        me010=intost(nunit)
        call runend('A WRONG ELEMENT WITH '//trim(me010)//' NODES HAS BEEN FOUND')

     case(14_ip)
        !
        ! End of analysis (runend)
        !
        write(nunit,1400)

     case(15_ip)
        !
        ! Time step (setgts)
        !
        if( kfl_outpu == 1) write(nunit,1500) ioutp(1),routp(1),routp(2)

     case(16_ip)
        !
        ! header (openfi)
        !
        if( kfl_rstar == 2 ) then
           write(nunit,1600)
        else
           write(nunit,1610) adjustl(trim(title))
        end if

     case(17_ip)
        !
        ! Information (outinf)
        !
        if( coutp(1) == '') coutp(1)='NONE'
        write(nunit,1700)&
             trim(coutp(1)),trim(coutp(2)),trim(coutp(3)),&
             routp(5),routp(6),routp(7),&
             ioutp(1),ioutp(2),ioutp(11),&
             bandw_dom,profi_dom,&
             routp(1),routp(2),routp(3),ioutp(4),&
             routp(4),ioutp(4),ioutp(5),ioutp(6),ioutp(7),&
             trim(coutp(4)),trim(coutp(5)),ioutp(8),&
             ioutp(9),ioutp(10),&
             trim(coutp(6)),trim(coutp(7))
        if( kfl_outpu == 1) write(nunit,1702)
        call flush(nunit)

     case(18_ip)
        !
        ! CPU times (outcpu)
        !        
        write(nunit,1800)&
             routp( 1),&
             routp( 2),routp( 3),routp( 4),routp( 5),&
             routp( 6),routp( 7),routp( 8),routp( 9),&
             routp(10),routp(11)

     case(19_ip)
        !
        ! CPU times (outcpu)
        !    
        load_balance_assembly = routp( 3) / (routp( 5)+zeror)
        load_balance_solver   = routp( 7) / (routp( 9)+zeror)
        load_balance_post     = routp(11) / (routp(13)+zeror)
        write(nunit,1900)&
             trim(coutp(1)),&
             routp( 1) , routp( 2) , & ! Total time
             routp( 3) , routp( 4) , & ! Matrix construction average
             routp( 5) , routp( 6) , & ! Matrix construction max
             load_balance_assembly , & ! Load balance
             routp( 7) , routp( 8) , & ! Solvers average
             routp( 9) , routp(10) , & ! Solvers max
             load_balance_solver,    & ! Load balance
             routp(11) , routp(12) , & ! Post average
             routp(13) , routp(14) , & ! Post max
             load_balance_post         ! Load balance

     case(20_ip)
        !
        ! CPU times (outcpu)
        !        
        !write(nunit,2000)&
        !     routp(1),routp(2),routp(3),routp(4)
        write(nunit,2001)&
             routp(1),routp(2)

     case(21_ip)
        !
        ! Memory (outmem)
        !        
        if( ioutp(50) == 1 ) then
           write(nunit,2100) 
           write(nunit,2102) 
        else 
           write(nunit,2103)
        end if
        write(nunit,2101) &
             routp(1),trim(coutp(1)),&
             routp(2),trim(coutp(2)),&
             routp(3),trim(coutp(3))

     case(22_ip)
        !
        ! Memory (outmem)
        !        
        write(nunit,2200) trim(coutp(1)),routp(1),trim(coutp(2))

     case(23_ip)
        !
        ! Memory (outmem)
        !        
        write(nunit,2300) coutp(1)(1:6),routp(1),coutp(2)(1:6)

     case(24_ip)
        !
        ! Memory (outmem)
        !        
        write(nunit,2400) routp(1),trim(coutp(1))

     case(25_ip)
        !
        ! Generic section titles for modules log files
        !
        me100=''
        do ii=1,len(trim(messa))
           me100(ii:ii)='-'
        end do
        write(nunit,2500) trim(me100),trim(messa),trim(me100)

     case(26_ip)
        !
        ! End of run (Turnof)
        !
        write(nunit,600) trim(naser(servi))

     case(27_ip)
        !
        ! Title (*_outinf)
        !
        write(nunit,1200) naser(servi),trim(title)

     case(28_ip)
        !
        ! Steady state (*_cvgunk)
        !
        write(nunit,2800) namod(modul),ittim

     case(29_ip)
        !
        ! Summary of computing time: title
        !
        write(nunit,2900) max(routp(1),0.0_rp)

     case(30_ip)
        !
        ! Summary of computing time: section
        !
        write(nunit,3000) adjustl(coutp(1)),max(routp(1),0.0_rp),max(routp(2),0.0_rp)

     case(31_ip)
        !
        ! Summary of computing time: subsection
        !
        write(nunit,3100) adjustl(coutp(1)),max(routp(1),0.0_rp),max(routp(2),0.0_rp)

     case(32_ip)
        !
        ! Sets results: iteration header
        !
        write(nunit,3200) ioutp(1),ioutp(2),ioutp(3),routp(1)

     case(33_ip)
        !
        ! Sets results: number of columns
        !
        write(nunit,3300) ioutp(1),ioutp(2)

     case(34_ip)
        !
        ! Set results: file header
        !
        write(nunit,3400) trim(coutp(1))

     case(35_ip)
        !
        ! Set results: variable, column
        !
        write(nunit,3500) trim(coutp(1)),ioutp(1)

     case(36_ip)
        !
        ! Solver header
        !
        write(nunit,3600) 

     case(37_ip)
        !
        ! Solver statistics
        !
        nsolv=size(solve_sol)
        if( nsolv >= 1) write(nunit,3600)
        do isolv = 1,nsolv
           if( solve_sol(isolv) % nsolv > 0 ) then
              if( solve_sol(isolv) % kfl_algso/=-999 ) then
                 ii=len(trim(solve_sol(isolv) % wprob))+1
                 write(nunit,3700) &
                      trim(solve_sol(isolv) % wprob),&
                      woutl(1:ii),&
                      trim(solve_sol(isolv) % wsolv),&
                      solve_sol(isolv) % nsolv
                 if( solve_sol(isolv) % kfl_algso == 2 ) then
                    write(nunit,3702)&
                         'NUMBER OF GROUPS=     ',&
                         solve_sol(isolv) % ngrou
                 end if
                 if( solve_sol(isolv) % kfl_algso>=1.or.solve_sol(isolv) % kfl_algso == -2 ) then
                    write(nunit,3703)&
                         'PRECONDITIONER:       ',&
                         trim(solve_sol(isolv) % wprec)
                    if( solve_sol(isolv) % kfl_preco == 4 ) then
                       write(nunit,3704)&
                            'TOLERANCE PREC.=      ',&
                            solve_sol(isolv) % toler                 
                    end if
                 end if
                 if( solve_sol(isolv) % kfl_algso >= 1 .or. solve_sol(isolv) % kfl_algso == -2 ) then

                    write(nunit,3701)& 
                         solve_sol(isolv) % itsol(1),&
                         solve_sol(isolv) % itsol(2),&
                         solve_sol(isolv) % itsol(3)/solve_sol(isolv) % nsolv,&
                         solve_sol(isolv) % itsol(3),&
                         solve_sol(isolv) % cputi(1)

                    if( solve_sol(isolv) % kfl_schur > 0 ) then
                       !
                       ! Schur complement solver
                       !
                       routp( 1)  = sum(solve_sol(isolv) % cpu_schur)
                       routp( 1)  = max(zeror,routp(1))
                       dummr      = 100.0_rp / routp(1)
                       routp( 2)  = solve_sol(isolv) % cpu_schur(1)                   
                       routp( 3)  = solve_sol(isolv) % cpu_schur(1)*dummr       ! Invert Aii
                       routp( 4)  = solve_sol(isolv) % cpu_schur(2)
                       routp( 5)  = solve_sol(isolv) % cpu_schur(2)*dummr       ! Compute preconditioner P
                       routp( 6)  = solve_sol(isolv) % cpu_schur(3)
                       routp( 7)  = solve_sol(isolv) % cpu_schur(3)*dummr       ! Factorize preconditioner P
                       routp( 8)  = solve_sol(isolv) % cpu_schur(4)
                       routp( 9)  = solve_sol(isolv) % cpu_schur(4)*dummr       ! Solver initialization
                       routp(10)  = solve_sol(isolv) % cpu_schur(6)
                       routp(11)  = solve_sol(isolv) % cpu_schur(6)*dummr       ! Main loop: q = A p = Abb p - Abi Aii^{-1} Aib p     
                       routp(12)  = solve_sol(isolv) % cpu_schur(7)
                       routp(13)  = solve_sol(isolv) % cpu_schur(7)*dummr       ! Main loop: x = x + alpha*p     
                       routp(14)  = solve_sol(isolv) % cpu_schur(8)
                       routp(15)  = solve_sol(isolv) % cpu_schur(8)*dummr       ! Main loop: L z = q     
                       routp(16)  = solve_sol(isolv) % cpu_schur(5)&
                            -(solve_sol(isolv) % cpu_schur(6) &
                            + solve_sol(isolv) % cpu_schur(7) &
                            + solve_sol(isolv) % cpu_schur(8)  )
                       routp(16)  = max(zeror,routp(16))        
                       routp(17)  = routp(16)*dummr                             ! Main loop: others      
                       write(nunit,3706) &
                            routp( 1) ,  &
                            routp( 2),routp( 3),routp( 4),routp( 5) , &
                            routp( 6),routp( 7),routp( 8),routp( 9) , &
                            routp(10),routp(11),routp(12),routp(13) , &
                            routp(14),routp(15),routp(16),routp(17)                       
                    else
                       !
                       ! Iterative solver
                       !
                       routp     = 0.0_rp
                       routp(1)  = 100.0_rp / max(solve_sol(isolv) % cputi(1),zeror)
                       routp(2)  = solve_sol(isolv) % cputi(2) * routp(1)
                       routp(3)  = solve_sol(isolv) % cputi(3) * routp(1)
                       routp(4)  = solve_sol(isolv) % cputi(4) * routp(1)
                       routp(5)  = solve_sol(isolv) % cputi(5) * routp(1)
                       dummr     = solve_sol(isolv) % cputi(1)-sum(solve_sol(isolv) % cputi(2:10))
                       dummr     = max(dummr,0.0_rp)
                       routp(10) = dummr * routp(1)
                       write(nunit,3705)&
                            solve_sol(isolv) % cputi(1),&
                            solve_sol(isolv) % cputi(2),routp(2),&
                            solve_sol(isolv) % cputi(3),routp(3),&
                            solve_sol(isolv) % cputi(4),routp(4),&
                            solve_sol(isolv) % cputi(5),routp(5),&
                            dummr,routp(10)   
                    end if
                 end if
              end if
              write(nunit,*)
           end if
        end do

     case(38_ip)
        !
        ! Partition summary
        !
        write(nunit,3800) (ioutp(ii),ii=1,32)

     case(39_ip)
        !
        ! Algebraic solver header
        !
        if( solve_sol(1) % kfl_solve == 1 ) then
           if( kfl_rstar == 2 ) then
              write(solve_sol(1) % lun_solve,3929)
           end if
           if( solve_sol(1) % kfl_schur == 1 ) then
              write(solve_sol(1) % lun_solve,3903) trim(solve_sol(1) % wsolv)
              if( solve_sol(1) % kfl_scaii == 0 ) then
                 write(solve_sol(1) % lun_solve,3926) 'CHOLESKY IN SKYLINE FORMAT'
              else
                 write(solve_sol(1) % lun_solve,3926) 'DIRECT SPARSE CSR' 
              end if
           else
              write(solve_sol(1) % lun_solve,3903) trim(solve_sol(1) % wsolv) 
           end if
           if( solve_sol(1) % kfl_blogs == 1 ) then
               write(solve_sol(1) % lun_solve,3928) 'ON'
           end if
           if( solve_sol(1) % kfl_algso == 0 ) then
           else if( solve_sol(1) % kfl_algso == -1 ) then
           else if( solve_sol(1) % kfl_algso == -2 ) then
              write(solve_sol(1) % lun_solve,3904) solve_sol(1) % relso
           else if( solve_sol(1) % kfl_algso == -3 ) then
              continue
           else if( solve_sol(1) % kfl_algso == 1 ) then
              write(solve_sol(1) % lun_solve,3906) solve_sol(1) % solco
              write(solve_sol(1) % lun_solve,3907) solve_sol(1) % miter
              write(solve_sol(1) % lun_solve,3910) solve_sol(1) % nzmat,rp
           else if( solve_sol(1) % kfl_algso == 2 ) then
              write(solve_sol(1) % lun_solve,3908) solve_sol(1) % ngrou
              if( solve_sol(1) % kfl_defso == 0 ) then
                 if( solve_sol(1) % kfl_defas == 0 ) then
                    write(solve_sol(1) % lun_solve,3911) 'CHOLESKY (SKYLINE)'
                 else
                    write(solve_sol(1) % lun_solve,3911) 'DIRECT SPARSE (CSR)'
                 end if
              else
                 write(solve_sol(1) % lun_solve,3911) 'ITERATIVE SPARSE (CSR)'
              end if
              if( solve_sol(1) % kfl_gathe == 1 ) then
                 write(solve_sol(1) % lun_solve,3912) 'ALL GATHER'
              else
                 write(solve_sol(1) % lun_solve,3912) 'ALL REDUCE'
              end if

              write(solve_sol(1) % lun_solve,3906) solve_sol(1) % solco
              write(solve_sol(1) % lun_solve,3907) solve_sol(1) % miter
              write(solve_sol(1) % lun_solve,3910) solve_sol(1) % nzmat,rp
           else if( solve_sol(1) % kfl_algso == 5 ) then 
              write(solve_sol(1) % lun_solve,3906) solve_sol(1) % solco
              write(solve_sol(1) % lun_solve,3907) solve_sol(1) % miter
              write(solve_sol(1) % lun_solve,3910) solve_sol(1) % nzmat,rp
           else if( solve_sol(1) % kfl_algso == 8 ) then
              write(solve_sol(1) % lun_solve,3905) solve_sol(1) % nkryd
              write(solve_sol(1) % lun_solve,3906) solve_sol(1) % solco
              write(solve_sol(1) % lun_solve,3907) solve_sol(1) % miter
              write(solve_sol(1) % lun_solve,3910) solve_sol(1) % nzmat,rp
           end if
           !
           ! Coarse solver
           !
           if( solve_sol(1) % kfl_coarse /= 0 ) then
              write(solve_sol(1) % lun_solve,3913) 
              write(solve_sol(1) % lun_solve,3914) solve_sol(1) % ngrou
           end if
           !
           ! Preconditioner header
           !
           if( solve_sol(1) % kfl_algso/=-1.and.solve_sol(1) % kfl_algso/=0 ) then
              write(solve_sol(1) % lun_solve,3920) solve_sol(1) % wprec
              if(    solve_sol(1) % kfl_algso == SOL_SOLVER_CG           .or. &
                   & solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_CG  .or. &
                   & solve_sol(1) % kfl_algso == SOL_SOLVER_PIPELINED_CG ) then
                    write(solve_sol(1) % lun_solve,3931) 'M^{-1/2} A M^{-1/2} M^{1/2} x = M^{-1/2} b'                 
              else
                 if( solve_sol(1) % kfl_leftr == 0 ) then
                    write(solve_sol(1) % lun_solve,3931) 'M^{-1} A x = M^{-1} b'
                 else
                    write(solve_sol(1) % lun_solve,3931) 'A M^{-1} M x = b' 
                 end if
              end if
              if( solve_sol(1) % kfl_preco == 4 ) then
                 write(solve_sol(1) % lun_solve,3909) &
                      solve_sol(1) % toler,solve_sol(1) % nline,solve_sol(1) % nlin1,solve_sol(1) % npoin
              end if
              if( solve_sol(1) % kfl_preco == SOL_MULTIGRID ) then
                 write(solve_sol(1) % lun_solve,3922) solve_sol(1) % ngrou 
                 if( solve_sol(1) % kfl_defso == 0 ) then
                    if( solve_sol(1) % kfl_defas == 0 ) then
                       write(solve_sol(1) % lun_solve,3921) 'CHOLESKY (SKYLINE)'
                    else
                       write(solve_sol(1) % lun_solve,3921) 'DIRECT SPARSE (CSR)'
                    end if                    
                 else
                    write(solve_sol(1) % lun_solve,3921) 'ITERATIVE SPARSE (CSR)'
                 end if 
                 if( solve_sol(1) % kfl_defpr == 2 ) then
                    write(solve_sol(1) % lun_solve,3924) 'JACOBI WITH DIAGONAL PRECONDITIONER'
                 else if( solve_sol(1) % kfl_defpr == 4 ) then
                    write(solve_sol(1) % lun_solve,3923) 'JACOBI WITH LINELET PRECONDITIONER'
                    write(solve_sol(1) % lun_solve,3909) &
                         solve_sol(1) % toler,solve_sol(1) % nline,solve_sol(1) % nlin1,solve_sol(1) % npoin                    
                 end if
                 write(solve_sol(1) % lun_solve,3925) solve_sol(1) % itpre
              end if
              if( solve_sol(1) % kfl_preco == 11 ) then
                 if( solve_sol(1) % kfl_scpre == 0 ) then
                    write(solve_sol(1) % lun_solve,3927) 'CHOLESKY IN SKYLINE FORMAT'
                 else
                    write(solve_sol(1) % lun_solve,3927) 'DIRECT SPARSE CSR' 
                 end if
              end if
           end if
           !
           ! Solver header
           !
           if( solve_sol(1) % kfl_algso/=0.and.solve_sol(1) % kfl_algso/=-1)&
                write(solve_sol(1) % lun_solve,3930)
        end if
        if( solve_sol(1) % kfl_cvgso == 1 ) then
           write(solve_sol(1) % lun_cvgso,3940)
        end if

     case(40_ip)
        !
        ! Svn info
        !
        if( adjustl(coutp(1)(1:4)) == 'Path'.and.adjustl(coutp(1)(1:3))/='END' ) then
           write(nunit,4000) 
           ii=1
           do while(coutp(ii)/='END')
              write(nunit,4001) trim(coutp(ii))
              ii=ii+1
           end do
        end if

     case(41_ip)
        !
        ! Makefile info
        !
        if( adjustl(coutp(1)(1:3))/='END' ) then
           write(nunit,4100)
           write(nunit,4102) 
           ii=1
           do while(coutp(ii)/='END'.and.ii<10)
              write(nunit,4101) adjustl(trim(coutp(ii)))
              ii=ii+1
           end do
           write(nunit,4103) 
           ii=10
           do while(coutp(ii)/='END'.and.ii<20)
              write(nunit,4101) adjustl(trim(coutp(ii)))
              ii=ii+1
           end do
           flush(nunit)
        end if

     case(42_ip)
        !
        ! Boundary condition codes
        !
        write(nunit,4200)

     case(43_ip)
        !
        ! List of boundary condition codes
        !
        !write(nunit,4300) (ioutp(ii),ii=1,ioutp(49))
        !write(nunit,4301) ioutp(50)
        if( ioutp(2) == -100 ) then
           write(nunit,4301) ioutp(1)
        else if( ioutp(3) == -100 ) then
           write(nunit,4302) ioutp(1),ioutp(2)
        else 
           write(nunit,4303) ioutp(1),ioutp(2),ioutp(3)
        end if

     case(44_ip)
        !
        ! Geomtrical condition nodes
        !
        write(nunit,4400) (ioutp(ii),ii=1,10)

     case(45_ip)
        !
        ! Bad edge
        !
        write(nunit,100) 'BAD EDGES HAVE BEEN FOUND'
        write(nunit,4500) ioutp(1),ioutp(2),ioutp(3),ioutp(4)
        call runend('BAD EDGES HAVE FOUND: SEE LOG FILE')

     case(46_ip)
        !
        ! Sets results: time header
        !
        write(nunit,4600) ioutp(1),routp(1)

     case(47_ip)
        !
        ! Error in kernel
        !
        call runend(trim(messa))

     case(48_ip)
        !
        ! Error in kernel
        !
        if( lun_livei /= 0 ) call livinf(0_ip,trim(messa),0_ip)
        call runend(trim(messa))

     case(49_ip)
        !
        ! CPU times (subelm)
        !        
        write(nunit,4900) &
             routp( 1) ,  &
             routp( 2),routp( 3),routp( 4),routp( 5) , &
             routp( 6),routp( 7),routp( 8),routp( 9) , &
             routp(10),routp(11),routp(12),routp(13) , &
             routp(14),routp(15),routp(16),routp(17) , &
             routp(18),routp(19)

     case(50_ip)
        !
        ! Witness points
        !        
        write(nunit,5000)

     case(51_ip)
        !
        ! Witness points: List of missing witness points
        !        
        if( ioutp(2) == 0 ) write(nunit,5101) 
        write(nunit,5100) ioutp(1)

     case(52_ip)
        !
        ! Witness points: OK
        !        
        write(nunit,5200)

     case(53_ip)
        !
        ! Lagrangian particles: results
        !        
        write(nunit,5399)
        if( igene == 0 ) then
           write(nunit,5300)
        else if( igene == 1 ) then
           write(nunit,5301)
        else if( igene == 2 ) then
           write(nunit,5302)
        else if( igene == 3 ) then
           write(nunit,5303)
        else if( igene == 4 ) then
           write(nunit,5304)
        end if

     case(54_ip)
        !
        ! Lagrangian particles: convergence
        !        
        write(nunit,5400)

     case(56_ip)
        !
        ! Header CPU times of other operations
        !        
        write(nunit,5600) 
 
    case(57_ip)
        !
        ! Lagrangian particles: deposition map
        !        
        write(nunit,5700)
        !write(nunit,5701)

    case(58_ip)
        !
        ! Support geometry projection
        !        
        write(nunit,5800) routp(1),routp(2),routp(3)

     case(59_ip)
        !
        ! Wake model 
        !
        write(nunit,5900) 

     case(60_ip)
        !
        ! Wake model 
        !
        write(nunit,6000) ioutp(1),routp(1:13)

     case(61_ip)
        !
        ! Generic message
        !
        write(nunit,6100) coutp(1),ioutp(1)

     case(62_ip)
        !
        ! Generic message
        !
        write(nunit,6200) coutp(1),routp(1)

     case(63_ip)
        !
        ! Header zone-wise partition
        !
        write(nunit,6300) 

     case(64_ip)
        !
        ! Zone-wise partition
        !
        write(nunit,6400) ioutp(1),ioutp(2) 
        
     case(65_ip)
        !
        ! Thrust and power
        !
        write(abs(nunit),6500) ioutp(1),ioutp(2), routp(1),routp(2),routp(3), routp(4)

     case(66_ip)
        !
        ! Inter-color Communicators
        !
        write(nunit,6600)
        write(nunit,6601) ioutp(1),ioutp(2),ioutp(3),&
             &            ioutp(4),ioutp(5)

     case(67_ip)
        !
        ! List of inter-color Communicators
        !
        coutp(1) = 'CODE= '
        coutp(2) = 'ZONE= '
        coutp(3) = 'SUBD= '
        !write(nunit,6700) trim(coutp(1)),ioutp(1),&
        !     &            trim(coutp(2)),ioutp(2),&
        !     &            trim(coutp(3)),ioutp(3),&
        !     &            trim(coutp(1)),ioutp(4),&
        !     &            trim(coutp(2)),ioutp(5),&
        !     &            trim(coutp(3)),ioutp(6)        
        write(nunit,6700) trim(coutp(1)),ioutp(1),ioutp(4),&
             &            trim(coutp(2)),ioutp(2),ioutp(5),&
             &            trim(coutp(3)),ioutp(3),ioutp(6)

     case(68_ip)
        !
        ! Parall bin structure
        !
        write(nunit,6800) ioutp(1),ioutp(2),ioutp(3)
 
     case(69_ip)
        !
        ! Max slave memory (outmem)
        !        
        write(nunit,6900) &
             routp(1),trim(coutp(1)),&
             routp(2),trim(coutp(2))

     case(70_ip)
        !
        ! Load balance and communications
        !        
        write(nunit,7000)

     case(71_ip)
        !
        ! Load balance and communications
        !        
        write(nunit,7100) routp(1),routp(2),routp(3)

     case(72_ip) 
        !
        ! Load balance and communications
        !        
        ioutp(1) = par_omp_num_threads
        ioutp(2) = par_omp_num_blocks
        ioutp(3) = par_omp_nelem_chunk
        ioutp(4) = par_omp_nboun_chunk
        ioutp(5) = par_omp_npoin_chunk
#ifndef NO_COLORING
        ioutp(6) = par_omp_num_colors
#else
        ioutp(6) = 0
#endif
        call PAR_AVERAGE(4_ip,ioutp(3:6),'IN MY CODE')
        if( INOTSLAVE ) then 
           write(nunit,7200) ioutp(1:6)
           if( par_omp_num_colors > 0 ) then
              write(nunit,7203)  
              do ii = 1,igene
                 write(nunit,7201) ii,par_omp_ia_colors(ii+1)-par_omp_ia_colors(ii)
              end do
           end if
        end if

     case(73_ip) 
        !
        ! Optimization options
        !   
#ifdef MPI3
        wmpi3 = 'ON'
#else
        wmpi3 = 'OFF'
#endif
#ifdef BLAS
        wblas = 'ON'
#else
        wblas = 'OFF'
#endif
        write(lun_outpu,7300) wmpi3,wblas

     end select

     if( nunit /= 0 ) call flush(nunit)

  end if
  !
  ! When slaves should stop
  !
  if( ISLAVE ) then
     if( itask == 4 ) then
        if( trim(messa) == '1' ) then
           call runend(trim(messa)//' ERROR HAS BEEN FOUND IN '//&
                trim(namod(modul))//' DATA FILE')
        else 
           call runend(trim(messa)//' ERRORS HAVE BEEN FOUND IN '//&
                trim(namod(modul))//' DATA FILE')
        end if
     end if
  end if
  !
  ! Formats
  !
1 format(//,&
       & 5x,//,&
       & 5x,'--------------------------------------------------------',/,&
       & 5x,/,&
       & 5x,'|- AN ERROR HAS BEEN DETECTED:',/,/,&
       & 9x,a,/)
100 format(&
       & 5x,'|- ERROR:   ',a)
200 format(&
       & 5x,'|- WARNING: ',a)
300 format(&
       & 5x,'|- ',a)
500 format(&
       & '# Time step: ',i5,', Outer iter:  ',i5,', Inner iter:  ',i5)
600 format(//,&
       & 5x,'|- END OF ',a,' RUN',/)
800 format(5x,&
       & 5x,' ',a6,':',/,&
       & 5x,'      - CRITICAL TIME STEP DTC: ',e12.6,/,&
       & 5x,'      - RATIO DT/DTC:           ',e12.6 )
801 format(5x,&
       & 5x,' ',a6,':',/,&
       & 5x,'      - CRITICAL TIME STEP DTC: ',e12.6,/,&
       & 5x,'      - USING LOCAL TIME STEP. MIN= ',e12.6,' MAX= ',e12.6)
900 format(&
       & 5x,'      RESIDUAL OF ',a17,' : ',e16.6,' (%)')
1000 format(/,&
       & 5x,'      GLOBAL ITERATION NUMBER: ',i5,', BLOCK NUMBER: ',i2,/,&
       & 5x,'      CURRENT CPU TIME:        ',e12.6)
1100 format(///,&
       & 5x,'|- THIS IS A TIME RESTART RUN: CONTINUING... ')
1200 format(///,&
       & 5x,'   --------------------------',/,&
       & 5x,'|- ',a6,' OUTPUT FOR PROBLEM: ',a,/,&
       & 5x,'   --------------------------',/)
1400 format(//,&
       & 5x,'|- END OF ANALYSIS',/)
1500 format(/,&
       &    5x,'   |- TIME STEP NUMBER: ',i12,/&
       &    5x,'      CURRENT TIME STEP dt = ',e12.6,/&
       &    5x,'      CURRENT TIME t       = ',e12.6)
1600 format(/,&
       & 5x,'|- THIS IS A TIME RESTART RUN: CONTINUING...',//)
1610 format(///,&
       & 5x,'   --------',/,& 
       & 5x,'|- A L Y A: ',a,/,&
       & 5x,'   --------',/)
1700 format(//,&
       & 5x,'   -----------------------',/&
       & 5x,'|- SUMMARY OF PROBLEM DATA',/,&
       & 5x,'   -----------------------',/,&
       & /,&
       & /,&
       & 5x,'   |- GENERAL DATA:',/,&
       & /,&
       & 5x,'      MODULES SOLVED:       ',a,/,&
       & 5x,'      BLOCK DEFINITION:     ',a,/,&
       & 5x,'      TIME STEP STRATEGY:   ',a,/,&
       & 5x,'      TIME STEP=            ',e12.6,/,&
       & 5x,'      TIME RANGE=           ',e12.6,',',e12.6,/,&
       & /,&
       & 5x,'   |- DOMAIN DATA:',/,&
       & /,&
       & 5x,'      DOMAIN DIMENSION=     ',i9,   /,&
       & 5x,'      TOTAL NODES=          ',i9,   /,&
       & 5x,'      HOLE NODES=           ',i9,   /,&
       & 5x,'      ..................... ',      /,&
       & 5x,'      BANDWIDTH=            ',i9,   /,&
       & 5x,'      PROFILE=              ',e9.3, /,&
       & 5x,'      ..................... ',      /,&
       & 5x,'      TOTAL VOLUME=         ',e12.6,/,&
       & 5x,'      AVE. ELEMENT VOLUME=  ',e12.6,/,&
       & 5x,'      MIN. ELEMENT VOLUME=  ',e12.6,' (ELEMENT ',i9,')',/,&
       & 5x,'      MAX. ELEMENT VOLUME=  ',e12.6,' (ELEMENT ',i9,')',/,&
       & 5x,'      ..................... ',  /,&
       & 5x,'      TOTAL ELEMENTS=       ',i9,/,&
       & 5x,'      EXTENSION ELEMENTS=   ',i9,/,&
       & 5x,'      HOLE ELEMENTS=        ',i9,/,&
       & 5x,'      ELEMENTS USED:        ',a,/,&
       & 5x,'      GAUSS POINTS (RULE)=  ',a,/,&
       & 5x,'      ..................... ',  /,&
       & 5x,'      TOTAL BOUNDARIES=     ',i9,/,&
       & 5x,'      EXTENSION BOUNDARIES= ',i9,/,&
       & 5x,'      HOLE BOUNDARIES=      ',i9,/,&
       & 5x,'      BOUNDARIES USED:      ',a,/,&
       & 5x,'      GAUSS POINTS (RULE)=  ',a,/,&
       & /)
1702 format(&
       & 5x,'   ----------------------',/&
       & 5x,'|- SUMMARY OF CONVERGENCE',/,&
       & 5x,'   ----------------------',/)
1800 format(//,&
       & 5x,'   --------------------------',/&
       & 5x,'|- SUMMARY OF COMPUTING TIMES',/,&
       & 5x,'   --------------------------',/,&
       & /,&
       & /,&
       & 5x,'   TOTAL CPU TIME:              ',F10.2,/,&
       & 5x,'   STARTING OPERATIONS:         ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Reading and partition:     ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Mesh multiplication:       ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Domain construction:       ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Additional arrays:         ',F10.2,' (',F6.2,' % )')
1900 format(&
       & 5x,'   ',a6,' MODULE:               ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Matrix construction:       ',/,&
       & 5x,'          Average:              ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'          Maximum:              ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'          Load balance:         ',F10.2,/,&
       & 5x,'     Algebraic solver:          ',/,&
       & 5x,'          Average:              ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'          Maximum:              ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'          Load balance:         ',F10.2,/,&
       & 5x,'     Postprocess:               ',/,&
       & 5x,'          Average:              ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'          Maximum:              ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'          Load balance:         ',F10.2)
2000 format(&
       & 5x,'   OUTPUT OPERATIONS:           ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'   OTHER OPERATIONS:            ',F10.2,' (',F6.2,' % )')
2001 format(&
       & 5x,'   OTHER OPERATIONS:            ',F10.2,' (',F6.2,' % )')
2100 format(//,&
       & 5x,'   --------------------------',/&
       & 5x,'|- SUMMARY OF REQUIRED MEMORY',/,&
       & 5x,'   --------------------------',/,&
       & /,&
       & /)
2102 format(&
       & 5x,'   MASTER COUNT:',/,&
       & 5x,'   -------------',/)
2103 format(/,&
       & 5x,'   SLAVE MAX COUNT:',/,&
       & 5x,'   ----------------',/)
2101 format(&
       & 5x,'   TOTAL MEMORY:               ',f7.2,1x,a6,/,&
       & 5x,'   CONSTRUCTION OF THE DOMAIN: ',f7.2,1x,a6,/,&
       & 5x,'   ELEMENT SEARCH STRATEGY:    ',f7.2,1x,a6 )
2200 format(&
       & 5x,'   ',a6,' MODULE:              ',f7.2,1x,a6 )
2300 format(&
       & 5x,'   ',a6,' SERVICE:             ',f7.2,1x,a6 )
2400 format(&
       & 5x,'   SOLVERS:                    ',f7.2,1x,a6 )
2500 format(/,&
       & 5x, '   ',a,/,&
       & 5x, '|- ',a,/,&
       & 5x, '   ',a,/)
2800 format(/,&
       & 5x,'|- ',a6,' IS STATIONARY AT TIME STEP ',i5)
  !
  ! ----------------------------------------------- COMPUTING TIMES FORMATS 
  !
2900 format(//,&
       & 5x,'   --------------------------',/&
       & 5x,'|- SUMMARY OF COMPUTING TIMES',/,&
       & 5x,'   --------------------------',/,&
       & //,&
       & 5x,'   TOTAL CPU TIME:            ',f10.2)
3000 format(&
       & 5x,'   ',a27,f10.2,' (',f6.2,' % )')
3100 format(&
       & 5x,'     ',a25,f10.2,' (',f6.2,' % )')
  !
  ! ---------------------------------------------- COMPUTING TIMES FORMATS 
  !
3200 format(&
       & '# Iterations  = ',3(1x,i6),/,&
       & '# Time        = ',e16.8E3)
3300 format(&
       & '# NUMVARIABLES : ',i7,/,&
       & '# NUMSETS      : ',i7,/,&
       & '# START')
3400 format(&
       & '# ALYA ',a,' set results',/,&
       & '#',/,&
       & '# HEADER')
3500 format('# ',a,' , Column : ',i3)

3600 format(//,&
       & 5x,'   -----------------',/&
       & 5x,'|- SUMMARY OF SOLVER',/,&
       & 5x,'   -----------------',/,&
       & /)
3700 format(&
       & 5x,'   ',a,':',/,&
       & 5x,'   ',a,/,&
       & /,&
       & 5x,'   SOLVER:               ',a,/,&
       & 5x,'   # SOLVES=             ',i8)
3701 format(&
       & 5x,'   MIN. # ITERATIONS=    ',i8,/,&
       & 5x,'   MAX. # ITERATIONS=    ',i8,/,&
       & 5x,'   AVE. # ITERATIONS=    ',i8,/,&
       & 5x,'   TOT. # ITERATIONS=    ',i8,/,&
       & 5x,'   TOT. CPU TIME=        ',e16.8E3)
3702 format(&
       & 5x,'   ',a22,i8)
3703 format(&
       & 5x,'   ',a22,a)
3704 format(&
       & 5x,'   ',a22,e12.6)
3705 format(//,&
       & 5x,'   SUMMARY OF SOLVER TIMES:',/,&
       & /,&
       & 5x,'   TOTAL SOLVER CPU TIME:        ',e12.6,/,&
       & 5x,'     Initial operations:         ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Preconditioning:            ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Coarse system solver (DCG): ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Orthogonalization (GMRES):  ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Others:                     ',e12.6,' (',F6.2,' % )')

3706 format(//,&
       & 5x,'   TOTAL SOLVER CPU TIME:        ',e12.6,/,&
       & 5x,'     Invert Aii:                 ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Compute preconditioner:     ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Factorize preconditioner:   ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Solver initialization:      ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Solver main loop. q = A p:  ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Solver main loop. descent:  ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Solver main loop. L z = q:  ',e12.6,' (',F6.2,' % )',/,&
       & 5x,'     Solver main loop. others:   ',e12.6,' (',F6.2,' % )')

3800 format(//,&
       & 5x,'   --------------------',/&
       & 5x,'|- SUMMARY OF PARTITION',/,&
       & 5x,'   --------------------',//,&
       & 5x,'   # SUBDOMAINS=         ',i8,/,&
       & 5x,'   TOTAL # NODES=        ',i8,' (INTERIOR+REPEATED BOUNDARY)',//,&
       & 5x,'   MIN. # NPOIN=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   MAX. # NPOIN=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   AVE. # NPOIN=         ',i8,/,&
       & 5x,'   MIN. # NELEM=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   MAX. # NELEM=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   AVE. # NELEM=         ',i8,/,&
       & 5x,'   MIN. # NELEM(WEIGHT)= ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   MAX. # NELEM(WEIGHT)= ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   AVE. # NELEM(WEIGHT)= ',i8,/,&
       & 5x,'   MIN. # NBOUN=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   MAX. # NBOUN=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   AVE. # NBOUN=         ',i8,//,&
       & 5x,'   MIN. # NNEIG=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   MAX. # NNEIG=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   AVE. # NNEIG=         ',i8,/,&
       & 5x,'   MIN. # NBBOU=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   MAX. # NBBOU=         ',i8,' (SUBDOMAIN ',i5,')',/,&
       & 5x,'   AVE. # NBBOU=         ',i8)

3903 format(&
       & '#',/,&
       & '# -----------------',/,&
       & '# ALGEBRAIC SOLVER:',/,&
       & '# -----------------',/,&
       & '# ',/,&
       & '# TYPE OF SOLVER        : ',a,/,&
       & '# --------------')
3904 format(&
       & '# RELAXATION            = ',e12.6)
3905 format(&
       & '# KRYLOV DIMENSION      = ',i10)
3906 format(&
       & '# TOLERANCE             = ',e10.4)
3907 format(&
       & '# MAX. ITERATIONS       = ',i10)
3908 format(&
       & '# NUMBER OF GROUPS      = ',i10)
3909 format(&
       & '# TOLERANCE OF LINELETS = ',e12.6,/,&
       & '# NUMBER OF LINELETS    = ',i10,/,&
       & '# CROSSED BY BOUNDARIES = ',i10,/,&
       & '# LINELET NODES         = ',i10,' %')
3910 format(&
       & '# NON-ZERO COEFF.       = ',i10,' (R*',i1,')')
3911 format(&
       & '# COARSE SOLVER         = ',a)
3912 format(&
       & '# COARSE RHS COMMUNIC.  = ',a)
3913 format(&
       & '#',/,&
       & '# COARSE CORRECTION     : ',/,&
       & '# -----------------')
3914 format(&
       & '# COARSE SPACE SIZE     = ',i10)
3929 format(&
       & '#',/,&
       & '# THIS IS A TIME RESTART RUN: CONTINUING... ',/,'#')
3920 format(&
       & '#',/,&
       & '# PRECONDITIONER        : ',a,/,&
       & '# --------------')
3921 format(&
       & '# COARSE SYSTEM SOLVER  : ',a)
3922 format(&
       & '# NUMBER OF GROUPS      = ',i10)
3923 format(&
       & '# PRECONDITIONER        = ',a)
3928 format(&
       & '# BLOCK GAUSS-SEIDEL    : ',a)
3924 format(&
       & '# SMOOTHER              = ',a)
3925 format(&
       & '# SMOOTHER ITERATIONS   = ',i10)
3926 format(&
       & '# AII SOLVER            = ',a)
3927 format(&
       & '# ABB SOLVER            = ',a)
3931 format(&
       & '# LEFT/RIGHT            : ',a)
3930 format(&
       & '#',/,&
       & '# --| Columns displayed:' ,/,&
       & '# --| 1. Number of iter.        2. Initial prec. residual   ',/,&
       & '# --| 3. Final prec. residual.  4. Initial residual         ',/,&
       & '# --| 5. Final residual         6. Convergence rate         ',/,& 
       & '# --| 7. CPU time               8. Orthogonality            ',/,& 
       & '#   ','  1','             2','             3','             4', &
       &              '             5','             6','             7', &
       &              '             8',/,&
       & '#')

3940 format(&
       & '#',/,&
       & '# --| Columns displayed:' ,/,&
       & '# --| 1. Number of iter.        2. Prec. residual           ',/,&
       & '# --| 3. Residual               ',/,&
       & '# ','    1','            2','            3',/,&
       & '#')

4000 format(//,&
       & 5x,'   -----------------------',/&
       & 5x,'|- SUMMARY OF SVN REVISION',/,&
       & 5x,'   -----------------------',/,&
       & /,&
       & /)
4001 format(5x,'   ',a)
4100 format(//,&
       & 5x,'   -------------------',/&
       & 5x,'|- SUMMARY OF MAKEFILE',/,&
       & 5x,'   -------------------',/,&
       & /)
4101 format(8x,'   ',a)
4102 format(/,&
       & 5x,'   |- USER AND SYSTEM:',/)
4103 format(/,&
       & 5x,'   |- F90 AND C COMPILERS:',/)

4200 format(//,&
       & 5x,'   ------------------------',/&
       & 5x,'|- BOUNDARY CONDITION CODES',/,&
       & 5x,'   ------------------------',/,&
       & /)
4301 format(&
       & 5x,'   CODE=            ',(i2))
4302 format(&
       & 5x,'   CODE=            ',i2,'  & ',i2)
4303 format(&
       & 5x,'   CODE=            ',i2,'  & ',i2,'  & ',i2)
!4301 format(&
!       & 5x,'   NUMBER OF NODES= ',i10)

4400 format(//,&
       & 5x,'   ---------------------------',/  &
       & 5x,'|- GEOMETRICAL CONDITION NODES',/, &
       & 5x,'   ---------------------------',//,&
       & 5x,'   PRESCRIBED=       ',i10,/,&
       & 5x,'   FREESTREAM=       ',i10,/,&
       & 5x,'   WALL LAW=         ',i10,/,&
       & 5x,'   WALL LAW (LINE)=  ',i10,/,&
       & 5x,'   SYMMETRY=         ',i10,/,&
       & 5x,'   SYMMETRY (LINE)=  ',i10,/,&
       & 5x,'   NO SLIP=          ',i10,/,&
       & 5x,'   FREE/OUTFLOW=     ',i10)

4500 format(//,&
       & 5x,'   BAD EDGES:        ',i10,/,&
       & 5x,'   LAST EDGE FOUND:  ',i10,'-',i10,/,&
       & 5x,'   LAST BOUNDARY=    ',i10)
4600 format(&
       & '# Iteration  = ',1(1x,i6),/,&
       & '# Time       = ',e16.8E3)
4900 format(&
       & /,&
       & /,&   
       & 5x,'   MESH MULTIPLICATION:        ',F10.2,/,&
       & 5x,'     Edge table:               ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Face table:               ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Element divisor:          ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Parall, own nodes:        ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Parall, common edges:     ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Parall, common faces:     ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Parall, boundaries:       ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Parall, loc. renumerbing: ',F10.2,' (',F6.2,' % )',/,&
       & 5x,'     Parall, glo. numbering:   ',F10.2,' (',F6.2,' % )')
5000 format(//,&
       & 5x,'   --------------',/&
       & 5x,'|- WITNESS POINTS',/,&
       & 5x,'   --------------',/)
5100 format(5x,i9)
5101 format(//,&
       & 9x,'SOME WITNESS POINTS DO NOT HAVE HOST ELEMENTS:')
5200 format(//,&
       & 9x,'ALL WITNESS POINTS HAVE FOUND HOST ELEMENTS')
5399 format('# --| ALYA Lagrangian postprocess  ' ,/,&
       & '# --| Columns displayed:' ,/,&
       & '# --|  1. Current time          2. Particle number        3. x            ',/,&
       & '# --|  4. y                     5. z                      6. vx           ',/,&
       & '# --|  7. vy                    8. vz                     9. Type number  ',/,&
       & '# --| 10. Subdomain            11. ax                    12. ay           ',/,&
       & '# --| 13. az                   14. Cd                    15. Re           ',/,&
       & '# --| 16. vfx                  17. vfy                   18. vfz          ',/,&
       & '# --| 19. adx                  20. ady                   21. adz          ',/,&
       & '# --| 22. aex                  23. aey                   24. aez          ',/,&
       & '# --| 25. agx                  26. agy                   27. agz          ',/,&
       & '# --| 28+ physical properties')
5300 format(&
       & '# ','         1','        2','             3',                                 &
       &      '             4','             5','             6','             7',       &
       &      '             8','        9','       10','            11','            12')
5301 format(&
       & '# ','         1','        2','             3',                                 &
       &      '             4','             5','             6','             7',       &
       &      '             8','        9','       10','            11','            12',&
       &      '            13','            14')
5302 format(&
       & '# ','         1','        2','             3',                                 &
       &      '             4','             5','             6','             7',       &
       &      '             8','        9','       10','            11','            12',&
       &      '            13','            14','            15','            16',       &
       &      '            17')
5303 format(&
       & '# ','         1','        2','             3',&
       &      '             4','             5','             6','             7',       &
       &      '             8','        9','       10','            11','            12',&
       &      '            13','            14','            15','            16',       &
       &      '            17','            18','            19','            20',       &
       &      '            21','            22','            23','            24',       &
       &      '            25','            26')
5304 format(&
       & '# ','         1','        2','             3',&
       &      '             4','             5','             6','             7',       &
       &      '             8','        9','       10','            11','            12',&
       &      '            13','            14','            15','            16',       &
       &      '            17','            18','            19','            20',       &
       &      '            21','            22','            23','            24',       &
       &      '            25','            26','        extras')
5400 format('# --| ALYA Lagrangian convergence  ' ,/,&
       & '# --| Columns displayed:' ,/,&
       & '# --|  1. Current time          2. Accumulated particles  3. Existing particles',/,&
       & '# --|  4. Deposited particles   5. Particles with dt=0    6. CPU time max      ',/,&
       & '# --|  7. CPU time max          8. Load balance           9. Max particles sent',/,&
       & '#---| 10. Max particles recv   11. Comm. loops           ',/,&
       & '# ','         1','          2','        3','        4','        5','             6',&
       &      '             7','             8','        9','       10','       11') 

5600 format(//,&
       & 5x,'   ---------------------------',/&
       & 5x,'|- OTHER TASKS COMPUTING TIMES',/,&
       & 5x,'   ---------------------------')

5700 format('Current time,Particle number,Particle type,x,y,z,wall(-2)/outflow(-4)')
!5700 format('# --| ALYA Lagrangian deposition  ' ,/,&
!       & '# --| Columns displayed:' ,/,&
!       & '# --|  1. Current time          2. Particle number        3. Particle type',/,&
!       & '# --|  3. x                     4. y                      5. z')
!5701 format(&
!       & '# ','         1','        2','        3',                                 &
!       &      '                4','             5')
5800 format(//,&
       & 5x,'   -----------------------------',/  &
       & 5x,'|- PROJECTION ON SUPPORT SURFACE',/, &
       & 5x,'   -----------------------------',//,&
       & 5x,'   MIN DISLACEMENT = ',e12.6,' % ',/,&
       & 5x,'   MAX DISLACEMENT = ',e12.6,' % ',/,&
       & 5x,'   AVE DISLACEMENT = ',e12.6,' % ')

5900 format(//,&
       & 5x,'   -----------------------------',/  &
       & 5x,'|- WAKE MODEL INFORMATION'       ,/, &
       & 5x,'   -----------------------------',//)
6000 format(&
       & /,&   
       & 5x,'   WIND TURBINE:               ',i4,/,&
       & 5x,'     A_disk=                   ',1(1x,f15.3),/,&
       & 5x,'     d_ref=                    ',1(1x,f15.7),/,&
       & 5x,'     n_disk=                   ',3(1x,f15.7),/,&
       & 5x,'     x_disk=                   ',3(1x,f15.7),/,&
       & 5x,'     x_ref=                    ',3(1x,f15.7),/,&
       & 5x,'     V_disk=                   ',1(1x,f15.3),/,&
       & 5x,'     d_disk=                   ',1(1x,f15.7))
 
6100 format(&
       & 8x,a31,i9)
6200 format(&
       & 8x,a31,f6.2)
6300 format(//,&
       & 5x,'   -------------------',/&
       & 5x,'|- ZONE-WISE PARTITION',/,&
       & 5x,'   -------------------',/)
6400 format(&
       & 5x,'   ZONE, # SUBDOMAINS    ',i3,',',i8)
6500 format(&
       & /,&   
       & 5x,'   STEP:                 ',i4,/,&
       & 5x,'   WIND TURBINE:         ',i4,/,&
       & 5x,'   Ct =                  ',1(1x,f13.6),/,&
       & 5x,'   Cp =                  ',1(1x,f13.6),/,&
       & 5x,'   Veinf =               ',1(1x,f13.6),/,&
       & 5x,'   Power(W) =            ',1(1x,f17.6))
6600 format(//,&
       & 5x,'   ------------------------',/,&
       & 5x,'|- INTER COLOR COMMUNICATOR',/,&
       & 5x,'   ------------------------',/)
6601 format(&
       & /,&   
       & 5x,'   TOTAL NUMBER OF CODES=      ',i4,/,&
       & 5x,'   TOTAL NUMBER OF ZONES=      ',i4,/,&
       & 5x,'   TOTAL NUMBER OF SUBDS=      ',i4,/,&
       & /,&
       & 5x,'   NUMBER OF ZONES IN MY CODE= ',i4,/,&
       & 5x,'   NUMBER OF SUBDS IN MY CODE= ',i4,/,&
       & 5x,'                               ',/,&
       & 5x,'   LIST OF COMMUNICATORS:      ',/)
6700 format(&
       & 5x,'   ',a,'(',i3,',',i3,'), ',a,'(',i3,',',i3,'), ',a,'(',i3,',',i3,')')
          
6800 format(//,&
       & 5x,'   ------------------------',/,&
       & 5x,'|- PARTITIONS BIN STRUCTURE',/,&
       & 5x,'   ------------------------',/,&
       & /,&   
       & 5x,'   BIN DIMENSIONS=             ',i5,i5,i5)
6900 format(/,&
       & 5x,'   MAXIMUM OVER SLAVES:        ',/&
       & 5x,'   --------------------        ',//&
       & 5x,'   TOTAL MEMORY:               ',f7.2,1x,a6,/&
       & 5x,'   MAXIMUM MEMORY REQUIRED:    ',f7.2,1x,a6)

7000 format(//,&
       & 5x,'   -----------------------------',/,&
       & 5x,'|- LOAD BALANCE & COMMUNICATIONS',/,&
       & 5x,'   -----------------------------',/,&
       & /,&   
       & 5x,'    # % elements % boundaries % neighbors')
7100 format(&
       & 5x,3x,3(1x,e12.6))
7200 format(//,&
       & 5x,'   ------',/,&
       & 5x,'|- OPENMP',/,&
       & 5x,'   ------',/,&
       & /,&   
       & 5x,'   NUMBER OF THREADS=          ',i7,/,&
       & 5x,'   NUMBER OF BLOCKS=           ',i7,/,&
       & 5x,'   AVERAGE ELEMENT CHUNK=      ',i7,/,&
       & 5x,'   AVERAGE BOUNDARY CHUNK=     ',i7,/,&
       & 5x,'   AVERAGE NODE CHUNK=         ',i7,/,&
       & 5x,'   AVERAGE # OF COLORS=        ',i7)
7203 format(&
       & 5x,'    #  Color  number_of_elements')
  
7201 format(&
       & 5x,3x,2(1x,i9))
7300 format(//,&
       & 5x,'   --------------------',/,&
       & 5x,'|- OPTIMIZATION OPTIONS',/,&
       & 5x,'   ---------------------',/,&
       & /,&   
       & 5x,'   MPI3:                       ',a3,/&
       & 5x,'   BLAS:                       ',a3)

end subroutine outfor

