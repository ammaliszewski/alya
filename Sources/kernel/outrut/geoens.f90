subroutine geoens()
  !-----------------------------------------------------------------------
  !****f* outrut/geoens
  ! NAME 
  !    engold_initia
  ! DESCRIPTION
  !    This routine initializes Ensight:
  !    ITASK=1 ... Initial values and open files
  !    ITASK=2 ... Define parts characteristics
  ! USES
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_elmtyp
  use def_domain
  use mod_iofile
  use def_postpr
  implicit none
  integer(ip)             :: ipart,ipoin,ielem,jelem,inode,idime,ielty
  integer(ip)             :: lnugo
  real(rp)                :: xauxi
  character(40)           :: chens
  character(10)           :: cengo

  if( kfl_rstar /= 2 ) then
     !
     ! Write case file
     !   
     call engold_wrcase(1_ip,'NULL')
     !
     ! Write mesh
     !
     parts_pos(1)%npoin = npoin
     chens=adjustl(trim(namda))
     write(lun_outpu_dom,15) 'Problem name:  ',adjustl(trim(chens))
     write(lun_outpu_dom,10) 'Geometry file  '
     write(lun_outpu_dom,10) 'node id given'
     write(lun_outpu_dom,10) 'element id given'
     do ipart=1,npart_pos
        write(lun_outpu_dom,10) 'part'
        write(lun_outpu_dom,20) ipart
        write(lun_outpu_dom,10) parts_pos(ipart)%name
        write(lun_outpu_dom,10) 'coordinates'
        write(lun_outpu_dom,20) parts_pos(ipart)%npoin
        !
        ! Coordinates
        !
        do ipoin=1, parts_pos(ipart)%npoin
           write(lun_outpu_dom,20) ipoin
        end do
        do idime=1, ndime
           do ipoin=1, parts_pos(ipart)%npoin
              write(lun_outpu_dom,30) coord(idime,ipoin)
           end do
        end do
        if (ndime.eq.2) then
           xauxi= 0.0_rp
           do ipoin=1, parts_pos(ipart)%npoin
              write(lun_outpu_dom,30) xauxi
           end do
        end if
        !
        ! Volume elements
        !
        do ielty=iesta_dom,iesto_dom
           if(lexis(ielty)>0) then
              cengo = cenal(ielty)
              lnugo = lnuty(ielty)
              if (cenal(ielty)=='tri3')  cengo = 'tria3'
              if (cenal(ielty)=='pyra5') cengo = 'pyramid5'

              if (cenal(ielty)=='quad9') then
                 cengo = 'quad4'     ! convert quad9 to four quad4
                 lnugo = 4*lnugo

                 write(lun_outpu_dom,10) trim(cengo)
                 write(lun_outpu_dom,20) lnugo
                 jelem= 1
                 do ielem=1,nelem
                    if (ltype(ielem) == ielty) then
                       write(lun_outpu_dom,20) jelem
                       write(lun_outpu_dom,20) jelem+1
                       write(lun_outpu_dom,20) jelem+2
                       write(lun_outpu_dom,20) jelem+3
                    end if
                    jelem= jelem+4
                 end do
                 do ielem=1,nelem
                    if (ltype(ielem) == ielty) then
                       write(lun_outpu_dom,25) lnods(1,ielem),lnods(5,ielem),lnods(9,ielem),lnods(8,ielem)
                       write(lun_outpu_dom,25) lnods(5,ielem),lnods(2,ielem),lnods(6,ielem),lnods(9,ielem)
                       write(lun_outpu_dom,25) lnods(8,ielem),lnods(9,ielem),lnods(7,ielem),lnods(4,ielem)
                       write(lun_outpu_dom,25) lnods(9,ielem),lnods(6,ielem),lnods(3,ielem),lnods(7,ielem)
                    end if
                 end do
              else
                 write(lun_outpu_dom,10) trim(cengo)
                 write(lun_outpu_dom,20) lnugo
                 do ielem=1,nelem
                    if (ltype(ielem) == ielty) then
                       write(lun_outpu_dom,20) ielem                    
                    end if
                 end do
                 do ielem=1,nelem
                    if (ltype(ielem) == ielty) then
                       write(lun_outpu_dom,25) (lnods(inode,ielem) , inode=1,nnode(ielty))
                    end if
                 end do
              end if

           end if
        end do

     end do

     call flush(lun_outpu_dom)

  end if

10 format(a)
15 format(2a)
20 format(i10)
25 format(20i10)
30 format(e12.5)

end subroutine geoens
