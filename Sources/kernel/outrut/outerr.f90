subroutine outerr(itask)
  !------------------------------------------------------------------------
  !****f* master/outerr
  ! NAME 
  !    outerr
  ! DESCRIPTION
  !    This routine checks if there are errros and warnings
  ! USES
  ! USED BY
  !    Turnon
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_kermod
  use def_domain
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ierro=0,iwarn=0,imodu,iorde,itype
  integer(ip)             :: korde(mmodu)
  character(20)           :: messa

  ierro = 0
  iwarn = 0

  select case ( itask )

  case ( 0_ip )
     !
     ! Check compatibility of data read in rrudat and readat
     !
     if( kfl_outfo == 50 .and. kfl_servi(ID_HDFPOS) == 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,lun_outpu,&
             'SERVICE HDFPOS MUST BE COMPILED IF POSTPROCESS FORMAT IS HDF')
     end if
     !
     ! Velocity function
     !
     if( kfl_vefun /= 0 .and. kfl_modul(ID_NASTIN) + kfl_modul(ID_NASTAL) > 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,lun_outpu,&
             'CANNOT SOLVE NASTIN OR NASTAL AND USE A VELOCITY FUNCTION')        
     end if

  case ( 1_ip )

     if( INOTSLAVE ) then
        !
        ! Check if a problem is put to on that it is solved
        !
        korde=0
        do iblok=1,nblok
           do imodu=1,mmodu
              iorde=lmord(imodu,iblok)
              if(iorde>0) then
                 korde(iorde)=korde(iorde)+1
              end if
           end do
        end do
        do imodu=1,mmodu-1
           if(kfl_modul(imodu)/=0.and.korde(imodu)==0) then
              ierro=ierro+1
              messa=intost(imodu)
              call outfor(1_ip,lun_outpu,&
                   'MODULE '//trim(messa)//' IS SOLVED BUT DOES NOT APPEAR'//&
                   ' IN BLOCK DEFINITION')
           end if
        end do
        !
        ! Postprocess
        !
        if(kfl_postp_par==0.and.kfl_outfo==30.and.kfl_paral==0) then
           ierro=ierro+1
           call outfor(1_ip,lun_outpu,&
                'CANNOT POSTPROCESS ON SLAVES USING VU FORMAT')        
        end if
        !
        ! Witness points
        !
        if(nwitn/=0.and.kfl_elses==0) then
           ierro=ierro+1
           nwitn=0
           call outfor(1_ip,lun_outpu,&
                'DEFINE ELSEST FIELD TO OUTPUT WITNESS POINTS')
        end if
     end if

  end select
  !
  ! Stop
  !
  messa = intost(ierro)
  if( ierro == 1 ) then
     call runend('OUTERR: '//trim(messa)//' ERROR HAS BEEN FOUND')
  else if( ierro >= 2 ) then
     call runend('OUTERR: '//trim(messa)//' ERRORS HAVE BEEN FOUND')
  end if

end subroutine outerr
