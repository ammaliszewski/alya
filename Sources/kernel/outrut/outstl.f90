!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    outstl.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Output boudnary mesh
!> @details Output boudnary mesh in STL format
!> @} 
!-----------------------------------------------------------------------
subroutine outstl()
  use def_kintyp,         only :  ip,rp,i1p
  use def_kintyp,         only :  comm_data_par
  use def_master,         only :  INOTMASTER,lninv_loc
  use def_master,         only :  INOTSLAVE
  use def_master,         only :  lun_tempo
  use def_master,         only :  namda
  use def_master,         only :  zeror
  use def_master,         only :  kfl_paral
  use def_master,         only :  intost
  use def_kermod,         only :  kfl_oustl
  use def_domain,         only :  cenam,ndime,nboun
  use def_domain,         only :  ltypb,lnnob,coord,npoin
  use def_domain,         only :  lnodb

  use def_domain,         only :  nelem,mnode,mnodb,nelty
  use def_domain,         only :  lnods,lnnod,ltype,nnodf
  use def_domain,         only :  nface,pelpo,lelpo,lface
  use def_domain,         only :  mface,ltypf

  use def_elmtyp,         only :  TRI03,QUA04
  use mod_communications, only :  PAR_DEFINE_COMMUNICATOR
  use mod_communications, only :  PAR_GATHER
  use mod_communications, only :  PAR_GATHERV
  use mod_parall,         only :  PAR_CODE_SIZE
  use mod_iofile,         only :  iofile
  use mod_graphs,         only :  graphs_list_faces
  use mod_graphs,         only :  graphs_deallocate_list_faces
  use mod_boundary_coarsening, only: boundary_coarsening,boundary_coarsening_graph
  implicit none
  type(comm_data_par), pointer :: commu
  integer(ip)                  :: idime,istl
  integer(ip)                  :: pblty,iboun,inodb,ipoin
  integer(ip)                  :: ipart,ipoi1,ipoi2,ipoi3
  integer(ip)                  :: ifacg,ielem,iface,ielty
  integer(4)                   :: nstl4,nstl4_tot
  real(rp)                     :: vec(3,3),vnor,rboun
  integer(4),          pointer :: nstl4_gat(:)
  real(rp),            pointer :: xstl_gat(:)
  real(rp),            pointer :: xstl(:,:)
  character(150)               :: fil_tempo

  integer(ip)                  :: nfacg
  integer(ip),         pointer :: lfacg(:,:) 
  type(i1p),           pointer :: lelfa(:)   

  integer(ip)                  :: nboun_coarse          !< Number of boundaries
  integer(ip)                  :: npoin_coarse          !< Number of nodes
  integer(ip),         pointer :: lnodb_coarse(:,:)     !< Boundary connectivity
  real(rp),            pointer :: coord_coarse(:,:)     !< Boundary connectivity

  integer(ip)                  :: nbounThreshold
  integer(ip)                  :: coarseningPoints
  integer(ip)                  :: maxBoundElems

  select case ( kfl_oustl ) 

  case ( 1_ip )

     !-------------------------------------------------------------------
     !
     ! Master outputs the geometrical STL using lnodb
     !
     !-------------------------------------------------------------------

     if( ndime == 3 ) then

        call livinf(0_ip,'OUTPUT BOUNDARY MESH IN STL FORMAT',0_ip)
        nullify(nstl4_gat)
        nullify(xstl_gat)
        nullify(xstl)
        nstl4 = 0 
        !call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE,commu)
        !
        ! Open file
        !
        fil_tempo = adjustl(trim(namda))//'.stl'
        call iofile(0_ip,lun_tempo,fil_tempo,'SYSTEM INFO')
        !
        ! Gather all STL 
        !
        if( INOTSLAVE ) allocate( nstl4_gat(0:PAR_CODE_SIZE-1) )
        !
        ! Workers convert Alya format to STL 
        !
        if( INOTMASTER ) then
           do iboun = 1,nboun
              pblty = ltypb(iboun)
              if( pblty == TRI03 ) then
                 nstl4  = nstl4 + 1
              else if( pblty == QUA04 ) then
                 nstl4  = nstl4 + 2
              end if
           end do
           allocate( xstl(9,nstl4) )
           nstl4 = 0
           do iboun = 1,nboun
              pblty = ltypb(iboun)
              if( pblty == TRI03 ) then
                 nstl4  = nstl4 + 1
                 idime = 1
                 do inodb = 1,lnnob(iboun)
                    ipoin = lnodb(inodb,iboun)              
                    xstl(idime:idime+2,nstl4) = coord(1:ndime,ipoin)
                    idime = idime + 3
                 end do
              else if( pblty == QUA04 ) then
                 nstl4  = nstl4 + 1
                 idime = 1
                 ipoi1 = lnodb(1,iboun)              
                 ipoi2 = lnodb(2,iboun)              
                 ipoi3 = lnodb(3,iboun)              
                 xstl(idime:idime+2,nstl4) = coord(1:ndime,ipoi1) ; idime = idime + 3
                 xstl(idime:idime+2,nstl4) = coord(1:ndime,ipoi2) ; idime = idime + 3
                 xstl(idime:idime+2,nstl4) = coord(1:ndime,ipoi3)
                 nstl4  = nstl4 + 1
                 idime = 1
                 ipoi1 = lnodb(1,iboun)              
                 ipoi2 = lnodb(3,iboun)              
                 ipoi3 = lnodb(4,iboun)              
                 xstl(idime:idime+2,nstl4) = coord(1:ndime,ipoi1) ; idime = idime + 3
                 xstl(idime:idime+2,nstl4) = coord(1:ndime,ipoi2) ; idime = idime + 3
                 xstl(idime:idime+2,nstl4) = coord(1:ndime,ipoi3) 
              else
                 call runend('OUTSTL: CANNOT GENERATE STL FOR THIS BOUNDARY TYPE '//cenam(pblty))
              end if
           end do
        end if
        !
        ! Gather all STL
        ! 
        nstl4 = 9_4*nstl4
        call PAR_GATHER(nstl4,nstl4_gat,'IN MY CODE')

        if( INOTSLAVE ) then
           nstl4_tot = 0
           do ipart = 0,PAR_CODE_SIZE-1
              nstl4_tot = nstl4_tot + nstl4_gat(ipart)
           end do
           allocate( xstl_gat(nstl4_tot) )
        end if

        call PAR_GATHERV(xstl,xstl_gat,nstl4_gat,'IN MY CODE')
        !
        ! Master outputs STL
        !
        write(lun_tempo,'(a)') 'solid mesh_boundary' 
        if( INOTSLAVE ) then
           nstl4 = 0
           do ipart = 0,PAR_CODE_SIZE-1
              do istl = 1,nstl4_gat(ipart)/9
                 vec(1,1) = xstl_gat(nstl4+4) - xstl_gat(nstl4+1)
                 vec(2,1) = xstl_gat(nstl4+5) - xstl_gat(nstl4+2)
                 vec(3,1) = xstl_gat(nstl4+6) - xstl_gat(nstl4+3)
                 vec(1,2) = xstl_gat(nstl4+7) - xstl_gat(nstl4+1)
                 vec(2,2) = xstl_gat(nstl4+8) - xstl_gat(nstl4+2)
                 vec(3,2) = xstl_gat(nstl4+9) - xstl_gat(nstl4+3)
                 call vecpro(vec(1,1),vec(1,2),vec(1,3),ndime)
                 call vecnor(vec(1,3),ndime,vnor,2_ip)
                 vec(1:3,3) = vec(1:3,3) / ( vnor + zeror )
                 write(lun_tempo,1)     'facet normal',vec(1:3,3)
                 write(lun_tempo,'(a)') 'outer loop'
                 write(lun_tempo,1)     'vertex',xstl_gat(nstl4+1:nstl4+3)
                 write(lun_tempo,1)     'vertex',xstl_gat(nstl4+4:nstl4+6)
                 write(lun_tempo,1)     'vertex',xstl_gat(nstl4+7:nstl4+9)
                 write(lun_tempo,'(a)') 'endloop'
                 write(lun_tempo,'(a)') 'endfacet'
                 nstl4 = nstl4 + 9
              end do
           end do
           write(lun_tempo,'(a)') 'endsolid mesh_boundary'
           deallocate( nstl4_gat )
           deallocate( xstl_gat  )
        else
           deallocate( xstl )
        end if

        call flush(lun_tempo)
        close(lun_tempo)

     end if

  case ( 2_ip )

     !-------------------------------------------------------------------
     !
     ! Each slaves output its STL including subdomain interfaces
     !
     !-------------------------------------------------------------------

     if( INOTMASTER ) then
        nullify(lfacg)
        nullify(lelfa)
        call graphs_list_faces(&
             nelem,mnode,mnodb,nelty,mface,lnods,lnnod,ltype,&
             nnodf,lface,nface,pelpo,lelpo,nfacg,lfacg,lelfa)

        nbounThreshold = 1000
        rboun = real(nboun,rp)
        !maxBoundElems = 100000
        !coarseningPoints = maxBoundElems*(tanh( nboun/dfloat(maxBoundElems) ) +1)/2.0
        if(nboun>nbounThreshold) then!!!!
           !
           if(nboun<1e4) then
              coarseningPoints = 2 + 1*(int(tanh(rboun/1000.0-1),ip)  +1)/2
           else if(nboun<1e5) then
              coarseningPoints = 4 + 2*(int(tanh(rboun/10000.0-1),ip) +1)/2
           else
              coarseningPoints = 6 + 4*(int(tanh(rboun/100000.0-1),ip)+1)/2
           end if

           !-***default call
           !            call boundary_coarsening_graph(&
           !                nelem,mnode,mnodb,nelty,mface,lnods,lnnod,ltype,&
           !                nnodf,lface,nface,pelpo,lelpo,nfacg,lfacg,lelfa,&
           !                ndime,npoin,coord,&
           !                nboun_coarse,npoin_coarse,lnodb_coarse,coord_coarse)!Default:'divide',10
           !-*** call with optional arguments
           !-** 'divide' requires the factor to compute npoin_coarse=npoin/factor
           call boundary_coarsening_graph(&
                nelem,mnode,mnodb,nelty,mface,lnods,lnnod,ltype,&
                nnodf,lface,nface,pelpo,lelpo,nfacg,lfacg,lelfa,&
                ndime,npoin,coord,&
                nboun_coarse,npoin_coarse,lnodb_coarse,coord_coarse,&
                'divide',coarseningPoints)
           !-** 'npoin_coarse' provides npoin_coarse (approximately)
           !            call boundary_coarsening_graph(&
           !                nelem,mnode,mnodb,nelty,mface,lnods,lnnod,ltype,&
           !                nnodf,lface,nface,pelpo,lelpo,nfacg,lfacg,lelfa,&
           !                ndime,npoin,coord,&
           !                nboun_coarse,npoin_coarse,lnodb_coarse,coord_coarse,&
           !                'npoin_coarse',coarseningPoints)

           fil_tempo = adjustl(trim(namda))//'-'//trim(intost(kfl_paral))//'.stl'
           call iofile(0_ip,lun_tempo,fil_tempo,'SYSTEM INFO')
           write(lun_tempo,'(a)') 'solid subdomain_boundary' 
           do iboun = 1,nboun_coarse
              ipoi1    = lnodb_coarse(1,iboun)
              ipoi2    = lnodb_coarse(2,iboun)
              ipoi3    = lnodb_coarse(3,iboun)
              vec(1,1) = coord_coarse(1,ipoi2) - coord_coarse(1,ipoi1) 
              vec(2,1) = coord_coarse(2,ipoi2) - coord_coarse(2,ipoi1) 
              vec(3,1) = coord_coarse(3,ipoi2) - coord_coarse(3,ipoi1) 
              vec(1,2) = coord_coarse(1,ipoi3) - coord_coarse(1,ipoi1)
              vec(2,2) = coord_coarse(2,ipoi3) - coord_coarse(2,ipoi1)
              vec(3,2) = coord_coarse(3,ipoi3) - coord_coarse(3,ipoi1)
              call vecpro(vec(1,1),vec(1,2),vec(1,3),ndime)
              call vecnor(vec(1,3),ndime,vnor,2_ip)
              vec(1:3,3) = vec(1:3,3) / ( vnor + zeror )
              write(lun_tempo,1)     'facet normal',vec(1:3,3)
              write(lun_tempo,'(a)') 'outer loop'
              write(lun_tempo,1)     'vertex',coord_coarse(1:3,ipoi1)
              write(lun_tempo,1)     'vertex',coord_coarse(1:3,ipoi2)
              write(lun_tempo,1)     'vertex',coord_coarse(1:3,ipoi3)
              write(lun_tempo,'(a)') 'endloop'
              write(lun_tempo,'(a)') 'endfacet'
           end do
           deallocate( coord_coarse )
           deallocate( lnodb_coarse )

        else!!!!

           fil_tempo = adjustl(trim(namda))//'-'//trim(intost(kfl_paral))//'.stl'
           call iofile(0_ip,lun_tempo,fil_tempo,'SYSTEM INFO')
           write(lun_tempo,'(a)') 'solid subdomain_boundary'
           do ifacg = 1,nfacg
              if( lfacg(2,ifacg) == 0 ) then
                 iface = lfacg(3,ifacg)
                 ielem = lfacg(1,ifacg)
                 ielty = ltype(ielem)
                 pblty = ltypf(ielty) % l(iface)
                 if( pblty == TRI03 ) then
                    ipoi1    = lnods(lface(ielty) % l(1,iface),ielem)
                    ipoi2    = lnods(lface(ielty) % l(2,iface),ielem)
                    ipoi3    = lnods(lface(ielty) % l(3,iface),ielem)
                    vec(1,1) = coord(1,ipoi2) - coord(1,ipoi1)
                    vec(2,1) = coord(2,ipoi2) - coord(2,ipoi1)
                    vec(3,1) = coord(3,ipoi2) - coord(3,ipoi1)
                    vec(1,2) = coord(1,ipoi3) - coord(1,ipoi1)
                    vec(2,2) = coord(2,ipoi3) - coord(2,ipoi1)
                    vec(3,2) = coord(3,ipoi3) - coord(3,ipoi1)
                    call vecpro(vec(1,1),vec(1,2),vec(1,3),ndime)
                    call vecnor(vec(1,3),ndime,vnor,2_ip)
                    vec(1:3,3) = vec(1:3,3) / ( vnor + zeror )
                    write(lun_tempo,1)     'facet normal',vec(1:3,3)
                    write(lun_tempo,'(a)') 'outer loop'
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi1)
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi2)
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi3)
                    write(lun_tempo,'(a)') 'endloop'
                    write(lun_tempo,'(a)') 'endfacet'
                 else if( pblty == QUA04 ) then
                    ipoi1    = lnods(lface(ielty) % l(1,iface),ielem)
                    ipoi2    = lnods(lface(ielty) % l(2,iface),ielem)
                    ipoi3    = lnods(lface(ielty) % l(3,iface),ielem)
                    vec(1,1) = coord(1,ipoi2) - coord(1,ipoi1)
                    vec(2,1) = coord(2,ipoi2) - coord(2,ipoi1)
                    vec(3,1) = coord(3,ipoi2) - coord(3,ipoi1)
                    vec(1,2) = coord(1,ipoi3) - coord(1,ipoi1)
                    vec(2,2) = coord(2,ipoi3) - coord(2,ipoi1)
                    vec(3,2) = coord(3,ipoi3) - coord(3,ipoi1)
                    call vecpro(vec(1,1),vec(1,2),vec(1,3),ndime)
                    call vecnor(vec(1,3),ndime,vnor,2_ip)
                    vec(1:3,3) = vec(1:3,3) / ( vnor + zeror )
                    write(lun_tempo,1)     'facet normal',vec(1:3,3)
                    write(lun_tempo,'(a)') 'outer loop'
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi1)
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi2)
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi3)
                    write(lun_tempo,'(a)') 'endloop'
                    write(lun_tempo,'(a)') 'endfacet'
                    ipoi1    = lnods(lface(ielty) % l(1,iface),ielem)
                    ipoi2    = lnods(lface(ielty) % l(3,iface),ielem)
                    ipoi3    = lnods(lface(ielty) % l(4,iface),ielem)
                    vec(1,1) = coord(1,ipoi2) - coord(1,ipoi1)
                    vec(2,1) = coord(2,ipoi2) - coord(2,ipoi1)
                    vec(3,1) = coord(3,ipoi2) - coord(3,ipoi1)
                    vec(1,2) = coord(1,ipoi3) - coord(1,ipoi1)
                    vec(2,2) = coord(2,ipoi3) - coord(2,ipoi1)
                    vec(3,2) = coord(3,ipoi3) - coord(3,ipoi1)
                    call vecpro(vec(1,1),vec(1,2),vec(1,3),ndime)
                    call vecnor(vec(1,3),ndime,vnor,2_ip)
                    vec(1:3,3) = vec(1:3,3) / ( vnor + zeror )
                    write(lun_tempo,1)     'facet normal',vec(1:3,3)
                    write(lun_tempo,'(a)') 'outer loop'
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi1)
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi2)
                    write(lun_tempo,1)     'vertex',coord(1:3,ipoi3)
                    write(lun_tempo,'(a)') 'endloop'
                    write(lun_tempo,'(a)') 'endfacet'
                 else
                    call runend('OUTSTL: CANNOT GENERATE STL FOR THIS BOUNDARY TYPE '//cenam(pblty))
                 end if
              end if
           end do

        end if!!!!!

        write(lun_tempo,'(a)') 'endsolid subdomain_boundary' 
        call flush(lun_tempo) 
        close(lun_tempo)
        call graphs_deallocate_list_faces(lfacg,lelfa)
     end if

  end select

1 format(a,3(1x,e13.6),1x,i7)

end subroutine outstl
