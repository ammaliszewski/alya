subroutine outcpu()
  !-----------------------------------------------------------------------
  !      
  ! This routine writes a summary of spent computing time. The
  ! quantities computed by the code are:      
  !
  ! cpuin   :  Reference time taken at the beginning of the run      
  ! cpstr(1):  Total starting CPU time
  ! cpstr(2):  CPU time for the domain construction      
  ! cpout   :  Output CPU
  !   
  !-----------------------------------------------------------------------
  use def_master
  use def_kermod
  use def_domain
  use def_solver
  use mod_communications, only : PAR_SUM 
  use mod_communications, only : PAR_MAX
  use mod_parall,         only : PAR_CODE_SIZE
  implicit none
  real(rp)    :: cpu_refer,cpu_minim,cpu_denom
  real(rp)    :: cpu_modut,cpu_servt,cpu_multi,cpu_total
  integer(ip) :: imodu,ipass
  real(rp)    :: cpu_ave_assembly(mmodu) , cpu_max_assembly(mmodu)
  real(rp)    :: cpu_ave_solver(mmodu)   , cpu_max_solver(mmodu)
  real(rp)    :: cpu_ave_post(mmodu)     , cpu_max_post(mmodu)

  !----------------------------------------------------------------------
  !
  ! Compute maximum times over slaves for assembly, colver and output
  !
  !----------------------------------------------------------------------

  do imodu = 1,mmodu 
     if( kfl_modul(imodu) /= 0 ) then
        cpu_ave_assembly(imodu) = cpu_modul(CPU_ASSEMBLY,imodu)
        cpu_max_assembly(imodu) = cpu_modul(CPU_ASSEMBLY,imodu)
        call PAR_MAX(cpu_max_assembly(imodu),'IN MY CODE')
        call PAR_SUM(cpu_ave_assembly(imodu),'IN MY CODE')
        cpu_ave_assembly(imodu) = cpu_ave_assembly(imodu) / max(1.0_rp,real(PAR_CODE_SIZE-1,rp))

        cpu_ave_solver(imodu) = cpu_modul(CPU_SOLVER,imodu)+cpu_modul(CPU_EIGEN_SOLVER,imodu)
        cpu_max_solver(imodu) = cpu_modul(CPU_SOLVER,imodu)+cpu_modul(CPU_EIGEN_SOLVER,imodu)
        call PAR_MAX(cpu_max_solver(imodu),'IN MY CODE')
        call PAR_SUM(cpu_ave_solver(imodu),'IN MY CODE')
        cpu_ave_solver(imodu) = cpu_ave_solver(imodu) / max(1.0_rp,real(PAR_CODE_SIZE-1,rp))

        cpu_ave_post(imodu) = cpu_modul(ITASK_OUTPUT,imodu)
        cpu_max_post(imodu) = cpu_modul(ITASK_OUTPUT,imodu)
        call PAR_MAX(cpu_max_post(imodu),'IN MY CODE')
        call PAR_SUM(cpu_ave_post(imodu),'IN MY CODE')
        cpu_ave_post(imodu) = cpu_ave_post(imodu) / max(1.0_rp,real(PAR_CODE_SIZE-1,rp))
     end if
  end do
  !
  ! Mesh multiplication
  !
  if( ndivi > 0 ) then
     call PAR_MAX(9_ip,cpu_other,'IN MY CODE')
  end if
  !
  ! Starting operations
  ! 
  call PAR_MAX(4_ip,cpu_start,'IN MY CODE')

  !----------------------------------------------------------------------
  !
  ! Write CPU times
  !
  !----------------------------------------------------------------------

  if( INOTSLAVE ) then
     !
     ! Initializations
     !
     call cputim(cpu_refer)
     cpu_modut = 0.0_rp
     cpu_servt = 0.0_rp
     ipass     = 0
     !
     ! Total CPU and CPU for starting operations
     !
     cpu_total =   cpu_refer - cpu_initi
     routp( 1) =   cpu_total

     routp( 2) =   cpu_start(CPU_READ_DOMAIN)      + cpu_start(CPU_MESH_MULTIPLICATION) &
          &      + cpu_start(CPU_CONSTRUCT_DOMAIN) + cpu_start(CPU_ADDTIONAL_ARRAYS)
     routp( 3) = 100.0_rp * routp(2) / cpu_total

     routp( 4) = cpu_start(CPU_READ_DOMAIN)
     routp( 5) = 100.0_rp * routp( 4) / cpu_total
     routp( 6) = cpu_start(CPU_MESH_MULTIPLICATION) 
     routp( 7) = 100.0_rp * routp( 6) / cpu_total
     routp( 8) = cpu_start(CPU_CONSTRUCT_DOMAIN)
     routp( 9) = 100.0_rp * routp( 8) / cpu_total
     routp(10) = cpu_start(CPU_ADDTIONAL_ARRAYS)
     routp(11) = 100.0_rp * routp(10) / cpu_total
     call outfor(18_ip,lun_outpu,' ')
     !
     ! Module times
     !
     do imodu = 1,mmodu 
        if( kfl_modul(imodu) /= 0 ) then
           cpu_modut  = cpu_max_assembly(imodu) + cpu_max_solver(imodu) + cpu_max_post(imodu)
           cpu_minim  = 1.0e-6_rp
           cpu_denom  = max(cpu_modut,cpu_minim)
           coutp(1)   = namod(imodu)

           routp(1)   = cpu_modut                              ! Total time
           routp(2)   = 100.0_rp*routp(1)/cpu_total

           routp(3)   = cpu_ave_assembly(imodu)                ! Matrix construction average
           routp(4)   = 100.0_rp*routp(3)/cpu_denom

           routp(5)   = cpu_max_assembly(imodu)                ! Matrix construction max
           routp(6)   = 100.0_rp*routp(5)/cpu_denom

           routp(7)   = cpu_ave_solver(imodu)                  ! Solvers average
           routp(8)   = 100.0_rp*routp(7)/cpu_denom 

           routp( 9)  = cpu_ave_solver(imodu)                  ! Solvers max
           routp(10)  = 100.0_rp*routp(9)/cpu_denom 

           routp(11)   = cpu_ave_post(imodu)                   ! Post average
           routp(12)   = 100.0_rp*routp(11)/cpu_denom 

           routp(13)  = cpu_ave_post(imodu)                    ! Post max
           routp(14)  = 100.0_rp*routp(13)/cpu_denom 

           call outfor(-19_ip,lun_outpu,' ')     
        end if
     end do
     !
     ! Mesh multiplication
     !
     !if( ndivi > 0 .and. .not. PART_AND_WRITE() ) then
     !   if( ipass == 0 ) call outfor(-56_ip,lun_outpu,' ')
     !   ipass      = 1
     !   cpu_multi  =   cpu_other(1) + cpu_other(2) + cpu_other(3) &
     !        &       + cpu_other(4) + cpu_other(5) + cpu_other(6) &
     !        &       + cpu_other(7) + cpu_other(8) + cpu_other(9)
     !   cpu_multi  = max(zeror,cpu_multi)
     !   routp( 1)  = cpu_multi
     !   routp( 2)  = cpu_other(1)
     !   routp( 3)  = 100.0_rp*routp( 2)/cpu_multi
     !   routp( 4)  = cpu_other(2)
     !   routp( 5)  = 100.0_rp*routp( 4)/cpu_multi
     !   routp( 6)  = cpu_other(3)
     !   routp( 7)  = 100.0_rp*routp( 6)/cpu_multi
     !   routp( 8)  = cpu_other(4)
     !   routp( 9)  = 100.0_rp*routp( 8)/cpu_multi
     !   routp(10)  = cpu_other(5)
     !   routp(11)  = 100.0_rp*routp(10)/cpu_multi
     !   routp(12)  = cpu_other(6)
     !   routp(13)  = 100.0_rp*routp(12)/cpu_multi
     !   routp(14)  = cpu_other(7)
     !   routp(15)  = 100.0_rp*routp(14)/cpu_multi
     !   routp(16)  = cpu_other(8)
     !   routp(17)  = 100.0_rp*routp(16)/cpu_multi
     !   routp(18)  = cpu_other(9)
     !   routp(19)  = 100.0_rp*routp(18)/cpu_multi
     !   call outfor(49_ip,lun_outpu,' ')
     !end if

  end if
  
end subroutine outcpu
