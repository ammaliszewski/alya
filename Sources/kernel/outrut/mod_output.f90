module mod_output

  use def_kintyp
  interface output
     module procedure outsca,outvec,outmat
  end interface
    
contains
  
  subroutine outsca(bridge,ittim,ttime,lunit)
    
    !-----------------------------------------------------------------------
    !
    ! Write results in ascii format
    !
    !-----------------------------------------------------------------------
    implicit none
    real(rp),     intent(in)  :: bridge(:)
    integer(ip) , intent(in)  :: ittim,lunit
    real(rp)    , intent(in)  :: ttime
    integer(ip)               :: npoin,ipoin
    
    npoin=size(bridge)      
    write(lunit,100) ittim,ttime
    do ipoin = 1,npoin
       write(lunit,101) ipoin, bridge(ipoin)
    end do
    call flush(lunit)
!
! Formats
!
100 format(/,&
         & 5x,'>>>  TIME STEP= ',i5,', CURRENT TIME= ',e16.8e3,//,&
         & 5x     ,'     Point',5x,'Temperature')
101 format(8x,i7,5x,e16.8e3)
    
  end subroutine outsca

  subroutine outvec(bridge,ittim,ttime,lunit)
    
    !-----------------------------------------------------------------------
    !
    ! Write results in ascii format
    !
    !-----------------------------------------------------------------------
    implicit none
    real(rp),     intent(in)  :: bridge(:,:)
    integer(ip) , intent(in)  :: ittim,lunit
    real(rp)    , intent(in)  :: ttime
    integer(ip)               :: ndime,npoin,idime,ipoin

    ndime=size(bridge,1)
    npoin=size(bridge,2)
    write(lunit,100) ittim,ttime
    do ipoin = 1,npoin
       write(lunit,101) ipoin,(bridge(idime,ipoin),idime=1,ndime)
    end do
    call flush(lunit)
!
! Formats
!
100 format(/,&
         & 5x,'>>>  TIME STEP= ',i5,', CURRENT TIME= ',e16.8e3,//,&
         & 5x     ,'     Point',5x,'Temperature')
101 format(8x,i7,5x,e16.8e3,5x,e16.8e3)

  end subroutine outvec


  subroutine outmat(bridge,ittim,ttime,lunit)
    
    !-----------------------------------------------------------------------
    !
    ! Write results in ascii format
    !
    !-----------------------------------------------------------------------
    implicit none
    real(rp),     intent(in)  :: bridge(:,:,:)
    integer(ip) , intent(in)  :: ittim,lunit
    real(rp)    , intent(in)  :: ttime
    integer(ip)               :: ndime,npoin,ncomp,idime,ipoin


    ndime=size(bridge,1)
    npoin=size(bridge,2)
    ncomp=size(bridge,3)
	if(ncomp/=1) call runend('OUTMAT: WRONG DIMENSION IN BRIDGE')
    write(lunit,100) ittim,ttime
    do ipoin = 1,npoin
       write(lunit,101) ipoin,(bridge(idime,ipoin,1),idime=1,ndime)
    end do
    call flush(lunit)
!
! Formats
!
100 format(/,&
         & 5x,'>>>  TIME STEP= ',i5,', CURRENT TIME= ',e16.8e3,//,&
         & 5x     ,'     Point',5x,'Temperature')
101 format(8x,i7,5x,e16.8e3,5x,e16.8e3)

  end subroutine outmat



end module mod_output
