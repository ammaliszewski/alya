!-----------------------------------------------------------------------
!> @addtogroup Outrut
!> @{
!> @file    livinf.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966   
!> @brief   Echo a given message on screen
!> @details Echo a given message on screen
!> @} 
!-----------------------------------------------------------------------
subroutine livinf(itask,message,inume)
 
  !-----------------------------------------------------------------------
  !****f* outrut/livinf
  ! NAME
  !    livinf
  ! DESCRIPTION
  !    This routine write live information on run.
  ! USES
  ! USED BY
  !    Reapro
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  implicit none
  integer(ip),   intent(in) :: itask,inume
  character(*)              :: message
  character(300)            :: messa,dumml,mess1,mess2,mess3
  character(25)             :: wspac
  character(3)              :: whea1
  character(10)             :: whea2
  integer(ip)               :: ii
  character(1)              :: wbyte(3)
  real(rp)                  :: rbyte

  !-------------------------------------------------------------------
  !
  ! Styles
  !
  !-------------------------------------------------------------------

  !wspac = '..................'
  !whea1 = '   '
  !whea2 = '    ALYA  '

  !wspac = '                  '
  !whea1 = '   '
  !whea2 = '    >     '

  !wspac = '                         '
  !whea1 = '   '
  !whea2 = '         '

  wspac = '                         '
  whea1 = '--|'
  whea2 = '--| ALYA '


  !-------------------------------------------------------------------
  !
  ! Form message
  !
  !-------------------------------------------------------------------

  if( INOTSLAVE .and. lun_livei/= 0 ) then

     if((itask==1.and.lun_livei==6).or.(itask==-1.and.lun_livei/=6) ) then
        if(kfl_rstar==0 ) then
           write(lun_livei,3) whea1
           write(lun_livei,3) whea2//'START ALYA FOR PROBLEM: '//trim(title)
           write(lun_livei,3) whea1
           call flush(lun_livei)
           return
        else
           write(lun_livei,3) whea1
           write(lun_livei,3) whea2//'RESTART ALYA FOR PROBLEM: '//trim(title)
           write(lun_livei,3) whea1
           call flush(lun_livei)
           return
        end if
     else if((itask==2.and.lun_livei==6).or.(itask==-2.and.lun_livei/=6) ) then
        messa = 'READ PROBLEM DATA'
     else if( itask == 3 ) then
        messa = 'MODULE DATA'
        inews = 1
     else if( itask == 4 ) then
        if(kfl_timco/=-1 ) then
           messa = 'START TIME STEP '//trim(intost(ittim+1_ip))
           inews = 1
        else
           return
        end if
     else if( itask == 5 ) then 
        if(nblok>1.and.itcou==1 ) then
           messa = 'START BLOCK '//trim(intost(iblok))
           inews = 1
        else
           return
        end if
     else if( itask == 6 ) then
        if(micou(iblok)>1 ) then
           messa = 'START COUPLING ITERATION '//trim(intost(itcou))
           inews = 1
        else
           return
        end if
     else if( itask == 7 ) then
        if(micou(iblok)>1 ) then
           messa = 'END COUPLING ITERATION '
           isect = isect - 3
        else
           return
        end if
     else if( itask == 8 ) then
        if(nblok>1 ) then
           messa = 'END BLOCK'
           isect = isect - 3
        else
           return
        end if
     else if( itask == 9 ) then
        if(kfl_timco/=-1 ) then
           messa = 'END TIME STEP'
           isect = isect - 3
        else
           return
        end if
     else if( itask == 10 ) then
        messa = 'END PROBLEM'
        isect = isect - 3
     else if( itask == 11 ) then
        messa = 'START CONSTRUCT DOMAIN'
        inews = 1
     else if( itask == 12 ) then
        messa = 'READ MESH DATA'
     else if( itask == 13 ) then
        messa = 'END CONSTRUCT DOMAIN'
        isect = isect - 3
     else if( itask == 14 ) then
        if(kfl_ptask==0 ) then
           messa = 'COMPUTE GRAPH'
        else
           messa = 'COMPUTE GRAPH'
        end if
     else if( itask == 15 ) then
        messa = 'SOLVE '//trim(namod(inume))
     else if( itask == 16 ) then
        dumml=intost(inume)
        messa = ' ('//trim(dumml)//')'
      else if( itask == 17 ) then
        messa = 'CHECK ELEMENT  ORDERING'
     else if( itask == 21 ) then
        messa = 'CHECK BOUNDARY ORDERING'
     else if( itask == 22 ) then
        messa = 'CHECK ELEMENT  TYPES'
     else if( itask == 23 ) then
        messa = 'CHECK ELEMENT  CONNECTIVITY'
     else if( itask == 24 ) then
        messa = 'CHECK BOUNDARY TYPES'
     else if( itask == 25 ) then
        messa = 'CHECK BOUNDARY CONNECTIVITY'
     else if( itask == 26 ) then
        messa = 'CHECK CONNECTIVITY BOUNDARY/ELEMENT'
     else if( itask == 27 ) then
        messa = 'READ  ELEMENT  TYPES'
     else if( itask == 28 ) then
        messa = 'READ  ELEMENT  CONNECTIVITY'
     else if( itask == 29 ) then
        messa = 'READ  BOUNDARY CONNECTIVITY AND TYPES'
     else if( itask == 30 ) then
        messa = 'READ  BOUNDARY CONNECTIVITY, TYPES AND ELEMENT CONNECTIVITY'
     else if( itask == 31 ) then
        messa = 'READ MESH ARRAYS'
        inews = 1
     else if( itask == 32 ) then
        messa = 'END READ MESH ARRAYS'
        isect = isect - 3
     else if( itask == 33 ) then
        messa = 'READ  COORDINATES'
     else if( itask == 34 ) then
        messa = 'OUTPUT MESH'
     else if( itask == 35 ) then
        messa = 'READ  BOUNDARY CONNECTIVITY'
     else if( itask == 36 ) then
        messa = 'READ  BOUNDARY TYPES'
     else if( itask == 37 ) then
        messa = 'READ  BOUNDARY/ELEMENT CONNECTIVITY'
     else if( itask == 38 ) then
        messa = 'READ  MESH FROM BINARY FILE'
     else if( itask == 39 ) then
        messa = 'WRITE MESH IN BINARY FILE'
     else if( itask == 40 ) then
        messa = 'COMPUTE EXTERIOR NORMALS'
     else if( itask == 41 ) then
        messa = 'COMPUTE MASS MATRIX'
     else if( itask == 42 ) then
        ! THIS IS THE GENERAL LIVINF, TO WRITE ANYTHING YOU NEED ON SCREEN
        messa=trim(message)
     else if( itask == 43 ) then
        messa = 'SOLVE '//trim(namod(inume))
     else if( itask == 44 ) then
        messa = 'SOLVE '//trim(namod(inume))
     else if( itask == 45 ) then
        messa = 'CREATE BOUNDARY SET MESH'
     else if( itask == 46 ) then
        messa = 'CREATE PLANE MESH'
     else if( itask == 47 ) then
        messa=trim(namod(modul))//': OPEN  '//trim(message)//' FILE DYNAMIC COUPLING'
     else if( itask == 48 ) then
        messa=trim(namod(modul))//': CLOSE '//trim(message)//' FILE DYNAMIC COUPLING'        
     else if( itask == 49 ) then
        messa = 'CONSTRUCT MESH DATA OF SELECTED EXAMPLE'
     else if( itask == 50 ) then
        messa = 'WARNINGS HAVE BEEN FOUND IN MODULE '//trim(namod(modul))
     else if( itask == 51 ) then
        messa = ''//trim(namod(inume))//': READ DATA'
     else if( itask == 52 ) then
        messa = 'END MODULE DATA'
        isect = isect - 3
     else if( itask == 53 ) then
        messa = ''//trim(namod(inume))//': INITIAL SOLUTION'
     else if( itask == 56 ) then
        messa = 'SOLVE '//trim(namod(inume))
     else if( itask == 57 ) then
        messa = ''//trim(namod(inume))//': COMPUTE GROUPS OF DEFLATED CG'
     else if( itask == 58 ) then 
        dumml=intost(inume)
        messa = 'CHECK OPENMP. MAX # THREADS='//trim(dumml)
     else if( itask == 59 ) then
        messa=trim(namod(modul))//': '//trim(message)   
     else if( itask == 60 ) then
        messa = 'GENERATE CARTESIAN MESH'
        inews = 1
     else if( itask == 61 ) then
        messa = 'COMPUTE LOCAL NORMAL BASIS'
     else if( itask == 62 ) then
        messa = 'COMPUTE SYMMETRIC GRAPH'
     else if( itask == 63 ) then
        messa = 'END GENERATE CARTESIAN MESH'
        isect = isect - 3
     else if( itask == 64 ) then
        mess1=intost(ioutp(1))
        mess2=intost(ioutp(2))
        mess3=intost(ioutp(3))
     else if( itask == 65 ) then
        messa = 'READ  HANGING NODES FROM BINARY FILE'
     else if( itask == 66 ) then
        messa = 'READ  IMMERSED BOUNDARY FROM BINARY FILE'
     else if( itask == 67 ) then
        if( inume == -2  ) then
            messa = 'KERNEL: READ LAGRANGIAN PARTICLE RESTART FILE'
        else if( modul == -1  ) then
            messa = 'KERNEL: READ IB RESTART FILE'
        else if( modul == 0  ) then
            messa = 'KERNEL: READ RESTART FILE'
         else
            messa = ''//trim(namod(modul))//': READ RESTART FILE'
         end if
     else if( itask == 68 ) then
        if( inume == -2  ) then
           messa = 'KERNEL: WRITE LAGRANGIAN PARTICLE RESTART FILE'
        else if( modul == -1  ) then
           messa = 'KERNEL: WRITE IB RESTART FILE'
        else if( modul == 0  ) then
           messa = 'KERNEL: WRITE RESTART FILE'
        else
           messa=trim(namod(modul))//': WRITE RESTART FILE'
        end if
     else if( itask == 69 ) then
        messa = 'IMMBOU: COMPUTE IMMERSED BOUNDARY PROPERTIES'
     else if( itask == 70 ) then
        messa=trim(namod(modul))//': COMPUTE FORCES AND TORQUES ON IB'
     else if( itask == 71 ) then
        messa = 'IB GAUSS POINTS FALL ON SUBDOMAIN INTERFACE'
     else if( itask == 72 ) then
        messa = 'SOLVE EULER EQUATIONS FOR IB'
     else if( itask == 73 ) then
        messa = 'ADAPT MESH'
     else if( itask == 75 ) then
        messa = 'INITIAL SOLUTION'
        inews = 1
     else if( itask == 76 ) then
        messa = 'END INITIAL SOLUTION'
        isect = isect - 3
     else if( itask == 77 ) then
        messa = ''//trim(namod(modul))
     else if( itask == 78 ) then
        messa = 'SAVE ELEMENT DATA BASE'
     else if( itask == 79 ) then
        messa = ''//trim(namod(inume))//': '//trim(message) ! End of time step
     else if( itask == 80 ) then
        if(kfl_paral<=0)&
             messa = 'RENUMBER NODES'
     else if( itask == 81 ) then
        messa = ''//trim(namod(modul))//': '//trim(message) ! End of time step
     else if( itask == 82 ) then
        messa = 'IMMERSED BOUNDARY GAUSS POINTS'
     else if( itask == 83 ) then
        messa = 'DIVIDE MESH'
     else if( itask == 84 ) then
        messa = 'RECONSTRUCT BOUNDARY/ELEMENT CONNECTIVITY'
     else if( itask == 85 ) then
        messa = 'END ADAPT MESH'
        isect = isect - 3
     else if( itask == 86 ) then
        mess1=intost(nelem)
        mess2=intost(npoin)
        mess3=intost(nboun)
        messa = 'NEW MESH: ELEMENTS= '//trim(mess1)//', NODES= '//trim(mess2)//', BOUNDARIES= '//trim(mess3)
     else if( itask == 88 ) then
        messa = 'WRITE NEW GEOMETRY IN FILES'
     else if( itask == 89 ) then
        messa = 'MARK ELEMENTS TO BE ADAPTED'
     else if( itask == 90 ) then
        messa = 'COMPUTE GEOMETRICAL NORMALS'
     else if( itask == 91 ) then
        if(kfl_paral<=0)&
             messa = 'RENUMBER ELEMENTS'
     else if( itask == 92 ) then
        messa = 'CONSTRUCT BOUNDARY/ELEMENT CONNECTIVITY'
     else if( itask == 93 ) then
        messa = 'READ  BOUNDARY CONDITIONS'
     else if( itask == 94 ) then
        messa = 'READ  MATERIALS'
     else if( itask == 95 ) then
        messa = 'CHECK PROJECTION'
     else if( itask == 96 ) then
        messa = 'IMMERSED BOUNDARY FRINGE NODES'
     else if( itask == 97 ) then
        continue     
     else if( itask == 98 ) then   ! similar to 69 but for ale
        messa = 'ALEFOR: COMPUTE RIGID BODY PROPERTIES'
     else if( itask == 100 ) then
        messa = 'READ  FIELDS'
    else if( itask == 160 ) then
        messa = ' ('
     else if( itask == 161 ) then
        dumml=intost(inume)
        messa = ' ('//trim(dumml)//','
     else if( itask == 162 ) then
        dumml=intost(inume)
        messa=trim(dumml)//')'
     else if( itask == 163 ) then
        dumml=intost(inume)
        messa=trim(dumml)//','
     else if( itask == 164 ) then
        messa = ')'
     else if( itask == 165 ) then
        messa=trim(message)
     else if( itask == 166 ) then
        messa=trim(message)
     else if( itask == 201 ) then
        isect = isect-3
        write(lun_livei,'(a)') whea2//wspac(1:isect)//'CRITICAL TIME OVERSHOOT'
!        write(lun_livei,'(a)') whea2//wspac(1:isect-3)//'RESETTING TIME STEP '//trim(intost(ittim))
        write(lun_livei,'(a,e16.8E3)') whea2//wspac(1:isect)//'GOING BACK TO TIME t= ',cutim
        messa=trim('RESTARTING TIME STEP '//trim(intost(ittim)))
        isect = isect+3
     else if( itask == 202 ) then
        messa = 'CALL INITIALIZE COPROCESSING'
     else if( itask == 203 ) then
        messa = 'CALL COPROCESSING'
     else if( itask == 204 ) then
        messa = 'CALL FINALIZE COPROCESSING'
     else if( itask == 999 ) then
        !write(6,3) ' '
        messa = 'ABORTED IN |---->   '//trim(message)
     else if( itask == 1000 ) then
        !messa = 'FINISHED NORMALLY'
        messa = 'CALCULATIONS CORRECT'
     end if

     !-------------------------------------------------------------------
     !
     ! Generic messages: to be used preferently
     !
     !-------------------------------------------------------------------

     if( itask == 0  ) then
        write(lun_livei,'(a)') whea2//wspac(1:isect)//trim(message)

     else if( itask == -2  ) then
        !write(lun_livei,'(a,$)') whea2//wspac(1:isect)//trim(message)
        write(lun_livei,'(a)',advance='no') whea2//wspac(1:isect)//trim(message)

     else if( itask == -3  ) then 
        write(lun_livei,'(a)') trim(message)

     else if( itask == -4 ) then
        inews = 1
        write(lun_livei,'(a)') whea2//wspac(1:isect)//trim(message)

     else if( itask == -5 ) then
        isect = isect - 3
        write(lun_livei,'(a)') whea2//wspac(1:isect)//trim(message)

     else if( itask == -6 ) then
        inews = 1
        messa = intost(inume)
        write(lun_livei,'(a)') whea2//wspac(1:isect)//trim(message)//' '//trim(messa)

     else if( itask == -7 ) then

        write(lun_livei,'(a,i7)') whea2//wspac(1:isect)//trim(message)//': ',inume

     else if( itask == -8 ) then

        write(lun_livei,'(a,a1,i7,a1,a,a1,i7)') whea2//wspac(1:isect)//trim(coutp(1)),&
             ' ',ioutp(1),' ',trim(coutp(2)),' ',ioutp(2)

     else if( itask == -9 ) then

        if( inume < 0 ) then
           write(lun_livei,'(a,i7)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message),inume
        else if( inume >= 10**6 ) then
           write(lun_livei,'(a,i7)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message),inume
        else if( inume >= 10**5 ) then
           write(lun_livei,'(a,i6)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message),inume
        else if( inume >= 10**4 ) then
           write(lun_livei,'(a,i5)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message),inume
        else if( inume >= 10**3 ) then
           write(lun_livei,'(a,i4)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message),inume
        else if( inume >= 10**2 ) then
           write(lun_livei,'(a,i3)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message),inume
        else if( inume >= 10 ) then
           write(lun_livei,'(a,i2)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message),inume
        else
           write(lun_livei,'(a,i1)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message),inume
        end if

     else if( itask == -10 ) then

        write(lun_livei,'(a,a1,i7,a1,a,a1,i7)') whea2//wspac(1:isect)//trim(namod(modul))//': '//&
             trim(coutp(1)),' ',ioutp(1),' ',trim(coutp(2)),' ',ioutp(2)

     else if( itask == -11  ) then
        write(lun_livei,'(a)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message)

     else if( itask == -12  ) then
        write(lun_livei,'(a)') ' '
        write(lun_livei,'(a)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(message)
        
     else if( itask == -13 ) then
        isect = isect - 3
        messa = intost(inume)
        write(lun_livei,'(a)') whea2//wspac(1:isect)//trim(message)//' '//trim(messa)

     else if( itask == -14 ) then
        messa = intost(inume)
        write(lun_livei,'(a)') whea2//wspac(1:isect)//trim(message)//' '//trim(messa)

     else if( itask == -15 ) then

        write(lun_livei,'(a,a1,i7)') whea2//wspac(1:isect)//trim(namod(modul))//': '//&
             trim(coutp(1)),' ',ioutp(1)

     else if( itask == -16 ) then

        write(lun_livei,'(a,e12.6)') whea2//wspac(1:isect)//trim(namod(modul))//': '//trim(messaGE)//'= ',routp(1)

     !-------------------------------------------------------------------
     !
     ! Specific messages
     !
     !-------------------------------------------------------------------

     else if( itask == 4 ) then
        write(lun_livei,'(a)',advance='no') whea2//wspac(1:isect)//trim(messa)

     else if( itask == 15.or.itask==44.or.itask==43.or.itask==56 ) then 
        write(lun_livei,'(a,$)') whea2//wspac(1:isect)//trim(messa)
        !write(lun_livei,'(a)',advance='no') whea2//wspac(1:isect)//trim(messa)

     else if( itask == 165.or.itask==161.or.itask==160.or.itask==163 ) then 
        write(lun_livei,'(a,$)') trim(messa)
        !write(lun_livei,'(a)',advance='no') trim(messa)

     else if( itask == 164.or.itask==16 ) then 
        write(lun_livei,'(a)') trim(messa)

     else if( itask == 166 ) then 
        write(lun_livei,3) whea2//wspac(1:isect)//' '

     else if( itask == 17.or.itask==21 ) then 
        write(lun_livei,2) whea2//wspac(1:isect)//trim(messa)

     else if( itask == 18 ) then 
        write(lun_livei,'(a,e16.8E3)') ', t= ',cutim

     else if( itask == 19 ) then 
        write(lun_livei,1) inume

     else if( itask == 20 ) then 
        write(lun_livei,*) 

     else if( itask == 97 ) then
        do ii = 1,3
           if( routp(ii) >= 1.0e9_rp ) then
              rbyte     = 1.0e9_rp
              wbyte(ii) = 'B'
           else if( routp(ii) >= 1.0e6_rp ) then 
              rbyte     = 1.0e6_rp
              wbyte(ii) = 'M'     
           else if( routp(ii) >= 1.0e3_rp ) then 
              rbyte     = 1.0e3_rp
              wbyte(ii) = 'k'       
           else  
              rbyte     = 1.0_rp
              wbyte(ii) = ''     
           end if
           routp(ii) = routp(ii) / rbyte
           ioutp(ii) = int(routp(ii),ip)
        end do
        !mess1 = intost(ioutp(1))//trim(wbyte(1))
        !mess2 = intost(ioutp(2))//trim(wbyte(2))
        !mess3 = intost(ioutp(3))//trim(wbyte(3))
        !messa = ' (NELEM= '//trim(mess1)//', NPOIN= '//trim(mess2)//', NBOUN= '//trim(mess3)//')'
        !write(lun_livei,3) whea2//wspac(1:isect)//trim(message)//' '//adjustl(trim(messa))        

        messa = ' (NELEM= '//trim(mess1)//', NPOIN= '//trim(mess2)//', NBOUN= '//trim(mess3)//')'
        write(lun_livei,5) whea2//wspac(1:isect)//trim(message)//' '&
             //' (NELEM= ',(routp(1)),wbyte(1) &
             //', NPOIN= ',(routp(2)),wbyte(2) &
             //', NBOUN= ',(routp(3)),wbyte(3) &
             //')'

        !mess1 = intost(ioutp(1))
        !mess2 = intost(ioutp(2))
        !mess3 = intost(ioutp(3))
        !messa = ' (NELEM= '//trim(mess1)//', NPOIN= '//trim(mess2)//', NBOUN= '//trim(mess3)//')'
        !write(lun_livei,3) whea2//wspac(1:isect)//trim(message)//' '//adjustl(trim(messa))        

    else if( itask == 98  ) then
        write(lun_livei,'(a,e16.8E3)') whea2//wspac(1:isect)//trim(message),routp(1)

    else if( itask == 99  ) then
        write(lun_livei,'(a,i5)') whea2//wspac(1:isect)//trim(message),ioutp(1)

     else if( itask == 999 ) then 
        write(lun_livei,3) whea1
        write(lun_livei,3) whea2//wspac(1:isect)//adjustl(trim(messa))
        write(lun_livei,3) whea1     

     else if( itask == 1000 ) then
        write(lun_livei,3) whea1
        write(lun_livei,3) whea2//wspac(1:isect)//adjustl(trim(messa))
        write(lun_livei,3) whea1

     else if( itask == 10000 ) then
        write(lun_livei,3) whea1
        messa=whea1//' WARNING: '// adjustl(trim(message))
        write(lun_livei,3) whea2//wspac(1:isect)//adjustl(trim(messa))
        write(lun_livei,3) whea1
        flush(lun_livei)

     else if( itask == -1 ) then
        messa = ''//trim(message)
        write(lun_livei,3) trim(messa)

     else if( itask == 64 ) then
        write(lun_livei,4) '',trim(mess1),' CELLS MARKED FOR REFINEMENT ON ',&
             &             trim(mess2),' TOTAL CELLS FOR ITER ',trim(mess3)

    else

        write(lun_livei,3) whea2//wspac(1:isect)//trim(messa)

     end if
     if(itask>=0) call flush(lun_livei)

  else if(kfl_paral>=-1 ) then

     if( itask == 999 ) then
        write(6,3) ' '
        messa=whea2//'ABORTED. '//trim(message)
        write(6,3) trim(messa)
     end if
     
  end if

  if( inews == 1 ) isect = isect + 3
  inews = 0

1 format(i3,'%...',$)
2 format(a,':',$)
3 format(a)
4 format(a,a11,1x,a,1x,a11,1x,a,1x,a3)
5 format(a,f5.1,a,f5.1,a,f5.1,a)

end subroutine livinf
