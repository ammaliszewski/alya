subroutine outinf
  !-----------------------------------------------------------------------
  !****f* Master/outinf
  ! NAME
  !    outinf
  ! DESCRIPTION
  !    This routine writes some information about the run
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_elmtyp
  use      def_master
  use      def_domain
  use      mod_memchk
  implicit none
  integer(ip)    :: ielty,imodu,ibloc,iorde
  integer(ip)    :: nelem_exte,nelem_hole,ielem
  integer(ip)    :: nboun_exte,nboun_hole,iboun
  integer(ip)    :: npoin_hole,ipoin
  character(100) :: celem,ceint,cblem,cbint,cmodu,cbloc,ctimc
  character(7)   :: crule
  character(20)  :: clnut

  if( kfl_rstar /= 2 .and. .not. READ_AND_RUN() ) then

     if( INOTMASTER ) then
        !
        ! Count elements
        !
        nelem_exte = 0
        nelem_hole = 0
        do ielem = 1,nelem
           if( lelch(ielem) == ELEXT ) nelem_exte = nelem_exte + 1
           if( lelch(ielem) == ELHOL ) nelem_hole = nelem_hole + 1
        end do
        nboun_exte = 0
        nboun_hole = 0
        do iboun = 1,nboun
           if( lboch(iboun) == BOEXT ) nboun_exte = nboun_exte + 1
           if( lboch(iboun) == BOHOL ) nboun_hole = nboun_hole + 1
        end do
        npoin_hole = 0
        do ipoin = 1,npoi1
           if( lnoch(ipoin) == NOHOL ) npoin_hole = npoin_hole + 1
        end do
        do ipoin = npoi2,npoi3
           if( lnoch(ipoin) == NOHOL ) npoin_hole = npoin_hole + 1
        end do
     end if
     call parari('SUM',0_ip,1_ip,nelem_exte)
     call parari('SUM',0_ip,1_ip,nelem_hole)
     call parari('SUM',0_ip,1_ip,nboun_exte)
     call parari('SUM',0_ip,1_ip,nboun_hole)
     call parari('SUM',0_ip,1_ip,npoin_hole)

     if( INOTSLAVE ) then
        !
        ! svn revision and makefile info
        !
        coutp(1) = 'END'
        call infmak()
        call outfor(41_ip,lun_syste,' ')
        coutp(1) = 'END'
        call infsvn()
        call outfor(40_ip,lun_syste,' ')
        !
        ! General data
        !
        cmodu = ''
        cbloc = ''
        do imodu = 1,mmodu
           if( kfl_modul(imodu) /= 0 ) then
              cmodu = trim(cmodu)//trim(namod(imodu))//','
           end if
        end do
        cmodu = trim(cmodu(1:len(trim(cmodu))-1))

        do ibloc = 1,nblok
           cbloc = trim(cbloc)//trim(intost(ibloc))//': '
           do iorde = 1,mmodu
              imodu = lmord(iorde,ibloc)
              if( imodu /= 0 ) then
                 if( kfl_modul(imodu) /= 0 ) then
                    cbloc = trim(cbloc) // trim(namod(imodu)) // ','
                 end if
              end if
           end do
           cbloc = trim(cbloc(1:len(trim(cbloc))))
        end do
        if( len(trim(cbloc))-1 > 0 ) cbloc = trim(cbloc(1:len(trim(cbloc))-1))

        if(kfl_timco==0) then
           ctimc='PRESCRIBED'
        else if(kfl_timco==1) then
           ctimc='MINIMUM OF CRITICAL TIME STEPS'
        else
           ctimc='LOCAL TIME STEP'
        end if
        !
        ! Domain data
        !
        celem=''
        ceint=''
        do ielty = iesta_dom,iesto_dom
           if( lexis(ielty) == 1 ) then
              clnut = intost(lnuty(ielty))
              celem = trim(celem)//trim(clnut)//' '//trim(cenam(ielty))//','
              if( lquad(ielty) == 0 ) then
                 crule = '(open)'
              else
                 crule = '(close)'
              end if
              ceint = trim(ceint)//trim(intost(ngaus(ielty)))&
                   //trim(crule)//','
           end if
        end do
        celem = trim(celem(1:len(trim(celem))-1))
        ceint = trim(ceint(1:len(trim(ceint))-1))
        cblem = ''
        cbint = ''
        do ielty = ibsta_dom,ibsto_dom
           if( lexis(ielty) == 1 ) then
              clnut = intost(lnuty(ielty))
              cblem = trim(cblem)//trim(clnut)//' '//trim(cenam(ielty))//','
              if( lquad(ielty) == 0 ) then
                 crule = '(open)'
              else
                 crule = '(close)'
              end if
              cbint = trim(cbint)//trim(intost(ngaus(ielty)))&
                   //trim(crule)//','
           end if
        end do
        cblem = trim(cblem(1:len(trim(cblem))-1))
        cbint = trim(cbint(1:len(trim(cbint))-1))
        !
        ! Write information
        !
        coutp(1) = adjustl(trim(cmodu))
        coutp(2) = adjustl(trim(cbloc))
        coutp(3) = adjustl(trim(ctimc))
        if( kfl_timco == -1 ) then
           routp(5) = 0.0_rp
           routp(6) = 0.0_rp
           routp(7) = 0.0_rp
        else
           routp(5) = dtime
           routp(6) = timei
           routp(7) = timef
        end if
        ioutp(1)  = ndime
        ioutp(2)  = npoin
        ioutp(11) = npoin_hole
        routp(1)  = vodom
        routp(2)  = voave
        routp(3)  = vomin
        ioutp(3)  = elmin
        routp(4)  = vomax
        ioutp(4)  = elmax
        ioutp(5)  = nelem
        ioutp(6)  = nelem_exte
        ioutp(7)  = nelem_hole
        coutp(4)  = adjustl(trim(celem))
        coutp(5)  = adjustl(trim(ceint))
        ioutp(8)  = nboun
        ioutp(9)  = nboun_exte
        ioutp(10) = nboun_hole
        coutp(6)  = adjustl(trim(cblem))
        coutp(7)  = adjustl(trim(cbint))
        call outfor(17_ip,lun_outpu,' ')
     end if

  end if
  if( INOTSLAVE ) call flush(lun_outpu)

end subroutine outinf
