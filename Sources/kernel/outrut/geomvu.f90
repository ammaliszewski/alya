subroutine geomvu(itask)
  !------------------------------------------------------------------------
  !****f* Domain/geomvu
  ! NAME
  !    connpo
  ! DESCRIPTION
  !    This routine dumps the geometry in VU format. VU units are:
  !    - lun_outpu_dom ... Geometry ASCII file
  !    - lun_pos02 ....... Geometry binary file
  !    - lun_pos00 ....... Result ASCII file
  !    - lun_pos01 ....... Result binary file
  !
  !    FIXME: Uncomment !-b to dump boundary mesh
  ! OUTPUT
  ! USED BY
  !    outdom
  !***
  !------------------------------------------------------------------------
  use def_kintyp 
  use def_parame
  use def_domain
  use def_elmtyp
  use def_master
  use def_inpout
  use def_postpr
  use mod_iofile
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip), pointer    :: lnods_tmp(:,:),label_tmp(:)
  integer(ip), pointer    :: lnodb_tmp(:,:),labbo_tmp(:)
  integer(ip), pointer    :: lnoib_tmp(:,:),labib_tmp(:)
  integer(ip), pointer    :: lnlev_tmp(:)
  integer(ip)             :: inode,ielem,ielty,pnode,jelem
  integer(ip)             :: inodb,iboun,iblty,pnodb,jboun
  integer(ip)             :: idime,ipoin,iposi,nleve,ileve
  integer(ip)             :: iimbo,ipoib,inoib,iboib,kfl_oubou
  integer(ip), pointer    :: ntrad(:)
  integer(4)              :: istat
  character(150)          :: dumml
  character(6)            :: creal,celem
  !
  ! Geometry file header
  !
  if(rp==4) then
     creal='float'
  else
     creal='double'
  end if
  iposi=0_ip
  if( nboun > 0 ) then
     kfl_oubou = 1
  else
     kfl_oubou = 0
  end if

  !----------------------------------------------------------------------
  !
  ! Coordinates
  !
  !----------------------------------------------------------------------

  if( itask == 1 ) then

     iposi=iposi+4_ip
     write(lun_outpu_dom,10) trim(creal),trim(fil_pos02),npoin*ndime,iposi 
     iposi=iposi+4_ip+npoin*ndime*rp
     write(lun_pos02) ((coord(idime,ipoin),idime=1,ndime),ipoin=1,npoin)

  else

     iposi=iposi+4_ip
     write(lun_mshib,10) trim(creal),trim(fil_mshi2),npoib*ndime,iposi 
     iposi=iposi+4_ip+npoib*ndime*rp
     write(lun_mshi2) (((imbou(iimbo)%cooib(idime,ipoib),idime=1,ndime),ipoib=1,imbou(iimbo)%npoib),iimbo = 1,nimbo)

  end if

  !----------------------------------------------------------------------
  !
  ! Allocate memory
  !
  !----------------------------------------------------------------------

  if( itask == 1 ) then

     allocate(lnods_tmp(mnode,nelem),stat=istat)
     call memchk(zero,istat,memor_dom,'LNODS_TMP','geomvu',lnods_tmp)
     allocate(label_tmp(nelem),stat=istat)
     call memchk(zero,istat,memor_dom,'LABEL_TMP','geomvu',label_tmp)
     if(kfl_oubou==1) then
        allocate(lnodb_tmp(mnodb,nboun),stat=istat)
        call memchk(zero,istat,memor_dom,'LNODB_TMP','geomvu',lnodb_tmp)
        allocate(labbo_tmp(nboun),stat=istat)
        call memchk(zero,istat,memor_dom,'LABBO_TMP','geomvu',labbo_tmp)
     end if

  else

     allocate(lnoib_tmp(mnoib,nboib),stat=istat)
     call memchk(zero,istat,memor_dom,'LNOIB_TMP','geomvu',lnoib_tmp)
     allocate(labib_tmp(nboib),stat=istat)
     call memchk(zero,istat,memor_dom,'LABIB_TMP','geomvu',labib_tmp)

  end if

  !----------------------------------------------------------------------
  !
  ! Reordering
  !
  !----------------------------------------------------------------------

  if( itask == 1 ) then
     !
     ! Reorder elements
     ! 
     if( kfl_markm == 2 ) then
        !
        ! Zone is Cartesian level
        !
        pnode = 8
        ielty = HEX08
        if(ndime==2) pnode=4
        if(ndime==2) ielty=QUA04

        call memgen(1_ip,nelem,0_ip) 
        call meshin(4_ip)       
        allocate(lnlev_tmp(1000),stat=istat)
        call memchk(zero,istat,memor_dom,'LNLEV_TMP','geomvu',lnlev_tmp)
        nleve = 0
        do ielem=1,nelem
           if(gisca(ielem)>0) then 
              ileve = gisca(ielem)
              nleve = nleve + 1
              do jelem=1,nelem
                 if( gisca(jelem) == ileve ) then
                    lnlev_tmp(ileve) =  lnlev_tmp(ileve) + 1
                    gisca(jelem)     = -gisca(jelem)
                 end if
              end do
           end if
        end do

        do ielem=1,nelem
           gisca(ielem)=-gisca(ielem)
        end do

        jelem = 0
        do ileve=1,nleve
           if(lnlev_tmp(ileve)/=0) then
              pnode=nnode(ielty)
              do ielem=1,nelem
                 if(gisca(ielem)==ileve) then
                    jelem=jelem+1
                    label_tmp(jelem)=ielem
                    do inode=1,pnode 
                       lnods_tmp(inode,jelem)=lnods(inode,ielem)
                    end do
                 end if
              end do
           end if
        end do

     else
        !
        ! Zone is element type
        !
        jelem=0
        do ielty=iesta_dom,iesto_dom
           pnode=nnode(ielty)
           do ielem=1,nelem
              if(ltype(ielem)==ielty) then
                 jelem=jelem+1
                 label_tmp(jelem)=ielem
                 do inode=1,pnode 
                    lnods_tmp(inode,jelem)=lnods(inode,ielem)
                 end do
              end if
           end do
        end do
     end if
     !
     ! Reorder boundaries
     ! 
     if(kfl_oubou==1) then
        jboun=0
        do iblty=ibsta_dom,ibsto_dom
           pnodb=nnode(iblty)
           do iboun=1,nboun
              if(ltypb(iboun)==iblty) then
                 jboun=jboun+1
                 labbo_tmp(jboun)=iboun
                 do inodb=1,pnodb
                    lnodb_tmp(inodb,jboun)=lnodb(inodb,iboun)
                 end do
              end if
           end do
        end do
     end if

  else
     !
     ! Reorder IB
     ! 
     !if(kfl_oubou==1.and.nboib>0) then
     !   jboun=0
     !   do iblty=ibsta_dom,ibsto_dom
     !      pnodb=nnode(iblty)
     !      do iboun=1,nboib
     !         if(ltyib(iboun)==iblty) then
     !            jboun=jboun+1
     !            labib_tmp(jboun)=iboun
     !            do inodb=1,pnodb
     !               lnoib_tmp(inodb,jboun)=lnoib(inodb,iboun)
     !            end do
     !         end if
     !      end do
     !   end do
     !end if

  end if

  !----------------------------------------------------------------------
  !
  ! Connectivity in *.msh.vubin file
  !
  !----------------------------------------------------------------------

  if( itask == 1 ) then
     !
     ! Element 
     !
     if( kfl_markm == 2 ) then
        !
        ! Zone is Cartesian level
        !
        pnode = 8
        ielty = HEX08
        if(ndime==2) pnode=4
        if(ndime==2) ielty=QUA04

        jelem = 1
        do ileve = 1,nleve
           if(lnlev_tmp(ileve)/=0) then
              iposi=iposi+4_ip
              celem=intost(ileve)
              write(lun_outpu_dom,20) 'int',trim(celem),trim(fil_pos02),lnlev_tmp(ileve)*pnode,iposi
              iposi=iposi+4_ip+lnlev_tmp(ileve)*pnode*ip
              write(lun_pos02) ((lnods_tmp(inode,ielem),inode=1,pnode),ielem=jelem,jelem+lnlev_tmp(ileve)-1)
              jelem=jelem+lnlev_tmp(ileve)
           end if
        end do

     else
        !
        ! Zone is element type
        !
        jelem=1
        do ielty=iesta_dom,iesto_dom
           if(lexis(ielty)/=0) then
              pnode=nnode(ielty)
              iposi=iposi+4_ip
              celem=intost(ielty)
              write(lun_outpu_dom,20) 'int',trim(celem),trim(fil_pos02),lnuty(ielty)*pnode,iposi
              iposi=iposi+4_ip+lnuty(ielty)*pnode*ip
              write(lun_pos02) ((lnods_tmp(inode,ielem),inode=1,pnode),ielem=jelem,jelem+lnuty(ielty)-1)
              jelem=jelem+lnuty(ielty)
           end if
        end do
     end if
     !
     ! Boundary 
     !
     if(kfl_oubou==1) then
        jboun=1
        do iblty=ibsta_dom,ibsto_dom
           if(lexis(iblty)/=0) then
              pnodb=nnode(iblty)
              iposi=iposi+4_ip
              celem=intost(iblty)
              write(lun_outpu_dom,20) 'int',trim(celem),trim(fil_pos02),lnuty(iblty)*pnodb,iposi
              iposi=iposi+4_ip+lnuty(iblty)*pnodb*ip
              write(lun_pos02) ((lnodb_tmp(inodb,iboun),inodb=1,pnodb),iboun=jboun,jboun+lnuty(iblty)-1)
              jboun=jboun+lnuty(iblty)
           end if
        end do
     end if

  else
     !
     ! IB (1 zone for all or 1 zone per IB)
     !
     allocate(ntrad(nimbo),stat=istat)
     ntrad(1) = 0
     do iimbo = 2,nimbo
        ntrad(iimbo) = ntrad(iimbo-1) + imbou(iimbo) % npoib
     end do

     if( 1 == 1 ) then
        iposi = iposi+4_ip
        write(lun_mshib,20) 'int','IB',trim(fil_mshi2),nboib*mnoib,iposi
        iposi = iposi+4_ip+nboib*mnoib*ip
        write(lun_mshi2) ((( imbou(iimbo) % lnoib(inoib,iboib) + ntrad(iimbo), inoib = 1,mnoib),&
             iboib = 1,imbou(iimbo)%nboib ),iimbo = 1,nimbo ) 
     else
        jboun = 1
        do iimbo = 1,nimbo       
           iposi = iposi+4_ip
           celem = intost(iblty)
           write(lun_mshib,20) 'int','IB'//trim(celem),trim(fil_mshi2),imbou(iimbo)%nboib*mnoib,iposi
           iposi = iposi+4_ip+imbou(iimbo)%nboib*mnoib*ip
           write(lun_mshi2) (( imbou(iimbo) % lnoib(inoib,iboib), inoib = 1,mnoib),&
                iboib = 1,imbou(iimbo)%nboib )
           jboun = jboun+imbou(iimbo)%nboib
        end do
     end if

     deallocate(ntrad,stat=istat)

  end if

  !----------------------------------------------------------------------
  !
  ! Labeling 
  !
  !----------------------------------------------------------------------

  if( itask == 1 ) then
     !
     ! Labeling of the elements
     !
     if( kfl_markm == 2 ) then
        !
        ! Zone is Cartesian level
        !
        jelem = 1
        do ileve = 1,nleve
           if( lnlev_tmp(ileve) /= 0 ) then
              iposi = iposi+4_ip
              celem = intost(ileve)
              write(lun_outpu_dom,25) 'int',trim(celem),trim(fil_pos02),lnlev_tmp(ileve),iposi
              iposi = iposi + 4_ip + lnlev_tmp(ileve)*ip
              write(lun_pos02) (label_tmp(ielem),ielem=jelem,jelem+lnlev_tmp(ileve)-1)
              jelem = jelem + lnlev_tmp(ileve)
           end if
        end do
     else
        !
        ! Zone is element type
        !
        jelem = 1
        do ielty = iesta_dom,iesto_dom
           if( lexis(ielty) /= 0 ) then
              iposi = iposi + 4_ip
              celem = intost(ielty)
              write(lun_outpu_dom,25) 'int',trim(celem),trim(fil_pos02),lnuty(ielty),iposi
              iposi = iposi + 4_ip + lnuty(ielty)*ip
              write(lun_pos02) (label_tmp(ielem),ielem=jelem,jelem+lnuty(ielty)-1)
              jelem = jelem + lnuty(ielty)
           end if
        end do
     end if
     !
     ! Labeling of the boundaries
     !
     if( kfl_oubou == 1 ) then
        jboun = 1
        do iblty = ibsta_dom,ibsto_dom
           if( lexis(iblty) /= 0 ) then
              iposi = iposi + 4_ip
              celem = intost(iblty)
              write(lun_outpu_dom,25) 'int',trim(celem),trim(fil_pos02),lnuty(iblty),iposi
              iposi = iposi + 4_ip + lnuty(iblty)*ip
              write(lun_pos02) (labbo_tmp(iboun),iboun=jboun,jboun+lnuty(iblty)-1)
              jboun = jboun + lnuty(iblty)
           end if
        end do
     end if

  else
     !
     ! Labeling of IB
     !
     !if( kfl_oubou == 1 .and. nboib > 0 ) then
     !   jboun = 1
     !   do iblty = ibsta_dom,ibsto_dom
     !      if( lexis(iblty) /= 0 ) then
     !         iposi = iposi+4_ip
     !         celem = intost(iblty)
     !         write(lun_outpu_dom,25) 'int','IB'//trim(celem),trim(fil_pos02),lnuib(iblty),iposi
     !         iposi = iposi + 4_ip + lnuib(iblty)*ip
     !         write(lun_pos02) (labib_tmp(iboun),iboun=jboun,jboun+lnuib(iblty)-1)
     !         jboun = jboun + lnuib(iblty)
     !      end if
     !   end do
     !end if

  end if

  !----------------------------------------------------------------------
  !
  ! Header for *.msh.vu file, definition of zones
  !
  !----------------------------------------------------------------------
  !
  ! Mesh zones 
  !

  if( itask == 1 ) then

     write(lun_outpu_dom,30) 'M_'//trim(title) 
     write(lun_outpu_dom,40)

     if( kfl_markm == 2 ) then
        !
        ! Zone is Cartesian level
        !
        ielty=HEX08
        if(ndime==2) ielty=QUA04
        do ileve=1,nleve 
           if(lnlev_tmp(ileve)/=0) then
              celem=intost(ileve)
              write(lun_outpu_dom,50) trim(celem),trim(cepos(ielty)),&
                   ndime,trim(celem),trim(celem)
           end if
        end do
     else
        !
        ! Zone is element type
        !
        do ielty=iesta_dom,iesto_dom
           if(lexis(ielty)/=0) then
              celem=intost(ielty)
              write(lun_outpu_dom,50) trim(celem),trim(cepos(ielty)),&
                   ndime,trim(celem),trim(celem)        
           end if
        end do
     end if
     if(kfl_oubou==1) then
        do iblty=ibsta_dom,ibsto_dom
           if(lexis(iblty)/=0) then
              celem=intost(iblty)
              write(lun_outpu_dom,50) trim(celem),trim(celem)
           end if
        end do
     end if
     write(lun_outpu_dom,60)

  else
     !
     ! IB: only 1 zone for all
     !
     write(lun_mshib,30) 'M_'//trim(title) 
     write(lun_mshib,40)
     iblty = imbou(1)%ltyib(1)
     write(lun_mshib,51) 'IB',trim(cepos(iblty)),ndime,'IB'
     write(lun_mshib,60)

  end if

  !
  ! Close mesh file
  !
  if( itask == 1 ) then
     
     call iofile(two,lun_outpu_dom,dumml,'MESH') 
     call iofile(two,lun_pos02,dumml,'MESH') 

  else

     call iofile(two,lun_mshib,dumml,'MESH') 
     call iofile(two,lun_mshi2,dumml,'MESH')      

  end if

  !----------------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !----------------------------------------------------------------------

  if( itask == 1 ) then

     if( kfl_markm == 2 ) then
        call memgen(3_ip,nelem,0_ip) 
        call memchk(two,istat,memor_dom,'LNLEV_TMP','geomvu',lnlev_tmp)
        deallocate(lnlev_tmp,stat=istat)
        if(istat/=0) call memerr(two,'LNLEV_TMP','geomvu',0_ip)
     end if
     call memchk(two,istat,memor_dom,'LNODS_TMP','geomvu',lnods_tmp)
     deallocate(lnods_tmp,stat=istat)
     if(istat/=0) call memerr(two,'LNODS_TMP','geomvu',0_ip)
     call memchk(two,istat,memor_dom,'LABEL_TMP','geomvu',label_tmp)
     deallocate(label_tmp,stat=istat)
     if(istat/=0) call memerr(two,'LABEL_TMP','geomvu',0_ip)
     if(kfl_oubou==1) then
        call memchk(two,istat,memor_dom,'LNODB_TMP','geomvu',lnodb_tmp)
        deallocate(lnodb_tmp,stat=istat)
        if(istat/=0) call memerr(two,'LNODB_TMP','geomvu',0_ip)
        call memchk(two,istat,memor_dom,'LABBO_TMP','geomvu',labbo_tmp)
        deallocate(labbo_tmp,stat=istat)
        if(istat/=0) call memerr(two,'LABBO_TMP','geomvu',0_ip)
     end if

  else

     call memchk(two,istat,memor_dom,'LNOIB_TMP','geomvu',lnoib_tmp)
     deallocate(lnoib_tmp,stat=istat)
     if(istat/=0) call memerr(two,'LNOIB_TMP','geomvu',0_ip)
     call memchk(two,istat,memor_dom,'LABIB_TMP','geomvu',labib_tmp)
     deallocate(labib_tmp,stat=istat)
     if(istat/=0) call memerr(two,'LABIB_TMP','geomvu',0_ip)

  end if
  !
  ! Format
  !
10 format('FIELD<',a,'> Coord("',a,'",',i12,',',i1,');')
20 format('FIELD<',a,'> Connec',a,'("',a,'",',i12,',',i12,');')
25 format('FIELD<',a,'> Numer',a,'("',a,'",',i12,',',i12,');')
30 format('MESH ',a,'( ) =')
40 format('{')
50 format(' ZONE Zone',a,'( ',a,',Coord%',i1,',Connec',a,',,Numer',a,' );')
51 format(' ZONE Zone',a,'( ',a,',Coord%',i1,',Connec',a,' );')
60 format('};')

end subroutine geomvu


