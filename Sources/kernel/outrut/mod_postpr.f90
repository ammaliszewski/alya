module mod_postpr
  !-----------------------------------------------------------------------
  !****f* outrut/mod_postpr
  ! NAME
  !   mod_postpr
  ! DESCRIPTION
  !   This routine manages the postprocess
  ! USES
  ! USED BY
  !   output_***
  !   outvar_***
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_elmtyp
  use def_postpr
  implicit none

  private

  integer(ip), parameter :: nwwww=10
  character(5)           :: wwwww(nwwww)
  character(8)           :: wwww8(nwwww)
  integer(4)             :: iiiii(10)
  real(8)                :: rrrrr(10)
  integer(ip)            :: kfl_origi,kfl_permu
  ! vtk variable
  integer(ip)            :: success
  integer(ip)            :: mastervtk = 666,opart

  
  interface postpr 
     module procedure &
          possca,posvec,poisca,poivec,posr3p,&
          posscx,posvex
  end interface
  interface postpr_right_now
     module procedure &
          postpr_right_now_i1,&
          postpr_right_now_r1,&
          postpr_right_now_r2
  end interface postpr_right_now
  interface postgp
     module procedure pogpsc
  end interface

  public :: postpr
  public :: postpr_right_now
  public :: postpr_at_current_time_step

contains

  subroutine postpr_right_now_i1(wopo1,wopo2,wopo3,iscalar)
    character(5),         intent(in) :: wopo1,wopo2,wopo3
    integer(ip), pointer, intent(in) :: iscalar(:)
    character(5)                     :: wopos_loc(3)
    integer(ip)                      :: ipoin,nbound

    if( INOTMASTER ) then
       if( wopo3 == 'NELEM') then
          nbound = nelem
       else
          nbound = npoin
       end if
       call mepost(0_ip,nbound,0_ip)
       do ipoin = 1,nbound
          gescp(ipoin) = real(iscalar(ipoin),rp)
       end do
    end if
    wopos_loc(1) = wopo1
    wopos_loc(2) = wopo2
    wopos_loc(3) = wopo3
    call postpr(gescp,wopos_loc,0_ip,0.0_rp)  
    if( INOTMASTER ) call mepost(2_ip,nbound,0_ip)

  end subroutine postpr_right_now_i1

  subroutine postpr_right_now_r1(wopo1,wopo2,wopo3,rvector)
    character(5),         intent(in) :: wopo1,wopo2,wopo3
    real(rp),    pointer, intent(inout) :: rvector(:)
    character(5)                     :: wopos_loc(3)
    integer(ip)                      :: ipoin,nbound

    wopos_loc(1) = wopo1
    wopos_loc(2) = wopo2
    wopos_loc(3) = wopo3
    call postpr(rvector,wopos_loc,0_ip,0.0_rp)  

  end subroutine postpr_right_now_r1

  subroutine postpr_right_now_r2(wopo1,wopo2,wopo3,rvector)
    character(5),         intent(in) :: wopo1,wopo2,wopo3
    real(rp),    pointer, intent(in) :: rvector(:,:)
    character(5)                     :: wopos_loc(3)
    integer(ip)                      :: ipoin,nbound

    if( INOTMASTER ) then
       if( wopo3 == 'NELEM') then
          nbound = nelem
       else
          nbound = npoin
       end if
       call mepost(0_ip,ndime,nbound)
       do ipoin = 1,nbound
          gevep(1:ndime,ipoin) = rvector(1:ndime,ipoin)
       end do
    end if
    wopos_loc(1) = wopo1
    wopos_loc(2) = wopo2
    wopos_loc(3) = wopo3
    call postpr(gevep,wopos_loc,0_ip,0.0_rp)  
    if( INOTMASTER ) call mepost(2_ip,ndime,nbound)

  end subroutine postpr_right_now_r2

  function postpr_at_current_time_step()
    integer(ip) :: postpr_at_current_time_step
    integer(ip) :: imodu,ivari,itime,iok
    logical(lg) :: if_already

    postpr_at_current_time_step = 0

    do imodu = 1,mmodu
       if( kfl_modul(imodu) /= 0 ) then
          do ivari = 1,nvarp

             if_already = .false.

             if( ittyp == ITASK_ENDRUN ) then
                iok = 1
             else 
                iok = 0
             end if

              if( &
                   ittim >= momod(imodu) % postp(1) % npp_inits .and. &
                   iok == 0 .and. &
                   momod(imodu) % postp(1) % npp_stepi(ivari) > 0 ) then     
                 if( mod(ittim, momod(imodu) % postp(1) % npp_stepi(ivari)) == 0 ) then
                    postpr_at_current_time_step = postpr_at_current_time_step + 1
                    if_already = .true.
                 end if
              end if
              !
              ! At a given time
              !
              if( iok == 0 .and. ( .not. if_already ) ) then  
                 do itime = 1,nvart
                    if(   abs( momod(imodu) % postp(1) % pos_times(itime,ivari)-cutim) < (0.5_rp*dtime) .and. &
                         &     momod(imodu) % postp(1) % pos_times(itime,ivari)        > 0.0_rp) then
                       postpr_at_current_time_step = postpr_at_current_time_step + 1
                       if_already = .true.
                    end if
                 end do 
              end if
              !
              ! At a given time period
              !
              if( cutim >= momod(imodu) % postp(1) % pos_tinit .and. iok == 0 &
                   .and. ( .not. if_already ) ) then  
                 if(    abs(momod(imodu) % postp(1) % pos_times(1,ivari)-cutim) < (0.6_rp*dtime).and.&
                      &     momod(imodu) % postp(1) % pos_perio(ivari)          > 0.0_rp) then
                    postpr_at_current_time_step = postpr_at_current_time_step + 1
                    if_already = .true.
                 end if
              end if

          end do
       end if
    end do
  end function postpr_at_current_time_step

  subroutine possca(bridge,wopos,itste,ttime,worig)

    !-----------------------------------------------------------------------
    !
    ! Postprocess a real scalar
    !
    !-----------------------------------------------------------------------

    implicit none
    character(*), intent(in)            :: wopos(*)
    real(rp),     intent(inout), target :: bridge(:)
    integer(ip) , intent(in)            :: itste
    real(rp)    , intent(in)            :: ttime
    character(5), optional              :: worig
    integer(ip)                         :: pdime
    real(rp)                            :: dummr(2)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    pdime = 1
    kfl_origi = 1
    if( present(worig) ) then
       if( worig == 'ORIGI' ) kfl_origi = 0
    end if
    !
    !
    if( kfl_outfo == 50 ) then
       !
       ! HDFPOS
       !
       wopos_hdf(1) =  wopos(1)
       wopos_hdf(2) =  wopos(2)
       gesca_hdf    => bridge
       call Hdfpos(1_ip)
       !
       ! VTK ( only write postpr not restart, restart is alya)
       !
    else if(( kfl_outfo == 40 .OR. kfl_outfo == 41).AND. kfl_reawr == 0) then 
       if( IMASTER ) then
          call geovtk(dummr,wopos,itste,ttime,pdime)
       else if ( ISLAVE ) then
          call geovtk(bridge,wopos,itste,ttime,pdime)
       else if( ISEQUEN ) then
          call geovtk(bridge,wopos,itste,ttime,pdime)
       endif
    else
       !
       ! Alya
       !
       if( IMASTER ) then
          call posrea( dummr,wopos,itste,ttime,pdime)
       else
          call posrea(bridge,wopos,itste,ttime,pdime)
       end if
       
    end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine possca

  subroutine posvec(bridge,wopos,itste,ttime,kdime,worig)

    !-----------------------------------------------------------------------
    !
    ! Postprocess a real vector. When kdime is present, kdime rules over ndime.
    !
    !-----------------------------------------------------------------------

    implicit none
    character(*), intent(in)            :: wopos(*)
    real(rp),     intent(inout), target :: bridge(:,:)
    integer(ip) , intent(in)            :: itste
    real(rp)    , intent(in)            :: ttime
    integer(ip),  optional              :: kdime
    character(5), optional              :: worig
    integer(ip)                         :: pdime
    real(rp)                            :: dummr(2,2)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    if( present(kdime) ) then
       pdime = kdime 
    else
       pdime = ndime
    end if
    kfl_origi = 1
    if( present(worig) ) then
       if( worig == 'ORIGI' ) kfl_origi = 0
    end if

    if( kfl_outfo == 50 ) then
       !
       ! HDFPOS
       !
       wopos_hdf(1) =  wopos(1)
       wopos_hdf(2) =  wopos(2)
       gevec_hdf    => bridge
       !
       ! VORTEX EXTRACTION HDF5
       !
       if( wopos(3) == 'VORTX' ) then
          call Hdfpos(11_ip)
       else
          call Hdfpos(2_ip) 
       end if
       !
       ! VTK ( only write postpr not restart, restart is alya format)
       !
    else if(( kfl_outfo == 40 .OR. kfl_outfo == 41).AND. kfl_reawr == 0) then  
       if( IMASTER ) then
          call geovtk(dummr,wopos,itste,ttime,pdime)
       else if ( ISLAVE ) then
          call geovtk(bridge,wopos,itste,ttime,pdime)
       else if( ISEQUEN ) then
          call geovtk(bridge,wopos,itste,ttime,pdime)
       endif
    else
       !
       ! Alya
       !
       if( IMASTER ) then
          call posrea( dummr,wopos,itste,ttime,pdime)
       else
          call posrea(bridge,wopos,itste,ttime,pdime)
       end if
		

    end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine posvec

  subroutine poisca(bridge,wopos,itste,ttime,worig)

    !-----------------------------------------------------------------------
    !
    ! Postprocess an integer scalar
    !
    !-----------------------------------------------------------------------

    implicit none
    character(*), intent(in)            :: wopos(*)
    integer(ip),  intent(inout), target :: bridge(:)
    integer(ip) , intent(in)            :: itste
    real(rp)    , intent(in)            :: ttime
    character(5), optional              :: worig
    integer(ip)                         :: pdime
    integer(ip)                         :: dummi(2)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    pdime = 1
    kfl_origi = 1
    if( present(worig) ) then
       if( worig == 'ORIGI' ) kfl_origi = 0
    end if

    if( kfl_outfo == 50 ) then
       !
       ! HDFPOS
       !
       call runend('NOT CODED')
    else if( kfl_outfo == 40 .AND. kfl_outfo == 41 ) then
       !
       ! VTK
       !
       call runend('NOT CODED')
    else
       !
       ! Alya
       !
       if( IMASTER ) then
          call posint( dummi,wopos,itste,ttime,pdime)
       else
          call posint(bridge,wopos,itste,ttime,pdime)
       end if

    end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine poisca

  subroutine poivec(bridge,wopos,itste,ttime,kdime,worig)

    !-----------------------------------------------------------------------
    !
    ! Postprocess an integer vector. When kdime is present, kdime rules over ndime.
    !
    !-----------------------------------------------------------------------

    implicit none
    character(*), intent(in)            :: wopos(*)
    integer(ip),  intent(inout), target :: bridge(:,:)
    integer(ip) , intent(in)            :: itste
    real(rp)    , intent(in)            :: ttime
    integer(ip),  optional              :: kdime
    character(5), optional              :: worig
    integer(ip)                         :: pdime
    integer(ip)                         :: dummi(2,2)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    if( present(kdime) ) then
       pdime = kdime 
    else
       pdime = ndime
    end if
    kfl_origi = 1
    if( present(worig) ) then
       if( worig == 'ORIGI' ) kfl_origi = 0
    end if

    if( kfl_outfo == 50 ) then
       !
       ! HDFPOS
       !
       call runend('NOT CODED')
    else if( kfl_outfo == 40 .AND. kfl_outfo == 41 ) then
       !
       ! VTK
       !
       call runend('NOT CODED')
    else
       !
       ! Alya
       !
       if( IMASTER ) then
          call posint( dummi,wopos,itste,ttime,pdime)
       else
          call posint(bridge,wopos,itste,ttime,pdime)
       end if

    end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine poivec

  subroutine pogpsc(bridge,wopos,itste,ttime)

    !-----------------------------------------------------------------------
    !
    ! NOT USED
    !
    !-----------------------------------------------------------------------
    implicit none
    character(*), intent(in)  :: wopos
    real(rp),     intent(in)  :: bridge(:,:)
    integer(ip) , intent(in)  :: itste
    real(rp)    , intent(in)  :: ttime

  end subroutine pogpsc

  subroutine posr3p(bridge,wopos,itste,ttime,kdime)

    !-----------------------------------------------------------------------
    !
    ! Write a vector in postprocess file
    !
    !-----------------------------------------------------------------------
    implicit none
    character(*), intent(in)              :: wopos(*)
    type(r3p),    intent(inout), target   :: bridge(:)
    integer(ip),  intent(in)              :: itste
    real(rp),     intent(in)              :: ttime
    integer(ip),  intent(in),    optional :: kdime
    integer(ip)                           :: ileng,pleng
    integer(ip)                           :: idim1,idim2,ptota,ielty,ii
    integer(ip)                           :: pdim1,pdim2,itota,pdime

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    if( present(kdime) ) then
       pdime = kdime 
    else
       pdime = 1
    end if

    if( kfl_outfo == 50 ) then
       !
       ! HDFPOS
       !
       wopos_hdf(1) =  wopos(1)
       wopos_hdf(2) =  wopos(2)
       ger3p_hdf   => bridge
       
       call Hdfpos(3_ip)
    else 
       ! 
       ! Type of postprocess
       !
       wwwww(1) = 'ALYA '
       wwwww(2) = 'V0002'
       wwwww(3) = wopos(1)
       wwwww(4) = wopos(2)
       wwwww(5) = 'R3P  '
       wwwww(6) = 'REAL '
       wwwww(7) = '8BYTE'
       if( wopos(2) == 'R3PVE' ) then 
          iiiii(1) = int(ndime,4)
       else
          iiiii(1) = 1_4
       end if
       iiiii(2) = 0_4
       iiiii(4) = int(itste,4)
       rrrrr(1) = ttime
       wopos_pos(1) = wopos(1)
       parii    = NELEM_TYPE
       pleng    = nelem
       if( IMASTER ) pleng = nelem_total

       iiiii(2) = int(pleng,4)
       !
       ! Output solution
       !
       call opfpos(1_ip)
       if( kfl_reawr < 0 ) then
          kfl_reawr = abs(kfl_reawr)
          return
       end if

       call poshea()

       if( ISEQUEN ) then

          if( kfl_reawr /= 1 ) then
             ptota = 0
             do ileng = 1,nelem
                pdim1 = size(bridge(ileng)%a,1)
                pdim2 = size(bridge(ileng)%a,2)
                ptota = ptota + pdim1 * pdim2 
             end do
             write(lun_postp) ( ngaus(ielty),ielty=iesta_dom,iesto_dom)
             write(lun_postp) pleng
             write(lun_postp) ptota
             write(lun_postp) (((bridge(ileng)%a(idim1,idim2,pdime),idim1=1,size(bridge(ileng)%a,1)),&
                  idim2=1,size(bridge(ileng)%a,2)),ileng=1,pleng) 
          else
             read(lun_postp)  ( ii,ielty=iesta_dom,iesto_dom)             
             read(lun_postp)  pleng
             read(lun_postp)  ptota
             read(lun_postp)  (((bridge(ileng)%a(idim1,idim2,pdime),idim1=1,size(bridge(ileng)%a,1)),&
                  idim2=1,size(bridge(ileng)%a,2)),ileng=1,pleng) 
          end if

       else if( IMASTER ) then

          if( kfl_reawr /= 1 ) then
             write(lun_postp) ( ngaus(ielty),ielty=iesta_dom,iesto_dom)
          else if( kfl_reawr == 1 ) then
             read(lun_postp) ( ii,ielty=iesta_dom,iesto_dom)  
          end if

          do kfl_desti_par = 1,npart

             if( kfl_reawr /= 1 ) then
                !
                ! - Master writes postpro without filter
                ! - Master writes restart 
                !
                pleng = nelem_par(kfl_desti_par)
                call pararr('RCV',0_ip,1_ip,ptota)
                write(lun_postp) pleng
                write(lun_postp) ptota
                call mepost(0_ip,ptota,0_ip)
                call pararr('RCV',0_ip,ptota,gescp)
                write(lun_postp) ( gescp(ileng), ileng = 1,ptota )
                call mepost(2_ip,ptota,0_ip)

             else if( kfl_reawr == 1 ) then
                !
                ! - Master reads restart and sends to slave
                !
                pleng = nelem_par(kfl_desti_par)
                read(lun_postp) pleng
                read(lun_postp) ptota
                call mepost(0_ip,ptota,0_ip)
                read(lun_postp) ( gescp(ileng), ileng = 1,ptota )
                call pararr('SND',0_ip,ptota,gescp)
                call mepost(2_ip,ptota,0_ip)             

             end if

          end do

       else if( ISLAVE ) then

          kfl_desti_par = 0

          if( kfl_reawr /= 1 ) then
             !
             ! Slaves without filter
             !
             ptota = 0
             do ileng = 1,nelem
                pdim1 = size(bridge(ileng)%a,1)
                pdim2 = size(bridge(ileng)%a,2)
                ptota = ptota + pdim1 * pdim2 
             end do
             call parari('SND',0_ip,1_ip,ptota)
             call mepost(0_ip,ptota,0_ip)
             itota = 0
             do ileng = 1,nelem
                pdim1 = size(bridge(ileng)%a,1)
                pdim2 = size(bridge(ileng)%a,2)
                do idim2=1,pdim2
                   do idim1=1,pdim1
                      itota = itota + 1
                      gescp(itota) = bridge(ileng)%a(idim1,idim2,pdime)
                   end do
                end do
             end do
             call pararr('SND',0_ip,ptota,gescp)
             call mepost(2_ip,ptota,0_ip)

          else if( kfl_reawr == 1 ) then
             !
             ! Slaves receive 
             !
             ptota = 0
             do ileng = 1,nelem
                pdim1 = size(bridge(ileng)%a,1)
                pdim2 = size(bridge(ileng)%a,2)
                ptota = ptota + pdim1 * pdim2 
             end do
             call mepost(0_ip,ptota,0_ip)
             call pararr('RCV',0_ip,ptota,gescp)
             itota = 0
             do ileng = 1,nelem
                pdim1 = size(bridge(ileng)%a,1)
                pdim2 = size(bridge(ileng)%a,2)
                do idim2=1,pdim2
                   do idim1=1,pdim1
                      itota = itota + 1
                      bridge(ileng)%a(idim1,idim2,pdime) = gescp(itota) 
                   end do
                end do
             end do
             call mepost(2_ip,ptota,0_ip)

          end if

       end if

       call opfpos(2_ip)

    end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine posr3p

  subroutine posscx(bridge,wopos,itste,ttime,kpoin)

    !-----------------------------------------------------------------------
    !
    ! Write a scalar in postprocess file
    !
    !-----------------------------------------------------------------------
    implicit none
    character(*), intent(in)         :: wopos(*)
    complex(rp),  intent(in), target :: bridge(:)
    integer(ip) , intent(in)         :: itste
    real(rp)    , intent(in)         :: ttime
    integer(ip),  optional           :: kpoin

  end subroutine posscx

  subroutine posvex(bridge,wopos,itste,ttime,kdime,worig)

    !-----------------------------------------------------------------------
    !
    ! REAL(:,:)
    !
    !-----------------------------------------------------------------------

    implicit none
    character(*), intent(in)            :: wopos(*)
    complex(rp),  intent(inout), target :: bridge(:,:)
    integer(ip) , intent(in)            :: itste
    real(rp)    , intent(in)            :: ttime
    integer(ip),  optional              :: kdime
    character(5), optional              :: worig
    integer(ip)                         :: pdime

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    if( present(kdime) ) then
       pdime = kdime 
    else
       pdime = ndime
    end if
    kfl_origi = 1
    if( present(worig) ) then
       if( worig == 'ORIGI' ) kfl_origi = 0
    end if

    if( kfl_outfo == 50 ) then
       !
       ! HDFPOS
       !
       call runend('POSVEX: NOT CODED')
       !wopos_hdf(1) =  wopos(1)
       !wopos_hdf(2) =  wopos(2)
       !gevec_hdf    => bridge
       !if( wopos(3) == 'VORTX' ) then
       !   call Hdfpos(11_ip)
       !else
       !   call Hdfpos(2_ip)
       !end if

    else
       !
       ! Alya
       !
       !if( IMASTER ) then
       !   call posrex( dummr,wopos,itste,ttime,pdime)
       !else
       !   call posrex(bridge,wopos,itste,ttime,pdime)
       !end if
	
    end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine posvex
  
  subroutine posrea(bridge,wopos,itste,ttime,pdime)

    !-----------------------------------------------------------------------
    !
    ! Alya format for: REAL(:) and REAL(:,:)
    !
    !-----------------------------------------------------------------------

    implicit none
    character(*), intent(in)    :: wopos(*)
    real(rp),     intent(inout) :: bridge(*)
    integer(ip),  intent(in)    :: itste
    real(rp),     intent(in)    :: ttime
    integer(ip),  intent(in)    :: pdime
    integer(ip)                 :: ileng,idime,pleng,kpoin,ipoin
    integer(ip)                 :: imesh,idofn,qpoin,pdofn

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    !
    ! Type of postprocess
    !
    wwwww(1) = 'ALYA '
    wwwww(2) = 'V0002'
    wwwww(3) = wopos(1)
    wwwww(4) = wopos(2)
    wwwww(5) = wopos(3)
    wwwww(6) = 'REAL '
    wwwww(7) = '8BYTE'
    iiiii(1) = int(pdime,4)
    iiiii(2) = 0_4
    iiiii(4) = int(itste,4)
    rrrrr(1) = ttime

    wopos_pos(1) = wopos(1)

    if( kfl_reawr == 0 ) then
       imesh = kfl_posdi
    else
       imesh = ndivi
    end if
    !
    ! Map the result onto original mesh:
    ! - Only for NPOIN type results
    !
    if( kfl_origi == 1 .and. ( kfl_posdi /= ndivi ) .and. wopos(3) == 'NPOIN' ) then
       kfl_permu = 1
    else
       kfl_permu = 0
    end if

    if( wopos(3) == 'NPOIN' ) then

       parii = NPOIN_TYPE 
       pleng = meshe(imesh) % npoin
       if( IMASTER ) pleng = meshe(imesh) % npoin_total

    else if( wopos(3) == 'NELEM' ) then

       parii = NELEM_TYPE
       pleng = meshe(imesh) % nelem
       if( IMASTER ) pleng = meshe(imesh) % nelem_total 

    else if( wopos(3) == 'NBOUN' ) then

       parii = NBOUN_TYPE
       pleng = meshe(imesh) % nboun
       if( IMASTER ) pleng = meshe(imesh) % nboun_total
       
    else if( wopos(3) == 'VORTX' ) then
       call vortwr()
       ! write that file in vu format : call vortvu()
       return
    else
       call runend('UNDEFINED POSTPROCESS')
    end if
    iiiii(2) = int(pleng,4)
    !
    ! Get filter
    !
    call fildef(3_ip)
    !
    ! Output solution
    !
    call opfpos(1_ip)
    if( kfl_reawr < 0 ) then
       kfl_reawr = abs(kfl_reawr)
       return
    end if

    call poshea()

    if( ISEQUEN ) then

       !-----------------------------------------------------------------
       !
       ! SEQUENTIAL
       !
       !-----------------------------------------------------------------

       if( kfl_reawr == 0 .and. kfl_filte == 0 ) then
          !
          ! Postprocess without filter
          !
          write(lun_postp) pleng
          write(lun_postp) ( bridge(ileng), ileng = 1,pdime*pleng )

       else if( kfl_reawr == 0 .and. kfl_filte /= 0 ) then
          !
          ! Postprocess with filter
          !
          call mepost(0_ip,pdime*meshe(imesh) % npoin,0_ip)
          call mepost(1_ip,meshe(imesh) % npoin,0_ip)
          
          pleng = 0
          do qpoin = 1,meshe(imesh) % npoin
             if( kfl_permu == 1 ) then
                ipoin = lpmsh(qpoin)
             else
                ipoin = qpoin
             end if
             if( gefil(ipoin) > 0 ) then
                pleng = pleng + 1
                giscp(pleng) = ipoin
                pdofn = (pleng-1) * pdime
                idofn = (ipoin-1) * pdime
                do idime = 1,pdime
                   pdofn = pdofn + 1
                   idofn = idofn + 1
                   gescp(pdofn) = bridge(idofn)
                end do
             end if
          end do

          write(lun_postp) pleng
          write(lun_postp) ( giscp(ileng), ileng = 1,pleng )
          write(lun_postp) ( gescp(ileng), ileng = 1,pdime*pleng )

          call mepost(3_ip,meshe(imesh) % npoin,0_ip)
          call mepost(2_ip,pdime*meshe(imesh) % npoin,0_ip)

       else if( kfl_reawr == 2 ) then
          !
          ! Preliminary
          !
          write(lun_postp) pleng
          write(lun_postp) ( bridge(ileng), ileng = 1,pdime*pleng )  
        
       else if( kfl_reawr == 1 ) then
          !
          ! Restart
          !
          read(lun_postp) pleng
          read(lun_postp)  ( bridge(ileng), ileng = 1,pdime*pleng )

       end if

    else if( IMASTER ) then

       !-----------------------------------------------------------------
       !
       ! MASTER
       !
       !-----------------------------------------------------------------

       do kfl_desti_par = 1,npart

          if( kfl_filte == 0 .and. kfl_reawr /= 1 ) then
             !
             ! - Master writes postpro without filter
             ! - Master writes restart 
             !
             if( wopos(3) == 'NPOIN' ) then
                pleng = meshe(imesh) % npoin_par(kfl_desti_par)
             else if( wopos(3) == 'NELEM' ) then
                pleng = meshe(imesh) % nelem_par(kfl_desti_par)
             else if( wopos(3) == 'NBOUN' ) then
                pleng = meshe(imesh) % nboun_par(kfl_desti_par)
             end if
             call mepost(0_ip,pdime*pleng,0_ip)
             call pararr('RCV',0_ip,pdime*pleng,gescp)
             write(lun_postp) pleng
             write(lun_postp) ( gescp(ileng), ileng = 1,pdime*pleng )
             call mepost(2_ip,pdime*pleng,0_ip)

          else if( kfl_reawr == 1 ) then
             !
             ! - Master reads restart and sends to slave
             !
             if( wopos(3) == 'NPOIN' ) then
                pleng = npoin_par(kfl_desti_par)
             else if( wopos(3) == 'NELEM' ) then
                pleng = nelem_par(kfl_desti_par)
             else if( wopos(3) == 'NBOUN' ) then
                pleng = nboun_par(kfl_desti_par)
             end if
             call mepost(0_ip,pdime*pleng,0_ip)
             read(lun_postp) pleng
             read(lun_postp) ( gescp(ileng), ileng = 1,pdime*pleng )
             call pararr('SND',0_ip,pdime*pleng,gescp)
             call mepost(2_ip,pdime*pleng,0_ip)             

          else
             !
             ! - Master writes postpro with filter
             !
             call parari('RCV',0_ip,1_ip,pleng)
             if( pleng > 0 ) then
                call mepost(1_ip,pleng,0_ip)
                call parari('RCV',0_ip,pleng,giscp)
                call mepost(0_ip,pdime*pleng,0_ip)
                call pararr('RCV',0_ip,pdime*pleng,gescp)
                write(lun_postp) pleng
                write(lun_postp) ( giscp(ileng), ileng = 1,pleng )
                write(lun_postp) ( gescp(ileng), ileng = 1,pdime*pleng )
                call mepost(2_ip,pdime*pleng,0_ip)
                call mepost(3_ip,      pleng,0_ip)
             else
                write(lun_postp) pleng
             end if
          end if
       end do

    else if( ISLAVE ) then

       !-----------------------------------------------------------------
       !
       ! SLAVES
       !
       !-----------------------------------------------------------------

       kfl_desti_par = 0

       if( kfl_reawr == 0 .and. kfl_filte == 0 ) then
          !
          ! Slaves without filter for postprocess
          !
          if( kfl_permu == 1 ) then
             call mepost(0_ip,pdime,pleng)
             do ipoin = 1,pleng
                kpoin = lpmsh(ipoin)
                idofn = (kpoin-1)*pdime
                do idime = 1,pdime
                   idofn = idofn + 1
                   gevep(idime,ipoin) = bridge(idofn)                   
                end do
             end do
             call pararr('SND',0_ip,pdime*pleng,gevep)
             call mepost(2_ip,pdime,pleng)
          else
             call pararr('SND',0_ip,pdime*pleng,bridge)
          end if

       else if( kfl_reawr == 0 .and. kfl_filte /= 0 ) then
          !
          ! Slaves with filter for postprocess
          !
          call mepost(0_ip,pdime,meshe(imesh) % npoin)
          call mepost(1_ip,meshe(imesh) % npoin,0_ip)

          pleng = 0
          do qpoin = 1,meshe(imesh) % npoin
             if( kfl_permu == 1 ) then
                ipoin = lpmsh(qpoin)
             else
                ipoin = qpoin
             end if
             if( gefil(ipoin) /= 0 ) then
                kpoin = (ipoin-1) * pdime
                pleng = pleng + 1
                giscp(pleng) = qpoin
                do idime = 1,pdime
                   kpoin = kpoin + 1
                   gevep(idime,pleng) = bridge(kpoin)
                end do
             end if
          end do

          
          call parari('SND',0_ip, 1_ip,pleng)
          if( pleng > 0 ) then
             call parari('SND',0_ip,pleng,giscp)
             call pararr('SND',0_ip,pdime*pleng,gevep)
          end if

          call mepost(3_ip,meshe(imesh) % npoin,0_ip)            
          call mepost(2_ip,pdime,meshe(imesh) % npoin) 

       else if( kfl_reawr == 1 ) then
          !
          ! Slaves receive for restart
          !
          call pararr('RCV',0_ip,pdime*pleng,bridge)

       else if( kfl_reawr == 2 ) then
          !
          ! Slaves send for preliminary
          !
          call pararr('SND',0_ip,pdime*pleng,bridge)

       end if

    end if

    call opfpos(2_ip)

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine posrea

  subroutine posint(bridge,wopos,itste,ttime,pdime)

    !-----------------------------------------------------------------------
    !
    ! Alya format for: INT(:) and INT(:,:)
    !
    !-----------------------------------------------------------------------

    implicit none
    character(*), intent(in)    :: wopos(*)
    integer(ip),  intent(inout) :: bridge(*)
    integer(ip),  intent(in)    :: itste
    real(rp),     intent(in)    :: ttime
    integer(ip),  intent(in)    :: pdime
    integer(ip)                 :: ileng,idime,pleng,kpoin,ipoin,ppoin
    integer(ip)                 :: imesh,kinteger
    integer(ip),  pointer       :: gisc2(:)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    !
    ! Type of postprocess
    !
    kinteger = kind(pdime)
    wwwww(1) = 'ALYA '
    wwwww(2) = 'V0002'
    wwwww(3) = wopos(1)
    wwwww(4) = wopos(2)
    wwwww(5) = wopos(3)
    wwwww(6) = 'INTEG'    
    wwwww(7) = '4BYTE'
    iiiii(1) = int(pdime,4)
    iiiii(2) = 0_4
    iiiii(4) = int(itste,4)
    rrrrr(1) = ttime
    
    if (kinteger==8_ip) then
       wwwww(7) = '8BYTE'
    end if

    wopos_pos(1) = wopos(1)

    if( kfl_reawr == 0 ) then
       imesh = kfl_posdi
    else
       imesh = ndivi
    end if

    if( wopos(3) == 'NPOIN' ) then

       parii = NPOIN_TYPE
       pleng = meshe(imesh) % npoin
       if( IMASTER ) pleng = meshe(imesh) % npoin_total

    else if( wopos(3) == 'NELEM' ) then

       parii = NELEM_TYPE
       pleng = meshe(imesh) % nelem
       if( IMASTER ) pleng = meshe(imesh) % nelem_total

    else if( wopos(3) == 'NBOUN' ) then

       parii = NBOUN_TYPE
       pleng =  meshe(imesh) % nboun
       if( IMASTER ) pleng = meshe(imesh) % nboun_total

    else
       call runend('UNDEFINED POSTPROCESS')
    end if
    iiiii(2) = int(pleng,4)
!!    if (kinteger==8_ip) iiiii(2) = int(pleng,8)
    !
    ! Get filter
    !
    call fildef(3_ip)
    !
    ! Output solution
    !
    call opfpos(1_ip)
    if( kfl_reawr < 0 ) then
       kfl_reawr = abs(kfl_reawr)
       return
    end if
    call poshea()

    if( ISEQUEN ) then

       if( kfl_filte == 0 .and. kfl_reawr /= 1 ) then
          write(lun_postp) pleng
          write(lun_postp) ( bridge(ileng), ileng = 1,pdime*pleng )
       else if( kfl_reawr == 1 ) then
          read(lun_postp) pleng
          read(lun_postp)  ( bridge(ileng), ileng = 1,pdime*pleng )
       else if( kfl_filte /= 0 ) then
          call runend('POSINT: FILTER NOT CODED')
       end if

    else if( IMASTER ) then

       do kfl_desti_par = 1,npart

          if( kfl_filte == 0 ) then
             !
             ! Master without filter
             !
             if( wopos(3) == 'NPOIN' ) then
                pleng = meshe(imesh) % npoin_par(kfl_desti_par)
             else if( wopos(3) == 'NELEM' ) then
                pleng = meshe(imesh) % nelem_par(kfl_desti_par)
             else if( wopos(3) == 'NBOUN' ) then
                pleng = meshe(imesh) % nboun_par(kfl_desti_par)
             end if
             call mepost(1_ip,pdime*pleng,0_ip)
             if( pleng > 0 ) call parari('RCV',0_ip,pdime*pleng,giscp)
             write(lun_postp) pleng
             write(lun_postp) ( giscp(ileng), ileng = 1,pdime*pleng )
             call mepost(3_ip,pdime*pleng,0_ip)

          else
             !
             ! Master with filter
             !
             call parari('RCV',0_ip,1_ip,pleng)
             if( pleng > 0 ) then
                allocate(gisc2(pleng))
                call parari('RCV',0_ip,pleng,gisc2)
                call mepost(1_ip,pdime*pleng,0_ip)
                call parari('RCV',0_ip,pdime*pleng,giscp)
                write(lun_postp) pleng
                write(lun_postp) ( gisc2(ileng), ileng = 1,pleng )
                write(lun_postp) ( giscp(ileng), ileng = 1,pdime*pleng )
                call mepost(3_ip,pdime*pleng,0_ip)
                deallocate(gisc2)
             else
                write(lun_postp) pleng
             end if
          end if
       end do

    else if( ISLAVE ) then

       kfl_desti_par = 0
       if( kfl_filte == 0 ) then
          !
          ! Slaves without filter
          !
          if( pleng > 0 ) call parari('SND',0_ip,pdime*pleng,bridge)
       else
          !
          ! Salves with filter
          !
          call mepost(1_ip,pdime*npoin,0_ip)
          pleng = 0
          loop_ppoin: do ppoin = 1,npoin
             ipoin = gefil(ppoin)
             if( ipoin /= 0 ) then
                kpoin = (ipoin-1) * pdime
                do idime = 1,pdime
                   pleng = pleng + 1
                   kpoin = kpoin + 1
                   giscp(pleng) = bridge(kpoin)
                end do
             else
                exit loop_ppoin
             end if
          end do loop_ppoin
          pleng = pleng / pdime
          call parari('SND',0_ip, 1_ip,pleng)
          if( pleng > 0 ) then
             call parari('SND',0_ip,pleng,gefil)
             call parari('SND',0_ip,pdime*pleng,giscp)
          end if
          call mepost(3_ip,pdime*npoin,0_ip)
       end if

    end if

    call opfpos(2_ip)

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine posint

  subroutine poshea()

    !-----------------------------------------------------------------------
    !
    ! File header
    !
    !-----------------------------------------------------------------------

    implicit none
    integer(ip) :: ii
    integer(4)  :: ihead

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    npari = 0
    nparr = 0
    nparc = 0
    nparx = 0
    pardi = int(iiiii(1),ip)

    if( INOTSLAVE ) then
       if( IMASTER ) then
          wwwww(8) = 'PARAL'
          iiiii(3) = int(npart,4)
       else
          wwwww(8) = 'SEQUE'
          iiiii(3) = int(1,4)
       end if
       if( kfl_filte == 0 ) then
          wwwww(9) = 'NOFIL'
       else
          wwwww(9) = 'FILTE'
       end if

       if( kfl_reawr == 1 ) then
          read(lun_postp)  ihead
          if( ihead /= 1234 ) call runend('MOD_POSTPR: ALYA FILE HAS A WRONG BINARY FORMAT')
          read(lun_postp)  wwww8(1) 
          read(lun_postp)  wwww8(2)
          read(lun_postp)  wwww8(3)
          read(lun_postp)  wwww8(4)
          read(lun_postp)  wwww8(5)
          read(lun_postp)  wwww8(6)
          read(lun_postp)  wwww8(7)
          read(lun_postp)  wwww8(8)
          read(lun_postp)  wwww8(9)
          read(lun_postp)  iiiii(1) 
          read(lun_postp)  iiiii(2) 
          read(lun_postp)  iiiii(3) 
          read(lun_postp)  iiiii(4) 
          read(lun_postp)  rrrrr(1) 
          do ii = 1,nwwww
             wwwww(ii)=wwww8(ii)(1:5)
          end do
       else
          wwww8(1) = 'ALYAPOST'
          do ii = 2,nwwww
             wwww8(ii)=wwwww(ii)//'   '
          end do
          ihead = 1234_4
          write(lun_postp) ihead
          write(lun_postp) wwww8(1)
          write(lun_postp) wwww8(2)
          write(lun_postp) wwww8(3)
          write(lun_postp) wwww8(4)
          write(lun_postp) wwww8(5)
          write(lun_postp) wwww8(6)
          write(lun_postp) wwww8(7)
          write(lun_postp) wwww8(8)
          write(lun_postp) wwww8(9)
          write(lun_postp) iiiii(1) 
          write(lun_postp) iiiii(2) 
          write(lun_postp) iiiii(3) 
          write(lun_postp) iiiii(4) 
          write(lun_postp) rrrrr(1) 

          if( kfl_reawr == 0 ) then
             if( ncoun_pos == 0 ) then
                ncoun_pos = ncoun_pos + 1
                write( lun_pos02,'(a,1x,i9,1x,e12.5)') 'START',iiiii(4),rrrrr(1) 
             end if
             write( lun_pos02,'(a)') wwwww(3)(1:5)
             call flush(lun_pos02)
          end if

       end if
    end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine poshea

  subroutine mepost(itask,ndim1,ndim2)
    !------------------------------------------------------------------------
    !****f* memory/mepost
    ! NAME 
    !    mepost
    ! DESCRIPTION
    !    Allocate and deallocate memory for generic scalar and vector arrays:
    !    ITASK=0 ... Allocate memory
    !    ITASK=2 ... Deallocate memory
    ! USES
    ! USED BY
    !    nsi_outvar
    !***
    !------------------------------------------------------------------------
    use def_parame
    use def_master
    use mod_memchk
    implicit none
    integer(ip), intent(in) :: itask,ndim1,ndim2
    integer(4)              :: istat

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

    select case(itask)

    case(0_ip)
       !
       ! Allocate memory for real 
       !
       if(ndim1>0.and.ndim2==0) then
          if(associated(gescp)) gescp => null()
          allocate(gescp(ndim1),stat=istat)     
          call memchk(zero,istat,memke,'GESCP','mepost',gescp)
       else if(ndim1>0.and.ndim2>0) then      
          if(associated(gevep)) gevep => null()
          allocate(gevep(ndim1,ndim2),stat=istat)     
          call memchk(zero,istat,memke,'GEVEP','mepost',gevep)        
       else if(ndim1<0.and.ndim2>0) then      
          !if(associated(getep)) getep => null()
          allocate(getep(-ndim1,-ndim1,ndim2),stat=istat)     
          call memchk(zero,istat,memke,'GETEP','mepost',getep)        
       end if

    case(1_ip)
       !
       ! Allocate memory for integer
       !
       if(ndim1/=0.and.ndim2==0) then
          if(associated(giscp)) giscp => null()
          allocate(giscp(ndim1),stat=istat)     
          call memchk(zero,istat,memke,'GISCP','mepost',giscp)
       else if(ndim1/=0.and.ndim2/=0) then      
          if(associated(givep)) givep => null()
          allocate(givep(ndim1,ndim2),stat=istat)     
          call memchk(zero,istat,memke,'GIVEP','mepost',givep)        
       end if

    case(2_ip)
       !
       ! Deallocate memory for real
       !
       if(ndim1>0.and.ndim2==0) then
          call memchk(two,istat,memke,'GESCP','mepost',gescp)
          deallocate(gescp,stat=istat)
          if(istat/=0) call memerr(two,'GESCP','mepost',0_ip)
       else if(ndim1>0.and.ndim2>0) then 
          call memchk(two,istat,memke,'GEVEP','mepost',gevep)
          deallocate(gevep,stat=istat)
          if(istat/=0) call memerr(two,'GEVEP','mepost',0_ip)         
       else if(ndim1<0.and.ndim2>0) then 
          call memchk(two,istat,memke,'GETEP','mepost',getep)
          deallocate(getep,stat=istat)
          if(istat/=0) call memerr(two,'GETEP','mepost',0_ip)         
       end if

    case(3_ip)
       !
       ! Deallocate memory for integer
       !
       if(ndim1/=0.and.ndim2==0) then
          call memchk(two,istat,memke,'GISCP','mepost',giscp)
          deallocate(giscp,stat=istat)
          if(istat/=0) call memerr(two,'GISCP','mepost',0_ip)
       else if(ndim1/=0.and.ndim2/=0) then 
          call memchk(two,istat,memke,'GIVEP','mepost',givep)
          deallocate(givep,stat=istat)
          if(istat/=0) call memerr(two,'GIVEP','mepost',0_ip)         
       end if

    case(4_ip)
       !
       ! Allocate memory for r3p 
       !
       call runend('MEPOST: NOT PRGRAMMED')

    case(5_ip)
       !
       ! Deallocate memory for r3p 
       !
       call runend('MEPOST: NOT PRGRAMMED')

    end select

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

  end subroutine mepost

end module mod_postpr


