subroutine reasol(itask)
  !-----------------------------------------------------------------------
  !****f* outrut/reasol
  ! NAME 
  !    reasol
  ! DESCRIPTION
  !    This routine reads the solver data
  ! INPUT
  ! OUTPUT
  ! USES
  ! USED BY
  !    ***_reanut
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only    :  ip
  use def_solver, only    :  solve_sol
  use def_solver, only    :  SOL_SOLVER_DEFLATED_CG,SOL_GDECG
  use def_solver, only    :  SOL_SOLVER_PIPELINED_DEFLATED_CG
  use def_master, only    :  kfl_symgr,kfl_schur,kfl_aiipr
  use def_domain, only    :  ngrou_dom
  use def_inpout
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: kfl_check

  kfl_check = 0

  if( itask == 1 ) then

     if( trim(words(2)) /= '' ) then

        call intsol(1_ip) 
        call intsol(3_ip) 
        call intsol(4_ip) 
        call intsol(5_ip) 

     else

        kfl_check = 1
        do while( words(1) /= 'ENDAL' ) 
           if( words(1) == 'SOLVE' ) then
              call intsol(1_ip)
           else if( words(1) == 'PRECO' ) then
              call intsol(2_ip)
           else if( words(1) == 'OUTPU' ) then
              call intsol(3_ip)
           else if( words(1) == 'CONVE' ) then
              call intsol(4_ip)
           else if( words(1) == 'OPTIO' ) then
              call intsol(5_ip)
           else if( words(1) == 'SMOOT' ) then
              call intsol(6_ip)
           else if( words(1) == 'COARS' ) then
              call intsol(7_ip)
           end if
           call ecoute('REASOL')
        end do

     end if

  end if


  if( kfl_check == 0 .and. ( itask == 1 .or. itask == 2 ) ) then
     
     if( trim(words(1)) /= '' ) then
        call intsol(2_ip)
     end if
  end if

end subroutine reasol

subroutine intsol(itask)
  !-----------------------------------------------------------------------
  !****f* outrut/reasol
  ! NAME 
  !    reasol
  ! DESCRIPTION
  !    Interprte solver
  ! INPUT
  ! OUTPUT
  ! USES
  ! USED BY
  !    ***_reanut
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only    :  ip
  use def_solver
  use def_master, only    :  kfl_symgr,kfl_schur,kfl_aiipr
  use def_domain, only    :  ngrou_dom
  use def_inpout
  implicit none
  integer(ip), intent(in) :: itask

  if( itask == 1 ) then

     !-------------------------------------------------------------------
     !
     ! Solver 
     !
     !-------------------------------------------------------------------
               
     if( words(2) == 'PASTI' ) solve_sol(1) % kfl_algso = -3    ! PASTIX - SPARSE DIRECT       
     if( words(2) == 'PASTI' ) solve_sol(1) % kfl_algso = -2    ! ALYA   - SPARSE DIRECT  (now=14)
     if( words(2) == 'MUMPS' ) solve_sol(1) % kfl_algso = -1    ! MUMPS  - SPARSE DIRECT       
     if( words(2) == 'CG   ' ) solve_sol(1) % kfl_algso =  1    ! CONJUGATE GRADIENT          
     if( words(2) == 'CONJU' ) solve_sol(1) % kfl_algso =  1    ! CONJUGATE GRADIENT          
     if( words(2) == 'DEFLA' ) solve_sol(1) % kfl_algso =  2    ! DEFLATED CONJUGATE GRADIENT
     if( words(2) == 'BICGS' ) solve_sol(1) % kfl_algso =  5    ! BiCGSTAB                   
     if( words(2) == 'GMRES' ) solve_sol(1) % kfl_algso =  8    ! GMRES
     if( words(2) == 'DIAGO' ) solve_sol(1) % kfl_algso =  9    ! RICHARDSON WITH MASS
     if( words(2) == 'RICHA' ) solve_sol(1) % kfl_algso =  9    ! RICHARDSON
     if( words(2) == 'MATRI' ) solve_sol(1) % kfl_algso = 10    ! RICHARDSON WITH MASS AND MATRIX
     if( words(2) == 'DEFBC' ) solve_sol(1) % kfl_algso = 12    ! DEFLATED BiCGSTAB
     if( words(2) == 'DEFGM' ) solve_sol(1) % kfl_algso = 13    ! DEFLATED GMRES
     if( words(2) == 'SPARS' ) solve_sol(1) % kfl_algso = 14    ! SPARSE DIRECT SOLVER
     if( words(2) == 'STEEP' ) solve_sol(1) % kfl_algso = 15    ! STEEPEST DESCENT
     if( words(2) == 'JACOB' ) solve_sol(1) % kfl_algso = 16    ! JACOBI
     if( words(2) == 'PCG'   ) solve_sol(1) % kfl_algso = 18    ! PIPELINED CONJUGATE GRADIENT
     if( words(2) == 'PDCG'  ) solve_sol(1) % kfl_algso = 19    ! PIPELINED DEFLATED CONJUGATE GRADIENT
     if( words(2) == 'OFF  ' ) solve_sol(1) % kfl_algso = -999  ! NO SOLVER   
     if( words(2) == 'GCG  '  ) solve_sol(1) % kfl_algso = 100   ! GPU Conjugate Gradient
     if( words(2) == 'GGMR ' ) solve_sol(1) % kfl_algso = 101  ! GPU GMRES
     if( words(2) == 'GDECG' ) solve_sol(1) % kfl_algso = 102  ! GPU DEFCG
     !
     ! Define symmetry of the equation
     !
     if( words(2) == 'DIREC' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'LDU  ' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'RICHA' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'MUMPS' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'CG   ' ) solve_sol(1) % kfl_symeq =  1
     if( words(2) == 'DEFLA' ) solve_sol(1) % kfl_symeq =  1
     if( words(2) == 'BICGS' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'TBICG' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'GMRES' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'DIAGO' ) solve_sol(1) % kfl_symeq =  1
     if( words(2) == 'MATRI' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'DEFBC' ) solve_sol(1) % kfl_symeq =  0
     if( words(2) == 'DEFGM' ) solve_sol(1) % kfl_symeq =  0 
     if( words(2) == 'SPARS' ) solve_sol(1) % kfl_symeq =  0 
     if( words(2) == 'STEEP' ) solve_sol(1) % kfl_symeq =  0 
     if( words(2) == 'PCG  ' ) solve_sol(1) % kfl_symeq =  1
     !
     ! Schur or not Schur
     !
     if( exists('SCHUR') ) then
        solve_sol(1) % kfl_schur = 1
        kfl_schur = max(1_ip,kfl_schur)
        if(exists('CSR  ')) solve_sol(1) % kfl_scaii = 1
        if(exists('SPARS')) solve_sol(1) % kfl_scaii = 1
        if(exists('SKYLI')) solve_sol(1) % kfl_scaii = 0
        if(exists('CHOLE')) solve_sol(1) % kfl_scaii = 0
     end if
     !
     ! Define symmetry. Force matrice to be always fully assembled
     !
     if( solve_sol(1) % kfl_algso == 1 .or. solve_sol(1) % kfl_algso == 2 ) then
        solve_sol(1) % kfl_symme = 0
     end if
     !
     ! Deflated: define options
     !
     if(    solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_CG           .or. &
          solve_sol(1) % kfl_algso == SOL_GDECG .or. &
          & solve_sol(1) % kfl_algso == SOL_SOLVER_PIPELINED_DEFLATED_CG ) then
        if( exists('ALLRE') ) solve_sol(1) % kfl_gathe = 0   ! DCG: All reduce
        if( exists('ALLGA') ) solve_sol(1) % kfl_gathe = 1   ! DCG: All gather
        if( exists('SENDR') ) solve_sol(1) % kfl_gathe = 2   ! Send/receive
        if( exists('COARS') ) then
           if( exists('DIREC') ) then
              solve_sol(1) % kfl_defso = 0
           else if( exists('ITERA') ) then
              solve_sol(1) % kfl_defso = 1
           end if
           if( exists('CSR  ') .or. exists('SPARS') ) then
              solve_sol(1) % kfl_defas = 1
           else if( exists('SKYLI') .or. exists('CHOLE') ) then
              solve_sol(1) % kfl_defas = 0
           else if( exists('DENSE') ) then
              solve_sol(1) % kfl_defas = 2
           end if
           if( solve_sol(1) % kfl_defso == 1 .and. solve_sol(1) % kfl_defas /= 2 ) then
              solve_sol(1) % kfl_defas = 1
           end if
        end if
     end if
     !
     ! Complex solver
     !
     if( exists('COMPL') ) solve_sol(1) % kfl_cmplx = 1
     !
     ! Deflated CG
     !
     if(exists('GROUP')) then
        call runend('GROUPS OF DEFALTED SHOULD BE DECLARED IN MASTER')
     end if
     !
     ! GMRES
     !
     if( solve_sol(1) % kfl_algso == 8 ) then
        if(exists('MODIF')) solve_sol(1) % kfl_ortho = 1                                                   ! GMRES: Modified  Gram-Schmidt
        if(exists('CLASS')) solve_sol(1) % kfl_ortho = 0                                                   ! GMRES: Classical Gram-Schmidt
        solve_sol(1) % nkryd = getint('KRYLO',10_ip,'#Solver Krylov dimension')
     end if
     !
     ! Symmetric graph
     !
     if( solve_sol(1) % kfl_symme == 1 ) kfl_symgr = 1

  else if( itask == 2 ) then

     !-------------------------------------------------------------------
     !
     ! Preconditioner 
     !
     !-------------------------------------------------------------------

     if( exists('LEFT ') ) then
        solve_sol(1) % kfl_leftr = 0
     else if( exists('RIGHT') ) then
        solve_sol(1) % kfl_leftr = 1
     end if

     if(exists('SQUAR')) then
        solve_sol(1) % kfl_preco = 1
        solve_sol(1) % wprec     = 'LEFT-RIGHT SYMMETRIC: sqrt[|diag(A)|]'
        !call runend('REASOL: NOT AVAILABLE')

     else if(exists('DIAGO')) then
        solve_sol(1) % kfl_preco = SOL_DIAGONAL
        solve_sol(1) % wprec     = 'LEFT: DIAGONAL diag(A)'

     else if(exists('MATRI')) then
        solve_sol(1) % kfl_preco = SOL_MATRIX
        solve_sol(1) % wprec     = 'LEFT: MATRIX'

     else if(exists('LINEL').and. (.not.exists('MULTI'))) then
        solve_sol(1) % kfl_preco = SOL_LINELET
        solve_sol(1) % wprec     = 'LINELET'
        solve_sol(1) % toler     = getrea('TOLER',10.0_rp,'#Linelet tolerance') 
        if( exists('BOUND') ) then
           solve_sol(1) % kfl_linty = 1
        else if( exists('INTER') ) then
           solve_sol(1) % kfl_linty = 0
        else if( exists('PRESC') ) then
           solve_sol(1) % kfl_linty = 2
        else if( exists('FIELD') ) then
           solve_sol(1) % kfl_linty = -getint('FIELD',-1_ip,'#Field for linelet') 
        end if

     else if(exists('MASSM')) then
        solve_sol(1) % kfl_preco = SOL_MASS_MATRIX
        solve_sol(1) % wprec     = 'MASS MATRIX (DIAGONAL)'

     else if(exists('GAUSS')) then
        solve_sol(1) % kfl_preco = SOL_GAUSS_SEIDEL
        if( exists('SYMME') ) then
           solve_sol(1) % kfl_renumbered_gs = -1
           solve_sol(1) % wprec             = 'SYMMETRIC GAUSS-SEIDEL'
        else if( exists('STREA') ) then
           solve_sol(1) % kfl_renumbered_gs =  1
           solve_sol(1) % wprec             = 'STREAMWISE GAUSS-SEIDEL'
        else
           solve_sol(1) % kfl_renumbered_gs =  0
           solve_sol(1) % wprec             = 'GAUSS-SEIDEL'
        end if
        
     else if(exists('SSOR ')) then
        solve_sol(1) % kfl_preco            = SOL_GAUSS_SEIDEL
           solve_sol(1) % kfl_renumbered_gs = -1
           solve_sol(1) % wprec             = 'LEFT: SYMMETRIC GAUSS-SEIDEL'
        
     else if(exists('CLOSE')) then
        solve_sol(1) % kfl_preco = SOL_CLOSE_MASS_MATRIX
        solve_sol(1) % wprec     = 'CLOSE MASS MATRIX (DIAGONAL)'

     else if(exists('RICHA')) then
        solve_sol(1) % kfl_preco = SOL_RICHARDSON
        solve_sol(1) % wprec     = 'LEFT: RICHARDSON'

     else if(exists('ORTHO')) then
        solve_sol(1) % kfl_preco = SOL_ORTHOMIN
        solve_sol(1) % wprec     = 'LEFT: ORTHOMIN(1)'

     else if(exists('OFF  ').or.exists('NONE ')) then
        solve_sol(1) % kfl_preco = 0
        solve_sol(1) % wprec     = 'NONE'

     else if(exists('APPRO')) then
        solve_sol(1) % kfl_preco = SOL_APPROXIMATE_SCHUR
        solve_sol(1) % wprec     = 'LEFT: APPROXIMATE SCHUR= ABB - ABI diag(AII)^{-1} AIB'
        kfl_schur                = max(3_ip,kfl_schur)

     else if(exists('ABB  ')) then
        solve_sol(1) % kfl_preco = SOL_ABB
        solve_sol(1) % wprec     = 'LEFT: ABB'
        kfl_schur                = max(2_ip,kfl_schur)
        if(exists('CSR  ')) solve_sol(1) % kfl_scpre = 1
        if(exists('SPARS')) solve_sol(1) % kfl_scpre = 1
        if(exists('SKYLI')) solve_sol(1) % kfl_scpre = 0
        if(exists('CHOLE')) solve_sol(1) % kfl_scpre = 0

     else if(exists('MODDI')) then
        solve_sol(1) % kfl_preco = SOL_MOD_DIAGONAL
        solve_sol(1) % wprec     = 'LEFT: diag(ABB) - diag( ABI diag(AII)^{-1} AIB)'
        kfl_schur                = max(2_ip,kfl_schur)

     else if(exists('AII  ')) then
        if( exists('COARS') ) then
           solve_sol(1) % kfl_preco = SOL_COARSE_AII
           solve_sol(1) % wprec     = 'LEFT: COARSE AII^{-1}'
           solve_sol(1) % ngaii     = getint('GROUP',1_ip,'#Number of groups') 
           if( exists('CSR  ') .or. exists('SPARS') ) then
              solve_sol(1) % kfl_deaii = 1
           else if( exists('SKYLI') .or. exists('CHOLE') ) then
              solve_sol(1) % kfl_deaii = 0
           end if
        else
           solve_sol(1) % kfl_preco = SOL_AII
           solve_sol(1) % wprec     = 'LEFT: AII^{-1}'
           kfl_aiipr                = 1
        end if

     else if(exists('AUGME')) then
        solve_sol(1) % kfl_preco = SOL_AUGMENTED_DIAGONAL
        solve_sol(1) % wprec     = 'AUGMENTED DIAGONAL'

     else if(exists('MULTI')) then
        solve_sol(1) % kfl_preco = SOL_MULTIGRID
        solve_sol(1) % wprec     = 'MULTIGRID DEFLATED'
        if( exists('DIREC') ) then
           solve_sol(1) % kfl_defso = 0
        else
           solve_sol(1) % kfl_defso = 1
           solve_sol(1) % kfl_defas = 1
        end if
        if( exists('CSR  ') .or. exists('SPARS') ) then
           solve_sol(1) % kfl_defas = 1
        else if( exists('SKYLI') .or. exists('CHOLE') ) then
           solve_sol(1) % kfl_defas = 0
        end if
        if( solve_sol(1) % kfl_defso == 1 ) then
           solve_sol(1) % kfl_defas = 1
        end if
        if( exists('LINEL') ) then
           solve_sol(1) % kfl_defpr = 4
           solve_sol(1) % toler     = getrea('TOLER',10.0_rp,'#Linelet tolerance') 
           if( exists('BOUND') ) then
              solve_sol(1) % kfl_linty = 1
           else if( exists('INTER') ) then
              solve_sol(1) % kfl_linty = 0
           else if( exists('PRESC') ) then
              solve_sol(1) % kfl_linty = 2
           else if( exists('FIELD') ) then
              solve_sol(1) % kfl_linty = -getint('FIELD',-1_ip,'#Field for linelet') 
           end if
        else if( exists('DIAGO') .or.  exists('JACOB') ) then
           solve_sol(1) % kfl_defpr = 2
        else if( exists('GAUSS') ) then
           solve_sol(1) % kfl_defpr = 6
        else
           solve_sol(1) % kfl_defpr = 2
        end if

     else if(exists('NEUMA')) then
        solve_sol(1) % kfl_preco = SOL_NEUMANN
        solve_sol(1) % wprec     = 'NEUMANN'

     else if(exists('LOCDI')) then
        solve_sol(1) % kfl_preco = SOL_LOCAL_DIAGONAL
        solve_sol(1) % wprec     = 'LOCAL DIAGONAL: DIAGONAL WITH DIFFERENT ENTRIES'

     else if(exists('RAS  ')) then
        solve_sol(1) % kfl_preco = SOL_RAS
        solve_sol(1) % wprec     = 'RAS: RESTRICTED ADDITIVE SCHWARZ'

     else if(exists('BLOCK')) then
        solve_sol(1) % kfl_preco = SOL_BLOCK_DIAGONAL
        solve_sol(1) % wprec     = 'BLOCK DIAGONAL'

     end if

  else if( itask == 3 ) then

     !-------------------------------------------------------------------
     !
     ! Output 
     !
     !-------------------------------------------------------------------

     if(exists('CONVE')) solve_sol(1) % kfl_cvgso = 1                                                   ! Convergence
     if(exists('MARKE')) solve_sol(1) % kfl_marke = 1                                                   ! Output matrix in market format
     if(exists('MATRI')) solve_sol(1) % kfl_marke = 2                                                   ! Output matrix in ASCII format
     if(exists('NONPR')) solve_sol(1) % kfl_exres = 1                                                   ! Output non-preconditioned residual
     if(exists('OUTPU')) solve_sol(1) % kfl_solve = 1                                                   ! Output solver information
     if(exists('POSTP')) solve_sol(1) % kfl_posgr = 1                                                   ! Postprocess groups

  else if( itask == 4 ) then

     !-------------------------------------------------------------------
     !
     ! Convergence 
     !
     !-------------------------------------------------------------------

     if(exists('REALR')) solve_sol(1) % kfl_resid = 1                                                   ! Use non-preconditioned residual
     if(exists('ADAPT')) solve_sol(1) % kfl_adres = 1                                                   ! Adaptive tolerance
     if(exists('RELAX')) solve_sol(1) % relso = getrea('RELAX',1.0_rp, '#Solver relaxation parameter')  ! Ralaxation
     if(exists('TOLER')) solve_sol(1) % solco = getrea('TOLER',0.01_rp,'#Solver tolerance')             ! Tolerance
     if(exists('ITERA')) solve_sol(1) % miter = getint('ITERA',1_ip, '#Solver number of iterations')    ! Max, # iterations
     if(exists('RATIO')) solve_sol(1) % adres = getrea('RATIO',0.1_rp, '#Ratio wr to initial residual') ! Adpative residual
     solve_sol(1) % solmi = solve_sol(1) % solco                                                        ! Minimum tolerance

  else if( itask == 5 ) then

     !-------------------------------------------------------------------
     !
     ! Options 
     !
     !-------------------------------------------------------------------

     if(exists('UNSYM')) solve_sol(1) % kfl_symme = 0                                                   ! Force non-symmetric assembly
     if(exists('NONSY')) solve_sol(1) % kfl_symme = 0                                                   ! Force non-symmetric assembly
     if(exists('SYMME')) solve_sol(1) % kfl_symme = 1                                                   ! Force symmetric assembly
     if(exists('PENAL')) then
        if( exists('DUALT') ) solve_sol(1) % kfl_penal = 1 
        solve_sol(1) % penal = getrea('PARAM',1.0_rp, '#Penalization parameter')
     end if
     if(exists('FORCE')) solve_sol(1) % kfl_force = 1                                                   ! Force continuity after solver
     if(exists('LIMIT')) solve_sol(1) % kfl_limit = 1                                                   ! Algebraic limiter
     if(exists('CSR  ')) solve_sol(1) % kfl_defas = 1                                                   ! DCG: Sparse assembly
     if(exists('ALLRE')) solve_sol(1) % kfl_gathe = 0                                                   ! DCG: All reduce
     if(exists('ALLGA')) solve_sol(1) % kfl_gathe = 1                                                   ! DCG: All gather
     if(exists('SENDR')) solve_sol(1) % kfl_gathe = 2                                                   ! Send/receive
     if(exists('FIXIT')) solve_sol(1) % kfl_iffix = 1                                                   ! Fixity imposed by solver
     if(exists('ZEROF')) solve_sol(1) % kfl_iffix = 2                                                   ! Fixity imposed by solver with zero value
     if(exists('UNKNO')) solve_sol(1) % kfl_iffix = 3                                                   ! Use unknown to prescribe Dirichlet
     if(exists('MODIF')) solve_sol(1) % kfl_ortho = 1                                                   ! GMRES: Modified  Gram-Schmidt
     if(exists('CLASS')) solve_sol(1) % kfl_ortho = 0                                                   ! GMRES: Classical Gram-Schmidt
     if(exists('SCHUR')) solve_sol(1) % kfl_schum = 1                                                   ! If matrix comes from Schur complement
     if(exists('BLOCK')) then
        solve_sol(1) % kfl_blogs = 1                                                                    ! Block Gauss-Seidel treatment
        if( exists('NUMBL') ) then
           solve_sol(1) % nblok = getint('NUMBL',1_ip,'#Number of blocks Gauss-Seidel') 
           !
           ! Example:    ndofn = 6, block = 3, [ u1 u2 u3 u4 u5 u6 ] => 
           ! Contiguous:   [ u1 u2 ] [ u3 u4 ] [ u5 u6 ]
           ! Uncontiguous: [ u1 u3 ] [ u2 u5 ] [ u3 u6 ]
           ! 
           if( exists('UNCON') ) then
              solve_sol(1) % nblok = -abs(solve_sol(1) % nblok)
           else if( exists('CONTI') ) then
              solve_sol(1) % nblok =  abs(solve_sol(1) % nblok)
           end if
        end if
     end if

  else if( itask == 6 ) then

     !-------------------------------------------------------------------
     !
     ! Smoother: only for multigrid solver 
     !
     !-------------------------------------------------------------------

     if( exists('ITERA') ) then
        solve_sol(1) % itpre = getint('ITERA',1_ip,'#Jacobi iterations') 
     end if
     if( exists('LINEL') ) then
        solve_sol(1) % kfl_defpr = 4
        solve_sol(1) % toler     = getrea('TOLER',10.0_rp,'#Linelet tolerance') 
        if( exists('BOUND') ) then
           solve_sol(1) % kfl_linty = 1
        else if( exists('INTER') ) then
           solve_sol(1) % kfl_linty = 0
        else if( exists('PRESC') ) then
           solve_sol(1) % kfl_linty = 2
        else if( exists('FIELD') ) then
           solve_sol(1) % kfl_linty = -getint('FIELD',-1_ip,'#Field for linelet') 
        end if
     else if( exists('DIAGO') ) then
        solve_sol(1) % kfl_defpr = 2
     else if( exists('GAUSS') ) then
        solve_sol(1) % kfl_defpr = 6
     else
        solve_sol(1) % kfl_defpr = 2
     end if

  else if( itask == 7 ) then

     !-------------------------------------------------------------------
     !
     ! Coarse solver
     !
     !-------------------------------------------------------------------

     if( words(2) == 'ON   ' .or. words(2) == 'YES  ' ) then 
        solve_sol(1) % kfl_coarse = 1
        solve_sol(1) % kfl_defas  = 1
     end if

  end if

end subroutine intsol

