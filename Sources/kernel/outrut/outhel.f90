subroutine outhel
  !------------------------------------------------------------------------
  !****f* master/outhel
  ! NAME
  !    outhel
  ! DESCRIPTION
  !    This routine output a help message
  ! OUTPUT
  !   
  ! USED BY
  !    openfi
  !***
  !------------------------------------------------------------------------

  use      def_master
  implicit none

  write(6,1)
  stop

1 format(&
       & '--| ',/,&
       & '--| ALYA USAGE:',/,&
       & '--| ',/,&
       & '--| Alya.x [-help] name ',/,&
       & '--| ',/,&
       & '--| -help ..... Displays this help',/,&
       & '--| ',/,&
       & '--| Runs Alya for problem name. ',/,&
       & '--| ',/,&
       & '--| The following i/o files are located/created in current directory:',/,&
       & '--| (I)   name.dat:         run data file',/,&
       & '--| (I)   name.dom.dat:     domain (mesh) data file',/,&
       & '--| (I)   name.*.dat:       module/service * data file',/,&
       & '--| (O)   name.log:         log',/,&      
       & '--| (O)   name.mem:         memory',/,&
       & '--| (O)   name.cvg:         convergence',/,&      
       & '--| (O)   name.post.res:    postprocess',/,&  
       & '--| (O)   name.liv:         live info',/,&
       & '--| (O)   name.rst:         restart',/,&    
       & '--| (O)   name-latex.tex:   latex',/,&    
       & '--| (O)   name-latex.plt:   gnuplot',/,& 
       & '--| (O)   name.post.msh:    mesh',/,&
       & '--| (O)   name-DD.post.msh: DD mesh',/,&
       & '--| (I/O) name.geo.bin:     binary geometry (in/out)',/,&
       & '--| ')

end subroutine outhel
