!-----------------------------------------------------------------------
!
!> @addtogroup SearchSortToolBox
!! @{
!> @name    array_search_rank
!! @file    array_search_rank.f90
!> @author  Antoni Artigues
!! @brief   Binary search on an ordered integer array with array ranking.
!! @details Binary search in an ordered integer array, the order of
!!          the integer array is in another array called ranking
!! @{
!
!-----------------------------------------------------------------------
subroutine array_search_rank(list, dime, ranking, item, itemIdx) 

  use mod_mrgrnk
  use def_kintyp,         only :  ip,rp

  implicit none

  integer(ip), intent(in)    :: dime !> Array dimension
  integer(ip), dimension(dime),  intent(in)    :: list !> array to search in
  integer(ip), dimension(dime),  intent(in) :: ranking !> Ranking of the array
  integer(ip), intent(in)    :: item !> Value to search in the array
  LOGICAL        :: found
  integer(ip), intent(out)    :: itemIdx !> index of the value in the array
  
  integer(ip) :: start, finish, mid, range
  
  !binary search with the ranked list
  start =  1
  finish = dime
  range = finish - start
  mid = (start + finish) /2
  !write(*,*) 'FINISH:' , finish
  !write(*,*) 'MID:' , mid
  !write(*,*) 'range:' , range
  !write(*,*) 'ranking(mid):' , ranking(mid)
  do while( list(ranking(mid)) /= item .and. range >  0)
      if (item > list(ranking(mid))) then
        start = mid + 1
      else
        finish = mid - 1
      end if
      range = finish - start
      mid = (start + finish)/2
      !write(*,*) 'FINISH:' , finish
      !write(*,*) 'MID:' , mid
      !write(*,*) 'range:' , range
      !write(*,*) 'ranking(mid):' , ranking(mid)
  end do

    if( list(ranking(mid)) /= item) then
      found = .FALSE.
      itemIdx = 0
    else
      found = .TRUE.
      itemIdx = ranking(mid)
    end if
  
end subroutine array_search_rank
