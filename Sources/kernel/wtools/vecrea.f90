 subroutine vecrea(ndofn,vecto)
!-----------------------------------------------------------------------
!
! This routine reads vectors
!
!-----------------------------------------------------------------------
  use      def_inpout
  implicit none
  integer(ip) :: ndofn
  real(rp)    :: vecto(ndofn)
  integer(ip) :: idofn

  call ecoute('vecrea')
  do idofn = 1,ndofn
     vecto(idofn) = param(idofn)
  end do

end subroutine vecrea


