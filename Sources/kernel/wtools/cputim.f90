subroutine cputim(rtime)
  !-----------------------------------------------------------------------
  !****f* wtools/cputim
  ! NAME
  !    cputim
  ! DESCRIPTION
  !    This routine finds out the CPU time in seconds
  ! OUTPUT
  !   
  ! USES
  ! USED BY
  !***
  !-----------------------------------------------------------------------
#ifdef _OPENMP
  use omp_lib
#endif
  use def_kintyp, only  :  rp
  use def_master, only  :  rate_time
  implicit none
  real(rp), intent(out) :: rtime
  real(8)               :: rtim8
  real(4)               :: rtim4,elap4(2),etime
  integer(4)            :: itim4

#ifdef _OPENMP
  rtime = omp_get_wtime()
#else
  if( 1 == 1 ) then
    
     call system_clock(itim4)
     rtime = real(itim4,rp) * rate_time

  else if( 1 == 2 ) then

     call cpu_time(rtime)

  else if( 1 == 3 ) then
     !
     ! This method requires linking with -lrt
     ! Commented temporarly (discussion is required)
     ! ! Using C Wall time routine
     ! call wall_time(rtim8)
     ! ! Just in case rp is not 8
     ! rtime = rtim8
  end if

#endif

end subroutine cputim
