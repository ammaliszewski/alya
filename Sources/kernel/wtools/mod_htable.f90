module mod_htable

  use def_kintyp
  integer(ip), parameter :: NIL=-1

  type hash_t
     integer(ip)          :: size, nelem
     integer(ip), pointer :: dades(:)
     integer(ip), pointer :: indic(:)
  end type hash_t

contains

  subroutine htaini( ht, size )
    use def_kintyp
    implicit none
    type(hash_t)         :: ht
    integer(ip)          :: size
    integer(ip)          :: istat

    call HTablePrimeNumber( size )
    ht%size  = size
    allocate(ht%dades(size),stat=istat)

  end subroutine htaini

  subroutine htades( ht )
    use def_kintyp
    implicit none
    type(hash_t)         :: ht
    integer(ip)          :: istat

    deallocate(ht%dades,stat=istat)

  end subroutine htades

  subroutine htares( ht, indic )
    use def_kintyp
    implicit none
    type(hash_t)         :: ht
    integer(ip), target  :: indic(:)
    integer(ip)          :: ii

    ht%nelem = 0
    ht%indic => indic
    do ii= 1, ht%size
       ht%dades(ii) = NIL
    enddo
  end subroutine htares

  subroutine htaadd( ht, value )
    use def_kintyp
    implicit none
    type(hash_t)         :: ht
    integer(ip)          :: value 
    integer(ip)          :: pos, incr, val
    logical(lg)          :: noEncontrado

    pos   = MOD(value,ht%size) + 1_ip
    incr  = MOD(value,ht%size-1_ip)

    noEncontrado = .true.
    do while(noEncontrado)
       val = ht%dades(pos)
       if (val==NIL) then
          ht%nelem           = ht%nelem + 1
          ht%dades(pos)      = value
          ht%indic(ht%nelem) = value
          noEncontrado = .false.
       else if (val==value) then
          noEncontrado = .false.
       else
          pos = MOD(pos+incr,ht%size) + 1
       endif
    enddo
  end subroutine Htaadd

  subroutine HTablePrimeNumber( size )
    use def_kintyp
    implicit none
    integer(ip)            :: size
    integer(ip)            :: ii
    integer(ip), parameter :: HTablePrimes(117) = &
         (/      7,       11,       19,       31,      127, &
         211,      383,      631,      887,     1151, &
         1399,     1663,     1913,     2161,     2423, &
         2687,     2939,     3191,     3449,     3709, &
         3967,     4219,     4463,     4733,     4987, &
         5237,     5503,     5749,     6011,     6271, &
         6521,     6781,     7039,     7283,     7549, &
         7793,     8059,     8317,     8573,     8831, &
         9067,     9343,     9587,     9851,    10111, &
         10357,    10613,    10867,    11131,    11383, &
         11633,    11903,    12157,    12413,    12671, &
         12923,    13183,    13421,    13693,    13933, &
         14207,    14461,    14717,    14969,    15227, &
         15473,    15739,    15991,    16253,    16493, &
         18553,    20599,    22651,    24697,    26737, &
         28793,    30841,    32887,    34939,    36979, &
         39023,    41081,    43133,    45181,    47221, &
         49279,    51307,    53359,    55411,    57467, &
         59513,    61561,    63611,    65657,    82039, &
         98429,   114809,   131171,   147583,   163927, &
         180347,   196727,   213119,   229499,   245881, &
         262271,   327799,   393331,   458879,   524413, &
         589933,   655471,   721013,   786553,   852079, &
         917629,   983153 /)
    ii = 1
    do while(HTablePrimes(ii)<size)
       ii = ii + 1
    enddo
    size = HTablePrimes(ii)
  end subroutine HTablePrimeNumber

end module mod_htable
