subroutine matrea(ndofn,ndim1,ndim2,matri)
!-----------------------------------------------------------------------
!
! This routine reads matrices
!
!-----------------------------------------------------------------------
  use      def_inpout
  implicit none
  integer(ip) ::  ndofn,ndim1,ndim2
  real(rp)    ::  matri(ndofn,ndofn,ndim1,ndim2)
  integer(ip) ::  idime,jdime,idofn,jdofn

  do idime = 1,ndim1
     do jdime = 1,ndim2
        do idofn = 1,ndofn
           call ecoute('matrea')
           do jdofn = 1,ndofn
              matri(idofn,jdofn,idime,jdime) = param(jdofn)
           end do
        end do
     end do
  end do

end subroutine matrea
