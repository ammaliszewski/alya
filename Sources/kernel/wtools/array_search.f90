!-----------------------------------------------------------------------
!
!> @addtogroup SearchSortToolBox
!! @{
!> @name    array_search
!! @file    array_search.f90
!> @author  Antoni Artigues
!! @brief   Binary search on an ordered integer array.
!! @details Binary search in an ordered integer
!! @{
!
!-----------------------------------------------------------------------
subroutine array_search(list, dime, item, itemIdx) 
  use mod_mrgrnk
  use def_kintyp,         only :  ip,rp

  implicit none

  integer(ip), intent(in)    :: dime !> Array dimension
  integer(ip), dimension(dime),  intent(in)    :: list !> array to search in
  integer(ip), intent(in)    :: item !> Value to search in the array
  LOGICAL        :: found
  integer(ip), intent(out)    :: itemIdx !> index of the value in the array
  
  integer(ip), dimension(dime)  :: ranking
  integer(ip) :: start, finish, mid, range

  !rank de list
  call mrgrnk(list, ranking)
  
  !binary search with the ranked list
  start =  1
  finish = dime
  range = finish - start
  mid = (start + finish) /2

  do while( list(ranking(mid)) /= item .and. range >  0)
      if (item > list(ranking(mid))) then
        start = mid + 1
      else
        finish = mid - 1
      end if
      range = finish - start
      mid = (start + finish)/2
  end do

    if( list(ranking(mid)) /= item) then
      found = .FALSE.
      itemIdx = 0
    else
      found = .TRUE.
      itemIdx = ranking(mid)
    end if
  
end subroutine array_search
