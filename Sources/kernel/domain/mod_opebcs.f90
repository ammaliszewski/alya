module mod_opebcs
  !-----------------------------------------------------------------------
  !****f* outrut/mod_opebcs
  ! NAME
  !   mod_opebcs
  ! DESCRIPTION
  !   This routine manages the opebcsocess
  ! USES
  ! USED BY
  !   output_***
  !   outvar_***
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_elmtyp
  use mod_memory
  implicit none
  integer(ip), parameter :: mcodc=100_ip


contains

  subroutine opnbcs(itask,nvari,ndofn,kfl_ibopo,tncod_xxx)

    !--------------------------------------------------------------------
    !
    ! NODE
    !
    !--------------------------------------------------------------------

    implicit none
    integer(ip), intent(in) :: itask,nvari,ndofn,kfl_ibopo
    integer(ip)             :: icode,icono,idofn,ivari
    integer(4)              :: istat
    type(bc_nodes), intent(out), pointer :: tncod_xxx(:)

    select case ( itask )

    case ( 0_ip )
       !
       ! Allocate type for NVARI variables
       !
       allocate( tncod_xxx(nvari) , stat = istat )
       do ivari = 1,nvari
          allocate( tncod_xxx(ivari) % l(mcodc) , stat = istat )
          tncod_xxx(ivari) % kfl_ibopo = kfl_ibopo
          tncod_xxx(ivari) % ndofn = ndofn  
          tncod_xxx(ivari) % ncode = 0
          do icode = 1,mcodc 
             allocate( tncod_xxx(ivari) % l(icode) % lcode(mcono) , stat= istat )
             allocate( tncod_xxx(ivari) % l(icode) % bvess(ndofn) , stat= istat )
             tncod_xxx(ivari) % l(icode) % kfl_fixno = 0
             tncod_xxx(ivari) % l(icode) % kfl_value = 0
             tncod_xxx(ivari) % l(icode) % kfl_funno = 0
             tncod_xxx(ivari) % l(icode) % kfl_fixrs = 0
             tncod_xxx(ivari) % l(icode) % tag       = ''
             tncod_xxx(ivari) % l(icode) % fname     = ''
             do icono = 1,mcono
                tncod_xxx(ivari) % l(icode) % lcode(icono) =  mcodb+1
             end do
             do idofn = 1,ndofn
                tncod_xxx(ivari) % l(icode) % bvess(idofn) =  0.0_rp
             end do
           end do
        end do

    case ( 1_ip )
       !
       ! Allocate type for NVARI variables
       !
       allocate( tncod_xxx(nvari) , stat = istat )

    case ( 2_ip )
       !
       ! Allocate memory for each code ICODE
       !
       allocate( tncod_xxx(nvari) % l(mcodc) , stat = istat )
       tncod_xxx(nvari) % kfl_ibopo = kfl_ibopo
       tncod_xxx(nvari) % ndofn = ndofn     
       tncod_xxx(nvari) % ncode = 0     
       do icode = 1,mcodc
          allocate( tncod_xxx(nvari) % l(icode) % lcode(mcono) , stat= istat )
          allocate( tncod_xxx(nvari) % l(icode) % bvess(ndofn) , stat= istat )
          tncod_xxx(nvari) % l(icode) % kfl_fixno = 0
          tncod_xxx(nvari) % l(icode) % kfl_value = 0
          tncod_xxx(nvari) % l(icode) % kfl_funno = 0
          tncod_xxx(nvari) % l(icode) % kfl_fixrs = 0
          tncod_xxx(nvari) % l(icode) % tag       = ''
          tncod_xxx(nvari) % l(icode) % fname     = ''
          do icono = 1,mcono
             tncod_xxx(nvari) % l(icode) % lcode(icono) =  mcodb+1
          end do
          do idofn = 1,ndofn
             tncod_xxx(nvari) % l(icode) % bvess(idofn) =  0.0_rp
          end do
       end do

    case ( 3_ip )
       !
       ! Deallocate whole type
       !
       do ivari = 1,size(tncod_xxx)
          do icode = 1,mcodc
             deallocate( tncod_xxx(ivari) % l(icode) % lcode , stat= istat )
             deallocate( tncod_xxx(ivari) % l(icode) % bvess , stat= istat )
          end do
          deallocate( tncod_xxx(ivari) % l , stat = istat )
       end do
       deallocate( tncod_xxx , stat = istat )

    end select

  end subroutine opnbcs

  subroutine opbbcs(itask,nvari,ndofn,tbcod_xxx)

    !--------------------------------------------------------------------
    !
    ! BOUNDARY
    !
    !--------------------------------------------------------------------

    implicit none
    integer(ip), intent(in) :: itask,nvari,ndofn
    integer(ip)             :: icode,ivari,idofn
    integer(4)              :: istat
    type(bc_bound), pointer, intent(out) :: tbcod_xxx(:)

    select case ( itask )

    case ( 0_ip )
       !
       ! Allocate type for NVARI variables
       !
       allocate( tbcod_xxx(nvari) , stat = istat )
       do ivari = 1,nvari
          allocate( tbcod_xxx(ivari) % l(mcodc) , stat = istat )
          tbcod_xxx(ivari) % ndofn = ndofn  
          tbcod_xxx(ivari) % ncode = 0
          do icode = 1,mcodc
             allocate( tbcod_xxx(ivari) % l(icode) % bvnat(ndofn) , stat= istat )
             tbcod_xxx(ivari) % l(icode) % kfl_fixbo = 0
             tbcod_xxx(ivari) % l(icode) % kfl_value = 0
             tbcod_xxx(ivari) % l(icode) % kfl_funbo = 0
             do idofn = 1,ndofn
                tbcod_xxx(ivari) % l(icode) % bvnat(idofn) =  0.0_rp
             end do
          end do
       end do

    case ( 1_ip )
       !
       ! Allocate type for NVARI variables
       !
       allocate( tbcod_xxx(nvari) , stat = istat )

    case ( 2_ip )
       !
       ! Allocate memory for each code ICODE
       !
       allocate( tbcod_xxx(nvari) % l(mcodc) , stat = istat )
       tbcod_xxx(nvari) % ndofn = ndofn     
       tbcod_xxx(nvari) % ncode = 0
       do icode = 1,mcodc
          allocate( tbcod_xxx(nvari) % l(icode) % bvnat(ndofn) , stat= istat )
          tbcod_xxx(nvari) % l(icode) % kfl_fixbo =  0
          tbcod_xxx(nvari) % l(icode) % kfl_value =  0
          tbcod_xxx(nvari) % l(icode) % lcode     =  mcodb+1
          tbcod_xxx(nvari) % l(icode) % kfl_funbo =  0
           do idofn = 1,ndofn
             tbcod_xxx(nvari) % l(icode) % bvnat(idofn) =  0.0_rp
          end do
      end do

    case ( 3_ip )
       !
       ! Deallocate whole type
       !
       do ivari = 1,size(tbcod_xxx)
          do icode = 1,mcodc
             deallocate( tbcod_xxx(ivari) % l(icode) % bvnat , stat= istat )
          end do
          deallocate( tbcod_xxx(ivari) % l , stat = istat )
       end do
       deallocate( tbcod_xxx , stat = istat )

    end select

  end subroutine opbbcs

  subroutine spnbcs(tncod_xxx)

    !--------------------------------------------------------------------
    !
    ! PARALL for TNCOD_XXX
    !
    !--------------------------------------------------------------------

    implicit none
    integer(ip)             :: dummi,idofn,nvari
    integer(ip)             :: ivari,icono,ndofn,icode
    integer(4)              :: istat
    type(bc_nodes), pointer, intent(inout) :: tncod_xxx(:)

    if( IPARALL .and. associated(tncod_xxx) ) then

       nvari = size(tncod_xxx)

       do parii = 1,2 
          npari = 0
          nparr = 0
          nparc = 0
          nparx = 0

          do ivari = 1,nvari
             ndofn = tncod_xxx(ivari) % ndofn
             call iexcha( tncod_xxx(ivari) % kfl_ibopo )
             call iexcha( tncod_xxx(ivari) % ndofn )  
             call iexcha( tncod_xxx(ivari) % ncode )
             do icode = 1,mcodc                  
                call iexcha( tncod_xxx(ivari) % l(icode) % kfl_fixno  )
                call iexcha( tncod_xxx(ivari) % l(icode) % kfl_value  )
                call iexcha( tncod_xxx(ivari) % l(icode) % kfl_funno  )
                call iexcha( tncod_xxx(ivari) % l(icode) % kfl_fixrs  )
                call cexcha( 5_ip,tncod_xxx(ivari) % l(icode) % tag   )
                call cexcha( 5_ip,tncod_xxx(ivari) % l(icode) % fname )
                do icono = 1,mcono
                   call iexcha( tncod_xxx(ivari) % l(icode) % lcode(icono) )
                end do
                do idofn = 1,ndofn
                   call rexcha( tncod_xxx(ivari) % l(icode) % bvess(idofn) )
                end do
             end do
          end do
          !
          ! Allocate memory for the first pass
          !
          if( parii == 1 ) then
             allocate( parin(npari) )
             allocate( parre(nparr) )
             !call memory_alloca(mem_modul(1:2,modul),'PARIN','mod_opebcs',parin,npari,'DO_NOT_INITIALIZE')
             !call memory_alloca(mem_modul(1:2,modul),'PARRE','mod_opebcs',parre,nparr,'DO_NOT_INITIALIZE')
             if( ISLAVE ) call Parall(2_ip)
          end if
       end do
       if( IMASTER ) call Parall(2_ip)

       deallocate( parin )
       deallocate( parre )
       !call memory_deallo(mem_modul(1:2,modul),'PARRE','mod_opebcs',parre)
       !call memory_deallo(mem_modul(1:2,modul),'PARIN','mod_opebcs',parin)
       !
       ! Master deallocates memory
       !
       if( IMASTER ) then
          call opnbcs(3_ip,dummi,dummi,dummi,tncod_xxx)
       end if

    end if

  end subroutine spnbcs

  subroutine spgbcs(tncod_xxx)

    !--------------------------------------------------------------------
    !
    ! PARALL for TGCOD_XXX
    !
    !--------------------------------------------------------------------

    implicit none
    integer(ip)             :: dummi,idofn,nvari
    integer(ip)             :: ivari,icono,ndofn,icode
    integer(4)              :: istat
    type(bc_nodes), pointer, intent(inout) :: tncod_xxx(:)

    if( kfl_geome /= 0 .and. IPARALL .and. associated(tncod_xxx) ) then

       nvari = size(tncod_xxx)

       do parii = 1,2 
          npari = 0
          nparr = 0
          nparc = 0
          do ivari = 1,nvari
             ndofn = tncod_xxx(ivari) % ndofn
             call iexcha( tncod_xxx(ivari) % kfl_ibopo )
             call iexcha( tncod_xxx(ivari) % ndofn )  
             call iexcha( tncod_xxx(ivari) % ncode )
             do icode = 1,mcodc                  
                call iexcha( tncod_xxx(ivari) % l(icode) % kfl_fixno )
                call iexcha( tncod_xxx(ivari) % l(icode) % kfl_value )
                call iexcha( tncod_xxx(ivari) % l(icode) % kfl_funno )
                call iexcha( tncod_xxx(ivari) % l(icode) % kfl_fixrs )
                call cexcha( 5_ip , tncod_xxx(ivari) % l(icode) % tag   )
                call cexcha( 5_ip , tncod_xxx(ivari) % l(icode) % fname )
                do icono = 1,mcono
                   call iexcha( tncod_xxx(ivari) % l(icode) % lcode(icono) )
                end do
                do idofn = 1,ndofn
                   call rexcha( tncod_xxx(ivari) % l(icode) % bvess(idofn) )
                end do
             end do
          end do
          !
          ! Allocate memory for the first pass
          !
          if( parii == 1 ) then
             allocate( parin(npari) )
             allocate( parre(nparr) )
             !call memory_alloca(mem_modul(1:2,modul),'PARIN','mod_opebcs',parin,npari,'DO_NOT_INITIALIZE')
             !call memory_alloca(mem_modul(1:2,modul),'PARRE','mod_opebcs',parre,nparr,'DO_NOT_INITIALIZE')
             if( ISLAVE ) call Parall(2_ip)
          end if
       end do
       if( IMASTER ) call Parall(2_ip)

       deallocate( parin )
       deallocate( parre )
       !call memory_deallo(mem_modul(1:2,modul),'PARRE','mod_opebcs',parre)
       !call memory_deallo(mem_modul(1:2,modul),'PARIN','mod_opebcs',parin)
       !
       ! Master deallocates memory
       !
       if( IMASTER ) then
          call opnbcs(3_ip,dummi,dummi,dummi,tncod_xxx)
       end if

    end if

  end subroutine spgbcs

  subroutine spbbcs(tbcod_xxx)

    !--------------------------------------------------------------------
    !
    ! PARALL for TBCOD_XXX
    !
    !--------------------------------------------------------------------

    implicit none
    integer(ip)             :: dummi,idofn,nvari
    integer(ip)             :: ivari,ndofn,icode
    integer(4)              :: istat
    type(bc_bound), pointer, intent(inout) :: tbcod_xxx(:)

    if( kfl_icodb /= 0 .and. IPARALL .and. associated(tbcod_xxx) ) then

       nvari = size(tbcod_xxx)

       do parii = 1,2 
          npari = 0
          nparr = 0
          nparc = 0

          do ivari = 1,nvari
             ndofn = tbcod_xxx(ivari) % ndofn
             call iexcha( tbcod_xxx(ivari) % ndofn )  
             call iexcha( tbcod_xxx(ivari) % ncode )
             do icode = 1,mcodc                  
                call iexcha( tbcod_xxx(ivari) % l(icode) % kfl_fixbo )
                call iexcha( tbcod_xxx(ivari) % l(icode) % kfl_value )
                call iexcha( tbcod_xxx(ivari) % l(icode) % kfl_funbo )
                call iexcha( tbcod_xxx(ivari) % l(icode) % lcode )
                do idofn = 1,ndofn
                   call rexcha( tbcod_xxx(ivari) % l(icode) % bvnat(idofn) )
                end do
             end do
          end do
          !
          ! Allocate memory for the first pass
          !
          if( parii == 1 ) then
             allocate( parin(npari) )
             allocate( parre(nparr) )
             !call memory_alloca(mem_modul(1:2,modul),'PARIN','mod_opebcs',parin,npari,'DO_NOT_INITIALIZE')
             !call memory_alloca(mem_modul(1:2,modul),'PARRE','mod_opebcs',parre,nparr,'DO_NOT_INITIALIZE')
             if( ISLAVE ) call Parall(2_ip)
          end if
       end do
       if( IMASTER ) call Parall(2_ip)

       deallocate( parin )
       deallocate( parre )
       !call memory_deallo(mem_modul(1:2,modul),'PARRE','mod_opebcs',parre)
       !call memory_deallo(mem_modul(1:2,modul),'PARIN','mod_opebcs',parin)

       !kfl_ptask = kfl_ptask_old
       !call vocabu(-1_ip,0_ip,0_ip) 
       !
       ! Master deallocates memory
       !
       if( IMASTER ) then
          call opbbcs(3_ip,dummi,dummi,tbcod_xxx)
       end if

    end if

  end subroutine spbbcs

  subroutine cpybcs(ivari,ivaro,tncod_xxx)

    !--------------------------------------------------------------------
    !
    ! Copy TNCOD_XXX to TNCOD_YYY
    !
    !--------------------------------------------------------------------

    implicit none
    integer(ip),    intent(in)    :: ivari,ivaro
    type(bc_nodes), intent(inout) :: tncod_xxx(:)
    integer(ip)                   :: dummi,idofn
    integer(ip)                   :: icono,ndofn,icode

    if( kfl_icodn /= 0) then

       ndofn = tncod_xxx(ivari) % ndofn
       tncod_xxx(ivaro) % kfl_ibopo = tncod_xxx(ivari) % kfl_ibopo 
       tncod_xxx(ivaro) % ndofn     = tncod_xxx(ivari) % ndofn      
       tncod_xxx(ivaro) % ncode     = tncod_xxx(ivari) % ncode      
       do icode = 1,mcodc                  
          tncod_xxx(ivaro) % l(icode) % kfl_fixno = tncod_xxx(ivari) % l(icode) % kfl_fixno
          tncod_xxx(ivaro) % l(icode) % kfl_value = tncod_xxx(ivari) % l(icode) % kfl_value
          tncod_xxx(ivaro) % l(icode) % kfl_funno = tncod_xxx(ivari) % l(icode) % kfl_funno
          tncod_xxx(ivaro) % l(icode) % kfl_fixrs = tncod_xxx(ivari) % l(icode) % kfl_fixrs
          tncod_xxx(ivaro) % l(icode) % tag       = tncod_xxx(ivari) % l(icode) % tag
          tncod_xxx(ivaro) % l(icode) % fname     = tncod_xxx(ivari) % l(icode) % fname
          do icono = 1,mcono
             tncod_xxx(ivaro) % l(icode) % lcode(icono) = tncod_xxx(ivari) % l(icode) % lcode(icono)
          end do
          do idofn = 1,ndofn
             tncod_xxx(ivaro) % l(icode) % bvess(idofn) = tncod_xxx(ivari) % l(icode) % bvess(idofn)
          end do
       end do

    end if

  end subroutine cpybcs



end module mod_opebcs

