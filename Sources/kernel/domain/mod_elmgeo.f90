!-----------------------------------------------------------------------
!
!> @addtogroup ElementalGeometricToolBox
!> @{
!> @name    ToolBox for elemental and general geometrical operations
!> @file    mod_elmgeo.f90
!> @author  Guillaume Houzeaux
!> @brief   ToolBox for elements
!> @details Different functions useful in finite element implementations
!> @{
!
!-----------------------------------------------------------------------

module mod_elmgeo

  use def_kintyp, only : ip,rp,lg,i1p,i2p
  use def_elmtyp, only : POINT
  use def_elmtyp, only : BAR02,BAR03,BAR04,TRI03,TRI06,QUA04,QUA08,QUA09,QUA16
  use def_elmtyp, only : TET04,TET10,PYR05,PYR14,PEN06,PEN15,PEN18,HEX08
  use def_elmtyp, only : HEX20,HEX27,HEX64,SHELL,BAR3D
  implicit none
  private
  integer(ip), parameter :: perm1(3) = (/2,1,1/)
  integer(ip), parameter :: perm2(3) = (/3,3,2/)
  real(rp),    parameter :: epsil    = epsilon(1.0_rp)
  !
  ! List of edges and faces
  !
  integer(ip), parameter :: list_edges_BAR02(2,1)  = reshape ( (/ 1,       2                                                 /), (/2,1 /) )
  integer(ip), parameter :: list_faces_BAR02(1,2)  = reshape ( (/ 1,       2                                                 /), (/1,2 /) )
  integer(ip), parameter :: type_faces_BAR02(2)    =           (/ POINT,   POINT                                             /)
  integer(ip), parameter :: node_faces_BAR02(2)    =           (/ 1,       1                                                 /)

  integer(ip), parameter :: list_edges_TRI03(2,3)  = reshape ( (/ 1,2, 2,3, 3,1                                              /), (/2,3 /) )
  integer(ip), parameter :: list_faces_TRI03(2,3)  = reshape ( (/ 1,2,     2,3,     3,1                                      /), (/2,3 /) )
  integer(ip), parameter :: type_faces_TRI03(3)    =           (/ BAR02,   BAR02,   BAR02                                    /)
  integer(ip), parameter :: node_faces_TRI03(3)    =           (/ 2,       2,       2                                        /)

  integer(ip), parameter :: list_edges_QUA04(2,4)  = reshape ( (/ 1,2, 2,3, 3,4, 4,1                                         /), (/2,4 /) )
  integer(ip), parameter :: list_faces_QUA04(2,4)  = reshape ( (/ 1,2,     2,3,     3,4,     4,1                             /), (/2,4 /) )
  integer(ip), parameter :: type_faces_QUA04(4)    =           (/ BAR02,   BAR02,   BAR02,   BAR02                           /)
  integer(ip), parameter :: node_faces_QUA04(4)    =           (/ 2,       2 ,      2,       2                               /)

  integer(ip), parameter :: list_edges_TET04(2,6)  = reshape ( (/ 1,2, 1,3, 1,4, 2,3, 4,2, 3,4                               /), (/2,6 /) )
  integer(ip), parameter :: list_faces_TET04(3,4)  = reshape ( (/ 1,3,2,   2,3,4,   1,2,4,   3,1,4                           /), (/3,4 /) )
  integer(ip), parameter :: type_faces_TET04(4)    =           (/ TRI03,   TRI03,   TRI03,   TRI03                           /)
  integer(ip), parameter :: node_faces_TET04(4)    =           (/ 3,       3,       3,       3                               /)

  integer(ip), parameter :: list_edges_PYR05(2,8)  = reshape ( (/ 1,2, 2,3, 3,4, 4,1, 1,5, 2,5, 3,5, 4,5                     /), (/2,8 /) )
  integer(ip), parameter :: list_faces_PYR05(4,5)  = reshape ( (/ 1,4,3,2, 1,2,5,0, 2,3,5,0, 3,4,5,0, 4,1,5,0                /), (/4,5 /) )
  integer(ip), parameter :: type_faces_PYR05(5)    =           (/ QUA04,   TRI03,   TRI03,   TRI03,   TRI03                  /)
  integer(ip), parameter :: node_faces_PYR05(5)    =           (/ 4,       3,       3,       3,       3                      /)

  integer(ip), parameter :: list_edges_PEN06(2,9)  = reshape ( (/ 1,2, 2,3, 3,1, 4,5, 5,6, 6,4, 1,4, 3,6, 2,5                /), (/2,9 /) )
  integer(ip), parameter :: list_faces_PEN06(4,5)  = reshape ( (/ 1,2,5,4, 2,3,6,5, 3,1,4,6, 1,3,2,0, 4,5,6,0                /), (/4,5 /) )
  integer(ip), parameter :: type_faces_PEN06(5)    =           (/ QUA04,   QUA04,   QUA04,   TRI03,   TRI03                  /)
  integer(ip), parameter :: node_faces_PEN06(5)    =           (/ 4,       4,       4,       3,       3                      /)

  integer(ip), parameter :: list_edges_HEX08(2,12) = reshape ( (/ 1,2, 1,4, 1,5, 2,3, 2,6, 3,4, 3,7, 4,8, 5,6, 5,8, 6,7, 7,8 /), (/2,12/) )
  integer(ip), parameter :: list_faces_HEX08(4,6)  = reshape ( (/ 1,4,3,2, 2,3,7,6, 5,6,7,8, 4,1,5,8, 1,2,6,5, 3,4,8,7       /), (/4,6 /) )
  integer(ip), parameter :: type_faces_HEX08(6)    =           (/ QUA04,   QUA04,   QUA04,   QUA04,   QUA04,   QUA04         /)
  integer(ip), parameter :: node_faces_HEX08(6)    =           (/ 4,       4,       4,       4,       4,       4             /)
  !
  ! Element type
  !
  integer(ip), parameter  :: nelty = 60
  type elem_typ
     character(5)         :: name             !< Name of the element
     character(20)        :: nametopo         !< Name of element topology
     integer(ip)          :: dimension        !< Dimension of the element
     integer(ip)          :: order            !< Order of the interpolation
     integer(ip)          :: topology         !< Topologoy of the element
     integer(ip)          :: number_nodes     !< Number of nodes
     logical(lg)          :: hessian          !< If Hessain exists
     integer(ip)          :: max_face_nodes   !< Max number of node per face
     integer(ip)          :: number_faces     !< Number of faces
     integer(ip)          :: number_edges     !< Number of edges (excluding diagonal and repeated edges)
     integer(ip), pointer :: list_edges(:,:)  !< List of edges
     integer(ip), pointer :: type_faces(:)    !< Face types
     integer(ip), pointer :: node_faces(:)    !< Face number of nodes
     integer(ip), pointer :: list_faces(:,:)  !< List of faces (normal pointing inward)
  end type elem_typ
  type(elem_typ) :: element_type(nelty)
  type(elem_typ) :: element_type_init = elem_typ(&
       'NULL',&
       'NULL',&
       0_ip,&
       0_ip,&
       0_ip,&
       0_ip,&
       .false.,&
       0_ip,&
       0_ip,&
       0_ip,&
       null(),&
       null(),&
       null(),&
       null())

  public :: elmgeo_element_characteristic_length
  public :: elmgeo_segpla
  public :: elmgeo_segfac
  public :: elmgeo_bounor
  public :: elmgeo_natural_coordinates
  public :: elmgeo_shapf_deriv_heslo
  public :: elmgeo_inside_TET04
  public :: elmgeo_inside_TRI03_QUA04
  public :: elmgeo_newrap
  public :: elmgeo_where_is
  public :: elmgeo_shape2
  public :: elmgeo_shape3
  public :: elmgeo_shapf_deriv_heslo_bubble
  public :: elmgeo_jacobian_matrix
  public :: elmgeo_natural_coordinates_on_boundaries
  public :: elmgeo_inside_element_bounding_box
  public :: elmgeo_inside_element_using_faces
  public :: elmgeo_element_type_initialization
  public :: elmgeo_nearest_point_on_element_faces
  public :: elmgeo_cartesian_derivatives
  public :: elmgeo_projection_on_a_face
  public :: elmgeo_intersection_segment_face
  public :: elmgeo_intersection_segment_QUA04

  public :: list_faces_BAR02,type_faces_BAR02
  public :: list_faces_TRI03,type_faces_TRI03
  public :: list_faces_QUA04,type_faces_QUA04
  public :: list_faces_TET04,type_faces_TET04
  public :: list_faces_PYR05,type_faces_PYR05
  public :: list_faces_PEN06,type_faces_PEN06
  public :: list_faces_HEX08,type_faces_HEX08
  public :: element_type

contains 

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Fill in element data base
  !> @details Element data base in mod_elmgeo. This redundant information
  !>          is required so that mod_elmgeo can be a stand-alone 
  !>          module. Optional arguments can be used to test the 
  !>          coherence between this database and Alya's one.
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_element_type_initialization(&
       iesta_dom,iesto_dom,nface,lexis,needg,leedg,&
       ldime,lorde,ltopo,nnode,nnodf,ltypf,lface)

    integer(ip), intent(in), optional :: iesta_dom
    integer(ip), intent(in), optional :: iesto_dom
    integer(ip), intent(in), optional :: nface(*)
    integer(ip), intent(in), optional :: lexis(*)
    integer(ip), intent(in), optional :: needg(*)
    integer(ip), intent(in), optional :: leedg(2,20,*)
    integer(ip), intent(in), optional :: ldime(*)
    integer(ip), intent(in), optional :: lorde(*)
    integer(ip), intent(in), optional :: ltopo(*)
    integer(ip), intent(in), optional :: nnode(*)
    type(i1p),   intent(in), optional :: nnodf(*)
    type(i1p),   intent(in), optional :: ltypf(*)
    type(i2p),   intent(in), optional :: lface(*)
    integer(ip)                       :: iface,inodf,pnodf,pflty,iedge
    integer(ip)                       :: inode,ielty,pface,pedge

    element_type(1:nelty) = element_type_init
    !                                                          dim  ord top nnode hess   max face edge
    element_type(POINT) = elem_typ( 'POINT' , 'Point'         , 0 , 1 ,-2 , 1 , .false. , 0 , 0 , 0 , null(), null(), null(), null() )
    element_type(BAR02) = elem_typ( 'BAR02' , 'Linear'        , 1 , 1 ,-1 , 2 , .false. , 1 , 2 , 1 , null(), null(), null(), null() )
    element_type(BAR03) = elem_typ( 'BAR03' , 'Linear'        , 1 , 2 ,-1 , 3 , .true.  , 1 , 2 , 0 , null(), null(), null(), null() )
    element_type(BAR04) = elem_typ( 'BAR04' , 'Linear'        , 1 , 3 ,-1 , 4 , .true.  , 1 , 2 , 0 , null(), null(), null(), null() )
    element_type(TRI03) = elem_typ( 'TRI03' , 'Triangle'      , 2 , 1 , 1 , 3 , .false. , 2 , 3 , 3 , null(), null(), null(), null() )
    element_type(TRI06) = elem_typ( 'TRI06' , 'Triangle'      , 2 , 2 , 1 , 6 , .true.  , 3 , 3 , 0 , null(), null(), null(), null() )
    element_type(QUA04) = elem_typ( 'QUA04' , 'Quadrilateral' , 2 , 1 , 0 , 4 , .true.  , 2 , 4 , 4 , null(), null(), null(), null() )
    element_type(QUA08) = elem_typ( 'QUA08' , 'Quadrilateral' , 2 , 2 , 0 , 8 , .true.  , 3 , 4 , 0 , null(), null(), null(), null() )
    element_type(QUA09) = elem_typ( 'QUA09' , 'Quadrilateral' , 2 , 2 , 0 , 9 , .true.  , 3 , 4 , 0 , null(), null(), null(), null() )
    element_type(QUA16) = elem_typ( 'QUA16' , 'Quadrilateral' , 2 , 3 , 0 ,16 , .true.  , 4 , 4 , 0 , null(), null(), null(), null() )
    element_type(TET04) = elem_typ( 'TET04' , 'Tetrahedra'    , 3 , 1 , 1 , 4 , .false. , 3 , 4 , 6 , null(), null(), null(), null() )
    element_type(TET10) = elem_typ( 'TET10' , 'Tetrahedra'    , 3 , 2 , 1 ,10 , .true.  , 0 , 0 , 0 , null(), null(), null(), null() )
    element_type(PYR05) = elem_typ( 'PYR05' , 'Pyramid'       , 3 , 1 , 3 , 5 , .false. , 4 , 5 , 8 , null(), null(), null(), null() )
    element_type(PYR14) = elem_typ( 'PYR14' , 'Pyramid'       , 3 , 2 , 3 ,14 , .true.  , 0 , 0 , 0 , null(), null(), null(), null() )
    element_type(PEN06) = elem_typ( 'PEN06' , 'Prism'         , 3 , 1 , 2 , 6 , .false. , 4 , 5 , 9 , null(), null(), null(), null() )
    element_type(PEN15) = elem_typ( 'PEN15' , 'Prism'         , 3 , 2 , 2 ,15 , .true.  , 0 , 0 , 0 , null(), null(), null(), null() )
    element_type(PEN18) = elem_typ( 'PEN18' , 'Prism'         , 3 , 2 , 2 ,18 , .true.  , 0 , 0 , 0 , null(), null(), null(), null() )
    element_type(HEX08) = elem_typ( 'HEX08' , 'Hexahedra'     , 3 , 1 , 0 , 8 , .false. , 4 , 6 ,12 , null(), null(), null(), null() )
    element_type(HEX20) = elem_typ( 'HEX20' , 'Hexahedra'     , 3 , 2 , 0 ,20 , .false. , 0 , 0 , 0 , null(), null(), null(), null() )
    element_type(HEX27) = elem_typ( 'HEX27' , 'Hexahedra'     , 3 , 2 , 0 ,27 , .false. , 0 , 0 , 0 , null(), null(), null(), null() )
    element_type(SHELL) = elem_typ( 'SHELL' , 'Triangle'      , 2 , 1 , 1 , 3 , .false. , 2 , 3 , 3 , null(), null(), null(), null() )
    element_type(BAR3D) = elem_typ( 'BAR3D' , 'Linear'        , 1 , 1 ,-1 , 2 , .false. , 1 , 2 , 1 , null(), null(), null(), null() )

    do ielty = 1,nelty

       pface = element_type(ielty) % number_faces
       pnodf = element_type(ielty) % max_face_nodes
       pedge = element_type(ielty) % number_edges

       allocate( element_type(ielty) % type_faces(pface)       )
       allocate( element_type(ielty) % node_faces(pface)       )
       allocate( element_type(ielty) % list_faces(pnodf,pface) )
       allocate( element_type(ielty) % list_edges(2,pedge)     )

       if( ielty == BAR02 ) then
          !
          ! BAR elements: BAR02 to BAR04 
          !
          element_type(ielty) % type_faces = type_faces_BAR02
          element_type(ielty) % node_faces = node_faces_BAR02
          element_type(ielty) % list_faces = list_faces_BAR02
          element_type(ielty) % list_edges = list_edges_BAR02

       else if( ielty == TRI03 ) then
          !
          ! TRI03
          !
          element_type(ielty) % type_faces = type_faces_TRI03
          element_type(ielty) % node_faces = node_faces_TRI03
          element_type(ielty) % list_faces = list_faces_TRI03
          element_type(ielty) % list_edges = list_edges_TRI03

       else if( ielty == QUA04 ) then
          !
          ! QUA04
          !
          element_type(ielty) % type_faces = type_faces_QUA04
          element_type(ielty) % node_faces = node_faces_QUA04
          element_type(ielty) % list_faces = list_faces_QUA04
          element_type(ielty) % list_edges = list_edges_QUA04

       else if( ielty == TRI06 ) then
          !
          ! TRI06
          !

       else if( ielty == QUA08 ) then
          !
          ! QUA08 
          !

       else if( ielty == QUA09 ) then
          !
          ! QUA09
          !

       else if ( ielty == QUA16 ) then
          !
          ! QUA16
          !

       else if( ielty == TET04 ) then
          !
          ! TET04
          !
          element_type(ielty) % type_faces = type_faces_TET04
          element_type(ielty) % node_faces = node_faces_TET04
          element_type(ielty) % list_faces = list_faces_TET04
          element_type(ielty) % list_edges = list_edges_TET04

       else if( ielty == PYR05 ) then
          !
          ! PYR05
          !
          element_type(ielty) % type_faces = type_faces_PYR05
          element_type(ielty) % node_faces = node_faces_PYR05
          element_type(ielty) % list_faces = list_faces_PYR05
          element_type(ielty) % list_edges = list_edges_PYR05

       else if( ielty == PEN06 ) then
          !
          ! PEN06
          !
          element_type(ielty) % type_faces = type_faces_PEN06
          element_type(ielty) % node_faces = node_faces_PEN06
          element_type(ielty) % list_faces = list_faces_PEN06
          element_type(ielty) % list_edges = list_edges_PEN06

       else if( ielty == HEX08 ) then
          !
          ! HEX08
          !
          element_type(ielty) % type_faces = type_faces_HEX08
          element_type(ielty) % node_faces = node_faces_HEX08
          element_type(ielty) % list_faces = list_faces_HEX08
          element_type(ielty) % list_edges = list_edges_HEX08

       else if( ielty == TET10 ) then
          !
          ! TET10
          !

       else if( ielty == HEX27 ) then
          !
          ! HEX27
          !

       else if ( ielty == SHELL ) then
          !
          ! SHELL
          !
          element_type(ielty) % type_faces = type_faces_TRI03
          element_type(ielty) % node_faces = node_faces_TRI03
          element_type(ielty) % list_faces = list_faces_TRI03
          element_type(ielty) % list_edges = list_edges_TRI03

       else if( ielty == BAR3D ) then
          !
          ! BAR3D
          !
          element_type(ielty) % type_faces = type_faces_BAR02
          element_type(ielty) % node_faces = node_faces_BAR02
          element_type(ielty) % list_faces = list_faces_BAR02
          element_type(ielty) % list_edges = list_edges_BAR02

       else

          !call runend('FACES NOT PROGRAMMED')
       end if

    end do

    if( present(iesto_dom) ) then

       do ielty = iesta_dom,iesto_dom
          if( trim(element_type(ielty) % name) /= 'NULL' ) then
             if( ldime(ielty) /= element_type(ielty) % dimension    ) print*,'ERROR  1: ',element_type(ielty) % name
             if( lorde(ielty) /= element_type(ielty) % order        ) print*,'ERROR  2: ',element_type(ielty) % name
             if( ltopo(ielty) /= element_type(ielty) % topology     ) print*,'ERROR  3: ',element_type(ielty) % name
             if( nnode(ielty) /= element_type(ielty) % number_nodes ) print*,'ERROR  4: ',element_type(ielty) % name,nnode(ielty),element_type(ielty) % number_nodes
             if( needg(ielty) /= element_type(ielty) % number_edges ) print*,'ERROR  5: ',element_type(ielty) % name
             if( nface(ielty) /= element_type(ielty) % number_faces ) print*,'ERROR  6: ',element_type(ielty) % name
             if( lexis(ielty) == 1 ) then
                do iface = 1,nface(ielty)
                   !print*,'CHECKING FACE ',iface,' OF ELEMENT '//element_type(ielty) % name
                   pnodf = nnodf(ielty) % l(iface)
                   pflty = ltypf(ielty) % l(iface)
                   if( pnodf /= element_type(ielty) % node_faces(iface) ) print*,'ERROR  7: ',element_type(ielty) % name
                   if( pflty /= element_type(ielty) % type_faces(iface) ) print*,'ERROR  8: ',element_type(ielty) % name
                   do inodf = 1,pnodf
                      inode = lface(ielty) % l(inodf,iface) 
                      if( inode /= element_type(ielty) % list_faces(inodf,iface) ) print*,'ERROR  9: ',element_type(ielty) % name
                   end do
                end do
             end if
             do iedge = 1,needg(ielty)
                !print*,'CHECKING EDGE ',iedge,' OF ELEMENT '//element_type(ielty) % name
                if( leedg(1,iedge,ielty) /= element_type(ielty) % list_edges(1,iedge) ) print*,'ERROR  9: ',element_type(ielty) % name,' =',leedg(1,iedge,ielty),element_type(ielty) % list_edges(1,iedge)
                if( leedg(2,iedge,ielty) /= element_type(ielty) % list_edges(2,iedge) ) print*,'ERROR 10: ',element_type(ielty) % name,' =',leedg(2,iedge,ielty),element_type(ielty) % list_edges(2,iedge)          
             end do
          end if
       end do

    end if

  end subroutine elmgeo_element_type_initialization

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Compute intersection between a segment and a plane
  !> @details Compute intersection between a segment and a plane
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_segpla(ndime,xplan,xcoo1,xcoo2,ifoun,plapo,toler)
    implicit none
    integer(ip),           intent(in)  :: ndime                                      !< Dimension
    real(rp),              intent(in)  :: xplan(*)                                   !< Plane equation
    real(rp),              intent(in)  :: xcoo1(ndime)                               !< Segment first point
    real(rp),              intent(in)  :: xcoo2(ndime)                               !< Segment second point
    integer(ip),           intent(out) :: ifoun                                      !< If inside or not
    real(rp),    optional, intent(out) :: plapo(ndime)                               !< Intersection coordinates
    real(rp),    optional, intent(in)  :: toler                                      !< Tolerance
    real(rp)                           :: t,epsil

    ifoun = 0
    if( present(toler) ) epsil = toler

    if( ndime == 2 ) then
       !
       ! 2D
       !
       t = xplan(1)*(xcoo2(1)-xcoo1(1)) + xplan(2)*(xcoo2(2)-xcoo1(2))

       if( t /= 0.0_rp ) then

          t = (- xplan(3) - xplan(1)*xcoo1(1) - xplan(2)*xcoo1(2) ) / t
          if( t >= epsil .and. t <= 1.0_rp+epsil ) then
             ifoun = 1
             if( present(plapo) ) then
                plapo(1) = t * (xcoo2(1)-xcoo1(1)) + xcoo1(1)
                plapo(2) = t * (xcoo2(2)-xcoo1(2)) + xcoo1(2)
             end if
          end if

       else if( abs( xplan(1) * xcoo1(1) + xplan(2) * xcoo1(2) + xplan(3) ) <= epsil ) then

          ifoun = 1
          if( present(plapo) ) then
             plapo(1) = 0.5_rp * ( xcoo1(1) + xcoo2(1) )
             plapo(2) = 0.5_rp * ( xcoo1(2) + xcoo2(2) )
          end if

       end if

    else
       !
       ! Scalar product t = n.(P1,P2)
       !
       t = xplan(1)*(xcoo2(1)-xcoo1(1)) + xplan(2)*(xcoo2(2)-xcoo1(2)) + xplan(3)*(xcoo2(3)-xcoo1(3))  

       if( t /= 0.0_rp ) then
          !
          ! Compute parametric coordinate t on P1-P2
          !
          t = (- xplan(4) - xplan(1)*xcoo1(1) - xplan(2)*xcoo1(2) - xplan(3)*xcoo1(3) ) / t
          if( t >= epsil .and. t <= 1.0_rp+epsil ) then
             ifoun = 1
             if( present(plapo) ) then
                plapo(1) = t * (xcoo2(1)-xcoo1(1)) + xcoo1(1)
                plapo(2) = t * (xcoo2(2)-xcoo1(2)) + xcoo1(2)
                plapo(3) = t * (xcoo2(3)-xcoo1(3)) + xcoo1(3)
             end if
          end if

       else if( abs( xplan(1) * xcoo1(1) + xplan(2) * xcoo1(2) + xplan(3) * xcoo1(3) + xplan(4) ) <= epsil ) then
          !
          ! (P1,P2) is parallel to plane: check if P1 on plane
          !
          ifoun = 1
          if( present(plapo) ) then
             plapo(1) = 0.5_rp * ( xcoo1(1) + xcoo2(1) )
             plapo(2) = 0.5_rp * ( xcoo1(2) + xcoo2(2) )
             plapo(3) = 0.5_rp * ( xcoo1(3) + xcoo2(3) )
          end if
       end if
    end if

  end subroutine elmgeo_segpla

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute intersection between a segment and a face
  !> @details Compute intersection between a segment and a face
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_segfac(ndime,pnodb,bocod,xcoo1,xcoo2,ifoun,plapo,toler)
    implicit none
    integer(ip),           intent(in)  :: ndime                                      !< Dimension
    integer(ip),           intent(in)  :: pnodb                                      !< Number of boundary nodes
    real(rp),              intent(in)  :: bocod(ndime,pnodb)                         !< Boundary coordinates
    real(rp),              intent(in)  :: xcoo1(ndime)                               !< Segment first point
    real(rp),              intent(in)  :: xcoo2(ndime)                               !< Segment second point
    integer(ip),           intent(out) :: ifoun                                      !< If inside or not
    real(rp),              intent(out) :: plapo(ndime)                               !< Bandwidt
    real(rp),    optional, intent(in)  :: toler                                      !< Tolerance
    integer(ip)                        :: idime,iboun,nboun,inodb
    logical(lg)                        :: kfl_qua04
    real(rp)                           :: boco2(ndime,3)
    real(rp)                           :: bouno(3),eps1,eps2
    real(rp)                           :: numer,denom,temdi,epsil
    !
    ! Tolerance and initialization
    !
    if( present(toler) ) then
       epsil = toler
    else
       epsil = 1.0e-3_rp
    end if

    eps1  = -epsil
    eps2  =  1.0_rp+epsil
    ifoun =  0
    iboun =  0
    !
    ! Check if boundary is a QUA04 element
    !
    if( ndime == 3 .and. pnodb == 4 ) then
       kfl_qua04 = .true.
       nboun     = 2
    else
       kfl_qua04 = .false.
       nboun     = 1
       do inodb = 1,pnodb
          do idime = 1,ndime
             boco2(idime,inodb) = bocod(idime,inodb)
          end do
       end do
    end if

    do while( ifoun == 0 .and. iboun < nboun )
       iboun = iboun + 1
       if( kfl_qua04 ) then
          if( iboun == 1 ) then
             do idime = 1,3
                boco2(idime,1) = bocod(idime,1)
                boco2(idime,2) = bocod(idime,2)
                boco2(idime,3) = bocod(idime,4)
             end do
          else
             do idime = 1,3
                boco2(idime,1) = bocod(idime,2)
                boco2(idime,2) = bocod(idime,3)
                boco2(idime,3) = bocod(idime,4)
             end do
          end if
       end if
       !
       ! bouno: Exterior normal           
       !
       call elmgeo_exttri(1_ip,ndime,pnodb,boco2,bouno)
       !
       ! Get characteristic dimensions
       !
       numer =  0.0_rp
       denom =  0.0_rp
       do idime = 1,ndime
          numer = numer + ( bouno(idime) * ( boco2(idime,1) - xcoo1(idime) )  )
          denom = denom + ( bouno(idime) * ( xcoo2(idime)   - xcoo1(idime) )  )
       end do

       if( denom /= 0.0_rp ) then       
          !
          ! temdi: Normalized Distance measure from the first node
          !
          temdi = numer / denom 
          !
          ! The segment intersects the plane that contains the iboun
          !
          if ( temdi >= eps1 .and. temdi <= eps2 ) then           
             do idime = 1,ndime
                plapo(idime) = xcoo1(idime) + temdi * ( xcoo2(idime) - xcoo1(idime) )
             end do
             !
             ! Determine if the projection point on plane is inside the iboun
             !
             if( ndime == 3 ) then
                call elmgeo_instri(plapo,boco2,epsil,ifoun)
             else if( ndime == 2 ) then
                call elmgeo_insbar(plapo,boco2,epsil,ifoun)
             end if
          end if
       end if
    end do

  end subroutine elmgeo_segfac

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute external normal to a boundary
  !> @details Compute external normal to a boundary
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_exttri(itask,ndime,pnodb,bocod,bouno)
    implicit none
    integer(ip), intent(in)  :: itask
    integer(ip), intent(in)  :: ndime
    integer(ip), intent(in)  :: pnodb
    real(rp),    intent(in)  :: bocod(ndime,pnodb)
    real(rp),    intent(out) :: bouno(ndime)
    integer(ip)              :: p1,p2,p3,idime
    real(rp)                 :: xfact,vec(3,3)

    if( ndime == 2 ) then

       if(itask == 1 ) then
          p1 = 1
          p2 = 2
       else
          p1 = 2
          p2 = 1
       end if

       vec(1,1) =  bocod(1,p2) - bocod(1,p1)
       vec(2,1) =  bocod(2,p2) - bocod(2,p1)
       bouno(1) =  vec(2,1)
       bouno(2) = -vec(1,1)

    else 

       p1 = 1
       if( itask == 1 ) then
          p2 = 2
          p3 = 3  
       else
          p2 = 3
          p3 = 2
       end if

       vec(1,1) = bocod(1,p2) - bocod(1,p1)
       vec(2,1) = bocod(2,p2) - bocod(2,p1)
       vec(3,1) = bocod(3,p2) - bocod(3,p1)
       vec(1,2) = bocod(1,p3) - bocod(1,p1)
       vec(2,2) = bocod(2,p3) - bocod(2,p1)
       vec(3,2) = bocod(3,p3) - bocod(3,p1)

       bouno(1) = vec(2,1) * vec(3,2) - vec(3,1) * vec(2,2) 
       bouno(2) = vec(3,1) * vec(1,2) - vec(1,1) * vec(3,2) 
       bouno(3) = vec(1,1) * vec(2,2) - vec(2,1) * vec(1,2) 

    end if

    xfact = 0.0_rp
    do idime = 1,ndime
       xfact = xfact + bouno(idime) * bouno(idime)
    end do
    xfact = sqrt(xfact)
    if( xfact > 1.0e-12_rp ) then
       xfact = 1.0_rp / xfact
       do idime = 1,ndime
          bouno(idime) = xfact * bouno(idime)
       end do
    end if

  end subroutine elmgeo_exttri

  !-----------------------------------------------------------------------
  !
  !> @brief   Determine if a point is inside a triangle 
  !> @details Determine if a point is inside a triangle using the same side technique
  !!          point: point facoo(dimension,vertices): triangle coordinates
  !!          ifoun: If is equal to 1, the point is inside the triangle, 0 otherwise
  !> @author  Cristobal Samaniego
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_instri(plapo,bocod,epsil,ifoun)
    implicit none
    real(rp),    intent(in)  :: plapo(3)
    real(rp),    intent(in)  :: bocod(3,3)
    real(rp),    intent(in)  :: epsil
    integer(ip), intent(out) :: ifoun
    real(rp)                 :: v0(3),v1(3),v2(3)
    real(rp)                 :: dot00,dot01,dot02,dot11,dot12
    real(rp)                 :: invDenom,bari1,bari2
    !
    ! 3D
    !
    v0(1) = bocod(1,3) - bocod(1,1)
    v0(2) = bocod(2,3) - bocod(2,1)
    v0(3) = bocod(3,3) - bocod(3,1)

    v1(1) = bocod(1,2) - bocod(1,1)
    v1(2) = bocod(2,2) - bocod(2,1)
    v1(3) = bocod(3,2) - bocod(3,1)

    v2(1) = plapo(1)   - bocod(1,1)
    v2(2) = plapo(2)   - bocod(2,1)
    v2(3) = plapo(3)   - bocod(3,1)

    dot00 = v0(1) * v0(1) + v0(2) * v0(2) + v0(3) * v0(3)
    dot01 = v0(1) * v1(1) + v0(2) * v1(2) + v0(3) * v1(3)
    dot02 = v0(1) * v2(1) + v0(2) * v2(2) + v0(3) * v2(3)
    dot11 = v1(1) * v1(1) + v1(2) * v1(2) + v1(3) * v1(3)
    dot12 = v1(1) * v2(1) + v1(2) * v2(2) + v1(3) * v2(3)  
    !
    ! Compute barycentric coordinates
    !
    ifoun = 0
    if( abs(dot00 * dot11 - dot01 * dot01) > epsilon(1.0_rp) ) then
       invDenom = 1.0_rp / (dot00 * dot11 - dot01 * dot01)
       bari1    = (dot11 * dot02 - dot01 * dot12) * invDenom
       bari2    = (dot00 * dot12 - dot01 * dot02) * invDenom
       !
       ! Check
       !
       if( bari1 >= -epsil .and. bari2 >= -epsil .and. bari1 + bari2 <= 1.0_rp+epsil ) then
          ifoun = 1
       end if
    end if

  end subroutine elmgeo_instri

  subroutine elmgeo_insbar(plapo,bocod,epsil,ifoun)
    !-----------------------------------------------------------------------
    ! NAME
    !    segdis
    ! DESCRIPTION
    !    Minimun distance between a point and a segment
    !    plapo : point plapoinates 
    !    coor1,coor2 : defines the segment
    !    ndime: dimension
    !    dista: distance
    !    proje: projection of the point on the segment
    !    ifoun = 1 the projection point is inside the segment
    !    ifoun = 0 the projection point is outside the segment
    ! USED BY
    !    pofadi
    !***
    !----------------------------------------------------------------------- 
    implicit none
    real(rp),    intent(in)  :: plapo(2)
    real(rp),    intent(in)  :: bocod(2,2)
    real(rp),    intent(in)  :: epsil
    integer(ip), intent(out) :: ifoun
    integer(ip)              :: idime
    real(rp)                 :: numer,denom,dsegm,eps1,eps2

    eps1  = -epsil
    eps2  = 1.0_rp + epsil
    numer = 0.0_rp
    denom = 0.0_rp
    do idime = 1,2
       numer = numer + (bocod(idime,2) - bocod(idime,1)) * (plapo(idime)   - bocod(idime,1))
       denom = denom + (bocod(idime,2) - bocod(idime,1)) * (bocod(idime,2) - bocod(idime,1))
    end do

    dsegm = numer / denom

    if( dsegm < eps1 ) then    

       ifoun = 0_ip

    else if ( dsegm >= eps1 .and. dsegm <= eps2 ) then

       ifoun = 1_ip

    else

       ifoun = 0_ip

    end if

  end subroutine elmgeo_insbar

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute the quality of a TET04 element
  !> @details Determine the quality based on gamma-team (INRIA) criterion
  !>          P.L. George, Improvement on Delaunay based 3D automatic mesh generator, 
  !>          Finite Elements in Analysis and Design, Vol. 25, pp. 297--317, 1997
  !> @date    26/02/2013
  !> @author  Beatriz Eguzkitza
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_quatet_gamma(elcod,Q,sign)
    implicit none
    real(rp),    intent(in)  :: elcod(3,4)
    real(rp),    intent(out) :: Q
    integer(ip), intent(out) :: sign
    integer(ip)              :: idime
    real(rp)                 :: hmax,alpha,volum,sur1,sur2,sur3,sur4,surf
    real(rp)                 :: side1,side2,side3,side4,side5,side6,hbase
    real(rp)                 :: cora1(3),cora2(3),cora4(3)
    real(rp)                 :: corb1(3),corb2(3),corb3(3)
    !
    ! Edges vectors
    !
    do idime = 1,3
       cora1(idime) = elcod(idime,1) - elcod(idime,2)  ! 2 -> 1
       corb1(idime) = elcod(idime,4) - elcod(idime,2)  ! 2 -> 4 
       cora2(idime) = elcod(idime,4) - elcod(idime,1)  ! 1 -> 4  
       corb2(idime) = elcod(idime,3) - elcod(idime,1)  ! 1 -> 3 
       corb3(idime) = elcod(idime,3) - elcod(idime,2)  ! 2 -> 3 
       cora4(idime) = elcod(idime,4) - elcod(idime,3)  ! 3 -> 4
    end do
    !
    ! hmax
    !
    side1 = 0.0_rp
    side2 = 0.0_rp
    side3 = 0.0_rp
    side4 = 0.0_rp
    side5 = 0.0_rp
    side6 = 0.0_rp

    do idime = 1,3 
       side1 = side1 + cora1(idime) * cora1(idime) 
       side2 = side2 + corb1(idime) * corb1(idime)
       side3 = side3 + cora2(idime) * cora2(idime)
       side4 = side4 + corb2(idime) * corb2(idime) 
       side5 = side5 + corb3(idime) * corb3(idime) 
       side6 = side6 + cora4(idime) * cora4(idime)
    end do

    hbase = min(side1,side3,side4)

    side1 = sqrt(side1)
    side2 = sqrt(side2)
    side3 = sqrt(side3)
    side4 = sqrt(side4)
    side5 = sqrt(side5)
    side6 = sqrt(side6)
    hmax  = max(side1,side2,side3,side4,side5,side6)
    !
    ! SURF= Total surface
    !
    sur1 =    ( cora1(1) * corb1(2) - cora1(2) * corb1(1)) * (cora1(1) * corb1(2) - cora1(2) * corb1(1) ) &
         &  + ( cora1(3) * corb1(1) - cora1(1) * corb1(3)) * (cora1(3) * corb1(1) - cora1(1) * corb1(3) ) &
         &  + ( cora1(2) * corb1(3) - cora1(3) * corb1(2)) * (cora1(2) * corb1(3) - cora1(3) * corb1(2) )

    sur2 =    ( cora2(1) * corb2(2) - corb2(1) * cora2(2)) * (cora2(1) * corb2(2) - corb2(1) * cora2(2) ) &
         &  + ( cora2(3) * corb2(1) - cora2(1) * corb2(3)) * (cora2(3) * corb2(1) - cora2(1) * corb2(3) ) &
         &  + ( cora2(2) * corb2(3) - cora2(3) * corb2(2)) * (cora2(2) * corb2(3) - cora2(3) * corb2(2) )

    sur3 =    ( cora1(1) * corb3(2) - corb3(1) * cora1(2)) * (cora1(1) * corb3(2) - corb3(1) * cora1(2) ) &
         &  + ( cora1(3) * corb3(1) - cora1(1) * corb3(3)) * (cora1(3) * corb3(1) - cora1(1) * corb3(3) ) &
         &  + ( cora1(2) * corb3(3) - cora1(3) * corb3(2)) * (cora1(2) * corb3(3) - cora1(3) * corb3(2) )

    sur4 =    ( cora4(1) * corb1(2) - corb1(1) * cora4(2)) * (cora4(1) * corb1(2) - corb1(1) * cora4(2) ) &
         &  + ( cora4(3) * corb1(1) - cora4(1) * corb1(3)) * (cora4(3) * corb1(1) - cora4(1) * corb1(3) ) &
         &  + ( cora4(2) * corb1(3) - cora4(3) * corb1(2)) * (cora4(2) * corb1(3) - cora4(3) * corb1(2) )

    surf = 0.5_rp * ( sqrt(sur1) + sqrt(sur2) + sqrt(sur3) + sqrt(sur4) )
    !
    ! VOLUM= Volume and SIGN= sign
    !
    volum = (  cora1(1) * cora2(2) * corb2(3) + cora1(2) * cora2(3) * corb2(1) + cora1(3) * cora2(1) * corb2(2) &
         &    -cora1(3) * cora2(2) * corb2(1) - cora1(2) * cora2(1) * corb2(3) - cora1(1) * cora2(3) * corb2(2) ) / 6.0_rp
    if( volum <= 0.0_rp ) then
       sign = -1_ip
    else
       sign  = 1_ip
    end if
    !
    ! Q= Quality  
    !
    alpha = sqrt(6.0_rp)/12.0_rp
    Q     = alpha * hmax * surf / ( 3.0_rp * abs(volum) + epsil )

  end subroutine elmgeo_quatet_gamma

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute the quality and aspect ratio of a TET04 element
  !> @details Determine the quality based on coondition number kappaS and
  !>          aspect ratio asrad. See Knupp criterion:
  !>          P.Knupp, "Algebraic Mesh Quality Metrics," SIAM J. Sci. 
  !>          Comput., Vol. 23, No. 1, pp193-218, 2001.
  !> @date    26/02/2013
  !> @author  Beatriz Eguzkitza
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_quatet_kappar(ndime,elcod,kappaS,asrad)
    implicit none
    integer(ip), intent(in)           :: ndime
    real(rp),    intent(in)           :: elcod(ndime,*)
    real(rp),    intent(out)          :: kappaS
    real(rp),    intent(out)          :: asrad
    integer(ip)                       :: idime,inode,jnode,tetra,ii,jj,pnode,pelty
    real(rp)                          :: invW(ndime,3),tinvW(3,ndime),invWt(3,ndime)
    real(rp)                          :: A(ndime,3),S(ndime,3),invS(ndime,3)
    real(rp)                          :: normS,norminvS,StS(ndime,3)
    real(rp)                          :: invSt(ndime,3)
    real(rp)                          :: aux(ndime,3),aux2(ndime,3)
    real(rp)                          :: detinvWt,detS,detSt,volum
    real(rp)                          :: xjaci(ndime,ndime),xjacm(ndime,ndime),detjm
    real(rp)                          :: gpcar(ndime,4),sumsi,t1,t2,t3
    real(rp)                          :: side1(ndime),side2(ndime),side3(ndime)
    real(rp)                          :: side4(ndime),side5(ndime),side6(ndime)
    !
    ! Condition number kappaS
    !
    detinvWt  =  0.0_rp
    detS      =  0.0_rp
    if( ndime == 3 ) then
       pnode     =  4
       invW(1,1) =  1.0_rp
       invW(1,2) = -1.0_rp/3.0_rp * sqrt(3.0_rp)         ! -5.77350269189626e-01
       invW(1,3) = -1.0_rp/6.0_rp * sqrt(6.0_rp)         ! -4.08248290463863e-01 
       invW(2,1) =  0.0_rp
       invW(2,2) =  2.0_rp/3.0_rp * sqrt(3.0_rp)         !  1.15470053837925e+00 
       invW(2,3) = -1.0_rp/6.0_rp * sqrt(6.0_rp)         ! -4.08248290463863e-01
       invW(3,1) =  0.0_rp
       invW(3,2) =  0.0_rp
       invW(3,3) =  1.0_rp/2.0_rp * sqrt(6.0_rp)         !  1.22474487139159e+00
    else
       pnode     =  3
       invW(1,1) =  1.0_rp
       invW(1,2) =  1.0_rp/2.0_rp              
       invW(2,1) =  0.0_rp
       invW(2,2) =  sqrt(3.0_rp)/2.0_rp                  !  1.15470053837925e+00 
    end if

    normS    =  0.0_rp
    norminvS =  0.0_rp

    do inode = 1,pnode-1
       do idime = 1,ndime
          A(idime,inode)    = 0.0_rp
          S(idime,inode)    = 0.0_rp
          StS (idime,inode) = 0.0_rp
          invS(idime,inode) = 0.0_rp
       end do
    end do
    do inode = 1,pnode-1
       do idime = 1,ndime
          tinvW (idime,inode) = invW(inode,idime)
       end do
    end do
    call invmtx(tinvW,invWt,detinvWt,ndime)

    ii = 0
    do inode = 2,pnode
       ii = ii + 1
       do idime = 1,ndime
          A(idime,ii) = elcod(idime,inode) - elcod(idime,1)
       end do
    end do

    do inode = 1,pnode-1
       do idime = 1,ndime
          do jj = 1,pnode-1
             S(idime,inode) = S(idime,inode) + A(idime,jj) * invW(jj,inode)
          end do
       end do
    end do

    do inode = 1,pnode-1
       do idime = 1,ndime
          normS = normS + S(idime,inode) * S(idime,inode)  
          do jj = 1,pnode-1
             StS (idime,inode) = StS(idime,inode) + S(jj,idime) * S(jj,inode)
          end do
       end do
    end do
    normS = sqrt(normS)

    do inode = 1,pnode-1
       do idime = 1,ndime
          aux(idime,inode) = S(idime,inode)
       end do
    end do
    call invmtx(aux,aux2,detS,ndime)
    do inode = 1,pnode-1
       do idime=1,ndime
          invS(idime,inode) = aux2(idime,inode) 
       end do
    end do

    do inode = 1,pnode-1
       do idime=1,ndime
          norminvS = norminvS + invS(idime,inode) * invS(idime,inode)  
       end do
    end do

    norminvS = sqrt(norminvS)
    kappaS   = normS *  norminvS
    kappaS   = kappaS /3.0_rp     ! So that ideal element has kappaS=1
    !
    ! VOLUM of the element
    !
    if( ndime == 2 ) then
       detjm      =  (-elcod(1,1)+elcod(1,2))*(-elcod(2,1)+elcod(2,3)) &
            &       -(-elcod(2,1)+elcod(2,2))*(-elcod(1,1)+elcod(1,3))
       volum      =  0.5_rp / detjm
    else
       gpcar(1,1) =  elcod(1,2) - elcod(1,1)
       gpcar(1,2) =  elcod(1,3) - elcod(1,1)
       gpcar(1,3) =  elcod(1,4) - elcod(1,1)
       gpcar(2,1) =  elcod(2,2) - elcod(2,1)
       gpcar(2,2) =  elcod(2,3) - elcod(2,1)
       gpcar(2,3) =  elcod(2,4) - elcod(2,1)
       gpcar(3,1) =  elcod(3,2) - elcod(3,1)
       gpcar(3,2) =  elcod(3,3) - elcod(3,1)
       gpcar(3,3) =  elcod(3,4) - elcod(3,1)
       t1         =  gpcar(2,2) * gpcar(3,3) - gpcar(3,2) * gpcar(2,3)
       t2         = -gpcar(2,1) * gpcar(3,3) + gpcar(3,1) * gpcar(2,3)
       t3         =  gpcar(2,1) * gpcar(3,2) - gpcar(3,1) * gpcar(2,2)
       detjm      =  gpcar(1,1) * t1 + gpcar(1,2) * t2 + gpcar(1,3) * t3
       volum      =  1.0_rp / ( 6.0_rp * detjm )
    end if

    if( ndime == 3 ) then
       do idime = 1,ndime
          side1(idime) = elcod(idime,4) - elcod(idime,1)
          side2(idime) = elcod(idime,3) - elcod(idime,1)
          side3(idime) = elcod(idime,2) - elcod(idime,1)
          side4(idime) = elcod(idime,4) - elcod(idime,2)
          side5(idime) = elcod(idime,3) - elcod(idime,2)
          side6(idime) = elcod(idime,4) - elcod(idime,3) 
       end do
    else
       do idime = 1,ndime
          side1(idime) = elcod(idime,3)-elcod(idime,1)
          side2(idime) = elcod(idime,2)-elcod(idime,1)
          side3(idime) = elcod(idime,3)-elcod(idime,2)
          side4(idime) = 0.0_rp
          side5(idime) = 0.0_rp
          side6(idime) = 0.0_rp
       end do
    end if

    sumsi = 0.0_rp
    do idime = 1,ndime
       sumsi = sumsi + &
            side1(idime) * side1(idime) + side2(idime) * side2(idime) + &
            side3(idime) * side3(idime) + side4(idime) * side4(idime) + &
            side5(idime) * side5(idime) + side6(idime) * side6(idime)
    end do
    !
    ! Aspect ratio
    !
    asrad = sumsi / ( volum + epsil )  

  end subroutine elmgeo_quatet_kappar

  !-----------------------------------------------------------------------
  !> @addtogroup Domain
  !> @{
  !> @file    elmgeo_bounor.f90
  !> @author  Guillaume Houzeaux
  !> @date    18/10/2012
  !> @brief   Computes the boundary normals
  !> @details Comppute the boundary normals. Only valid for 
  !>          TRI03, TRI06, QUA04, QUA08, QUA09 boundaries.
  !>          For QUA04 and QUA09, thye exterior normal is averaged
  !>          over the two triangles forming the boundary element. 
  !>          According to the mesher used, we have two options:\n
  !>          - Clock wise numbering: normal points inwards
  !>          - Counterclock wise numbering: normal points outwards
  !>            - GiD: normal points inwards
  !>            - Fensap: normal points outwards
  !>            - Windmesh: normal points outwards
  !>          \verbatim
  !>          - BOUNO(1:NDIME,IBOUN) .... Exterior normal to IBOUN
  !>          - NINVE ................... Number of inverted normals
  !>                                      that were pointing inwards
  !>          \endverbatim
  !> @} 
  !-----------------------------------------------------------------------

  subroutine elmgeo_bounor(&
       ichek,kboun,ndime,mnodb,mnode,lnodb,ltypb,&
       lboel,ltype,lnods,nnode,coord,ninve,bouno)
    implicit none
    integer(ip), intent(in)            :: ichek                          !< Check or don't check
    integer(ip), intent(in)            :: kboun                          !< Number of boundaries
    integer(ip), intent(in)            :: ndime                          !< Space dimension
    integer(ip), intent(in)            :: mnodb                          !< Max number of nodes per boundary
    integer(ip), intent(in)            :: mnode                          !< Max number of nodes per element
    integer(ip), intent(in)            :: lnodb(mnodb,kboun)             !< Boundary connectivity
    integer(ip), intent(in)            :: ltypb(kboun)                   !< Boundary type
    integer(ip), intent(in), optional  :: lboel(mnodb+1,kboun)           !< Boundary/element connectvity
    integer(ip), intent(in)            :: ltype(*)                       !< Element type
    integer(ip), intent(in)            :: nnode(*)                       !< Element type number of node
    integer(ip), intent(in)            :: lnods(mnode,*)                 !< Element connectivity
    real(rp),    intent(out)           :: coord(ndime,*)                 !< Node coordiantes
    integer(ip), intent(out)           :: ninve                          !< Number of inverted boundaries
    real(rp),    intent(out)           :: bouno(ndime,kboun)             !< Boundary exterior normals
    integer(ip)                        :: iboun,p1,p2,p3,idime
    integer(ip)                        :: ipoin,inode,inodb,ielem
    integer(ip)                        :: pnodb,pblty,pnode
    real(rp)                           :: elcod(ndime,mnode)
    real(rp)                           :: bocod(ndime,mnodb)
    real(rp)                           :: vec(3,3),mod

    if( ndime == 2 ) then

       !-------------------------------------------------------------------
       !
       ! 2D case
       !
       !-------------------------------------------------------------------

       do iboun = 1,kboun
          p1             =  lnodb(1,iboun)
          p2             =  lnodb(2,iboun)
          vec(1,1)       =  coord(1,p2) - coord(1,p1)
          vec(2,1)       =  coord(2,p2) - coord(2,p1)
          bouno(1,iboun) =  vec(2,1) 
          bouno(2,iboun) = -vec(1,1) 
       end do

    else if( ndime == 3 ) then

       !-------------------------------------------------------------------
       !
       ! 3D case
       !
       !-------------------------------------------------------------------

       do iboun = 1,kboun
          pblty = abs(ltypb(iboun))

          if( pblty == TRI03 .or.  pblty == TRI06 ) then

             p1                 = lnodb(1,iboun)
             p2                 = lnodb(2,iboun)
             p3                 = lnodb(3,iboun)
             call nortri(p1,p2,p3,coord,vec,ndime)
             bouno(    1,iboun) = 0.5_rp*vec(1,3)
             bouno(    2,iboun) = 0.5_rp*vec(2,3)
             bouno(ndime,iboun) = 0.5_rp*vec(3,3)

          else if( pblty == QUA04 .or. pblty == QUA09 ) then

             p1             = lnodb(1,iboun)
             p2             = lnodb(2,iboun)
             p3             = lnodb(3,iboun)
             call nortri(p1,p2,p3,coord,vec,ndime)
             bouno(1,iboun) = vec(1,3)
             bouno(2,iboun) = vec(2,3)
             bouno(3,iboun) = vec(3,3)

             p1             = lnodb(1,iboun)
             p2             = lnodb(3,iboun)
             p3             = lnodb(4,iboun)
             call nortri(p1,p2,p3,coord,vec,ndime)
             bouno(1,iboun) = bouno(1,iboun) + vec(1,3)
             bouno(2,iboun) = bouno(2,iboun) + vec(2,3)
             bouno(3,iboun) = bouno(3,iboun) + vec(3,3)
             !
             ! HEX: We assume that ordering points towards inside
             !
             mod = sqrt(   bouno(1,iboun) * bouno(1,iboun) &
                  &      + bouno(2,iboun) * bouno(2,iboun) &
                  &      + bouno(3,iboun) * bouno(3,iboun) )
             bouno(1,iboun) = bouno(1,iboun) / mod
             bouno(2,iboun) = bouno(2,iboun) / mod
             bouno(3,iboun) = bouno(3,iboun) / mod

          end if

       end do
    end if
    !
    ! Ensure normal points outwards and normalize it
    !
    if( ichek == 1 ) then
       do iboun = 1,kboun
          pblty = abs(ltypb(iboun))
          pnodb = nnode(pblty)
          ielem = lboel(pnodb+1,iboun)
          pnode = nnode(ltype(ielem))
          do inodb = 1,pnodb
             ipoin = lnodb(inodb,iboun)
             do idime = 1,ndime
                bocod(idime,inodb) = coord(idime,ipoin)
             end do
          end do
          do inode = 1,pnode
             ipoin = lnods(inode,ielem)
             do idime = 1,ndime
                elcod(idime,inode) = coord(idime,ipoin)
             end do
          end do
          call chebou(pnode,pnodb,bouno(1,iboun),bocod,elcod,ninve)
          call vecuni(ndime,bouno(1,iboun),mod)
       end do
    end if

  end subroutine elmgeo_bounor

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   1D shape functions
  !> @details Evaluate shape functions and their first derivates 
  !>          for 1-d continuos with 2 3 & 4 nodes
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_shape1(s,nnode,shapf,deriv,heslo)
    implicit none
    real(rp),    intent(in)            :: s
    integer(ip), intent(in)            :: nnode
    real(rp),    intent(out)           :: shapf(nnode)
    real(rp),    intent(out), optional :: deriv(1,nnode)
    real(rp),    intent(out), optional :: heslo(1,nnode)

    if( present(heslo) ) then
       heslo(1,1:nnode) = 0.0_rp
    end if
    if( present(deriv) ) then
       deriv(1,1:nnode) = 0.0_rp
    end if

    if( nnode == 2 ) then     

       shapf(1) =  0.5_rp*(1.0_rp-s)
       shapf(2) =  0.5_rp*(1.0_rp+s)
       if( present(deriv) ) then
          deriv(1,1) = -0.5_rp
          deriv(1,2) =  0.5_rp
       end if

    else if( nnode == 3 ) then

       shapf(1) =  0.5_rp*s*(s-1.0_rp)
       shapf(2) =  0.5_rp*s*(s+1.0_rp)
       shapf(3) = -(s+1.0_rp)*(s-1.0_rp)
       if( present(deriv) ) then
          deriv(1,1) =  s-0.5_rp
          deriv(1,2) =  s+0.5_rp
          deriv(1,3) = -2.0_rp*s
       end if
       if( present(heslo) ) then
          heslo(1,1) =   1.0_rp
          heslo(1,2) =   1.0_rp
          heslo(1,3) =  -2.0_rp
       end if

    else if( nnode == 4 ) then

       shapf(1) = -9.0_rp/16.0_rp*(s+1.0_rp/3.0_rp)*(s-1.0_rp/3.0_rp)*(s-1.0_rp)
       shapf(2) =  9.0_rp/16.0_rp*(s+1.0_rp)*(s+1.0_rp/3.0_rp)*(s-1.0_rp/3.0_rp)
       shapf(3) = 27.0_rp/16.0_rp*(s+1.0_rp)*(s-1.0_rp/3.0_rp)*(s-1.0_rp)   
       shapf(4) = -27.0_rp/16.0_rp*(s+1.0_rp)*(s+1.0_rp/3.0_rp)*(s-1.0_rp)

       if( present(deriv) ) then
          deriv(1,1) = -9.0_rp/16.0_rp*((s-1.0_rp/3.0_rp)*(s-1.0_rp)+(s+1.0_rp/3.0_rp)&
               *(s-1.0_rp)+(s+1.0_rp/3.0_rp)*(s-1.0_rp/3.0_rp))
          deriv(1,2) =  9.0_rp/16.0_rp*((s+1.0_rp/3.0_rp)*(s-1.0_rp/3.0_rp)+(s+1.0_rp)&
               *(s-1.0_rp/3.0_rp)+(s+1.0_rp)*(s+1.0_rp/3.0_rp))
          deriv(1,3) = 27.0_rp/16.0_rp*((s-1.0_rp/3.0_rp)*(s-1.0_rp)+ (s+1.0_rp)&
               *(s-1.0_rp)+ (s+1.0_rp)*(s-1.0_rp/3.0_rp))
          deriv(1,4) = -27.0_rp/16.0_rp*((s+1.0_rp/3.0_rp)*(s-1.0_rp)+(s+1.0_rp)&
               *(s-1.0_rp)+(s+1.0_rp)*(s+1.0_rp/3.0_rp)) 
       end if
       if( present(heslo) ) then
          heslo(1,1) =  -9.0_rp/16.0_rp*(2.0_rp*(s-1.0_rp)+2.0_rp*(s+1.0_rp/3.0_rp)+2.0_rp*(s-1.0_rp/3.0_rp))
          heslo(1,2) =   9.0_rp/16.0_rp*(2.0_rp*(s-1.0_rp/3.0_rp)+2.0_rp*(s+1.0_rp/3.0_rp)+2.0_rp*(s+1.0_rp))
          heslo(1,3) =  27.0_rp/16.0_rp*(2.0_rp*(s-1.0_rp)+2.0_rp*(s-1.0_rp/3.0_rp)+2.0_rp*(s+1.0_rp))
          heslo(1,4) = -27.0_rp/16.0_rp*(2.0_rp*(s-1.0_rp)+2.0_rp*(s+1.0_rp/3.0_rp)+2.0_rp*(s+1.0_rp))
       end if
    end if

  end subroutine elmgeo_shape1

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   2D shape functions
  !> @details Evaluate shape functions, derivates and Hessian
  !>
  !>          TRIANGLES       3   6  &  10  nodes
  !>          QUADRILATERALS  4   9  &  16  nodes
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_shape2(s,t,nnode,shapf,deriv,heslo)
    implicit none
    integer(ip), intent(in)            :: nnode
    real(rp),    intent(in)            :: s,t
    real(rp),    intent(out)           :: shapf(nnode)
    real(rp),    intent(out), optional :: deriv(2,nnode)
    real(rp),    intent(out), optional :: heslo(3,nnode)
    integer(ip)                        :: ii,jj
    real(rp)                           :: st,a1,a2,a3,ss,tt,s1,s2,s3,s4
    real(rp)                           :: t1,t2,t3,t4,s9,t9,c,a

    if( present(heslo) ) then
       do ii = 1,nnode
          do jj = 1,3
             heslo(jj,ii) = 0.0_rp
          end do
       end do
    end if
    if( present(deriv) ) then
       do ii = 1,nnode
          do jj = 1,2
             deriv(jj,ii) = 0.0_rp
          end do
       end do
    end if

    if( nnode == 3 ) then     
       shapf(1)   =  1.0_rp-s-t                                
       shapf(2)   =  s                                
       shapf(3)   =  t           
       if( present(deriv) ) then                              !  3  
          deriv(1,1) = -1.0_rp                                !   
          deriv(1,2) =  1.0_rp                                !
          deriv(1,3) =  0.0_rp                                !
          deriv(2,1) = -1.0_rp                                !  1       2
          deriv(2,2) =  0.0_rp
          deriv(2,3) =  1.0_rp
       end if

    else if( nnode == 4 ) then
       st         = s*t                                           
       shapf(1)   = (1.0_rp-t-s+st)*0.25_rp                   !  4         3
       shapf(2)   = (1.0_rp-t+s-st)*0.25_rp                   !
       shapf(3)   = (1.0_rp+t+s+st)*0.25_rp                   !      
       shapf(4)   = (1.0_rp+t-s-st)*0.25_rp                   !
       if( present(deriv) ) then                              !  1         2
          deriv(1,1) = (-1.0_rp+t)*0.25_rp
          deriv(1,2) = (+1.0_rp-t)*0.25_rp
          deriv(1,3) = (+1.0_rp+t)*0.25_rp
          deriv(1,4) = (-1.0_rp-t)*0.25_rp
          deriv(2,1) = (-1.0_rp+s)*0.25_rp
          deriv(2,2) = (-1.0_rp-s)*0.25_rp
          deriv(2,3) = (+1.0_rp+s)*0.25_rp
          deriv(2,4) = (+1.0_rp-s)*0.25_rp
       end if
       if( present(heslo) ) then
          heslo(3,1) =  0.25_rp
          heslo(3,2) = -0.25_rp
          heslo(3,3) =  0.25_rp
          heslo(3,4) = -0.25_rp
       end if

    else if( nnode == 6 ) then
       a1         = 1.0_rp-s-t
       a2         = s                                             
       a3         = t
       shapf( 1)  = (2.0_rp*a1-1.0_rp)*a1                     !  3
       shapf( 2)  = (2.0_rp*a2-1.0_rp)*a2                     !   
       shapf( 3)  = (2.0_rp*a3-1.0_rp)*a3                     !   
       shapf( 4)  = 4.0_rp*a1*a2                              !  6      5
       shapf( 5)  = 4.0_rp*a2*a3                              !     
       shapf( 6)  = 4.0_rp*a1*a3                              !   
       if( present(deriv) ) then                              !  1     4     2
          deriv(1,1) = 1.0_rp-4.0_rp*a1                          
          deriv(1,2) = 4.0_rp*a2-1.0_rp    
          deriv(1,3) = 0.0_rp           
          deriv(1,4) = 4.0_rp*(a1-a2)   
          deriv(1,5) = 4.0_rp*a3        
          deriv(1,6) =-4.0_rp*a3       
          deriv(2,1) = 1.0_rp-4.0_rp*a1    
          deriv(2,2) = 0.0_rp           
          deriv(2,3) = 4.0_rp*a3-1.0_rp    
          deriv(2,4) =-4.0_rp*a2       
          deriv(2,5) = 4.0_rp*a2        
          deriv(2,6) = 4.0_rp*(a1-a3)
       end if
       if( present(heslo) ) then
          heslo(1,1) = 4.0_rp
          heslo(1,2) = 4.0_rp
          heslo(1,4) =-8.0_rp
          heslo(2,1) = 4.0_rp
          heslo(2,3) = 4.0_rp
          heslo(2,6) =-8.0_rp
          heslo(3,1) = 4.0_rp
          heslo(3,4) =-4.0_rp
          heslo(3,5) = 4.0_rp
          heslo(3,6) =-4.0_rp
       end if

    else if( nnode == 9 ) then
       ss         = s*s
       st         = s*t
       tt         = t*t
       s1         = s+1.0_rp
       t1         = t+1.0_rp
       s2         = s*2.0_rp
       t2         = t*2.0_rp
       s9         = s-1.0_rp                               
       t9         = t-1.0_rp                                   !  4      7      3
       shapf( 1)  = 0.25_rp*s9*st*t9                           !
       shapf( 2)  = 0.25_rp*s1*st*t9                           !        
       shapf( 3)  = 0.25_rp*s1*st*t1                           !      
       shapf( 4)  = 0.25_rp*s9*st*t1                           !  8      9      6
       shapf( 5)  = 0.5_rp*(1.0_rp-ss)*t*t9                    !    
       shapf( 6)  = 0.5_rp*s*s1*(1.0_rp-tt)                    !   
       shapf( 7)  = 0.5_rp*(1.0_rp-ss)*t*t1                    !  
       shapf( 8)  = 0.5_rp*s*s9*(1.0_rp-tt)                    !  1      5      2
       shapf( 9)  = (1.0_rp-ss)*(1.0_rp-tt)
       if( present(deriv) ) then
          deriv(1,1) = 0.25_rp*t*t9*(-1.0_rp+s2)
          deriv(1,2) = 0.25_rp*(1.0_rp+s2)*t*t9
          deriv(1,3) = 0.25_rp*(1.0_rp+s2)*t*t1
          deriv(1,4) = 0.25_rp*(-1.0_rp+s2)*t*t1
          deriv(1,5) = -st*t9
          deriv(1,6) =  0.5_rp*(1.0_rp+s2)*(1.0_rp-tt)
          deriv(1,7) = -st*t1
          deriv(1,8) =  0.5_rp*(-1.0_rp+s2)*(1.0_rp-tt)
          deriv(1,9) = -s2*(1.0_rp-tt)
          deriv(2,1) =  0.25_rp*(-1.0_rp+t2)*s*s9
          deriv(2,2) =  0.25_rp*s*s1*(-1.0_rp+t2)
          deriv(2,3) =  0.25_rp*s*s1*(1.0_rp+t2)
          deriv(2,4) =  0.25_rp*s*s9*(1.0_rp+t2)
          deriv(2,5) =  0.5_rp*(1.0_rp-ss)*(-1.0_rp+t2)
          deriv(2,6) = -st*s1
          deriv(2,7) =  0.5_rp*(1.0_rp-ss)*(1.0_rp+t2)
          deriv(2,8) = -st*s9
          deriv(2,9) = -t2*(1.0_rp-ss)
       end if
       if( present(heslo) ) then
          heslo(1,1) = 0.5_rp*t*t9
          heslo(1,2) = 0.5_rp*t*t9
          heslo(1,3) = 0.5_rp*t*t1
          heslo(1,4) = 0.5_rp*t*t1
          heslo(1,5) =-t*t9
          heslo(1,6) = 1.0_rp-tt
          heslo(1,7) =-t*t1
          heslo(1,8) = 1.0_rp-tt
          heslo(1,9) =-2.0_rp*(1.0_rp-tt)
          heslo(2,1) = 0.5_rp*s*s9
          heslo(2,2) = 0.5_rp*s*s1
          heslo(2,3) = 0.5_rp*s*s1
          heslo(2,4) = 0.5_rp*s*s9
          heslo(2,5) = 1.0_rp-ss
          heslo(2,6) =-s*s1
          heslo(2,7) = 1.0_rp-ss
          heslo(2,8) =-s*s9
          heslo(2,9) =-2.0_rp*(1.0_rp-ss)
          heslo(3,1) = 0.25_rp*(-1.0_rp+t2)*(s9+s)
          heslo(3,2) = 0.25_rp*(-1.0_rp+t2)*(s1+s)
          heslo(3,3) = 0.25_rp*(1.0_rp+t2)*(s1+s)
          heslo(3,4) = 0.25_rp*(1.0_rp+t2)*(s9+s)
          heslo(3,5) =-s*(-1.0_rp+t2)
          heslo(3,6) =-t*s1-st
          heslo(3,7) =-s*(1.0_rp+t2)
          heslo(3,8) =-t*s9-st
          heslo(3,9) = s2*t2
       end if

    else if( nnode == 10 ) then
       c           = 9.0_rp/2.0_rp
       a1          = 1.0_rp-s-t
       a2          = 2.0_rp/3.0_rp-s-t
       a3          = 1.0_rp/3.0_rp-s-t
       shapf( 1)   = c*a1*a2*a3                                 !  3            
       shapf( 2)   = c*(1.0_rp/3.0_rp-s)*(2.0_rp/3.0_rp-s)*s    !               
       shapf( 3)   = c*(1.0_rp/3.0_rp-t)*(2.0_rp/3.0_rp-t)*t    !               
       shapf( 4)   = 3.0_rp*c*a1*a2*s                           !  8    7  
       shapf( 5)   =-3.0_rp*c*a1*(1.0_rp/3.0_rp-s)              !               
       shapf( 6)   =-3.0_rp*c*(1.0_rp/3.0_rp-s)*s*t             !               
       shapf( 7)   =-3.0_rp*c*s*(1.0_rp/3.0_rp-t)*t             !  9   10    6
       shapf( 8)   =-3.0_rp*c*a1*(1.0_rp/3.0_rp-t)*t            !
       shapf( 9)   = 3.0_rp*c*a1*a2*t                           !
       shapf(10)   = 6.0_rp*c*a1*s*t                            !  1    4    5    2
       if( present(deriv) ) then
          deriv(1, 1) =-c*(a1*a2+a1*a3+a2*a3)       
          deriv(1, 2) =-c*((2.0_rp/3.0_rp-s)*s &
               &        + (1.0_rp/3.0_rp-s)*s-(1.0_rp/3.0_rp-s)*(2.0_rp/3.0_rp-s))
          deriv(1, 3) = 0.0_rp
          deriv(1, 4) = 3.0_rp*c*(a1*a2-a1*s-a2*s)
          deriv(1, 5) =-3.0_rp*c*(a1*(1.0_rp/3.0_rp-s)&
               &        -a1*s-(1.0_rp/3.0_rp-s)*s)
          deriv(1, 6) =-3.0_rp*c*((1.0_rp/3.0_rp-s)*t-s*t)
          deriv(1, 7) =-3.0_rp*c*((1.0_rp/3.0_rp-t)*t)
          deriv(1, 8) = 3.0_rp*c*((1.0_rp/3.0_rp-t)*t)
          deriv(1, 9) = 3.0_rp*c*(-a1*t-a2*t)
          deriv(1,10) = 6.0_rp*c*(a1*t-s*t)
          deriv(2, 1) =-c*(a1*a2+a1*a3+a2*a3)
          deriv(2, 2) = 0.0_rp
          deriv(2, 3) =-c*((2.0_rp/3.0_rp-t)*t&
               &        + (1.0_rp/3.0_rp-t)*t-(1.0_rp/3.0_rp-t)*(2.0_rp/3.0_rp-t))
          deriv(2, 4) = 3.0_rp*c*(-a1*s-a2*s)
          deriv(2, 5) =-3.0_rp*c*(-(1.0_rp/3.0_rp-s)*s)
          deriv(2, 6) =-3.0_rp*c*((1.0_rp/3.0_rp-s)*s)
          deriv(2, 7) =-3.0_rp*c*((1.0_rp/3.0_rp-t)*s-s*t)
          deriv(2, 8) =-3.0_rp*c*(-(1.0_rp/3.0_rp-t)*t&
               &        -a1*t+a1*(1.0_rp/3.0_rp-t))
          deriv(2, 9) = 3.0_rp*c*(-a1*t-a2*t+a1*a2)
          deriv(2,10) = 6.0_rp*c*(a1*s-s*t)
       end if
       if( present(heslo) ) then
          heslo(1, 1) = 2.0_rp*c*(a1+a2+a3) 
          heslo(1, 2) =-c*(2.0_rp-6.0_rp*s) 
          heslo(1, 3) = 0.0_rp 
          heslo(1, 4) = 3.0_rp*c*(-2.0_rp*a1-2.0_rp*a2+2.0_rp*s) 
          heslo(1, 5) =-3.0_rp*c*(-2.0_rp*a1-2.0_rp*(1.0_rp/3.0_rp-s)+2.0_rp*s) 
          heslo(1, 6) = 3.0_rp*c*2.0_rp*t 
          heslo(1, 7) = 0.0_rp 
          heslo(1, 8) = 0.0_rp  
          heslo(1, 9) = 3.0_rp*c*2.0_rp*t 
          heslo(1,10) =-6.0_rp*c*2.0_rp*t 
          heslo(2, 1) = c*(2.0_rp*a1+2.0_rp*a2+2.0_rp*a3) 
          heslo(2, 2) = 0.0_rp 
          heslo(2, 3) =-c*(2.0_rp-6.0_rp*t)
          heslo(2, 4) = 3.0_rp*c*2.0_rp*s
          heslo(2, 5) = 0.0_rp
          heslo(2, 6) = 0.0_rp
          heslo(2, 7) = 3.0_rp*c*2.0_rp*s
          heslo(2, 8) =-3.0_rp*c*(-2.0_rp*a1-2.0_rp*(1.0_rp/3.0_rp-t)+2.0_rp*t)
          heslo(2, 9) = 3.0_rp*c*(-2.0_rp*a1-2.0_rp*a2+2.0_rp*t)
          heslo(2,10) =-6.0_rp*c*2.0_rp*s
          heslo(3, 1) = 2.0_rp*c*(a1+a2+a3) 
          heslo(3, 2) = 0.0_rp  
          heslo(3, 3) = 0.0_rp 
          heslo(3, 4) = 3.0_rp*c*(-a1-a2+2.0_rp*s) 
          heslo(3, 5) =-3.0_rp*c*(-(1.0_rp/3.0_rp-s)+s) 
          heslo(3, 6) =-3.0_rp*c*(1.0_rp/3.0_rp-2.0_rp*s)
          heslo(3, 7) =-3.0_rp*c*(1.0_rp/3.0_rp-2.0_rp*t)
          heslo(3, 8) = 3.0_rp*c*(1.0_rp/3.0_rp-2.0_rp*t) 
          heslo(3, 9) = 3.0_rp*c*(-a1-a2+2.0_rp*t) 
          heslo(3,10) = 6.0_rp*c*(a1-s-t)
       end if

    else if( nnode == 16 ) then
       a =81.0_rp/256.0_rp
       c =1.0_rp/3.0_rp
       s1=1.0_rp+s
       s2=c+s
       s3=c-s
       s4=1.0_rp-s
       t1=1.0_rp+t
       t2=c+t
       t3=c-t
       t4=1.0_rp-t
       shapf( 1) =   a*s2*s3*s4*t2*t3*t4                   ! 4    10    9    3
       shapf( 2) =   a*s1*s2*s3*t2*t3*t4                   ! 
       shapf( 3) =   a*s1*s2*s3*t1*t2*t3                   ! 
       shapf( 4) =   a*s2*s3*s4*t1*t2*t3                   ! 11   16   15    8
       shapf( 5) =-3.0_rp*a*s1*s3*s4*t2*t3*t4              !
       shapf( 6) =-3.0_rp*a*s1*s2*s4*t2*t3*t4              !
       shapf( 7) =-3.0_rp*a*s1*s2*s3*t1*t3*t4              ! 12   13   14    7
       shapf( 8) =-3.0_rp*a*s1*s2*s3*t1*t2*t4              !
       shapf( 9) =-3.0_rp*a*s1*s2*s4*t1*t2*t3              !
       shapf(10) =-3.0_rp*a*s1*s3*s4*t1*t2*t3              ! 1     5    6    2
       shapf(11) =-3.0_rp*a*s2*s3*s4*t1*t2*t4                 
       shapf(12) =-3.0_rp*a*s2*s3*s4*t1*t3*t4
       shapf(13) = 9.0_rp*a*s1*s3*s4*t1*t3*t4
       shapf(14) = 9.0_rp*a*s1*s2*s4*t1*t3*t4
       shapf(15) = 9.0_rp*a*s1*s2*s4*t1*t2*t4
       shapf(16) = 9.0_rp*a*s1*s3*s4*t1*t2*t4
       if( present(deriv) ) then
          deriv(1, 1)=  a *t2*t3*t4*(-s2*s3-s2*s4+s3*s4)
          deriv(1, 2)=  a *t2*t3*t4*(-s1*s2+s1*s3+s2*s3)
          deriv(1, 3)=  a *t1*t2*t3*(-s1*s2+s1*s3+s2*s3)
          deriv(1, 4)=  a *t1*t2*t3*(-s2*s3-s2*s4+s3*s4)
          deriv(1, 5)=-3.0_rp*a*t2*t3*t4*(-s1*s3-s1*s4+s3*s4)
          deriv(1, 6)=-3.0_rp*a*t2*t3*t4*(-s1*s2+s1*s4+s2*s4)
          deriv(1, 7)=-3.0_rp*a*t1*t3*t4*(-s1*s2+s1*s3+s2*s3)
          deriv(1, 8)=-3.0_rp*a*t1*t2*t4*(-s1*s2+s1*s3+s2*s3)
          deriv(1, 9)=-3.0_rp*a*t1*t2*t3*(-s1*s2+s1*s4+s2*s4)
          deriv(1,10)=-3.0_rp*a*t1*t2*t3*(-s1*s3-s1*s4+s3*s4)
          deriv(1,11)=-3.0_rp*a*t1*t2*t4*(-s2*s3-s2*s4+s3*s4)
          deriv(1,12)=-3.0_rp*a*t1*t3*t4*(-s2*s3-s2*s4+s3*s4)
          deriv(1,13)= 9.0_rp*a*t1*t3*t4*(-s1*s3-s1*s4+s3*s4)
          deriv(1,14)= 9.0_rp*a*t1*t3*t4*(-s1*s2+s1*s4+s2*s4)
          deriv(1,15)= 9.0_rp*a*t1*t2*t4*(-s1*s2+s1*s4+s2*s4)
          deriv(1,16)= 9.0_rp*a*t1*t2*t4*(-s1*s3-s1*s4+s3*s4)
          deriv(2, 1)=  a   *s2*s3*s4*(-t2*t3-t2*t4+t3*t4)
          deriv(2, 2)=  a   *s1*s2*s3*(-t2*t3-t2*t4+t3*t4)
          deriv(2, 3)=  a   *s1*s2*s3*(-t1*t2+t1*t3+t2*t3)
          deriv(2, 4)=  a   *s2*s3*s4*(-t1*t2+t1*t3+t2*t3)
          deriv(2, 5)= -3.0_rp*a *s1*s3*s4*(-t2*t3-t2*t4+t3*t4)
          deriv(2, 6)= -3.0_rp*a *s1*s2*s4*(-t2*t3-t2*t4+t3*t4)
          deriv(2, 7)= -3.0_rp*a *s1*s2*s3*(-t1*t3-t1*t4+t3*t4)
          deriv(2, 8)= -3.0_rp*a *s1*s2*s3*(-t1*t2+t1*t4+t2*t4)
          deriv(2, 9)= -3.0_rp*a *s1*s2*s4*(-t1*t2+t1*t3+t2*t3)
          deriv(2,10)= -3.0_rp*a *s1*s3*s4*(-t1*t2+t1*t3+t2*t3)
          deriv(2,11)= -3.0_rp*a *s2*s3*s4*(-t1*t2+t1*t4+t2*t4)
          deriv(2,12)= -3.0_rp*a *s2*s3*s4*(-t1*t3-t1*t4+t3*t4)
          deriv(2,13)=  9.0_rp*a *s1*s3*s4*(-t1*t3-t1*t4+t3*t4)
          deriv(2,14)=  9.0_rp*a *s1*s2*s4*(-t1*t3-t1*t4+t3*t4)
          deriv(2,15)=  9.0_rp*a *s1*s2*s4*(-t1*t2+t1*t4+t2*t4)
          deriv(2,16)=  9.0_rp*a *s1*s3*s4*(-t1*t2+t1*t4+t2*t4)
       end if
       if( present(heslo) ) then
          heslo(1, 1) =&
               a *t2*t3*t4*(2.0_rp*s2-2.0_rp*s3-2.0_rp*s4)
          heslo(1, 2) =&
               a *t2*t3*t4*(-2.0_rp*s1-2.0_rp*s2+2.0_rp*s3)
          heslo(1, 3) =&
               a *t1*t2*t3*(-2.0_rp*s1-2.0_rp*s2+2.0_rp*s3)
          heslo(1, 4) =&
               a *t1*t2*t3*(2.0_rp*s2-2.0_rp*s3-2.0_rp*s4)
          heslo(1, 5) =&
               -3.0_rp*a *t2*t3*t4*(2.0_rp*s1-2.0_rp*s3-2.0_rp*s4)
          heslo(1, 6) =&
               -3.0_rp*a *t2*t3*t4*(-2.0_rp*s1-2.0_rp*s2+2.0_rp*s4)
          heslo(1, 7) =&
               -3.0_rp*a *t1*t3*t4*(-2.0_rp*s1-2.0_rp*s2+2.0_rp*s3)
          heslo(1, 8) =&
               -3.0_rp*a *t1*t2*t4*(-2.0_rp*s1-2.0_rp*s2+2.0_rp*s3)
          heslo(1, 9) =&
               -3.0_rp*a *t1*t2*t3*(-2.0_rp*s1-2.0_rp*s2+2.0_rp*s4)
          heslo(1,10) =&
               -3.0_rp*a *t1*t2*t3*(2.0_rp*s1-2.0_rp*s3-2.0_rp*s4)
          heslo(1,11) =&
               -3.0_rp*a *t1*t2*t4*(2.0_rp*s2-2.0_rp*s3-2.0_rp*s4)
          heslo(1,12) =&
               -3.0_rp*a *t1*t3*t4*(2.0_rp*s2-2.0_rp*s3-2.0_rp*s4)
          heslo(1,13) =&
               9.0_rp*a *t1*t3*t4*(2.0_rp*s1-2.0_rp*s3-2.0_rp*s4)
          heslo(1,14) =&
               9.0_rp*a *t1*t3*t4*(-2.0_rp*s1-2.0_rp*s2+2.0_rp*s4)
          heslo(1,15) =&
               9.0_rp*a *t1*t2*t4*(-2.0_rp*s1-2.0_rp*s2+2.0_rp*s4)
          heslo(1,16) =&
               9.0_rp*a *t1*t2*t4*(2.0_rp*s1-2.0_rp*s3-2.0_rp*s4)
          heslo(2, 1) =&
               a *s2*s3*s4*(2.0_rp*t2-2.0_rp*t3-2.0_rp*t4)
          heslo(2, 2) =&
               a *s1*s2*s3*(2.0_rp*t2-2.0_rp*t3-2.0_rp*t4)
          heslo(2, 3) =&
               a *s1*s2*s3*(-2.0_rp*t1-2.0_rp*t2+2.0_rp*t3)
          heslo(2, 4) =&
               a *s2*s3*s4*(-2.0_rp*t1-2.0_rp*t2+2.0_rp*t3)
          heslo(2, 5) =&
               -3.0_rp*a *s1*s3*s4*(2.0_rp*t2-2.0_rp*t3-2.0_rp*t4)
          heslo(2, 6) =&
               -3.0_rp*a *s1*s2*s4*(2.0_rp*t2-2.0_rp*t3-2.0_rp*t4)
          heslo(2, 7) =&
               -3.0_rp*a *s1*s2*s3*(2.0_rp*t1-2.0_rp*t3-2.0_rp*t4)
          heslo(2, 8) =&
               -3.0_rp*a *s1*s2*s3*(-2.0_rp*t1-2.0_rp*t2+2.0_rp*t4)
          heslo(2, 9) =&
               -3.0_rp*a *s1*s2*s4*(-2.0_rp*t1-2.0_rp*t2+2.0_rp*t3)
          heslo(2,10) =&
               -3.0_rp*a *s1*s3*s4*(-2.0_rp*t1-2.0_rp*t2+2.0_rp*t3)
          heslo(2,11) =&
               -3.0_rp*a *s2*s3*s4*(-2.0_rp*t1-2.0_rp*t2+2.0_rp*t4)
          heslo(2,12) =&
               -3.0_rp*a *s2*s3*s4*(2.0_rp*t1-2.0_rp*t3-2.0_rp*t4)
          heslo(2,13) =&
               9.0_rp*a *s1*s3*s4*(2.0_rp*t1-2.0_rp*t3-2.0_rp*t4)
          heslo(2,14) =&
               9.0_rp*a *s1*s2*s4*(2.0_rp*t1-2.0_rp*t3-2.0_rp*t4)
          heslo(2,15) =&
               9.0_rp*a *s1*s2*s4*(-2.0_rp*t1-2.0_rp*t2+2.0_rp*t4)
          heslo(2,16) =&
               9.0_rp*a *s1*s3*s4*(-2.0_rp*t1-2.0_rp*t2+2.0_rp*t4)
          heslo(3, 1) =&
               a*(-s2*s3-s2*s4+s3*s4)*(-t2*t3-t2*t4+t3*t4)
          heslo(3, 2) =&
               a*(-s1*s2+s1*s3+s2*s3)*(-t2*t3-t2*t4+t3*t4)       
          heslo(3, 3) =&
               a*(-s1*s2+s1*s3+s2*s3)*(-t1*t2+t1*t3+t2*t3)
          heslo(3, 4) =&
               a*(-s2*s3-s2*s4+s3*s4)*(-t1*t2+t1*t3+t2*t3)
          heslo(3, 5) =&
               -3.0_rp*a*(-s1*s3-s1*s4+s3*s4)*(-t2*t3-t2*t4+t3*t4)
          heslo(3, 6) =&
               -3.0_rp*a*(-s1*s2+s1*s4+s2*s4)*(-t2*t3-t2*t4+t3*t4)
          heslo(3, 7) =&
               -3.0_rp*a*(-s1*s2+s1*s3+s2*s3)*(-t1*t3-t1*t4+t3*t4)
          heslo(3, 8) =&
               -3.0_rp*a*(-s1*s2+s1*s3+s2*s3)*(-t1*t2+t1*t4+t2*t4)
          heslo(3, 9) =&
               -3.0_rp*a*(-s1*s2+s1*s4+s2*s4)*(-t1*t2+t1*t3+t2*t3)
          heslo(3,10) =&
               -3.0_rp*a*(-s1*s3-s1*s4+s3*s4)*(-t1*t2+t1*t3+t2*t3)
          heslo(3,11) =&
               -3.0_rp*a*(-s2*s3-s2*s4+s3*s4)*(-t1*t2+t1*t4+t2*t4)
          heslo(3,12) =&
               -3.0_rp*a*(-s2*s3-s2*s4+s3*s4)*(-t1*t3-t1*t4+t3*t4)
          heslo(3,13) =&
               9.0_rp*a*(-s1*s3-s1*s4+s3*s4)*(-t1*t3-t1*t4+t3*t4)
          heslo(3,14) =&
               9.0_rp*a*(-s1*s2+s1*s4+s2*s4)*(-t1*t3-t1*t4+t3*t4)
          heslo(3,15) =&
               9.0_rp*a*(-s1*s2+s1*s4+s2*s4)*(-t1*t2+t1*t4+t2*t4)
          heslo(3,16) =&
               9.0_rp*a*(-s1*s3-s1*s4+s3*s4)*(-t1*t2+t1*t4+t2*t4)
       end if
    else
       call runend('ELEMENT DOES NOT EXIST')
    end if

  end subroutine elmgeo_shape2

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   1D shape functions
  !> @details Evaluate shape functions, derivative and Hessian 
  !>          for 1-d continuos with 2 3 & 4 nodes
  !> 
  !>          TETRAHEDRA:  4  10  &  20  nodes
  !>          HEXAHEDRA:   8  27  &  64  nodes
  !>          PRISM:       6             nodes
  !
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_shape3(s,t,z,nnode,shapf,deriv,heslo)
    implicit none
    integer(ip), intent(in)            :: nnode
    real(rp),    intent(in)            :: s,t,z
    real(rp),    intent(out)           :: shapf(nnode)
    real(rp),    intent(out), optional :: deriv(3,nnode)
    real(rp),    intent(out), optional :: heslo(6,nnode)
    integer(ip)                        :: i,ii,jj
    real(rp)                           :: a1,a2,a3,a4,a,p1,p2,p3,z1,z2,z3,z4,s1,s2,s3,s4
    real(rp)                           :: t1,t2,t3,t4,sm,tm,zm,sq,tp,zp,s11,s21,s31,s41
    real(rp)                           :: t11,t21,t31,t41,z11,z21,z31,s12,s22,s32,s42
    real(rp)                           :: t12,t22,t32,t42,z41,z12,z22,z32,z42,sl,tl,zl
    real(rp)                           :: one8

    if( present(heslo) ) then
       do ii = 1,nnode
          do jj = 1,6
             heslo(jj,ii) = 0.0_rp
          end do
       end do
    end if
    if( present(deriv) ) then
       do ii = 1,nnode
          do jj = 1,3
             deriv(jj,ii) = 0.0_rp
          end do
       end do
    end if

    if( nnode == 4 ) then
       !
       ! Linear tetrahedron: s=[0:1], t=[0:1], z=[0:1]
       !
       shapf(   1) = 1.0_rp-s-t-z
       shapf(   2) = s
       shapf(   3) = t
       shapf(   4) = z
       deriv(1, 1) =-1.0_rp
       deriv(2, 1) =-1.0_rp
       deriv(3, 1) =-1.0_rp
       deriv(1, 2) = 1.0_rp
       deriv(2, 3) = 1.0_rp
       deriv(3, 4) = 1.0_rp

    else if( nnode == 5 ) then
       !
       ! Linear Pyramid: s=[-1:1], t=[-1:1], z=[-1:1]
       !
       one8        =  0.125_rp
       shapf(   1) =  one8*(1.0_rp-s)*(1.0_rp-t)*(1.0_rp-z)
       shapf(   2) =  one8*(1.0_rp+s)*(1.0_rp-t)*(1.0_rp-z)
       shapf(   3) =  one8*(1.0_rp+s)*(1.0_rp+t)*(1.0_rp-z) 
       shapf(   4) =  one8*(1.0_rp-s)*(1.0_rp+t)*(1.0_rp-z)
       shapf(   5) =  0.500_rp*(1.0_rp+z)  
       if( present(deriv) ) then
          deriv(1, 1) = -one8*(1.0_rp-t)*(1.0_rp-z)
          deriv(2, 1) = -one8*(1.0_rp-s)*(1.0_rp-z)
          deriv(3, 1) = -one8*(1.0_rp-s)*(1.0_rp-t)
          deriv(1, 2) =  one8*(1.0_rp-t)*(1.0_rp-z)
          deriv(2, 2) = -one8*(1.0_rp+s)*(1.0_rp-z)
          deriv(3, 2) = -one8*(1.0_rp+s)*(1.0_rp-t)
          deriv(1, 3) =  one8*(1.0_rp+t)*(1.0_rp-z) 
          deriv(2, 3) =  one8*(1.0_rp+s)*(1.0_rp-z) 
          deriv(3, 3) = -one8*(1.0_rp+s)*(1.0_rp+t)
          deriv(1, 4) = -one8*(1.0_rp+t)*(1.0_rp-z)
          deriv(2, 4) =  one8*(1.0_rp-s)*(1.0_rp-z)
          deriv(3, 4) = -one8*(1.0_rp-s)*(1.0_rp+t)
          deriv(1, 5) =  0.0_rp
          deriv(2, 5) =  0.0_rp
          deriv(3, 5) =  0.5_rp 
       end if
       if( present(heslo) ) then
          heslo(4, 1) =  one8*(1.0_rp-z)
          heslo(5, 1) =  one8*(1.0_rp-t)
          heslo(6, 1) =  one8*(1.0_rp-s)
          heslo(4, 2) = -one8*(1.0_rp-z)
          heslo(5, 2) = -one8*(1.0_rp-t)
          heslo(6, 2) =  one8*(1.0_rp+s)
          heslo(4, 3) =  one8*(1.0_rp-z)
          heslo(5, 3) = -one8*(1.0_rp+t)
          heslo(6, 3) = -one8*(1.0_rp+s)
          heslo(4, 4) = -one8*(1.0_rp-z)
          heslo(5, 4) =  one8*(1.0_rp+t)
          heslo(6, 4) = -one8*(1.0_rp-s)   
       end if

    else if( nnode == 6 ) then
       !
       ! Linear Prism: s=[0:1], t=[0:1], z=[0:1]
       !
       shapf(   1) = (1.0_rp-s-t)*(1.0_rp-z)
       shapf(   2) = s*(1.0_rp-z)
       shapf(   3) = t*(1.0_rp-z)
       shapf(   4) = (1.0_rp-s-t)*z
       shapf(   5) = s*z
       shapf(   6) = t*z
       if( present(deriv) ) then
          deriv(1, 1) = z-1.0_rp
          deriv(2, 1) = z-1.0_rp
          deriv(3, 1) = s+t-1.0_rp
          deriv(1, 2) = 1.0_rp-z
          deriv(3, 2) = -s
          deriv(2, 3) = 1.0_rp-z
          deriv(3, 3) = -t
          deriv(1, 4) = -z
          deriv(2, 4) = -z
          deriv(3, 4) = 1.0_rp-s-t
          deriv(1, 5) = z
          deriv(3, 5) = s
          deriv(2, 6) = z
          deriv(3, 6) = t 
       end if

    else if( nnode == 10 ) then
       !
       ! Quadratic tetrahedron 
       !
       a1= 1.0_rp-s-t-z
       a2=s
       a3=t
       a4=z
       shapf(   1) = (2.0_rp*a1-1.0_rp)*a1
       shapf(   2) = (2.0_rp*a2-1.0_rp)*a2
       shapf(   3) = (2.0_rp*a3-1.0_rp)*a3
       shapf(   4) = (2.0_rp*a4-1.0_rp)*a4
       shapf(   5) = 4.0_rp*a1*a2
       shapf(   6) = 4.0_rp*a2*a3
       shapf(   7) = 4.0_rp*a1*a3
       shapf(   8) = 4.0_rp*a1*a4
       shapf(   9) = 4.0_rp*a2*a4
       shapf(  10) = 4.0_rp*a3*a4
       if( present(deriv) ) then
          deriv(1, 1) = 1.0_rp-4.0_rp*a1
          deriv(2, 1) = 1.0_rp-4.0_rp*a1
          deriv(3, 1) = 1.0_rp-4.0_rp*a1
          deriv(1, 2) = 4.0_rp*a2-1.0_rp
          deriv(2, 3) = 4.0_rp*a3-1.0_rp
          deriv(3, 4) = 4.0_rp*a4-1.0_rp
          deriv(1, 5) = 4.0_rp*(a1-a2)
          deriv(2, 5) =-4.0_rp*a2
          deriv(3, 5) =-4.0_rp*a2
          deriv(1, 6) = 4.0_rp*a3
          deriv(2, 6) = 4.0_rp*a2
          deriv(1, 7) =-4.0_rp*a3
          deriv(2, 7) = 4.0_rp*(a1-a3)
          deriv(3, 7) =-4.0_rp*a3
          deriv(1, 8) =-4.0_rp*a4
          deriv(2, 8) =-4.0_rp*a4
          deriv(3, 8) = 4.0_rp*(a1-a4)
          deriv(1, 9) = 4.0_rp*a4
          deriv(3, 9) = 4.0_rp*a2
          deriv(2,10) = 4.0_rp*a4
          deriv(3,10) = 4.0_rp*a3
       end if
       if( present(heslo) ) then
          heslo(1:6,1) = 4.0_rp
          heslo(2, 3)  = 4.0_rp
          heslo(3, 4)  = 4.0_rp
          heslo(1, 2)  = 4.0_rp
          heslo(1, 5)  =-8.0_rp
          heslo(4, 5)  =-4.0_rp
          heslo(5, 5)  =-4.0_rp
          heslo(4, 6)  = 4.0_rp
          heslo(2, 7)  =-8.0_rp
          heslo(4, 7)  =-4.0_rp
          heslo(6, 7)  =-4.0_rp
          heslo(3, 8)  =-8.0_rp
          heslo(5, 8)  =-4.0_rp
          heslo(6, 8)  =-4.0_rp
          heslo(5, 9)  = 4.0_rp
          heslo(6,10)  = 4.0_rp
       end if

    else if( nnode == 20 ) then
       !
       ! Cubic tetrahedron
       !        
       a  = 4.5_rp
       p1 = 1-s-t-z
       p2 = 2.0_rp/3.0_rp-s-t-z
       p3 = 1.0_rp/3.0_rp-s-t-z
       z1 = 2.0_rp/3.0_rp-z
       z2 = 1.0_rp/3.0_rp-z
       s1 = 2.0_rp/3.0_rp-s
       s2 = 1.0_rp/3.0_rp-s
       t1 = 2.0_rp/3.0_rp-t
       t2 = 1.0_rp/3.0_rp-t

       shapf(   1) = a*p1*p2*p3
       shapf(   2) = a*z*z1*z2
       shapf(   3) = a*s*s1*s2
       shapf(   4) = a*t*t1*t2
       shapf(   5) = 3.0_rp*a*p1*p2*t
       shapf(   6) = 3.0_rp*a*p1*p2*z
       shapf(   7) = 3.0_rp*a*p1*p2*s
       shapf(   8) =-3.0_rp*a*p1*t2*t
       shapf(   9) =-3.0_rp*a*p1*s2*s
       shapf(  10) =-3.0_rp*a*p1*z2*z
       shapf(  11) =-3.0_rp*a*t2*t*z
       shapf(  12) =-3.0_rp*a*z2*t*z
       shapf(  13) =-3.0_rp*a*z2*s*z
       shapf(  14) =-3.0_rp*a*s2*s*z
       shapf(  15) =-3.0_rp*a*s2*s*t
       shapf(  16) =-3.0_rp*a*t2*t*s
       shapf(  17) = 27.0_rp*p1*t*z
       shapf(  18) = 27.0_rp*p1*s*z
       shapf(  19) = 27.0_rp*p1*s*t
       shapf(  20) = 27.0_rp*s*t*z

       if( present(deriv) ) then
          deriv(1, 1) =-a*(p1*p2+p1*p3+p2*p3)
          deriv(2, 1) =-a*(p1*p2+p1*p3+p2*p3)
          deriv(3, 1) =-a*(p1*p2+p1*p3+p2*p3)
          deriv(1, 2) = 0.0_rp
          deriv(2, 2) = 0.0_rp
          deriv(3, 2) =-a*(z*z1+z*z2-z1*z2)
          deriv(1, 3) =-a*(s*s1+s*s2-s1*s2)
          deriv(2, 3) = 0.0_rp
          deriv(3, 3) = 0.0_rp
          deriv(1, 4) = 0.0_rp
          deriv(2, 4) =-a*(t*t1+t*t2-t1*t2)
          deriv(3, 4) = 0.0_rp
          deriv(1, 5) = 3.0_rp*a*(-p2*t-p1*t)
          deriv(2, 5) = 3.0_rp*a*(-p2*t-p1*t+p1*p2)
          deriv(3, 5) = 3.0_rp*a*(-p2*t-p1*t)      
          deriv(1, 6) = 3.0_rp*a*(-p2*z-p1*z)      
          deriv(2, 6) = 3.0_rp*a*(-p2*z-p1*z)
          deriv(3, 6) = 3.0_rp*a*(-p2*z-p1*z+p1*p2)      
          deriv(1, 7) = 3.0_rp*a*(-p2*s-p1*s+p1*p2)
          deriv(2, 7) = 3.0_rp*a*(-p2*s-p1*s)
          deriv(3, 7) = 3.0_rp*a*(-p2*s-p1*s)
          deriv(1, 8) = 3.0_rp*a*(t2*t)
          deriv(2, 8) = 3.0_rp*a*(t2*t+p1*t-p1*t2)
          deriv(3, 8) = 3.0_rp*a*(t2*t)
          deriv(1, 9) = 3.0_rp*a*(s2*s+p1*s-p1*s2)
          deriv(2, 9) = 3.0_rp*a*(s2*s)
          deriv(3, 9) = 3.0_rp*a*(s2*s)
          deriv(1,10) = 3.0_rp*a*(z2*z)
          deriv(2,10) = 3.0_rp*a*(z2*z)
          deriv(3,10) = 3.0_rp*a*(z2*z+p1*z-p1*z2)
          deriv(1,11) = 0.0_rp
          deriv(2,11) =-3.0_rp*a*(t2*z-t*z)
          deriv(3,11) =-3.0_rp*a*t2*t
          deriv(1,12) = 0.0_rp
          deriv(2,12) =-3.0_rp*a*z2*z
          deriv(3,12) =-3.0_rp*a*(t*z2-t*z)
          deriv(1,13) =-3.0_rp*a*z2*z
          deriv(2,13) = 0.0_rp
          deriv(3,13) =-3.0_rp*a*(s*z2-s*z)
          deriv(1,14) =-3.0_rp*a*(s2*z-s*z)
          deriv(2,14) = 0.0_rp
          deriv(3,14) =-3.0_rp*a*s2*s
          deriv(1,15) =-3.0_rp*a*(s2*t-s*t)
          deriv(2,15) =-3.0_rp*a*s2*s
          deriv(3,15) = 0.0_rp
          deriv(1,16) =-3.0_rp*a*t2*t
          deriv(2,16) =-3.0_rp*a*(t2*s-s*t)
          deriv(3,16) = 0.0_rp
          deriv(1,17) =-27.0_rp*t*z
          deriv(2,17) = 27.0_rp*(p1*z-t*z)
          deriv(3,17) = 27.0_rp*(p1*t-t*z)
          deriv(1,18) = 27.0_rp*(p1*z-s*z)
          deriv(2,18) =-27.0_rp*s*z
          deriv(3,18) = 27.0_rp*(p1*s-s*z)
          deriv(1,19) = 27.0_rp*(p1*t-s*t)
          deriv(2,19) = 27.0_rp*(p1*s-s*t)
          deriv(3,19) =-27.0_rp*s*t
          deriv(1,20) = 27.0_rp*z*t
          deriv(2,20) = 27.0_rp*s*z
          deriv(3,20) = 27.0_rp*s*t
       end if

       if( present(heslo) ) then
          heslo(1, 1) = 2.0_rp*a*(p1+p2+p3)
          heslo(2, 1) = 2.0_rp*a*(p1+p2+p3)
          heslo(3, 1) = 2.0_rp*a*(p1+p2+p3)
          heslo(4, 1) = 2.0_rp*a*(p1+p2+p3)
          heslo(5, 1) = 2.0_rp*a*(p1+p2+p3)
          heslo(6, 1) = 2.0_rp*a*(p1+p2+p3)

          heslo(3, 2) = 2.0_rp*a*(z-z1-z2)

          heslo(1, 3) = 2.0_rp*a*(s-s1-s2)

          heslo(2, 4) = 2.0_rp*a*(t-t1-t2)

          heslo(1, 5) = 6.0_rp*a*t
          heslo(2, 5) = 6.0_rp*a*(-p1-p2+t)
          heslo(3, 5) = 6.0_rp*a*t
          heslo(4, 5) = 3.0_rp*a*(-p2-p1+2*t)
          heslo(5, 5) = 6.0_rp*a*t
          heslo(6, 5) = 3.0_rp*a*(-p2-p1+2*t)

          heslo(1, 6) = 6.0_rp*a*z
          heslo(2, 6) = 6.0_rp*a*z
          heslo(3, 6) = 6.0_rp*a*(-p1-p2+z)
          heslo(4, 6) = 6.0_rp*a*z
          heslo(5, 6) = 3.0_rp*a*(-p2-p1+2*z)
          heslo(6, 6) = 3.0_rp*a*(-p2-p1+2*z)

          heslo(1, 7) = 6.0_rp*a*(-p1-p2+s)
          heslo(2, 7) = 6.0_rp*a*s
          heslo(3, 7) = 6.0_rp*a*s
          heslo(4, 7) = 3.0_rp*a*(-p2-p1+2*s)
          heslo(5, 7) = 3.0_rp*a*(-p2-p1+2*s)
          heslo(6, 7) = 6.0_rp*a*s

          heslo(1, 8) = 0.0_rp
          heslo(2, 8) = 6.0_rp*a*(t2-t+p1)
          heslo(3, 8) = 0.0_rp
          heslo(4, 8) = 3.0_rp*a*(t2-t)
          heslo(5, 8) = 0.0_rp
          heslo(6, 8) = 3.0_rp*a*(t2-t)

          heslo(1, 9) = 6.0_rp*a*(s2-s+p1)
          heslo(2, 9) = 0.0_rp
          heslo(3, 9) = 0.0_rp
          heslo(4, 9) = 3.0_rp*a*(s2-s)
          heslo(5, 9) = 3.0_rp*a*(s2-s)
          heslo(6, 9) = 0.0_rp

          heslo(1,10) = 0.0_rp
          heslo(2,10) = 0.0_rp
          heslo(3,10) = 6.0_rp*a*(z2-z+p1)
          heslo(4,10) = 0.0_rp
          heslo(5,10) = 3.0_rp*a*(z2-z)           
          heslo(6,10) = 3.0_rp*a*(z2-z)

          heslo(1,11) = 0.0_rp
          heslo(2,11) = 6.0_rp*a*z
          heslo(3,11) = 0.0_rp
          heslo(4,11) = 0.0_rp
          heslo(5,11) = 0.0_rp
          heslo(6,11) = 3.0_rp*a*(t-t2)

          heslo(1,12) = 0.0_rp
          heslo(2,12) = 0.0_rp
          heslo(3,12) = 6.0_rp*a*t
          heslo(4,12) = 0.0_rp
          heslo(5,12) = 0.0_rp
          heslo(6,12) = 3.0_rp*a*(z-z2)

          heslo(1,13) = 0.0_rp
          heslo(2,13) = 0.0_rp
          heslo(3,13) = 6.0_rp*a*s
          heslo(4,13) = 0.0_rp
          heslo(5,13) = 3.0_rp*a*(z-z2)
          heslo(6,13) = 0.0_rp

          heslo(1,14) = 6.0_rp*a*z
          heslo(2,14) = 0.0_rp
          heslo(3,14) = 0.0_rp
          heslo(4,14) = 0.0_rp
          heslo(5,14) = 3.0_rp*a*(s-s2)
          heslo(6,14) = 0.0_rp

          heslo(1,15) = 6.0_rp*a*t
          heslo(2,15) = 0.0_rp
          heslo(3,15) = 0.0_rp
          heslo(4,15) = 3.0_rp*a*(s-s2)
          heslo(5,15) = 0.0_rp
          heslo(6,15) = 0.0_rp

          heslo(1,16) = 0.0_rp
          heslo(2,16) = 6.0_rp*a*s
          heslo(3,16) = 0.0_rp
          heslo(4,16) = 3.0_rp*a*(t-t2)
          heslo(5,16) = 0.0_rp
          heslo(6,16) = 0.0_rp

          heslo(1,17) = 0.0_rp
          heslo(2,17) =-54.0_rp*z
          heslo(3,17) =-54.0_rp*t
          heslo(4,17) =-27.0_rp*z
          heslo(5,17) =-27.0_rp*t
          heslo(6,17) = 27.0_rp*(p1-z-t)

          heslo(1,18) =-54.0_rp*z
          heslo(2,18) = 0.0_rp
          heslo(3,18) =-54.0_rp*s
          heslo(4,18) =-27.0_rp*z
          heslo(5,18) =-27.0_rp*s
          heslo(6,18) = 27.0_rp*(p1-z-s)

          heslo(1,19) =-54.0_rp*t
          heslo(2,19) =-54.0_rp*s
          heslo(3,19) = 0.0_rp
          heslo(4,19) = 27.0_rp*(p1-t-s)
          heslo(5,19) =-27.0_rp*t
          heslo(6,19) =-27.0_rp*s

          heslo(1,20) = 0.0_rp
          heslo(2,20) = 0.0_rp
          heslo(3,20) = 0.0_rp
          heslo(4,20) = 27.0_rp*z
          heslo(5,20) = 27.0_rp*t
          heslo(6,20) = 27.0_rp*s
       end if

    else if( nnode == 8 ) then
       !
       ! Trilinear brick: s=[-1:1], t=[-1:1], z=[-1:1] 
       !   
       sm = 0.5_rp*(1.0_rp-s)
       tm = 0.5_rp*(1.0_rp-t)
       zm = 0.5_rp*(1.0_rp-z)
       sq = 0.5_rp*(1.0_rp+s)
       tp = 0.5_rp*(1.0_rp+t)
       zp = 0.5_rp*(1.0_rp+z)
       shapf(   1) = sm*tm*zm
       shapf(   2) = sq*tm*zm
       shapf(   3) = sq*tp*zm
       shapf(   4) = sm*tp*zm
       shapf(   5) = sm*tm*zp
       shapf(   6) = sq*tm*zp 
       shapf(   7) = sq*tp*zp
       shapf(   8) = sm*tp*zp
       if( present(deriv) ) then
          deriv(1, 1) =-0.5_rp*tm*zm
          deriv(2, 1) =-0.5_rp*sm*zm
          deriv(3, 1) =-0.5_rp*sm*tm
          deriv(1, 2) = 0.5_rp*tm*zm
          deriv(2, 2) =-0.5_rp*sq*zm
          deriv(3, 2) =-0.5_rp*sq*tm
          deriv(1, 3) = 0.5_rp*tp*zm
          deriv(2, 3) = 0.5_rp*sq*zm
          deriv(3, 3) =-0.5_rp*sq*tp
          deriv(1, 4) =-0.5_rp*tp*zm
          deriv(2, 4) = 0.5_rp*sm*zm
          deriv(3, 4) =-0.5_rp*sm*tp
          deriv(1, 5) =-0.5_rp*tm*zp
          deriv(2, 5) =-0.5_rp*sm*zp
          deriv(3, 5) = 0.5_rp*sm*tm
          deriv(1, 6) = 0.5_rp*tm*zp
          deriv(2, 6) =-0.5_rp*sq*zp
          deriv(3, 6) = 0.5_rp*sq*tm
          deriv(1, 7) = 0.5_rp*tp*zp
          deriv(2, 7) = 0.5_rp*sq*zp
          deriv(3, 7) = 0.5_rp*sq*tp
          deriv(1, 8) =-0.5_rp*tp*zp
          deriv(2, 8) = 0.5_rp*sm*zp
          deriv(3, 8) = 0.5_rp*sm*tp
       end if
       if( present(heslo) ) then
          heslo(4, 1) = 0.25_rp*zm
          heslo(5, 1) = 0.25_rp*tm
          heslo(6, 1) = 0.25_rp*sm
          heslo(4, 2) =-0.25_rp*zm
          heslo(5, 2) =-0.25_rp*tm
          heslo(6, 2) = 0.25_rp*sq
          heslo(4, 3) = 0.25_rp*zm
          heslo(5, 3) =-0.25_rp*tp
          heslo(6, 3) =-0.25_rp*sq
          heslo(4, 4) =-0.25_rp*zm
          heslo(5, 4) = 0.25_rp*tp
          heslo(6, 4) =-0.25_rp*sm
          heslo(4, 5) = 0.25_rp*zp
          heslo(5, 5) =-0.25_rp*tm
          heslo(6, 5) =-0.25_rp*sm
          heslo(4, 6) =-0.25_rp*zp
          heslo(5, 6) = 0.25_rp*tm
          heslo(6, 6) =-0.25_rp*sq
          heslo(4, 7) = 0.25_rp*zp
          heslo(5, 7) = 0.25_rp*tp
          heslo(6, 7) = 0.25_rp*sq
          heslo(4, 8) =-0.25_rp*zp
          heslo(5, 8) =-0.25_rp*tp
          heslo(6, 8) = 0.25_rp*sm
       end if

    else if( nnode == 27 ) then
       !
       ! Triquadratic brick
       !        
       sl=s*(s-1.0_rp)
       tl=t*(t-1.0_rp)
       zl=z*(z-1.0_rp)
       sq=s*(s+1.0_rp)
       tp=t*(t+1.0_rp)
       zp=z*(z+1.0_rp)
       s1= 2.0_rp*s-1.0_rp
       t1= 2.0_rp*t-1.0_rp
       z1= 2.0_rp*z-1.0_rp
       s2= 1.0_rp-s*s
       t2= 1.0_rp-t*t
       z2= 1.0_rp-z*z
       s3= 1.0_rp+2.0_rp*s
       t3= 1.0_rp+2.0_rp*t
       z3= 1.0_rp+2.0_rp*z
       s4=-2.0_rp*s
       t4=-2.0_rp*t
       z4=-2.0_rp*z
       shapf(   1) = 0.125_rp*sl*tl*zl
       shapf(   2) = 0.125_rp*sq*tl*zl
       shapf(   3) = 0.125_rp*sq*tp*zl
       shapf(   4) = 0.125_rp*sl*tp*zl
       shapf(   5) = 0.125_rp*sl*tl*zp
       shapf(   6) = 0.125_rp*sq*tl*zp
       shapf(   7) = 0.125_rp*sq*tp*zp
       shapf(   8) = 0.125_rp*sl*tp*zp
       shapf(   9) = 0.25_rp*s2*tl*zl
       shapf(  10) = 0.25_rp*sq*t2*zl
       shapf(  11) = 0.25_rp*s2*tp*zl
       shapf(  12) = 0.25_rp*sl*t2*zl
       shapf(  13) = 0.25_rp*sl*tl*z2
       shapf(  14) = 0.25_rp*sq*tl*z2
       shapf(  15) = 0.25_rp*sq*tp*z2
       shapf(  16) = 0.25_rp*sl*tp*z2
       shapf(  17) = 0.25_rp*s2*tl*zp
       shapf(  18) = 0.25_rp*sq*t2*zp
       shapf(  19) = 0.25_rp*s2*tp*zp
       shapf(  20) = 0.25_rp*sl*t2*zp
       shapf(  21) = 0.5_rp*s2*t2*zl
       shapf(  22) = 0.5_rp*s2*tl*z2
       shapf(  23) = 0.5_rp*sq*t2*z2
       shapf(  24) = 0.5_rp*s2*tp*z2
       shapf(  25) = 0.5_rp*sl*t2*z2
       shapf(  26) = 0.5_rp*s2*t2*zp
       shapf(  27) = s2*t2*z2
       if( present(deriv) ) then
          deriv(1, 1) = 0.125_rp*s1*tl*zl
          deriv(2, 1) = 0.125_rp*sl*t1*zl
          deriv(3, 1) = 0.125_rp*sl*tl*z1
          deriv(1, 2) = 0.125_rp*s3*tl*zl
          deriv(2, 2) = 0.125_rp*sq*t1*zl
          deriv(3, 2) = 0.125_rp*sq*tl*z1
          deriv(1, 3) = 0.125_rp*s3*tp*zl
          deriv(2, 3) = 0.125_rp*sq*t3*zl
          deriv(3, 3) = 0.125_rp*sq*tp*z1
          deriv(1, 4) = 0.125_rp*s1*tp*zl
          deriv(2, 4) = 0.125_rp*sl*t3*zl
          deriv(3, 4) = 0.125_rp*sl*tp*z1
          deriv(1, 5) = 0.125_rp*s1*tl*zp
          deriv(2, 5) = 0.125_rp*sl*t1*zp
          deriv(3, 5) = 0.125_rp*sl*tl*z3
          deriv(1, 6) = 0.125_rp*s3*tl*zp
          deriv(2, 6) = 0.125_rp*sq*t1*zp
          deriv(3, 6) = 0.125_rp*sq*tl*z3
          deriv(1, 7) = 0.125_rp*s3*tp*zp
          deriv(2, 7) = 0.125_rp*sq*t3*zp
          deriv(3, 7) = 0.125_rp*sq*tp*z3
          deriv(1, 8) = 0.125_rp*s1*tp*zp
          deriv(2, 8) = 0.125_rp*sl*t3*zp
          deriv(3, 8) = 0.125_rp*sl*tp*z3
          deriv(1, 9) = 0.25_rp*s4*tl*zl
          deriv(2, 9) = 0.25_rp*s2*t1*zl
          deriv(3, 9) = 0.25_rp*s2*tl*z1
          deriv(1,10) = 0.25_rp*s3*t2*zl
          deriv(2,10) = 0.25_rp*sq*t4*zl
          deriv(3,10) = 0.25_rp*sq*t2*z1
          deriv(1,11) = 0.25_rp*s4*tp*zl
          deriv(2,11) = 0.25_rp*s2*t3*zl
          deriv(3,11) = 0.25_rp*s2*tp*z1
          deriv(1,12) = 0.25_rp*s1*t2*zl
          deriv(2,12) = 0.25_rp*sl*t4*zl
          deriv(3,12) = 0.25_rp*sl*t2*z1
          deriv(1,13) = 0.25_rp*s1*tl*z2
          deriv(2,13) = 0.25_rp*sl*t1*z2
          deriv(3,13) = 0.25_rp*sl*tl*z4
          deriv(1,14) = 0.25_rp*s3*tl*z2
          deriv(2,14) = 0.25_rp*sq*t1*z2
          deriv(3,14) = 0.25_rp*sq*tl*z4
          deriv(1,15) = 0.25_rp*s3*tp*z2
          deriv(2,15) = 0.25_rp*sq*t3*z2
          deriv(3,15) = 0.25_rp*sq*tp*z4
          deriv(1,16) = 0.25_rp*s1*tp*z2
          deriv(2,16) = 0.25_rp*sl*t3*z2
          deriv(3,16) = 0.25_rp*sl*tp*z4
          deriv(1,17) = 0.25_rp*s4*tl*zp
          deriv(2,17) = 0.25_rp*s2*t1*zp
          deriv(3,17) = 0.25_rp*s2*tl*z3
          deriv(1,18) = 0.25_rp*s3*t2*zp
          deriv(2,18) = 0.25_rp*sq*t4*zp
          deriv(3,18) = 0.25_rp*sq*t2*z3
          deriv(1,19) = 0.25_rp*s4*tp*zp
          deriv(2,19) = 0.25_rp*s2*t3*zp
          deriv(3,19) = 0.25_rp*s2*tp*z3
          deriv(1,20) = 0.25_rp*s1*t2*zp
          deriv(2,20) = 0.25_rp*sl*t4*zp
          deriv(3,20) = 0.25_rp*sl*t2*z3
          deriv(1,21) = 0.5_rp*s4*t2*zl
          deriv(2,21) = 0.5_rp*s2*t4*zl
          deriv(3,21) = 0.5_rp*s2*t2*z1
          deriv(1,22) = 0.5_rp*s4*tl*z2
          deriv(2,22) = 0.5_rp*s2*t1*z2
          deriv(3,22) = 0.5_rp*s2*tl*z4
          deriv(1,23) = 0.5_rp*s3*t2*z2
          deriv(2,23) = 0.5_rp*sq*t4*z2
          deriv(3,23) = 0.5_rp*sq*t2*z4
          deriv(1,24) = 0.5_rp*s4*tp*z2
          deriv(2,24) = 0.5_rp*s2*t3*z2
          deriv(3,24) = 0.5_rp*s2*tp*z4
          deriv(1,25) = 0.5_rp*s1*t2*z2
          deriv(2,25) = 0.5_rp*sl*t4*z2
          deriv(3,25) = 0.5_rp*sl*t2*z4
          deriv(1,26) = 0.5_rp*s4*t2*zp
          deriv(2,26) = 0.5_rp*s2*t4*zp
          deriv(3,26) = 0.5_rp*s2*t2*z3
          deriv(1,27) = s4*t2*z2
          deriv(2,27) = s2*t4*z2
          deriv(3,27) = s2*t2*z4
       end if
       if( present(heslo) ) then
          heslo(1, 1) = 0.25_rp*tl*zl
          heslo(2, 1) = 0.25_rp*sl*zl
          heslo(3, 1) = 0.25_rp*sl*tl
          heslo(4, 1) = 0.125_rp*s1*t1*zl
          heslo(5, 1) = 0.125_rp*s1*tl*z1
          heslo(6, 1) = 0.125_rp*sl*t1*z1
          heslo(1, 2) = 0.25_rp*tl*zl
          heslo(2, 2) = 0.25_rp*sq*zl
          heslo(3, 2) = 0.25_rp*sq*tl
          heslo(4, 2) = 0.125_rp*s3*t1*zl
          heslo(5, 2) = 0.125_rp*s3*tl*z1
          heslo(6, 2) = 0.125_rp*sq*t1*z1
          heslo(1, 3) = 0.25_rp*tp*zl
          heslo(2, 3) = 0.25_rp*sq*zl
          heslo(3, 3) = 0.25_rp*sq*tp
          heslo(4, 3) = 0.125_rp*s3*t3*zl
          heslo(5, 3) = 0.125_rp*s3*tp*z1
          heslo(6, 3) = 0.125_rp*sq*t3*z1
          heslo(1, 4) = 0.25_rp*tp*zl
          heslo(2, 4) = 0.25_rp*sl*zl
          heslo(3, 4) = 0.25_rp*sl*tp
          heslo(4, 4) = 0.125_rp*s1*t3*zl
          heslo(5, 4) = 0.125_rp*s1*tp*z1
          heslo(6, 4) = 0.125_rp*sl*t3*z1
          heslo(1, 5) = 0.25_rp*tl*zp
          heslo(2, 5) = 0.25_rp*sl*zp
          heslo(3, 5) = 0.25_rp*sl*tl
          heslo(4, 5) = 0.125_rp*s1*t1*zp
          heslo(5, 5) = 0.125_rp*s1*tl*z3
          heslo(6, 5) = 0.125_rp*sl*t1*z3
          heslo(1, 6) = 0.25_rp*tl*zp
          heslo(2, 6) = 0.25_rp*sq*zp
          heslo(3, 6) = 0.25_rp*sq*tl
          heslo(4, 6) = 0.125_rp*s3*t1*zp
          heslo(5, 6) = 0.125_rp*s3*tl*z3
          heslo(6, 6) = 0.125_rp*sq*t1*z3
          heslo(1, 7) = 0.25_rp*tp*zp
          heslo(2, 7) = 0.25_rp*sq*zp
          heslo(3, 7) = 0.25_rp*sq*tp
          heslo(4, 7) = 0.125_rp*s3*t3*zp
          heslo(5, 7) = 0.125_rp*s3*tp*z3
          heslo(6, 7) = 0.125_rp*sq*t3*z3
          heslo(1, 8) = 0.25_rp*tp*zp
          heslo(2, 8) = 0.25_rp*sl*zp
          heslo(3, 8) = 0.25_rp*sl*tp
          heslo(4, 8) = 0.125_rp*s1*t3*zp
          heslo(5, 8) = 0.125_rp*s1*tp*z3
          heslo(6, 8) = 0.125_rp*sl*t3*z3
          heslo(1, 9) =-0.5_rp*tl*zl
          heslo(2, 9) = 0.5_rp*s2*zl
          heslo(3, 9) = 0.5_rp*s2*tl
          heslo(4, 9) = 0.25_rp*s4*t1*zl
          heslo(5, 9) = 0.25_rp*s4*tl*z1
          heslo(6, 9) = 0.25_rp*s2*t1*z1
          heslo(1,10) = 0.5_rp*t2*zl
          heslo(2,10) =-0.5_rp*sq*zl
          heslo(3,10) = 0.5_rp*sq*t2
          heslo(4,10) = 0.25_rp*s3*t4*zl
          heslo(5,10) = 0.25_rp*s3*t2*z1
          heslo(6,10) = 0.25_rp*sq*t4*z1
          heslo(1,11) =-0.5_rp*tp*zl
          heslo(2,11) = 0.5_rp*s2*zl
          heslo(3,11) = 0.5_rp*s2*tp
          heslo(4,11) = 0.25_rp*s4*t3*zl
          heslo(5,11) = 0.25_rp*s4*tp*z1
          heslo(6,11) = 0.25_rp*s2*t3*z1
          heslo(1,12) = 0.5_rp*t2*zl
          heslo(2,12) =-0.5_rp*sl*zl
          heslo(3,12) = 0.5_rp*sl*t2
          heslo(4,12) = 0.25_rp*s1*t4*zl
          heslo(5,12) = 0.25_rp*s1*t2*z1
          heslo(6,12) = 0.25_rp*sl*t4*z1
          heslo(1,13) = 0.5_rp*tl*z2
          heslo(2,13) = 0.5_rp*sl*z2
          heslo(3,13) =-0.5_rp*sl*tl
          heslo(4,13) = 0.25_rp*s1*t1*z2
          heslo(5,13) = 0.25_rp*s1*tl*z4
          heslo(6,13) = 0.25_rp*sl*t1*z4
          heslo(1,14) = 0.5_rp*tl*z2
          heslo(2,14) = 0.5_rp*sq*z2
          heslo(3,14) =-0.5_rp*sq*tl
          heslo(4,14) = 0.25_rp*s3*t1*z2
          heslo(5,14) = 0.25_rp*s3*tl*z4
          heslo(6,14) = 0.25_rp*sq*t1*z4
          heslo(1,15) = 0.5_rp*tp*z2
          heslo(2,15) = 0.5_rp*sq*z2
          heslo(3,15) =-0.5_rp*sq*tp
          heslo(4,15) = 0.25_rp*s3*t3*z2
          heslo(5,15) = 0.25_rp*s3*tp*z4
          heslo(6,15) = 0.25_rp*sq*t3*z4
          heslo(1,16) = 0.5_rp*tp*z2
          heslo(2,16) = 0.5_rp*sl*z2
          heslo(3,16) =-0.5_rp*sl*tp
          heslo(4,16) = 0.25_rp*s1*t3*z2
          heslo(5,16) = 0.25_rp*s1*tp*z4
          heslo(6,16) = 0.25_rp*sl*t3*z4
          heslo(1,17) =-0.5_rp*tl*zp
          heslo(2,17) = 0.5_rp*s2*zp
          heslo(3,17) = 0.5_rp*s2*tl
          heslo(4,17) = 0.25_rp*s4*t1*zp
          heslo(5,17) = 0.25_rp*s4*tl*z3
          heslo(6,17) = 0.25_rp*s2*t1*z3
          heslo(1,18) = 0.5_rp*t2*zp
          heslo(2,18) =-0.5_rp*sq*zp
          heslo(3,18) = 0.5_rp*sq*t2
          heslo(4,18) = 0.25_rp*s3*t4*zp
          heslo(5,18) = 0.25_rp*s3*t2*z3
          heslo(6,18) = 0.25_rp*sq*t4*z3
          heslo(1,19) =-0.5_rp*tp*zp
          heslo(2,19) = 0.5_rp*s2*zp
          heslo(3,19) = 0.5_rp*s2*tp
          heslo(4,19) = 0.25_rp*s4*t3*zp
          heslo(5,19) = 0.25_rp*s4*tp*z3
          heslo(6,19) = 0.25_rp*s2*t3*z3
          heslo(1,20) = 0.5_rp*t2*zp
          heslo(2,20) =-0.5_rp*sl*zp
          heslo(3,20) = 0.5_rp*sl*t2
          heslo(4,20) = 0.25_rp*s1*t4*zp
          heslo(5,20) = 0.25_rp*s1*t2*z3
          heslo(6,20) = 0.25_rp*sl*t4*z3
          heslo(1,21) =-t2*zl
          heslo(2,21) =-s2*zl
          heslo(3,21) = s2*t2
          heslo(4,21) = 0.5_rp*s4*t4*zl
          heslo(5,21) = 0.5_rp*s4*t2*z1
          heslo(6,21) = 0.5_rp*s2*t4*z1
          heslo(1,22) =-tl*z2
          heslo(2,22) = s2*z2
          heslo(3,22) =-s2*tl
          heslo(4,22) = 0.5_rp*s4*t1*z2
          heslo(5,22) = 0.5_rp*s4*tl*z4
          heslo(6,22) = 0.5_rp*s2*t1*z4
          heslo(1,23) = t2*z2
          heslo(2,23) =-sq*z2
          heslo(3,23) =-sq*t2
          heslo(4,23) = 0.5_rp*s3*t4*z2
          heslo(5,23) = 0.5_rp*s3*t2*z4
          heslo(6,23) = 0.5_rp*sq*t4*z4
          heslo(1,24) =-tp*z2
          heslo(2,24) = s2*z2
          heslo(3,24) =-s2*tp
          heslo(4,24) = 0.5_rp*s4*t3*z2
          heslo(5,24) = 0.5_rp*s4*tp*z4
          heslo(6,24) = 0.5_rp*s2*t3*z4
          heslo(1,25) = t2*z2
          heslo(2,25) =-sl*z2
          heslo(3,25) =-sl*t2
          heslo(4,25) = 0.5_rp*s1*t4*z2
          heslo(5,25) = 0.5_rp*s1*t2*z4
          heslo(6,25) = 0.5_rp*sl*t4*z4
          heslo(1,26) =-t2*zp
          heslo(2,26) =-s2*zp
          heslo(3,26) = s2*t2
          heslo(4,26) = 0.5_rp*s4*t4*zp
          heslo(5,26) = 0.5_rp*s4*t2*z3
          heslo(6,26) = 0.5_rp*s2*t4*z3
          heslo(1,27) =-2.0_rp*t2*z2
          heslo(2,27) =-2.0_rp*s2*z2
          heslo(3,27) =-2.0_rp*s2*t2
          heslo(4,27) = s4*t4*z2
          heslo(5,27) = s4*t2*z4
          heslo(6,27) = s2*t4*z4
       end if

    else if( nnode == 64 ) then
       !
       ! Tricubic brick
       !        
       a=729.0_rp/4096.0_rp
       s1=(1.0_rp/3.0_rp+s)*(1.0_rp/3.0_rp-s)*(1.0_rp-s)                           
       s2=(1.0_rp+s)*(1.0_rp/3.0_rp-s)*(1.0_rp-s)                              
       s3=(1.0_rp+s)*(1.0_rp/3.0_rp+s)*(1.0_rp-s)
       s4=(1.0_rp+s)*(1.0_rp/3.0_rp+s)*(1.0_rp/3.0_rp-s)                                
       t1=(1.0_rp/3.0_rp+t)*(1.0_rp/3.0_rp-t)*(1.0_rp-t)         
       t2=(1.0_rp+s)*(1.0_rp/3.0_rp-t)*(1.0_rp-t)
       t3=(1.0_rp+t)*(1.0_rp/3.0_rp+t)*(1.0_rp-t)                         
       t4=(1.0_rp+t)*(1.0_rp/3.0_rp+t)*(1.0_rp/3.0_rp-t)
       z1=(1.0_rp/3.0_rp+z)*(1.0_rp/3.0_rp-z)*(1.0_rp-z)
       z2=(1.0_rp+z)*(1.0_rp/3.0_rp-z)*(1.0_rp-z)
       z3=(1.0_rp+z)*(1.0_rp/3.0_rp+z)*(1.0_rp-z)
       z4=(1.0_rp+z)*(1.0_rp/3.0_rp+z)*(1.0_rp/3.0_rp-z)
       s11=(1.0_rp/3.0_rp-s)*(1.0_rp-s)-(1.0_rp/3.0_rp+s)*(1.0_rp-s)&
            -(1.0_rp/3.0_rp+s)*(1.0_rp/3.0_rp-s)
       s21=(1.0_rp/3.0_rp-s)*(1.0_rp-s)-(1.0_rp+s)*(1.0_rp-s)&
            -(1.0_rp+s)*(1.0_rp/3.0_rp-s)
       s31=(1.0_rp/3.0_rp+s)*(1.0_rp-s)+(1.0_rp+s)*(1.0_rp-s)&
            -(1.0_rp+s)*(1.0_rp/3.0_rp+s)
       s41=(1.0_rp/3.0_rp+s)*(1.0_rp/3.0_rp-s)+(1.0_rp+s)*(1.0_rp/3.0_rp-s)&
            -(1.0_rp+s)*(1.0_rp/3.0_rp+s)
       t11=(1.0_rp/3.0_rp-t)*(1.0_rp-t)-(1.0_rp/3.0_rp+t)*(1.0_rp-t)&
            -(1.0_rp/3.0_rp+t)*(1.0_rp/3.0_rp-t)
       t21=(1.0_rp/3.0_rp-t)*(1.0_rp-t)-(1.0_rp+t)*(1.0_rp-t)&
            -(1.0_rp+t)*(1.0_rp/3.0_rp-t)    
       t31=(1.0_rp/3.0_rp+t)*(1.0_rp-t)+(1.0_rp+t)*(1.0_rp-t)&
            -(1.0_rp+t)*(1.0_rp/3.0_rp+t)    
       t41=(1.0_rp/3.0_rp+t)*(1.0_rp/3.0_rp-t)+(1.0_rp+t)*(1.0_rp/3.0_rp-t)&
            -(1.0_rp+t)*(1.0_rp/3.0_rp+t)
       z11=(1.0_rp/3.0_rp-z)*(1.0_rp-z)-(1.0_rp/3.0_rp+z)*(1.0_rp-z)&
            -(1.0_rp/3.0_rp+z)*(1.0_rp/3.0_rp-z)
       z21=(1.0_rp/3.0_rp-z)*(1.0_rp-z)-(1.0_rp+z)*(1.0_rp-z)&
            -(1.0_rp+z)*(1.0_rp/3.0_rp-z)    
       z31=(1.0_rp/3.0_rp+z)*(1.0_rp-z)+(1.0_rp+z)*(1.0_rp-z)&
            -(1.0_rp+z)*(1.0_rp/3.0_rp+z)    
       z41=(1.0_rp/3.0_rp+z)*(1.0_rp/3.0_rp-z)+(1.0_rp+z)*(1.0_rp/3.0_rp-z)&
            -(1.0_rp+z)*(1.0_rp/3.0_rp+z)
       shapf(   1) =   a*s1*t1*z1
       shapf(   2) =   a*s4*t1*z1
       shapf(   3) =   a*s4*t4*z1
       shapf(   4) =   a*s1*t4*z1
       shapf(   5) =   a*s1*t1*z4
       shapf(   6) =   a*s4*t1*z4
       shapf(   7) =   a*s4*t4*z4
       shapf(   8) =   a*s1*t4*z4
       shapf(   9) = 3.0_rp*a*s2*t1*z1
       shapf(  10) = 3.0_rp*a*s3*t1*z1
       shapf(  11) = 3.0_rp*a*s4*t2*z1
       shapf(  12) = 3.0_rp*a*s4*t3*z1
       shapf(  13) = 3.0_rp*a*s3*t4*z1
       shapf(  14) = 3.0_rp*a*s2*t4*z1
       shapf(  15) = 3.0_rp*a*s1*t3*z1
       shapf(  16) = 3.0_rp*a*s1*t2*z1
       shapf(  17) = 3.0_rp*a*s1*t1*z2
       shapf(  18) = 3.0_rp*a*s4*t1*z2
       shapf(  19) = 3.0_rp*a*s4*t4*z2
       shapf(  20) = 3.0_rp*a*s1*t4*z2
       shapf(  21) = 3.0_rp*a*s1*t1*z3
       shapf(  22) = 3.0_rp*a*s4*t1*z3
       shapf(  23) = 3.0_rp*a*s4*t4*z3
       shapf(  24) = 3.0_rp*a*s1*t4*z3
       shapf(  25) = 3.0_rp*a*s2*t1*z4
       shapf(  26) = 3.0_rp*a*s3*t1*z4
       shapf(  27) = 3.0_rp*a*s4*t2*z4
       shapf(  28) = 3.0_rp*a*s4*t3*z4
       shapf(  29) = 3.0_rp*a*s3*t4*z4
       shapf(  30) = 3.0_rp*a*s2*t4*z4
       shapf(  31) = 3.0_rp*a*s1*t3*z4
       shapf(  32) = 3.0_rp*a*s1*t2*z4
       shapf(  33) =-9.0_rp*a*s2*t2*z1
       shapf(  34) =-9.0_rp*a*s3*t2*z1
       shapf(  35) =-9.0_rp*a*s3*t3*z1
       shapf(  36) =-9.0_rp*a*s2*t3*z1
       shapf(  37) =-9.0_rp*a*s2*t1*z2
       shapf(  38) =-9.0_rp*a*s3*t1*z2
       shapf(  39) =-9.0_rp*a*s4*t2*z2
       shapf(  40) =-9.0_rp*a*s4*t3*z2
       shapf(  41) =-9.0_rp*a*s3*t4*z2
       shapf(  42) =-9.0_rp*a*s2*t4*z2
       shapf(  43) =-9.0_rp*a*s1*t3*z2
       shapf(  44) =-9.0_rp*a*s1*t2*z2
       shapf(  45) =-9.0_rp*a*s2*t1*z3
       shapf(  46) =-9.0_rp*a*s3*t1*z3
       shapf(  47) =-9.0_rp*a*s4*t2*z3
       shapf(  48) =-9.0_rp*a*s4*t3*z3
       shapf(  49) =-9.0_rp*a*s3*t4*z3
       shapf(  50) =-9.0_rp*a*s2*t4*z3
       shapf(  51) =-9.0_rp*a*s1*t3*z3
       shapf(  52) =-9.0_rp*a*s1*t2*z3
       shapf(  53) =-9.0_rp*a*s2*t2*z4
       shapf(  54) =-9.0_rp*a*s3*t2*z4
       shapf(  55) =-9.0_rp*a*s3*t3*z4
       shapf(  56) =-9.0_rp*a*s2*t3*z4
       shapf(  57) = 27.0_rp*a*s2*t2*z2
       shapf(  58) = 27.0_rp*a*s3*t2*z2
       shapf(  59) = 27.0_rp*a*s3*t3*z2
       shapf(  60) = 27.0_rp*a*s2*t3*z2
       shapf(  61) = 27.0_rp*a*s2*t2*z3
       shapf(  62) = 27.0_rp*a*s3*t2*z3
       shapf(  63) = 27.0_rp*a*s3*t3*z3
       shapf(  64) = 27.0_rp*a*s2*t3*z3

       if( present(deriv) ) then
          deriv(1, 1) =   a*s11*t1*z1
          deriv(2, 1) =   a*s1*t11*z1
          deriv(3, 1) =   a*s1*t1*z11
          deriv(1, 2) =   a*s41*t1*z1
          deriv(2, 2) =   a*s4*t11*z1
          deriv(3, 2) =   a*s4*t1*z11
          deriv(1, 3) =   a*s41*t4*z1
          deriv(2, 3) =   a*s4*t41*z1
          deriv(3, 3) =   a*s4*t4*z11
          deriv(1, 4) =   a*s11*t4*z1
          deriv(2, 4) =   a*s1*t41*z1
          deriv(3, 4) =   a*s1*t4*z11
          deriv(1, 5) =   a*s11*t1*z4
          deriv(2, 5) =   a*s1*t11*z4
          deriv(3, 5) =   a*s1*t1*z41
          deriv(1, 6) =   a*s41*t1*z4
          deriv(2, 6) =   a*s4*t11*z4
          deriv(3, 6) =   a*s4*t1*z41
          deriv(1, 7) =   a*s41*t4*z4
          deriv(2, 7) =   a*s4*t41*z4
          deriv(3, 7) =   a*s4*t4*z41
          deriv(1, 8) =   a*s11*t4*z4
          deriv(2, 8) =   a*s1*t41*z4
          deriv(3, 8) =   a*s1*t4*z41
          deriv(1, 9) = 3.0_rp*a*s21*t1*z1
          deriv(2, 9) = 3.0_rp*a*s2*t11*z1
          deriv(3, 9) = 3.0_rp*a*s2*t1*z11
          deriv(1,10) = 3.0_rp*a*s31*t1*z1
          deriv(2,10) = 3.0_rp*a*s3*t11*z1
          deriv(3,10) = 3.0_rp*a*s3*t1*z11        
          deriv(1,11) = 3.0_rp*a*s41*t2*z1
          deriv(2,11) = 3.0_rp*a*s4*t21*z1
          deriv(3,11) = 3.0_rp*a*s4*t2*z11        
          deriv(1,12) = 3.0_rp*a*s41*t3*z1        
          deriv(2,12) = 3.0_rp*a*s4*t31*z1        
          deriv(3,12) = 3.0_rp*a*s4*t3*z11                
          deriv(1,13) = 3.0_rp*a*s31*t4*z1
          deriv(2,13) = 3.0_rp*a*s3*t41*z1
          deriv(3,13) = 3.0_rp*a*s3*t4*z11        
          deriv(1,14) = 3.0_rp*a*s21*t4*z1
          deriv(2,14) = 3.0_rp*a*s2*t41*z1
          deriv(3,14) = 3.0_rp*a*s2*t4*z11        
          deriv(1,15) = 3.0_rp*a*s11*t3*z1
          deriv(2,15) = 3.0_rp*a*s1*t31*z1
          deriv(3,15) = 3.0_rp*a*s1*t3*z11        
          deriv(1,16) = 3.0_rp*a*s11*t2*z1
          deriv(2,16) = 3.0_rp*a*s1*t21*z1
          deriv(3,16) = 3.0_rp*a*s1*t2*z11        
          deriv(1,17) = 3.0_rp*a*s11*t1*z2
          deriv(2,17) = 3.0_rp*a*s1*t11*z2
          deriv(3,17) = 3.0_rp*a*s1*t1*z21        
          deriv(1,18) = 3.0_rp*a*s41*t1*z2
          deriv(2,18) = 3.0_rp*a*s4*t11*z2
          deriv(3,18) = 3.0_rp*a*s4*t1*z21        
          deriv(1,19) = 3.0_rp*a*s41*t4*z2
          deriv(2,19) = 3.0_rp*a*s4*t41*z2
          deriv(3,19) = 3.0_rp*a*s4*t4*z21        
          deriv(1,20) = 3.0_rp*a*s11*t4*z2
          deriv(2,20) = 3.0_rp*a*s1*t41*z2
          deriv(3,20) = 3.0_rp*a*s1*t4*z21        
          deriv(1,21) = 3.0_rp*a*s11*t1*z3
          deriv(2,21) = 3.0_rp*a*s1*t11*z3
          deriv(3,21) = 3.0_rp*a*s1*t1*z31        
          deriv(1,22) = 3.0_rp*a*s41*t1*z3
          deriv(2,22) = 3.0_rp*a*s4*t11*z3
          deriv(3,22) = 3.0_rp*a*s4*t1*z31
          deriv(1,23) = 3.0_rp*a*s41*t4*z3
          deriv(2,23) = 3.0_rp*a*s4*t41*z3
          deriv(3,23) = 3.0_rp*a*s4*t4*z31        
          deriv(1,24) = 3.0_rp*a*s11*t4*z3
          deriv(2,24) = 3.0_rp*a*s1*t41*z3
          deriv(3,24) = 3.0_rp*a*s1*t4*z31        
          deriv(1,25) = 3.0_rp*a*s21*t1*z4
          deriv(2,25) = 3.0_rp*a*s2*t11*z4
          deriv(3,25) = 3.0_rp*a*s2*t1*z41        
          deriv(1,21) = 3.0_rp*a*s31*t1*z4
          deriv(2,21) = 3.0_rp*a*s3*t11*z4
          deriv(3,21) = 3.0_rp*a*s3*t1*z41        
          deriv(1,27) = 3.0_rp*a*s41*t2*z4
          deriv(2,27) = 3.0_rp*a*s4*t21*z4
          deriv(3,27) = 3.0_rp*a*s4*t2*z41        
          deriv(1,28) = 3.0_rp*a*s41*t3*z4
          deriv(2,28) = 3.0_rp*a*s4*t31*z4
          deriv(3,28) = 3.0_rp*a*s4*t3*z41        
          deriv(1,29) = 3.0_rp*a*s31*t4*z4
          deriv(2,29) = 3.0_rp*a*s3*t41*z4
          deriv(3,29) = 3.0_rp*a*s3*t4*z41        
          deriv(1,30) = 3.0_rp*a*s21*t4*z4  
          deriv(2,30) = 3.0_rp*a*s2*t41*z4  
          deriv(3,30) = 3.0_rp*a*s2*t4*z41          
          deriv(1,31) = 3.0_rp*a*s11*t3*z4
          deriv(2,31) = 3.0_rp*a*s1*t31*z4
          deriv(3,31) = 3.0_rp*a*s1*t3*z41        
          deriv(1,32) = 3.0_rp*a*s11*t2*z4
          deriv(2,32) = 3.0_rp*a*s1*t21*z4
          deriv(3,32) = 3.0_rp*a*s1*t2*z41        
          deriv(1,33) =-9.0_rp*a*s21*t2*z1
          deriv(2,33) =-9.0_rp*a*s2*t21*z1
          deriv(3,33) =-9.0_rp*a*s2*t2*z11        
          deriv(1,34) =-9.0_rp*a*s31*t2*z1
          deriv(2,34) =-9.0_rp*a*s3*t21*z1
          deriv(3,34) =-9.0_rp*a*s3*t2*z11        
          deriv(1,35) =-9.0_rp*a*s31*t3*z1
          deriv(2,35) =-9.0_rp*a*s3*t31*z1
          deriv(3,35) =-9.0_rp*a*s3*t3*z11        
          deriv(1,36) =-9.0_rp*a*s21*t3*z1
          deriv(2,36) =-9.0_rp*a*s2*t31*z1
          deriv(3,36) =-9.0_rp*a*s2*t3*z11        
          deriv(1,37) =-9.0_rp*a*s21*t1*z2
          deriv(2,37) =-9.0_rp*a*s2*t11*z2
          deriv(3,37) =-9.0_rp*a*s2*t1*z21        
          deriv(1,38) =-9.0_rp*a*s31*t1*z2        
          deriv(2,38) =-9.0_rp*a*s3*t11*z2        
          deriv(3,38) =-9.0_rp*a*s3*t1*z21                
          deriv(1,39) =-9.0_rp*a*s41*t2*z2        
          deriv(2,39) =-9.0_rp*a*s4*t21*z2        
          deriv(3,39) =-9.0_rp*a*s4*t2*z21                
          deriv(1,40) =-9.0_rp*a*s41*t3*z2        
          deriv(2,40) =-9.0_rp*a*s4*t31*z2        
          deriv(3,40) =-9.0_rp*a*s4*t3*z21                
          deriv(1,41) =-9.0_rp*a*s31*t4*z2        
          deriv(2,41) =-9.0_rp*a*s3*t41*z2        
          deriv(3,41) =-9.0_rp*a*s3*t4*z21                
          deriv(1,42) =-9.0_rp*a*s21*t4*z2             
          deriv(2,42) =-9.0_rp*a*s2*t41*z2             
          deriv(3,42) =-9.0_rp*a*s2*t4*z21                     
          deriv(1,43) =-9.0_rp*a*s11*t3*z2             
          deriv(2,43) =-9.0_rp*a*s1*t31*z2             
          deriv(3,43) =-9.0_rp*a*s1*t3*z21                     
          deriv(1,44) =-9.0_rp*a*s11*t2*z2             
          deriv(2,44) =-9.0_rp*a*s1*t21*z2             
          deriv(3,44) =-9.0_rp*a*s1*t2*z21                     
          deriv(1,45) =-9.0_rp*a*s21*t1*z3             
          deriv(2,45) =-9.0_rp*a*s2*t11*z3             
          deriv(3,45) =-9.0_rp*a*s2*t1*z31                     
          deriv(1,46) =-9.0_rp*a*s31*t1*z3
          deriv(2,46) =-9.0_rp*a*s3*t11*z3
          deriv(3,46) =-9.0_rp*a*s3*t1*z31        
          deriv(1,47) =-9.0_rp*a*s41*t2*z3
          deriv(2,47) =-9.0_rp*a*s4*t21*z3
          deriv(3,47) =-9.0_rp*a*s4*t2*z31
          deriv(1,48) =-9.0_rp*a*s41*t3*z3
          deriv(2,48) =-9.0_rp*a*s4*t31*z3
          deriv(3,48) =-9.0_rp*a*s4*t3*z31        
          deriv(1,49) =-9.0_rp*a*s31*t4*z3
          deriv(2,49) =-9.0_rp*a*s3*t41*z3
          deriv(3,49) =-9.0_rp*a*s3*t4*z31        
          deriv(1,50) =-9.0_rp*a*s21*t4*z3
          deriv(2,50) =-9.0_rp*a*s2*t41*z3
          deriv(3,50) =-9.0_rp*a*s2*t4*z31        
          deriv(1,51) =-9.0_rp*a*s11*t3*z3
          deriv(2,51) =-9.0_rp*a*s1*t31*z3
          deriv(3,51) =-9.0_rp*a*s1*t3*z31        
          deriv(1,52) =-9.0_rp*a*s11*t2*z3
          deriv(2,52) =-9.0_rp*a*s1*t21*z3
          deriv(3,52) =-9.0_rp*a*s1*t2*z31        
          deriv(1,53) =-9.0_rp*a*s21*t2*z4
          deriv(2,53) =-9.0_rp*a*s2*t21*z4
          deriv(3,53) =-9.0_rp*a*s2*t2*z41        
          deriv(1,54) =-9.0_rp*a*s31*t2*z4
          deriv(2,54) =-9.0_rp*a*s3*t21*z4
          deriv(3,54) =-9.0_rp*a*s3*t2*z41        
          deriv(1,55) =-9.0_rp*a*s31*t3*z4
          deriv(2,55) =-9.0_rp*a*s3*t31*z4
          deriv(3,55) =-9.0_rp*a*s3*t3*z41        
          deriv(1,56) =-9.0_rp*a*s21*t3*z4
          deriv(2,56) =-9.0_rp*a*s2*t31*z4
          deriv(3,56) =-9.0_rp*a*s2*t3*z41        
          deriv(1,57) = 27.0_rp*a*s21*t2*z2
          deriv(2,57) = 27.0_rp*a*s2*t21*z2
          deriv(3,57) = 27.0_rp*a*s2*t2*z21        
          deriv(1,58) = 27.0_rp*a*s31*t2*z2
          deriv(2,58) = 27.0_rp*a*s3*t21*z2
          deriv(3,58) = 27.0_rp*a*s3*t2*z21        
          deriv(1,59) = 27.0_rp*a*s31*t3*z2
          deriv(2,59) = 27.0_rp*a*s3*t31*z2
          deriv(3,59) = 27.0_rp*a*s3*t3*z21        
          deriv(1,60) = 27.0_rp*a*s21*t3*z2
          deriv(2,60) = 27.0_rp*a*s2*t31*z2
          deriv(3,60) = 27.0_rp*a*s2*t3*z21        
          deriv(1,61) = 27.0_rp*a*s21*t2*z3
          deriv(2,61) = 27.0_rp*a*s2*t21*z3
          deriv(3,61) = 27.0_rp*a*s2*t2*z31        
          deriv(1,62) = 27.0_rp*a*s31*t2*z3
          deriv(2,62) = 27.0_rp*a*s3*t21*z3
          deriv(3,62) = 27.0_rp*a*s3*t2*z31        
          deriv(1,63) = 27.0_rp*a*s31*t3*z3
          deriv(2,63) = 27.0_rp*a*s3*t31*z3
          deriv(3,63) = 27.0_rp*a*s3*t3*z31        
          deriv(1,64) = 27.0_rp*a*s21*t3*z3
          deriv(2,64) = 27.0_rp*a*s2*t31*z3
          deriv(3,64) = 27.0_rp*a*s2*t3*z31
       end if

       if( present(heslo) ) then

          s12=-2.0_rp*((1.0_rp-s)+(1.0_rp/3.0_rp-s)-(1.0_rp/3.0_rp+s))
          s22=-2.0_rp*((1.0_rp-s)+(1.0_rp/3.0_rp-s)-(1.0_rp+s))
          s32= 2.0_rp*((1.0_rp-s)-(1.0_rp/3.0_rp+s)-(1.0_rp+s))
          s42= 2.0_rp*((1.0_rp/3.0_rp-s)-(1.0_rp/3.0_rp+s)-(1.0_rp+s))
          t12=-2.0_rp*((1.0_rp-t)+(1.0_rp/3.0_rp-t)-(1.0_rp/3.0_rp+t))
          t22=-2.0_rp*((1.0_rp-t)+(1.0_rp/3.0_rp-t)-(1.0_rp+t)) 
          t32= 2.0_rp*((1.0_rp-t)-(1.0_rp/3.0_rp+t)-(1.0_rp+t))  
          t42= 2.0_rp*((1.0_rp/3.0_rp-t)-(1.0_rp/3.0_rp+t)-(1.0_rp+t))
          z12=-2.0_rp*((1.0_rp-z)+(1.0_rp/3.0_rp-z)-(1.0_rp/3.0_rp+z))
          z22=-2.0_rp*((1.0_rp-z)+(1.0_rp/3.0_rp-z)-(1.0_rp+z)) 
          z32= 2.0_rp*((1.0_rp-z)-(1.0_rp/3.0_rp+z)-(1.0_rp+z))  
          z42= 2.0_rp*((1.0_rp/3.0_rp-z)-(1.0_rp/3.0_rp+z)-(1.0_rp+z))

          heslo(1, 1) =   a*s12*t1*z1
          heslo(2, 1) =   a*s1*t12*z1
          heslo(3, 1) =   a*s1*t1*z12
          heslo(4, 1) =   a*s11*t11*z1
          heslo(5, 1) =   a*s11*t1*z11
          heslo(6, 1) =   a*s1*t11*z11

          heslo(1, 2) =   a*s42*t1*z1  
          heslo(2, 2) =   a*s4*t12*z1 
          heslo(3, 2) =   a*s4*t1*z12 
          heslo(4, 2) =   a*s41*t11*z1
          heslo(5, 2) =   a*s41*t1*z11
          heslo(6, 2) =   a*s4*t11*z11          

          heslo(1, 3) =   a*s42*t4*z1           
          heslo(2, 3) =   a*s4*t42*z1  
          heslo(3, 3) =   a*s4*t4*z12      
          heslo(4, 3) =   a*s41*t41*z1
          heslo(5, 3) =   a*s41*t4*z11
          heslo(6, 3) =   a*s4*t41*z11      

          heslo(1, 4) =   a*s12*t4*z1 
          heslo(2, 4) =   a*s1*t42*z1 
          heslo(3, 4) =   a*s1*t4*z12 
          heslo(4, 4) =   a*s11*t41*z1
          heslo(5, 4) =   a*s11*t4*z11
          heslo(6, 4) =   a*s1*t41*z11

          heslo(1, 5) =   a*s12*t1*z4 
          heslo(2, 5) =   a*s1*t12*z4 
          heslo(3, 5) =   a*s1*t1*z42 
          heslo(4, 5) =   a*s11*t11*z4
          heslo(5, 5) =   a*s11*t1*z41
          heslo(6, 5) =   a*s1*t11*z41

          heslo(1, 6) =   a*s42*t4*z4 
          heslo(2, 6) =   a*s4*t42*z4 
          heslo(3, 6) =   a*s4*t4*z42 
          heslo(4, 6) =   a*s41*t41*z4
          heslo(5, 6) =   a*s41*t4*z41
          heslo(6, 6) =   a*s4*t41*z41

          heslo(1, 7) =   a*s42*t4*z4 
          heslo(2, 7) =   a*s4*t42*z4 
          heslo(3, 7) =   a*s4*t4*z42 
          heslo(4, 7) =   a*s41*t41*z4
          heslo(5, 7) =   a*s41*t4*z41
          heslo(6, 7) =   a*s4*t41*z41

          heslo(1, 8) =   a*s12*t4*z4 
          heslo(2, 8) =   a*s1*t42*z4 
          heslo(3, 8) =   a*s1*t4*z42 
          heslo(4, 8) =   a*s11*t41*z4
          heslo(5, 8) =   a*s11*t4*z41
          heslo(6, 8) =   a*s1*t41*z41

          heslo(1, 9) = 3.0_rp*a*s22*t1*z1 
          heslo(2, 9) = 3.0_rp*a*s2*t12*z1 
          heslo(3, 9) = 3.0_rp*a*s2*t1*z12 
          heslo(4, 9) = 3.0_rp*a*s21*t11*z1
          heslo(5, 9) = 3.0_rp*a*s21*t1*z11
          heslo(6, 9) = 3.0_rp*a*s2*t11*z11

          heslo(1,10) = 3.0_rp*a*s32*t1*z1 
          heslo(2,10) = 3.0_rp*a*s3*t12*z1 
          heslo(3,10) = 3.0_rp*a*s3*t1*z12 
          heslo(4,10) = 3.0_rp*a*s31*t11*z1
          heslo(5,10) = 3.0_rp*a*s31*t1*z11
          heslo(6,10) = 3.0_rp*a*s3*t11*z11

          heslo(1,11) = 3.0_rp*a*s42*t2*z1 
          heslo(2,11) = 3.0_rp*a*s4*t22*z1 
          heslo(3,11) = 3.0_rp*a*s4*t2*z12 
          heslo(4,11) = 3.0_rp*a*s41*t21*z1
          heslo(5,11) = 3.0_rp*a*s41*t2*z11
          heslo(6,11) = 3.0_rp*a*s4*t21*z11

          heslo(1,12) = 3.0_rp*a*s42*t3*z1 
          heslo(2,12) = 3.0_rp*a*s4*t32*z1 
          heslo(3,12) = 3.0_rp*a*s4*t3*z12 
          heslo(4,12) = 3.0_rp*a*s41*t31*z1
          heslo(5,12) = 3.0_rp*a*s41*t3*z11
          heslo(6,12) = 3.0_rp*a*s4*t31*z11

          heslo(1,13) = 3.0_rp*a*s32*t4*z1 
          heslo(2,13) = 3.0_rp*a*s3*t42*z1 
          heslo(3,13) = 3.0_rp*a*s3*t4*z12 
          heslo(4,13) = 3.0_rp*a*s31*t41*z1
          heslo(5,13) = 3.0_rp*a*s31*t4*z11
          heslo(6,13) = 3.0_rp*a*s3*t41*z11

          heslo(1,14) = 3.0_rp*a*s22*t4*z1 
          heslo(2,14) = 3.0_rp*a*s2*t42*z1 
          heslo(3,14) = 3.0_rp*a*s2*t4*z12 
          heslo(4,14) = 3.0_rp*a*s21*t41*z1
          heslo(5,14) = 3.0_rp*a*s21*t4*z11
          heslo(6,14) = 3.0_rp*a*s2*t41*z11

          heslo(1,15) = 3.0_rp*a*s12*t3*z1 
          heslo(2,15) = 3.0_rp*a*s1*t32*z1 
          heslo(3,15) = 3.0_rp*a*s1*t3*z12 
          heslo(4,15) = 3.0_rp*a*s11*t31*z1
          heslo(5,15) = 3.0_rp*a*s11*t3*z11
          heslo(6,15) = 3.0_rp*a*s1*t31*z11

          heslo(1,16) = 3.0_rp*a*s12*t2*z1 
          heslo(2,16) = 3.0_rp*a*s1*t22*z1 
          heslo(3,16) = 3.0_rp*a*s1*t2*z12 
          heslo(4,16) = 3.0_rp*a*s11*t21*z1
          heslo(5,16) = 3.0_rp*a*s11*t2*z11
          heslo(6,16) = 3.0_rp*a*s1*t21*z11

          heslo(1,17) = 3.0_rp*a*s12*t1*z2 
          heslo(2,17) = 3.0_rp*a*s1*t12*z2 
          heslo(3,17) = 3.0_rp*a*s1*t1*z22 
          heslo(4,17) = 3.0_rp*a*s11*t11*z2
          heslo(5,17) = 3.0_rp*a*s11*t1*z21
          heslo(6,17) = 3.0_rp*a*s1*t11*z21

          heslo(1,18) = 3.0_rp*a*s42*t1*z2 
          heslo(2,18) = 3.0_rp*a*s4*t12*z2 
          heslo(3,18) = 3.0_rp*a*s4*t1*z22 
          heslo(4,18) = 3.0_rp*a*s41*t11*z2
          heslo(5,18) = 3.0_rp*a*s41*t1*z21
          heslo(6,18) = 3.0_rp*a*s4*t11*z21

          heslo(1,19) = 3.0_rp*a*s42*t4*z2 
          heslo(2,19) = 3.0_rp*a*s4*t42*z2 
          heslo(3,19) = 3.0_rp*a*s4*t4*z22 
          heslo(4,19) = 3.0_rp*a*s41*t41*z2
          heslo(5,19) = 3.0_rp*a*s41*t4*z21
          heslo(6,19) = 3.0_rp*a*s4*t41*z21

          heslo(1,20) = 3.0_rp*a*s12*t4*z2 
          heslo(2,20) = 3.0_rp*a*s1*t42*z2 
          heslo(3,20) = 3.0_rp*a*s1*t4*z22 
          heslo(4,20) = 3.0_rp*a*s11*t41*z2
          heslo(5,20) = 3.0_rp*a*s11*t4*z21
          heslo(6,20) = 3.0_rp*a*s1*t41*z21

          heslo(1,21) = 3.0_rp*a*s12*t1*z3 
          heslo(2,21) = 3.0_rp*a*s1*t12*z3 
          heslo(3,21) = 3.0_rp*a*s1*t1*z32 
          heslo(4,21) = 3.0_rp*a*s11*t11*z3
          heslo(5,21) = 3.0_rp*a*s11*t1*z31
          heslo(6,21) = 3.0_rp*a*s1*t11*z31

          heslo(1,22) = 3.0_rp*a*s42*t1*z3 
          heslo(2,22) = 3.0_rp*a*s4*t12*z3 
          heslo(3,22) = 3.0_rp*a*s4*t1*z32 
          heslo(4,22) = 3.0_rp*a*s41*t11*z3
          heslo(5,22) = 3.0_rp*a*s41*t1*z31
          heslo(6,22) = 3.0_rp*a*s4*t11*z31

          heslo(1,23) = 3.0_rp*a*s42*t4*z3 
          heslo(2,23) = 3.0_rp*a*s4*t42*z3 
          heslo(3,23) = 3.0_rp*a*s4*t4*z32 
          heslo(4,23) = 3.0_rp*a*s41*t41*z3
          heslo(5,23) = 3.0_rp*a*s41*t4*z31
          heslo(6,23) = 3.0_rp*a*s4*t41*z31

          heslo(1,24) = 3.0_rp*a*s12*t4*z3 
          heslo(2,24) = 3.0_rp*a*s1*t42*z3 
          heslo(3,24) = 3.0_rp*a*s1*t4*z32 
          heslo(4,24) = 3.0_rp*a*s11*t41*z3
          heslo(5,24) = 3.0_rp*a*s11*t4*z31
          heslo(6,24) = 3.0_rp*a*s1*t41*z31

          heslo(1,25) = 3.0_rp*a*s22*t4*z4 
          heslo(2,25) = 3.0_rp*a*s2*t42*z4 
          heslo(3,25) = 3.0_rp*a*s2*t4*z42 
          heslo(4,25) = 3.0_rp*a*s21*t41*z4
          heslo(5,25) = 3.0_rp*a*s21*t4*z41
          heslo(6,25) = 3.0_rp*a*s2*t41*z41

          heslo(1,26) = 3.0_rp*a*s32*t1*z4 
          heslo(2,26) = 3.0_rp*a*s3*t12*z4 
          heslo(3,26) = 3.0_rp*a*s3*t1*z42 
          heslo(4,26) = 3.0_rp*a*s31*t11*z4
          heslo(5,26) = 3.0_rp*a*s31*t1*z41
          heslo(6,26) = 3.0_rp*a*s3*t11*z41

          heslo(1,27) = 3.0_rp*a*s42*t2*z4 
          heslo(2,27) = 3.0_rp*a*s4*t22*z4 
          heslo(3,27) = 3.0_rp*a*s4*t2*z42 
          heslo(4,27) = 3.0_rp*a*s41*t21*z4
          heslo(5,27) = 3.0_rp*a*s41*t2*z41
          heslo(6,27) = 3.0_rp*a*s4*t21*z41

          heslo(1,28) = 3.0_rp*a*s42*t3*z4 
          heslo(2,28) = 3.0_rp*a*s4*t32*z4 
          heslo(3,28) = 3.0_rp*a*s4*t3*z42 
          heslo(4,28) = 3.0_rp*a*s41*t31*z4
          heslo(5,28) = 3.0_rp*a*s41*t3*z41
          heslo(6,28) = 3.0_rp*a*s4*t31*z41

          heslo(1,29) = 3.0_rp*a*s32*t4*z4 
          heslo(2,29) = 3.0_rp*a*s3*t42*z4 
          heslo(3,29) = 3.0_rp*a*s3*t4*z42 
          heslo(4,29) = 3.0_rp*a*s31*t41*z4
          heslo(5,29) = 3.0_rp*a*s31*t4*z41
          heslo(6,29) = 3.0_rp*a*s3*t41*z41

          heslo(1,30) = 3.0_rp*a*s22*t4*z4 
          heslo(2,30) = 3.0_rp*a*s2*t42*z4 
          heslo(3,30) = 3.0_rp*a*s2*t4*z42 
          heslo(4,30) = 3.0_rp*a*s21*t41*z4
          heslo(5,30) = 3.0_rp*a*s21*t4*z41
          heslo(6,30) = 3.0_rp*a*s2*t41*z31

          heslo(1,31) = 3.0_rp*a*s12*t3*z4 
          heslo(2,31) = 3.0_rp*a*s1*t32*z4 
          heslo(3,31) = 3.0_rp*a*s1*t3*z42 
          heslo(4,31) = 3.0_rp*a*s11*t31*z4
          heslo(5,31) = 3.0_rp*a*s11*t3*z41
          heslo(6,31) = 3.0_rp*a*s1*t31*z41

          heslo(1,32) = 3.0_rp*a*s12*t2*z4 
          heslo(2,32) = 3.0_rp*a*s1*t22*z4 
          heslo(3,32) = 3.0_rp*a*s1*t2*z42 
          heslo(4,32) = 3.0_rp*a*s11*t21*z4
          heslo(5,32) = 3.0_rp*a*s11*t2*z41
          heslo(6,32) = 3.0_rp*a*s1*t21*z41

          heslo(1,33) =-9.0_rp*a*s22*t2*z1 
          heslo(2,33) =-9.0_rp*a*s2*t22*z1 
          heslo(3,33) =-9.0_rp*a*s2*t2*z12 
          heslo(4,33) =-9.0_rp*a*s21*t21*z1
          heslo(5,33) =-9.0_rp*a*s21*t2*z11
          heslo(6,33) =-9.0_rp*a*s2*t21*z11

          heslo(1,34) =-9.0_rp*a*s32*t2*z1 
          heslo(2,34) =-9.0_rp*a*s3*t22*z1 
          heslo(3,34) =-9.0_rp*a*s3*t2*z12 
          heslo(4,34) =-9.0_rp*a*s31*t21*z1
          heslo(5,34) =-9.0_rp*a*s31*t2*z11
          heslo(6,34) =-9.0_rp*a*s3*t21*z11

          heslo(1,35) =-9.0_rp*a*s32*t3*z1 
          heslo(2,35) =-9.0_rp*a*s3*t32*z1 
          heslo(3,35) =-9.0_rp*a*s3*t3*z12 
          heslo(4,35) =-9.0_rp*a*s31*t31*z1
          heslo(5,35) =-9.0_rp*a*s31*t3*z11
          heslo(6,35) =-9.0_rp*a*s3*t31*z11

          heslo(1,36) =-9.0_rp*a*s22*t3*z1 
          heslo(2,36) =-9.0_rp*a*s2*t32*z1 
          heslo(3,36) =-9.0_rp*a*s2*t3*z12 
          heslo(4,36) =-9.0_rp*a*s21*t31*z1
          heslo(5,36) =-9.0_rp*a*s21*t3*z11
          heslo(6,36) =-9.0_rp*a*s2*t31*z11

          heslo(1,37) =-9.0_rp*a*s22*t1*z2 
          heslo(2,37) =-9.0_rp*a*s2*t12*z2 
          heslo(3,37) =-9.0_rp*a*s2*t1*z22 
          heslo(4,37) =-9.0_rp*a*s21*t11*z2
          heslo(5,37) =-9.0_rp*a*s21*t1*z21
          heslo(6,37) =-9.0_rp*a*s2*t11*z21

          heslo(1,38) =-9.0_rp*a*s32*t1*z2 
          heslo(2,38) =-9.0_rp*a*s3*t12*z2 
          heslo(3,38) =-9.0_rp*a*s3*t1*z22 
          heslo(4,38) =-9.0_rp*a*s31*t11*z2
          heslo(5,38) =-9.0_rp*a*s31*t1*z21
          heslo(6,38) =-9.0_rp*a*s3*t11*z21

          heslo(1,39) =-9.0_rp*a*s42*t2*z2 
          heslo(2,39) =-9.0_rp*a*s4*t22*z2 
          heslo(3,39) =-9.0_rp*a*s4*t2*z22 
          heslo(4,39) =-9.0_rp*a*s41*t21*z2
          heslo(5,39) =-9.0_rp*a*s41*t2*z21
          heslo(6,39) =-9.0_rp*a*s4*t21*z21

          heslo(1,40) =-9.0_rp*a*s42*t3*z2 
          heslo(2,40) =-9.0_rp*a*s4*t32*z2 
          heslo(3,40) =-9.0_rp*a*s4*t3*z22 
          heslo(4,40) =-9.0_rp*a*s41*t31*z2
          heslo(5,40) =-9.0_rp*a*s41*t3*z21
          heslo(6,40) =-9.0_rp*a*s4*t31*z21

          heslo(1,41) =-9.0_rp*a*s32*t4*z2 
          heslo(2,41) =-9.0_rp*a*s3*t42*z2 
          heslo(3,41) =-9.0_rp*a*s3*t4*z22 
          heslo(4,41) =-9.0_rp*a*s31*t41*z2
          heslo(5,41) =-9.0_rp*a*s31*t4*z21
          heslo(6,41) =-9.0_rp*a*s3*t41*z21

          heslo(1,42) =-9.0_rp*a*s22*t4*z2 
          heslo(2,42) =-9.0_rp*a*s2*t42*z2 
          heslo(3,42) =-9.0_rp*a*s2*t4*z22 
          heslo(4,42) =-9.0_rp*a*s21*t41*z2
          heslo(5,42) =-9.0_rp*a*s21*t4*z21
          heslo(6,42) =-9.0_rp*a*s2*t41*z21

          heslo(1,43) =-9.0_rp*a*s12*t3*z2 
          heslo(2,43) =-9.0_rp*a*s1*t32*z2 
          heslo(3,43) =-9.0_rp*a*s1*t3*z22 
          heslo(4,43) =-9.0_rp*a*s11*t31*z2
          heslo(5,43) =-9.0_rp*a*s11*t3*z21
          heslo(6,43) =-9.0_rp*a*s1*t31*z21

          heslo(1,44) =-9.0_rp*a*s12*t2*z2 
          heslo(2,44) =-9.0_rp*a*s1*t22*z2 
          heslo(3,44) =-9.0_rp*a*s1*t2*z22 
          heslo(4,44) =-9.0_rp*a*s11*t21*z2
          heslo(5,44) =-9.0_rp*a*s11*t2*z21
          heslo(6,44) =-9.0_rp*a*s1*t21*z21

          heslo(1,45) =-9.0_rp*a*s22*t1*z3 
          heslo(2,45) =-9.0_rp*a*s2*t12*z3 
          heslo(3,45) =-9.0_rp*a*s2*t1*z32 
          heslo(4,45) =-9.0_rp*a*s21*t11*z3
          heslo(5,45) =-9.0_rp*a*s21*t1*z31
          heslo(6,45) =-9.0_rp*a*s2*t11*z31

          heslo(1,46) =-9.0_rp*a*s32*t1*z3 
          heslo(2,46) =-9.0_rp*a*s3*t12*z3 
          heslo(3,46) =-9.0_rp*a*s3*t1*z32 
          heslo(4,46) =-9.0_rp*a*s31*t11*z3
          heslo(5,46) =-9.0_rp*a*s31*t1*z31
          heslo(6,46) =-9.0_rp*a*s3*t11*z31

          heslo(1,47) =-9.0_rp*a*s42*t2*z3 
          heslo(2,47) =-9.0_rp*a*s4*t22*z3 
          heslo(3,47) =-9.0_rp*a*s4*t2*z32 
          heslo(4,47) =-9.0_rp*a*s41*t21*z3
          heslo(5,47) =-9.0_rp*a*s41*t2*z31
          heslo(6,47) =-9.0_rp*a*s4*t21*z31

          heslo(1,48) =-9.0_rp*a*s42*t3*z3 
          heslo(2,48) =-9.0_rp*a*s4*t32*z3 
          heslo(3,48) =-9.0_rp*a*s4*t3*z32 
          heslo(4,48) =-9.0_rp*a*s41*t31*z3
          heslo(5,48) =-9.0_rp*a*s41*t3*z31
          heslo(6,48) =-9.0_rp*a*s4*t31*z31

          heslo(1,49) =-9.0_rp*a*s32*t4*z3 
          heslo(2,49) =-9.0_rp*a*s3*t42*z3 
          heslo(3,49) =-9.0_rp*a*s3*t4*z32 
          heslo(4,49) =-9.0_rp*a*s31*t41*z3
          heslo(5,49) =-9.0_rp*a*s31*t4*z31
          heslo(6,49) =-9.0_rp*a*s3*t41*z31

          heslo(1,50) =-9.0_rp*a*s22*t4*z3 
          heslo(2,50) =-9.0_rp*a*s2*t42*z3 
          heslo(3,50) =-9.0_rp*a*s2*t4*z32 
          heslo(4,50) =-9.0_rp*a*s21*t41*z3
          heslo(5,50) =-9.0_rp*a*s21*t4*z31
          heslo(6,50) =-9.0_rp*a*s2*t41*z31

          heslo(1,51) =-9.0_rp*a*s12*t3*z3 
          heslo(2,51) =-9.0_rp*a*s1*t32*z3 
          heslo(3,51) =-9.0_rp*a*s1*t3*z32 
          heslo(4,51) =-9.0_rp*a*s11*t31*z3
          heslo(5,51) =-9.0_rp*a*s11*t3*z31
          heslo(6,51) =-9.0_rp*a*s1*t31*z31

          heslo(1,52) =-9.0_rp*a*s12*t2*z3 
          heslo(2,52) =-9.0_rp*a*s1*t22*z3 
          heslo(3,52) =-9.0_rp*a*s1*t2*z32 
          heslo(4,52) =-9.0_rp*a*s11*t21*z3
          heslo(5,52) =-9.0_rp*a*s11*t2*z31
          heslo(6,52) =-9.0_rp*a*s1*t21*z31

          heslo(1,53) =-9.0_rp*a*s22*t2*z4 
          heslo(2,53) =-9.0_rp*a*s2*t22*z4 
          heslo(3,53) =-9.0_rp*a*s2*t2*z42 
          heslo(4,53) =-9.0_rp*a*s21*t21*z4
          heslo(5,53) =-9.0_rp*a*s21*t2*z41
          heslo(6,53) =-9.0_rp*a*s2*t21*z41

          heslo(1,54) =-9.0_rp*a*s32*t2*z4 
          heslo(2,54) =-9.0_rp*a*s3*t22*z4 
          heslo(3,54) =-9.0_rp*a*s3*t2*z42 
          heslo(4,54) =-9.0_rp*a*s31*t21*z4
          heslo(5,54) =-9.0_rp*a*s31*t2*z41
          heslo(6,54) =-9.0_rp*a*s3*t21*z41

          heslo(1,55) =-9.0_rp*a*s32*t3*z4 
          heslo(2,55) =-9.0_rp*a*s3*t32*z4 
          heslo(3,55) =-9.0_rp*a*s3*t3*z42 
          heslo(4,55) =-9.0_rp*a*s31*t31*z4
          heslo(5,55) =-9.0_rp*a*s31*t3*z41
          heslo(6,55) =-9.0_rp*a*s3*t31*z41

          heslo(1,56) =-9.0_rp*a*s22*t3*z4 
          heslo(2,56) =-9.0_rp*a*s2*t32*z4 
          heslo(3,56) =-9.0_rp*a*s2*t3*z42 
          heslo(4,56) =-9.0_rp*a*s21*t31*z4
          heslo(5,56) =-9.0_rp*a*s21*t3*z41
          heslo(6,56) =-9.0_rp*a*s2*t31*z41

          heslo(1,57) = 27.0_rp*a*s22*t2*z2 
          heslo(2,57) = 27.0_rp*a*s2*t22*z2 
          heslo(3,57) = 27.0_rp*a*s2*t2*z22 
          heslo(4,57) = 27.0_rp*a*s21*t21*z2
          heslo(5,57) = 27.0_rp*a*s21*t2*z21
          heslo(6,57) = 27.0_rp*a*s2*t21*z21

          heslo(1,58) = 27.0_rp*a*s32*t2*z2 
          heslo(2,58) = 27.0_rp*a*s3*t22*z2 
          heslo(3,58) = 27.0_rp*a*s3*t2*z22 
          heslo(4,58) = 27.0_rp*a*s31*t21*z2
          heslo(5,58) = 27.0_rp*a*s31*t2*z21
          heslo(6,58) = 27.0_rp*a*s3*t21*z21

          heslo(1,59) = 27.0_rp*a*s32*t3*z2 
          heslo(2,59) = 27.0_rp*a*s3*t32*z2 
          heslo(3,59) = 27.0_rp*a*s3*t3*z22 
          heslo(4,59) = 27.0_rp*a*s31*t31*z2
          heslo(5,59) = 27.0_rp*a*s31*t3*z21
          heslo(6,59) = 27.0_rp*a*s3*t31*z21

          heslo(1,60) = 27.0_rp*a*s22*t3*z2 
          heslo(2,60) = 27.0_rp*a*s2*t32*z2 
          heslo(3,60) = 27.0_rp*a*s2*t3*z22 
          heslo(4,60) = 27.0_rp*a*s21*t31*z2
          heslo(5,60) = 27.0_rp*a*s21*t3*z21
          heslo(6,60) = 27.0_rp*a*s2*t31*z21

          heslo(1,61) = 27.0_rp*a*s22*t2*z3 
          heslo(2,61) = 27.0_rp*a*s2*t22*z3 
          heslo(3,61) = 27.0_rp*a*s2*t2*z32 
          heslo(4,61) = 27.0_rp*a*s21*t21*z3
          heslo(5,61) = 27.0_rp*a*s21*t2*z31
          heslo(6,61) = 27.0_rp*a*s2*t21*z31

          heslo(1,62) = 27.0_rp*a*s32*t2*z3 
          heslo(2,62) = 27.0_rp*a*s3*t22*z3 
          heslo(3,62) = 27.0_rp*a*s3*t2*z32 
          heslo(4,62) = 27.0_rp*a*s31*t21*z3
          heslo(5,62) = 27.0_rp*a*s31*t2*z31
          heslo(6,62) = 27.0_rp*a*s3*t21*z31

          heslo(1,63) = 27.0_rp*a*s32*t3*z3 
          heslo(2,63) = 27.0_rp*a*s3*t32*z3 
          heslo(3,63) = 27.0_rp*a*s3*t3*z32 
          heslo(4,63) = 27.0_rp*a*s31*t31*z3
          heslo(5,63) = 27.0_rp*a*s31*t3*z31
          heslo(6,63) = 27.0_rp*a*s3*t31*z31

          heslo(1,64) = 27.0_rp*a*s22*t3*z3 
          heslo(2,64) = 27.0_rp*a*s2*t32*z3 
          heslo(3,64) = 27.0_rp*a*s2*t3*z32 
          heslo(4,64) = 27.0_rp*a*s21*t31*z3
          heslo(5,64) = 27.0_rp*a*s21*t3*z31
          heslo(6,64) = 27.0_rp*a*s2*t31*z31
       end if

    else if( nnode == 3 ) then
       !
       ! SHELL
       !     
       shapf(1) =1.0_rp-s-t                                
       shapf(2)   = s                                  
       shapf(3)   = t                                        !  3
       deriv(1,1) =-1.0_rp                                   !   
       deriv(1,2) = 1.0_rp                                   !
       deriv(1,3) = 0.0_rp                                   !
       deriv(2,1) =-1.0_rp                                   !  1       2
       deriv(2,2) = 0.0_rp
       deriv(2,3) = 1.0_rp

    else
       call runend('SHAPE3: NOT AVAILABLE ELEMENT INERPOLATION')
    end if

  end subroutine elmgeo_shape3


  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates
  !> @details Evaluate the natural coordinate of a point in an element
  !>          or in a boundary element.
  !>          - Point COLOC(3) is in an element
  !>          - Point COLOC(3) is in a boundary element. In this case it
  !>            it should be on the curve
  !>          INPUT
  !>             NDIME ... Dimension
  !>             PTOPO ... Element topology
  !>                       -1 Bar
  !>                        0 Quadrilateral  Hexahedra
  !>                        1 Triangle       Tetrahedra
  !>                        2    -           Pentahedra (wedge-shaped)
  !>                        3    -           Pyramid
  !>             PNODE ... Number of element nodes
  !>             ELCOD ... Element node coordinates
  !>             COGLO ... Global coordinates of test point
  !>             LMINI ... Minimum local coordinate (0.0)
  !>             LMAXI ... Maximum local coordinate (1.0)
  !>          OUTPUT
  !>             IFOUN ... 1 if point is in element
  !>                       0 otherwise
  !>             COLOC ... Local coordinates of test point
  !>             SHAPF ... Shape function of test point in element
  !>             DERIV ... Shape function derivatives of test point in element
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_natural_coordinates( &
       ndime,pelty,pnode,elcod,shapf,    &
       deriv,coglo,coloc,ifoun,toler)
    integer(ip), intent(in)           :: ndime              !< Problem dimension
    integer(ip), intent(in)           :: pelty              !< Element type
    integer(ip), intent(in)           :: pnode              !< Element number of nodes
    real(rp),    intent(in)           :: coglo(ndime)       !< Test point coordinates
    real(rp),    intent(in)           :: elcod(ndime,pnode) !< Element coordinates
    integer(ip), intent(out)          :: ifoun              !< Test point is in/out element
    real(rp),    intent(out)          :: coloc(3)           !< Local coordinates of test point in element
    real(rp),    intent(out)          :: deriv(ndime,pnode) !< Shape funcitons derivatives of test point 
    real(rp),    intent(out)          :: shapf(pnode)       !< Shape functions of test point  
    real(rp),    intent(in), optional :: toler              !< Tolerance rto decide it's in/ it's out
    integer(ip)                       :: i,j,k
    real(rp)                          :: ezzzt
    real(rp)                          :: epsil,lmini,lmaxi
    !
    ! Define tolerance
    !
    if( present(toler) ) then
       epsil = toler
    else
       epsil = epsilon(1.0_rp)
    end if
    lmini = - epsil
    lmaxi = 1.0_rp + epsil
    ifoun = 0

    if( elmgeo_inside_element_bounding_box(ndime,pnode,elcod,coglo,epsil) ) then
       !
       ! Point COGLO is in element bounding box
       !
       if( pelty < 0 ) then
          !
          ! Hole element
          !
          call runend('ELMGEO_NATURAL_COORDIANTES: DO NOT KNOW WHAT TO DO')

       else if( pelty == BAR02 ) then
          !
          ! BAR02
          !
          call elmgeo_inside_BAR02(&
               pnode,lmaxi,elcod,coglo,coloc,shapf,deriv,ifoun)

       else if( ( pelty == TRI03 .or. pelty == QUA04 ) .and. ndime == 2 ) then
          !
          ! TRI03 and QUA04 
          !
          call elmgeo_inside_TRI03_QUA04(&
               pnode,lmini,lmaxi,elcod,coglo,coloc,shapf,deriv,ifoun)

       else if( pelty == TET04 ) then
          !
          ! TET04
          !
          call elmgeo_inside_TET04(&
               lmini,lmaxi,elcod,coglo,coloc,&
               shapf,deriv,ifoun)
       else
          !
          ! Compute local from global coordinates
          !
          if( elmgeo_inside_element_using_faces(pelty,elcod,coglo) ) then

             call elmgeo_newrap(coglo,coloc,ndime,pnode,elcod,shapf,deriv) 
             ifoun = 1

          else

             ifoun = 0
             return

          end if

       end if

    end if

  end subroutine elmgeo_natural_coordinates

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates
  !> @details Check if point with global coordinates (x,y,z)=COGLO is inside
  !>          a TET04 element
  !>
  !>          +-                    -+ +-  -+   +-    -+
  !>          | -x1+x2 -x1+x3 -x1+x4 | | s1 |   | x-x1 |
  !>          | -y1+y2 -y1+y3 -y1+y3 | | s2 | = | y-y1 | 
  !>          | -z1+z2 -z1+z3 -z1+z4 | | s3 |   | z-z1 |
  !>          +-                    -+ +-  -+   +-    -+
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_inside_TET04(&
       lmini,lmaxi,elcod,coglo,coloc,&
       shapf,deriv,ifoun)
    real(rp),    intent(in)  :: lmini
    real(rp),    intent(in)  :: lmaxi
    real(rp),    intent(in)  :: elcod(3,4)
    real(rp),    intent(in)  :: coglo(3)
    integer(ip), intent(out) :: ifoun
    real(rp),    intent(out) :: coloc(3)
    real(rp),    intent(out) :: shapf(4)
    real(rp),    intent(out) :: deriv(3,4)
    integer(ip)              :: idime,jdime
    real(rp)                 :: deter,xjaci(3,3),xjacm(3,3)
    real(rp)                 :: rhsid(3),ezzzt,denom,t1,t2,t3

    ifoun      = 0
    xjacm(1,1) = -elcod(1,1) + elcod(1,2)
    xjacm(2,1) = -elcod(2,1) + elcod(2,2)
    xjacm(3,1) = -elcod(3,1) + elcod(3,2)
    xjacm(1,2) = -elcod(1,1) + elcod(1,3)
    xjacm(2,2) = -elcod(2,1) + elcod(2,3)
    xjacm(3,2) = -elcod(3,1) + elcod(3,3)
    xjacm(1,3) = -elcod(1,1) + elcod(1,4)
    xjacm(2,3) = -elcod(2,1) + elcod(2,4) 
    xjacm(3,3) = -elcod(3,1) + elcod(3,4) 

    t1        =  xjacm(2,2) * xjacm(3,3) - xjacm(3,2) * xjacm(2,3)
    t2        = -xjacm(2,1) * xjacm(3,3) + xjacm(3,1) * xjacm(2,3)
    t3        =  xjacm(2,1) * xjacm(3,2) - xjacm(3,1) * xjacm(2,2)
    deter     =  xjacm(1,1) * t1 + xjacm(1,2) * t2 + xjacm(1,3) * t3

    if( abs(deter) > epsilon(1.0_rp) ) then
       denom       = 1.0_rp / deter
       xjaci(1,1) =  t1 * denom
       xjaci(2,1) =  t2 * denom
       xjaci(3,1) =  t3 * denom
       xjaci(2,2) =  ( xjacm(1,1) * xjacm(3,3) - xjacm(3,1) * xjacm(1,3)) * denom
       xjaci(3,2) =  (-xjacm(1,1) * xjacm(3,2) + xjacm(1,2) * xjacm(3,1)) * denom
       xjaci(3,3) =  ( xjacm(1,1) * xjacm(2,2) - xjacm(2,1) * xjacm(1,2)) * denom
       xjaci(1,2) =  (-xjacm(1,2) * xjacm(3,3) + xjacm(3,2) * xjacm(1,3)) * denom
       xjaci(1,3) =  ( xjacm(1,2) * xjacm(2,3) - xjacm(2,2) * xjacm(1,3)) * denom
       xjaci(2,3) =  (-xjacm(1,1) * xjacm(2,3) + xjacm(2,1) * xjacm(1,3)) * denom

       rhsid(1)    =  coglo(1) - elcod(1,1)
       rhsid(2)    =  coglo(2) - elcod(2,1)
       rhsid(3)    =  coglo(3) - elcod(3,1)
       coloc(1)    =  xjaci(1,1) * rhsid(1) + xjaci(1,2) * rhsid(2) + xjaci(1,3) * rhsid(3) 
       coloc(2)    =  xjaci(2,1) * rhsid(1) + xjaci(2,2) * rhsid(2) + xjaci(2,3) * rhsid(3) 
       coloc(3)    =  xjaci(3,1) * rhsid(1) + xjaci(3,2) * rhsid(2) + xjaci(3,3) * rhsid(3) 

       if( ( coloc(1) >= lmini ) .and. ( coloc(1) <= lmaxi ) ) then
          if( ( coloc(2) >= lmini ) .and. ( coloc(2) <= lmaxi ) ) then
             if( ( coloc(3) >= lmini ) .and. ( coloc(3) <= lmaxi ) ) then
                ezzzt = 1.0_rp - coloc(1) - coloc(2) - coloc(3)
                if( ( ezzzt >= lmini ) .and. ( ezzzt <= lmaxi ) ) then
                   call elmgeo_shapf_deriv_heslo(3_ip,4_ip,coloc,shapf,deriv)
                   ifoun = 1                   
                end if
             end if
          end if
       end if

    end if

  end subroutine elmgeo_inside_TET04

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates in BAR02 lement
  !> @details Check if a point x is inside a BAR02 element
  !>
  !>          x1      x2
  !>          o-------o
  !>                       (x-x1)
  !>          s = -1 + 2 * ------
  !>                       (x2-x1)
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_inside_BAR02(&
       pnode,lmaxi,elcod,coglo,coloc,shapf,deriv,ifoun)

    integer(ip), intent(in)  :: pnode
    real(rp),    intent(in)  :: lmaxi
    real(rp),    intent(in)  :: elcod(1,pnode)
    real(rp),    intent(in)  :: coglo(1)
    integer(ip), intent(out) :: ifoun
    real(rp),    intent(out) :: coloc(*)
    real(rp),    intent(out) :: shapf(pnode)
    real(rp),    intent(out) :: deriv(1,pnode)
    real(rp)                 :: deter,colo3,x2x1,y2y1,x3x1,y3y1,xx1,yy1

    coloc(1) = -1.0_rp + 2.0_rp * (coglo(1)-elcod(1,1))/(elcod(1,2)-elcod(1,1))

    if( coloc(1) >= -lmaxi .and. coloc(1) <= lmaxi ) then
       ifoun      =  1
       shapf(1)   =  0.5_rp*(1.0_rp-coloc(1))
       shapf(2)   =  0.5_rp*(1.0_rp+coloc(1))
       deriv(1,1) = -0.5_rp
       deriv(1,2) =  0.5_rp
    else
       ifoun      =  0
    end if


  end subroutine elmgeo_inside_BAR02

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates
  !> @details Check if point with global coordinates (x,y)=COGLO is inside
  !>          a triangle P1 or a quadrilateral Q1. The Q1 element is
  !>          divided into two P1 elements. Returns the local coordinates
  !>          (s,t)=COLOC
  !>
  !>          For P1 triangles we have:
  !>          x = (1-s-t)*x1 + s*x2 + t*x3
  !>          y = (1-s-t)*y1 + s*y2 + t*y3
  !>      
  !>          This linear problem is solved for (s,t):
  !>               (x3-x1)(y-y1) -(y3-y1)(x-x1)
  !>          s =  ----------------------------
  !>               (x3-x1)(y2-y1)-(y3-y1)(x2-x1)
  !>      
  !>               (x-x1)(y2-y1) -(y-y1)(x2-x1)
  !>          t =  ----------------------------
  !>               (x3-x1)(y2-y1)-(y3-y1)(x2-x1)
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_inside_TRI03_QUA04(&
       pnode,lmini,lmaxi,elcod,coglo,coloc,&
       shapf,deriv,ifoun)
    integer(ip), intent(in)  :: pnode
    real(rp),    intent(in)  :: lmini
    real(rp),    intent(in)  :: lmaxi
    real(rp),    intent(in)  :: elcod(2,pnode)
    real(rp),    intent(in)  :: coglo(3)
    integer(ip), intent(out) :: ifoun
    real(rp),    intent(out) :: coloc(*)
    real(rp),    intent(out) :: shapf(pnode)
    real(rp),    intent(out) :: deriv(2,pnode)
    real(rp)                 :: deter,colo3,x2x1,y2y1,x3x1,y3y1,xx1,yy1

    ifoun = 0
    !
    ! P1 and Q1: Check if point is in first triangle: nodes 1-2-3
    !
    x2x1     = elcod(1,2) - elcod(1,1)
    y2y1     = elcod(2,2) - elcod(2,1)
    x3x1     = elcod(1,3) - elcod(1,1)
    y3y1     = elcod(2,3) - elcod(2,1)
    xx1      = coglo(1)   - elcod(1,1)
    yy1      = coglo(2)   - elcod(2,1)
    deter    = 1.0_rp / (x3x1*y2y1-y3y1*x2x1)
    coloc(1) = deter  * (x3x1*yy1 -y3y1*xx1)
    if( coloc(1) >= lmini .and. coloc(1) <= lmaxi ) then
       coloc(2) = deter * (y2y1*xx1-x2x1*yy1)
       if( coloc(2) >= lmini .and. coloc(1) + coloc(2) <= lmaxi ) then
          ifoun = 1
       end if
    end if

    if( pnode == 4 ) then
       !
       ! Q1: Check if point is in second triangle: nodes 1-3-4
       !
       if( ifoun == 0 ) then
          x2x1     = elcod(1,3)-elcod(1,1)
          y2y1     = elcod(2,3)-elcod(2,1)
          x3x1     = elcod(1,4)-elcod(1,1)
          y3y1     = elcod(2,4)-elcod(2,1)
          xx1      = coglo(1)  -elcod(1,1)
          yy1      = coglo(2)  -elcod(2,1)
          deter    = 1.0_rp / (x3x1*y2y1-y3y1*x2x1)
          coloc(1) = deter  * (x3x1*yy1 -y3y1*xx1)
          if( coloc(1) >= lmini .and. coloc(1) <= lmaxi ) then
             coloc(2) = deter * (y2y1*xx1-x2x1*yy1)
             if( coloc(2) >= lmini .and. coloc(1) + coloc(2) <= lmaxi ) then
                ifoun = 1
             end if
          end if
       end if
       if( ifoun == 1 ) then
          call elmgeo_newrap(&
               coglo,coloc,2_ip,4_ip,elcod,shapf,deriv)
       end if

    else if( pnode == 3 .and. ifoun == 1 ) then
       !
       ! P1: Compute shape function and derivatives
       !
       shapf(1)   =  1.0_rp - coloc(1) - coloc(2)
       shapf(2)   =  coloc(1)
       shapf(3)   =  coloc(2)
       deriv(1,1) = -1.0_rp
       deriv(1,2) =  1.0_rp
       deriv(1,3) =  0.0_rp
       deriv(2,1) = -1.0_rp
       deriv(2,2) =  0.0_rp
       deriv(2,3) =  1.0_rp

    end if

  end subroutine elmgeo_inside_TRI03_QUA04

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates
  !> @details Calculate the inverse transformation (x,y,z)-->(s,t,r)
  !>        
  !>          \verbatim
  !>
  !>          Solve the following system:
  !>
  !>          sum_k Nk(s,t) xk - x = 0 => f1(s,t) = 0
  !>          sum_k Nk(s,t) yk - y = 0 => f2(s,t) = 0
  !>     
  !>          Apply Newton-Raphson:
  !>
  !>          +-               -+   +-           -+   +-             -+ +-  -+
  !>          | f1(s^i+1,t^i+1) |   | f1(s^i,t^i) |   | df1/ds df1/dt | | ds |
  !>          |                 | = |             | + |               | |    |
  !>          | f2(s^i+1,t^i+1) |   | f2(s^i,t^i) |   | df2/ds df2/dt | | dt |
  !>          +-               -+   +-           -+   +-             -+ +-  -+
  !>                                     DELTX              XJACM       DELTS
  !>
  !>          => DELTS = - XJACM^-1 . DELTX, with
  !>
  !>          df1/ds      = sum_k dNk(s,t)/ds xk
  !>          df1/dt      = sum_k dNk(s,t)/dt xk
  !>          df2/ds      = sum_k dNk(s,t)/ds yk
  !>          df2/dt      = sum_k dNk(s,t)/dt yk
  !>
  !>          s^i+1       = s^i + DELTS(1)
  !>          t^i+1       = t^i + DELTS(2)
  !>
  !>          f1(s^i,t^i) = sum_k Nk(s,t) xk - x = DELTX(1)
  !>          f2(s^i,t^i) = sum_k Nk(s,t) yk - y = DELTX(2)
  !>
  !>          \endverbatim
  !
  !------------------------------------------------------------------------

  subroutine elmgeo_newrap(&
       coglo,coloc,ndime,pnode,elcod,shapf,deriv,what)
    use def_master
    implicit none
    real(rp),     intent(in)            :: coglo(*)
    real(rp),     intent(out)           :: coloc(*)
    integer(ip),  intent(in)            :: ndime
    integer(ip),  intent(in)            :: pnode
    real(rp),     intent(in)            :: elcod(ndime,pnode)
    real(rp),     intent(out)           :: shapf(pnode)
    real(rp),     intent(out)           :: deriv(ndime,pnode)
    character(*), intent(in), optional  :: what
    integer(ip)                         :: inode,iiter,jdime,maxit,idime
    integer(ip)                         :: ntens,jnode,kdime,ierro
    integer(ip)                         :: permu(3)
    real(rp)                            :: xjacm(ndime,ndime)
    real(rp)                            :: xjaci(ndime,ndime)
    real(rp)                            :: deltx(3),delts(3),detja,dummr,rhuge,toler,zerorr
    real(rp)                            :: diame(3),xnorm(3),diam2(3)
    !
    ! Initial condition s^0 = (0,0,0) or given by user
    !
    if( .not. present(what) ) coloc(1:ndime) = 0.0_rp
    !
    ! Iterate for ds^i+1 = - (df/ds)^-1 . f(s^i)
    !
    toler = 1.0e-08_rp
    rhuge = huge(1.0_rp)
    iiter = 0
    maxit = 200
    delts = 1.0_rp

    nr_iterations: do while( maxval(abs(delts(1:ndime))) > toler .and. iiter < maxit )
       !
       ! Iteration counter
       !
       iiter = iiter + 1
       !
       ! Compute f^i = sum_k Nk(s^i) xk - x = DELTX 
       !
       call elmgeo_shapf_deriv_heslo(ndime,pnode,coloc,shapf,deriv)
       deltx(1:ndime) = 0.0_rp
       do inode = 1,pnode
          deltx(1:ndime) = deltx(1:ndime) + shapf(inode) * elcod(1:ndime,inode)
       end do
       deltx(1:ndime) = deltx(1:ndime) - coglo(1:ndime)
       !
       !             +-             -+   +-                                   -+
       !             | df1/ds df1/dt |   | (sum_k DNk/ds xk) (sum_k DNk/dt xk) |
       ! Compute J = |               | = |                                     |
       !             | df2/ds df2/dt |   | (sum_k DNk/ds yk) (sum_k DNk/dt yk) |
       !             +-             -+   +-                                   -+
       !
       xjacm = 0.0_rp
       do inode = 1,pnode
          do jdime = 1,ndime
             do idime = 1,ndime
                xjacm(idime,jdime) = xjacm(idime,jdime) + deriv(jdime,inode) * elcod(idime,inode)
             end do
          end do
       end do
       !
       ! Compute J^{-1}
       !
       call invmtx(xjacm,xjaci,detja,ndime)
       !
       ! If jacobian is singlar, use the descent method instead with a small relaxation
       ! factor a. This can happen for example near the apex of a pyramid
       ! 
       ! Non-singular: ds^i+1 = -J^-1.f(s^i)
       ! Singular:     ds^i+1 = - a*I.f(s^i)
       !
       if( abs(detja) < epsil ) then
          xjaci = 0.0_rp
          do idime = 1,ndime
             xjaci(idime,idime) = 0.1_rp
          end do
          !print*,'detja=',detja
          !print*,'coloc=',coloc(1:ndime)
          !do idime = 1,ndime
          !   print*,'xjacm=',xjacm(idime,1:ndime)
          !end do
          !call runend('ELMGEO_NEWRAP: WE ARE IN TROUBLE, ZERO JACOBIAN')
       end if
       !
       ! Compute ds = -J^{-1}.dx
       !     
       delts = 0.0_rp
       do jdime = 1,ndime
          do idime = 1,ndime
             delts(idime) = delts(idime) - xjaci(idime,jdime) * deltx(jdime)
          end do
       end do
       coloc(1:ndime) = coloc(1:ndime) + delts(1:ndime)

       !write(90,*) iiter,sqrt(delts(1)*delts(1)+delts(2)*delts(2)+delts(3)*delts(3)),coloc(1:3)

       if ( coloc(1) > rhuge .or. coloc(2) > rhuge .or. coloc(3) > rhuge ) then
          iiter = maxit + 1
          exit nr_iterations
       end if

    end do nr_iterations
    !
    ! Final shape function and derivative
    !
    if( iiter >= maxit ) then
       do inode = 1, pnode 
         write(3000+kfl_paral,*) '###', elcod(1:ndime,inode), '###'
         call flush (3000+kfl_paral)
       end do
       write(3000+kfl_paral,*) '<<<', coglo(1:ndime), '>>>'
       call flush (3000+kfl_paral)
       coloc(1) = 2.0_rp
       call runend('ELMGEO_NEWRAP: NEWTON-RAPHSON HAS NOT CONVERGED')
    else
       call elmgeo_shapf_deriv_heslo(ndime,pnode,coloc,shapf,deriv)
    end if

  end subroutine elmgeo_newrap

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates
  !> @details 0 dimensional shape function
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_shape0(nnode,shapf)
    implicit none
    integer(ip), intent(in)  :: nnode
    real(rp),    intent(out) :: shapf(nnode)

    shapf(1) = 1.0_rp

  end subroutine elmgeo_shape0

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates
  !> @details Evaluates shape functions, derivatives and Hessian
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_shapf_deriv_heslo(&
       ndime,nnode,posgp,shapf,deriv,heslo)
    implicit none    
    integer(ip), intent(in)            :: ndime
    integer(ip), intent(in)            :: nnode
    real(rp),    intent(in)            :: posgp(ndime)
    real(rp),    intent(out)           :: shapf(nnode)
    real(rp),    intent(out), optional :: deriv(*)
    real(rp),    intent(out), optional :: heslo(*)

    if( ndime == 0 ) then
       call elmgeo_shape0(nnode,shapf)
    else if( ndime == 1 ) then
       call elmgeo_shape1(posgp(1),nnode,shapf,deriv,heslo)
    else if( ndime == 2 ) then
       call elmgeo_shape2(posgp(1),posgp(2),nnode,shapf,deriv,heslo)
    else if( ndime == 3 ) then
       call elmgeo_shape3(posgp(1),posgp(2),posgp(3),nnode,shapf,deriv,heslo)
    end if

  end subroutine elmgeo_shapf_deriv_heslo

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates
  !> @details Evaluates shape functions, derivatives and Hessian 
  !>          for th bubble
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_shapf_deriv_heslo_bubble(&
       ndime,nnode,posgp,shapf,deriv,heslo)
    implicit none    
    integer(ip), intent(in)            :: ndime
    integer(ip), intent(in)            :: nnode
    real(rp),    intent(in)            :: posgp(ndime)
    real(rp),    intent(out)           :: shapf(*)
    real(rp),    intent(out), optional :: deriv(*)
    real(rp),    intent(out), optional :: heslo(*)

    if( ndime == 0 ) then
       return
    else if( ndime == 1 ) then
       return
    else if( ndime == 2 ) then
       if( nnode == 3 ) then
          call elmgeo_shape_bubble_triangle(posgp(1),posgp(2),shapf,deriv,heslo)
       else if( nnode == 4 ) then
          call elmgeo_shape_bubble_quadrilateral(posgp(1),posgp(2),shapf,deriv,heslo)
       end if
    else if( ndime == 3 ) then
       return
    end if

  end subroutine elmgeo_shapf_deriv_heslo_bubble

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux and beatriz Eguzkitza
  !> @date    01/10/2015
  !> @brief   Bubble function for triangle
  !> @details Bubble function for triangle
  !>
  !----------------------------------------------------------------------

  subroutine elmgeo_shape_bubble_triangle(s,t,shapf,deriv,heslo)
    implicit none
    real(rp),    intent(in)            :: s,t
    real(rp),    intent(out)           :: shapf(1)
    real(rp),    intent(out), optional :: deriv(2,1)
    real(rp),    intent(out), optional :: heslo(3,1)
    integer(ip)                        :: ii,jj
    real(rp)                           :: st,a1,a2,a3,ss,tt,s1,s2,s3,s4
    real(rp)                           :: t1,t2,t3,t4,s9,t9,c,a,z

    if( present(heslo) ) then
       do ii = 1,1
          do jj = 1,3
             heslo(jj,ii) = 0.0_rp
          end do
       end do
    end if
    if( present(deriv) ) then
       do ii = 1,1
          do jj = 1,2
             deriv(jj,ii) = 0.0_rp
          end do
       end do
    end if
    z        =  1.0_rp-s-t
    shapf(1) =  27.0_rp*s*t*z
    if( present(deriv) ) then  
       deriv(1,1) = 27.0_rp*(t*z-s*t)
       deriv(2,1) = 27.0_rp*(s*z-s*t)
    end if
    if( present(heslo) ) then  
       !deriv(1,1) = 27.0_rp*(t*z-s*t)
       !deriv(2,1) = 27.0_rp*(s*z-s*t)
    end if

  end subroutine elmgeo_shape_bubble_triangle

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux and beatriz Eguzkitza
  !> @date    01/10/2015
  !> @brief   Bubble function for triangle
  !> @details Bubble function for triangle
  !>
  !----------------------------------------------------------------------

  subroutine elmgeo_shape_bubble_quadrilateral(s,t,shapf,deriv,heslo)
    implicit none
    real(rp),    intent(in)            :: s,t
    real(rp),    intent(out)           :: shapf(1)
    real(rp),    intent(out), optional :: deriv(2,1)
    real(rp),    intent(out), optional :: heslo(3,1)
    integer(ip)                        :: ii,jj
    real(rp)                           :: st,a1,a2,a3,ss,tt,s1,s2,s3,s4
    real(rp)                           :: t1,t2,t3,t4,s9,t9,c,a,z

    if( present(heslo) ) then
       do ii = 1,1
          do jj = 1,3
             heslo(jj,ii) = 0.0_rp
          end do
       end do
    end if
    if( present(deriv) ) then
       do ii = 1,1
          do jj = 1,2
             deriv(jj,ii) = 0.0_rp
          end do
       end do
    end if

    shapf(1) = (1.0_rp-s)*(1.0_rp+s)*(1.0_rp-t)*(1.0_rp+t)
    if( present(deriv) ) then  
       deriv(1,1) = (1.0_rp-t)*(1.0_rp+t)*2.0_rp*s
       deriv(2,1) = (1.0_rp-s)*(1.0_rp+s)*2.0_rp*t
    end if
    if( present(heslo) ) then  
       !deriv(1,1) = 27.0_rp*(t*z-s*t)
       !deriv(2,1) = 27.0_rp*(s*z-s*t)
    end if

  end subroutine elmgeo_shape_bubble_quadrilateral

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Tells where a point is inside an element
  !> @details Tells where a point is inside an element
  !>          PTOPO        2D            3D
  !>          -----------------------------------
  !>          0        Quadrilateral  Hexahedra
  !>          1        Triangle       Tetrahedra
  !>          2               -       Pentahedra
  !>          3               -       Pyramid
  !>
  !----------------------------------------------------------------------

  subroutine elmgeo_where_is(&
       ndime,pnode,ptopo,pelty,elcod,coglo,winou,wwher,llist)
    implicit none
    integer(ip),   intent(in)            :: ndime
    integer(ip),   intent(in)            :: pnode
    integer(ip),   intent(in)            :: ptopo
    integer(ip),   intent(in)            :: pelty
    real(rp),      intent(in)            :: elcod(ndime,pnode)    
    real(rp),      intent(in)            :: coglo(ndime)
    character(20), intent(out)           :: winou
    character(20), intent(out)           :: wwher
    integer(ip),   intent(out), optional :: llist(*)
    integer(ip)                          :: ifoun
    integer(ip)                          :: inode
    integer(ip)                          :: jnode
    real(rp)                             :: coloc(3)
    real(rp)                             :: deriv(3*64)
    real(rp)                             :: shapf(64)
    real(rp)                             :: lmaxi
    real(rp)                             :: lmini
    real(rp)                             :: zeror

    zeror =  1.0e-12_rp
    lmini = -zeror
    lmaxi =  1.0_rp+zeror
    call elmgeo_natural_coordinates(    &
         ndime,pelty,pnode,elcod,shapf, &
         deriv,coglo,coloc,ifoun,zeror)

    !call elsest_chkelm(&
    !     ndime,ptopo,pnode,elcod,shapf,deriv,&
    !     coglo,coloc,ifoun,lmini,lmaxi)

    if( ifoun == 0 ) then
       winou = 'OUTSIDE'
       wwher = 'NO_WHERE'
       return
    else
       winou = 'INSIDE'
    end if

    if( ifoun == 0 ) return

    if( ptopo == 0 ) then
       if( ndime == 2 ) then
          if( abs(abs(coloc(1)) - 1.0_rp) < zeror .and. abs(abs(coloc(2)) - 1.0_rp) < zeror ) then
             wwher = 'VERTEX'
          else if( abs(abs(coloc(1)) - 1.0_rp) < zeror .and. abs(coloc(2)) < zeror ) then
             wwher = 'MID_EDGE'
          else if( abs(abs(coloc(2)) - 1.0_rp) < zeror .and. abs(coloc(1)) < zeror ) then
             wwher = 'MID_EDGE'
          else if( abs(coloc(1)) < zeror .and. abs(coloc(2)) < zeror ) then
             wwher = 'COG'
          else
             wwher = 'ANYWHERE'
          end if
       else
          call runend('ELEMNT_WHERIS: NOT CODED')
       end if
    else
       call runend('ELEMNT_WHERIS: NOT CODED')
    end if

    if(present(llist)) then
       if( trim(wwher) == 'VERTEX' ) then
          inode = 0
          jnode = 0
          do while( inode < pnode )
             inode = inode + 1
             if(        abs(coglo(    1) - elcod(    1,inode)) <= zeror &
                  .and. abs(coglo(    2) - elcod(    2,inode)) <= zeror &
                  .and. abs(coglo(ndime) - elcod(ndime,inode)) <= zeror ) then
                jnode = inode
                inode = pnode
             end if
          end do
          llist(1) = jnode
       else if( trim(wwher) == 'MID_EDGE' ) then
          if(      abs(coloc(1)) < zeror        .and. abs(coloc(2)+1.0_rp) < zeror ) then
             llist(1) = 1
             llist(2) = 2
          else if( abs(coloc(1)-1.0_rp) < zeror .and. abs(coloc(2)) < zeror ) then
             llist(1) = 2
             llist(2) = 3
          else if( abs(coloc(1)) < zeror        .and. abs(coloc(2)-1.0_rp) < zeror ) then
             llist(1) = 3
             llist(2) = 4
          else
             llist(1) = 4
             llist(2) = 1
          end if
       else
          llist(1) = 0
       end if
    end if

  end subroutine elmgeo_where_is


  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Element length
  !> @details \verbatim
  !>          HLENG ... Element length with: 
  !>                    HLENG(1)     = Max length
  !>                    HLENG(NDIME) = Min length
  !>          \endverbatim
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_element_node_length(&
       ndime,pnode,elcod,hleng)
    implicit none 
    integer(ip), intent(in)           :: ndime
    integer(ip), intent(in)           :: pnode
    real(rp),    intent(in)           :: elcod(ndime,pnode)
    real(rp),    intent(out)          :: hleng(ndime)
    integer(ip)                       :: inode,jnode,idime
    real(rp)                          :: dista

    hleng(1)     = -huge(1.0_rp) 
    hleng(ndime) =  huge(1.0_rp) 
    do inode = 1,pnode
       do jnode = inode+1,pnode
          dista = 0.0_rp
          do idime = 1,ndime
             dista = dista &
                  & + (elcod(idime,inode)-elcod(idime,jnode)) &
                  & * (elcod(idime,inode)-elcod(idime,jnode))
          end do
          dista = sqrt(dista)
          hleng(1)     = max(hleng(1),    dista)
          hleng(ndime) = min(hleng(ndime),dista)
       end do
    end do
    if( ndime == 3 ) hleng(2) = 0.0_rp

  end subroutine elmgeo_element_node_length

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Element characteristic length
  !> @details \verbatim
  !>          HLENG ... Element length with: 
  !>                    HLENG(1)     = Max length
  !>                    HLENG(NDIME) = Min length
  !>          \endverbatim
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_element_characteristic_length(&
       ndime,pnode,deriv,elcod,hleng,hnatu_opt,tragl_opt)
    implicit none 
    integer(ip), intent(in)           :: ndime,pnode
    real(rp),    intent(in)           :: elcod(ndime,pnode)
    real(rp),    intent(in)           :: deriv(ndime,pnode)
    real(rp),    intent(out)          :: hleng(ndime)
    real(rp),    intent(in), optional :: hnatu_opt
    real(rp),    intent(out),optional :: tragl_opt(ndime,ndime)
    integer(ip)                       :: inode
    real(rp)                          :: enor0,h_tem,gpdet,denom
    real(rp)                          :: xjacm(3,3),t1,t2,t3
    real(rp)                          :: tragl(ndime,ndime),hnatu

    if( present(hnatu_opt) ) then
       hnatu = hnatu_opt
    else
       hnatu = 1.0_rp
    end if

    if( ndime == 1 ) then

       xjacm(1,1) = 0.0_rp
       do inode = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,inode) * deriv(1,inode)
       end do
       tragl(1,1) = 1.0_rp/xjacm(1,1)
       enor0      = tragl(1,1) * tragl(1,1)
       hleng(1)   = hnatu/sqrt(enor0)

    else if( ndime == 2 ) then

       xjacm(1,1) = 0.0_rp
       xjacm(1,2) = 0.0_rp
       xjacm(2,1) = 0.0_rp
       xjacm(2,2) = 0.0_rp
       do inode = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,inode) * deriv(1,inode)
          xjacm(1,2) = xjacm(1,2) + elcod(1,inode) * deriv(2,inode)
          xjacm(2,1) = xjacm(2,1) + elcod(2,inode) * deriv(1,inode)
          xjacm(2,2) = xjacm(2,2) + elcod(2,inode) * deriv(2,inode)
       end do

       gpdet      =  xjacm(1,1) * xjacm(2,2) - xjacm(2,1) * xjacm(1,2)          
       denom      =  1.0_rp/gpdet
       tragl(1,1) =  xjacm(2,2) * denom
       tragl(2,2) =  xjacm(1,1) * denom
       tragl(2,1) = -xjacm(2,1) * denom
       tragl(1,2) = -xjacm(1,2) * denom  

       enor0      =  tragl(1,1) * tragl(1,1) + tragl(1,2) * tragl(1,2) 
       hleng(1)   =  hnatu/sqrt(enor0)
       enor0      =  tragl(2,1) * tragl(2,1) + tragl(2,2) * tragl(2,2) 
       hleng(2)   =  hnatu/sqrt(enor0)

       if( hleng(2) > hleng(1) ) then
          h_tem    = hleng(2)
          hleng(2) = hleng(1)
          hleng(1) = h_tem
       end if

    else if( ndime == 3 ) then

       xjacm(1,1) = 0.0_rp ! xjacm = elcod * deriv^t
       xjacm(1,2) = 0.0_rp ! tragl = xjacm^-1
       xjacm(1,3) = 0.0_rp 
       xjacm(2,1) = 0.0_rp
       xjacm(2,2) = 0.0_rp
       xjacm(2,3) = 0.0_rp
       xjacm(3,1) = 0.0_rp
       xjacm(3,2) = 0.0_rp
       xjacm(3,3) = 0.0_rp
       do inode = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,inode) * deriv(1,inode)
          xjacm(1,2) = xjacm(1,2) + elcod(1,inode) * deriv(2,inode)
          xjacm(1,3) = xjacm(1,3) + elcod(1,inode) * deriv(3,inode)
          xjacm(2,1) = xjacm(2,1) + elcod(2,inode) * deriv(1,inode)
          xjacm(2,2) = xjacm(2,2) + elcod(2,inode) * deriv(2,inode)
          xjacm(2,3) = xjacm(2,3) + elcod(2,inode) * deriv(3,inode)
          xjacm(3,1) = xjacm(3,1) + elcod(3,inode) * deriv(1,inode)
          xjacm(3,2) = xjacm(3,2) + elcod(3,inode) * deriv(2,inode)
          xjacm(3,3) = xjacm(3,3) + elcod(3,inode) * deriv(3,inode)
       end do

       t1         =  xjacm(2,2) * xjacm(3,3) - xjacm(3,2) * xjacm(2,3)
       t2         = -xjacm(2,1) * xjacm(3,3) + xjacm(3,1) * xjacm(2,3)
       t3         =  xjacm(2,1) * xjacm(3,2) - xjacm(3,1) * xjacm(2,2)
       gpdet      =  xjacm(1,1) * t1 + xjacm(1,2) * t2 + xjacm(1,3) * t3
       denom      =  1.0_rp / gpdet
       tragl(1,1) =  t1 * denom
       tragl(2,1) =  t2 * denom
       tragl(3,1) =  t3 * denom
       tragl(2,2) =  ( xjacm(1,1) * xjacm(3,3) - xjacm(3,1) * xjacm(1,3)) * denom
       tragl(3,2) =  (-xjacm(1,1) * xjacm(3,2) + xjacm(1,2) * xjacm(3,1)) * denom
       tragl(3,3) =  ( xjacm(1,1) * xjacm(2,2) - xjacm(2,1) * xjacm(1,2)) * denom
       tragl(1,2) =  (-xjacm(1,2) * xjacm(3,3) + xjacm(3,2) * xjacm(1,3)) * denom
       tragl(1,3) =  ( xjacm(1,2) * xjacm(2,3) - xjacm(2,2) * xjacm(1,3)) * denom
       tragl(2,3) =  (-xjacm(1,1) * xjacm(2,3) + xjacm(2,1) * xjacm(1,3)) * denom
       !
       ! Element length HLENG
       !
       enor0    =   tragl(1,1) * tragl(1,1) &
            &     + tragl(1,2) * tragl(1,2) &
            &     + tragl(1,3) * tragl(1,3)
       hleng(1) = hnatu/sqrt(enor0)
       enor0    =   tragl(2,1) * tragl(2,1) &
            &     + tragl(2,2) * tragl(2,2) &
            &     + tragl(2,3) * tragl(2,3)
       hleng(2) = hnatu/sqrt(enor0)
       enor0    =   tragl(3,1) * tragl(3,1) &
            &     + tragl(3,2) * tragl(3,2) &
            &     + tragl(3,3) * tragl(3,3) 
       hleng(3) = hnatu/sqrt(enor0) 
       !
       ! Sort hleng: hleng(1)=max; hleng(ndime)=min
       !     
       if( hleng(2) > hleng(1) ) then
          h_tem    = hleng(2)
          hleng(2) = hleng(1)
          hleng(1) = h_tem
       end if
       if( hleng(3) > hleng(1) ) then
          h_tem    = hleng(3)
          hleng(3) = hleng(1)
          hleng(1) = h_tem
       end if
       if( hleng(3) > hleng(2) ) then
          h_tem    = hleng(3)
          hleng(3) = hleng(2)
          hleng(2) = h_tem
       end if

    end if

    if( present(tragl_opt) ) then
       tragl_opt = tragl
    end if

  end subroutine elmgeo_element_characteristic_length

  !-----------------------------------------------------------------------
  !> @addtogroup Domain
  !> @{
  !> @file    jacobi.f90
  !> @author  Guillaume Houzeaux
  !> @date    27/01/2007
  !> @brief   Computes the Jacobian, Jacobian determinant and
  !>          Cartesian derivatives of shape function of an element
  !> @details The jacobian is
  !>                             _           _                    
  !>                            | dx/ds dx/dt |                t
  !>          Jacobian: XJACM = |             | = ELCOD * DERIV 
  !>                            |_dy/ds dy/dt_|
  !>          with
  !>                  _        _             _                    _
  !>                 | x1 x2 x3 |           | dN1/ds dN2/ds dN3/ds |
  !>         ELCOD = |          |,  DERIV = |                      |
  !>                 |_y1 y2 y3_|           |_dN1/dt dN2/dt dN3/dt_|
  !>
  !>           => Jacobian determinant: GPDET = det(XJACM)  
  !>
  !> @} 
  !-----------------------------------------------------------------------

  subroutine elmgeo_jacobian_matrix(ndime,pnode,elcod,deriv,xjacm,xjaci,gpdet)

    integer(ip), intent(in)  :: ndime               !< Dimension
    integer(ip), intent(in)  :: pnode               !< Number of nodes
    real(rp),    intent(in)  :: elcod(ndime,pnode)  !< Element coordinates
    real(rp),    intent(in)  :: deriv(ndime,pnode)  !< Shape function derivatives
    real(rp),    intent(out) :: xjacm(ndime,ndime)  !< Jacobian
    real(rp),    intent(out) :: xjaci(ndime,ndime)  !< Inverse Jacobian
    real(rp),    intent(out) :: gpdet
    integer(ip)              :: j,k
    real(rp)                 :: t1,t2,t3,denom,gpcar(3,3)

    if( ndime == 2 .and. pnode == 3 ) then
       !
       ! 2D P1 element
       !
       !gpdet = (-elcod(1,1)+elcod(1,2))*(-elcod(2,1)+elcod(2,3)) &
       !     & -(-elcod(2,1)+elcod(2,2))*(-elcod(1,1)+elcod(1,3))
       !if( gpdet == 0.0_rp ) return     
       !denom = 1.0_rp/gpdet

       xjacm(1,1) = 0.0_rp
       xjacm(1,2) = 0.0_rp
       xjacm(2,1) = 0.0_rp
       xjacm(2,2) = 0.0_rp
       do k = 1,3
          xjacm(1,1) = xjacm(1,1) + elcod(1,k) * deriv(1,k)
          xjacm(1,2) = xjacm(1,2) + elcod(1,k) * deriv(2,k)
          xjacm(2,1) = xjacm(2,1) + elcod(2,k) * deriv(1,k)
          xjacm(2,2) = xjacm(2,2) + elcod(2,k) * deriv(2,k)
       end do

       gpdet = xjacm(1,1) * xjacm(2,2) - xjacm(2,1) * xjacm(1,2)
       if( gpdet == 0.0_rp ) return
       denom = 1.0_rp/gpdet
       xjaci(1,1) =  xjacm(2,2) * denom
       xjaci(2,2) =  xjacm(1,1) * denom
       xjaci(2,1) = -xjacm(2,1) * denom
       xjaci(1,2) = -xjacm(1,2) * denom  

    else if( ndime == 3 .and. pnode == 4 ) then
       !
       ! 3D P1 element
       !
       gpcar(1,1) =  elcod(1,2) - elcod(1,1)
       gpcar(1,2) =  elcod(1,3) - elcod(1,1)
       gpcar(1,3) =  elcod(1,4) - elcod(1,1)
       gpcar(2,1) =  elcod(2,2) - elcod(2,1)
       gpcar(2,2) =  elcod(2,3) - elcod(2,1)
       gpcar(2,3) =  elcod(2,4) - elcod(2,1)
       gpcar(3,1) =  elcod(3,2) - elcod(3,1)
       gpcar(3,2) =  elcod(3,3) - elcod(3,1)
       gpcar(3,3) =  elcod(3,4) - elcod(3,1)
       t1         =  gpcar(2,2) * gpcar(3,3) - gpcar(3,2) * gpcar(2,3)
       t2         = -gpcar(2,1) * gpcar(3,3) + gpcar(3,1) * gpcar(2,3)
       t3         =  gpcar(2,1) * gpcar(3,2) - gpcar(3,1) * gpcar(2,2)
       gpdet      =  gpcar(1,1) * t1 + gpcar(1,2) * t2 + gpcar(1,3) * t3
       if( gpdet == 0.0_rp ) return
       denom      =  1.0_rp/gpdet

       xjaci(1,1) =  t1 * denom
       xjaci(2,1) =  t2 * denom
       xjaci(3,1) =  t3 * denom
       xjaci(2,2) = ( gpcar(1,1) * gpcar(3,3) - gpcar(3,1) * gpcar(1,3)) * denom
       xjaci(3,2) = (-gpcar(1,1) * gpcar(3,2) + gpcar(1,2) * gpcar(3,1)) * denom
       xjaci(3,3) = ( gpcar(1,1) * gpcar(2,2) - gpcar(2,1) * gpcar(1,2)) * denom
       xjaci(1,2) = (-gpcar(1,2) * gpcar(3,3) + gpcar(3,2) * gpcar(1,3)) * denom
       xjaci(1,3) = ( gpcar(1,2) * gpcar(2,3) - gpcar(2,2) * gpcar(1,3)) * denom
       xjaci(2,3) = (-gpcar(1,1) * gpcar(2,3) + gpcar(2,1) * gpcar(1,3)) * denom

    else if ( ndime == 1 ) then
       !
       ! 1D
       !
       xjacm(1,1) = 0.0_rp
       do k = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,k) * deriv(1,k)
       end do
       gpdet = xjacm(1,1)
       if( gpdet == 0.0_rp ) return
       xjaci(1,1) = 1.0_rp/xjacm(1,1)

    else if ( ndime == 2 ) then
       !
       ! 2D
       !
       xjacm(1,1) = 0.0_rp
       xjacm(1,2) = 0.0_rp
       xjacm(2,1) = 0.0_rp
       xjacm(2,2) = 0.0_rp
       do k = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,k) * deriv(1,k)
          xjacm(1,2) = xjacm(1,2) + elcod(1,k) * deriv(2,k)
          xjacm(2,1) = xjacm(2,1) + elcod(2,k) * deriv(1,k)
          xjacm(2,2) = xjacm(2,2) + elcod(2,k) * deriv(2,k)
       end do

       gpdet = xjacm(1,1) * xjacm(2,2) - xjacm(2,1) * xjacm(1,2)
       if( gpdet == 0.0_rp ) return
       denom = 1.0_rp/gpdet
       xjaci(1,1) =  xjacm(2,2) * denom
       xjaci(2,2) =  xjacm(1,1) * denom
       xjaci(2,1) = -xjacm(2,1) * denom
       xjaci(1,2) = -xjacm(1,2) * denom  

    else if ( ndime == 3 ) then
       !
       ! 3D
       !
       xjacm(1,1) = 0.0_rp ! xjacm = elcod * deriv^t
       xjacm(1,2) = 0.0_rp ! xjaci = xjacm^-1
       xjacm(1,3) = 0.0_rp ! gpcar = xjaci^t * deriv 
       xjacm(2,1) = 0.0_rp
       xjacm(2,2) = 0.0_rp
       xjacm(2,3) = 0.0_rp
       xjacm(3,1) = 0.0_rp
       xjacm(3,2) = 0.0_rp
       xjacm(3,3) = 0.0_rp
       do k = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,k) * deriv(1,k)
          xjacm(1,2) = xjacm(1,2) + elcod(1,k) * deriv(2,k)
          xjacm(1,3) = xjacm(1,3) + elcod(1,k) * deriv(3,k)
          xjacm(2,1) = xjacm(2,1) + elcod(2,k) * deriv(1,k)
          xjacm(2,2) = xjacm(2,2) + elcod(2,k) * deriv(2,k)
          xjacm(2,3) = xjacm(2,3) + elcod(2,k) * deriv(3,k)
          xjacm(3,1) = xjacm(3,1) + elcod(3,k) * deriv(1,k)
          xjacm(3,2) = xjacm(3,2) + elcod(3,k) * deriv(2,k)
          xjacm(3,3) = xjacm(3,3) + elcod(3,k) * deriv(3,k)
       end do

       t1    =  xjacm(2,2) * xjacm(3,3) - xjacm(3,2) * xjacm(2,3)
       t2    = -xjacm(2,1) * xjacm(3,3) + xjacm(3,1) * xjacm(2,3)
       t3    =  xjacm(2,1) * xjacm(3,2) - xjacm(3,1) * xjacm(2,2)
       gpdet =  xjacm(1,1) * t1 + xjacm(1,2) * t2 + xjacm(1,3) * t3
       if(gpdet == 0.0_rp ) return
       denom = 1.0_rp / gpdet
       xjaci(1,1) = t1*denom
       xjaci(2,1) = t2*denom
       xjaci(3,1) = t3*denom
       xjaci(2,2) = ( xjacm(1,1) * xjacm(3,3) - xjacm(3,1) * xjacm(1,3)) * denom
       xjaci(3,2) = (-xjacm(1,1) * xjacm(3,2) + xjacm(1,2) * xjacm(3,1)) * denom
       xjaci(3,3) = ( xjacm(1,1) * xjacm(2,2) - xjacm(2,1) * xjacm(1,2)) * denom
       xjaci(1,2) = (-xjacm(1,2) * xjacm(3,3) + xjacm(3,2) * xjacm(1,3)) * denom
       xjaci(1,3) = ( xjacm(1,2) * xjacm(2,3) - xjacm(2,2) * xjacm(1,3)) * denom
       xjaci(2,3) = (-xjacm(1,1) * xjacm(2,3) + xjacm(2,1) * xjacm(1,3)) * denom

    end if

  end subroutine elmgeo_jacobian_matrix

  !-----------------------------------------------------------------------
  !> 
  !> @author  Guillaume Houzeaux and J.C. Cajas
  !> @date    09/11/2015
  !> @brief   Local coordinates on a boundary
  !> @details Determine the local coordinates on a 3D boundary
  !>     
  !-----------------------------------------------------------------------

  subroutine elmgeo_natural_coordinates_on_boundaries(&
       ndime,pblty,pnodb,bocod,shapb,derib,coglo,coloc,ifoun,toler_opt)

    integer(ip), intent(in)           :: ndime 
    integer(ip), intent(in)           :: pblty 
    integer(ip), intent(in)           :: pnodb 
    real(rp),    intent(in)           :: bocod(ndime,pnodb)
    real(rp),    intent(out)          :: shapb(pnodb)
    real(rp),    intent(out)          :: derib(ndime-1,pnodb)
    real(rp),    intent(in)           :: coglo(ndime)
    real(rp),    intent(out)          :: coloc(ndime-1)
    integer(ip), intent(out)          :: ifoun
    real(rp),    intent(in), optional :: toler_opt
    integer(ip)                       :: ndimb
    real(rp)                          :: proje(3),toler
    real(rp)                          :: uu(3),lmini,lmaxi,ezzzt

    integer(ip)                       :: ii,iimax(1)

    if( present(toler_opt) ) then
       toler = toler_opt
    else
       toler = 0.01_rp
    end if
    lmini = - toler
    lmaxi = 1.0_rp + toler
    ndimb = ndime - 1
    ifoun = 0

    if( pblty == BAR02 ) then
       !
       ! BAR02 element
       !       
       call elmgeo_natural_coordinates_on_BAR02(bocod,coglo,coloc)
       call elmgeo_shape1(coloc(1),2_ip,shapb,derib)
       if( coloc(1) >= -lmaxi .and. coloc(1) <= lmaxi ) ifoun = 1

    else if( pblty == TRI03 ) then
       !
       ! TRI03 element
       !
       call elmgeo_natural_coordinates_on_TRI03(bocod,coglo,coloc)
       call elmgeo_shape2(coloc(1),coloc(2),3_ip,shapb,derib)

       if(    coloc(1) >= lmini .and. coloc(1) <= lmaxi ) then
          if( coloc(2) >= lmini .and. coloc(2) <= lmaxi ) then
             ezzzt = 1.0_rp-coloc(1)-coloc(2)
             if( ezzzt >= lmini .and. ezzzt <= lmaxi ) then
                ifoun = 1
             end if
          end if
       end if

    else if( pblty == QUA04 ) then
       !
       ! QUA04 element 
       !
       call elmgeo_natural_coordinates_on_QUA04(bocod,coglo,coloc)
       call elmgeo_shape2(coloc(1),coloc(2),4_ip,shapb,derib)

       if(   coloc(1) >= -lmaxi .and. coloc(1) <= lmaxi ) then
          if( coloc(2)>= -lmaxi .and. coloc(2) <= lmaxi ) then
             ifoun = 1
          end if
       end if

    else

       call runend('ELMGEO_NATURAL_COORDINATES_ON_BOUNDARIES: PRORGRAMALO SI TE ATREVES!')

    end if

  end subroutine elmgeo_natural_coordinates_on_boundaries

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Check if an element is in element bounding box
  !> @details Check if an element is in element bounding box
  !
  !------------------------------------------------------------------------

  function elmgeo_inside_element_bounding_box(ndime,pnode,elcod,coglo,toler)
    integer(ip), intent(in) :: ndime
    integer(ip), intent(in) :: pnode
    real(rp),    intent(in) :: elcod(ndime,pnode)
    real(rp),    intent(in) :: coglo(ndime)
    real(rp),    intent(in) :: toler
    real(rp)                :: xdist(3),xmini(3),xmaxi(3)
    logical(lg)             :: elmgeo_inside_element_bounding_box
    integer(ip)             :: idime,inode

    if( ndime == 2 ) then

       xmini(1)   = minval(elcod(1,1:pnode))
       xmaxi(1)   = maxval(elcod(1,1:pnode))
       xmini(2)   = minval(elcod(2,1:pnode))
       xmaxi(2)   = maxval(elcod(2,1:pnode))
       xdist(1:2) = toler * (xmaxi(1:2)-xmini(1:2))
       xmini(1:2) = xmini(1:2) - xdist(1:2) 
       xmaxi(1:2) = xmaxi(1:2) + xdist(1:2) 

       if(      coglo(1) > xmaxi(1)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else if( coglo(2) > xmaxi(2)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else if( coglo(1) < xmini(1)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else if( coglo(2) < xmini(2)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else
          elmgeo_inside_element_bounding_box = .true.          
       end if

    else

       xmini(1)   = minval(elcod(1,1:pnode))
       xmaxi(1)   = maxval(elcod(1,1:pnode))
       xmini(2)   = minval(elcod(2,1:pnode))
       xmaxi(2)   = maxval(elcod(2,1:pnode))
       xmini(3)   = minval(elcod(3,1:pnode))
       xmaxi(3)   = maxval(elcod(3,1:pnode))
       xdist(1:3) = toler * (xmaxi(1:3)-xmini(1:3))
       xmini(1:3) = xmini(1:3) - xdist(1:3) 
       xmaxi(1:3) = xmaxi(1:3) + xdist(1:3) 

       if(      coglo(1) > xmaxi(1)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else if( coglo(2) > xmaxi(2)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else if( coglo(3) > xmaxi(3)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else if( coglo(1) < xmini(1)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else if( coglo(2) < xmini(2)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else if( coglo(3) < xmini(3)  ) then
          elmgeo_inside_element_bounding_box = .false.
       else
          elmgeo_inside_element_bounding_box = .true.          
       end if

    end if

  end function elmgeo_inside_element_bounding_box

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Transform coordinates from one tetra to another
  !> @details Transform coordinates in a tetrahedron to the coordinates
  !>          in the tetrahedra coming from the decomposition of 
  !>          pyramid, prisms and hexahedra
  !
  !------------------------------------------------------------------------

  subroutine elmgeo_tetrahedra_to_tetrahedra(pelty,itetr,coloc)

    integer(ip), intent(in)    :: pelty         !< Element type
    integer(ip), intent(in)    :: itetr         !< Tetrahedra number
    real(rp),    intent(inout) :: coloc(3)      !< Final coordinate
    real(rp)                   :: elcod(3,4)
    real(rp)                   :: xjacm(3,3)
    real(rp)                   :: coloc_cpy(3)

    if( pelty == PYR05 .and. itetr == 1 ) then
       !
       ! First tetrahedra of PYR05 (1,2,4,5)
       !
       elcod(1,1) = -1.0_rp
       elcod(2,1) = -1.0_rp
       elcod(3,1) = -1.0_rp

       elcod(1,2) =  1.0_rp
       elcod(2,2) = -1.0_rp
       elcod(3,2) = -1.0_rp

       elcod(1,3) = -1.0_rp
       elcod(2,3) =  1.0_rp
       elcod(3,3) = -1.0_rp

       elcod(1,4) =  0.0_rp
       elcod(2,4) =  0.0_rp
       elcod(3,4) =  1.0_rp

    else if( pelty == PYR05 .and. itetr == 2 ) then
       !
       ! Second tetrahedra of PYR05 (2,3,4,5)
       !
       elcod(1,1) =  1.0_rp
       elcod(2,1) = -1.0_rp
       elcod(3,1) = -1.0_rp

       elcod(1,2) =  1.0_rp
       elcod(2,2) =  1.0_rp
       elcod(3,2) = -1.0_rp

       elcod(1,3) = -1.0_rp
       elcod(2,3) =  1.0_rp
       elcod(3,3) = -1.0_rp

       elcod(1,4) =  0.0_rp
       elcod(2,4) =  0.0_rp
       elcod(3,4) =  1.0_rp

    else if( pelty == PEN06 .and. itetr == 1 ) then
       !
       ! Second tetrahedra of PEN06 (1,2,3,5)
       !
       elcod(1,1) =  0.0_rp
       elcod(2,1) =  0.0_rp
       elcod(3,1) =  0.0_rp

       elcod(1,2) =  1.0_rp
       elcod(2,2) =  0.0_rp
       elcod(3,2) =  0.0_rp

       elcod(1,3) =  0.0_rp
       elcod(2,3) =  1.0_rp
       elcod(3,3) =  0.0_rp

       elcod(1,4) =  1.0_rp
       elcod(2,4) =  0.0_rp
       elcod(3,4) =  1.0_rp

    else if( pelty == PEN06 .and. itetr == 2 ) then
       !
       ! Second tetrahedra of PEN06 (4,1,6,5)
       !
       elcod(1,1) =  0.0_rp
       elcod(2,1) =  0.0_rp
       elcod(3,1) =  1.0_rp

       elcod(1,2) =  0.0_rp
       elcod(2,2) =  0.0_rp
       elcod(3,2) =  0.0_rp

       elcod(1,3) =  0.0_rp
       elcod(2,3) =  1.0_rp
       elcod(3,3) =  1.0_rp

       elcod(1,4) =  1.0_rp
       elcod(2,4) =  0.0_rp
       elcod(3,4) =  1.0_rp

    else if( pelty == PEN06 .and. itetr == 3 ) then
       !
       ! Second tetrahedra of PEN06 (3,6,1,5)
       !
       elcod(1,1) =  0.0_rp
       elcod(2,1) =  1.0_rp
       elcod(3,1) =  0.0_rp

       elcod(1,2) =  0.0_rp
       elcod(2,2) =  1.0_rp
       elcod(3,2) =  1.0_rp

       elcod(1,3) =  0.0_rp
       elcod(2,3) =  0.0_rp
       elcod(3,3) =  0.0_rp

       elcod(1,4) =  1.0_rp
       elcod(2,4) =  0.0_rp
       elcod(3,4) =  1.0_rp

    end if

    coloc_cpy    = coloc
    xjacm(1:3,1) = elcod(1:3,2) - elcod(1:3,1)
    xjacm(1:3,2) = elcod(1:3,3) - elcod(1:3,1)
    xjacm(1:3,3) = elcod(1:3,4) - elcod(1:3,1)
    coloc(1)     = elcod(1,1)   + dot_product(xjacm(1,1:3),coloc_cpy(1:3)) 
    coloc(2)     = elcod(2,1)   + dot_product(xjacm(2,1:3),coloc_cpy(1:3)) 
    coloc(3)     = elcod(3,1)   + dot_product(xjacm(3,1:3),coloc_cpy(1:3)) 

  end subroutine elmgeo_tetrahedra_to_tetrahedra

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Nearest point on element faces
  !> @details Look for the nearest point XX element faces
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_nearest_point_on_element_faces(&
       ndime,pelty,elcod,coglo,coloc,shapf,deriv,dista,toler_opt)
    integer(ip), intent(in)            :: ndime
    integer(ip), intent(in)            :: pelty
    real(rp),    intent(in)            :: elcod(ndime,*)
    real(rp),    intent(in)            :: coglo(ndime)
    real(rp),    intent(out)           :: coloc(*)             !< Parametrical coordinates
    real(rp),    intent(out)           :: shapf(*)             !< Shape functions of test point  
    real(rp),    intent(out)           :: deriv(ndime,*)       !< Shape funcitons derivatives of test point 
    real(rp),    intent(out)           :: dista                !< Original distance to element
    real(rp),    intent(in),  optional :: toler_opt            !< Tolerance
    integer(ip)                        :: iface,pnodf,inodf
    integer(ip)                        :: inode,ipoin,pflty
    integer(ip)                        :: pnode,ifoun,jnode
    integer(ip)                        :: jfoun
    real(rp)                           :: bocod(ndime,16)
    real(rp)                           :: nn(3),xx(3),toler
    real(rp)                           :: ezzzt,lmini,lmaxi
    real(rp)                           :: xdist,xx_min(3)

    if( present(toler_opt) ) then
       toler = toler_opt
    else
       toler = epsil
    end if
    lmini = -toler
    lmaxi = 1.0_rp + toler
    pnode = element_type(pelty) % number_nodes
    ifoun = 0
    xdist = huge(1.0_rp)

    loop_faces: do iface = 1,element_type(pelty) % number_faces

       jfoun = 0
       pnodf = element_type(pelty) % node_faces(iface)
       pflty = element_type(pelty) % type_faces(iface)
       do inodf = 1,pnodf
          inode = element_type(pelty) % list_faces(inodf,iface)
          bocod(1:ndime,inodf) = elcod(1:ndime,inode)
       end do
       !
       ! Projection point XX on faces
       !
       if( pflty == BAR02 ) then
          !
          ! BAR02
          !
          call elmgeo_nearest_point_on_BAR02(bocod,coglo,xx,nn)
          call elmgeo_natural_coordinates_on_BAR02(bocod,xx,coloc)
          if( coloc(1) >= -lmaxi .and. coloc(1) <= lmaxi ) then
             jfoun = 1
          end if

       else if( pflty == TRI03 ) then
          !
          ! TRI03 
          !
          call elmgeo_nearest_point_on_TRI03(bocod,coglo,xx,nn)
          call elmgeo_natural_coordinates_on_TRI03(bocod,xx,coloc)

          if(    coloc(1) >= lmini .and. coloc(1) <= lmaxi ) then
             if( coloc(2) >= lmini .and. coloc(2) <= lmaxi ) then
                ezzzt = 1.0_rp-coloc(1)-coloc(2)
                if( ezzzt >= lmini .and. ezzzt <= lmaxi ) then
                   jfoun = 1
                end if
             end if
          end if

       else if( pflty == QUA04 ) then
          !
          ! QUA04
          !
          call elmgeo_nearest_point_on_QUA04(bocod,coglo,xx,nn)
          call elmgeo_natural_coordinates_on_QUA04(bocod,xx,coloc)

          if(   coloc(1) >= -lmaxi .and. coloc(1) <= lmaxi ) then
             if( coloc(2)>= -lmaxi .and. coloc(2) <= lmaxi ) then
                jfoun = 1
             end if
          end if

       else

          call runend('ELMGEO_INSIDE_ELEMENT_USING_FACES: UNKNOWN TYPE OF FACE')

       end if

       if( jfoun == 1 ) then
          ifoun = 1
          xdist = sqrt(dot_product(xx(1:ndime)-coglo(1:ndime),xx(1:ndime)-coglo(1:ndime)))
          if( xdist < dista ) then
             dista = xdist
             xx_min(1:ndime) = xx(1:ndime)
          end if
       end if

    end do loop_faces
    !
    ! Find natural coordinate of the projection XX in the element
    !
    if( ifoun == 1 ) then
       call elmgeo_natural_coordinates(    &
            ndime,pelty,pnode,elcod,shapf, &
            deriv,xx_min,coloc,ifoun,toler)
    end if
    !
    ! If we are not the element look for nearest node
    !
    if( ifoun == 0 ) then
       dista = huge(1.0_rp)
       do inode = 1,pnode
          xdist = dot_product( coglo(1:ndime)-elcod(1:ndime,inode),coglo(1:ndime)-elcod(1:ndime,inode) )
          if( xdist <= dista ) then
             jnode = inode
             dista = xdist
          end if
       end do
       xx_min(1:ndime) = elcod(1:ndime,jnode)
       dista = sqrt(dista)
       call elmgeo_natural_coordinates(    &
            ndime,pelty,pnode,elcod,shapf, &
            deriv,xx_min,coloc,ifoun,toler)
    end if

  end subroutine elmgeo_nearest_point_on_element_faces

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Nearest point on a face
  !> @details Porject point COGLO on a face with coordinates BOCOD
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_projection_on_a_face(&
       ndime,pflty,bocod,coglo,projection)
    integer(ip), intent(in)            :: ndime
    integer(ip), intent(in)            :: pflty
    real(rp),    intent(in)            :: bocod(ndime,*)
    real(rp),    intent(in)            :: coglo(ndime)
    real(rp),    intent(out)           :: projection(ndime)     !< Coordinates of the projection point
    real(rp)                           :: nn(3)
 
    if( pflty == BAR02 ) then
       !
       ! BAR02
       !
       call elmgeo_nearest_point_on_BAR02(bocod,coglo,projection,nn)
       
    else if( pflty == TRI03 ) then
       !
       ! TRI03 
       !
       call elmgeo_nearest_point_on_TRI03(bocod,coglo,projection,nn)
       
    else if( pflty == QUA04 ) then
       !
       ! QUA04
       !
       call elmgeo_nearest_point_on_QUA04(bocod,coglo,projection,nn)

    else
       
       call runend('ELMGEO_PROJECTION_ON_A_FACE: UNKNOWN TYPE OF FACE')
       
    end if

  end subroutine elmgeo_projection_on_a_face

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Projection on a line
  !> @details Look for the nearest point XX to COGLO on a line
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_nearest_point_on_BAR02(bocod,coglo,xx,nn)
    real(rp),    intent(in)  :: bocod(2,2)
    real(rp),    intent(in)  :: coglo(2)
    real(rp),    intent(out) :: xx(2)
    real(rp),    intent(out) :: nn(2)
    real(rp)                 :: t

    nn(1)   = - ( bocod(2,2) - bocod(2,1) )
    nn(2)   =   ( bocod(1,2) - bocod(1,1) )
    nn(1:2) =   nn(1:2) / sqrt( nn(1)*nn(1) + nn(2)*nn(2) )
    t       =   dot_product(nn(1:2),bocod(1:2,1)-coglo(1:2))   
    xx(1:2) =   coglo(1:2) + t * nn(1:2)

  end subroutine elmgeo_nearest_point_on_BAR02

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Projection on a linear surface
  !> @details Look for the nearest point XX to COGLO on a linear
  !>          surface
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_nearest_point_on_TRI03(bocod,coglo,xx,nn)
    real(rp),    intent(in)  :: bocod(3,4)
    real(rp),    intent(in)  :: coglo(3)
    real(rp),    intent(out) :: xx(3)
    real(rp),    intent(out) :: nn(3)
    real(rp)                 :: a(3),b(3),t

    a(1:3)  =   bocod(1:3,2) - bocod(1:3,1)
    b(1:3)  =   bocod(1:3,3) - bocod(1:3,1)
    nn(1)   =   a(2) * b(3) - a(3) * b(2) 
    nn(2)   = - a(1) * b(3) + a(3) * b(1)  
    nn(3)   =   a(1) * b(2) - a(2) * b(1)
    nn(1:3) =   nn(1:3) / sqrt( nn(1)*nn(1) + nn(2)*nn(2) + nn(3)*nn(3) )
    t       =   dot_product(nn(1:3),bocod(1:3,1)-coglo(1:3))   
    xx(1:3) =   coglo(1:3) + t * nn(1:3)

  end subroutine elmgeo_nearest_point_on_TRI03

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Projection on a bilinear surface
  !> @details Look for the nearest point XX to COGLO on a bilinear
  !>          surface
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_nearest_point_on_QUA04(bocod,coglo,xx,nn)
    real(rp),    intent(in)  :: bocod(3,4)
    real(rp),    intent(in)  :: coglo(3)
    real(rp),    intent(out) :: xx(3)
    real(rp),    intent(out) :: nn(3)
    integer(ip)              :: iiter,maxit
    real(rp)                 :: a(3),b(3),c(3),d(3),u,v,toler,denom
    real(rp)                 :: a2,ab,ac,ad,ax,bc,bd,bx,b2,c2,cd,cx,f(2)
    real(rp)                 :: dfdu(2,2),dfduinv(2,2),vec1(3),vec2(3)
    !
    ! P(u,v) = (1-u)*[(1-v)*P00+v*P01] + u*[(1-v)*P10+v*P11]
    !
    a(1:3) = - bocod(1:3,1) + bocod(1:3,2)
    b(1:3) = - bocod(1:3,1) + bocod(1:3,4)
    c(1:3) =   bocod(1:3,1) - bocod(1:3,4) - bocod(1:3,2) + bocod(1:3,3)
    d(1:3) =   bocod(1:3,1)

    vec1(1:3) =   bocod(1:3,2) - bocod(1:3,1) 
    vec2(1:3) =   bocod(1:3,3) - bocod(1:3,1)
    nn(1)     =   vec1(2) * vec2(3) - vec1(3) * vec2(2) 
    nn(2)     = - vec1(1) * vec2(3) + vec1(3) * vec2(1)  
    nn(3)     =   vec1(1) * vec2(2) - vec1(2) * vec2(1)
    nn(1:3)   =   nn(1:3) / sqrt( nn(1)*nn(1) + nn(2)*nn(2) + nn(3)*nn(3) )

    a2     = a(1)*a(1) + a(2)*a(2) + a(3)*a(3)
    b2     = b(1)*b(1) + b(2)*b(2) + b(3)*b(3)
    ab     = a(1)*b(1) + a(2)*b(2) + a(3)*b(3)
    ac     = a(1)*c(1) + a(2)*c(2) + a(3)*c(3)
    ad     = a(1)*d(1) + a(2)*d(2) + a(3)*d(3)
    bd     = b(1)*d(1) + b(2)*d(2) + b(3)*d(3)
    bc     = b(1)*c(1) + b(2)*c(2) + b(3)*c(3)
    c2     = c(1)*c(1) + c(2)*c(2) + c(3)*c(3)
    cd     = c(1)*d(1) + c(2)*d(2) + c(3)*d(3)
    ax     = a(1)*coglo(1) + a(2)*coglo(2) + a(3)*coglo(3)
    bx     = b(1)*coglo(1) + b(2)*coglo(2) + b(3)*coglo(3)
    cx     = c(1)*coglo(1) + c(2)*coglo(2) + c(3)*coglo(3)

    toler  = 1.0e-08_rp
    iiter  = 0
    maxit  = 100
    u      = 0.5_rp
    v      = 0.5_rp
    f      = 1.0_rp

    do while( maxval(abs(f(1:2))) > toler .and. iiter < maxit )
       iiter        = iiter + 1
       f(1)         =         a2*u  + (ab+cd-cx)*v + 2.0_rp*ac*u*v + c2*u*v*v + bc*v*v + ad - ax
       f(2)         = (ab+cd-cx)*u  +         b2*v + 2.0_rp*bc*u*v + c2*u*u*v + ac*u*u + bd - bx
       dfdu(1,1)    = a2 + 2.0_rp*ac*v + c2*v*v
       dfdu(1,2)    = (ab+cd-cx) + 2.0_rp*ac*u + 2.0_rp*c2*u*v + 2.0_rp*bc*v
       dfdu(2,1)    = dfdu(1,2)
       dfdu(2,2)    = b2 + 2.0_rp*bc*u + c2*u*u

       denom        =  dfdu(1,1) * dfdu(2,2) - dfdu(2,1) * dfdu(1,2)
       denom        =  1.0_rp / denom
       dfduinv(1,1) =  dfdu(2,2) * denom
       dfduinv(2,2) =  dfdu(1,1) * denom
       dfduinv(2,1) = -dfdu(2,1) * denom
       dfduinv(1,2) = -dfdu(1,2) * denom  

       u            = u - ( dfduinv(1,1) * f(1) + dfduinv(1,2) * f(2) )
       v            = v - ( dfduinv(2,1) * f(1) + dfduinv(2,2) * f(2) )

    end do

    xx(1:3) = a(1:3)*u + b(1:3)*v + c(1:3)*u*v + d(1:3)

  end subroutine elmgeo_nearest_point_on_QUA04

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Check if a point is inside an element
  !> @details Compute the scalar product with the face normals to check
  !>          if a point is inside and element 
  !>
  !-----------------------------------------------------------------------

  function elmgeo_inside_element_using_faces(pelty,elcod,coglo,toler_opt)
    integer(ip), intent(in)           :: pelty
    real(rp),    intent(in)           :: elcod(3,*)
    real(rp),    intent(in)           :: coglo(3)
    real(rp),    intent(in), optional :: toler_opt
    logical(lg)                       :: elmgeo_inside_element_using_faces
    integer(ip)                       :: iface,pnodf,inodf,inode,ipoin,pflty
    real(rp)                          :: bocod(3,16)
    logical(lg)                       :: inside
    real(rp)                          :: vec1(3),vec2(3),toler
    real(rp)                          :: nn(3),xx(3),dotpro

    if( present(toler_opt) ) then
       toler =  toler_opt
    else
       toler = -epsil
    end if

    elmgeo_inside_element_using_faces = .true.

    do iface = 1,element_type(pelty) % number_faces
       pnodf = element_type(pelty) % node_faces(iface)
       pflty = element_type(pelty) % type_faces(iface)
       do inodf = 1,pnodf
          inode = element_type(pelty) % list_faces(inodf,iface)
          bocod(1:3,inodf) = elcod(1:3,inode)
       end do

       if( pflty == TRI03 ) then
          !
          ! TRI03 
          !
          call elmgeo_nearest_point_on_TRI03(bocod,coglo,xx,nn)
          dotpro = dot_product(xx(1:3)-coglo(1:3),nn(1:3))

       else if( pflty == QUA04 ) then
          !
          ! QUA04
          !
          call elmgeo_nearest_point_on_QUA04(bocod,coglo,xx,nn)
          dotpro = dot_product(xx(1:3)-coglo(1:3),nn(1:3))

       else

          call runend('ELMGEO_INSIDE_ELEMENT_USING_FACES: UNKNOWN TYPE OF FACE')

       end if

       if( dotpro <= toler ) then
          elmgeo_inside_element_using_faces = .false.
          return
       end if

    end do

  end function elmgeo_inside_element_using_faces

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates on a bilinear surface
  !> @details 
  !>        4(-1,1)    3 (1,1)
  !>          o----------o
  !>          |          |
  !>          |          |
  !>          |          |
  !>          o----------o
  !>        1(-1,-1)   2 (1,-1)
  !>
  !>          +-           -+ +- -+   +-  -+
  !>          | -1 -1  1  1 | | a |   | x1 |
  !>          |  1 -1 -1  1 | | b |   | x2 |
  !>          |  1  1  1  1 | | c | = | x3 |
  !>          | -1  1 -1  1 | | d |   | x4 |
  !>          +-           -+ +- -+   +-  -+
  !>
  !>          +- -+     +-           -+ +-  -+    
  !>          | a |     | -1  1  1 -1 | | x1 |
  !>          | b |   1 | -1 -1  1  1 | | x2 |
  !>          | c | = - |  1 -1  1 -1 | | x3 |
  !>          | d |   4 | -1  1  1  1 | | x4 |
  !>          +- -+     +-           -+ +-  -+
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_natural_coordinates_on_QUA04(bocod,coglo,coloc)
    use def_kintyp
    real(rp),    intent(in)  :: bocod(3,4)
    real(rp),    intent(in)  :: coglo(3)
    real(rp),    intent(out) :: coloc(2)
    integer(ip)              :: iiter,maxit,ii,jj,iimax(1),jjmax(1)
    real(rp)                 :: a(3),b(3),c(3),d(3),toler,denom,s,t
    real(rp)                 :: f(2),df(2,2),dfinv(2,2),bcpy(1:3)
    !
    ! x = a*u + b*v + c*u*v + d
    !
    a(1:3) = 0.25_rp * ( - bocod(1:3,1) + bocod(1:3,2) + bocod(1:3,3) - bocod(1:3,4) )  
    b(1:3) = 0.25_rp * ( - bocod(1:3,1) - bocod(1:3,2) + bocod(1:3,3) + bocod(1:3,4) )  
    c(1:3) = 0.25_rp * (   bocod(1:3,1) - bocod(1:3,2) + bocod(1:3,3) - bocod(1:3,4) )  
    d(1:3) = 0.25_rp * (   bocod(1:3,1) + bocod(1:3,2) + bocod(1:3,3) + bocod(1:3,4) )  
    bcpy   = b
    !
    ! Choose coordinates with highest change to invert system
    !
    iimax    =  maxloc(abs(a(1:3)))
    ii       =  iimax(1)
    bcpy(ii) =  0.0_rp
    jjmax    =  maxloc(abs(bcpy(1:3)))
    jj       =  jjmax(1)
    !
    ! f(u,v) = a*u + b*v + c*u*v + d - x
    !
    toler  = 1.0e-08_rp
    iiter  = 0
    maxit  = 100
    s      = 0.0_rp
    t      = 0.0_rp
    f      = 1.0_rp
    !
    ! Newton-Raphson: s^i+1 = s - [df/ds]^-1.f(s)
    !
    do while( maxval(abs(f(1:2))) > toler .and. iiter < maxit )
       iiter      =  iiter + 1
       f(1)       =  a(ii)*s + b(ii)*t + c(ii)*s*t + d(ii) - coglo(ii)
       f(2)       =  a(jj)*s + b(jj)*t + c(jj)*s*t + d(jj) - coglo(jj)

       df(1,1)    =  a(ii) + c(ii)*t  
       df(1,2)    =  b(ii) + c(ii)*s
       df(2,1)    =  a(jj) + c(jj)*t 
       df(2,2)    =  b(jj) + c(jj)*s

       denom      =  df(1,1) * df(2,2) - df(2,1) * df(1,2)
       denom      =  1.0_rp  / denom
       dfinv(1,1) =  df(2,2) * denom
       dfinv(2,2) =  df(1,1) * denom
       dfinv(2,1) = -df(2,1) * denom
       dfinv(1,2) = -df(1,2) * denom  

       s          =  s - ( dfinv(1,1) * f(1) + dfinv(1,2) * f(2) )
       t          =  t - ( dfinv(2,1) * f(1) + dfinv(2,2) * f(2) )
    end do

    if( iiter > maxit ) then
       call runend('elmgeo_natural_coordinates_on_QUA04: NEWTON-RAPHSON NOT CONVERGED')
    else
       coloc(1) = s
       coloc(2) = t
    end if

  end subroutine elmgeo_natural_coordinates_on_QUA04

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates on a segment
  !> @details Natural coordinates on a segment
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_natural_coordinates_on_BAR02(bocod,coglo,coloc)

    real(rp),    intent(in)  :: bocod(2,2)
    real(rp),    intent(in)  :: coglo(2)
    real(rp),    intent(out) :: coloc(1)
    integer(ip)              :: ii,iimax(1)
    real(rp)                 :: uu(3),lmini,lmaxi,ezzzt

    uu(1:2)  =  bocod(1:2,2)-bocod(1:2,1)
    iimax    =  maxloc(abs(uu(1:2)))
    ii       =  iimax(1)
    coloc(1) = -1.0_rp + 2.0_rp * ( coglo(ii)-bocod(ii,1) ) / uu(ii)

  end subroutine elmgeo_natural_coordinates_on_BAR02

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Natural coordinates on a linear surface
  !> @details 
  !>          3
  !>          o        
  !>          |\        
  !>          |  \
  !>          o---o           
  !>          1   2
  !>        
  !>         Equation of the triangle: x = (x2-x1) * s + (x3-x1) * t + x1 
  !>         One equation is redundant so take the tywo one corresponding
  !>         to the lowest normal components:
  !>         +-            -+ +- -+    +-    -+
  !>         | x2-x1  x3-x1 | | s |    | x-x1 |
  !>         |              | |   | =  |      |
  !>         | y2-y1  y3-y1 | | t |    | y-y1 |
  !>         +-            -+ +- -+    +-    -+
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_natural_coordinates_on_TRI03(bocod,coglo,coloc)

    real(rp),    intent(in)  :: bocod(3,3)
    real(rp),    intent(in)  :: coglo(3)
    real(rp),    intent(out) :: coloc(2)
    integer(ip)              :: kkmax(1),ii,jj
    real(rp)                 :: mat(2,2),invmat(2,2)
    real(rp)                 :: a(3),b(3),rhs(3),deter,n(3)
    !
    ! Do not consider direction of maximum normal component
    !
    a(1:3)   =  bocod(1:3,2) - bocod(1:3,1)
    b(1:3)   =  bocod(1:3,3) - bocod(1:3,1)
    n(1)     =  a(2) * b(3) - a(3) * b(2) 
    n(2)     = -a(1) * b(3) + a(3) * b(1)  
    n(3)     =  a(1) * b(2) - a(2) * b(1)
    kkmax    =  maxloc(abs(n))
    ii       =  perm1(kkmax(1))
    jj       =  perm2(kkmax(1))

    mat(1,1) =  a(ii)
    mat(1,2) =  b(ii)
    rhs(1)   =  coglo(ii) - bocod(ii,1)

    mat(2,1) =  a(jj)
    mat(2,2) =  b(jj)
    rhs(2)   =  coglo(jj) - bocod(jj,1)

    call invmtx(mat,invmat,deter,2_ip)

    coloc(1) = invmat(1,1) * rhs(1) + invmat(1,2) * rhs(2) 
    coloc(2) = invmat(2,1) * rhs(1) + invmat(2,2) * rhs(2) 

  end subroutine elmgeo_natural_coordinates_on_TRI03

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Test subroutine
  !> @details This subroutine tests elmgeo_natural_coordinates_on_boundaries
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_test_natural_coordinates_on_boundaries(&
       ndime,mnode,nelem,ltype,lnods,coord)

    integer(ip), intent(in) :: ndime
    integer(ip), intent(in) :: mnode
    integer(ip), intent(in) :: nelem  
    integer(ip), intent(in) :: ltype(*)
    integer(ip), intent(in) :: lnods(mnode,*)
    real(rp),    intent(in) :: coord(ndime,*)
    integer(ip)             :: ifoun,pelty,ielem
    integer(ip)             :: iface,inodf,inode,pflty,pnodf,ii
    real(rp)                :: bocod(ndime,64),coglo2(3)
    real(rp)                :: shapb(64),derib(ndime,64)
    real(rp)                :: coglo(3),coloc(3),xdiff

    do ielem = 1,nelem

       pelty = ltype(ielem)

       do iface = 1,element_type(pelty) % number_faces
          pnodf = element_type(pelty) % node_faces(iface)
          pflty = element_type(pelty) % type_faces(iface)
          do inodf = 1,pnodf
             inode = element_type(pelty) % list_faces(inodf,iface)
             bocod(1:ndime,inodf) = coord(1:ndime,lnods(inode,ielem))
          end do

          !do iface = 1,nface(pelty)
          !   pnodf = nnodf(pelty) % l(iface)
          !   pflty = ltypf(pelty) % l(iface)
          !   do inodf = 1,pnodf
          !      inode = lface(pelty) % l(inodf,iface) 
          !      bocod(1:ndime,inodf) = coord(1:ndime,lnods(inode,ielem))
          !   end do
          !do inodf = 1,pnodf
          !   inode = lface(pelty) % l(inodf,iface) 

          do inodf = 1,pnodf
             inode = element_type(pelty) % list_faces(inodf,iface)
             coglo(1:ndime) = bocod(1:ndime,inodf)

             call elmgeo_natural_coordinates_on_boundaries(&
                  ndime,pflty,pnodf,bocod,shapb,derib,coglo,coloc,ifoun)

             coglo2 = 0.0_rp
             do ii = 1,pnodf
                coglo2(1:ndime) = coglo2(1:ndime) + shapb(ii)*bocod(1:ndime,ii)
             end do
             xdiff = sqrt( dot_product(coglo2(1:ndime)-coglo(1:ndime),coglo2(1:ndime)-coglo(1:ndime)) )
             if( abs(xdiff) > epsilon(1.0_rp) ) call runend('PROBLEM WITH INTERPOLATION')
          end do

       end do
    end do

  end subroutine elmgeo_test_natural_coordinates_on_boundaries

  !-----------------------------------------------------------------------
  !> @addtogroup Domain
  !> @{
  !> @file    jacobi.f90
  !> @author  Guillaume Houzeaux
  !> @date    27/01/2007
  !> @brief   Computes the Jacobian, Jacobian determinant and
  !           Cartesian derivatives of shape function of an element
  !> @details The jacobian is
  !                              _           _                    
  !                             | dx/ds dx/dt |                t
  !           Jacobian: XJACM = |             | = ELCOD * DERIV 
  !                             |_dy/ds dy/dt_|
  !           with
  !                   _        _             _                    _
  !                  | x1 x2 x3 |           | dN1/ds dN2/ds dN3/ds |
  !          ELCOD = |          |,  DERIV = |                      |
  !                  |_y1 y2 y3_|           |_dN1/dt dN2/dt dN3/dt_|
  !
  !           => Jacobian determinant: GPDET = det(XJACM)  
  !!
  !> @} 
  !-----------------------------------------------------------------------

  subroutine elmgeo_cartesian_derivatives(ndime,pnode,elcod,deriv,gpcar,gpdet)

    integer(ip), intent(in)  :: ndime,pnode
    real(rp),    intent(in)  :: elcod(ndime,pnode),deriv(ndime,pnode)
    real(rp),    intent(out) :: gpcar(ndime,pnode),gpdet
    integer(ip)              :: j,k
    real(rp)                 :: t1,t2,t3,denom
    real(rp)                 :: xjacm(ndime,ndime),xjaci(ndime,ndime)

    if( ndime == 2 .and. pnode == 3 ) then
       !
       ! 2D P1 element
       !
       gpdet = (-elcod(1,1)+elcod(1,2))*(-elcod(2,1)+elcod(2,3)) &
            & -(-elcod(2,1)+elcod(2,2))*(-elcod(1,1)+elcod(1,3))
       if( gpdet == 0.0_rp ) return     
       denom = 1.0_rp/gpdet

       gpcar(1,1) = ( -elcod(2,3) + elcod(2,2) ) * denom
       gpcar(1,2) = ( -elcod(2,1) + elcod(2,3) ) * denom
       gpcar(1,3) = (  elcod(2,1) - elcod(2,2) ) * denom
       gpcar(2,1) = (  elcod(1,3) - elcod(1,2) ) * denom
       gpcar(2,2) = (  elcod(1,1) - elcod(1,3) ) * denom
       gpcar(2,3) = ( -elcod(1,1) + elcod(1,2) ) * denom

    else if( ndime == 3 .and. pnode == 4 ) then
       !
       ! 3D P1 element
       !
       gpcar(1,1) =  elcod(1,2) - elcod(1,1)
       gpcar(1,2) =  elcod(1,3) - elcod(1,1)
       gpcar(1,3) =  elcod(1,4) - elcod(1,1)
       gpcar(2,1) =  elcod(2,2) - elcod(2,1)
       gpcar(2,2) =  elcod(2,3) - elcod(2,1)
       gpcar(2,3) =  elcod(2,4) - elcod(2,1)
       gpcar(3,1) =  elcod(3,2) - elcod(3,1)
       gpcar(3,2) =  elcod(3,3) - elcod(3,1)
       gpcar(3,3) =  elcod(3,4) - elcod(3,1)
       t1         =  gpcar(2,2) * gpcar(3,3) - gpcar(3,2) * gpcar(2,3)
       t2         = -gpcar(2,1) * gpcar(3,3) + gpcar(3,1) * gpcar(2,3)
       t3         =  gpcar(2,1) * gpcar(3,2) - gpcar(3,1) * gpcar(2,2)
       gpdet      =  gpcar(1,1) * t1 + gpcar(1,2) * t2 + gpcar(1,3) * t3
       if( gpdet == 0.0_rp ) return
       denom      =  1.0_rp/gpdet

       xjaci(1,1) =  t1 * denom
       xjaci(2,1) =  t2 * denom
       xjaci(3,1) =  t3 * denom
       xjaci(2,2) = ( gpcar(1,1) * gpcar(3,3) - gpcar(3,1) * gpcar(1,3)) * denom
       xjaci(3,2) = (-gpcar(1,1) * gpcar(3,2) + gpcar(1,2) * gpcar(3,1)) * denom
       xjaci(3,3) = ( gpcar(1,1) * gpcar(2,2) - gpcar(2,1) * gpcar(1,2)) * denom
       xjaci(1,2) = (-gpcar(1,2) * gpcar(3,3) + gpcar(3,2) * gpcar(1,3)) * denom
       xjaci(1,3) = ( gpcar(1,2) * gpcar(2,3) - gpcar(2,2) * gpcar(1,3)) * denom
       xjaci(2,3) = (-gpcar(1,1) * gpcar(2,3) + gpcar(2,1) * gpcar(1,3)) * denom
       gpcar(1,1) = -xjaci(1,1)-xjaci(2,1)-xjaci(3,1)
       gpcar(1,2) =  xjaci(1,1)
       gpcar(1,3) =  xjaci(2,1)
       gpcar(1,4) =  xjaci(3,1)
       gpcar(2,1) = -xjaci(1,2)-xjaci(2,2)-xjaci(3,2)
       gpcar(2,2) =  xjaci(1,2)
       gpcar(2,3) =  xjaci(2,2)
       gpcar(2,4) =  xjaci(3,2)
       gpcar(3,1) = -xjaci(1,3)-xjaci(2,3)-xjaci(3,3)
       gpcar(3,2) =  xjaci(1,3)
       gpcar(3,3) =  xjaci(2,3)
       gpcar(3,4) =  xjaci(3,3)

    else if ( ndime == 1 ) then
       !
       ! 1D
       !
       xjacm(1,1) = 0.0_rp
       do k = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,k) * deriv(1,k)
       end do
       gpdet = xjacm(1,1)
       if( gpdet == 0.0_rp ) return
       xjaci(1,1) = 1.0_rp/xjacm(1,1)
       do j = 1,pnode
          gpcar(1,j) = xjaci(1,1) * deriv(1,j)
       end do

    else if ( ndime == 2 ) then
       !
       ! 2D
       !
       xjacm(1,1) = 0.0_rp
       xjacm(1,2) = 0.0_rp
       xjacm(2,1) = 0.0_rp
       xjacm(2,2) = 0.0_rp
       do k = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,k) * deriv(1,k)
          xjacm(1,2) = xjacm(1,2) + elcod(1,k) * deriv(2,k)
          xjacm(2,1) = xjacm(2,1) + elcod(2,k) * deriv(1,k)
          xjacm(2,2) = xjacm(2,2) + elcod(2,k) * deriv(2,k)
       end do

       gpdet = xjacm(1,1) * xjacm(2,2) - xjacm(2,1) * xjacm(1,2)
       if( gpdet == 0.0_rp ) return
       denom = 1.0_rp/gpdet
       xjaci(1,1) =  xjacm(2,2) * denom
       xjaci(2,2) =  xjacm(1,1) * denom
       xjaci(2,1) = -xjacm(2,1) * denom
       xjaci(1,2) = -xjacm(1,2) * denom  

       do j = 1, pnode
          gpcar(1,j) =   xjaci(1,1) * deriv(1,j) &
               &       + xjaci(2,1) * deriv(2,j)

          gpcar(2,j) =   xjaci(1,2) * deriv(1,j) &
               &       + xjaci(2,2) * deriv(2,j)
       end do

    else if ( ndime == 3 ) then
       !
       ! 3D
       !
       xjacm(1,1) = 0.0_rp ! xjacm = elcod * deriv^t
       xjacm(1,2) = 0.0_rp ! xjaci = xjacm^-1
       xjacm(1,3) = 0.0_rp ! gpcar = xjaci^t * deriv 
       xjacm(2,1) = 0.0_rp
       xjacm(2,2) = 0.0_rp
       xjacm(2,3) = 0.0_rp
       xjacm(3,1) = 0.0_rp
       xjacm(3,2) = 0.0_rp
       xjacm(3,3) = 0.0_rp
       do k = 1,pnode
          xjacm(1,1) = xjacm(1,1) + elcod(1,k) * deriv(1,k)
          xjacm(1,2) = xjacm(1,2) + elcod(1,k) * deriv(2,k)
          xjacm(1,3) = xjacm(1,3) + elcod(1,k) * deriv(3,k)
          xjacm(2,1) = xjacm(2,1) + elcod(2,k) * deriv(1,k)
          xjacm(2,2) = xjacm(2,2) + elcod(2,k) * deriv(2,k)
          xjacm(2,3) = xjacm(2,3) + elcod(2,k) * deriv(3,k)
          xjacm(3,1) = xjacm(3,1) + elcod(3,k) * deriv(1,k)
          xjacm(3,2) = xjacm(3,2) + elcod(3,k) * deriv(2,k)
          xjacm(3,3) = xjacm(3,3) + elcod(3,k) * deriv(3,k)
       end do

       t1    =  xjacm(2,2) * xjacm(3,3) - xjacm(3,2) * xjacm(2,3)
       t2    = -xjacm(2,1) * xjacm(3,3) + xjacm(3,1) * xjacm(2,3)
       t3    =  xjacm(2,1) * xjacm(3,2) - xjacm(3,1) * xjacm(2,2)
       gpdet =  xjacm(1,1) * t1 + xjacm(1,2) * t2 + xjacm(1,3) * t3
       if(gpdet == 0.0_rp ) return
       denom = 1.0_rp / gpdet
       xjaci(1,1) = t1*denom
       xjaci(2,1) = t2*denom
       xjaci(3,1) = t3*denom
       xjaci(2,2) = ( xjacm(1,1) * xjacm(3,3) - xjacm(3,1) * xjacm(1,3)) * denom
       xjaci(3,2) = (-xjacm(1,1) * xjacm(3,2) + xjacm(1,2) * xjacm(3,1)) * denom
       xjaci(3,3) = ( xjacm(1,1) * xjacm(2,2) - xjacm(2,1) * xjacm(1,2)) * denom
       xjaci(1,2) = (-xjacm(1,2) * xjacm(3,3) + xjacm(3,2) * xjacm(1,3)) * denom
       xjaci(1,3) = ( xjacm(1,2) * xjacm(2,3) - xjacm(2,2) * xjacm(1,3)) * denom
       xjaci(2,3) = (-xjacm(1,1) * xjacm(2,3) + xjacm(2,1) * xjacm(1,3)) * denom

       do j = 1, pnode
          gpcar(1,j) =   xjaci(1,1) * deriv(1,j) &
               &       + xjaci(2,1) * deriv(2,j) &
               &       + xjaci(3,1) * deriv(3,j)

          gpcar(2,j) =   xjaci(1,2) * deriv(1,j) &
               &       + xjaci(2,2) * deriv(2,j) &
               &       + xjaci(3,2) * deriv(3,j)

          gpcar(3,j) =   xjaci(1,3) * deriv(1,j) &
               &       + xjaci(2,3) * deriv(2,j) &
               &       + xjaci(3,3) * deriv(3,j)
       end do

    end if

  end subroutine elmgeo_cartesian_derivatives

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Intersection segement-face
  !> @details Compute the intersection point between a segment (xx1,xx2) 
  !>          and a face
  !>          IFOUN = -1 ... Segment in element plane for linear elements
  !>                =  0 ... Segment does not intersect with element
  !>                =  1 ... Segment intersects element
  !>                =  2 ... Segment overlaps element for linear elements
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_intersection_segment_face(&
       ndime,pflty,bocod,xx1,xx2,intersection,t,ifoun,toler)
    integer(ip), intent(in)            :: ndime                   !< Problem dimension
    integer(ip), intent(in)            :: pflty                   !< Face type
    real(rp),    intent(in)            :: bocod(ndime,*)          !< Face node coordinates
    real(rp),    intent(in)            :: xx1(ndime)              !< Segment 1st point
    real(rp),    intent(in)            :: xx2(ndime)              !< Segment 2nd point
    real(rp),    intent(out)           :: intersection(ndime)     !< Coordinates of the intersection point
    real(rp),    intent(out)           :: t                       !< Natural coordinate [0,1] on segment
    integer(ip), intent(out)           :: ifoun                   !< Intersects or not
    real(rp),    intent(in), optional  :: toler                   !< Tolerance
    real(rp)                           :: eps,d1,d2
    integer(ip)                        :: idime
 
    if( present(toler) ) then
       eps = toler
    else
       eps = 0.0_rp
    end if

    if( pflty == BAR02 ) then
       !
       ! BAR02
       !
       call elmgeo_intersection_segment_BAR02(bocod,xx1,xx2,eps,intersection,ifoun)
       !intersection(1:ndime) = bocod(1:ndime,1)
       !do idime = 1, ndime
       !   d1 = xx1(idime)**2.0_rp - bocod(idime,1)**2.0_rp
       !end do
       !do idime = 1, ndime
       !   d2 = xx2(idime)**2.0_rp - bocod(idime,1)**2.0_rp
       !end do
       !d1 = sqrt(d1)
       !d2 = sqrt(d2)
       !t = d1/(d1+d2+0.00000001_rp)
       
    else if( pflty == TRI03 ) then
       !
       ! TRI03 
       !
       call elmgeo_intersection_segment_TRI03(bocod,xx1,xx2,eps,intersection,ifoun)
       
    else if( pflty == QUA04 ) then
       !
       ! QUA04
       !
       call elmgeo_intersection_segment_QUA04(bocod,xx1,xx2,eps,intersection,ifoun)

    else
       
       call runend('ELMGEO_INTERSECTION_ON_A_FACE: UNKNOWN TYPE OF FACE')
       
    end if

  end subroutine elmgeo_intersection_segment_face

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Intersection segement-BAR02
  !> @details Compute the intersection point between a segment (xx1,xx2) 
  !>          and a BAR02 element
  !>          IFOUN = 0 ... Segment does not intersect with element
  !>                = 1 ... Segment intersects element
  !>                = 2 ... Segment overlaps element
  !>
  !>          \verbatim
  !>
  !>          q+s
  !>           _
  !>          |\     p+r
  !>            \    _
  !>             \   /|
  !>              \ /
  !>               o  p + t*r = q + u*s
  !>              / \
  !>             /   \
  !>            /     \ 
  !>          p=xx1  q=bocod(:,1) 
  !>
  !>          \endverbatim
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_intersection_segment_BAR02(&
       bocod,xx1,xx2,toler,intersection,ifoun)
    real(rp),    intent(in)   :: bocod(2,2)      !< Face node coordinates
    real(rp),    intent(in)   :: xx1(2)          !< Segment 1st point
    real(rp),    intent(in)   :: xx2(2)          !< Segment 2nd point
    real(rp),    intent(in)   :: toler           !< Tolerance
    real(rp),    intent(out)  :: intersection(2) !< Coordinates of the intersection point
    integer(ip), intent(out)  :: ifoun           !< Intersects or not
    real(rp)                  :: r(2)
    real(rp)                  :: s(2)
    real(rp)                  :: qmp(2)
    real(rp)                  :: r_cross_s
    real(rp)                  :: qmp_cross_r
    real(rp)                  :: qmp_cross_s
    real(rp)                  :: t0,t1,t,u,r_dot_r
    real(rp)                  :: tmin,tmax
    real(rp)                  :: lmini,lmaxi

    ifoun       = 0
    lmini       = -toler
    lmaxi       = 1.0_rp + toler
    r(1:2)      = xx2(1:2)     - xx1(1:2)
    s(1:2)      = bocod(1:2,2) - bocod(1:2,1)
    qmp(1:2)    = bocod(1:2,1) - xx1(1:2)    
    
    r_cross_s   = r(1)*s(2)   - r(2)*s(1)
    qmp_cross_r = qmp(1)*r(2) - qmp(2)*r(1)

    if( abs(r_cross_s) < epsil .and. abs(qmp_cross_r) < epsil ) then
       !
       ! Two lines are colinear
       !
       r_dot_r = dot_product(r,r) + epsil
       t0      = dot_product(qmp,r)   / r_dot_r
       t1      = dot_product(qmp+s,r) / r_dot_r
       tmin    = min(t0,t1)
       tmax    = max(t0,t1)
       if( tmin <= lmaxi .and. tmax >= lmini ) then
          ifoun = 2
          t     = 0.5_rp * (max(0.0_rp,tmin)+min(1.0_rp,tmax))
       end if

    else if( abs(r_cross_s) < epsil .and. abs(qmp_cross_r) >= epsil ) then
       !
       ! Lines are parallel but non-intersecting
       !
       continue
       
    else
       !
       ! Lines intersect
       !
       qmp_cross_s = qmp(1)*s(2) - qmp(2)*s(1)
       t           = qmp_cross_s / r_cross_s
       u           = qmp_cross_r / r_cross_s
       if(     t >= -toler .and. t <= 1.0_rp + toler .and. &
            &  u >= -toler .and. u <= 1.0_rp + toler ) then
          ifoun = 1
       end if

    end if

    if( ifoun > 0 ) then
       intersection = xx1 + t * r
    else
       intersection = 0.0_rp
    end if

  end subroutine elmgeo_intersection_segment_BAR02

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Intersection segement-BAR02
  !> @details Compute the intersection point between a segment (xx1,xx2) 
  !>          and a TRI03 element
  !>          IFOUN = -1 ... Segment does not intersect with element
  !>                =  0 ... Segment does not intersect with element
  !>                =  1 ... Segment intersects element
  !>                =  2 ... Segment overlaps element
  !
  !-----------------------------------------------------------------------

  subroutine elmgeo_intersection_segment_TRI03(&
       bocod,xx1,xx2,toler,intersection,ifoun)
    real(rp),    intent(in)   :: bocod(3,3)      !< Face node coordinates
    real(rp),    intent(in)   :: xx1(3)          !< Segment 1st point
    real(rp),    intent(in)   :: xx2(3)          !< Segment 2nd point
    real(rp),    intent(in)   :: toler           !< Tolerance
    real(rp),    intent(out)  :: intersection(3) !< Coordinates of the intersection point
    integer(ip), intent(out)  :: ifoun           !< Intersects or not
    real(rp)                  :: u(3)
    real(rp)                  :: v(3)
    real(rp)                  :: w(3)
    real(rp)                  :: n(3)
    real(rp)                  :: r(3)
    real(rp)                  :: lmini,lmaxi
    real(rp)                  :: u_dot_u,v_dot_v
    real(rp)                  :: u_dot_w,v_dot_w,nn
    real(rp)                  :: u_dot_v,s,t,d,a,b

    ifoun =  0
    lmini =  -toler
    lmaxi =  1.0_rp + toler
    u     =  bocod(:,2) - bocod(:,1) 
    v     =  bocod(:,3) - bocod(:,1) 
    r     =  xx2        - xx1
    n(1)  =  u(2)*v(3)  - u(3)*v(2)
    n(2)  =  u(3)*v(1)  - u(1)*v(3)
    n(3)  =  u(1)*v(2)  - u(2)*v(1)
    nn    =  dot_product(n,n)
    b     =  dot_product(n,r)
    a     =  dot_product(n,bocod(:,1)-xx1)

    if( nn < epsil ) then
       !
       ! Triangle degenerates
       !
       ifoun = -1
    else
       if( abs(b) < epsil ) then
          !
          ! Segment parallel to triangle plane
          !
          if( abs(a) < epsil ) then
             ifoun = 2 ! Segment lies in triangle plane
          else
             ifoun = 0 ! Segment disjoint from plane
          end if
       else
          t = a / b
          if( t >= lmini .and. t <= lmaxi ) then
             !
             ! Segment intersects with plane
             !
             intersection = xx1 + t * r
             w            = intersection - bocod(:,1) 
             u_dot_u      = dot_product(u,u)
             v_dot_v      = dot_product(v,v)
             u_dot_v      = dot_product(u,v)
             u_dot_w      = dot_product(u,w)
             v_dot_w      = dot_product(v,w)
             a            = 1.0_rp / ( u_dot_v*u_dot_v - u_dot_u*v_dot_v )
             s            = ( u_dot_v*v_dot_w - v_dot_v*u_dot_w ) * a
             if( s >= lmini .and. s <= lmaxi ) then             
                t = ( u_dot_v*u_dot_w - u_dot_u*v_dot_w ) * a
                if( t >= lmini .and. s+t <= lmaxi ) then
                   ifoun = 1
                end if
             end if
          end if
       end if
    end if

  end subroutine elmgeo_intersection_segment_TRI03

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Intersection segement-BAR02
  !> @details Compute the intersection point between a segment (xx1,xx2) 
  !>          and a TRI03 element. See
  !>          S.D. Ramsey, K. Potter and C. Hansen. 
  !>          "Ray Bilinear Patch Intersections"
  !>          IFOUN =  0 ... Segment does not intersect with element
  !>                =  1 ... Segment intersects element
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_intersection_segment_QUA04(&
       bocod,xx1,xx2,toler,intersection,ifoun)
    real(rp),    intent(in)   :: bocod(3,4)      !< Face node coordinates
    real(rp),    intent(in)   :: xx1(3)          !< Segment 1st point
    real(rp),    intent(in)   :: xx2(3)          !< Segment 2nd point
    real(rp),    intent(in)   :: toler           !< Tolerance
    real(rp),    intent(out)  :: intersection(3) !< Coordinates of the intersection point
    integer(ip), intent(out)  :: ifoun           !< Intersects or not
    real(rp)                  :: a(3)
    real(rp)                  :: b(3)
    real(rp)                  :: c(3)
    real(rp)                  :: d(3)
    real(rp)                  :: q(3)
    real(rp)                  :: r(3)
    real(rp)                  :: p(3)
    real(rp)                  :: lmini,lmaxi
    real(rp)                  :: A1,B1,C1,D1
    real(rp)                  :: A2,B2,C2,D2
    real(rp)                  :: aa,bb,cc,delta
    real(rp)                  :: v1,v2,p1(3),p2(3)
    real(rp)                  :: t,t1,t2,u1,u2
    integer(ip)               :: ii
    logical(lg)               :: patch1,patch2

    a  = bocod(:,3) - bocod(:,2) - bocod(:,4) + bocod(:,1) 
    b  = bocod(:,2) - bocod(:,1)
    c  = bocod(:,4) - bocod(:,1)
    d  = bocod(:,1)
    q  = xx2 - xx1
    r  = xx1

    A1 = a(1)*q(3) - a(3)*q(1)
    B1 = b(1)*q(3) - b(3)*q(1)
    C1 = c(1)*q(3) - c(3)*q(1)
    D1 = ( d(1)-r(1) )*q(3) - ( d(3)-r(3) )*q(1)

    A2 = a(2)*q(3) - a(3)*q(2)
    B2 = b(2)*q(3) - b(3)*q(2)
    C2 = c(2)*q(3) - c(3)*q(2)
    D2 = ( d(2)-r(2) )*q(3) - ( d(3)-r(3) )*q(2)
    !
    ! Quadratic equation for v: aa*v^2 + bb*v + cc = 0
    !
    aa    = A2*C1 - A1*C2
    bb    = A2*D1 - A1*D2 + B2*C1 - B1*C2
    cc    = B2*D1 - B1*D2
    delta = bb*bb - 4.0_rp*aa*cc

    patch1 = .false.
    patch2 = .false.

    if( abs(aa) <= epsil ) then
       !
       ! Linear equation
       !
       v1 = -cc/bb
       call elmgeo_solve_patch(A1,B1,C1,D1,A2,B2,C2,D2,a,b,c,d,q,r,v1,u1,t1,p1,patch1)

    else if( delta >= 0.0_rp ) then

       if( delta <= epsil ) then
          !
          ! One real solution
          !
          v1 = -bb/(2.0_rp*aa)
          call elmgeo_solve_patch(A1,B1,C1,D1,A2,B2,C2,D2,a,b,c,d,q,r,v1,u1,t1,p1,patch1)

       else
          !
          ! Two real solutions
          !
          delta = sqrt(delta)
          v1    = (-bb+delta) / (2.0_rp*aa)
          v2    = (-bb-delta) / (2.0_rp*aa)
          call elmgeo_solve_patch(A1,B1,C1,D1,A2,B2,C2,D2,a,b,c,d,q,r,v1,u1,t1,p1,patch1)
          call elmgeo_solve_patch(A1,B1,C1,D1,A2,B2,C2,D2,a,b,c,d,q,r,v2,u2,t2,p2,patch2)
       end if
    end if

    if( .not. patch1 .and. patch2 ) then
       ifoun        = 1
       intersection = p2             
    else if( .not. patch2 .and. patch1 ) then
       ifoun        = 1
       intersection = p1             
    else if( patch1 .and. patch2 ) then
       if( t1 < t2 ) then
          ifoun        = 1
          intersection = p1
       else
          ifoun        = 1
          intersection = p2
       end if
    end if

  end subroutine elmgeo_intersection_segment_QUA04

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @brief   Patch test 
  !> @details Patch test 
  !>
  !-----------------------------------------------------------------------

  subroutine elmgeo_solve_patch(A1,B1,C1,D1,A2,B2,C2,D2,a,b,c,d,q,r,v,u,t,p,patch)
    real(rp),    intent(in)  :: A1,B1,C1,D1
    real(rp),    intent(in)  :: A2,B2,C2,D2
    real(rp),    intent(in)  :: a(3),b(3),c(3),d(3)
    real(rp),    intent(in)  :: q(3)
    real(rp),    intent(in)  :: r(3)
    real(rp),    intent(in)  :: v
    real(rp),    intent(out) :: u
    real(rp),    intent(out) :: t
    real(rp),    intent(out) :: p(3)
    logical(lg), intent(out) :: patch
    real(rp)                 :: aa,bb

    patch = .false. 

    if( v >= 0.0_rp .and. v <= 1.0_rp ) then
       aa = v*A2 + B2
       bb = v*(A2 - A1) + B2 - B1
       if( abs(bb) >= abs(aa) ) then
          u = (v*(C1-C2)+D1-D2) / bb
       else
          u = (-v*C2-D2) / aa       
       end if
       if( u >= 0.0_rp .and. u <= 1.0_rp ) then
          p = u*v*a + u*b + v*c + d
          if( abs(q(1)) >= abs(q(2)) .and. abs(q(1)) >= abs(q(3)) ) then
             t = (p(1)-r(1)) / q(1)
          else if( abs(q(2)) > abs(q(3)) ) then
             t = (p(2)-r(2)) / q(2)
          else
             t = (p(3)-r(3)) / q(3)
          end if
          if( t >= 0.0_rp .and. t <= 1.0_rp ) patch = .true.
       end if
    end if

  end subroutine elmgeo_solve_patch

end module mod_elmgeo
!> @}
