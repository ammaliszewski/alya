subroutine savdom()
  !-----------------------------------------------------------------------
  !****f* domain/savdom
  ! NAME
  !    savdom
  ! DESCRIPTION
  !    This routine save the element data base:
  !    GPCAR: Cartesian derivatives
  !    GPVOL: Unit volume
  !    GPHES: Hessian matrix
  ! USES
  !    elmcar
  ! USED BY
  !    ***_elmope
  !    extnor
  ! SOURCE
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use mod_memchk
  implicit none
  integer(ip) :: pelty,pnode,pgaus,plapl
  real(rp)    :: elcod(ndime,mnode)
  integer(ip) :: inode,idime,ipoin,ielem
  integer(4)  :: istat
  real(rp)    :: dummr

  if( INOTMASTER .and. kfl_savda == 1 ) then
     !
     ! Allocate memory for data base
     !
     call livinf(78_ip,' ',0_ip)
     allocate(elmda(nelem),stat=istat)

     elements: do ielem = 1,nelem
        !
        ! Element properties and dimensions
        !
        pelty = ltype(ielem)
        pnode = nnode(pelty)
        pgaus = ngaus(pelty)
        plapl = llapl(pelty)
        !
        ! Gather
        !
        do inode = 1,pnode
           ipoin = lnods(inode,ielem)
           do idime = 1,ndime
              elcod(idime,inode) = coord(idime,ipoin)
           end do
        end do
        !
        ! Allocate memory
        !
        allocate(elmda(ielem)%gpcar(ndime,mnode,pgaus),stat=istat)
        call memchk(zero,istat,memor_dom,'ELMDA(IELEM)%GPCAR','savdom',elmda(ielem)%gpcar)

        allocate(elmda(ielem)%gpvol(pgaus),stat=istat)
        call memchk(zero,istat,memor_dom,'ELMDA(IELEM)%GPVOL','savdom',elmda(ielem)%gpvol)

        allocate(elmda(ielem)%hleng(ndime),stat=istat)
        call memchk(zero,istat,memor_dom,'ELMDA(IELEM)%HLENG','savdom',elmda(ielem)%hleng)

        allocate(elmda(ielem)%tragl(ndime,ndime),stat=istat)
        call memchk(zero,istat,memor_dom,'ELMDA(IELEM)%TRAGL','savdom',elmda(ielem)%tragl)
        !
        ! HLENG and TRAGL at center of gravity
        !
        call elmlen(&
             ndime,pnode,elmar(pelty)%dercg,elmda(ielem)%tragl,elcod,&
             hnatu(pelty),elmda(ielem)%hleng)

        if( plapl == 1 ) then
           !
           ! With Hessian: GPVOL, GPCAR, GPHES
           !
           allocate(elmda(ielem)%gphes(ntens,mnode,pgaus),stat=istat)
           call memchk(zero,istat,memor_dom,'ELMDA(IELEM)%GPHES','savdom',elmda(ielem)%gphes)

           call elmcar(&
                pnode,pgaus,plapl,elmar(pelty)%weigp,elmar(pelty)%shape,&
                elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,elmda(ielem) % gpvol,&
                elmda(ielem) % gpcar,elmda(ielem) % gphes,ielem)  

        else
           !
           ! Without Hessian: GPVOL, GPCAR
           !
           call elmcar(&
                pnode,pgaus,plapl,elmar(pelty)%weigp,elmar(pelty)%shape,&
                elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,elmda(ielem) % gpvol,&
                elmda(ielem) % gpcar,dummr,ielem)
             
        end if

     end do elements

     kfl_savda = 2

  end if

end subroutine savdom
