!-----------------------------------------------------------------------
!> @addtogroup DomainInput
!> @{
!> @file    reaset.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Automatically impose element sets
!> @details Assign element sets to elements startinbg from a boundary
!> @} 
!-----------------------------------------------------------------------
subroutine genset()
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_iofile
  use mod_memory
  use mod_elmgeo
  use mod_graphs
  implicit none
  integer(ip)          :: ipoin,ielem,inode,pnode,knode
  integer(ip)          :: kpoin,izdom,jpoin,nmarkt,idime,ifoun
  integer(ip)          :: jdime
  integer(ip)          :: keset,jelem,ieset
  real(rp)             :: dotpr,xplan(4),vect1(3),vect2(3),xinte(3)
  real(rp)             :: dimin,dista,dummr
  integer(ip), pointer :: lside(:) => null()

  If( INOTSLAVE .and. kfl_neset /= 0 ) then

     call livinf(0_ip,'GENERATE ELEMENT SETS AUTOMATICALLY',0_ip)
     !
     ! Allocate memory
     !
     call memory_alloca(memor_dom,'LSIDE','genset',lside,npoin)
     !
     ! Line or plane equation
     !
     if( ndime == 2 ) then
        if( setpo(2,1) /= setpo(2,2) ) then
           xplan(1) = 1.0_rp
           xplan(2) = -(setpo(1,1)-setpo(1,2))/(setpo(2,1)-setpo(2,2))
           xplan(3) = -setpo(1,1)-xplan(2)*setpo(2,1)
        else
           xplan(1) = 0.0_rp
           xplan(2) = 1.0_rp
           xplan(3) = -setpo(2,1)
        end if
     else
        do idime = 1,ndime
           vect1(idime) = setpo(idime,2) - setpo(idime,1)
           vect2(idime) = setpo(idime,3) - setpo(idime,1)
        end do
        call vecpro(vect1,vect2,xplan,ndime)
        xplan(4) = -xplan(1)*setpo(1,1)-xplan(2)*setpo(2,1)-xplan(3)*setpo(3,1)  
     end if
     !
     ! Find intersection edges IPOIN-JPOIN: LSIDE(IPOIN/JPOIN) = -1
     !
     do ipoin = 1,npoin
        do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
           jpoin = c_dom(izdom)
           if( ipoin < jpoin ) then
              call elmgeo_segpla(&
                   ndime,xplan,coord(1:ndime,ipoin),coord(1:ndime,jpoin),ifoun,xinte,1.0e-3_rp)
              if( ifoun /= 0 ) then
                 lside(ipoin) = -1
                 lside(jpoin) = -1
              end if
           end if
        end do
     end do
     !
     ! Look for starting node KPOIN. Take the nearest node to the barycenter of the
     ! nodes defining the plane/line located in the direction of the normal
     !
     kpoin = 0
     dimin = 1.0e12_rp
     do jdime = 1,ndime
        do idime = 1,ndime
           setpo(jdime,4) = setpo(jdime,4) + setpo(jdime,idime) 
        end do
        setpo(jdime,4) = setpo(jdime,4) / real(ndime,rp)
     end do
     loop_nodes: do ipoin = 1,npoin
        if( lside(ipoin) == 0 ) then
           dotpr = 0.0_rp
           dista = 0.0_rp
           do idime = 1,ndime
              dummr = ( coord(idime,ipoin) - setpo(idime,4) )
              dotpr = dotpr + dummr * setno(idime)
              dista = dista + dummr * dummr
           end do
           if( dotpr > 0.0_rp .and. dista < dimin ) then
              dimin = dista
              kpoin = ipoin
           end if
        end if
     end do loop_nodes
     if( kpoin == 0 ) call runend('GENSET: COULD NOT FIND STARTING NODE')
     !
     ! Mark graph recursively
     !
     call graphs_recurs(npoin,r_dom,c_dom,lside,kpoin)
     !
     ! Mark neighboring nodes of marked nodes
     !
     do ipoin = 1,npoin
        if( lside(ipoin) == 1 ) then
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              jpoin = c_dom(izdom)
              if( lside(jpoin) == -1 ) lside(jpoin) = 2 
           end do
        end if
     end do
     !
     ! LSIDE(IPOIN) = 0 or 1
     !
     do ipoin = 1,npoin
        lside(ipoin) = max(lside(ipoin),0_ip)
        lside(ipoin) = min(lside(ipoin),1_ip)
     end do
     nmarkt = 0
     do ipoin = 1,npoin
        nmarkt = nmarkt + lside(ipoin)
     end do
     if( nmarkt == npoin ) call runend('GENSET: ALL ELEMENTS ARE IN SAME SET')
     !
     ! Assign set  LSIDE => LESET
     !
     do ielem = 1,nelem   
        knode = 0
        pnode = lnnod(ielem)
        do inode = 1,lnnod(ielem)
           ipoin = lnods(inode,ielem)
           knode = knode + lside(ipoin)
        end do
        if( knode == pnode ) leset(ielem) = kfl_neset
     end do
     !
     ! Recompute LESEC.
     ! Count number of element sets and define the element set connectivity LESEC
     ! Local # body of ielem = LESEC(LESET(IELEM))
     !
     call memose(-5_ip)
     neset = 0
     do ielem = 1,nelem
        if( leset(ielem) > 0 ) then
           neset = neset + 1
           keset = leset(ielem)
           do jelem = ielem,nelem
              if( leset(jelem) == keset ) leset(jelem) = -keset
           end do
        end if
     end do
     call memose(5_ip)
     ieset = 0
     do ielem = 1,nelem
        if( leset(ielem) < 0 ) then
           ieset = ieset + 1
           keset = leset(ielem)
           do jelem = ielem,nelem
              if( leset(jelem) == keset ) leset(jelem) = -keset
           end do
           lesec(ieset) = leset(ielem)
        end if
     end do
     !
     ! Deallocate memory
     !
     call memory_deallo(memor_dom,'LSIDE','genset',lside)

  end if

end subroutine genset
