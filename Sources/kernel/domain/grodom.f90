!----------------------------------------------------------------------
!> @addtogroup kernel/domain
!> @{
!> @file      grodom.f90
!> @author
!> @date 
!> @brief     Computing the groups for the defelated CG
!> @details   This routine computes the groups for the deflated CG.\n
!!    Starting nodes are those node ipoin imposed, that is when\n
!!    LIMPO(ipoin)>0.\n 
!!    We have two possiblities:\n
!!    ITASK = 1 ... NGROU_DOM groups are computed by master\n
!!                  NGROU_DOM = -1: automatic # groups\n
!!    ITASK = 2 ... Groups are computed by slaves:\n
!!                  One group per subdomain\n
!!        
!!    The main variables are 4:\n
!!    
!!     lqueu(:):   array containing the points that a group has.\n
!!     lgrou(:):   array containg the number of gruops (max. number of groups = max. number of points)\n 
!!     lfron(:):   contains the points that are in the front.\n
!!     lmark(:):   contains the points which do not belong to a group.\n
!!
!!     Description of other variables:\n
!!
!!      ipoin:  point number (it is related with a node)\n
!!      npogr:  number of points per group. \n
!!      npomi:  minum number of points per group. \n
!!      nmark:  number of marked points. \n
!!      nmarkt: total number of marked points. \n
!!      kpoin:  point number in lqueu. \n 
!!      kgrou:  group number. \n
!!      nfron, nfnew: maximum number of points in lfron(:) in each loop (maxnfron = npoin)\n
!!      jpoin, iqueu, jqueu, igrou: counting variables.\n
!! 
!!      Description of the main loop:\n
!!
!!      open-loop:\n
!!          neigh (loop):...................Loop on the queue. Computes de nonzero ipoin, looks if they belong to a group,\n
!!                                          if the group is full and if they are marked, allocating them in lqueu.\n
!!          Adding, cleaning and\n
!!          compressing nodes of the front..The last point added is then used to make the new front. This is done in three steps:\n
!!                                               1. First looks for the nonzero points of the mesh that follow this last point \n
!!                                                  (izdom = r_dom(ipoin), r_dom(ipoin+1)-1)\n
!!                                               2. Then looks if they belong to a group and if they are marked (HOLE point),\n
!!                                                  if these two are satisfied, then allocates the point in lgrou, computing the new value\n
!!                                                  for nfron. \n
!!                                               3. Cleans lgrou (unmarking the points)\n
!!                                               4. Computes the new fornt nfnew \n
!!                                          using the same array in lfron in each loop to allocate the point in the front\n
!!          Not enough nodes................Looks for nodes that belong to other groups using the last points added.\n
!!          Final check.....................Checks if all the points are marked anf if not looks for the ones that are not marked.\n
!!                                  
!!         
!!                                                         
!>  @}
!-----------------------------------------------------------------------
subroutine grodom(itask)
 
  use def_parame
  use def_master
  use def_domain
  use mod_memchk  
  use def_elmtyp
  use mod_postpr
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,nqueu,npogr,igrou,jgrou,iqueu
  integer(ip)             :: kgrou,kpoin,izdom,jpoin,npomi
  integer(ip)             :: nfron,nfold,jqueu,ifront,nfnew,icheck
  integer(ip)             :: nmarkt,nmark
  real(rp)                :: f,g,h
  integer(4)              :: istat
  integer(ip), pointer    :: lqueu(:) 
  integer(ip), pointer    :: lfron(:) 
  integer(ip), pointer    :: lgrou(:) 
  logical(lg), pointer    :: lmark(:) 

  nullify(lqueu)
  nullify(lfron)
  nullify(lgrou)
  nullify(lmark)

  if( INOTSLAVE .and. ( ngrou_dom == -1 .or. ngrou_dom > 0 ) .and. itask == 1 ) then

     !----------------------------------------------------------------------
     !
     ! Allocate memory 
     !
     !----------------------------------------------------------------------

     call livinf(57_ip,' ',modul)
     !
     ! Automatic number of groups
     !
     if( ngrou_dom == -1 ) then
        f = real(npoin) / 2000.0_rp + 10.0_rp
        g = 50.0_rp * log(real(npoin))
        call mixing(1_ip,h,1.0e-6,1.0e6,real(npoin))
        ngrou_dom = int( g*h+(1.0_rp-h)*f )
     end if
     !
     ! LGROU: allocate memory
     !     
     call memgeo(27_ip)
     lgrou => lgrou_dom
     nmarkt = npoin 
     !
     ! Fill in LQUEU, LMARK, LFRON
     !
     allocate(lqueu(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LQUEU','grodom',lqueu)
     allocate(lmark(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LMARK','grodom',lmark)
     allocate(lfron(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LFRON','grodom',lfron)
     do ipoin = 1,npoin
        lmark(ipoin) = .false.
     end do 
     !
     ! Limit number of groups to number of nodes
     !
     if( ngrou_dom >= npoin ) then
        do ipoin = 1,npoin
           lgrou(ipoin) = ipoin
        end do
        ngrou_dom = npoin
        goto 999
     end if
     !
     ! Mark hole nodes (they are like imposed Dirichlet nodes) 
     !
     do ipoin = 1,npoin
        if( lnoch(ipoin) == NOHOL ) lgrou(ipoin) = -1
     end do  
     !
     ! Count imposed nodes
     !
     do ipoin = 1,npoin
        if( lgrou(ipoin) == -1 ) then
           nmarkt = nmarkt - 1
           lmark(ipoin) = .true.
        end if
     end do
     !
     ! Compute points per group and minimal threshold
     !
     npogr = max(npoin/ngrou_dom,1_ip)
     npomi = npogr/3
     !
     ! Find first non marked point
     !
     kpoin = 1

     !----------------------------------------------------------------------
     !
     ! Construct groups
     !
     !----------------------------------------------------------------------
     !
     !  Initialize main loop
     !
     nmark        = 0
     igrou        = 1
     lqueu(1)     = kpoin
     lgrou(kpoin) = igrou
     nmark        = nmark + 1
     nqueu        = 1
     iqueu        = 1
     nfron        = 0

     open_loop: do

        !
        ! Loop on queue
        ! 

        neigh: do 
           ipoin = lqueu(iqueu)
           !
           ! Loop on neighbors
           ! 
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              jpoin = c_dom(izdom)
              !
              ! Has the group of this point been assigned before ?
              !
              if( lgrou(jpoin) == 0 ) then
                 !
                 ! Did we reach the maximum number of points per group
                 !
                 if( nqueu == npogr ) exit neigh
                 lgrou(jpoin) = igrou
                 nmark        = nmark + 1    
                 nqueu        = nqueu + 1
                 lqueu(nqueu) = jpoin
                 !
                 ! Does this point belong to the current front
                 !
                 if( .not. lmark(jpoin) ) lmark(jpoin) = .true.
              endif
           end do
           !
           ! Did we exhaust the queue?
           !        
           if( iqueu == nqueu ) exit neigh
           iqueu = iqueu + 1

        end do neigh
        !
        ! Add nodes to the front
        !
        nfold = nfron + 1

        do jqueu = iqueu,nqueu
           ipoin = lqueu(jqueu)
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              jpoin = c_dom(izdom)
              if( lgrou(jpoin) == 0 ) then
                 if( .not. lmark(jpoin) ) then
                    nfron = nfron + 1
                    if( nfron > npoin ) nfron = npoin
                    lfron(nfron) = jpoin
                    lmark(jpoin) = .true.
                 end if
              end if
           end do
        end do
        !
        ! Clean up the last points
        !
        do ifront = nfold,nfron
           ipoin = lfron(ifront)
           lmark(ipoin) = .false.
        end do
        !
        ! Compress the front
        !
        nfnew = 0
        do ifront = 1,nfron
           ipoin = lfron(ifront)
           if( .not. lmark(ipoin) ) then
              nfnew = nfnew + 1
              lfron(nfnew) = lfron(ifront)
           end if
        end do
        nfron = nfnew
        !
        ! Special case: Do we have enough nodes   
        !
        if( nqueu < npomi ) then
           !
           ! Find a neighboring group
           !
           jgrou = 0
           glue: do iqueu = 1,nqueu
              ipoin = lqueu(iqueu)
              do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                 jpoin = c_dom(izdom)
                 jgrou = lgrou(jpoin)
                 if( jgrou /= igrou .and. jgrou /= -1 ) then
                    exit glue
                 endif
              enddo
           end do glue

           if( jgrou == 0 ) then
              ! This could be a lost node!
              lgrou(ipoin) = -1
              !call runend('GRODOM: PROBLEM WITH MESH')
           else
              do ipoin = 1,npoin
                 kgrou = lgrou(ipoin)
                 if( kgrou == igrou ) then
                    lgrou(ipoin) = jgrou 
                 end if
              end do
           end if

           nqueu = 0
           igrou = igrou-1

        endif
        !
        ! Is the front empty --> go home
        !
        if( nfron == 0 ) then
           !
           ! Did we mark all the points
           !
           if( nmark == nmarkt ) then
              !
              ! Is the front empty --> go home
              !
              exit open_loop

           else
              !
              ! Find a non marked point
              !
              icheck = 0_ip
              do ipoin = 1,npoin
                 if( lgrou(ipoin) == 0 )then
                    lfron(1) = ipoin
                    icheck = 1_ip
                    exit
                 end if
              end do
              if( icheck == 0 ) then
                 call runend('GRODOM: nmark/=nmarkt and point not found')
              end if
           end if

        end if
        !
        ! Find new seed
        !
        jpoin = lfron(1)
        !
        ! Initialize new round
        !
        igrou        = igrou+1
        lqueu(1)     = jpoin
        lgrou(jpoin) = igrou
        nmark        = nmark + 1    
        nqueu        = 1
        iqueu        = 1 
        lmark(jpoin) = .true.

     end do open_loop
     !
     ! Redefine ngrou
     !
     ngrou_dom = max(igrou,ngrou_dom)
     !
     ! LQUEU, LMARK, LFRON: Deallocate memory
     !
999 continue
     call memchk(2_ip,istat,memor_dom,'LQUEU','grodom',lqueu)
     deallocate(lqueu,stat=istat)
     if(istat/=0) call memerr(2_ip,'LQUEU','grodom',0_ip)

     call memchk(2_ip,istat,memor_dom,'LMARK','grodom',lmark)
     deallocate(lmark,stat=istat)
     if(istat/=0) call memerr(2_ip,'LMARK','grodom',0_ip)

     call memchk(2_ip,istat,memor_dom,'LFRON','grodom',lfron)
     deallocate(lfron,stat=istat)
     if(istat/=0) call memerr(2_ip,'LFRON','grodom',0_ip)

  else if( ngrou_dom == -2 .and. itask == 2 ) then

     !----------------------------------------------------------------------
     !
     ! 1 group per subdomaim. On non-own boundary nodes, take the group
     ! defined by my neighbors.
     !
     !----------------------------------------------------------------------

     ngrou_dom = max(1_ip,npart)
     if( INOTMASTER ) then
        call memgeo(27_ip)
        if( ISLAVE ) then
           do ipoin = 1,npoi1
              lgrou_dom(ipoin) = kfl_paral
           end do
           do ipoin = npoi2,npoi3
              lgrou_dom(ipoin) = kfl_paral
           end do
           call parari('SLX',NPOIN_TYPE,npoin,lgrou_dom)
        else
           do ipoin = 1,npoi1
              lgrou_dom(ipoin) = 1
           end do
        end if
     else
        call livinf(57_ip,' ',modul)
     end if

  end if

end subroutine grodom
