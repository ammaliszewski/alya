subroutine export_bound
  !-----------------------------------------------------------------------
  !****f* Domain/dombou
  ! NAME
  !    dombou
  ! DESCRIPTION
  !    Compute boundary arrays automatically
  ! OUTPUT
  !    NBOUN .................. # of boundaries
  !    LNODB(MNODB,NBOUN) ..... Boundary connectivity
  !    LBOEL(MNODB+1,NBOUN) ... Boundary/Element connectivity
  !    LTYPB(NBOUN) ........... Type of boundary
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      mod_memchk
  use mod_elmgeo, only : element_type
  implicit none 
  integer(ip), pointer     :: faces(:,:,:),bomsh(:,:)
  integer(ip)              :: ielty,ielem,iboun,iface,inodb,ilist
  integer(ip)              :: inode,jelem,jface,jelty,ipoin,jpoin,iblty
  integer(ip)              :: pepoi,ielpo,pnodb
  integer(4)               :: istat
  logical(lg)              :: equal_faces
  !
  ! Allocate memory for FACES, BOMSH
  !
  nullify(faces)
  nullify(bomsh)
  allocate(faces(mnodb,mface,nelem),STAT=istat)
  call memchk(zero,istat,memor_dom,'FACES','dombou',faces)
  allocate(bomsh(mface,nelem),      STAT=istat)
  call memchk(zero,istat,memor_dom,'BOMSH','dombou',bomsh)
  !
  ! Construct and sort FACES
  !
  do ielem = 1,nelem                                         
     ielty = ltype(ielem)
     do iface = 1,element_type(ielty) % number_faces
        pnodb = element_type(ielty) % node_faces(iface)           
        do inodb = 1,pnodb
           inode = element_type(ielty) % list_faces(inodb,iface)
           faces(inodb,iface,ielem)=lnods(inode,ielem)
        end do
        call sortin(pnodb,faces(1,iface,ielem))
     end do
  end do
  !
  ! Compute NBOUN and fill in BOMSH
  !
  nboun=0
  do ielem=1,nelem                                            ! Compare the faces and 
     ielty=ltype(ielem)                                       ! eliminate the repited faces
     do iface=1,element_type(ielty) % number_faces
        ipoin=faces(1,iface,ielem)
        if(ipoin/=0) then
           ilist=1
           pepoi=pelpo(ipoin+1)-pelpo(ipoin)
           ielpo=pelpo(ipoin)-1
           do while(ilist<=pepoi)
              ielpo=ielpo+1
              jelem=lelpo(ielpo)
              if(jelem/=ielem) then
                 jelty=ltype(jelem)                              ! eliminate the repited faces
                 jface=0
                 do while(jface/=element_type(jelty) % number_faces)
                    jface=jface+1
                    if(faces(1,jface,jelem)/=0) then
                       equal_faces=.true.
                       inodb=0
                       do while(equal_faces.and.inodb/=element_type(jelty) % node_faces(jface))
                          inodb=inodb+1
                          if(faces(inodb,iface,ielem) /=faces(inodb,jface,jelem))&
                               equal_faces=.false.
                       end do
                       if(equal_faces) then
                          faces(1,iface,ielem)=0                   ! IFACE and JFACE
                          faces(1,jface,jelem)=0                   ! are eliminated
                          jface=element_type(jelty) % number_faces ! Exit JFACE do
                          ilist=pepoi                              ! Exit JELEM do
                       end if
                    end if
                 end do
              end if
              ilist=ilist+1
           end do
           if(faces(1,iface,ielem)/=0) then
              nboun=nboun+1
              do inodb=1,element_type(ielty) % node_faces(iface) 
                 inode=element_type(ielty) % list_faces(inodb,iface)
                 faces(inodb,iface,ielem)=lnods(inode,ielem)
              end do
              bomsh(iface,ielem)=nboun
           end if
        end if
     end do
  end do
  !
  ! Allocate memory for LNODB, LBOEL and LTYPB
  ! 
  allocate(lnodb(mnodb,nboun),  stat=istat)
  call memchk(zero,istat,memor_dom,'LNODB','dombou',lnodb)
  allocate(lboel(mnodb+1,nboun),stat=istat)
  call memchk(zero,istat,memor_dom,'LBO2L','dombou',lboel)
  allocate(ltypb(nboun),        stat=istat)
  call memchk(zero,istat,memor_dom,'LTYPB','dombou',ltypb)
  !
  ! Compute LNODB, LBOEL AND LTYPB
  !
  do ielem=1,nelem
     ielty=ltype(ielem)
     do iface=1,element_type(ielty) % number_faces
        if(faces(1,iface,ielem)/=0) then  
           call fintyp(ndimb,element_type(ielty) % node_faces(iface),iblty)
           iboun=bomsh(iface,ielem)
           lboel(nnode(iblty)+1,iboun)=ielem
           ltypb(iboun)=iblty
           do inodb=1,nnode(iblty) 
              lnodb(inodb,iboun)=faces(inodb,iface,ielem)
           end do
           do inodb=1,nnode(iblty)
              ipoin=lnodb(inodb,iboun)
              nodes: do inode=1,nnode(ielty)
                 jpoin=lnods(inode,ielem)
                 if(ipoin==jpoin) then
                    lboel(inodb,iboun)=inode
                    exit nodes
                 end if
              end do nodes
           end do

        end if
     end do
  end do
  !
  !  DEBUG: WRITE LNODB WITH LBOEL (LEAVE IT COMMENTED UNLESS YOU NEED IT)
  !
  open(978,file='lnodb-lboel.out')
  write(978,*) 'BOUNDARIES, ELEMENTS'
  do iboun=1,nboun
     iblty=ltypb(iboun)
     write(978,*) iboun, lnodb(1:nnode(iblty),iboun),lboel(nnode(iblty)+1,iboun)
  end do
  write(978,*) 'END_BOUNDARIES'
  close(978)

  ! Leave

  call runend('EXPORT_BOUND: lnods-lboel.out FILE WRITTEN. DEBUG MODE!!!')

end subroutine export_bound

