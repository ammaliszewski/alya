!------------------------------------------------------------------------
!> @addtogroup MatrixToolBox
!! @{
!> @name    ToolBox for matrix operations
!! @file    mod_matrix.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!! @brief   ToolBox for matrix operations
!! @details ToolBox for matrix operations: fill in, etc.
!! @{
!------------------------------------------------------------------------

module mod_matrix
  use def_kintyp, only : ip,rp,lg
  use mod_memory, only : memory_alloca,memory_deallo
  use def_master, only : INOTMASTER,NPOIN_TYPE
  implicit none

  private

  interface matrix_initialize
     module procedure matrix_initialize_rp_1,&
          &           matrix_initialize_rp_2,&
          &           matrix_initialize_rp_3
  end interface matrix_initialize
 
  public :: matrix_initialize
  public :: matrix_asscsr
  public :: matrix_assrhs
  public :: matrix_assexp
  public :: matrix_blokgs
  public :: matrix_invdia
  public :: matrix_matgr2
  public :: matrix_output_gid_format

contains

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    26/09/2014
  !> @brief   Initialize a matrix (or array)
  !> @details Initialize a matrix (or array)
  !
  !----------------------------------------------------------------------

  subroutine matrix_initialize_rp_1(aa)
    real(rp),   pointer :: aa(:)
    integer(ip)         :: i1
    do i1 = 1,size(aa)
       aa(i1) = 0.0_rp
    end do
  end subroutine matrix_initialize_rp_1
  subroutine matrix_initialize_rp_2(aa)
    real(rp),   pointer :: aa(:,:)
    integer(ip)         :: i1,i2
    do i2 = 1,size(aa,2)
       do i1 = 1,size(aa,1)
          aa(i1,i2) = 0.0_rp
       end do
    end do
  end subroutine matrix_initialize_rp_2
  subroutine matrix_initialize_rp_3(aa)
    real(rp),   pointer :: aa(:,:,:)
    integer(ip)         :: i1,i2,i3
    do i3 = 1,size(aa,3)
       do i2 = 1,size(aa,2)
          do i1 = 1,size(aa,1)
             aa(i1,i2,i3) = 0.0_rp
          end do
       end do
    end do
  end subroutine matrix_initialize_rp_3

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Agglomerate a matrix
  !> @details Agglomerate a matrix
  !
  !----------------------------------------------------------------------

  subroutine matrix_matgr2(&
       npoin,ngrou,ndofn,lgrou,ia,ja,iagroup,jagroup,aa,aagroup,memit)
    implicit none
    integer(ip),           intent(in)    :: npoin               !< Number of nodes
    integer(ip),           intent(in)    :: ngrou               !< Number of groups
    integer(ip),           intent(in)    :: ndofn               !< Nb of dof
    integer(ip),           intent(in)    :: lgrou(:)            !< List of groups
    integer(ip),           intent(in)    :: ia(*)
    integer(ip),           intent(in)    :: ja(*)
    integer(ip), pointer,  intent(in)    :: iagroup(:)
    integer(ip), pointer,  intent(in)    :: jagroup(:)
    real(rp),              intent(in)    :: aa(ndofn,ndofn,*)
    real(rp),    pointer,  intent(out)   :: aagroup(:,:,:)
    integer(8),  optional, intent(inout) :: memit(2)            !< Memory counter
    integer(ip)                          :: idofn,izdom,izgro
    integer(ip)                          :: nzgro,igrou,jgrou
    integer(ip)                          :: jdofn,ipoin,jpoin
    integer(8)                           :: memor(2)
    logical(lg), pointer                 :: lchek(:) => null()
    !
    ! Allocate matrix if necessary
    !
    if( .not. associated(aagroup) ) then
       nzgro = size(jagroup)
       if( present(memit) ) then
          call memory_alloca(memit,'AAGROUP','matrix_matgro',aagroup,ndofn,ndofn,nzgro)
       else
          memor = 0
          call memory_alloca(memor,'AAGROUP','matrix_matgro',aagroup,ndofn,ndofn,nzgro)          
       end if
    end if
    !
    ! Check array
    !
    allocate( lchek(ngrou) )
    do igrou = 1,ngrou
       lchek(igrou) = .true.
    end do
    !
    ! Agglomerate matrix AAGROUP <= AA
    !
    if( ndofn == 1 ) then

       do ipoin = 1,npoin
          if( lgrou(ipoin) > 0 ) then
             igrou = lgrou(ipoin)
             do izdom = ia(ipoin),ia(ipoin+1)-1
                jpoin = ja(izdom)
                if( lgrou(jpoin) > 0 ) then
                   jgrou = lgrou(jpoin)
                   izgro = iagroup(igrou)
                   iifzgro1: do while( izgro <= iagroup(igrou+1)-1 )
                      if( jagroup(izgro) == jgrou ) exit iifzgro1
                      izgro = izgro + 1
                   end do iifzgro1
                   lchek(igrou) = .false.
                   aagroup(1,1,izgro) = aagroup(1,1,izgro) + aa(1,1,izdom)
                end if
             end do
          end if
       end do

    else

       do ipoin = 1,npoin
          if( lgrou(ipoin) > 0 ) then
             igrou = lgrou(ipoin)
             do izdom = ia(ipoin),ia(ipoin+1)-1
                jpoin = ja(izdom)
                if( lgrou(jpoin) > 0 ) then
                   jgrou = lgrou(jpoin)
                   izgro = iagroup(igrou)
                   iifzgro2: do while( izgro <= iagroup(igrou+1)-1 )
                      if( jagroup(izgro) == jgrou ) exit iifzgro2
                      izgro = izgro + 1
                   end do iifzgro2
                   lchek(igrou) = .false.
                   do jdofn = 1,ndofn
                      do idofn = 1,ndofn
                         aagroup(idofn,jdofn,izgro) = aagroup(idofn,jdofn,izgro) &
                              + aa(idofn,jdofn,izdom)
                      end do
                   end do
                end if
             end do
          end if
       end do

    end if
    !
    ! Check if group has a non-zero on its row
    !
    do igrou = 1,ngrou
       if( lchek(igrou) ) then
          izgro = iagroup(igrou)
          iifzgro3: do while( izgro <= iagroup(igrou)+1-1 )
             if( jagroup(izgro) == igrou ) exit iifzgro3
             izgro = izgro + 1
          end do iifzgro3
          do idofn = 1,ndofn
             aagroup(idofn,idofn,izgro) = 1.0_rp 
          end do
       end if
    end do
    deallocate(lchek)
    !
    ! Output matrix in ps format
    !
    !call pspltm(&
    !     ngrou,ngrou,1_ip,0_ip,jagroup,iagroup,aagroup,&
    !     'caca',0_ip,18.0_rp,'cm',&
    !     0_ip,0_ip,2_ip,99)

  end subroutine matrix_matgr2


  !-----------------------------------------------------------------------
  !
  !> @brief   Check the symmetry of a matrix
  !> @details Check the symmetry of a matrix
  !> @date    04/07/2012
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine matrix_chksym(nn,nbvar,ia,ja,aa,xsymm,memit)
    use def_kintyp, only :  ip,rp
    implicit none
    integer(ip),           intent(in)    :: nn
    integer(ip),           intent(in)    :: nbvar
    integer(ip),           intent(in)    :: ia(*)
    integer(ip),           intent(in)    :: ja(*)
    real(rp),              intent(in)    :: aa(nbvar,nbvar,*)
    real(rp),              intent(out)   :: xsymm
    integer(8),  optional, intent(inout) :: memit(2)            !< Memory counter
    integer(ip)                          :: nz,ii,kk,ll,jj,qq,mm
    real(rp),    pointer                 :: bb(:,:) 
    real(rp)                             :: xx(nbvar)
    integer(8)                           :: memor(2)

    nullify(bb)
    nz = (ia(nn+1)-1)*nbvar*nbvar
    if( present(memit) ) then
       call memory_alloca(memit,'BB','matrix_chksym',bb,nbvar,nz)
    else
       memor = 0
       call memory_alloca(memor,'BB','matrix_chksym',bb,nbvar,nz)       
    end if

    do ii = 1,nn
       do kk = ia(ii),ia(ii+1)-1
          jj = ja(kk)
          if( jj /= ii ) then
             do ll = ia(jj),ia(jj+1)-1
                if( ja(ll) == ii ) then
                   if( abs(aa(1,1,kk)-aa(1,1,ll)) > 1.0e-12_rp ) then
                      bb(1,kk) = abs(aa(1,1,kk)-aa(1,1,ll))&
                           &        /(0.5_rp*abs(aa(1,1,kk))+abs(aa(1,1,ll)))
                      bb(1,ll) = bb(1,kk)
                      if( nbvar > 1 ) then
                         do qq = 2,nbvar
                            if( abs(aa(qq,qq,kk))+abs(aa(qq,qq,ll)) > 1.0e-12_rp ) then
                               bb(qq,kk) = abs(aa(qq,qq,kk)-aa(qq,qq,ll))&
                                    &         /(0.5_rp*abs(aa(qq,qq,kk))+abs(aa(qq,qq,ll)))
                               bb(qq,ll) = bb(qq,kk)
                            end if
                         end do
                      end if
                   end if
                end if
             end do
          end if
       end do
    end do
    !
    ! Average symmetry
    !
    mm = 0
    xx = 0.0_rp
    do ii = 1,nn
       do kk = ia(ii),ia(ii+1)-1
          jj = ja(kk)
          if( jj /= ii ) then
             mm = mm + 1
             do qq = 1,nbvar
                xx(qq) = xx(qq) + bb(qq,kk)
             end do
          end if
       end do
    end do
    xsymm = 0.0_rp
    do qq = 1,nbvar
       xsymm  = xsymm + xx(qq) / real(mm,rp)
    end do
    xsymm = xsymm / real(nbvar,rp)
    call memory_deallo(memit,'BB','matrix_chksym',bb)

  end subroutine matrix_chksym

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Block Gauss-Seidel
  !> @details Block Gauss-Seidel
  !
  !> To come back to the previous version (nblok=ndofn) just put ndonf_n_per_block = 1
  !
  !----------------------------------------------------------------------

  subroutine matrix_blokgs(&
       itask,nequa,ndofn_per_block,nblok,iblok,ia,ja,aa,bb,xx,&
       aa_aux,bb_idofn,xx_idofn,memit,lperm)
    implicit none
    integer(ip),          intent(in)    :: itask                                   !< Small system or solution update
    integer(ip),          intent(in)    :: nequa                                   !< Number of nodes
    integer(ip),          intent(in)    :: ndofn_per_block                         !< Number of dof per node in the block
    integer(ip),          intent(in)    :: nblok                                   !< Nb of dof
    integer(ip),          intent(in)    :: iblok                                   !< Dof to be solved
    integer(ip),          intent(in)    :: ia(*)                                   !< CSR format
    integer(ip),          intent(in)    :: ja(*)                                   !< CSR format
    real(rp),             intent(in)    :: aa(ndofn_per_block*nblok,ndofn_per_block*nblok,*)   !< Original matrix
    real(rp),             intent(in)    :: bb(ndofn_per_block*nblok,*)                   !< Original RHS
    real(rp),             intent(inout) :: xx(ndofn_per_block*nblok,*)                   !< Original solution
    real(rp),    pointer                :: aa_idofn(:,:,:)                             !< IDOFN Block system matrix
    real(rp),    pointer, intent(out)   :: aa_aux(:)
    real(rp),    pointer, intent(out)   :: bb_idofn(:)                             !< IDOFN Block system RHS
    real(rp),    pointer, intent(out)   :: xx_idofn(:)                             !< IDOFN Block system solution
    integer(8),  optional,intent(inout) :: memit(2)                                !< Memory counter
    integer(ip)                         :: iequa,iz,jequa,cont,jcont
    integer(ip)                         :: nz,jdofn,icont
    integer(8)                          :: memor(2)
    integer(ip), pointer, optional      :: lperm(:,:)                              !< permutation vector

    select case ( itask ) 

    case ( 1_ip ) 
       !
       ! Allocate if necessary
       !
       nz = ia(nequa+1)-1
       if( present( memit ) ) then
          if( .not. associated(aa_idofn) ) call memory_alloca(memit,'AA_IDOFN','matrix_blokgs',aa_idofn,ndofn_per_block,ndofn_per_block,nz)
          if( .not. associated(xx_idofn) ) call memory_alloca(memit,'XX_IDOFN','matrix_blokgs',xx_idofn,ndofn_per_block*nequa)
          if( .not. associated(bb_idofn) ) call memory_alloca(memit,'BB_IDOFN','matrix_blokgs',bb_idofn,ndofn_per_block*nequa)
       else
          memor = 0
          if( .not. associated(aa_idofn) ) call memory_alloca(memor,'AA_IDOFN','matrix_blokgs',aa_idofn,ndofn_per_block,ndofn_per_block,nz)
          if( .not. associated(xx_idofn) ) call memory_alloca(memor,'XX_IDOFN','matrix_blokgs',xx_idofn,ndofn_per_block*nequa)
          if( .not. associated(bb_idofn) ) call memory_alloca(memor,'BB_IDOFN','matrix_blokgs',bb_idofn,ndofn_per_block*nequa)          
       end if
       !
       ! Construct IDOFN system: AA_IDOFN(:) XX_IDOFN(:) = BB_IDOFN(:)
       !


       if( present( lperm ) ) then
 
       else
          do iequa = 1,nequa  !loop on points (nodes)
             do jdofn = 1,ndofn_per_block
                xx_idofn((iequa-1)*ndofn_per_block + jdofn) = xx((iblok-1)*ndofn_per_block+jdofn, iequa)
                bb_idofn((iequa-1)*ndofn_per_block + jdofn) = bb((iblok-1)*ndofn_per_block+jdofn, iequa)
             end do
             do iz = ia(iequa), ia(iequa+1)-1
                jequa = ja(iz)
                do icont=1,ndofn_per_block
                   do jdofn=1,ndofn_per_block
                      aa_idofn(icont,jdofn,iz) = aa((iblok-1)*ndofn_per_block + icont,(iblok-1)*ndofn_per_block+jdofn,iz)
                   end do
                end do
                do jdofn = 1,nblok
                   if( jdofn /= iblok) then
                      do icont = 1,ndofn_per_block
                         do jcont = 1,ndofn_per_block
                             bb_idofn((iequa-1)*ndofn_per_block+icont) = bb_idofn((iequa-1)*ndofn_per_block+icont) &
                                                                        - aa((jdofn-1)*ndofn_per_block+icont,(iblok-1)*ndofn_per_block+jcont,iz) * xx((jdofn-1)*ndofn_per_block+jcont,jequa)
                         end do
                      end do
                   end if
                end do
             end do
          end do
       end if


       ! reshape matrix aa_idofn into a vector
       allocate(aa_aux(ndofn_per_block*ndofn_per_block*nz))
       do iz=1,nz
          cont = 1
          do iequa=1,ndofn_per_block
             do jdofn=1,ndofn_per_block
                aa_aux(cont) = aa_idofn(iequa,jdofn,iz)
                cont = cont+1
             end do
          end do
       end do
       

    case ( 2_ip ) 
       !
       ! Update unknown in global array XX(IDOFN,:) <= XX(:)
       !
       if( present( lperm ) ) then
       else
          do iequa=1,nequa
             do icont = 1,ndofn_per_block
                xx(icont,iequa) = xx_idofn((iequa-1)*ndofn_per_block+icont)
             end do
          end do
       end if
 

    case ( 3_ip ) 
       !
       ! Deallocate memory
       !
       if( present( memit ) ) then
          call memory_deallo(memit,'AA_IDOFN','matrix_blokgs',aa_idofn)
          call memory_deallo(memit,'XX_IDOFN','matrix_blokgs',xx_idofn)
          call memory_deallo(memit,'BB_IDOFN','matrix_blokgs',bb_idofn)
       else
          call memory_deallo(memor,'AA_IDOFN','matrix_blokgs',aa_idofn)
          call memory_deallo(memor,'XX_IDOFN','matrix_blokgs',xx_idofn)
          call memory_deallo(memor,'BB_IDOFN','matrix_blokgs',bb_idofn)          
       end if

    end select

  end subroutine matrix_blokgs

   !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Block Gauss-Seidel
  !> @details Block Gauss-Seidel
  !
  !----------------------------------------------------------------------

  !subroutine matrix_blokgs(&
  !     itask,nequa,ndofn,idofn,ia,ja,aa,bb,xx,&
  !     aa_idofn,bb_idofn,xx_idofn,memit)
  !  implicit none
  !  integer(ip),          intent(in)    :: itask               !< Small system or solution update
  !  integer(ip),          intent(in)    :: nequa               !< Number of nodes
  !  integer(ip),          intent(in)    :: ndofn               !< Nb of dof
  !  integer(ip),          intent(in)    :: idofn               !< Dof to be solved
  !  integer(ip),          intent(in)    :: ia(*)               !< CSR format
  !  integer(ip),          intent(in)    :: ja(*)               !< CSR format
  !  real(rp),             intent(in)    :: aa(ndofn,ndofn,*)   !< Original matrix
  !  real(rp),             intent(in)    :: bb(ndofn,*)         !< Original RHS
  !  real(rp),             intent(inout) :: xx(ndofn,*)         !< Original solution
  !  real(rp),    pointer, intent(out)   :: aa_idofn(:)         !< IDOFN Block system matrix
  !  real(rp),    pointer, intent(out)   :: bb_idofn(:)         !< IDOFN Block system RHS
  !  real(rp),    pointer, intent(out)   :: xx_idofn(:)         !< IDOFN Block system solution
  !  integer(8),  optional,intent(inout) :: memit(2)            !< Memory counter
  !  integer(ip)                         :: iequa,iz,jequa
  !  integer(ip)                         :: nz,jdofn
  !  integer(8)                          :: memor(2)

  !  select case ( itask ) 

  !  case ( 1_ip ) 
       !
       ! Allocate if necessary
       !
  !     nz = ia(nequa+1)-1
  !     if( present( memit ) ) then
  !        if( .not. associated(aa_idofn) ) call memory_alloca(memit,'AA_IDOFN','matrix_blokgs',aa_idofn,nz)
  !        if( .not. associated(xx_idofn) ) call memory_alloca(memit,'XX_IDOFN','matrix_blokgs',xx_idofn,nequa)
  !        if( .not. associated(bb_idofn) ) call memory_alloca(memit,'BB_IDOFN','matrix_blokgs',bb_idofn,nequa)
  !     else
  !        memor = 0
  !        if( .not. associated(aa_idofn) ) call memory_alloca(memor,'AA_IDOFN','matrix_blokgs',aa_idofn,nz)
  !        if( .not. associated(xx_idofn) ) call memory_alloca(memor,'XX_IDOFN','matrix_blokgs',xx_idofn,nequa)
  !        if( .not. associated(bb_idofn) ) call memory_alloca(memor,'BB_IDOFN','matrix_blokgs',bb_idofn,nequa)          
  !     end if
  !     !
  !     ! Construct IDOFN system: AA_IDOFN(:) XX_IDOFN(:) = BB_IDOFN(:)
  !     !
  !     do iequa = 1,nequa
  !        xx_idofn(iequa) = xx(idofn,iequa)
  !        bb_idofn(iequa) = bb(idofn,iequa)
  !        do iz = ia(iequa),ia(iequa+1)-1
  !           jequa = ja(iz)
  !           aa_idofn(iz) = aa(idofn,idofn,iz)
  !           do jdofn = 1,ndofn
  !              if( jdofn /= idofn ) &
  !                   bb_idofn(iequa) = bb_idofn(iequa) - aa(jdofn,idofn,iz) * xx(jdofn,jequa)
  !           end do
  !        end do
  !     end do

  !  case ( 2_ip ) 
  !     !
  !     ! Update unknown in global array XX(IDOFN,:) <= XX(:)
  !     !
  !     do iequa = 1,nequa
  !        xx(idofn,iequa) = xx_idofn(iequa) 
  !     end do

  !  case ( 3_ip ) 
       !
       ! Deallocate memory
       !
  !     if( present( memit ) ) then
  !        call memory_deallo(memit,'AA_IDOFN','matrix_blokgs',aa_idofn)
  !        call memory_deallo(memit,'XX_IDOFN','matrix_blokgs',xx_idofn)
  !        call memory_deallo(memit,'BB_IDOFN','matrix_blokgs',bb_idofn)
  !     else
  !        call memory_deallo(memor,'AA_IDOFN','matrix_blokgs',aa_idofn)
  !        call memory_deallo(memor,'XX_IDOFN','matrix_blokgs',xx_idofn)
  !        call memory_deallo(memor,'BB_IDOFN','matrix_blokgs',bb_idofn)          
  !     end if

  !  end select

  !end subroutine matrix_blokgs_ECR

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Agglomerate a matrix
  !> @details Agglomerate a matrix
  !
  !----------------------------------------------------------------------

  subroutine matrix_matgro(&
       itask,npoin,ngrou,ndofn,kfl_assem_group,kfl_symme,lgrou,&
       ia,ja,iagroup,jagroup,nsizegroup,aa,aagroup)
    implicit none
    integer(ip),          intent(in)  :: itask                  !< Perform an all reduce
    integer(ip),          intent(in)  :: npoin                  !< Number of nodes
    integer(ip),          intent(in)  :: ngrou                  !< Number of groups
    integer(ip),          intent(in)  :: ndofn                  !< Nb of dof
    integer(ip),          intent(in)  :: kfl_assem_group        !< Assembly type of coarse matrix (0: skyline, 1=CSR)
    integer(ip),          intent(in)  :: kfl_symme              !< Symmetric assembly (1) or not (0)
    integer(ip),          intent(in)  :: lgrou(:)               !< List of groups
    integer(ip),          intent(in)  :: ia(*)                  !< Matrix graph
    integer(ip),          intent(in)  :: ja(*)                  !< Matrix graph
    integer(ip), pointer, intent(in)  :: iagroup(:)             !< Skyline: iskyl, CSR: iagrou
    integer(ip), pointer, intent(in)  :: jagroup(:)             !< Skyline: null, CSR: jagrou
    integer(ip),          intent(in)  :: nsizegroup             !< Size of the coarse matrix
    real(rp),             intent(in)  :: aa(ndofn,ndofn,*)      !< Fine matrix
    real(rp),             intent(out) :: aagroup(ndofn,ndofn,*) !< Coarse matrix
    integer(ip)                       :: idofn,izdom,izgro
    integer(ip)                       :: igrou,jgrou
    integer(ip)                       :: jdofn,ipoin,jpoin
    integer(ip)                       :: kskyl
    logical(lg), pointer              :: lchek(:)
    integer(ip), pointer              :: iskyl(:) 

    nullify(iskyl)
    nullify(lchek)
    !
    ! Check array
    !
    allocate( lchek(ngrou) )
    do igrou = 1,ngrou
       lchek(igrou) = .true.
    end do
    !
    ! Agglomerate matrix AAGROUP <= AA
    !
    if( kfl_assem_group ==  0 ) then
       !
       ! Skyline format
       !
       iskyl => iagroup

       if( ndofn == 1 ) then

          if( kfl_symme == 0 ) then
             !
             ! Skyline format: A(1,1,:) is not symmetric
             !          
             do ipoin = 1,npoin
                if( lgrou(ipoin) > 0 ) then
                   igrou = lgrou(ipoin)
                   do izdom = ia(ipoin),ia(ipoin+1)-1
                      jpoin = ja(izdom)
                      if( jpoin < ipoin ) then
                         if( lgrou(jpoin) > 0 ) then
                            jgrou = lgrou(jpoin)
                            if( igrou > jgrou ) then
                               kskyl              = iskyl(igrou+1) - 1 - (igrou-jgrou)
                               aagroup(1,1,kskyl) = aagroup(1,1,kskyl) + aa(1,1,izdom)
                            else if( igrou < jgrou ) then
                               kskyl              = iskyl(jgrou+1) - 1 - (jgrou-igrou)
                               aagroup(1,1,kskyl) = aagroup(1,1,kskyl) + aa(1,1,izdom)
                            else
                               kskyl              = iskyl(igrou+1) - 1
                               aagroup(1,1,kskyl) = aagroup(1,1,kskyl) + 2.0_rp*aa(1,1,izdom)
                            end if
                         end if
                      else if( ipoin == jpoin ) then
                         kskyl              = iskyl(igrou+1) - 1
                         aagroup(1,1,kskyl) = aagroup(1,1,kskyl) + aa(1,1,izdom)  
                      end if
                   end do
                end if
             end do

          else
             !
             ! Skyline format: A(1,1,:) is symmetric
             !          
             do ipoin = 1,npoin
                if( lgrou(ipoin) > 0 ) then
                   igrou = lgrou(ipoin)
                   do izdom = ia(ipoin),ia(ipoin+1)-2
                      jpoin = ja(izdom)
                      if( lgrou(jpoin) > 0 ) then
                         jgrou = lgrou(jpoin)
                         if( igrou > jgrou ) then
                            kskyl              = iskyl(igrou+1)-1-(igrou-jgrou)
                            aagroup(1,1,kskyl) = aagroup(1,1,kskyl)+aa(1,1,izdom)
                         else if( igrou < jgrou ) then
                            kskyl              = iskyl(jgrou+1)-1-(jgrou-igrou)
                            aagroup(1,1,kskyl) = aagroup(1,1,kskyl)+aa(1,1,izdom)
                         else
                            kskyl              = iskyl(igrou+1)-1
                            aagroup(1,1,kskyl) = aagroup(1,1,kskyl)+2.0_rp*aa(1,1,izdom)
                         end if
                      end if
                   end do
                   izdom              = ia(ipoin+1)-1
                   kskyl              = iskyl(igrou+1)-1
                   aagroup(1,1,kskyl) = aagroup(1,1,kskyl) + aa(1,1,izdom)  
                end if
             end do


          end if

       else

          call runend('MATRIX_MATGRO: FORMAT NOT CODED')

       end if

    else if( kfl_assem_group ==  1 ) then
       !
       ! CSR format
       !
       if( ndofn == 1 ) then

          if( kfl_symme == 0 ) then
             !
             ! CSR format: A(1,1,:) is not symmetric
             !          
             do ipoin = 1,npoin
                if( lgrou(ipoin) > 0 ) then
                   igrou = lgrou(ipoin)
                   do izdom = ia(ipoin),ia(ipoin+1)-1
                      jpoin = ja(izdom)
                      if( lgrou(jpoin) > 0 ) then
                         jgrou = lgrou(jpoin)
                         izgro = iagroup(igrou)
                         iifzgro1: do while( izgro <= iagroup(igrou+1)-1 )
                            if( jagroup(izgro) == jgrou ) exit iifzgro1
                            izgro = izgro + 1
                         end do iifzgro1
                         lchek(igrou) = .false.
                         aagroup(1,1,izgro) = aagroup(1,1,izgro) + aa(1,1,izdom)
                      end if
                   end do
                end if
             end do

          else
             !
             ! CSR format: A(1,1,:) is symmetric
             !          
             call runend('MATRIX_MATGRO: NOT CODED')

          end if

       else

          if( kfl_symme == 0 ) then
             !
             ! CSR format: A(NDOFN,NDOFN,1,:) is not symmetric
             !          
             do ipoin = 1,npoin
                if( lgrou(ipoin) > 0 ) then
                   igrou = lgrou(ipoin)
                   do izdom = ia(ipoin),ia(ipoin+1)-1
                      jpoin = ja(izdom)
                      if( lgrou(jpoin) > 0 ) then
                         jgrou = lgrou(jpoin)
                         izgro = iagroup(igrou)
                         iifzgro3: do while( izgro <= iagroup(igrou+1)-1 )
                            if( jagroup(izgro) == jgrou ) exit iifzgro3
                            izgro = izgro + 1
                         end do iifzgro3
                         lchek(igrou) = .false.
                         do jdofn = 1,ndofn
                            do idofn = 1,ndofn
                               aagroup(idofn,jdofn,izgro) = aagroup(idofn,jdofn,izgro) &
                                    + aa(idofn,jdofn,izdom)
                            end do
                         end do
                      end if
                   end do
                end if
             end do

          else
             !
             ! CSR format: A(NDOFN,NDOFN,1,:) is symmetric
             !          
             call runend('MATRIX_MATGRO: NOT CODED')

          end if

       end if

    end if
    !
    ! Check if group has a non-zero on its row
    !
    do igrou = 1,ngrou
       if( lchek(igrou) ) then
          izgro = iagroup(igrou)
          iifzgro4: do while( izgro <= iagroup(igrou)+1-1 )
             if( jagroup(izgro) == igrou ) exit iifzgro4
             izgro = izgro + 1
          end do iifzgro4
          do idofn = 1,ndofn
             aagroup(idofn,idofn,izgro) = 1.0_rp 
          end do
       end if
    end do
    deallocate(lchek)
    !
    ! Perform an all reduce
    !
    if( itask == 1 ) then
       call pararr('SUM',0_ip,nsizegroup,aagroup)
    end if
    !
    ! Output matrix in ps format
    !
    !call pspltm(&
    !     ngrou,ngrou,1_ip,0_ip,jagroup,iagroup,aagroup,&
    !     'caca',0_ip,18.0_rp,'cm',&
    !     0_ip,0_ip,2_ip,99)

  end subroutine matrix_matgro

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Assemble a RHS
  !> @details Assemble a RHS
  !
  !----------------------------------------------------------------------

  subroutine matrix_assrhs(ndofn_glo,ndofn_ele,pnode,npoin,lnods,elrhs,rhsid,kdofn)
    implicit none
    integer(ip),  intent(in)           :: ndofn_glo
    integer(ip),  intent(in)           :: ndofn_ele
    integer(ip),  intent(in)           :: pnode
    integer(ip),  intent(in)           :: npoin
    integer(ip),  intent(in)           :: lnods(pnode)
    real(rp),     intent(in)           :: elrhs(*)
    real(rp),     intent(inout)        :: rhsid(*)
    integer(ip),  intent(in), optional :: kdofn
    integer(ip)                        :: inode,ipoin,idofl,idofg,idofn,ndof2

    if( present(kdofn) ) then
       !
       ! NDOFN_GLO DOF's but only KDOFN is assembled
       !
       if( ndofn_ele == 1 ) then
          do inode = 1,pnode           
             ipoin = lnods(inode)
             idofg = (ipoin-1) * ndofn_glo + kdofn
             rhsid(idofg) = rhsid(idofg) + elrhs(inode)
          end do
       else
          call runend('MATRIX_ASSRHS: VERY STRANGE RHS ASSEMBLY INDEED...')
       end if

    else

       if(ndofn_glo==1) then
          !
          ! 1 DOF
          !
          do inode=1,pnode
             ipoin=lnods(inode)
             rhsid(ipoin)=rhsid(ipoin)+elrhs(inode)
          end do

       else if(ndofn_glo==2) then
          !
          ! 2 DOF's
          !
          do inode=1,pnode           
             ipoin=lnods(inode)
             idofg=2*ipoin-1
             idofl=2*inode-1
             rhsid(idofg)=rhsid(idofg)+elrhs(idofl)
             idofg=idofg+1
             idofl=idofl+1
             rhsid(idofg)=rhsid(idofg)+elrhs(idofl)
          end do

       else if(ndofn_glo>2) then
          !
          ! >2 DOF's
          !
          do inode=1,pnode           
             ipoin=lnods(inode)
             idofg=(ipoin-1)*ndofn_glo
             idofl=(inode-1)*ndofn_glo
             do idofn=1,ndofn_glo
                idofg=idofg+1
                idofl=idofl+1
                rhsid(idofg)=rhsid(idofg)+elrhs(idofl)
             end do
          end do

       else if(ndofn_glo<0) then
          !
          ! >2 DOF's
          !
          ndof2=abs(ndofn_glo)
          do inode=1,pnode           
             ipoin=lnods(inode)
             idofg=(ipoin-1)*ndof2
             idofl=(inode-1)*ndof2
             do idofn=1,ndof2
                idofg=(idofn-1)*npoin+ipoin
                idofl=idofl+1
                rhsid(idofg)=rhsid(idofg)+elrhs(idofl)
             end do
          end do

       end if
    end if

  end subroutine matrix_assrhs


  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Assemble a RHS
  !> @details Assemble a RHS
  !
  !----------------------------------------------------------------------

  subroutine matrix_assexp(ndofn_glo,ndofn_ele,pnode,npoin,lnods,elrhs,elmat,elunk,rhsid,kdofn)
    implicit none
    integer(ip),  intent(in)           :: ndofn_glo
    integer(ip),  intent(in)           :: ndofn_ele
    integer(ip),  intent(in)           :: pnode
    integer(ip),  intent(in)           :: npoin
    integer(ip),  intent(in)           :: lnods(pnode)
    real(rp),     intent(in)           :: elrhs(*)
    real(rp),     intent(in)           :: elmat(pnode*ndofn_ele,pnode*ndofn_ele)
    real(rp),     intent(in)           :: elunk(pnode*ndofn_ele,2)
    real(rp),     intent(inout)        :: rhsid(*)
    integer(ip),  intent(in), optional :: kdofn
    integer(ip)                        :: inode,ipoin,idofl,idofg,idofn,ndof2,jnode

    if( present(kdofn) ) then
       !
       ! NDOFN_GLO DOF's but only KDOFN is assembled
       !
       call runend('NOT CODED, ESPECIE DE VAGO')

    else

       if(ndofn_glo==1) then
          !
          ! 1 DOF
          !
          do inode = 1,pnode
             ipoin = lnods(inode)
             rhsid(ipoin) = rhsid(ipoin) + elrhs(inode)
             do jnode = 1,pnode
                rhsid(ipoin) = rhsid(ipoin) - elmat(inode,jnode) * elunk(jnode,2)
             end do
          end do

       else 

          call runend('NOT CODED, ESPECIE DE VAGO')

       end if
    end if

  end subroutine matrix_assexp

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Assemble a BCSR matrix
  !> @details Assemble a BCSR matrix
  !
  !----------------------------------------------------------------------

  subroutine matrix_asscsr(ndofn_glo,ndofn_ele,ia,ja,pnode,lnods,elmat,amatr,kdofn)
    implicit none  
    integer(ip), intent(in)           :: ndofn_glo
    integer(ip), intent(in)           :: ndofn_ele
    integer(ip), intent(in)           :: ia(*)
    integer(ip), intent(in)           :: ja(*)
    integer(ip), intent(in)           :: pnode
    integer(ip), intent(in)           :: lnods(pnode)
    real(rp),    intent(in)           :: elmat(ndofn_ele*pnode,ndofn_ele*pnode)
    real(rp),    intent(inout)        :: amatr(ndofn_glo,ndofn_glo,*)
    integer(ip), intent(in), optional :: kdofn
    integer(ip)                       :: ievat,jevat,idofn,jdofn,inode,jnode,ndofi
    integer(ip)                       :: ipoin,jpoin,izsol,jcolu,idime,jdime

    if( present(kdofn) ) then
       !
       ! NDOFN unknowns but assemble only KDOFN
       !   
       if( ndofn_ele == 1 ) then

          do inode = 1,pnode
             ipoin = lnods(inode)
             do jnode = 1,pnode
                jpoin = lnods(jnode)
                izsol = ia(ipoin)
                jcolu = ja(izsol)
                do while( jcolu /= jpoin .and. izsol < ia(ipoin+1)-1 )
                   izsol = izsol + 1
                   jcolu = ja(izsol)
                end do
                if( jpoin == jcolu ) then                  
                   idofn = kdofn
                   jdofn = kdofn
                   ievat = (inode-1) * ndofn_glo + idofn
                   jevat = (jnode-1) * ndofn_glo + jdofn
                   amatr(jdofn,idofn,izsol) = amatr(jdofn,idofn,izsol) + elmat(inode,jnode) ! elmat(ievat,jevat) WHY THIS IEVAT IF THE MATRIX ONLY BRINGS IPOINS
                end if
             end do
          end do

       else 

          call runend('MATRIX_ASSCSR: VERY STRANGE CASE INDEED...')

       end if

    else

       if( ndofn_glo == 1 ) then
          !
          ! 1 unknown
          !   
          do inode = 1,pnode
             ipoin = lnods(inode)
             do jnode = 1,pnode
                jpoin = lnods(jnode)
                izsol = ia(ipoin)
                jcolu = ja(izsol)
                do while( jcolu /= jpoin .and. izsol < ia(ipoin+1)-1)
                   izsol = izsol + 1
                   jcolu = ja(izsol)
                end do
                if( jcolu == jpoin ) then
                   amatr(1,1,izsol) = amatr(1,1,izsol) + elmat(inode,jnode)
                end if
             end do
          end do

       else
          !
          ! NDOFN unknown
          !   
          do inode = 1,pnode
             ipoin = lnods(inode)
             do jnode=1,pnode
                jpoin = lnods(jnode)
                izsol = ia(ipoin)
                jcolu = ja(izsol)
                do while(jcolu/=jpoin .and. izsol < ia(ipoin+1)-1 )
                   izsol = izsol + 1
                   jcolu = ja(izsol)
                end do
                if( jpoin == jcolu ) then
                   do idofn = 1,ndofn_glo
                      ievat = (inode-1) * ndofn_glo + idofn
                      do jdofn = 1,ndofn_glo
                         jevat = (jnode-1) * ndofn_glo + jdofn
                         amatr(jdofn,idofn,izsol) = amatr(jdofn,idofn,izsol) + elmat(ievat,jevat)
                      end do
                   end do
                end if
             end do
          end do
       end if

    end if

  end subroutine matrix_asscsr

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Compute the inverse diagonal of a matrix
  !> @details Assemble a BCSR matrix
  !
  !----------------------------------------------------------------------

  subroutine matrix_invdia(npoin,nbvar,kfl_symme,ia,ja,an,invdiag,memit)
    integer(ip), intent(in)             :: npoin,nbvar,kfl_symme
    integer(ip), intent(in)             :: ia(*),ja(*)
    real(rp),    intent(in)             :: an(nbvar,nbvar,*)
    real(rp),    intent(inout), pointer :: invdiag(:)
    integer(8),  intent(inout)          :: memit(2)            !< Memory counter
    integer(ip)                         :: ii,jj,kk,ll

    if( INOTMASTER ) then

       if( .not. associated(invdiag) ) then
          call memory_alloca(memit,'INVDIAG','matrix_invdia',invdiag,npoin*nbvar)
       end if

       if( kfl_symme == 1 ) then
          !
          ! Symmetric graph
          !
          if( nbvar == 1 ) then
             do ii= 1, npoin
                ll = ia(ii+1)-1
                invdiag(ii) = an(1,1,ll)
             end do
          else
             do ii= 1, npoin
                ll = ia(ii+1)-1
                jj = (ii-1) * nbvar 
                do kk= 1, nbvar
                   invdiag(jj+kk) = an(kk,kk,ll)
                end do
             end do
          end if

       else
          !
          ! Unsymmetric graph
          !
          if( nbvar == 1 ) then
             do ii= 1, npoin 
                jj = ia(ii)
                ll = -1
                do while (jj< ia(ii+1) .and. ll ==-1)
                   if(ja(jj)==ii) then
                      ll = jj
                   end if
                   jj = jj+1
                end do
                if(ll/=-1) then
                   invdiag(ii)=an(1,1,ll)
                else
                   invdiag(ii)=0.0_rp
                end if
             end do
          else
             do ii= 1, npoin 
                jj = ia(ii)
                ll = -1
                do while (jj< ia(ii+1) .and. ll ==-1)
                   if(ja(jj)==ii) then
                      ll = jj
                   end if
                   jj = jj+1
                end do
                if(ll/=-1) then
                   jj = (ii-1) * nbvar
                   do kk= 1, nbvar
                      invdiag(jj+kk)=an(kk,kk,ll)
                   end do
                else
                   jj = (ii-1) * nbvar
                   do kk= 1, nbvar
                      invdiag(jj+kk)=0.0_rp
                   end do
                end if
             end do
          end if

       end if
       !
       ! Periodicity and Parallelization
       !
       call pararr('SLX',NPOIN_TYPE,nbvar*npoin,invdiag)
       !
       ! Inverse diagonal
       ! 
       do ii = 1,npoin*nbvar
          if( invdiag(ii) /= 0.0_rp ) then
             invdiag(ii) = 1.0_rp / invdiag(ii)
          end if
        end do

    end if
    
  end subroutine matrix_invdia

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Diagonal of a Schur complement
  !> @details Diagonal of a Schur complement
  !
  !----------------------------------------------------------------------

  subroutine matrix_diagonal_schur(nbnodes,nbvar,ndofn_A3,A1,A2,invA3,A4,ia,ja,diag) 
    implicit none
    integer(ip), intent(in)          :: nbnodes,nbvar,ndofn_A3
    integer(ip), intent(in)          :: ia(*)
    integer(ip), intent(in)          :: ja(*)
    real(rp),    intent(in)          :: A1(nbvar,nbvar,*)
    real(rp),    intent(in)          :: A2(ndofn_A3,*) 
    real(rp),    intent(in)          :: invA3(ndofn_A3,nbnodes)
    real(rp),    intent(in)          :: A4(ndofn_A3,*)
    real(rp),    intent(out)         :: diag(*)
    real(rp)                         :: raux,raux1,raux2,raux3
    integer(ip)                      :: izdom,kzdom,lzdom,ii
    integer(ip)                      :: jj,kk,mm,idofn

    if( INOTMASTER ) then

       if( nbvar == 1 ) then

          do ii = 1,nbnodes
             do izdom = ia(ii),ia(ii+1)-1
                jj = ja(izdom)
                if( ii == jj ) then
                   diag(ii) = A1(1,1,izdom)
                   do kzdom = ia(ii),ia(ii+1)-1
                      kk    = ja(kzdom)
                      lzdom = ia(kk)
                      do while( lzdom <= ia(kk+1)-1 )
                         mm = ja(lzdom)
                         if( mm == jj ) then
                            do idofn = 1,ndofn_A3
                               diag(ii) = diag(ii) - A2(idofn,kzdom) * A4(idofn,lzdom) * invA3(idofn,kk)
                            end do
                            lzdom = ia(kk+1)
                         end if
                         lzdom = lzdom + 1
                      end do
                   end do
                end if
             end do
          end do

       end if

    end if

  end subroutine matrix_diagonal_schur

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    29/06/2015
  !> @brief   Output a matrix in GiD format
  !> @detailsOutput a matrix in GiD format
  !
  !----------------------------------------------------------------------

  subroutine matrix_output_gid_format(npoin,nbvar,ia,ja,an,invpr)
    integer(ip), intent(in)           :: npoin
    integer(ip), intent(in)           :: nbvar
    integer(ip), intent(in)           :: ia(*)
    integer(ip), intent(in)           :: ja(*)
    real(rp),    intent(in)           :: an(nbvar,nbvar,*)
    integer(ip), intent(in), optional :: invpr(*)
    !integer(ip), intent(in), optional :: lgrou(*)
    integer(ip)                       :: ii,jj,kk,ll,nn,ne,ke,ie,je
    integer(ip)                       :: nx,ny,ex,ey,izdom,ipoin
    integer(ip)                       :: ii_old,jj_old
    real(rp)                          :: xx,yy
    logical(lg)                       :: notfound

    open( unit = 98 , file = 'matrix.post.msh' , status = 'unknown' )
    open( unit = 99 , file = 'matrix.post.res' , status = 'unknown' )

    nx = npoin + 1
    ny = npoin + 1
    ex = npoin
    ey = npoin
    ! 
    ! Mesh
    !
    write(98,'(a)') 'MESH MATRIX dimension 2 Elemtype Quadrilateral Nnode 4'
    kk = 0
    write(98,*) 'coordinates'
    do jj = 1,nx
       yy = real(ny-(jj-1)*nbvar,rp)          
       do ii = 1,ny
          xx = real((ii-1)*nbvar,rp)
          kk = kk + 1 
          write(98,*) kk,xx,yy
       end do
    end do
    write(98,*) 'end coordinates'

    ke = 0
    write(98,*) 'elements'
    do je = 1,ey
       do ie = 1,ex
          ke = ke + 1
          ii = nx*(je-1) + ie
          write(98,*) ke,ii,ii+1,ii+nx+1,ii+nx
       end do
    end do
    write(98,*) 'end elements'
    !
    ! Matrix coefficients
    !
    write(99,*) 'GiD Post Results File 1.0'
    write(99,*) ' '
    write(99,*) 'GaussPoints GP Elemtype Quadrilateral'
    write(99,*) 'Number of Gauss Points: 1'
    write(99,*) 'Natural Coordinates: Internal'
    write(99,*) 'End GaussPoints'  
    write(99,*) 'Result MATRIX ALYA  0.00000000E+00 Scalar OnGaussPoints GP'
    write(99,*) 'ComponentNames MATRIX'
    write(99,*) 'Values'

    if( .not. present(invpr) ) then
       do ii = 1,npoin
          do jj = 1,npoin
             izdom    = ia(ii)-1
             notfound = .true.
             do while( notfound .and. izdom < ia(ii+1)-1 )
                izdom = izdom + 1
                if( ja(izdom) == jj ) notfound = .false.
             end do
             ipoin = (ii-1)*ex + jj
             if( notfound ) then            
                write(99,*) ipoin,0.0_rp
             else
                write(99,*) ipoin,abs(an(1,1,izdom))
             end if
          end do
       end do
    else
       do ii = 1,npoin
          ii_old = invpr(ii)
          do jj = 1,npoin
             jj_old   = invpr(jj)
             izdom    = ia(ii_old)-1
             notfound = .true.
             do while( notfound .and. izdom < ia(ii_old+1)-1 )
                izdom = izdom + 1
                if( ja(izdom) == jj_old ) notfound = .false.
             end do
             ipoin = (ii-1)*ex + jj
             if( .not. notfound ) then 
                write(99,*) ipoin,abs(an(1,1,izdom))
             else
                write(99,*) ipoin,0.0_rp
             end if
          end do
       end do
    end if
    write(99,*) 'End Values'

!!$    if( present(lgrou) ) then
!!$       !
!!$       ! Groups
!!$       !
!!$       write(99,*) 'Result GROUPS ALYA  0.00000000E+00 Scalar OnGaussPoints GP'
!!$       write(99,*) 'ComponentNames GROUPS'
!!$       write(99,*) 'Values'
!!$
!!$       if( .not. present(invpr) ) then
!!$          do ii = 1,npoin
!!$             do jj = 1,npoin
!!$                ipoin = (ii-1)*ex + jj
!!$                if( notfound ) then            
!!$                   write(99,*) ipoin,0.0_rp
!!$                else
!!$                   write(99,*) ipoin,lgrou(ipoin)
!!$                end if
!!$             end do
!!$          end do
!!$       else
!!$          do ii = 1,npoin
!!$             ii_old = invpr(ii)
!!$             do jj = 1,npoin
!!$                jj_old = invpr(jj)
!!$                ipoin = (ii-1)*ex + jj
!!$                if( lgrou(ii_old) == lgrou(jj_old) ) then 
!!$                   write(99,*) ipoin,lgrou(ii_old)
!!$                else
!!$                   write(99,*) ipoin,0.0_rp
!!$                end if
!!$             end do
!!$          end do
!!$       end if
!!$       write(99,*) 'End Values'       
!!$    end if

    close( unit = 98 )
    close( unit = 99 )


  end subroutine matrix_output_gid_format

end module mod_matrix
