!-----------------------------------------------------------------------
!
!> @addtogroup MeshesToolBox
!> @{
!> @name    ToolBox for meshes operations
!> @file    mod_meshes.f90
!> @author  Guillaume Houzeaux
!> @brief   ToolBox for meshes operations
!> @details ToolBox for meshes operations
!> @{
!
!-----------------------------------------------------------------------

module mod_meshes

  use def_kintyp, only : ip,rp,lg
  use def_elmtyp, only : ELFEM
  use def_elmtyp, only : BAR02,TRI03
  use def_master, only : INOTMASTER,kfl_paral,leinv_loc
  use def_domain, only : memor_dom
  use def_domain, only : mesh_type
  use def_domain, only : mnode
  use mod_memory, only : memory_alloca
  use mod_memory, only : memory_deallo
  implicit none
  private
  real(rp)   :: epsil = epsilon(1.0_rp)

  public :: meshes_submesh
  public :: meshes_surface_from_nodal_array
  public :: meshes_surface_from_nodal_array_deallocate

contains 

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute a submesh
  !> @details Compute a submesh given a permutaiton array
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine meshes_submesh(                                                    &
       & ndime,    npoin    ,nelem    ,coord    ,lnods    ,ltype    ,lnnod,     &
       &           npoin_sub,nelem_sub,coord_sub,lnods_sub,ltype_sub,lnnod_sub, &
       &           lperm   )

    integer(ip),          intent(in)  :: ndime
    integer(ip),          intent(in)  :: nelem
    integer(ip),          intent(in)  :: npoin
    real(rp),    pointer, intent(in)  :: coord(:,:)
    integer(ip), pointer, intent(in)  :: lnods(:,:)
    integer(ip), pointer, intent(in)  :: ltype(:)
    integer(ip), pointer, intent(in)  :: lnnod(:)
    integer(ip),          intent(out) :: nelem_sub
    integer(ip),          intent(out) :: npoin_sub
    real(rp),    pointer, intent(out) :: coord_sub(:,:)
    integer(ip), pointer, intent(out) :: lnods_sub(:,:)
    integer(ip), pointer, intent(out) :: ltype_sub(:)
    integer(ip), pointer, intent(out) :: lnnod_sub(:)
    integer(ip), pointer, intent(out) :: lperm(:)
    integer(ip)                       :: ielem,inode,ipoin
    integer(ip)                       :: pelty,pnode,mnode
    integer(ip)                       :: kelem,ipoin_sub
    integer(ip), pointer              :: lpoin(:)

    mnode     = size(lnods,1)
    npoin_sub = 0
    if( .not. associated(lperm) ) then
       nelem_sub = 0
    else
       nelem_sub = size(lperm)
    end if

    if( nelem_sub /= 0 ) then

       call memory_alloca(memor_dom,'LNODS_SUB','meshes_submesh',lnods_sub,mnode,nelem_sub)
       call memory_alloca(memor_dom,'LTYPE_SUB','meshes_submesh',ltype_sub,      nelem_sub)
       call memory_alloca(memor_dom,'LNNOD_SUB','meshes_submesh',lnnod_sub,      nelem_sub)

       allocate(lpoin(npoin))
       do ipoin = 1,npoin
          lpoin(ipoin) = 0
       end do
       !
       ! Renumber nodes
       !
       do kelem = 1,nelem_sub
          ielem = lperm(kelem)
          pelty = abs(ltype(ielem))
          pnode = lnnod(ielem)
          ltype_sub(kelem) = pelty
          lnnod_sub(kelem) = pnode
          do inode = 1,pnode
             ipoin = lnods(inode,ielem)
             if( lpoin(ipoin) == 0 ) then
                npoin_sub = npoin_sub + 1
                lpoin(ipoin) = npoin_sub
             end if
          end do
       end do
       !
       ! Copy connectivity and coordinates
       !
       call memory_alloca(memor_dom,'COORD_SUB','meshes_submesh',coord_sub,ndime,npoin_sub)

       do kelem = 1,nelem_sub
          ielem = lperm(kelem)
          pelty = abs(ltype(ielem))
          pnode = lnnod(ielem)
          do inode = 1,pnode
             ipoin = lnods(inode,ielem)
             lnods_sub(inode,kelem) = lpoin(ipoin)
          end do
       end do
       do ipoin = 1,npoin
          ipoin_sub = lpoin(ipoin)
          if( ipoin_sub /= 0 ) &
               coord_sub(1:ndime,ipoin_sub) = coord(1:ndime,ipoin) 
       end do

       deallocate(lpoin)

    else

       npoin_sub = 0
       nelem_sub = 0

    end if

  end subroutine meshes_submesh

  !-----------------------------------------------------------------------
  !
  !> @brief   Construct a surface mesh
  !> @details Construct a surface mesh from a nodal array xarra
  !> @date    29/09/2015
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine meshes_surface_from_nodal_array_deallocate(&
       lnodb_sur,coord_sur,ltypb_sur,lelem_sur)
    implicit none
    integer(ip),     pointer,           intent(out) :: lnodb_sur(:,:)
    real(rp),        pointer,           intent(out) :: coord_sur(:,:)
    integer(ip),     pointer, optional, intent(out) :: ltypb_sur(:)
    integer(ip),     pointer, optional, intent(out) :: lelem_sur(:)

    if( INOTMASTER ) then
       call memory_deallo(memor_dom,'LNODB_SUR','meshes_surface_from_nodal_array',lnodb_sur)
       call memory_deallo(memor_dom,'COORD_SUR','meshes_surface_from_nodal_array',coord_sur)
       if( present(ltypb_sur) ) call memory_deallo(memor_dom,'LTYPB_SUR','meshes_surface_from_nodal_array',ltypb_sur)
       if( present(lelem_sur) ) call memory_deallo(memor_dom,'LELEM_SUR','meshes_surface_from_nodal_array',lelem_sur)
    end if

  end subroutine meshes_surface_from_nodal_array_deallocate

  !-----------------------------------------------------------------------
  !
  !> @brief   Construct a surface mesh
  !> @details Construct a surface mesh from a nodal array xarra
  !> @date    29/09/2015
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine meshes_surface_from_nodal_array(&
       xarra,meshe,npoin_sur,nboun_sur,lnodb_sur,coord_sur,ltypb_sur,lelem_sur)
    implicit none
    real(rp),        pointer,           intent(in)  :: xarra(:,:)                 !< 
    type(mesh_type),                    intent(in)  :: meshe                      !< Mesh type
    integer(ip),                        intent(out) :: npoin_sur
    integer(ip),                        intent(out) :: nboun_sur
    integer(ip),     pointer,           intent(out) :: lnodb_sur(:,:)
    real(rp),        pointer,           intent(out) :: coord_sur(:,:)
    integer(ip),     pointer, optional, intent(out) :: ltypb_sur(:)
    integer(ip),     pointer, optional, intent(out) :: lelem_sur(:)
    integer(ip)                                     :: ielem,idime,icomp          ! Indices and dimensions
    integer(ip)                                     :: pelty,pnode,p1,p2,p3
    integer(ip)                                     :: inode,jnode,ipoin
    integer(ip)                                     :: compt,compl,compg,compi
    integer(ip)                                     :: ii,ij,inod1,inod2          ! element crossed by interface = 1 
    integer(ip)                                     :: signn,signp,sigtn,sigtp    ! sign inside the element 
    integer(ip)                                     :: tetra(4_ip,6_ip)
    integer(ip)                                     :: tetrp(4_ip,3_ip)
    integer(ip)                                     :: tetpy(4_ip,2_ip)
    integer(ip)                                     :: ipasq,ipasp,ipapy,itype
    integer(ip)                                     :: pnodb
    real(rp)                                        :: elcod(3,mnode)
    real(rp)                                        :: elarr(mnode),inter(3,4)
    real(rp)                                        :: l1,lp,x1,y1,x2,y2,z1,z2 
    real(rp)                                        :: gpgrl(3),gpcar(3,mnode)
    real(rp)                                        :: xjaci(9),xjacm(9),gpdet
    real(rp)                                        :: vec(3,3),coord_xxx(3)
    real(rp)                                        :: xx,nx,ny,nz
    real(rp)                                        :: norma(3),vecto(3)
    !
    ! Mesh arrays required
    !
    integer(ip)                                     :: nelem
    integer(ip)                                     :: npoin
    integer(ip)                                     :: ndime
    integer(ip),      pointer                       :: lnods(:,:)
    integer(ip),      pointer                       :: lnnod(:)
    integer(ip),      pointer                       :: lelch(:)
    real(rp),         pointer                       :: coord(:,:)

    if( INOTMASTER ) then
       !
       ! Mesh arrays
       !
       nelem =  meshe % nelem 
       npoin =  meshe % npoin 
       ndime =  meshe % ndime 
       lnods => meshe % lnods
       lnnod => meshe % lnnod
       lelch => meshe % lelch
       coord => meshe % coord
       compt =  0_ip

       if( ndime == 2 ) then
          !
          ! Loop over the elements to construct discrete interface 
          ! collection of segments (2d case)
          !
          pnodb = 2
          do ielem = 1,nelem
             if( lelch(ielem) == ELFEM ) then
                pnode = lnnod(ielem)
                do inode = 1,pnode
                   ipoin = lnods(inode,ielem)
                   elarr(inode) = xarra(ipoin,1)
                   elcod(1:ndime,inode) = coord(1:ndime,ipoin)
                end do

                compl = 0_ip
                do inode = 1,pnode
                   if( inode == pnode ) then
                      jnode = 1 
                   else
                      jnode = inode + 1
                   endif
                   !
                   ! Count interface points 
                   !
                   if( elarr(inode)*elarr(jnode) < 0.0_rp ) then
                      compl = compl + 1
                      if( compl == 3 ) compl = 1         ! Count interface points 
                      if( compl == 1 ) compt = compt + 1 ! Count interface segments
                   endif
                end do
             end if
          end do
          !
          ! Registering of discrete interface vectors size
          !
          nboun_sur = compt
          npoin_sur = ndime * nboun_sur
          compt     = 0_ip
          compg     = 0_ip
          !
          ! Memory allocation for discrete interface vectors
          ! connectivity (lnodb_sur) and points coordinates (coord_sur)
          !
          call memory_deallo(memor_dom,'LNODB_SUR','meshes_surface_from_nodal_array',lnodb_sur)
          call memory_deallo(memor_dom,'COORD_SUR','meshes_surface_from_nodal_array',coord_sur)
          if( present(ltypb_sur) ) call memory_deallo(memor_dom,'LTYPB_SUR','meshes_surface_from_nodal_array',ltypb_sur)
          if( present(lelem_sur) ) call memory_deallo(memor_dom,'LELEM_SUR','meshes_surface_from_nodal_array',lelem_sur)

          call memory_alloca(memor_dom,'LNODB_SUR','meshes_surface_from_nodal_array',lnodb_sur,pnodb,nboun_sur)
          call memory_alloca(memor_dom,'COORD_SUR','meshes_surface_from_nodal_array',coord_sur,ndime,npoin_sur)
          if( present(ltypb_sur) ) call memory_alloca(memor_dom,'LTYPB_SUR','meshes_surface_from_nodal_array',ltypb_sur,nboun_sur)
          if( present(lelem_sur) ) call memory_alloca(memor_dom,'LELEM_SUR','meshes_surface_from_nodal_array',lelem_sur,nboun_sur)
          !
          ! filling of discrete interface vectors 
          ! connectivity (lnodb_sur) and points coordinates (coord_sur)
          !
          do ielem=1,nelem
             if( lelch(ielem) == ELFEM ) then
                pnode = lnnod(ielem)
                do inode = 1,pnode
                   ipoin                = lnods(inode,ielem)
                   elarr(inode)         = xarra(ipoin,1)
                   elcod(1:ndime,inode) = coord(1:ndime,ipoin)
                end do

                compl = 0_ip

                do inode = 1,pnode
                   if( inode == pnode ) then
                      jnode = 1 
                   else
                      jnode = inode + 1
                   endif
                   if( elarr(inode)*elarr(jnode) < 0.0_rp ) then
                      compg = compg + 1
                      compl = compl + 1
                      if( compl == 3 ) compl = 1
                      if( compl == 1 ) compt = compt + 1
                      !
                      ! Compute the intersection of the elements with the surface 
                      !
                      l1 = abs(elarr(inode))
                      lp = abs(elarr(inode)-elarr(jnode))
                      x1 = elcod(1,inode)
                      x2 = elcod(1,jnode)
                      y1 = elcod(2,inode)
                      y2 = elcod(2,jnode)
                      lnodb_sur(compl,compt) = compg
                      coord_sur(1,compg)     =  x1 * (1.0_rp-l1/lp) + x2 * l1 / lp  
                      coord_sur(2,compg)     =  y1 * (1.0_rp-l1/lp) + y2 * l1 / lp
                      if( present(ltypb_sur) ) ltypb_sur(compt) = BAR02
                      if( present(lelem_sur) ) lelem_sur(compt) = ielem
                   end if
                end do
                !
                ! Orientate surface
                !
                if( compl == 2 ) then
                   x1       = coord_sur(1,compg-1)
                   y1       = coord_sur(2,compg-1)
                   x2       = coord_sur(1,compg)
                   y2       = coord_sur(2,compg)
                   norma(1) = y1-y2
                   norma(2) = x2-x1            
                   inode    = 1
                   vecto(1) = (elcod(1,inode)-0.5_rp*(x1+x2))
                   vecto(2) = (elcod(2,inode)-0.5_rp*(y1+y2))
                   l1       = norma(1)*vecto(1)+norma(2)*vecto(2)
                   if( l1 > 0.0_rp ) then
                      if( elarr(inode) >= 0.0_rp ) then
                         lnodb_sur(1,compt) = compg
                         lnodb_sur(2,compt) = compg-1
                      end if
                   else                      
                      if( elarr(inode) < 0.0_rp ) then
                         lnodb_sur(1,compt) = compg
                         lnodb_sur(2,compt) = compg-1
                      end if
                   end if
                end if

             end if
          end do

       else

          call runend('NOT CODED')

       end if

    end if

  end subroutine meshes_surface_from_nodal_array

end module mod_meshes
!> @}
