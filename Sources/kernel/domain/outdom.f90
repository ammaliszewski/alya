subroutine outdom(itask)
  !------------------------------------------------------------------------
  !****f* Domain/outdom
  ! NAME
  !    outdom
  ! DESCRIPTION
  !    This routine dumps the geometry if required by the user:
  !    - coordinates
  !    - connectivity 
  !    Depending on the format the mesh is written whatever says the user.
  !    This is the case of:
  !    - FEMVIEW
  !    - ENSIGHT
  !    Also, postprocess the exterior normal
  !    - exterior normal
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_elmtyp
  use def_master
  use def_kermod
  use def_domain
  use mod_postpr  
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipart

  select case ( itask )

  case ( 1_ip ) 

     if( kfl_oumes == 1 ) then
        !
        ! Output mesh
        !
        call livinf(0_ip,'OUTPUT MESH',0_ip)
        if( kfl_outfo == 50 ) then
           !
           ! HDFPOS
           !
           if( IPARALL ) call Hdfpos(8_ip)               

        else if ( kfl_outfo /= 50 .OR. kfl_outfo /= 40 .OR. kfl_outfo /= 41) then
           !
           ! ALYA
           !
           if( IMASTER ) then
              write(lun_pos00,1) npart
              do ipart = 1,npart
                 write(lun_pos00,2) ipart,&
                      &             meshe(kfl_posdi) % nelem_par(ipart),&
                      &             meshe(kfl_posdi) % npoin_par(ipart),&
                      &             meshe(kfl_posdi) % nboun_par(ipart)
              end do
           else if( ISEQUEN ) then
              write(lun_pos00,1) 1_ip
              write(lun_pos00,2) 1_ip,&
                   &          meshe(kfl_posdi) % nelem,&
                   &          meshe(kfl_posdi) % npoin,&
                   &          meshe(kfl_posdi) % nboun
           end if
           if( INOTSLAVE ) call flush(lun_pos00)

           call postpr(meshe(kfl_posdi) % coord,postp(1) % wopos(:,16),ittim,cutim,ndime,'ORIGI')    ! COORD
           call postpr(meshe(kfl_posdi) % lnods,postp(1) % wopos(:,15),ittim,cutim,mnode,'ORIGI')    ! LNODS
           call postpr(meshe(kfl_posdi) % ltype,postp(1) % wopos(:,17),ittim,cutim,      'ORIGI')    ! LTYPE           
           call postpr(meshe(kfl_posdi) % lelch,postp(1) % wopos(:,19),ittim,cutim,      'ORIGI')    ! LELCH
           call postpr(meshe(kfl_posdi) % lninv_loc,postp(1) % wopos(:,18),ittim,cutim,  'ORIGI')    ! LNINV_LOC

           if( nboun > 0 ) then
              call postpr(meshe(kfl_posdi) % lnodb,postp(1) % wopos(:,20),ittim,cutim,mnodb,'ORIGI')    ! LNODB
              call postpr(meshe(kfl_posdi) % ltypb,postp(1) % wopos(:,21),ittim,cutim,      'ORIGI')    ! LTYPB
           end if
           if( nsubd > 1 ) then
              call postpr(meshe(kfl_posdi) % lesub,postp(1) % wopos(:,22),ittim,cutim,      'ORIGI')    ! LESUB
           end if

        end if
     end if

     !
     ! Output bounday mesh in STL format
     !
     if( kfl_oustl /= 0 ) call outstl()

  end select

1 format(i9)
2 format(10(1x,i9))

end subroutine outdom
