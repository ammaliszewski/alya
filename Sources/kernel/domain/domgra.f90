subroutine domgra()
  !-----------------------------------------------------------------------
  !****f* Domain/domgra
  ! NAME
  !    domgra
  ! DESCRIPTION
  !    This routine allocates memory for the coefficients of 
  !    the mesh graph.
  ! OUTPUT
  !    NZDOM ... Number of nonzero coefficients of the graph
  !    R_DOM ... Pointer to the array of rows r_dom(npoin+1) (r_dom(ipoin) = 
  !              coefficient of the graph where row ipoin starts)
  !    C_DOM ... Pointer to the array of columns c_dom(nzdom) (c_dom (izdom)
  !              = column of the izdom coefficient of mesh graph)
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_kermod, only     :  ndivi
  use mod_memchk
  implicit none
  integer(ip)              :: ipoin,ielem,jelem,icoef
  integer(ip)              :: izdom,ncoef,nlelp,mtouc,lsize
  integer(4)               :: istat
  integer(ip), allocatable :: lista(:)
  logical(lg), allocatable :: touch(:)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

  call livinf(0_ip,'COMPUTE GRAPH',0_ip)
  !
  ! Compute node-element connectivity if not already computed: PELPO, LELPO
  !
  if( mpopo == 0 ) call connpo()
  !
  ! Compute node-node graph: C_DOM and R_DOM
  !
  allocate(r_dom(npoin+1),stat=istat)
  call memchk(zero,istat,memor_dom,'R_DOM','domgra',r_dom)

  if( kfl_lotme == 0 ) then

     !-------------------------------------------------------------------
     !
     ! Strategy 1: slow but does not require lots of memory
     !
     !-------------------------------------------------------------------

     mtouc = 0
     do ipoin = 1,npoin
        mtouc = max(mtouc,(pelpo(ipoin+1)-pelpo(ipoin))*mnode)
     end do
     nzdom = 0

     allocate(touch(mtouc),stat=istat)
     call memchk(zero,istat,memor_dom,'TOUCH','domgra',touch)
     do ipoin = 1,npoin
        nlelp          = pelpo(ipoin+1) - pelpo(ipoin)
        ncoef          = nlelp * mnode
        do icoef=1,ncoef          
           touch(icoef) = .false.
        end do
        call nzecof(&
             nlelp,ncoef,nzdom,lelpo(pelpo(ipoin)),touch)
     end do
     !
     ! Construct the array of indexes
     !
     allocate(c_dom(nzdom),stat=istat)
     call memchk(zero,istat,memor_dom,'C_DOM','domgra',c_dom)
     izdom = 1
     do ipoin = 1,npoin
        nlelp = pelpo(ipoin+1) - pelpo(ipoin)
        ncoef = nlelp * mnode
        do icoef = 1,ncoef          
           touch(icoef) = .false.
        end do
        call arrind(&
             nlelp,ncoef,lelpo(pelpo(ipoin)),touch,izdom,ipoin)
     end do

     r_dom(npoin+1) = nzdom + 1
     call memchk(two,istat,memor_dom,'TOUCH','domgra',touch)
     deallocate(touch,stat=istat)
     if(istat/=0) call memerr(two,'TOUCH','domgra',0_ip) 

  else

     !-------------------------------------------------------------------
     !
     ! Strategy 2: quick but requires lots of memory
     ! 
     !-------------------------------------------------------------------

     allocate(lista(mpopo),stat=istat)
     call memchk(zero,istat,memor_dom,'LISTA','domgra',lista)     
     !
     ! Construct the array of indexes
     !     
     r_dom(1) = 1
     do ipoin = 1, npoin
        lsize = 0
        do ielem = pelpo(ipoin), pelpo(ipoin+1)-1
           jelem = lelpo(ielem)
           call mergli( lista(r_dom(ipoin)), lsize, lnods(1,jelem), &
                lnnod(jelem), mone )
           !call mergli( lista(r_dom(ipoin)), lsize, lnods(1,jelem), &
           !     nnode(ltype(jelem)), mone )
        enddo
        r_dom(ipoin+1) = r_dom(ipoin) + lsize
     enddo
     nzdom = r_dom(npoin+1)-1
     allocate(c_dom(nzdom),stat=istat)
     call memchk(zero,istat,memor_dom,'C_DOM','domgra',c_dom)     
     do ipoin=1, nzdom
        c_dom(ipoin) = lista(ipoin)
     enddo
     call memchk(two,istat,memor_dom,'LISTA','domgra',lista)
     deallocate(lista,stat=istat)
     if(istat/=0) call memerr(two,'LISTA','domgra',0_ip)
  end if

  !-------------------------------------------------------------------
  !
  ! Order graph: the test LSIZE > 0 is necessary to treat nodes
  ! without graphs. Example: master nodes comming from a neighboring
  ! subdomain.
  ! 
  !-------------------------------------------------------------------

  do ipoin = 1,npoin
     lsize = r_dom(ipoin+1) - r_dom(ipoin)
     if( lsize > 0 ) call heapsorti1(2_ip,lsize,c_dom(r_dom(ipoin)))
  end do

  !-------------------------------------------------------------------
  !
  ! Compute profile and bandwidth
  ! 
  !-------------------------------------------------------------------

  call gtband(npoin,c_dom,r_dom,bandw_dom,profi_dom)

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine domgra
