subroutine elmca2(&
     pnode,pgaus,plapl,weigp,shapf,deriv,heslo,&
     elcod,gpvol,gpsha,gpcar,gphes,ielem)
  !-----------------------------------------------------------------------
  !****f* domain/elmca2
  ! NAME
  !    elmca2
  ! DESCRIPTION
  !    This routine calculates:
  !    GPCAR: Cartesian derivatives
  !    GPVOL: Unit volume
  !    GPHES: Hessian matrix
  ! USES
  !    invmtx
  ! USED BY
  !    ***_elmope
  !    extnor
  ! SOURCE
  !-----------------------------------------------------------------------
  use def_parame, only       :  twopi
  use def_domain, only       :  ndime,ntens,kfl_naxis,kfl_spher,mnode,&
       &                        kfl_savda,elmda,lelch,ltype
  use def_elmtyp, only       :  ELCUT
  use def_kintyp, only       :  ip,rp
  use mod_cutele, only       :  elmcar_cut
  implicit none
  integer(ip), intent(in)    :: pnode,plapl,ielem
  integer(ip), intent(inout) :: pgaus
  real(rp),    intent(in)    :: weigp(*)
  real(rp),    intent(in)    :: shapf(pnode,*)
  real(rp),    intent(in)    :: deriv(ndime,pnode,*)
  real(rp),    intent(in)    :: heslo(ntens,pnode,*)
  real(rp),    intent(in)    :: elcod(ndime,pnode)
  real(rp),    intent(out)   :: gpvol(*)
  real(rp),    intent(out)   :: gpsha(pnode,*)
  real(rp),    intent(out)   :: gpcar(ndime,mnode,*)
  real(rp),    intent(out)   :: gphes(ntens,mnode,*)
  integer(ip)                :: igaus,inode,idime,itens
  real(rp)                   :: gpcod,gpdet,d2sdx(27)
  real(rp)                   :: xjacm(9),xjaci(9)

  if( lelch(ielem) == ELCUT ) then

     call elmcar_cut(&
          ltype(ielem),pnode,plapl,ielem,elcod,pgaus,gpvol,gpsha,gpcar,gphes)

  else

     do inode = 1,pnode
        do igaus = 1,pgaus
           gpsha(inode,igaus) = shapf(inode,igaus)
        end do
     end do
     
     if( kfl_savda == 2 .and. plapl == 1 ) then

        do igaus = 1,pgaus
           gpvol(igaus) = elmda(ielem)%gpvol(igaus) 
           do inode = 1,pnode
              do itens = 1,ntens
                 gphes(itens,inode,igaus) = elmda(ielem)%gphes(itens,inode,igaus) 
              end do
              do idime = 1,ndime
                 gpcar(idime,inode,igaus) = elmda(ielem)%gpcar(idime,inode,igaus) 
              end do
           end do
        end do

     else if( kfl_savda == 2 .and. plapl == 0 ) then

        do igaus = 1,pgaus
           gpvol(igaus) = elmda(ielem)%gpvol(igaus) 
           do inode = 1,pnode
              do idime = 1,ndime
                 gpcar(idime,inode,igaus) = elmda(ielem)%gpcar(idime,inode,igaus) 
              end do
           end do
        end do
        do igaus = 1,pgaus
           do inode = 1,mnode
              do itens = 1,ntens
                 gphes(itens,inode,igaus) = 0.0_rp
              end do
           end do
        end do

     else

        if( plapl == 0 ) then

           !-------------------------------------------------------------------
           !
           ! GPCAR and GPVOL: Hessian not needed
           !
           !-------------------------------------------------------------------

           do igaus = 1,pgaus
              do inode = 1,pnode
                 do itens = 1,ntens
                    gphes(itens,inode,igaus) = 0.0_rp
                 end do
              end do
           end do

           if( ( ndime == 2 .and. pnode == 3 ) .or. ( ndime == 3 .and. pnode == 4 ) ) then
              !
              ! 2D-3D P1 element (linear elements)
              !
              call elmdel(pnode,ndime,elcod,gpcar,gpdet)
              gpvol(1) = weigp(1)*gpdet
              do igaus = 2,pgaus
                 do inode = 1,pnode
                    do idime = 1,ndime
                       gpcar(idime,inode,igaus) = gpcar(idime,inode,1)
                    end do
                 end do
                 gpvol(igaus) = gpvol(1)
              end do

           else
              !
              ! Other elements
              !
              do igaus = 1,pgaus
                 call jacobi(&
                      ndime,pnode,elcod,deriv(1,1,igaus),&
                      xjacm,xjaci,gpcar(1,1,igaus),gpdet)  
                 gpvol(igaus) = weigp(igaus) * gpdet
              end do
           end if

        else

           !-------------------------------------------------------------------
           !
           ! GPCAR, GPHES and GPVOL: Hessian needed
           !
           !-------------------------------------------------------------------

           do igaus = 1,pgaus
              call jacobi(&
                   ndime,pnode,elcod,deriv(1,1,igaus),&
                   xjacm,xjaci,gpcar(1,1,igaus),gpdet)
              gpvol(igaus) = weigp(igaus) * gpdet
              call elmhes(&
                   heslo(1,1,igaus),gphes(1,1,igaus),ndime,pnode,ntens,&
                   xjaci,d2sdx,deriv(1,1,igaus),elcod)
           end do
        end if

        if( kfl_naxis == 1 ) then
           !
           ! Axi-symmetric coordinates
           !
           do igaus = 1,pgaus
              gpcod = 0.0_rp
              do inode = 1,pnode
                 gpcod = gpcod + elcod(1,inode) * shapf(inode,igaus)
              end do
              gpvol(igaus) = twopi * gpcod * gpvol(igaus)
           end do

        else if( kfl_spher == 1 ) then
           !
           ! Spherical coordinates
           ! 
           do igaus = 1,pgaus
              gpcod = 0.0_rp
              do inode = 1,pnode
                 gpcod = gpcod + elcod(1,inode) * elcod(1,inode) * shapf(inode,igaus)
              end do
              gpvol(igaus) = 2.0_rp * twopi * gpcod * gpvol(igaus)
           end do

        end if

     end if

  end if

end subroutine elmca2
