subroutine opfpos(itask)
  !-----------------------------------------------------------------------
  !****f* Domain/opfpos
  ! NAME
  !    opfpos
  ! DESCRIPTION
  !    This subroutine gets ALL the file names to be used by Alya in two
  !    possible ways and them open them:
  ! 
  !    1. Recalling them from the environment, when Alya is launched
  !    encapsulated in a shell script, or
  ! 
  !    2. Composing the names out of the problem name which is given as argument
  !    when the binary file Alya is launched "naked". 
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_iofile
  use def_postpr
  implicit none
  integer(ip), intent(in) :: itask
  character(150)          :: filsa
  character(200)          :: messa

  select case (itask)

  case(1_ip)

     !-------------------------------------------------------------------
     !
     ! Open postprocess file
     !
     !-------------------------------------------------------------------

     if( INOTSLAVE ) then
        !
        ! Time step identifier 
        !
        write(nunam_pos,'(i8)') ittim         
        if( ittim < 10 ) then
           write(nunam_pos,'(a,i1)') '0000000',ittim
        else if( ittim < 100 ) then
           write(nunam_pos,'(a,i2)') '000000',ittim
        else if( ittim < 1000 ) then
           write(nunam_pos,'(a,i3)') '00000',ittim
        else if( ittim < 10000 ) then
           write(nunam_pos,'(a,i4)') '0000',ittim
        else if( ittim < 100000 ) then
           write(nunam_pos,'(a,i5)') '000',ittim
        else if( ittim < 1000000 ) then
           write(nunam_pos,'(a,i6)') '00',ittim
        else if( ittim < 10000000 ) then
           write(nunam_pos,'(a,i7)') '0',ittim
        end if
        !
        ! Compose file name: example.VELOC-123456.h5
        !
        filsa = adjustl(trim(namda))
     end if

     if(  wopos_pos(1) == 'COORD' .or. &
          wopos_pos(1) == 'LNODS' .or. &
          wopos_pos(1) == 'LTYPE' .or. &
          wopos_pos(1) == 'LNINV' .or. &
          wopos_pos(1) == 'LELCH' .or. &
          wopos_pos(1) == 'LNODB' .or. &
          wopos_pos(1) == 'LTYPB' .or. &
          wopos_pos(1) == 'LESUB' ) then
        !
        ! Open geometry arrays file
        !
        if( INOTSLAVE ) then
           fil_postp = trim(filsa)//'-'//trim(wopos_pos(1))//'.post.alyabin'
           call iofile(0_ip,lun_postp,fil_postp,'ALYA POSTPROCESS FILE','unknown','unformatted')
        end if

     else

        if( kfl_reawr == 0 ) then
           !
           ! Open postprocess file name
           !
           if( INOTSLAVE ) then
              fil_postp = trim(filsa)//'-'//trim(wopos_pos(1))//'-'//adjustl(trim(nunam_pos))//'.post.alyabin'
              call iofile(0_ip,lun_postp,fil_postp,'ALYA POSTPROCESS FILE','unknown','unformatted')
              write(lun_pos01,'(a)') trim(fil_postp)
              call flush(lun_pos01)
           end if

        else

           fil_postp = trim(filsa)//'-'//trim(wopos_pos(1))//'.rst'

           if( kfl_reawr == 1 ) then
              !
              ! Open restart file name for reading
              !
              if( INOTSLAVE ) call iofile(4_ip,lun_postp,fil_postp,'ALYA RESTART FILE','unknown','unformatted')
              call parari('BCT',0_ip,1_ip,kfl_reawr)
              if( kfl_reawr < 0 ) then
                 messa = 'CANNOT OPEN RESTART FILE: '//trim(fil_postp)
                 call outfor(2_ip,0_ip,trim(messa))
                 call livinf(0_ip,trim(messa),0_ip)
              end if
              if( kfl_reawr == 1 .and. INOTSLAVE ) then
                 call iofile(0_ip,lun_postp,fil_postp,'ALYA RESTART FILE','unknown','unformatted')
              end if

           else if( kfl_reawr == 2 .and. INOTSLAVE ) then
              !
              ! Open restart file name for writing
              !
              if( kfl_rsfil == 1 ) call appnum(ittim,fil_postp)
              call iofile(0_ip,lun_postp,fil_postp,'ALYA RESTART FILE','unknown','unformatted')
           end if

        end if
     end if

  case(2_ip)

     !-------------------------------------------------------------------
     !
     ! Close postprocess/restart file
     !
     !-------------------------------------------------------------------

     if( INOTSLAVE ) then
        if( kfl_reawr == 0 ) then
           call iofile(2_ip,lun_postp,' ','ALYA POSTPROCESS FILE')
        else
           call iofile(2_ip,lun_postp,' ','ALYA RESTART FILE')
        end if
     end if

  case(3_ip)

     !-------------------------------------------------------------------
     !
     ! Open voxel file
     !
     !-------------------------------------------------------------------

     if( INOTSLAVE ) then
        write(nunam_pos,'(i8)') ittim         
        if( ittim < 10 ) then
           write(nunam_pos,'(a,i1)') '0000000',ittim
        else if( ittim < 100 ) then
           write(nunam_pos,'(a,i2)') '000000',ittim
        else if( ittim < 1000 ) then
           write(nunam_pos,'(a,i3)') '00000',ittim
        else if( ittim < 10000 ) then
           write(nunam_pos,'(a,i4)') '0000',ittim
        else if( ittim < 100000 ) then
           write(nunam_pos,'(a,i5)') '000',ittim
        else if( ittim < 1000000 ) then
           write(nunam_pos,'(a,i6)') '00',ittim
        else if( ittim < 10000000 ) then
           write(nunam_pos,'(a,i7)') '0',ittim
        end if
	filsa = adjustl(trim(namda))
        fil_posvx = trim(filsa)//'-'//trim(wopos_pos(1))//'-'//adjustl(trim(nunam_pos))//'.bvox'
        open (unit=lun_posvx, FILE = trim(fil_posvx) ,ACTION='WRITE', &
             & STATUS = 'UNKNOWN', form='unformatted', access='stream' )
     end if

  end select

end subroutine opfpos
