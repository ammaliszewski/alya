subroutine renelm()
  !-----------------------------------------------------------------------
  !****f* domain/renelm
  ! NAME
  !    domain
  ! DESCRIPTION
  !    Renumber the elements
  ! OUTPUT
  ! USED BY
  !    Turnon
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_domain
  use def_master
  use mod_memory
  implicit none
  integer(ip)          :: ielem,jelem,iboun,pnodb,ipoin,ipmax,ipmin
  integer(ip)          :: inode,ienew,izone,ifiel
  integer(ip), pointer :: lrene(:)  
  integer(ip), pointer :: lpoim(:)  
  integer(ip), pointer :: ellis(:,:)
  integer(ip), pointer :: lren2(:)  
  !
  ! GEOMETRY
  !
  integer(ip), pointer :: lnods_tmp(:,:)      ! NELEM
  integer(ip), pointer :: ltype_tmp(:)        ! NELEM
  integer(ip), pointer :: lelch_tmp(:)        ! NELEM
  integer(ip), pointer :: lnnod_tmp(:)        ! NELEM
  integer(ip), pointer :: lesub_tmp(:)        ! NELEM
  !
  ! SETS
  !
  integer(ip), pointer :: leset_tmp(:)        ! NELEM
  !
  ! MATERIALS
  !
  integer(ip), pointer :: lmate_tmp(:)        ! NELEM
  !
  ! NUMBERING
  !
  integer(ip), pointer :: leinv_tmp(:)        ! NELEM

  call livinf(0_ip,'RENUMBER ELEMENTS',0_ip)
  if( IMASTER ) return

  !----------------------------------------------------------------------
  !
  ! Nullify pointers
  !
  !----------------------------------------------------------------------

  nullify(lrene)   
  nullify(lpoim)   
  nullify(ellis)     
  nullify(lren2)     
  nullify(lnods_tmp) 
  nullify(ltype_tmp) 
  nullify(lelch_tmp) 
  nullify(lnnod_tmp) 
  nullify(lesub_tmp) 
  nullify(leset_tmp) 
  nullify(lmate_tmp) 
  nullify(leinv_tmp) 
  
  !----------------------------------------------------------------------
  !
  ! Compute permutation array: LRENE
  !
  !----------------------------------------------------------------------
  !
  ! LRENE: Allocate memory
  !
  call memory_alloca(memor_dom,'LRENE','renelm',lrene,nelem)
  ! 
  ! This subroutine renumber the elements
  ! It assumes that the first point in the element is the smallest
  !
  call memory_alloca(memor_dom,'LPOIM'    ,'renelm',lpoim,npoin+1_ip)
  call memory_alloca(memor_dom,'LNODS_TMP','renelm',lnods_tmp,mnode,nelem)
  call memory_alloca(memor_dom,'ELLIS'    ,'renelm',ellis,2_ip,nelem)
  call memory_alloca(memor_dom,'LREN2'    ,'renelm',lren2,nelem)
  !
  ! Order LNODS in increasing order
  !
  do ielem = 1,nelem
     ipmin = huge(1_ip)
     ipmax = 0
     do inode = 1,nnode(ltype(ielem))
        ipoin = lnods(inode,ielem)
        if( ipoin > ipmax ) ipmax = ipoin
        if( ipoin < ipmin ) ipmin = ipoin
     end do
     ellis(1,ielem) = ipmin
     ellis(2,ielem) = ipmax
  end do
  !
  ! First reorder with respect to the max
  !
  do ielem = 1,nelem
     ipoin = ellis(2,ielem) + 1
     lpoim(ipoin) = lpoim(ipoin) + 1
  end do

  lpoim(1) = 1
  do ipoin = 2,npoin+1
     lpoim(ipoin) = lpoim(ipoin) + lpoim(ipoin-1)
  end do

  do ielem = 1,nelem
     ipoin        = ellis(2,ielem)
     ienew        = lpoim(ipoin)
     lrene(ielem) = ienew
     lpoim(ipoin) = lpoim(ipoin) + 1
  end do

  do ielem = 1,nelem
     ienew = lrene(ielem)
     do inode = 1,mnode
        lnods_tmp(inode,ienew) = lnods(inode,ielem)
     end do
  end do

  do ielem = 1,nelem
     do inode = 1,mnode
        lnods(inode,ielem) = lnods_tmp(inode,ielem)
     end do
  end do
  !
  ! Then with respect to the min
  !
  do ipoin = 1,npoin+1
     lpoim(ipoin) = 0
  end do

  do ielem = 1,nelem
     ipoin = ellis(1,ielem) + 1
     lpoim(ipoin) = lpoim(ipoin) + 1
  end do

  lpoim(1) = 1
  do ipoin = 2,npoin+1
     lpoim(ipoin) = lpoim(ipoin) + lpoim(ipoin-1)
  end do

  do ielem = 1,nelem
     ipoin        = ellis(1,ielem)
     ienew        = lpoim(ipoin)
     lren2(ielem) = ienew
     lpoim(ipoin) = lpoim(ipoin) + 1
  end do

  do ielem = 1,nelem
     ienew = lren2(ielem)
     do inode = 1,mnode
        lnods_tmp(inode,ienew) = lnods(inode,ielem) 
     end do
  end do

  do ielem = 1,nelem 
     do inode = 1,mnode
        lnods(inode,ielem) = lnods_tmp(inode,ielem) 
     end do
     lrene(ielem) = lren2(lrene(ielem))
  end do

  call memory_deallo(memor_dom,'LREN2'    ,'renelm',lren2)
  call memory_deallo(memor_dom,'ELLIS'    ,'renelm',ellis)
  call memory_deallo(memor_dom,'LNODS_TMP','renelm',lnods_tmp)
  call memory_deallo(memor_dom,'LPOIM'    ,'renelm',lpoim)

  !----------------------------------------------------------------------
  !
  ! LTYPE, LNNOD, LELCH, LBOEL, LESUB, LMATE and LEINV_LOC
  !
  !----------------------------------------------------------------------

  call memory_copy(memor_dom,'LTYPE_TMP','renelm',ltype    ,ltype_tmp,'DO_NOT_DEALLOCATE')
  call memory_copy(memor_dom,'LELCH_TMP','renelm',lelch    ,lelch_tmp,'DO_NOT_DEALLOCATE')
  call memory_copy(memor_dom,'LNNOD_TMP','renelm',lnnod    ,lnnod_tmp,'DO_NOT_DEALLOCATE')
  call memory_copy(memor_dom,'LESUB_TMP','renelm',lesub    ,lesub_tmp,'DO_NOT_DEALLOCATE')
  call memory_copy(memor_dom,'LMATE_TMP','renelm',lmate    ,lmate_tmp,'DO_NOT_DEALLOCATE')
  call memory_copy(memor_dom,'LEINV_TMP','renelm',leinv_loc,leinv_tmp,'DO_NOT_DEALLOCATE')

  do ielem = 1,nelem
     jelem            = lrene(ielem)
     ltype(jelem)     = ltype_tmp(ielem)
     lelch(jelem)     = lelch_tmp(ielem)
     lnnod(jelem)     = lnnod_tmp(ielem)
     lesub(jelem)     = lesub_tmp(ielem)
     lmate(jelem)     = lmate_tmp(ielem)
     leinv_loc(jelem) = leinv_tmp(ielem)
  end do

  call memory_deallo(memor_dom,'LEINV_TMP','renelm',leinv_tmp)
  call memory_deallo(memor_dom,'LMATE_TMP','renelm',lmate_tmp)
  call memory_deallo(memor_dom,'LESUB_TMP','renelm',lesub_tmp)
  call memory_deallo(memor_dom,'LNNOD_TMP','renelm',lnnod_tmp)
  call memory_deallo(memor_dom,'LELCH_TMP','renelm',lelch_tmp)
  call memory_deallo(memor_dom,'LTYPE_TMP','renelm',ltype_tmp)

  !----------------------------------------------------------------------
  !
  ! LBOEL
  !
  !----------------------------------------------------------------------

  do iboun = 1,nboun
     pnodb = nnode(ltypb(iboun))
     ielem = lboel(pnodb+1,iboun)
     if( ielem /= 0 ) then
        lboel(pnodb+1,iboun) = lrene(ielem)
     end if
  end do

  !----------------------------------------------------------------------
  !
  ! LESET
  !
  !----------------------------------------------------------------------

  if( neset > 0 ) then
     call memory_alloca(memor_dom,'LESET_TMP','renelm',leset_tmp,nelem)
     do ielem = 1,nelem
        leset_tmp(ielem) = leset(ielem)
     end do
     do ielem = 1,nelem
        jelem        = lrene(ielem)
        leset(jelem) = leset_tmp(ielem)
     end do
     call memory_deallo(memor_dom,'LESET_TMP','renelm',leset_tmp)
  end if

  !----------------------------------------------------------------------
  !
  ! LELEZ
  !
  !----------------------------------------------------------------------

  do izone = 1,nzone
     do jelem = 1,nelez(izone)
        ielem = lelez(izone) % l(jelem)
        lelez(izone) % l(jelem) = lrene(ielem)
     end do
  end do

  !----------------------------------------------------------------------
  !
  ! Fields
  !
  !----------------------------------------------------------------------

  do ifiel = 1,nfiel
     if( kfl_field(2,ifiel) == NELEM_TYPE ) then
        call memory_renumber(memor_dom,'XFIEL','renelm',xfiel(ifiel) % a,lrene)
     end if
  end do
  
  !----------------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !----------------------------------------------------------------------

  call memory_deallo(memor_dom,'LRENE','renelm',lrene)

end subroutine renelm
