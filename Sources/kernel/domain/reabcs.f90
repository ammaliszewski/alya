!-----------------------------------------------------------------------
!> @addtogroup DomainInput
!> @{
!> @file    reabcs.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Read boundary conditions
!> @details Read boundary conditions:
!!          \verbatim
!!          - Scalars: 
!!            - NCODN ..................... # codes on nodes (computed later on; here 1 or 0)
!!            - NCODB ..................... # codes on boundaries (computed later on; here 1 or 0)
!!            - KFL_GEOME ................. If geometrical conditions exist
!!            - KFL_CONVX ................. What to do with convex node (if geometrical conditions are applied)
!!            - KFL_ICODN ................. If there are codes on nodes
!!            - KFL_ICODB ................. If the are codes on boundaries
!!            - MCONO ..................... Max # codes per nodes
!!            - MVCOD ..................... Max # of node value arrays
!!            - NVCOD ..................... # of boundary value arrays
!!            - MVCOB ..................... Max # of node value arrays
!!            - NVCOB ..................... # of boundary value arrays
!!            - KFL_EXTRA ................. If boundary codes are extrapolated to nodes
!!            - NPBCS ..................... Max # of possibe geometrical conditions
!!            - GEOAN ..................... Geometrical angle to decide if we have corners (if geometrical conditions are applied)
!!          - Arrays:
!!            - LCODN(-MCODB-1:MCODB+1) ... If a code has been assigned to a node
!!            - LCODB(-MCODB-1:MCODB+1) ... If a code has been assigned to a boundary
!!            - KFL_CODNO(MCONO,NPOIN) .... Codes on nodes (can be multiple if extrapolated from KFL_CODBO)
!!            - KFL_CODBO(NBOUN) .......... Codes on boundaries
!!            - LVCOD(2,MVCOD) ............ Nodal value functions: dimension, function number
!!            - LVCOB(2,MVCOB) ............ Boundary value functions: dimension, function number
!!            - BVCOD(NVCOB)%a(1:NN,II) ... Nodal values on II of dimension NN
!!            - BVCOB(NVCOB)%a(1:NN,II) ... Boundary values on II of dimension NN
!!            - LMATE(NMATE) .............. Modify materials using boundary codes
!!            - LSBCS(8,100) .............. List of boundaries for a given geometrical code (first argument is condition)
!!          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine reabcs()
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_memchk
  implicit none
  integer(ip)          :: ipoin,iboun,icode,pnodb,knodb(60),jnode,kdime
  integer(ip)          :: icond,icobd,ipara,ielem,imate,inode,jcode
  integer(ip)          :: ilaye,inodb,nlaye,icono,ivcod,ivcob,idime,iposi
  integer(ip)          :: iread
  integer(4)           :: istat
  integer(ip), pointer :: lpbcs(:)
  integer(ip), pointer :: lnodb_tmp(:,:)
  character(300)       :: messa

  nullify(lpbcs)
  nullify(lnodb_tmp)

  if( kfl_camsh == 1 ) return

  if( ISEQUEN .or. ( IMASTER .and. kfl_ptask /= 2 ) ) then

     call livinf(0_ip,'READ BOUNDARY CONDITIONS',0_ip)
     !
     ! Initializations and allocations
     !
     if( kfl_examp == 0 ) then
        ncodn     =  0         ! There is no node code array
        ncodb     =  0         ! There is no boundary code array
        mcono     =  3         ! Max # codes per nodes
        mvcod     = 10         ! Nax # of node value arrays
        nvcod     =  0         ! # of node value arrays
        mvcob     = 10         ! Nax # of boundary value arrays
        nvcob     =  0         ! # of boundary value arrays
        kfl_extra =  0         ! Extrapolate from boundary to node
        icobd     = -99999
     end if
     kfl_geome = 0             ! Do not compute geometrical normals
     kfl_convx = 1             ! Use exnor for convex angle nodes
     kfl_frees = 0             ! Freestream criteria. 0: use wind angle to decide inflow/outflow
     npbcs     = 0             ! Max # of possibe geometrical conditions
     lsbcs     = 0             ! List of boundaries for a given geometrical code
     awind     =  0.0_rp       ! Wind angle for freestream
     tolan     =  0.0_rp       ! Tolerance used to define inflow from freestream 
     geoan     = 45.0_rp       ! Angle for geometrical normals
     !
     ! Allocate memory
     !
     call membcs( 8_ip)        ! LVCOD(2,MVCOD), BVCOD(MVCOD)
     call membcs(10_ip)        ! LVCOB(2,MVCOB), BVCOD(MVCOB)
     !
     ! Reach section
     !
     call ecoute('REABCS')
     do while( words(1) /= 'BOUND' )
        call ecoute('REABCS')
     end do
     !
     ! Check if boundary codes are extrapolated to nodes
     !
     if( kfl_examp == 0 ) then
        if( exists('EXTRA') ) then
           kfl_extra = 1 
           mcono     = 3
           if(exists('MAXIM')) mcono = getint('MAXIM',3_ip,'*MAXIMUM NUMBER OF B.C. PER NODE')
        end if
     else
        kfl_extra = 1
     end if
 
     if (kfl_extra == 0) then
        messa = 'WARNING: KERNEL WILL NOT EXTRAPOLATE FROM BOUNDARY CODES TO NODE CODES'
     else if (kfl_extra == 1) then
        messa = 'WARNING: KERNEL WILL EXTRAPOLATE FROM BOUNDARY CODES TO NODE CODES'
     end if
     call livinf(0_ip,messa,one)

     !
     ! Initialization of local variables
     !
     ivcod = 0
     ivcob = 0
     iread = 0
     !
     ! Code permutation
     !
     allocate( lpbcs(-mcodb:mcodb) , stat = istat )
     do icode = -mcodb,mcodb
        lpbcs(icode) = icode 
     end do
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Boundary conditions
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> BOUNDARY_CONDITIONS [, EXTRAPOLATE] 
     ! ADOC[d]> <b>BOUNDARY_CONDITIONS</b>:
     ! ADOC[d]> In this field, codes are assigned to nodes or boundaries. These codes
     ! ADOC[d]> are generic, just numbers, and do not have any physical interpretation.
     ! ADOC[d]> Each module should interpret these codes in the BOUNDARY_CONDITIONS field
     ! ADOC[d]> of its data file in terms of Dirichlet or Neumann conditions.<p>
     ! ADOC[d]> The EXTRAPOLATE option should be used to extrapolate boundary codes imposed on boundaries to nodes.
     ! ADOC[d]> If this option is used, a node can have multiple codes. For example, if a corner node belongs
     ! ADOC[d]> to 3 boundary elements with code 4,7 and 3, respectively, then the node inherits the three codes.
     ! ADOC[d]> Extrapolated boundary conditions from boundaries to nodes should be preferred if one plans
     ! ADOC[d]> to use the mesh multiplication algorithm: this is in fact the only way to uniquely define the
     ! ADOC[d]> boundary conditions on the refined meshes.
     ! ADOC[d]> Note that the ON_NODES conditions overwrites the extrapolated ones.
     !
     do while( words(1) /= 'ENDBO' )
        call ecoute('reabcs')

        if( words(1) == 'PERMU' ) then 
           call ecoute('reabcs')
           do while( words(1) /= 'ENDPE' )
              icode = int(param(1))
              jcode = int(param(2))
              !print*,'PERMUTOOOOO',icode,jcode
              if( icode < -mcodb .or. icode > mcodb ) call runend('REABCS: WRONG CODE PERMUTATION')
              if( jcode < -mcodb .or. jcode > mcodb ) call runend('REABCS: WRONG CODE PERMUTATION')
              lpbcs(icode) = jcode 
              call ecoute('reabcs')
           end do

        else if( words(1) == 'ONNOD' .and. kfl_examp == 0 ) then 

           !-------------------------------------------------------------
           !
           ! Node codes
           !
           !-------------------------------------------------------------
           !
           ! ADOC[1]> ON_NODES
           ! ADOC[2]>   ...
           ! ADOC[2]>   int1 int2                                                                   $ Node, code
           ! ADOC[2]>   ...
           ! ADOC[1]> END_ON_NODES
           ! ADOC[d]> <b>ON_NODES</b>:
           ! ADOC[d]> This field contains the list of node codes. int1 is the node and int2 the 
           ! ADOC[d]> associated code.
           !
           messa = 'WARNING: KERNEL SETS BOUNDARY CODES ON NODES'
           call livinf(0_ip,messa,one)

           if( exists('POSIT') ) then
              iposi = 1
           else
              iposi = 0
           end if

           if( exists('DEFAU') ) then
              icond = getint('DEFAU',0_ip,'*DEFAULT CODE ON NODES')
              if(icond>mcodb.or.icond<-mcodb)&
                   call runend('REABCS: WRONG DEFAULT NODE CODE')
           else
              icond = -99999
           end if

           call ecoute('reabcs')
           kfl_icodn = 1
           if( iread == 0 ) then ! Could have been allocated by mesh generator 
              ncodn = 1
              iread = 1
              call membcs(1_ip)
              do icode=-mcodb,mcodb
                 lcodn(icode)=.false.
              end do
              if(icond==-99999) then
                 do ipoin = 1,npoin
                    do icono = 1,mcono
                       kfl_codno(icono,ipoin) = mcodb+1
                    end do
                 end do
              else
                 lcodn(icond) = .true.
                 do ipoin = 1,npoin
                    kfl_codno(1,ipoin) = icond
                 end do
              end if
           else
              do ipoin = 1,npoin
                 icode = kfl_codno(1,ipoin)
                 lcodn(icode) = .true.
              end do
           end if

           if( iposi == 1 ) then
              !
              ! Positional
              !
              call poscod(1_ip)

           else
              !
              ! Given code
              !
              do while( words(1) /= 'ENDON' )
                 ipoin = int(param(1))
                 icode = int(param(2))
                 kfl_codno(1,ipoin) = lpbcs(icode)
                 lcodn(icode) = .true.
                 call ecoute('reabcs')
              end do

           end if

        else if( words(1) == 'ONBOU' .and. kfl_examp == 0 ) then 

           !-------------------------------------------------------------
           !
           ! Boundary codes
           !
           !-------------------------------------------------------------
           !
           ! ADOC[1]> ON_BOUNDARIES [, DEFAULT= int3]
           ! ADOC[1]>   ...
           ! ADOC[1]>   int1 int2                                                                   $ Node, code
           ! ADOC[1]>   ...
           ! ADOC[1]> END_ON_BOUNDARIES
           ! ADOC[d]> <b>BOUNDARIES</b>:
           ! ADOC[d]> This field contains the list of boundary codes. int1 is the boundary number and int2 the associated code.
           ! ADOC[d]> The default option enables to impose automatically the code int3 to all the boundaries.
           !


           messa = 'WARNING: KERNEL READS BOUNDARY CODES ON BOUNDARIES'
           call livinf(0_ip,messa,one)

           if( exists('POSIT') ) then
              iposi = 1
           else
              iposi = 0
           end if

           if( exists('DEFAU') ) then
              icond = getint('DEFAU',0_ip,'*DEFAULT CODE ON BOUNDARIES')
              if( icond > mcodb .or. icond < -mcodb ) &
                   call runend('REABCS: WRONG DEFAULT BOUNDARIES CODE')
           else
              icond = mcodb + 1
           end if

           if( words(2) == 'UNKNO' ) then
              call ecoute('reabcs')

              allocate(lnodb_tmp(mnodb,nboun))
              do iboun = 1,nboun
                 pnodb = nnode(ltypb(iboun))
                 lnodb_tmp(1:mnodb,iboun) = lnodb(1:mnodb,iboun)
                 call heapsorti1(2_ip,pnodb,lnodb_tmp(1,iboun))
              end do
              if( words(1) /= 'ENDON' ) then
                 ncodb = 1
                 kfl_icodb = 1
                 if( .not. associated(kfl_codbo) ) then
                    call membcs(2_ip)
                    do iboun = 1,nboun
                       kfl_codbo(iboun) = icond
                    end do
                    do icode = -mcodb,mcodb
                       lcodb(icode) = .false.
                    end do
                 else 
                    do iboun = 1,nboun
                       icode = lpbcs(kfl_codbo(iboun))
                       lcodb(icode) = .true.
                    end do
                 end if
                 if( iposi == 1 ) then
                    !
                    ! Positional
                    !
                    !call poscod(2_ip)  !falta crear
                    call runend('POSCOD: POSITIONAL makes no sense for  UNKNO') 

                 else
                    !
                    ! Given code
                    !
                    do while( words(1) /= 'ENDON')
                       pnodb = int(param(2))
                       knodb(1:pnodb) = int(param(3:2+pnodb))
                       call heapsorti1(2_ip,pnodb,knodb)
                       call finbou_fast(pnodb,knodb,iboun,lnodb_tmp)
                       !call finbou(pnodb,knodb,iboun)
                       if( iboun == 0 .or. iboun > nboun ) then
                          icode = int(param(3+pnodb))
                          print*,'BOUNDARY= ',knodb(1:pnodb), 'LAST CODE=',icode
                          call runend('REABCS: BOUNDARY CODES HAVE BEEN IMPOSED ON A NON-EXISTING BOUNDARY')
                       else
                          icode = int(param(3+pnodb))
                          kfl_codbo(iboun) = lpbcs(icode)
                          lcodb(icode) = .true.
                       end if
                       call ecoute('reabcs')
                    end do
                 end if
              end if
              deallocate(lnodb_tmp)

           else
              call ecoute('reabcs')
              if( words(1) /= 'ENDON' ) then
                 ncodb = 1
                 kfl_icodb = 1
                 if( .not. associated(kfl_codbo) ) then
                    call membcs(2_ip)
                    do iboun = 1,nboun
                       kfl_codbo(iboun) = mcodb + 1
                    end do
                    do icode = -mcodb,mcodb
                       lcodb(icode) = .false.
                    end do
                 else 
                    do iboun = 1,nboun
                       icode = lpbcs(kfl_codbo(iboun))
                       lcodb(icode) = .true.
                    end do
                 end if
                 if( iposi == 1 ) then
                    !
                    ! Positional
                    !
                    call poscod(2_ip)
                 else
                    !
                    ! Given code
                    !                   
                    do while( words(1) /= 'ENDON' )
                       iboun = int(param(1))
                       icode = int(param(2))
                       if (iboun == 0) then
                          messa = 'WARNING: KERNEL FOUND NO DIRECT CONDITIONS ON_NODES'
                          call livinf(0_ip,messa,one)                          
                       else
                          kfl_codbo(iboun) = lpbcs(icode)
                          lcodb(icode) = .true.
                       end if
                       call ecoute('reabcs')
                    end do
                 end if
              end if
           end if

        else if( words(1) == 'MATER' ) then 

           !-------------------------------------------------------------
           !
           ! Materials depending on boundary codes
           !
           !-------------------------------------------------------------
           !
           ! ADOC[1]> MATERIALS, BOUNDARIES, DEFAULT= int
           ! ADOC[1]>   CODE=     int                                                               $ From which code material should be generated
           ! ADOC[1]>   MATERIAL= int                                                               $ Material number
           ! ADOC[1]>   LAYERS=   int                                                               $ Number of element layers
           ! ADOC[1]> END_MATERIALS 
           ! ADOC[d]> <b>MATERIALS, BOUNDARIES</b>:
           ! ADOC[d]> This option enables to generate automatically a certain number of element layers (LAYERS= int) of one material (MATERIAL= int)       
           ! ADOC[d]> starting from boundaries with a given code (CODE= int). The option DEFAULT= int assign automatically this material
           ! ADOC[d]> to all the element before applying the layers. It can be useful for example in CFD to define a different material at outflows
           ! ADOC[d]> on which the viscosity will be higher.
           !
           if( words(2) == 'BOUND' ) then

              if( exists('DEFAU') ) then
                 imate = getint('DEFAU',1_ip,'*DEFAULT MATERIAL')
                 do ielem = 1,nelem
                    lmate(ielem) = imate
                 end do
              end if
              call ecoute('reabcs')
              call memgen(1_ip,npoin,0_ip)

              do while(words(1)/='ENDMA')
                 if( exists('CODE ') ) icode = getint('CODE ',1_ip,'*MATERIAL DEPENDING ON CODE')
                 if( exists('MATER') ) imate = getint('MATER',1_ip,'*MATERIAL DEPENDING ON CODE')
                 if( exists('LAYER') ) nlaye = getint('LAYER',1_ip,'*MATERIAL DEPENDING ON CODE')
                 call ecoute('reabcs')
              end do

              do iboun = 1,nboun
                 if( kfl_codbo(iboun) == icode ) then
                    do inodb = 1,nnode(ltypb(iboun))
                       gisca(lnodb(inodb,iboun)) = 1
                    end do
                    ielem = lboel(nnode(ltypb(iboun))+1,iboun)
                    if( ielem == 0 ) call runend('REABCS: WRONG ELEMENT NUMBER')
                    lmate(ielem) = imate
                    do inode = 1,nnode(ltype(ielem))
                       gisca(lnods(inode,ielem)) = 1
                    end do
                 end if
              end do

              do ilaye = 1,nlaye-1
                 do ielem = 1,nelem
                    inode = 1
                    do while( inode <= nnode(ltype(ielem)) )
                       if( gisca(lnods(inode,ielem)) == ilaye ) then
                          do jnode = 1,nnode(ltype(ielem))
                             ipoin = lnods(jnode,ielem)
                             if( gisca(ipoin) == 0 ) gisca(ipoin) = ilaye + 1
                          end do
                          lmate(ielem) = imate
                          inode = nnode(ltype(ielem))
                       end if
                       inode = inode + 1
                    end do
                 end do
              end do

              call memgen(3_ip,npoin,0_ip)

           end if

        else if( words(1) == 'GEOME' ) then 

           !-------------------------------------------------------------
           !
           ! Geometrical boundaries
           !
           ! Define boundary codes: 
           ! Prescribed ......... 1
           ! Freestream ......... 2
           ! Wall law ........... 3
           ! Symmetry / Splip ... 4
           ! No slip / Wall ..... 5
           ! Free / Outflow ..... 6
           !
           !-------------------------------------------------------------
           !
           ! ADOC[1]> GEOMETRICAL_BOUNDARY_CONDITIONS
           ! ADOC[1]>   PRESCRIBED          = int, int...                                           $ List of boundary codes 
           ! ADOC[1]>   FREE                = int, int...                                           $ List of boundary codes 
           ! ADOC[1]>   WALL_LAW            = int, int...                                           $ List of boundary codes 
           ! ADOC[1]>   WALL                = int, int...                                           $ List of boundary codes 
           ! ADOC[1]>   SYMMETRY            = int, int...                                           $ List of boundary codes 
           ! ADOC[1]>   FREESTREAM          = int, int...                                           $ List of boundary codes 
           ! ADOC[1]>   ANGLE    , GEOAN    = real                                                  $ Geometrical angle
           ! ADOC[1]>   CONVEX              = FREE | EXNOR | FIXED | GEOMETRICAL                    $ What to do with convex corner nodes
           ! ADOC[1]>   CRITERION_FREESTREAM: WIND_ANGLE= real1 | VALUE_FUNCTION= int, TOLER=real2  $ Criterion: freestream is inflow or outflow
           ! ADOC[1]> END_GEOMETRICAL_BOUNDARY_CONDITIONS
           ! ADOC[d]> <b>GEOMETRICAL_BOUNDARY_CONDITIONS</b>:
           ! ADOC[d]> This option enables to impose geometrical based boundary conditions
           ! ADOC[d]> and let Alya decide on the computation of local axis and condition type.
           ! ADOC[d]> This can be useful essentially in CFD to compute automatically local basis
           ! ADOC[d]> and to impose freestream boundary conditions.
           ! ADOC[d]> For this, one must associate the boundary codes with geometrical conditions:
           ! ADOC[d]> PRESCRIBED, FREE, WALL_LAW, WALL, SYMMETRY (SLIP), FREESTREAM, ANGLE, CONVEX.
           ! ADOC[d]> The FREESTREAM is converted into inflow according to the wind angle defined in the 
           ! ADOC[d]> *.ker.dat file. 
           ! ADOC[d]> Use ANGLE to decide the limit above which we have a corner. if n1.n2 < cos(ANGLE) then
           ! ADOC[d]> the node is a corner node.
           ! ADOC[d]> Option CONVEX is used to decide what to do with convex corner nodes: free the node,
           ! ADOC[d]> use the numerical normal, the geometrical normal, or fix the node as a wall node.
           ! ADOC[d]> The CRITERION_FREESTREAM option enables to decide wether the freestream should be converted
           ! ADOC[d]> into inflow or outflow. WIND_ANGLE (real1) is the horizontal angle: we have an inflow if angle.n <= 0.
           ! ADOC[d]> One can also choose a VALUE FUNCTION field.
           ! ADOC[d]> Tolerance enables to controle more inflow; if angle.n <= cos(real2), the freestream node is inflow.
           !
           kfl_geome = 1

           do while( words(1) /= 'ENDGE' )

              icond = 0
              if( words(1) == 'PRESC' .or. words(1) == 'INFLO' ) then
                 icond = 1
              else if( words(1) == 'FREES' ) then
                 icond = 2
              else if( words(1) == 'WALLL' ) then
                 icond = 3
              else if( words(1) == 'SYMME' .or.  words(1) == 'SLIPW' ) then
                 icond = 4
              else if( words(1) == 'NOSLI' .or. words(1) == 'WALL ' ) then
                 icond = 5
              else if( words(1) == 'FREE ' .or. words(1) == 'OUTFL' ) then
                 icond = 6

              else if( words(1) == 'ANGLE' ) then
                 geoan = getrea('GEOAN',45.0_rp,'#Geometric angle')

              else if( words(1) == 'CONVE' ) then
                 if( words(2) == 'FREE ') then
                    kfl_convx = 0
                 else if(words(2) == 'EXNOR') then
                    kfl_convx = 1
                 else if(words(2) == 'FIXED') then
                    kfl_convx = 2
                 else if(words(2) == 'GEOME') then
                    kfl_convx = 3
                 end if

              else if( words(1) == 'CRITE' ) then
                 if( words(2) == 'WINDA') then
                    kfl_frees = 0
                    awind = getrea('WINDA',0.0_rp,'#Wind angle')
                    awind = awind / 180.0_rp * pi

                 else if(words(2) == 'VALUE') then
                    kfl_frees = getint('VALUE',1_ip,'*FUNCTION NUMBER')
                 end if

                 if( exists('TOLER') ) then
                    tolan = getrea('TOLER',5.0_rp,'#Tolerance for determining inflow')
                    tolan = tolan / 180.0_rp * pi
                 end if

              end if
              if( icond /= 0 ) then
                 npbcs(icond) = nnpar
                 if( nnpar > size(lsbcs,1) ) call runend('REABCS: TOO MANY PARAMETERS')
                 do ipara = 1, nnpar
                    lsbcs(ipara,icond) = int(param(ipara),ip) 
                 end do
              end if
              call ecoute('reabcs')

           end do

        else if( words(1) == 'VALUE' ) then 
           !
           ! ADOC[1]> VALUES, FUNCTION= int, DIMENSION= int, ON_NODES/ON_BOUNDARIES, CODE= int
           ! ADOC[1]>   int real real...                                                            $ Node/boundary, value1, value2...
           ! ADOC[1]>   ...
           ! ADOC[1]> END_VALUES
           ! ADOC[d]> <b>VALUES</b>:
           ! ADOC[d]> Define function (FUNCTION= int) of a given dimension (DIMENSION= int) of nodal/boundary values to 
           ! ADOC[d]> be used when imposing boundary conditions. This can be useful for example to impose a Dirichlet
           ! ADOC[d]> or initial condition that is non-constant in space. If the option CODE (only for nodal values) is present,
           ! ADOC[d]> Alya imposes this code on the node listed in the field.
           !
           if( exists('BOUND') .or. exists('ONBOU') ) then

              !-------------------------------------------------------------
              !
              ! Values on boundaries
              !
              !-------------------------------------------------------------

              kdime = 0
              if( exists('FUNCT') ) then
                 nvcob = getint('FUNCT',1_ip,'*FUNCTION NUMBER')
                 if( nvcob > mvcob .or. nvcob <= 0 ) &
                      call runend('REABCS: WRONG DEFAULT VALUE FUNCTION')
              end if
              if( exists('DIMEN') ) then
                 kdime = getint('DIMEN',1_ip,'*DIMENSION')
              end if

              call ecoute('REABCS')
              if( kdime == 0 ) kdime = nnpar - 1
              ivcob = ivcob + 1
              lvcob(1,nvcob) = ivcob             ! Function number
              lvcob(2,nvcob) = nnpar - 1         ! Dimension
              call membcs(11_ip)                 ! Allocate memory BVCOB(NVCOB) % A ( LVCOB(2,NVCOB) , NBOUN )

              do while( words(1) /= 'ENDVA' )
                 iboun = int(param(1))
                 do idime = 1,kdime
                    bvcob(nvcob)%a(idime,iboun) = param(idime+1)
                 end do
                 call ecoute('reabcs')
              end do

           else

              !-------------------------------------------------------------
              !
              ! Values on nodes
              !
              !-------------------------------------------------------------

              kdime = 0
              if( exists('FUNCT') ) then
                 nvcod = getint('FUNCT',1_ip,'*FUNCTION NUMBER')
                 if( nvcod > mvcod .or. nvcod <= 0 ) &
                      call runend('REABCS: WRONG DEFAULT VALUE FUNCTION')
              end if
              if( exists('DIMEN') ) then
                 kdime = getint('DIMEN',1_ip,'*DIMENSION')
              end if
              icond = mcodb+1
              if( exists('CODE ') ) then
                 icond = getint('CODE ',0_ip,'*DEFAULT CODE ON NODES')
              end if

              if( nvcod < 1 ) call runend('REABCS: WRONG CODE NUMBER')
              call ecoute('REABCS')
              if( kdime == 0 ) kdime = nnpar - 1
              ivcod = ivcod + 1
              lvcod(1,nvcod) = ivcod      ! Function number
              lvcod(2,nvcod) = kdime      ! Dimension
              call membcs(9_ip)           ! Allocate memory BVCOD(NVCOD) % A ( LVCOD(2,NVCOD) , NPOIN )

              if( icond /= mcodb + 1 ) then
                 do while( words(1) /= 'ENDVA' )
                    ipoin = int(param(1))
                    kfl_codno(1,ipoin) = icond
                    kfl_codno(2:mcono,ipoin) = mcodb+1
                    do idime = 1,kdime
                       bvcod(nvcod)%a(idime,ipoin) = param(idime+1)
                    end do
                    call ecoute('reabcs')
                 end do
              else
                 do while( words(1) /= 'ENDVA' )
                    ipoin = int(param(1))
                    do idime = 1,kdime
                       bvcod(nvcod)%a(idime,ipoin) = param(idime+1)
                    end do
                    call ecoute('reabcs')
                 end do
              end if

           end if

        end if

     end do
     !
     ! ADOC[0]> END_BOUNDARY_CONDITIONS
     !
     !
     ! Deallocate memory
     !
     deallocate( lpbcs )
     call livinf(-5_ip,'END READ MESH ARRAYS',0_ip)
 
  end if

end subroutine reabcs
