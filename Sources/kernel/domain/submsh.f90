subroutine submsh()
  !-----------------------------------------------------------------------
  !****f* domain/submsh
  ! NAME
  !    submsh
  ! DESCRIPTION
  !    This subroutine recursively subdivides the mesh
  ! OUTPUT
  ! USED BY
  !    Turnon
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_domain
  use def_master
  use def_kermod
  implicit none
  integer(ip) :: idivi
  real(rp)    :: time1,time2,time3,time4

  call savmsh(1_ip)

  if( ndivi > 0 ) then

     call livinf(-4,'START MESH SUBDIVISION',0_ip)
     !
     ! Subdivide recursively the mesh
     !
     call Parall(421_ip)                             ! Parall: Allocate some memory
     call submem(1_ip)                               ! Allocate memory
     do idivi = 1,ndivi
        call livinf(-6_ip,'LEVEL',idivi)
        call cputim(time1)
        call ledges()                                ! Edge table
        call cputim(time2)
        call lfaces()                                ! Face table
        call cputim(time3)
        call subelm( )                               ! Subdivide elements
        call cputim(time4) 
        call Parall(422_ip)                          ! Parall: Reconstruct interface
        call subdim()                                ! New mesh dimensions
        call submem(2_ip)                            ! Deallocate memory
        call domvar(2_ip)                            ! LNUTY...
        call Parall(423_ip)                          ! Parall: Perform some checkings

        if( kfl_timin == 1 ) call Parall(20_ip)
        call livinf(-5_ip,'END LEVEL',idivi)
        cpu_other(1) = cpu_other(1) + time2 - time1 
        cpu_other(2) = cpu_other(2) + time3 - time2 
        cpu_other(3) = cpu_other(3) + time4 - time3 
     end do

     call livinf(-5,'END MESH SUBDIVISION',0_ip)

  else

     call submem(1_ip)                               ! Allocate memory

  end if

  call savmsh(2_ip)

end subroutine submsh

subroutine ledges()
  !-----------------------------------------------------------------------
  !****f* domain/ledges
  ! NAME
  !    ledges
  ! DESCRIPTION
  !    Create edge table
  ! OUTPUT
  !    NNEDG ... Number of edges
  !    LEDGG ... Edge table
  !    LEDGB ... Boundary edge table (when Parall is on)
  ! USED BY
  !    Turnon
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_domain
  use def_master
  use mod_memchk
  implicit none
  integer(ip) :: mpop2,ipoin,lsize,iedgg,ielem,jelem
  integer(ip) :: inode,ilisn,nlelp,jpoin
  integer(4)  :: istat

  call livinf(0_ip,'EDGE TABLE',0_ip)

  if( INOTMASTER ) then
     !
     ! Deallocate node-element graph if needed
     !
     if( mpopo > 0 ) then
        mpopo = 0
        call memchk(two,istat,memor_dom,'LELPO','submsh',lelpo)
        deallocate(lelpo,stat=istat)
        if(istat/=0) call memerr(two,'LELPO','submsh',0_ip)
        call memchk(two,istat,memor_dom,'PELPO','submsh',pelpo)
        deallocate(pelpo,stat=istat)
        if(istat/=0) call memerr(two,'PELPO','submsh',0_ip)
     end if
     !
     ! Allocate memory for NEPOI and compute it
     !
     allocate(nepoi(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'NEPOI','submsh',nepoi)
     mpop2 = 0
     do ielem = 1,nelem
        mpop2 = mpop2 + lnnod(ielem)*lnnod(ielem)
        do inode = 1,lnnod(ielem)
           ipoin = lnods(inode,ielem)
           nepoi(ipoin) = nepoi(ipoin) + 1
        end do
     end do
     !
     ! Allocate memory for PELPO and compute it
     !
     allocate(pelpo(npoin+1),stat=istat)
     call memchk(zero,istat,memor_dom,'PELPO','submsh',pelpo)
     pelpo(1) = 1
     do ipoin = 1,npoin
        pelpo(ipoin+1) = pelpo(ipoin) + nepoi(ipoin)
     end do
     !
     ! Allocate memory for LELPO and construct the list
     !
     nlelp = pelpo(npoin+1)
     allocate(lelpo(nlelp),stat=istat)
     call memchk(zero,istat,memor_dom,'LELPO','submsh',lelpo)
     do ielem = 1,nelem
        do inode = 1,lnnod(ielem)
           ipoin = lnods(inode,ielem)
           lelpo(pelpo(ipoin)) = ielem
           pelpo(ipoin) = pelpo(ipoin) + 1
        end do
     end do
     !
     ! Recompute PELPO and maximum number of element neighbors MEPOI
     !
     pelpo(1) =  1
     do ipoin = 1,npoin
        pelpo(ipoin+1) = pelpo(ipoin) + nepoi(ipoin)
     end do
     !
     ! Allocate memory
     !
     allocate(ledgp(mpop2),stat=istat)
     call memchk(zero,istat,memor_dom,'LEDGP','submsh',ledgp)     
     allocate(pedgp(npoin+1),stat=istat)
     call memchk(zero,istat,memor_dom,'PEDGP','submsh',pedgp)     
     !
     ! Construct the array of indexes
     !     
     pedgp(1) = 1
     do ipoin = 1,npoin
        lsize = 0
        do ielem = pelpo(ipoin),pelpo(ipoin+1)-1
           jelem = lelpo(ielem)
           call mergl4( ipoin, ledgp(pedgp(ipoin)), lsize, lnods(1,jelem), &
                lnnod(jelem) )
        end do
        pedgp(ipoin+1) = pedgp(ipoin) + lsize
     end do
     nedgg = pedgp(npoin+1) - 1
     !
     ! Fill in edge table
     !
     allocate( ledgg(4,nedgg), stat=istat )
     call memchk(zero,istat,memor_dom,'LEDGG','submsh',ledgg)     
     iedgg = 0
     do ipoin = 1,npoin
        do ilisn = 1,pedgp(ipoin+1)-pedgp(ipoin)
           iedgg          = iedgg + 1
           jpoin          = ledgp(iedgg)
           ledgg(1,iedgg) = jpoin
           ledgg(2,iedgg) = ipoin
        end do
     end do
     !
     ! Deallocate memory
     !
     call memchk(two,istat,memor_dom,'LEDGP','submsh',ledgp)
     deallocate(ledgp,stat=istat)
     if(istat/=0) call memerr(two,'LEDGP','submsh',0_ip)

     !call memchk(two,istat,memor_dom,'LELPO','submsh',lelpo)
     !deallocate(lelpo,stat=istat)
     !if(istat/=0) call memerr(two,'LELPO','submsh',0_ip)
     !call memchk(two,istat,memor_dom,'PELPO','submsh',pelpo)
     !deallocate(pelpo,stat=istat)
     !if(istat/=0) call memerr(two,'PELPO','submsh',0_ip)
     !call memchk(two,istat,memor_dom,'NEPOI','submsh',nepoi)
     !deallocate(nepoi,stat=istat)
     !if(istat/=0) call memerr(two,'NEPOI','submsh',0_ip)

  end if

end subroutine ledges

subroutine subelm()
  !-----------------------------------------------------------------------
  !****f* domain/subelm
  ! NAME
  !    domain
  ! DESCRIPTION
  !    Subdivide elements
  !    Nodes are numbered as follows:
  !    1 ................... NPOIN_OLD:       Old nodes
  !    NPOIN_OLD+1 ......... NPOIN_OLD+NEDDG: Edge nodes
  !    NPOIN_OLD+NEDDG+1 ... NPOIN:           Central nodes
  ! OUTPUT
  ! USED BY
  !    submsh
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_elmtyp
  use def_domain
  use def_master
  use def_kermod
  use mod_memchk
  use mod_memory
  implicit none
  integer(ip)          :: ielem,inode,ipoin,idime,ivcod,pnode
  integer(ip)          :: ipoi1,ipoi2,ipoi3,ipoi4,izone,kk
  integer(ip)          :: node1,node2,node3,node4,npoif,iface
  integer(ip)          :: iedgg,kelem,ivcob,pelty,ipoif,ipoic
  integer(ip)          :: iboun,inodb,kboun,ii,npoic,jpoin
  integer(ip)          :: lnodx(100),igrou,jgrou,ifacg,ifiel
  integer(ip)          :: code1,code2,code3,code4,mcod1
  integer(ip)          :: howdi(nelty),nelez_old,knode
  integer(4)           :: istat
  real(rp)             :: dummr,xx(8)
  !
  ! GEOMETRY
  !
  integer(ip), pointer :: lnods_old(:,:)     => null()  ! NELEM
  integer(ip), pointer :: ltype_old(:)       => null()  ! NELEM
  integer(ip), pointer :: lelch_old(:)       => null()  ! NELEM
  integer(ip), pointer :: lnnod_old(:)       => null()  ! NELEM
  integer(ip), pointer :: lesub_old(:)       => null()  ! NELEM
  integer(ip), pointer :: lnodb_old(:,:)     => null()  ! NBOUN
  integer(ip), pointer :: lboel_old(:,:)     => null()  ! NBOUN
  integer(ip), pointer :: ltypb_old(:)       => null()  ! NBOUN
  integer(ip), pointer :: lboch_old(:)       => null()  ! NBOUN
  real(rp),    pointer :: coord_old(:,:)     => null()  ! NPOIN
  integer(ip), pointer :: lgrou_dom_old(:)   => null()  ! NPOIN
  !
  ! FIELDS
  !
  type(r2p),   pointer :: xfiel_old(:)       => null()  ! NPOIN/NBOUN
  !
  ! ZONES
  !
  integer(ip), pointer :: lelez_old(:,:)     => null()  ! NELEM
  !
  ! MATERIALS
  !
  integer(ip), pointer :: lmate_old(:)       => null()  ! NELEM
  !
  ! SETS
  !
  integer(ip), pointer :: leset_old(:)       => null()  ! NELEM
  integer(ip), pointer :: lbset_old(:)       => null()  ! NBOUN
  !
  ! BOUNDARY CONDITIONS
  !
  integer(ip), pointer :: kfl_codno_old(:,:) => null()  ! NPOIN
  type(r2p),   pointer :: bvcod_old(:)       => null()  ! NPOIN
  integer(ip), pointer :: kfl_codbo_old(:)   => null()  ! NBOUN 
  type(r2p),   pointer :: bvcob_old(:)       => null()  ! NBOUN
  !
  ! INTERPOLATION
  !
  type(linno_type), pointer :: linno(:)      => null()  ! NPOIN
  !
  ! Permutation with original mesh
  !
  integer(ip), pointer :: lnlev_old(:)       => null()  ! NPOIN
  integer(ip), pointer :: lelev_old(:)       => null()  ! NELEM
  integer(ip), pointer :: lblev_old(:)       => null()  ! NBOUN

  !--------------------------------------------------------------------
  !
  ! Nullify pointers
  !
  !--------------------------------------------------------------------

  nullify(lnods_old)    
  nullify(ltype_old)      
  nullify(lelch_old)      
  nullify(lnnod_old)      
  nullify(lesub_old)      
  nullify(lnodb_old)    
  nullify(lboel_old)    
  nullify(ltypb_old)      
  nullify(lboch_old)      
  nullify(coord_old)    
  nullify(lgrou_dom_old)  
  nullify(xfiel_old)      
  nullify(lelez_old)    
  nullify(lmate_old)      
  nullify(leset_old)      
  nullify(lbset_old)      
  nullify(kfl_codno_old)
  nullify(bvcod_old)      
  nullify(kfl_codbo_old)  
  nullify(bvcob_old)      
  nullify(linno)          
  nullify(lblev_old)      

  !--------------------------------------------------------------------
  !
  ! How elements are divided
  !
  !--------------------------------------------------------------------

  if( ndime == 2 ) then
     do pelty = 1,nelty
        howdi(pelty) = 4
     end do
  else
     do pelty = 1,nelty
        howdi(pelty) = 8
     end do
     howdi(PYR05) = 10
  end if

  !--------------------------------------------------------------------
  !
  ! Copy old mesh data
  !
  !--------------------------------------------------------------------

  call livinf(0_ip,'ADD NEW NODES AND ELEMENTS',0_ip)

  if( INOTMASTER ) then

     nelem_old = nelem
     nboun_old = nboun
     npoin_old = npoin

     call memory_alloca(memor_dom,'LNODS_OLD','subelm',lnods_old,mnode,nelem,  'DO_NOT_INITIALIZE')  
     call memory_alloca(memor_dom,'LTYPE_OLD','subelm',ltype_old,nelem,        'DO_NOT_INITIALIZE')         
     call memory_alloca(memor_dom,'LELCH_OLD','subelm',lelch_old,nelem,        'DO_NOT_INITIALIZE')         
     call memory_alloca(memor_dom,'LNNOD_OLD','subelm',lnnod_old,nelem,        'DO_NOT_INITIALIZE')         
     call memory_alloca(memor_dom,'LESUB_OLD','subelm',lesub_old,nelem,        'DO_NOT_INITIALIZE')         
     call memory_alloca(memor_dom,'LNODB_OLD','subelm',lnodb_old,mnodb,nboun,  'DO_NOT_INITIALIZE')  
     call memory_alloca(memor_dom,'LBOEL_OLD','subelm',lboel_old,mnodb+1,nboun,'DO_NOT_INITIALIZE') 
     call memory_alloca(memor_dom,'LTYPB_OLD','subelm',ltypb_old,nboun,        'DO_NOT_INITIALIZE')         
     call memory_alloca(memor_dom,'LBOCH_OLD','subelm',lboch_old,nboun,        'DO_NOT_INITIALIZE')         
     call memory_alloca(memor_dom,'COORD_OLD','subelm',coord_old,ndime,npoin,  'DO_NOT_INITIALIZE')  

     do ielem = 1,nelem
        ltype_old(ielem) = ltype(ielem)
        lelch_old(ielem) = lelch(ielem)
        lnnod_old(ielem) = lnnod(ielem)
        lesub_old(ielem) = lesub(ielem)
        do inode = 1,mnode
           lnods_old(inode,ielem) = lnods(inode,ielem)
        end do
     end do
     do iboun = 1,nboun
        do inodb = 1,mnodb
           lnodb_old(inodb,iboun) = lnodb(inodb,iboun) 
           lboel_old(inodb,iboun) = lboel(inodb,iboun) 
        end do
        lboel_old(mnodb+1,iboun) = lboel(mnodb+1,iboun) 
        ltypb_old(iboun)         = ltypb(iboun) 
        lboch_old(iboun)         = lboch(iboun) 
     end do
     do ipoin = 1,npoin
        do idime = 1,ndime
           coord_old(idime,ipoin) = coord(idime,ipoin)
        end do
     end do
     !
     ! Allocate memory fo new mesh
     !
     call memgeo(-1_ip)
     if( ndime == 2 ) then
        nelem = ( lnuty(TRI03) + lnuty(QUA04) ) * 4
        nboun = nboun_old * 2 
        npoin = npoin_old + nedgg + lnuty(QUA04)
     else
        nelem = ( lnuty(TET04) + lnuty(PEN06) + lnuty(HEX08) ) * 8 + lnuty(PYR05) * 10
        nboun = nboun_old * 4
        npoin = npoin_old + nedgg + nfacg + lnuty(HEX08)
     end if

     !--------------------------------------------------------------------
     !
     ! Permutation arrays
     !
     !--------------------------------------------------------------------

     call memory_alloca(memor_dom,'LNLEV_OLD','subelm',lnlev_old,npoin,'DO_NOT_INITIALIZE')         
     call memory_alloca(memor_dom,'LELEV_OLD','subelm',lelev_old,nelem,'DO_NOT_INITIALIZE')         
     call memory_alloca(memor_dom,'LBLEV_OLD','subelm',lblev_old,nboun,'DO_NOT_INITIALIZE')         

     do ipoin = 1,npoin_old
        lnlev_old(ipoin) = lnlev(ipoin)
     end do
     call submem(-1_ip)
     call submem( 1_ip)
     do ipoin = 1,npoin_old
        lnlev(ipoin) = lnlev_old(ipoin)
     end do

     if( ISEQUEN ) then
        !
        ! OJO: should be taken off when renumbering sequential run
        !
        do ipoin = npoin_old+1,npoin
           lnlev(ipoin) = 0
        end do
     end if

     call memory_deallo(memor_dom,'LBLEV_OLD','subelm',lblev_old)         
     call memory_deallo(memor_dom,'LELEV_OLD','subelm',lelev_old)         
     call memory_deallo(memor_dom,'LNLEV_OLD','subelm',lnlev_old)         

     !--------------------------------------------------------------------
     !
     ! New mesh
     !
     !--------------------------------------------------------------------

     call memgeo( 1_ip)
     !
     ! NPOIN arrays
     !
     ! Old nodes
     do ipoin = 1,npoin_old
        do idime = 1,ndime
           coord(idime,ipoin) = coord_old(idime,ipoin)
        end do
     end do
     ! Edge nodes
     ipoin = npoin_old
     do iedgg = 1,nedgg
        ipoin = ipoin + 1
        node1 = ledgg(1,iedgg)
        node2 = ledgg(2,iedgg)
        ledgg(3,iedgg) = ipoin
        do idime = 1,ndime
           coord(idime,ipoin) = 0.5_rp * ( coord(idime,node1) + coord(idime,node2) )
        end do
     end do
     ! Face nodes
     npoif = ipoin
     do ifacg = 1,nfacg
        ipoin = ipoin + 1
        node1 = lfacg(1,ifacg)
        node2 = lfacg(2,ifacg)
        node3 = lfacg(3,ifacg)
        node4 = lfacg(4,ifacg)
        lfacg(5,ifacg) = ipoin
        do idime = 1,ndime
           coord(idime,ipoin) = 0.25_rp * ( coord(idime,node1) + coord(idime,node2) &
                &                         + coord(idime,node3) + coord(idime,node4) )
        end do
     end do
     ! Center nodes 
     npoic = ipoin
     do ielem = 1,nelem_old

        if( abs(ltype_old(ielem)) == QUA04 ) then

           ipoin = ipoin + 1
           coord(1,ipoin) = 0.0_rp
           coord(2,ipoin) = 0.0_rp
           do inode = 1,4
              jpoin = lnods_old(inode,ielem)
              coord(1,ipoin) = coord(1,ipoin) + coord_old(1,jpoin)
              coord(2,ipoin) = coord(2,ipoin) + coord_old(2,jpoin)
           end do
           coord(1,ipoin) = 0.25_rp * coord(1,ipoin) 
           coord(2,ipoin) = 0.25_rp * coord(2,ipoin)

        else if(  abs(ltype_old(ielem)) == HEX08 ) then

           ipoin = ipoin + 1
           coord(1,ipoin) = 0.0_rp
           coord(2,ipoin) = 0.0_rp
           coord(3,ipoin) = 0.0_rp
           do inode = 1,8
              jpoin = lnods_old(inode,ielem)
              coord(1,ipoin) = coord(1,ipoin) + coord_old(1,jpoin)
              coord(2,ipoin) = coord(2,ipoin) + coord_old(2,jpoin)
              coord(3,ipoin) = coord(3,ipoin) + coord_old(3,jpoin)
           end do
           coord(1,ipoin) = 0.125_rp * coord(1,ipoin) 
           coord(2,ipoin) = 0.125_rp * coord(2,ipoin)
           coord(3,ipoin) = 0.125_rp * coord(3,ipoin)

        end if
     end do
     !
     ! NELEM arrays
     !  
     kelem = 0
     ipoic = npoic
     ipoif = npoif

     if( ndime == 2 ) then
        do ielem = 1,nelem_old

           if( abs(ltype_old(ielem)) == TRI03 ) then
              !
              !        3
              !        o
              !  (3)  /4\  (2)
              !      *---*
              !    / 1\2/3 \
              !   o----*----o
              !   1   (1)   2
              !
              call edgnod(lnods_old(1,ielem),lnods_old(2,ielem),lnodx(1)) ! Edge 1-2
              call edgnod(lnods_old(2,ielem),lnods_old(3,ielem),lnodx(2)) ! Edge 2-3
              call edgnod(lnods_old(1,ielem),lnods_old(3,ielem),lnodx(3)) ! Edge 1-3

              kelem          = kelem + 1
              lnods(1,kelem) = lnods_old(1,ielem)
              lnods(2,kelem) = lnodx(1)
              lnods(3,kelem) = lnodx(3)
              ltype(kelem)   = TRI03
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(1)
              lnods(2,kelem) = lnodx(2)
              lnods(3,kelem) = lnodx(3)
              ltype(kelem)   = TRI03
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(1)
              lnods(2,kelem) = lnods_old(2,ielem)
              lnods(3,kelem) = lnodx(2)
              ltype(kelem)   = TRI03
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(3)
              lnods(2,kelem) = lnodx(2)
              lnods(3,kelem) = lnods_old(3,ielem)
              ltype(kelem)   = TRI03
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

              if( lelch_old(ielem) == ELEXT ) then
                 lelch(kelem)   = ELEXT
                 lelch(kelem-1) = ELFEM
                 lelch(kelem-2) = ELFEM
                 lelch(kelem-2) = ELFEM
              end if

           else if( abs(ltype_old(ielem)) == QUA04 ) then

              call edgnod(lnods_old(1,ielem),lnods_old(2,ielem),lnodx(1)) ! Edge 1-2
              call edgnod(lnods_old(2,ielem),lnods_old(3,ielem),lnodx(2)) ! Edge 2-3
              call edgnod(lnods_old(3,ielem),lnods_old(4,ielem),lnodx(3)) ! Edge 3-4
              call edgnod(lnods_old(4,ielem),lnods_old(1,ielem),lnodx(4)) ! Edge 4-1

              ipoic          = ipoic + 1

              kelem          = kelem + 1
              lnods(1,kelem) = lnods_old(1,ielem)
              lnods(2,kelem) = lnodx(1)
              lnods(3,kelem) = ipoic
              lnods(4,kelem) = lnodx(4)
              ltype(kelem)   = QUA04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(1) 
              lnods(2,kelem) = lnods_old(2,ielem)
              lnods(3,kelem) = lnodx(2)
              lnods(4,kelem) = ipoic
              ltype(kelem)   = QUA04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

              kelem          = kelem + 1
              lnods(1,kelem) = ipoic
              lnods(2,kelem) = lnodx(2)
              lnods(3,kelem) = lnods_old(3,ielem)
              lnods(4,kelem) = lnodx(3)
              ltype(kelem)   = QUA04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(4)
              lnods(2,kelem) = ipoic
              lnods(3,kelem) = lnodx(3)
              lnods(4,kelem) = lnods_old(4,ielem)
              ltype(kelem)   = QUA04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

              if( lelch_old(ielem) == ELEXT ) then
                 lelch(kelem)   = ELHOL
                 lelch(kelem-1) = ELHOL
                 lelch(kelem-2) = ELHOL
              end if

           else

              call runend('SUBELM: ELEMENT NOT CODED')

           end if

        end do

     else

        do ielem = 1,nelem_old

           if( abs(ltype_old(ielem)) == TET04 ) then

              call edgnod(lnods_old(1,ielem),lnods_old(2,ielem),lnodx(1)) ! Edge 1-2
              call edgnod(lnods_old(2,ielem),lnods_old(3,ielem),lnodx(2)) ! Edge 2-3
              call edgnod(lnods_old(1,ielem),lnods_old(3,ielem),lnodx(3)) ! Edge 1-3
              call edgnod(lnods_old(1,ielem),lnods_old(4,ielem),lnodx(4)) ! Edge 1-4
              call edgnod(lnods_old(2,ielem),lnods_old(4,ielem),lnodx(5)) ! Edge 2-4
              call edgnod(lnods_old(3,ielem),lnods_old(4,ielem),lnodx(6)) ! Edge 3-4
              ! 1
              kelem          = kelem + 1
              lnods(1,kelem) = lnods_old(1,ielem)
              lnods(2,kelem) = lnodx(1)
              lnods(3,kelem) = lnodx(3)
              lnods(4,kelem) = lnodx(4)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 2
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(1)
              lnods(2,kelem) = lnods_old(2,ielem)
              lnods(3,kelem) = lnodx(2)
              lnods(4,kelem) = lnodx(5)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 3
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(2)
              lnods(2,kelem) = lnods_old(3,ielem)
              lnods(3,kelem) = lnodx(3)
              lnods(4,kelem) = lnodx(6)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 4
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(1)
              lnods(2,kelem) = lnodx(5)
              lnods(3,kelem) = lnodx(2)
              lnods(4,kelem) = lnodx(4)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 5
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(5)
              lnods(2,kelem) = lnodx(6)
              lnods(3,kelem) = lnodx(2)
              lnods(4,kelem) = lnodx(4)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 6
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(3)
              lnods(2,kelem) = lnodx(2)
              lnods(3,kelem) = lnodx(6)
              lnods(4,kelem) = lnodx(4)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 7
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(3)
              lnods(2,kelem) = lnodx(1)
              lnods(3,kelem) = lnodx(2)
              lnods(4,kelem) = lnodx(4)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 8
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(6)
              lnods(2,kelem) = lnodx(4)
              lnods(3,kelem) = lnodx(5)
              lnods(4,kelem) = lnods_old(4,ielem)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

           else if( abs(ltype_old(ielem)) == HEX08 ) then

              call edgnod(lnods_old(1,ielem),lnods_old(2,ielem),lnodx( 1)) ! Edge 1-2
              call edgnod(lnods_old(2,ielem),lnods_old(3,ielem),lnodx( 2)) ! Edge 2-3
              call edgnod(lnods_old(3,ielem),lnods_old(4,ielem),lnodx( 3)) ! Edge 3-4
              call edgnod(lnods_old(4,ielem),lnods_old(1,ielem),lnodx( 4)) ! Edge 4-1
              call edgnod(lnods_old(5,ielem),lnods_old(6,ielem),lnodx( 5)) ! Edge 5-6
              call edgnod(lnods_old(6,ielem),lnods_old(7,ielem),lnodx( 6)) ! Edge 6-7
              call edgnod(lnods_old(7,ielem),lnods_old(8,ielem),lnodx( 7)) ! Edge 7-8
              call edgnod(lnods_old(8,ielem),lnods_old(5,ielem),lnodx( 8)) ! Edge 8-5
              call edgnod(lnods_old(1,ielem),lnods_old(5,ielem),lnodx( 9)) ! Edge 1-5
              call edgnod(lnods_old(2,ielem),lnods_old(6,ielem),lnodx(10)) ! Edge 2-6
              call edgnod(lnods_old(3,ielem),lnods_old(7,ielem),lnodx(11)) ! Edge 3-7
              call edgnod(lnods_old(4,ielem),lnods_old(8,ielem),lnodx(12)) ! Edge 4-8

              ipoif = 12
              do iface = 1,6
                 node1 = lface(HEX08) % l(1,iface) 
                 ipoi1 = lnods_old(node1,ielem)
                 node2 = lface(HEX08) % l(2,iface) 
                 ipoi2 = lnods_old(node2,ielem)
                 node3 = lface(HEX08) % l(3,iface) 
                 ipoi3 = lnods_old(node3,ielem)
                 node4 = lface(HEX08) % l(4,iface) 
                 ipoi4 = lnods_old(node4,ielem)
                 ipoif = ipoif + 1
                 ifacg = facel(1,iface,ielem)
                 lnodx(ipoif) = lfacg(5,ifacg)
                 !call facnod(ipoi1,ipoi2,ipoi3,ipoi4,lnodx(ipoif))         ! Face IFACE
              end do
              ipoic          = ipoic + 1
              lnodx(19)      = ipoic
              ! 1
              kelem          = kelem + 1
              lnods(1,kelem) = lnods_old(1,ielem)
              lnods(2,kelem) = lnodx( 1)
              lnods(3,kelem) = lnodx(13)
              lnods(4,kelem) = lnodx( 4)
              lnods(5,kelem) = lnodx( 9)
              lnods(6,kelem) = lnodx(17)
              lnods(7,kelem) = lnodx(19)
              lnods(8,kelem) = lnodx(16)
              ltype(kelem)   = HEX08
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 2
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx( 1)
              lnods(2,kelem) = lnods_old(2,ielem)
              lnods(3,kelem) = lnodx( 2)
              lnods(4,kelem) = lnodx(13)
              lnods(5,kelem) = lnodx(17)
              lnods(6,kelem) = lnodx(10)
              lnods(7,kelem) = lnodx(14)
              lnods(8,kelem) = lnodx(19)
              ltype(kelem)   = HEX08
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 3
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(13)
              lnods(2,kelem) = lnodx( 2)
              lnods(3,kelem) = lnods_old(3,ielem)
              lnods(4,kelem) = lnodx( 3)
              lnods(5,kelem) = lnodx(19)
              lnods(6,kelem) = lnodx(14)
              lnods(7,kelem) = lnodx(11)
              lnods(8,kelem) = lnodx(18)
              ltype(kelem)   = HEX08
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 4
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx( 4)
              lnods(2,kelem) = lnodx(13)
              lnods(3,kelem) = lnodx( 3)
              lnods(4,kelem) = lnods_old(4,ielem)
              lnods(5,kelem) = lnodx(16)
              lnods(6,kelem) = lnodx(19)
              lnods(7,kelem) = lnodx(18)
              lnods(8,kelem) = lnodx(12)
              ltype(kelem)   = HEX08
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 5
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx( 9)
              lnods(2,kelem) = lnodx(17)
              lnods(3,kelem) = lnodx(19)
              lnods(4,kelem) = lnodx(16)
              lnods(5,kelem) = lnods_old(5,ielem)
              lnods(6,kelem) = lnodx( 5)
              lnods(7,kelem) = lnodx(15)
              lnods(8,kelem) = lnodx( 8)
              ltype(kelem)   = HEX08
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 6
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(17)
              lnods(2,kelem) = lnodx(10)
              lnods(3,kelem) = lnodx(14)
              lnods(4,kelem) = lnodx(19)
              lnods(5,kelem) = lnodx( 5)
              lnods(6,kelem) = lnods_old(6,ielem)
              lnods(7,kelem) = lnodx( 6)
              lnods(8,kelem) = lnodx(15)
              ltype(kelem)   = HEX08
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 7
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(19)
              lnods(2,kelem) = lnodx(14)
              lnods(3,kelem) = lnodx(11)
              lnods(4,kelem) = lnodx(18)
              lnods(5,kelem) = lnodx(15)
              lnods(6,kelem) = lnodx( 6)
              lnods(7,kelem) = lnods_old(7,ielem)
              lnods(8,kelem) = lnodx( 7)
              ltype(kelem)   = HEX08
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 8
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(16)
              lnods(2,kelem) = lnodx(19)
              lnods(3,kelem) = lnodx(18)
              lnods(4,kelem) = lnodx(12)
              lnods(5,kelem) = lnodx( 8)
              lnods(6,kelem) = lnodx(15)
              lnods(7,kelem) = lnodx( 7)
              lnods(8,kelem) = lnods_old(8,ielem)
              ltype(kelem)   = HEX08
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

           else if( abs(ltype_old(ielem)) == PEN06 ) then

              call edgnod(lnods_old(1,ielem),lnods_old(2,ielem),lnodx( 1)) ! Edge 1-2
              call edgnod(lnods_old(2,ielem),lnods_old(3,ielem),lnodx( 2)) ! Edge 2-3
              call edgnod(lnods_old(3,ielem),lnods_old(1,ielem),lnodx( 3)) ! Edge 3-1

              call edgnod(lnods_old(4,ielem),lnods_old(5,ielem),lnodx( 4)) ! Edge 4-5
              call edgnod(lnods_old(5,ielem),lnods_old(6,ielem),lnodx( 5)) ! Edge 5-6
              call edgnod(lnods_old(6,ielem),lnods_old(4,ielem),lnodx( 6)) ! Edge 6-4

              call edgnod(lnods_old(1,ielem),lnods_old(4,ielem),lnodx( 7)) ! Edge 1-4
              call edgnod(lnods_old(2,ielem),lnods_old(5,ielem),lnodx( 8)) ! Edge 2-5
              call edgnod(lnods_old(3,ielem),lnods_old(6,ielem),lnodx( 9)) ! Edge 3-6

              ipoif = 9
              do iface = 1,3
                 node1 = lface(PEN06) % l(1,iface) 
                 ipoi1 = lnods_old(node1,ielem)
                 node2 = lface(PEN06) % l(2,iface) 
                 ipoi2 = lnods_old(node2,ielem)
                 node3 = lface(PEN06) % l(3,iface) 
                 ipoi3 = lnods_old(node3,ielem)
                 node4 = lface(PEN06) % l(4,iface) 
                 ipoi4 = lnods_old(node4,ielem)
                 ipoif = ipoif + 1
                 ifacg = facel(1,iface,ielem)
                 lnodx(ipoif) = lfacg(5,ifacg)
                 !call facnod(ipoi1,ipoi2,ipoi3,ipoi4,lnodx(ipoif))         ! Face IFACE
              end do
              ! 1
              kelem          = kelem + 1
              lnods(1,kelem) = lnods_old(1,ielem)
              lnods(2,kelem) = lnodx( 1)
              lnods(3,kelem) = lnodx( 3)
              lnods(4,kelem) = lnodx( 7)
              lnods(5,kelem) = lnodx(10)
              lnods(6,kelem) = lnodx(12)
              ltype(kelem)   = PEN06
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 2
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx( 1)
              lnods(2,kelem) = lnodx( 2)
              lnods(3,kelem) = lnodx( 3)
              lnods(4,kelem) = lnodx(10)
              lnods(5,kelem) = lnodx(11)
              lnods(6,kelem) = lnodx(12)
              ltype(kelem)   = PEN06
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 3
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx( 1)
              lnods(2,kelem) = lnods_old(2,ielem)
              lnods(3,kelem) = lnodx( 2)
              lnods(4,kelem) = lnodx(10)
              lnods(5,kelem) = lnodx( 8)
              lnods(6,kelem) = lnodx(11)
              ltype(kelem)   = PEN06
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 4
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx( 3)
              lnods(2,kelem) = lnodx( 2)
              lnods(3,kelem) = lnods_old(3,ielem)
              lnods(4,kelem) = lnodx(12)
              lnods(5,kelem) = lnodx(11)
              lnods(6,kelem) = lnodx( 9)
              ltype(kelem)   = PEN06
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 5
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx( 7)
              lnods(2,kelem) = lnodx(10)
              lnods(3,kelem) = lnodx(12)
              lnods(4,kelem) = lnods_old(4,ielem)
              lnods(5,kelem) = lnodx( 4)
              lnods(6,kelem) = lnodx( 6)
              ltype(kelem)   = PEN06
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 6
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(10)
              lnods(2,kelem) = lnodx(11)
              lnods(3,kelem) = lnodx(12)
              lnods(4,kelem) = lnodx( 4)
              lnods(5,kelem) = lnodx( 5)
              lnods(6,kelem) = lnodx( 6)
              ltype(kelem)   = PEN06
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 7
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(10)
              lnods(2,kelem) = lnodx( 8)
              lnods(3,kelem) = lnodx(11)
              lnods(4,kelem) = lnodx( 4)
              lnods(5,kelem) = lnods_old(5,ielem)
              lnods(6,kelem) = lnodx( 5)
              ltype(kelem)   = PEN06
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 8
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(12)
              lnods(2,kelem) = lnodx(11)
              lnods(3,kelem) = lnodx( 9)
              lnods(4,kelem) = lnodx( 6)
              lnods(5,kelem) = lnodx( 5)
              lnods(6,kelem) = lnods_old(6,ielem)
              ltype(kelem)   = PEN06
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)

           else if( abs(ltype_old(ielem)) == PYR05 ) then

              call edgnod(lnods_old(1,ielem),lnods_old(2,ielem),lnodx( 1)) ! Edge 1-2
              call edgnod(lnods_old(2,ielem),lnods_old(3,ielem),lnodx( 2)) ! Edge 2-3
              call edgnod(lnods_old(3,ielem),lnods_old(4,ielem),lnodx( 3)) ! Edge 3-4
              call edgnod(lnods_old(4,ielem),lnods_old(1,ielem),lnodx( 4)) ! Edge 4-1

              call edgnod(lnods_old(1,ielem),lnods_old(5,ielem),lnodx( 5)) ! Edge 1-5
              call edgnod(lnods_old(2,ielem),lnods_old(5,ielem),lnodx( 6)) ! Edge 2-5
              call edgnod(lnods_old(3,ielem),lnods_old(5,ielem),lnodx( 7)) ! Edge 3-5
              call edgnod(lnods_old(4,ielem),lnods_old(5,ielem),lnodx( 8)) ! Edge 4-5

              ifacg    = facel(1,1,ielem)
              lnodx(9) = lfacg(5,ifacg)
              ! 1
              kelem          = kelem + 1
              lnods(1,kelem) = lnods_old(1,ielem)
              lnods(2,kelem) = lnodx(1)
              lnods(3,kelem) = lnodx(9)
              lnods(4,kelem) = lnodx(4)
              lnods(5,kelem) = lnodx(5)
              ltype(kelem)   = PYR05
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 2
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(1)
              lnods(2,kelem) = lnods_old(2,ielem)
              lnods(3,kelem) = lnodx(2)
              lnods(4,kelem) = lnodx(9)
              lnods(5,kelem) = lnodx(6)
              ltype(kelem)   = PYR05
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 3
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(9)
              lnods(2,kelem) = lnodx(2)
              lnods(3,kelem) = lnods_old(3,ielem)
              lnods(4,kelem) = lnodx(3)
              lnods(5,kelem) = lnodx(7)
              ltype(kelem)   = PYR05
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 4
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(4)
              lnods(2,kelem) = lnodx(9)
              lnods(3,kelem) = lnodx(3)
              lnods(4,kelem) = lnods_old(4,ielem)
              lnods(5,kelem) = lnodx(8)
              ltype(kelem)   = PYR05
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 5
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(8)
              lnods(2,kelem) = lnodx(7)
              lnods(3,kelem) = lnodx(6)
              lnods(4,kelem) = lnodx(5)
              lnods(5,kelem) = lnodx(9)
              ltype(kelem)   = PYR05
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 6
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(5)
              lnods(2,kelem) = lnodx(6)
              lnods(3,kelem) = lnodx(7)
              lnods(4,kelem) = lnodx(8)
              lnods(5,kelem) = lnods_old(5,ielem)
              ltype(kelem)   = PYR05
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = lnnod_old(ielem)
              lesub(kelem)   = lesub_old(ielem)
              ! 7
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(5)
              lnods(2,kelem) = lnodx(6)
              lnods(3,kelem) = lnodx(1)
              lnods(4,kelem) = lnodx(9)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = 4
              lesub(kelem)   = lesub_old(ielem)
              ! 8
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(6)
              lnods(2,kelem) = lnodx(7)
              lnods(3,kelem) = lnodx(2)
              lnods(4,kelem) = lnodx(9)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = 4
              lesub(kelem)   = lesub_old(ielem)
              ! 9
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(7)
              lnods(2,kelem) = lnodx(8)
              lnods(3,kelem) = lnodx(3)
              lnods(4,kelem) = lnodx(9)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = 4
              lesub(kelem)   = lesub_old(ielem)
              ! 10
              kelem          = kelem + 1
              lnods(1,kelem) = lnodx(8)
              lnods(2,kelem) = lnodx(5)
              lnods(3,kelem) = lnodx(4)
              lnods(4,kelem) = lnodx(9)
              ltype(kelem)   = TET04
              lelch(kelem)   = lelch_old(ielem)
              lnnod(kelem)   = 4
              lesub(kelem)   = lesub_old(ielem)

           else

              call runend('NOT CODED')

           end if
        end do
     end if
     !
     ! Deallocate memory of FACEL
     !
     if( nfacg > 0 ) then
        call memory_deallo(memor_dom,'FACEL','subelm',facel)
     end if
     ! 
     ! NBOUN arrays
     !  
     kboun     = 0
     kfl_bouel = 0
     if( ndime == 2 ) then
        do iboun = 1,nboun_old
           call edgnod(lnodb_old(1,iboun),lnodb_old(2,iboun),lnodx(1)) ! Edge 1-2
           kboun          = kboun + 1
           lnodb(1,kboun) = lnodb_old(1,iboun)
           lnodb(2,kboun) = lnodx(1)
           ltypb(kboun)   = ltypb_old(iboun)
           lboch(kboun)   = lboch_old(iboun)
           kboun          = kboun + 1
           lnodb(1,kboun) = lnodx(1)
           lnodb(2,kboun) = lnodb_old(2,iboun)
           ltypb(kboun)   = ltypb_old(iboun)
           lboch(kboun)   = lboch_old(iboun)
        end do
     else
        do iboun = 1,nboun_old

           if( ltypb_old(iboun) == TRI03 ) then

              call edgnod(lnodb_old(1,iboun),lnodb_old(2,iboun),lnodx(1)) ! Edge 1-2
              call edgnod(lnodb_old(2,iboun),lnodb_old(3,iboun),lnodx(2)) ! Edge 2-3
              call edgnod(lnodb_old(1,iboun),lnodb_old(3,iboun),lnodx(3)) ! Edge 1-3

              kboun          = kboun + 1
              lnodb(1,kboun) = lnodb_old(1,iboun)
              lnodb(2,kboun) = lnodx(1)
              lnodb(3,kboun) = lnodx(3)
              ltypb(kboun)   = ltypb_old(iboun)
              lboch(kboun)   = lboch_old(iboun)

              kboun          = kboun + 1
              lnodb(1,kboun) = lnodx(1)
              lnodb(2,kboun) = lnodx(2)
              lnodb(3,kboun) = lnodx(3)
              ltypb(kboun)   = ltypb_old(iboun)
              lboch(kboun)   = lboch_old(iboun)

              kboun          = kboun + 1
              lnodb(1,kboun) = lnodx(1)
              lnodb(2,kboun) = lnodb_old(2,iboun)
              lnodb(3,kboun) = lnodx(2)
              ltypb(kboun)   = ltypb_old(iboun)
              lboch(kboun)   = lboch_old(iboun)

              kboun          = kboun + 1
              lnodb(1,kboun) = lnodx(2)
              lnodb(2,kboun) = lnodb_old(3,iboun)
              lnodb(3,kboun) = lnodx(3)
              ltypb(kboun)   = ltypb_old(iboun)
              lboch(kboun)   = lboch_old(iboun)

           else if( ltypb_old(iboun) == QUA04 ) then

              ipoi1 = lnodb_old(1,iboun)
              ipoi2 = lnodb_old(2,iboun)
              ipoi3 = lnodb_old(3,iboun)
              ipoi4 = lnodb_old(4,iboun)
              call edgnod(ipoi1,ipoi2,lnodx(1)) ! Edge 1-2
              call edgnod(ipoi2,ipoi3,lnodx(2)) ! Edge 2-3
              call edgnod(ipoi3,ipoi4,lnodx(3)) ! Edge 3-4
              call edgnod(ipoi4,ipoi1,lnodx(4)) ! Edge 4-1
              call facnod(ipoi1,ipoi2,ipoi3,ipoi4,lnodx(5)) 

              kboun          = kboun + 1
              lnodb(1,kboun) = lnodb_old(1,iboun)
              lnodb(2,kboun) = lnodx(1)
              lnodb(3,kboun) = lnodx(5)
              lnodb(4,kboun) = lnodx(4)
              ltypb(kboun)   = ltypb_old(iboun)
              lboch(kboun)   = lboch_old(iboun)

              kboun          = kboun + 1
              lnodb(1,kboun) = lnodx(1)
              lnodb(2,kboun) = lnodb_old(2,iboun)
              lnodb(3,kboun) = lnodx(2)
              lnodb(4,kboun) = lnodx(5)
              ltypb(kboun)   = ltypb_old(iboun)
              lboch(kboun)   = lboch_old(iboun)

              kboun          = kboun + 1
              lnodb(1,kboun) = lnodx(5)
              lnodb(2,kboun) = lnodx(2)
              lnodb(3,kboun) = lnodb_old(3,iboun)
              lnodb(4,kboun) = lnodx(3)
              ltypb(kboun)   = ltypb_old(iboun)
              lboch(kboun)   = lboch_old(iboun)

              kboun          = kboun + 1
              lnodb(1,kboun) = lnodx(4)
              lnodb(2,kboun) = lnodx(5)
              lnodb(3,kboun) = lnodx(3)
              lnodb(4,kboun) = lnodb_old(4,iboun)
              ltypb(kboun)   = ltypb_old(iboun)
              lboch(kboun)   = lboch_old(iboun)

           end if

        end do
     end if
     !
     ! Deallocate edge memory
     !
     call memory_deallo(memor_dom,'PEDGP','subelm',pedgp)

     !--------------------------------------------------------------------
     !
     ! Groups
     !
     !--------------------------------------------------------------------

     if( ngrou_dom > 0 ) then

        call memory_copy(memor_dom,'LGROU_DOM_OLD','subelm',lgrou_dom,lgrou_dom_old,'DO_NOT_DEALLOCATE')
        call memgeo(-27_ip)
        call memgeo( 27_ip)
        ! Old nodes
        do ipoin = 1,npoin_old
           lgrou_dom(ipoin) = lgrou_dom_old(ipoin)
        end do
        ! Edge nodes
        ipoin = npoin_old
        do iedgg = 1,nedgg
           ipoin = ipoin + 1
           node1 = ledgg(1,iedgg)
           node2 = ledgg(2,iedgg)
           lgrou_dom(ipoin) = min(lgrou_dom_old(node1),lgrou_dom_old(node2))
        end do
        ! Face nodes
        do ifacg = 1,nfacg
           ipoin = ipoin + 1
           node1 = lfacg(1,ifacg)
           node2 = lfacg(2,ifacg)
           node3 = lfacg(3,ifacg)
           node4 = lfacg(4,ifacg)
           lgrou_dom(ipoin) = min(lgrou_dom_old(node1),lgrou_dom_old(node2),&
                &                 lgrou_dom_old(node3),lgrou_dom_old(node4))
        end do
        ! Central nodes
        if( lnuty(QUA04) > 0 .or. lnuty(HEX08) > 0 ) then
           call memgen(1_ip,ngrou_dom,0_ip)
           do ielem = 1,nelem_old
              pelty = abs(ltype_old(ielem))
              if( pelty == QUA04 .or. pelty == HEX08 ) then
                 pnode = nnode(pelty)
                 ipoin = ipoin + 1
                 do inode = 1,pnode
                    igrou = lgrou_dom_old(lnods_old(inode,ielem))
                    gisca(igrou) = gisca(igrou) + 1 
                 end do
                 jgrou = 1
                 do igrou = 2,ngrou_dom
                    if( gisca(igrou) > gisca(jgrou) ) jgrou = igrou
                 end do
                 lgrou_dom(ipoin) = jgrou
                 do inode = 1,pnode
                    igrou = lgrou_dom_old(lnods_old(inode,ielem))
                    gisca(igrou) = 0
                 end do
              end if
           end do
           call memgen(3_ip,ngrou_dom,0_ip)
        end if

        call memory_deallo(memor_dom,'LGROU_DOM_OLD','subelm',lgrou_dom_old)

     end if

     !--------------------------------------------------------------------
     !
     ! Zones: LELEZ
     !
     !--------------------------------------------------------------------

     call memory_alloca(memor_dom,'LELEZ_OLD','subelm',lelez_old,nzone,nelem_old,'DO_NOT_INITIALIZE')

     do izone = 1,nzone
        nelez_old = nelez(izone)        
        do kelem = 1,nelez_old
           ielem = lelez(izone) % l(kelem)
           lelez_old(izone,ielem) = 1
        end do
        nelez(izone) = 0
        do kelem = 1,nelez_old
           ielem = lelez(izone) % l(kelem)
           nelez(izone) = nelez(izone) + howdi(abs(ltype_old(ielem)))
        end do
        igene = izone
        call memgeo(-51_ip)
        call memgeo( 51_ip)
     end do
     do izone = 1,nzone
        kelem = 0
        kk    = 0
        do ielem = 1,nelem_old
           if( lelez_old(izone,ielem) == 1 ) then
              do ii = 1,howdi(abs(ltype_old(ielem)))
                 kelem = kelem + 1
                 kk    = kk + 1
                 lelez(izone) % l(kk) = kelem
              end do
           else
              kelem = kelem + howdi(abs(ltype_old(ielem)))
           end if
        end do
     end do

     call memory_deallo(memor_dom,'LELEZ_OLD','subelm',lelez_old)

     !--------------------------------------------------------------------
     !
     ! Materials: LMATE
     !
     !--------------------------------------------------------------------

     if( nmate >= 0 ) then
        call memory_copy(memor_dom,'LMATE_OLD','subelm',lmate,lmate_old,'DO_NOT_DEALLOCATE')
        call memgeo(-34_ip)
        call memgeo( 34_ip)
        kelem = 0
        if( ndime == 2 ) then
           do ielem = 1,nelem_old
              do ii = 1,howdi(abs(ltype_old(ielem)))
                 kelem = kelem + 1
                 lmate(kelem) = lmate_old(ielem)
              end do
           end do
        else
           do ielem = 1,nelem_old
              do ii = 1,howdi(abs(ltype_old(ielem)))
                 kelem = kelem + 1
                 lmate(kelem) = lmate_old(ielem)
              end do
           end do
        end if
        call memory_deallo(memor_dom,'LMATE_OLD','subelm',lmate_old)
     end if

     !--------------------------------------------------------------------
     !
     ! Sets: LBSET, LNSET 
     !
     !--------------------------------------------------------------------

     if( neset > 0 ) then
        call memory_copy(memor_dom,'LESET_OLD','subelm',leset,leset_old,'DO_NOT_DEALLOCATE')
     end if
     if( nbset > 0 ) then
        call memory_copy(memor_dom,'LBSET_OLD','subelm',lbset,lbset_old,'DO_NOT_DEALLOCATE')
     end if
     call memose(10_ip)
     call memose( 1_ip)
     call memose( 2_ip)
     kelem = 0
     kboun = 0
     if( ndime == 2 ) then
        if( neset > 0 ) then
           do ielem = 1,nelem_old
              do ii = 1,4
                 kelem = kelem + 1
                 leset(kelem) = leset_old(ielem)
              end do
           end do
        end if
        if( nbset > 0 ) then
           do iboun = 1,nboun_old
              do ii = 1,2
                 kboun = kboun + 1
                 lbset(kboun) = lbset_old(iboun)
              end do
           end do
        end if
     else
        if( neset > 0 ) then
           do ielem = 1,nelem_old
              do ii = 1,howdi(abs(ltype_old(ielem)))
                 kelem = kelem + 1
                 leset(kelem) = leset_old(ielem)
              end do
           end do
        end if
        if( nbset > 0 ) then
           do iboun = 1,nboun_old
              do ii = 1,4
                 kboun = kboun + 1
                 lbset(kboun) = lbset_old(iboun)
              end do
           end do
        end if
     end if

     if( neset > 0 ) call memory_deallo(memor_dom,'LESET_OLD','subelm',leset_old)
     if( nbset > 0 ) call memory_deallo(memor_dom,'LBSET_OLD','subelm',lbset_old)

     !--------------------------------------------------------------------
     !
     ! Fields: XFIEL (node/element/boundary)
     !
     !--------------------------------------------------------------------

     if( nfiel > 0 ) then  
        call memory_alloca(memor_dom,'XFIEL_OLD','subelm',xfiel_old,nfiel)
        do ifiel = 1,nfiel
           if( kfl_field(1,ifiel) > 0 ) then
              if( kfl_field(2,ifiel) == NPOIN_TYPE ) then
                 call memory_alloca(memor_dom,'XFIEL_OLD%A','subelm',xfiel_old(ifiel) % a,kfl_field(1,ifiel)*kfl_field(4,ifiel),npoin_old,'DO_NOT_INITIALIZE')
                 do ipoin = 1,npoin_old
                    do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                       xfiel_old(ifiel) % a(idime,ipoin) = xfiel(ifiel) % a(idime,ipoin) 
                    end do
                 end do
                 igene = ifiel
                 call memgeo(-29_ip)
                 call memgeo( 29_ip)
              else if( kfl_field(2,ifiel) == NBOUN_TYPE ) then
                 call memory_alloca(memor_dom,'XFIEL_OLD%A','subelm',xfiel_old(ifiel) % a,kfl_field(1,ifiel)*kfl_field(4,ifiel),nboun_old,'DO_NOT_INITIALIZE')
                 do iboun = 1,nboun_old
                    do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                       xfiel_old(ifiel) % a(idime,iboun) = xfiel(ifiel) % a(idime,iboun) 
                    end do
                 end do
                 igene = ifiel
                 call memgeo(-29_ip)
                 call memgeo( 29_ip)
              else if( kfl_field(2,ifiel) == NELEM_TYPE ) then
                 call memory_alloca(memor_dom,'XFIEL_OLD%A','subelm',xfiel_old(ifiel) % a,kfl_field(1,ifiel)*kfl_field(4,ifiel),nelem_old,'DO_NOT_INITIALIZE')
                 do ielem = 1,nelem_old
                    do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                       xfiel_old(ifiel) % a(idime,ielem) = xfiel(ifiel) % a(idime,ielem) 
                    end do
                 end do
                 igene = ifiel
                 call memgeo(-29_ip)
                 call memgeo( 29_ip)
              end if
           end if
        end do
        do ifiel = 1,nfiel
           if( kfl_field(1,ifiel) > 0 ) then
              if( kfl_field(2,ifiel) == NPOIN_TYPE ) then
                 ! Old nodes
                 do ipoin = 1,npoin_old
                    do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                       xfiel(ifiel) % a(idime,ipoin) = xfiel_old(ifiel) % a(idime,ipoin) 
                    end do
                 end do
                 ! Edge nodes
                 ipoin = npoin_old
                 if( kfl_field(1,ifiel) == 0 ) then
                    do iedgg = 1,nedgg
                       ipoin = ipoin + 1
                       node1 = ledgg(1,iedgg)
                       node2 = ledgg(2,iedgg)
                       do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                          xfiel(ifiel) % a(idime,ipoin) = 0.5_rp&
                               *(xfiel_old(ifiel) % a(idime,node1) + xfiel_old(ifiel) % a(idime,node2) )
                       end do
                    end do
                 else
                    do iedgg = 1,nedgg
                       ipoin = ipoin + 1
                       node1 = ledgg(1,iedgg)
                       node2 = ledgg(2,iedgg)                       
                       do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                          knode = 0
                          do inode = 1,2
                             xx(inode) = xfiel_old(ifiel) % a(idime,ledgg(inode,iedgg))
                             if( xx(inode) > 0.0_rp ) then
                                knode = knode + 1
                             end if
                          end do
                          if( knode == 2 ) &
                               xfiel(ifiel) % a(idime,ipoin) = 0.5_rp * ( xx(1) + xx(2) )
                       end do
                    end do                    
                 end if
                 ! Face nodes
                 if( kfl_field(1,ifiel) == 0 ) then
                    do ifacg = 1,nfacg
                       ipoin = ipoin + 1
                       node1 = lfacg(1,ifacg)
                       node2 = lfacg(2,ifacg)
                       node3 = lfacg(3,ifacg)
                       node4 = lfacg(4,ifacg)
                       do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                          xfiel(ifiel) % a(idime,ipoin) = 0.25_rp&
                               &  * (   xfiel_old(ifiel) % a(idime,node1) + xfiel_old(ifiel) % a(idime,node2) &
                               &      + xfiel_old(ifiel) % a(idime,node3) + xfiel_old(ifiel) % a(idime,node4) )
                       end do
                    end do
                 else                    
                    do ifacg = 1,nfacg
                       ipoin = ipoin + 1
                       do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                          knode = 0
                          do inode = 1,4
                             xx(inode) = xfiel_old(ifiel) % a(idime,lfacg(inode,ifacg))
                             if( xx(inode) > 0.0_rp ) then
                                knode = knode + 1
                             end if
                          end do
                          if( knode == 4 ) &
                               xfiel(ifiel) % a(idime,ipoin) = 0.25_rp * ( xx(1) + xx(2) + xx(3) + xx(4) )
                       end do
                    end do
                 end if
                 ! Central nodes
                 if( lnuty(QUA04) > 0 .or. lnuty(HEX08) > 0 ) then
                    do ielem = 1,nelem_old
                       pelty = abs(ltype_old(ielem))
                       if( pelty == QUA04 .or. pelty == HEX08 ) then
                          ipoin = ipoin + 1
                          pnode = nnode(pelty)
                          do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                             xfiel(ifiel) % a(idime,ipoin) = 0.0_rp
                          end do
                          if( kfl_field(1,ifiel) == 0 ) then
                             do inode = 1,pnode
                                jpoin = lnods_old(inode,ielem)
                                do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                                   xfiel(ifiel) % a(idime,ipoin) = xfiel(ifiel) % a(idime,ipoin) &
                                        & + xfiel(ifiel) % a(idime,jpoin) 
                                end do
                             end do
                             dummr = 1.0_rp / real(pnode)
                             do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                                xfiel(ifiel) % a(idime,ipoin) = dummr * xfiel(ifiel) % a(idime,ipoin)
                             end do
                          else
                             dummr = 1.0_rp / real(pnode)
                             do inode = 1,pnode
                                jpoin = lnods_old(inode,ielem)
                                do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                                   xfiel(ifiel) % a(idime,ipoin) = xfiel(ifiel) % a(idime,ipoin) &
                                        & + xfiel(ifiel) % a(idime,jpoin) 
                                end do
                             end do
                             do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)

                                knode = 0
                                do inode = 1,pnode
                                   xx(inode) = xfiel(ifiel) % a(idime,jpoin) 
                                   if( xx(inode) > 0.0_rp ) then
                                      knode = knode + 1
                                   end if
                                end do
                                if( knode == pnode ) then 
                                   xfiel(ifiel) % a(idime,ipoin) = dummr * xfiel(ifiel) % a(idime,ipoin)
                                else
                                   xfiel(ifiel) % a(idime,ipoin) = 0.0_rp
                                end if
                             end do

                          end if
                       end if
                    end do
                 end if
              else if( kfl_field(2,ifiel) == NBOUN_TYPE ) then
                 kboun = 0
                 do iboun = 1,nboun_old
                    do ii = 1,4
                       kboun = kboun + 1
                       do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                          xfiel(ifiel) % a(idime,kboun) = xfiel_old(ifiel) % a(idime,iboun)
                       end do
                    end do
                 end do
              else if( kfl_field(2,ifiel) == NELEM_TYPE ) then  
                 kelem = 0
                 do ielem = 1,nelem_old
                    do ii = 1,howdi(abs(ltype_old(ielem)))
                       kelem = kelem + 1
                       do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                          xfiel(ifiel) % a(idime,kelem) = xfiel_old(ifiel) % a(idime,ielem)
                       end do
                    end do
                 end do
              else
                 call runend('SUBMSH: DIVISOR WITH ELEMENT FIELD NOT CODED')
              end if
              call memory_deallo(memor_dom,'XFIEL_OLD%A','subelm',xfiel_old(ifiel) % a)
           end if
        end do
        call memory_deallo(memor_dom,'XFIEL_OLD','subelm',xfiel_old)
     end if

     !--------------------------------------------------------------------
     !
     ! Boundary conditions
     !
     !--------------------------------------------------------------------

     !
     ! KFL_CODNO
     !
     if( kfl_icodn > 0 ) then
        allocate( kfl_codno_old(mcono,npoin_old), stat = istat )        
        do ipoin = 1,npoin_old
           do ii = 1,mcono
              kfl_codno_old(ii,ipoin) = kfl_codno(ii,ipoin)
           end do
        end do
        call membcs(-1_ip) ! Deallocate KFL_CODNO
        call membcs( 1_ip) ! Allocate KFL_CODNO
        ! Old nodes
        do ipoin = 1,npoin_old
           do ii = 1,mcono
              kfl_codno(ii,ipoin) = kfl_codno_old(ii,ipoin)
           end do
        end do
        mcod1 = mcodb + 1
        do ipoin = npoin_old+1,npoin
           do ii = 1,mcono
              kfl_codno(ii,ipoin) = mcod1
           end do
        end do
        ! Edge nodes
        ipoin = npoin_old
        do iedgg = 1,nedgg
           ipoin = ipoin + 1
           node1 = ledgg(1,iedgg)
           node2 = ledgg(2,iedgg)
           do ii = 1,1
              code1 = kfl_codno_old(ii,node1)
              code2 = kfl_codno_old(ii,node2)
              if( code1 /= mcod1 .and. code2 /= mcod1 ) then
                 kfl_codno(ii,ipoin) = min(kfl_codno_old(ii,node1),kfl_codno_old(ii,node2))
              end if
           end do
        end do
        ! Face nodes
        do ifacg = 1,nfacg
           ipoin = ipoin + 1
           node1 = lfacg(1,ifacg)
           node2 = lfacg(2,ifacg)
           node3 = lfacg(3,ifacg)
           node4 = lfacg(4,ifacg)
           do ii = 1,1
              code1 = kfl_codno_old(ii,node1)
              code2 = kfl_codno_old(ii,node2)
              code3 = kfl_codno_old(ii,node3)
              code4 = kfl_codno_old(ii,node4)
              if( code1 /= mcod1 .and. code2 /= mcod1 .and. code3 /= mcod1 .and. code4 /= mcod1 ) then
                 kfl_codno(ii,ipoin) = min(kfl_codno_old(ii,node1),kfl_codno_old(ii,node2),&
                      &                    kfl_codno_old(ii,node3),kfl_codno_old(ii,node4))
              end if
           end do
        end do
        ! Central nodes
        if( lnuty(QUA04) > 0 .or. lnuty(HEX08) > 0 ) then
           do ielem = 1,nelem_old
              pelty = abs(ltype_old(ielem))
              if( pelty == QUA04 .or. pelty == HEX08 ) then
                 ipoin = ipoin + 1
                 do ii = 1,1
                    kfl_codno(ii,ipoin) = mcodb + 1
                 end do
              end if
           end do
        end if

        deallocate( kfl_codno_old, stat = istat )

     end if
     !
     ! KFL_CODBO
     !
     if( kfl_icodb > 0 ) then
        allocate( kfl_codbo_old(nboun_old), stat = istat )
        do iboun = 1,nboun_old
           kfl_codbo_old(iboun) = kfl_codbo(iboun)
        end do
        call membcs(-2_ip) ! Deallocate KFL_CODBO
        call membcs( 2_ip) ! Allocate KFL_CODBO
        kboun = 0
        if( ndime == 2 ) then
           do iboun = 1,nboun_old
              do ii = 1,2
                 kboun = kboun + 1
                 kfl_codbo(kboun) = kfl_codbo_old(iboun)
              end do
           end do
        else
           do iboun = 1,nboun_old
              do ii = 1,4
                 kboun = kboun + 1
                 kfl_codbo(kboun) = kfl_codbo_old(iboun)
              end do
           end do
        end if
        deallocate( kfl_codbo_old, stat = istat )
     end if
     !
     ! BVCOD
     !
     if( nvcod > 0 ) then
        call memory_alloca(memor_dom,'BVCOD_OLD','subelm',bvcod_old,mvcod)
        do ivcod = 1,mvcod
           if( lvcod(1,ivcod) > 0 ) then
              call memory_alloca(memor_dom,'BVCOD_OLD%A','subelm',bvcod_old(ivcod)%a,lvcod(2,ivcod),npoin_old,'DO_NOT_INITIALIZE')
              do ipoin = 1,npoin_old
                 do idime = 1,lvcod(2,ivcod)
                    bvcod_old(ivcod) % a(idime,ipoin) = bvcod(ivcod) % a(idime,ipoin)
                 end do
              end do
              nvcod = ivcod
              call membcs(-9_ip)
              call membcs( 9_ip)
           end if
        end do
        do ivcod = 1,mvcod
           if( lvcod(1,ivcod) > 0 ) then
              ! Old nodes
              do ipoin = 1,npoin_old
                 do idime = 1,lvcod(2,ivcod)
                    bvcod(ivcod) % a(idime,ipoin) = bvcod_old(ivcod) % a(idime,ipoin) 
                 end do
              end do
              ! Edge nodes
              ipoin = npoin_old
              do iedgg = 1,nedgg
                 ipoin = ipoin + 1
                 node1 = ledgg(1,iedgg)
                 node2 = ledgg(2,iedgg)
                 do idime = 1,lvcod(2,ivcod)
                    bvcod(ivcod) % a(idime,ipoin) = 0.5_rp&
                         *(bvcod_old(ivcod) % a(idime,node1) + bvcod_old(ivcod) % a(idime,node2) )
                 end do
              end do
              ! Face nodes
              do ifacg = 1,nfacg
                 ipoin = ipoin + 1
                 node1 = lfacg(1,ifacg)
                 node2 = lfacg(2,ifacg)
                 node3 = lfacg(3,ifacg)
                 node4 = lfacg(4,ifacg)
                  do idime = 1,lvcod(2,ivcod)
                    bvcod(ivcod) % a(idime,ipoin) = 0.25_rp&
                         & * (   bvcod_old(ivcod) % a(idime,node1) + bvcod_old(ivcod) % a(idime,node2) &
                         &     + bvcod_old(ivcod) % a(idime,node3) + bvcod_old(ivcod) % a(idime,node4) )
                 end do
              end do
              ! Central nodes
              if( lnuty(QUA04) > 0 .or. lnuty(HEX08) > 0 ) then
                 do ielem = 1,nelem_old
                    pelty = abs(ltype_old(ielem))
                    if( pelty == QUA04 .or. pelty == HEX08 ) then
                       ipoin = ipoin + 1
                       pnode = nnode(pelty)
                       do idime = 1,lvcod(2,ivcod)
                          bvcod(ivcod) % a(idime,ipoin) = 0.0_rp
                       end do
                       do inode = 1,pnode
                          jpoin = lnods_old(inode,ielem)
                          do idime = 1,lvcod(2,ivcod)
                             bvcod(ivcod) % a(idime,ipoin) = bvcod(ivcod) % a(idime,ipoin) &
                                  & + bvcod_old(ivcod) % a(idime,jpoin) 
                          end do
                       end do
                       dummr = 1.0_rp / real(pnode)
                       do idime = 1,lvcod(2,ivcod)
                          bvcod(ivcod) % a(idime,ipoin) = dummr * bvcod(ivcod) % a(idime,ipoin)
                       end do
                    end if
                 end do
              end if

              call memory_deallo(memor_dom,'BVCOD_OLD%A','subelm',bvcod_old(ivcod)%a)

           end if
        end do
        call memory_deallo(memor_dom,'BVCOD_OLD','subelm',bvcod_old)
     end if
     !
     ! BVCOB
     !
     if( nvcob > 0 ) then
        call memory_alloca(memor_dom,'BVCOB_OLD','subelm',bvcob_old,mvcob)
        do ivcob = 1,mvcob
           if( lvcob(1,ivcob) > 0 ) then
              call memory_alloca(memor_dom,'BVCOB_OLD%A','subelm',bvcob_old(ivcob)%a,lvcob(2,ivcob),nboun_old,'DO_NOT_INITIALIZE')
              do iboun = 1,nboun_old
                 do idime = 1,lvcob(2,ivcob)
                    bvcob_old(ivcob) % a(idime,iboun) = bvcob(ivcob) % a(idime,iboun)
                 end do
              end do
              nvcob = ivcob
              call membcs(-11_ip)
              call membcs( 11_ip)
           end if
        end do
        do ivcob = 1,mvcob
           if( lvcob(1,ivcob) > 0 ) then
              do iboun = 1,nboun_old
                 do idime = 1,lvcob(2,ivcob)
                    bvcob(ivcob) % a(idime,iboun) = bvcob_old(ivcob) % a(idime,iboun) 
                 end do
              end do
              kboun = 0
              if( ndime == 2 ) then
                 do iboun = 1,nboun_old
                    do ii = 1,2
                       kboun = kboun + 1
                       do idime = 1,lvcob(2,ivcob)
                          bvcob(ivcob) % a(idime,kboun) = bvcob_old(ivcob) % a(idime,iboun) 
                       end do
                    end do
                 end do
              else
                 do iboun = 1,nboun_old
                    do ii = 1,4
                       kboun = kboun + 1
                       do idime = 1,lvcob(2,ivcob)
                          bvcob(ivcob) % a(idime,kboun) = bvcob_old(ivcob) % a(idime,iboun) 
                       end do
                    end do
                 end do
              end if
              call memory_deallo(memor_dom,'BVCOB_OLD%A','subelm',bvcob_old(ivcob)%a)
           end if
        end do
        call memory_deallo(memor_dom,'BVCOB_OLD','subelm',bvcob_old)
     end if

     !--------------------------------------------------------------------
     !
     ! Interpolation
     !
     !--------------------------------------------------------------------

     if( 1 == 2 ) then

        linno => meshe(ndivi) % linno
        ! Old nodes
        do ipoin = 1,npoin_old
           linno(ipoin) % n = 1
           allocate( linno(ipoin) % l(linno(ipoin) % n) , stat = istat )
           linno(ipoin) % l(1) = ipoin
        end do
        ! Edge nodes
        ipoin = npoin_old
        do iedgg = 1,nedgg
           ipoin = ipoin + 1
           linno(ipoin) % n = 2
           allocate( linno(ipoin) % l(linno(ipoin) % n) , stat = istat )
           linno(ipoin) % l(1) = ledgg(1,iedgg)
           linno(ipoin) % l(2) = ledgg(2,iedgg) 
        end do
        ! Face nodes
        do ifacg = 1,nfacg
           ipoin = ipoin + 1
           linno(ipoin) % n = 4
           allocate( linno(ipoin) % l(linno(ipoin) % n) , stat = istat )
           linno(ipoin) % l(1) = lfacg(1,ifacg)
           linno(ipoin) % l(2) = lfacg(2,ifacg)
           linno(ipoin) % l(3) = lfacg(3,ifacg)
           linno(ipoin) % l(4) = lfacg(4,ifacg)
        end do
        ! Central nodes
        if( lnuty(QUA04) > 0 .or. lnuty(HEX08) > 0 ) then
           do ielem = 1,nelem_old
              pelty = abs(ltype_old(ielem))
              if( pelty == QUA04 .or. pelty == HEX08 ) then
                 pnode = nnode(pelty)
                 linno(ipoin) % n = pnode
                 allocate( linno(ipoin) % l(linno(ipoin) % n) , stat = istat )
                 ipoin = ipoin + 1
                 do inode = 1,pnode
                    linno(ipoin) % l(inode) = lnods_old(inode,ielem)
                 end do
              end if
           end do
        end if
     end if
     !
     ! Deallocate memory
     !
     call memory_deallo(memor_dom,'COORD_OLD','subelm',coord_old)  
     call memory_deallo(memor_dom,'LBOCH_OLD','subelm',lboch_old)         
     call memory_deallo(memor_dom,'LBOEL_OLD','subelm',lboel_old) 
     call memory_deallo(memor_dom,'LNODB_OLD','subelm',lnodb_old)  
     call memory_deallo(memor_dom,'LTYPB_OLD','subelm',ltypb_old)         
     call memory_deallo(memor_dom,'LNNOD_OLD','subelm',lnnod_old)         
     call memory_deallo(memor_dom,'LELCH_OLD','subelm',lelch_old)         
     call memory_deallo(memor_dom,'LTYPE_OLD','subelm',ltype_old)         
     call memory_deallo(memor_dom,'LNODS_OLD','subelm',lnods_old)  
 
  end if

  if( ISEQUEN ) then
     !
     ! LNINV_LOC: Node renumbering for sequential run
     !     
     deallocate( lninv_loc , stat = istat )
     if(istat/=0) call memerr(two,'LNINV_LOC','subelm',0_ip)
     call memchk(two,istat,memor_dom,'LNINV_LOC','subelm',lninv_loc)
     allocate( lninv_loc(npoin) , stat = istat )
     call memchk(zero,istat,memor_dom,'LNINV_LOC','subelm',lninv_loc)
     do ipoin = 1,npoin
        lninv_loc(ipoin) = ipoin
     end do
     !
     ! LEINV_LOC: Element renumbering for sequential run
     !     
     deallocate( leinv_loc , stat = istat )
     if(istat/=0) call memerr(two,'LEINV_LOC','subelm',0_ip)
     call memchk(two,istat,memor_dom,'LEINV_LOC','subelm',leinv_loc)
     allocate( leinv_loc(nelem) , stat = istat )
     call memchk(zero,istat,memor_dom,'LEINV_LOC','subelm',leinv_loc)
     do ielem = 1,nelem
        leinv_loc(ielem) = ielem
     end do
  end if

end subroutine subelm

subroutine edgnod(node1,node2,nodex)
  !-----------------------------------------------------------------------
  !****f* domain/edgnod
  ! NAME
  !    edgnod
  ! DESCRIPTION
  !    Find the extra node NODEX on edge NODE1-NODE2
  ! OUTPUT
  ! USED BY
  !    Turnon
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip
  use def_master, only     :  ledgg,pedgp
  implicit none
  integer(ip), intent(in)  :: node1,node2
  integer(ip), intent(out) :: nodex
  integer(ip)              :: iedgg
  integer(ip)              :: iedgp

  if( node1 > node2 ) then
     iedgg = pedgp(node1)-1
     do iedgp = 1,pedgp(node1+1)-pedgp(node1)
        iedgg = iedgg + 1
        if( ledgg(1,iedgg) == node2 ) then
           nodex = ledgg(3,iedgg)
           return
        end if
     end do
  else
     iedgg = pedgp(node2)-1
     do iedgp = 1,pedgp(node2+1)-pedgp(node2)
        iedgg = iedgg + 1
        if( ledgg(1,iedgg) == node1 ) then
           nodex = ledgg(3,iedgg)
           return
        end if
     end do
  end if

  print*,'EDGE NOT FOUND= ',node1,node2
  call runend('EDGNOD: EDGE MIDDLE NODE NOT FOUND')

end subroutine edgnod

subroutine facnod(node1,node2,node3,node4,nodex)
  !-----------------------------------------------------------------------
  !****f* domain/facnod
  ! NAME
  !    facnod
  ! DESCRIPTION
  !    Find the extra node NODEX on face NODE1-NODE2
  ! OUTPUT
  ! USED BY
  !    Turnon
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip
  use def_master, only     :  lfacg,nfacg
  implicit none
  integer(ip), intent(in)  :: node1,node2,node3,node4
  integer(ip), intent(out) :: nodex
  integer(ip)              :: ifacg
  integer(ip)              :: i,j,k
  integer(ip)              :: lnode(4)
  !
  ! Order nodes
  !
  lnode(1) = node1
  lnode(2) = node2
  lnode(3) = node3
  lnode(4) = node4
  do i = 1,3
     do j = i+1,4
        if( lnode(i) > lnode(j) ) then
           k        = lnode(i)
           lnode(i) = lnode(j) 
           lnode(j) = k
        end if
     end do
  end do
  !
  ! Find face
  !
  ifacg = 0
  do while( ifacg < nfacg )
     ifacg = ifacg + 1
     if( lnode(1) == lfacg(1,ifacg) ) then
        if( lnode(2) == lfacg(2,ifacg) ) then
           if( lnode(3) == lfacg(3,ifacg) ) then
              if( lnode(4) == lfacg(4,ifacg) ) then
                 nodex = lfacg(5,ifacg)
                 return
              end if
           end if
        end if
     end if
  end do
  print*,'FACE NOT FOUND= ',lnode(1:4)
  do ifacg =1, nfacg
     print*,'FACE= ',ifacg,' NODES= ',lfacg(1:4,ifacg)
  end do
  call runend('FACNOD: COULD NOT FIND FACE MIDDLE NODE')

end subroutine facnod

!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    renpoi.f90
!> @date    07/11/2013
!> @author  Guillaume Houzeaux
!> @brief   Renumber geometrical arrays
!> @details Renumber geometrical arrays, read from files,
!>          using a node permutation array LRENN
!>          \verbatim
!>            LNODS .......
!>            LNODB .......
!>            COORD .......
!>            LNSEC .......
!>            LGROU_DOM ...
!>            KFL_CODNO ...
!>            BVCOD .......
!>            XFIEL .......
!>          \endverbatim
!>
!> @} 
!-----------------------------------------------------------------------

subroutine renpoi()
  use def_kintyp
  use def_parame
  use def_domain
  use def_master
  use def_kermod
  use mod_memchk
  implicit none
  integer(ip)               :: ipoin,ielem,inode,iboun,idime,jpoin
  integer(ip)               :: icono,inset,inodb,pblty,ivcod,ifiel
  integer(ip)               :: nn,ii
  integer(ip),      pointer :: kfl_codno_tmp(:,:) => null()
  integer(ip),      pointer :: lgrou_dom_tmp(:)   => null()
  real(rp),         pointer :: coord_tmp(:,:)     => null()
  type(r2p),        pointer :: xfiel_tmp(:)       => null()
  type(r2p),        pointer :: bvcod_tmp(:)       => null()
  type(linno_type), pointer :: linno_tmp(:)       => null()
  integer(4)                :: istat

  !----------------------------------------------------------------------
  !
  ! LNODS, LNODB, COORD: geometrical arrays
  !
  !----------------------------------------------------------------------

  do ielem = 1,nelem
     do inode = 1,lnnod(ielem)
        lnods(inode,ielem) = lrenn(lnods(inode,ielem))
     end do
  end do

  do iboun = 1,nboun
     pblty = ltypb(iboun)
     do inodb = 1,nnode(pblty)
        lnodb(inodb,iboun) = lrenn(lnodb(inodb,iboun))
     end do
  end do

  allocate(coord_tmp(ndime,npoin),stat=istat)  
  call memchk(zero,istat,memor_dom,'COORD_TMP','renpoi',coord_tmp)
  do ipoin = 1,npoin
     do idime = 1,ndime
        coord_tmp(idime,ipoin) = coord(idime,ipoin)
     end do
  end do
  do ipoin = 1,npoin
     jpoin = lrenn(ipoin)
     do idime = 1,ndime
        coord(idime,jpoin) = coord_tmp(idime,ipoin)
     end do
  end do
  deallocate(coord_tmp,stat=istat)
  if(istat/=0) call memerr(two,'COORD_TMP','renpoi',0_ip)
  call memchk(two,istat,memor_dom,'COORD_TMP','renpoi',coord_tmp)

  !--------------------------------------------------------------------
  !
  ! Sets: LNSEC
  !
  !--------------------------------------------------------------------

  do inset = 1,nnset
     ipoin = lnsec(inset)
     lnsec(inset) = lrenn(ipoin)
  end do

  !--------------------------------------------------------------------
  !
  ! Fields
  !
  !--------------------------------------------------------------------

  if( nfiel > 0 ) then

     allocate(xfiel_tmp(nfiel))
     do ifiel = 1,nfiel
        if( kfl_field(1,ifiel) > 0 ) then
           if( kfl_field(2,ifiel) == NPOIN_TYPE ) then
              allocate(xfiel_tmp(ifiel)%a(kfl_field(1,ifiel)*kfl_field(4,ifiel),npoin))
              do ipoin = 1,npoin
                 do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                    xfiel_tmp(ifiel) % a(idime,ipoin) = xfiel(ifiel) % a(idime,ipoin)
                 end do
              end do
              do ipoin = 1,npoin
                 jpoin = lrenn(ipoin)
                 do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                    xfiel(ifiel) % a(idime,jpoin) = xfiel_tmp(ifiel) % a(idime,ipoin)
                 end do
              end do
              deallocate(xfiel_tmp(ifiel)%a)
           end if
        end if
     end do
     deallocate(xfiel_tmp)

  end if

  !--------------------------------------------------------------------
  !
  ! Groups
  !
  !--------------------------------------------------------------------

  if( ngrou_dom > 0 ) then

     allocate(lgrou_dom_tmp(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LGROU_DOM_TMP','renpoi',lgrou_dom_tmp)

     do ipoin = 1,npoin
        lgrou_dom_tmp(ipoin) = lgrou_dom(ipoin)
     end do
     do ipoin = 1,npoin
        jpoin = lrenn(ipoin)
        lgrou_dom(jpoin) = lgrou_dom_tmp(ipoin)
     end do

     deallocate(lgrou_dom_tmp,stat=istat)
     if(istat/=0) call memerr(two,'LGROU_DOM_TMP','renpoi',0_ip)
     call memchk(two,istat,memor_dom,'LGROU_DOM_TMP','renpoi',lgrou_dom_tmp)

  end if

  !--------------------------------------------------------------------
  !
  ! Boundary conditions: KFL_CODNO
  !
  !--------------------------------------------------------------------

  if( kfl_icodn > 0 ) then

     allocate(kfl_codno_tmp(mcono,npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'KFL_CODNO_TMP','renpoi',kfl_codno_tmp)

     do ipoin = 1,npoin
        do icono = 1,mcono
           kfl_codno_tmp(icono,ipoin) = kfl_codno(icono,ipoin)
        end do
     end do
     do ipoin = 1,npoin
        jpoin = lrenn(ipoin)
        do icono = 1,mcono
           kfl_codno(icono,jpoin) = kfl_codno_tmp(icono,ipoin)
        end do
     end do

     deallocate(kfl_codno_tmp,stat=istat)
     if(istat/=0) call memerr(two,'KFL_CODNO_TMP','renpoi',0_ip)
     call memchk(two,istat,memor_dom,'KFL_CODNO_TMP','renpoi',kfl_codno_tmp)

  end if

  !--------------------------------------------------------------------
  !
  ! Boundary conditions: BVCOD
  !
  !--------------------------------------------------------------------

  if( nvcod > 0 ) then

     allocate(bvcod_tmp(mvcod))
     do ivcod = 1,mvcod
        if( lvcod(1,ivcod) > 0 ) then
           allocate(bvcod_tmp(ivcod)%a(lvcod(2,ivcod),npoin))
           do ipoin = 1,npoin
              do idime = 1,lvcod(2,ivcod)
                 bvcod_tmp(ivcod) % a(idime,ipoin) = bvcod(ivcod) % a(idime,ipoin)
              end do
           end do
           do ipoin = 1,npoin
              jpoin = lrenn(ipoin)
              do idime = 1,lvcod(2,ivcod)
                 bvcod(ivcod) % a(idime,jpoin) = bvcod_tmp(ivcod) % a(idime,ipoin)
              end do
           end do
           deallocate(bvcod_tmp(ivcod)%a)
        end if
     end do
     deallocate(bvcod_tmp)

  end if

  !--------------------------------------------------------------------
  !
  ! Interpolation
  !
  !--------------------------------------------------------------------

  if( 1 == 2 ) then
     
     allocate(linno_tmp(npoin))
     do ipoin = 1,npoin
        nn    = meshe(ndivi) % linno(ipoin) % n
        linno_tmp(ipoin) % n = nn
        allocate( linno_tmp(ipoin) % l(nn))
        do ii = 1,nn
           linno_tmp(ipoin) % l(ii) = meshe(ndivi) % linno(ipoin) % l(ii)
        end do
     end do
     do ipoin = 1,npoin
        jpoin = lrenn(ipoin)
        nn    = linno_tmp(ipoin) % n
        meshe(ndivi) % linno(jpoin) % n = nn
        allocate( linno_tmp(ipoin) % l(nn))
        do ii = 1,nn
           meshe(ndivi) % linno(jpoin) % l(ii) = linno_tmp(ipoin) % l(ii)
        end do
     end do
     deallocate(linno_tmp)

  end if

end subroutine renpoi

subroutine subdim()
  !-----------------------------------------------------------------------
  !****f* domain/subdim
  ! NAME
  !    domain
  ! DESCRIPTION
  !    Create edge table
  ! OUTPUT
  !    NNEDG ... Number of edges
  !    LEDGG ... Edge table
  !    LEDGB ... Boundary edge table (when Parall is on)
  ! USED BY
  !    Turnon
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_domain
  use def_master
  use def_kermod
  use mod_memchk
  implicit none
  integer(ip) :: ipart
  !
  ! Output geometry 
  !
  if( IMASTER ) then
     ioutp(1) = 0
     ioutp(3) = 0
     ioutp(2) = 0
     routp(1) = 0.0_rp
     routp(2) = 0.0_rp
     routp(3) = 0.0_rp
     do ipart = 2,npart+1
        ioutp(1) = ioutp(1) + nelem_tot(ipart)
        routp(1) = routp(1) + real(nelem_tot(ipart),rp)
        ioutp(2) = ioutp(2) + npoin_tot(ipart)
        routp(2) = routp(2) + real(npoin_tot(ipart),rp)
        ioutp(3) = ioutp(3) + nboun_tot(ipart)
        routp(3) = routp(3) + real(nboun_tot(ipart),rp)
     end do
  else
     ioutp(1) = nelem
     ioutp(2) = npoin
     ioutp(3) = nboun
     routp(1) = real(nelem,rp)
     routp(2) = real(npoin,rp)
     routp(3) = real(nboun,rp)
  end if

  call livinf(97_ip,'MESH DIMENSIONS',0_ip)

end subroutine subdim

subroutine submem(itask)
  !-----------------------------------------------------------------------
  !****f* domain/submem
  ! NAME
  !    domain
  ! DESCRIPTION
  !    De/allocate memory for mesh division algorithm
  ! OUTPUT
  ! USED BY
  !    Turnon
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_domain
  use def_master
  use def_kermod
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin
  integer(4)              :: istat

  if( INOTMASTER ) then

     select case ( itask )

     case ( 1_ip ) 
        !
        ! Allocate memory
        !
        allocate(lnlev(npoin),stat=istat)
        call memchk(zero,istat,memor_dom,'LNLEV','submem',lnlev)
        do ipoin = 1,npoin
           lnlev(ipoin) = ipoin
        end do
        !
        ! Unuseful but avoids a lot of if's
        !
        !if( ISEQUEN ) then
        !   allocate( lninv_loc(npoin),stat=istat)
        !   call memchk(zero,istat,memor_dom,'lninv_loc','submem',lninv_loc )
        !   do ipoin = 1,npoin
        !      lninv_loc(ipoin) = ipoin
        !   end do
        !end if

        !allocate(lelev(nelem),stat=istat)
        !call memchk(zero,istat,memor_dom,'LELEV','par_submem',lelev)
        !do ielem = 1,nelem
        !   lelev(ielem) = ielem
        !end do

        !allocate(lblev(nboun),stat=istat)
        !call memchk(zero,istat,memor_dom,'LBLEV','par_submem',lblev)
        !do iboun = 1,nboun
        !   lblev(iboun) = iboun
        !end do

     case ( -1_ip ) 
        !
        ! LNLEV, LELEV, LBLEV: Deallocate memory
        !
        deallocate(lnlev,stat=istat)
        if(istat/=0) call memerr(two,'LNLEV','submem',0_ip)
        call memchk(two,istat,memor_dom,'LNLEV','submem',lnlev)

        !deallocate(lelev,stat=istat)
        !if(istat/=0) call memerr(two,'LELEV','par_submem',0_ip)
        !call memchk(two,istat,memor_dom,'LELEV','par_submem',lelev)

        !deallocate(lblev,stat=istat)
        !if(istat/=0) call memerr(two,'LBLEV','par_submem',0_ip)
        !call memchk(two,istat,memor_dom,'LBLEV','par_submem',lblev)

     case ( 2_ip ) 
        !
        ! LEDGG: Deallocate memory
        !
        deallocate(ledgg,stat=istat)
        if(istat/=0) call memerr(two,'LEDGG','submem',0_ip)
        call memchk(two,istat,memor_dom,'LEDGG','submem',ledgg)
        !
        ! LFACG: Deallocate memory
        !
        if( nfacg > 0 ) then
           deallocate(lfacg,stat=istat)
           if(istat/=0) call memerr(two,'LFACG','submem',0_ip)
           call memchk(two,istat,memor_dom,'LFACG','submem',lfacg)
        end if

     end select

  end if

end subroutine submem

subroutine savmsh(itask)
  !-----------------------------------------------------------------------
  !****f* domain/savmsh
  ! NAME
  !    savmsh
  ! DESCRIPTION
  !    Save original mesh for pstprocess purpose
  ! OUTPUT
  ! USED BY
  !    submsh
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_domain
  use def_master
  use def_kermod
  use mod_memory
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ielem,inode,ipoin,idime,inodb,iboun,ipart
  integer(ip)             :: idivi,kpoin
  integer(4)              :: istat

  if( itask == 1 ) then

     !-------------------------------------------------------------------
     !
     ! Allocate mesh structure
     ! Save postprocess mesh
     !
     !-------------------------------------------------------------------
     !
     ! Allocate memory of mesh structure
     !
     allocate( meshe(-1:ndivi) )

     do idivi = -1,ndivi
        meshe(idivi) % ndime = 0      
        meshe(idivi) % ntens = 0      
        meshe(idivi) % npoin = 0            
        meshe(idivi) % nelem = 0            
        meshe(idivi) % nboun = 0            
        meshe(idivi) % mnode = 0            
        meshe(idivi) % mgaus = 0            
        meshe(idivi) % nbopo = 0            
        meshe(idivi) % npoi1 = 0            
        meshe(idivi) % npoi2 = 0            
        meshe(idivi) % npoi3 = 0            
        nullify(meshe(idivi) % npoin_par)
        nullify(meshe(idivi) % nelem_par)
        nullify(meshe(idivi) % nboun_par)
        nullify(meshe(idivi) % lnods)      
        nullify(meshe(idivi) % ltype)        
        nullify(meshe(idivi) % lelch)        
        nullify(meshe(idivi) % lnnod)        
        nullify(meshe(idivi) % lesub)        
        nullify(meshe(idivi) % leinv_loc)    
        nullify(meshe(idivi) % lnodb)      
        nullify(meshe(idivi) % lboel)      
        nullify(meshe(idivi) % ltypb)        
        nullify(meshe(idivi) % lboch)        
        nullify(meshe(idivi) % lnnob)        
        nullify(meshe(idivi) % lninv_loc)    
        nullify(meshe(idivi) % coord)      
        nullify(meshe(idivi) % lnoch)        
        nullify(meshe(idivi) % lpoty)        
        
        nullify(meshe(idivi) % leset)        
        nullify(meshe(idivi) % lbset)        
        nullify(meshe(idivi) % lnset)        
        
        nullify(meshe(idivi) % kfl_codno)  
        nullify(meshe(idivi) % kfl_codbo)    
        
        nullify(meshe(idivi) % lgrou_dom)    
        
        nullify(meshe(idivi) % linno)        
        
        nullify(meshe(idivi) % c_dom)        
        nullify(meshe(idivi) % r_dom)        
        nullify(meshe(idivi) % c_sym)        
        nullify(meshe(idivi) % r_sym)        
        
        nullify(meshe(idivi) % exnor)    
        nullify(meshe(idivi) % vmass)        
        nullify(meshe(idivi) % vmasc)                
     end do

     !npoin_old = npoin
     !nelem_old = nelem
     !nboun_old = nboun

     if( INOTSLAVE ) then 
        do idivi = 0,ndivi
           allocate( meshe(idivi) % npoin_par(npart) )
           allocate( meshe(idivi) % nelem_par(npart) )
           allocate( meshe(idivi) % nboun_par(npart) )
        end do
     end if
     !
     ! Save LNINV_LOC of original mesh
     ! It is useful to refer to original mesh numbering
     !   - IPOIN = lninv_loc(lnlev(JPOIN)) 
     !     - IPOIN:  original numbering
     !     - JPOIN:  local numbering
     !     - Check that lnlev(JPOIN) /= 0
     ! ISEQUEN does it too to avoid many if's
     ! MASTER allocate minimum memory
     ! Save also LEINV_LOC of original mesh
     !
     if( INOTMASTER ) then

        if( ISEQUEN ) then
           call memory_alloca(memor_dom,'LNINV_LOC','savmsh',lninv_loc,npoin)
           do ipoin = 1,npoin
              lninv_loc(ipoin) = ipoin
           end do
           call memory_alloca(memor_dom,'LEINV_LOC','savmsh',leinv_loc,nelem)
           do ielem = 1,nelem
              leinv_loc(ielem) = ielem
           end do
        end if
        if( ndivi > 0 ) then
           call memory_alloca(memor_dom,'LNINV_LOC','savmsh',meshe(0) % lninv_loc,npoin)
           do ipoin = 1,npoin
              meshe(0) % lninv_loc(ipoin) = lninv_loc(ipoin)
           end do
           call memory_alloca(memor_dom,'LNINV_LOC','savmsh',meshe(0) % leinv_loc,nelem)
           do ielem = 1,nelem
              meshe(0) % leinv_loc(ielem) = leinv_loc(ielem)
           end do
        else
           meshe(0) % lninv_loc => lninv_loc
           meshe(0) % leinv_loc => leinv_loc
        end if

     else if( IMASTER ) then
 
        call memory_alloca(memor_dom,'LEINV_LOC','savmsh',meshe(kfl_posdi) % leinv_loc,2_ip)
        call memory_alloca(memor_dom,'LNINV_LOC','savmsh',meshe(kfl_posdi) % lninv_loc,2_ip)

        call memory_alloca(memor_dom,'LNODS'    ,'savmsh',meshe(0) % lnods,ndime,2_ip)
        call memory_alloca(memor_dom,'LTYPE'    ,'savmsh',meshe(0) % ltype,2_ip)
        call memory_alloca(memor_dom,'LELCH'    ,'savmsh',meshe(0) % lelch,2_ip)
        call memory_alloca(memor_dom,'LNNOD'    ,'savmsh',meshe(0) % lnnod,2_ip)
        call memory_alloca(memor_dom,'LESUB'    ,'savmsh',meshe(0) % lesub,2_ip)
        call memory_alloca(memor_dom,'LNODB'    ,'savmsh',meshe(0) % lnodb,mnodb,2_ip)
        call memory_alloca(memor_dom,'LTYPB'    ,'savmsh',meshe(0) % ltypb,2_ip)
        call memory_alloca(memor_dom,'LBOCH'    ,'savmsh',meshe(0) % lboch,2_ip)

        call memory_alloca(memor_dom,'COORD'    ,'savmsh',meshe(0) % coord,ndime,2_ip)

     end if

     if( ndivi > 0 .and. kfl_posdi == 0 ) then
        !
        ! Mesh has been divided and postprocess is on original mesh
        !
        if( IMASTER ) then
           !
           ! Master and sequen save global geometry parameters
           !
           meshe(0) % npoin_total = npoin_total
           meshe(0) % nelem_total = nelem_total
           meshe(0) % nboun_total = nboun_total
           do ipart = 1,npart
              meshe(0) % npoin_par(ipart) = npoin_par(ipart)
              meshe(0) % nelem_par(ipart) = nelem_par(ipart)
              meshe(0) % nboun_par(ipart) = nboun_par(ipart)
           end do

        end if

        if( INOTMASTER ) then
           !
           ! Slaves and sequen save original geometry
           !
           meshe(0) % ndime = ndime
           meshe(0) % ntens = ntens
           meshe(0) % npoin = npoin
           meshe(0) % nelem = nelem
           meshe(0) % nboun = nboun
           meshe(0) % mnode = mnode
           meshe(0) % mgaus = mgaus
           meshe(0) % nbopo = nbopo
           meshe(0) % npoi1 = npoi1
           meshe(0) % npoi2 = npoi2
           meshe(0) % npoi3 = npoi3
 
           call memory_alloca(memor_dom,'LNODS','savmsh',meshe(0) % lnods,mnode,nelem)
           call memory_alloca(memor_dom,'LTYPE','savmsh',meshe(0) % ltype,nelem)
           call memory_alloca(memor_dom,'LNNOD','savmsh',meshe(0) % lnnod,nelem)
           call memory_alloca(memor_dom,'LELCH','savmsh',meshe(0) % lelch,nelem)
           call memory_alloca(memor_dom,'LESUB','savmsh',meshe(0) % lesub,nelem)

           call memory_alloca(memor_dom,'LNODB','savmsh',meshe(0) % lnodb,mnodb,nboun)
           call memory_alloca(memor_dom,'LTYPB','savmsh',meshe(0) % ltypb,nboun)
           call memory_alloca(memor_dom,'LBOCH','savmsh',meshe(0) % lboch,nboun)

           call memory_alloca(memor_dom,'COORD','savmsh',meshe(0) % coord,ndime,npoin)

           do ielem = 1,nelem
              meshe(0) % ltype(ielem) = ltype(ielem)
              do inode = 1,mnode
                 meshe(0) % lnods(inode,ielem) = lnods(inode,ielem)
              end do
           end do

            do ielem = 1,nelem
              meshe(0) % lelch(ielem) = lelch(ielem)
              meshe(0) % lnnod(ielem) = lnnod(ielem)
              meshe(0) % lesub(ielem) = lesub(ielem)
           end do

          do iboun = 1,nboun
              do inodb = 1,mnodb
                 meshe(0) % lnodb(inodb,iboun) = lnodb(inodb,iboun) 
              end do
              meshe(0) % ltypb(iboun) = ltypb(iboun) 
              meshe(0) % lboch(iboun) = lboch(iboun) 
           end do

           do ipoin = 1,npoin
              do idime = 1,ndime
                 meshe(0) % coord(idime,ipoin) = coord(idime,ipoin)
              end do
           end do
        end if
     end if

  else if( itask == 2 ) then

     !-------------------------------------------------------------------
     !
     ! Point to postprocess mesh
     !
     !-------------------------------------------------------------------

     meshe(ndivi) % ndime     =  ndime
     meshe(ndivi) % ntens     =  ntens
     meshe(ndivi) % npoin     =  npoin
     meshe(ndivi) % nelem     =  nelem
     meshe(ndivi) % nboun     =  nboun
     meshe(ndivi) % mnode     =  mnode
     meshe(ndivi) % mgaus     =  mgaus
     meshe(ndivi) % nbopo     =  nbopo     ! Computed in extnor
     meshe(ndivi) % npoi1     =  npoi1
     meshe(ndivi) % npoi2     =  npoi2
     meshe(ndivi) % npoi3     =  npoi3

     meshe(ndivi) % lnods     => lnods
     meshe(ndivi) % ltype     => ltype
     meshe(ndivi) % lelch     => lelch
     meshe(ndivi) % lnnod     => lnnod
     meshe(ndivi) % lesub     => lesub
     meshe(ndivi) % leinv_loc => leinv_loc
     meshe(ndivi) % lnodb     => lnodb
     meshe(ndivi) % ltypb     => ltypb
     meshe(ndivi) % lboch     => lboch

     meshe(ndivi) % lninv_loc => lninv_loc
     meshe(ndivi) % coord     => coord 
     meshe(ndivi) % lnoch     => lnoch    
     meshe(ndivi) % lpoty     => lpoty        ! Computed in extnor

     meshe(ndivi) % r_dom     => r_dom        ! Computed in domgra
     meshe(ndivi) % c_dom     => c_dom        ! Computed in domgra

     meshe(ndivi) % exnor     => exnor        ! Computed in extnor
     meshe(ndivi) % vmass     => vmass        ! Computed in massma
     meshe(ndivi) % vmasc     => vmasc        ! Computed in massmc

     if( INOTSLAVE ) then
        meshe(ndivi) % npoin_total = npoin_total
        meshe(ndivi) % nelem_total = nelem_total
        meshe(ndivi) % nboun_total = nboun_total
        do ipart = 1,npart
           meshe(ndivi) % npoin_par(ipart) = npoin_par(ipart)
           meshe(ndivi) % nelem_par(ipart) = nelem_par(ipart)
           meshe(ndivi) % nboun_par(ipart) = nboun_par(ipart)
        end do
     end if

     if( INOTMASTER .and. ndivi > 0 .and. kfl_posdi == 0 ) then        
        allocate( lpmsh(meshe(0) % npoin) , stat = istat )
        do ipoin = 1,npoin
           kpoin = lnlev(ipoin)
           if( kpoin > 0 ) lpmsh(kpoin) = ipoin
        end do
     end if

  else if( itask == 3 ) then

     !-------------------------------------------------------------------
     !
     ! Deallocate postprocess mesh
     !
     !-------------------------------------------------------------------
 
     if( INOTMASTER ) then

        call memory_deallo(memor_dom,'LNODS'    ,'savmsh',meshe(0) % lnods)
        call memory_deallo(memor_dom,'LTYPE'    ,'savmsh',meshe(0) % ltype)
        call memory_deallo(memor_dom,'LNNOD'    ,'savmsh',meshe(0) % lnnod)
        call memory_deallo(memor_dom,'LELCH'    ,'savmsh',meshe(0) % lelch)
        call memory_deallo(memor_dom,'LESUB'    ,'savmsh',meshe(0) % lesub)
        !call memory_deallo(memor_dom,'LEINV_LOC','savmsh',meshe(0) % leinv_loc)
        call memory_deallo(memor_dom,'LNODB'    ,'savmsh',meshe(0) % lnodb)
        call memory_deallo(memor_dom,'LTYPB'    ,'savmsh',meshe(0) % ltypb)
        call memory_deallo(memor_dom,'LBOCH'    ,'savmsh',meshe(0) % lboch)
        !call memory_deallo(memor_dom,'LBOCH'    ,'savmsh',meshe(0) % lboel)

        !call memory_deallo(memor_dom,'LNINV_LOC','savmsh',meshe(0) % lninv_loc)
        call memory_deallo(memor_dom,'COORD'    ,'savmsh',meshe(0) % coord)
        !call memory_deallo(memor_dom,'LNOCH'    ,'savmsh',meshe(0) % lnoch)
        !call memory_deallo(memor_dom,'LPOTY'    ,'savmsh',meshe(0) % lpoty)

     end if

  else if( itask == 4 ) then

     !-------------------------------------------------------------------
     !
     ! Repoint to postprocess mesh after computing fringe geometry 
     ! Called by par_fringe
     !
     !-------------------------------------------------------------------

     meshe(ndivi) % lnods     => lnods
     meshe(ndivi) % ltype     => ltype
     meshe(ndivi) % lnnod     => lnnod
     meshe(ndivi) % lelch     => lelch
     meshe(ndivi) % lesub     => lesub
     meshe(ndivi) % leinv_loc => leinv_loc
     meshe(ndivi) % lnodb     => lnodb
     meshe(ndivi) % ltypb     => ltypb
     meshe(ndivi) % lboch     => lboch
     meshe(ndivi) % lboel     => lboel
     meshe(ndivi) % lnnob     => lnnob

     meshe(ndivi) % lninv_loc => lninv_loc
     meshe(ndivi) % coord     => coord 
     meshe(ndivi) % lnoch     => lnoch    
     meshe(ndivi) % lpoty     => lpoty  

     meshe(ndivi) % r_dom     => r_dom        ! Computed in domgra
     meshe(ndivi) % c_dom     => c_dom        ! Computed in domgra

     meshe(ndivi) % exnor     => exnor        ! Computed in extnor
     meshe(ndivi) % vmass     => vmass        ! Computed in massma
     meshe(ndivi) % vmasc     => vmasc        ! Computed in massmc

  end if

end subroutine savmsh

subroutine codbcs()
  !-----------------------------------------------------------------------
  !****f* domain/subelm
  ! NAME
  !    domain
  ! DESCRIPTION
  !    Cancel codes for nodes which are not on the boundary to avoid
  !    false boundary condition when interpolating them
  ! OUTPUT
  ! USED BY
  !    submsh
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_elmtyp
  use def_domain
  use def_master
  use def_kermod
  implicit none
  integer(ip) :: ipoin,ii

  if( ndivi > 0 .and. INOTMASTER ) then
     if( kfl_icodn > 0 ) then
        do ipoin = 1,npoin
           if( lpoty(ipoin) == 0 ) then
              do ii = 1,mcono
                 kfl_codno(ii,ipoin) = mcodb + 1
              end do
           end if
        end do
     end if
  end if

end subroutine codbcs
