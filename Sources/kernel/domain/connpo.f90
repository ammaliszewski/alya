subroutine connpo()
  !-----------------------------------------------------------------------
  !****f* Domain/connpo
  ! NAME
  !    connpo
  ! DESCRIPTION
  !    This routine compute the node/element connectivity arrays.
  ! OUTPUT
  !    NEPOI(NPOIN) ............ # of elements connected to nodes
  !    PELPO(NPOIN+1) .......... Pointer to list of element LELPO
  !    LELPO(PELPO(NPOIN+1)) ... List of elements connected to nodes
  ! USED BY
  !    domgra
  !    par_partit
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use mod_memchk
  implicit none
  integer(ip) :: inode,ipoin,ielem,nlelp
  integer(4)  :: istat 
  !
  ! Allocate memory for NEPOI and compute it
  !
  allocate(nepoi(npoin),stat = istat)
  call memchk(zero,istat,memor_dom,'NEPOI','connpo',nepoi)

  do ielem = 1,nelem
     do inode = 1,lnnod(ielem)
        ipoin = lnods(inode,ielem)
        nepoi(ipoin) = nepoi(ipoin) + 1
     end do
  end do
  !
  ! Allocate memory for PELPO and compute it
  !
  allocate(pelpo(npoin+1),stat = istat)
  call memchk(zero,istat,memor_dom,'PELPO','connpo',pelpo)
  pelpo(1) = 1
  do ipoin = 1,npoin
     pelpo(ipoin+1) = pelpo(ipoin) + nepoi(ipoin)
  end do
  !
  ! Allocate memory for LELPO and construct the list
  !
  nlelp = pelpo(npoin+1)
  allocate(lelpo(nlelp),stat = istat)
  call memchk(zero,istat,memor_dom,'LELPO','connpo',lelpo)
  mpopo = 0
  do ielem = 1,nelem
     mpopo  =  mpopo + lnnod(ielem) * lnnod(ielem)
     do inode = 1,lnnod(ielem)
        ipoin = lnods(inode,ielem)
        lelpo(pelpo(ipoin)) = ielem
        pelpo(ipoin) = pelpo(ipoin)+1
     end do
  end do
  !
  ! Recompute PELPO and maximum number of element neighbors MEPOI
  !
  pelpo(1) = 1
  mepoi = -1
  do ipoin = 1,npoin
     pelpo(ipoin+1) = pelpo(ipoin)+nepoi(ipoin)
     mepoi = max(mepoi,nepoi(ipoin))
  end do
  !
  ! Deallocate memory for temporary node/element connectivity
  !
  call memchk(two,istat,memor_dom,'NEPOI','connpo',nepoi)
  deallocate(nepoi,stat = istat)
  if(istat /= 0 ) call memerr(two,'NEPOI','connpo',0_ip)

end subroutine connpo
