subroutine rennod(itask)
  !------------------------------------------------------------------------
  !****f* domain/rennod
  ! NAME
  !    rennod
  ! DESCRIPTION
  !    This routine renumbers the nodes
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use mod_memchk
  implicit none   
  integer(ip), intent(in) :: itask
  integer(ip)             :: idime,ipoin,jpoin,inode,ielem,pelty
  integer(ip)             :: iboun,inodb,pblty,icono,inset,jelem,lsize
  integer(ip)             :: izsym,izsta,izdom
  integer(ip), pointer    :: kfl_codno_tmp(:,:)  => null()
  integer(ip), pointer    :: lista(:)            => null()
  real(rp),    pointer    :: coord_tmp(:,:)      => null()
  integer(4)              :: istat

  if( ISLAVE ) return
  call livinf(80_ip,' ',0_ip)
  !
  ! LRENN: Allocate memory
  !
  allocate(lrenn(npoin),stat=istat)  
  call memchk(zero,istat,memor_dom,'LRENN','rennod',lrenn)
  !
  ! Compute graph
  !
  call connpo()
  allocate(lista(mpopo),stat=istat)
  call memchk(zero,istat,memor_dom,'LISTA','domgra',lista)     
  allocate(r_dom(npoin+1),stat=istat)
  call memchk(zero,istat,memor_dom,'R_DOM','domgra',r_dom)
  !
  ! Construct the array of indexes
  !     
  r_dom(1) = 1
  do ipoin = 1, npoin
     lsize = 0
     do ielem = pelpo(ipoin), pelpo(ipoin+1)-1
        jelem = lelpo(ielem)
        call mergl2( ipoin, lista(r_dom(ipoin)), lsize, lnods(1,jelem), &
             nnode(ltype(jelem)), mone )
     enddo
     r_dom(ipoin+1) = r_dom(ipoin) + lsize 
  enddo
  nzdom = r_dom(npoin+1)-1
  allocate(c_dom(nzdom),stat=istat)
  call memchk(zero,istat,memor_dom,'C_DOM','domgra',c_dom)     
  do ipoin=1, nzdom
     c_dom(ipoin) = lista(ipoin)
  enddo
  mpopo = 0
  call memchk(two,istat,memor_dom,'LISTA','domgra',lista)
  deallocate(lista,stat=istat)
  if(istat/=0) call memerr(two,'LISTA','domgra',0_ip)
  call memchk(two,istat,memor_dom,'LELPO','domgra',lelpo)
  deallocate(lelpo,stat=istat)
  if(istat/=0) call memerr(two,'LELPO','domgra',0_ip)
  call memchk(two,istat,memor_dom,'PELPO','domgra',pelpo)
  deallocate(pelpo,stat=istat)
  if(istat/=0) call memerr(two,'PELPO','domgra',0_ip)
  !
  ! Get band and profile
  !
  call gtband(npoin,c_dom,r_dom,bandw_dom,profi_dom)
  !print*,bandw_dom,profi_dom
  !
  ! Renumber
  !
  !call rennod_renupoking2(npoin,ndime,coord,c_dom,r_dom,0_ip,lrenn)
  call rennod_renufront(npoin,ndime,coord,c_dom,r_dom,0_ip,lrenn)
  !
  ! Deallocate graph
  !
  deallocate(c_dom,stat=istat)
  if(istat/=0) call memerr(two,'C_DOM','memgeo',0_ip)
  call memchk(two,istat,memor_dom,'C_DOM','memgeo',c_dom)     
  deallocate(r_dom,stat=istat)
  if(istat/=0) call memerr(two,'R_DOM','memgeo',0_ip)
  call memchk(two,istat,memor_dom,'R_DOM','memgeo',r_dom)

  call renpoi(lrenn)
  !----------------------------------------------------------------------
  !
  ! LNODS, LNODB, COORD: geometrical arrays
  !
  !----------------------------------------------------------------------

  do ielem = 1,nelem
     pelty = ltype(ielem)
     do inode = 1,nnode(pelty)
        lnods(inode,ielem) = lrenn(lnods(inode,ielem))
     end do
  end do
  do iboun = 1,nboun
     pblty = ltypb(iboun)
     do inodb = 1,nnode(pblty)
        lnodb(inodb,iboun) = lrenn(lnodb(inodb,iboun))
     end do
  end do

  allocate(coord_tmp(ndime,npoin),stat=istat)  
  call memchk(zero,istat,memor_dom,'COORD_TMP','rennod',coord_tmp)
  do ipoin = 1,npoin
     do idime = 1,ndime
        coord_tmp(idime,ipoin) = coord(idime,ipoin)
     end do
  end do
  do ipoin = 1,npoin
     jpoin = lrenn(ipoin)
     do idime = 1,ndime
        coord(idime,jpoin) = coord_tmp(idime,ipoin)
     end do
  end do
  deallocate(coord_tmp,stat=istat)
  if(istat/=0) call memerr(two,'COORD_TMP','memgeo',0_ip)
  call memchk(two,istat,memor_dom,'COORD_TMP','memgeo',coord_tmp)

  if( itask == 2 ) then

     !----------------------------------------------------------------------
     !
     ! Sets and boundary conditions: LNSEC, KFL_CODNO
     !
     !----------------------------------------------------------------------

     do inset = 1,nnset
        ipoin = lnsec(inset)
        lnsec(inset) = lrenn(ipoin)
     end do

     if( ncodn > 0 ) then

        allocate(kfl_codno_tmp(mcono,npoin),stat=istat)
        call memchk(zero,istat,memor_dom,'KFL_CODNO_TMP','rennod',kfl_codno_tmp)

        do ipoin = 1,npoin
           do icono = 1,mcono
              kfl_codno_tmp(icono,ipoin) = kfl_codno(icono,ipoin)
           end do
        end do
        do ipoin = 1,npoin
           jpoin = lrenn(ipoin)
           do icono = 1,mcono
              kfl_codno(icono,jpoin) = kfl_codno_tmp(icono,ipoin)
           end do
        end do

        deallocate(kfl_codno_tmp,stat=istat)
        if(istat/=0) call memerr(two,'KFL_CODNO_TMP','rennod',0_ip)
        call memchk(two,istat,memor_dom,'KFL_CODNO_TMP','rennod',kfl_codno_tmp)

     end if

  end if
  !deallocate(lrenn,stat=istat)
  !if(istat/=0) call memerr(two,'LRENN','rennod',0_ip)
  !call memchk(two,istat,memor_dom,'LRENN','rennod',lrenn)

end subroutine rennod

subroutine rennod_renupoking2(npoin,ndim,coor,ptopo1,ptopo2,irev,lrenn)
  use def_kintyp, only       :  ip,rp,lg
  use mod_memchk
  use def_meshin, only          : memor_msh  
  implicit none
  integer(ip), intent(in)    :: npoin,ndim,irev
  real(rp),    intent(inout) :: coor(ndim,npoin)
  integer(ip), intent(in)    :: ptopo1(*),ptopo2(npoin+1)
  integer(ip), intent(inout) :: lrenn(npoin)
  integer(ip), pointer       :: lpnt(:,:)  => null()
  integer(ip), pointer       :: ldeg(:)    => null()
  integer(ip), pointer       :: lhash(:,:) => null()
  integer(ip), pointer       :: lstack(:)  => null()
  integer(ip), pointer       :: lstack2(:) => null()
  integer(ip)                :: ipoin,istack,nstack,iter,ipo,ielem,j,ip1,ipmax
  integer(ip)                :: jpmax,k,nstackp,jpoin,ismall 
  integer(ip)                :: nstack0,nstack1,iwidth,iwidthm,nrenu,nhash,ipmin 
  integer(ip)                :: idegmin,iloc,jp1,jpo,ilay 
  integer(4)                 :: istat
  !
  !     This subroutine renumbers the points based on a variation of King strategy
  !
  !
  !     Allocate help arrays
  !
  allocate(lstack(npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'LSTACK','renupo',lstack)
  allocate(lstack2(npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'LSTACK2','renupo',lstack2)
  allocate(ldeg(npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'LDEG','renupo',ldeg)
  allocate(lpnt(2,npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'LPNT','renupo',lpnt)
  !
  !     Find a diameter
  !
  call rennod_finddiam(npoin,ndim,coor,ptopo1,ptopo2,lrenn,lstack,nstack,&
       jpmax)
  !
  !     Fill lstack2 from jpmax
  !
  nstack0=1_ip
  nstack1=1_ip
  ilay=1_ip
  lstack2(1)=jpmax

  do 
     !
     !     Update layer number
     !
     ilay=ilay+1

     do istack=nstack0,nstack1 

        ipoin=lstack2(istack)
        !
        !     Loop on elements surrounding points
        !
        do ipo=ptopo2(ipoin),ptopo2(ipoin+1)-1
           ip1=ptopo1(ipo)
           if(lstack(ip1)==0)then
              !
              !     Has this point been marked before?
              !
              lstack(ip1)=ilay
              nstack=nstack+1
              lstack2(nstack)=ip1
           endif
        enddo
     enddo
     !
     !     Are there still points to renumber
     !
     if(nstack==nstack1)exit
     !
     !     Update nstack0 and nstack1    
     !
     nstack0=nstack1+1_ip
     nstack1=nstack 

  enddo
  !
  !     Reverse lstack2
  !
  do ipoin=1,npoin
     lstack2(ipoin)=ilay-lstack2(ipoin)+1_ip
  enddo
  !
  !     Compute the degree
  ! 
  nhash=0_ip 
  do ipoin=1,npoin
     !
     !     Add one to the degree to avoid degenerate situation for the last point
     !
     ldeg(ipoin)=ptopo2(ipoin+1)-ptopo2(ipoin)+1
     !
     !     Find max degree
     !  
     if(ldeg(ipoin)>nhash)nhash=ldeg(ipoin)

  enddo
  !
  !     Allocate lhash
  !
  allocate(lhash(2,nhash),stat=istat)
  call memchk(zero,istat,memor_msh,'LHASH','renupo',lhash)
  !
  !     Initialize nrenu
  !
  nrenu=0_ip
  !
  !     Loop on connex component
  !
  do
     !
     !     Retrieve ipmax
     ! 
     nrenu=nrenu+1_ip
     ipmax=lstack(nrenu)
     !
     !     Clear lstack as it will be used to keep track of insertion order in stack
     !
     lstack(nrenu)=0_ip
     !
     !    Set ipmax as the first point in stack
     !
     lstack(ipmax)=nrenu
     !
     !     Renumber ipmax
     ! 
     lrenn(ipmax)=nrenu
     !
     !     Set minimum degree to maximum +1
     ! 
     idegmin=nhash+1
     !
     !     Add the neighbors of ipmax as a first set
     ! 
     do ipo=ptopo2(ipmax),ptopo2(ipmax+1)-1
        ip1=ptopo1(ipo)
        !
        !     Lower the degree for ipmax
        !
        ldeg(ip1)=ldeg(ip1)-1_ip
        !
        !     Put it in the table
        !
        call rennod_addhash(npoin,idegmin,lhash,nhash,lpnt,ip1,ldeg,lstack)
        !
        !     Mark the point as active
        !
        lrenn(ip1)=-1_ip
        !
        !     Update nstack
        !
        nstack=nstack+1_ip
        !
        !     Remember the position in stack for sorting inside hashing
        !
        lstack(ip1)=lstack2(ip1)
        !
        !     Loop on surrounding points of ip1
        !
        do jpo=ptopo2(ip1),ptopo2(ip1+1)-1
           jp1=ptopo1(jpo)
           !
           !     Has the point been already touched?
           !
           if(lrenn(jp1)==0)then
              !
              !     Only lower its degree
              !
              ldeg(jp1)=ldeg(jp1)-1_ip 
              !
              !     Is the point in the hash table?
              ! 
           else if(lrenn(jp1)<0)then
              !
              !     Remove the point from the hash table
              !
              call rennod_delhash(npoin,idegmin,lhash,nhash,lpnt,jp1,ldeg)
              !
              !     Update the degree
              !
              ldeg(jp1)=ldeg(jp1)-1_ip
              !
              !     Put it back in the table
              !
              call rennod_addhash(npoin,idegmin,lhash,nhash,lpnt,jp1,ldeg,lstack)

           endif

        enddo

     enddo
     !
     !     Renumber 
     !    
     iter=1_ip
     !
     !     Loop on points inside the connex component
     !
     do
        !write(*,*)'iter=',iter
        !iter=iter+1_ip

        !
        !     DBG
        !
        goto 100
        do jpoin=1,npoin
           if(lrenn(jpoin)<1)then
              iloc=1_ip
              do jpo=ptopo2(jpoin),ptopo2(jpoin+1)-1
                 ip1=ptopo1(jpo)
                 if(lrenn(ip1)==0)iloc=iloc+1
              enddo
              if(ldeg(jpoin)/=iloc)then
                 write(*,*)'Error renupoking2'
                 write(*,*)'ipoin=',jpoin
                 stop
              endif
           endif
        enddo
100     continue
        !
        !     Delete the point from the hash table with the lowest degree 
        !     and with the lowest stack position 
        ! 
        call rennod_gthash(npoin,idegmin,lhash,nhash,lpnt,ipoin,ldeg)
        !
        !     DBG
        !       
        if(lrenn(ipoin)/=-1)then
           write(*,*)'Error in renupoking2'
           write(*,*)'ipoin=',ipoin,'lrenn(ipoin)=',lrenn(ipoin)
           stop
        endif
        !
        !     Renumber it 
        !
        nrenu=nrenu+1_ip
        lrenn(ipoin)=nrenu
        !
        !     DBG
        !     
        iloc=1_ip
        !
        !     Update the neighbors and add them to the hash table 
        !  
        do ipo=ptopo2(ipoin),ptopo2(ipoin+1)-1
           ip1=ptopo1(ipo)
           !
           !     Has the point been already touched?
           !
           if(lrenn(ip1)==0)then
              !
              !     Put it in the table
              !
              call rennod_addhash(npoin,idegmin,lhash,nhash,lpnt,ip1,ldeg,lstack)
              !
              !     Get stack position
              !  
              nstack=nstack+1_ip
              lstack(ip1)=lstack2(ip1)
              !
              !     Mark it as active
              !
              lrenn(ip1)=-1_ip
              !
              !     Loop on surrounding points of ip1
              !
              do jpo=ptopo2(ip1),ptopo2(ip1+1)-1
                 jp1=ptopo1(jpo)
                 !
                 !     Has the point been already touched?
                 !
                 if(lrenn(jp1)==0)then
                    !
                    !     Only lower its degree
                    !
                    ldeg(jp1)=ldeg(jp1)-1_ip 
                    !
                    !     Is the point in the hash table?
                    ! 
                 else if(lrenn(jp1)<0)then
                    !
                    !     Remove the point from the hash table
                    !
                    call rennod_delhash(npoin,idegmin,lhash,nhash,lpnt,jp1,ldeg)
                    !
                    !     Update the degree
                    !
                    ldeg(jp1)=ldeg(jp1)-1_ip
                    !
                    !     Put it back in the table
                    !
                    call rennod_addhash(npoin,idegmin,lhash,nhash,lpnt,jp1,ldeg,lstack)

                 endif

              enddo
              !
              !     DBG
              !      
              iloc=iloc+1_ip

           endif
        enddo
        !
        !     DBG
        !
        if(iloc/=ldeg(ipoin))then
           write(*,*)'Error in repoking2 iloc'
           write(*,*)iloc,ldeg(ipoin)
           stop  
        endif
        !
        !     Are there any point left?
        !
        if(idegmin>nhash)exit

     enddo

     if(nrenu/=npoin)then
        !
        !     Find a new connex component
        !
        idegmin=nhash+1_ip
        ipmin=0_ip
        do ipoin=1,npoin
           if(lrenn(ipoin)==0_ip)then
              if(ldeg(ipoin)<idegmin)then
                 idegmin=ldeg(ipoin)
                 ipmin=ipoin 
              endif
           endif
        enddo


        if(ipmin==0)then
           write(*,*)'error renupoking2'
           do ipoin=1,npoin
              if(lrenn(ipoin)==0)then
                 write(*,*)ipoin
              endif
           enddo
           stop
        endif

        lstack(nrenu+1_ip)=ipmin

     else

        exit

     endif

  enddo


  do ipoin=1,npoin
     lrenn(ipoin)=abs(lrenn(ipoin))
  enddo
  !
  !     Do we want reverse?
  !
  if(irev==1)then
     do ipoin=1,npoin
        lrenn(ipoin)=npoin-lrenn(ipoin)+1_ip
     enddo
  endif

  call memchk(2_ip,istat,memor_msh,'LHASH','renupo',lhash)
  deallocate(lhash,stat=istat)
  if(istat/=0) call memerr(2_ip,'LHASH','renupo',0_ip)
  call memchk(2_ip,istat,memor_msh,'LPNT','renupo',lpnt)
  deallocate(lpnt,stat=istat)
  if(istat/=0) call memerr(2_ip,'LPNT','renupo',0_ip)
  call memchk(2_ip,istat,memor_msh,'LDEG','renupo',ldeg)
  deallocate(ldeg,stat=istat)
  if(istat/=0) call memerr(2_ip,'LDEG','renupo',0_ip)
  call memchk(2_ip,istat,memor_msh,'LSTACK2','renupo',lstack2)
  deallocate(lstack2,stat=istat)
  if(istat/=0) call memerr(2_ip,'LSTACK2','renupo',0_ip)
  call memchk(2_ip,istat,memor_msh,'LSTACK','renupo',lstack)
  deallocate(lstack,stat=istat)
  if(istat/=0) call memerr(2_ip,'LSTACK','renupo',0_ip)

end subroutine rennod_renupoking2

subroutine rennod_addhash(npoin,idegmin,lhash,nhash,lpnt,ipoin,ldeg,lstack)
  use def_kintyp, only       :  ip,rp,lg
  implicit none
  integer(ip),intent(in)    :: npoin,nhash
  integer(ip),intent(in)    :: ldeg(npoin),lstack(npoin)
  integer(ip),intent(inout) :: lhash(2,nhash)
  integer(ip),intent(inout) :: idegmin,lpnt(2,npoin)
  integer(ip)               :: ipoin,ideg,ilast,ilastn   
  !
  !     This subroutine adds a point to the hash layer by also ordering inside
  !     each layer
  !
  ideg=ldeg(ipoin)
  !
  !     Update idegmin
  !
  if(ideg<idegmin)idegmin=ideg
  !
  !     DBG
  !
  if(ideg>nhash .or. ideg<1)then
     write(*,*)'Error hashedg'
     write(*,*)'ideg=',ideg,'nhash=',nhash
     stop 
  endif
  !
  !     Do we have points at this level?
  !
  if(lhash(1,ideg)==0)then

     lhash(1,ideg)=ipoin
     lhash(2,ideg)=ipoin

  else
     !
     !     Order the point inside this layer with respect to lstack
     ! 
     ilastn=lhash(2,ideg)
     !
     !     DBG
     !
     if(lpnt(2,ilastn)/=0)then
        write(*,*)'Error in addhash'
        write(*,*)'ilastn=',ilastn,'lpnt(2,ilastn)=',lpnt(2,ilastn)
        stop
     endif
     !
     !     Is the last place the correct one?
     ! 
     if(lstack(ipoin)>lstack(ilastn))then

        lhash(2,ideg)=ipoin
        lpnt(2,ilastn)=ipoin
        lpnt(1,ipoin)=ilastn

     else 

        ilast=lpnt(1,ilastn)
        !
        !     Find the right place
        !
        do 
           !
           !     Did we reach the end of this layer?
           !
           if(ilast==0)then

              lhash(1,ideg)=ipoin
              lpnt(2,ipoin)=ilastn
              lpnt(1,ilastn)=ipoin
              exit

           endif
           !
           !     Compare the lstack value
           !
           if(lstack(ipoin)<lstack(ilast))then

              ilastn=ilast
              ilast=lpnt(1,ilast)

           else

              lpnt(2,ilast)=ipoin
              lpnt(1,ilastn)=ipoin
              lpnt(1,ipoin)=ilast
              lpnt(2,ipoin)=ilastn
              exit

           endif

        enddo

     endif

  endif

end subroutine rennod_addhash

subroutine rennod_delhash(npoin,idegmin,lhash,nhash,lpnt,ipoin,ldeg)
  use def_kintyp, only       :  ip,rp,lg
  implicit none
  integer(ip),intent(in)    :: npoin,nhash
  integer(ip),intent(in)    :: ldeg(npoin)
  integer(ip),intent(inout) :: lhash(2,nhash)
  integer(ip),intent(inout) :: idegmin,lpnt(2,npoin)
  integer(ip)               :: ipoin,inext,ideg,ilast   
  !
  !     This subroutine deletes ipoin from lhash
  !   
  ideg=ldeg(ipoin)
  !
  !     Is there a last pointer?
  !
  if(lpnt(1,ipoin)==0)then
     !
     !     Is there a next pointer?
     !
     if(lpnt(2,ipoin)==0)then
        !
        !     ipoin was the last point at this level
        !
        lhash(1,ideg)=0_ip
        lhash(2,ideg)=0_ip
        !
        !     Was ipoin the point with minimum degree?
        ! 
        if(ideg==idegmin)then
           !
           !     Find new minimum degree
           !
           do
              if(idegmin==nhash)exit
              idegmin=idegmin+1_ip
              if(lhash(1,idegmin)/=0)exit 
           enddo
        endif

     else       

        inext=lpnt(2,ipoin)
        lhash(1,ideg)=inext
        lpnt(1,inext)=0_ip 

     endif
     !
     !     Is there a next pointer?
     !
  else if(lpnt(2,ipoin)==0)then

     ilast=lpnt(1,ipoin)
     lhash(2,ideg)=ilast
     lpnt(2,ilast)=0_ip     

  else 

     !
     !     Rechain the pointers with the last and previous points
     !
     ilast=lpnt(1,ipoin)
     inext=lpnt(2,ipoin)
     lpnt(2,ilast)=inext
     lpnt(1,inext)=ilast

  endif
  !
  !     Clean up lpnt
  !
  lpnt(1,ipoin)=0_ip 
  lpnt(2,ipoin)=0_ip 
  !
  !     DBG 
  !
  if(lhash(1,ideg)/=0 .and. lhash(2,ideg)==0)then
     write(*,*)'Error in delhash'
     write(*,*)'ideg=',ideg,'ipoin=',ipoin
     stop
  endif


end subroutine rennod_delhash

subroutine rennod_gthash(npoin,idegmin,lhash,nhash,lpnt,ipoin,ldeg)
  use def_kintyp, only       :  ip,rp,lg
  implicit none
  integer(ip),intent(in)    :: npoin,nhash
  integer(ip),intent(in)    :: ldeg(npoin)
  integer(ip),intent(inout) :: lhash(2,nhash),ipoin
  integer(ip),intent(inout) :: idegmin,lpnt(2,npoin)
  integer(ip)               :: inext,ilast,ideg
  !
  !     Get the active point with the minimum degree
  !
  !
  !   DBG
  !
  if(idegmin>nhash)then
     write(*,*)'Error, ipoin=',ipoin
     stop
  endif
  !write(*,*)ipoin,idegmin,nhash


  ipoin=lhash(1,idegmin)
  !
  !     DBG
  !
  ideg=ldeg(ipoin)
  !
  !     DBG
  !
  if(lpnt(1,ipoin)/=0)then
     write(*,*)'Error in gthash'
     stop
  endif
  !
  !     Is there a next pointer
  !
  if(lpnt(2,ipoin)==0)then
     !
     !     ipoin was the last point at this level
     !
     lhash(1,idegmin)=0_ip
     lhash(2,idegmin)=0_ip
     !
     !     Find new minimum degree
     !
     do
        idegmin=idegmin+1_ip
        if(idegmin>nhash)exit
        if(lhash(1,idegmin)/=0)exit 
     enddo

  else
     !
     !     Rechain the pointers with the next point
     !
     inext=lpnt(2,ipoin)
     lhash(1,idegmin)=inext
     lpnt(1,inext)=0_ip

  endif

  !write(*,*)ipoin,idegmin,nhash

end subroutine rennod_gthash

subroutine rennod_finddiam(&
     npoin,ndim,coor,ptopo1,ptopo2,lrenn,lstack,nstack,jpmax)
  use def_kintyp, only     :  ip,rp,lg
  use mod_memchk
  use def_meshin, only     :  memor_msh  
  implicit none
  integer(ip),intent(in)   :: npoin,ndim
  real(rp),intent(in)      :: coor(ndim,npoin)
  integer(ip),intent(in)   :: ptopo1(*),ptopo2(npoin+1)
  integer(ip),intent(inout):: lrenn(npoin),lstack(npoin),nstack,jpmax
  integer(ip),pointer      :: lstack2(:) => null()
  real(rp)                 :: xmax,ymax,zmax
  integer(ip)              :: ipmax,ipoin,istack,iter,ipo,j,jpo 
  integer(ip)              :: ip1,nstack0,nstack1 
  integer(ip)              :: jpoin,jstack,jel,jp1,idepth,jdepth
  integer(ip)              :: nstack2,nstack20,nstack21,jpoinm
  integer(ip)              :: jwidth,jwidthm,jwidthp,iwidth,iwidthm
  integer(ip)              :: jdeg,ideg,kaka
  integer(4)               :: istat
  !
  !     This subroutine finds a point of the diameter of the graph associated to elem
  !     On output, nstack=1, lstack(1)=ipmax and lrenn(ipmax)=1
  !
  allocate(lstack2(npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'LSTACK2','finddiam',lstack2)
  !
  !     Find the geometric max
  ! 
  xmax=coor(1,1)
  ymax=coor(2,1)
  ipmax=1_ip

  if( ndim == 2 ) then

     do ipoin=2,npoin
        if(coor(1,ipoin)>xmax)then
           xmax=coor(1,ipoin)
           ymax=coor(2,ipoin)
           ipmax=ipoin
        endif
     enddo

     do ipoin=1,npoin
        if(coor(1,ipoin)==xmax)then
           if(coor(2,ipoin)>ymax)then
              xmax=coor(1,ipoin)
              ymax=coor(2,ipoin)
              ipmax=ipoin
           endif
        endif
     enddo

  else
     zmax=coor(3,1)

     do ipoin=2,npoin
        if(coor(1,ipoin)>xmax)then
           xmax=coor(1,ipoin)
           ymax=coor(2,ipoin)
           zmax=coor(3,ipoin)
           ipmax=ipoin
        endif
     enddo

     do ipoin=1,npoin
        if(coor(1,ipoin)==xmax)then
           if(coor(2,ipoin)>ymax)then
              xmax=coor(1,ipoin)
              ymax=coor(2,ipoin)
              zmax=coor(3,ipoin)
              ipmax=ipoin
           endif
        endif
     enddo

     do ipoin=1,npoin
        if(coor(1,ipoin)==xmax)then
           if(coor(2,ipoin)==ymax)then
              if(coor(3,ipoin)>zmax)then
                 xmax=coor(1,ipoin)
                 ymax=coor(2,ipoin)
                 zmax=coor(3,ipoin)
                 ipmax=ipoin
              endif
           endif
        endif
     enddo
  end if
  !
  !     Find a diameter (ipmax,jpmax)
  !
  do 
     !
     !     Begin to form the levels from ipmax
     !
     do ipoin=1,npoin
        lrenn(ipoin)=0
     enddo
     lstack(1)=ipmax
     lrenn(ipmax)=1_ip
     nstack=1_ip
     nstack0=1_ip
     nstack1=1_ip
     idepth=1_ip
     iwidth=0_ip
     !
     !     Loop on the depth of the graph
     ! 
     do
        !
        !     Increase depth number
        !
        idepth=idepth+1_ip
        !
        !     Compute width
        !
        iwidthm=nstack1-nstack0+1_ip
        if(iwidthm>iwidth)iwidth=iwidthm 
        !
        !     Loop on the levels
        ! 
        do istack=nstack0,nstack1
           ipoin=lstack(istack)
           !
           !     Loop on points surrounding points
           !
           do ipo=ptopo2(ipoin),ptopo2(ipoin+1)-1
              ip1=ptopo1(ipo)
              if(lrenn(ip1)==0)then
                 !
                 !     Has this point been marked before?
                 !
                 nstack=nstack+1
                 lstack(nstack)=ip1
                 !lrenn(ip1)=lrenn(ipoin)+1_ip
                 lrenn(ip1)=idepth
              endif
           enddo
        enddo
        !
        !     Did we add points to the stack?   
        !
        if(nstack==nstack1)exit
        !
        !     Update nstack0 and nstack1
        !   
        nstack0=nstack1+1
        nstack1=nstack

     enddo
     !
     !     Now we have the points candidates from the last level 
     !
     jwidth=npoin

     do istack=nstack0,min(nstack1,nstack0+50_ip) 

        jpmax=lstack(istack)
        do ipoin=1,npoin
           lrenn(ipoin)=0_ip
        enddo
        lrenn(jpmax)=1_ip 
        lstack2(1)=jpmax
        nstack2=1_ip
        nstack20=1_ip
        nstack21=1_ip
        jdepth=1_ip
        jwidthp=0_ip
        !
        !     Loop on the depth of the graph
        ! 
        do
           !
           !     Increase depth number
           !
           jdepth=jdepth+1_ip
           !
           !     Compute width
           !
           jwidthm=nstack21-nstack20+1_ip
           if(jwidthm>jwidthp)jwidthp=jwidthm
           !
           !     Loop on the levels
           ! 
           do jstack=nstack20,nstack21
              jpoin=lstack2(jstack)
              !
              !     Loop on points surrounding points
              !
              do jpo=ptopo2(jpoin),ptopo2(jpoin+1)-1
                 jp1=ptopo1(jpo)
                 if(lrenn(jp1)==0)then
                    !
                    !     Has this point been marked before?
                    !
                    nstack2=nstack2+1
                    lstack2(nstack2)=jp1
                    lrenn(jp1)=jdepth
                 endif
              enddo
           enddo
           !
           !     Did we add points to the stack?   
           !
           if(nstack2==nstack21)exit
           !
           !     Update nstack0 and nstack1
           !   
           nstack20=nstack21+1
           nstack21=nstack2

        enddo
        !
        !     Compare the length of the graph
        !
        if(jdepth>idepth)then
           ipmax=jpmax 
           goto 100 
        endif
        !
        !     Remember the maximal width for this range of points
        ! 
        if(jwidthp<jwidth)then
           jwidth=jwidthp  
           jpoinm=jpmax
        endif

     enddo
     !
     !     We have found a pseudo diameter (ipmax,jpoin)
     !
     exit

100  continue           

  enddo

  !call outgidpnt(npoin,ndim,elem,nelem,nnode,coor,lrenn)

  jpmax=jpoinm
  !
  !     Check the width
  !
  !if(jwidth<iwidth)then
  !   jpmax=ipmax
  !   ipmax=jpoinm 
  !endif 
  !
  !     Sum up
  !
  nstack=1_ip
  lstack(1)=ipmax
  do ipoin=1,npoin
     lrenn(ipoin)=0_ip
  enddo
  lrenn(ipmax)=1_ip

  !write(*,*)'New graph depth=',idepth
  !write(*,*)'New graph width=',iwidth,jwidth


  call memchk(2_ip,istat,memor_msh,'LSTACK2','finddiam',lstack2)
  deallocate(lstack2,stat=istat)
  if(istat/=0) call memerr(2_ip,'LSTACK2','finddiam',0_ip)

end subroutine rennod_finddiam

subroutine rennod_renufront(npoin,ndim,coor,ptopo1,ptopo2,irev,lrenu)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_meshin, only          : memor_msh  
  implicit none
  integer(ip),intent(in)   :: npoin,ndim,irev
  integer(ip),intent(inout):: lrenu(npoin)
  real(rp),intent(inout)   :: coor(ndim,npoin)
  integer(ip),intent(in)   :: ptopo1(*),ptopo2(npoin+1)
  integer(ip),pointer      :: lstack(:) => null()
  real(rp)                 :: xmax,ymax,zmax
  integer(ip)              :: ielem,j,ip1,ipnew,iface,jpmax 
  integer(ip)              :: ipmax,ipoin,istack,nstack,iter,ipo 
  integer(4)               :: istat
  !
  !     This subroutine renumbers the points based on an advancing front like renumbering
  !
  !
  !     Allocate help arrays
  !
  allocate(lstack(npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'LSTACK','renupo',lstack)
  !
  !     Find a diameter
  !
  call rennod_finddiam(npoin,ndim,coor,ptopo1,ptopo2,lrenu,lstack,nstack,&
       jpmax)
  !
  !     Renumber the points
  !
  istack=0_ip

  do 
     if(istack==nstack)exit
     istack=istack+1
     ipoin=lstack(istack)
     !
     !     Loop on elements surrounding points
     !
     do ipo=ptopo2(ipoin),ptopo2(ipoin+1)-1
        !
        !     Loop on nodes of elements
        !
        ip1=ptopo1(ipo)
        if(lrenu(ip1)==0)then
           !
           !     Has this point been marked before?
           !
           nstack=nstack+1
           lrenu(ip1)=nstack
           lstack(nstack)=ip1
        endif
     enddo

  enddo

  if(istack/=npoin)then
     write(*,*)'error renupo'
     stop
  endif
  !
  !     Do we want reverse?
  !
  if(irev==1)then
     do ipoin=1,npoin
        lrenu(ipoin)=npoin-lrenu(ipoin)+1_ip
     enddo
  endif

  call memchk(2_ip,istat,memor_msh,'LSTACK','renupo',lstack)
  deallocate(lstack,stat=istat)
  if(istat/=0) call memerr(2_ip,'LSTACK','renupo',0_ip)

end subroutine rennod_renufront

