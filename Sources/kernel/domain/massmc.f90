subroutine massmc(itask)
  !-----------------------------------------------------------------------
  !****f* domain/massmc
  ! NAME
  !    massmc
  ! DESCRIPTION
  !    This routines calculates the diagonal mass matrix using a 
  !    closed rule
  ! OUTPUT
  !    VMASC(NPOIN) : Diagonal mass matrix
  ! USED BY
  !    Domain
  !*** 
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_elmtyp
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,inode,igaus,ielem,knode
  integer(ip)             :: pgaus,pnode,pelty,ichkm,pdime
  real(rp)                :: elcod(ndime,mnode)
  real(rp)                :: elmas(mnode)
  real(rp)                :: gpdet,gpvol,xjacm(9)
  real(rp)                :: numer(8),aux,time1,time2
  character(20)           :: chnod

#ifdef EVENT
  call mpitrace_user_function(1)
#endif
  call cputim(time1)

  call livinf(0_ip,'COMPUTE CLOSED MASS MATRIX',0_ip)

  if( INOTMASTER ) then
     !
     ! Allocate memory
     ! 
     if( itask == 1 ) then
        call memgeo(33_ip)
     else
        do ipoin = 1,npoin
           vmasc(ipoin) = 0.0_rp
        end do
     end if
     !
     ! Loop over elements
     !
     if( ( ( ndime == 3 .and. lexis(PYR05) == 0 ) .or. ndime == 2 ) .and. kfl_naxis == 0 .and. kfl_spher == 0 ) then
        !
        ! 3D: Avoid if and go fast
        !
        !$OMP  PARALLEL DO SCHEDULE (GUIDED)                                     & 
        !$OMP  DEFAULT (NONE)                                                    &
        !$OMP  PRIVATE ( aux, elcod, elmas, gpdet, gpvol, xjacm, ielem, igaus,   &
        !$OMP            inode, ipoin, numer, pelty, pgaus, pnode, pdime )       &
        !$OMP  SHARED  ( coord, elmar, lnods, ltype, ndime, nelem, ngaus, nnode, &
        !$OMP            vmasc, kfl_naxis, kfl_spher ,lelch, ldime )
        !
        do ielem = 1,nelem
           pelty = ltype(ielem) 

           if( pelty > 0 ) then

              pdime = ldime(pelty)
              pnode = nnode(pelty)
              do inode = 1,pnode
                 ipoin = lnods(inode,ielem)
                 elcod(1:ndime,inode) = coord(1:ndime,ipoin)
              end do
              if( lelch(ielem) == ELEXT ) then
                 inode = 1
                 call jacdet(&
                      pdime,pnode,elcod,elmar(pelty)%deric(1,1,inode),&
                      xjacm,gpdet)
                 elmas(inode) = elmar(pelty)%weigc(inode)*gpdet
                 ipoin = lnods(inode,ielem)
                 !$OMP ATOMIC
                 vmasc(ipoin) = vmasc(ipoin) + elmas(inode)
              else 
                 do inode = 1,pnode
                    call jacdet(&
                         pdime,pnode,elcod,elmar(pelty)%deric(1,1,inode),&
                         xjacm,gpdet)
                    elmas(inode) = elmar(pelty)%weigc(inode)*gpdet
                 end do
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    !$OMP ATOMIC
                    vmasc(ipoin) = vmasc(ipoin) + elmas(inode)
                 end do
              end if
           end if

        end do

     else
        !   
        !$OMP  PARALLEL DO SCHEDULE (GUIDED)                                     & 
        !$OMP  DEFAULT (NONE)                                                    &
        !$OMP  PRIVATE ( aux,   elcod, elmas, gpdet, gpvol,                      &
        !$OMP            ielem, igaus, inode, ipoin, numer, pelty, pgaus, pnode, &
        !$OMP            xjacm, knode, pdime )                                   &
        !$OMP  SHARED  ( coord, elmar, lnods, ltype, ndime, nelem, ngaus, nnode, &
        !$OMP            vmasc, kfl_naxis, kfl_spher, lelch, ldime )
        !
        do ielem = 1,nelem
           pelty = ltype(ielem) 
           if( pelty > 0 ) then
              pdime = ldime(pelty)
              pnode = nnode(pelty)
              if( ndime == 1 ) then
                 do inode=1,pnode
                    ipoin          = lnods(inode,ielem)
                    elcod(1,inode) = coord(1,ipoin) 
                 end do
              else if( ndime == 2 ) then
                 do inode=1,pnode
                    ipoin          = lnods(inode,ielem)
                    elcod(1,inode) = coord(1,ipoin)
                    elcod(2,inode) = coord(2,ipoin)
                 end do
              else
                 do inode = 1,pnode
                    ipoin          = lnods(inode,ielem)
                    elcod(1,inode) = coord(1,ipoin)
                    elcod(2,inode) = coord(2,ipoin)
                    elcod(3,inode) = coord(3,ipoin)
                 end do
              end if
              !
              ! Treat extension elements
              !
              if( lelch(ielem) == ELEXT ) then
                 knode = 1
              else
                 knode = pnode
              end if

              if( pelty == PYR05 ) then
                 !
                 ! PYR05: Pyramid element
                 !
                 pgaus = ngaus(pelty)

                 do inode = 1,pnode
                    numer(inode) = 0.0_rp
                 end do

                 do igaus = 1,pgaus
                    call jacdet(&
                         pdime,pnode,elcod,elmar(pelty)%deriv(1,1,igaus),&
                         xjacm,gpdet)
                    gpvol = elmar(pelty) % weigp(igaus) * gpdet
                    do inode = 1,knode
                       numer(inode) = numer(inode) + gpvol * elmar(pelty) % shape(inode,igaus)
                    end do
                 end do

                 do inode = 1,knode
                    ipoin        = lnods(inode,ielem)
                    aux          = numer(inode)
                    !$OMP         ATOMIC
                    vmasc(ipoin) = vmasc(ipoin) + aux
                 end do

              else
                 !
                 ! Other elements: loop over Gauss points (which are nodes)
                 !
                 if( kfl_naxis == 1 ) then

                    do inode=1,pnode
                       ipoin=lnods(inode,ielem)
                       call jacdet(&
                            pdime,pnode,elcod,elmar(pelty)%deric(1,1,inode),&
                            xjacm,gpdet)
                       gpvol=elmar(pelty)%weigc(inode)*gpdet
                       if( elcod(1,inode) == 0.0_rp ) then
                          gpvol = gpvol * twopi * 1.0e-12
                       else
                          gpvol = gpvol * twopi * elcod(1,inode)
                       end if
                       !$OMP ATOMIC             
                       vmasc(ipoin) = vmasc(ipoin) + gpvol
                    end do

                 else if( kfl_spher == 1 ) then

                    do inode=1,pnode
                       ipoin=lnods(inode,ielem)
                       call jacdet(&
                            pdime,pnode,elcod,elmar(pelty)%deric(1,1,inode),&
                            xjacm,gpdet)
                       gpvol=elmar(pelty)%weigc(inode)*gpdet
                       if( elcod(1,inode) == 0.0_rp ) then
                          gpvol = gpvol * twopi * 1.0e-12
                       else
                          gpvol = gpvol * 2.0_rp * twopi * elcod(1,inode) * elcod(1,inode)
                       end if
                       !$OMP ATOMIC             
                       vmasc(ipoin) = vmasc(ipoin) + gpvol
                    end do

                 else

                    do inode = 1,knode
                       ipoin = lnods(inode,ielem)
                       call jacdet(&
                            pdime,pnode,elcod,elmar(pelty)%deric(1,1,inode),&
                            xjacm,gpdet)
                       elmas(inode) = elmar(pelty)%weigc(inode)*gpdet
                    end do
                    do inode = 1,knode
                       ipoin = lnods(inode,ielem)
                       !$OMP ATOMIC             
                       vmasc(ipoin) = vmasc(ipoin) + elmas(inode)
                    end do
                 end if

              end if
           end if
        end do
     end if
     !
     ! Modify RHS due to periodicity and Parall service
     !
     call rhsmod(1_ip,vmasc)
     !
     ! Hanging nodes
     ! 
     if( nhang > 0 ) then
        if( IMASTER ) call runend('MASSMC: NOT PROGRAMMED')
        !do ihang=1,nhang
        !   ipoin=lhang(1,ihang)
        !   xfact=1.0_rp/(real(lhang(0,ihang))-1.0_rp)
        !   do jnode=2,lhang(0,ihang)
        !      jpoin=lhang(jnode,ihang)
        !      vmasc(ipoin)=vmasc(ipoin)+xfact*vmasc(jpoin)
        !   end do
        !end do
     end if
     !
     ! Loop over nodes to control zero-volume points
     !
     if( kfl_horde == 0 ) then
        ichkm = 0
        aux   = epsilon(1.0_rp)
        nodes: do ipoin = 1,npoin
           if( lnoch(ipoin) == NOHOL ) then
              vmasc(ipoin) = 1.0_rp
           else if( vmasc(ipoin) < aux ) then
              ichkm = 1
              chnod = intost(ipoin)
              write(*,*)'massmc:lninv_loc(ipoin),vmasc(ipoin),aux',lninv_loc(ipoin),vmasc(ipoin),aux
              call livinf(10000_ip, &
                   'NODE NUMBER ( '//adjustl(trim(chnod)) &
                   // ' HAS ZERO MASS)',0_ip)   
              vmasc(ipoin) = 1.0_rp
           end if
        end do nodes

!        if( ichkm == 1 ) call runend('MASSMC: ZERO MASS NODES FOUND')
     else

     end if

  end if
  !
  ! If there are quadratic elements, use always lumped matrix
  !
  if( kfl_horde /= 0 ) then
     if( kfl_grpro == 1 ) then
        call outfor(2_ip,0_ip,'THERE ARE QUADRATIC ELEMENT: USE ALWAYS LUMPED MATRIX')
        kfl_grpro = 0        
     end if
  end if

  !-------------------------------------------------------------------
  !
  ! Fill in mesh type
  !
  !-------------------------------------------------------------------

  meshe(ndivi) % vmasc => vmasc

#ifdef EVENT
  call mpitrace_user_function(0)
#endif
  call cputim(time2)

end subroutine massmc
