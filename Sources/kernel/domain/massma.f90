subroutine massma(itask)
  !-----------------------------------------------------------------------
  !****f* domain/massma
  ! NAME
  !    massma
  ! DESCRIPTION
  !    This routines calculates the diagonal mass matrix using a 
  !    closed rule
  ! OUTPUT
  !    VMASS(NPOIN) : Diagonal mass matrix
  ! USED BY
  !    Domain
  !*** 
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_elmtyp
  use mod_memchk
use mod_parall, only : PAR_NODE_NUMBER_OF_PARTITIONS
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,inode,igaus,ielem,pdime
  integer(ip)             :: pgaus,pnode,pelty,ichkm,ihang,jnode,jpoin
  real(rp)                :: elcod(ndime,mnode),gpdet,gpvol,xjacm(9)
  real(rp)                :: numer(mnode),xfact,aux,time1,time2,T,d
  character(15)           :: chnod
#ifdef EVENT
  call mpitrace_user_function(1)
#endif

  call cputim(time1)

  call livinf(0_ip,'COMPUTE LUMPED MASS MATRIX',0_ip)

  if( INOTMASTER ) then
     !
     ! Allocate memory
     ! 
     if( itask == 1 ) then
        call memgeo(32_ip)
     else
        do ipoin = 1,npoin
           vmass(ipoin) = 0.0_rp
        end do
     end if
     !
     ! Loop over elements
     !
     if( kfl_naxis == 0 .and. kfl_spher == 0 ) then

        if( kfl_horde == 0 ) then
           !
           ! Only linear elements
           !
           !$OMP  PARALLEL DO SCHEDULE (GUIDED)                                     & 
           !$OMP  DEFAULT (NONE)                                                    &
           !$OMP  PRIVATE ( aux, elcod, gpdet, gpvol,                               &
           !$OMP            ielem, igaus, inode, ipoin, numer, pelty, pgaus, pnode, &
           !$OMP            xjacm, xfact, pdime )                                   &
           !$OMP  SHARED  ( coord, elmar, lnods, ltype, ndime, nelem, ngaus, nnode, &
           !$OMP            vmass, kfl_naxis, kfl_spher , lelch, ldime )
           do ielem = 1,nelem
              !
              ! Element properties and dimensions
              !
              pelty = ltype(ielem) 
              if( pelty > 0 ) then

                 pdime = ldime(pelty)
                 pnode = nnode(pelty)
                 pgaus = ngaus(pelty)
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    elcod(1:ndime,inode) = coord(1:ndime,ipoin)
                 end do

                 !if( pelty == PYR05 ) then
                 !    !
                 !    ! Pyramid element: Hinton strategy
                 !    !
                 !    pgaus = ngaus(pelty)
                 !    volum = 0.0_rp
                 !    denom = 0.0_rp
                 !    do inode=1,pnode
                 !       numer(inode)=0.0_rp
                 !       do igaus=1,pgaus
                 !          call jacdet(&
                 !               ndime,pnode,elcod,elmar(pelty)%deriv(1,1,igaus),&
                 !               xjacm,gpdet)
                 !          xfact = elmar(pelty)%weigp(igaus) * gpdet * elmar(pelty)%shape(inode,igaus)
                 !          volum = volum + xfact
                 !          denom = denom + xfact * elmar(pelty)%shape(inode,igaus)
                 !          numer(inode)=numer(inode) + xfact * elmar(pelty)%shape(inode,igaus)
                 !       end do
                 !    end do
                 !    xfact=volum/denom
                 !    do inode=1,pnode
                 !       ipoin        = lnods(inode,ielem)
                 !       aux          = numer(inode)*xfact
                 !       !*OMP         ATOMIC
                 !       vmass(ipoin) = vmass(ipoin)+aux
                 !    end do
                 !else

                 do inode = 1,pnode
                    numer(inode) = 0.0_rp
                 end do

                 do igaus = 1,pgaus
                    call jacdet(&
                         pdime,pnode,elcod,elmar(pelty) % deriv(1,1,igaus),&
                         xjacm,gpdet)
                    gpvol = elmar(pelty) % weigp(igaus) * gpdet
                    do inode = 1,pnode
                       numer(inode) = numer(inode) &
                            + gpvol * elmar(pelty) % shape(inode,igaus)

                    end do
                 end do

                 if( lelch(ielem) == ELEXT ) then
                    inode        = 1
                    ipoin        = lnods(inode,ielem)
                    aux          = numer(inode)
                    !$OMP         ATOMIC
                    vmass(ipoin) = vmass(ipoin) + aux
                 else
                    do inode = 1,pnode           
                       ipoin        = lnods(inode,ielem)
                       aux          = numer(inode)
                       !$OMP         ATOMIC
                       vmass(ipoin) = vmass(ipoin) + aux
                    end do
                 end if

              end if

           end do
           
        else
           !
           ! There are quadratic elements
           !
           !$OMP  PARALLEL DO SCHEDULE (GUIDED)                                     & 
           !$OMP  DEFAULT (NONE)                                                    &
           !$OMP  PRIVATE ( aux,   elcod, gpdet, gpvol,                             &
           !$OMP            ielem, igaus, inode, ipoin, numer, pelty, pgaus, pnode, &
           !$OMP            xjacm, xfact, t, D, pdime )                             &
           !$OMP  SHARED  ( coord, elmar, lnods, ltype, ndime, nelem, ngaus, nnode, &
           !$OMP            vmass, kfl_naxis, kfl_spher ,lelch, lorde, ldime )
           do ielem = 1,nelem

              pelty = ltype(ielem) 

              if( pelty > 0 ) then

                 pdime = ldime(pelty)
                 pnode = nnode(pelty)
                 pgaus = ngaus(pelty)
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    elcod(1:ndime,inode) = coord(1:ndime,ipoin) 
                 end do

                 if( lorde(pelty) == 1 ) then
                    do inode = 1,pnode
                       numer(inode) = 0.0_rp
                    end do
                    do igaus = 1,pgaus
                       call jacdet(&
                            pdime,pnode,elcod,elmar(pelty)%deriv(1,1,igaus),&
                            xjacm,gpdet)
                       gpvol = elmar(pelty)%weigp(igaus) * gpdet
                       do inode = 1,pnode                       
                          numer(inode) = numer(inode) &
                               + gpvol * elmar(pelty)%shape(inode,igaus)
                       end do
                    end do
                    if( lelch(ielem) == ELEXT ) then
                       inode        = 1
                       ipoin        = lnods(inode,ielem)
                       aux          = numer(inode)
                       !$OMP         ATOMIC
                       vmass(ipoin) = vmass(ipoin) + aux
                    else
                       do inode = 1,pnode           
                          ipoin        = lnods(inode,ielem)
                          aux          = numer(inode)
                          !$OMP         ATOMIC
                          vmass(ipoin) = vmass(ipoin) + aux
                       end do
                    end if

                 else
                    !
                    ! This is a more than linear element: Use D-matrix
                    !
                    do inode = 1,pnode
                       numer(inode) = 0.0_rp
                    end do
                    T = 0.0_rp
                    d = 0.0_rp
                    do igaus = 1,pgaus
                       call jacdet(&
                            pdime,pnode,elcod,elmar(pelty)%deriv(1,1,igaus),&
                            xjacm,gpdet)
                       gpvol = elmar(pelty)%weigp(igaus) * gpdet
                       T     = T + gpvol
                       do inode = 1,pnode      
                          numer(inode) = numer(inode) &
                               + gpvol &
                               * elmar(pelty)%shape(inode,igaus) &
                               * elmar(pelty)%shape(inode,igaus)
                       end do
                    end do
                    do inode = 1,pnode
                       d = d + numer(inode)
                    end do
                    d = 1.0_rp / d
                    do inode = 1,pnode
                       numer(inode) = numer(inode) * T * d
                    end do
                    if( lelch(ielem) == ELEXT ) then
                       inode        = 1
                       ipoin        = lnods(inode,ielem)
                       aux          = numer(inode)
                       !$OMP         ATOMIC
                       vmass(ipoin) = vmass(ipoin) + aux
                    else
                       do inode = 1,pnode           
                          ipoin        = lnods(inode,ielem)
                          aux          = numer(inode)
                          !$OMP         ATOMIC
                          vmass(ipoin) = vmass(ipoin) + aux
                       end do
                    end if

                 end if

              end if

           end do

        end if

     else 

        write(6,*) 'SPHERICAL COORDINATES'
        call runend('LUMPED MASS MATRIX NOT CODED IN NON-CARTESIAN COORDINATE SYSTEM')

     end if
     !
     ! Modify RHS due to periodicity and Parall service
     !
     call rhsmod(1_ip,vmass)
     !
     ! Hanging nodes
     ! 
     if( nhang > 0 ) then
        if( IMASTER ) call runend('MASSMA: NOT PROGRAMMED')
        do ihang=1,nhang
           !ipoin=lhang(1,ihang)
           !xfact=1.0_rp/(real(lhang(0,ihang))-1.0_rp)
           !do jnode=2,lhang(0,ihang)
           !   jpoin=lhang(jnode,ihang)
           !   vmass(ipoin)=vmass(ipoin)+xfact*vmass(jpoin)
           !end do
        end do
     end if
     !
     ! Loop over nodes to control zero-volume points
     !
     ichkm = 0

     nodes: do ipoin = 1,npoin
        if( lnoch(ipoin) == NOHOL ) then
           vmass(ipoin) = 1.0_rp
        else if( vmass(ipoin) < zeror ) then
           ichkm = ichkm + 1
           chnod = intost(ipoin)
           !write(6,*) 'pipi',vmass(ipoin),zeror
           write(6,'(a,i10,1(1x,e12.6))') 'MASSMA: lninv_loc(ipoin),vmass(ipoin)= ',lninv_loc(ipoin),vmass(ipoin)
           call livinf(10000_ip, &
                'NODE NUMBER ( '//adjustl(trim(chnod)) &
                // ' HAS ZERO MASS)',0_ip)      
           vmass(ipoin) = 1.0_rp
        end if
     end do nodes
     
     if( ichkm > 0 ) then
     !   call runend('MASSMA: ZERO MASS NODES FOUND')
     end if
     
  end if

  !-------------------------------------------------------------------
  !
  ! Fill in mesh type
  !
  !-------------------------------------------------------------------

  meshe(ndivi) % vmass => vmass

#ifdef EVENT
  call mpitrace_user_function(0)
#endif
  call cputim(time2)

end subroutine massma
