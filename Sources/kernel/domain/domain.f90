!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    domain.f90
!> @author  Guillaume Houzeaux
!> @brief   Domain construction
!> @details Perform the operations 
!>          needed to build up the domain data for the run.
!>          All the arrays computed here only depend on the kernel 
!>          requierements. Other arrays required e.g. by the modules are
!>          computed later on in Turnon.
!>          -  Subdivide mesh
!>          - Define domain variables
!>          - Compute shape functions & derivatives
!>
!> @} 
!-----------------------------------------------------------------------
subroutine domain()

  use def_elmtyp
  use def_master
  use def_kermod
  use def_domain

  use mod_parall
  use mod_ghost_geometry,   only : par_ghost_geometry
  use mod_ADR,              only : ADR_load_mesh

  use mod_commdom_alya,     only: CPLNG, CHT, CC
#ifdef COMMDOM
  use mod_commdom_plepp,    only: commdom_plepp_set_mesh 
#if   COMMDOM==1
  use mod_commdom_alya_cht, only: CHT_CPLNG, commdom_alya_init_cht, commdom_cht_memall 
#elif COMMDOM==2
  use mod_commdom_driver,   only: commdom_driver_init_contact
  use mod_commdom_driver,   only: commdom_driver_init_cht 
  use mod_commdom_dynamic,  only: commdom_dynamic_set_mesh
  use mod_commdom_alya,     only: commdom_alya_memall
  use mod_commdom_driver,   only: CNT_CPLNG
#elif COMMDOM==3
  use mod_commdom_lmc,      only: LMC_CPLNG, commdom_lmc_init, commdom_lmc_memall
#elif COMMDOM==4
  use mod_commdom_cc,       only: commdom_cc_init, commdom_cc_memall
#endif 
#endif

#ifdef PRECICE  
  use mod_precice,         only : precice_create, precice_initialize
  use mod_precice_driver,  only : PRCC  
#endif

  implicit none
  real(rp) :: time1,time2,time3

  !----------------------------------------------------------------------
  !
  ! From now on, Master does not have any mesh info
  !
  !----------------------------------------------------------------------
  !
  ! Change coordinate system
  !
  call domvar(0_ip)                        ! COORD 
  !
  ! Zones and subdomains
  !
  call in_zone_in_subd()
  !
  ! Compute parallel communicators
  !
#ifdef COMMDOM
#if   COMMDOM==1
  call commdom_alya_init_cht(CHT_CPLNG)
#elif COMMDOM==2
  call commdom_driver_init_contact( CNT_CPLNG )
  !call commdom_driver_init_cht( CNT_CPLNG )
#elif COMMDOM==3
  call commdom_lmc_init(LMC_CPLNG)
#elif COMMDOM==4
  call commdom_cc_init( CPLNG(CC) )
#endif
#endif 
  !
#ifdef PRECICE  
  call precice_create( PRCC )
#endif
  !
  call par_color_communicators()
  call par_bin_structure()
  !
  ! Domain variables
  !
  call domvar(1_ip)                        ! LFACE 
  call domvar(2_ip)                        ! LNUTY, LTYPF, NNODF
  !
  ! Subdivide mesh
  !
  call cputim(time1) 
  call submsh()
  call cputim(time2)
  cpu_start(CPU_MESH_MULTIPLICATION) = time2 - time1
  !
  ! Renumber elements
  !
  if( IPARALL ) call renelm()
  !
  ! Domain variables depending on mesh
  !
  call domvar(3_ip)                        ! LPOIZ, MECNT: mesh must have been renumbered/contacts
  !
  ! Groups of deflated: 1 group / subdomain
  !
  call grodom(2_ip)                        ! LGROU_DOM
  !
  ! Compute shape functions & derivatives
  !
  call cshder(3_ip)                        ! SHAPE, DERIV, HESLO, WEIGP...116
  !
  ! Save element data base
  !
  call savdom()
  !
  ! Turn on the remeshing algorithm
  !
  if( kfl_algor_msh > 0 ) call Newmsh(0_ip)
  !
  ! Create mesh graph
  !
  if( INOTMASTER ) then
     if( ISLAVE .or. ( ISEQUEN .and.ndivi > 0 ) ) then  
        call domgra()                      ! R_DOM, C_DOM, NZDOM
        meshe(ndivi) % r_dom => r_dom      ! Point to mesh type
        meshe(ndivi) % c_dom => c_dom      ! Point to mesh type
     end if
  end if
  !
  ! Color elements for OMP
  !
  call par_omp_coloring(meshe(ndivi),mepoi,pelpo,lelpo)
  !
  ! Size of blocks and chunk sizes for OpenMP
  !
  call par_omp_chunk_sizes(meshe(ndivi)) 
  !
  ! Extended graph
  !
  if( kfl_graph == 1 ) call par_extgra()   ! R_DOM, C_DOM, NZDOM
  !
  ! Periodicity and sym. graph dimensions
  !
  call cresla()                            ! NZSYM
  !
  ! Element to csr
  !
  call elecsr()
  !
  ! Compute boundary connectivity 
  !
  call setlbe()                            ! LBOEL
  !
  ! Check mesh
  !
  call mescek(1_ip)
  !
  ! Materials
  !
  call materi()                            ! LMATN
  !
  ! Prepare voxels postprocess
  !
  call prevox()
  !
  ! Parall: Fringe geometry
  !
  call par_ghost_geometry()
  !
  ! Parall communication arrays depending on zones
  !
  call Parall(608_ip)
  call par_zone_communication_arrays()     ! PAR_COMM_COLOR_ARRAY
  !
  ! Initialize Elsest
  !
  call elsini()
  !
  ! Witness point information
  !
  call witnes()                            ! LEWIT, SHWIT
  !
  ! Coupling
  !
#ifdef COMMDOM
  !
#if   COMMDOM==1
  call commdom_plepp_set_mesh(CHT_CPLNG%current_fixbo, CHT_CPLNG%n_dof) 
#elif COMMDOM==2
  call commdom_dynamic_set_mesh(CNT_CPLNG%current_fixbo, CNT_CPLNG%n_dof)
#elif COMMDOM==3
  call commdom_plepp_set_mesh(LMC_CPLNG%current_fixbo, LMC_CPLNG%n_dof)
#elif COMMDOM==4
  call commdom_plepp_set_mesh( CPLNG(CC)%current_fixbo,  CPLNG(CC)%n_dof)
#endif 
#if   COMMDOM==1
  call commdom_cht_memall(CHT_CPLNG)
#elif COMMDOM==2
  call commdom_alya_memall( CNT_CPLNG )
#elif COMMDOM==3
  call commdom_lmc_memall(LMC_CPLNG)
#elif COMMDOM==4
  call commdom_cc_memall( CPLNG(CC) )
#endif 
  !
#elif PRECICE  
  call precice_initialize( PRCC )
  !
#else
  !
  call cou_turnon()
  call cou_define_wet_geometry()           ! COUPLING_TYPE
  call cou_initialize_coupling()           ! COUPLING_TYPE
  call cou_communication_arrays()
  call cou_output()
  !
#endif 
  !
  ! Output domain mesh
  !
  call outdom(1_ip)
  !
  ! Output partitioning and coupling
  !
  call par_output_partition() 
  ! 
  ! Compute mesh dependent variables
  !
  call domarr(1_ip)                        ! VMASS, VMASC, EXNOR, YWALP, YWALB, WALLD, SKCOS, COORD
  !
  ! Operations on boundary conditions
  !
  call opebcs(1_ip)
  !
  ! Modify code if mesh subdivision has been used
  !
  call codbcs()
  !
  ! Geometrical normals and check codes
  !
  call opebcs(2_ip)
  call chkcod() 
  !
  ! Load mesh array and parameters for ADR toolbox
  !
  call ADR_load_mesh(meshe(ndivi:ndivi),elmar)
  !
  ! Close domain unit
  ! 
  call opfdom(3_ip) 

  call cputim(time3)
  cpu_start(CPU_CONSTRUCT_DOMAIN) = time3 - time2

end subroutine domain

subroutine test()
  use def_kintyp
  use mod_elmgeo
  implicit none
  real(rp)    :: xx1(3),xx2(3)
  real(rp)    :: bocod(3,4)
  real(rp)    :: intersection(3)
  real(rp)    :: toler
  integer(ip) :: ifoun

  xx1 = (/ 1.5_rp, 1.5_rp,  2.0_rp /)
  xx2 = (/ 1.5_rp, 1.5_rp,  0.1_rp /)


  bocod(1,1) = 1.0_rp
  bocod(2,1) = 1.0_rp
  bocod(3,1) = 1.0_rp

  bocod(1,2) = 2.0_rp
  bocod(2,2) = 1.0_rp
  bocod(3,2) = 1.0_rp

  bocod(1,3) = 2.0_rp
  bocod(2,3) = 2.0_rp
  bocod(3,3) = 1.0_rp
 
  bocod(1,4) = 1.0_rp
  bocod(2,4) = 2.0_rp
  bocod(3,4) = 0.0_rp
 
  toler = 0.0_rp 
  call elmgeo_intersection_segment_QUA04(&
       bocod,xx1,xx2,toler,intersection,ifoun)

  print*,'A=',ifoun
  if( ifoun /= 0 ) then
     print*,'B=',intersection
  end if
  stop

end subroutine test
