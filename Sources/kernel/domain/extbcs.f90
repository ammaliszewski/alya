subroutine extbcs()
  !-----------------------------------------------------------------------
  !****f* Domain/extbcs
  ! NAME
  !    extbcs
  ! DESCRIPTION
  !    Extrapolate cvonditions on boundaries to conditions on nodes
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_elmtyp
  use mod_communications, only : PAR_GHOST_BOUNDARY_EXCHANGE
  implicit none
  integer(ip)   :: ipoin,iboun,inodb,kodbo,kcode,icono,mcod1
  character(20) :: mess1,mess2

  mess1 = intost(mcono)
  !
  ! Parall: Exchange KFL_CODBO on fringe nodes
  !
  if( ISLAVE ) then
     call memgen(1_ip,nboun_2,0_ip)
     do iboun = 1,nboun
        gisca(iboun) = kfl_codbo(iboun)
     end do  
     call PAR_GHOST_BOUNDARY_EXCHANGE(gisca,'SUBSTITUTE','IN MY CODE')
     !pari1 => gisca
     !call Parall(410_ip)
  else 
     gisca => kfl_codbo
  end if
  !
  ! Extrapolate to a dummi array GIVEC
  !
  call memgen(1_ip,mcono,npoin_2)
  mcod1 = mcodb+1
  do ipoin = 1,npoin_2
     do icono = 1,mcono
        givec(icono,ipoin) = mcod1
     end do
  end do

  do iboun = 1,nboun_2
     if( lboch(iboun) /= BOEXT ) then
        kodbo = gisca(iboun)
        if( abs(kodbo) <= mcodb ) then
           do inodb = 1,nnode(ltypb(iboun))
              ipoin = lnodb(inodb,iboun)
              !
              ! Look for a place
              !
              kcode = 1
              mcono_loop2: do while( givec(kcode,ipoin) /= mcod1 )
                 if( kodbo == givec(kcode,ipoin) ) kcode = mcono + 1           
                 kcode = kcode + 1
                 if( kcode > mcono ) exit mcono_loop2
              end do mcono_loop2

              if( kcode <= mcono ) then
                 if( givec(kcode,ipoin) /= mcod1 ) then
                    mess2=intost(ipoin)
                    call outfor(2_ip,lun_outpu,'NODE '//trim(mess1)&
                         //' HAVE MORE THAN '//trim(mess2))
                 else
                    lcodn(kodbo) = .true.
                    givec(kcode,ipoin) = kodbo
                 end if
              end if

           end do

        end if
     end if

  end do
  !
  ! Deallocate memory
  !
  if( ISLAVE ) then
     call memgen(3_ip,nboun_2,0_ip)
  else
     nullify(gisca)
  end if
  !
  ! Sort codes
  !
  do ipoin = 1,npoin
     kcode = 1
     kcode_loop2: do while( givec(kcode,ipoin) /= mcod1 )        
        kcode = kcode + 1
        if( kcode > mcono ) exit kcode_loop2
     end do kcode_loop2
     kcode = kcode - 1
     call heapsorti1(2_ip,kcode,givec(1,ipoin))
  end do
  !
  ! Write over KFL_CODNO
  ! 
  do ipoin = 1,npoin
     if( kfl_codno(1,ipoin) == mcod1 ) then
        do icono = 1,mcono
           kfl_codno(icono,ipoin) = givec(icono,ipoin)
        end do
     end if
  end do
  call memgen(3_ip,mcono,npoin_2)
  !
  ! NCODC: Counter number of combinations
  !
  !call outfor(42_ip,lun_outpu,' ')
  !ncodc=0
  !do ipoin=1,npoin
  !   if(kfl_codno(1,ipoin)>0) then
  !      icode=1
  !      icode_loop2: do while( kfl_codno(icode,ipoin) /= 0 )
  !         ioutp(icode) = kfl_codno(icode,ipoin)
  !         icode        = icode+1
  !         if( icode > mcono ) exit icode_loop2
  !      end do icode_loop2
  !      nnoco = 1
  !      icode = icode-1
  !      ialre = 0
  !      do jpoin=ipoin+1,npoin
  !         kcode=1
  !         jcode=0
  !         kcod2_llop2: do while( kfl_codno(kcode,jpoin) > 0 )
  !            if(kfl_codno(kcode,ipoin)==kfl_codno(kcode,jpoin)) then
  !               jcode = jcode+1
  !            end if
  !            kcode = kcode+1
  !            if( kcode > mcono) exit kcod2_llop2
  !         end do kcod2_llop2
  !         if( jcode == icode ) then
  !            nnoco = nnoco+1
  !            do kcode = 1,mcono
  !               kfl_codno(kcode,jpoin)=-abs(kfl_codno(kcode,jpoin))
  !            end do
  !            if(ialre==0) then
  !               ialre=1
  !               ncodc=ncodc+1
  !            end if
  !         end if
  !      end do
  !      ioutp(49)=icode
  !      ioutp(50)=nnoco
  !      call outfor(43_ip,lun_outpu,' ')
  !   end if
  !end do
  !do ipoin=1,npoin
  !   do kcode=1,mcono
  !      kfl_codno(kcode,ipoin)=abs(kfl_codno(kcode,ipoin))
  !   end do
  !end do

end subroutine extbcs

