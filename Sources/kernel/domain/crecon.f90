!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    crecon.f90
!> @author  Mariano Vazquez
!> @date    02/10/2013
!> @brief   Create contacts
!> @details Create contacts
!> @} 
!-----------------------------------------------------------------------
subroutine crecon()
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use def_elmtyp
  use mod_memchk
  implicit none
  integer(ip) :: &
       ielty,inode,ipnew,ielem,jelem,kelem,izone,inodc,nnodc,lauxi(100),&
       iface_local,iface,max_ncont,ncont,ipoin,jpoin,npnew,pnode,iboun,inodb,pnodb,icont,&
       condition_solid,condition_fluid,ielem_solid,ielem_fluid,jcont,itype
  integer(ip), pointer :: &
       lnoco(:,:), poaux(:),lzone_read(:),lnocf(:)
  real(rp), pointer :: &
       conew(:,:)
  character(200) :: messa,output_file

  !
  !    LFACG(4,NFACG) and LELFA(1:NELEM) % L(1:NFACE(LTYPE(IELEM)))
  !
  !    LFACG(1,IFACG) = IELEM
  !    LFACG(2,IFACG) = JELEM/0 ... 0 if boundary face
  !    LFACG(3,IFACG) = IFACE ..... Local IELEM face
  !    LFACG(4,IFACG) = JFACE/0 ... Local JELEM face
  !    Caution: 0 < IELEM < NELEM_2 so IELEM can be a neighbor's element
  !
  !    LELFA(IELEM) % L(1:NFACE) ... Global face connectivity for IELEM
  !


  messa='CREATING CONTACTS...'
  call livinf(zero,messa,zero)

  
  ! MAXIMUM VALUE FOR NCONT --> INCREASE IF NEEDED
  max_ncont= 400000
  allocate(lnoco(10,max_ncont))
  allocate(lnocf(   max_ncont))
  allocate(conew( 3,max_ncont))
  allocate(poaux(       npoin))
  allocate(lzone_read(  nelem))

  !
  ! Compute all geometrical stuff required 
  !

  if (.not.associated(ltype)) allocate(ltype(        nelem))
  if (.not.associated(lelch)) allocate(lelch(        nelem))
  if (.not.associated(lpoty)) allocate(lpoty(        npoin))

  if (associated(nepoi)) deallocate(nepoi)
  if (associated(pelpo)) deallocate(pelpo)
  if (associated(lelpo)) deallocate(lelpo)
  if (associated(lelfa)) deallocate(lelfa)
  if (associated(facel)) deallocate(facel)
  if (associated(lfacg)) deallocate(lfacg)
  if (associated(lelbf)) deallocate(lelbf)

  messa='   TOTAL ELEMENTS'
  call livinf(-7_ip,messa,          nelem)
  messa='   ELEMENTS IN ZONE 1'
  call livinf(-7_ip,messa,nelez(1))
  messa='   ELEMENTS IN ZONE 2'
  call livinf(-7_ip,messa,nelez(2))

  do izone=1,nzone
     do kelem = 1,nelez(izone)
        ielem = lelez(izone) % l(kelem)
        lzone_read(ielem) = izone
     end do
  end do
  do ielem=1,nelem
     call fintyp(ndime,lnnod(ielem),ielty)
     lexis(ielty)= 1
     ltype(ielem)= ielty
  end do

  call cderda
  call connpo

  nelem_2 = nelem
  pelpo_2 => pelpo
  lelpo_2 => lelpo

  kfl_lface= 1
  kfl_lelbf= 0

  lpoty = 1   ! esto es necesario

  call lgface

  !
  ! Compute contacts
  !

  ncont= 0
  poaux= 0
  ipnew= 0
  do ielem= 1,nelem
     izone= lzone_read(ielem)
     inodc= 0
     lauxi= 0
     
     if (izone == 1) then                    ! solo mirar los de la zona 1

        ielty= abs(ltype(ielem))
        do iface_local= 1,nface(ielty)
           iface= lelfa(ielem)%l(iface_local)
           ! ielem cae en la zona 1 (fluido)
           ! jelem es el elemento del otro lado de la cara 
           jelem= lfacg(1,iface)
           if (ielem == jelem) jelem= lfacg(2,iface)
           if (jelem > 0) then
              if (izone .ne. lzone_read(jelem)) then   
                 ! contacto identificado, porque jelem y ielem no estan en la misma zona
                 ncont= ncont+1        ! numero de contactos
                 if (ncont > max_ncont) then
                    call runend("CRECON: INCREASE MAX_NCONT AND RECOMPILE")
                 else
                    nnodc = nnodf(ielty) % l(iface_local)
                    do inodc =1, nnodc
                       inode = lface(ielty)%l(inodc,iface_local) 
                       ! los nodos viejos del contacto se quedan del lado del solido, o sea jelem
                       ! y los nuevos iran en poaux y son los del lado del fluido, o sea ielem
                       ipoin = lnods(inode,ielem)
                       jpoin = ipoin
                       if (poaux(ipoin) == 0) then
                          ipnew= ipnew+1
                          poaux(ipoin) = ipnew + npoin  ! la numeracion nueva para el nodo nuevo
                       end if
!ojj!!!                       lnoco(inodc      ,ncont)= poaux(ipoin) ! lado del fluido
!ojj!!!                       lnoco(inodc+nnodc,ncont)= jpoin        ! lado del solido
                       lnoco(inodc      ,ncont)= jpoin        ! lado del solido
                       lnoco(inodc+nnodc,ncont)= poaux(ipoin) ! lado del fluido
                    end do
                    ! el elemento que esta del lado fluido y que usara nodo nuevo
!ojj!!                    lnoco(nnodc*2+1,ncont) = ielem   
                    ! el elemento que esta del lado solido 
!oj!!                    lnoco(nnodc*2+2,ncont) = jelem   

                    ! el elemento que esta del lado solido 
                    lnoco(nnodc*2+1,ncont) = jelem   
                    ! el elemento que esta del lado fluido y que usara nodo nuevo
                    lnoco(nnodc*2+2,ncont) = ielem   
                    
!                    if (ielem == 131078) then
!                       write (6,*) 'totooooo',ielem,jelem,ncont
!                       write (6,*) lnoco(      1:  nnodc,ncont)
!                       write (6,*) lnoco(nnodc+1:2*nnodc,ncont)                       
!                       write (6,*) 'totooooo'
!                    end if

                    ! en 2d, la parte del contacto con nodos nuevos (fluido) va en orden inverso
                    if (ndime==2) then
                       do inodc= 1,nnodc
                          lauxi(nnodc - inodc + 1)= lnoco(inodc,ncont)  
                       end do
                       lnoco(1:nnodc,ncont)  = lauxi(1:nnodc) 
                    end if
                    
                    lnocf(ncont) = nnodc

                 end if
              end if
           end if
        end do
     end if
  end do

  npnew= ipnew
  messa='   CONTACTS'
  call livinf(-7_ip,messa,ncont)
  messa='   NEW POINTS'
  call livinf(-7_ip,messa,npnew+npoin)
  messa='   OLD POINTS'
  call livinf(-7_ip,messa,npoin)
  !
  ! Set the coordinates for the new nodes
  ! 
  do ipoin= 1,npoin
     if (poaux(ipoin) > 0) then
        ipnew= poaux(ipoin) - npoin
        conew(1:ndime,ipnew) = coord(1:ndime,ipoin)
     end if
  end do
  !
  ! Correct lnods
  ! 
  do ielem= 1,nelem
     izone= lzone_read(ielem)
     if (izone == 1) then                 ! Only correct those of zone 1
        ielty= abs(ltype(ielem))
        pnode= lnnod(ielem)
        do inode=1,pnode
           ipoin= lnods(inode,ielem)
           if (ipoin .le. npoin) then     ! When ipoin is larger than the corrected npoin
              if (poaux(ipoin) > 0) then
                 lnods(inode,ielem)= poaux(ipoin)
              end if
           end if
        end do
     end if
  end do

  ! Correct lnodb
  do iboun= 1,nboun
     pnodb = nnode(ltypb(iboun))
     ielem = lboel((pnodb+1),iboun)
     izone= lzone_read(ielem)
     if (izone == 1) then                 ! Only correct those of zone 1        
        do inodb= 1,mnodb
           ipoin= lnodb(inodb,iboun)
           if ((ipoin .le. npoin).and.(ipoin.gt.0)) then     
              if (poaux(ipoin) > 0) then
                 lnodb(inodb,iboun)= poaux(ipoin)
              end if
           end if
        end do
     end if
  end do

  messa='   NEW ELEMENTS'
  call livinf(-7_ip,messa,nelem+ncont)
  messa='   OLD ELEMENTS'
  call livinf(-7_ip,messa,nelem)

  open (616,file='README.CONTACTS')

  messa='INSTRUCTIONS AFTER CREATING CONTACTS'
  write(616,*) trim(messa)
  messa=' '
  write(616,*) trim(messa)

  messa='   NEW FILES TO BE INCLUDED IN THE NEW DOM DAT:'
  write(616,*) trim(messa)

  messa='   WRITE NEW FILES...'
  call livinf(zero,messa,zero)
  
  ! write the new files
  output_file = 'cnt-zones.geo.dat'  
  open (606,file=output_file)
  messa = '      '//output_file
  write(616,*) '  INCLUDE',trim(messa)
  call livinf(zero,messa,zero)
  write(606,*) 'ZONES'
  write(606,*) 'ZONE, NUMBER = 1'
  do kelem= 1,nelez(1)
     ielem = lelez(1) % l(kelem)
     write(606,*) ielem
  end do
  write(606,*) 'END_ZONE'
  write(606,*) 'ZONE, NUMBER = 2'
  do kelem= 1,nelez(2)
     ielem = lelez(2) % l(kelem)
     write(606,*) ielem
  end do
  write(606,*) 'END_ZONE'
  write(606,*) 'ZONE, NUMBER = 3'
  do kelem=1,ncont
     ielem = kelem+nelem
     write(606,*) ielem
  end do
  write(606,*) 'END_ZONE'
  write(606,*) 'END_ZONES'

  itype = 3
  write(606,'(a)') 'CHARACTERISTICS'
  do icont= 1,ncont
     write(606,100) icont+nelem, itype
  end do
  write(606,'(a)') 'END_CHARACTERISTICS'
  write(606,*) 
  close(606)

  output_file = 'cnt-nodeselements.geo.dat'  
  open (606,file=output_file)
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  write(606,'(a)') 'NODES_PER_ELEMENT'
  do ielem= 1,nelem
     write(606,100) ielem, lnnod(ielem)
  end do
  do icont= 1,ncont
     nnodc= lnocf(icont) 
     write(606,100) icont+nelem, nnodc*2
  end do
  write(606,'(a)') 'END_NODES_PER_ELEMENT'
  close(606)

  output_file = 'cnt-elements.geo.dat'  
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  open (606,file=output_file)
  write(606,'(a)') 'ELEMENTS'
  do ielem= 1,nelem
     write(606,100) ielem,lnods(1:lnnod(ielem),ielem)     
  end do
  do icont= 1,ncont
     nnodc= lnocf(icont) 
     write(606,100) icont+nelem, lnoco(1:nnodc*2,icont)
  end do
  write(606,'(a)') 'END_ELEMENTS'
  close(606)

  output_file = 'cnt-coordinates.geo.dat'  
  open (606,file=output_file)
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  write(606,'(a)') 'COORDINATES'
  do ipoin= 1,npoin
     write(606,200) ipoin,coord(1:ndime,ipoin)
!     write(636,*) ipoin,'  0.0  0.0  1.0'   ! this is to create a bogus fiber field
  end do
  do ipnew= 1,npnew
     write(606,200) ipnew+npoin,conew(1:ndime,ipnew)
!     write(636,*) ipnew+npoin,'  0.0  0.0  1.0'   ! this is to create a bogus fiber field
  end do
  write(606,'(a)') 'END_COORDINATES'
  close(606)

  output_file = 'cnt-boundaries.geo.dat'  
  open (606,file=output_file)
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  output_file = 'cnt-bocon.geo.dat'  
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  open (608,file=output_file)

  write(606,'(a)') 'BOUNDARIES, ELEMENT'
  write(608,'(a)') 'BOUNDARY_CONDITIONS, EXTRAPOLATE'
  write(608,'(a)') 'ON_BOUNDARIES,UNKNOWN'
  ! old boundaries with corrected lnodb
  do iboun= 1,nboun
     pnodb = nnode(ltypb(iboun))
     ielem = lboel((pnodb+1),iboun)
     write(606,100) iboun,lnodb(1:mnodb,iboun),ielem                      ! boundary element
     write(608,100) iboun,mnodb,lnodb(1:mnodb,iboun),kfl_codbo(iboun)     ! boundary condition
  end do

  ! new boundaries:
  ! those for the new contacts in 606 and in 608
  ! for the contact, they are 30 on the fluid side and 40 on the solid side

  condition_fluid= 30
  condition_solid= 40

  jcont= 0
  do icont= 1,ncont
!     ielem_fluid = lnoco(nnodc*2+1,icont)
!     ielem_solid = lnoco(nnodc*2+2,icont)
!     jcont= jcont+1
!     write(606,100) jcont+nboun,        lnoco(nnodc+1:nnodc*2,icont), ielem_solid
!     write(608,100) jcont+nboun, mnodb, lnoco(nnodc+1:nnodc*2,icont), condition_solid
!     jcont= jcont+1
!     write(606,100) jcont+nboun,        lnoco(      1:nnodc  ,icont), ielem_fluid
!     write(608,100) jcont+nboun, mnodb, lnoco(      1:nnodc  ,icont), condition_fluid

     ielem_solid = lnoco(nnodc*2+1,icont)
     ielem_fluid = lnoco(nnodc*2+2,icont)
     jcont= jcont+1
     write(606,100) jcont+nboun,        lnoco(      1:nnodc  ,icont), ielem_solid
     write(608,100) jcont+nboun, mnodb, lnoco(      1:nnodc  ,icont), condition_solid
     jcont= jcont+1
     write(606,100) jcont+nboun,        lnoco(nnodc+1:nnodc*2,icont), ielem_fluid
     write(608,100) jcont+nboun, mnodb, lnoco(nnodc+1:nnodc*2,icont), condition_fluid

  end do
  write(606,'(a)') 'END_BOUNDARIES'
  write(608,'(a)') 'END_ON_BOUNDARIES'
  write(608,'(a)') 'END_BOUNDARY_CONDITIONS'  
  close(606)
  close(608)

  if (nmate > 1) then
     output_file = 'cnt-materials.geo.dat'  
     open (606,file=output_file)     
     messa = '      '//output_file
     call livinf(zero,messa,zero)
     write(606,*) 'MATERIALS, FLUID_MATERIAL=1, DEFAULT= 1, NUMBER = ',nmate
     write(616,*) '  INCLUDE',trim(messa)
     do ielem=1,nelem
        write(606,*) ielem,lmate(ielem)
     end do
     write(606,*) 'END_MATERIALS'
     close(606)
  end if

  messa='   WRITE NEW FILES... DONE.'
  call livinf(zero,messa,zero)

  messa='CREATING CONTACTS... DONE.'
  call livinf(zero,messa,zero)

  messa=' '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa='VERY IMPORTANT!!'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa=' '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '     DO NOT FORGET TO CORRECT DOM.DAT, INCLUDING:  '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '     1. FOR 3D PROBLEMS, ADD ELEMENT 34 TO THE TYPES OF ELEMENTS    '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        TYPES_OF_ELEMENTS= 0, 0, 0, 0, 0, 30, 0, 0, 0, 34, 0 '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        FOR 2D PROBLEMS, ADD ELEMENT 12 (when needed) TO THE TYPES OF ELEMENTS    '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        TYPES_OF_ELEMENTS= 10, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0 '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '     2. ADD THE NEW BOUNDARY CONDITION TO THE FLUID AND TO ALEFOR:'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        *.NSI.DAT:'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '          30 --> SOLID WALL OR SLIP CONDITION'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        *.ALE.DAT:'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '          30 --> DIRICHLET CONDITION TO 0.0'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '     3. CORRECT THE NUMBER OF ZONES, NOW IT IS ZONES : 3'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        CORRECT THE NUMBER OF NODAL_POINTS, NOW IT IS NODAL_POINTS '
  write(616,*) trim(messa), npnew+npoin
  call livinf(-7_ip,messa,npnew+npoin)
  messa= '        CORRECT THE NUMBER OF ELEMENTS, NOW IT IS ELEMENTS'
  write(616,*) trim(messa),nelem+ncont
  call livinf(-7_ip,messa,nelem+ncont)
  messa= '        CORRECT THE NUMBER OF BOUNDARIES, NOW IT IS BOUNDARIES '
  write(616,*) trim(messa),nboun+jcont
  call livinf(-7_ip,messa,nboun+jcont)
  messa=' '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)

  close(616)

  100 format(10(2x,i6))
  200 format(i8,3(2x,e12.5))

end subroutine crecon
