subroutine calc_kx_tran_fiel() 
  !-----------------------------------------------------------------------
  !****f* Domain/calc_kx_tran_fiel
  ! NAME
  !    calc_kx_tran_fiel
  ! DESCRIPTION
  !    Obtains k_tran_fiel and x_tran_fiel for field with more than 1 step
  !    k_tran_fiel(ifiel) indicates to which interval the current time belongs.
  !    x_tran_fiel(ifiel) indicates the position between the begining and end of the interval. 
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_domain

  implicit none
  
  integer(ip) :: ifiel,kk,istep

  if( INOTMASTER ) then  ! time_field has bee deallocated for master
     do ifiel = 1,nfiel
        if ( kfl_field(4,ifiel)>1 ) then
           kexist_tran_fiel = 1
           if (abs(time_field(ifiel) % a(1)) > 1.0e-15) then   ! I am not sure if this check is actually needed ( ej for a conti )
              print*,'field number', ifiel,'the first time field is supposed to be cero'
              call runend('calc_kx_tran_fiel: the first time field is supposed to be cero')
           end if
           steps: do istep = 2,kfl_field(4,ifiel)
              if ( cutim < time_field(ifiel) % a(istep) ) then
                 kk = istep - 1
                 k_tran_fiel(ifiel) = kk
                 exit steps
              end if
           end do steps
           x_tran_fiel(ifiel) = (time_field(ifiel) % a(kk+1)- cutim ) / &
                (time_field(ifiel) % a(kk+1) - time_field(ifiel) % a(kk))
        end if
     end do
  end if


end subroutine calc_kx_tran_fiel
