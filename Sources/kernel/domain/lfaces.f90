subroutine lfaces()
  !------------------------------------------------------------------------
  !****f* domain/lfaces
  ! NAME
  !    lfaces
  ! DESCRIPTION
  !    This routine computes the list of faces
  ! USED BY
  !    submsh
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_elmtyp
  use def_master
  use def_domain
  use mod_memchk
  use mod_htable
  implicit none  
  integer(ip) :: ielty,ielem,iface,inodb,ilist
  integer(ip) :: inode,jelem,jface,jelty,ipoin,pnodf
  integer(ip) :: pepoi,ielpo
  integer(4)  :: istat
  logical(lg) :: equal_faces

  call livinf(0_ip,'FACE TABLE',0_ip)
  nfacg = 0

  if( INOTMASTER ) then

     if( lnuty(HEX08) /= 0 .or. lnuty(PYR05) /= 0 .or. lnuty(PEN06) /= 0 ) then
        !
        ! Faces graph: Allocate memory for FACES, CFAEL AND NNODG
        !
        allocate(facel(mnodb+1,mface,nelem),STAT=istat)
        call memchk(zero,istat,memor_dom,'FACEL','lfaces',facel)
        !
        ! Construct and sort FACES
        !
        !*OMP  PARALLEL DO SCHEDULE (GUIDED)           & 
        !*OMP  DEFAULT (NONE)                          &
        !*OMP  PRIVATE (ielem,ielty,iface,inodb,inode) &
        !*OMP  SHARED  (ltype,cfael,faces,lnods,nnodg,nelem,nface) 
        !
        do ielem = 1,nelem                                         
           ielty = abs(ltype(ielem))
           do iface = 1,nface(ielty)
              if( ltypf(ielty)%l(iface) == QUA04 ) then
                 pnodf = nnodf(ielty) % l(iface)
                 do inodb = 1,pnodf
                    inode = lface(ielty)%l(inodb,iface) 
                    facel(inodb,iface,ielem) = lnods(inode,ielem)
                 end do
                 facel(mnodb+1,iface,ielem) = 1
                 call sortin(pnodf,facel(1,iface,ielem))
              else
                 facel(mnodb+1,iface,ielem) = 0
              end if
           end do
        end do
        !
        ! Compute FACES
        !
        do ielem = 1,nelem                                            ! Compare the faces and 
           ielty = abs(ltype(ielem))                                  ! eliminate the repited faces
           do iface = 1,nface(ielty)
              if( facel(mnodb+1,iface,ielem) > 0 ) then
                 nfacg = nfacg + 1
                 ipoin = facel(1,iface,ielem)
                 ilist = 1 
                 pepoi = pelpo(ipoin+1)-pelpo(ipoin)
                 ielpo = pelpo(ipoin)-1
                 do while( ilist <= pepoi )
                    ielpo = ielpo + 1
                    jelem = lelpo(ielpo)
                    if( jelem /= ielem ) then
                       jelty = abs(ltype(jelem))                              ! eliminate the repited faces
                       jface = 0
                       do while( jface /= nface(jelty) )
                          jface = jface + 1
                          if( facel(mnodb+1,jface,jelem) > 0 ) then
                             equal_faces = .true.
                             inodb = 0
                             do while( equal_faces .and. inodb /= nnodf(jelty) % l(jface) )
                                inodb = inodb+1
                                if( facel(inodb,iface,ielem) /= facel(inodb,jface,jelem) ) &
                                     equal_faces = .false.
                             end do
                             if( equal_faces ) then
                                facel(mnodb+1,iface,ielem) =  jelem  ! Keep IELEM face
                                facel(mnodb+1,jface,jelem) = -ielem  ! Elminate JELEM face
                                facel(1,jface,jelem)       =  iface  ! Remember IFACE face
                                jface = nface(jelty)                 ! Exit JFACE do
                                ilist = pepoi                        ! Exit JELEM do  
                             end if
                          end if
                       end do
                    end if
                    ilist = ilist + 1
                 end do
              end if
           end do
        end do
        !
        ! Allocate memory
        !
        allocate( lfacg(7,nfacg), stat=istat )
        call memchk(zero,istat,memor_dom,'LFACG','submsh',lfacg)     

        nfacg = 0
        do ielem = 1,nelem                                            ! Compare the faces and 
           ielty = abs(ltype(ielem))                                  ! eliminate the repeated faces
           do iface = 1,nface(ielty)
              if( facel(mnodb+1,iface,ielem) > 0 ) then
                 nfacg = nfacg + 1
                 pnodf = nnodf(ielty) % l(iface)
                 do inode = 1,pnodf
                    lfacg(inode,nfacg) = facel(inode,iface,ielem)
                 end do
                 facel(1,iface,ielem) = nfacg
              end if
           end do
        end do

        do ielem = 1,nelem            
           ielty = abs(ltype(ielem))  
           do iface = 1,nface(ielty)
              if( facel(mnodb+1,iface,ielem) < 0 ) then
                 jelem = -facel(mnodb+1,iface,ielem)
                 jface =  facel(1,iface,ielem)
                 facel(1,iface,ielem) = facel(1,jface,jelem)
              end if
           end do
        end do
        !
        ! Order faces
        !
        do iface = 1,nfacg
           call sortin(4_ip,lfacg(1,iface))
        end do

     end if

  end if

end subroutine lfaces
