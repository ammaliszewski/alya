subroutine lbouel()
  !-----------------------------------------------------------------------
  !****f* meshin/lbouel
  ! NAME
  !    lbouel
  ! DESCRIPTION
  !    This routine associates a boundary element with a volume element
  !    in array LBOEL
  ! OUTPUT
  !    LBOEL
  ! USED BY
  !    domain
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_elmtyp
  use def_master
  use def_domain
  use mod_memchk
  use mod_href
  implicit none
  integer(ip)             :: ipoin,inodb,pnode,pnodb,jelem,kelem
  integer(ip)             :: jpoin,icoin,kpoin,inode,ielem,iboun
  integer(4)              :: istat
  type(i1p),   pointer    :: lboib(:) => null()
  type(i1p),   pointer    :: lpoel(:) => null()
  integer(ip), pointer    :: lnoel(:) => null()

  if( kfl_bouel == 0 .and. nboun /= 0 ) then
     !
     ! Allocate memory
     !
     call livinf(92_ip,' ',zero)
     allocate(lboib(nboun),stat=istat)
     call memchk(zero,istat,memor_dom,'LBOIB','lbouel',lboib)
     allocate(lpoel(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LPOEL','lbouel',lpoel)
     allocate(lnoel(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LNOEL','lbouel',lnoel)
     !
     ! Construct lboel
     !
     do ipoin = 1,npoin
        lnoel(ipoin) = 0
     end do
     do ielem = 1,nelem
        pnode = nnode(ltype(ielem))
        do inode = 1,pnode
           ipoin = lnods(inode,ielem)
           lnoel(ipoin) = lnoel(ipoin) + 1
        end do
     end do

     do ipoin = 1,npoin
        if( lnoel(ipoin) > 0 ) then
           kelem = lnoel(ipoin)
           allocate( lpoel(ipoin) % l(kelem), stat=istat )
           do ielem = 1,kelem
              lpoel(ipoin) % l(ielem) = 0
           end do
           lnoel(ipoin) = 0
        end if
     end do

     do ielem = 1,nelem
        pnode = nnode(ltype(ielem))
        do inode = 1,pnode
           ipoin = lnods(inode,ielem)
           lnoel(ipoin) = lnoel(ipoin) + 1
           lpoel(ipoin) % l(lnoel(ipoin)) = ielem
        end do
     end do


     do iboun = 1,nboun
        kpoin = 0
        do inodb = 1,nnode(ltypb(iboun))
           ipoin = lnodb(inodb,iboun)
           kpoin = kpoin + lnoel(ipoin)
        end do
        allocate( lboib(iboun) % l(kpoin), stat=istat )
        kpoin = 0
        do inodb = 1,nnode(ltypb(iboun))
           ipoin = lnodb(inodb,iboun)
           do kelem = 1,lnoel(ipoin)
              kpoin = kpoin + 1
              lboib(iboun) % l(kpoin) = lpoel(ipoin) % l(kelem)
           end do
        end do
     end do


     do iboun = 1,nboun
        pnodb = nnode(ltypb(iboun))
        kelem = 0
        do while( kelem < size(lboib(iboun) % l) )
           kelem = kelem + 1
           icoin = 0
           ielem = lboib(iboun) % l(kelem)    
           if( ielem > 0 ) then
              pnode = nnode(ltype(ielem))              
              do inodb = 1,pnodb
                 ipoin = lnodb(inodb,iboun)
                 inode = 0
                 do while( inode < pnode )
                    inode = inode + 1
                    jpoin = lnods(inode,ielem)
                    if( ipoin == jpoin ) then
                       icoin = icoin + 1
                       inode = pnode
                    end if
                 end do
              end do
              !
              ! Element IELEM has been found
              !
              if( icoin == pnodb ) then
                 lboel(pnodb+1,iboun) = ielem

    !             write(570,*) iboun,lnodb(1:pnodb,iboun),lboel(pnodb+1,iboun)

                 kelem = size(lboib(iboun) % l)
              else
                 do jelem = ielem,size(lboib(iboun) % l)
                    if( ielem == lboib(iboun) % l(jelem) ) then
                       lboib(iboun) % l(jelem) = 0
                    end if
                 end do
              end if
           end if
        end do

     end do
     !
     ! Deallocate memory
     !
     do iboun = 1,nboun
        deallocate( lboib(iboun) % l, stat=istat )
     end do
     do ipoin = 1,npoin
        deallocate( lpoel(ipoin) % l, stat=istat )
     end do

     call memchk(two,istat,memor_dom,'LNOEL','lbouel',lnoel)
     deallocate(lnoel,stat=istat)
     if(istat/=0) call memerr(two,'LNOEL','lbouel',0_ip)

     call memchk(two,istat,memor_dom,'LPOEL','lbouel',lpoel)
     deallocate(lpoel,stat=istat)
     if(istat/=0) call memerr(two,'LPOEL','lbouel',0_ip)

     if(istat/=0) call memerr(two,'LBOIB','lbouel',0_ip)
     deallocate(lboib,stat=istat)
     if(istat/=0) call memerr(two,'LBOIB','lbouel',0_ip)
 
     kfl_bouel = 1

  end if

end subroutine lbouel
