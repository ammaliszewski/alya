subroutine memgeo(itask)
  !-----------------------------------------------------------------------
  !****f* Domain/memgeo
  ! NAME
  !    memgeo
  ! DESCRIPTION
  !    Allocate/Deallocate the geometry arrays 
  !    ITASK=1 ... Allocate memory
  !    ITASK=2 ... Deallocate memory
  ! OUTPUT
  ! USED BY
  !    reageo
  !    sengeo
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_memchk
  use mod_memory
  use mod_iofile
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: izone,nbou1,ipoin,kpoin,ifiel
  integer(ip)             :: inode,ielem,inodb,iboun

  nbou1 = max(1_ip,nboun)

  select case(itask)

  case(1_ip)
     !
     ! Allocate memory (deallocate in case = -1 )
     !
     call memory_alloca(memor_dom,'LTYPE','memgeo' , ltype ,  nelem   )
     call memory_alloca(memor_dom,'LNNOD','memgeo' , lnnod ,  nelem   )
     call memory_alloca(memor_dom,'LELCH','memgeo' , lelch ,  nelem   )
     call memory_alloca(memor_dom,'LNODS','memgeo' , lnods ,  mnode   , nelem )
     call memory_alloca(memor_dom,'LESUB','memgeo' , lesub ,  nelem   )

     call memory_alloca(memor_dom,'COORD','memgeo' , coord ,  ndime   , npoin )
     call memory_alloca(memor_dom,'LNOCH','memgeo' , lnoch ,  npoin   )
 
     call memory_alloca(memor_dom,'LNODB','memgeo' , lnodb ,  mnodb   , nbou1 )
     call memory_alloca(memor_dom,'LBOEL','memgeo' , lboel ,  mnodb+1 , nbou1 )
     call memory_alloca(memor_dom,'LTYPB','memgeo' , ltypb ,  nbou1   )
     call memory_alloca(memor_dom,'LBOCH','memgeo' , lboch ,  nbou1   )

     call memory_alloca(memor_dom,'SKCOS','memgeo' , skcos ,  ndime   , ndime , nskew )

     !if( nhang > 0 ) then                                         ! Hanging nodes
     !   allocate(lhang(0:2*mnodb,nhang),stat=istat)
     !   call memchk(zero,istat,memor_dom,'LHANG','memgeo',lhang)        
     !end if

  case(-1_ip)
     !
     ! Deallocate memory allocate in case = 1
     !
     call memory_deallo(memor_dom,'LTYPE','memgeo' , ltype )
     call memory_deallo(memor_dom,'LNNOD','memgeo' , lnnod )
     call memory_deallo(memor_dom,'LELCH','memgeo' , lelch )
     call memory_deallo(memor_dom,'LNODS','memgeo' , lnods )
     call memory_deallo(memor_dom,'LESUB','memgeo' , lesub )

     call memory_deallo(memor_dom,'COORD','memgeo' , coord )
     call memory_deallo(memor_dom,'LNOCH','memgeo' , lnoch )

     call memory_deallo(memor_dom,'LNODB','memgeo' , lnodb )
     call memory_deallo(memor_dom,'LBOEL','memgeo' , lboel )
     call memory_deallo(memor_dom,'LTYPB','memgeo' , ltypb )
     call memory_deallo(memor_dom,'LBOCH','memgeo' , lboch )

     call memory_deallo(memor_dom,'SKCOS','memgeo' , skcos )

  case(2_ip)
     !
     ! Deallocate memory: Mesh arrays
     !
     call memory_deallo(memor_dom,'LTYPE','memgeo' , ltype )
     call memory_deallo(memor_dom,'LNNOD','memgeo' , lnnod )
     call memory_deallo(memor_dom,'LELCH','memgeo' , lelch )
     call memory_deallo(memor_dom,'LNODS','memgeo' , lnods )
     call memory_deallo(memor_dom,'LESUB','memgeo' , lesub )

     call memory_deallo(memor_dom,'COORD','memgeo' , coord )
     call memory_deallo(memor_dom,'LNOCH','memgeo' , lnoch )

     call memory_deallo(memor_dom,'LNODB','memgeo' , lnodb )
     call memory_deallo(memor_dom,'LBOEL','memgeo' , lboel )
     call memory_deallo(memor_dom,'LTYPB','memgeo' , ltypb )
     call memory_deallo(memor_dom,'LBOCH','memgeo' , lboch )

     call memory_deallo(memor_dom,'SKCOS','memgeo' , skcos )
     !
     ! Dealloctae R_DOM and C_DOM to minimum size (used in deflated CG)
     !
     call memory_deallo(memor_dom,'R_DOM','memgeo' , r_dom )
     call memory_deallo(memor_dom,'C_DOM','memgeo' , c_dom )
     call memory_alloca(memor_dom,'R_DOM','memgeo' , r_dom , 1_ip )
     call memory_alloca(memor_dom,'C_DOM','memgeo' , c_dom , 1_ip )
     !
     ! Deallocate LELPO and PELPO (used in parall)
     !
     call memory_deallo(memor_dom,'PELPO','memgeo' , pelpo )
     call memory_deallo(memor_dom,'LELPO','memgeo' , lelpo )
     !
     ! Deallocate memory
     !
     call memose(10_ip)

  case(3_ip)
     !
     ! Deallocate memory: Mesh arrays
     !
     !call memory_deallo(memor_dom,'LTYPE','memgeo' , ltype )
     call memory_deallo(memor_dom,'LNNOD','memgeo' , lnnod )
     call memory_deallo(memor_dom,'LELCH','memgeo' , lelch )
     !call memory_deallo(memor_dom,'LNODS','memgeo' , lnods )
     call memory_deallo(memor_dom,'LESUB','memgeo' , lesub )

     !call memory_deallo(memor_dom,'COORD','memgeo' , coord )
     call memory_deallo(memor_dom,'LNOCH','memgeo' , lnoch )

     call memory_deallo(memor_dom,'LNODB','memgeo' , lnodb )
     call memory_deallo(memor_dom,'LBOEL','memgeo' , lboel )
     call memory_deallo(memor_dom,'LTYPB','memgeo' , ltypb )
     call memory_deallo(memor_dom,'LBOCH','memgeo' , lboch )

     call memory_deallo(memor_dom,'SKCOS','memgeo' , skcos )
     !
     ! Dealloctae R_DOM and C_DOM to minimum size (used in deflated CG)
     !
     call memory_deallo(memor_dom,'R_DOM','memgeo' , r_dom )
     call memory_deallo(memor_dom,'C_DOM','memgeo' , c_dom )
     call memory_alloca(memor_dom,'R_DOM','memgeo' , r_dom , 1_ip )
     call memory_alloca(memor_dom,'C_DOM','memgeo' , c_dom , 1_ip )
     !
     ! Deallocate LELPO and PELPO (used in parall)
     !
     !call memory_deallo(memor_dom,'PELPO','memgeo' , pelpo )
     !call memory_deallo(memor_dom,'LELPO','memgeo' , lelpo )
     !
     ! Deallocate memory
     !
     call memose(10_ip)
  case(4_ip)

     call memory_deallo(memor_dom,'COORD','memgeo' , coord )

  case(8_ip)
     !
     ! LELBO
     !
     call memory_alloca(memor_dom,'LELBO','memgeo' , lelbo , nelem )

  case(14_ip)
     !
     ! Allocate SKCOS
     !
     call memory_alloca(memor_dom,'SKCOS','memgeo' , skcos , ndime , ndime , nbopo )

  case(15_ip)
     !
     ! Allocate EXNOR
     !
     call memory_alloca(memor_dom,'EXNOR','memgeo' , exnor , ndime , ndime , nbopo )

  case(-15_ip)
     !
     ! Allocate EXNOR
     !
     call memory_deallo(memor_dom,'EXNOR','memgeo' , exnor )

  case( 19_ip)
     !
     ! Allocate  boundary graph: R_BOU and C_BOU
     !
     if( kfl_crbou == 0 ) then
        call memory_alloca(memor_dom,'R_BOU','memgeo' , r_bou , npoin+1 )
        call memory_alloca(memor_dom,'C_BOU','memgeo' , c_bou , nzbou   )
        kfl_crbou = 1
     end if

  case(-19_ip)
     !
     ! Deallocate boundary graph: R_BOU and C_BOU
     !
     if( kfl_crbou == 1 ) then
        call memory_deallo(memor_dom,'R_BOU','memgeo' , r_bou )
        call memory_deallo(memor_dom,'C_BOU','memgeo' , c_bou )
        kfl_crbou = 0
     end if

  case(24_ip)
     !
     ! Arrays for variable wall distance on boundaries and boundary nodes
     !
     call memory_alloca(memor_dom,'YWALB','memgeo' , ywalb , nboun )
     call memory_alloca(memor_dom,'YWALP','memgeo' , ywalp , nbopo )

  case(-24_ip)
     !
     ! Arrays for variable wall distance
     !
     call memory_deallo(memor_dom,'YWALB','memgeo' , ywalb )
     call memory_deallo(memor_dom,'YWALP','memgeo' , ywalp )

  case(-25_ip)
     !
     ! Deallocate boundary
     !
     call memory_deallo(memor_dom,'LNODB','memgeo' , lnodb )
     call memory_deallo(memor_dom,'LBOEL','memgeo' , lboel )
     call memory_deallo(memor_dom,'LTYPB','memgeo' , ltypb )
     call memory_deallo(memor_dom,'LBOCH','memgeo' , lboch )

  case( 25_ip)
     !
     ! Allocate boundary
     !
     call memory_alloca(memor_dom,'LNODB','memgeo' , lnodb , mnodb   , nbou1 )
     call memory_alloca(memor_dom,'LBOEL','memgeo' , lboel , mnodb+1 , nbou1 )
     call memory_alloca(memor_dom,'LTYPB','memgeo' , ltypb , nbou1 )
     call memory_alloca(memor_dom,'LBOCH','memgeo' , lboch , nbou1 )

  case ( 27_ip )
     !
     ! Groups for deflated CG
     !
     call memory_alloca(memor_dom,'LGROU_DOM','memgeo' , lgrou_dom , npoin )

  case ( -27_ip )
     !
     ! Groups for deflated CG
     !
     call memory_deallo(memor_dom,'LGROU_DOM','memgeo' , lgrou_dom )

  case ( 28_ip )
     !
     ! Fields: allocate
     !
     call memory_alloca(memor_dom,'KFL_FIELD','memgeo' , kfl_field , 4_ip , nfiel )
     call memory_alloca(memor_dom,'XFIEL'    ,'memgeo' , xfiel , nfiel )
     call memory_alloca(memor_dom,'TIME_FIELD','memgeo' , time_field , nfiel )
     call memory_alloca(memor_dom,'X_TRAN_FIEL','memgeo' , x_tran_fiel , nfiel )
     call memory_alloca(memor_dom,'K_TRAN_FIEL','memgeo' , k_tran_fiel , nfiel )

  case ( -28_ip )
     !
     ! Fields: deallocate
     !
     call memory_deallo(memor_dom,'KFL_FIELD','memgeo' , kfl_field )
     call memory_deallo(memor_dom,'XFIEL'    ,'memgeo' , xfiel     )
     call memory_deallo(memor_dom,'TIME_FIELD','memgeo' , time_field   )
     call memory_deallo(memor_dom,'X_TRAN_FIEL','memgeo' , x_tran_fiel )
     call memory_deallo(memor_dom,'K_TRAN_FIEL','memgeo' , k_tran_fiel ) 

  case ( 29_ip )
     !
     ! Fields values: allocate
     !
     if(      kfl_field(2,igene) == NELEM_TYPE ) then
        call memory_alloca(memor_dom,'XFIEL%A','memgeo' , xfiel(igene) % a , kfl_field(1,igene) * kfl_field(4,igene) , nelem )
     else if( kfl_field(2,igene) == NPOIN_TYPE ) then
        call memory_alloca(memor_dom,'XFIEL%A','memgeo' , xfiel(igene) % a , kfl_field(1,igene) * kfl_field(4,igene) , npoin )
     else if( kfl_field(2,igene) == NBOUN_TYPE ) then
        call memory_alloca(memor_dom,'XFIEL%A','memgeo' , xfiel(igene) % a , kfl_field(1,igene) * kfl_field(4,igene) , nbou1 )
     end if
     call memory_alloca(memor_dom,'TIME_FIELD%A','memgeo' , time_field(igene) % a , kfl_field(4,igene) )

  case ( -29_ip )
     !
     ! Fields values: deallocate
     !
     call memory_deallo(memor_dom,'XFIEL%A','memgeo' , xfiel(igene) % a )
     call memory_deallo(memor_dom,'TIME_FIELD%A','memgeo' , time_field(igene) % a )

  case (  32_ip )
     !
     ! VMASS
     !
     call memory_alloca(memor_dom,'VMASS','memgeo' , vmass , npoin )

  case ( -32_ip )
     !
     ! VMASS
     !
     call memory_deallo(memor_dom,'VMASS','memgeo' , vmass )

  case (  33_ip )
     !
     ! VMASC
     !
     call memory_alloca(memor_dom,'VMASC','memgeo' , vmasc , npoin )

  case ( -33_ip )
     !
     ! VMASC
     !
     call memory_deallo(memor_dom,'VMASC','memgeo' , vmasc )

  case( 34_ip )
     !
     ! Allocate materials 
     !
     call memory_alloca(memor_dom,'LMATE','memgeo' , lmate , nelem )

  case ( -34_ip )
     !
     ! Deallocate materials LMATE
     !
     call memory_deallo(memor_dom,'LMATE','memgeo' , lmate )

  case( 35_ip )
     !
     ! Allocate materials: LMATN and NMATN
     !
     call memory_alloca(memor_dom,'LMATN','memgeo' , lmatn , npoin )
     call memory_alloca(memor_dom,'NMATN','memgeo' , nmatn , nmate )

  case ( -35_ip )
     !
     ! Deallocate materials: LMATN and NMATN
     !
     call memory_deallo(memor_dom,'LMATN','memgeo' , lmatn )
     call memory_deallo(memor_dom,'NMATN','memgeo' , nmatn )

  case( 36_ip)
     !
     ! LMATN(IMATE) % L
     !
     call memory_alloca(memor_dom,'LMATN(IMATE)%L','memgeo' , lmatn(igene)%l , igen2 )

  case(-36_ip)
     !
     ! LMATN(IMATE) % L
     !
     call memory_deallo(memor_dom,'LMATN(IMATE)%L','memgeo' , lmatn(igene)%l )

  case( 40_ip)
     !
     ! LFCNT and LNCNT: Allocate lfcnt
     !
     call memory_alloca(memor_dom,'LFCNT','memgeo' , lfcnt , 5_ip , necnt )
     call memory_alloca(memor_dom,'LNCNT','memgeo' , lncnt , 2_ip , nncnt )

  case(-40_ip)
     !
     ! LFCNT and LNCNT: Dellocate lfcnt
     !
     call memory_deallo(memor_dom,'LFCNT','memgeo' , lfcnt )
     call memory_deallo(memor_dom,'LNCNT','memgeo' , lncnt )

  case( 41_ip)
     !
     ! PELEL
     !
     kfl_pelel = 1
     call memory_alloca(memor_dom,'PELEL','memgeo' , pelel , nelem+1_ip )

  case(-41_ip)
     !
     ! PELEL
     !
     kfl_pelel = 0
     call memory_deallo(memor_dom,'PELEL','memgeo' , pelel )

  case( 43_ip)
     !
     ! LELEL
     !
     call memory_alloca(memor_dom,'LELEL','memgeo' , lelel , nedge )

  case(-43_ip)
     !
     ! LELEL
     !
     call memory_deallo(memor_dom,'LELEL','memgeo' , lelel )

  case( 49_ip)
     !
     ! Allocate zones: elements
     !
     call memory_alloca(memor_dom,'NELEZ','memgeo' , nelez , nzone )
     call memory_alloca(memor_dom,'LELEZ','memgeo' , lelez , nzone )

  case(-49_ip)
     !
     ! Deallocate zones: elements
     !
     call memory_deallo(memor_dom,'NELEZ','memgeo' , nelez )
     call memory_deallo(memor_dom,'LELEZ','memgeo' , lelez )

  case( 50_ip)
     !
     ! Allocate zones: nodes
     !
     call memory_alloca(memor_dom,'NPOIZ','memgeo' , npoiz , nzone+1_ip , 'INITIALIZE' , 0_ip )
     call memory_alloca(memor_dom,'LPOIZ','memgeo' , lpoiz , nzone+1_ip , 'INITIALIZE' , 0_ip )

  case(-50_ip)
     !
     ! Deallocate zones: nodes
     !
     call memory_deallo(memor_dom,'NPOIZ','memgeo' , npoiz )
     call memory_deallo(memor_dom,'LPOIZ','memgeo' , lpoiz )

  case( 51_ip)
     !
     ! Allocate zones IGENE
     !
     izone = igene
     call memory_alloca(memor_dom,'LELEZ(IZONE)%L','memgeo' , lelez(izone) % l, nelez(izone) )

  case(-51_ip)
     !
     ! Deallocate one zone 
     !
     izone = igene
     call memory_deallo(memor_dom,'LELEZ(IZONE)%L','memgeo' , lelez(izone) % l )

  case( 52_ip)
     !
     ! Allocate zones IGENE
     !
     izone = igene
     call memory_alloca(memor_dom,'LPOIZ(IZONE)%L','memgeo' , lpoiz(izone) % l, npoiz(izone) )

  case(-52_ip)
     !
     ! Deallocate one zone 
     !
     izone = igene
     call memory_deallo(memor_dom,'LPOIZ(IZONE)%L','memgeo' , lpoiz(izone) % l )

  case(-511_ip)
     !
     ! Deallocate all zones 
     !
     do izone = 1,nzone
        call memory_deallo(memor_dom,'LELEZ(IZONE)%L','memgeo' , lelez(izone) % l )
     end do

  case( 53_ip)
     !
     ! LPERI: periodicity
     !
     call memory_alloca(memor_dom,'LPERI','memgeo' , lperi , 2_ip , nperi )

  case(-53_ip)
     !
     ! LPERI: periodicity
     !
     call memory_deallo(memor_dom,'LPERI','memgeo' , lperi )

  case( 54_ip)
     !
     ! LEZDO and LBZDO
     !
     call memory_alloca(memor_dom,'LEZDO','memgeo' , lezdo , mnode , mnode , nelem )
     call memory_alloca(memor_dom,'LBZDO','memgeo' , lbzdo , mnodb , mnodb , nbou1 )

  case(-54_ip)
     !
     ! LEZDO and LBZDO
     !
     call memory_deallo(memor_dom,'LEZDO','memgeo' , lezdo )
     call memory_deallo(memor_dom,'LBZDO','memgeo' , lbzdo )

  case( 55_ip)
     !
     ! Allocate zones: boundaries
     !
     call memory_alloca(memor_dom,'NBOUZ','memgeo' , nbouz , nzone )
     call memory_alloca(memor_dom,'LBOUZ','memgeo' , lbouz , nzone )

  case(-55_ip)
     !
     ! Deallocate zones: boundaries
     !
     call memory_deallo(memor_dom,'NBOUZ','memgeo' , nbouz )
     call memory_deallo(memor_dom,'LBOUZ','memgeo' , lbouz )

  case( 56_ip)
     !
     ! Allocate zones IGENE
     !
     izone = igene
     call memory_alloca(memor_dom,'LBOUZ(IZONE) % L','memgeo' , lbouz(izone) % l , nbouz(izone) )

  case(-56_ip)
     !
     ! Deallocate all zones 
     !
     izone = igene
     call memory_deallo(memor_dom,'LBOUZ(IZONE) % L','memgeo' , lbouz(izone) % l )

  case( 58_ip)
     !
     ! Renumber mesh when node numbering has changed
     !
     call memory_renumber(memor_dom,'COORD','memgeo',coord,gisca)
     call memory_renumber(memor_dom,'LNOCH','memgeo',lnoch,gisca)
     do ielem = 1,nelem
        do inode = 1,lnnod(ielem)
           ipoin = lnods(inode,ielem)
           kpoin = gisca(ipoin)
           lnods(inode,ielem) = kpoin
        end do
     end do
     do iboun = 1,nboun
        do inodb = 1,nnode(abs(ltypb(iboun)))
           ipoin = lnodb(inodb,iboun)
           kpoin = gisca(ipoin)
           lnodb(inodb,iboun) = kpoin
        end do
     end do
     do ifiel = 1,nfiel
        if( kfl_field(2,ifiel) == NPOIN_TYPE ) then
           call memory_renumber(memor_dom,'COORD','memgeo',xfiel(ifiel) % a,gisca)
        end if
     end do

  case ( 59_ip )
     !
     ! Which zones and subdomain I'm in   
     !     
     call memory_alloca(memor_dom,'I_AM_IN_ZONE','memgeo',I_AM_IN_ZONE,nzone+1,'INITIALIZE',0_ip)
     call memory_alloca(memor_dom,'I_AM_IN_SUBD','memgeo',I_AM_IN_SUBD,nsubd+1,'INITIALIZE',0_ip)

  case (-59_ip )
     !
     ! Which zones and subdomain I'm in   
     !     
     call memory_deallo(memor_dom,'I_AM_IN_ZONE','memgeo',I_AM_IN_ZONE)
     call memory_deallo(memor_dom,'I_AM_IN_SUBD','memgeo',I_AM_IN_SUBD)

  case ( 60_ip )
     !
     ! List of boundary nodes
     !     
     call memory_alloca(memor_dom,'LBONO','memgeo',lbono,nbono)

  case (-60_ip )
     !
     ! List of boundary nodes
     !     
     call memory_deallo(memor_dom,'LBONO','memgeo',lbono)

  case ( 61_ip )
     !
     ! Number of nodes per boundary
     !
     call memory_alloca(memor_dom,'LNNOB','memgeo' , lnnob ,  nboun )

  case (-61_ip )
     !
     ! Number of nodes per boundary
     !
     call memory_deallo(memor_dom,'LNNOB','memgeo' , lnnob )

  case( 62_ip)
     !
     ! Allocate list of masters
     !
     if( nperi > 0 ) call memory_alloca(memor_dom,'LMAST','memgeo' , lmast , npoin )

  case(-62_ip)
     !
     ! Deallocate list of masters
     !
     call memory_deallo(memor_dom,'LMAST','memgeo' , lmast )

  case (  63_ip )
     !
     ! lumma - for use in dual time step preconditioner
     !
     call memory_alloca(memor_dom,'LUMMA','memgeo' , lumma , npoin )      !nzdom * ndime * ndime    ! If I want full matrix

  case ( -63_ip )
     !
     ! lumma
     !
     call memory_deallo(memor_dom,'LUMMA','memgeo' , lumma )


  end select

end subroutine memgeo
