subroutine reatyp(kfl_icgns,neleg,ktype,ltypg,lexig)
  !-----------------------------------------------------------------------
  !****f* Domain/reatyp
  ! NAME
  !    reatyp
  ! DESCRIPTION
  !    Allocate the geometry arrays and read or define them
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_memchk
  use mod_iofile
  implicit none
  integer(ip), intent(in)  :: kfl_icgns,neleg
  integer(ip), intent(out) :: ktype,ltypg(*),lexig(*)
  integer(ip)              :: ielty,ielem,ipara,dummi
  integer(ip)              :: ltnew

  if(words(2)=='ALL  ') then
     !
     ! All elements are of the same type
     !
     ktype = 1
     ielty = int(param(2))
     if( ielty == 0 ) then
        ipara = 2
        do while(trim(words(ipara))/="")
           call elmstr(words(ipara),ielty)
           if( ielty >= 2 .and. ielty <= nelty ) lexig(ielty)=1
           ipara=ipara+1
        end do
     end if
     do ielem=1,neleg
        ltypg(ielem)=ielty
     end do
     ktype=neleg
     do while(words(1)/='ENDTY')
        call ecoute('reageo')
     end do

  else

     ktype=0
     call livinf(27_ip,' ',0_ip)
     if(kfl_icgns==1) then
        !
        ! CGNS type, old Alya type
        !
        do ielem=1,neleg
           read(nunit,*,err=1) dummi,ielty
           ielty=ltnew(ielty)
           lexig(ielty)=1
           ltypg(ielem)=ielty
        end do
        ktype=neleg
     else
        !
        ! Alya type as defined in def_elmtyp
        !
        do ielem=1,neleg
           read(nunit,*,err=1) dummi,ielty
           lexig(ielty)=1
           ltypg(ielem)=ielty
        end do
        ktype=neleg        
     end if
     call ecoute('reageo')
     if(words(1)/='ENDTY')&
          call runend('REAGEO: WRONG TYPES OF ELEMENT FIELD')
  end if

  return

1 call runend('REAGEO: WRONG NUMBER OF NODES FOR ELEMENT '//trim(intost(ielem)))

end subroutine reatyp

function ltnew(ityol)
  !-----------------------------------------------------------------------
  !
  ! Converts old type to new type
  !
  !-----------------------------------------------------------------------
  use def_kintyp
  integer(ip) :: ltnew,ityol
  
  if (ityol ==  2) ltnew= 2
  if (ityol ==  3) ltnew= 3
  if (ityol ==  4) ltnew= 4
  if (ityol ==  5) ltnew= 10
  if (ityol ==  6) ltnew= 11
  if (ityol ==  7) ltnew= 12
  if (ityol ==  8) ltnew= 13
  if (ityol ==  9) ltnew= 14
  if (ityol == 10) ltnew= 30
  if (ityol == 11) ltnew= 31
  if (ityol == 12) ltnew= 32
  if (ityol == 13) ltnew= 33
  if (ityol == 14) ltnew= 34
  if (ityol == 15) ltnew= 35
  if (ityol == 16) ltnew= 36
  if (ityol == 17) ltnew= 37
  if (ityol == 18) ltnew= 38
  if (ityol == 19) ltnew= 39  
  
end function ltnew
