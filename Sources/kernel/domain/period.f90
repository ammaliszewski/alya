!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    period.f90
!> @author  Guillaume Houzeaux
!> @brief   This routine modifies LNODS to account for periodicity
!> @details Periodicity is implemented as follows:
!>          \verbatim
!>
!>          Input:  lnods(1:mnode,1:nelem)
!>          Output: lnods(1:2*mnode,nelem)
!>
!>          1. nperi= numero de master-slave couples
!>             NB: a master can have various slaves
!>          2. lperi(2,1:nperi) = list of slave-master couples
!>              lperi(1,iperi) = master
!>              lperi(2,iperi) = slave
!>          3. If ielem is an element containing a slave node:
!>             add the corresponding master to the element connectivity
!>             of ielem: lnods(1:2*mnode:ielem)
!>             Examples: lnods(1:3,ielem) = 23,27,31
!>                       lperi(1,56) = 127
!>                       lperi(2,56) = 23
!>             => lnods(1:4,ielem) = 23,27,31,127
!>          4. Construct node-node graph (R_DOM,C_DOM) and node-element 
!>             graph (PELPO,LELPO) using this modified lnods
!>          5. Recover original lnode
!>          6. After calling METIS, each time a slave is in a subdomain,
!>             then the master will. Note that the partitioning will reflect
!>             the presence of periodicity only if the partition is carried
!>             out using the common-node criterion (not the face one) as
!>             it uses LELPO and PELPO
!>          7. During the assembly of a matrix in parallel, put all
!>             the coefficients related to a slave to the master position
!>             (row and columns). Do the same for RHS.
!>          8. The slave wil have a null row and column. At the end
!>             of the solver put the slave (jpoin) value to its master 
!>             counterpart (ipoin): xx(jpoin) = xx(ipoin)
!>
!>          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine period(itask)
  use def_parame
  use def_master
  use def_domain
  use mod_memory
  implicit none
  integer(ip), intent(in)    :: itask
  integer(ip)                :: ipoin,ielem,inode,jpoin,iperi,kperi
  integer(ip)                :: knode,ii
  integer(4)                 :: istat
  integer(ip), pointer, save :: lnods_tmp(:,:) 
  integer(ip), pointer, save :: lnods_sav(:,:) 
  integer(ip), pointer, save :: lnnod_tmp(:)   
  integer(ip), pointer, save :: lnnod_sav(:)   
  integer(ip),          save :: mnode_sav

  if( nperi > 0 .and. IMASTER ) then

     if( itask == 1 ) then
        !
        ! Change LNODS, LNNOD
        !
        lnnod_sav => lnnod
        lnods_sav => lnods
        mnode_sav =  mnode

        mnode = 2 * mnode 

        nullify(lnods_tmp)
        nullify(lnnod_tmp)
        call memory_alloca(memor_dom,'LNODS_TMP','period',lnods_tmp,mnode+1,nelem)
        call memory_alloca(memor_dom,'LNNOD_TMP','period',lnnod_tmp,nelem)

        do ielem = 1,nelem
           lnnod_tmp(ielem) = lnnod(ielem)
           do inode = 1,lnnod(ielem)
              lnods_tmp(inode,ielem) = lnods(inode,ielem)
           end do
        end do

        call memgen(1_ip,npoin,0_ip)
        !
        ! If IPOIN is a master and JPOINT its slave: GISCA(JPOIN) = IPOIN
        !
        do iperi = 1,nperi
           kperi = size(lperi,1_ip)
           ipoin = lperi(1,iperi)
           do ii = 2,kperi
              jpoin = lperi(ii,iperi)
              gisca(jpoin) = ipoin
           end do
        end do

        do ielem = 1,nelem
           knode = lnnod(ielem)
           do inode = 1,lnnod(ielem)
              ipoin = lnods_tmp(inode,ielem)
              if( gisca(ipoin) > 0 ) then
                 knode = knode + 1
                 lnods_tmp(knode,ielem) = gisca(ipoin)
              end if
           end do
           lnnod_tmp(ielem) = knode
        end do

        call memgen(3_ip,npoin,0_ip)

        lnnod => lnnod_tmp
        lnods => lnods_tmp

     else
        !
        ! Recover LNODS, LNNOD
        !
        lnnod => lnnod_sav
        lnods => lnods_sav
        mnode =  mnode_sav
     
        call memory_deallo(memor_dom,'LNNOD_TMP','period',lnnod_tmp)
        call memory_deallo(memor_dom,'LNODS_TMP','period',lnods_tmp)

     end if

  end if

end subroutine period
