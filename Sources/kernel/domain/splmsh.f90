!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    crecon.f90
!> @author  Mariano Vazquez
!> @date    02/10/2013
!> @brief   Create contacts
!> @details Create contacts
!> @} 
!-----------------------------------------------------------------------
subroutine splmsh()
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use def_elmtyp
  use mod_memchk
  implicit none
  integer(ip) :: &
       ielty,inode,ipnew,ielem,jelem,kelem,izone,inodc,nnodc,lauxi(100),&
       iface_local,iface,max_ncont,ncont,ipoin,jpoin,npnew,pnode,iboun,inodb,pnodb,icont,&
       condition_solid,condition_fluid,ielem_solid,ielem_fluid,jcont,itype,&
       npoin_fluid,elem_count_fluid,node_count_fluid,bound_count_fluid,node_bound_count_fluid,&
       npoin_solid,elem_count_solid,node_count_solid,bound_count_solid,node_bound_count_solid
  integer(ip), pointer :: &
       lnoco(:,:), poaux(:),lzone_read(:,:),lnocf(:),&
       nodes_fluid(:), elem_fluid(:), lnods_fluid(:,:), lnodb_fluid(:,:), snodeb(:),&
       nodes_solid(:), elem_solid(:), lnods_solid(:,:), lnodb_solid(:,:), fnodeb(:)
  real(rp), pointer :: &
       conew(:,:)
  character(200) :: messa,output_file

  !
  !    LFACG(4,NFACG) and LELFA(1:NELEM) % L(1:NFACE(LTYPE(IELEM)))
  !
  !    LFACG(1,IFACG) = IELEM
  !    LFACG(2,IFACG) = JELEM/0 ... 0 if boundary face
  !    LFACG(3,IFACG) = IFACE ..... Local IELEM face
  !    LFACG(4,IFACG) = JFACE/0 ... Local JELEM face
  !    Caution: 0 < IELEM < NELEM_2 so IELEM can be a neighbor's element
  !
  !    LELFA(IELEM) % L(1:NFACE) ... Global face connectivity for IELEM
  !


  messa='CREATING CONTACTS...'
  call livinf(zero,messa,zero)

  
  ! MAXIMUM VALUE FOR NCONT --> INCREASE IF NEEDED
  max_ncont= 400000
  allocate( lnoco(10,max_ncont) )
  allocate( lnocf(   max_ncont) )
  allocate( conew( 3,max_ncont) )
  allocate( poaux(       npoin) )
  allocate( lzone_read(nzone,nelem) )
! *****************************
  allocate( nodes_fluid(npoin) )          ! permutation from old nodes to new fluid nodes
  allocate( elem_fluid(nelem) )           ! permutation from old elements to new elements fluid side
  allocate( lnods_fluid(10,nelem) )       ! new list of nodes per element in the fluid side
  allocate( lnodb_fluid(mnodb+1,nelem) )  ! new lnodb array for the fluid

  allocate( nodes_solid(npoin) )          ! permutation from old nodes to new solid nodes
  allocate( elem_solid(nelem) )           ! permutation from old elements to new elements solid side
  allocate( lnods_solid(10,nelem) )       ! new list of nodes per element in the soli side
  allocate( lnodb_solid(mnodb+1,nelem) )  ! new lnodb array for the solid

  allocate( fnodeb(nboun) )               ! permutation from old boundaries to new boundaries fluid side
  allocate( snodeb(nboun) )               ! permutation from old boundaries to new boundaries solid side


  !
  ! Compute all geometrical stuff required 
  !

  if (.not.associated(ltype)) allocate(ltype(        nelem))
  if (.not.associated(lelch)) allocate(lelch(        nelem))
  if (.not.associated(lpoty)) allocate(lpoty(        npoin))

  if (associated(nepoi)) deallocate(nepoi)
  if (associated(pelpo)) deallocate(pelpo)
  if (associated(lelpo)) deallocate(lelpo)
  if (associated(lelfa)) deallocate(lelfa)
  if (associated(facel)) deallocate(facel)
  if (associated(lfacg)) deallocate(lfacg)
  if (associated(lelbf)) deallocate(lelbf)

  messa='   TOTAL ELEMENTS'
  call livinf(-7_ip,messa,          nelem)
  messa='   ELEMENTS IN ZONE 1'
  call livinf(-7_ip,messa,nelez(1))
  messa='   ELEMENTS IN ZONE 2'
  call livinf(-7_ip,messa,nelez(2))

  lzone_read = 0_ip
  do izone=1,nzone
     do kelem = 1,nelez(izone)
        ielem = lelez(izone) % l(kelem)
        lzone_read(izone,ielem) = izone
     end do
  end do

! ***************************************************************************
! Creates the permutation arrays to go from original numbering to the new one
  node_count_fluid  = 0_ip
  node_count_solid  = 0_ip

  elem_count_fluid  = 0_ip
  elem_count_solid  = 0_ip

  elem_fluid  =-1_ip
  elem_solid  =-1_ip

  nodes_fluid =-1_ip
  nodes_solid =-1_ip
  !
  ! Search and renumerate elements
  ! 
  ! ------------------------------
  do ielem = 1, nelem
! print*, "DEBUG: ", lzone_read(ielem)
     if (lzone_read(1_ip,ielem) == 1_ip) then
        elem_count_fluid   = elem_count_fluid + 1_ip
        elem_fluid(ielem)  = elem_count_fluid
! print*, "DEBUG: ", ielem, elem_fluid(ielem)
        pnode = lnnod(ielem)
        do inode = 1, pnode
           ipoin = lnods(inode,ielem)
           if (nodes_fluid(ipoin) == -1_ip) then 
              node_count_fluid   = node_count_fluid + 1
              nodes_fluid(ipoin) = node_count_fluid
              lnods_fluid(inode,elem_fluid(ielem))   = nodes_fluid(ipoin)
           else                                       ! Using an already renumbered node
              lnods_fluid(inode,elem_fluid(ielem))   = nodes_fluid(ipoin)
           endif
        end do
     else
        elem_count_solid  = elem_count_solid + 1_ip
        elem_solid(ielem) = elem_count_solid
! print*, "DEBUG: ", ielem, elem_fluid(ielem)
        pnode = lnnod(ielem)
        do inode = 1, pnode
           ipoin = lnods(inode,ielem)
           if (nodes_solid(ipoin) == -1_ip) then 
              node_count_solid   = node_count_solid + 1
              nodes_solid(ipoin) = node_count_solid
              lnods_solid(inode,elem_solid(ielem))   = nodes_solid(ipoin)
           else                                       ! Using an already renumbered node
              lnods_solid(inode,elem_solid(ielem))   = nodes_solid(ipoin)
           endif
        end do
     endif
  end do

  !
  ! Search and renumerate boundaries
  !
  ! --------------------------------
  snodeb =-1_ip
  fnodeb =-1_ip

  lnodb_fluid =-1_ip
  lnodb_solid =-1_ip

  bound_count_fluid = 0_ip
  bound_count_solid = 0_ip

  node_bound_count_fluid = 0_ip
  node_bound_count_solid = 0_ip  

  do iboun = 1_ip, nboun
     pnodb  = nnode(ltypb(iboun))
     ielem  = lboel((pnodb+1),iboun)
     do inodb = 1_ip, mnodb
        ipoin = lnodb(inodb,iboun)
        if (nodes_fluid(ipoin) == -1_ip) exit
        node_bound_count_fluid = node_bound_count_fluid + 1_ip
     end do
     if (node_bound_count_fluid == mnodb) then
! print*, "DEBUG b) ", ielem, elem_fluid(ielem)
! print*, "DEBUG : iboun, ielem ", bound_count, ielem, lzone_read(ielem)
        bound_count_fluid = bound_count_fluid + 1_ip
        fnodeb(iboun)     = bound_count_fluid
! print*, "DEBUG2: ielem ", bound_count, ielem, permcnt(ielem), kfl_codbo(iboun)
        do inodb = 1, mnodb
           lnodb_fluid(inodb,fnodeb(iboun)) = nodes_fluid(lnodb(inodb,iboun))
        end do
        lnodb_fluid(mnodb+1,fnodeb(iboun)) = elem_fluid(ielem)
     end if
     node_bound_count_fluid = 0_ip
  end do

  do iboun = 1_ip, nboun
     pnodb  = nnode(ltypb(iboun))
     ielem  = lboel((pnodb+1),iboun)
     do inodb = 1_ip, mnodb
        ipoin = lnodb(inodb,iboun)
        if (nodes_solid(ipoin) == -1_ip) exit
        node_bound_count_solid = node_bound_count_solid + 1_ip
     end do
     if (node_bound_count_solid == mnodb) then
! print*, "DEBUG b) ", ielem, elem_fluid(ielem)
! print*, "DEBUG : iboun, ielem ", bound_count, ielem, lzone_read(ielem)
        bound_count_solid = bound_count_solid + 1_ip
        snodeb(iboun)     = bound_count_solid
! print*, "DEBUG2: ielem ", bound_count, ielem, permcnt(ielem), kfl_codbo(iboun)
        do inodb = 1, mnodb
           lnodb_solid(inodb,snodeb(iboun)) = nodes_solid(lnodb(inodb,iboun))
        end do
        lnodb_solid(mnodb+1,snodeb(iboun)) = elem_solid(ielem)
     end if
     node_bound_count_solid = 0_ip
  end do
! ****************************************

  do ielem=1,nelem
     call fintyp(ndime,lnnod(ielem),ielty)
     lexis(ielty)= 1
     ltype(ielem)= ielty
  end do

  call cderda
  call connpo

  nelem_2 = nelem
  pelpo_2 => pelpo
  lelpo_2 => lelpo

  kfl_lface= 1
  kfl_lelbf= 0

  lpoty = 1   ! esto es necesario

  call lgface

  !
  ! Compute contacts
  !

  ncont= 0
  poaux= 0
  ipnew= 0
  do ielem= 1,nelem
     izone= lzone_read(1_ip,ielem)
     inodc= 0
     lauxi= 0
     
     if (izone == 1) then                    ! solo mirar los de la zona 1

        ielty= abs(ltype(ielem))
        do iface_local= 1,nface(ielty)
           iface= lelfa(ielem)%l(iface_local)
           ! ielem cae en la zona 1 (fluido)
           ! jelem es el elemento del otro lado de la cara 
           jelem= lfacg(1,iface)
           if (ielem == jelem) jelem= lfacg(2,iface)
           if (jelem > 0) then
              if (izone .ne. lzone_read(1_ip,jelem)) then   
                 ! contacto identificado, porque jelem y ielem no estan en la misma zona
                 ncont= ncont+1        ! numero de contactos
                 if (ncont > max_ncont) then
                    call runend("CRECON: INCREASE MAX_NCONT AND RECOMPILE")
                 else
                    nnodc = nnodf(ielty) % l(iface_local)
                    do inodc =1, nnodc
                       inode = lface(ielty)%l(inodc,iface_local) 
                       ! los nodos viejos del contacto se quedan del lado del solido, o sea jelem
                       ! y los nuevos iran en poaux y son los del lado del fluido, o sea ielem
                       ipoin = lnods(inode,ielem)
                       jpoin = ipoin
!                        if (poaux_fluid(ipoin) == 0) then
                       if (poaux(ipoin) == 0) then
                          ipnew= ipnew+1
                          poaux(ipoin) = ipnew + npoin  ! la numeracion nueva para el nodo nuevo
!                           poaux_fluid(ipoin) = ipnew + npoin_fluid
                       end if
                       ! *******************************************************
                       ! Creates the new boundaries with the original numeration
                       ! the traduction will be performed later
                       lnoco(inodc      ,ncont)= nodes_solid(jpoin) ! lado del solido
                       lnoco(inodc+nnodc,ncont)= nodes_fluid(ipoin) ! lado del fluido
                       ! ******************************************************
!                        lnoco(inodc+nnodc,ncont) = poaux_fluid(ipoin)
                    end do
                    ! el elemento que esta del lado solido 
                    lnoco(nnodc*2+1,ncont) = elem_solid(jelem)   
                    ! el elemento que esta del lado fluido y que usara nodo nuevo
                    lnoco(nnodc*2+2,ncont) = elem_fluid(ielem) 

                    ! en 2d, la parte del contacto con nodos nuevos (fluido) va en orden inverso
                    if (ndime==2) then
                       do inodc= 1,nnodc
                          lauxi(nnodc - inodc + 1)= lnoco(inodc,ncont)  
                       end do
                       lnoco(1:nnodc,ncont)  = lauxi(1:nnodc) 
                    end if
                    
                    lnocf(ncont) = nnodc

                 end if
              end if
           end if
        end do
     end if
  end do  
  npnew= ipnew
print*, "DEBUG: npnew", npnew

  messa='   CONTACTS'
  call livinf(-7_ip,messa,ncont)
  messa='   NEW POINTS'
  call livinf(-7_ip,messa,npnew+npoin)
  messa='   OLD POINTS'
  call livinf(-7_ip,messa,npoin)

  messa='   NEW ELEMENTS'
  call livinf(-7_ip,messa,nelem+ncont)
  messa='   OLD ELEMENTS'
  call livinf(-7_ip,messa,nelem)

  open (616,file='README.CONTACTS')

  messa='INSTRUCTIONS AFTER CREATING CONTACTS'
  write(616,*) trim(messa)
  messa=' '
  write(616,*) trim(messa)

  messa='   NEW FILES TO BE INCLUDED IN THE NEW DOM DAT:'
  write(616,*) trim(messa)

  messa='   WRITE NEW FILES...'
  call livinf(zero,messa,zero)
  
!!$  ! write the new files
!!$  output_file = 'spl-zones.geo.dat'  
!!$  open (606,file=output_file)
!!$  messa = '      '//output_file
!!$  write(616,*) '  INCLUDE',trim(messa)
!!$  call livinf(zero,messa,zero)
!!$  write(606,*) 'ZONES'
!!$  write(606,*) 'ZONE, NUMBER = 1'
!!$  do kelem= 1,nelez(1)
!!$     ielem = lelez(1) % l(kelem)
!!$     write(606,*) ielem
!!$  end do
!!$  write(606,*) 'END_ZONE'
!!$  write(606,*) 'ZONE, NUMBER = 2'
!!$  do kelem= 1,nelez(2)
!!$     ielem = lelez(2) % l(kelem)
!!$     write(606,*) ielem
!!$  end do
!!$  write(606,*) 'END_ZONE'
!!$  write(606,*) 'ZONE, NUMBER = 3'
!!$  do kelem=1,ncont
!!$     ielem = kelem+nelem
!!$     write(606,*) ielem
!!$  end do
!!$  write(606,*) 'END_ZONE'
!!$  write(606,*) 'END_ZONES'
!!$
!!$  itype = 3
!!$  write(606,'(a)') 'CHARACTERISTICS'
!!$  do icont= 1,ncont
!!$     write(606,100) icont+nelem, itype
!!$  end do
!!$  write(606,'(a)') 'END_CHARACTERISTICS'
!!$  write(606,*) 
!!$  close(606)

  output_file = 'spl-nodeselements-fluid.geo.dat'  
  open (606,file=output_file)
  open (607,file='spl-nodeselements-solid.geo.dat')
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  write(606,'(a)') 'NODES_PER_ELEMENT'
  write(607,'(a)') 'NODES_PER_ELEMENT'
  do ielem= 1,nelem
     if (lzone_read(1_ip,ielem) == 1) write(606,100) elem_fluid(ielem), lnnod(ielem)
     if (lzone_read(2_ip,ielem) == 2) write(607,100) elem_solid(ielem), lnnod(ielem)     
  end do
!   do icont= 1,ncont
!      nnodc= lnocf(icont) 
!      write(606,100) icont+nelem, nnodc*2
!   end do
  write(606,'(a)') 'END_NODES_PER_ELEMENT'
  write(607,'(a)') 'END_NODES_PER_ELEMENT'
  close(606)
  close(607)

  output_file = 'spl-elements-fluid.geo.dat'  
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  open (606,file=output_file)
  open(607,file='spl-elements-solid.geo.dat')
  write(606,'(a)') 'ELEMENTS'
  write(607,'(a)') 'ELEMENTS'
  do ielem= 1,nelem
     if (lzone_read(1_ip,ielem) == 1_ip) write(606,100) elem_fluid(ielem),lnods_fluid(1:lnnod(ielem),elem_fluid(ielem))
     if (lzone_read(2_ip,ielem) == 2_ip) write(607,100) elem_solid(ielem),lnods_solid(1:lnnod(ielem),elem_solid(ielem))
  end do

  write(606,'(a)') 'END_ELEMENTS'
  write(607,'(a)') 'END_ELEMENTS'
  close(606)
  close(607)

  output_file = 'spl-coordinates-fluid.geo.dat'  
  open (606,file=output_file)
  open (607,file='spl-coordinates-solid.geo.dat')
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  write(606,'(a)') 'COORDINATES,NOT_SORTED '
  write(607,'(a)') 'COORDINATES,NOT_SORTED '
  do ipoin= 1,npoin
     if (nodes_fluid(ipoin) /= -1_ip) write(606,200) nodes_fluid(ipoin),coord(1:ndime,ipoin)
     if (nodes_solid(ipoin) /= -1_ip) write(607,200) nodes_solid(ipoin),coord(1:ndime,ipoin)
  end do

  write(606,'(a)') 'END_COORDINATES'
  write(607,'(a)') 'END_COORDINATES'
  close(606)
  close(607)

  output_file = 'spl-boundaries-fluid.geo.dat'  
  open (606,file=output_file)
  open (607,file='spl-boundaries-solid.geo.dat')
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  output_file = 'spl-bocon-fluid.geo.dat'  
  messa = '      '//output_file
  call livinf(zero,messa,zero)
  write(616,*) '  INCLUDE',trim(messa)
  open (608,file=output_file)
  open (609,file='spl-bocon-solid.geo.dat')

  write(606,'(a)') 'BOUNDARIES, ELEMENT'
  write(607,'(a)') 'BOUNDARIES, ELEMENT'

  write(608,'(a)') 'BOUNDARY_CONDITIONS, EXTRAPOLATE'
  write(608,'(a)') 'ON_BOUNDARIES,UNKNOWN'

  write(609,'(a)') 'BOUNDARY_CONDITIONS, EXTRAPOLATE'
  write(609,'(a)') 'ON_BOUNDARIES,UNKNOWN'
  ! old boundaries with corrected lnodb
  do iboun= 1,nboun
     pnodb = nnode(ltypb(iboun))
     ielem = lboel((pnodb+1),iboun)
     if (fnodeb(iboun) /=-1_ip) write(606,100) fnodeb(iboun),lnodb_fluid(1:mnodb,fnodeb(iboun)),elem_fluid(ielem)          ! boundary element
     if (snodeb(iboun) /=-1_ip) write(607,100) snodeb(iboun),lnodb_solid(1:mnodb,snodeb(iboun)),elem_solid(ielem)          ! boundary element
     if (fnodeb(iboun) /=-1_ip) write(608,100) fnodeb(iboun),mnodb,lnodb_fluid(1:mnodb,fnodeb(iboun)),kfl_codbo(iboun)     ! boundary condition
     if (snodeb(iboun) /=-1_ip) write(609,100) snodeb(iboun),mnodb,lnodb_solid(1:mnodb,snodeb(iboun)),kfl_codbo(iboun)     ! boundary condition
  end do

  ! new boundaries:
  ! those for the new contacts in 606 and in 608
  ! for the contact, they are 30 on the fluid side and 40 on the solid side

  condition_fluid= 20
  condition_solid= 24

  jcont= 0
  do icont= 1,ncont

     ielem_solid = lnoco(nnodc*2+1,icont)
     ielem_fluid = lnoco(nnodc*2+2,icont)
     jcont= jcont+1
     write(607,100) (jcont+1)/2_ip + bound_count_solid,        lnoco(      1:nnodc  ,icont), ielem_solid
     write(609,100) (jcont+1)/2_ip + bound_count_solid, mnodb, lnoco(      1:nnodc  ,icont), condition_solid

     jcont= jcont+1
     write(606,100) jcont/2_ip+bound_count_fluid,        lnoco(nnodc+1:nnodc*2,icont), ielem_fluid
     write(608,100) jcont/2_ip+bound_count_fluid, mnodb, lnoco(nnodc+1:nnodc*2,icont), condition_fluid

  end do
  write(606,'(a)') 'END_BOUNDARIES'
  write(607,'(a)') 'END_BOUNDARIES'

  write(608,'(a)') 'END_ON_BOUNDARIES'
  write(608,'(a)') 'END_BOUNDARY_CONDITIONS'
  
  write(609,'(a)') 'END_ON_BOUNDARIES'
  write(609,'(a)') 'END_BOUNDARY_CONDITIONS'
  close(606)
  close(607)
  close(608)
  close(609)

!!$  if (nmate > 1) then
!!$     output_file = 'cnt-materials.geo.dat'  
!!$     open (606,file=output_file)     
!!$     messa = '      '//output_file
!!$     call livinf(zero,messa,zero)
!!$     write(606,*) 'MATERIALS, FLUID_MATERIAL=1, DEFAULT= 1, NUMBER = ',nmate
!!$     write(616,*) '  INCLUDE',trim(messa)
!!$     do ielem=1,nelem
!!$        write(606,*) ielem,lmate(ielem)
!!$     end do
!!$     write(606,*) 'END_MATERIALS'
!!$     close(606)
!!$  end if

  messa='   WRITE NEW FILES... DONE.'
  call livinf(zero,messa,zero)

  messa='CREATING CONTACTS... DONE.'
  call livinf(zero,messa,zero)

  messa=' '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa='VERY IMPORTANT!!'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa=' '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '     DO NOT FORGET TO CORRECT DOM.DAT, INCLUDING:  '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '     1. FOR 3D PROBLEMS, ADD ELEMENT 34 TO THE TYPES OF ELEMENTS    '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        TYPES_OF_ELEMENTS= 0, 0, 0, 0, 0, 30, 0, 0, 0, 34, 0 '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        FOR 2D PROBLEMS, ADD ELEMENT 12 (when needed) TO THE TYPES OF ELEMENTS    '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        TYPES_OF_ELEMENTS= 10, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0 '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '     2. ADD THE NEW BOUNDARY CONDITION TO THE FLUID AND TO ALEFOR:'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        *.NSI.DAT:'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '          30 --> SOLID WALL OR SLIP CONDITION'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        *.ALE.DAT:'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '          30 --> DIRICHLET CONDITION TO 0.0'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '     3. CORRECT THE NUMBER OF ZONES, NOW IT IS ZONES : 3'
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)
  messa= '        CORRECT THE NUMBER OF NODAL_POINTS, NOW IT IS NODAL_POINTS '
  write(616,*) trim(messa), npnew+npoin
  call livinf(-7_ip,messa,npnew+npoin)
  messa= '        CORRECT THE NUMBER OF ELEMENTS, NOW IT IS ELEMENTS'
  write(616,*) trim(messa),nelem+ncont
  call livinf(-7_ip,messa,nelem+ncont)
  messa= '        CORRECT THE NUMBER OF BOUNDARIES, NOW IT IS BOUNDARIES '
  write(616,*) trim(messa),nboun+jcont
  call livinf(-7_ip,messa,nboun+jcont)
  messa=' '
  write(616,*) trim(messa)
  call livinf(zero,messa,zero)

  close(616)
print*, "DEBUG: ", bound_count_fluid, ncont
  output_file = 'spl-dim.dat'
  open (606,file=output_file)
  messa = '      '//output_file
  write(606,'(a)') 'DIMENSIONES'
  write(606,*) ''
  write(606,*) 'NODES FLUID    = ', node_count_fluid
  write(606,*) 'ELEMENTS FLUID = ', elem_count_fluid
  write(606,*) 'BOUNDS FLUID   = ', bound_count_fluid+ncont
  write(606,*) ''
  write(606,*) 'NODES SOLID    = ', node_count_solid
  write(606,*) 'ELEMENTS SOLID = ', elem_count_solid
  write(606,*) 'BOUNDS SOLID   = ', bound_count_solid+ncont
  close(606)

  100 format(10(2x,i6))
  200 format(i8,3(2x,e12.5))

end subroutine splmsh
