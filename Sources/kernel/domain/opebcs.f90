subroutine opebcs(itask)
  !-----------------------------------------------------------------------
  !****f* Domain/opebcs
  ! NAME
  !    opebcs
  ! DESCRIPTION
  !    Operation on boundaries:
  !    - Geometrical boundary conditions
  !    - Extrapolaiton from boundaries to nodes
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: iboun,icond,ipara,ncond,ivalu
  integer(ip)             :: icode,ipoin,icono

  select case( itask )

  case ( 1_ip )
     !
     ! Define used codes
     !
     if( INOTMASTER ) then
        if( kfl_icodn > 0 ) then
           do ipoin = 1,npoin
              do icono = 1,mcono
                 icode = kfl_codno(icono,ipoin) 
                 if( abs(icode) <= mcodb ) lcodn(icode) = .true.
              end do
           end do
        end if
        if( kfl_icodb > 0 ) then
           do iboun = 1,nboun
              icode = kfl_codbo(iboun)
              if( abs(icode) <= mcodb ) lcodb(icode) = .true.
           end do
        end if
        !
        ! Extrapolate from boundary to node
        ! 
        ncodc = 0

        if( kfl_extra == 1 ) then
           if( kfl_icodn == 0 ) then
              kfl_icodn = 1
              call membcs(1_ip)
              do ipoin = 1,npoin
                 kfl_codno(1,ipoin) = mcodb+1
              end do
           end if 
           call extbcs()
        end if
        !
        ! Code only on boundary nodes
        !
!       if( kfl_icodn == 1 ) then
!          do ipoin = 1,npoin
!             if( lpoty(ipoin) == 0 .and. nzone == 1 ) then
!                kfl_codno(1,ipoin) = mcodb+1
!                kfl_codno(2,ipoin) = mcodb+1
!                kfl_codno(3,ipoin) = mcodb+1
!             end if
!          end do
!       end if
        !
        ! Count code numbers
        !
        if( ncodc == 0 ) ncodc = ncodn
        call coubcs(kfl_extra)

     else

        if( kfl_extra == 1 ) then
           call livinf(0_ip,'EXTRAPOLATE BOUNDARY CODES TO NODE CODES',0_ip)
           kfl_icodn = 1
        end if

     end if

  case ( 2_ip )

     if( kfl_camsh == 1 ) return

     if( INOTMASTER ) then
        !
        ! Initializations and allocations
        !
        if( kfl_geome == 1 ) then 

           !-------------------------------------------------------------
           !
           ! Geometrical boundaries
           !
           ! Define boundary codes: 
           ! Prescribed ........ 10
           ! Freestream ........ 20
           ! Wall law .......... 30
           ! Symmetry .......... 40
           ! No slip / Wall .... 50
           ! Free / Outflow .... 60
           !
           !-------------------------------------------------------------
           !
           ! Allocate memory for KFL_GEOBO: includes fringe boundaries
           !
           call membcs(4_ip) 
           if( kfl_icodb <= 0 ) then
              call runend('NO BOUNDARY CODE HAS BEEN IMPOSED: CANNOT GENERATE GEOMETRICAL NORMALS')
           end if
           ncond = size(npbcs)
           do iboun = 1,nboun
              do icond = 1,ncond
                 ivalu = 10*icond
                 do ipara = 1,npbcs(icond)
                    if( kfl_codbo(iboun) == lsbcs(ipara,icond) ) kfl_geobo(iboun) = ivalu
                 end do
              end do
           end do
           call geonor(1_ip)
        end if

     else

        if( kfl_geome == 1 ) then
           call geonor(1_ip)
        end if

     end if

  end select

end subroutine opebcs
