!�����������������������������������
!> @addtogroup kernel/domain
!> @{
!> @file      numdom.f90
!> @author
!> @date 
!> @brief     Computing the groups for the defelated CG
!> @details   This routine computes the groups for the deflated CG.\n
!!            COMPUTE THE GROUPS FOLLOWING THE STREAMWISE VELOCITY
!!
!!    Starting nodes are those node ipoin imposed, that is when\n
!!    LIMPO(ipoin)>0.\n 
!!    We have two possiblities:\n
!!    ITASK = 1 ... NGROU_DOM groups are computed by master\n
!!                  NGROU_DOM = -1: automatic # groups\n
!!    ITASK = 2 ... Groups are computed by slaves:\n
!!                  One group per subdomain\n
!!        
!!    The main variables are 4:\n
!!    
!!     lqueu(:):   array containing the points that a group has.\n
!!     lgrou(:):   array containg the number of gruops (max. number of groups = max. number of points)\n 
!!     lfron(:):   contains the points that are in the front.\n
!!     lmark(:):   contains the points which do not belong to a group.\n
!!
!!     Description of other variables:\n
!!
!!      npogr:  number of points per group. \n
!!      npomi:  minimum number of points per group. \n
!!      nmark:  number of marked points (updated in each loop). \n
!!      nmarkt: total number of marked points. \n
!!      kgrou:  group number. \n
!!      nfron, nfnew: maximum number of points in lfron(:) in each loop (maxnfron = npoin)\n
!!      ipoin, kpoin, jpoin, iqueu, jqueu, igrou: counting variables.\n
!! 
!!      Description of the main loop:\n
!!
!!      open-loop:\n
!!          neigh (loop):...................Loop on the queue. Computes the nonzero ipoin.\n
!!                                          Three conditions are needed to include a point in a group: \n 
!!                                          1)the point doesn't belong to any group,\n 
!!                                          2)the group is not full,\n 
!(!                                          3)the point is not marked (HOLE point)\n
!!
!!          Adding, cleaning and\n
!!          compressing nodes of the front..The last points added are used to make the new front (this is done in three steps,\n
!!                                          using the same array in lfron in each loop to allocate the point in the front.\n
!!          Not enough nodes................Search for other nodes belonging to other groups using the last points added.\n
!!          Final check.....................Check if all the points are marked anf if not search for the ones that are not marked.\n
!!                                                         
!>  @}
!-----------------------------------------------------------------------
subroutine numdom(itask)

  use def_parame
  use def_master
  use def_domain
  use mod_memchk  
  use def_elmtyp
  use mod_postpr

  implicit none
  integer(ip), intent(in)     :: itask
  integer(ip)                 :: ipoin,nqueu,idime,igrou,iqueu, ipinl
  integer(ip)                 :: izdom,jpoin,kpoin
  integer(ip)                 :: nfron,ifron, nneg, npinl, nauxi
  integer(ip)                 :: nmarkt,nmark
  integer(4)                  :: istat
  real(rp), dimension(3)      :: vpoin, vrefe
  real(rp)                    :: cangl, cangm!, scala, modpo, modre
  integer(ip), pointer        :: lqueu(:) 
  integer(ip), pointer        :: lfron(:) 
  integer(ip), pointer        :: lgrou(:)
  integer(ip), pointer        :: inlet(:) 
  logical(lg), pointer        :: lmark(:) 
  integer(ip), pointer        :: lcoor(:)
  logical(lg), pointer        :: lvaux(:)
  logical(lg), pointer        :: lvfro(:)

  nullify(lqueu)
  nullify(lfron)
  nullify(lgrou)
  nullify(inlet)
  nullify(lmark)
  nullify(lcoor)
  nullify(lvaux)
  nullify(lvfro)

  if( INOTSLAVE ) then



     !----------------------------------------------------------------------
     !
     ! Allocate memory 
     !
     !----------------------------------------------------------------------

     call livinf(57_ip,' ',modul)

     ! LGROU: allocate memory
     !     
     call memgeo(27_ip)
     lgrou => lgrou_dom
     nmarkt = npoin
     npinl = 0 

     !
     ! Fill in LQUEU, LMARK, LFRON
     !
     allocate(lqueu(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LQUEU','numdom',lqueu)
     allocate(lmark(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LMARK','numdom',lmark)
     allocate(lfron(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LFRON','numdom',lfron)
     allocate(inlet(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'INLET','numdom',inlet)
     allocate(lcoor(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LCOOR','numdom',lcoor)
     allocate(lvaux(npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'LVAUX','numdom',lvaux)
     allocate(lvfro(npoin), stat=istat)
     call memchk(zero,istat,memor_dom,'LVFRO','numdom',lvfro)

     ipinl = 1

     do ipoin = 1,npoin
        lmark(ipoin) = .false.
     end do
     !
     ! Mark hole nodes (they are like imposed Dirichlet nodes) 
     !
     do ipoin = 1,npoin
        if( lnoch(ipoin) == NOHOL ) lgrou(ipoin) = -1
     end do

     !
     ! Count imposed nodes
     !
     do ipoin = 1,npoin
        if( lgrou(ipoin) == -1 ) then
           nmarkt = nmarkt - 1
           lmark(ipoin) = .true.

        else 

           if( coord(3, ipoin) == 0.75_rp) then
              lcoor(ipoin) = 0
              npinl = npinl + 1
              inlet(ipinl) = ipoin
              ipinl = ipinl + 1
           else if (coord(3, ipoin).NE.0.75_rp) then
              lcoor(ipoin) = -1   
           end if
        end if
     end do


     !----------------------------------------------------------------------
     !
     ! Construct groups
     !
     !----------------------------------------------------------------------
     !
     !  Initialize main loop
     !

     nmark        = 0
     igrou        = 0
     nauxi        = 0
     !     lqueu(1)     = kpoin
     !     lgrou(kpoin) = igrou
     !     nmark        = nmark + 1
     !     nqueu        = 1
     !     iqueu        = 1
     !     nfron        = 0
     !     nneg         = 0
     ! Para cada punto de la boundary inlet hay que crear un grupo

     nullcor: do ipinl = 1, npinl   !loop on inlet points
        iqueu  = 1
        nqueu  = 1  
        igrou  = igrou + 1
        ipoin = inlet(ipinl)

        if (lcoor(ipoin) == 0) then      
           if (.not.lvaux(ipoin)) then
              lvaux(ipoin) = .true.

              lqueu(iqueu) = ipoin
              if(.not. lmark(ipoin)) then
                 lmark(ipoin) = .true.
                 nmark = nmark + 1
              end if
           end if
           lgrou(ipoin) = igrou
        end if
    
        !    if (igrou == npinl) exit nullcor !nmarkt => has to be equal to the number of points of the inlet

        queu: do   !loop on lqueu
           !ATENCION: ipoin=lqueu(1) no est� a�adido a lgrou  !EVA
           ipoin = lqueu(iqueu)
           nfron = 0
           ifron = 1
           neigh:  do izdom  = r_dom(ipoin),r_dom(ipoin+1)-1
              jpoin  = c_dom(izdom)

              if (lcoor(jpoin) == -1) then
                 if (.not. lmark(jpoin)) then
                    nfron =  nfron + 1
                    lfron(nfron) = jpoin
                    lvfro(ifron) = .true.
                    ifron = ifron + 1
                 end if
              end if
           end do  neigh

           cangl     = 0
           cangm     = 0
           nneg      = 0
           !        vrefe     =(/0.0,1.0,0.0/)
           ang:   do ifron = 1, nfron
              kpoin = lfron(ifron)

              if (.not.lmark(kpoin)) then         
                 vpoin = (/coord(1,kpoin)-coord(1,lqueu(1)),coord(2,kpoin)-coord(2,lqueu(1)),coord(3,kpoin)-coord(3,lqueu(iqueu))/)
                 vrefe  = (/0.0_rp, 0.0_rp, -0.75_rp/)
                 cangl = sum(vpoin*vrefe)/(sqrt(sum(vpoin*vpoin))*sqrt(sum(vrefe*vrefe))) 
              end if
              if(cangl<=0) then
                 nneg = nneg + 1 
                 if (nneg == nfron) then
                    exit queu      
                 end if

              else if(cangl>cangm) then
                 cangm = cangl
                 lqueu(iqueu+1)=kpoin
              end if

           end do ang

           !   Clean the nodes from the front
           !     
           do ifron = 1, nfron                       
              kpoin = lfron(ifron)
              lvfro(kpoin) = .false.
           end do
           lmark(lqueu(iqueu+1))=.true.                            
           iqueu = iqueu + 1
           nqueu = nqueu + 1
           nmark = nmark + 1
           ipoin = lqueu(iqueu)
           lgrou(ipoin) = igrou
           if (nmark == nmarkt) exit queu

        end do queu

     end do nullcor

     !EVA
     do iqueu=1,igrou
         print*, 'NEW GROUP', iqueu
         do ipoin=1,npoin
            if (lgrou(ipoin) == iqueu) then
               print*, ipoin
            end if
         end do
     end do


     !
     ! Redefine ngrou
     !
     ngrou_dom = max(igrou,ngrou_dom)
     !
     ! LQUEU, LMARK, LFRON: Deallocate memory
     !
999  continue
     call memchk(2_ip,istat,memor_dom,'LQUEU','numdom',lqueu)
     deallocate(lqueu,stat=istat)
     if(istat/=0) call memerr(2_ip,'LQUEU','numdom',0_ip)

     call memchk(2_ip,istat,memor_dom,'LMARK','numdom',lmark)
     deallocate(lmark,stat=istat)
     if(istat/=0) call memerr(2_ip,'LMARK','numdom',0_ip)

     call memchk(2_ip,istat,memor_dom,'LFRON','numdom',lfron)
     deallocate(lfron,stat=istat)
     if(istat/=0) call memerr(2_ip,'LFRON','numdom',0_ip)

     call memchk(2_ip,istat,memor_dom,'inlet','numdom',inlet)
     deallocate(inlet,stat=istat)
     if(istat/=0) call memerr(2_ip,'inlet','numdom',0_ip)

     call memchk(2_ip,istat,memor_dom,'LCOOR','numdom',lcoor)
     deallocate(lcoor,stat=istat)
     if(istat/=0) call memerr(2_ip,'LCOOR','numdom',0_ip)

     call memchk(2_ip,istat,memor_dom,'LVAUX','numdom',lvaux)
     deallocate(lvaux,stat=istat)
     if(istat/=0) call memerr(2_ip,'LVAUX','numdom',0_ip)

     call memchk(2_ip,istat,memor_dom,'LVFRO','numdom',lvfro)
     deallocate(lvfro,stat=istat)
     if(istat/=0) call memerr(2_ip,'LVFRO','numdom',0_ip)



     !  else if( ngrou_dom == -2 .and. itask == 2 ) then

  end if

end subroutine numdom
