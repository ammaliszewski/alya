subroutine membcs(order)
  !-----------------------------------------------------------------------
  !****f* Domain/membcs
  ! NAME
  !    membcs
  ! DESCRIPTION
  !    Allocate/Deallocate memory for node and boundary codes
  ! USED BY
  !    reaset
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_memchk
  use mod_iofile
  implicit none
  integer(ip), intent(in) :: order
  integer(4)              :: istat

  select case (order) 

  case (1_ip)

     if( kfl_icodn > 0 ) then
        allocate(kfl_codno(mcono,npoin),stat=istat)
        call memchk(zero,istat,memor_dom,'KFL_CODNO','membcs',kfl_codno)
     end if

  case (-1_ip)

     if( kfl_icodn > 0 ) then
        call memchk(two,istat,memor_dom,'KFL_CODNO','membcs',kfl_codno)
        deallocate(kfl_codno,stat=istat)
        if(istat/=0) call memerr(two,'KFL_CODNO','membcs',0_ip)       
     end if

  case (2_ip)

     if( kfl_icodb > 0 ) then
        allocate(kfl_codbo(max(1_ip,nboun)),stat=istat)
        call memchk(zero,istat,memor_dom,'KFL_CODBO','membcs',kfl_codbo)
     end if

  case (-2_ip) 

     if( kfl_icodb > 0 ) then
        call memchk(two,istat,memor_dom,'KFL_CODBO','membcs',kfl_codbo)
        deallocate(kfl_codbo,stat=istat)
        if(istat/=0) call memerr(two,'KFL_CODBO','membcs',0_ip)           
     end if

  case (3_ip)

     if( kfl_icodn > 0 ) then
        call memchk(two,istat,memor_dom,'KFL_CODNO','membcs',kfl_codno)
        deallocate(kfl_codno,stat=istat)
        if(istat/=0) call memerr(two,'KFL_CODNO','membcs',0_ip)           
     end if

     if( kfl_icodb > 0 ) then
        call memchk(two,istat,memor_dom,'KFL_CODBO','membcs',kfl_codbo)
        deallocate(kfl_codbo,stat=istat)
        if(istat/=0) call memerr(two,'KFL_CODBO','membcs',0_ip)           
     end if

  case (4_ip)

     if( kfl_icodb > 0 ) then
        !allocate(kfl_geobo(max(1_ip,nboun_2)),stat=istat)
        allocate(kfl_geobo(nboun_2),stat=istat)
        call memchk(zero,istat,memor_dom,'KFL_GEOBO','membcs',kfl_geobo)
     end if

  case (5_ip)

     allocate(kfl_geono(nbopo),stat=istat)
     call memchk(zero,istat,memor_dom,'KFL_GEONO','membcs',kfl_geono)

  case (7_ip)

     if( kfl_icodb > 0 ) then
        allocate(kfl_codbo(max(1_ip,nboun)),stat=istat)
        call memchk(zero,istat,memor_dom,'KFL_CODBO','membcs',kfl_codbo)
     else
        allocate(kfl_codbo(1),stat=istat)
        call memchk(zero,istat,memor_dom,'KFL_CODBO','membcs',kfl_codbo)        
     end if

  case (-7_ip)

     call memchk(two,istat,memor_dom,'KFL_CODBO','membcs',kfl_codbo)
     deallocate(kfl_codbo,stat=istat)
     if(istat/=0) call memerr(two,'KFL_CODBO','membcs',0_ip)           

  case (8_ip)
     !
     ! Allocate LVCOD, BVCOD
     !
     allocate(lvcod(2,mvcod),stat=istat)
     call memchk(zero,istat,memor_dom,'LVCOD','membcs',lvcod)

     allocate(bvcod(mvcod),stat=istat)

  case (-8_ip)
     !
     ! Deallocate LVCOD, BVCOD
     !
     call memchk(two,istat,memor_dom,'LVCOD','membcs',lvcod)
     deallocate(lvcod,stat=istat)
     if(istat/=0) call memerr(two,'LVCOD','membcs',0_ip)   

     deallocate(bvcod,stat=istat)

  case (9_ip)
     !
     ! Allocate BVCOD(NVCOD) % A
     !
     allocate(bvcod(nvcod)%a(lvcod(2,nvcod),npoin),stat=istat)
     call memchk(zero,istat,memor_dom,'BVCOD%A','membcs',bvcod(nvcod)%a)

  case (-9_ip)
     !
     ! Deallocate BVCOD(NVCOD) % A
     !
     call memchk(two,istat,memor_dom,'BVCOD','membcs',bvcod(nvcod)%a)
     deallocate(bvcod(nvcod)%a,stat=istat)
     if(istat/=0) call memerr(two,'BVCOD%A','membcs',0_ip)   

  case (10_ip)
     !
     ! Allocate LVCOB, BVCOB
     !
     allocate(lvcob(2,mvcob),stat=istat)
     call memchk(zero,istat,memor_dom,'LVCOB','membcs',lvcob)

     allocate(bvcob(mvcob),stat=istat)

  case (-10_ip)
     !
     ! Deallocate LVCOB, BVCOB
     !
     call memchk(two,istat,memor_dom,'LVCOB','membcs',lvcob)
     deallocate(lvcob,stat=istat)
     if(istat/=0) call memerr(two,'LVCOB','membcs',0_ip)   

     deallocate(bvcob,stat=istat)

  case (11_ip)
     !
     ! Allocate BVCOB(NVCOB) % A
     !
     allocate(bvcob(nvcob)%a(lvcob(2,nvcob),max(1_ip,nboun)),stat=istat)
     call memchk(zero,istat,memor_dom,'BVCOB%A','membcs',bvcob(nvcob)%a)

  case (-11_ip)
     !
     ! Deallocate BVCOB(NVCOB) % A
     !
     call memchk(two,istat,memor_dom,'BVCOB%A','membcs',bvcob(nvcob)%a)
     deallocate(bvcob(nvcob)%a,stat=istat)
     if(istat/=0) call memerr(two,'BVCOB%A','membcs',0_ip)   

  end select

end subroutine membcs
