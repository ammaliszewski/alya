subroutine order_boundaries
  use def_parame
  use def_master
  use def_domain
  use def_elmtyp
  implicit none
  integer(ip)                        :: ielem,pelty,iboun,pblty,pface,ipoin
  integer(ip)                        :: inode,inodb,knode,kpoin,pnodb,jnodb,cont
  integer(ip)                        :: pnode,iface,inodf,jface,jnodf,knodf
  logical(lg)                        :: change_order,select_face

  if(INOTMASTER)then
     do iboun= 1,nboun
        pnodb = nnode(ltypb(iboun))
        ielem = lboel(pnodb+1,iboun)
        pelty = ltype(ielem)
        pnode = nnode(pelty)
        pface = nface(pelty)
        inodb = 1
        inode = lboel(inodb,iboun)
        ipoin = lnodb(inodb,iboun)
        jface = 0
        select_face = .false.
        do while (jface < pface)
           jface = jface + 1
           iface = jface
           jnodf = 0
           do while (jnodf < nnodf(pelty) % l(iface))
              jnodf = jnodf + 1
              inodf = jnodf
              if(ipoin== lnods( lface(pelty) % l(inodf,iface),ielem) )then
                 cont = 1
                 do inodb = 2,pnodb
                    do jnodb = 1, nnodf(pelty) % l(iface)
                       if(lnodb(inodb,iboun)==lnods( lface(pelty) % l(jnodb,iface),ielem) )then
                          cont = cont + 1
                       end if
                    end do
                 end do
                 if(cont==pnodb)then
                    jface = pface
                    jnodf = nnodf(pelty) % l(iface)
                    select_face = .true.
                 end if
                 change_order = .true.
                 if(select_face) then
                    cont = 1
                    do jnodb=2,pnodb
                       if(jnodb==2)then
                          if(inodf == pnodb)then
                             knodf = 1
                          else
                             knodf = inodf+1
                          end if
                       else if(jnodb==pnodb)then
                          if(inodf==1)then
                             knodf = pnodb
                          else
                             knodf = inodf - 1
                          end if
                       else
                          if(inodf == pnodb - 1) then
                             knodf = 1
                          else if (inodf == pnodb ) then
                             knodf = 2
                          else
                             knodf = inodf + 2
                          end if
                       end if
                       if(lnodb(jnodb,iboun) == lnods(lface(pelty) % l(knodf,iface),ielem) )cont = cont + 1
                    end do
                    if (cont == pnodb) change_order = .false. 
                    if ( change_order ) then
                       kpoin = lnodb(pnodb,iboun)
                       lnodb(pnodb,iboun) = lnodb(2,iboun)
                       lnodb(2,iboun) = kpoin
                       knode = lboel(pnodb,iboun)
                       lboel(pnodb,iboun) = lboel(2,iboun)
                       lboel(2,iboun) = knode                       
                    end if
                 end if
              end if
           end do
        end do
     end do

  end if


end subroutine order_boundaries
