subroutine exampl()
  !-----------------------------------------------------------------------
  !****f* domain/exampl
  ! NAME
  !    exampl
  ! DESCRIPTION
  !    This routines generates predefined cases
  !    KFL_EXAMP = 1 ... Periodic fully developed channel flow
  !                2 ... Thermal cavity
  !                3 ... Wall with inclined inflow
  !                7 ... Developping channel flow
  !
  !     +------------+
  !     |\            \               /|\ y
  ! Ly  | \            \               |
  !     O  +------------+              |
  !   Lz \ |            |              +-----> x
  !       \|            |               \
  !        +------------+                \| 
  !             Lx                      -+ z
  !
  !   
  !
  ! REFINEMENT RATIO must be / 0 !!!!!!!!!!!!!!!!!!!!!!!
  !
  ! Z,Y,X direction refinement type
  !       if Z,y,xdir = 0  : standard  without refinement
  !       if z,y,xdir = 1  : simple refinement 
  !       if z,y,xdir = 2  : double centered refinement (npz must be impair) !!
  !       if z,y,xdir = 3  : double refinement  (npz must be impair) !!!!!
  !
  !if SPACE DIMENSION = 2 it must be blank space about zdir !!!!!!!!!!!!!!!!!!
  !
  !
  !
  !
  ! USED BY
  !    Turnon
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_kermod
  use def_elmtyp
  use def_inpout
  implicit none
  integer(ip) :: ielem,ipoin,iperi,icode,istat,deminp,idemi,pnodb,ii
  integer(ip) :: npx,npy,npz,ipx,ipy,ipz,ix,iy,iz,kzdir,kydir,kxdir
  integer(ip) :: typel,pelty
  integer(ip) :: ipoi1,ipoi2,ipoi3,ipoi4,ipoi5,ipoi6,ipoi7,ipoi8,iboun
  integer(ip) :: ipoi9,ipoi10,ipoi11,ipoi12,ipoi13,ipoi14,ipoi15,ipoi16
  integer(ip) :: ipoi17,ipoi18,ipoi19,ipoi20,ipoi21,ipoi22,ipoi23,ipoi24
  integer(ip) :: ipoi25,ipoi26,ipoi27
  real(rp)    :: lx,ly,lz,x,y,z
  real(rp)    :: epcox,epcoy,epcoz,fpcon,cpcox,cpcoy,cpcoz,factx,facty,factz
  real(rp)    :: ratio,zauxi,yauxi,xauxi,demiaz,demiay,demiax
  real(rp)    :: dzcoo(100000),dycoo(100000),dxcoo(100000)

  if(kfl_paral==-1.or.(kfl_paral==0.and.kfl_ptask/=2)) then

     mcono     = 3
     kxdir     = 0
     kydir     = 0 
     kzdir     = 0
     epcox     = 0.25_rp
     epcoy     = 0.25_rp
     epcoz     = 0.25_rp
     
#ifdef NDIMEPAR

#else
  ndime     = 3
#endif     
     typel     = 1
     kfl_naxis = 0
     kfl_chege = 0
     delta_dom = 0.0_rp
     rough_dom = 0.0_rp

     call livinf(49_ip,' ',zero)
     !
     ! Reach the DIMENSIONS section
     !
     call ecoute('exampl')
     do while(words(1)/='DIMEN')
        call ecoute('exampl')
     end do
     !
     ! Read geometry properties as in GEOMETRY field (see reageo)
     !
     if(exists('AXISY')) kfl_naxis = 1
     if(exists('CHECK')) kfl_chege = 1
     if(exists('WALLD')) then
        delta_dom=getrea('WALLD',0.0_rp,'#Distance to the wall')
     end if
     if(exists('ROUGH')) then
        rough_dom=getrea('ROUGH',0.0_rp,'#Wall roughness')
     end if
     !
     ! Read mesh dimensions
     !
     do while(words(1)/='ENDDI')
        if     (words(1)=='SPACE') then
#ifdef NDIMEPAR

#else
           ndime=int(param(1))
#endif
        else if(words(1)=='TYPEL') then
           typel=int(param(1))
        else if(words(1)=='LX   ') then
           lx=param(1)
        else if(words(1)=='LY   ') then
           ly=param(1)
        else if(words(1)=='LZ   ') then
           lz=param(1)
        else if(words(1)=='NX   ') then
           nxexa=int(param(1))
        else if(words(1)=='NY   ') then
           nyexa=int(param(1))
        else if(words(1)=='NZ   ') then
           nzexa=int(param(1))
        else if(words(1)=='PX   ') then
           nxexp=int(param(1))
        else if(words(1)=='PY   ') then
           nyexp=int(param(1))
        else if(words(1)=='PZ   ') then
           nzexp=int(param(1))
        else if(words(1)=='ZREFI') then
           if      (words(2)=='NORMA') then
              kzdir=0
           else if (words(2)=='SIMPL') then
              kzdir=1
           else if (words(2)=='CENTE') then
              kzdir=2
           else if (words(2)=='DOUBL') then
              kzdir=3
           end if
           if(exists('VALUE')) &
                epcoz=getrea('VALUE',0.25_rp,'#Z-refinement value')
        else if(words(1)=='YREFI') then
           if      (words(2)=='NORMA') then
              kydir=0
           else if (words(2)=='SIMPL') then
              kydir=1
           else if (words(2)=='CENTE') then
              kydir=2
           else if (words(2)=='DOUBL') then
              kydir=3
           end if
           if(exists('VALUE')) &
                epcoy=getrea('VALUE',0.25_rp,'#Y-refinement value')
        else if(words(1)=='XREFI') then
           if      (words(2)=='NORMA') then
              kxdir=0
           else if (words(2)=='SIMPL') then
              kxdir=1
           else if (words(2)=='CENTE') then
              kxdir=2
           else if (words(2)=='DOUBL') then
              kxdir=3
           end if
           if(exists('VALUE')) &
                epcox=getrea('VALUE',0.25_rp,'#X-refinement value')
        else if(words(1)=='AUTOM') then
           if(words(2)=='ON   '.or.words(2)=='YES  ') kfl_parex=1
        end if
        call ecoute('exampl')
     end do
     !
     ! Check erros
     !
     if(ndime==3.and.nzexa==0) call runend('WRONG NUMBER OF ELEMENTS IN Z-DIRECTION')
     if(ndime==2.and.nyexa==0) call runend('WRONG NUMBER OF ELEMENTS IN Y-DIRECTION')

     if(typel==1) then  
        npx = nxexa+1
        npy = nyexa+1
        npz = nzexa+1
     else if(typel==2) then      
        npx = 2*nxexa+1
        npy = 2*nyexa+1
        npz = 2*nzexa+1
     endif

     !
     ! Dimensions and strategy
     !

     if      (ndime==3) then
        npoin        = npx*npy*npz
        nelem        = nxexa*nyexa*nzexa
     else if (ndime==2) then
        npoin        = npx*npy
        nelem        = nxexa*nyexa
     end if
     nskew = 0
     if(kfl_examp==1.or.kfl_examp==6) then
        if(ndime==2) then
           nperi = npy
        else
           nperi = (nyexa+1)+(nxexa-1)*(nyexa+1)+(nzexa-1)*(nyexa+1)
        end if
     end if
     if(kfl_examp==5.or.kfl_examp==6.or.kfl_examp==7.or.kfl_examp==8) then
        if(ndime==2) then
           nboun        = 2*(nxexa+nyexa)
           kfl_bouel    = 1
        else
           nboun        = 2*(nxexa*nzexa+nyexa*nzexa+nxexa*nyexa)
           kfl_bouel    = 1
        end if
     end if

     if(ndime==2) then
        if(typel==1) then
           lexis(QUA04) = 1
        else if(typel==2) then
           lexis(QUA09) = 1
        endif
     else
        if(typel==1) then
           lexis(HEX08) = 1
        else if(typel==2) then
           lexis(HEX27) = 1
        endif
     end if

     call reastr()
     call cderda()

     !
     ! Allocate memory for geometry
     !
     call memgeo(1_ip)
     call memgeo(27_ip)
     !
     !  refinement 
     !
     ! model constants:
     !
     fpcon = 1.0_rp    
     cpcox = 1.0_rp - exp(-2.0_rp / (2.0_rp * fpcon + 1.0_rp) * epcox )
     factx = - epcox * 0.5_rp * (2.0_rp * fpcon + 1.0_rp)

     cpcoy = 1.0_rp - exp(-2.0_rp / (2.0_rp * fpcon + 1.0_rp) * epcoy )
     facty = - epcoy * 0.5_rp * (2.0_rp * fpcon + 1.0_rp)

     cpcoz = 1.0_rp - exp(-2.0_rp / (2.0_rp * fpcon + 1.0_rp) * epcoz )
     factz = - epcoz * 0.5_rp * (2.0_rp * fpcon + 1.0_rp)

     !     
     ! z-direction:
     ! zauxi = non-normalized height
     ! compute the refinement    
     ! 
     !  zdir = 1  : simple refinemnt
     !

     if (kzdir==1) then

        zauxi = 0.0_rp
        do ipz=1,npz-1
           dzcoo(ipz)= factz * log(1.0_rp - 2.0_rp * cpcoz * real(ipz) / real(npz-1))  
           zauxi = zauxi + lz * dzcoo(ipz)
        end do

        ratio = lz / zauxi

        ! normalize the computed refinement
        zauxi = 0.0_rp
        do ipz=1,npz-1
           dzcoo(ipz) = lz * dzcoo(ipz) * ratio
           zauxi = zauxi + dzcoo(ipz)
        end do

        ! 
        !  zdir = 2  : double centered refinemnt (npz must be impair)!!!!!!!!!!!!!!!!!!
        !

     else if (kzdir==2) then

        zauxi = 0.0_rp
        deminp = (npz-1) / 2
        demiaz = lz / 2.0_rp

        do ipz=1,deminp
           dzcoo(ipz)= factz * log(1.0_rp - 2.0_rp * cpcoz * real(ipz) / real(deminp))
           zauxi = zauxi + demiaz * dzcoo(ipz)
        end do

        ratio = demiaz / zauxi

        ! normalize the computed refinement
        zauxi = 0.0_rp
        do ipz=1,deminp 
           dzcoo(ipz) = demiaz * dzcoo(ipz) * ratio
        end do
        !     
        !    computed deminp+1 toward npz-1 (inverse refinement)     
        !
        zauxi = 0.0_rp
        do ipz=deminp+1,npz-1
           dzcoo(ipz) = dzcoo(ipz-deminp)
        end do
        !     
        !    computed deminp toward 1      
        !
        zauxi = 0.0_rp
        idemi=deminp
        do ipz=deminp,1,-1     
           idemi= idemi+1
           dzcoo(ipz) = dzcoo(idemi)
        end do

        ! 
        !  zdir = 3  : double refinemnt (npz must be impair)!!!!!!!!!!!!!!!!!!
        !

     else if (kzdir==3) then


        zauxi = 0.0_rp
        deminp = (npz-1) / 2
        demiaz = lz / 2.0_rp

        do ipz=1,deminp
           dzcoo(ipz)= factz * log(1.0_rp - 2.0_rp * cpcoz * real(ipz) / real(deminp))
           zauxi = zauxi + demiaz * dzcoo(ipz)
        end do

        ratio = demiaz / zauxi

        ! normalize the computed refinement
        zauxi = 0.0_rp
        do ipz=1,deminp 
           dzcoo(ipz) = demiaz * dzcoo(ipz) * ratio
        end do
        !     
        !    computed deminp+1 toward npz-1      
        !
        zauxi = 0.0_rp
        do ipz=deminp+1,npz-1
           dzcoo(ipz) = dzcoo(npz-ipz)
        end do
        !     
        !    computed 1 toward deminp       
        !
        zauxi = 0.0_rp
        do ipz=1,deminp     
           dzcoo(ipz) = dzcoo(ipz)
        end do
     end if

     !     
     ! y-direction:
     ! yauxi = non-normalized height
     ! compute the refinement    
     ! 
     !  ydir = 1  : simple refinemnt
     !

     if (kydir==1) then

        yauxi = 0.0_rp
        do ipy=1,npy-1
           dycoo(ipy)= facty * log(1.0_rp - 2.0_rp * cpcoy * real(ipy) / real(npy-1))  
           yauxi = yauxi + ly * dycoo(ipy)
        end do

        ratio = ly / yauxi

        ! normalize the computed refinement
        yauxi = 0.0_rp
        do ipy=1,npy-1
           dycoo(ipy) = ly * dycoo(ipy) * ratio
           yauxi = yauxi + dycoo(ipy)
        end do

        ! 
        !  ydir = 2  : double centered refinemnt (npz must be impair)!!!!!!!!!!!!!!!!!!
        !

     else if (kydir==2) then

        yauxi = 0.0_rp
        deminp = (npy-1) / 2
        demiay = ly / 2.0_rp

        do ipy=1,deminp
           dycoo(ipy)= facty * log(1.0_rp - 2.0_rp * cpcoy * real(ipy) / real(deminp))
           yauxi = yauxi + demiay * dycoo(ipy)
        end do

        ratio = demiay / yauxi

        ! normalize the computed refinement
        yauxi = 0.0_rp
        do ipy=1,deminp 
           dycoo(ipy) = demiay * dycoo(ipy) * ratio
        end do
        !     
        !    computed deminp+1 toward npy-1 (inverse refinement)     
        !
        yauxi = 0.0_rp
        do ipy=deminp+1,npy-1
           dycoo(ipy) = dycoo(ipy-deminp)
        end do
        !     
        !    computed deminp toward 1      
        !
        yauxi = 0.0_rp
        idemi=deminp
        do ipy=deminp,1,-1     
           idemi= idemi+1
           dycoo(ipy) = dycoo(idemi)
        end do

        ! 
        !  ydir = 3  : double refinemnt (npz must be impair)!!!!!!!!!!!!!!!!!!
        !

     else if (kydir==3) then


        yauxi = 0.0_rp
        deminp = (npy-1) / 2
        demiay = ly / 2.0_rp

        do ipy=1,deminp
           dycoo(ipy)= facty * log(1.0_rp - 2.0_rp * cpcoy * real(ipy) / real(deminp))
           yauxi = yauxi + demiay * dycoo(ipy)
        end do

        ratio = demiay / yauxi

        ! normalize the computed refinement
        yauxi = 0.0_rp
        do ipy=1,deminp 
           dycoo(ipy) = demiay * dycoo(ipy) * ratio
        end do
        !     
        !    computed deminp+1 toward npy-1      
        !
        yauxi = 0.0_rp
        do ipy=deminp+1,npy-1
           dycoo(ipy) = dycoo(npy-ipy)
        end do
        !     
        !    computed 1 toward deminp       
        !
        yauxi = 0.0_rp
        do ipy=1,deminp     
           dycoo(ipy) = dycoo(ipy)
        end do


     end if

     !     
     ! x-direction:
     ! xauxi = non-normalized height
     ! compute the refinement    
     ! 
     !  xdir = 1  : simple refinemnt
     !

     if (kxdir==1) then

        xauxi = 0.0_rp
        do ipx=1,npx-1
           dxcoo(ipx)= factx * log(1.0_rp - 2.0_rp * cpcox * real(ipx) / real(npx-1))  
           xauxi = xauxi + lx * dxcoo(ipx)
        end do

        ratio = lx / xauxi

        ! normalize the computed refinement
        xauxi = 0.0_rp
        do ipx=1,npx-1
           dxcoo(ipx) = lx * dxcoo(ipx) * ratio
           xauxi = xauxi + dxcoo(ipx)
        end do

        ! 
        !  xdir = 2  : double centered refinemnt (npx must be impair)!!!!!!!!!!!!!!!!!!
        !

     else if (kxdir==2) then

        xauxi = 0.0_rp
        deminp = (npx-1) / 2
        demiax = lx / 2.0_rp

        do ipx=1,deminp
           dxcoo(ipx)= factx * log(1.0_rp - 2.0_rp * cpcox * real(ipx) / real(deminp))
           xauxi = xauxi + demiax * dxcoo(ipx)
        end do

        ratio = demiax / xauxi

        ! normalize the computed refinement
        xauxi = 0.0_rp
        do ipx=1,deminp 
           dxcoo(ipx) = demiax * dxcoo(ipx) * ratio
        end do
        !     
        !    computed deminp+1 toward npx-1 (inverse refinement)     
        !
        xauxi = 0.0_rp
        do ipx=deminp+1,npx-1
           dxcoo(ipx) = dxcoo(ipx-deminp)
        end do
        !     
        !    computed deminp toward 1      
        !
        xauxi = 0.0_rp
        idemi=deminp
        do ipx=deminp,1,-1     
           idemi= idemi+1
           dxcoo(ipx) = dxcoo(idemi)
        end do

        ! 
        !  xdir = 3  : double refinemnt (npx must be impair)!!!!!!!!!!!!!!!!!!
        !

     else if (kxdir==3) then

        xauxi  = 0.0_rp
        deminp = (npx-1) / 2
        demiax = lx / 2.0_rp

        do ipx=1,deminp
           dxcoo(ipx)= factx * log(1.0_rp - 2.0_rp * cpcox * real(ipx) / real(deminp))
           xauxi = xauxi + demiax * dxcoo(ipx)
        end do

        ratio = demiax / xauxi

        ! normalize the computed refinement
        xauxi = 0.0_rp
        do ipx=1,deminp 
           dxcoo(ipx) = demiax * dxcoo(ipx) * ratio
        end do
        !     
        !    computed deminp+1 toward npx-1      
        !
        xauxi = 0.0_rp
        do ipx=deminp+1,npx-1
           dxcoo(ipx) = dxcoo(npx-ipx)
        end do
        !     
        !    computed 1 toward deminp       
        !
        xauxi = 0.0_rp
        do ipx=1,deminp     
           dxcoo(ipx) = dxcoo(ipx)
        end do
     end if

     !-----------------------------------------------------------------------------------
     !
     !if standard, without refinement 
     !
     !----------------------------------------------------------------------------------- 
     !
     ! COORD
     !
     ipoin=0

     if (ndime==3) then 

        do ipy=1,npy
           if (kydir==0) y=ly*real((ipy-1))/real((npy-1))
           do ipz=1,npz
              if (kzdir==0) z=lz*real((ipz-1))/real((npz-1))
              do ipx=1,npx
                 if (kxdir==0) x=lx*real((ipx-1))/real((npx-1))
                 ipoin=ipoin+1
                 coord(1,ipoin)=x
                 coord(2,ipoin)=y
                 coord(3,ipoin)=z
                 if (kxdir/=0) x= x + dxcoo(ipx)  
              end do
              if (kxdir/=0) x = 0.0_rp
              if (kzdir/=0) z = z + dzcoo(ipz)  
           end do
           if (kydir/=0) y = y + dycoo(ipy) 
           if (kzdir/=0) z = 0.0_rp
        end do
        if (kydir/=0) y=0.0

     else if (ndime==2) then 

        do ipy=1,npy
           if (kydir==0) y=ly*real((ipy-1))/real((npy-1))
           do ipx=1,npx
              if (kxdir==0) x=lx*real((ipx-1))/real((npx-1))
              ipoin=ipoin+1
              coord(1,ipoin)=x
              coord(2,ipoin)=y
              if (kxdir/=0) x= x + dxcoo(ipx)  
           end do
           if (kxdir/=0) x = 0.0_rp
           if (kydir/=0) y = y + dycoo(ipy)  
        end do
        if (kydir/=0) y = 0.0_rp

     end if

     !
     ! LNODS and LTYPE
     !

     if (ndime==3) then

        if(typel==1) then
           ielem=0
           do iy=1,nyexa
              ipoi1=npx*npz*(iy-1)
              do iz=1,nzexa
                 do ix=1,nxexa
                    ipoi1=ipoi1+1
                    ielem          = ielem+1 
                    ipoi2          = ipoi1+npx
                    ipoi3          = ipoi2+1
                    ipoi4          = ipoi1+1
                    ipoi5          = ipoi1+npx*npz
                    ipoi6          = ipoi5+npx
                    ipoi7          = ipoi6+1
                    ipoi8          = ipoi5+1
                    lnods(1,ielem) = ipoi1
                    lnods(2,ielem) = ipoi2
                    lnods(3,ielem) = ipoi3
                    lnods(4,ielem) = ipoi4
                    lnods(5,ielem) = ipoi5
                    lnods(6,ielem) = ipoi6
                    lnods(7,ielem) = ipoi7
                    lnods(8,ielem) = ipoi8
                    ltype(ielem)   = HEX08
                 end do
                 ipoi1=ipoi1+1
              end do
           end do
        else if(typel==2) then
           ielem=0
           ipoi1=0
           do iy=1,nyexa
              do iz=1,nzexa
                 do ix=1,nxexa
                    ipoi1          = 1+(ix-1)*2+(iz-1)*2*npx+npx*npz*(iy-1)*2
                    ielem          = ielem+1 
                    ipoi2          = ipoi1+2*npx
                    ipoi3          = ipoi2+2
                    ipoi4          = ipoi1+2
                    ipoi5          = ipoi1+2*npx*npz
                    ipoi6          = ipoi5+2*npx
                    ipoi7          = ipoi6+2
                    ipoi8          = ipoi5+2
                    ipoi9          = ipoi1+npx
                    ipoi10         = ipoi2+1
                    ipoi11         = ipoi9+2
                    ipoi12         = ipoi1+1
                    ipoi13         = ipoi1+npx*npz
                    ipoi14         = ipoi2+npx*npz
                    ipoi15         = ipoi3+npx*npz
                    ipoi16         = ipoi4+npx*npz
                    ipoi17         = ipoi5+npx
                    ipoi18         = ipoi6+1
                    ipoi19         = ipoi17+2
                    ipoi20         = ipoi5+1
                    ipoi21         = ipoi9+1
                    ipoi22         = ipoi13+npx
                    ipoi23         = ipoi14+1
                    ipoi24         = ipoi22+2
                    ipoi25         = ipoi13+1
                    ipoi26         = ipoi17+1
                    ipoi27         = ipoi22+1

                    lnods(1,ielem) = ipoi1
                    lnods(2,ielem) = ipoi2
                    lnods(3,ielem) = ipoi3
                    lnods(4,ielem) = ipoi4
                    lnods(5,ielem) = ipoi5
                    lnods(6,ielem) = ipoi6
                    lnods(7,ielem) = ipoi7
                    lnods(8,ielem) = ipoi8
                    lnods(9,ielem) = ipoi9
                    lnods(10,ielem) = ipoi10
                    lnods(11,ielem) = ipoi11
                    lnods(12,ielem) = ipoi12
                    lnods(13,ielem) = ipoi13
                    lnods(14,ielem) = ipoi14
                    lnods(15,ielem) = ipoi15
                    lnods(16,ielem) = ipoi16
                    lnods(17,ielem) = ipoi17
                    lnods(18,ielem) = ipoi18
                    lnods(19,ielem) = ipoi19
                    lnods(20,ielem) = ipoi20
                    lnods(21,ielem) = ipoi21
                    lnods(22,ielem) = ipoi22
                    lnods(23,ielem) = ipoi23
                    lnods(24,ielem) = ipoi24
                    lnods(25,ielem) = ipoi25
                    lnods(26,ielem) = ipoi26
                    lnods(27,ielem) = ipoi27
                    ltype(ielem)   = HEX27

                 end do
              end do
           end do
        end if

     else if (ndime==2) then 

        if(typel==1) then
           ielem=0
           do iy=1,nyexa
              do ix=1,nxexa
                 ielem                = ielem+1 
                 ipoi1                = (iy-1)*npx+ix
                 ipoi2                = ipoi1+1
                 ipoi3                = ipoi2+npx
                 ipoi4                = ipoi1+npx
                 lnods(1,ielem)       = ipoi1
                 lnods(2,ielem)       = ipoi2
                 lnods(3,ielem)       = ipoi3
                 lnods(4,ielem)       = ipoi4
                 ltype(ielem)         = QUA04

              end do
           end do

        else if(typel==2) then
           ielem=0
           do iy=1,nyexa
              do ix=1,nxexa
                 ipoi1                = (iy-1)*2*npx+(ix-1)*2+1
                 ipoi2                = ipoi1+2
                 ipoi3                = ipoi2+2*npx
                 ipoi4                = ipoi1+2*npx
                 ipoi5                = ipoi1+1
                 ipoi6                = ipoi2+npx
                 ipoi7                = ipoi5+2*npx
                 ipoi8                = ipoi1+npx 
                 ipoi9                = ipoi5+npx                     
                 ielem                = ielem+1
                 lnods(1,ielem)       = ipoi1
                 lnods(2,ielem)       = ipoi2
                 lnods(3,ielem)       = ipoi3
                 lnods(4,ielem)       = ipoi4
                 lnods(5,ielem)       = ipoi5
                 lnods(6,ielem)       = ipoi6
                 lnods(7,ielem)       = ipoi7
                 lnods(8,ielem)       = ipoi8
                 lnods(9,ielem)       = ipoi9
                 ltype(ielem)         = QUA09
              end do
           end do
        end if

     end if
     !
     ! LNODB
     !
     if(nboun>0) then
        iboun=0
        if(typel==1) then
           ielem=0
           if(ndime==2) then
              ! Face 1: y=0
              ielem=1
              ipoi1=1
              do ix=1,nxexa
                 ipoi2                = ipoi1+1
                 iboun                = iboun+1
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 ltypb(iboun)         = BAR02
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
                 ielem                = ielem+1
                 ipoi1                = ipoi1+1
              end do
              ! Face 2: x=Lx
              ielem=nxexa
              ipoi1=npx
              do iy=1,nyexa
                 ipoi2                = ipoi1+npx
                 iboun                = iboun+1
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 ltypb(iboun)         = BAR02
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
                 ielem                = ielem+nxexa
                 ipoi1                = ipoi2
              end do
              ! Face 3: y=0
              ielem=nxexa*nyexa
              ipoi1=npx*npy
              do ix=1,nxexa
                 ipoi2                = ipoi1-1
                 iboun                = iboun+1
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 ltypb(iboun)         = BAR02
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
                 ielem                = ielem-1
                 ipoi1                = ipoi1-1
              end do
              ! Face 4: x=0
              ielem=(nyexa-1)*nxexa+1
              ipoi1=(npy-1)*npx+1
              do iy=1,nyexa
                 ipoi2                = ipoi1-npx
                 iboun                = iboun+1
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 ltypb(iboun)         = BAR02
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
                 ielem                = ielem-nxexa
                 ipoi1                = ipoi2
              end do

           else
              ! Face 1: y=0
              do iz=1,nzexa
                 do ix=1,nxexa
                    ipoi1                = (iz-1)*npx+ix
                    ipoi2                = ipoi1+npx
                    ipoi4                = ipoi1+1
                    ipoi3                = ipoi2+1
                    iboun                = iboun+1
                    ielem                = ielem+1
                    lnodb(1,iboun)       = ipoi1
                    lnodb(2,iboun)       = ipoi2
                    lnodb(3,iboun)       = ipoi3
                    lnodb(4,iboun)       = ipoi4
                    ltypb(iboun)         = QUA04
                    pnodb                = nnode(ltypb(iboun))
                    lboel(pnodb+1,iboun) = ielem
                 end do
              end do
              ! Face 2: y=Ly
              ielem=nxexa*nzexa*(nyexa-1)
              do iz=1,nzexa
                 do ix=1,nxexa
                    ipoi1                = (iz-1)*npx+ix+npx*npz*(npy-1)
                    ipoi2                = ipoi1+npx
                    ipoi4                = ipoi1+1
                    ipoi3                = ipoi2+1
                    iboun                = iboun+1
                    ielem                = ielem+1
                    lnodb(1,iboun)       = ipoi1
                    lnodb(2,iboun)       = ipoi2
                    lnodb(3,iboun)       = ipoi3
                    lnodb(4,iboun)       = ipoi4
                    ltypb(iboun)         = QUA04
                    pnodb                = nnode(ltypb(iboun))
                    lboel(pnodb+1,iboun) = ielem
                 end do
              end do
              ! Face 3: x=0
              do iy=1,nyexa
                 do iz=1,nzexa
                    ipoi1                = 1+(iz-1)*npx+(iy-1)*npx*npz
                    ipoi2                = ipoi1+npx
                    ipoi3                = ipoi2+npx*npz
                    ipoi4                = ipoi1+npx*npz
                    iboun                = iboun+1
                    ielem                = 1+(iz-1)*nxexa+nxexa*nzexa*(iy-1)
                    lnodb(1,iboun)       = ipoi1
                    lnodb(2,iboun)       = ipoi2
                    lnodb(3,iboun)       = ipoi3
                    lnodb(4,iboun)       = ipoi4
                    ltypb(iboun)         = QUA04
                    pnodb                = nnode(ltypb(iboun))
                    lboel(pnodb+1,iboun) = ielem
                 end do
              end do
              ! Face 4: x=Lx
              do iy=1,nyexa
                 do iz=1,nzexa
                    ipoi1                = iz*npx+(iy-1)*npx*npz
                    ipoi2                = ipoi1+npx
                    ipoi3                = ipoi2+npx*npz
                    ipoi4                = ipoi1+npx*npz
                    iboun                = iboun+1
                    ielem                = iz*nxexa+nxexa*nzexa*(iy-1)
                    lnodb(1,iboun)       = ipoi1
                    lnodb(2,iboun)       = ipoi2
                    lnodb(3,iboun)       = ipoi3
                    lnodb(4,iboun)       = ipoi4
                    ltypb(iboun)         = QUA04
                    pnodb                = nnode(ltypb(iboun))
                    lboel(pnodb+1,iboun) = ielem
                 end do
              end do
              ! Face 5: z=0
              do iy=1,nyexa
                 do ix=1,nxexa
                    ipoi1                = ix+(iy-1)*npx*npz
                    ipoi2                = ipoi1+1
                    ipoi3                = ipoi2+npx*npz
                    ipoi4                = ipoi1+npx*npz
                    iboun                = iboun+1
                    ielem                = ix+(iy-1)*nxexa*nzexa
                    lnodb(1,iboun)       = ipoi1
                    lnodb(2,iboun)       = ipoi2
                    lnodb(3,iboun)       = ipoi3
                    lnodb(4,iboun)       = ipoi4
                    ltypb(iboun)         = QUA04
                    pnodb                = nnode(ltypb(iboun))
                    lboel(pnodb+1,iboun) = ielem
                 end do
              end do
              ! Face 6: z=Lz
              do iy=1,nyexa
                 do ix=1,nxexa
                    ipoi1                = ix+(npz-1)*npx+(iy-1)*npx*npz 
                    ipoi2                = ipoi1+1
                    ipoi3                = ipoi2+npx*npz
                    ipoi4                = ipoi1+npx*npz
                    iboun                = iboun+1
                    ielem                = ix+(nzexa-1)*nxexa+(iy-1)*nxexa*nzexa
                    lnodb(1,iboun)       = ipoi1
                    lnodb(2,iboun)       = ipoi2
                    lnodb(3,iboun)       = ipoi3
                    lnodb(4,iboun)       = ipoi4
                    ltypb(iboun)         = QUA04
                    pnodb                = nnode(ltypb(iboun))
                    lboel(pnodb+1,iboun) = ielem
                 end do
              end do
           end if

        else if(typel==2) then
           ielem=0
           ! Face 1: y=0
           do iz=1,nzexa
              do ix=1,nxexa
                 ipoi1                = (iz-1)*2*npx+(ix-1)*2+1
                 ipoi2                = ipoi1+2*npx
                 ipoi4                = ipoi1+2
                 ipoi3                = ipoi2+2
                 ipoi5                = ipoi1+npx
                 ipoi6                = ipoi2+1
                 ipoi7                = ipoi5+2
                 ipoi8                = ipoi1+1 
                 ipoi9                = ipoi5+1                     
                 iboun                = iboun+1
                 ielem                = ielem+1
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 lnodb(3,iboun)       = ipoi3
                 lnodb(4,iboun)       = ipoi4
                 lnodb(5,iboun)       = ipoi5
                 lnodb(6,iboun)       = ipoi6
                 lnodb(7,iboun)       = ipoi7
                 lnodb(8,iboun)       = ipoi8
                 lnodb(9,iboun)       = ipoi9
                 ltypb(iboun)         = QUA09
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
              end do
           end do
           ! Face 2: y=Ly
           ielem=nxexa*nzexa*(nyexa-1)
           do iz=1,nzexa
              do ix=1,nxexa
                 ipoi1                = (iz-1)*2*npx+(ix-1)*2+npx*npz*(npy-1)+1
                 ipoi2                = ipoi1+2*npx
                 ipoi4                = ipoi1+2
                 ipoi3                = ipoi2+2
                 ipoi5                = ipoi1+npx
                 ipoi6                = ipoi2+1
                 ipoi7                = ipoi5+2
                 ipoi8                = ipoi1+1 
                 ipoi9                = ipoi5+1                     
                 iboun                = iboun+1
                 ielem                = ielem+1
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 lnodb(3,iboun)       = ipoi3
                 lnodb(4,iboun)       = ipoi4
                 lnodb(5,iboun)       = ipoi5
                 lnodb(6,iboun)       = ipoi6
                 lnodb(7,iboun)       = ipoi7
                 lnodb(8,iboun)       = ipoi8
                 lnodb(9,iboun)       = ipoi9
                 ltypb(iboun)         = QUA09
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
              end do
           end do
           ! Face 3: x=0
           do iy=1,nyexa
              do iz=1,nzexa
                 ipoi1                = 1+(iz-1)*2*npx+(iy-1)*2*npx*npz
                 ipoi2                = ipoi1+2*npx
                 ipoi3                = ipoi2+2*npx*npz
                 ipoi4                = ipoi1+2*npx*npz
                 ipoi5                = ipoi1+npx
                 ipoi6                = ipoi2+npx*npz
                 ipoi7                = ipoi5+2*npx*npz
                 ipoi8                = ipoi1+npx*npz
                 ipoi9                = ipoi5+npx*npz
                 iboun                = iboun+1
                 ielem                = 1+(iz-1)*nxexa+nxexa*nzexa*(iy-1)
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 lnodb(3,iboun)       = ipoi3
                 lnodb(4,iboun)       = ipoi4
                 lnodb(5,iboun)       = ipoi5
                 lnodb(6,iboun)       = ipoi6
                 lnodb(7,iboun)       = ipoi7
                 lnodb(8,iboun)       = ipoi8
                 lnodb(9,iboun)       = ipoi9
                 ltypb(iboun)         = QUA09
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
              end do
           end do
           ! Face 4: x=Lx
           do iy=1,nyexa
              do iz=1,nzexa
                 ipoi1                = npx+(iz-1)*2*npx+(iy-1)*2*npx*npz
                 ipoi2                = ipoi1+2*npx
                 ipoi3                = ipoi2+2*npx*npz
                 ipoi4                = ipoi1+2*npx*npz
                 ipoi5                = ipoi1+npx
                 ipoi6                = ipoi2+npx*npz
                 ipoi7                = ipoi5+2*npx*npz
                 ipoi8                = ipoi1+npx*npz
                 ipoi9                = ipoi5+npx*npz
                 iboun                = iboun+1
                 ielem                = iz*nxexa+nxexa*nzexa*(iy-1)
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 lnodb(3,iboun)       = ipoi3
                 lnodb(4,iboun)       = ipoi4
                 lnodb(5,iboun)       = ipoi5
                 lnodb(6,iboun)       = ipoi6
                 lnodb(7,iboun)       = ipoi7
                 lnodb(8,iboun)       = ipoi8
                 lnodb(9,iboun)       = ipoi9
                 ltypb(iboun)         = QUA09
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
              end do
           end do
           ! Face 5: z=0
           do iy=1,nyexa
              do ix=1,nxexa
                 ipoi1                = 1+(ix-1)*2+(iy-1)*2*npx*npz
                 ipoi2                = ipoi1+2
                 ipoi3                = ipoi2+2*npx*npz
                 ipoi4                = ipoi1+2*npx*npz
                 ipoi5                = ipoi1+1
                 ipoi6                = ipoi2+npx*npz
                 ipoi7                = ipoi5+2*npx*npz
                 ipoi8                = ipoi1+npx*npz
                 ipoi9                = ipoi5+npx*npz
                 iboun                = iboun+1
                 ielem                = ix+(iy-1)*nxexa*nzexa
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 lnodb(3,iboun)       = ipoi3
                 lnodb(4,iboun)       = ipoi4
                 lnodb(5,iboun)       = ipoi5
                 lnodb(6,iboun)       = ipoi6
                 lnodb(7,iboun)       = ipoi7
                 lnodb(8,iboun)       = ipoi8
                 lnodb(9,iboun)       = ipoi9
                 ltypb(iboun)         = QUA09
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
              end do
           end do
           ! Face 6: z=Lz
           do iy=1,nyexa
              do ix=1,nxexa
                 ipoi1                = npx*(npz-1)+1+(ix-1)*2+(iy-1)*2*npx*npz
                 ipoi2                = ipoi1+2
                 ipoi3                = ipoi2+2*npx*npz
                 ipoi4                = ipoi1+2*npx*npz
                 ipoi5                = ipoi1+1
                 ipoi6                = ipoi2+npx*npz
                 ipoi7                = ipoi5+2*npx*npz
                 ipoi8                = ipoi1+npx*npz
                 ipoi9                = ipoi5+npx*npz
                 iboun                = iboun+1
                 ielem                = nxexa*(nzexa-1)+ix+(iy-1)*nxexa*nzexa
                 lnodb(1,iboun)       = ipoi1
                 lnodb(2,iboun)       = ipoi2
                 lnodb(3,iboun)       = ipoi3
                 lnodb(4,iboun)       = ipoi4
                 lnodb(5,iboun)       = ipoi5
                 lnodb(6,iboun)       = ipoi6
                 lnodb(7,iboun)       = ipoi7
                 lnodb(8,iboun)       = ipoi8
                 lnodb(9,iboun)       = ipoi9
                 ltypb(iboun)         = QUA09
                 pnodb                = nnode(ltypb(iboun))
                 lboel(pnodb+1,iboun) = ielem
              end do
           end do
        end if
     end if
     !
     ! Write periodicity
     !
     if(kfl_examp==1.or.kfl_examp==6) then
        iperi=0
        if(ndime==2) then
           iperi=npy
           do ipy=1,npy
              allocate(lperi(2,ipy),stat=istat)
              lperi(1,ipy)=(ipy-1)*npx+1
              lperi(2,ipy)=lperi(1,ipy)+npx-1
           end do
        else
           !
           ! 4-node periodicity
           !
           do ipy=1,npy
              ipoi1 = 1+(ipy-1)*npx*npz
              ipoi2 = ipoi1+npx*(npz-1)
              ipoi3 = ipoi2+nxexa
              ipoi4 = ipoi1+nxexa
              iperi = iperi+1
              !allocate(lperi(2,4),stat=istat)
              !lperi(1,iperi)%l(1)=ipoi1
              !lperi(iperi)%l(2)=ipoi2
              !lperi(iperi)%l(3)=ipoi3
              !lperi(iperi)%l(4)=ipoi4
           end do
           !
           ! x periodicity
           !
           do ipy=1,npy
              ipoi1=npx*npz*(ipy-1)+1
              do ipz=1,npz-2
                 ipoi1 = ipoi1+npx
                 ipoi2 = ipoi1+nxexa
                 iperi = iperi+1
                 !allocate(lperi(iperi)%l(2),stat=istat)
                 !lperi(iperi)%l(1)=ipoi1
                 !lperi(iperi)%l(2)=ipoi2           
              end do
           end do
           !
           ! z periodicity
           !
           do ipy=1,npy
              ipoi1=npx*npz*(ipy-1)+1
              do ipx=1,npx-2
                 ipoi1 = ipoi1+1
                 ipoi2 = ipoi1+npx*nzexa
                 iperi = iperi+1
                 !allocate(lperi(iperi)%l(2),stat=istat)
                 !lperi(iperi)%l(1)=ipoi1
                 !lperi(iperi)%l(2)=ipoi2           
              end do
           end do
        end if
     end if
     !
     ! KFL_CODNO: Fixity
     !
     ncodn=0
     ncodb=0
     if(  kfl_examp==1.or.kfl_examp==2.or.&
          kfl_examp==3.or.kfl_examp==4.or.&
          kfl_examp==6.or.kfl_examp==7.or.&
          kfl_examp==9) then
        ncodn=1
     end if
     if(kfl_examp==5.or.kfl_examp==7) then
        ncodb=1
     end if
     call membcs(one)
     call membcs(two)

     if(kfl_examp==1.or.kfl_examp==6) then
        !
        ! Periodic channel flow
        !
        if(ndime==2) then
           lcodn(0)=.true.
           lcodn(1)=.true.
           do ipoin=1,npx*npy      ! Interior
              kfl_codno(1,ipoin)=0
           end do
           if(kfl_examp==1) then
              kfl_codno(1,1)             = 1

              do ipoin=1,npx          ! Bottom wall: y=0'
                 kfl_codno(1,ipoin)=1
              end do
              ipoi1=npx*nyexa         ! Top wall: y=Ly'
              do ipoin=1,npx
                 ipoi1=ipoi1+1
                 kfl_codno(1,ipoi1)=1
              end do

              !kfl_codno(1,npx)           = 1
              !kfl_codno(1,npx*(npy-1)+1) = 1
              !kfl_codno(1,npx*npy)       = 1
           else
              lcodn(2)                 = .true.
              kfl_codno(1,1)             = 1
              kfl_codno(1,npx)           = 1
              kfl_codno(1,npx*(npy-1)+1) = 2
              kfl_codno(1,npx*npy)       = 2
           end if
        else
           lcodn(1)=.true.
           do ipoin=1,npx*npz      ! Bottom wall: y=0'
              kfl_codno(1,ipoin)=1
           end do
           ipoi1=npx*npz*nyexa     ! Top wall: y=Ly'
           do ipoin=1,npx*npz
              ipoi1=ipoi1+1
              kfl_codno(1,ipoi1)=1
           end do
        end if

     else if(kfl_examp==2) then
        !
        ! Thermal cavity
        !
        lcodn(0)=.true.
        lcodn(1)=.true.
        lcodn(2)=.true.
        lcodn(3)=.true.
        lcodn(4)=.true.
        lcodn(5)=.true.
        lcodn(6)=.true.
        ! 
        ! All domain'
        !
        do ipoin=1,npoin
           kfl_codno(1,ipoin)=0
        end do
        ! 
        ! Bottom wall: y=0'
        !
        do ipoin=1,npx*npz
           kfl_codno(1,ipoin)=1
        end do
        ! 
        ! Top wall: y=Ly'
        !
        ipoi1=npx*npz*nyexa
        do ipoin=1,npx*npz
           ipoi1=ipoi1+1
           kfl_codno(1,ipoi1)=2
        end do
        !
        ! x=0 wall
        !
        do ipy=1,npy
           ipoin=1+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=3
              ipoin=ipoin+npx
           end do
        end do
        ! 
        ! x=Lx wall
        !
        do ipy=1,npy
           ipoin=npx+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=4
              ipoin=ipoin+npx
           end do
        end do
        !
        ! z=0: Hot wall 
        !
        do ipy=1,npy
           ipoin=(ipy-1)*npx*npz
           do ipx=1,npx
              ipoin=ipoin+1
              kfl_codno(1,ipoin)=5
           end do
        end do
        !
        ! z=Lz: Cold wall
        !
        do ipy=1,npy
           ipoin=ipy*npx*npz-npx
           do ipx=1,npx
              ipoin=ipoin+1
              kfl_codno(1,ipoin)=6
           end do
        end do

     else if(kfl_examp==3) then
        !
        ! Wall
        !
        lcodn(0)=.true.
        lcodn(1)=.true.
        lcodn(2)=.true.
        lcodn(3)=.true.
        lcodn(4)=.true.
        lcodn(5)=.true.
        lcodn(6)=.true.
        ! 
        ! All domain
        !
        do ipoin=1,npoin
           kfl_codno(1,ipoin)=0
        end do
        ! 
        ! x=Lx wall
        !
        do ipy=1,npy
           ipoin=npx+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=4
              ipoin=ipoin+npx
           end do
        end do
        !
        ! z=0 wall
        !
        do ipy=1,npy
           ipoin=(ipy-1)*npx*npz
           do ipx=1,npx
              ipoin=ipoin+1
              kfl_codno(1,ipoin)=5
           end do
        end do
        !
        ! z=Lz wall
        !
        do ipy=1,npy
           ipoin=ipy*npx*npz-npx
           do ipx=1,npx
              ipoin=ipoin+1
              kfl_codno(1,ipoin)=6
           end do
        end do
        !
        ! Bottom wall: y=0
        !
        do ipoin=1,npx*npz
           kfl_codno(1,ipoin)=1
        end do
        ! 
        ! Top wall: y=Ly
        !
        ipoi1=npx*npz*nyexa
        do ipoin=1,npx*npz
           ipoi1=ipoi1+1
           kfl_codno(1,ipoi1)=2
        end do
        !
        ! x=0 wall
        !
        do ipy=1,npy
           ipoin=1+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=3
              ipoin=ipoin+npx
           end do
        end do

     else if(kfl_examp==4) then
        !
        ! Cavity
        !
        lcodn(0)=.true.
        lcodn(1)=.true.
        lcodn(2)=.true.
        ! 
        ! All domain'
        !
        do ipoin=1,npoin
           kfl_codno(1,ipoin)=0
        end do
        ! 
        ! Bottom wall: y=0'
        !
        do ipoin=1,npx*npz
           kfl_codno(1,ipoin)=1
        end do
        !
        ! x=0 wall
        !
        do ipy=1,npy
           ipoin=1+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=1
              ipoin=ipoin+npx
           end do
        end do
        ! 
        ! x=Lx wall
        !
        do ipy=1,npy
           ipoin=npx+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=1
              ipoin=ipoin+npx
           end do
        end do
        !
        ! z=0: Hot wall 
        !
        do ipy=1,npy
           ipoin=(ipy-1)*npx*npz
           do ipx=1,npx
              ipoin=ipoin+1
              kfl_codno(1,ipoin)=1
           end do
        end do
        !
        ! z=Lz: Cold wall
        !
        do ipy=1,npy
           ipoin=ipy*npx*npz-npx
           do ipx=1,npx
              ipoin=ipoin+1
              kfl_codno(1,ipoin)=1
           end do
        end do
        ! 
        ! Top wall: y=Ly'
        !
        ipoi1=npx*npz*nyexa
        do ipoin=1,npx*npz
           ipoi1=ipoi1+1
           kfl_codno(1,ipoi1)=2
        end do

     else if(kfl_examp==5) then
        !
        ! Wavequ: open domain on whole boundary
        !
        lcodb(1)=.true.
        lcodb(2)=.true.
        lcodb(3)=.true.
        lcodb(4)=.true.
        lcodb(5)=.true.
        lcodb(6)=.true.
        iboun=0
        do ii=1,nzexa*nxexa
           iboun=iboun+1
           kfl_codbo(iboun)=1
        end do
        do ii=1,nzexa*nxexa
           iboun=iboun+1
           kfl_codbo(iboun)=2
        end do
        do ii=1,nyexa*nzexa
           iboun=iboun+1
           kfl_codbo(iboun)=3
        end do
        do ii=1,nyexa*nzexa
           iboun=iboun+1
           kfl_codbo(iboun)=4
        end do
        do ii=1,nyexa*nxexa
           iboun=iboun+1
           kfl_codbo(iboun)=5
        end do
        do ii=1,nyexa*nxexa
           iboun=iboun+1
           kfl_codbo(iboun)=6
        end do

     else if(kfl_examp==7) then
        !
        ! Developping channel flow
        !
        lcodn(0:8)=.true.
        lcodb(1:4)=.true.
        do ipoin=1,npoin              ! All domain
           kfl_codno(1,ipoin)=0
        end do
        do ipoin=1,npx*npz            ! Bottom wall: y=0
           kfl_codno(1,ipoin)=1
        end do
        do ipy=1,npy                  ! Left wall:   x=0
           ipoin=1+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=2
              ipoin=ipoin+npx
           end do
        end do
        do ipy=1,npy                  ! Right wall:  x=Lx
           ipoin=npx+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=3
              ipoin=ipoin+npx
           end do
        end do
        ipoi1=npx*npz*nyexa
        do ipoin=1,npx*npz            ! Top wall:    y=Ly
           ipoi1=ipoi1+1
           kfl_codno(1,ipoi1)=7
        end do
        kfl_codno(1,1)             = 4  ! Bot left corner
        kfl_codno(1,npx)           = 8  ! Bot right corner
        kfl_codno(1,npx*npy-npx+1) = 5  ! Top left corner
        kfl_codno(1,npx*npy)       = 6  ! Top right corner
        
        iboun=0
        do ii=1,nxexa
           iboun=iboun+1
           kfl_codbo(iboun)=1
        end do
        do ii=1,nyexa
           iboun=iboun+1
           kfl_codbo(iboun)=2
        end do
        do ii=1,nxexa
           iboun=iboun+1
           kfl_codbo(iboun)=3
        end do
        do ii=1,nyexa
           iboun=iboun+1
           kfl_codbo(iboun)=4
        end do

     else if(kfl_examp==9) then
        !
        ! Cavity
        !
        lcodn(0)=.true.
        lcodn(1)=.true.
        lcodn(2)=.true.
        ! 
        ! All domain'
        !
        do ipoin=1,npoin
           kfl_codno(1,ipoin)=0
        end do
        ! 
        ! Bottom wall: y=0'
        !
        do ipoin=1,npx*npz
           kfl_codno(1,ipoin)=2
        end do
        ! 
        ! Top wall: y=Ly'
        !
        ipoi1=npx*npz*nyexa
        do ipoin=1,npx*npz
           ipoi1=ipoi1+1
           kfl_codno(1,ipoi1)=2
        end do
        !
        ! x=0 wall
        !
        do ipy=1,npy
           ipoin=1+(ipy-1)*npx*npz
           do ipz=1,npz
              kfl_codno(1,ipoin)=1
              ipoin=ipoin+npx
           end do
        end do

     end if
     !
     ! Number of used codes
     !
     if(ncodn>0) then
        ncodn=0
        do icode=-mcodb,mcodb
           if(lcodn(icode)) ncodn=ncodn+1
        end do
     end if
     if(ncodb>0) then
        ncodb=0
        do icode=-mcodb,mcodb
           if(lcodb(icode)) ncodb=ncodb+1
        end do
     end if
     !
     ! Sets
     !
     if(nboun>0) then
        if(ndime==2) then
           nbset=4
        else
           nbset=6
        end if
        neset=1
        call memose(1_ip)
        call memose(2_ip)
        call memose(4_ip)
        call memose(5_ip)
        lesec(1)=1
        do ielem=1,nelem
           leset(ielem)=1
        end do
        if(ndime==2) then
           lbsec(1)=1
           lbsec(2)=2
           lbsec(3)=3
           lbsec(4)=4
           iboun=0
           do ii=1,nxexa
              iboun=iboun+1
              lbset(iboun)=1
           end do
           do ii=1,nyexa
              iboun=iboun+1
              lbset(iboun)=2
           end do
           do ii=1,nxexa
              iboun=iboun+1
              lbset(iboun)=3
           end do
           do ii=1,nyexa
              iboun=iboun+1
              lbset(iboun)=4
           end do
        end if

     end if
     !
     ! LNNOD
     !
     do ielem = 1,nelem
        pelty = ltype(ielem)
        lnnod(ielem) = nnode(pelty)
     end do
     !
     ! Read b.c.
     !
     call reabcs()
     !
     ! LBOEL
     !
     call lbouel()

  end if

end subroutine exampl 
