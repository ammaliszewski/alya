subroutine readom()
  !-----------------------------------------------------------------------
  !****f* domain/domain
  ! NAME
  !    domain
  ! DESCRIPTION
  !    This is the main routine of the domain. It performs the operations 
  !    needed to build up the domain data for the run.
  ! USED BY
  !    Turnon
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  implicit none
  !
  ! Open files
  !
  call opfdom(1_ip)
  !
  ! Read geometry data
  ! 
  if( kfl_examp /= 0 ) then
     call exampl()
  else
     call readim()                         ! Read dimensions: NDIME, NPOIN, NELEM...
     call reastr()                         ! Read strategy:   LQUAD, NGAUS, SCALE, TRANS...
     call cderda()                         ! Derivated param: NTENS, HNATU, MNODE, MGAUS...
     call reageo()                         ! Read arrays:     LTYPE, LNODS, LNODB, LTYPB, COORD...
     call reaset()                         ! Read sets:       LESET, LBSET, LNSET
     call reabcs()                         ! Read bcs:        KFL_FIXNO, KFL_FIXBO, KFL_GEONO
  end if
  !
  ! Create contacts, when needed. And leave run.
  !
  if (kfl_creco == 1) then
     if ( IPARALL ) call runend('DOMAIN: CONTACT CREATION MUST BE DONE SEQUENTIALLY')
     call crecon
     call runend('O.K.!')
  end if
  !
  ! Split mesh in zones
  !
  if (kfl_creco ==2) then
     if ( IPARALL ) call runend('DOMAIN: MESH SPLITTING MUST BE DONE SEQUENTIALLY')
     call splmsh()
     call runend('O.K!')
  end if
  !
  ! Debugging subroutine XTRACT
  ! Its call must remain commented unless a mesh sector extraction is being carried out
  !
  !!call xtract 
  ! Divide mesh
  !
  call divtet()  
  !
  ! Adapt mesh
  !
  call adamsh(1_ip)
  !
  ! Services
  !
  call Dodeme(1_ip)                        ! Create extension elements
  !
  ! Compute node/element connectivity
  ! 
  if( INOTSLAVE .and. .not. READ_AND_RUN() ) then
     call period(1_ip)        
     call domgra()                         ! NEPOI, PELPO and LELPO, R_DOM, C_DOM
     call period(2_ip)
  end if
  !
  ! Check if tehre exist unconnected meshes
  !
  call mescek(8_ip)  
  !
  ! Automatic boundaries
  !
  call dombou()
  !
  ! Groups for deflated-like solvers
  !
  call grodom(1_ip)                        ! LGROU_DOM
  !
  ! Generate element sets automatically
  !
  call genset()
  !
  ! Services
  ! 
  call Parall(1_ip)                        ! Partition graph
  !
  ! temporary writing of 2d mesh
  !
  !call wriaux()

end subroutine readom
!------------------------------------------------------------------------
! NOTES
! 
!    Flags and scalars:
!       KFL_CHEGE
!       KFL_NAXIS .................. Axi-symmetric geometry (=1)
!       DELTA_DOM .................. Distance to the wall    
!       ROUGH_DOM .................. Roughness of the wall
!    Element types:
!       NELTY ...................... Total number of element types (def_elmtyp)
!       MGAUS ...................... Maximum # of element Gauss points in the mesh
!       MLAPL ...................... 1 if at least one element needs Laplacian
!       CENAL(NELTY) ............... Alternative element name (for Ensight format)
!       CENAM(NELTY) ............... Element name: 3 letters (type) + 2 figures (# nodes)
!       CETOP(NELTY) ............... Topology name
!       HNATU(NELTY) ............... Element natural length: 2 Quad/Hexa, 1 Others
!       LDIME(NELTY) ............... Dimension
!       LLAPL(NELTY) ............... If Laplacian exists
!       LNUTY(NELTY) ............... Number of elements for corresponding type
!       LORDE(NELTY) ............... Order of the interpolation (1,2,3, etc)
!       LQUAD(NELTY) ............... Integration quadrature
!       LRULE(NELTY) ............... Integration rule
!       LTOPO(NELTY) ............... Topology (line, quad, tru, penta, pyr)
!       NGAUS(NELTY) ............... # of integration points 
!       NNODE(NELTY) ............... Number of nodes
!    Element arrays:
!       NELEM ...................... Number of elements
!       MNODE ...................... Maximum number of nodes per element
!       NBOPO ...................... Number of boundary nodes
!       NTENS ...................... # of independent Hessian matrix values
!       NINER ...................... # of independent inertia tensor values
!    ** COORD(NDIME,NPOIN) ......... Node coordinates
!       EXNOR(NDIME,NDIME,NBOPO) ... Exterior normal
!       LEXIS(NELEM) ............... If element type exists
!    ** LNODS(MNODE,NELEM) ......... Connectivity
!       LPOTY(NPOIN) ............... Node type: 0=interior, IBOPO=boundary
!    ** LTYPE(NELEM) ............... Type (BAR02, QUA04, etc as defined in def_elmtyp)
!       SKCOS(NDIME,NDIME,NSKEW) ... Local skew systems
!       VMASS(NPOIN) ............... Diagonal mass matrix
!    Boundary arrays:
!       MGAUB ...................... Maximum # of boundary Gaus points in the mesh
!       MNODB ...................... Maximum number of nodes per boundary
!       NDIMB ...................... Boundary dimension (NDIME-1)
!       LBOEL(MNODB+1,NBOUN) ....... Boundary/Element connectivity + connected element
!       LNODB(MNODB,NBOUN) ......... Connectivity
!       LTYPB(NBOUN) ............... Boundary Type
!       NBONO ...................... Number of boundary nodes (Explicitly declared in LNODB)
!       LBONO(NBONO) ............... List of boundary nodes (Explicitly declared in LNODB)
!    Graphs:
!       R_DOM(NPOIN+1) ............. Node/node       graph: pointer to C_DOM
!       C_DOM(NZDOM) ............... Node/node       graph: list of connectivities
!       LELPO(NPOIN+1) ............. Node/element    graph: pointer to PELPO
!       PELPO(LELPO(NPOIN+1)) ...... Node/element    graph: list of connectivities
!       LELEL(NPOIN+1) ............. Element/element graph: pointer to PELEL
!       PELEL(LELEL(NPOIN+1)) ...... Element/element graph: list of connectivities
!    Witness points:
!       MWITN ...................... Max # Number of witness points
!       NWITN ...................... # of witness points
!       LEWIT(NWITN) ............... Host elements of witness points
!       COWIT(NWITN) ............... Coordinates of witness points
!       SHWIT(MNODE,MWITN) ......... Shape function in host element
!    Materials:
!       NMATE ...................... # materials
!    ** LMATE(NELEM) ............... List of materials
!       LMATN(NPOIN) ............... List of nodol materials
!    Sets:
!       NESET ...................... # element sets
!       NBSET ...................... # boundary sets
!       NNSET ...................... # node sets
!    ** LESET(NELEM) ............... List of element sets
!       LESEC(NESET) ............... Element sets numbers
!    ** LBSET(NBOUN) ............... List of boundary sets
!       LBSEC(NBSET) ............... Boundary sets numbers
!       LNSEC(NNSET) ............... Node sets numbers
!    Zones:
!       NZONE ...................... Number of zones
!       NELEZ(NZONE) ............... Number of elements per zone
!    ** LELEZ(NZONE) % L(:) ........ List of element for each zone
!    Subdomains:
!       NSUBD ...................... Number of subdomains
!    ** LESUB(NELEM) ............... List of element subdomains (value=1:NSUBD)
!    Fields:
!    ** XFIEL(NFIEL) % A(:,NPOIN/NBOUN/nelem) ... Values of field 
!    Permutation with original mesh:
!       LNLEV(NPOIN) ............... Node permutation
!       LELEV(NELEM) ............... Element permutation
!       LBLEV(NBOUN) ............... Boundary permutation
!    Groups:
!       LGROU_DOM(NPOIN) ........... List of groups (computed) 
!  
!    README:
!    -------
!
!     *: scalar read from geometry file
!    **: array read from geometry file
!
!    A geometrical array should be involved in the following subroutines:
!    - def_domain ................. Declaration of variable
!    - inidom ..................... Nullify pointer
!    - readim ..................... Variable dimension (possibly)
!    - reageo/reaset/reabcs ....... Read the array
!    - submsh ..................... Mesh multiplication
!    - renelm ..................... Element renumbering
!    - par_sendat ................. Send dimension (possibly)
!    - par_sengeo ................. Parall distribution to subdomains
!    - par_fringe ................. Fringe geometry
!
!***
!------------------------------------------------------------------------
