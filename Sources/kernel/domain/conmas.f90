subroutine conmas()
  !-----------------------------------------------------------------------
  !****f* domain/conmas
  ! NAME
  !    conmas
  ! DESCRIPTION
  !    This routines calculates the consistent mass matrix (symmetric) 
  ! OUTPUT
  !    AMATR(NZSYM) : Consistent mass matrix
  ! USED BY
  !    Domain
  !*** 
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_master
  use def_domain
  implicit none
  integer(ip)  :: ipoin,idime,inode,ielem,izsym
  integer(ip)  :: pgaus,pnode,pelty
  real(rp)     :: gpvol(mgaus),dummr
  real(rp)     :: gpcar(ndime,mnode,mgaus)
  real(rp)     :: elcod(ndime,mnode),elmat(mnode,mnode)

  if( INOTMASTER .and. kfl_conma == 1 ) then

     call livinf(0_ip,'COMPUTE CONSISTENT MASS MATRIX',0_ip)
     !
     ! Initialization
     !
     do izsym = 1,nzsym
        cmass(izsym) = 0.0_rp
     end do
     !
     ! Loop over elements
     !
     elements: do ielem=1,nelem

        pelty = ltype(ielem) 
        pnode = nnode(pelty)
        pgaus = ngaus(pelty)

        do inode = 1,pnode
           ipoin = lnods(inode,ielem)
           do idime = 1,ndime
              elcod(idime,inode) = coord(idime,ipoin)
           end do
        end do

        call elmcar(&
             pnode,pgaus,0_ip,elmar(pelty)%weigp,elmar(pelty)%shape,&
             elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,gpvol,gpcar,&
             dummr,ielem)
        call elmmas(pnode,pgaus,gpvol,elmar(pelty)%shape,elmat)
        call csrase(elmat,cmass,1_ip,pnode,pnode,lnods(1,ielem),3_ip)

     end do elements

  end if

end subroutine conmas
