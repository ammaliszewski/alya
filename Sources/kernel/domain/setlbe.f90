subroutine setlbe
  !-----------------------------------------------------------------------
  !****f* Domain/setlbe
  ! NAME
  !    setlbe
  ! DESCRIPTION
  !    This routine constucts boundary arrays
  ! OUTPUT
  !    LBOEL(MNODB+1,NBOUN) ... Boundary/Element connectivity
  !    LTYPB(NBOUN) ........... Type of boundary
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  implicit none
  integer(ip)          :: iboun,ielem,inodb,ipoin,jpoin,inode,idime
  integer(ip)          :: knodb,knode

  real(rp)             :: disti

  !
  !  DEBUG: WRITE LNODB WITH LBOEL (LEAVE IT COMMENTED UNLESS YOU NEED IT - RUTH)
  !
  !  call export_bound


  !
  !  DEBUG: CREATE ZONES FILES ACCORDING TO A CERTAIN CRITERIUM (LEAVE IT COMMENTED UNLESS YOU NEED IT)
  !
!    do ielem=1,nelem
!       knode= 0
!       do inode=1,4
!          ipoin= lnods(inode,ielem)
!          disti = 0.0_rp
!          do idime=1,ndime
!             disti= disti + coord(idime,ipoin)*coord(idime,ipoin)
!          end do
!          disti= sqrt(disti)
!          if (disti < 0.0081) then
!             knode= knode+1
!          end if
!       end do
!       if (knode == 4) then
!          write(939,*) ielem
!       else
!          write(940,*) ielem
!       end if
!    end do  
!    stop


  if( INOTMASTER ) then     
     !
     ! LBOEL if the elements connected to the boundaries are unknown
     !
     call lbouel()
     !
     ! Calculate only the boundary/element nodal connectivity
     !
     do iboun = 1,nboun
        knodb = nnode(ltypb(iboun))
        ielem = lboel(knodb+1,iboun)
        knode = nnode(ltype(ielem))
        do inodb = 1,knodb
           ipoin = lnodb(inodb,iboun)
           nodes3: do inode = 1,knode
              jpoin = lnods(inode,ielem)
              if( ipoin == jpoin ) then
                 lboel(inodb,iboun) = inode
                 exit nodes3
              end if
           end do nodes3
        end do
     end do
     !
     ! Check element/boundary local numbering
     !
     call mescek(7_ip)

  end if

100 format(20(2x,i12))
200 format(i12,2x,20(2x,f15.8))

end subroutine setlbe
