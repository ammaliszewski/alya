subroutine materi()
  !-----------------------------------------------------------------------
  !****f* Domain/materi
  ! NAME
  !    materi
  ! DESCRIPTION
  !    Separate fluid from solid materials: LMATN
  !    1=fluid, -1=solid, 0=interface 
  !
  !    Solid    Interface       Fluid
  !     -1          0             1
  !     +-----------+-------------+
  !     |           |             |
  !     |           |             |
  !     |           |             |
  !     |           |             |
  !     +-----------+-------------+ 
  !     -1          0             1
  ! OUTPUT
  ! USED BY
  !    domain
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_elmtyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_memchk
  implicit none
  integer(ip) :: ipoin,ielem,inode,imate,kmate,kpoin,ierro
  integer(ip) :: kfl_mater(100)
  !
  ! Check non-assigned materials
  !
  if( INOTMASTER ) then
     ierro = 0
     do ielem = 1,nelem
        if( lmate(ielem) == 0 ) then
           ierro = ierro + 1
        end if
     end do
  end if
  call parari('SUM',0_ip,1_ip,ierro)
  if( ierro /= 0 ) call runend('SOME ELEMENTS WERE NOT ASSIGNED A MATERIAL')
  !
  ! Contact elements should be declared as solid elements
  !
  if( INOTMASTER ) then
     do ielem = 1,nelem
        if( lelch(ielem) == ELCNT ) then
           lmate(ielem) = min(nmate,nmatf+1)
        else if( lelch(ielem) == ELCOH ) then
           lmate(ielem) = min(nmate,nmatf+1)
        end if
     end do
     !
     ! Count number of materials
     !
     do imate = 1,100
        kfl_mater(imate) = 0
     end do
     kmate = 0
     do ielem = 1,nelem
        imate = lmate(ielem)
        if( kfl_mater(imate) == 0 ) then
           kmate = kmate + 1
           kfl_mater(imate) = 1
        end if
     end do
     if( nmate <= 1 ) nmate = kmate
     !
     ! Allocate memory for LMATN and NMATN
     !
     call memgeo(35_ip)
     call memgen(1_ip,npoin,0_ip)

     do imate = 1,nmate
        !
        ! GISCA(IPOIN) = 1: nodes with material IMATE
        !
        do ielem = 1,nelem
           if( lmate(ielem) == imate ) then
              do inode = 1,lnnod(ielem)
                 ipoin = lnods(inode,ielem)
                 gisca(ipoin) = 1
              end do
           end if
        end do
        call parari('SLX',NPOIN_TYPE,npoin,gisca)
        !
        ! KPOIN = number of nodes with material IMATE
        !
        kpoin = 0
        do ipoin = 1,npoin
           if( gisca(ipoin) > 0 ) kpoin = kpoin + 1
        end do
        igene = imate
        igen2 = kpoin
        call memgeo(36_ip)
        !
        ! LMATN(IMATE) % L(:) = List of node with material IMATE
        !
        kpoin = 0
        do ipoin = 1,npoin
           if( gisca(ipoin) > 0 ) then
              kpoin = kpoin + 1
              lmatn(imate) % l(kpoin) = ipoin
              gisca(ipoin) = 0
           end if
        end do
        nmatn(imate) = kpoin
     end do
     call memgen(3_ip,npoin,0_ip)

  end if

end subroutine materi
