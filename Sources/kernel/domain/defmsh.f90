subroutine defmsh
!-----------------------------------------------------------------------
!****f* domain/defmsh
! NAME
!    defmsh
! DESCRIPTION
!    This routine defines a new mesh
! USES
! USED BY
!    Newmsh
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solver
  use      mod_memchk

!  use dfport
!  use dflib

  implicit none

  if(kfl_algor_msh==1) then                  ! SHEAR_SLIP

  !renum = .false.
     !
     ! Compute boundary connectivity. 
     !
  !if(kfl_autbo==1) then
!     call dombou                                         ! LNODB and LBOEL
  !else
  !   call setlbe                                         ! LBOEL
  !end if
!
! Compute shape & deriv for elem and bound ; also shaga.
!
  !call cshder
!
! Check and output geometry, if needed.
!
  !if(kfl_chege==1) call mescek
  !if(kfl_oumes==1) call domtst(one)                      ! Postprocess geometry
!
! Create connectivities of points with neighbours.
!
  call connpo                                            ! General connectivities
!
! Compute the external normal
!
  call extnor(1_ip)                                            ! EXNOR, LPOTY
!
! Read list of slaves and create solver connectivities.
!
  call cresla                                            ! LMASL, R_SOL, C_SOL
!
! Output exterior normals
!
  !if(kfl_oumes==1) call domtst(two)                      ! Postprocess EXNOR

     !nzmat   = max(nzsol,nzsky)
     !allocate(amatr(mxdof*mxdof*nzmat*nusol),stat=istat)
     !call memchk(zero,istat,mem_modul(1:2,modul),'AMATR','defmsh',amatr)

  elseif(kfl_algor_msh==2) then              ! GLOBAL REFINEMENT

     !istat = runqq ('refine.bat', '')
     !istat=system('refine.bat')
                 ! A call to gid to generate the new mesh goes here

     call Domain ! read the mesh again from gid

  end if

end subroutine defmsh
