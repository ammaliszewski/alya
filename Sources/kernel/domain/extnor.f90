!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    extnor.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Compute exterior normals
!> @details Computes exterior normals EXNOR and determine 
!>          boundary nodes LPOTY.
!>
!>          \verbatim
!>       
!>          ITASK = 1 ... EXNOR and LPOTY
!>                = 2 ... EXNOR
!> 
!>          Let u = 1 on node, 0 elsewehere
!>          Let We be the patch of node i
!>          Let Se be the boundary of the patch We
!>          Let n_x^i be the normal in x for node i        
!>
!>          Interior node    Boundary node
!>          0----0----0      0----1----0      
!>          |    |    |      |    |    |
!>          0----1----0      0----0----0
!>          |    |    |      |    |    |
!>          0----0----0      0----0----0
!>
!>          n_x^i = Sum_e Sum_g dNi/dx |J|w_g = int_We du/dx = int_Se u.nx
!>          So n_x^i  = 0 for interior node
!>                   /= 0 for boundary nodes 
!>
!>       OUTPUT
!>          NBOPO ...................... Number of points located on the boundary
!>          EXNOR(NDIME,NDIME,NBOPO) ... Local basis at boundary nodes
!>          LPOTY(NPOIN) ............... defined by LPOTY(IPOIN):   
!>                                 = 0      if IPOIN is interior
!>                                 = IBOPO  # of boundary pointRead mesh dimensions:
!>          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine extnor(itask)
  use def_kintyp
  use def_parame
  use def_master
  use def_elmtyp
  use def_domain
  use def_kermod,         only : ndivi
  use mod_memory
  use mod_communications, only : PAR_SUM
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ielem,igaus,pelty,iblty,inode
  integer(ip)             :: ipoin,iboun,inodb,pnode,ifoun
  integer(ip)             :: ierro,knode,idime
  real(rp)                :: gpcar(ndime,mnode)
  real(rp)                :: xjaci(ndime,ndime)
  real(rp)                :: xjacm(ndime,ndime) 
  real(rp)                :: elcod(ndime,mnode)
  real(rp)                :: elaux(ndime,mnode)
  real(rp)                :: gpvol,gpdet,exwn2,xfact

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

  call livinf(0_ip,'COMPUTE EXTERIOR NORMALS',0_ip)

  ierro = 0

  if( INOTMASTER ) then
     !
     ! Memory allocation and initializations 
     !
     if( itask == 1 ) then

        call memory_alloca(memor_dom,'LPOTY','extnor',lpoty,npoin)

     else if( itask == 2 ) then     ! this idea does nor work it fail for elements that have a node at the boundary but no boundary
        !
        ! GISCA(IELEM) = 1 if the element is near the boundary
        !
        call memgen(1_ip,nelem,0_ip)
        elements: do ielem = 1,nelem
           pelty = ltype(ielem) 
           pnode = nnode(pelty)
           do inode =1, pnode
              ipoin = lnods(inode,ielem)
              if ( lpoty(ipoin) /= 0 ) gisca(ielem) = 1
           end do
        end do elements
     end if

     call memory_alloca(memor_dom,'EXAUX','extnor',exaux,ndime,npoin)

     !-------------------------------------------------------------------
     !
     ! Compute element integrals that contribute to exaux  
     !  
     !-------------------------------------------------------------------
     !
     !$OMP  PARALLEL DO SCHEDULE (GUIDED)                           & 
     !$OMP  DEFAULT (NONE)                                          &
     !$OMP  PRIVATE ( ielem, pelty, pnode, inode, ipoin, igaus,     &
     !$OMP            gpcar, gpdet, xjacm, xjaci, gpvol, elcod,     &
     !$OMP            elaux, ifoun, knode )                         &
     !$OMP   SHARED ( ltype, nnode, lnods, coord, nelem, ngaus,     &
     !$OMP            ndime, elmar, exaux, lelch, itask, gisca  )
     !
     do ielem = 1,nelem
        pelty = ltype(ielem)
        ifoun = 1
        if( itask == 2 ) then
           if( gisca(ielem) == 0 ) ifoun = 0
        end if

        if( pelty > 0 .and. ifoun == 1 ) then
           pnode = nnode(pelty)
           if( lelch(ielem) == ELEXT ) then
              knode = 1
           else
              knode = pnode
           end if
           do inode = 1,pnode
              ipoin = lnods(inode,ielem)
              elcod(1:ndime,inode) = coord(1:ndime,ipoin)
              elaux(1:ndime,inode) = 0.0_rp
           end do
           do igaus = 1,ngaus(pelty)
              call jacobi(&
                   ndime,pnode,elcod,elmar(pelty) % deriv(1,1,igaus),&
                   xjacm,xjaci,gpcar,gpdet)
              gpvol = elmar(pelty) % weigp(igaus) * gpdet
              do inode = 1,knode
                 elaux(1:ndime,inode) = elaux(1:ndime,inode) + gpcar(1:ndime,inode) * gpvol
              end do
           end do          
           do inode = 1,knode
              ipoin = lnods(inode,ielem)
              do idime = 1,ndime
                 !$OMP ATOMIC             
                 exaux(idime,ipoin) = exaux(idime,ipoin) + elaux(idime,inode)
              end do
           end do
        end if
     end do

     !-------------------------------------------------------------------
     !
     ! Modify EXAUX due to periodicity and Parall service
     !
     !-------------------------------------------------------------------

     call rhsmod(ndime,exaux)

     !-------------------------------------------------------------------
     !
     ! NBOPO: Counting of the boundary points
     !
     !-------------------------------------------------------------------

     if( itask == 1 ) then

        nbopo = 0 
        if( ndime == 1 ) then

           do ipoin = 1,npoin
              exwn2 = exaux(1,ipoin) * exaux(1,ipoin) 
              if( exwn2 > 0.0001_rp ) then
                 nbopo        = nbopo + 1
                 lpoty(ipoin) = 1           
              end if
           end do

        else if( ndime == 2 ) then

           do ipoin = 1,npoin
              exwn2 = exaux(1,ipoin) * exaux(1,ipoin) &
                   +  exaux(2,ipoin) * exaux(2,ipoin)
              if( exwn2 > 0.001_rp*vmass(ipoin) ) then
                 nbopo        = nbopo + 1
                 lpoty(ipoin) = 1           
              end if
           end do

        else

           xfact = 4.0_rp/3.0_rp
           do ipoin = 1,npoin
              exwn2 = exaux(1,ipoin) * exaux(1,ipoin) &
                   +  exaux(2,ipoin) * exaux(2,ipoin) &
                   +  exaux(3,ipoin) * exaux(3,ipoin)
              if( exwn2 > 0.001_rp*vmass(ipoin)**xfact ) then
                 nbopo        = nbopo + 1
                 lpoty(ipoin) = 1           
              end if
           end do

        end if

        !-------------------------------------------------------------------
        !
        ! Look for additional boundary nodes not detected by exaux.
        ! They can be given explicitly by the user in LNODB.
        ! The nodes of hole boundaries are taken off from this list.
        ! This is useful for example for Nastin which wants to automatically
        ! prescribe the pressure when LPOTY(IPOIN) /= 0.
        !
        !-------------------------------------------------------------------

        do iboun = 1,nboun_2
           iblty = ltypb(iboun)
           if( lboch(iboun) /= BOEXT ) then
              do inodb = 1,nnode(iblty)
                 ipoin = lnodb(inodb,iboun)
                 if( ipoin <= npoin ) then
                    if( lpoty(ipoin) == 0 ) then
                       nbopo        =  nbopo + 1
                       lpoty(ipoin) = -1
                    end if
                 end if
              end do
           end if
        end do

     end if

     !-------------------------------------------------------------------
     !
     ! Compute the vectors EXNOR and LPOTY
     !
     !-------------------------------------------------------------------

     if( nbopo > 0 ) then
        if( itask == 1 ) call memgeo(15_ip)
        call setext(itask,exaux,ierro)
     end if
     !
     ! Deallocate memory
     !  
     call memory_deallo(memor_dom,'EXAUX','extnor',exaux)

     if( itask == 2 ) call memgen(3_ip,nelem,0_ip)

  end if

  !-------------------------------------------------------------------
  !
  ! Check errors
  !
  !-------------------------------------------------------------------

  call PAR_SUM(ierro)

  !-------------------------------------------------------------------
  !
  ! Fill in mesh type
  !
  !-------------------------------------------------------------------

  meshe(ndivi) % exnor => exnor
  meshe(ndivi) % nbopo =  nbopo
  meshe(ndivi) % lpoty => lpoty
  
  !call parari('SUM',0_ip,1_ip,ierro)
  if( ierro > 0 ) then
!     call runend('EXTNOR: SOME TANGENT VECTORS COULD NOT BE COMPUTED')
     if( INOTSLAVE ) write(*,*)'EXTNOR: SOME TANGENT VECTORS COULD NOT BE COMPUTED - SEE SETEXT: NORMAL COULD NOT BE ...'
  end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif


end subroutine extnor

