subroutine elsini()
  !-----------------------------------------------------------------------
  !****f* elsest/elsini
  ! NAME
  !    elsini
  ! DESCRIPTION
  !    This routine prepares Elsest
  ! USED BY
  !    domain
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_inpout
  use mod_elsest, only : elsest_initialization
  implicit none

  if( kfl_elses == 1 .and. INOTMASTER ) then
     !ielse(7)  = 90
     !ielse(12) = 91
     !ielse(13) = 92
     call elsest_initialization(ielse,relse,meshe(ndivi))
  end if

end subroutine elsini
