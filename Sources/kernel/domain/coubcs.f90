subroutine coubcs(ibono)
  !-----------------------------------------------------------------------
  !****f* Domain/coubcs
  ! NAME
  !    coubcs
  ! DESCRIPTION
  !    Reads boundary conditions flags
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_iofile
  implicit none
  integer(ip), intent(in) :: ibono
  integer(ip)             :: icode
  !
  ! Number of used codes
  !
  if( kfl_icodn > 0 ) then
     ncodn = 0
     do icode = -mcodb,mcodb
        if( lcodn(icode) ) ncodn = ncodn + 1
     end do
  end if
  if( ncodb > 0 ) then
     ncodb = 0
     do icode = -mcodb,mcodb
        if( lcodb(icode) ) ncodb = ncodb + 1
     end do
  end if
  !
  ! Combinations
  !
  if( ibono == 1 ) then
     ncodn = ncodc
  end if

end subroutine coubcs
