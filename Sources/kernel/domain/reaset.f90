!-----------------------------------------------------------------------
!> @addtogroup DomainInput
!> @{
!> @file    reaset.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Read sets definitions
!> @details Read sets definitions.
!!          Sets are used to compute some global values, computed as 
!!          volume integrals, boundary integrals or node-wise.
!!          It can be useful for example to compute the force exterted
!!          by the fluid on a body, by assigning a set to all the boundary elements
!!          that define the body. For each module with extension 'ext', the results 
!!          are written in the following files:
!!          - Element sets: *-element.ext.set
!!          - Boundary sets: *-boundary.ext.set
!!          - Node sets: *-node.ext.set
!!          The output variables ares:
!!          \verbatim
!!          - NESET .......... Number of element sets (read)
!!          - NBSET .......... Number of boundary sets (read)
!!          - NNSET .......... Number of node sets (read)
!!          - LESET(NELEM) ... List of element sets (read)
!!          - LNSET(NBOUN) ... List of boundary sets (read)
!!          - LESEC(NESET) ... List of element set numbers
!!          - LBSEC(NBSET) ... List of boundary set numbers
!!          - LNSEC(NNSET) ... List of node set numbers
!!          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine reaset()
  use      def_kintyp
  use      def_parame
  use      def_master
  use      def_domain
  use      def_inpout
  use      mod_iofile
  implicit none
  integer(ip) :: ielem,jelem,keset,ieset,iesta,iesto
  integer(ip) :: ibsta,ibsto
  integer(ip) :: iboun,jboun,kbset,ibset,ipara,ipoin,kpoin
  integer(ip) :: idime
  real(rp)    :: coori(3),dista,dimin,dummr

  if( ISEQUEN .or. ( IMASTER .and. kfl_ptask /= 2 ) ) then
     !
     ! Initializations
     !
     neset     = 0                   ! There is no element set
     nbset     = 0                   ! There is no boundary set
     nnset     = 0                   ! There is no node set
     kfl_neset = 0                   ! No automatic element sets
     setno     = 0.0_rp              ! Automatic element sets: normal
     setpo     = 0.0_rp              ! Automatic element sets: 4 nodes
     !
     ! Read vectors
     !
     call ecoute('REASET')
     do while( words(1) /= 'SETS ' .and. words(1) /= 'SETSA' )
        call ecoute('REASET')
     end do
     if( exists('OFF  ') ) then
        do while( words(1) /= 'ENDSE' )
           call ecoute('reaset')
        end do
        return
     end if
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Sets definitions
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> SETS
     ! ADOC[d]> SETS:
     ! ADOC[d]> Sets are used to compute some global values, computed as 
     ! ADOC[d]> volume integrals, boundary integrals or node-wise.
     ! ADOC[d]> It can be useful for example to compute the force exterted
     ! ADOC[d]> by the fluid on a body, by assigning a set to all the boundary elements
     ! ADOC[d]> that define the body. For each module with extension 'ext', the results 
     ! ADOC[d]> are written in the following files:
     ! ADOC[d]> <ul>
     ! ADOC[d]> <li> Element sets: *-element.ext.set    </li>
     ! ADOC[d]> <li> Boundary sets: *-boundary.ext.set  </li>
     ! ADOC[d]> <li> Node sets: *-node.ext.set          </li>
     ! ADOC[d]> </ul>
     !
     do while( words(1) /='ENDSE')
        call ecoute('reaset')

        if( words(1) =='ELEME' ) then 
           !
           ! ADOC[1]> ELEMENTS: [ALL= int]
           ! ADOC[1]>   int1 int2                                                                   $ Element, set number
           ! ADOC[1]>   ...
           ! ADOC[1]> END_ELEMENTS
           ! ADOC[d]> ELEMENTS:
           ! ADOC[d]> This field defines the elements sets. It contains a list of elements (int1) and 
           ! ADOC[d]> the set assigned to it (int2). The option ALL= int assigns automatically the
           ! ADOC[d]> set int to all the elements of the mesh.
           ! 
           if( words(2) == 'ALL  ' ) then
              neset = 1
              call memose(one)
              ieset = getint('ALL  ',0_ip,'#SET NUMBER')
              if( ieset == 0 ) call runend('REASET: WRONG SET NUMBER')
              do ielem = 1,nelem
                 leset(ielem)=ieset
              end do
              do while( words(1) /='ENDEL')
                 call ecoute('reaset')
              end do
           else if( words(2) == 'AUTOM' ) then
              neset     = 1
              ieset     = 0
              kfl_neset = getint('AUTOM',1_ip,'#SET NUMBER')
              call memose(one)
              if( exists('DEFAU') ) ieset = getint('DEFAU',0_ip,'#SET NUMBER')
              if( ieset /= 0 ) then
                 do ielem = 1,nelem
                    leset(ielem) = ieset
                 end do
              end if
              call ecoute('reaset')
              do while( words(1) /='ENDEL')
                 if( words(1) == 'NORMA' ) then
                    setno(1:ndime) = param(1:ndime)
                 else if( words(1) == 'POINT' ) then
                    ipoin = getint('POINT',1_ip,'#SET NUMBER')
                    setpo(1:ndime,ipoin) = param(2:ndime+1)
                 else
                    ielem = int(param(1))
                    leset(ielem) = int(param(2))
                 end if
                 call ecoute('reaset')
              end do
           else
              call ecoute('reaset')
              if( words(1) /='ENDEL' ) then
                 neset = 1
                 call memose(one)
                 do while( words(1) /='ENDEL')
                    if( words(1) =='FROM ' ) then
                       iesta=int(param(1))
                       iesto=int(param(2))
                       ieset=int(param(3))
                       do ielem=iesta,iesto
                          leset(ielem)=ieset
                       end do
                    else
                       ielem=int(param(1))
                       if(ielem<0.or.ielem>nelem) &
                            call runend('REASET: WRONG NUMBER OF ELEMENT '&
                            //adjustl(trim(intost(ielem)))) 
                       leset(ielem)=int(param(2))
                    end if
                    call ecoute('reaset')
                 end do
              end if
           end if

        else if( words(1) =='BOUND' ) then 
           !
           ! ADOC[1]> BOUNDARIES: [ALL= int]
           ! ADOC[1]>   int1 int2                                                                   $ Boundary, set number
           ! ADOC[1]>   ...
           ! ADOC[1]> END_BOUNDARIES
           ! ADOC[d]> BOUNDARIES:
           ! ADOC[d]> This field defines the boundary sets. It contains a list of boundaries (int1) and 
           ! ADOC[d]> the set assigned to it (int2). The option ALL= int assigns automatically the
           ! ADOC[d]> set int to all the boundaries of the mesh.
           ! 
           if( words(2) == 'ALL  ' ) then
              nbset = 1
              ibset = getint('ALL  ',1_ip,'#SET NUMBER')
              call memose(two)
              do iboun = 1,nboun
                 lbset(iboun) = ibset
              end do
              do while( words(1) /='ENDBO')
                 call ecoute('reaset')
              end do
           end if

           if( nbset == 0 ) call ecoute('reaset')
           if( words(1) /='ENDBO' ) then
              if( kfl_autbo == 1 ) then
                 call runend('BOUNDARY SET DEFINITION IMPOSSIBLE '&
                      //'WHEN USING AUTOMATIC BOUNDARY OPTION')
              end if
              nbset = 1
              call memose(two)
              do while( words(1) /='ENDBO')
                 if( words(1) =='FROM ' ) then
                    ibsta = int(param(1))
                    ibsto = int(param(2))
                    ibset = int(param(3))
                    do iboun = ibsta,ibsto
                       lbset(iboun) = ibset
                    end do
                 else
                    iboun = int(param(1))
                    if( iboun < 0 .or. iboun > nboun ) &
                         call runend('REASET: WRONG NUMBER OF BOUNDARY '&
                         //adjustl(trim(intost(iboun)))) 
                    lbset(iboun) = int(param(2))
                 end if
                 call ecoute('reaset')
              end do
           end if

        else if( words(1) =='NODES' ) then 
           !
           ! ADOC[1]> NODES: 
           ! ADOC[1]>   int1                                                                        $ Node
           ! ADOC[1]>   ...
           ! ADOC[1]> END_NODES
           ! ADOC[d]> NODES:
           ! ADOC[d]> This field defines the node sets. It contains a list of nodes (int1).
           ! 

           !-------------------------------------------------------------
           !
           ! LNSET: Nodes
           !
           !-------------------------------------------------------------

           call memgen(1_ip,npoin,0_ip)
           call ecoute('reaset')
           nnset = 1
           do while( words(1) /='ENDNO')
              if( words(1) =='NEARE' ) then
                 !
                 ! Look for nearest node
                 !
                 do idime=1,ndime
                    coori(idime)=param(idime)
                 end do
                 dimin=1.0e9
                 do ipoin=1,npoin
                    dista=0.0_rp
                    do idime=1,ndime
                       dummr=coori(idime)-coord(idime,ipoin)
                       dista=dista+dummr*dummr
                    end do
                    if(dista<dimin) then
                       kpoin=ipoin
                       dimin=dista
                    end if
                 end do
                 gisca(kpoin)=1
              else
                 !
                 ! List of nodes
                 !
                 do ipara = 1,nnpar
                    ipoin = int(param(ipara))
                    if( ipoin > 0 .and. ipoin <= npoin) gisca(ipoin) = 1
                 end do
              end if
              call ecoute('reaset')
           end do

        end if
     end do
     !
     ! ADOC[0]> END_SETS
     !
     ! Count number of element sets and define the element set connectivity LESEC
     ! Local # body of ielem = LESEC(LESET(IELEM))
     !
     if( neset == 1 ) then
        neset = 0
        do ielem = 1,nelem
           if( leset(ielem) > 0 ) then
              neset = neset + 1
              keset = leset(ielem)
              do jelem = ielem,nelem
                 if( leset(jelem) == keset ) leset(jelem) = -keset
              end do
           end if
        end do
        call memose(five)
        ieset = 0
        do ielem = 1,nelem
           if( leset(ielem) < 0 ) then
              ieset = ieset + 1
              keset = leset(ielem)
              do jelem = ielem,nelem
                 if( leset(jelem) == keset ) leset(jelem) = -keset
              end do
              lesec(ieset) = leset(ielem)
           end if
        end do
     end if
     !
     ! Count number of boundary sets and define the boundary set connectivity LBSEC
     ! Local # body of iboun = LBSEC(LBSET(IBOUN))
     !
     if( nbset == 1 ) then
        nbset = 0
        do iboun = 1,nboun
           if( lbset(iboun) > 0 ) then
              nbset = nbset + 1
              kbset = lbset(iboun)
              do jboun = iboun,nboun
                 if( lbset(jboun) == kbset ) lbset(jboun) = -kbset
              end do
           end if
        end do
        call memose(four)
        ibset = 0
        do iboun = 1,nboun
           if( lbset(iboun) < 0 ) then
              ibset = ibset+1
              kbset = lbset(iboun)
              do jboun = iboun,nboun
                 if( lbset(jboun) == kbset ) lbset(jboun) = -kbset
              end do
              lbsec(ibset) = lbset(iboun)
           end if
        end do
     end if
     !
     ! Define the node set connectivity LNSEC
     ! Node number= LBSEC(INSET)
     !
     if( nnset >= 1 ) then
        nnset = 0
        do ipoin = 1,npoin
           if( gisca(ipoin) == 1 ) nnset = nnset + 1
        end do
        call memose(three)
        nnset = 0
        do ipoin = 1,npoin
           if( gisca(ipoin) == 1 ) then
              nnset = nnset + 1
              lnsec(nnset) = ipoin
           end if
        end do
        call memgen(3_ip,npoin,0_ip)
     end if
  end if

end subroutine reaset
