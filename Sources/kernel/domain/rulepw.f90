subroutine rulepw(ndime,ngaus,nrule,posgp,weigp,ierro)
  !-----------------------------------------------------------------------
  !****f* domain/rulepw
  ! NAME
  !    rulepw
  ! DESCRIPTION
  !    This routine sets up the integration constants
  ! OUTPUT
  !    An n-point Gaussian quadrature rule is a quadrature rule constructed 
  !    to yield an exact result for polynomials of degree 2n-1
  !    POSGP(NDIME,NGAUS) ... Local coordinates of Gauss points
  !    WEIGP(NGAUS) ......... Weights of Gauss points
  ! USES
  !    ruqope
  !    ruqclo
  !    rutope
  !    rutclo
  !    rupope
  !    rupclo
  !    ruyope
  !    ruyclo
  ! USED BY
  !    domain
  !***
  !----------------------------------------------------------------------- 
  use      def_kintyp
  implicit none
  integer(ip), intent(in)  ::  ndime,ngaus,nrule,ierro
  real(rp),    intent(out) ::  posgp(max(1_ip,ndime),ngaus), weigp(ngaus)

  posgp = 0.0_rp
  weigp = 0.0_rp

  select case(nrule)

  case(-2) 
     call rupoin(ngaus,weigp,ierro)             ! point
  case(-1) 
     call rubope(ngaus,posgp,weigp,ierro)       ! Line:         open
  case( 0) 
     call rubclo(ngaus,posgp,weigp,ierro)       ! Line:         closed
  case( 1) 
     call ruqope(ndime,ngaus,posgp,weigp,ierro) ! Quad/Hexa:    open
  case( 2) 
     call ruqclo(ndime,ngaus,posgp,weigp,ierro) ! Quad/Hexa:    closed
  case( 3) 
     call rutope(ndime,ngaus,posgp,weigp,ierro) ! Tria/Tetra:   open 
  case( 4) 
     call rutclo(ndime,ngaus,posgp,weigp,ierro) ! Tria/Tetra:   closed 
  case( 5) 
     call rupope(ndime,ngaus,posgp,weigp,ierro) ! Prism(Penta): open 
  case( 6) 
     call rupclo(ndime,ngaus,posgp,weigp,ierro) ! Prism(Penta): closed
  case( 7) 
     call ruyope(ndime,ngaus,posgp,weigp,ierro) ! Pyramid:      open
  case( 8) 
     call ruyclo(ndime,ngaus,posgp,weigp,ierro) ! Pyramid:      closed

  end select

end subroutine rulepw
