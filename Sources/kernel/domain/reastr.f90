!-----------------------------------------------------------------------
!> @addtogroup DomainInput
!> @{
!> @file    reastr.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Read mesh strategy
!> @details Read mesh strategy:       
!!          \verbatim
!!          - LQUAD(NELTY) ... Type of integration rule (1 = closed, 0 = open)
!!          - NGAUS(NELTY) ... Number of domain integration points
!!          - KFL_SAVDA ...... If element data base should be saved
!!          - SCALE(3) ....... Geometrical scale factor
!!          - TRANS(3) ....... Geometrical translation vector
!!          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine reastr()
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use def_elmtyp
  use mod_memchk
  use mod_iofile
  implicit none
  integer(ip) :: ielty,jelty


  if( ISEQUEN .or. ( IMASTER .and. kfl_ptask /= 2 ) ) then

     lquad      = 0                                         ! Open integration rule
     kfl_binar  = 0                                         ! Don't write mesh in binary format
     kfl_immbo  = 0                                         ! Immersed boundary method
     kfl_markm  = 0                                         ! Mark mesh 
     kfl_savda  = 0                                         ! Do not save element data base
     kfl_creco  = 0                                         ! Do not create contacts

     scale(1)   = 1.0_rp                                    ! X scale factor
     scale(2)   = 1.0_rp                                    ! Y scale factor
     scale(3)   = 1.0_rp                                    ! Z scale factor

     trans(1)   = 0.0_rp                                    ! X translation
     trans(2)   = 0.0_rp                                    ! Y translation 
     trans(3)   = 0.0_rp                                    ! Z translation 
     !
     ! Reach the section
     !
     call ecoute('REASTR')
     do while( words(1) /= 'STRAT' )
        call ecoute('REASTR')
     end do
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Strategy for the mesh related operations
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> STRATEGY
     !
     do while(words(1) /= 'ENDST')
        call ecoute('REASTR')

        if( words(1) == 'INTEG' ) then
           !
           ! ADOC[1]> INTEGRATION_RULE = Open/Close, Open/Close...              $ Integration rule (Open by default)
           ! ADOC[d]> INTEGRATION_RULE:
           ! ADOC[d]> List of intgeration rules for each of the element type declared in DIMENSION field.
           ! ADOC[d]> The list should be written in the following ordering:  
           ! ADOC[d]> 1D: BAR02,BAR03,BAR04
           ! ADOC[d]> 2D: TRI03,TRI06,QUA04,QUA08,QUA09,QUA16
           ! ADOC[d]> 3D: TET04,TET10,PYR05,PYR14,PEN06,PEN15,PEN18,HEX08,HEX20,HEX27,HEX64
           ! ADOC[d]> If only one option is given, it is applied to all element types. 
           !
           if( words(3) == '' ) then
              if( words(2) == 'CLOSE' ) then
                 lquad = (/(1,ielty=1,nelty)/)
              else
                 lquad = (/(0,ielty=1,nelty)/)
              end if
           else
              do ielty = 1,nelty
                 if( words(ielty+1) == 'CLOSE' ) then
                    lquad(ielty) = 1
                 else
                    lquad(ielty) = 0
                 end if
              end do
           end if

        else if( words(1) == 'CREAT' ) then 


           ! ADOC[1]> CREATE_CONTACTS
           ! ADOC[d]> Create contacts and leave the run. Write the new dom files.

           kfl_creco= 1

        else if( words(1) == 'SPLIT' ) then


           ! ADOC[1]> SPLIT_MESH
           ! ADOC[d]> Split mesh in zones

           kfl_creco= 2


        else if( words(1) == 'DOMAI' ) then 
           !
           ! ADOC[1]> DOMAIN_INTEGRATION_POINTS = int, int...                   $ Number of Integration points
           ! ADOC[d]> DOMAIN_INTEGRATION_POINTS:
           ! ADOC[d]> Number of integration points (0 for automatic) for each of the element type declared in DIMENSION field.
           ! ADOC[d]> When automatic, the number of integration points COINCIDES with the number of element nodes
           ! ADOC[d]> To explicitly give a value, the list should be written using the following element ordering:
           ! ADOC[d]> 1D: BAR02,BAR03,BAR04
           ! ADOC[d]> 2D: TRI03,TRI06,QUA04,QUA08,QUA09,QUA16
           ! ADOC[d]> 3D: TET04,TET10,PYR05,PYR14,PEN06,PEN15,PEN18,HEX08,HEX20,HEX27,HEX64
           !
           jelty = 0
           do ielty = 1,nelty
              if( lexis(ielty) > 0 ) then
                 jelty = jelty + 1
                 ngaus(ielty) = int(param(jelty))
                 if( ngaus(ielty) == 1 ) lquad(ielty) = 0      ! One gauss point forced to open rule 
              end if
           end do

        else if( words(1)=='SAVEE') then
           !
           ! ADOC[1]> SAVE_ELEMENT_DATA_BASE = Yes/No                           $ Save element data base (shape function derivatives, Hessian, etc.)
           ! ADOC[d]> SAVE_ELEMENT_DATA_BASE:
           ! ADOC[d]> To avoid recomputing, each time they are needed, the shape functions derivatives
           ! ADOC[d]> Hessian, etc., the element data base can be saved. Advantage: the code is faster.
           ! ADOC[d]> Drawback: it requires more memory
           !
           if( words(2) == 'ON   ' .or. words(2) == 'YES  ' ) kfl_savda = 1

        else if( words(1) == 'OUTPU' ) then    
           !                 
           ! Write mesh data: domain, set and plane
           !
           call ecoute('reastr')
           do while( words(1) /= 'ENDOU' )
              !
              ! OUTPUT BINARY
              !
              if(exists('BINAR')) then
                 kfl_binar = 1
              else
                 call runend('REASTR: OUTPUT FORMAT HAS CHANGED!!!')
              end if

              call ecoute('reastr')
           end do

        else if(words(1)=='SCALE') then 
           !
           ! ADOC[1]> SCALE : XSCALE= real, YSCALE= real, ZSCALE= real          $ Scaling factors for the geometry
           ! ADOC[d]> SCALE:
           ! ADOC[d]> Multiply each coordinates by its corresponding scaling factor.
           ! ADOC[d]> In postprocess, the geometry is written using this scaling.
           !
           scale(1) = getrea('XSCAL',1.0_rp,'#x-factor')
           scale(2) = getrea('YSCAL',1.0_rp,'#y-factor')
           scale(3) = getrea('ZSCAL',1.0_rp,'#z-factor')

        else if( words(1) == 'TRANS' ) then 
           !
           ! ADOC[1]> TRANSLATION = XTRANS= real, YTRANS= real, ZTRANS= real    $ Translation of the geometry
           ! ADOC[d]> TRANSLATION:
           ! ADOC[d]> Translate the geometry.
           !
           trans(1) = getrea('XTRAN',0.0_rp,'#x-translation')
           trans(2) = getrea('YTRAN',0.0_rp,'#y-translation')
           trans(3) = getrea('ZTRAN',0.0_rp,'#z-translation') 

        end if
     end do

     !-----------------------------------------------------------------------
     ! ADOC[0]> END_STRATEGY
     !-----------------------------------------------------------------------

  end if


end subroutine reastr
