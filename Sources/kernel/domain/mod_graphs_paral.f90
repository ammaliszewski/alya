!-----------------------------------------------------------------------
!
!> @addtogroup GraphsToolBox
!! @{
!> @name    ToolBox for graphs and renumbering
!! @file    mod_graphs.f90
!> @author  Guillaume Houzeaux
!! @brief   ToolBox for graphs and renumbering.
!! @details ToolBox for graphs and renumbering. Uses METIS_NodeND,
!!          (Node dissection) for renumbering
!! @{
!
!-----------------------------------------------------------------------

module mod_graphs_paral

  use mod_memchk
  use mod_memory
  use def_parame
  use def_kintyp, only : ip,rp,lg
  use def_master, only : kfl_lotme
  use def_domain, only : memor_dom
  use def_domain, only : needg,leedg           ! List of edges
  type TeliTree
     integer(ip)          :: nn                !< Number of nodes 			 
     integer(ip), pointer :: descendiente(:)   !< Descendiente del nodo i 	 
     integer(ip), pointer :: nant(:)           !< N. antecesores del nodo i  
     integer(ip), pointer :: Tnant(:)          !< N total de antecesores de i	 
     type(i1p),   pointer :: antecesores(:)    !< Antecesores del nodo i         
  end type TeliTree

contains

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute a node-node graph
  !> @details Compute a node-node graph
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine graphs_poipoi(&
       npoin,nelem,mnode,lnods,lnnod,ltype,ia,ja,&
       bandw,profi,pelpo,lelpo,mepoi,message)
    implicit none
    integer(ip),          intent(in)            :: npoin                    !< Number of nodes
    integer(ip),          intent(in)            :: nelem                    !< Number of elements
    integer(ip),          intent(in)            :: mnode                    !< Max. number of nodes per element
    integer(ip),          intent(in)            :: lnods(mnode,nelem)       !< Connectivity array
    integer(ip),          intent(in)            :: lnnod(nelem)             !< Array of number of element nodes
    integer(ip),          intent(in)            :: ltype(nelem)             !< Array of element types
    integer(ip), pointer, intent(out)           :: ia(:)                    !< Linked list of (node-node) pointer 
    integer(ip), pointer, intent(out)           :: ja(:)                    !< Linked list of (node-node) elements
    integer(ip),          intent(out), optional :: bandw                    !< Bandwidt
    real(rp),             intent(out), optional :: profi                    !< Profile
    integer(ip), pointer, intent(out), optional :: pelpo(:)                 !< Linked list of (element-node) pointer 
    integer(ip), pointer, intent(out), optional :: lelpo(:)                 !< Linked list of (element-node) elements
    integer(ip),          intent(out), optional :: mepoi                    !< Max number of element per node
    character(*),         intent(in),  optional :: message                  !< Message for options
    integer(ip), pointer                        :: pelp2(:)                 
    integer(ip), pointer                        :: lelp2(:)                 
    integer(ip)                                 :: ipoin,ielem,jelem,icoef
    integer(ip)                                 :: izdom,ncoef,nlelp,mtouc
    integer(ip)                                 :: lsize,mpopo,nz,ioption
    integer(ip)                                 :: which_graph
    integer(ip), pointer                        :: lista(:)
    logical(lg), pointer                        :: touch(:)
    logical(lg)                                 :: only_edges
    !
    ! Initialization
    !
    nullify(lista)
    nullify(touch)
    which_graph = 0                                    ! 0:all, 1:all without diagonal, -1: inferior part, -2: edges
    only_edges  = .false.
    if( present(message) ) then
       if(      message == 'REMOVE DIAGONAL' ) then
          which_graph =  1                             !  1: Remvole diagonal
       else if( message == 'LOWER PART' ) then  
          which_graph = -1                             ! -1: only lower part
       else if( message == 'EDGES' ) then
          which_graph = -1                             ! -1+only_edges: only edges
          only_edges  = .true.
       end if
    end if
    !
    ! Compute node-element connectivity if not already computed: PELP2, LELP2
    !
    if( present(pelpo) .and. present(lelpo) ) then
       if( .not. associated(pelpo) ) then
          call graphs_elepoi(npoin,nelem,mnode,lnods,lnnod,mepoi,pelpo,lelpo)
       end if
       pelp2 => pelpo
       lelp2 => lelpo
    else
       call graphs_elepoi(npoin,nelem,mnode,lnods,lnnod,mepoi,pelp2,lelp2)
    end if
    ! 
    ! Compute node-node graph: JA and IA
    !
    call memory_alloca(memor_dom,'IA','graphs_poipoi',ia,npoin+1)

    if( kfl_lotme == 0 ) then

       !-------------------------------------------------------------------
       !
       ! Strategy 1: slow but does not require lots of memory
       !
       !-------------------------------------------------------------------

       mtouc = 0
       do ipoin = 1,npoin
          mtouc = max(mtouc,pelp2(ipoin+1)-pelp2(ipoin))
       end do
       mtouc = mtouc * mnode
       nz    = 0

       call memory_alloca(memor_dom,'TOUCH','graphs_poipoi',touch,mtouc)

       if( which_graph == 0 ) then
          do ipoin = 1,npoin
             nlelp = pelp2(ipoin+1) - pelp2(ipoin)
             ncoef = nlelp * mnode
             do icoef = 1,ncoef          
                touch(icoef) = .false.
             end do
             call graphs_nzecof(mnode,lnods,lnnod,ltype,nlelp,ncoef,nz,&
                  lelp2(pelp2(ipoin):),touch,0_ip,only_edges)
          end do
          !
          ! Construct the array of indexes
          ! 
          call memory_alloca(memor_dom,'JA','graphs_poipoi',ja,nz)
          izdom = 1
          do ipoin = 1,npoin
             nlelp = pelp2(ipoin+1) - pelp2(ipoin)
             ncoef = nlelp * mnode
             do icoef = 1,ncoef          
                touch(icoef) = .false.
             end do
             call graphs_arrind(mnode,lnods,lnnod,ltype,nlelp,ncoef,&
                  lelp2(pelp2(ipoin):),touch,izdom,ipoin,ia,ja,0_ip,&
                  only_edges)
          end do
          
       else if( which_graph == 1 ) then

          do ipoin = 1,npoin
             nlelp = pelp2(ipoin+1) - pelp2(ipoin)
             ncoef = nlelp * mnode
             do icoef = 1,ncoef          
                touch(icoef) = .false.
             end do
             call graphs_nzecof(mnode,lnods,lnnod,ltype,nlelp,ncoef,nz,&
                  lelp2(pelp2(ipoin):),touch,ipoin,only_edges)
          end do
          !
          ! Construct the array of indexes
          ! 
          call memory_alloca(memor_dom,'JA','graphs_poipoi',ja,nz)
          izdom = 1
          do ipoin = 1,npoin
             nlelp = pelp2(ipoin+1) - pelp2(ipoin)
             ncoef = nlelp * mnode
             do icoef = 1,ncoef          
                touch(icoef) = .false.
             end do
            call graphs_arrind(mnode,lnods,lnnod,ltype,nlelp,ncoef,&
                  lelp2(pelp2(ipoin):),touch,izdom,ipoin,ia,ja,ipoin,&
                  only_edges)
          end do

       else if( which_graph == -1 ) then

          do ipoin = 1,npoin
             nlelp = pelp2(ipoin+1) - pelp2(ipoin)
             ncoef = nlelp * mnode
             do icoef = 1,ncoef          
                touch(icoef) = .false.
             end do
            call graphs_nzecof(mnode,lnods,lnnod,ltype,nlelp,ncoef,nz,&
                  lelp2(pelp2(ipoin):),touch,-ipoin,only_edges)
          end do
          !
          ! Construct the array of indexes
          ! 
          call memory_alloca(memor_dom,'JA','graphs_poipoi',ja,nz)
          izdom = 1
          do ipoin = 1,npoin
             nlelp = pelp2(ipoin+1) - pelp2(ipoin)
             ncoef = nlelp * mnode
             do icoef = 1,ncoef          
                touch(icoef) = .false.
             end do
            call graphs_arrind(mnode,lnods,lnnod,ltype,nlelp,ncoef,&
                  lelp2(pelp2(ipoin):),touch,izdom,ipoin,ia,ja,-ipoin,&
                  only_edges)
          end do

       end if

       ia(npoin+1) = nz + 1
       call memory_deallo(memor_dom,'TOUCH','graphs_poipoi',touch)

    else

       !-------------------------------------------------------------------
       !
       ! Strategy 2: quick but requires lots of memory
       ! 
       !-------------------------------------------------------------------

       call memory_alloca(memor_dom,'LISTA','graphs_poipoi',lista,mpopo)
       !
       ! Construct the array of indexes
       !     
       ia(1) = 1
       if( which_graph == 0 ) then
          do ipoin = 1, npoin
             lsize = 0
             do ielem = pelp2(ipoin), pelp2(ipoin+1)-1
                jelem = lelp2(ielem)
                call graphs_mergli( lista(ia(ipoin):), lsize, lnnod(jelem), &
                     lnods(1,jelem), ltype(jelem), 0_ip , only_edges)
             enddo
             ia(ipoin+1) = ia(ipoin) + lsize
          end do
       else if( which_graph == 1 ) then
          do ipoin = 1, npoin
             lsize = 0
             do ielem = pelp2(ipoin), pelp2(ipoin+1)-1
                jelem = lelp2(ielem)
                call graphs_mergli( lista(ia(ipoin):), lsize, lnnod(jelem), &
                     lnods(1,jelem), ltype(jelem), ipoin , only_edges)
             enddo
             ia(ipoin+1) = ia(ipoin) + lsize
          end do
       else if( which_graph == -1 ) then
          do ipoin = 1, npoin
             lsize = 0
             do ielem = pelp2(ipoin), pelp2(ipoin+1)-1
                jelem = lelp2(ielem)
                call graphs_mergli( lista(ia(ipoin):), lsize, lnnod(jelem), &
                     lnods(1,jelem), ltype(jelem), -ipoin , only_edges)
             enddo
             ia(ipoin+1) = ia(ipoin) + lsize
          end do
       end if
       nz = ia(npoin+1)-1
       call memory_alloca(memor_dom,'JA','graphs_poipoi',ja,nz)  
       do ipoin=1, nz
          ja(ipoin) = lista(ipoin)
       enddo
       call memory_deallo(memor_dom,'LISTA','graphs_poipoi',lista)
    end if
    !
    ! Deallocate memory
    !
    if( present(pelpo) .and. present(lelpo) ) then
       continue
    else
       call graphs_dealep(pelp2,lelp2)
    end if

    !-------------------------------------------------------------------
    !
    ! Order graph: the tst LSIZE > 0 is necessary to treat nodes
    ! without graphs. Example: master nodes comming from a neighboring
    ! subdomain.
    ! 
    !-------------------------------------------------------------------

    do ipoin = 1,npoin
       lsize = ia(ipoin+1) - ia(ipoin)
       if( lsize > 0 ) call heapsorti1(2_ip,lsize,ja(ia(ipoin)))
    end do

    !-------------------------------------------------------------------
    !
    ! Compute profile and bandwidth
    ! 
    !-------------------------------------------------------------------

    call graphs_gtband(npoin,ja,ia,bandw,profi)

  end subroutine graphs_poipoi

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute the node/element connectivity arrays.
  !> @details Compute the node/element connectivity arrays.
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine graphs_elepoi(&
       npoin,nelem,mnode,lnods,lnnod,mepoi,pelpo,lelpo,leper,lninv)

    implicit none
    integer(ip),          intent(in)           :: npoin                    !< Number of nodes
    integer(ip),          intent(in)           :: nelem                    !< Number of elements
    integer(ip),          intent(in)           :: mnode                    !< Max. number of nodes per element
    integer(ip),          intent(in)           :: lnods(mnode,*)           !< Connectivity array
    integer(ip),          intent(in)           :: lnnod(*)                 !< Array of number of element nodes
    integer(ip),          intent(out)          :: mepoi                    !< Max. number of element per node
    integer(ip), pointer, intent(out)          :: pelpo(:)                 !< Linked list of (element-node) pointer
    integer(ip), pointer, intent(out)          :: lelpo(:)                 !< Linked list of (element-node) elements
    integer(ip), pointer, intent(in), optional :: leper(:)                 !< Element permutation array
    integer(ip), pointer, intent(in), optional :: lninv(:)                 !< Nodal permutation array
    integer(ip), pointer                       :: nepoi(:)
    integer(ip)                                :: inode,ipoin,ielem
    integer(ip)                                :: jelem,jpoin,nlelp
    logical(lg)                                :: lperm
    !
    ! Initialization
    !
    nullify(nepoi)
    !
    ! Permutation?
    !
    lperm = present(leper) .and. present(lninv)
    !
    ! Allocate memory for NEPOI and compute it
    !
    call memory_alloca(memor_dom,'NEPOI','elepoi',nepoi,npoin)
    !
    ! NEPOI: Number of element per node
    !
    if( lperm ) then
       do ielem = 1,nelem
          jelem = leper(ielem)
          do inode = 1,lnnod(jelem)
             ipoin = lnods(inode,jelem)
             jpoin = lninv(ipoin)
             nepoi(jpoin) = nepoi(jpoin) + 1
          end do
       end do
    else
       do ielem = 1,nelem
          do inode = 1,lnnod(ielem)
             ipoin = lnods(inode,ielem)
             nepoi(ipoin) = nepoi(ipoin) + 1
          end do
       end do
    end if
    !
    ! Allocate memory for PELPO and compute it
    !
    call memory_alloca(memor_dom,'LELPO','elepoi',pelpo,npoin+1_ip)
    pelpo(1) = 1
    do ipoin = 1,npoin
       pelpo(ipoin+1) = pelpo(ipoin) + nepoi(ipoin)
    end do
    !
    ! Allocate memory for LELPO and construct the list
    !
    nlelp = pelpo(npoin+1)
    call memory_alloca(memor_dom,'LELPO','elepoi',lelpo,nlelp)
    if( lperm ) then
       do ielem = 1,nelem
          jelem = leper(ielem)
          do inode = 1,lnnod(jelem)
             ipoin = lnods(inode,jelem)
             jpoin = lninv(ipoin)
             lelpo(pelpo(jpoin)) = ielem
             pelpo(jpoin) = pelpo(jpoin)+1
          end do
       end do
    else
       do ielem = 1,nelem
          do inode = 1,lnnod(ielem)
             ipoin = lnods(inode,ielem)
             lelpo(pelpo(ipoin)) = ielem
             pelpo(ipoin) = pelpo(ipoin)+1
          end do
       end do
    end if
    !
    ! Recompute PELPO and maximum number of element neighbors MEPOI
    !
    pelpo(1) =  1
    mepoi    = -1
    do ipoin = 1,npoin
       pelpo(ipoin+1) = pelpo(ipoin) + nepoi(ipoin)
       mepoi = max(mepoi,nepoi(ipoin))
    end do
    !
    ! Deallocate memory for temporary node/element connectivity
    !
    call memory_deallo(memor_dom,'NEPOI','elepoi',nepoi)

  end subroutine graphs_elepoi

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute a graph
  !> @details Compute the element-element graph using nodal connectivity
  !>    For example, given the following mesh, element 5 will have the
  !>    following neighbors: \n
  !>    @verbatim
  !>    +---+---+---+
  !>    | 1 | 2 | 3 |  1,2,3,4,6,7,8,9
  !>    +---+---+---+
  !>    | 4 | 5 | 6 |
  !>    +---+---+---+
  !>    | 7 | 8 | 9 |
  !>    +---+---+---+
  !>    @endverbatim
  !>    \n
  !>    Working arrays:\n
  !>    NEPOI:      Number of occurencies of a node in connectivity\n
  !>                Node ipoin belongs to nepoi(ipoin) elements\n
  !>    PELPO:      Pointer to node connectivities arrays lelpo\n
  !>    LELPO:      List of node to element connectivities\n
  !>    MELEL:      Maximum number of neighbors=max(nepoi)*mnode\n
  !>                                                  \n
  !>    Example:                                      \n
  !>    @verbatim
  !>                 1---2---3---4---5  
  !>                 |   |   |   |   | 
  !>                 6---7---8---9--10 
  !>                 |   |   |   |   |
  !>                11--12--13--14--15  
  !>    @endverbatim
  !>                                                  \n
  !>    @verbatim
  !>    nedge:  22                               
  !>    element #:  1  2  3  4  5 ...   
  !>    pelel:  1  3  8  9 13 ...                
  !>                |  |  |                       
  !>                |  |  +--------------+    
  !>                |  |                 |        
  !>                |  +--+              |          
  !>                |     |              |            
  !>    lelel:  2  6  1  3  6  7  8  2  4  7  8  9 ...
  !>    @endverbatim
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine graphs_eleele(&
       nelem,npoin,mnode,mepoi,lnods,lnnod,&
       pelpo,lelpo,nedge,medge,pelel,lelel,&
       leper,lninv)

    use mod_htable
    implicit none  
    integer(ip),          intent(in)              :: nelem                           !< Number of elements 		
    integer(ip),          intent(in)              :: npoin                           !< Number of nodes 		
    integer(ip),          intent(in)              :: mnode                           !< Max. number of nodes per element
    integer(ip),          intent(inout)           :: mepoi                           !< Max. number of element per node
    integer(ip),          intent(in)              :: lnods(mnode,*)                  !< Connectivity array
    integer(ip),          intent(in)              :: lnnod(*)                        !< Array of number of element nodes
    integer(ip), pointer, intent(inout), optional :: pelpo(:)                        !< Linked list of (element-node) pointer
    integer(ip), pointer, intent(inout), optional :: lelpo(:)                        !< Linked list of (element-node) elements
    integer(ip),          intent(out)             :: nedge                           !< Number of edges
    integer(ip),          intent(out)             :: medge                           !< Max. number of edges per element
    integer(ip), pointer, intent(out)             :: pelel(:)                        !< Linked list of (element-element) pointer
    integer(ip), pointer, intent(out)             :: lelel(:)                        !< Linked list of (element-element) elements
    integer(ip), pointer, intent(in),    optional :: leper(:)                        !< Element permutation array
    integer(ip), pointer, intent(in),    optional :: lninv(:)                        !< Nodal permutation array
    integer(ip)                                   :: ielem,kelem,inode,jelem
    integer(ip)                                   :: melel,jnode,kpoin,pelem
    integer(8)                                    :: msize
    type(hash_t)                                  :: ht
    integer(ip), pointer                          :: lista(:)
    integer(ip), pointer                          :: pelp2(:)
    integer(ip), pointer                          :: lelp2(:)
    logical(lg)                                   :: lperm,lelpo_local
    !
    ! Initialization
    !
    nullify(lista)
    !
    ! Pelpo and lelpo available?
    !
    lelpo_local = .false.
    if( present(pelpo) .and. present(lelpo) ) then
       if( .not. associated(pelpo) ) then
          call graphs_elepoi(npoin,nelem,mnode,lnods,lnnod,mepoi,pelpo,lelpo)
       end if
       pelp2 => pelpo
       lelp2 => lelpo
    else
       lelpo_local = .true.
       call graphs_elepoi(npoin,nelem,mnode,lnods,lnnod,mepoi,pelp2,lelp2)
    end if
    !
    ! Permutation?
    !
    lperm = present(leper) .and. present(lninv)
    !
    ! Allocate memory for PELEL
    !
    call memory_alloca(memor_dom,'PELEL','eleele',pelel,nelem+1_ip)
    melel = mepoi*mnode ! Optimize: it is overdimensionalized

    if( kfl_lotme == 1 ) then

       !-----------------------------------------------------------------
       !
       ! First option: we have a lot of memory
       !
       !-----------------------------------------------------------------

       msize = melel*nelem
       !
       ! Compute Hash table (initialize, reset, add and destroy)
       !
       call memory_alloca(memor_dom,'LISTA','eleele',lista,msize)
       call htaini( ht, melel )
       pelel(1) = 1

       if( lperm ) then

          do ielem= 1,nelem
             kelem = leper(ielem)
             call htares( ht, lista(pelel(kelem):) )
             do inode = 1,lnnod(kelem)
                jnode = lnods(inode,kelem)
                kpoin = lninv(jnode)
                do jelem = pelp2(kpoin), pelp2(kpoin+1)-1
                   kelem = lelp2(jelem)
                   if( kelem /= ielem ) then
                      call htaadd( ht, kelem )
                   end if
                end do
             end do
             pelel(ielem+1) = pelel(ielem) + ht % nelem
          end do

       else

          do ielem= 1, nelem
             call htares( ht, lista(pelel(ielem):) )
             do inode = 1, lnnod(ielem)
                jnode = lnods(inode,ielem)
                do jelem = pelp2(jnode), pelp2(jnode+1)-1
                   kelem = lelp2(jelem)
                   if( kelem /= ielem ) then
                      call htaadd( ht, kelem )
                   end if
                end do
             end do
             pelel(ielem+1) = pelel(ielem) + ht % nelem
          end do

       end if

       call htades( ht )
       nedge = pelel(nelem+1)-1
       !
       ! Allocate memory and compute list of adjacancies LELEL
       ! 
       call memory_alloca(memor_dom,'LELEL','eleele',lelel,nedge)
       do ielem = 1, nedge
          lelel(ielem) = lista(ielem)
       end do

    else if( kfl_lotme == 0 ) then

       !-----------------------------------------------------------------
       !
       ! Second option: we DO NOT have a lot of memory
       !
       !-----------------------------------------------------------------

       msize = melel 
       call memory_alloca(memor_dom,'LISTA','eleele',lista,msize)

       call htaini( ht, melel )
       pelel(1) = 1

       if( lperm ) then

          do ielem = 1,nelem
             pelem = leper(ielem)
             call htares( ht, lista )
             do inode = 1,lnnod(pelem)
                jnode = lnods(inode,pelem)
                kpoin = lninv(jnode)
                do jelem = pelp2(kpoin), pelp2(kpoin+1)-1
                   kelem = lelp2(jelem)
                   if( kelem /= ielem ) then
                      call htaadd( ht, kelem )
                   end if
                end do
             end do
             pelel(ielem+1) = pelel(ielem) + ht%nelem
          end do
          nedge = pelel(nelem+1)-1    

       else

          do ielem = 1,nelem
             call htares( ht, lista )
             do inode = 1,lnnod(ielem)
                jnode = lnods(inode,ielem)
                do jelem = pelp2(jnode), pelp2(jnode+1)-1
                   kelem = lelp2(jelem)
                   if (kelem/=ielem) then
                      call htaadd( ht, kelem )
                   end if
                end do
             end do
             pelel(ielem+1) = pelel(ielem) + ht%nelem
          end do
          nedge = pelel(nelem+1)-1    

       end if

       call memory_alloca(memor_dom,'LELEL','eleele',lelel,nedge)

       if( lperm ) then

          do ielem = 1,nelem
             pelem = leper(ielem)
             call htares( ht, lelel(pelel(ielem):) )
             do inode = 1,lnnod(pelem)
                jnode = lnods(inode,pelem)
                kpoin = lninv(jnode)
                do jelem = pelp2(kpoin), pelp2(kpoin+1)-1
                   kelem = lelp2(jelem)
                   if( kelem /= ielem ) then
                      call htaadd( ht, kelem )
                   end if
                end do
             end do
          end do

       else

          do ielem = 1,nelem
             call htares( ht, lelel(pelel(ielem):) )
             do inode= 1, lnnod(ielem)
                jnode = lnods(inode,ielem)
                do jelem = pelp2(jnode), pelp2(jnode+1)-1
                   kelem = lelp2(jelem)
                   if (kelem/=ielem) then
                      call htaadd( ht, kelem )
                   end if
                end do
             end do
          end do

       end if

       call htades( ht )
    end if
    !
    ! Deallocate LISTA
    !
    call memory_deallo(memor_dom,'LISTA','eleele',lista)
    !
    ! Maximum number of edges in the mesh
    !
    medge = 0
    do ielem = 1,nelem
       if( pelel(ielem+1)-pelel(ielem) > medge ) then
          medge = pelel(ielem+1)-pelel(ielem)
       end if
    end do
    !
    ! deallocate memory if necessary
    !
    if( lelpo_local ) then
       call graphs_dealep(pelp2,lelp2)
    end if

  end subroutine graphs_eleele

  !------------------------------------------------------------------------
  !
  !> @brief   Deallocate some variables needed to compute graph
  !> @details Deallocate some variables needed to compute graph
  !> @author  Guillaume Houzeaux
  !
  !------------------------------------------------------------------------

  subroutine graphs_dealep(pelpo,lelpo)

    implicit none  
    integer(ip), pointer, intent(out) :: pelpo(:)
    integer(ip), pointer, intent(out) :: lelpo(:)
    integer(4)                        :: istat

    call memory_deallo(memor_dom,'LELPO','dealep',lelpo)
    call memory_deallo(memor_dom,'PELPO','dealep',pelpo)

  end subroutine graphs_dealep

  !------------------------------------------------------------------------
  !
  !> @brief   Deallocate a graph
  !> @details Deallocate a graph
  !> @author  Guillaume Houzeaux
  !
  !------------------------------------------------------------------------

  subroutine graphs_deagra(ia,ja)

    implicit none  
    integer(ip), pointer, intent(out) :: ia(:)
    integer(ip), pointer, intent(out) :: ja(:)
    integer(4)                        :: istat

    call memchk(two,istat,memor_dom,'IA','deagra',ia)
    deallocate(ia,stat = istat)
    if(istat /= 0 ) call memerr(two,'IA','deagra',0_ip)

    call memchk(two,istat,memor_dom,'JA','deagra',ja)
    deallocate(ja,stat = istat)
    if(istat /= 0 ) call memerr(two,'JA','deagra',0_ip)

  end subroutine graphs_deagra

  !------------------------------------------------------------------------
  !
  !> @brief   Remove diagonal from a graph
  !> @details Remove the diagonal from a graph.
  !!          It can be useful for example to feed METIS which does not
  !!          require the diagonal to renumber or to partition a graph 
  !> @author  Guillaume Houzeaux
  !
  !------------------------------------------------------------------------

  subroutine graphs_offdia(itask,nn,nnz,ia,ja,iaren,jaren)

    implicit none  
    integer(ip),          intent(in)            :: itask
    integer(ip),          intent(in)            :: nn
    integer(ip),          intent(in)            :: nnz
    integer(ip), pointer, intent(inout)         :: ia(:)
    integer(ip), pointer, intent(inout)         :: ja(:)
    integer(ip), pointer, intent(out), optional :: iaren(:)
    integer(ip), pointer, intent(out), optional :: jaren(:)
    integer(ip), pointer                        :: iare2(:)
    integer(ip), pointer                        :: jare2(:)
    integer(ip)                                 :: ii,iiz,jjz

    select case ( itask )

    case (  1_ip ) 
       !
       ! Allocate and take off diagonal
       !
       if( present(iaren) ) then
          call memory_alloca(memor_dom,'IAREN','graphs_offdia',iaren,nn+1)
          call memory_alloca(memor_dom,'JAREN','graphs_offdia',jaren,max(1_ip,nnz-nn))
          iare2 => iaren
          jare2 => jaren
       else
          call memory_alloca(memor_dom,'IAREN','graphs_offdia',iare2,nn+1)
          call memory_alloca(memor_dom,'JAREN','graphs_offdia',jare2,max(1_ip,nnz-nn))          
       end if

       iare2(1) = 1
       do ii = 2,nn+1
          iare2(ii) = ia(ii)-ii+1
       end do

       jjz = 0
       do ii = 1,nn
          do iiz = ia(ii),ia(ii+1)-1
             if( ja(iiz) /= ii ) then
                jjz = jjz + 1
                jare2(jjz) = ja(iiz)
             end if
          end do
       end do

       if( .not. present(iaren) ) then
          call memory_deallo(memor_dom,'IA','graphs_offdia',ia)
          call memory_deallo(memor_dom,'JA','graphs_offdia',ja)  
          call memory_alloca(memor_dom,'IA','graphs_offdia',ia,nn+1)
          call memory_alloca(memor_dom,'JA','graphs_offdia',ja,max(1_ip,nnz-nn))
          do ii = 1,nn+1
             ia(ii) = iare2(ii)
          end do
          do iiz = 1,max(1_ip,nnz-nn)
             ja(iiz) = jare2(iiz)
          end do
          call memory_deallo(memor_dom,'IARE2','graphs_offdia',iare2)
          call memory_deallo(memor_dom,'JARE2','graphs_offdia',jare2)          
       end if

    case ( -1_ip )  
       !
       ! Deallocate graph
       !
       if( present(iaren) ) then
          call memory_deallo(memor_dom,'IAREN','graphs_offdia',iaren)
          call memory_deallo(memor_dom,'JAREN','graphs_offdia',jaren)
       end if

    end select

  end subroutine graphs_offdia

  !------------------------------------------------------------------------
  !
  !> @brief   Renumber a graph
  !> @details This subroutine renumber a graph (IA, JA)  using permutation 
  !!          arrays (PERMR, INVPR) and order the entries of JA
  !> @author  Guillaume Houzeaux
  !
  !------------------------------------------------------------------------

  subroutine graphs_rengra(nn,nnz,permr,invpr,ia,ja)

    implicit none  
    integer(ip),          intent(in)    :: nn
    integer(ip),          intent(in)    :: nnz
    integer(ip), pointer, intent(in)    :: permr(:)
    integer(ip), pointer, intent(in)    :: invpr(:)
    integer(ip), pointer, intent(inout) :: ia(:)
    integer(ip), pointer, intent(inout) :: ja(:)
    integer(ip), pointer                :: iacpy(:)
    integer(ip), pointer                :: jacpy(:)
    integer(ip)                         :: ii,jj,kk,iiz,jjz,ll
    !
    ! Initialization
    !
    nullify(iacpy)
    nullify(jacpy)
    !
    ! Copy graph in temporary graphs IACPY, JACPY
    !
    call memory_alloca(memor_dom,'IACPY','graphs_rendia',iacpy,nn+1_ip)
    call memory_alloca(memor_dom,'JACPY','graphs_rendia',jacpy,nnz)
    !
    ! Copy graph to IACPY, JACPY 
    !
    do ii = 1,nn+1
       iacpy(ii) = ia(ii)
    end do
    do iiz = 1,nnz
       jacpy(iiz) = ja(iiz)
    end do
    !
    ! IA(II) is new number of connections of II
    !
    do ii = 1,nn
       jj     = invpr(ii)
       ia(ii) = iacpy(jj+1) - iacpy(jj)
    end do
    !
    ! Renumber
    !
    kk    = ia(1)
    ia(1) = 1 
    do ii = 2,nn+1
       ll     = ia(ii)
       ia(ii) = ia(ii-1) + kk
       kk     = ll
    end do

    do ii = 1,nn
       jj = invpr(ii)
       kk = ia(ii)
       do jjz = iacpy(jj),iacpy(jj+1)-1
          ja(kk) = permr(jacpy(jjz))
          kk     = kk + 1
       end do
    end do
    !
    ! Order graph
    !
    do ii = 1,nn
       ll = ia(ii+1)-ia(ii)
       if( ll > 0 ) call heapsorti1(2_ip,ll,ja(ia(ii)))
    end do
    !
    ! Deallocate temporary graphs IACPY, JACPY
    !
    call memory_deallo(memor_dom,'JACPY','graphs_rendia',jacpy)
    call memory_deallo(memor_dom,'IACPY','graphs_rendia',iacpy)

  end subroutine graphs_rengra

  !------------------------------------------------------------------------
  !
  !> @brief   Compute a subgraph from a graph
  !> @details This subroutine renumber a graph (IA, JA)  using permutation 
  !>          arrays (PERMR, INVPR) and order the entries of JA
  !> @author  Guillaume Houzeaux
  !
  !------------------------------------------------------------------------

  subroutine graphs_subgra(nn,nnz,permr,invpr,ia,ja)

    implicit none  
    integer(ip),          intent(in)    :: nn
    integer(ip),          intent(inout) :: nnz
    integer(ip), pointer, intent(in)    :: permr(:)
    integer(ip), pointer, intent(in)    :: invpr(:)
    integer(ip), pointer, intent(inout) :: ia(:)
    integer(ip), pointer, intent(inout) :: ja(:)
    integer(ip), pointer                :: iacpy(:) 
    integer(ip), pointer                :: jacpy(:) 
    integer(ip)                         :: ii,jj,kk,iiz,jjz,ll,pp,mm
    !
    ! Initialization
    !
    nullify(iacpy)
    nullify(jacpy)
    !
    ! Copy graph in temporary graphs IACPY, JACPY
    !
    call memory_alloca(memor_dom,'IACPY','graphs_subgra',iacpy,nn+1_ip)
    call memory_alloca(memor_dom,'JACPY','graphs_subgra',jacpy,nnz)
    !
    ! Copy graph to IACPY, JACPY 
    !
    do ii = 1,nn+1
       iacpy(ii) = ia(ii)
       ia(ii)    = 0
    end do
    do iiz = 1,nnz
       jacpy(iiz) = ja(iiz)
    end do
    !
    ! IA(II) is new number of connections of II
    ! II, JJ: OLD
    ! KK, LL: NEW => NEW = INVPR(OLD) 
    !                    = 0 if vertex does not exist in new graph
    !                OLD = PERMR(NEW)
    !
    mm = 0
    do ii = 1,nn
       kk = invpr(ii)
       if( kk /= 0 ) then
          mm = mm + 1
          do jjz = iacpy(ii),iacpy(ii+1)-1
             jj = jacpy(jjz)
             ll = invpr(jj)
             if( ll /= 0 ) ia(kk) = ia(kk) + 1
          end do
       end if
    end do
    !
    ! IA is now total number of edges on new subgraph for each node
    !
    jj    = ia(1)
    ia(1) = 1 
    do ii = 2,mm+1
       ll     = ia(ii)
       ia(ii) = ia(ii-1) + jj
       jj     = ll
    end do
    !
    ! Compute subgraph
    !
    do kk = 1,mm
       ii = permr(kk)
       pp = 0
       do jjz = iacpy(ii),iacpy(ii+1)-1
          jj = jacpy(jjz)
          ll = invpr(jj)
          if( ll /= 0 ) then    
             ja( ia(kk) + pp ) = ll
             pp = pp + 1
          end if
       end do
    end do
    !
    ! Order graph
    !
    do ii = 1,mm
       ll = ia(ii+1)-ia(ii)
       if( ll > 0 ) call heapsorti1(2_ip,ll,ja(ia(ii)))
    end do
    nnz = ja(ia(mm+1)-1)
    !
    ! Deallocate temporary graphs IACPY, JACPY
    !
    call memory_deallo(memor_dom,'JACPY','graphs_subgra',jacpy)
    call memory_deallo(memor_dom,'IACPY','graphs_subgra',iacpy)

  end subroutine graphs_subgra

  !------------------------------------------------------------------------
  !
  !> @brief   Compute a subgraph from a graph using a condition
  !> @details This subroutine create a graph IA2, JA2 
  !> @author  Guillaume Houzeaux
  !
  !------------------------------------------------------------------------

  subroutine graphs_congra(nn,ia,ja,ifnode,ia2,ja2)

    implicit none  
    integer(ip),          intent(in)    :: nn
    integer(ip),          intent(in)    :: ia(*)
    integer(ip),          intent(in)    :: ja(*)
    logical(lg), pointer, intent(in)    :: ifnode(:)
    integer(ip), pointer, intent(out)   :: ia2(:)
    integer(ip), pointer, intent(out)   :: ja2(:)
    integer(ip)                         :: ii,jj,kk,iiz,jjz,ll,pp,mm
    integer(ip)                         :: nnz2
    !
    ! Initialization
    !
    nullify(ia2)
    nullify(ja2)
    !
    ! Copy graph in temporary graphs IACPY, JACPY
    !
    call memory_alloca(memor_dom,'IA2','graphs_congra',ia2,nn+1_ip)
    nnz2   = 0
    ia2(1) = 1
    do ii = 1,nn
       kk = 0
       if( ifnode(ii) ) then
          do jjz = ia(ii),ia(ii+1)-1
             jj = ja(jjz)
             if( ifnode(jj) ) kk = kk + 1
          end do
       end if
       ia2(ii+1) = ia2(ii) + kk 
       nnz2 = nnz2 + kk
    end do
    call memory_alloca(memor_dom,'JA2','graphs_congra',ja2,nnz2)
    nnz2 = 0
    do ii = 1,nn
       if( ifnode(ii) ) then
          do jjz = ia(ii),ia(ii+1)-1
             jj = ja(jjz)
             if( ifnode(jj) ) then
                nnz2 = nnz2 + 1
                ja2(nnz2) = jj
             end if
          end do
       end if
    end do

  end subroutine graphs_congra

  !-----------------------------------------------------------------------
  !
  !> @brief   Permutes a graph
  !> @details Permutes a graph containing its diagonal \n
  !!          1. Take off diag \n
  !!          2. Compute permutation arrays permr and invpr \n
  !!          3. Renumber the graph using nested dissection (METIS) \n
  !!          4. Order the graph \n
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine graphs_permut(nn,nnz,ia,ja,permr,invpr)

    use def_master, only : nnren_par,permr_par,invpr_par
    use def_master, only : iaren_par,jaren_par
    implicit none
    integer(ip),          intent(in)    :: nn         !< Number of rows
    integer(ip),          intent(in)    :: nnz        !< Size of graph
    integer(ip), pointer, intent(out)   :: permr(:)   !< Permutation:  NEW = PERMR(OLD)
    integer(ip), pointer, intent(out)   :: invpr(:)   !< Inverse perm: OLD = INVPR(NEW)
    integer(ip), pointer, intent(inout) :: ia(:)      !< Graph pointer
    integer(ip), pointer, intent(inout) :: ja(:)      !< Graph list
    integer(ip)                         :: ii
    !
    ! Permutation arrays
    !
    call memory_alloca(memor_dom,'PERMR','graphs_permut',permr,nn)   
    call memory_alloca(memor_dom,'INVPR','graphs_permut',invpr,nn)   
    !
    ! Initialize permutation: useful if Parall is off
    !    
    do ii = 1,nn
       permr(ii) = ii
       invpr(ii) = ii
    end do
    !
    ! IAREN, JAREN: Compute graph without diagonal from IA, JA
    !
    call graphs_offdia(1_ip,nn,nnz,ia,ja,iaren_par,jaren_par)
    !
    ! Renumber graph using METIS (if available via Parall service)
    ! using Nested Bisection
    !     
    nnren_par =  nn
    permr_par => permr
    invpr_par => invpr
    call Parall(2000_ip)
    !
    ! Deallocate memory for non-diagonal graph
    !
    call graphs_offdia(-1_ip,nn,nnz,ia,ja,iaren_par,jaren_par)
    !
    ! Renumber and sort graph
    !
    call graphs_rengra(nn,nnz,permr,invpr,ia,ja)

  end subroutine graphs_permut

  subroutine graphs_deaper(permr,invpr)
    !------------------------------------------------------------------------
    !****f* domain/graphs_deaper
    ! NAME
    !    graphs_deaper
    ! DESCRIPTION
    !    Deallocate permutation arrays
    ! USED BY
    !    par_partit
    !***
    !------------------------------------------------------------------------
    implicit none  
    integer(ip), pointer, intent(out) :: permr(:)
    integer(ip), pointer, intent(out) :: invpr(:)

    call memory_deallo(memor_dom,'INVPR','graphs_permut',invpr)   
    call memory_deallo(memor_dom,'PERMR','graphs_permut',permr)  

  end subroutine graphs_deaper

  subroutine graphs_renmat(ndof,nn,invpr,ia,ja,ianew,janew,an)
    !------------------------------------------------------------------------
    !****f* domain/graphs_rrenmat
    ! NAME
    !    graphs_rrenmat
    ! DESCRIPTION
    !    This subroutine permutes a matrix
    ! USED BY
    !***
    !------------------------------------------------------------------------
    implicit none  
    integer(ip),          intent(in)    :: ndof
    integer(ip),          intent(in)    :: nn
    integer(ip), pointer, intent(in)    :: invpr(:)
    integer(ip), pointer, intent(in)    :: ia(:)
    integer(ip), pointer, intent(in)    :: ja(:)
    integer(ip), pointer, intent(in)    :: ianew(:)
    integer(ip), pointer, intent(in)    :: janew(:)
    real(rp),    pointer, intent(inout) :: an(:,:,:)
    real(rp),    pointer                :: ancpy(:,:,:)
    integer(ip)                         :: ii,jj,kk,iiz
    integer(ip)                         :: idof,jdof,nnz
    integer(ip)                         :: iiznew,iinew,jjnew
    integer(4)                          :: istat

    nnz = size(an,3_ip)
    if( nnz /= ia(nn+1) - 1 ) call runend('GRAPHS_RENMAT: WRONG MATRIX DIMENSIONS')

    allocate( ancpy(ndof,ndof,nnz) , stat = istat )
    call memchk(zero,istat,memor_dom,'ANCPY','graphs_renmat',ancpy)       

    do iiz = 1,nnz
       do idof = 1,ndof
          do jdof = 1,ndof
             ancpy(jdof,idof,iiz) = an(jdof,idof,iiz)
          end do
       end do
    end do

    do iinew = 1,nn
       ii = invpr(iinew)
       do iiznew = ianew(iinew),ianew(iinew+1)-1
          jjnew = janew(iiznew)
          jj    = invpr(jjnew)
          iiz   = ia(ii)
          kk    = ja(iiz)
          do while( jj /= kk )
             iiz = iiz + 1
             kk  = ja(iiz)
          end do
          if( iiz > ia(ii+1)-1 ) call runend('OUPS')
          do idof = 1,ndof
             do jdof = 1,ndof
                an(jdof,idof,iiznew) = ancpy(jdof,idof,iiz)
             end do
          end do
       end do
    end do

    call memchk(two,istat,memor_dom,'ANCPY','graphs_renmat',ancpy)
    deallocate(ancpy,stat = istat)
    if(istat /= 0 ) call memerr(two,'ANCPY','graphs_renmat',0_ip)

  end subroutine graphs_renmat

  subroutine graphs_copyij(nn,ia,ja,iacpy,jacpy)
    !------------------------------------------------------------------------
    !****f* domain/graphs_copyij
    ! NAME
    !    graphs_cpoyij
    ! DESCRIPTION
    !    This subroutine copy a graph
    ! INPUT
    !    IA, JA ......... Graph
    ! OUTPUT
    !    IACPY, JACPY ... IA = IACPY, JA = JACPY
    ! USED BY
    !***
    !------------------------------------------------------------------------
    implicit none  
    integer(ip),          intent(in)  :: nn
    integer(ip), pointer, intent(in)  :: ia(:)
    integer(ip), pointer, intent(in)  :: ja(:)
    integer(ip), pointer, intent(out) :: iacpy(:)
    integer(ip), pointer, intent(out) :: jacpy(:)
    integer(ip)                       :: ii,iiz,nnz
    integer(4)                        :: istat
    !
    ! Copy graph in IACPY, JACPY
    !
    nnz = size(ja)
    if( nnz /= ia(nn+1) - 1 ) call runend('GRAPHS_RENMAT: WRONG MATRIX DIMENSIONS')

    allocate( iacpy(nn+1) , stat = istat )
    call memchk(zero,istat,memor_dom,'IACPY','graphs_copyij',iacpy)       
    allocate( jacpy(nnz)  , stat = istat )
    call memchk(zero,istat,memor_dom,'JACPY','graphs_copyij',jacpy)   

    do ii = 1,nn+1
       iacpy(ii) = ia(ii)
    end do
    do iiz = 1,nnz
       jacpy(iiz) = ja(iiz)
    end do

  end subroutine graphs_copyij

  subroutine graphs_iniper(nn,permr,invpr)
    !------------------------------------------------------------------------
    !****f* domain/graphs_iniper
    ! NAME
    !    graphs_iniper
    ! DESCRIPTION
    !    This subroutine allocates and initializes permutation arrays
    ! INPUT
    !    NN ................ Number of rows
    ! OUTPUT
    !    PERMR(NN) ......... Permutation:  Identity
    !    INVPR(NN) ......... Inverse perm: Identity
    ! USED BY
    !***
    !------------------------------------------------------------------------
    implicit none
    integer(ip),          intent(in)    :: nn
    integer(ip), pointer, intent(out)   :: permr(:)
    integer(ip), pointer, intent(out)   :: invpr(:)
    integer(ip)                         :: ii
    integer(4)                          :: istat
    !
    ! Permutation arrays
    !
    allocate( permr(nn) , stat = istat )
    call memchk(zero,istat,memor_dom,'PERMR','graphs_iniper',permr)       
    allocate( invpr(nn) , stat = istat )
    call memchk(zero,istat,memor_dom,'INVPR','graphs_iniper',invpr) 
    !
    ! Initialize permutation: useful if Parall is off
    !    
    do ii = 1,nn
       permr(ii) = ii
       invpr(ii) = ii
    end do

  end subroutine graphs_iniper

  subroutine graphs_postorder(nn,ia,ja,permr,invpr)
    !------------------------------------------------------------------------
    !****f* domain/graphs_iniper
    ! NAME
    !    graphs_iniper
    ! DESCRIPTION
    !    This subroutine allocates and initializes permutation arrays
    ! INPUT
    !    NN ................ Number of rows
    ! OUTPUT
    !    PERMR(NN) ......... Permutation:  Identity
    !    INVPR(NN) ......... Inverse perm: Identity
    ! USED BY
    !***
    !------------------------------------------------------------------------
    use mod_csrdir
    implicit none
    integer(ip),          intent(in)  :: nn
    integer(ip), pointer, intent(in)  :: ia(:)
    integer(ip), pointer, intent(in)  :: ja(:)
    integer(ip), pointer, intent(out) :: permr(:)
    integer(ip), pointer, intent(out) :: invpr(:)
    type(TeliTree)                    :: tree
    integer(ip)                       :: node,last
    integer(ip), pointer              :: iL(:),jL(:)
    integer(ip), pointer              :: iU(:),jU(:)
    !
    ! Symbolic factorization
    !
    nullify(iL,jL,iU,jU)
    call Symbolical_CSR_LU(nn,iA,jA,iL,jL,iU,jU)
    !
    ! Build Elimination tree
    !
    call graphs_BuildEliminationTree(nn,iL,jL,tree)

    node = nn    ! Nodo en la cima del arbol
    last = 1     ! Inicio de la nueva numeracion
    !
    ! Mientras queden sub-arboles independientes 
    !
    do while( node >= 1 )

       call graphs_generatePostOrder1(tree,node,invpr,last)
       !
       ! Look for top of next sub-tree 
       !
       node = node - 1

       loop_node: do while ( node >= 1 )
          if( tree % descendiente(node) >= 1 ) then
             node = node - 1
          else
             exit loop_node
          end if
       end do loop_node

    end do

    if ( last /= nn + 1 ) call runend('WRONG TREE')

    do node = 1,nn
       permr(invpr(node)) = node
    end do
    !
    ! Deallocate memory
    !
    call Symbolical_CSR_LU_Deallocate(iL,jL,iU,jU)
    call graphs_deallocatetree(tree)

  end subroutine graphs_postorder

  subroutine graphs_BuildEliminationTree(nn,iL,jL,tree)
    !------------------------------------------------------------------------
    !****f* domain/graphs_iniper
    ! NAME
    !    graphs_iniper
    ! DESCRIPTION
    !    Computes the elimination tree of the factorisation.
    ! INPUT
    !   NN ...... Number of rows
    !   IL,JL ... L-matrix graph
    ! OUTPUT
    !   TREE .... Elimination tree
    ! USED BY
    !***
    !------------------------------------------------------------------------
    implicit none
    integer(ip),          intent(in)  :: nn
    integer(ip), pointer, intent(in)  :: iL(:)
    integer(ip), pointer, intent(in)  :: jL(:)
    type(TeliTree),       intent(out) :: tree
    integer(ip)                       :: ii,jj,kk
    integer(ip), pointer              :: descendiente(:)
    integer(ip), pointer              :: nant(:)
    integer(ip), pointer              :: Tnant(:)
    type(i1p),   pointer              :: antecesores(:)
    integer(4)                        :: istat

    tree % nn = nn
    allocate( tree % descendiente(nn) , stat = istat )
    call memchk(zero,istat,memor_dom,'tree % descendiente','graphs_BuildEliminationTree',tree % descendiente)       

    allocate( tree % nant(nn) , stat = istat )
    call memchk(zero,istat,memor_dom,'tree % nant','graphs_BuildEliminationTree',tree % nant)       

    allocate( tree % Tnant(nn) , stat = istat )
    call memchk(zero,istat,memor_dom,'tree % Tnant','graphs_BuildEliminationTree',tree % Tnant)       

    allocate( tree % antecesores(nn) , stat = istat )
    call memchk(zero,istat,memor_dom,'tree % antecesores','graphs_BuildEliminationTree',tree % antecesores)       

    descendiente => tree % descendiente
    nant         => tree % nant
    Tnant        => tree % Tnant
    antecesores  => tree % antecesores

    do ii = 1,nn
       do kk = iL(ii),iL(ii+1)-1
          jj = jL(kk)
          if( descendiente(jj) <= 0 ) descendiente(jj) = ii
       end do
    end do
    !
    ! Count the amount of predecessors
    ! and the accumulated load for each node
    !
    do ii = 1,nn
       nant(ii)  = 0
       Tnant(ii) = 1
    end do

    do ii = 1,nn
       kk = descendiente(ii)
       if( kk > 0 ) then
          nant(kk)  = nant(kk)  + 1
          Tnant(kk) = Tnant(kk) + Tnant(ii)
       end if
    end do
    !
    ! Allocate space for the predecessor list
    !
    do ii = 1,nn
       kk = nant(ii)
       if( kk > 0 ) then
          allocate( antecesores(ii) % l(kk) )
       else
          nullify( antecesores(ii) % l )
       end if
    end do
    !
    ! Fill the predecessor list for each node
    !
    do ii = 1,nn
       nant(ii) = 0
    end do

    do ii = 1,nn
       kk = descendiente(ii)
       if( kk > 0 ) then
          nant(kk) = nant(kk) + 1
          antecesores(kk) % l(nant(kk)) = ii
       end if
    end do

  end subroutine graphs_BuildEliminationTree

  subroutine graphs_generatePostOrder1(tree,node,invpr,last)
    !------------------------------------------------------------------------
    !****f* domain/graphs_generatePostOrder1
    ! NAME
    !    graphs_generatePostOrder1
    ! DESCRIPTION
    !  Generates a postorder with maximum locality. It returns the number
    !   of ordered nodes starting form "node".
    ! INPUT
    !   tree        Elimination tree
    !   node        Starting node
    !   invp        Inverse permutation array
    !   last        Number of ordered nodes
    ! OUTPUT
    ! USED BY
    !***
    !------------------------------------------------------------------------
    implicit none
    type(TeliTree),       intent(in)  :: tree
    integer(ip),          intent(in)  :: node
    integer(ip), pointer, intent(out) :: invpr(:)
    integer(ip),          intent(out) :: last
    integer(ip)                       :: ii,kk,active_node,n_act
    integer(ip)                       :: nn
    integer(ip), pointer              :: nant(:)
    type(i1p),   pointer              :: antecesores(:)
    integer(ip), pointer              :: iwa1(:)
    logical(lg), pointer              :: mask(:)
    integer(4)                        :: istat

    nant         => tree % nant
    antecesores  => tree % antecesores
    nn           =  tree % nn

    allocate( iwa1(nn) , stat = istat )
    call memchk(zero,istat,memor_dom,'iwa1','graphs_generatePostOrder1',iwa1)       

    allocate( mask(nn) , stat = istat )
    call memchk(zero,istat,memor_dom,'mask','graphs_generatePostOrder1',mask)       

    do ii = 1,nn
       mask(ii) = .false.
    end do

    active_node = 1
    iwa1(active_node) = node

    do while( active_node > 0 )

       n_act = iwa1(active_node)

       if ( .not. mask(n_act) ) then

          mask(n_act) = .true.
          do ii = nant(n_act),1,-1
             active_node = active_node + 1
             kk = antecesores(n_act) % l(ii)
             iwa1(active_node) = kk
          end do

       else

          active_node = active_node - 1
          invpr(last) = n_act
          last = last + 1

       end if

    end do

    call memchk(two,istat,memor_dom,'MASK','graphs_generatePostOrder1',mask)
    deallocate(mask,stat = istat)
    if(istat /= 0 ) call memerr(two,'MASK','graphs_generatePostOrder1',0_ip)

    call memchk(two,istat,memor_dom,'IWA1','graphs_generatePostOrder1',iwa1)
    deallocate(iwa1,stat = istat)
    if(istat /= 0 ) call memerr(two,'IWA1','graphs_generatePostOrder1',0_ip)

  end subroutine graphs_generatePostOrder1

  subroutine graphs_deallocatetree(tree)
    !------------------------------------------------------------------------
    !****f* domain/graphs_deallocatetree
    ! NAME
    !    graphs_deallocatetree
    ! DESCRIPTION
    !    Deallocates the tree
    ! INPUT
    !    TREE ................ tree
    ! OUTPUT
    ! USED BY
    !***
    !------------------------------------------------------------------------
    use mod_csrdir
    implicit none
    type(TeliTree)  :: tree
    integer(ip)     :: ii,nn
    integer(4)      :: istat

    nn = size( tree % antecesores )
    do ii = 1,nn
       deallocate( tree % antecesores(ii) % l , stat = istat )
    end do
    call memchk(two,istat,memor_dom,'tree % descendientes','graphs_deallocatetree',tree % descendiente)
    deallocate( tree % descendiente , stat = istat )
    if(istat /= 0 ) call memerr(two,'tree % descendientes','graphs_deallocatetree',0_ip)

    deallocate( tree % nant ,         stat = istat )
    deallocate( tree % Tnant ,        stat = istat )
    deallocate( tree % antecesores ,  stat = istat )

  end subroutine graphs_deallocatetree

  subroutine graphs_comper(nn,permr_1,invpr_1,permr_2,invpr_2)
    !------------------------------------------------------------------------
    !****f* domain/graphs_comper
    ! NAME
    !    graphs_comper
    ! DESCRIPTION
    !    This subroutine composes permutations
    !                       II
    !                       ||
    !                       \/
    !    II=INVPR_1(KK)     KK     KK=PERMR_1(II)
    !                       ||
    !                       \/
    !    KK=INVPR_2(JJ)     JJ     JJ=PERMR_2(KK)
    !
    !    JJ = PERMR_2(PERMR_1(II))
    !    II = INVPR_1(INVPR_2(jj))
    !
    ! INPUT
    !    PERMR_1, INVPR_1 ............ First permutation
    !    PERMR_2, INVPR_2 ............ Second permutation
    ! OUTPUT
    !    PERMR_1, INVPR_1 ............ Final permutation
    ! USED BY
    !***
    !------------------------------------------------------------------------
    use mod_csrdir
    implicit none
    integer(ip),          intent(in)    :: nn
    integer(ip), pointer, intent(inout) :: permr_1(:)
    integer(ip), pointer, intent(inout) :: invpr_1(:)
    integer(ip), pointer, intent(in)    :: permr_2(:)
    integer(ip), pointer, intent(in)    :: invpr_2(:)
    integer(ip), pointer                :: permr_cpy(:)
    integer(ip), pointer                :: invpr_cpy(:)
    integer(ip)                         :: ii,jj,kk
    !
    ! Copy permutation
    !
    call graphs_iniper(nn,permr_cpy,invpr_cpy)
    do ii = 1,nn
       permr_cpy(ii) = permr_1(ii)
       invpr_cpy(ii) = invpr_1(ii)
    end do
    !
    ! II = OLD, JJ = NEW, KK = INTERMEDIATE
    !
    do ii = 1,nn
       kk = permr_cpy(ii)
       jj = permr_2(kk)
       permr_1(ii) = jj 
    end do
    do jj = 1,nn
       kk = invpr_2(jj)
       ii = invpr_cpy(kk)
       invpr_1(jj) = ii 
    end do
    !
    ! Deallocate
    !
    call graphs_deaper(permr_cpy,invpr_cpy)

  end subroutine graphs_comper

  !-----------------------------------------------------------------------
  !> @file    graphs_groups.f90
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Coarsen a mesh
  !> @details Create a list of groups
  !-----------------------------------------------------------------------

  subroutine graphs_groups(npoi1,ngrou,npoin,ia,ja,lgrou,kfl_fixno) 

    use def_kintyp, only : ip,rp
    implicit none
    integer(ip), intent(in)                     :: npoi1          !< Start prescribed nodes at ipoi1
    integer(ip), intent(inout)                  :: ngrou          !< Number of groups (can be internally changed)
    integer(ip), intent(in)                     :: npoin          !< Number of nodes
    integer(ip), intent(in)                     :: ia(:)          !< Graph linked list
    integer(ip), intent(in)                     :: ja(:)          !< Graph linked list
    integer(ip), intent(out), pointer           :: lgrou(:)       !< List of groups
    integer(ip), intent(out), pointer, optional :: kfl_fixno(:,:) !< Prescribed nodes
    integer(ip)                                 :: ipoin,nqueu,npogr,igrou,jgrou
    integer(ip)                                 :: kgrou,izdom,npomi,kpoin,jpoin
    integer(ip)                                 :: nfron,nfold,jqueu,ifront,nfnew
    integer(ip)                                 :: nmarkt,nmark,iqueu,icheck
    integer(4)                                  :: istat
    integer(ip), pointer                        :: lqueu(:)
    integer(ip), pointer                        :: lfron(:)
    logical(lg), pointer                        :: lmark(:)
    !
    ! LGROU: allocate memory 
    !     
    allocate(lgrou(npoin),stat=istat)
    call memchk(zero,istat,memor_dom,'LGROU','graphs_groups',lgrou)
    !
    ! If dof are prescribed
    !
    nmarkt = npoin
    if(present(kfl_fixno)) then
       do ipoin = 1,npoin
          if( maxval(kfl_fixno(:,ipoin)) > 0 ) then
             lgrou(ipoin) = -1
          end if
       end do
    end if
    !
    ! If dof should be prescribed on subromains boundaries
    !
    do ipoin = npoi1+1,npoin
       lgrou(ipoin) = -1 
    end do
    !
    ! NMARKT: Recompute marked nodes
    !
    do ipoin = 1,npoin
       if( lgrou(ipoin) == -1 ) nmarkt = nmarkt - 1
    end do
    !
    ! Fill in LQUEU, LMARK, LFRON
    !
    allocate(lqueu(npoin),stat=istat)
    call memchk(zero,istat,memor_dom,'LQUEU','grodom',lqueu)
    allocate(lmark(npoin),stat=istat)
    call memchk(zero,istat,memor_dom,'LMARK','grodom',lmark)
    allocate(lfron(npoin),stat=istat)
    call memchk(zero,istat,memor_dom,'LFRON','grodom',lfron)
    do ipoin=1,npoin
       lmark(ipoin)=.false.
    end do
    !
    ! Limit number of groups to number of nodes
    !
    if( ngrou >= npoin ) then
       do ipoin = 1,npoin
          lgrou(ipoin) = ipoin
       end do
       ngrou = npoin
       goto 999
    end if
    !
    ! Compute points per group and minimal threshold
    !
    npogr = max(npoin/ngrou,1_ip)
    npomi = npogr/3
    !
    ! Find first non marked point
    !
    kpoin = 1

    !----------------------------------------------------------------------
    !
    ! Construct groups
    !
    !----------------------------------------------------------------------
    !
    !  Initialize main loop
    !
    nmark        = 0
    igrou        = 1
    lqueu(1)     = kpoin
    lgrou(kpoin) = igrou
    nmark        = nmark + 1
    nqueu        = 1
    iqueu        = 1
    nfron        = 0

    open_loop: do

       !
       ! Loop on queue
       ! 

       neigh: do 
          ipoin = lqueu(iqueu)
          !
          ! Loop on neighbors
          ! 
          do izdom = ia(ipoin),ia(ipoin+1)-1
             jpoin = ja(izdom)
             !
             ! Has the group of this point been assigned before ?
             !
             if( lgrou(jpoin) == 0 ) then
                !
                ! Did we reach the maximum number of points per group
                !
                if( nqueu == npogr ) exit neigh
                lgrou(jpoin) = igrou
                nmark        = nmark + 1    
                nqueu        = nqueu + 1
                lqueu(nqueu) = jpoin
                !
                ! Does this point belong to the current front
                !
                if( .not. lmark(jpoin) ) lmark(jpoin) = .true.
             endif
          end do
          !
          ! Did we exhaust the queue?
          !        
          if( iqueu == nqueu ) exit neigh
          iqueu = iqueu + 1

       end do neigh
       !
       ! Add nodes to the front
       !
       nfold = nfron + 1

       do jqueu = iqueu,nqueu
          ipoin = lqueu(jqueu)
          do izdom = ia(ipoin),ia(ipoin+1)-1
             jpoin = ja(izdom)
             if( lgrou(jpoin) == 0 ) then
                if( .not. lmark(jpoin) ) then
                   nfron = nfron + 1
                   if( nfron > npoin ) nfron = npoin
                   lfron(nfron) = jpoin
                   lmark(jpoin) = .true.
                end if
             end if
          end do
       end do
       !
       ! Clean up the last points
       !
       do ifront = nfold,nfron
          ipoin = lfron(ifront)
          lmark(ipoin) = .false.
       end do
       !
       ! Compress the front
       !
       nfnew = 0
       do ifront = 1,nfron
          ipoin = lfron(ifront)
          if( .not. lmark(ipoin) ) then
             nfnew = nfnew + 1
             lfron(nfnew) = lfron(ifront)
          end if
       end do
       nfron = nfnew
       !
       ! Special case: Do we have enough nodes   
       !
       if( nqueu < npomi ) then
          !
          ! Find a neighboring group
          !
          glue: do iqueu = 1,nqueu
             ipoin = lqueu(iqueu)
             do izdom = ia(ipoin),ia(ipoin+1)-1
                jpoin = ja(izdom)
                jgrou = lgrou(jpoin)
                if( jgrou /= igrou .and. jgrou /= -1 ) then
                   exit glue
                endif
             enddo
          end do glue

          do ipoin = 1,npoin
             kgrou = lgrou(ipoin)
             if( kgrou == igrou )then
                lgrou(ipoin) = jgrou 
             end if
          end do

          nqueu = 0
          igrou = igrou-1

       endif
       !
       ! Is the front empty --> go home
       !
       if( nfron == 0 ) then
          !
          ! Did we mark all the points
          !
          if( nmark == nmarkt ) then
             !
             ! Is the front empty --> go home
             !
             exit open_loop

          else
             !
             ! Find a non marked point
             !
             icheck = 0_ip
             do ipoin = 1,npoin
                if( lgrou(ipoin) == 0 )then
                   lfron(1) = ipoin
                   icheck = 1_ip
                   exit
                end if
             end do
             if( icheck == 0 ) then
                call runend('GRODOM: nmark/=nmarkt and point not found')
             end if
          end if

       end if
       !
       ! Find new seed
       !
       jpoin = lfron(1)
       !
       ! Initialize new round
       !
       igrou        = igrou+1
       lqueu(1)     = jpoin
       lgrou(jpoin) = igrou
       nmark        = nmark + 1    
       nqueu        = 1
       iqueu        = 1 
       lmark(jpoin) = .true.

    end do open_loop
    !
    ! Redefine ngrou
    !
    ngrou = igrou
    !
    ! LQUEU, LMARK, LFRON: Deallocate memory
    !
999 continue
    call memchk(2_ip,istat,memor_dom,'LQUEU','grodom',lqueu)
    deallocate(lqueu,stat=istat)
    if(istat/=0) call memerr(2_ip,'LQUEU','grodom',0_ip)

    call memchk(2_ip,istat,memor_dom,'LMARK','grodom',lmark)
    deallocate(lmark,stat=istat)
    if(istat/=0) call memerr(2_ip,'LMARK','grodom',0_ip)

    call memchk(2_ip,istat,memor_dom,'LFRON','grodom',lfron)
    deallocate(lfron,stat=istat)
    if(istat/=0) call memerr(2_ip,'LFRON','grodom',0_ip)

  end subroutine graphs_groups

  !-----------------------------------------------------------------------
  !> @file    graphs_coagra.f90
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Construct a caorse graph from a group list
  !> @details Create a list of groups
  !-----------------------------------------------------------------------

  subroutine graphs_gragro(npoin,ngrou,lgrou,ia,ja,nzgroup,iagroup,jagroup)
    use def_kintyp, only : ip,rp
    implicit none
    integer(ip), intent(in)             :: npoin
    integer(ip), intent(in)             :: ngrou
    integer(ip), intent(inout), pointer :: lgrou(:)
    integer(ip), intent(in),    pointer :: ia(:)
    integer(ip), intent(in),    pointer :: ja(:)
    integer(ip), intent(out)            :: nzgroup
    integer(ip), intent(out),   pointer :: iagroup(:)
    integer(ip), intent(out),   pointer :: jagroup(:)
    integer(ip)                         :: ipoin,igrou,jgrou,izdom
    integer(ip)                         :: izgro,nlelp,mpopo
    integer(4)                          :: istat
    integer(ip),                pointer :: ngpoi(:)
    integer(ip),                pointer :: pgrpo(:)
    integer(ip),                pointer :: lgrpo(:)
    logical(lg),                pointer :: touch(:)
    integer(ip),                pointer :: permr(:)
    integer(ip),                pointer :: invpr(:)  
    !
    ! NGPI: number of nodes per group
    !
    allocate(ngpoi(ngrou),stat = istat)
    call memchk(zero,istat,memor_dom,'NGPOI','graphs_gragro',ngpoi)
    do ipoin = 1,npoin
       igrou = lgrou(ipoin)
       if( igrou > 0 ) ngpoi(igrou) = ngpoi(igrou) + 1
    end do
    ! 
    ! Allocate memory for PGRPO and compute it
    !
    allocate(pgrpo(ngrou+1),stat = istat)
    call memchk(zero,istat,memor_dom,'PGRPO','graphs_gragro',pgrpo)
    pgrpo(1) = 1
    do igrou = 1,ngrou
       pgrpo(igrou+1) = pgrpo(igrou) + ngpoi(igrou)
    end do
    !
    ! Allocate memory for LGRPO and construct the list
    !
    nlelp = pgrpo(ngrou+1)
    allocate(lgrpo(nlelp),stat = istat)
    call memchk(zero,istat,memor_dom,'LGRPO','graphs_gragro',lgrpo)
    mpopo = 0        
    do ipoin = 1,npoin
       igrou = lgrou(ipoin)
       if( igrou > 0 ) then
          lgrpo(pgrpo(igrou)) = ipoin
          pgrpo(igrou) = pgrpo(igrou) + 1
       end if
    end do
    !
    ! Recompute PGRPO 
    !
    pgrpo(1) = 1
    do igrou = 1,ngrou
       pgrpo(igrou+1) = pgrpo(igrou) + ngpoi(igrou)
    end do
    !
    ! Compute graph size NZGRO
    !
    nzgroup = 0
    allocate(touch(ngrou),stat=istat) 
    call memchk(zero,istat,memor_dom,'TOUCH','graphs_gragro',touch)
    do igrou = 1,ngrou
       do jgrou = 1,ngrou       
          touch(jgrou) = .false.
       end do
       do izgro = pgrpo(igrou),pgrpo(igrou+1)-1
          ipoin = lgrpo(izgro)
          do izdom = ia(ipoin),ia(ipoin+1)-1
             jgrou = lgrou(ja(izdom))
             if( jgrou > 0 ) then
                if( .not. touch(jgrou) ) then
                   touch(jgrou) = .true.
                   nzgroup = nzgroup + 1
                end if
             end if
          end do
       end do
    end do
    !
    ! Construct the array of indexes
    !
    allocate( iagroup(ngrou+1) , stat = istat ) 
    call memchk(two,istat,memor_dom,'IAGRPOUP','graphs_gragro',iagroup)
    allocate( jagroup(nzgroup) , stat = istat ) 
    call memchk(two,istat,memor_dom,'JAGRPOUP','graphs_gragro',jagroup)

    nzgroup = 1
    do igrou = 1,ngrou
       do jgrou = 1,ngrou       
          touch(jgrou) = .false.
       end do
       iagroup(igrou) = nzgroup
       do izgro = pgrpo(igrou),pgrpo(igrou+1)-1
          ipoin = lgrpo(izgro)
          do izdom = ia(ipoin),ia(ipoin+1)-1
             jgrou = lgrou(ja(izdom))
             if( jgrou > 0 ) then
                if( .not. touch(jgrou) ) then
                   touch(jgrou) = .true.
                   jagroup(nzgroup) = jgrou
                   nzgroup = nzgroup + 1
                end if
             end if
          end do
       end do
    end do

    nzgroup = nzgroup - 1
    iagroup(ngrou+1) = nzgroup + 1

    call memchk(two,istat,memor_dom,'TOUCH','graphs_gragro',touch)
    deallocate(touch,stat=istat)
    if(istat/=0) call memerr(two,'TOUCH','graphs_gragro',0_ip) 

    call memchk(two,istat,memor_dom,'PGRPO','graphs_gragro',pgrpo)
    deallocate(pgrpo,stat=istat)
    if(istat/=0) call memerr(two,'PGRPO','graphs_gragro',0_ip) 

    call memchk(two,istat,memor_dom,'LGRPO','graphs_gragro',lgrpo)
    deallocate(lgrpo,stat=istat)
    if(istat/=0) call memerr(two,'LGRPO','graphs_gragro',0_ip) 
    !
    ! Renumber graph and groups
    !    
    call graphs_permut(ngrou,nzgroup,iagroup,jagroup,permr,invpr)
    do ipoin = 1,npoin
       jgrou = lgrou(ipoin)
       if( jgrou > 0 ) then
          igrou = permr(jgrou)
          lgrou(ipoin) = igrou
       end if
    end do
    call graphs_deaper(permr,invpr)

  end subroutine graphs_gragro

  !-----------------------------------------------------------------------
  !> @file    graphs_gtband.f90
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Compute the bandwth and profile of the mesh
  !> @details Compute the bandwth and profile of the mesh
  !-----------------------------------------------------------------------

  subroutine graphs_gtband(npoin,ja,ia,bandw,profi)
    implicit none
    integer(ip),          intent(in)  :: npoin
    integer(ip), pointer, intent(in)  :: ia(:)
    integer(ip), pointer, intent(in)  :: ja(:)
    integer(ip),          intent(out) :: bandw
    real(rp),             intent(out) :: profi
    integer(ip)                       :: ipoin,izdomin,band,izdom,bandloc,ipmax,jpmax  

    bandw =  0_ip
    profi =  0.0_rp
    !naved =  0      ! Average number of edges
    !nmied =  1e6    ! Min number of edges
    !nmaed = -1e6    ! Max number of edges

    do ipoin = 1,npoin
       !
       ! Initialize local bandwth
       ! 
       bandloc = 0_ip
       !
       ! Loop on neighbors
       !
       do izdom = ia(ipoin),ia(ipoin+1)-1
          izdomin = ja(izdom)
          if( ipoin /= izdomin ) then
             band = abs(izdomin-ipoin)
             !
             ! Test bandwth
             !
             if( band > bandw ) then
                bandw = band
                ipmax = ipoin
                jpmax = izdomin
             endif
             !
             ! Test profile
             !
             if( izdomin < ipoin ) then
                if( band > bandloc ) bandloc = band
             end if
          end if
       end do
       !
       ! Accumulate profile
       !
       profi = profi + real(bandloc,rp)

    end do

  end subroutine graphs_gtband

  !-----------------------------------------------------------------------
  !> @file    graphs_nzecof.f90
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   computes the number of non-zero coefficients graph
  !> @details computes the number of non-zero coefficients of a
  !>           mesh graph stored in compressed sparse row (CSR) format 
  !-----------------------------------------------------------------------

  subroutine graphs_nzecof(&
       mnode,lnods,lnnod,ltype,nlist,ncoef,nz,liste,&
       touch,me,only_edges)
    implicit none
    integer(ip), intent(in)             :: mnode
    integer(ip), intent(in)             :: lnods(mnode,*)
    integer(ip), intent(in)             :: lnnod(*)
    integer(ip), intent(in)             :: ltype(*)
    integer(ip), intent(in)             :: nlist
    integer(ip), intent(in)             :: ncoef
    integer(ip), intent(in)             :: liste(nlist)
    integer(ip), intent(inout)          :: nz
    logical(lg), intent(inout)          :: touch(ncoef)
    integer(ip), intent(in)             :: me
    logical(lg), intent(in)             :: only_edges
    integer(ip)                         :: jelem,jnode,jpoin,nnodj,jposi,jlist
    integer(ip)                         :: kelem,knode,kpoin,nnodk,kposi,klist
    integer(ip)                         :: jedge,jtype,ipoin,jnod1,jnod2

    if( only_edges ) then
       !
       ! Use only edges lower part to construct graph
       ! Consider only edges by imposing TOUCH = .FALSE.
       !
       ipoin = -me
       do jlist = 1,nlist                              
          jelem = liste(jlist)
          nnodj = lnnod(jelem)
          do jnode = 1,nnodj
             jposi        = (jlist-1)*mnode+jnode
             touch(jposi) = .true.
          end do
       end do
       do jlist = 1,nlist   
          jelem = liste(jlist)
          jtype = abs(ltype(jelem))
          do jedge = 1,needg(jtype)
             jnod1 = leedg(1,jedge,jtype)
             jnod2 = leedg(2,jedge,jtype)
             if( lnods(jnod1,jelem) == ipoin ) then
                jposi        = (jlist-1)*mnode+jnod2
                touch(jposi) = .false.                
             else if( lnods(jnod2,jelem) == ipoin ) then
                jposi        = (jlist-1)*mnode+jnod1
                touch(jposi) = .false.
             end if
          end do
       end do
    end if

    if( me == 0 ) then
       !
       ! All graph
       !
       do jlist = 1,nlist                                      ! Loop over those elements 
          jelem = liste(jlist)                                 ! where the point is
          nnodj = lnnod(jelem)
          do jnode = 1,nnodj
             jpoin = lnods(jnode,jelem)
             jposi = (jlist-1)*mnode+jnode
             if( .not. touch(jposi) ) then                     ! Position not touched           
                do klist = 1,nlist                             ! Search other elements 
                   kelem = liste(klist)                        ! where JPOIN is and 
                   nnodk = lnnod(kelem)
                   do knode = 1,nnodk                          ! touch their position
                      kpoin = lnods(knode,kelem)
                      if( kpoin == jpoin ) then
                         kposi = (klist-1)*mnode+knode
                         touch(kposi) = .true.
                      end if
                   end do
                end do
                nz = nz + 1
             end if
          end do
       end do

    else if( me < 0 ) then
       !
       ! Lower graph
       !
       do jlist = 1,nlist                                      ! Loop over those elements 
          jelem = liste(jlist)                                 ! where the point is
          nnodj = lnnod(jelem)
          do jnode = 1,nnodj
             jpoin = lnods(jnode,jelem)
             if( jpoin < -me ) then
                jposi = (jlist-1)*mnode+jnode
                if( .not. touch(jposi) ) then                     ! Position not touched           
                   do klist = 1,nlist                             ! Search other elements 
                      kelem = liste(klist)                        ! where JPOIN is and 
                      nnodk = lnnod(kelem)
                      do knode = 1,nnodk                          ! touch their position
                         kpoin = lnods(knode,kelem)
                         if( kpoin == jpoin ) then
                            kposi = (klist-1)*mnode+knode
                            touch(kposi) = .true.
                         end if
                      end do
                   end do
                   nz = nz + 1
                end if
             end if
          end do
       end do

    else
       !
       ! All graph without diagonal
       !
       do jlist = 1,nlist                                      ! Loop over those elements 
          jelem = liste(jlist)                                 ! where the point is
          nnodj = lnnod(jelem)
          do jnode = 1,nnodj
             jpoin = lnods(jnode,jelem)
             if( jpoin /= me ) then
                jposi = (jlist-1)*mnode+jnode
                if( .not. touch(jposi) ) then                     ! Position not touched           
                   do klist = 1,nlist                             ! Search other elements 
                      kelem = liste(klist)                        ! where JPOIN is and 
                      nnodk = lnnod(kelem)
                      do knode = 1,nnodk                          ! touch their position
                         kpoin = lnods(knode,kelem)
                         if( kpoin == jpoin ) then
                            kposi = (klist-1)*mnode+knode
                            touch(kposi) = .true.
                         end if
                      end do
                   end do
                   nz = nz + 1
                end if
             end if
          end do
       end do

    end if

  end subroutine graphs_nzecof

  !-----------------------------------------------------------------------
  !> @file    graphs_arrind.f90
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Constructs the arrays of indexes for a mesh graph
  !> @details Constructs the arrays of indexes for a mesh graph
  !>          These are organized as follows (CSR format):
  !>          IA(II) = coefficient of the graph where IPOIN starts,
  !>          JA(NZ) = column of the NZ coefficient of the graph
  !>          mesh graph stored in compressed sparse row (CSR) format 
  !-----------------------------------------------------------------------

  subroutine graphs_arrind(&
       mnode,lnods,lnnod,ltype,nlist,ncoef,liste,touch,&
       nz,ipoin,ia,ja,me,only_edges)
    implicit none
    integer(ip),          intent(in)    :: mnode
    integer(ip),          intent(in)    :: lnods(mnode,*)
    integer(ip),          intent(in)    :: lnnod(*)
    integer(ip),          intent(in)    :: ltype(*)
    integer(ip),          intent(in)    :: nlist
    integer(ip),          intent(in)    :: ncoef
    integer(ip),          intent(in)    :: ipoin
    integer(ip),          intent(in)    :: liste(nlist)
    integer(ip),          intent(inout) :: nz
    logical(lg),          intent(inout) :: touch(ncoef)
    integer(ip), pointer, intent(inout) :: ia(:)
    integer(ip), pointer, intent(inout) :: ja(:)
    integer(ip), intent(in)             :: me
    logical(lg), intent(in)             :: only_edges
    integer(ip)                         :: jelem,jnode,jpoin,nnodj,jposi,jlist
    integer(ip)                         :: kelem,knode,kpoin,nnodk,kposi,klist
    integer(ip)                         :: jtype,jedge,jnod1,jnod2

    ia(ipoin) = nz

    if( only_edges ) then
       !
       ! Use only edges lower part to construct graph
       !
       do jlist = 1,nlist                              
          jelem = liste(jlist)
          nnodj = lnnod(jelem)
          do jnode = 1,nnodj
             jposi = (jlist-1)*mnode+jnode
             touch(jposi) = .true.
          end do
       end do
       do jlist = 1,nlist   
          jelem = liste(jlist)
          jtype = abs(ltype(jelem))
          do jedge = 1,needg(jtype)
             jnod1 = leedg(1,jedge,jtype)
             jnod2 = leedg(2,jedge,jtype)
             if( lnods(jnod1,jelem) == ipoin ) then
                jposi        = (jlist-1)*mnode+jnod2
                touch(jposi) = .false.                
             else if( lnods(jnod2,jelem) == ipoin ) then
                jposi        = (jlist-1)*mnode+jnod1
                touch(jposi) = .false.
             end if
          end do
       end do
    end if

    if( me == 0 ) then

       do jlist = 1,nlist
          jelem = liste(jlist)
          nnodj = lnnod(jelem)
          do jnode = 1,nnodj
             jpoin = lnods(jnode,jelem)
             jposi = (jlist-1) * mnode + jnode
             if( .not. touch(jposi) ) then
                do klist = 1,nlist
                   kelem = liste(klist)
                   nnodk = lnnod(kelem)
                   do knode = 1,nnodk
                      kpoin = lnods(knode,kelem)
                      if(kpoin==jpoin) then
                         kposi = (klist-1)*mnode+knode
                         touch(kposi) = .true.
                      end if
                   end do
                end do
                ja(nz) = jpoin
                nz = nz+1
             end if
          end do
       end do

    else if( me < 0 ) then

       do jlist = 1,nlist
          jelem = liste(jlist)
          nnodj = lnnod(jelem)
          do jnode = 1,nnodj
             jpoin = lnods(jnode,jelem)
             if( jpoin < -me ) then
                jposi = (jlist-1) * mnode + jnode
                if( .not. touch(jposi) ) then
                   do klist = 1,nlist
                      kelem = liste(klist)
                      nnodk = lnnod(kelem)
                      do knode = 1,nnodk
                         kpoin = lnods(knode,kelem)
                         if(kpoin==jpoin) then
                            kposi = (klist-1)*mnode+knode
                            touch(kposi) = .true.
                         end if
                      end do
                   end do
                   ja(nz) = jpoin
                   nz = nz+1
                end if
             end if
          end do
       end do

    else

       do jlist = 1,nlist
          jelem = liste(jlist)
          nnodj = lnnod(jelem)
          do jnode = 1,nnodj
             jpoin = lnods(jnode,jelem)
             if( jpoin /= me ) then
                jposi = (jlist-1) * mnode + jnode
                if( .not. touch(jposi) ) then
                   do klist = 1,nlist
                      kelem = liste(klist)
                      nnodk = lnnod(kelem)
                      do knode = 1,nnodk
                         kpoin = lnods(knode,kelem)
                         if(kpoin==jpoin) then
                            kposi = (klist-1)*mnode+knode
                            touch(kposi) = .true.
                         end if
                      end do
                   end do
                   ja(nz) = jpoin
                   nz = nz+1
                end if
             end if
          end do
       end do

    end if

  end subroutine graphs_arrind

  !-----------------------------------------------------------------------
  !> @file    graphs_mergli.f90
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Merges to list of nodes
  !> @details Merges to list of nodes
  !-----------------------------------------------------------------------

  subroutine graphs_mergli(lista,lsize,nnode,lnods,ltype,me,only_edges)
    implicit none
    integer(ip), intent(inout) :: lsize
    integer(ip), intent(inout) :: lista(*)
    integer(ip), intent(in)    :: nnode
    integer(ip), intent(in)    :: lnods(nnode)
    integer(ip), intent(in)    :: ltype
    integer(ip), intent(in)    :: me
    logical(lg), intent(in)    :: only_edges
    integer(ip)                :: ii, jj, n1, n2
    logical(lg)                :: noEncontrado
    integer(ip)                :: lnods_2(nnode)
    integer(ip)                :: itype,i1,i2,iedge,meabs
    integer(ip)                :: ipoin1,ipoin2

    if( only_edges ) then
       !
       ! Use only edges lower part to construct graph
       !
       meabs = abs(me)
       do ii = 1,nnode
          lnods_2(ii) = 0
       end do
       itype = abs(ltype)
       do iedge = 1,needg(itype)
          i1     = leedg(1,iedge,itype)
          i2     = leedg(2,iedge,itype)
          ipoin1 = lnods(i1)
          ipoin2 = lnods(i2)
          if( ipoin1 == meabs .or. ipoin2 == meabs ) then
             lnods_2(i1) = lnods(i1)
             lnods_2(i2) = lnods(i2)
          end if
       end do
       do ii = 1, nnode
          n1 = lnods_2(ii)
          if( n1 < -me .and. n1 /= 0 ) then
             jj = 1
             noEncontrado = .true.
             do while( jj <= lsize .and. noEncontrado )
                n2 = lista(jj)
                if( n1 == n2 ) then
                   noEncontrado = .false.
                end if
                jj = jj + 1
             end do
             if( noEncontrado ) then
                lsize = lsize + 1
                lista(lsize) = n1
             end if
          end if
       end do

    else 

       if( me == 0 ) then

          do ii = 1,nnode
             n1 = lnods(ii)
             jj = 1
             noEncontrado = .true.
             do while( jj <= lsize .and. noEncontrado )
                n2 = lista(jj)
                if ( n1 == n2 ) then
                   noEncontrado = .false.
                end if
                jj = jj + 1
             end do
             if ( noEncontrado ) then
                lsize = lsize + 1
                lista(lsize) = n1
             end if
          end do

       else if( me < 0 ) then

          do ii = 1, nnode
             n1 = lnods(ii)
             if( n1 < -me ) then
                jj = 1
                noEncontrado = .true.
                do while( jj <= lsize .and. noEncontrado )
                   n2 = lista(jj)
                   if( n1 == n2 ) then
                      noEncontrado = .false.
                   end if
                   jj = jj + 1
                end do
                if( noEncontrado ) then
                   lsize = lsize + 1
                   lista(lsize) = n1
                end if
             end if
          end do

       else

          do ii = 1, nnode
             n1 = lnods(ii)
             if( n1 /= me ) then
                jj = 1
                noEncontrado = .true.
                do while( jj <= lsize .and. noEncontrado )
                   n2 = lista(jj)
                   if( n1 == n2 ) then
                      noEncontrado = .false.
                   end if
                   jj = jj + 1
                end do
                if( noEncontrado ) then
                   lsize = lsize + 1
                   lista(lsize) = n1
                end if
             end if
          end do

       end if

    end if

  end subroutine graphs_mergli

  !-----------------------------------------------------------------------
  !> @file    graphs_recurs.f90
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Mark recursively the node of a graph
  !> @details Mark recursively the node of a graph
  !-----------------------------------------------------------------------

  subroutine graphs_recurs(npoin,ia,ja,lmark,ii)

    implicit none
    integer(ip),          intent(in)    :: npoin
    integer(ip), pointer, intent(in)    :: ia(:)
    integer(ip), pointer, intent(in)    :: ja(:)
    integer(ip), pointer, intent(inout) :: lmark(:)
    integer(ip),          intent(inout) :: ii
    integer(ip), pointer                :: lstack(:) => null()
    logical(lg), pointer                :: touch(:)  => null()
    integer(ip)                         :: nstack,istack,izdom,jj
    !
    ! Initialization
    !
    nullify(lstack)
    nullify(touch)

    call memory_alloca(memor_dom,'LSTACK','graphs_recurs',lstack,npoin)
    call memory_alloca(memor_dom,'TOUCH' ,'graphs_recurs',touch, npoin)
    nstack    = 1
    lstack(1) = ii
    touch(ii) = .true.
    istack    = 0
    lmark(ii) = 1

    do while( istack /= nstack ) 

       istack = istack + 1  
       ii     = lstack(istack)

       do izdom = ia(ii),ia(ii+1)-1
          jj = ja(izdom)
          if( jj /= ii ) then
             if( .not. touch(jj) .and. lmark(jj) >= 0 ) then
                touch(jj)      = .true.
                lmark(jj)      = 1
                nstack         = nstack + 1
                lstack(nstack) = jj
             end if
          end if
       end do

    end do

    call memory_deallo(memor_dom,'TOUCH' ,'graphs_recurs',touch)
    call memory_deallo(memor_dom,'LSTACK','graphs_recurs',lstack)

  end subroutine graphs_recurs

  !-----------------------------------------------------------------------
  !> @file    graphs_recurs.f90
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Mark recursively the node of a graph
  !> @details Mark recursively the node of a graph
  !-----------------------------------------------------------------------

  subroutine graphs_recurl(npoin,nlaye,ia,ja,nstack,lmark,lstack)

    implicit none
    integer(ip),          intent(in)    :: npoin
    integer(ip),          intent(in)    :: nlaye
    integer(ip), pointer, intent(in)    :: ia(:)
    integer(ip), pointer, intent(in)    :: ja(:)
    integer(ip),          intent(inout) :: nstack
    integer(ip), pointer, intent(inout) :: lmark(:)
    integer(ip), pointer, intent(inout) :: lstack(:)
    logical(lg), pointer                :: touch(:)
    integer(ip)                         :: istack,izdom,ii,jj,klaye
    !
    ! Initialization
    !
    nullify(touch)

    call memory_alloca(memor_dom,'TOUCH' ,'graphs_recurl',touch,npoin)
    !
    ! Fill in stack
    !
    do istack = 1,nstack
       ii = lstack(istack) 
       touch(ii) = .true.
    end do
    if( nstack == 0 ) call runend('GRAPHS_RECURL: COULD NOT FIND SEED FOR RECURSIVE WITH LAYERS')
    !
    ! Recursive loop: take only NLAYE layers
    !
    istack = 0
    klaye  = 1

    do while( istack /= nstack ) 

       istack = istack + 1  
       ii     = lstack(istack)
       klaye  = lmark(ii) + 1

       if( lmark(ii) < nlaye ) then
          do izdom = ia(ii),ia(ii+1)-1
             jj = ja(izdom)
             if( jj /= ii ) then
                if( .not. touch(jj) .and. lmark(jj) >= 0 .and. lmark(ii) < nlaye ) then
                   touch(jj)      = .true.
                   lmark(jj)      = klaye
                   nstack         = nstack + 1
                   lstack(nstack) = jj
                end if
             end if
          end do
       end if

    end do

    call memory_deallo(memor_dom,'TOUCH' ,'graphs_recurl',touch)

  end subroutine graphs_recurl

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute a graph
  !> @details Compute the element-element graph using face connectivity
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine graphs_eleele_faces(&
       nelem,mnode,mnodb,nelty,mepoi,mface,lnods,lnnod,ltype,&
       nnodf,cfael,nface,pelpo,lelpo,nedge,medge,pelel,lelel)
    implicit none  
    integer(ip),          intent(in)            :: nelem                           !< Number of elements 		
    integer(ip),          intent(in)            :: mnode                           !< Max. number of nodes per element
    integer(ip),          intent(in)            :: mnodb                           !< Max. number of nodes per face
    integer(ip),          intent(in)            :: nelty                           !< Number of element types
    integer(ip),          intent(in)            :: mepoi                           !< Max. number of element per node
    integer(ip),          intent(in)            :: mface                           !< Number of faces for all elements
    integer(ip),          intent(in)            :: lnods(mnode,*)                  !< Connectivity array
    integer(ip),          intent(in)            :: lnnod(*)                        !< Array of number of element nodes
    integer(ip),          intent(in)            :: ltype(*)                        !< Array of element types
    integer(ip),          intent(in)            :: nnodf(mface,nelty)              !< Number of face of element types
    integer(ip),          intent(in)            :: cfael(mnodb,mface,nelty)        !< Number of face of element types
    integer(ip),          intent(in)            :: nface(*)                        !< Number of face of element types
    integer(ip), pointer, intent(in)            :: pelpo(:)                        !< Linked list of (element-node) pointer
    integer(ip), pointer, intent(in)            :: lelpo(:)                        !< Linked list of (element-node) elements
    integer(ip),          intent(out)           :: nedge                           !< Number of edges
    integer(ip),          intent(out)           :: medge                           !< Max. number of edges per element
    integer(ip), pointer, intent(out)           :: pelel(:)                        !< Linked list of (element-element) pointer
    integer(ip), pointer, intent(out)           :: lelel(:)                        !< Linked list of (element-element) elements
    integer(ip)                                 :: ielem,kelem,inode,jelem
    integer(ip)                                 :: melel,jnode,kpoin,pelem
    integer(ip)                                 :: iface,jface,ielty,ipoin
    integer(ip)                                 :: ilist,dummi,ielpo,inodb
    integer(ip)                                 :: pepoi,jelty
    integer(ip), pointer                        :: faces(:,:,:)
    logical(lg)                                 :: equal_faces
    !
    ! Allocate memory
    !
    nullify(faces)
    call memory_alloca(memor_dom,'FACES','graphs_eleele_faces',faces,mnodb+1_ip,mface,nelem)
    call memory_alloca(memor_dom,'PELEL','graphs_eleele_faces',pelel,nelem+1_ip )
    !
    ! Construct and sort FACES 
    !
    do ielem = 1,nelem  
       ielty = abs(ltype(ielem))
       do iface = 1,nface(ielty) 
          do inodb = 1,nnodf(iface,ielty)
             inode = cfael(inodb,iface,ielty)
             faces(inodb,iface,ielem) = lnods(inode,ielem)
          end do
          faces(mnodb+1,iface,ielem) = 1
          call sortin(nnodf(iface,ielty),faces(1,iface,ielem))
       end do
    end do
    !
    ! Compute FACES
    !
    do ielem = 1,nelem                                          ! Compare the faces and 
       ielty = abs(ltype(ielem))                                ! eliminate the repited faces
       do iface = 1,nface(ielty)
          if( faces(mnodb+1,iface,ielem) > 0 ) then
             ipoin = faces(1,iface,ielem)
             if( ipoin /= 0 ) then
                ilist = 1
                pepoi = pelpo(ipoin+1)-pelpo(ipoin)
                ielpo = pelpo(ipoin)-1
                do while( ilist <= pepoi )
                   ielpo = ielpo+1
                   jelem = lelpo(ielpo)
                   if( jelem /= ielem ) then
                      jelty = abs(ltype(jelem))                              ! eliminate the repited faces
                      jface = 0
                      do while( jface /= nface(jelty) )
                         jface = jface + 1
                         if( faces(1,jface,jelem) /= 0 ) then
                            equal_faces = .true.
                            inodb       = 0
                            do while( equal_faces .and. inodb /= nnodf(jface,jelty) )
                               inodb = inodb + 1
                               if( faces(inodb,iface,ielem) /= faces(inodb,jface,jelem) ) &
                                    equal_faces = .false.
                            end do
                            if( equal_faces ) then
                               faces(1,iface,ielem)       = 0            ! IFACE and JFACE
                               faces(1,jface,jelem)       = 0            ! are eliminated
                               faces(mnodb+1,iface,ielem) = jelem        ! IFACE and JFACE
                               faces(mnodb+1,jface,jelem) = ielem        ! are eliminated
                               nedge                      = nedge + 2
                               jface                      = nface(jelty) ! Exit JFACE do
                               ilist                      = pepoi        ! Exit JELEM do
                            end if
                         end if
                      end do
                   end if
                   ilist = ilist + 1
                end do
             end if
          end if
       end do
    end do
    !
    ! Allocate memoty for adjacancies LELEL
    !
    call memory_alloca(memor_dom,'LELEL','graphs_eleele_faces',lelel,nedge)
    !
    ! Compute PELEL and LELEL
    !
    dummi    = 0
    pelel(1) = 1
    do ielem = 1,nelem
       ielty = abs(ltype(ielem))
       do iface = 1,nface(ielty)
          if( faces(1,iface,ielem) == 0 ) then
             dummi          = dummi + 1
             pelel(ielem+1) = pelel(ielem+1) + 1
             lelel(dummi)   = faces(mnodb+1,iface,ielem)
          end if
       end do
       pelel(ielem+1) = pelel(ielem) + pelel(ielem+1)
    end do
    !
    ! Deallocate memory of FACES, CFAEL and NNODF
    !
    call memory_deallo(memor_dom,'FACES','par_elmgra',faces)
    !
    ! Maximum number of edges in the mesh
    !
    medge = 0
    do ielem = 1,nelem
       if( pelel(ielem+1)-pelel(ielem) > medge ) then
          medge = pelel(ielem+1)-pelel(ielem)
       end if
    end do

  end subroutine graphs_eleele_faces

  !-----------------------------------------------------------------------
  !
  !> @brief   Compute a graph
  !> @details Compute the list of faces
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine graphs_list_faces(&
       nelem,mnode,mnodb,nelty,mface,lnods,lnnod,ltype,&
       nnodf,cfael,nface,pelpo,lelpo,nfacg,lface,lelfa,&
       lchek)
    implicit none  
    integer(ip),          intent(in)           :: nelem                           !< Number of elements 		
    integer(ip),          intent(in)           :: mnode                           !< Max. number of nodes per element
    integer(ip),          intent(in)           :: mnodb                           !< Max. number of nodes per face
    integer(ip),          intent(in)           :: nelty                           !< Number of element types
    integer(ip),          intent(in)           :: mface                           !< Number of faces for all elements
    integer(ip),          intent(in)           :: lnods(mnode,*)                  !< Connectivity array
    integer(ip),          intent(in)           :: lnnod(*)                        !< Array of number of element nodes
    integer(ip),          intent(in)           :: ltype(*)                        !< Array of element types
    integer(ip),          intent(in)           :: nnodf(mface,nelty)              !< Number of face of element types
    integer(ip),          intent(in)           :: cfael(mnodb,mface,nelty)        !< Number of face of element types
    integer(ip),          intent(in)           :: nface(*)                        !< Number of face of element types
    integer(ip), pointer, intent(in)           :: pelpo(:)                        !< Linked list of (element-node) pointer
    integer(ip), pointer, intent(in)           :: lelpo(:)                        !< Linked list of (element-node) elements
    integer(ip),          intent(out)          :: nfacg                           !< Number of faces
    integer(ip), pointer, intent(out)          :: lface(:,:)                      !< List of faces
    type(i1p),   pointer, intent(out)          :: lelfa(:)                        !< List of alement faces
    logical(lg), pointer, intent(in), optional :: lchek(:)                        !< Linked list of (element-node) elements
    integer(ip)                                :: ielem,kelem,inode,jelem
    integer(ip)                                :: melel,jnode,kpoin,pelem
    integer(ip)                                :: iface,jface,ielty,ipoin
    integer(ip)                                :: ilist,dummi,ielpo,inodb
    integer(ip)                                :: pepoi,jelty,pnodf
    integer(ip), pointer                       :: faces(:,:,:)
    logical(lg)                                :: equal_faces,ichek,jchek
    !
    ! Allocate memory
    !
    nullify(faces)
    call memory_alloca(memor_dom,'FACES','graphs_list_faces',faces,mnodb+1_ip,mface,nelem)
    call memory_alloca(memor_dom,'LELFA','graphs_list_faces',lelfa,nelem)
    !
    ! Construct and sort FACES 
    !
    do ielem = 1,nelem  
       ichek = .true.
       if( present(lchek) ) then
          ichek = lchek(ielem)
       end if
       if( ichek ) then
          ielty = abs(ltype(ielem))       
          call memory_alloca(memor_dom,'LELFA(IELEM) % L','lgface',lelfa(ielem)%l,nface(ielty))
          do iface = 1,nface(ielty) 
             do inodb = 1,nnodf(iface,ielty)
                inode = cfael(inodb,iface,ielty)
                faces(inodb,iface,ielem) = lnods(inode,ielem)
             end do
             faces(mnodb+1,iface,ielem) = 1
             call sortin(nnodf(iface,ielty),faces(1,iface,ielem))
          end do
       end if
    end do
    !
    ! Compute FACES
    !
    nfacg = 0
    do ielem = 1,nelem                                                          ! Compare the faces and 
       ichek = .true.
       if( present(lchek) ) then
          ichek = lchek(ielem)
       end if
       if( ichek ) then
          ielty = abs(ltype(ielem))                                                ! eliminate the repited faces
          do iface = 1,nface(ielty)
             if( faces(mnodb+1,iface,ielem) > 0 ) then
                nfacg = nfacg + 1
                ipoin = faces(1,iface,ielem)
                if( ipoin /= 0 ) then
                   ilist = 1
                   pepoi = pelpo(ipoin+1)-pelpo(ipoin)
                   ielpo = pelpo(ipoin)-1
                   do while( ilist <= pepoi )
                      ielpo = ielpo+1
                      jelem = lelpo(ielpo)
                      if( jelem /= ielem ) then
                         jchek = .true.
                         if( present(lchek) ) then
                            jchek = lchek(jelem)
                         end if
                         if( jchek ) then
                            jelty = abs(ltype(jelem))                                 ! eliminate the repited faces
                            jface = 0
                            do while( jface /= nface(jelty) )
                               jface = jface + 1
                               if( faces(1,jface,jelem) /= 0 ) then
                                  equal_faces = .true.
                                  inodb       = 0
                                  do while( equal_faces .and. inodb /= nnodf(jface,jelty) )
                                     inodb = inodb + 1
                                     if( faces(inodb,iface,ielem) /= faces(inodb,jface,jelem) ) &
                                          equal_faces = .false.
                                  end do
                                  if( equal_faces ) then
                                     faces(mnodb+1,iface,ielem) =  jelem        ! Keep IELEM face
                                     faces(mnodb+1,jface,jelem) = -ielem        ! Elminate JELEM face
                                     faces(      1,iface,ielem) = -jface        ! Remember IFACE face
                                     jface                      =  nface(jelty) ! Exit JFACE do
                                     ilist                      =  pepoi        ! Exit JELEM do  
                                  end if
                               end if
                            end do
                         end if
                      end if
                      ilist = ilist + 1
                   end do
                end if
             end if
          end do
       end if
    end do
    !
    ! List of faces if required
    !
    call memory_alloca(memor_dom,'LFACE','graphs_list_faces',lface,4_ip,nfacg)     
    nfacg = 0
    do ielem = 1,nelem                                            ! Compare the faces and 
       ichek = .true.
       if( present(lchek) ) then
          ichek = lchek(ielem)
       end if
       if( ichek ) then
          ielty = abs(ltype(ielem))                                  ! eliminate the repeated faces
          do iface = 1,nface(ielty)
             if( faces(mnodb+1,iface,ielem) > 0 ) then
                nfacg = nfacg + 1
                pnodf = nnodf(iface,ielty)
                if( faces(1,iface,ielem) < 0 ) then
                   jelem                   =  faces(mnodb+1,iface,ielem)
                   jface                   = -faces(      1,iface,ielem)
                   lelfa(ielem) % l(iface) =  nfacg
                   lelfa(jelem) % l(jface) =  nfacg
                   lface(1,nfacg)          =  ielem
                   lface(2,nfacg)          =  jelem
                   lface(3,nfacg)          =  iface
                   lface(4,nfacg)          =  jface
                else
                   lelfa(ielem) % l(iface) =  nfacg
                   lface(1,nfacg)          =  ielem
                   lface(2,nfacg)          =  0
                   lface(3,nfacg)          =  iface
                   lface(4,nfacg)          =  0
                end if
             end if
          end do
       end if
    end do
    !
    ! Deallocate memory of FACES, CFAEL and NNODF
    !
    call memory_deallo(memor_dom,'FACES','graphs_list_faces',faces)

  end subroutine graphs_list_faces

  subroutine graphs_deallocate_list_faces(&
       nelem,lface,lelfa,lchek)
    implicit none  
    integer(ip),          intent(in)           :: nelem                           !< Number of elements 		
    integer(ip), pointer, intent(out)          :: lface(:,:)                      !< List of faces
    type(i1p),   pointer, intent(out)          :: lelfa(:)                        !< List of alement faces
    logical(lg), pointer, intent(in), optional :: lchek(:)                        !< Linked list of (element-node) elements
    integer(ip)                                :: ielem
    logical(lg)                                :: ichek

    do ielem = 1,nelem  
       ichek = .true.
       if( present(lchek) ) then
          ichek = lchek(ielem) 
       end if
       if( ichek ) then
          call memory_deallo(memor_dom,'LELFA(IELEM) % L','lgface',lelfa(ielem) % l)
       end if
    end do
    call memory_deallo(memor_dom,'LELFA','graphs_list_faces',lelfa)
    call memory_deallo(memor_dom,'LFACE','graphs_list_faces',lface)     

  end subroutine graphs_deallocate_list_faces

end module mod_graphs_paral
!> @}
