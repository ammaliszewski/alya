subroutine inidom()
  !-----------------------------------------------------------------------
  !****f* domain/inidom
  ! NAME
  !    inidom
  ! DESCRIPTION
  !    This routines initialize domain variables.
  ! USED BY
  !    domain
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_meshin, only : memor_msh
  use def_inpout
  use mod_iofile
  use mod_memchk
  implicit none
  integer(ip) :: icode,idofn,ielty
  !
  ! Nullify pointers from reageo
  !
  nullify(lnods)         
  nullify(ltype)         
  nullify(lnodb)        
  nullify(ltypb)         
  nullify(lboch)         
  nullify(lnnod)         
  nullify(lelch)         
  nullify(lmate)         
  nullify(lesub)         
  nullify(nelez)      
  nullify(lnoch)       
  nullify(lgrou_dom)     
  nullify(kfl_field) 
  nullify(lperi)    
  nullify(coord)    
  nullify(skcos)    
  nullify(lelez)     
  nullify(lhang)    
  nullify(xfiel)  
  nullify(time_field)  
  !
  ! Nullify pointers from reaset
  !
  nullify(leset)
  nullify(lbset)
  nullify(lesec)
  nullify(lbsec)
  nullify(lnsec)
  nullify(lbpse)
  !
  ! Nullify pointers from reabcs
  !
  nullify(kfl_codno)
  nullify(kfl_codbo)
  nullify(lvcod)
  nullify(lvcob)
  nullify(bvcod)
  nullify(bvcob)
  !
  ! Nullify zones and nodal materials
  !
  nullify(npoiz)
  nullify(nbouz)
  nullify(lpoib)
  nullify(nmatn)
  nullify(lbono)
  nullify(lpoiz)
  nullify(lbouz)
  nullify(lmatn) 
  nullify(lnnob) 
  !
  ! Nullify other pointers
  ! 
  nullify(nehan)
  nullify(lehan)
  nullify(nepoi)
  nullify(pelpo)
  nullify(lelpo)
  nullify(pelpo_2)    
  nullify(lelpo_2)    
  nullify(pelel)      
  nullify(lelel)      
  nullify(pelel_2)    
  nullify(lelel_2)    
  nullify(lezdo)      
  nullify(lbzdo)   
  nullify(r_sol)      
  nullify(c_sol)       
  nullify(r_dom)      
  nullify(c_dom)      
  nullify(r_bou)      
  nullify(c_bou)      
  nullify(r_dom_aii)  
  nullify(permr_aii)  
  nullify(invpr_aii)  
  nullify(c_dom_aii)  
  nullify(r_dom_aib)  
  nullify(c_dom_aib)  
  nullify(r_dom_abi)  
  nullify(c_dom_abi)  
  nullify(r_dom_abb)  
  nullify(c_dom_abb)  
  nullify(permr_abb)  
  nullify(invpr_abb)  
  nullify(r_dom_prec) 
  nullify(c_dom_prec) 
  nullify(permr_prec) 
  nullify(invpr_prec) 
  nullify(r_sym)      
  nullify(c_sym)       
  nullify(lpoty)      
  nullify(lfacs)      
  nullify(lboel)      
  nullify(lelbo)      
  nullify(lgaib)      
  nullify(lrenn)      
  nullify(lfcnt)      
  nullify(lncnt) 
  nullify(lessl) 
  nullify(kfl_fixno)  
  nullify(kfl_fixbo)  
  nullify(kfl_funno)  
  nullify(kfl_funbo)  
  nullify(kfl_fixrs)  
  nullify(kfl_geobo)  
  nullify(kfl_geono)  
  nullify(lpoin)       
  nullify(bvess)      
  nullify(bvnat)       
  nullify(tncod)       
  nullify(tgcod)       
  nullify(exnor)      
  nullify(exaux)      
  nullify(vmass)      
  nullify(vmasc)      
  nullify(walld)      
  nullify(rough)      
  nullify(canhe)      
  nullify(heiov)      
  nullify(ywalb)      
  nullify(ywalp)       
  nullify(elmar)       
  nullify(elmda)       
  nullify(pefpo)     
  nullify(lefpo)     
  nullify(lnuew)     
  nullify(leldo)     
  nullify(facel)     
  nullify(lnlev)
  nullify(lelev)
  nullify(lblev)
  nullify(lmast)
  nullify(meshe)
  nullify(lpmsh)
  nullify(cutel)
  nullify(element_bin)
  nullify(k_tran_fiel)
  nullify(x_tran_fiel)
  !
  ! General
  !
  lispa         = 0
  lisda         = lun_pdata_dom  ! Reading file
  lisre         = lun_outpu_dom  ! Writing file
  nzsky         = 0              ! Components of the matrix
  memor_dom     = 0              ! Memory counters
  nperi         = 0              ! # of periodic nodes
  mperi         = 0              ! Max # of periodic nodes
  necnt         = 0              ! Number of contact elements
  nncnt         = 0              ! Number of contact elements
  kfl_parex     = 0              ! No automatic parallelization
  nbono         = 0              ! Number of boundary nodes
  kfl_crbou     = 0              ! Boundary graph has not been computed
  kfl_pelel     = 0              ! No element graph
  kfl_domar     = 0              ! Do not recompute mesh arrays (do not go to domarr)
  bandw_dom     = 0              ! Bandwidth
  profi_dom     = 0.0_rp         ! Profile
  kfl_horde     = 0              ! High order element do not exist  
  nfacg         = 0              ! Number of faces
  nface         = 0              ! Number of faces per element type
  mface         = 0              ! Max number of faces
  do ielty = 1,nelty
     lnuty(ielty) = 0 
     lnuib(ielty) = 0
     nullify(ltypf(ielty) % l)
     nullify(nnodf(ielty) % l)
     nullify(lface(ielty) % l)
  end do
  kexist_tran_fiel = 0               
  !
  ! Immersed boundary
  !
  mnoib         = 0              ! IB boundary nodes
  mnodi         = 0              ! IB element nodes
  mgaib         = 0              ! IB Gauss points
  mgaui         = 0              ! IB Gauss points
  !
  ! Mesh generator
  !
  memor_msh     = 0              !  Memory counters  
  !
  ! Variables read in readim
  !
  kfl_autbo =  0                 ! Automatic boundaries off
  npoin     = -1                 ! Obligatory
  nelem     = -1                 ! Obligatory
#ifdef NDIMEPAR

#else
  ndime     = -1                 ! Obligatory
#endif  
  nboun     = -1                 ! Optional
  if( ISEQUEN ) then
     allocate( npoin_par(1) )
     allocate( nelem_par(1) )
     allocate( nboun_par(1) )
  end if
  nperi     =  0                 ! Optional
  nskew     = -1                 ! Optional
  nboib     =  0                 ! Optional: # IB 
  npoib     =  0                 ! Optional: # IB points 
  nimbo     =  0                 ! Optional
  nrbod     =  0                 ! Optional
  !
  ! Variables read in reastr
  !
  lquad      = 0                 ! Open integration rule
  kfl_binar  = 0                 ! Don't write mesh in binary format
  !
  ! Variables set in reageo
  !
  kfl_chege = 0                   ! Don't check geometry
  kfl_naxis = 0                   ! Cartesian coordinate system
  kfl_spher = 0                   ! Cartesian coordinate system
  kfl_bouel = 0                   ! Element # connected to boundary unknown
  nmate     = 1                   ! No material
  nmatf     = 1                   ! Fluid material (when ALE, this is also the ALE SOLID material)
  !
  ! Variable read in reabcs
  !
  ncodn      = 0                  ! There is no node code array
  ncodb      = 0                  ! There is no boundary code array
  kfl_crbou  = 0                  ! Boundary graph allocated?
  nvcod      = 0                  ! Node values
  nvcob      = 0                  ! Boundary values
  !
  ! Element bin
  !
  element_bin_boxes(1:3) = 4      ! Read by kermod
  !
  ! Element array
  !
  call cshder(1_ip)

end subroutine inidom


