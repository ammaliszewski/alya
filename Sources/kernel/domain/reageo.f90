!-----------------------------------------------------------------------
!> @addtogroup DomainInput
!> @{
!> @file    reageo.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Read mesh arrays
!> @details Read mesh arrays:
!!          \verbatim
!!          - Element arrays:
!!            - LEXIS(NELTY) ............ Wether an element type exists or not
!!            - LNODS(MNODE,NELEM) ...... Element connectivity
!!            - LTYPE(NELEM) ............ Types of element
!!            - LMATE(NELEM) ............ Materials
!!            - LELCH(NELEM) ............ Element characteristic: 0,1,2 (finite element, extension, hole)
!!            - LELEZ(NZONE) % L(:) ..... List of elements for each zone
!!          - Nodal arrays:
!!            - COORD(NDIME,NPOIN) ...... Node coordinates
!!            - LNOCH(NPOIN) ............ Node characteristic: 0,2 (finite element, hole)
!!          - Boundary arrays:
!!            - LNODB(MNODB,NBOUN) ...... Boundary connectivity
!!            - LBOEL(MNODB+1,NBOUN) .... List of elements connected to boundaries
!!            - LTYPB(NBOUN) ............ Types of boundary
!!            - LBOCH(NBOUN) ............ Boundary characteristic: 0,1 (finite boundary, extension)
!!          - Others
!!            - NGROU_DOM ............... Number of groups for deflation based solvers
!!            - LPERI(2,NPERI) .......... List of master/slave periodic nodes
!!            - NFIEL ................... Number of fields
!!            - KFL_FIELD(4,NFIEL) ...... Dimension/type of fields,steps
!!            - XFIEL(NFIEL) % A(:,:) ... Values of field
!!            - TIME_FIELD(NFIEL) % A(:) .. Time corresponding to each step of a field
!!          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine reageo()
  use def_kintyp
  use def_parame
  use def_master 
  use def_domain
  use def_inpout
  use mod_iofile
  use mod_memory
  use def_elmtyp
  implicit none
  integer(ip)           :: ielem,ipoin,jpoin,inode,idime,iimbo
  integer(ip)           :: iboun,inodb,ielty,ktype,dummi,kfl_defau
  integer(ip)           :: iskew,jskew,jdime,imate,kelem,nlimi
  integer(ip)           :: iblty,knode,kfl_gidsk,kfl_dontr,kpoin
  integer(ip)           :: kfl_binme,ipara,iperi,imast,kfl_icgns
  integer(ip)           :: pelty,ifiel,izone,kfl_ifmat,kfl_elino
  integer(ip)           :: isubd,jstep
  real(rp)              :: dummr
  character(20)         :: chnod
  integer(ip),  pointer :: lelno(:)

  nullify(lelno)

  if( ISEQUEN .or. ( IMASTER .and. kfl_ptask /= 2 ) ) then

     call livinf(31_ip,' ',0_ip)
     !
     ! Initializations
     !
     kfl_chege  = 0                   ! Don't check geometry
     kfl_naxis  = 0                   ! Cartesian coordinate system
     kfl_spher  = 0                   ! Cartesian coordinate system
     kfl_bouel  = 0                   ! Element # connected to boundary unknown
     nmate      = 1                   ! No material
     nmatf      = 1                   ! Fluid material 
     ngrou_dom  = 0                   ! Groups (for deflated): -2 (slaves), -1 (automatic, master), >0 (master)
     kfl_gidsk  = 0                   ! General skew system
     kfl_dontr  = 0                   ! Read boundaries
     kfl_binme  = 0                   ! =1: Mesh must be read in binary format
     nfiel      = 0                   ! Number of fields
     kfl_elcoh  = 0                   ! Assume cohesive elements
     !
     ! Local variables
     !
     kfl_icgns  = 0                   ! New Alya format for types of elements
     kfl_ifmat  = 0                   ! Materials were not assigned
     ktype      = 0                   ! Element # of nodes is not calculated
     imast      = 0                   ! Master has not been read
     izone      = 0                   ! Zones have not been allocated
     kfl_elino  = 0                   ! Eliminate lost nodes
     !
     ! Memory allocation
     !
     call memgeo( 1_ip)               ! Mesh arrays
     call memgeo(49_ip)               ! Zones 
     call memgeo(53_ip)               ! Periodicity
     !
     ! Read options and arrays
     ! 
     call ecoute('REAGEO')
     do while( words(1) /= 'GEOME' )
        call ecoute('REAGEO')
     end do
     if( exists('AXISY') ) kfl_naxis = 1
     if( exists('SPHER') ) kfl_spher = 1
     if( exists('CHECK') ) kfl_chege = 1
     if( exists('GID  ') ) kfl_gidsk = 1
     if( exists('CGNST') ) kfl_icgns = 1
     if( exists('ELIMI') ) kfl_elino = 1
     !
     ! Binary file: read/write
     !
     if(exists('BINAR').or.exists('READB')) then
        call geobin(2_ip)
        do while(words(1)/='ENDGE')
           call ecoute('reageo')
        end do
        return
     end if
     if(exists('OUTPU').or.exists('WRITE')) kfl_binar=1
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Mesh definition
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> GEOMETRY
     !
     do while(words(1)/='ENDGE')
        call ecoute('reageo')

        if( words(1) == 'BINAR' ) then
           !
           ! Read geometry from binary file
           !
           call geobin(2_ip)

        else if( words(1) == 'NODES' ) then
           !
           ! LTYPE: Element types
           !
           ktype=nelem
           call livinf(27_ip,' ',0_ip)
           do ielem=1,nelem
              read(nunit,*,err=1) dummi,knode
              call fintyp(ndime,knode,ielty)
              lexis(ielty)=1
              ltype(ielem)=ielty
           end do
           call ecoute('reageo')
           if(words(1)/='ENDNO')&
                call runend('REAGEO: ERROR READING NODES PER ELEMENT, POSSIBLY MISMATCH IN NUMBER OF NODES IN DOM.DAT')
           call mescek(2_ip)

        else if( words(1) == 'TYPES' ) then 
           !
           ! ADOC[1]> TYPES_OF_ELEMENTS [, ALL= char]
           ! ADOC[1]>   int int                                                                     $ Element, type number
           ! ADOC[1]>   ...
           ! ADOC[1]> END_TYPES_OF_ELEMENTS
           ! ADOC[d]> TYPES_OF_ELEMENTS:
           ! ADOC[d]> This field contains the element types. At each line, the first figure is the element number and the second
           ! ADOC[d]> one the element type. If all the elements are the same (say TET04, the following option can be added to the header: 
           ! ADOC[d]> TYPES OF ELEMENTS, ALL=TET04 and then the list should be empty.
           ! ADOC[d]> The correspondance between element type and element number is the following:
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> 1D elements: </li>
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> BAR02 =    2 </li> 
           ! ADOC[d]> <li> BAR03 =    3 </li> 
           ! ADOC[d]> <li> BAR04 =    4 </li> 
           ! ADOC[d]> </ul>
           ! ADOC[d]> <li> 2D elements: </li>
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> TRI03 =   10 </li> 
           ! ADOC[d]> <li> TRI06 =   11 </li> 
           ! ADOC[d]> <li> QUA04 =   12 </li> 
           ! ADOC[d]> <li> QUA08 =   13 </li> 
           ! ADOC[d]> <li> QUA09 =   14 </li> 
           ! ADOC[d]> <li> QUA16 =   15 </li> 
           ! ADOC[d]> </ul>
           ! ADOC[d]> <li> 3D elements: </li>
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> TET04 =   30 </li> 
           ! ADOC[d]> <li> TET10 =   31 </li> 
           ! ADOC[d]> <li> PYR05 =   32 </li> 
           ! ADOC[d]> <li> PYR14 =   33 </li> 
           ! ADOC[d]> <li> PEN06 =   34 </li> 
           ! ADOC[d]> <li> PEN15 =   35 </li> 
           ! ADOC[d]> <li> PEN18 =   36 </li> 
           ! ADOC[d]> <li> HEX08 =   37 </li> 
           ! ADOC[d]> <li> HEX20 =   38 </li> 
           ! ADOC[d]> <li> HEX27 =   39 </li> 
           ! ADOC[d]> <li> HEX64 =   40 </li> 
           ! ADOC[d]> </ul>
           ! ADOC[d]> <li> 3D 2D-elements: </li>
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> SHELL =   50 </li> 
           ! ADOC[d]> </ul>
           ! ADOC[d]> </ul>
           !
           call reatyp(kfl_icgns,nelem,ktype,ltype,lexis)
           call mescek(2_ip)

        else if( words(1) == 'ELEME' ) then
           !
           ! ADOC[1]> ELEMENTS 
           ! ADOC[1]>   int int int ...                                                             $ Element, node1, node2, node3 ...
           ! ADOC[1]>   ...
           ! ADOC[1]> END_ELEMENTS 
           ! ADOC[d]> ELEMENTS:
           ! ADOC[d]> This field contains the element connectivity. The first figure is the element number,
           ! ADOC[d]> followed by the list of it nodes.
           !
           call livinf(28_ip,' ',0_ip)
           if( ktype == 0 ) then
              call ecoute('reageo')
              do while( words(1) /= 'ENDEL' ) 
                 ielem = int(param(1_ip))
                 if( ielem < 0 .or. ielem > nelem ) &
                      call runend('REAGEO: WRONG NUMBER OF ELEMENT '&
                      //adjustl(trim(intost(ielem))))
                 knode = nnpar-1
                 call fintyp(ndime,knode,ielty)
                 ltype(ielem) = ielty
                 do inode = 1,nnode(ielty)
                    lnods(inode,ielem) = int(param(inode+1))
                 end do
                 call ecoute('reageo')
              end do
           else
              do ielem = 1,nelem
                 ielty = ltype(ielem)
                 read(nunit,*,err=2) dummi,(lnods(inode,ielem),inode=1,nnode(ielty))
              end do
              call ecoute('reageo')
           end if
           if( words(1) /= 'ENDEL' )&
                call runend('REAGEO: WRONG ELEMENT FIELD')
           call mescek(3_ip)

        else if( words(1) == 'CHARA' ) then
           !
           ! ADOC[1]> CHARACTERISTICS [,ELEMENTS/BOUNDARIES/NODES]
           ! ADOC[1]>   int int                                                                     $ Element/Boundary/Nodes, characteristic number
           ! ADOC[1]>   ...
           ! ADOC[1]> END_CHARACTERISTICS
           ! ADOC[d]> CHARACTERISTICS:
           ! ADOC[d]> This field contains the element or boundary charcteristics. The first figure is the element/boundary number followed by
           ! ADOC[d]> the element/boundary characteristic=0,1,2 for finite element/boundary/node, extension element/boundary/node (Chimera), 
           ! ADOC[d]> hole element/node (Chimera, Embedded mesh).
           !
           if( exists('BOUND') ) then
              call ecoute('reageo')
              do while( words(1) /= 'ENDCH' ) 
                 iboun        = int(param(1))
                 lboch(iboun) = int(param(2))
                 call ecoute('reageo')
              end do
           else if( exists('NODES') ) then
              call ecoute('reageo')
              do while( words(1) /= 'ENDCH' ) 
                 ipoin        = int(param(1))
                 lnoch(ipoin) = int(param(2))
                 call ecoute('reageo')
              end do
           else
              call ecoute('reageo')
              do while( words(1) /= 'ENDCH' ) 
                 ielem        = int(param(1))
                 lelch(ielem) = int(param(2))
                 if (lelch(ielem) == ELCOH) then  !cohesive elementh
                     kfl_elcoh = 1
                 end if
                 call ecoute('reageo')
              end do
           end if

        else if( words(1) == 'BOUND' .and. kfl_autbo /= 1 ) then
           !
           ! ADOC[1]> BOUNDARIES [, ELEMENT]
           ! ADOC[1]>   int int... [int]                                                            $ Boundary, node1, node2, node3 [element number]
           ! ADOC[1]>   ...
           ! ADOC[1]> END_BOUNDARIES
           ! ADOC[d]> BOUNDARIES:
           ! ADOC[d]> This field contains the boundary connectivity. The first figure is the boundary number followed by its nodes. 
           ! ADOC[d]> If option ELEMENT is present in the header, the last parameter should be the element to which the boundary belongs. 
           ! ADOC[d]> It should be given if possible to avoid Alya compute it.
           !
           if(exists('ELEME')) kfl_bouel=1
           if(exists('DONTR')) kfl_dontr=1
           call ecoute('reageo')
           if( kfl_dontr == 1 ) then
              do while( words(1) /= 'ENDBO' )
                 call ecoute('reageo')
              end do
           else if( kfl_bouel == 0 ) then
              call livinf(29_ip,' ',0_ip)
              do while( words(1) /= 'ENDBO' )
                 iboun = int(param(1))
                 if( iboun < 0 .or. iboun > nboun ) &
                      call runend('REAGEO: WRONG NUMBER OF BOUNDARIES')
                 knode = nnpar-1
                 call fintyp(ndimb,knode,iblty)
                 ltypb(iboun) = iblty
                 do inodb = 1,nnode(iblty)
                    lnodb(inodb,iboun) = int(param(inodb+1))
                 end do
                 call ecoute('reageo')
              end do
              call mescek(4_ip)
              call mescek(6_ip)
           else
              call livinf(30_ip,' ',0_ip)
              do while( words(1) /= 'ENDBO' )
                 iboun = int(param(1))
                 if( iboun < 0 .or. iboun > nboun ) &
                      call runend('REAGEO: WRONG NUMBER OF BOUNDARIES')
                 knode = nnpar-2
                 call fintyp(ndimb,knode,iblty)
                 ltypb(iboun) = iblty
                 do inodb = 1,nnode(iblty)
                    lnodb(inodb,iboun) = int(param(inodb+1))
                 end do
                 lboel(nnode(iblty)+1,iboun) = int(param(nnpar))              
                 call ecoute('reageo')
              end do
              call mescek(4_ip)
              call mescek(6_ip)
              call mescek(5_ip)
           end if

        else if( words(1) == 'SUBDO' ) then
           !
           ! ADOC[1]> SUBDOMAIN, [DEFAULT= int]
           ! ADOC[1]>   int int                                                                     $ Element, subdomain
           ! ADOC[1]>   ...
           ! ADOC[1]> END_SUBDOMAIN
           ! ADOC[d]> SUBDOMAIN:
           ! ADOC[d]> List of element subdomain used for the HERMESH method.
           !
           isubd = 1
           if( exists('DEFAU') ) isubd = getint('DEFAU',1_ip,'#DEFAULT SUBDOMAIN')
           do ielem = 1,nelem
              lesub(ielem) = isubd
           end do
           call ecoute('reageo')
           do while( words(1) /= 'ENDSU' )
              ielem = int(param(1_ip))
              lesub(ielem) = int(param(2))
              call ecoute('reageo')
           end do

        else if( words(1) == 'COORD' ) then 
           !
           ! ADOC[1]> COORDINATES [NOT_SORTED]
           ! ADOC[1]>   int real real real      $ id_node, coor_x, coor_y, coor_z
           ! ADOC[1]>   ...
           ! ADOC[1]> END_COORDINATES
           ! ADOC[d]> COORDINATES:
           ! ADOC[d]> This field contains the coordinates of the nodes. The first figure is the node number 
           ! ADOC[d]> followed by its coordinates.
           ! ADOC[d]> "id_node" is not taken into account, unless the option NOT_SORTED is present, which means
           ! ADOC[d]> that nodes are not sorted by id_node.
           !
           call livinf(33_ip,' ',0_ip)
           if (words(2) == 'NOTSO') then
              do ipoin=1,npoin
                 read(nunit,*,err=3) jpoin,(coord(idime,jpoin),idime=1,ndime)
              end do
           else
              do ipoin=1,npoin
                 read(nunit,*,err=3) dummi,(coord(idime,ipoin),idime=1,ndime)
              end do
           end if
           call ecoute('reageo')
           if(words(1)/='ENDCO')&
                call runend('REAGEO: WRONG COORDINATE FIELD')

        else if( words(1) == 'SKEWS' .and. nskew > 0 ) then 
           !
           ! SKCOS: Skew systems
           !
           if( kfl_gidsk == 0 ) then
              iskew = 0
              do while( words(1) /= 'ENDSK' )
                 if( ( nnwor /= 0 ) .or. ( nnpar /= ndime*ndime+1 ) )&
                      call runend('REAGEO: WRONG CARD READING SKEW-SYSTEMS')
                 iskew = iskew + 1
                 jskew = int(param(1_ip))
                 do idime = 1,ndime
                    do jdime = 1,ndime
                       skcos(idime,jdime,jskew) = param(1+(idime-1)*ndime+jdime)
                    end do
                 end do
                 call ecoute('reageo')              
              end do
           else
              iskew = 0
              do while( words(1) /= 'ENDSK' )
                 if( ( nnwor /= 0 ) .or. ( nnpar /= 10 ) )&
                      call runend('REAGEO: WRONG CARD READING SKEW-SYSTEMS')
                 iskew = iskew + 1
                 jskew = int(param(1_ip))
                 if( ndime == 2 ) then
                    skcos(1,1,jskew)= param(3_ip)  ! nx
                    skcos(2,1,jskew)= param(6_ip)  ! ny
                    skcos(1,2,jskew)= param(2_ip)  ! tx
                    skcos(2,2,jskew)= param(5_ip)  ! ty
                 else                           
                    skcos(1,1,jskew)= param(4_ip)  ! nx
                    skcos(2,1,jskew)= param(7_ip)  ! ny
                    skcos(3,1,jskew)= param(10_ip) ! nz
                    skcos(1,2,jskew)= param(2_ip)  ! tx
                    skcos(2,2,jskew)= param(5_ip)  ! ty
                    skcos(3,2,jskew)= param(8_ip)  ! tz
                    skcos(1,3,jskew)= param(3_ip)  ! sx
                    skcos(2,3,jskew)= param(6_ip)  ! sy
                    skcos(3,3,jskew)= param(9_ip)  ! sz
                 end if
                 call ecoute('reageo')              
              end do
           end if
           if(iskew/=nskew) call runend('REAGEO: WRONG NUMBER OF SKEW SYSTEMS')

        else if( words(1) == 'PERIO' .and. nperi > 0 ) then
           !
           ! ADOC[1]> PERIODIC_NODES
           ! ADOC[1]>   int int                                                                     $ Master, slave
           ! ADOC[1]>   ...
           ! ADOC[1]> END_PERIODIC_NODES
           ! ADOC[d]> PERIODIC:
           ! ADOC[d]> This field contains the list of master (first figure) and slave (second figure) periodic nodes.
           !
           call ecoute('reageo')
           iperi = 0
           do while( words(1) /= 'ENDPE' )
              if( nnpar > 0 ) then
                 iperi = iperi + 1
                 if( nnpar > mperi) mperi = nnpar
                 if( iperi > nperi) call runend('REAGEO: WRONG PERIODICITY FIELD')
                 lperi(1,iperi) = int(param(1),ip)
                 lperi(2,iperi) = int(param(2),ip)
              end if
              call ecoute('reageo')
           end do
           if( iperi /= nperi ) call runend('WRONG PERIODICITY FIELD')
  
        else if( words(1) == 'MATER' .and. words(2) /= 'OFF  ' ) then
           !
           ! ADOC[1]> MATERIALS, NUMBER= int, [DEFAULT= int]
           ! ADOC[1]>   int int                                                                     $ Element, material number
           ! ADOC[1]>   ...
           ! ADOC[1]> END_MATERIALS
           ! ADOC[d]> MATERIALS:
           ! ADOC[d]> This field defines elemental materials. The first figure is the element and the second the material mumber. 
           ! ADOC[d]> The header option NUMBER= int is the total number of materials. The header option DEFAULT= imate is the default 
           ! ADOC[d]> material assigned to elements when they do not appear in the list.
           !
           nmatf = getint('FLUID',1_ip,'#FLUID MATERIAL')
           imate = 0
           if( exists('DEFAU') ) imate = getint('DEFAU',1_ip,'#DEFAULT MATERIAL')
           if( exists('NUMBE') ) nmate = getint('NUMBE',1_ip,'#NUMBER OF MATERIALS')
           if( nmate == 0 ) nmate = 1
           call ecoute('reageo')
           if( words(1) /= 'ENDMA' .or. nmate > 1 ) then
              call memgeo(34_ip)
              kfl_ifmat = 1
              if( imate > 0 ) then
                 do ielem = 1,nelem
                    lmate(ielem) = imate
                 end do
              end if
              do while( words(1) /= 'ENDMA' )
                 ielem = int(param(1_ip))
                 lmate(ielem) = int(param(2))
                 call ecoute('reageo')
              end do
           end if
        !else if( words(1) == 'HANGI'.and.nhang>0) then
        !   !
        !   ! LHANG: Hanging nodes
        !   !                 
        !   ihang=0
        !   call ecoute('reageo')
        !   do while(words(1)/='ENDHA')
        !      ihang=ihang+1
        !      lhang(0,ihang)=nnpar
        !      if(nnpar>2*mnodb)&
        !           call runend('REAGEO: WRONG HANGING NODE FIELD FOR NODE '&
        !           //intost(int(param(1),ip)))              
        !      do ipara=1,nnpar
        !         lhang(ipara,ihang)=int(param(ipara))
        !      end do
        !      call ecoute('reageo')
        !   end do

        else if( words(1) == 'FIELD' ) then
           !
           ! ADOC[1]> FIELDS, NUMBER= int
           ! ADOC[2]> FIELD= int, DIMENSION= int, NODE/ELEMENT/BOUNDARY, DEFAULT=real, EXCLUDE_NEGATIVE_VALUES , STEPS= int 
           ! ADOC[2]>   int real...                                                              $ Element/node/boundary, value1, value2...
           ! ADOC[2]> END_FIELD 
           ! ADOC[1]> END_FIELDS
           ! ADOC[d]> The field FIELDS deserves special attention. It declares fields of 1 or more dimensions defined on nodes or elements. 
           ! ADOC[d]> These fields can be used in the modules to assign nodeal/elemental values to some quantities. 
           ! ADOC[d]> For example, in the case of SOLIDZ module, a field can represent some material directions.
           ! ADOC[d]> FIELDS, NUMBER= int, int is the total number of fields to be declared.
           ! ADOC[d]> FIELD= ifiel, DIMENSION= ndimf, NODE/ELEMENT/BOUNDARY. Field ifiel is now going to be defined. The values are ndimf dimensional and the 
           ! ADOC[d]> fields is defined on node, element or boundary. The list of values has the form: ii, value(1,ii)...value(ndimf,ii) where 
           ! ADOC[d]> ii= ipoin for values on nodes and ii= ielem for values on elements.
           ! ADOC[d]> Now it can also deal with several steps. If there are more than 1 step they are stored they are stored :
           ! ADOC[d]> value(1 of step 1,ii)...value(ndimf of step 1,ii),value(1 of step 2,ii)...value(ndimf of step 2,ii),............ 
           ! ADOC[d]> DEFAULT: Impose a default value of real. EXCLUDE_NEGATIVE_VALUES can be useful when using the mesh multiplciation
           ! ADOC[d]> not to take these values nto account when interpolating
           !
           ! FIELDS: NFIEL, KFL_FIELD, XFIEL
           !
           call livinf(100_ip,' ',0_ip)
           nfiel = getint('NUMBE',1_ip,'#NUMBER OF FIELDS')
           call memgeo(28_ip)
           call ecoute('reageo')
           do while( words(1) /= 'ENDFI' )
              if( words(1) == 'FIELD' ) then
                 ifiel = getint('FIELD',1_ip,'#FIELD NUMBER')
                 if( ifiel < 0 .or. ifiel > nfiel ) call runend('REAGEO: WRONG FIELD NUMBER')
                 if( exists('DIMEN') ) then
                    kfl_field(1,ifiel) = getint('DIMEN',1_ip,'#FIELD DIMENSION')
                 else
                    call runend('REAGEO: FIELD DIMENSION IS MISSING')
                 end if
                 if( exists('STEPS') ) then
                    kfl_field(4,ifiel) = getint('STEPS',1_ip,'#FIELD DIMENSION')
                 else
                    kfl_field(4,ifiel) = 1_ip
                 end if
                 if( exists('ELEME') ) then
                    kfl_field(2,ifiel) = NELEM_TYPE
                    nlimi              = nelem
                 else if( exists('NODES') .or. exists('NODE ') ) then
                    kfl_field(2,ifiel) = NPOIN_TYPE
                    nlimi              = npoin
                 else if( exists('BOUND') ) then
                    kfl_field(2,ifiel) = NBOUN_TYPE
                    nlimi              = nboun
                 else
                    call runend('REAGEO: DEFINE IF FIELD IS DEFINED ON NODES OR ELEMENTS')
                 end if
                 if( exists('DEFAU') ) then
                    kfl_defau = 1
                    dummr     = getrea('DEFAU',1.0_rp,'#Default value')
                 else
                    kfl_defau = 0
                 end if
                 !
                 ! negative value excluded?                 
                 !
                 if( exists('EXCLU') ) then
                    kfl_field(3,ifiel) = 1
                 else
                    kfl_field(3,ifiel) = 0
                 end if
                 !
                 ! Allocate memory
                 !
                 igene = ifiel
                 call memgeo(29_ip)
                 !
                 ! Put default value
                 !
                 if( kfl_defau == 1 ) then
                    do ipoin = 1,nlimi
                       do idime = 1,kfl_field(1,ifiel)*kfl_field(4,ifiel)
                          xfiel(ifiel) % a(idime,ipoin) = dummr
                       end do
                    end do
                 end if
                 !
                 ! Read field
                 !
                 call ecoute('reageo')
                 if (kfl_field(4,ifiel) == 1_ip) then ! 1 Step - default behaviour
                    do while( words(1) /= 'ENDFI' )
                       ipoin = int(param(1))
                       do idime = 1,kfl_field(1,ifiel)
                          xfiel(ifiel) % a(idime,ipoin) = param(idime+1)
                       end do
                       call ecoute('reageo')
                    end do
                 else
                    do while( words(1) /= 'ENDFI' )
                       ! If there is more than 1 step the syntax is
                       ! FIELD
                       !  STEP 1, TIME=0.0
                       !   7   3.5
                       !   9   4.0
                       !  END_STEP
                       !  STEP 2, TIME=3600.0
                       !   7   5.0
                       !   9   6.6
                       !  END_STEP
                       ! END_FIELD
                       if( words(1) == 'STEP ' ) then
                          jstep = nint(param(1))
                          if (jstep > kfl_field(4,ifiel)) call runend('REABCS: jstep > kfl_field(4,ifiel)  - Check field steps')
                          time_field(ifiel) % a(jstep) = getrea('TIME ',0.0_rp,'#time')
                          call ecoute('reageo')
                          do while( words(1) /= 'ENDST' )
                             ipoin = int(param(1))
                             do idime = 1,kfl_field(1,ifiel)
                                xfiel(ifiel) % a(idime+(jstep-1)*kfl_field(1,ifiel),ipoin) = param(idime+1)
                             end do
                             call ecoute('reageo')
                          end do
                       end if
                       call ecoute('reageo')
                    end do
                 end if    ! 1 or more steps

              end if   !words(1) == 'FIELD'
              call ecoute('reageo')
           end do

        else if( words(1) == 'ZONES' ) then
           !
           ! ADOC[1]> ZONES
           ! ADOC[2]>   ZONE, NUMBER= int [ALL_ELEMENTS]
           ! ADOC[2]>     int                                                                    $ Element  
           ! ADOC[2]>     ...
           ! ADOC[2]>   END_ZONE  
           ! ADOC[1]> END_ZONES
           ! ADOC[d]> ZONES: 
           ! ADOC[d]> For each zone (which number is given by NUMBER= int ), give the list of elements.
           ! ADOC[d]> If all the elements are in a zone, put ALL_ELEMENTS option to avoid giving the full list.
           !
           call ecoute('reageo')
           do while( words(1) /= 'ENDZO' )
              if( words(1) == 'ZONE ' ) then
                 izone = getint('NUMBE',1_ip,'#ZONE NUMBER')
                 if( izone < 1 .or. izone > nzone ) call runend('REAGEO: WRONG ZONE NUMBER')
                 igene = izone
                 if( exists('ALLEL') ) then
                    nelez(izone) = nelem 
                    call memgeo(51_ip)
                    do ielem = 1,nelem
                       lelez(izone) % l(ielem) = ielem
                    end do
                    call ecoute('reageo')                    
                    do while( words(1) /= 'ENDZO' )
                       call ecoute('reageo')
                    end do
                 else
                    call memgen(1_ip,nelem,0_ip)
                    call ecoute('reageo')
                    ipara = 0
                    dummi = 0
                    do while( words(1) /= 'ENDZO' )
                       if( dummi /= int(param(1)) ) then
                          ipara = ipara + 1
                           gisca(ipara) = int(param(1))
                        end if
                       dummi = int(param(1))
                       call ecoute('reageo')
                    end do
                    nelez(izone) = ipara
                    call memgeo(51_ip)       
                    do ipara = 1,nelez(izone)                      
                       lelez(izone) % l(ipara) = gisca(ipara)
                    end do
                    call memgen(3_ip,nelem,0_ip)
                 end if
              end if
              call ecoute('reageo')
           end do

        else if( words(1) == 'MESHI' ) then
           !
           ! Meshing
           ! 
           call meshin(2_ip)             ! Read mesh parameters
           call meshin(3_ip)             ! Construct Cartesian mesh

        else if( words(1) == 'GROUP' ) then
           !
           ! ADOC[1]> GROUPS= int                                                                   $ Number of groups (used for deflation)
           ! ADOC[d]> GROUPS:
           ! ADOC[d]> This option is the number of groups to be constructed for deflation based solvers.
           ! ADOC[d]> - GROUPS = -1 ... The number of groups is computed in an automatic way.
           ! ADOC[d]> - GROUPS = -2 ... In parallel, one group per subdomain.
           ! ADOC[d]> - GROUPS >  0 ... Number of groups specified by the user (highly recommanded).
           !
           ngrou_dom = getint('GROUP',0_ip,'#NUMBER OF GROUPS')

        end if

     end do
     !
     ! ADOC[0]> END_GEOMETRY
     !
     !
     ! LNNOD
     !
     do ielem = 1,nelem
        pelty = ltype(ielem)
        lnnod(ielem) = nnode(pelty)
     end do
     !
     ! Eliminate lost nodes (nodes without elements)
     !     
     if( kfl_elino == 1 ) then
        call memory_alloca(memor_dom,'LELNO','reageo',lelno,npoin)   
        kpoin = 0
        do ielem = 1,nelem
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              if( lelno(ipoin) == 0 ) then
                 kpoin = kpoin + 1
                 lelno(ipoin) = kpoin
              end if
           end do 
        end do
        if( kpoin /= npoin ) then
           call outfor(2_ip,0_ip,'SOME NODES ARE GOING TO BE ELIMINATED AND MESH RENUMBERED')
           npoin =  kpoin
           gisca => lelno
           call memgeo(58_ip)
           nullify(gisca)
        end if
        call memory_deallo(memor_dom,'LELNO','reageo',lelno)   
     end if
     !
     ! If materials were not assigned
     !
     if( kfl_ifmat == 0 ) then
        call memgeo(34_ip)
        nmate = 1
        do ielem = 1,nelem
           lmate(ielem) = 1
        end do
     end if
     !
     ! Geometric translation
     !   
     if( trans(1) /= 0.0_rp .or. trans(2) /= 0.0_rp .or. trans(3) /= 0.0_rp ) then   
        do ipoin = 1,npoin
           do idime = 1,ndime
              coord(idime,ipoin) = trans(idime)+coord(idime,ipoin)
           end do
        end do
     end if
     !
     ! Geometric scale factor
     !      
     if( scale(1) /= 1.0_rp .or. scale(2) /= 1.0_rp .or. scale(3) /= 1.0_rp ) then
        do ipoin=1,npoin
           do idime=1,ndime
              coord(idime,ipoin) = scale(idime)*coord(idime,ipoin)
              !if (idime == 3) print *, coord (idime,ipoin)
           end do
        end do
     end if
     !
     ! Do not scale particles
     !
     if( npoib > 0 ) then
        do iimbo = 1,nimbo
           do ipoin = 1,imbou(iimbo)%npoib
              do idime=1,ndime
                 imbou(iimbo)%cooib(idime,ipoin) = scale(idime)*imbou(iimbo)%cooib(idime,ipoin)
              end do
           end do
        end do
     end if
     !
     ! LBOEL if the elements connected to the boundaries are unknown (kfl_bouel=0)
     !
     call lbouel()
     !
     ! Only one zone
     !
     if( nzone == 1 .and. izone == 0 ) then
        izone = 1
        igene = izone
        nelez(izone) = nelem
        call memgeo(51_ip)
        do ielem = 1,nelem
           lelez(izone) % l(ielem) = ielem
        end do
     end if
     !
     ! Check zones
     !
     call memgen(1_ip,nelem,0_ip)
     do izone = 1,nzone 
        do kelem = 1,nelez(izone)
           ielem = lelez(izone) % l(kelem)
           gisca(ielem) = 1
        end do
     end do
     dummi = 0
     do ielem = 1,nelem
        dummi = dummi + gisca(ielem)
     end do
     if( dummi /= nelem ) then
        do ielem = 1,nelem
           if( gisca(ielem) == 0 ) then
              chnod = intost(ielem)
              call livinf(10000_ip,'ELEMENT NUMBER '//adjustl(trim(chnod))// ' HAS NO ZONE',0_ip)     
           end if
        end do
     end if
     call memgen(3_ip,nelem,0_ip)
     !
     ! Output binary file
     !
     if (kfl_binar == 1 ) call geobin(1_ip)

  end if

  return

1 call runend('REAGEO: WRONG NUMBER OF NODES FOR ELEMENT '//trim(intost(ielem)))
2 call runend('REAGEO: WRONG ELEMENT CONNECTIVITY FOR ELEMENT '//trim(intost(ielem)))
3 call runend('REAGEO: WRONG COORDINATES FOR NODE '//trim(intost(ipoin)))

end subroutine reageo
