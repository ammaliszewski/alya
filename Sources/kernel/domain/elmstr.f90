subroutine elmstr(welty,ielty)
  !-----------------------------------------------------------------------
  !****f* Domain/elmstr
  ! NAME
  !    elmstr
  ! DESCRIPTION
  !    This routine converts the element name into the 
  !    corresponding integer
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_elmtyp
  implicit none
  character(5), intent(in)  :: welty
  integer(ip),  intent(out) :: ielty

  if(welty=='BAR02') then
     ielty=BAR02
  else if(welty=='BAR03')  then
     ielty=BAR03   
  else if(welty=='BAR04')  then
     ielty=BAR04   
  else if(welty=='TRI03')  then
     ielty=TRI03   
  else if(welty=='TRI06')  then
     ielty=TRI06   
  else if(welty=='QUA04')  then
     ielty=QUA04   
  else if(welty=='QUA08')  then
     ielty=QUA08   
  else if(welty=='QUA09')  then
     ielty=QUA09   
  else if(welty=='QUA16') then
     ielty=QUA16  
  else if(welty=='TET04')  then
     ielty=TET04  
  else if(welty=='TET10') then
     ielty=TET10 
  else if(welty=='PYR05')  then
     ielty=PYR05   
  else if(welty=='PYR14') then
     ielty=PYR14 
  else if(welty=='PEN06')  then
     ielty=PEN06   
  else if(welty=='PEN15') then
     ielty=PEN15  
  else if(welty=='PEN18') then
     ielty=PEN18  
  else if(welty=='HEX08')  then
     ielty=HEX08   
  else if(welty=='HEX20') then
     ielty=HEX20  
  else if(welty=='HEX27') then
     ielty=HEX27  
  else if(welty=='SHELL') then
     ielty=SHELL
  else if(welty=='BAR3D') then
     ielty=BAR3D
  else
     ielty=0
  end if

end subroutine elmstr
