!-----------------------------------------------------------------------
!> @addtogroup DomainInput
!> @{
!> @file    readim.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Read mesh dimensions
!> @details Read mesh dimensions:
!>          \verbatim
!>          - NPOIN .......... Number of nodes
!>          - NELEM .......... Number of elements
!>          - NDIME .......... Number of space dimensions (1,2,3)
!>          - NZONE .......... Number of zones
!>          - NBOUN .......... Number of boundary elements
!>          - NPERI....... ... Number of master/slave periodic nodes
!>          - NHANG....... ... Number of hanging nodes
!>          - LEXIS(NELTY) ... List of existing element types
!>          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine readim()
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use def_elmtyp
  use mod_memchk
  implicit none
  integer(ip) :: ielty,ipara,inode,ndife,ndime_aux


  if( ISEQUEN .or. ( IMASTER .and. kfl_ptask /= 2 ) ) then

     call livinf(0_ip,'READ MESH DATA',zero)
     lispa     = 0
     lisda     = lun_pdata_dom  ! Reading file
     lisre     = lun_outpu_dom  ! Writing file
     !
     ! Initializations
     !
     kfl_autbo =  0   ! Automatic boundaries off
     kfl_divid =  0   ! Divide elements into tetra

     npoin     = -1   ! Obligatory
     nelem     = -1   ! Obligatory
#ifdef NDIMEPAR

#else
  ndime     = -1                 ! Obligatory
#endif

     nboun     = -1   ! Optional
     nperi     =  0   ! Optional
     nskew     = -1   ! Optional
     nboib     =  0   ! Optional
     npoib     =  0   ! Optional
     nhang     =  0   ! Optional
     nzone     =  1   ! Optional
     nsubd     =  1   ! Optional
     kfl_camsh =  0   ! Optional
     !
     ! Reach the section
     !
     call ecoute('readim')
     do while( words(1) /= 'DIMEN' )
        call ecoute('readim')
     end do
     if( words(2) == 'CARTE' ) call meshin(1_ip) 

     call ecoute('readim')
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Dimension of the problem
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> DIMENSIONS
     !
     do while( words(1) /= 'ENDDI')

        if( words(1) == 'NODAL' ) then
           !
           ! ADOC[1]> NODAL_POINTS   = int                                                          $ Number of nodes
           !
           npoin = getint('NODAL',0_ip,'*NUMBER OF NODAL POINTS')

        else if( words(1) == 'ELEME' ) then
           !
           ! ADOC[1]> ELEMENTS       = int                                                          $ Number of elements
           !
           nelem = getint('ELEME',0_ip,'*NUMBER OF ELEMENTS')
           nelwh = nelem      

        else if( words(1) == 'SPACE' ) then
           !
           ! ADOC[1]> SPACE          = int                                                          $ Space dimension (1,2,3)
           !
#ifdef NDIMEPAR
           ndime_aux = getint('SPACE',2_ip,'*SPACE DIMENSION')
           if (ndime_aux/=ndime) call runend('READIM: running with ifdef NDIMEPAR and ndime_aux/=ndime')
#else
           ndime = getint('SPACE',2_ip,'*SPACE DIMENSION')
#endif            

        else if( words(1) == 'ZONES' ) then
           !
           ! ADOC[1]> ZONES          = int                                                          $ Number of zones (1 by default)
           !
           nzone = getint('ZONES',1_ip,'*NUMBER OF ZONES')

        else if( words(1) == 'SUBDO' ) then
           !
           ! ADOC[1]> SUBDOMAINS     = int                                                          $ Number of subdomains (1 by default)
           !
           nsubd = getint('SUBDO',1_ip,'*NUMBER OF SUBDOMAINS')

        else if( words(1) == 'BOUND' ) then
           !
           ! ADOC[1]> BOUNDARIES     = AUTOMATIC/int                                                $ Number of boundary elements
           ! ADOC[d]> <b>BOUNDARIES</b>:
           ! ADOC[d]> This field is not compulsory,
           ! ADOC[d]> Put AUTOMATIC to generate the boundary automatically.
           ! ADOC[d]> In this case no reference to the boundary is possible and
           ! ADOC[d]> it is only useful for postprocess purpose.
           !
           if(words(2) == 'AUTOM' ) then
              kfl_autbo = 1
           else
              nboun = getint('BOUND',0_ip,'*NUMBER OF BOUNDARY ELEMENTS')
           end if

        else if( words(1) == 'PERIO' ) then
           !
           ! ADOC[1]> PERIODIC_NODES = int                                                          $ Number of periodic nodes
           ! ADOC[d]> <b>PERIODIC_NODES</b>:
           ! ADOC[d]> This number if the number of periodic slave nodes. If a master has various slaves,
           ! ADOC[d]> all the slave nodes must be taken into account.
           !
           nperi = getint('PERIO',0_ip,'*NUMBER OF PERIODIC NODES')

        else if( words(1) == 'SKEWS' ) then
           nskew = getint('SKEWS',0_ip,'*NUMBER OF SKEW SYSTEMS')

        else if( words(1) == 'DIVID' ) then
           if( words(2) == 'ON   ' .or. words(2) == 'YES  ' ) kfl_divid = 1

        else if( words(1) == 'TYPES' ) then
           !
           ! ADOC[1]> TYPES          = char, char...                                                $ List of volume elements in the mesh
           ! ADOC[d]> <b>TYPES</b>:
           ! ADOC[d]> Elements available:
           ! ADOC[d]> <ul>
           ! ADOC[d]  <li>
           ! ADOC[d]> 1D: BAR02,BAR03,BAR04
           ! ADOC[d]  </li>
           ! ADOC[d]  <li>
           ! ADOC[d]> 2D: TRI03,TRI06,QUA04,QUA08,QUA09,QUA16
           ! ADOC[d]  </li>
           ! ADOC[d]  <li>
           ! ADOC[d]> 3D: TET04,TET10,PYR05,PYR14,PEN06,PEN15,PEN18,HEX08,HEX20,HEX27,HEX64
           ! ADOC[d]  </li>
           ! ADOC[d]> </ul>
           !
           if( nnpar == 0 ) then
              ipara = 2
              do while( trim(words(ipara)) /= '' )
                 call elmstr(words(ipara),ielty)
                 if( ielty >= 2 .and. ielty <= nelty ) lexis(ielty) = 1
                 ipara = ipara + 1
              end do
           else
              do ipara = 1,nnpar
                 ielty = int(param(ipara))
                 if( ielty >= 2 .and. ielty <= nelty ) lexis(ielty) = 1
              end do
           end if

        else if( words(1) == 'NODES' ) then
           do ipara = 1,nnpar
              inode = int(param(ipara))
              if( ndime ==0 ) &
                   call runend('READIM: SPACE DIMENSION SHOULD BE DEFINED FIRST')
              if( inode >= 1 ) then
                 call fintyp(ndime,inode,ielty)
                 lexis(ielty) = 1
              end if
           end do

        else if( words(1) == 'HANGI' ) then
           !
           ! ADOC[1]> HANGING_NODES  = int                                                          $ Number of hanging nodes
           !
           nhang = getint('HANGI',0_ip,'*NUMBER OF HANGING NODES')

        end if
        call ecoute('readim')
     end do

     !-----------------------------------------------------------------------
     ! ADOC[0]> END_DIMENSIONS
     !-----------------------------------------------------------------------

     !
     ! Check errors
     !
     if( kfl_camsh == 0 ) then
        if( npoin == -1 ) call runend('READIM: WRONG NUMBER OF NODES')
        if( nelem == -1 ) call runend('READIM: WRONG NUMBER OF ELEMENTS')
        if( ndime == -1 ) call runend('READIM: WRONG NUMBER OF SPACE DIMENSIONS')
     end if

     nboun   = max( nboun , 0_ip )
     nskew   = max( nskew , 0_ip )
     npoin_2 = npoin
     nelem_2 = nelem
     nboun_2 = nboun
     !
     ! Divide mesh
     !
     if( kfl_divid == 1 ) then
        ndife = 0
        do ielty = 1,nelty
           ndife = ndife + lexis(ielty)
        end do
        if( ndife /= 1 ) call runend('READIM: CANNOT DIVIDE MESH')
        if( lexis(HEX08) == 0 ) call runend('READIM: CAN ONLY DIVIDE HEXAHEDRA')
        lexis(TET04) = 1
     end if

  end if


end subroutine readim
