!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    domvar.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Define some domain variables
!> @details Define some domain variables
!>          \verbatim
!>          LPOIZ(IZONE) % L(KPOIN) ....... =  0 if IPOIN does not belong to zone IZONE
!>                                  ....... /= 0 if IPOIN does
!>          LNOCH(NPOIN) .................. Node characterstics, compute from the element
!>                                          characteristics.
!>                                          = NOFEM ... Node is a normal node
!>                                          = NOHOL ... Node is a hole node
!>          LMAST(NPOIN) .................. List of masters
!>                                          LMAST(JPOIN) = IPOIN if JPOIN is a slave and IPOIN master
!>                                          = 0 if JPOIN is not a slave
!>          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine domvar(itask)
  use def_parame
  use def_master
  use def_domain
  use def_elmtyp
  use def_kermod,         only       :  kfl_rotation_axe,ndivi
  use def_kermod,         only       :  rotation_angle
  use mod_memory,         only       :  memory_alloca
  use mod_memory,         only       :  memory_deallo
  use mod_communications, only       :  PAR_INTERFACE_NODE_EXCHANGE
  use mod_communications, only       :  PAR_GHOST_BOUNDARY_EXCHANGE
  use mod_communications, only       :  PAR_SUM
  use mod_communications, only       :  PAR_MAX
  use mod_elmgeo,         only       :  elmgeo_element_type_initialization
  implicit none

  integer(ip),            intent(in) :: itask
  integer(ip)                        :: ielem,pelty,iboun,pblty,pface,jpoin,idime
  integer(ip)                        :: izone,inode,kelem,kzone,ipoin,knode,jdime
  integer(ip)                        :: pnodb,kpoin,ihole_aux,isubd,inodb,iperi
  integer(ip)                        :: pnode,iface,inodf,jface,jnodf,jnodb,cont
  character(15)                      :: chnod
  integer(ip),            pointer    :: list_boundary_nodes(:)
  logical(lg)                        :: change_order,select_face
  real(rp)                           :: rotation_matrix(3,3),theta,xx(3)

  nullify(list_boundary_nodes)
  select case ( itask )

  case ( 0_ip ) 

     if( INOTMASTER .and. kfl_rotation_axe /= 0 ) then
        !
        ! Mesh rotation
        !
        rotation_matrix = 0.0_rp
        theta           = rotation_angle/180.0_rp*pi
        if( ndime == 2 ) then
           rotation_matrix(1,1) =  cos(theta)     
           rotation_matrix(1,2) = -sin(theta)     
           rotation_matrix(2,1) =  sin(theta)     
           rotation_matrix(2,2) =  cos(theta)     
        else if( kfl_rotation_axe == 1 ) then
           rotation_matrix(1,1) =  1.0_rp       !    +-                       -+
           rotation_matrix(2,2) =  cos(theta)   !    |  1        0        0    |
           rotation_matrix(2,3) = -sin(theta)   !    |  0      cos(t)  -sin(t) |
           rotation_matrix(3,2) =  sin(theta)   !    |  0      sin(t)   cos(t) |                      
           rotation_matrix(3,3) =  cos(theta)   !    +-                       -+
        else if( kfl_rotation_axe == 2 ) then  
           rotation_matrix(1,1) =  cos(theta)   !    +-                       -+ 
           rotation_matrix(1,3) =  sin(theta)   !    |  cos(t)   0      sin(t) |
           rotation_matrix(2,2) =  1.0_rp       !    |    0      1        0    |
           rotation_matrix(3,1) = -sin(theta)   !    | -sin(t)   0      cos(t) |
           rotation_matrix(3,3) =  cos(theta)   !    +-                       -+
        else if( kfl_rotation_axe == 3 ) then  
           rotation_matrix(1,1) =  cos(theta)   !    +-                       -+ 
           rotation_matrix(1,2) = -sin(theta)   !    |  cos(t)  -sin(t)   0    |
           rotation_matrix(2,1) =  sin(theta)   !    |  sin(t)   cos(t)   0    |
           rotation_matrix(2,2) =  cos(theta)   !    |    0       0       1    |
           rotation_matrix(3,3) =  1.0_rp       !    +-                       -+
        end if

        do ipoin = 1,npoin
           xx(1:ndime)          = coord(1:ndime,ipoin)
           coord(1:ndime,ipoin) = 0.0_rp
           do idime = 1,ndime
              do jdime = 1,ndime
                 coord(idime,ipoin) = coord(idime,ipoin) + rotation_matrix(idime,jdime) * xx(jdime)
              end do
           end do
        end do

     end if

  case ( 1_ip ) 

     if( ISLAVE ) then
        !
        ! Faces (IMASTER and ISEQUEN already did it in cderda) 
        !
        do pelty = iesta_dom,iesto_dom  
           pface = nface(pelty)
           if( lexis(pelty) == 1 .and. pface /= 0 ) then
              call memory_alloca(memor_dom,'LTYPF','domvar',ltypf(pelty) % l,pface)
              call memory_alloca(memor_dom,'NNODF','domvar',nnodf(pelty) % l,pface)
              call memory_alloca(memor_dom,'LFACE','domvar',lface(pelty) % l,mnodb,pface)
              call domfac(&
                   pelty,mnodb,pface,lface(pelty)%l,&
                   ltypf(pelty)%l,nnodf(pelty)%l)
           end if
        end do
        !
        ! Element data base
        !
        call elmgeo_element_type_initialization(&
             iesta_dom,iesto_dom,nface,lexis,needg,leedg,&
             ldime,lorde,ltopo,nnode(1:),nnodf,ltypf,lface)
     end if
     !
     ! Order boundaries
     !
     call order_boundaries()

  case ( 2_ip )

     if( INOTMASTER ) then
        !
        ! Initialize
        !
        do pelty = 1,nelty
           lnuty(pelty) = 0
        end do
        !
        ! Number of elements per type
        !
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           if( pelty > 0 ) lnuty(pelty) = lnuty(pelty) + 1
        end do
        !
        ! Number of boundaries per type
        !
        do iboun = 1,nboun
           pblty = abs(ltypb(iboun))
           lnuty(pblty) = lnuty(pblty) + 1
        end do
        !
        ! Check if high order element exist
        !   
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           if( pelty > 0 ) then
              if( lorde(pelty) > 1 ) kfl_horde = 1
           end if
        end do
        !
        ! Contact/hole/cohesive elements
        !
        do ielem = 1,nelem
           if( lelch(ielem) == ELCNT ) then
              ltype(ielem) = -abs(ltype(ielem))
           else if( lelch(ielem) == ELHOL ) then
              ltype(ielem) = -abs(ltype(ielem))
           else if( lelch(ielem) == ELCOH ) then
              ltype(ielem) = -abs(ltype(ielem))
           end if
        end do

     end if

     call PAR_MAX(kfl_horde,'IN MY CODE')

     if( ISEQUEN ) then
        npoi1        =  npoin
        npoi2        =  0
        npoi3        = -1
        npoi4        =  0
        npoin_par(1) = npoin
        nelem_par(1) = nelem
        nboun_par(1) = nboun
        npoin_total  = npoin
        nelem_total  = nelem
        nboun_total  = nboun
     end if
     !
     ! Copy of original mesh size
     !
     nelem_2 = nelem 
     nboun_2 = nboun 
     npoin_2 = npoin 

  case ( 3_ip )
     !
     ! Number of nodes per boundary
     !
     if( INOTMASTER ) then
        call memgeo(61_ip)
        do iboun = 1,nboun
           pblty = abs(ltypb(iboun))
           lnnob(iboun) = nnode(pblty) 
        end do
        meshe(ndivi) % lnnob => lnnob
     end if  
     !
     ! Zones: LPOIZ(:)
     !
     if( INOTMASTER ) then
        call memgeo(50_ip)
        npoiz(0) = npoin
        if( nzone == 1 ) then
           npoiz(1) = npoin
           igene    = 1
           call memgeo(52_ip)
           do ipoin = 1,npoin
              lpoiz(1) % l(ipoin) = ipoin
           end do
           lpoiz(0) % l => lpoiz(1) % l
        else
           call memgen(1_ip,npoin,0_ip)
           do izone = 1,nzone
              do kelem = 1,nelez(izone)
                 ielem = lelez(izone) % l(kelem)
                 do inode = 1,lnnod(ielem)
                    gisca(lnods(inode,ielem)) = 1
                 end do
              end do
              call PAR_INTERFACE_NODE_EXCHANGE(gisca,'MAX','IN MY CODE','SYNCHRONOUS')
              npoiz(izone) = 0
              do ipoin = 1,npoin
                 npoiz(izone) = npoiz(izone) + gisca(ipoin)
              end do
              igene = izone
              call memgeo(52_ip)
              kpoin = 0
              do ipoin = 1,npoin
                 if( gisca(ipoin) == 1 ) then
                    kpoin                   = kpoin + 1
                    lpoiz(izone) % l(kpoin) = ipoin
                    gisca(ipoin)            = 0
                 end if
              end do
           end do
           igene = 0
           call memgeo(52_ip)
           do ipoin = 1,npoin
              lpoiz(0) % l(ipoin) = ipoin
           end do
           call memgen(3_ip,npoin,0_ip)
        end if
     end if
     !
     ! Zones: NBOUZ(:), LBOUZ(:) % L (:)
     !
     if( INOTMASTER ) then
        call memgeo(55_ip)                   ! NBOUZ(:)
        if( nzone == 1 ) then
           nbouz(1) = nboun
           igene    = 1
           call memgeo(56_ip)                ! LBOUZ(:) % L (:)
           do iboun = 1,nboun
              lbouz(1) % l(iboun) = iboun
           end do
        else
           call memgen(1_ip,nzone,nelem)
           do izone = 1,nzone
              do kelem = 1,nelez(izone)
                 ielem = lelez(izone) % l(kelem)
                 givec(izone,ielem) = 1
              end do
              nbouz(izone) = 0
           end do
           do iboun = 1,nboun
              pblty = abs(ltypb(iboun))
              pnodb = nnode(pblty)
              ielem = lboel((pnodb+1),iboun)          
              do izone = 1,nzone
                 if( givec(izone,ielem) == 1 ) then
                    nbouz(izone) = nbouz(izone) + 1
                 end if
              end do
           end do
           do izone = 1,nzone
              igene = izone
              call memgeo(56_ip)             ! LBOUZ(:) % L (:)
              nbouz(izone) = 0
           end do
           do iboun = 1,nboun
              pblty = abs(ltypb(iboun))
              pnodb = nnode(pblty)
              ielem = lboel((pnodb+1),iboun)  
              do izone = 1,nzone
                 if( givec(izone,ielem) == 1 ) then
                    nbouz(izone) = nbouz(izone) + 1
                    lbouz(izone) % l (nbouz(izone)) = iboun
                 end if
              end do
           end do
           call memgen(3_ip,nzone,nelem)
        end if
     end if
     !
     ! MECNT: Check if contact exist
     ! NECNT: # of contact elements
     !
     mecnt = 0
     if( INOTMASTER ) then
        call memgen(1_ip,npoin,0_ip)
        necnt = 0
        nncnt = 0
        do ielem = 1,nelem
           if( lelch(ielem) == ELCNT ) then
              necnt = necnt + 1
              do inode = 1,lnnod(ielem)
                 ipoin = lnods(inode,ielem)
                 gisca(ipoin) = 1
              end do
           end if
        end do
        do ipoin = 1,npoin
           nncnt = nncnt + gisca(ipoin)
        end do
        nncnt = nncnt / 2
        if( necnt > 0 ) call memgeo(40_ip)
        mecnt = necnt
        call memgen(3_ip,npoin,0_ip)
     end if
     call PAR_SUM(mecnt,'IN MY CODE')
     !call parari('SUM',0_ip,1_ip,mecnt)
     if( INOTMASTER .and. mecnt > 0 ) then
        necnt = 0
        do ielem = 1,nelem
           if( lelch(ielem) == ELCNT ) then
              necnt = necnt + 1
           end if
        end do
     end if
     !
     ! Define LNOCH according to LELCH and detect holes automatically
     !
     if( INOTMASTER ) then
        ihole_aux= 0
        call memgen(1_ip,npoin_2,0_ip)
        do ielem = 1,nelem
           if( lelch(ielem) /= ELHOL ) then
              do inode = 1,nnode(abs(ltype(ielem)))
                 ipoin = lnods(inode,ielem)
                 gisca(ipoin) = 1
              end do
           else
              ihole_aux= ihole_aux+1
           end if
        end do
        call PAR_INTERFACE_NODE_EXCHANGE(gisca,'MAX','IN MY CODE')
        !call parari('SLX',NPOIN_TYPE,npoin,gisca)
        do ipoin = 1,npoin
           if( gisca(ipoin) == 0 ) then
              lnoch(ipoin) = NOHOL
              if (ihole_aux == 0) then
                 chnod = intost(ipoin)
                 call livinf(10000_ip, &
                      'NODE NUMBER ( '//adjustl(trim(chnod)) &
                      // ' HAS NO CONNECTIVITY DEFINED)',0_ip)      
              end if
           end if
        end do
        call memgen(3_ip,npoin_2,0_ip)
     end if
     !
     ! I am in zone, subdomain or not?
     !
     !call memgeo(59_ip)
     !call in_zone_in_subd()
     !if( IMASTER ) then
     !    I_AM_IN_ZONE(1:nzone) = .true.
     !    I_AM_IN_SUBD(1:nsubd) = .true.
     ! else
     !   do izone = 1,nzone
     !      if( nelez(izone) > 0 ) I_AM_IN_ZONE(izone) = .true.
     !   end do
     !   do ielem = 1,nelem
     !      isubd = max(1_ip,lesub(ielem))
     !      I_AM_IN_SUBD(isubd) = .true.
     !   end do
     !end if
     !
     ! List of boundary nodes
     ! NBONO .......... Number of boundary nodes
     ! LBONO(NBONO) ... List of boudanry nodes
     !
     if( INOTMASTER ) then
        call memory_alloca(memor_dom,'LIST_BOUNDARY_NODES','memgeo',list_boundary_nodes,npoin)
        nbono = 0
        do ipoin = 1,npoin
           list_boundary_nodes(ipoin) = 0
        end do
        do iboun = 1,nboun
           do inodb = 1,nnode(abs(ltypb(iboun)))
              ipoin = lnodb(inodb,iboun)
              if( list_boundary_nodes(ipoin) == 0 ) then
                 nbono = nbono + 1
                 list_boundary_nodes(ipoin) = 1
              end if
           end do
        end do
        call PAR_INTERFACE_NODE_EXCHANGE(list_boundary_nodes,'MAX','IN MY CODE')
        nbono = 0
        do ipoin = 1,npoin
           nbono = nbono + list_boundary_nodes(ipoin)
        end do
        call memgeo(60_ip)
        nbono = 0
        do ipoin = 1,npoin
           if( list_boundary_nodes(ipoin) /= 0 ) then
              nbono = nbono + 1
              lbono(nbono) = ipoin
           end if
        end do
        call memory_deallo(memor_dom,'LIST_BOUNDARY_NODES','memgeo',list_boundary_nodes)
     end if
     !
     ! List of masters
     ! LMAST(JPOIN) = IPOIN, IPOIN is the master of slave JPOIN
     !
     if( INOTMASTER .and. nperi > 0 ) then
        call memgeo(62_ip)
        do iperi = 1,nperi
           ipoin = lperi(1,iperi)
           jpoin = lperi(2,iperi)
           if ( ipoin > 0 .and. jpoin > 0 ) then 
              lmast(jpoin) = ipoin
           end if
       end do
    end if

  end select

end subroutine domvar


