!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file     cresla.f90
!> @author   Guillaume Houzeaux
!> @date     18/09/2012
!> @brief    Change graph to account for periodicity
!> @keywords periodicity,graph
!> @details  Changes the graphs to account for periodicity by adding
!>           the neighbors of the slave to the master.
!>           LPERI(1,1:NPERI) = list of masters
!>           LPERI(2,1:NPERI) = list of slaves
!>           A master can have up to 4 slaves in 2D and 8 slaves in 3D
!>
!>           \verbatim
!>
!>                  +------------+----------------+
!>                  |      CPU1  |                |
!>                  o---o        |            x---x
!>                  |   |        |            |   |
!>           Master M---o--------+   CPU3     x---S Slave
!>                  |   |        |            |   | M Master
!>                  o---o        |            x---x
!>                  |      CPU2  |                |
!>                  +------------+----------------+
!>
!>           \endverbatim
!>
!>           o are neighbors of Master M in respective CPUs \n
!>           x are neighbors of Slave  S in respective CPUs \n
!>           By construction, the Master is present in all the CPUs where 
!>           the slave is. Therefore, the row of the Slave can always
!>           be copied to that of the Master.
!>
!> @} 
!-----------------------------------------------------------------------

subroutine cresla()
  use def_kintyp, only : ip,i1p
  use def_domain, only : npoin
  use def_domain, only : r_dom
  use def_domain, only : r_sol
  use def_domain, only : c_dom
  use def_domain, only : c_sol
  use def_domain, only : nzdom
  use def_domain, only : nzsol
  use def_domain, only : nzsym
  use def_domain, only : lperi
  use def_domain, only : nperi
  use def_domain, only : memor_dom
  use def_master, only : INOTMASTER
  use mod_memory, only : memory_alloca
  use mod_memory, only : memory_deallo
  use mod_memory, only : memory_resize
  implicit none
  integer(ip)          :: ipoin,izdom,jpoin,jzdom,kpoin,ifoun
  integer(ip)          :: iperi,kzdom,madde,lsize,isize
  integer(ip), pointer :: lcdom_size(:) 
  integer(ip), pointer :: list_of_masters(:) 
  type(i1p),   pointer :: lcdom(:) 

  nullify(lcdom)
  nullify(lcdom_size)
  nullify(list_of_masters)

  if( nperi > 0 ) then

     call livinf(0_ip,'RECOMPUTE GRAPH FOR PERIODICITY',0_ip)

     if( INOTMASTER ) then
        !
        ! Allocate memory for LCDOM: temporary graph
        !
        call memory_alloca(memor_dom,'LCDOM'     ,'cresla',lcdom,     npoin)
        call memory_alloca(memor_dom,'LCDOM_SIZE','cresla',lcdom_size,npoin)
        do ipoin = 1,npoin
           lsize = r_dom(ipoin+1)-r_dom(ipoin)
           lcdom_size(ipoin) = lsize
           call memory_alloca(memor_dom,'LCDOM(IPOIN) % L','cresla',lcdom(ipoin) % l,lsize)
           lsize = 0
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              lsize = lsize + 1
              lcdom(ipoin) % l(lsize) = c_dom(izdom)
           end do
        end do
        !
        ! LIST_OF_MASTERS(JPOIN) = IPOIN : if JPOIN is slave of IPOIN
        !                        = 0     : others
        !
        call memory_alloca(memor_dom,'LIST_OF_MASTERS','cresla',list_of_masters,npoin)
        do iperi = 1,nperi
           ipoin = lperi(1,iperi)  
           jpoin = lperi(2,iperi)
           list_of_masters(jpoin) = ipoin
        end do
        !
        ! Put all slaves' columns into master's column
        ! KPOIN is neither master nor slave
        ! JPOIN is slave
        ! IPOIN is JPOIN's master
        !
        do kpoin = 1,npoin
           if( list_of_masters(kpoin) == 0 ) then
              madde = 0
              do kzdom = r_dom(kpoin),r_dom(kpoin+1)-1
                 jpoin = c_dom(kzdom)
                 if( list_of_masters(jpoin) > 0 ) then
                    ipoin = list_of_masters(jpoin)
                    ifoun = 0
                    lsize = lcdom_size(kpoin)
                    izdom1: do isize = 1,lsize
                       if( lcdom(kpoin) % l(isize) == ipoin )  then
                          ifoun = 1 
                          exit izdom1
                       end if
                    end do izdom1
                    if( ifoun == 0 ) then
                       call memory_resize(memor_dom,'LCDOM(KPOIN) % L','cresla',lcdom(kpoin) % l,lsize+1_ip)
                       lcdom_size(kpoin)         = lsize+1_ip
                       lcdom(kpoin) % l(lsize+1) = ipoin
                    end if
                 end if
              end do
           end if
        end do
        !
        ! Put all slaves' rows into master's row
        ! Put slave row to zero
        ! JPOIN:  slave
        ! IPOIN:  master
        ! Slave:  coef. JZDOM for JPOIN-KPOIN
        ! Master: coef. IZOMD for IPOIN-KPOIN
        !
        do jpoin = 1,npoin
           if( list_of_masters(jpoin) > 0 ) then
              madde = 0
              ipoin = list_of_masters(jpoin)
              do jzdom = r_dom(jpoin),r_dom(jpoin+1)-1
                 kpoin = c_dom(jzdom)     
                 if( list_of_masters(kpoin) > 0 ) kpoin = list_of_masters(kpoin)
                 ifoun = 0
                 lsize = lcdom_size(ipoin)
                 izdom2: do isize = 1,lsize
                    if( lcdom(ipoin) % l(isize) == kpoin )  then
                       ifoun = 1 
                       exit izdom2
                    end if
                 end do izdom2
                 if( ifoun == 0 ) then
                    call memory_resize(memor_dom,'LCDOM(IPOIN) % L','cresla',lcdom(ipoin) % l,lsize+1_ip)
                    lcdom_size(ipoin)         = lsize+1_ip
                    lcdom(ipoin) % l(lsize+1) = kpoin
                 end if
              end do
           end if
        end do
        !
        ! Reallocate C_DOM and recompute NZDOM, R_DOM
        ! Reorder graph C_DOM 
        !
        nzdom = 0
        do ipoin = 1,npoin
           nzdom = nzdom + lcdom_size(ipoin)
        end do
        call memory_alloca(memor_dom,'C_DOM','cresla',c_dom,nzdom+1_ip,'REALLOCATE')

        r_dom(1) = 1
        do ipoin = 2,npoin+1  
           r_dom(ipoin) = r_dom(ipoin-1) + lcdom_size(ipoin-1)
        end do
        do ipoin = 1,npoin
           isize = 0
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              isize = isize + 1
              c_dom(izdom) = lcdom(ipoin) % l(isize)
           end do
           if( isize > 0 ) call heapsorti1(2_ip,isize,c_dom(r_dom(ipoin)))
        end do
        !
        ! Deallocate memory 
        !
        call memory_deallo(memor_dom,'LIST_OF_MASTERS','cresla',list_of_masters)
        call memory_deallo(memor_dom,'LCDOM'          ,'cresla',lcdom)
        call memory_deallo(memor_dom,'LCDOM_SIZE'     ,'cresla',lcdom_size)   

     end if

  end if

  !-------------------------------------------------------------------
  !
  ! Solver dimensions and graphs
  ! 
  !-------------------------------------------------------------------

  c_sol => c_dom
  r_sol => r_dom
  nzsol =  nzdom
  nzsym =  (nzsol-npoin)/2 + npoin

end subroutine cresla
