subroutine elecsr()
  !-----------------------------------------------------------------------
  !****f* Domain/elecsr
  ! NAME
  !    elecsr
  ! DESCRIPTION
  !    This routine creates the arry from element to csr.
  !    Given ELMAT(INODE,JNODE,IELEM) => AMATR(IZDOM)
  !    with IZDOM = LZDOM(INODE,JNODE,IELEM)
  !    What to do when matrix is not referenced from position 1? 
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master
  use mod_memchk
  implicit none

  if( INOTMASTER .and. kfl_lotme == 1 ) then
     
     !call memgeo(54_ip)
     !do ielem = 1,nelem
     !   pnode = lnnod(ielem)
     !   do inode = 1,pnode
     !      ipoin = lnods(inode,ielem)
     !      do jnode = 1,pnode
     !         jpoin = lnods(jnode,ielem)
     !         izsol = r_sol(ipoin)
     !         jcolu = c_sol(izsol)
     !         do while( jcolu /= jpoin )
     !            izsol = izsol + 1
     !            jcolu = c_sol(izsol)
     !         end do
     !         lezdo(inode,jnode,ielem) = izsol
     !      end do
     !   end do
     !end do
     !do iboun = 1,nboun
     !   pnodb = abs(nnode(ltypb(iboun)))
     !   do inodb = 1,pnodb
     !      ipoin = lnodb(inodb,iboun)
     !      do jnodb = 1,pnodb
     !         jpoin = lnodb(jnodb,iboun)
     !         izsol = r_sol(ipoin)
     !         jcolu = c_sol(izsol)
     !         do while( jcolu /= jpoin )
     !            izsol = izsol + 1
     !            jcolu = c_sol(izsol)
     !         end do
     !         lbzdo(inodb,jnodb,iboun) = izsol
     !      end do
     !   end do
     !end do

  end if

end subroutine elecsr
