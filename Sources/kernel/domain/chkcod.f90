subroutine chkcod()
  !-----------------------------------------------------------------------
  !****f* Domain/chkcod
  ! NAME
  !    chkcod
  ! DESCRIPTION
  !    Check number of possible combinations of codes
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_domain
  use def_inpout
  use mod_iofile
  implicit none
  integer(ip)          :: ipoin,icono,icode,kcomb,icomb,ncomb,mcod1
  integer(ip)          :: icomb_par,ncomb_par
  integer(ip), pointer :: lcomb(:,:) 
  logical(lg)          :: lcodt(-mcodb-1:mcodb+1)

  if( kfl_icodn == 0 ) return

  nullify(lcomb)

  if( INOTMASTER ) then
     !
     ! NCOMB: Number of possible combinations
     !
     mcod1 = mcodb + 1
     do icode = -mcodb,mcodb
        lcodt(icode) = .false.
     end do
     do ipoin = 1,npoin
        do icono = 1,mcono
           icode = kfl_codno(icono,ipoin)
           lcodt(icode) = .true.
        end do
     end do
     ncomb = 0
     do icode = -mcodb,mcodb
        if( lcodt(icode) ) ncomb = ncomb + 1 
     end do

     ncomb =    ncomb &
          &  +  ncomb * ( ncomb - 1 ) / 2 &
          &  +  ncomb * ( ncomb - 1 ) * ( ncomb - 2 ) / 3

     allocate( lcomb(mcono,ncomb) )
     do icomb = 1,ncomb
        do icono = 1,mcono
           lcomb(icono,icomb) = mcodb + 1
        end do
     end do
     !
     ! Fill in combination table
     !
     if( ncomb > 0 ) then

        kcomb = 0

        do ipoin = 1,npoin

           if( kfl_codno(1,ipoin) /= mcod1 ) then
              icomb = 1
              icode = lcomb(1,1) 
              do while( icode /= mcodb + 1 )
                 if(    kfl_codno(1,ipoin) == lcomb(1,icomb) .and. &
                      & kfl_codno(2,ipoin) == lcomb(2,icomb) .and. &
                      & kfl_codno(3,ipoin) == lcomb(3,icomb) ) then
                    icomb = -1
                    icode = mcodb + 1
                 else
                    icomb = icomb + 1
                    icode = lcomb(1,icomb)
                 end if
              end do

              if( icomb /= -1 ) then
                 kcomb = kcomb + 1
                 lcomb(1,kcomb) = kfl_codno(1,ipoin) 
                 lcomb(2,kcomb) = kfl_codno(2,ipoin) 
                 lcomb(3,kcomb) = kfl_codno(3,ipoin)
              end if

           end if

        end do
        ncomb = kcomb
     end if

     do icomb = 1,ncomb
        if( lcomb(2,icomb) == mcod1 ) lcomb(2,icomb) = -mcod1 
        if( lcomb(3,icomb) == mcod1 ) lcomb(3,icomb) = -mcod1 
     end do
     if( ncomb > 0 ) call sorti3(ncomb,lcomb)
     kfl_desti_par = 0
     call parari('SND',0_ip,1_ip,ncomb)
     if( ncomb > 0 ) call parari('SND',0_ip,ncomb*mcono,lcomb)
  end if
  !
  ! Master receives slave combinations
  !
  if( IMASTER ) then
     allocate( lcomb(mcono,1000) )
     do icomb = 1,1000
        lcomb(1,icomb) = 0
     end do
     ncomb = 0
     do kfl_desti_par = 1,npart
        call parari('RCV',0_ip,1_ip,ncomb_par)
        allocate( givec(mcono,ncomb_par) )
        if( ncomb_par > 0 ) then
           call parari('RCV',0_ip,ncomb_par*mcono,givec)
           do icomb_par = 1,ncomb_par
              icomb = 1
              icode = lcomb(1,icomb)
              do while( icode > 0 ) 
                 if(          lcomb(1,icomb) == givec(1,icomb_par) &
                      & .and. lcomb(2,icomb) == givec(2,icomb_par) &
                      & .and. lcomb(3,icomb) == givec(3,icomb_par) ) then
                    icode = -1
                 else
                    icomb = icomb + 1
                    icode = lcomb(1,icomb)
                 end if
              end do
              if( icode /= -1 ) then
                 ncomb = ncomb + 1
                 lcomb(1,ncomb) = givec(1,icomb_par)
                 lcomb(2,ncomb) = givec(2,icomb_par)
                 lcomb(3,ncomb) = givec(3,icomb_par)
              end if
           end do
           deallocate( givec )
        end if
     end do
  end if

  if( INOTSLAVE ) then
     call sorti3(ncomb,lcomb)
     call outfor(42_ip,lun_outpu,' ')
     do icomb = 1,ncomb
        ioutp(1) = lcomb(1,icomb)
        ioutp(2) = lcomb(2,icomb)
        ioutp(3) = lcomb(3,icomb)
        call outfor(43_ip,lun_outpu,' ')
     end do
  end if

end subroutine chkcod
