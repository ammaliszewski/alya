subroutine memose(order)
  !-----------------------------------------------------------------------
  !****f* Domain/memose
  ! NAME
  !    memose
  ! DESCRIPTION
  !    Allocate memory for set arrays
  ! USED BY
  !    reaset
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_inpout
  use mod_memchk
  use mod_iofile
  implicit none
  integer(ip), intent(in) :: order
  integer(4)              :: istat

  select case (order) 

  case (1_ip)
     if( neset > 0 ) then
        allocate(leset(nelem),stat=istat)
        call memchk(zero,istat,memor_dom,'LESET','memose',leset)
     end if

  case (2_ip)
     if( nbset > 0 ) then
        allocate(lbset(nboun),stat=istat)
        call memchk(zero,istat,memor_dom,'LBSET','memose',lbset)
     end if

  case (-2_ip)
     if( nbset > 0 ) then
        call memchk(two,istat,memor_dom,'LBSET','memose',lbset)
        deallocate(lbset,stat=istat)
        if( istat/=0) call memerr(two,'LBSET','memose',0_ip)
     end if

  case (3_ip)
     if( nnset > 0 ) then
        allocate(lnsec(nnset),stat=istat)
        call memchk(zero,istat,memor_dom,'LNSEC','memose',lnsec)
     end if

  case (4_ip)
     if( nbset > 0 ) then
        allocate(lbsec(nbset),stat=istat)
        call memchk(zero,istat,memor_dom,'LBSEC','memose',lbsec)
     end if

  case (5_ip)
     if( neset > 0 ) then
        allocate(lesec(neset),stat=istat)
        call memchk(zero,istat,memor_dom,'LESEC','memose',lesec)
     end if

  case (-5_ip)
     if( neset > 0 ) then
        call memchk(two,istat,memor_dom,'LESEC','memose',lesec)
        deallocate(lesec,stat=istat)
        if( istat/=0) call memerr(two,'LESEC','memose',0_ip)
     end if

  case (6_ip)
     if( mwitn > 0 ) then
        allocate(shwit(mnode,mwitn),stat=istat)
        call memchk(zero,istat,memor_dom,'SHWIT','memose',shwit)
        allocate(dewit(ndime,mnode,mwitn),stat=istat)
        call memchk(zero,istat,memor_dom,'DEWIT','memose',dewit)
        allocate(lewit(mwitn),stat=istat)
        call memchk(zero,istat,memor_dom,'LEWIT','memose',lewit)
     end if

  case (7_ip)
     if( mwitn > 0 ) then
        allocate(cowit(3,mwitn),stat=istat)
        call memchk(zero,istat,memor_dom,'COWIT','memose',cowit)        
     end if

  case (10_ip)
     !
     ! Deallocate memory
     !
     if( neset > 0 ) then
        call memchk(two,istat,memor_dom,'LESET','memose',leset)
        deallocate(leset,stat=istat)
        if( istat/=0) call memerr(two,'LESET','memose',0_ip)
     end if
     if( nbset > 0 ) then
        call memchk(two,istat,memor_dom,'LBSET','memose',lbset)
        deallocate(lbset,stat=istat)
        if( istat/=0) call memerr(two,'LBSET','memose',0_ip)
     end if

  case (11_ip)
     !
     ! LNWIT
     !
     allocate(lnwit(nwitn),stat=istat)
     call memchk(zero,istat,memor_dom,'LNWIT','memose',lnwit)

  end select

end subroutine memose
