!-----------------------------------------------------------------------
!> @addtogroup AlgebraicSolvers
!> @{
!> @file    cgrpls.f90
!> @author  Guillaume Houzeaux
!> @brief   Groups information
!> @details Groups information
!> @} 
!-----------------------------------------------------------------------
subroutine outdef(itask)
  use def_kintyp
  use def_master
  use def_domain
  use def_solver
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ngrou,igrou,ipoin,lunit
  integer(ip), pointer    :: lgrou(:)

  if( itask == 1 ) then
     !
     ! Write number of nodes per group
     !
     ngrou = solve_sol(1) % ngrou
     allocate(lgrou(0:ngrou))
     do igrou = 0,ngrou
        lgrou(igrou) = 0
     end do
 
     if( INOTMASTER ) then
        do ipoin = 1,npoin
           igrou = solve_sol(1) % lgrou(ipoin)
           lgrou(igrou) = lgrou(igrou) + 1
        end do
     end if

     call parari('SUM',0_ip,ngrou+1_ip,lgrou(0:ngrou))

     if( IMASTER ) then
        do igrou = 0,ngrou
           write(solve_sol(1) % lun_solve,1) igrou,lgrou(igrou)
        end do
        call flush(solve_sol(1) % lun_solve)
     end if

     deallocate(lgrou)

  else
     !
     ! Postprocess sparse matrix
     !
     if( IMASTER ) then
        ngrou = solve_sol(1) % ngrou
        lunit = 100_ip
        call pspltm(&
             ngrou,ngrou,1_ip,0_ip,solve_sol(1) % ja,solve_sol(1) % ia,&
             solve_sol(1) % askyldef,&
             trim(title)//': '//namod(modul),0_ip,18.0_rp,'cm',&
             0_ip,0_ip,2_ip,lunit)
     end if

  end if

1 format('# GROUPS ',i7,i9)

end subroutine outdef
