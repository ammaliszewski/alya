!-----------------------------------------------------------------------
!> @addtogroup AlgebraicSolvers
!> @{
!> @file    cgrpls.f90
!> @author  Guillaume Houzeaux
!> @brief   Conjugate gradient solver
!> @details Deflated pipelined CG  
!>          \verbatim
!>
!>          Initial solution x_-1
!>          if( deflation ) then
!>            Factorize A'
!>            r_-1 = b - A x_-1
!>            x0   = x_-1 + ( W A'^-1 W^T ) w_0 ) r_-1
!>            u0'  = ( W A'^-1 W^T ) w_0
!>          else
!>            x0   = x_-1
!>            u0'  = 0
!>          end if
!>          r_0   = b - A x_0
!>          M u_0 = r_0
!>          w_0   = A u_0
!>          u_0   = u_0 - u0'
!>
!>          for i = 0,...
!>
!>            gamma_i = (r_i,u_i)
!>            delta   = (w_i,u_i)
!>            w'_i    = W^T w_i
!>            M m_i   = w_i
!>            n_i     = A m_i
!>            if( i > 0 ) then
!>              beta_i  = gamma_i / gamma_i-1
!>              alpha_i = gamma_i / ( delta - beta_i * gamma_i / alpha_i-1 )
!>            else
!>              beta_i  = 0
!>              alpha_i = gamma_i / delta
!>            end if
!>            if( deflation ) then
!>               A' d'_i = w'_i
!>               d_i     = W d'_i
!>            else
!>               d_i     = 0
!>            end if
!>            z_i = n_i + beta_i  * z_i-1 - A M^-1 A d_i
!>            q_i = m_i + beta_i  * q_i-1 - M^-1 A d_i
!>            s_i = w_i + beta_i  * s_i-1 - A d_i
!>            p_i = u_i + beta_i  * p_i-1 - d_i
!>            x_i = x_i + alpha_i * p_i
!>            r_i = r_i - alpha_i * s_i
!>            u_i = u_i - alpha_i * q_i
!>            w_i = w_i - alpha_i * z_i
!>
!>          end for
!>
!>          \endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine pipelined_CG( &
     nbnodes, nbvar, idprecon, maxiter, kfl_resid, &
     eps, an, pn, kfl_cvgso, lun_cvgso, kfl_solve, &
     lun_outso, ja, ia, bb, xx )
  use def_kintyp,         only :  ip,rp,lg
  use def_master,         only :  IMASTER,INOTMASTER,NPOIN_TYPE
  use def_solver,         only :  memit,resi1,resi2,iters,solve_sol
  use mod_memory,         only :  memory_alloca
  use mod_memory,         only :  memory_deallo 
  use mod_csrdir,         only :  CSR_LU_Factorization
  use mod_csrdir,         only :  CSR_LUsol
  use mod_solver,         only :  solver_scalar_product
  use mod_communications, only :  PAR_WAITALL
  use mod_communications, only :  PAR_SUM
  implicit none
  integer(ip), intent(in)      :: nbnodes
  integer(ip), intent(in)      :: nbvar
  integer(ip), intent(in)      :: idprecon
  integer(ip), intent(in)      :: maxiter
  integer(ip), intent(in)      :: kfl_resid
  integer(ip), intent(in)      :: kfl_cvgso
  integer(ip), intent(in)      :: lun_cvgso
  integer(ip), intent(in)      :: kfl_solve
  integer(ip), intent(in)      :: lun_outso
  real(rp),    intent(in)      :: eps
  real(rp),    intent(in)      :: an(nbvar,nbvar,*), pn(*)
  integer(ip), intent(in)      :: ja(*), ia(*)
  real(rp),    intent(in)      :: bb(*)
  real(rp),    intent(inout)   :: xx(nbvar*nbnodes)
  integer(ip)                  :: ii,nrows,ierr,npoin,ngrou
  integer(ip)                  :: igamma,idelta,nskyl,igrou,info
  real(rp)                     :: alpha, beta, stopcri, resid
  real(rp)                     :: invnb, newrho, dummr, gamma, delta
  real(rp)                     :: gamma_old, alpha_old, delta_0
  real(rp),    pointer         :: rr(:), pp(:), zz(:)
  real(rp),    pointer         :: ww(:), ss(:), uu(:), qq(:), dd(:)
  real(rp),    pointer         :: aa(:), maa(:), aamaa(:), w0(:), u0(:)
  real(rp),    pointer         :: mm(:), nn(:), invdiag(:)
  real(rp),    pointer         :: murhs(:),mu_gamma_delta(:),mu(:)
  integer(ip), pointer         :: iagro(:),jagro(:)
  !
  !
  ! r00  = b - A x00
  ! A'd0 = W^t r00
  ! x0   = x00 + W d0
  ! r0   = b - A x0
  ! M u0 = r0
  ! 
  ! w0   = A u0
  ! A'd  = W^t w0
  ! p0   = - W d + u0
  !
  ! s     = Ap
  ! gamma = (r,u)
  ! caca  = (p,s)
  ! alpha = gamma / caca
  !
  ! x   = x + alpha*p 
  ! r   = r - alpha*s
  ! M u = r
  !
  ! gamma = (r,u) 
  ! beta  = gamma / gamma_old
  ! A'd   = W^t A u
  ! 
  ! p     = u + beta*p - W d
  !
#ifdef EVENT
  call mpitrace_user_function(1)
#endif 
  !
  ! Skyline, CSR or dense format when using deflation
  !
  if( solve_sol(1) % ngrou > 0 ) then
     if( solve_sol(1) % kfl_defas == 0 ) then
        nskyl =  solve_sol(1) % nskyl
     else if( solve_sol(1) % kfl_defas == 1 ) then
        nskyl =  solve_sol(1) % nzgro * nbvar * nbvar
        iagro => solve_sol(1) % ia
        jagro => solve_sol(1) % ja
     else if( solve_sol(1) % kfl_defas == 2 ) then
        nskyl =  ngrou * ngrou
     end if
     igamma = nbvar * ngrou + 1
     idelta = nbvar * ngrou + 2
     ngrou  = solve_sol(1) % ngrou 
  else
     igamma = 1
     idelta = 2
     ngrou  = 0
  end if
  !
  ! Initialize variables
  !
  nullify(rr)
  nullify(pp)
  nullify(zz)
  nullify(dd)
  nullify(ww)
  nullify(ss)
  nullify(uu)
  nullify(qq)
  nullify(mm)
  nullify(nn)
  nullify(w0)
  nullify(u0)
  nullify(invdiag)
  nullify(mu_gamma_delta)

  nullify(aa)
  nullify(maa)
  nullify(aamaa)
  nullify(solve_sol(1) % askyldef)
  nullify(murhs)
  nullify(mu)

  if( IMASTER ) then
     nrows   = 1                ! Minimum memory for working arrays
     npoin   = 0                ! master does not perform any loop
  else
     nrows   = nbnodes * nbvar
     npoin   = nbnodes
  end if
  !
  ! Sequential and slaves: Working arrays
  !
  call memory_alloca(memit,'RR'            ,'pipelined_CG',rr            ,nrows)
  call memory_alloca(memit,'PP'            ,'pipelined_CG',pp            ,nrows)
  call memory_alloca(memit,'ZZ'            ,'pipelined_CG',zz            ,nrows)
  call memory_alloca(memit,'DD'            ,'pipelined_CG',dd            ,nrows)
  call memory_alloca(memit,'WW'            ,'pipelined_CG',ww            ,nrows)
  call memory_alloca(memit,'SS'            ,'pipelined_CG',ss            ,nrows)
  call memory_alloca(memit,'UU'            ,'pipelined_CG',uu            ,nrows)
  call memory_alloca(memit,'QQ'            ,'pipelined_CG',qq            ,nrows)
  call memory_alloca(memit,'MM'            ,'pipelined_CG',mm            ,nrows)
  call memory_alloca(memit,'NN'            ,'pipelined_CG',nn            ,nrows)
  call memory_alloca(memit,'W0'            ,'pipelined_CG',w0            ,nrows)
  call memory_alloca(memit,'U0'            ,'pipelined_CG',u0            ,nrows)
  call memory_alloca(memit,'INVDIAG'       ,'pipelined_CG',invdiag       ,nrows)
  call memory_alloca(memit,'MU_GAMMA_DELTA','pipelined_CG',mu_gamma_delta,ngrou*nbvar+2)

  call memory_alloca(memit,'AA'            ,'pipelined_CG',aa            ,nrows)
  call memory_alloca(memit,'MAA'           ,'pipelined_CG',maa           ,nrows)
  call memory_alloca(memit,'AAMAA'         ,'pipelined_CG',aamaa         ,nrows)
  call memory_alloca(memit,'MU'            ,'pipelined_CG',mu            ,ngrou*nbvar)

  if( IMASTER ) nrows = 0 ! Master does not perform any loop

  if( ngrou > 0 ) then

     !----------------------------------------------------------------------
     !
     ! Allocate memory for groups
     !
     !----------------------------------------------------------------------

     call memory_alloca(memit,'ASKYLDEF','deflcg',solve_sol(1) % askyldef,nskyl) 
     if( solve_sol(1) % kfl_defas /= 0 ) then
        call memory_alloca(memit,'MURHS','deflcg',murhs,nbvar*ngrou)
     end if
 
     !----------------------------------------------------------------------
     !
     ! Compute A'= W^T.A.W
     !
     !----------------------------------------------------------------------

     if( solve_sol(1) % kfl_symme == 1 ) then
        call matgro(ngrou,npoin,nskyl,nbvar,ia,ja,an,solve_sol(1) % askyldef)
     else
        if( solve_sol(1) % kfl_defas == 2 ) then 
           !
           ! Dense
           !
           call matgr3(ngrou,npoin,nskyl,nbvar,ia,ja,an,solve_sol(1) % askyldef)
        else
           !
           ! CSR and Skyline
           !
           call matgr2(ngrou,npoin,nskyl,nbvar,ia,ja,an,solve_sol(1) % askyldef)
        end if
     end if

     !----------------------------------------------------------------------
     !
     ! Factorize A'
     !
     !----------------------------------------------------------------------

     if( solve_sol(1) % kfl_defso == 0 .and. INOTMASTER .and. ngrou > 0 ) then
        if( solve_sol(1) % kfl_defas == 1 ) then
           !
           ! Inverse CSR matrix ASKYLDEF
           !
           nullify(solve_sol(1) % Ildef)
           nullify(solve_sol(1) % Jldef)
           nullify(solve_sol(1) % Lndef)
           nullify(solve_sol(1) % iUdef)
           nullify(solve_sol(1) % jUdef)
           nullify(solve_sol(1) % Undef)                                ! Permutation arrays
           nullify(solve_sol(1) % invpRdef)
           nullify(solve_sol(1) % invpCdef)
           call CSR_LU_Factorization(&                                  ! CSR LU Factorization  
                ngrou,nbvar,iagro,jagro,solve_sol(1) % askyldef,&
                solve_sol(1) % ILdef,solve_sol(1) % JLdef,&
                solve_sol(1) % LNdef,solve_sol(1) % IUdef,&
                solve_sol(1) % JUdef,solve_sol(1) % UNdef,info) 
           if( info /= 0 ) call runend('DEFLCG: SINGULAR MATRIX')

        else
           !
           ! Inverse skyline matrix ASKYL
           !
           call chofac(&
                nbvar*ngrou,solve_sol(1) % nskyl,&
                solve_sol(1) % iskyl,solve_sol(1) % askyldef,info)
           if( info /= 0 ) call runend('MATGRO: ERROR WHILE DOING CHOLESKY FACTORIZATION')
        end if

     end if 

     !----------------------------------------------------------------------
     !
     ! X_0: Compute pre-initial guess 
     !
     !----------------------------------------------------------------------

     if( solve_sol(1) % kfl_symme == 1 ) then
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )         ! A.x_{-1}
     else if( solve_sol(1) % kfl_schum == 1 ) then
        call bcsrax_schur( 1_ip, npoin, nbvar, solve_sol(1) % ndofn_A3 , &
             solve_sol(1) % A1, solve_sol(1) % A2, solve_sol(1) % invA3, &
             solve_sol(1) % A4, ja, ia, xx, rr )  
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )         ! A.x_{-1}
     end if
     do ii= 1, nrows
        rr(ii) = bb(ii) - rr(ii)                                      ! r_{-1} = b-A.x_{-1}
     end do

     call wtvect(npoin,ngrou,nbvar,mu_gamma_delta,rr)                 ! W^T.r_{-1}
     if( INOTMASTER ) then   
        if( solve_sol(1) % kfl_defas == 0 ) then
           call chosol(&
                solve_sol(1) % ngrou*nbvar,solve_sol(1) % nskyl,&
                solve_sol(1) % iskyl,1_ip,solve_sol(1) % askyldef,mu_gamma_delta,&
                solve_sol(1) % ngrou*nbvar,info)    
           if( info /= 0 ) &
                call runend('MATGRO: COULD NOT SOLVE INITIAL SYSTEM')
        else
           do igrou = 1,nbvar*ngrou
              murhs(igrou) = mu_gamma_delta(igrou)
           end do
           call CSR_LUsol(                                          &
                ngrou                   , nbvar                   , &
                solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
                solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
                solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
                solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
                murhs                   , mu_gamma_delta          )  
        end if
     end if
     call wvect(npoin,nbvar,mu_gamma_delta,rr)                        ! W.mu
     do ii = 1,nrows
        xx(ii) = xx(ii) + rr(ii)                                      ! x0 = x_{-1} + W.mu
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! Initial computations
  ! r0    = b - A x0
  ! M u_0 = r0
  ! w0    = A u0
  !
  !----------------------------------------------------------------------

  call solope(&
       1_ip, npoin, nrows, nbvar, idprecon, eps, an, pn, ja, ia, bb, xx , &
       ierr, stopcri, newrho, resid, invnb, rr, uu, pp, ww, &
       invdiag, ss )
  call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, uu, ww )  

  if( ierr /= 0 ) goto 10
  !
  ! Deflation
  !
  if( ngrou > 0 ) then
     !
     ! Needed to compute alpha_0 = (r0,r0) / (Ap0,p0) (p0 includes deflation)
     !
     call wtvect(nbnodes,ngrou,nbvar,mu_gamma_delta,ww)
     if( INOTMASTER ) then
        if( solve_sol(1) % kfl_defas == 0 ) then
           call chosol(&
                solve_sol(1) % ngrou*nbvar,solve_sol(1) % nskyl,&
                solve_sol(1) % iskyl,1_ip,solve_sol(1) % askyldef,mu_gamma_delta,&
                solve_sol(1) % ngrou*nbvar,info) 
        else
           do igrou = 1,ngrou*nbvar
              murhs(igrou) = mu_gamma_delta(igrou)
           end do
           call CSR_LUsol(                                          &
                ngrou                   , nbvar                   , &
                solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
                solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
                solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
                solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
                murhs                   , mu_gamma_delta          )   
        end if
     end if
     call wvect(npoin,nbvar,mu_gamma_delta,dd)
  else
     !
     ! No deflation
     !
     do ii = 1,nrows
        dd(ii)    = 0.0_rp
        aa(ii)    = 0.0_rp
        maa(ii)   = 0.0_rp
        aamaa(ii) = 0.0_rp
     end do
  end if

  do ii = 1,nrows
     u0(ii) = uu(ii) - dd(ii)
  end do
  !
  ! w0 = A u0 => delta0 = (Ap0,p0)
  !
  call bcsrax(1_ip,nbnodes,nbvar,an,ja,ia,u0,w0)  
  call prodxy(nbvar,nbnodes,w0,u0,delta_0)
 
  !-----------------------------------------------------------------
  !
  !  MAIN LOOP
  !
  !-----------------------------------------------------------------

  do while( iters < maxiter .and. resid > stopcri )
     !
     ! Deflation
     ! gamma = (r,u)
     ! delta = (w,u)
     !
     if( ngrou > 0 ) then 
        call wtvect_without_all_reduce(nbnodes,ngrou,nbvar,mu_gamma_delta,ww)
        call solver_scalar_product(nbvar,uu,rr,ww,mu_gamma_delta(nbvar*ngrou+1:),'DO NOT REDUCE')
        call PAR_SUM(nbvar*ngrou+2_ip,mu_gamma_delta,'IN MY ZONE','NON BLOCKING')
     else
        call solver_scalar_product(nbvar,uu,rr,ww,mu_gamma_delta,'NON BLOCKING')
     end if
     !
     ! L m^i = w^i
     ! n^i   = A m^i
     !
     call precon(&
          3_ip,nbvar,npoin,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,w0,ww,mm) 
     call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, mm, nn )
     ! 
     ! Wait all for the all reduce
     !
     call PAR_WAITALL()
     gamma = mu_gamma_delta(igamma)
     delta = mu_gamma_delta(idelta)
     !
     ! Beta and Alpha
     !
     if( iters > 0 ) then
        beta  = gamma / gamma_old
        delta = delta - beta * gamma / alpha_old 
        if( delta == 0.0_rp ) then
           ierr = 2
           goto 10
        else
           alpha = gamma / delta
        end if 
        !
        ! To obtain same iterate as original DCG, uncomment following lines
        !
        !if( ngrou /= 0 ) then 
        !   call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, uu, maa)
        !   call wtvect(nbnodes,ngrou,nbvar,mu,maa)
        !   if( INOTMASTER ) then
        !      if( solve_sol(1) % kfl_defas == 0 ) then
        !         call chosol(&
        !              solve_sol(1) % ngrou*nbvar,solve_sol(1) % nskyl,&
        !              solve_sol(1) % iskyl,1_ip,solve_sol(1) % askyldef,mu,&
        !              solve_sol(1) % ngrou*nbvar,info) 
        !      else
        !         do igrou = 1,ngrou*nbvar
        !            murhs(igrou) = mu(igrou)
        !         end do
        !         call CSR_LUsol(                                          &
        !              ngrou                   , nbvar                   , &
        !              solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
        !              solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
        !              solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
        !              solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
        !              murhs                   , mu                      )   
        !      end if
        !   end if
        !   call wvect(npoin,nbvar,mu,dd)
        !end if
        !do ii = 1,nrows
        !   u0(ii) = uu(ii) - dd(ii)             ! u^{i+1}
        !end do
        !call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, u0,w0)
        !call prodxy(nbvar,npoin,u0,w0,delta)
        !alpha = gamma / ( delta - beta * gamma / alpha_old )

     else
        beta  = 0.0_rp
        alpha = gamma / delta_0
     end if
     gamma_old = gamma
     alpha_old = alpha
     !
     ! Solve A'.mu = W^T.A.u^k, d = W mu
     !
     if( ngrou > 0 ) then 
        if( INOTMASTER ) then
           if( solve_sol(1) % kfl_defas == 0 ) then
              call chosol(&
                   solve_sol(1) % ngrou*nbvar,solve_sol(1) % nskyl,&
                   solve_sol(1) % iskyl,1_ip,solve_sol(1) % askyldef,mu_gamma_delta,&
                   solve_sol(1) % ngrou*nbvar,info) 
           else
              do igrou = 1,ngrou*nbvar
                 murhs(igrou) = mu_gamma_delta(igrou)
              end do
              call CSR_LUsol(                                          &
                   ngrou                   , nbvar                   , &
                   solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
                   solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
                   solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
                   solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
                   murhs                   , mu_gamma_delta          )   
           end if
        end if
        call wvect(npoin,nbvar,mu_gamma_delta,dd)
        !
        ! Arrays needed for deflation
        !
        call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, dd, aa )     ! A d
        call precon(&
             3_ip,nbvar,npoin,nrows,solve_sol(1) % kfl_symme,&      ! M^{-1} A d
             idprecon,ia,ja,an,pn,invdiag,w0,aa,maa) 
        call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, maa, aamaa ) ! A M^{-1} A d
     end if
     !
     ! Same iterate as original DCG
     !
     !do ii = 1,nrows
     !   zz(ii) = nn(ii) + beta  * zz(ii) - aamaa(ii) ! z^i
     !   qq(ii) = mm(ii) + beta  * qq(ii) - maa(ii)   ! q^i
     !   ss(ii) = ww(ii) + beta  * ss(ii) - aa(ii)    ! s^i
     !   pp(ii) = uu(ii) + beta  * pp(ii) - dd(ii)    ! p^i
     !end do
     !call prodxy(nbvar,npoin,pp,ss,raux1)
     !alpha = gamma / raux1
     !if(imaster) print*,abs(gamma / raux1-alpha)     
     !
     ! Updates
     !
#ifdef BLAS
     if( INOTMASTER ) then
        call DAXPY(nrows,beta,zz,1_ip,nn,1_ip)          ! n =  n + beta*z
        call DCOPY(nrows,nn,1_ip,zz,1_ip)               ! z <= n
        call DAXPY(nrows,beta,qq,1_ip,mm,1_ip)          ! m =  m + beta*q
        call DCOPY(nrows,mm,1_ip,qq,1_ip)               ! q <= m

        call DCOPY(nrows,ww,1_ip,w0,1_ip)
        call DAXPY(nrows,beta,ss,1_ip,w0,1_ip)          ! w =  w + beta*s
        call DCOPY(nrows,w0,1_ip,ss,1_ip)               ! s <= w

        call DCOPY(nrows,uu,1_ip,w0,1_ip)
        call DAXPY(nrows,beta,pp,1_ip,w0,1_ip)          ! u =  u + beta*p
        call DCOPY(nrows,w0,1_ip,pp,1_ip)               ! p <= u

        call DAXPY(nrows, alpha,pp,1_ip,xx,1_ip)        ! x =  x + alpha*p
        call DAXPY(nrows,-alpha,ss,1_ip,rr,1_ip)        ! r =  r - alpha*s
        call DAXPY(nrows,-alpha,qq,1_ip,uu,1_ip)        ! u =  u - alpha*q
        call DAXPY(nrows,-alpha,zz,1_ip,ww,1_ip)        ! w =  w -alpha*z
        if( ngrou > 0 ) then
           call DAXPY(nrows,-1.0_rp,aamaa,1_ip,zz,1_ip) 
           call DAXPY(nrows,-1.0_rp,maa  ,1_ip,qq,1_ip)
           call DAXPY(nrows,-1.0_rp,aa   ,1_ip,ss,1_ip)
           call DAXPY(nrows,-1.0_rp,dd   ,1_ip,pp,1_ip)
        end if
     end if
#else 
     do ii = 1,nrows 
        zz(ii) = nn(ii) + beta  * zz(ii) - aamaa(ii)    ! z^i
        qq(ii) = mm(ii) + beta  * qq(ii) - maa(ii)      ! q^i
        ss(ii) = ww(ii) + beta  * ss(ii) - aa(ii)       ! s^i
        pp(ii) = uu(ii) + beta  * pp(ii) - dd(ii)       ! p^i
        xx(ii) = xx(ii) + alpha * pp(ii)                ! x^{i+1}
        rr(ii) = rr(ii) - alpha * ss(ii)                ! r^{i+1}
        uu(ii) = uu(ii) - alpha * qq(ii)                ! u^{i+1}
        ww(ii) = ww(ii) - alpha * zz(ii)                ! w^{i+1}
     end do
#endif

     resid  = sqrt(gamma)
     resi2  = resi1
     resi1  = resid * invnb
     iters  = iters + 1
     !
     ! Convergence post process:
     ! kk    = iteration number
     ! resi1 = preconditioned residual
     !
     if( kfl_cvgso == 1 ) &
          call outcso(nbvar,nrows,npoin,invdiag,an,ja,ia,bb,xx)

  end do

  !-----------------------------------------------------------------
  !
  !  END MAIN LOOP
  !
  !-----------------------------------------------------------------

10 continue
  call solope(&
       2_ip, npoin, nrows, nbvar, idprecon, dummr, an, dummr, ja, ia, bb, xx , &
       ierr, dummr, dummr, resi1, dummr, rr, w0, pp, dummr, &
       invdiag, dummr ) 

  if( kfl_solve == 1 ) then
     if( ierr > 0 ) write(lun_outso,120) iters
  end if

  !-----------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !-----------------------------------------------------------------

  call memory_deallo(memit,'RR'            ,'pipelined_CG',rr)
  call memory_deallo(memit,'PP'            ,'pipelined_CG',pp)
  call memory_deallo(memit,'ZZ'            ,'pipelined_CG',zz)
  call memory_deallo(memit,'DD'            ,'pipelined_CG',dd)
  call memory_deallo(memit,'WW'            ,'pipelined_CG',ww)
  call memory_deallo(memit,'SS'            ,'pipelined_CG',ss)
  call memory_deallo(memit,'UU'            ,'pipelined_CG',uu)
  call memory_deallo(memit,'QQ'            ,'pipelined_CG',qq)
  call memory_deallo(memit,'MM'            ,'pipelined_CG',mm)
  call memory_deallo(memit,'NN'            ,'pipelined_CG',nn)
  call memory_deallo(memit,'W0'            ,'pipelined_CG',w0)
  call memory_deallo(memit,'U0'            ,'pipelined_CG',u0)
  call memory_deallo(memit,'INVDIAG'       ,'pipelined_CG',invdiag)
  call memory_deallo(memit,'ASKYLDEF'      ,'pipelined_CG',solve_sol(1) % askyldef) 
  call memory_deallo(memit,'AA'            ,'pipelined_CG',aa)
  call memory_deallo(memit,'MAA'           ,'pipelined_CG',maa)
  call memory_deallo(memit,'AAMAA'         ,'pipelined_CG',aamaa)
  call memory_deallo(memit,'MU_GAMMA_DELTA','pipelined_CG',mu_gamma_delta)
  call memory_deallo(memit,'MURHS'         ,'pipelined_CG',murhs)

120 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = gamma / ( delta - beta * gamma / alpha_old )')

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine pipelined_CG
