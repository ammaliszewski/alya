subroutine assgr2(ngrou,npopo,nskyl,ndofn,ia,ja,an,askyl)
  !
  ! ASKYL: Factorize group matrix
  !
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  INOTMASTER,parre,nparr,IPARALL,kfl_paral
  use def_solver, only               :  solve_sol
  use def_domain, only               :  r_dom,c_dom
  use mod_memchk
  implicit none
  integer(ip), intent(in)            :: ngrou,npopo,nskyl,ndofn
  integer(ip), intent(in)            :: ia(*),ja(*)
  real(rp),    intent(in)            :: an(ndofn,ndofn,*)
  real(rp),    intent(inout), target :: askyl(ndofn,ndofn,*)
  integer(ip), pointer               :: iskyl(:),lgrou(:)
  integer(ip)                        :: igrou,jgrou,ipoin,izdom,jpoin,izgro
  integer(ip)                        :: kskyl,idofn,jdofn,igrou1,jgrou1,ii,jj
  integer(ip), pointer               :: iagro(:),jagro(:)

  !----------------------------------------------------------------
  !
  ! Fill in sparse matrix ASKYL 
  !       
  !----------------------------------------------------------------
  
  lgrou => solve_sol(1) % lgrou
  iagro => solve_sol(1) % ia
  jagro => solve_sol(1) % ja
  
  do ipoin = 1,npopo
     if( lgrou(ipoin) > 0 ) then
        igrou = lgrou(ipoin)
        do izdom = ia(ipoin),ia(ipoin+1)-1
           jpoin = ja(izdom)
           if( lgrou(jpoin) > 0 ) then
              jgrou = lgrou(jpoin)
              izgro = iagro(igrou)
              iifzgro1: do while( jagro(izgro) /= jgrou )
                 izgro = izgro + 1
              end do iifzgro1
              do ii = 1,ndofn
                 do jj = 1,ndofn
                    askyl(jj,ii,izgro) = askyl(jj,ii,izgro) + an(jj,ii,izdom)
                 end do
              end do
           end if
        end do
     end if
  end do
end subroutine assgr2
