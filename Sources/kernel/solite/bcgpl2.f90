subroutine bcgpl2(&
     nbnodes, nbvar, idprecon, maxiter, eps, an, pn, &
     kfl_cvgso, lun_cvgso, kfl_solve, lun_outso,&
     ja, ia, bb, xx )  
  !-----------------------------------------------------------------------
  ! Objective: Solve    [A] [M]^-1  [M] x = b
  !                        [A']        x' = b'   by the BiCGstab method.
  !
  !            Three preconditioners are possible:
  !            idprecon = 0 => [M]^-1 = [M] = I
  !            idprecon = 2 => [M]^-1 = diag([A])
  !            idprecon = 3 => [M]^-1 = SPAI

  !            The diagonal terms of [A] must be the first in each row.
  !
  !            If Diag. Scaling is selected the preconditioned system is:
  !                    D^-1/2 [A] D^-1/2   D^1/2 x  =  D^-1/2 b
  !                          [A']              x'   =      b'
  !
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg
  use def_master, only       :  IMASTER,INOTSLAVE,kfl_paral,modul
  use def_solver, only       :  memit,SOL_NO_PRECOND,&
       &                        SOL_SQUARE,SOL_DIAGONAL,SOL_MATRIX,&
       &                        SOL_GAUSS_SEIDEL,SOL_LINELET,&
       &                        resin,resfi,resi1,resi2,iters,&
       &                        solve_sol
  use mod_memchk
  implicit none
  integer(ip), intent(in)    :: nbnodes,nbvar,idprecon,maxiter
  integer(ip), intent(in)    :: kfl_cvgso,lun_cvgso
  integer(ip), intent(in)    :: kfl_solve,lun_outso
  real(rp),    intent(inout) :: eps
  real(rp),    intent(in)    :: an(nbvar,nbvar,*),pn(*)
  integer(ip), intent(in)    :: ja(*),ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(inout) :: xx(*)
  real(rp),    pointer, save :: rr(:),r0(:),pp(:),tt(:),qq(:),ss(:)
  real(rp),    pointer, save :: ww(:),invdiag(:)
  integer(ip),          save :: ipass=0
  integer(ip)                :: ii,nrows,ierr,npoin,kk
  integer(4)                 :: istat
  real(rp),             save :: alpha,rho,omega
  real(rp)                   :: newrho,beta
  real(rp)                   :: raux,stopcri,invnb,resid,dummr

  if( IMASTER ) then
     nrows = 1                ! Minimum memory for working arrays
     npoin = 0                ! master does not perform any loop
  else
     npoin = nbnodes
     nrows = nbnodes * nbvar
  end if

  if( ipass == 0 ) then
     !
     ! Allocate memory for working arrays
     !
     allocate(rr(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'RR','bcgpls',rr)
     allocate(r0(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'R0','bcgpls',r0)
     allocate(pp(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'PP','bcgpls',pp)
     allocate(tt(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'TT','bcgpls',tt)
     allocate(qq(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'QQ','bcgpls',qq)
     allocate(ss(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'SS','bcgpls',ss)
     allocate(ww(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'WW','bcgpls',ww)
     allocate(invdiag(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'INVDIAG','bcgpls',invdiag)

  end if

  !----------------------------------------------------------------------
  !
  ! Initial computations
  !
  !----------------------------------------------------------------------

  call solope(&
       1_ip, npoin, nrows, nbvar, idprecon, eps, an, pn, ja, ia, bb, xx , &
       ierr, stopcri, newrho, resid, invnb, rr, r0, pp, ww, &
       invdiag, dummr )
  if( ierr /= 0 ) goto 10

  if( ipass == 0 ) then
     alpha = 0.0_rp
     omega = 1.0_rp
     rho   = 1.0_rp
  end if

  !----------------------------------------------------------------------
  !
  ! MAIN LOOP
  !
  !----------------------------------------------------------------------

  do while( iters < maxiter .and. resid > stopcri )
     !
     ! beta = (rho^k/rho^{k-1})*(alpha/omega)
     !
     if( rho /= 0.0_rp .and. omega /= 0.0_rp ) then
        beta = (newrho/rho) * (alpha/omega)
     else
        ierr = 1
        goto 10
     end if
     !
     ! p^{k+1} = r^{k} + beta*(p^k - omega*q^k)
     !
     if( iters == 0 .and. ipass == 0 ) then
        do ii= 1, nrows
           pp(ii) = rr(ii) 
        end do
     else
        do ii= 1, nrows
           pp(ii) = rr(ii) + beta * (pp(ii) - omega * qq(ii))
        end do
     end if
     !
     ! L q^{k+1} = A ( R^{-1} p^{k+1} )
     !
     call precon(&
          1_ip,nbvar,npoin,nrows,solve_sol(1)%kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,ww,pp,qq)
     !
     ! alpha = rho^k / <r0,q^{k+1}>
     !
     call prodxy(nbvar,npoin,r0,qq,alpha)

     if( alpha == 0.0_rp ) then
        ierr = 2
        goto 10
     end if
     alpha = newrho / alpha
     !
     ! s = r^k - alpha*q^{k+1}
     !
     do ii= 1, nrows
        ss(ii) = rr(ii) - alpha * qq(ii)
     end do
     !
     ! L t = A ( R^{-1} s ) 
     !
     call precon(&
          1_ip,nbvar,npoin,nrows,solve_sol(1)%kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,ww,ss,tt)
     !
     ! omega = <t,s> / <t,t>
     !
     call prodts(nbvar,npoin,tt,ss,raux,omega)

     if ( raux == 0.0_rp ) then 
        ierr = 2
        goto 10
     end if
     omega = omega / raux
     !
     ! x^{k+1} = x^k + alpha*p^{k+1} + omega*s
     !
     do ii= 1, nrows
        xx(ii)  = xx(ii) + alpha * pp(ii) + omega * ss(ii)
     end do
     !
     ! r^{k+1} = s - omega*t
     !
     do ii= 1, nrows
        rr(ii) = ss(ii) - omega * tt(ii)
     end do
     !
     ! rho^k = <r0,r^k> and || r^{k+1} ||
     !
     rho = newrho
     call prodts(nbvar,npoin,rr,r0,resid,newrho)
     resid = sqrt(resid)
     resi2 = resi1
     resi1 = resid * invnb           
     iters = iters + 1     
     !
     ! Convergence output
     !    
     if( kfl_cvgso == 1 ) &
          call outcso(nbvar,nrows,npoin,invdiag,an,ja,ia,bb,xx)

  end do

  !----------------------------------------------------------------------
  !
  ! END MAIN LOOP
  !
  !----------------------------------------------------------------------

10 continue
  call solope(&
       2_ip, npoin, nrows, nbvar, idprecon, dummr, an, dummr, ja, ia, &
       bb,xx ,ierr, dummr, dummr, dummr, invnb, rr, dummr, &
       dummr, dummr, invdiag, dummr )

  if( kfl_solve == 1 .and. INOTSLAVE ) then
     if( ierr == 1 ) write(lun_outso,201) iters
     if( ierr == 2 ) write(lun_outso,202) iters
  end if

  if( ipass == -1 ) then
     call memchk(2_ip,istat,memit,'INVDIAG','bcgpls',invdiag)
     deallocate(invdiag,stat=istat)
     if(istat/=0) call memerr(2_ip,'INVDIAG','bcgpls',0_ip)

     call memchk(2_ip,istat,memit,'WW','bcgpls',ww)
     deallocate(ww,stat=istat)
     if(istat/=0) call memerr(2_ip,'WW','bcgpls',0_ip)

     call memchk(2_ip,istat,memit,'SS','bcgpls',ss)
     deallocate(ss,stat=istat)
     if(istat/=0) call memerr(2_ip,'SS','bcgpls',0_ip)

     call memchk(2_ip,istat,memit,'QQ','bcgpls',qq)
     deallocate(qq,stat=istat)
     if(istat/=0) call memerr(2_ip,'QQ','bcgpls',0_ip)

     call memchk(2_ip,istat,memit,'TT','bcgpls',tt)
     deallocate(tt,stat=istat)
     if(istat/=0) call memerr(2_ip,'TT','bcgpls',0_ip)

     call memchk(2_ip,istat,memit,'PP','bcgpls',pp)
     deallocate(pp,stat=istat)
     if(istat/=0) call memerr(2_ip,'PP','bcgpls',0_ip)

     call memchk(2_ip,istat,memit,'R0','bcgpls',r0)
     deallocate(r0,stat=istat)
     if(istat/=0) call memerr(2_ip,'R0','bcgpls',0_ip)

     call memchk(2_ip,istat,memit,'RR','bcgpls',rr)
     deallocate(rr,stat=istat)
     if(istat/=0) call memerr(2_ip,'RR','bcgpls',0_ip)
  end if

  ipass = ipass + 1
100 format(i7,1x,e12.6)
110 format(i5,18(2x,e12.6))
201 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = beta = (rho^k/rho^{k-1})*(alpha/omega)')
202 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = beta = alpha = rho^k / <r0,q^{k+1}>')

end subroutine bcgpl2
