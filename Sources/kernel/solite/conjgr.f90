subroutine conjgr( &
     nbnodes, nbvar, maxiter, &
     eps, an, ja, ia, bb, xx )

  !-----------------------------------------------------------------------
  !****f* solite/solope
  ! NAME
  !    solope
  ! DESCRIPTION
  !    Sequential and stand alone conjugate gradient
  ! USED BY
  !    Iterative solvers
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  INOTSLAVE,IMASTER,kfl_paral
  implicit none
  integer(ip), intent(in)    :: nbnodes,nbvar,maxiter
  real(rp),    intent(in)    :: eps
  real(rp),    intent(in)    :: an(nbvar,nbvar,*)
  integer(ip), intent(in)    :: ja(*), ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(inout) :: xx(*)
  integer(ip)                :: ii,jj,kk,ll,nrows,ierro
  integer(ip)                :: iters,npoin
  real(rp)                   :: alpha,beta,rho,stopcri
  real(rp)                   :: resid,resin,resfi
  real(rp)                   :: invnb,newrho,bnorm
  real(rp),    pointer       :: rr(:),pp(:),zz(:),invdiag(:)

  !----------------------------------------------------------------------
  !
  ! Working arrays
  !
  !----------------------------------------------------------------------

  if( IMASTER ) then
     nrows = 1
     npoin = 0
  else
     nrows = nbnodes * nbvar
     npoin = nbnodes
  end if

  allocate( rr(nrows)      ) 
  allocate( pp(nrows)      )  
  allocate( zz(nrows)      ) 
  allocate( invdiag(nrows) )
  if( IMASTER ) nrows = 0

  !----------------------------------------------------------------------
  !
  ! Diagonal
  !
  !----------------------------------------------------------------------

  if( nbvar == 1 ) then
     do ii = 1,npoin 
        jj = ia(ii)
        ll = -1
        do while( jj < ia(ii+1) .and. ll == -1 )
           if( ja(jj) == ii ) then
              ll = jj
           end if
           jj = jj + 1
        end do
        if( ll /= -1 ) then
           invdiag(ii) = an(1,1,ll)
        end if
     end do
  else
     do ii = 1,npoin 
        jj = ia(ii)
        ll = -1
        do while ( jj< ia(ii+1) .and. ll == -1 )
           if( ja(jj) == ii ) then
              ll = jj
           end if
           jj = jj + 1 
        end do
        if( ll /= -1 ) then
           jj = (ii-1) * nbvar
           do kk = 1,nbvar
              invdiag(jj+kk) = an(kk,kk,ll)
           end do
        end if
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! Initial computations
  !
  !----------------------------------------------------------------------

  ierro = 0
  iters = 0
  bnorm = 0.0_rp
  do ii = 1,nrows 
     if( abs(invdiag(ii)) <= 1.0e-12_rp ) call runend('CONJGR: NULL DIAGONAL ELEMENT')
     invdiag(ii) = 1.0_rp / invdiag(ii)
     rr(ii)      = invdiag(ii) * bb(ii)
     bnorm       = bnorm + rr(ii) * rr(ii)
  end do
  call pararr('SUM',0_ip,1_ip,bnorm)
  bnorm = sqrt(bnorm)
  !
  ! || L^-1 b|| = 0: trivial solution x = 0
  !    
  if( bnorm <= 1.0e-12_rp ) then
     do ii = 1,nrows
        xx(ii) = 0.0_rp
     end do
     resid =  0.0_rp
     resin =  0.0_rp
     ierro = -1
     goto 10
  end if
  invnb = 1.0_rp / bnorm
  !
  ! r = b - Ax
  ! 
  !print*,'a=',kfl_paral
  !!call bcsrax_2(1_ip,npoin,nbvar,an,ja,ia,xx,rr)
  !print*,'b=',kfl_paral
  !call runend('SWSW')

  newrho = 0.0_rp
  do ii = 1,nrows
     rr(ii) = bb(ii) - rr(ii) 
     pp(ii) = invdiag(ii) * rr(ii) 
     newrho = newrho + rr(ii) * pp(ii)
  end do
  call pararr('SUM',0_ip,1_ip,newrho)
  resid   = sqrt(newrho)     
  resin   = resid * invnb
  resfi   = resin
  stopcri = eps * bnorm

  !-----------------------------------------------------------------
  !
  !  MAIN LOOP
  !
  !-----------------------------------------------------------------

  do while( iters < maxiter .and. resid > stopcri )
     !
     ! q^{k+1} =  A p^{k+1}
     !
     !!call bcsrax_2(1_ip,npoin,nbvar,an,ja,ia,pp,zz)  
     !
     ! alpha = rho^k / <p^{k+1},q^{k+1}>
     !
     alpha = 0.0_rp
     do ii = 1,nrows
        alpha = alpha + pp(ii) * zz(ii)
     end do
     call pararr('SUM',0_ip,1_ip,alpha)

     if( alpha == 0.0_rp ) then
        ierro = 2
        goto 10
     end if
     rho   = newrho
     alpha = newrho / alpha
     !
     ! x^{k+1} = x^k + alpha*p^{k+1}
     ! r^{k+1} = r^k - alpha*q^{k+1}
     ! z^{k+1} = D^{-1} r^{k+1}
     !
     newrho = 0.0_rp
     do ii = 1,nrows
        xx(ii) = xx(ii) + alpha * pp(ii)
        rr(ii) = rr(ii) - alpha * zz(ii)
        zz(ii) = invdiag(ii) * rr(ii) 
        newrho = newrho + rr(ii) * zz(ii)
     end do
     call pararr('SUM',0_ip,1_ip,newrho)
     !
     ! beta  = rho^k / rho^{k-1}  
     !
     beta = newrho / rho
     !
     ! p^{k+1} = z^k + beta*p^k - W.mu^k
     !
     do ii= 1, nrows
        pp(ii) = zz(ii) + beta * pp(ii)
     end do

     resid = sqrt(newrho)
     iters = iters + 1
     if( INOTSLAVE ) write(90,*) iters,resid * invnb

  end do

  !-----------------------------------------------------------------
  !
  !  END MAIN LOOP
  !
  !-----------------------------------------------------------------

10 continue

  !-----------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !-----------------------------------------------------------------

  deallocate( invdiag )
  deallocate( zz      )
  deallocate( pp      )
  deallocate( rr      )

120 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = rho^k / <p^{k+1},q^{k+1}>')

end subroutine conjgr

