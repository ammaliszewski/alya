!-----------------------------------------------------------------------
!> @addtogroup AlgebraicSolvers
!> @{
!> @file    cgrpls.f90
!> @author  Guillaume Houzeaux
!> @brief   Conjugate gradient solver
!> @details Conjugate gradient solver:
!!          @verbatim
!!          Compute r0 := b - Ax0, z0 = M-1r0, and p0 := z0
!!          For j = 0, 1,..., until convergence Do:
!!            vj   = Apj
!!            aj   = (rj, zj)/(v, pj)
!!            xj+1 = xj + aj pj
!!            rj+1 = rj - aj vj
!!            zj+1 = M-1 rj+1
!!            bj   = (rj+1, zj+1)/(rj, zj)
!!            pj+1 = zj+1 + bjpj
!!          EndDo
!!          @endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine cgrpls( &
     nbnodes, nbvar, idprecon, maxiter, kfl_resid, &
     eps, an, pn, kfl_cvgso, lun_cvgso, kfl_solve, &
     lun_outso, ja, ia, bb, xx )
  use def_kintyp, only       :  ip,rp,lg
  use def_master, only       :  IMASTER,INOTMASTER,kfl_paral,npoi1,&
       &                        IPARALL,kfl_async,parre,ISLAVE,&
       &                        INOTSLAVE,funin,NPOIN_TYPE
  use def_solver, only       :  memit,SOL_NO_PRECOND,SOL_LINELET,&
       &                        SOL_SQUARE,SOL_DIAGONAL,SOL_MATRIX,&
       &                        resin,resfi,resi1,resi2,iters,solve_sol
  use mod_memchk 
  use def_coupli,         only : mcoup
!use def_domain, only : coord
!use def_master, only : parr1
!use mod_postpr, only : postpr
  implicit none
  integer(ip), intent(in)    :: nbnodes, nbvar, idprecon, maxiter, kfl_resid
  integer(ip), intent(in)    :: kfl_cvgso, lun_cvgso
  integer(ip), intent(in)    :: kfl_solve, lun_outso
  real(rp),    intent(in)    :: eps
  real(rp),    intent(in)    :: an(nbvar,nbvar,*), pn(*)
  integer(ip), intent(in)    :: ja(*), ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(inout) :: xx(nbvar*nbnodes)
  integer(ip)                :: ii, nrows, ierr, npoin, kcoup
  integer(4)                 :: istat
  real(rp)                   :: alpha, beta, rho, stopcri, resid
  real(rp)                   :: invnb, newrho, dummr
  real(rp),    pointer       :: rr(:), pp(:), zz(:), invdiag(:)
  real(rp),    pointer       :: ww(:), p0(:)

!character(5) :: wopos(3)
!real(rp),    pointer       :: kk(:)
!if(inotmaster) allocate(kk(nbnodes))

#ifdef EVENT
  call mpitrace_user_function(1)
#endif 

  if(  IMASTER ) then
     nrows   = 1                ! Minimum memory for working arrays
     npoin   = 0                ! master does not perform any loop
  else
     nrows   = nbnodes * nbvar
     npoin   = nbnodes
  end if
  !
  ! Sequential and slaves: Working arrays
  !
  allocate(rr(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'RR','deflcg',rr)

  allocate(pp(nrows),stat=istat)  
  call memchk(0_ip,istat,memit,'PP','deflcg',pp)

  allocate(p0(nrows),stat=istat)  
  call memchk(0_ip,istat,memit,'P0','deflcg',p0)

  allocate(zz(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'ZZ','deflcg',zz)

  allocate(ww(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'WW','deflcg',ww)

  allocate(invdiag(nrows),stat=istat)
  call memchk(0_ip,istat,memit,'INVDIAG','deflcg',invdiag)

  if( IMASTER ) nrows = 0 ! Master does not perform any loop

  !----------------------------------------------------------------------
  !
  ! Initial computations
  !
  !----------------------------------------------------------------------

  call solope(&
       1_ip, npoin, nrows, nbvar, idprecon, eps, an, pn, ja, ia, bb, xx , &
       ierr, stopcri, newrho, resid, invnb, rr, zz, pp, ww, &
       invdiag, p0 )

  if( ierr /= 0 ) goto 10

  !-----------------------------------------------------------------
  !
  !  MAIN LOOP
  !
  !-----------------------------------------------------------------
  !kcoup = mcoup
  !mcoup = 0

  do while( iters < maxiter .and. resid > stopcri )
     !
     ! q^{k+1} =  A R^-1 p^{k+1}
     !
     call precon(&
          4_ip,nbvar,npoin,nrows,solve_sol(1)%kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,ww,pp,zz)
     !
     ! alpha = rho^k / <p^{k+1},q^{k+1}>
     !
     call prodxy(nbvar,npoin,pp,zz,alpha)

    if( alpha == 0.0_rp ) then
        ierr = 2
        goto 10
     end if
     rho   = newrho
     alpha = newrho / alpha 
     !
     ! x^{k+1} = x^k + alpha*p^{k+1}
     ! r^{k+1} = r^k - alpha*q^{k+1}
     !
#ifdef BLAS
     call DAXPY(nrows, alpha,pp,1_ip,xx,1_ip) 
     call DAXPY(nrows,-alpha,zz,1_ip,rr,1_ip) 
#else
     do ii = 1,nrows
        xx(ii) = xx(ii) + alpha * pp(ii)
        rr(ii) = rr(ii) - alpha * zz(ii)
     end do
#endif
     !
     !  L z^{k+1} = r^{k+1}
     !
     call precon(&
          3_ip,nbvar,npoin,nrows,solve_sol(1)%kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,ww,rr,zz)
     call prodxy(nbvar,npoin,rr,zz,newrho)
     !
     ! beta  = rho^k / rho^{k-1}  
     !
     beta = newrho / rho
     !
     ! p^{k+1} = z^k + beta*p^k 
     ! 
#ifdef BLAS
     call DAXPY(nrows,beta,pp,1_ip,zz,1_ip)  ! z =  z + beta*p
     call DCOPY(nrows,zz,1_ip,pp,1_ip)       ! p <= z
#else
     do ii= 1, nrows
        pp(ii) = zz(ii) + beta * pp(ii)
     end do
#endif

     resid  = sqrt(newrho)
     resi2  = resi1
     resi1  = resid * invnb
     iters  = iters + 1
     !
     ! Convergence post process:
     ! kk    = iteration number
     ! resi1 = preconditioned residual
     !
     if( kfl_cvgso == 1 ) &
          call outcso(nbvar,nrows,npoin,invdiag,an,ja,ia,bb,xx)
 
  end do

  !-----------------------------------------------------------------
  !
  !  END MAIN LOOP
  !
  !-----------------------------------------------------------------

10 continue
  call solope(&
       2_ip, npoin, nrows, nbvar, idprecon, dummr, an, dummr, ja, ia, bb, xx , &
       ierr, dummr, dummr, resi1, dummr, rr, p0, pp, dummr, &
       invdiag, dummr ) 

  if( kfl_solve == 1 ) then
     if( ierr > 0 ) write(lun_outso,120) iters
  end if


  !mcoup = kcoup

  !-----------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !-----------------------------------------------------------------

  call memchk(2_ip,istat,memit,'INVDIAG','deflcg',invdiag)
  deallocate(invdiag,stat=istat)
  if(istat/=0) call memerr(2_ip,'INVDIAG','deflcg',0_ip)

  call memchk(2_ip,istat,memit,'WW','deflcg',ww)
  deallocate(ww,stat=istat)
  if(istat/=0) call memerr(2_ip,'WW','deflcg',0_ip)

  call memchk(2_ip,istat,memit,'ZZ','deflcg',zz)
  deallocate(zz,stat=istat)
  if(istat/=0) call memerr(2_ip,'ZZ','deflcg',0_ip)

  call memchk(2_ip,istat,memit,'P0','deflcg',p0)
  deallocate(p0,stat=istat)
  if(istat/=0) call memerr(2_ip,'P0','deflcg',0_ip)

  call memchk(2_ip,istat,memit,'PP','deflcg',pp)
  deallocate(pp,stat=istat)
  if(istat/=0) call memerr(2_ip,'PP','deflcg',0_ip)

  call memchk(2_ip,istat,memit,'RR','deflcg',rr)
  deallocate(rr,stat=istat)
  if(istat/=0) call memerr(2_ip,'RR','deflcg',0_ip)

110 format(i5,18(2x,e12.6))
120 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = rho^k / <p^{k+1},q^{k+1}>')

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine cgrpls
