subroutine bcsrai(itask,nbnodes,nbvar,an,ja,ia,xx,yy) 
  !----------------------------------------------------------------------
  !****f* mathru/bcsrai
  ! NAME 
  !     bcsrai
  ! DESCRIPTION
  !     Multiply a non symmetric matrix stored in BCSR by a vector
  !     YY = A XX 
  ! INPUT
  !    NBNODES .... Number of equations
  !    NBVAR ...... Number of variables
  !    AN ......... Matrix
  !    JA ......... List of elements
  !    IA ......... Pointer to list of elements
  !    XX ......... Vector
  ! OUTPUT
  !    YY ......... result vector
  ! USES
  ! USED BY
  !***
  !----------------------------------------------------------------------
  use def_kintyp, only             :  ip,rp
  use def_master, only             :  INOTMASTER,npoi1,IPARALL,NPOIN_TYPE,kfl_paral
  implicit none
  integer(ip), intent(in)          :: itask,nbnodes,nbvar
  real(rp),    intent(in)          :: an(nbvar,nbvar,*)
  integer(ip), intent(in)          :: ja(*),ia(*)
  real(rp),    intent(in)          :: xx(nbvar,*)
  real(rp),    intent(out), target :: yy(nbvar,*)
  integer(ip)                      :: ii,jj,kk,ll,col
  real(rp)                         :: raux,raux1,raux2,raux3

  if( INOTMASTER ) then    

     !-------------------------------------------------------------------
     !
     ! Boundary nodes
     !
     !-------------------------------------------------------------------

#ifdef EVENT_MATVEC
     ! Compute boundary elements
     call mpitrace_eventandcounters(400,1)
#endif

     if( nbvar == 1 ) then
        !
        ! NBVAR=1
        !
        !$OMP PARALLEL  DO                                     &
        !$OMP SCHEDULE ( STATIC )                              &
        !$OMP DEFAULT  ( NONE )                                &
        !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux )            
        !
        do ii = npoi1+1,nbnodes
           yy(1,ii) = 0.0_rp
           do jj   = ia(ii),ia(ii+1)-1
              col  = ja(jj)
              raux = xx(1,col)
              yy(1,ii) = yy(1,ii) +an(1,1,jj) * raux
           end do
        end do

     else if( nbvar == 2 ) then
        !
        ! NBVAR=2
        !
        !$OMP PARALLEL  DO                                     &
        !$OMP SCHEDULE ( STATIC )                              &
        !$OMP DEFAULT  ( NONE )                                &
        !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux1, raux2 )    
        !
        do ii = npoi1+1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
           end do
        end do

     else if( nbvar == 3 ) then
        !
        ! NBVAR=3
        !
        !$OMP PARALLEL  DO                                       &
        !$OMP SCHEDULE ( STATIC )                                &
        !$OMP DEFAULT  ( NONE )                                  &
        !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )    &
        !$OMP PRIVATE  ( col, ii, jj, raux1, raux2, raux3 )    
        !
        do ii = npoi1+1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              raux3    = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux3
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux3
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux1
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux2
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux3
           end do

        end do

     else if( nbvar == 4 ) then
        !
        ! NBVAR=4
        !
        !$OMP PARALLEL  DO                                     &
        !$OMP SCHEDULE ( STATIC )                              &
        !$OMP DEFAULT  ( NONE )                                &
        !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux )            
        !
        do ii = npoi1+1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           yy(4,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux     = xx(1,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(1,4,jj) * raux
              raux     = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(2,4,jj) * raux
              raux     = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(3,4,jj) * raux
              raux     = xx(4,col)
              yy(1,ii) = yy(1,ii) + an(4,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(4,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(4,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(4,4,jj) * raux
           end do

        end do

     else
        !
        ! NBVAR = whatever
        !
        !$OMP PARALLEL  DO                                            &
        !$OMP SCHEDULE ( STATIC )                                     &
        !$OMP DEFAULT  ( NONE )                                       &
        !$OMP SHARED   ( an, ia, ja, nbnodes, nbvar, npoi1, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, kk, ll, raux )           
        !
        do ii = npoi1+1,nbnodes
           do kk = 1,nbvar
              yy(kk,ii) = 0.0_rp
           end do
           do jj  = ia(ii),ia(ii+1)-1
              col = ja(jj)
              do ll = 1,nbvar
                 raux = xx(ll,col)
                 do kk = 1,nbvar
                    yy(kk,ii) = yy(kk,ii) + an(ll,kk,jj) * raux
                 end do
              end do
           end do
        end do

     end if

#ifdef EVENT_MATVEC
     ! Send/recv boundary elements contributions asynchronously
     call mpitrace_eventandcounters(400,2)
#endif

     !-------------------------------------------------------------------
     !
     ! Modify YY due do periodicity and Parall service
     !
     !-------------------------------------------------------------------

     if( itask == 1 .and. IPARALL ) then
        call pararr('SLA',NPOIN_TYPE,nbnodes*nbvar,yy)
     end if

#ifdef EVENT_MATVEC
     ! Compute interior elements async
     call mpitrace_eventandcounters(400,3)
#endif

     !-------------------------------------------------------------------
     !
     ! Interior nodes
     !
     !-------------------------------------------------------------------

     if( nbvar == 1 ) then
        !
        ! NBVAR=1
        !
        !$OMP PARALLEL  DO                                     &
        !$OMP SCHEDULE ( STATIC )                              &
        !$OMP DEFAULT  ( NONE )                                &
        !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux )            
        !
        do ii = 1,npoi1
           yy(1,ii) = 0.0_rp
           do jj   = ia(ii),ia(ii+1)-1
              col  = ja(jj)
              raux = xx(1,col)
              yy(1,ii) = yy(1,ii) +an(1,1,jj) * raux
           end do
        end do

     else if( nbvar == 2 ) then
        !
        ! NBVAR=2
        !
        !$OMP PARALLEL  DO                                     &
        !$OMP SCHEDULE ( STATIC )                              &
        !$OMP DEFAULT  ( NONE )                                &
        !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux1, raux2 )    
        !
        do ii = 1,npoi1
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
           end do
        end do

     else if( nbvar == 3 ) then
        !
        ! NBVAR=3
        !
        !$OMP PARALLEL  DO                                       &
        !$OMP SCHEDULE ( STATIC )                                &
        !$OMP DEFAULT  ( NONE )                                  &
        !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )    &
        !$OMP PRIVATE  ( col, ii, jj, raux1, raux2, raux3 )    
        !
        do ii = 1,npoi1
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              raux3    = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux3
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux3
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux1
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux2
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux3
           end do

        end do

     else if( nbvar == 4 ) then
        !
        ! NBVAR=4
        !
        !$OMP PARALLEL  DO                                     &
        !$OMP SCHEDULE ( STATIC )                              &
        !$OMP DEFAULT  ( NONE )                                &
        !$OMP SHARED   ( an, ia, ja, nbnodes, npoi1, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux )            
        !
        do ii = 1,npoi1
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           yy(4,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux     = xx(1,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(1,4,jj) * raux
              raux     = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(2,4,jj) * raux
              raux     = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(3,4,jj) * raux
              raux     = xx(4,col)
              yy(1,ii) = yy(1,ii) + an(4,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(4,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(4,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(4,4,jj) * raux
           end do

        end do

     else
        !
        ! NBVAR = whatever
        !
        !$OMP PARALLEL  DO                                            &
        !$OMP SCHEDULE ( STATIC )                                     &
        !$OMP DEFAULT  ( NONE )                                       &
        !$OMP SHARED   ( an, ia, ja, nbnodes, nbvar, npoi1, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, kk, ll, raux )           
        !
        do ii = 1,npoi1
           do kk = 1,nbvar
              yy(kk,ii) = 0.0_rp
           end do
           do jj  = ia(ii),ia(ii+1)-1
              col = ja(jj)
              do ll = 1,nbvar
                 raux = xx(ll,col)
                 do kk = 1,nbvar
                    yy(kk,ii) = yy(kk,ii) + an(ll,kk,jj) * raux
                 end do
              end do
           end do
        end do
 
     end if

#ifdef EVENT_MATVEC
     ! Wait boundary elements contributions async
     call mpitrace_eventandcounters(400,4)
#endif

     !-------------------------------------------------------------------
     !
     ! Wait all and sum up contributions of boundary nodes
     !
     !-------------------------------------------------------------------

     if( itask == 1 .and. IPARALL ) then
        call pararr('SLA',NPOIN_TYPE,nbnodes*nbvar,yy)
     end if

#ifdef EVENT_MATVEC
     ! Finish
     call mpitrace_eventandcounters(400,0)
#endif

  end if

end subroutine bcsrai

