!-----------------------------------------------------------------------
!> @addtogroup Solver
!> @{                                                                   
!> @file    solope.f90
!> @author  Guillaume Houzeaux
!> @date    15/07/2015
!> @brief   This routine computes some preliminary arrays for iterative solvers
!>          which solve L^-1 A x = L^-1 b or A R^-1 R x = b    
!> @details 
!>          \verbatim
!>     
!>          ITASK == 1: Initial computations
!>          --------------------------------
!>          BB ........... RHS b
!>          NBNODES ...... Number of nodes
!>          XX ........... Initial solution
!>          PN ........... Preconditionning matrix
!>          IDPRECON ..... Preconditionner
!>          EPS .......... Solver tolerance
!>          AN ........... Matrix A
!>          IA, JA ....... Matrix graph in BCSR format
!>          INVDIAG ...... D^-1/2 or D^-1
!>          RR ........... b-Ax or L^-1 (b-Ax)
!>          ZZ ........... L^-1 (b-Ax)
!>          WW ........... Temporary array
!>          BP ........... Save first serach vector to compute loss of 
!>                         orthogonality
!>          INVNB ........ 1 / || L^-1 b ||
!>          NEWRHO ....... < L^-1 r , L^-1 r > or < L^-1 r , r > 
!>          RESID ........ sqrt(NEWRHO)
!>          RESIN ........ RESID * INVNB
!>          IERRO = -1 ... Trivial solution x = 0
!>                = -2 ... Initial guess is solution
!>
!>          In the case of right preconditioning, solve:
!>          A R^-1 R x  = b
!>          Solve for x': A R^-1 x' = b 
!>          so x'0 = R.x0
!>          and at the end recover x by solving R x = x'
!>
!>          ITASK == 2: End of solver
!>          --------------------------------
!>
!>          SOLVE_SOL(1) % RESF2 ... Final non-preconditionned residual
!>          Deallocate memory
!>
!>          \endverbatim
!>
!> @}                                                                   
!-----------------------------------------------------------------------
subroutine solope(&
     itask, nbnodes, nrows, nbvar, idprecon, eps, an, pn, ja, ia, &
     bb, xx , ierro, stopcri, newrho, resid, invnb, rr, zz,&
     pp, ww, invdiag , bp )
  use def_kintyp, only       :  ip,rp,lg
  use def_elmtyp
  use def_master, only       :  IMASTER,parr3,pard1,zeror,&
       &                        INOTMASTER,npoi1,kfl_timin,gisca,veloc
  use def_solver, only       :  memit,SOL_NO_PRECOND,SOL_AII,&
       &                        SOL_SQUARE,SOL_DIAGONAL,SOL_MATRIX,&
       &                        SOL_GAUSS_SEIDEL,SOL_LINELET,&
       &                        SOL_MULTIGRID,SOL_RAS,&
       &                        SOL_SOLVER_CG,&
       &                        SOL_SOLVER_DEFLATED_CG,&
       &                        SOL_SOLVER_PIPELINED_CG,&
       &                        resin,resfi,resi1,resi2,iters,&
       &                        solve_sol,nzdom_aii,&
       &                        SOL_SOLVER_DEFLATED_GMRES,&
       &                        SOL_SOLVER_GMRES,SOL_RIGHT_PRECONDITIONING,&
       &                        SOL_LEFT_PRECONDITIONING,&
       &                        SOL_BLOCK_DIAGONAL,block_diagonal,block_invdiagonal,&
       &                        SOL_SOLVER_BICGSTAB,&
       &                        SOL_SOLVER_DEFLATED_BICGSTAB
  use def_domain, only       :  r_dom_aii,c_dom_aii,nzdom,lnoch,nperi,&
       &                        lperi,invpr_aii,coord,meshe
  use def_kermod, only       :  ndivi
  use mod_memchk
  use mod_memory
  use mod_csrdir
  use mod_matrix
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  use mod_maths,          only : maths_invert_matrix
  use def_kermod,         only : kfl_detection
  use mod_ker_detection,  only : ker_detection_maximum_residual
  use mod_graphs,         only : graphs_permut_metis_postordering
  use mod_graphs,         only : graphs_permut
  use mod_graphs,         only : graphs_number_along_vector
  implicit none
  integer(ip), intent(in)    :: itask
  integer(ip), intent(in)    :: nbnodes
  integer(ip), intent(inout) :: nrows
  integer(ip), intent(in)    :: nbvar
  integer(ip), intent(in)    :: idprecon
  real(rp),    intent(inout) :: eps
  real(rp),    intent(in)    :: an(nbvar,nbvar,*)
  real(rp),    intent(in)    :: pn(*)
  integer(ip), intent(in)    :: ja(*)
  integer(ip), intent(in)    :: ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(inout) :: xx(*)
  real(rp),    intent(inout) :: bp(*)
  integer(ip), intent(out)   :: ierro
  real(rp),    intent(out)   :: stopcri
  real(rp),    intent(out)   :: newrho
  real(rp),    intent(out)   :: resid
  real(rp),    intent(out)   :: invnb
  real(rp),    intent(out)   :: rr(*)
  real(rp),    intent(out)   :: zz(*)
  real(rp),    intent(out)   :: pp(*)
  real(rp),    intent(out)   :: ww(*)
  real(rp),    intent(out)   :: invdiag(*)
  integer(ip)                :: ii,jj,kk,izdom,kzdom,ipoin,jpoin
  integer(ip)                :: iperi,nskyl,info,ipoin_old
  integer(ip)                :: max_residual_node,jpoin_old
  real(rp)                   :: bnorm,xnorm,rnorm,dummr
  real(rp)                   :: time1,time2
  real(rp)                   :: max_residual
  real(rp),    save          :: invb2

  select case ( itask )

  case ( 1_ip )

     !----------------------------------------------------------------------
     !
     ! Initial computations
     !
     !----------------------------------------------------------------------

     if( kfl_timin == 1 ) call Parall(20_ip)
     call cputim(time1)

     ierro = 0
     resi1 = 1.0_rp
     resi2 = 1.0_rp
     resin = 1.0_rp
     iters = 0
     solve_sol(1) % resin = resin
     if( IMASTER ) nrows = 0    ! Master does not perform any loop
     !
     call penali(nbvar,nbnodes,ia,ja,an,bb,xx)

     !----------------------------------------------------------------------
     !
     ! Matrix comes from Schur complement: Compute A3^-1 = diag(A3)^-1
     !
     !----------------------------------------------------------------------

     if( solve_sol(1) % kfl_schum == 1 .and. INOTMASTER ) then 
        call matrix_invdia(&
             nbnodes,solve_sol(1) % ndofn_A3,solve_sol(1) % kfl_symme,ia,ja,&
             solve_sol(1) % A3,solve_sol(1) % invA3,memit)
     end if

     !----------------------------------------------------------------------
     !
     ! INVDIAG = D^-1/2 or D^-1
     !
     !----------------------------------------------------------------------

     !if( solve_sol(1) % kfl_schum == 1 .and. INOTMASTER ) then
     !   call matrix_diagonal_schur(nbnodes,nbvar,solve_sol(1) % ndofn_A3,&
     !        solve_sol(1) % A1,solve_sol(1) % A2,solve_sol(1) % invA3,&
     !        solve_sol(1) % A4,ia,ja,invdiag) 
     !else
     call diagon(nbnodes,nbvar,solve_sol(1) % kfl_symme,ia,ja,an,invdiag)
     !end if

     if( idprecon == SOL_SQUARE ) then
        if( nbvar == 1 ) then
           do ii= 1, nbnodes
              invdiag(ii) = sqrt( abs(invdiag(ii)) )
           end do
        else
           do ii= 1, nbnodes
              jj = (ii-1) * nbvar
              do kk= 1, nbvar
                 invdiag(jj+kk) = sqrt( abs(invdiag(jj+kk)) )
              end do
           end do
        end if
     end if
     !
     ! Periodicity: put diagonal to 1
     !
     if( INOTMASTER ) then
        do iperi = 1,nperi
           ii = lperi(2,iperi)
           jj = ( ii - 1 )  * nbvar
           do kk = 1,nbvar
              jj = jj + 1
              !invdiag(jj) = 1.0_rp
           end do
        end do
     end if
     !
     ! Put zero inverse diagonal on hole nodes
     !
     if( solve_sol(1) % kfl_leftr == SOL_LEFT_PRECONDITIONING ) then
        jj = 0 
        do ii= 1,nbnodes
           if( lnoch(ii) == NOHOL ) then
              do kk = 1,nbvar
                 jj = jj + 1
                 invdiag(jj) = 0.0_rp
              end do
           else
              do kk = 1,nbvar
                 jj = jj + 1
                 if( invdiag(jj) == 0.0_rp ) then     
                    !write(*,*) ' '
                    !write(*,*) 'NULL ELEMENT ON ROW ',jj
                    !write(*,*) 'CHECK NODE ',lninv_loc(ii)
                    !call runend('SOLOPE: NULL DIAGONAL ELEMENT')
                 else
                    invdiag(jj) = 1.0_rp / invdiag(jj)
                 end if
              end do
           end if
        end do
     else
        jj = 0 
        do ii= 1,nbnodes
           do kk = 1,nbvar
              jj = jj + 1
              if( invdiag(jj) <= zeror ) then     
                 invdiag(jj) = 1.0_rp
              else
                 invdiag(jj) = 1.0_rp / invdiag(jj)
              end if
           end do
        end do
     end if

     !----------------------------------------------------------------------
     !
     ! Coarse solver: compute and factorize coarse matrix
     !
     !----------------------------------------------------------------------

     if( solve_sol(1) % kfl_coarse == 1 ) then
        nskyl =  solve_sol(1) % nzgro * nbvar * nbvar

        call memory_alloca(memit,'ASKYLPREDEF','solope',solve_sol(1) % askylpredef,nskyl)
        !
        ! ASKYLPREDEF: Assemble coarse matrix preconditioner 
        !
        if( solve_sol(1) % kfl_symme == 1 ) then
           call matgro(solve_sol(1) % ngrou,nbnodes,nskyl,nbvar,ia,ja,an,solve_sol(1) % askylpredef)
        else
           call matgr2(solve_sol(1) % ngrou,nbnodes,nskyl,nbvar,ia,ja,an,solve_sol(1) % askylpredef)
        end if

        if( INOTMASTER .and. solve_sol(1) % kfl_defas == 1 ) then 
           !
           ! Inverse CSR matrix ASKYLPREDEF
           !
           nullify(solve_sol(1) % Ilpredef)
           nullify(solve_sol(1) % Jlpredef)
           nullify(solve_sol(1) % Lnpredef)
           nullify(solve_sol(1) % iUpredef)
           nullify(solve_sol(1) % jUpredef)
           nullify(solve_sol(1) % Unpredef)     
           nullify(solve_sol(1) % invpRpredef)
           nullify(solve_sol(1) % invpCpredef)              

           call CSR_LU_Factorization(&  
                solve_sol(1) % ngrou,nbvar,solve_sol(1) % ia,solve_sol(1) % ja,&
                solve_sol(1) % askylpredef,&
                solve_sol(1) % ILpredef,solve_sol(1) % JLpredef,&
                solve_sol(1) % LNpredef,solve_sol(1) % IUpredef,&
                solve_sol(1) % JUpredef,solve_sol(1) % UNpredef,info)
           if( info /= 0 ) call runend('DEFLCG: SINGULAR MATRIX')
        end if

     end if

     !----------------------------------------------------------------------
     !
     ! Factorize preconditioners
     !
     !----------------------------------------------------------------------

     if( idprecon == SOL_LINELET .or. ( idprecon == SOL_MULTIGRID .and. solve_sol(1) % kfl_defpr == SOL_LINELET ) ) then
        !
        ! Linelet
        !
        call faclin(solve_sol(1) % ndofn,an,invdiag)

     else if( idprecon == SOL_GAUSS_SEIDEL .and. solve_sol(1) % kfl_renumbered_gs == 1 .and. INOTMASTER ) then
        !
        ! Streamwise Gauss-Seidel
        ! 
        call graphs_number_along_vector(&
             meshe(ndivi),solve_sol(1) % vecto_gs,solve_sol(1) % permr_gs,&
             solve_sol(1) % invpr_gs,solve_sol(1) % lgrou_gs,solve_sol(1) % kfl_fixno)                 

     else if( idprecon == SOL_AII .and. INOTMASTER ) then     
        !
        ! Block LU: complete LU for interior nodes new = permr(old)
        !
        call memory_alloca(memit,'AIIPRE','solope',solve_sol(1) % Aiipre,nbvar,nbvar,nzdom_aii)
        solve_sol(1) % invpR => invpr_aii
        solve_sol(1) % invpC => invpr_aii

        do ipoin = 1,npoi1
           ipoin_old = solve_sol(1) % invpR(ipoin)
           do izdom = r_dom_aii(ipoin),r_dom_aii(ipoin+1)-1
              jpoin     = c_dom_aii(izdom)
              jpoin_old = solve_sol(1) % invpR(jpoin)
              kzdom     = ia(ipoin_old)
              do while( ja(kzdom) /= jpoin_old )
                 kzdom = kzdom + 1
              end do
              solve_sol(1) % Aiipre(1:nbvar,1:nbvar,izdom) = an(1:nbvar,1:nbvar,kzdom)
           end do
        end do

        call CSR_LU_Factorization(&                 ! CSR LU Factorization  
             npoi1,nbvar,r_dom_aii,c_dom_aii,&
             solve_sol(1) % Aiipre,solve_sol(1) % ilpre,&
             solve_sol(1) % JLpre, solve_sol(1) % LNpre,&
             solve_sol(1) % IUpre, solve_sol(1) % JUpre,&
             solve_sol(1) % Unpre,ierro)
        if( ierro /= 0 ) call runend('SOLOPE: SINGULAR MATRIX OF BLOCK LU PRECONDITIONER')

     else if( idprecon == SOL_RAS .and. INOTMASTER ) then     
        !
        ! RAS: Restricted Additive Schwarz: SOLVE_SOL(1) % AIIPRE
        ! 
        call memory_alloca(memit,'ARAS','solope',solve_sol(1) % Aras,nbvar,nbvar,nzdom)

        call CSR_Permute_and_Copy_matrix(&
             nbnodes                 , nbvar                 , &
             ia                      , ja                    , &
             solve_sol(1) % ia_ras   , solve_sol(1) % ja_ras , &
             solve_sol(1) % invpRras , an                    , &
             solve_sol(1) % Aras     )

        pard1 =  nbvar                                            ! Exchange matrix on subdomain interfaces
        parr3 => solve_sol(1) % Aras                              ! => Matrix is complete on diagonal
        call Parall(432_ip)

        call CSR_LU_Factorization(&                               ! CSR LU Factorization  
             nbnodes,nbvar,&
             solve_sol(1) % ia_ras, solve_sol(1) % ja_ras, & 
             solve_sol(1) % Aras,   solve_sol(1) % ilras,  &
             solve_sol(1) % JLras,  solve_sol(1) % LNras,  &
             solve_sol(1) % IUras,  solve_sol(1) % JUras,  &
             solve_sol(1) % Unras,  ierro)
        if( ierro /= 0 ) call runend('SOLOPE: SINGULAR MATRIX OF RAS PRECONDITIONER')

     else if( idprecon == SOL_MULTIGRID ) then 
        !
        ! Deflated multigrid
        !
        if( solve_sol(1)%kfl_algso == 2 ) then
           solve_sol(1) % askylpredef => solve_sol(1) % askyldef
           if( solve_sol(1) % kfl_defas /= 0 ) then
              solve_sol(1) % Ilpredef    => solve_sol(1) % Ildef    
              solve_sol(1) % Jlpredef    => solve_sol(1) % Jldef    
              solve_sol(1) % Lnpredef    => solve_sol(1) % Lndef    
              solve_sol(1) % iUpredef    => solve_sol(1) % iUdef    
              solve_sol(1) % jUpredef    => solve_sol(1) % jUdef    
              solve_sol(1) % Unpredef    => solve_sol(1) % Undef    
              solve_sol(1) % invpRpredef => solve_sol(1) % invpRdef
              solve_sol(1) % invpCpredef => solve_sol(1) % invpCdef                           
           end if
        else 
           if( solve_sol(1) % kfl_defas == 0 ) then
              nskyl =  solve_sol(1) % nskyl
           else
              nskyl =  solve_sol(1) % nzgro * nbvar * nbvar
           end if
           call memory_alloca(memit,'ASKYLPREDEF','solope',solve_sol(1) % askylpredef,nskyl)
           !
           ! ASKYLPREDEF: Assemble coarse matrix preconditioner 
           !
           if( solve_sol(1) % kfl_symme == 1 ) then
              call matgro(solve_sol(1) % ngrou,nbnodes,nskyl,nbvar,ia,ja,an,solve_sol(1) % askylpredef)
           else
              call matgr2(solve_sol(1) % ngrou,nbnodes,nskyl,nbvar,ia,ja,an,solve_sol(1) % askylpredef)
           end if
           if( solve_sol(1) % kfl_defso == 0 .and. INOTMASTER ) then
              if( solve_sol(1) % kfl_defas == 1 ) then 
                 !
                 ! Inverse CSR matrix ASKYLPREDEF
                 !
                 nullify(solve_sol(1) % Ilpredef)
                 nullify(solve_sol(1) % Jlpredef)
                 nullify(solve_sol(1) % Lnpredef)
                 nullify(solve_sol(1) % iUpredef)
                 nullify(solve_sol(1) % jUpredef)
                 nullify(solve_sol(1) % Unpredef)     
                 nullify(solve_sol(1) % invpRpredef)
                 nullify(solve_sol(1) % invpCpredef)              

                 call CSR_LU_Factorization(&  
                      solve_sol(1) % ngrou,nbvar,solve_sol(1) % ia,solve_sol(1) % ja,&
                      solve_sol(1) % askylpredef,&
                      solve_sol(1) % ILpredef,solve_sol(1) % JLpredef,&
                      solve_sol(1) % LNpredef,solve_sol(1) % IUpredef,&
                      solve_sol(1) % JUpredef,solve_sol(1) % UNpredef,info)
                 if( info /= 0 ) call runend('DEFLCG: SINGULAR MATRIX')

              else
                 !
                 ! Inverse skyline matrix ASKYLPREDEF
                 !
                 call chofac(&
                      nbvar*solve_sol(1) % ngrou,solve_sol(1) % nskyl,&
                      solve_sol(1) % iskyl,solve_sol(1) % askylpredef,info)
                 if( info /= 0 ) call runend('MATGRO: ERROR WHILE DOING CHOLESKY FACTORIZATION')                 
              end if
           end if

        end if

     else if( idprecon == SOL_BLOCK_DIAGONAL .and. INOTMASTER ) then 
        !
        ! Block diagonal
        !
        nullify( block_diagonal )
        nullify( block_invdiagonal )
        call memory_alloca(memit,'BLOCK_DIAGONAL','solope',block_diagonal,   nbvar,nbvar,nbnodes)
        call memory_alloca(memit,'BLOCK_DIAGONAL','solope',block_invdiagonal,nbvar,nbvar,nbnodes)
        do ipoin = 1,nbnodes
           izdom = ia(ipoin)
           do while( izdom <= ia(ipoin+1)-1 )
              jpoin = ja(izdom) 
              if( ipoin == jpoin ) then
                 do jj = 1,nbvar
                    do ii = 1,nbvar
                       block_diagonal(ii,jj,ipoin) = an(jj,ii,izdom)
                    end do
                 end do
                 izdom = ia(ipoin+1)
              end if
              izdom = izdom + 1
           end do
        end do
        call PAR_INTERFACE_NODE_EXCHANGE(block_diagonal,'SUM','IN MY ZONE','SYNCHRONOUS')
        do ipoin = 1,nbnodes
           block_invdiagonal(1:nbvar,1:nbvar,ipoin) = block_diagonal(1:nbvar,1:nbvar,ipoin)
           call maths_invert_matrix(nbvar,block_invdiagonal(1:nbvar,1:nbvar,ipoin))
        end do

     end if

     !----------------------------------------------------------------------
     !
     ! INVNB = 1 / || L^-1 b ||
     !
     !----------------------------------------------------------------------
     !
     ! w = L^-1 b, BNORM = || L^-1 w ||
     !
     call precon(&
          3_ip,nbvar,nbnodes,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,rr,bb,ww)

     if(    solve_sol(1) % kfl_algso == SOL_SOLVER_CG           .or. &
          & solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_CG  .or. &
          & solve_sol(1) % kfl_algso == SOL_SOLVER_PIPELINED_CG ) then
        !
        ! CG and DCG: normalize residual by || b || := (b , L^-1 b )^1/2
        !
        call prodxy(nbvar,nbnodes,bb,ww,bnorm)
        bnorm = sqrt(bnorm)
     else
        !
        ! Other solvers: normalize residual by || b || := (L^-1 b , L^-1 b )^1/2
        !
        call norm2x(nbvar,ww,bnorm)
     end if
     !
     ! || L^-1 b|| = 0: trivial solution x = 0
     !    
     if( bnorm <= 1.0e-12_rp ) then
        do ii= 1, nrows
           xx(ii) = 0.0_rp
        end do
        resid                =  0.0_rp
        resin                =  0.0_rp
        resi1                =  0.0_rp
        invnb                =  0.0_rp
        solve_sol(1) % resin =  0.0_rp 
        solve_sol(1) % resi2 =  0.0_rp
        solve_sol(1) % reni2 =  0.0_rp
        ierro                = -1
        goto 10
     end if
     invnb = 1.0_rp / bnorm
     !
     ! GMRES needs L^-1 b
     !
     if(    solve_sol(1) % kfl_algso == SOL_SOLVER_GMRES .or. &
          & solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_GMRES ) then
        do ii= 1, nrows
           bp(ii) = ww(ii)
        end do
     end if

     !----------------------------------------------------------------------
     !
     ! Initial residual: r = b - Ax
     !
     !----------------------------------------------------------------------
     !
     ! XNORM = ||x||
     !
     call norm1x(nbvar,xx,xnorm)

     if( xnorm == 0.0_rp ) then
        !
        ! Initial x is zero: r = b
        !
        do ii= 1, nrows
           rr(ii) = bb(ii)
        end do

     else 
        !
        ! r = b - A x
        !
        if( solve_sol(1) % kfl_symme == 1 ) then     
           call bsymax( 1_ip, nbnodes, nbvar, an, ja, ia, xx, rr )    
        else if( solve_sol(1) % kfl_schum == 1 ) then
           call bcsrax_schur( 1_ip, nbnodes, nbvar, solve_sol(1) % ndofn_A3 , &
                solve_sol(1) % A1, solve_sol(1) % A2, solve_sol(1) % invA3, solve_sol(1) % A4,& 
                invdiag, ja, ia, xx, rr )  
        else     
           call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, xx, rr )  
        end if
        call norm2x(nbvar,rr,rnorm)

        do ii= 1, nrows
           rr(ii) = bb(ii) - rr(ii) 
        end do
     end if
     !
     ! Residual = 0 on Dirichlet nodes
     !
     !if( solve_sol(1) % kfl_iffix > 0 ) then     
     !   ii = 0
     !   do jj = 1,nbnodes
     !      do kk = 1,nbvar
     !         ii = ii + 1
     !         if( solve_sol(1) % kfl_fixno(kk,jj) > 0 ) rr(ii) = 0.0_rp
     !      end do
     !   end do
     !end if

     call norm2x(nbvar,rr,rnorm)
     call norm2x(nbvar,bb,invb2)

     if( invb2 /= 0.0_rp ) invb2 = 1.0_rp / invb2 
     solve_sol(1) % reni2 = rnorm                   ! Non-normalized residual
     solve_sol(1) % resi2 = rnorm * invb2           ! Normalized residual

     !----------------------------------------------------------------------
     !
     ! Preconditioned residual: z = L^{-1} r
     !
     !----------------------------------------------------------------------

     if(  solve_sol(1) % kfl_algso == SOL_SOLVER_GMRES .or. &
          solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_GMRES ) then
        call precon(&
             2_ip,nbvar,nbnodes,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
             pn,invdiag,ww,dummr,rr)
     else
        call precon(&
             3_ip,nbvar,nbnodes,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
             pn,invdiag,ww,rr,zz)
     end if

     !----------------------------------------------------------------------
     !
     ! Initial rho: BCGSTAB: rho = < L^-1 r , L^-1 r > 
     !              GMRES:   rho = < L^-1 r , L^-1 r > 
     !              CG:      rho = < L^-1 r ,      r > 
     !
     !----------------------------------------------------------------------

     if(    solve_sol(1) % kfl_algso == SOL_SOLVER_BICGSTAB .or. &
          & solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_BICGSTAB ) then
        !       
        ! BICGSTAB
        !
        do ii = 1,nrows
           rr(ii) = zz(ii)
        end do
        call prodxy(nbvar,nbnodes,zz,zz,newrho) 
     else if( solve_sol(1) % kfl_algso == SOL_SOLVER_GMRES .or. &
          &   solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_GMRES ) then  
        !
        ! GMRES
        !
        call prodxy(nbvar,nbnodes,rr,rr,newrho) 
     else if( solve_sol(1) % kfl_algso == 11 ) then      
        !                               
        ! RICHARDSON
        !
        call prodxy(nbvar,nbnodes,zz,zz,newrho) 
     else
        !                                                                               
        ! CG, DCG, pipelined CG
        !
        call prodxy(nbvar,nbnodes,rr,zz,newrho) 
     end if

     resid                = sqrt(newrho)     
     resin                = resid * invnb
     resi1                = resin
     solve_sol(1) % resin = resin
     !
     ! Convergence output
     !    
     if( solve_sol(1) % kfl_cvgso == 1 ) then
        call outcso(nbvar,nrows,nbnodes,invdiag,an,ja,ia,bb,xx)
     end if
     !
     ! Adaptive residual
     !
     if( solve_sol(1) % kfl_adres == 1 ) then
        eps = max( solve_sol(1) % resin*solve_sol(1) % adres , solve_sol(1) % solmi )
     end if
     !
     ! stop criterion = ||b|| * eps
     !
     stopcri = eps * bnorm 
     !
     ! Test if the initial guess is the solution
     !
     if ( resid <= stopcri ) then
        ierro = -2
        resfi =  resin
        goto 10
     end if
     !
     ! Initial x <= R x 
     ! 
     call precon(&
          6_ip,nbvar,nbnodes,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,ww,dummr,xx)
     !
     ! Initial direction: not needed by GMRES nor RICHARDSON
     !
     if(    solve_sol(1) % kfl_algso /= SOL_SOLVER_GMRES .and. &
          & solve_sol(1) % kfl_algso /= SOL_SOLVER_DEFLATED_GMRES) then
        do ii= 1, nrows
           pp(ii) = zz(ii)
        end do
     end if
     !
     ! Save first direction to check orthogonality: CG type
     !
     if(  solve_sol(1) % kfl_algso == SOL_SOLVER_CG          .or. &
          solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_CG .or. &
          solve_sol(1) % kfl_algso == SOL_SOLVER_PIPELINED_CG ) then    ! CG
        do ii= 1, nrows
           bp(ii) = zz(ii)
        end do
     end if

10   continue 

     if( kfl_timin == 1 ) call Parall(20_ip)
     call cputim(time2)
     solve_sol(1) % cputi(2) = solve_sol(1) % cputi(2) + time2 - time1

  case ( 2_ip ) 

     !----------------------------------------------------------------------
     !
     ! End of solver and non-preconditionned residual
     ! r = || b - Ax || / || b ||
     !
     !----------------------------------------------------------------------
     !
     ! Final R x = x'
     !
     if( ierro == 0 ) then 
        call precon(&
             5_ip,nbvar,nbnodes,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
             pn,invdiag,rr,bb,xx)
     end if
     !
     ! Final non-preconditioned residual
     !
     resfi = resi1 

     if( solve_sol(1) % kfl_schur == 1 ) then
        !call par_schuax(&
        !     nbvar,zi,xb,aib,abi,abb,r_dom_aib,c_dom_aib,r_dom_abb,&
        !     c_dom_abb,r_dom_abi,c_dom_abi,askyl,iskyl,nskyl,rr) 
     else 
        if( solve_sol(1) % kfl_symme == 1 ) then     
           call bsymax( 1_ip, nbnodes, nbvar, an, ja, ia, xx, rr )    
        else if( solve_sol(1) % kfl_schum == 1 ) then
           call bcsrax_schur( 1_ip, nbnodes, nbvar, solve_sol(1) % ndofn_A3 , &
                solve_sol(1) % A1, solve_sol(1) % A2, solve_sol(1) % invA3, solve_sol(1) % A4,& 
                invdiag, ja, ia, xx, rr )  
        else
           call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, xx, rr )  
        end if
     end if

     do ii = 1,nrows
        rr(ii) = bb(ii) - rr(ii) 
     end do
     call norm2x(nbvar,rr,rnorm)
     solve_sol(1) % resf2 = rnorm * invb2
     !
     ! Anomaly detection
     !
     if( iters >= solve_sol(1) % miter .and. kfl_detection /= 0 ) then
        max_residual = -1.0_rp
        max_residual_node = 0
        do ii = 1,nbnodes
           rnorm = 0.0_rp
           jj = (ii-1)*nbvar
           do kk = 1,nbvar
              jj = jj + 1
              rnorm = rnorm + rr(jj) * rr(jj)
           end do
           if( rnorm > max_residual ) then
              max_residual = sqrt(rnorm)
              max_residual_node = ii
           end if
        end do
        call ker_detection_maximum_residual(&
             max_residual_node,max_residual,nbvar,xx,&
             'SOLVER_NOT_CONVERGED',trim(solve_sol(1) % wprob),'NO_COMMENT')
     end if
     ! 
     ! Deallocate some memory for block LU
     !
     if( idprecon == SOL_AII .and. INOTMASTER ) then 
        call CSR_LUfin(&
             solve_sol(1) % ILpre,solve_sol(1) % JLpre,solve_sol(1) % LNpre,&
             solve_sol(1) % IUpre,solve_sol(1) % JUpre,solve_sol(1) % UNpre)
        call memory_deallo(memit,'AIIPRE','solope',solve_sol(1) % Aiipre)
     end if
     ! 
     ! Deallocate some memory for RAS
     !
     if( idprecon == SOL_RAS .and. INOTMASTER ) then 
        call CSR_LUfin(&
             solve_sol(1) % ILras,solve_sol(1) % JLras,solve_sol(1) % LNras,&
             solve_sol(1) % IUras,solve_sol(1) % JUras,solve_sol(1) % UNras)
        call memory_deallo(memit,'Aras','solope',solve_sol(1) % Aras)
     end if
     !
     ! Deallocate memory of deflated multigrid preconditioner or coarse solver
     !
     if( idprecon == SOL_MULTIGRID .and. solve_sol(1) % kfl_algso == 2 ) then
        continue
     else if( idprecon == SOL_MULTIGRID .or. solve_sol(1) % kfl_coarse == 1 ) then
        call memory_deallo(memit,'ASKYLPREDEF','solope',solve_sol(1) % askylpredef)
        if( solve_sol(1) % kfl_defso == 0 .and. solve_sol(1) % kfl_defas == 1 .and. INOTMASTER ) then
           call CSR_LUfin(&
                solve_sol(1) % ILpredef,solve_sol(1) % JLpredef,solve_sol(1) % LNpredef,&
                solve_sol(1) % IUpredef,solve_sol(1) % JUpredef,solve_sol(1) % UNpredef)        
        end if
     end if
     ! 
     ! Compute cos of angle between last p and p0 to check orthogonality
     ! <Ap,p0> / ( ||Ap|| ||p0|| ); zz is p0
     !
     if(  solve_sol(1) % kfl_algso == SOL_SOLVER_CG          .or. &
          solve_sol(1) % kfl_algso == SOL_SOLVER_DEFLATED_CG .or. &
          solve_sol(1) % kfl_algso == SOL_SOLVER_PIPELINED_CG ) then    ! CG
        if( solve_sol(1) % kfl_symme == 1 ) then     
           call bsymax( 1_ip, nbnodes, nbvar, an, ja, ia, pp, rr )    
        else if( solve_sol(1) % kfl_schum == 1 ) then
           call bcsrax_schur( 1_ip, nbnodes, nbvar, solve_sol(1) % ndofn_A3 , &
                solve_sol(1) % A1, solve_sol(1) % A2, solve_sol(1) % invA3, solve_sol(1) % A4,& 
                invdiag, ja, ia, pp, rr )  
        else
           call bcsrax( 1_ip, nbnodes, nbvar, an, ja, ia, pp, rr )  
        end if
        call cosixy(nbvar,nbnodes,rr,zz,solve_sol(1)%xorth)
     end if
     !
     ! Block diagonal
     !

     if( idprecon == SOL_BLOCK_DIAGONAL .and. INOTMASTER ) then 
        call memory_deallo(memit,'BLOCK_DIAGONAL',   'solope',block_diagonal)
        call memory_deallo(memit,'BLOCK_INVDIAGONAL','solope',block_invdiagonal)
     end if
     !
     ! Matrix from a Schur
     !
     if( solve_sol(1) % kfl_schum == 1 .and. INOTMASTER ) then
        call memory_deallo(memit,'INVA3','solope',solve_sol(1) % invA3)
     end if

  end select

end subroutine solope
