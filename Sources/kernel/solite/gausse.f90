subroutine gausse(&
     nbnodes, nbvar, idprecon, maxiter, eps, an, pn, &
     kfl_cvgso, lun_cvgso, kfl_solve, lun_outso,&
     ja, ia, bb, xx )  
  !-----------------------------------------------------------------------
  ! Objective: Solve    [A] [M]^-1  [M] x = b
  !                        [A']        x' = b'   by the BiCGstab method.
  !
  !            Three preconditioners are possible:
  !            idprecon = 0 => [M]^-1 = [M] = I
  !            idprecon = 2 => [M]^-1 = diag([A])
  !            idprecon = 3 => [M]^-1 = SPAI

  !            The diagonal terms of [A] must be the first in each row.
  !
  !            If Diag. Scaling is selected the preconditioned system is:
  !                    D^-1/2 [A] D^-1/2   D^1/2 x  =  D^-1/2 b
  !                          [A']              x'   =      b'
  !
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg
  use def_master, only       :  IMASTER,INOTSLAVE,kfl_paral,modul
  use def_solver, only       :  memit,SOL_NO_PRECOND,&
       &                        SOL_SQUARE,SOL_DIAGONAL,SOL_MATRIX,&
       &                        SOL_GAUSS_SEIDEL,SOL_LINELET,&
       &                        resin,resfi,resi1,resi2,iters,&
       &                        solve_sol
  use mod_memchk
  implicit none
  integer(ip), intent(in)    :: nbnodes,nbvar,idprecon,maxiter
  integer(ip), intent(in)    :: kfl_cvgso,lun_cvgso
  integer(ip), intent(in)    :: kfl_solve,lun_outso
  real(rp),    intent(inout) :: eps
  real(rp),    intent(in)    :: an(nbvar,nbvar,*),pn(*)
  integer(ip), intent(in)    :: ja(*),ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(inout) :: xx(*)
  real(rp),    pointer       :: rr(:),zz(:)
  real(rp),    pointer       :: invdiag(:)
  integer(ip)                :: ii,nrows,ierr,npoin
  integer(4)                 :: istat
  real(rp)                   :: raux,stopcri,invnb,resid,bnorm

  if( IMASTER ) then
     nrows = 1                ! Minimum memory for working arrays
     npoin = 0                ! master does not perform any loop
  else
     npoin = nbnodes
     nrows = nbnodes * nbvar
  end if
  !
  ! Allocate memory for working arrays
  !
  allocate(rr(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'RR','cgrpls',rr)
  allocate(zz(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'ZZ','cgrpls',zz)
  allocate(invdiag(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'INVDIAG','bcgpls',invdiag)

  !----------------------------------------------------------------------
  !
  ! Initial computations
  !
  !----------------------------------------------------------------------

  iters = 0
  ierr  = 0
  call diagon(nbnodes,nbvar,solve_sol(1)%kfl_symme,ia,ja,an,invdiag)
  do ii= 1, nrows
     invdiag(ii) = 1.0_rp / invdiag(ii)
  end do
  call gupper(nbnodes,nbvar,an,ja,ia,invdiag,rr,bb) 
  call norm2x(nbvar,rr,bnorm)
  stopcri = eps * bnorm
  if( bnorm /= 0.0_rp ) invnb = 1.0_rp/bnorm
  if( solve_sol(1) % kfl_symme == 1 ) then  
     call bsymax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )  
  else
     call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )  
  end if
  do ii = 1,nrows
     rr(ii) = bb(ii) - rr(ii)
  end do
  call norm2x(nbvar,rr,resid)  

  !----------------------------------------------------------------------
  !
  ! MAIN LOOP
  !
  !----------------------------------------------------------------------

  do while( iters < maxiter .and. resid > stopcri )
     !
     ! 1. (L+D) x^k+1 = b - U x^k
     !           
     if( solve_sol(1) % kfl_symme == 1 ) then  
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )  
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )  
     end if
     do ii = 1,nrows
        rr(ii) = bb(ii) - rr(ii)
     end do

     call gupper(nbnodes,nbvar,an,ja,ia,invdiag,zz,rr) 
stop
     do ii = 1,nrows
        xx(ii) = xx(ii) + zz(ii)
     end do
     call norm2x(nbvar,zz,resid)
     resi2 = resi1
     resi1 = resid * invnb           
     iters = iters + 1
     !
     ! Convergence output
     !    
     if( kfl_cvgso == 1 .and. INOTSLAVE ) write(lun_cvgso,100) iters,resi1

  end do

  !----------------------------------------------------------------------
  !
  ! END MAIN LOOP
  !
  !----------------------------------------------------------------------

  if( kfl_solve == 1 .and. INOTSLAVE ) then
     if( ierr == 1 ) write(lun_outso,201) iters
     if( ierr == 2 ) write(lun_outso,202) iters
  end if

  call memchk(2_ip,istat,memit,'INVDIAG','cgrpls',invdiag)
  deallocate(invdiag,stat=istat)
  if(istat/=0) call memerr(2_ip,'INVDIAG','cgrpls',0_ip)

  call memchk(2_ip,istat,memit,'ZZ','cgrpls',zz)
  deallocate(zz,stat=istat)
  if(istat/=0) call memerr(2_ip,'ZZ','cgrpls',0_ip)

  call memchk(2_ip,istat,memit,'RR','cgrpls',rr)
  deallocate(rr,stat=istat)
  if(istat/=0) call memerr(2_ip,'RR','cgrpls',0_ip)

100 format(i7,1x,e12.6)
110 format(i5,18(2x,e12.6))
201 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = beta = (rho^k/rho^{k-1})*(alpha/omega)')
202 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = beta = alpha = rho^k / <r0,q^{k+1}>')

end subroutine gausse

subroutine gupper(nbnodes,nbvar,an,ja,ia,invdiag,xx,bb) 
  !----------------------------------------------------------------------
  !****f* mathru/bcsrax
  ! NAME 
  !     bcsrax
  ! DESCRIPTION
  !     Solve (L+D) D^-1 (U+D) x = b
  ! INPUT
  !    NBNODES .... Number of equations
  !    NBVAR ...... Number of variables
  !    AN ......... Matrix
  !    JA ......... List of elements
  !    IA ......... Pointer to list of elements
  !    BB ......... Vector
  ! OUTPUT
  !    XX ......... result vector
  ! USES
  ! USED BY
  !***
  !----------------------------------------------------------------------
  use def_kintyp, only             :  ip,rp,lg
  use def_master, only             :  INOTMASTER,npoi1,npoi2,npoi3
  implicit none
  integer(ip), intent(in)          :: nbnodes,nbvar
  real(rp),    intent(in)          :: an(nbvar,nbvar,*)
  integer(ip), intent(in)          :: ja(*),ia(*)
  real(rp),    intent(in)          :: invdiag(nbvar,*)
  real(rp),    intent(in)          :: bb(nbvar,*)
  real(rp),    intent(inout)       :: xx(nbvar,*)
  integer(ip)                      :: ii,jj,kk,ll,col

  if( INOTMASTER ) then
     !
     ! NBVAR = whatever
     !
     !*OMP   PARALLEL DO SCHEDULE (GUIDED)               & 
     !*OMP   DEFAULT (NONE)                              &
     !*OMP   PRIVATE ( ii, jj, kk, ll, col, raux)        &
     !*OMP   SHARED ( nbnodes, nbvar, bb, xx, ia, ja, an)

     !
     ! (L+D) x = b
     !
     do ii = npoi1+1,nbnodes
        do kk = 1,nbvar
           xx(kk,ii) = bb(kk,ii) * invdiag(kk,ii)
        end do
     end do

     if( nbvar == 1 ) then
        !
        ! 1. (L+D) x = b
        !           
        do ii = 1,npoi1
           !
           ! x_i^{k+1} = 1/a_ii * b_i
           !
           xx(1,ii) = bb(1,ii) * invdiag(1,ii)

           do jj  = ia(ii),ia(ii+1)-1
              col = ja(jj) 
if(ii==2) print*,'voisin=',col,an(1,1,jj)
              if( col < ii ) then
                 !
                 ! j<i: x_i^{k+1} = x_i^{k+1} - 1/a_ii * a_ij * x_j^{k+1}
                 !
                 xx(1,ii) = xx(1,ii) - invdiag(1,ii) * an(1,1,jj) * xx(1,col) 
              end if
           end do
        end do
print*,xx(1,1:11)

     else
        !
        ! 1. (L+D) y = b
        !           
        do ii = 1,npoi1
           !
           ! x_i^{k+1} = 1/a_ii * b_i
           !
           do kk = 1,nbvar
              xx(kk,ii) = bb(kk,ii) * invdiag(kk,ii)
           end do

           do jj  = ia(ii),ia(ii+1)-1
              col = ja(jj) 
              if( col < ii ) then
                 !
                 ! j<i: x_i^{k+1} = x_i^{k+1} - 1/a_ii * a_ij * x_j^{k+1}
                 !
                 do kk = 1,nbvar
                    do ll = 1,nbvar
                       xx(kk,ii) = xx(kk,ii) - invdiag(kk,ii) * an(ll,kk,jj) * xx(ll,col) 
                    end do
                 end do
              end if
           end do
        end do

     end if

  end if

end subroutine gupper
