subroutine smoite(&
     nbvar,npoin,nrows,kfl_symme,idprecon,ia,ja,an,&
     pn,invdiag,ww,pp,qq)
  !------------------------------------------------------------------------
  !****f* solite/smoite
  ! NAME 
  !    smoite
  ! DESCRIPTION
  !
  !    Solve system L q = p
  !
  !    INVDIAG ... D^-1
  !    WW ........ Working vector
  !    Q ......... Output vector
  !    P ......... Input vector
  !    AN ........ Matrix
  !    PN ........ Preconditioner Matrix
  !
  !    1. w = A ( R p ) 
  !    2. L q = w
  !
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp
  use def_master, only      :  INOTMASTER,npoi1
  use def_solver, only      :  SOL_NO_PRECOND,SOL_SQUARE,SOL_DIAGONAL,&
       &                       SOL_MATRIX,SOL_GAUSS_SEIDEL,SOL_LINELET,&
       &                       SOL_ORTHOMIN,solve_sol,SOL_AII,&
       &                       SOL_MULTIGRID
  use def_domain, only      :  r_dom,c_dom,r_sym,c_sym
  use mod_csrdir, only      :  CSR_LUsol
  implicit none
  integer(ip), intent(in)   :: nbvar,npoin,nrows,kfl_symme,idprecon
  integer(ip), intent(in)   :: ia(*),ja(*)
  real(rp),    intent(in)   :: an(*),pn(*),invdiag(*),pp(*)
  real(rp),    intent(out)  :: ww(*),qq(*) 
  integer(ip)               :: ii,ibvar,ipoin
  real(rp)                  :: numer,denom,alpha

  !------------------------------------------------------------------- 
  !
  ! Solve L q = p
  !
  !------------------------------------------------------------------- 

  if( idprecon == SOL_NO_PRECOND ) then

     do ii= 1, nrows
        qq(ii) = pp(ii) 
     end do

  else if( idprecon == SOL_SQUARE .or. idprecon == SOL_DIAGONAL ) then

     do ii= 1, nrows
        qq(ii) = pp(ii) * invdiag(ii)
     end do

  else if( idprecon == SOL_MATRIX ) then

     if( kfl_symme == 1 ) then
        call bsymax( 1_ip, npoin, nbvar, pn, ja, ia, pp, qq )  
     else
        call bcsrax( 1_ip, npoin, nbvar, pn, ja, ia, pp, qq )
     end if

  !else if( idprecon == SOL_JACOBI ) then
  !
  !   do ii= 1, nrows
  !      qq(ii) = pp(ii) * invdiag(ii)
  !   end do
 
  else if( idprecon == SOL_GAUSS_SEIDEL ) then

     call bcsrgs( npoin, nbvar, an, ja, ia, invdiag, qq , pp)

  else if( idprecon == SOL_LINELET ) then

     do ii= 1, nrows
        qq(ii) = pp(ii)
     end do
     call sollin(nbvar,qq,ww,invdiag)

  else if( idprecon == SOL_ORTHOMIN ) then
     !
     ! x^1   = x^0 + alpha * D^-1 ( b - A x^0 ); x^0 = 0 =>
     ! x     = alpha * D^-1 b
     ! alpha = ( b , A D^-1 b ) / || A D^-1 b ||^2
     ! b     = p
     !   
     do ii= 1, nrows                                            ! q = A D^-1 b
        ww(ii) = invdiag(ii) * pp(ii)
     end do
     if( kfl_symme == 1 ) then                                
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, ww, qq )  
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, ww, qq )  
     end if
     call prodxy(nbvar,npoin,pp,qq,numer)
     call prodxy(nbvar,npoin,qq,qq,denom)
     if( denom == 0.0_rp ) then
        alpha = 1.0_rp
     else
        alpha = numer / denom
     end if

     do ii= 1, nrows
        qq(ii) = alpha * ww(ii)
     end do

  else if( idprecon == SOL_AII ) then
     !
     ! L q = p
     !
     do ii= 1, nrows                
        ww(ii) = pp(ii)
     end do
     if( INOTMASTER ) then
        call CSR_LUsol(&
             npoi1,nbvar,solve_sol(1)%invpR,solve_sol(1)%invpC,&
             solve_sol(1)%ILpre,solve_sol(1)%JLpre,&
             solve_sol(1)%LNpre,solve_sol(1)%IUpre,solve_sol(1)%JUpre,&
             solve_sol(1)%UNpre,ww,qq)
        if( nbvar == 1 ) then
           do ii = npoi1+1,npoin
              qq(ii) = invdiag(ii) * ww(ii)
           end do
        else
           do ipoin = npoi1+1,npoin
              ii = (ipoin-1)*nbvar
              do ibvar = 1,nbvar
                 ii = ii + 1
                 qq(ii) = invdiag(ii) * ww(ii)
              end do
           end do
        end if
     end if

  else if( idprecon == SOL_MULTIGRID ) then
     !
     ! L q = p
     !
     call coajac(&
          nbvar,npoin,nrows,ia,ja,an,invdiag,pp,qq)

  end if

end subroutine smoite
