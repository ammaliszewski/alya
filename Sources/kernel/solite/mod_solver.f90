!------------------------------------------------------------------------
!> @addtogroup Coupling
!> @{
!> @name    Coupling functions
!> @file    mod_parall.f90
!> @author  Guillaume Houzeaux
!> @date    13/06/2014
!> @brief   ToolBox for solvers
!> @details ToolBox for solvers. Matrix can be one block or multiblock
!>         
!>          Multiblock matrices are organized this way:
!>
!>           ndof1   ndof2
!>          <-----><------->
!>          +-----+--------+  
!>          |     |        |
!>          | 1,1 |  1,2   |
!>          +-----+--------+      
!>          |     |        |
!>          | 2,1 |  2,2   |
!>          |     |        |    
!>          +-----+--------+
!>
!> @{
!------------------------------------------------------------------------

module mod_solver
  use def_kintyp,         only :  ip,rp,lg,soltyp
  use def_master,         only :  INOTMASTER
  use def_master,         only :  IMASTER
  use def_master,         only :  IPARALL
  use def_master,         only :  current_zone,lninv_loc
  use def_master,         only :  npoi1,npoi2,npoi3
  use def_domain,         only :  nperi
  use def_domain,         only :  lperi
  use def_domain,         only :  lmast
  use def_domain,         only :  nzdom 
  use def_domain,         only :  r_dom 
  use def_domain,         only :  c_dom 
  use def_domain,         only :  npoiz,lpoiz
  use def_domain,         only :  npoin
  use def_domain,         only :  nzone,nzdom
  use def_master,         only :  npoi1,npoi2,npoi3
  use def_solver,         only :  SOL_SOLVER_RICHARDSON
  use def_solver,         only :  memit
  use def_solver,         only :  direct_solver_typ
  use def_solver,         only :  direct_solver_init
  use def_solver,         only :  SOL_DIRECT_SOLVER_PASTIX
  use def_solver,         only :  SOL_DIRECT_SOLVER_ALYA
  use def_solver,         only :  SOL_DIRECT_SOLVER_MUMPS
  use mod_memory,         only :  memory_alloca
  use mod_memory,         only :  memory_deallo
  use mod_communications, only :  PAR_INTERFACE_NODE_EXCHANGE
  use mod_communications, only :  PAR_SUM   
  use mod_graphs,         only :  graphs_permut_metis_postordering
  use mod_graphs,         only :  graphs_copyij
  implicit none 
  private
  real(rp),      parameter :: zeror                     =  epsilon(1.0_rp)

  interface solver_scalar_product
     module procedure solver_scalar_product_1,&
          &           solver_scalar_product_2_3,&
          &           solver_scalar_product_2_4
  end interface solver_scalar_product 

  public :: solver_preprocess
  public :: solver_postprocess
  public :: solver_solve_system
  public :: solver_solve
  public :: solver_periodicity
  public :: solver_initialize_matrix_and_rhs
  public :: solver_scalar_product
  public :: solver_smvp 
  public :: solver_residual
  public :: solver_pastix
  public :: direct_solver_typ

contains

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Matrix vector product
  !> @details Compute the L2 norm of a nodal array ||r||
  !>
  !----------------------------------------------------------------------
  subroutine solver_array_norm2(nbvar,xx,xnorm2)
    integer(ip), intent(in)  :: nbvar
    real(rp),    intent(in)  :: xx(*)
    real(rp),    intent(out) :: xnorm2
    integer(ip)              :: ii

    xnorm2 = 0.0_rp
    if( INOTMASTER ) then
       !
       ! Loop over interior nodes
       !
       !$OMP PARALLEL DO SCHEDULE (STATIC)  &
       !$OMP DEFAULT  ( NONE )              &
       !$OMP PRIVATE  ( ii )                &       
       !$OMP SHARED   ( xx, npoi1, nbvar )  &
       !$OMP REDUCTION (+:xnorm2)      
       do ii = 1,npoi1*nbvar
          xnorm2  = xnorm2  + xx(ii) * xx(ii)
       end do
       !$OMP END PARALLEL DO
       ! 
       ! Loop over boundary nodes
       !
       do ii = (npoi2-1)*nbvar+1,npoi3*nbvar
          xnorm2  = xnorm2  + xx(ii) * xx(ii)
       end do
    end if
    !
    ! MPI communication
    !
    call PAR_SUM(xnorm2,'IN MY CODE')

    xnorm2 = sqrt(xnorm2)

  end subroutine solver_array_norm2

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Matrix vector product
  !> @details Compute the residual of an equation:
  !>          r = b - Ax
  !>          It is assumed that RHS has already been exchaned in parallel
  !>          INPUT
  !>            NBNODES .... Number of equations
  !>            NDOFN ...... Number of variables
  !>            AN ......... Matrix
  !>            BB ......... RHS
  !>            JA ......... List of elements
  !>            IA ......... Pointer to list of elements
  !>            XX ......... Vector of unknowns
  !>          OUTPUT
  !>            RR ......... Equation residual
  !>
  !----------------------------------------------------------------------

  subroutine solver_residual(nbnodes,nbvar,an,bb,ja,ia,uu,rr,rnorm2) 
    integer(ip),  intent(in)            :: nbnodes
    integer(ip),  intent(in)            :: nbvar
    real(rp),     intent(in)            :: an(*)
    real(rp),     intent(in)            :: bb(*)
    integer(ip),  intent(in)            :: ja(*)
    integer(ip),  intent(in)            :: ia(*)
    real(rp),     intent(in)            :: uu(*)
    real(rp),     intent(out), optional :: rr(*)
    real(rp),     intent(out), optional :: rnorm2
    integer(ip)                         :: ii
    real(rp),     allocatable           :: rr_loc(:)
    
    if( present(rr) ) then
       if( INOTMASTER ) then
          call solver_smvp(nbnodes,nbvar,nbvar,an,ja,ia,uu,rr) 
          call PAR_INTERFACE_NODE_EXCHANGE(nbvar,rr,'SUM','IN MY CODE')
          !$OMP PARALLEL DO SCHEDULE (STATIC)     &
          !$OMP DEFAULT  ( NONE )                 &
          !$OMP PRIVATE  ( ii )                   &       
          !$OMP SHARED   ( rr, bb, nbvar, nbnodes )  
          do ii = 1, nbvar * nbnodes
             rr(ii) = bb(ii) - rr(ii)
          end do
          !$OMP END PARALLEL DO
       end if
       if( present(rnorm2) ) call solver_array_norm2(nbvar,rr,rnorm2)
    else
       if( present(rnorm2) ) then
          if( INOTMASTER ) then
             allocate( rr_loc(nbvar*nbnodes) )             
             call solver_smvp(nbnodes,nbvar,nbvar,an,ja,ia,uu,rr_loc) 
             call PAR_INTERFACE_NODE_EXCHANGE(nbvar,rr_loc,'SUM','IN MY CODE')
             !$OMP PARALLEL DO SCHEDULE (STATIC)         &
             !$OMP DEFAULT  ( NONE )                     &
             !$OMP PRIVATE  ( ii )                       &       
             !$OMP SHARED   ( rr_loc, bb, nbvar, nbnodes )  
             do ii = 1, nbvar * nbnodes
                rr_loc(ii) = bb(ii) - rr_loc(ii)
             end do
             !$OMP END PARALLEL DO
             call solver_array_norm2(nbvar,rr_loc,rnorm2)
             deallocate( rr_loc )
          end if
       else
          call runend('SOLVER: RESIDUAL: NOTHING TO DO!')
       end if
    end if

  end subroutine solver_residual

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Matrix vector product
  !> @details Multiply a non symmetric matrix stored in BCSR by a vector
  !>          YY = A XX 
  !>          INPUT
  !>          NBNODES .... Number of equations
  !>            NBVAR ...... Number of variables
  !>            AN ......... Matrix
  !>            JA ......... List of elements
  !>            IA ......... Pointer to list of elements
  !>            XX ......... Vector
  !>          OUTPUT
  !>            YY ......... result vector
  !>
  !----------------------------------------------------------------------

  subroutine solver_scalar_product_1(nbvar,xx,yy,xxdotyy) 
    integer(ip), intent(in)           :: nbvar
    real(rp),    intent(in),  pointer :: xx(:)
    real(rp),    intent(in),  pointer :: yy(:)
    real(rp),    intent(out)          :: xxdotyy
    integer(ip)                       :: ii

    xxdotyy = 0.0_rp
   
    do ii = 1,nbvar*npoi1
       xxdotyy = xxdotyy + xx(ii) * yy(ii)
    end do
    do ii = (npoi2-1)*nbvar+1,npoi3*nbvar
       xxdotyy  = xxdotyy  + xx(ii) * yy(ii)
    end do

    call PAR_SUM(xxdotyy,'IN MY CODE')

  end subroutine solver_scalar_product_1
 
  subroutine solver_scalar_product_2_3(nbvar,xx,yy,zz,xxdotyyzz,wsynch) 
    integer(ip), intent(in)            :: nbvar
    real(rp),    intent(in),  pointer  :: xx(:)
    real(rp),    intent(in),  pointer  :: yy(:)
    real(rp),    intent(in),  pointer  :: zz(:)
    real(rp),    intent(out)           :: xxdotyyzz(2)
    character(*),intent(in),  optional :: wsynch
    integer(ip)                        :: ii
    logical(lg)                        :: asynch
    logical(lg)                        :: all_reduce

    xxdotyyzz  = 0.0_rp
    asynch     = .false.
    all_reduce = .true.
    if( present(wsynch) ) then
       if( trim(wsynch) == 'NON BLOCKING' ) then
          asynch = .true.
       else if( trim(wsynch) == 'DO NOT REDUCE' ) then
          all_reduce = .false.
       end if
    end if

    if( INOTMASTER ) then
       !
       ! Loop over interior nodes
       !
       do ii = 1,nbvar*npoi1
          xxdotyyzz(1) = xxdotyyzz(1) + xx(ii) * yy(ii)
          xxdotyyzz(2) = xxdotyyzz(2) + xx(ii) * zz(ii)
       end do
       !
       ! Loop over own boundary nodes
       !
       do ii = (npoi2-1)*nbvar+1,npoi3*nbvar
          xxdotyyzz(1) = xxdotyyzz(1) + xx(ii) * yy(ii)
          xxdotyyzz(2) = xxdotyyzz(2) + xx(ii) * zz(ii)
       end do
    end if

    if( all_reduce .and. IPARALL ) then
       if( asynch ) then
          call PAR_SUM(2_ip,xxdotyyzz,'IN MY CODE',wsynch) 
       else
          call PAR_SUM(2_ip,xxdotyyzz,'IN MY CODE')
       end if
    end if

  end subroutine solver_scalar_product_2_3
 
  subroutine solver_scalar_product_2_4(nbvar,ww,xx,yy,zz,wwxx_yyzz,wsynch) 
    integer(ip), intent(in)            :: nbvar
    real(rp),    intent(in),  pointer  :: ww(:)
    real(rp),    intent(in),  pointer  :: xx(:)
    real(rp),    intent(in),  pointer  :: yy(:)
    real(rp),    intent(in),  pointer  :: zz(:)
    real(rp),    intent(out)           :: wwxx_yyzz(2)
    character(*),intent(in),  optional :: wsynch
    integer(ip)                        :: ii
    logical(lg)                        :: asynch
    logical(lg)                        :: all_reduce

    wwxx_yyzz  = 0.0_rp
    asynch     = .false.
    all_reduce = .true.
    if( present(wsynch) ) then
       if( trim(wsynch) == 'NON BLOCKING' ) then
          asynch = .true.
       else if( trim(wsynch) == 'DO NOT REDUCE' ) then
          all_reduce = .false.
       end if
    end if

    if( INOTMASTER ) then
       !
       ! Loop over interior nodes
       !
       do ii = 1,nbvar*npoi1
          wwxx_yyzz(1) = wwxx_yyzz(1) + ww(ii) * xx(ii)
          wwxx_yyzz(2) = wwxx_yyzz(2) + yy(ii) * zz(ii)
       end do
       !
       ! Loop over own boundary nodes
       !
       do ii = (npoi2-1)*nbvar+1,npoi3*nbvar
          wwxx_yyzz(1) = wwxx_yyzz(1) + ww(ii) * xx(ii)
          wwxx_yyzz(2) = wwxx_yyzz(2) + yy(ii) * zz(ii)
       end do
    end if

    if( all_reduce .and. IPARALL ) then
       if( asynch ) then
          call PAR_SUM(2_ip,wwxx_yyzz,'IN MY CODE',wsynch) 
       else
          call PAR_SUM(2_ip,wwxx_yyzz,'IN MY CODE')
       end if
    end if

  end subroutine solver_scalar_product_2_4
 
  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Matrix vector product
  !> @details Multiply a non symmetric matrix stored in BCSR by a vector
  !>          YY = A XX 
  !>          INPUT
  !>          NBNODES .... Number of equations
  !>            NBVAR ...... Number of variables
  !>            AN ......... Matrix
  !>            JA ......... List of elements
  !>            IA ......... Pointer to list of elements
  !>            XX ......... Vector
  !>          OUTPUT
  !>            YY ......... result vector
  !>
  !----------------------------------------------------------------------

  subroutine solver_smvp(nbnodes,ndof1,ndof2,an,ja,ia,xx,yy) 
    integer(ip),  intent(in)           :: nbnodes
    integer(ip),  intent(in)           :: ndof1
    integer(ip),  intent(in)           :: ndof2
    real(rp),     intent(in)           :: an(ndof1,ndof2,*)
    integer(ip),  intent(in)           :: ja(*),ia(*)
    real(rp),     intent(in)           :: xx(ndof1,*)
    real(rp),     intent(out)          :: yy(ndof2,*)
    integer(ip)                        :: ii,jj,kk,ll,col
    real(rp)                           :: raux,raux1,raux2,raux3

    if( INOTMASTER ) then

       if( ndof1 == 1 .and. ndof2 == 1 ) then
          !
          ! NBVAR=1
          !
          !$OMP PARALLEL DO SCHEDULE (DYNAMIC)   &
          !$OMP DEFAULT  ( NONE )                &
          !$OMP PRIVATE  ( ii, jj, col, raux )   &       
          !$OMP SHARED   ( xx, yy, an, nbnodes ) 
          do ii = 1,nbnodes
             yy(1,ii) = 0.0_rp
             do jj   = ia(ii),ia(ii+1)-1
                col  = ja(jj)
                raux = xx(1,col)
                yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux
             end do
          end do
          !$OMP END PARALLEL DO

       else if( ndof1 == 2 .and. ndof2 == 2 ) then
          !
          ! NBVAR=2
          !
          !$OMP PARALLEL DO SCHEDULE (DYNAMIC)         &
          !$OMP DEFAULT  ( NONE )                      &
          !$OMP PRIVATE  ( ii, jj, col, raux1, raux2 ) &       
          !$OMP SHARED   ( xx, yy, an, nbnodes ) 
          do ii = 1,nbnodes
             yy(1,ii) = 0.0_rp
             yy(2,ii) = 0.0_rp
             do jj       = ia(ii),ia(ii+1)-1
                col      = ja(jj)
                raux1    = xx(1,col)
                raux2    = xx(2,col)
                yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
                yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
                yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
                yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
             end do
          end do
          !$OMP END PARALLEL DO

       else if( ndof1 == 3 .and. ndof2 == 3 ) then
          !
          ! NBVAR=3
          !
          !$OMP PARALLEL DO SCHEDULE (DYNAMIC)                &
          !$OMP DEFAULT  ( NONE )                             &
          !$OMP PRIVATE  ( ii, jj, col, raux1, raux2, raux3 ) &       
          !$OMP SHARED   ( xx, yy, an, nbnodes ) 
          do ii = 1,nbnodes
             yy(1,ii) = 0.0_rp
             yy(2,ii) = 0.0_rp
             yy(3,ii) = 0.0_rp
             do jj       = ia(ii),ia(ii+1)-1
                col      = ja(jj)
                raux1    = xx(1,col)
                raux2    = xx(2,col)
                raux3    = xx(3,col)
                yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
                yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
                yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux3
                yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
                yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
                yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux3
                yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux1
                yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux2
                yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux3
             end do
          end do
          !$OMP END PARALLEL DO

       else if( ndof1 == 4 .and. ndof2 == 4 ) then
          !
          ! NBVAR=4
          !
          !$OMP PARALLEL DO SCHEDULE (DYNAMIC)   &
          !$OMP DEFAULT  ( NONE )                &
          !$OMP PRIVATE  ( ii, jj, col, raux )   &       
          !$OMP SHARED   ( xx, yy, an, nbnodes ) 
          do ii = 1,nbnodes
             yy(1,ii) = 0.0_rp
             yy(2,ii) = 0.0_rp
             yy(3,ii) = 0.0_rp
             yy(4,ii) = 0.0_rp
             do jj       = ia(ii),ia(ii+1)-1
                col      = ja(jj)
                raux     = xx(1,col)
                yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux
                yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux
                yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux
                yy(4,ii) = yy(4,ii) + an(1,4,jj) * raux
                raux     = xx(2,col)
                yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux
                yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux
                yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux
                yy(4,ii) = yy(4,ii) + an(2,4,jj) * raux
                raux     = xx(3,col)
                yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux
                yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux
                yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux
                yy(4,ii) = yy(4,ii) + an(3,4,jj) * raux
                raux     = xx(4,col)
                yy(1,ii) = yy(1,ii) + an(4,1,jj) * raux
                yy(2,ii) = yy(2,ii) + an(4,2,jj) * raux
                yy(3,ii) = yy(3,ii) + an(4,3,jj) * raux
                yy(4,ii) = yy(4,ii) + an(4,4,jj) * raux
             end do
          end do
          !$OMP END PARALLEL DO

       else
          !
          ! NBVAR = whatever
          !
          !$OMP PARALLEL DO SCHEDULE (DYNAMIC)               &
          !$OMP DEFAULT  ( NONE )                            &
          !$OMP PRIVATE  ( ii, jj, ll, col, raux )           &       
          !$OMP SHARED   ( xx, yy, an, ndof1, ndof2, nbnodes ) 
          do ii = 1,nbnodes
             yy(1:ndof2,ii) = 0.0_rp
             do jj  = ia(ii),ia(ii+1)-1
                col = ja(jj)
                do ll = 1,ndof1 
                   raux = xx(ll,col)
                   yy(1:ndof2,ii) = yy(1:ndof2,ii) + an(ll,1:ndof2,jj) * raux
                end do
             end do
          end do
          !$OMP END PARALLEL DO

       end if
       !
       ! MPI exchange
       !
       call PAR_INTERFACE_NODE_EXCHANGE(ndof2,yy,'SUM','IN MY ZONE','SYNCHRONOUS')

    end if

  end subroutine solver_smvp

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Solve a system including pre and postprocess
  !> @details Solve a system:
  !>          1. Preprocess: periodicity, reaction force, Neumann b.c.
  !>             and Dirichlet b.c. on A,b
  !>          2. Solve Au=b
  !>          3. Postprocess: reaction
  !>
  !----------------------------------------------------------------------

  subroutine solver_solve(solve,A,b,u,M)
    type(soltyp),      intent(inout),    pointer  :: solve(:)
    real(rp),          intent(inout)           :: A(*)
    real(rp),          intent(inout)           :: b(*)
    real(rp),          intent(inout)           :: u(*)
    real(rp),          intent(inout), optional :: M(*)

    if( solve(1) % kfl_version == 1 ) call solver_preprocess(solve,A,b,u)
    if( present(M) ) then
       call solver_solve_system(solve,A,b,u,M)
    else
       call solver_solve_system(solve,A,b,u)
    end if
    if( solve(1) % kfl_version == 1 ) call solver_postprocess(solve,A,b,u)

  end subroutine solver_solve

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Solve a system
  !> @details Solve the system Au=b. M can be a preconditioner
  !>
  !----------------------------------------------------------------------

  subroutine solver_solve_system(solve,A,b,u,M)
    type(soltyp),      intent(in),    pointer  :: solve(:)
    real(rp),          intent(inout)           :: A(*)
    real(rp),          intent(inout)           :: b(*)
    real(rp),          intent(inout)           :: u(*)
    real(rp),          intent(inout), optional :: M(*)
    real(rp)                                   :: dummr(2)

    if( present(M) ) then
       call solver(b,u,A,M)
    else
       call solver(b,u,A,dummr)
    end if

  end subroutine solver_solve_system

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Solver preprocess
  !> @details Preprocess the matrix and RHS A,b to account for:\n
  !>          1. Impose periodicity \n
  !>          2. Save matrix and RHS for reaction force \n
  !>          3. Impose Neumann boundary conditionsAssemble reaction \n
  !>          4. Impose Dirichlet boundary conditions \n
  !>
  !----------------------------------------------------------------------

  subroutine solver_preprocess(solve,A,b,u,P)
    type(soltyp), intent(inout),    pointer  :: solve(:)
    real(rp),     intent(inout)              :: A(*)
    real(rp),     intent(inout)              :: b(*)
    real(rp),     intent(inout)              :: u(*)
    real(rp),     intent(inout),    optional :: P(*)
    integer(ip)                              :: ndof1,ndof2
    integer(ip)                              :: pointer_a11
    integer(ip)                              :: pointer_a12
    integer(ip)                              :: pointer_a21
    integer(ip)                              :: pointer_a22
    integer(ip)                              :: pointer_b1
    integer(ip)                              :: pointer_b2
    integer(ip)                              :: num_blocks
    real(rp)                                 :: dummr(3)
    character(14)                            :: matrix_rhs

    if( INOTMASTER ) then

       num_blocks = solve(1) % num_blocks
       if( solve(1) % kfl_algso == SOL_SOLVER_RICHARDSON ) then
          matrix_rhs = 'RHS'
       else
          matrix_rhs = 'MATRIX AND RHS'
       end if

       !-----------------------------------------------------------------
       !
       ! 1. Impose periodicity 
       ! 2. Save matrix and RHS for reaction force
       ! 3. Assemble reaction
       ! 4. Impose boundary conditions
       !
       !-----------------------------------------------------------------

       if( num_blocks == 1 ) then

          ndof1 = solve(1) % ndofn
          ! 
          ! 1. Periodicity
          !
          call solver_periodicity(trim(matrix_rhs),solve,ndof1,ndof1,A,b,u)
          !
          ! 2. Reaction force: save matrix and RHS
          !
          call solver_reaction_force_1by1('SAVE SYSTEM',solve,ndof1,A,b,u)
          !
          ! 3. Assemble reaction force
          !
          call solver_reaction_force_1by1('ASSEMBLE REACTION',solve,ndof1,A,b,u)
          !
          ! 4. Impose Dirichlet
          !
          call solver_preprocess_dirichlet_1by1(trim(matrix_rhs),solve,ndof1,A,b,u)

       else if( num_blocks == 2 ) then
          !
          ! Check dimensions
          !
          ndof1       = solve(1) % block_dimensions(1)
          ndof2       = solve(1) % block_dimensions(2)
          pointer_a11 = 1
          pointer_a12 = pointer_a11 + nzdom * ndof1 * ndof1
          pointer_a21 = pointer_a12 + nzdom * ndof1 * ndof2 
          pointer_a22 = pointer_a21 + nzdom * ndof2 * ndof1
          pointer_b1  = 1
          pointer_b2  = pointer_b1  + npoin * ndof1 
          !
          ! 1. Periodicity
          !
          call solver_periodicity('MATRIX AND RHS',solve,ndof1,ndof1,A(pointer_a11),b(pointer_b1),u(pointer_b1))
          call solver_periodicity('MATRIX',        solve,ndof1,ndof2,A(pointer_a12),b(pointer_b1),u(pointer_b1))
          call solver_periodicity('MATRIX',        solve,ndof2,ndof1,A(pointer_a21),b(pointer_b2),u(pointer_b2))
          call solver_periodicity('MATRIX AND RHS',solve,ndof2,ndof2,A(pointer_a22),b(pointer_b2),u(pointer_b2))
          if( present(P) ) then
             call solver_periodicity('MATRIX',solve,ndof2,ndof2,P,dummr,dummr)
          end if
          !
          ! 2. Reaction force: save matrix and RHS
          !
          call solver_reaction_force_2by2(&
               & 'SAVE SYSTEM',solve,ndof1,ndof2,&
               & A(pointer_a11),A(pointer_a12),A(pointer_a21),A(pointer_a22),&
               & b(pointer_b1), b(pointer_b2), u(pointer_b1), u(pointer_b2))
          !
          ! 3. Assemble reaction force
          !
          call solver_reaction_force_2by2(&
               & 'ASSEMBLE REACTION',solve,ndof1,ndof2,&
               & A(pointer_a11),A(pointer_a12),A(pointer_a21),A(pointer_a22),&
               & b(pointer_b1), b(pointer_b2), u(pointer_b1), u(pointer_b2))
          !
          ! 4. Dirichlet
          !
          ! Not coded for now: When periodicity and local axes, take basis of master for slaves
          !
       else

          call runend('SOLVER_PREPROCESS: NOT READY FOR THIS KIND OF BLOCK MATRIX')

       end if

    end if

  end subroutine solver_preprocess

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Initialization
  !> @details Initialize matrix and RHS
  !>
  !----------------------------------------------------------------------

  subroutine solver_initialize_matrix_and_rhs(solve,A,b,P)
    type(soltyp), intent(inout),    pointer  :: solve(:)
    real(rp),     intent(inout)              :: A(*)
    real(rp),     intent(inout)              :: b(*)
    real(rp),     intent(inout),    optional :: P(*)
    integer(ip)                              :: ndof1,ndof2
    integer(ip)                              :: num_blocks
    integer(ip)                              :: nzd11,nzd12,nzd21,nzd22
    integer(ip)                              :: nzdal,ii

    if( INOTMASTER ) then

       num_blocks = solve(1) % num_blocks

       if( num_blocks == 1 ) then
          !
          ! 1 block
          !
          do ii = 1,solve(1) % nzmat
             A(ii) = 0.0_rp
          end do
          do ii = 1,solve(1) % nunkn
             b(ii) = 0.0_rp 
          end do

       else if( num_blocks == 2 ) then
          !
          ! 2 blocks
          !
          ndof1 = solve(1) % block_dimensions(1)
          ndof2 = solve(1) % block_dimensions(2)
          nzd11 = solve(1) % nzmat  ! <- for now this is the total size of the system
          nzd12 = solve(1) % nzmat / ndof1 * ndof2 
          nzd21 = nzd12
          nzd22 = solve(2) % nzmat
          nzdal = nzd11 + nzd12 + nzd21 + nzd22
          do ii = 1,nzd11
             A(ii) = 0.0_rp 
          end do
          do ii = 1,solve(1) % nequa * ( ndof1 + ndof2 )
             b(ii) = 0.0_rp 
          end do

          if( present(P) ) then             
             do ii = 1,solve(2) % nzmat
                P(ii) = 0.0_rp
             end do
          end if

       else

          call runend('SOLVER_PREPROCESS: NOT READY FOR THIS KIND OF BLOCK MATRIX')

       end if

    end if

  end subroutine solver_initialize_matrix_and_rhs

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Solver postprocess
  !> @details Posprocess the matrix RHS and u to account for: \n
  !>          1. Periodicity: recover solution on slave nodes \n
  !>          2. Compute reaction \n
  !>
  !----------------------------------------------------------------------

  subroutine solver_postprocess(solve,A,b,u)
    type(soltyp),      intent(inout),    pointer  :: solve(:)
    real(rp),          intent(inout)           :: A(*)
    real(rp),          intent(inout)           :: b(*)
    real(rp),          intent(inout)           :: u(*)
    integer(ip)                                :: ndof1,ndof2
    integer(ip)                                :: pointer_a11
    integer(ip)                                :: pointer_a12
    integer(ip)                                :: pointer_a21
    integer(ip)                                :: pointer_a22
    integer(ip)                                :: pointer_b1
    integer(ip)                                :: pointer_b2
    integer(ip)                                :: num_blocks
    real(rp)                                   :: dummr(3)
    character(14)                              :: matrix_rhs

    if( INOTMASTER ) then

       num_blocks = solve(1) % num_blocks
       if( solve(1) % kfl_algso == SOL_SOLVER_RICHARDSON ) then
          matrix_rhs = 'RHS'
       else
          matrix_rhs = 'MATRIX AND RHS'
       end if

       !-----------------------------------------------------------------
       !
       ! 1. Periodicity: recover solution on slave nodes
       ! 2. Compute reaction
       !
       !-----------------------------------------------------------------

       if( num_blocks == 1 ) then

          ndof1 = solve(1) % ndofn
          ! 
          ! 1. Periodicity
          !
          call solver_periodicity('SOLUTION',solve,ndof1,ndof1,A,b,u)
          !
          ! 2. Compute reaction force
          !
          call solver_reaction_force_1by1('REACTION FORCE',solve,ndof1,A,b,u)

       else if( num_blocks == 2 ) then
          ! 
          ! Define block dimensions and pointers
          !
          ndof1       = solve(1) % block_dimensions(1)
          ndof2       = solve(1) % block_dimensions(2)
          pointer_a11 = 1
          pointer_a12 = pointer_a11 + nzdom * ndof1 * ndof1
          pointer_a21 = pointer_a12 + nzdom * ndof1 * ndof2 
          pointer_a22 = pointer_a21 + nzdom * ndof2 * ndof1
          pointer_b1  = 1
          pointer_b2  = pointer_b1  + npoin * ndof1 
          !
          ! 1. Periodicity
          !
          call solver_periodicity('SOLUTION',solve,ndof1,ndof1,dummr,dummr,u(pointer_b1))
          call solver_periodicity('SOLUTION',solve,ndof2,ndof2,dummr,dummr,u(pointer_b2))
          !
          ! 2. Compute reaction force
          !
          call solver_reaction_force_2by2(&
               & 'REACTION FORCE',solve,ndof1,ndof2,&
               & A(pointer_a11),A(pointer_a12),A(pointer_a21),A(pointer_a22),&
               & b(pointer_b1), b(pointer_b2), u(pointer_b1), u(pointer_b2))

       else

          call runend('SOLVER_PREPROCESS: NOT READY FOR THIS KIND OF BLOCK MATRIX')

       end if

    end if

  end subroutine solver_postprocess

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    03/10/2014
  !> @brief   Impose periodicity
  !> @details Periodicity is imposed in two steps, as explaine below
  !>          \verbatim
  !> 
  !>               1 M 3 4 S 6 7 8             1 M 3 4 S 6 7 8             1 M 3 4 S 6 7 8  
  !>            +-                -+        +-                -+        +-                -+
  !>          1 |  x x             |      1 |  x x             |      1 |  x x             |
  !>          M |  x x x   ||      |      M |  x x x           |      M |  x x x X X X <=  |
  !>          3 |    x x x \/      |      3 |    x x x         |      3 |    x x x         |
  !>          4 |      x x x       |  =>  4 | => X x x 0       |  =>  4 |      x x x       |
  !>          S |        x x x     |      S |        x x x     |      S |        0 0 0 <=  |
  !>          6 |     <--- x x x   |      6 | => X     0 x x   |      6 |          x x x   |
  !>          7 |     move   x x x |      7 |            x x x |      7 |            x x x |
  !>          8 |              x x |      8 |              x x |      8 |              x x |
  !>            +-                -+        +-                -+        +-                -+
  !>
  !>            Original matrix             For nodes who are not       Move slave's rows to 
  !>                                        slaves, move slave's        their master's rows
  !>                                        column to master's one
  !>           \endverbatim    
  !>          
  !>           Comments:
  !>           - The master is always in the subdomains where it's slave is
  !>           - The master is in the inter-partition boundary list, slave may not
  !>
  !----------------------------------------------------------------------

  subroutine solver_periodicity(wtask,solve,ndof1,ndof2,A,b,u)
    character(*),      intent(in)             :: wtask
    !type(soltyp),      intent(in),    pointer :: solve(:)
    type(soltyp),      intent(in)             :: solve(*)
    integer(ip),       intent(in)             :: ndof1
    integer(ip),       intent(in)             :: ndof2
    real(rp),          intent(inout)          :: A(ndof2,ndof1,nzdom)
    real(rp),          intent(inout)          :: b(ndof1,npoin)
    real(rp),          intent(inout)          :: u(ndof1,npoin)
    integer(ip)                               :: izdom,jzdom,ifoun
    integer(ip)                               :: ipoin,jpoin,kpoin,jj,ii
    integer(ip)                               :: iperi,kzdom,lpoin
    integer(ip),                      pointer :: list_of_masters(:)
    logical(lg),                      pointer :: in_my_zone(:)
    logical(lg)                               :: periodicity_matrix
    logical(lg)                               :: periodicity_rhs
    logical(lg)                               :: periodicity_solution

    if( IMASTER )    return
    if( nperi == 0 ) return
    nullify( list_of_masters )
    nullify( in_my_zone )
    !
    ! Decide what to do
    !
    select case ( trim(wtask) )
    case ( 'RHS' ) 
       periodicity_matrix   = .false.
       periodicity_rhs      = .true.
       periodicity_solution = .false.
    case ( 'MATRIX' ) 
       periodicity_matrix   = .true.
       periodicity_rhs      = .false.
       periodicity_solution = .false.
    case ( 'SOLUTION' ) 
       periodicity_matrix   = .false.
       periodicity_rhs      = .false.
       periodicity_solution = .true.
    case ( 'MATRIX AND RHS' ) 
       periodicity_matrix   = .true.
       periodicity_rhs      = .true.
       periodicity_solution = .false.
    case default
       call runend('PERIODICITY: DO NOT KNOW WHAT TO DO')
    end select

    if( periodicity_rhs .and. ( .not. periodicity_matrix .and. .not. periodicity_solution ) ) then

       !--------------------------------------------------------------
       !
       ! Only RHS
       !
       !--------------------------------------------------------------

       if( nzone > 1 ) then 
          !do kpoin = 1,npoiz(current_zone)
          !   jpoin = lpoiz(current_zone) % l(kpoin)
          !   ipoin = lmast(jpoin)
          !   if( ipoin > 0 ) then
          !      if( in_my_zone(ipoin) .and. in_my_zone(jpoin) ) then
          !         b(1:ndof1,ipoin) = b(1:ndof1,ipoin) + b(1:ndof1,jpoin)
          !         b(1:ndof1,jpoin) = b(1:ndof1,ipoin)
          !      end if
          !   end if
          !end do
          call memory_alloca(memit,'IN_MY_ZONE','solver_periodicity',in_my_zone,npoin)
          do kpoin = 1,npoiz(current_zone)
             ipoin = lpoiz(current_zone) % l(kpoin)
             in_my_zone(ipoin) = .true.
          end do
          do iperi = 1,nperi
             ipoin = lperi(1,iperi)
             jpoin = lperi(2,iperi)
             if ( ipoin > 0 .and. jpoin > 0 ) then
                if( in_my_zone(ipoin) .and. in_my_zone(jpoin) ) then       
                   b(1:ndof1,ipoin) = b(1:ndof1,ipoin) + b(1:ndof1,jpoin)
                   b(1:ndof1,jpoin) = b(1:ndof1,ipoin)
                end if
             end if
          end do
          call memory_deallo(memit,'IN_MY_ZONE','solver_periodicity',in_my_zone)
       else
          !do jpoin = 1,npoin
          !   ipoin = lmast(jpoin)
          !   if( ipoin > 0 ) then
          !      b(1:ndof1,ipoin) = b(1:ndof1,ipoin) + b(1:ndof1,jpoin)
          !      b(1:ndof1,jpoin) = b(1:ndof1,ipoin)
          !   end if
          !end do
          do iperi = 1,nperi
             ipoin = lperi(1,iperi)
             jpoin = lperi(2,iperi)
             if( ipoin > 0 .and. jpoin > 0 ) then                 
                b(1:ndof1,ipoin) = b(1:ndof1,ipoin) + b(1:ndof1,jpoin)
                b(1:ndof1,jpoin) = b(1:ndof1,ipoin)
             end if
          end do
       end if

    else if( periodicity_matrix ) then

       !--------------------------------------------------------------
       !
       ! Matrix and/or RHS 
       !
       !--------------------------------------------------------------
       !
       ! Check errors
       !
       if( solve(1) % kfl_algso == 0 ) then
          call runend('PERIODICITY ONLY POSSIBLE WITH SPARSE SOLVERS')
       else if( solve(1) % kfl_symme == 1 ) then
          call runend('MATRIX PERIODICITY ONLY POSSIBLE WITH ASYMMETRIC SOLVERS')
       end if
       !
       ! Put all slaves' columns into master's column
       ! KPOIN is neither master nor slave
       ! LPERI(2,:) = JPOIN is slave
       ! LPERI(1,:) = IPOIN is JPOIN's master
       ! The master is in all the partitions where slave is present, so the entire
       ! row of the slave can be assembled into the master's row
       !
       call memory_alloca(memit,'IN_MY_ZONE',     'solver_periodicity',in_my_zone,npoin)
       call memory_alloca(memit,'LIST_OF_MASTERS','solver_periodicity',list_of_masters,npoin)
       do kpoin = 1,npoiz(current_zone)
          ipoin = lpoiz(current_zone) % l(kpoin)
          in_my_zone(ipoin) = .true.
       end do
       do iperi = 1,nperi
          ipoin = lperi(1,iperi)
          jpoin = lperi(2,iperi)
          if ( ipoin <= 0 .or. jpoin <= 0 ) then
             in_my_zone(abs(ipoin)) = .false.
          end if
       end do

       do iperi = 1,nperi
          ipoin = lperi(1,iperi)
          jpoin = lperi(2,iperi)
          if ( ipoin > 0 .and. jpoin > 0 ) then 
             if( in_my_zone(ipoin) .and. in_my_zone(jpoin) ) list_of_masters(jpoin) = ipoin
             !list_of_masters(jpoin) = ipoin ! Ask eva
          end if
       end do
       !
       ! Move slave's column to master's column
       !
       do lpoin = 1,npoiz(current_zone)
          kpoin = lpoiz(current_zone) % l(lpoin)

          if( list_of_masters(kpoin) == 0 ) then
             do kzdom = r_dom(kpoin),r_dom(kpoin+1)-1
                jpoin = c_dom(kzdom)
                if( list_of_masters(jpoin) > 0 .and. in_my_zone(jpoin) ) then
                   ipoin = list_of_masters(jpoin)                  
                   ifoun = 0
                   izdom2: do izdom = r_dom(kpoin),r_dom(kpoin+1)-1
                      if( c_dom(izdom) == ipoin ) then
                         ifoun = 1

                         do ii = 1,ndof2
                            do jj = 1,ndof1 
                               A(ii,jj,izdom) = A(ii,jj,izdom) + A(ii,jj,kzdom)
                               A(ii,jj,kzdom) = 0.0_rp
                            end do
                         end do

                         exit izdom2
                      end if
                   end do izdom2
                   if( ifoun == 0 ) then
                      print*,'NOT FOUND NODE, SLAVE, MASTER A=',kpoin,jpoin,ipoin
                      call runend('PRESOL 2: NODE NOT FOUND')          
                   end if
                end if

             end do

          end if
       end do
       !
       ! Put all slaves' rows into master's row
       ! Put slave row to zero
       ! JPOIN:  slave
       ! IPOIN:  master
       ! Slave:  coef. JZDOM for JPOIN-KPOIN
       ! Master: coef. IZOMD for IPOIN-KPOIN
       !
       do lpoin = 1,npoiz(current_zone)
          jpoin = lpoiz(current_zone) % l(lpoin)
          ipoin = list_of_masters(jpoin)
          if( ipoin > 0 ) then
             if( periodicity_rhs ) then
                b(1:ndof1,ipoin) = b(1:ndof1,ipoin) + b(1:ndof1,jpoin)
                b(1:ndof1,jpoin) = 0.0_rp                
             end if
             do jzdom = r_dom(jpoin),r_dom(jpoin+1)-1
                kpoin = c_dom(jzdom)
                if( in_my_zone(kpoin) ) then
                   if( list_of_masters(kpoin) > 0 ) kpoin = list_of_masters(kpoin)
                   ifoun = 0
                   izdom1: do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                      if( c_dom(izdom) == kpoin ) then
                         ifoun = 1
                         do ii = 1,ndof2
                            do jj = 1,ndof1 
                               A(ii,jj,izdom) = A(ii,jj,izdom) + A(ii,jj,jzdom)
                               A(ii,jj,jzdom) = 0.0_rp
                            end do
                         end do
                         exit izdom1
                      end if
                   end do izdom1
                   if( ifoun == 0 .and. ipoin /= kpoin ) then
                      !print*,'NOT FOUND NODE, SLAVE, MASTER B=',kfl_paral,lninv_loc(kpoin),lninv_loc(jpoin),lninv_loc(ipoin)
                      call runend('PRESOL 1: NODE NOT FOUND')
                   end if
                end if
             end do
          end if
       end do

       call memory_deallo(memit,'IN_MY_ZONE',     'solver_periodicity',in_my_zone)
       call memory_deallo(memit,'LIST_OF_MASTERS','solver_periodicity',list_of_masters)

    else if( periodicity_solution ) then

       !--------------------------------------------------------------
       !
       ! Solution
       !
       !--------------------------------------------------------------

       if( nzone > 1 ) then  
          call memory_alloca(memit,'IN_MY_ZONE','solver_periodicity',in_my_zone,npoin)
          do kpoin = 1,npoiz(current_zone)
             ipoin = lpoiz(current_zone) % l(kpoin)
             in_my_zone(ipoin) = .true.
          end do
          do iperi = 1,nperi
             jpoin = lperi(2,iperi)
             ipoin = lperi(1,iperi) 
             if( ipoin > 0 .and. jpoin > 0 ) then
                if( in_my_zone(ipoin) .and. in_my_zone(jpoin) ) then
                   u(1:ndof1,jpoin) = u(1:ndof1,ipoin)
                end if
             end if
          end do
          call memory_deallo(memit,'IN_MY_ZONE','solver_periodicity',in_my_zone)
       else
          do iperi = 1,nperi
             jpoin = lperi(2,iperi)
             ipoin = lperi(1,iperi)     
             if( ipoin > 0 .and. jpoin > 0 ) u(1:ndof1,jpoin) = u(1:ndof1,ipoin)
          end do
       end if

    end if

  end subroutine solver_periodicity

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    03/10/2014
  !> @brief   Save matrix and RHS to compute reaction forces
  !> @details Save matrix and RHS to compute reaction forces
  !>
  !----------------------------------------------------------------------

  subroutine solver_reaction_force_1by1(wtask,solve,ndof1,A11,b1,u1)

use def_domain, only : coord
    character(*),      intent(in)             :: wtask
    type(soltyp),      intent(inout),    pointer :: solve(:)
    integer(ip),       intent(in)             :: ndof1
    real(rp),          intent(inout)          :: A11(ndof1,ndof1,*)
    real(rp),          intent(inout)          :: b1(ndof1,*)
    real(rp),          intent(inout)          :: u1(ndof1,*)
    integer(ip)                               :: izdom,jzdom,lpoin,ipoin
    integer(ip)                               :: idofn,jdofn,jpoin

    if( associated(solve(1) % lpoin_reaction) ) then

       if( solve(1) % num_blocks /= 1 ) call runend('1BY1: WRONG NUMBER OF BLOCKS')

       select case ( trim(wtask) )

       case ( 'SAVE SYSTEM' )
          !
          ! Save system: matrix and RHS at reaction nodes
          !
          do lpoin = 1,npoiz(current_zone)
             ipoin = lpoiz(current_zone) % l(lpoin)
             if( solve(1) % lpoin_reaction(ipoin) ) then
                solve(1) % lpoin_block(ipoin) % block1_num(1) % rhs(1:ndof1) = b1(1:ndof1,ipoin) 
                jzdom = 0
                do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                   jzdom = jzdom + 1
                   solve(1) % lpoin_block(ipoin) % block2_num(1,1) % matrix(1:ndof1,1:ndof1,jzdom) = A11(1:ndof1,1:ndof1,izdom)
                end do
             end if
          end do

       case ( 'REACTION FORCE' )
          !
          ! Compute reaction force
          !         
          if( solve(1) % kfl_react == 1 ) then

             do lpoin = 1,npoiz(current_zone)
                ipoin = lpoiz(current_zone) % l(lpoin)
                solve(1) % reaction(1:ndof1,ipoin) = 0.0_rp
                if( solve(1) % lpoin_reaction(ipoin) ) then
                   jzdom = 0
                   solve(1) % reaction(1:ndof1,ipoin) = solve(1) % lpoin_block(ipoin) % block1_num(1) % rhs(1:ndof1)
                   do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                      jpoin = c_dom(izdom)
                      jzdom = jzdom + 1
                      do idofn = 1,ndof1
                         do jdofn = 1,ndof1
                            solve(1) % reaction(idofn,ipoin) = solve(1) % reaction(idofn,ipoin) &
                                 & - solve(1) % lpoin_block(ipoin) % block2_num(1,1) % matrix(jdofn,idofn,jzdom) * u1(jdofn,jpoin)
                         end do
                      end do
                   end do
                end if
             end do
             call PAR_INTERFACE_NODE_EXCHANGE(solve(1) % reaction,'SUM','IN MY ZONE')

          end if

       case ( 'ASSEMBLE REACTION' )
          !
          ! Assemble reaction
          !         
          if( solve(1) % kfl_bvnat == 1 ) then
             if( solve(1) % kfl_iffix /= 0 ) then     
                do lpoin = 1,npoiz(current_zone)
                   ipoin = lpoiz(current_zone) % l(lpoin)
                   do idofn = 1,ndof1
                      if( solve(1) % kfl_fixno(idofn,ipoin) <= 0 ) then
                         b1(idofn,ipoin) = b1(idofn,ipoin) + solve(1) % bvnat(idofn,ipoin) 
                      end if
                   end do
                end do
             else 
                do lpoin = 1,npoiz(current_zone)
                   ipoin = lpoiz(current_zone) % l(lpoin)
                   b1(1:ndof1,ipoin) = b1(1:ndof1,ipoin) + solve(1) % bvnat(1:ndof1,ipoin) 
                end do
             end if
          end if

       case default
          !
          ! Do not know what to do
          !
          call runend('1BY1: DOES KNOW WHAT TO DO')

       end select

    end if

  end subroutine solver_reaction_force_1by1

  subroutine solver_reaction_force_2by2(wtask,solve,ndof1,ndof2,A11,A12,A21,A22,b1,b2,u1,u2)
    character(*), intent(in)             :: wtask
    type(soltyp), intent(inout), pointer :: solve(:)
    integer(ip),  intent(in)             :: ndof1
    integer(ip),  intent(in)             :: ndof2
    real(rp),     intent(inout)          :: A11(ndof1,ndof1,*)
    real(rp),     intent(inout)          :: A12(ndof2,ndof1,*)
    real(rp),     intent(inout)          :: A21(ndof1,ndof2,*)
    real(rp),     intent(inout)          :: A22(ndof2,ndof2,*)
    real(rp),     intent(inout)          :: b1(ndof1,*)
    real(rp),     intent(inout)          :: b2(ndof2,*)
    real(rp),     intent(inout)          :: u1(ndof1,*)
    real(rp),     intent(inout)          :: u2(ndof2,*)
    integer(ip)                          :: izdom,jzdom,lpoin,ipoin
    integer(ip)                          :: jpoin,jdofn,idofn

    if( associated(solve(1) % lpoin_reaction) ) then

       if( solve(1) % num_blocks /= 2 ) call runend('2BY2: WRONG NUMBER OF BLOCKS')

       select case ( trim(wtask) )

       case ( 'SAVE SYSTEM' )
          !
          ! Save system: matrix and RHS at reaction nodes
          !
          do lpoin = 1,npoiz(current_zone)
             ipoin = lpoiz(current_zone) % l(lpoin)
             if( solve(1) % lpoin_reaction(ipoin) ) then
                solve(1) % lpoin_block(ipoin) % block1_num(1) % rhs(1:ndof1) = b1(1:ndof1,ipoin) 
                solve(1) % lpoin_block(ipoin) % block1_num(2) % rhs(1:ndof2) = b2(1:ndof2,ipoin) 
                jzdom = 0
                do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                   jzdom = jzdom + 1
                   solve(1) % lpoin_block(ipoin) % block2_num(1,1) % matrix(1:ndof1,1:ndof1,jzdom) = A11(1:ndof1,1:ndof1,izdom)
                   solve(1) % lpoin_block(ipoin) % block2_num(1,2) % matrix(1:ndof2,1:ndof1,jzdom) = A12(1:ndof2,1:ndof1,izdom)
                   solve(1) % lpoin_block(ipoin) % block2_num(2,1) % matrix(1:ndof1,1:ndof2,jzdom) = A21(1:ndof1,1:ndof2,izdom)
                   solve(1) % lpoin_block(ipoin) % block2_num(2,2) % matrix(1:ndof2,1:ndof2,jzdom) = A22(1:ndof2,1:ndof2,izdom)
                end do
             end if
          end do

       case ( 'REACTION FORCE' )
          !
          ! Compute reaction force
          !         
          if( solve(1) % kfl_react == 1 ) then

             do lpoin = 1,npoiz(current_zone)
                ipoin = lpoiz(current_zone) % l(lpoin)
                solve(1) % reaction(1:ndof1,ipoin) = 0.0_rp
                solve(2) % reaction(1:ndof2,ipoin) = 0.0_rp
                if( solve(1) % lpoin_reaction(ipoin) ) then
                   jzdom = 0
                   solve(1) % reaction(1:ndof1,ipoin) = solve(1) % lpoin_block(ipoin) % block1_num(1) % rhs(1:ndof1)
                   solve(2) % reaction(1:ndof2,ipoin) = solve(1) % lpoin_block(ipoin) % block1_num(2) % rhs(1:ndof2)
                   do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                      jpoin = c_dom(izdom)
                      jzdom = jzdom + 1
                      do idofn = 1,ndof1
                         do jdofn = 1,ndof1
                            solve(1) % reaction(idofn,ipoin) = solve(1) % reaction(idofn,ipoin) &
                                 & - solve(1) % lpoin_block(ipoin) % block2_num(1,1) % matrix(jdofn,idofn,jzdom) &
                                 & * u1(jdofn,jpoin)
                         end do
                         do jdofn = 1,ndof2
                            solve(1) % reaction(idofn,ipoin) = solve(1) % reaction(idofn,ipoin) &
                                 & - solve(1) % lpoin_block(ipoin) % block2_num(1,2) % matrix(jdofn,idofn,jzdom) &
                                 & * u2(jdofn,jpoin)
                         end do
                      end do
                      do idofn = 1,ndof2
                         do jdofn = 1,ndof1
                            solve(2) % reaction(idofn,ipoin) = solve(2) % reaction(idofn,ipoin) &
                                 & - solve(1) % lpoin_block(ipoin) % block2_num(2,1) % matrix(jdofn,idofn,jzdom) &
                                 & * u1(jdofn,jpoin)
                         end do
                         do jdofn = 1,ndof2
                            solve(2) % reaction(idofn,ipoin) = solve(2) % reaction(idofn,ipoin) &
                                 & - solve(1) % lpoin_block(ipoin) % block2_num(2,2) % matrix(jdofn,idofn,jzdom) &
                                 & * u2(jdofn,jpoin)
                         end do
                      end do
                   end do
                end if
             end do
             call PAR_INTERFACE_NODE_EXCHANGE(solve(1) % reaction,'SUM','IN MY ZONE')
             call PAR_INTERFACE_NODE_EXCHANGE(solve(2) % reaction,'SUM','IN MY ZONE')

          end if

       case ( 'ASSEMBLE REACTION' )
          !
          ! Assemble reaction
          !         
          if( solve(1) % kfl_bvnat == 1 ) then
             if( solve(1) % kfl_iffix /= 0 ) then     
                do lpoin = 1,npoiz(current_zone)
                   ipoin = lpoiz(current_zone) % l(lpoin)
                   do idofn = 1,ndof1
                      if( solve(1) % block_array(1) % kfl_fixno(idofn,ipoin) <= 0 ) then
                         b1(idofn,ipoin) = b1(idofn,ipoin) + solve(1) % block_array(1) % bvnat(idofn,ipoin) 
                      end if
                   end do
                   do idofn = 1,ndof2
                      if( solve(1) % block_array(2) % kfl_fixno(idofn,ipoin) <= 0 ) then
                         b2(idofn,ipoin) = b2(idofn,ipoin) + solve(1) % block_array(2) % bvnat(idofn,ipoin) 
                      end if
                   end do
                end do
             else 
                do lpoin = 1,npoiz(current_zone)
                   ipoin = lpoiz(current_zone) % l(lpoin)
                   b1(1:ndof1,ipoin) = b1(1:ndof1,ipoin) + solve(1) % block_array(1) % bvnat(1:ndof1,ipoin) 
                   b2(1:ndof2,ipoin) = b2(1:ndof2,ipoin) + solve(1) % block_array(2) % bvnat(1:ndof2,ipoin) 
                end do
             end if
          end if

       case default
          !
          ! Do not know what to do
          !
          call runend('1BY1: DOES KNOW WHAT TO DO')

       end select

    end if

  end subroutine solver_reaction_force_2by2

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    03/10/2014
  !> @brief   Impose Dirichlet b.c.
  !> @details Impose Dirichlet b.c. on RHS and matrix 
  !>
  !>          If SOLVE(1) % KFL_IFFIX = 1. For all IPOIN,
  !>
  !>            IF SOLVE(1) % KFL_FIXNO(:,IPOIN) > 0
  !>              - Residual-based solvers:
  !>                B1 (:,IPOIN) = 0
  !>              - Matrix-based solvers:
  !>                A11(:,:,IZDOM) = 0          
  !>                A11(1,1,IZDOM) = 1          
  !>                B1 (1,IPOIN)   = SOLVE(1) % BVESS(:,IPOIN)      
  !>                U1 (1,IPOIN)   = SOLVE(1) % BVESS(:,IPOIN)      
  !>            END IF
  !>
  !>          ELSE IF ( SOLVE(1) % KFL_IFFIX = 1 .and. .not. associated(SOLVE(1) % BVESS )
  !>
  !>            IF SOLVE(1) % KFL_FIXNO(:,IPOIN) > 0
  !>                A11(:,:,IZDOM) = 0          
  !>                A11(1,1,IZDOM) = 1          
  !>                B1 (1,IPOIN)   = SOLVE(1) % BVESS(:,IPOIN)      
  !>            END IF
  !>
  !>          ELSE If SOLVE(1) % KFL_IFFIX = 2
  !>
  !>            IF SOLVE(1) % KFL_FIXNO(:,IPOIN) > 0
  !>                A11(:,:,IZDOM) = 0          
  !>                A11(1,1,IZDOM) = 1 
  !>            END IF
  !>
  !>          END IF
  !>
  !----------------------------------------------------------------------

  subroutine solver_preprocess_dirichlet_1by1(wtask,solve,ndof1,A11,b1,u1)
    character(*),      intent(in)             :: wtask
    type(soltyp),      intent(in),    pointer :: solve(:)
    integer(ip),       intent(in)             :: ndof1
    real(rp),          intent(inout)          :: A11(ndof1,ndof1,*)
    real(rp),          intent(inout)          :: b1(ndof1,*)
    real(rp),          intent(inout)          :: u1(ndof1,*)
    integer(ip)                               :: lpoin,ipoin,idofn,jdofn
    integer(ip)                               :: izdod,izdom,jpoin,kpoin
    integer(ip)                               :: jzdom
    real(rp)                                  :: amatrd(ndof1)
    integer(ip),                      pointer :: kfl_fixno(:,:)
    real(rp),                         pointer :: bvess(:,:)

    nullify(kfl_fixno)
    nullify(bvess)
    if( solve(1) % kfl_iffix == 0 ) return
    kfl_fixno => solve(1) % kfl_fixno
    bvess     => solve(1) % bvess

    select case ( trim(wtask) )

    case ( 'ONLY RHS' )
       !
       ! Richardson residual-based solvers: put RHS=0 on Dirichlet nodes
       !
       do lpoin = 1,npoiz(current_zone)
          ipoin = lpoiz(current_zone) % l(lpoin)
          do idofn = 1,ndof1
             if( kfl_fixno(idofn,ipoin) > 0 ) then
                b1(idofn,ipoin) = 0.0_rp
             end if
          end do
       end do

    case ( 'MATRIX AND RHS' )
       !
       ! Matrix and RHS
       !
       if( solve(1) % kfl_iffix == 1 .and. associated(bvess) ) then     
          !
          ! Dirichlet value is given in BVESS
          !
          do lpoin = 1,npoiz(current_zone)
             ipoin = lpoiz(current_zone) % l(lpoin)

             do idofn = 1,ndof1

                if( kfl_fixno(idofn,ipoin) > 0 ) then
                   !
                   ! Eliminate dof of IPOIN from other equations (JPOIN)
                   ! Keep rows unchanged in order to compute the reaction force
                   !
                   do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                      jpoin = c_dom(izdom)
                      if( ipoin /= jpoin ) then
                         do jzdom = r_dom(jpoin),r_dom(jpoin+1) - 1
                            kpoin = c_dom(jzdom)
                            if( kpoin == ipoin ) then
                               do jdofn = 1,ndof1
                                  b1(jdofn,jpoin)        = b1(jdofn,jpoin) - A11(idofn,jdofn,jzdom) * bvess(idofn,ipoin)
                                  A11(idofn,jdofn,jzdom) = 0.0_rp
                               end do
                            end if
                         end do
                      end if
                   end do
                   !
                   ! IZDOD: Diagonal
                   !
                   izdod = r_dom(ipoin) - 1
                   jpoin = 0
                   do while( jpoin /= ipoin )
                      izdod = izdod + 1
                      jpoin = c_dom(izdod)
                   end do
                   amatrd(idofn) = A11(idofn,idofn,izdod)
                   if( abs(amatrd(idofn)) < zeror ) amatrd(idofn) = 1.0_rp
                   !
                   ! Set line to zero
                   !
                   do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                      do jdofn = 1,ndof1
                         A11(jdofn,idofn,izdom) = 0.0_rp
                      end do
                   end do
                   !
                   ! Presrcibe value 
                   !                 
                   A11(idofn,idofn,izdod) = amatrd(idofn)
                   b1(idofn,ipoin)        = bvess(idofn,ipoin) * amatrd(idofn)
                   u1(idofn,ipoin)        = bvess(idofn,ipoin)
                end if

             end do

          end do

       else if( solve(1) % kfl_iffix == 2 .or. ( solve(1) % kfl_iffix == 1 .and. .not. associated(bvess) ) ) then     
          !
          ! Dirichlet value is not given: Impose zero
          !
          do lpoin = 1,npoiz(current_zone)
             ipoin = lpoiz(current_zone) % l(lpoin)

             do idofn = 1,ndof1

                if( kfl_fixno(idofn,ipoin) > 0 ) then
                   !
                   ! Eliminate dof of IPOIN from other equations (JPOIN)
                   !
                   do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                      jpoin = c_dom(izdom)
                      if( ipoin /= jpoin ) then
                         do jzdom = r_dom(jpoin),r_dom(jpoin+1) - 1
                            kpoin = c_dom(jzdom)
                            if( kpoin == ipoin ) then
                               do jdofn = 1,ndof1
                                  A11(idofn,jdofn,jzdom) = 0.0_rp
                               end do
                            end if
                         end do

                      end if
                   end do
                   !
                   ! IZDOD: Diagonal
                   !
                   izdod = r_dom(ipoin) - 1
                   jpoin = 0
                   do while( jpoin /= ipoin )
                      izdod = izdod + 1
                      jpoin = c_dom(izdod)
                   end do
                   amatrd(idofn) = A11(idofn,idofn,izdod)
                   if( abs(amatrd(idofn)) < zeror ) amatrd(idofn) = 1.0_rp
                   !
                   ! Set line to zero
                   !
                   do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                      do jdofn = 1,ndof1
                         A11(jdofn,idofn,izdom) = 0.0_rp
                      end do
                   end do
                   !
                   ! Prescribe value 
                   !
                   A11(idofn,idofn,izdod) = amatrd(idofn)
                   b1(idofn,ipoin)        = 0.0_rp
                   u1(idofn,ipoin)        = 0.0_rp

                end if

             end do

          end do

       else if( solve(1) % kfl_iffix == 3 ) then     
          !
          ! Dirichlet value is given in unknown u1
          !
          do lpoin = 1,npoiz(current_zone)
             ipoin = lpoiz(current_zone) % l(lpoin)

             do idofn = 1,ndof1

                if( kfl_fixno(idofn,ipoin) > 0 ) then
                   !
                   ! Eliminate dof of IPOIN from other equations (JPOIN)
                   ! Keep rows unchanged in order to compute the reaction force
                   !
                   do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                      jpoin = c_dom(izdom)
                      if( ipoin /= jpoin ) then
                         do jzdom = r_dom(jpoin),r_dom(jpoin+1) - 1
                            kpoin = c_dom(jzdom)
                            if( kpoin == ipoin ) then
                               do jdofn = 1,ndof1
                                  b1(jdofn,jpoin)        = b1(jdofn,jpoin) - A11(idofn,jdofn,jzdom) * u1(idofn,ipoin)
                                  A11(idofn,jdofn,jzdom) = 0.0_rp
                               end do
                            end if
                         end do
                      end if
                   end do
                   !
                   ! IZDOD: Diagonal
                   !
                   izdod = r_dom(ipoin) - 1
                   jpoin = 0
                   do while( jpoin /= ipoin )
                      izdod = izdod + 1
                      jpoin = c_dom(izdod)
                   end do
                   amatrd(idofn) = A11(idofn,idofn,izdod)
                   if( abs(amatrd(idofn)) < zeror ) amatrd(idofn) = 1.0_rp
                   !
                   ! Set line to zero
                   !
                   do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                      do jdofn = 1,ndof1
                         A11(jdofn,idofn,izdom) = 0.0_rp
                      end do
                   end do
                   !
                   ! Presrcibe value 
                   !                 
                   A11(idofn,idofn,izdod) = amatrd(idofn)
                   b1(idofn,ipoin)        = u1(idofn,ipoin) * amatrd(idofn)
                end if

             end do

          end do

       end if

    end select

  end subroutine solver_preprocess_dirichlet_1by1

!!$  !----------------------------------------------------------------------
!!$  !
!!$  !> @author  Guillaume Houzeaux
!!$  !> @date    13/04/2014
!!$  !> @brief   Impose Dirichlet boundary conditions
!!$  !> @details Impose Dirichlet boundary conditions on a 2x2 block
!!$  !>          matrix. Rotate if required.
!!$  !>
!!$  !----------------------------------------------------------------------
!!$
!!$  subroutine solver_preprocess_dirichlet_2by2(solve,ndof1,ndof2,A11,A12,A21,A22,P,b1,b2)
!!$    implicit none
!!$    type(soltyp), intent(in),  pointer  :: solve(:)
!!$    integer(ip),  intent(in)            :: ndof1
!!$    integer(ip),  intent(in)            :: ndof2
!!$    real(rp),     intent(out)           :: A11(ndof1,ndof1,nzdom) 
!!$    real(rp),     intent(out)           :: A12(ndof1,nzdom)
!!$    real(rp),     intent(out)           :: A21(ndof1,nzdom)
!!$    real(rp),     intent(out)           :: A22(nzdom)
!!$    real(rp),     intent(out), optional :: P(nzdom)
!!$    real(rp),     intent(out)           :: b1(ndime,npoin)
!!$    real(rp),     intent(out)           :: b2(npoin)
!!$    real(rp)                            :: A11d(3),Qd,Appd
!!$    integer(ip)                         :: ipoin,jzdom,idime,jdime,izdom,jpoin
!!$    integer(ip)                         :: izdod,ibopo,jbopo,kpoin,iroty,kdime
!!$    integer(ip)                         :: kfl_addma,idofn
!!$    integer(4)                          :: istat
!!$    real(rp)                            :: worma(ndime,ndime)
!!$    real(rp),                  pointer  :: rotma(:,:)
!!$
!!$    if( ndof2 /= 1 ) then
!!$       call runend('PREPROCESS_DIRICHLET_2BY2: NOT CODED')
!!$    end if
!!$    if( ndof1 /= ndime .and. solve(1) % kfl_rotation == 1 ) then
!!$       call runend('PREPROCESS_DIRICHLET_2BY2: IMPOSSIBLE ROATION')
!!$    end if
!!$
!!$    if( solve(1) % kfl_iffix /= 0 ) then
!!$
!!$       !----------------------------------------------------------------------
!!$       !
!!$       ! Rotate matrix
!!$       !
!!$       !----------------------------------------------------------------------
!!$
!!$       if( solve(1) % kfl_rotation == 1 ) then 
!!$
!!$          do ipoin = 1,npoin
!!$             ibopo = lpoty(ipoin)
!!$             if( ibopo > 0 ) then
!!$                iroty =  solve(1) % rotation_type(ibopo)
!!$                if( iroty /= 0 ) then
!!$                   rotma => solve(1) % rotation_matrix(:,:,iroty)
!!$                   !if( iroty == -1 ) then                                    ! Tangent system
!!$                   !   rotma => exnor(:,:,ibopo)
!!$                   !else if( iroty >= 1 ) then                                ! Given system
!!$                   !   rotma =>  skcos(:,:,iroty)
!!$                   !else if( iroty == -2 ) then                               ! Given system
!!$                   !   rotma => skcos_nsi(:,:,ibopo)
!!$                   !else if( iroty == -3 ) then                               ! Geometrical normal
!!$                   !   rotma => skcos(:,:,ibopo)
!!$                   !end if
!!$                   !
!!$                   ! Modifies column number IPOIN of AMATR ( A_j,imodi <-- A_j,imodi R )
!!$                   !
!!$                   do jpoin = 1,npoin
!!$                      do izdom = r_dom(jpoin),r_dom(jpoin+1)-1
!!$                         kpoin = c_dom(izdom)
!!$                         if( kpoin == ipoin ) then
!!$                            do idime = 1,ndime
!!$                               do jdime = 1,ndime
!!$                                  worma(idime,jdime) = 0.0_rp
!!$                                  do kdime = 1,ndime
!!$                                     worma(idime,jdime) = worma(idime,jdime) &
!!$                                          + A11(kdime,idime,izdom) * rotma(kdime,jdime)
!!$                                  end do
!!$                               end do
!!$                            end do
!!$                            do idime = 1,ndime
!!$                               do jdime = 1,ndime
!!$                                  A11(jdime,idime,izdom) = worma(idime,jdime)
!!$                               end do
!!$                            end do
!!$
!!$                            do jdime = 1,ndime
!!$                               worma(1,jdime) = 0.0_rp
!!$                               do kdime = 1,ndime
!!$                                  worma(1,jdime) = worma(1,jdime)&
!!$                                       + A21(kdime,izdom) * rotma(kdime,jdime)
!!$                               end do
!!$                            end do
!!$                            do jdime = 1,ndime
!!$                               A21(jdime,izdom) = worma(1,jdime)
!!$                            end do
!!$
!!$                         end if
!!$                      end do
!!$                   end do
!!$
!!$                   do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
!!$                      !
!!$                      ! Modifies row number IPOIN of AMATR ( A_imodi,j <-- R^t A_imodi,j )
!!$                      !
!!$                      jpoin = c_dom(izdom)
!!$                      do idime = 1,ndime
!!$                         do jdime = 1,ndime
!!$                            worma(idime,jdime) = 0.0_rp
!!$                            do kdime = 1,ndime
!!$                               worma(idime,jdime) = worma(idime,jdime) &
!!$                                    + A11(jdime,kdime,izdom) * rotma(kdime,idime)
!!$                            end do
!!$                         end do
!!$                      end do
!!$                      do idime = 1,ndime
!!$                         do jdime = 1,ndime
!!$                            A11(jdime,idime,izdom) = worma(idime,jdime)
!!$                         end do
!!$                      end do
!!$                      !
!!$                      ! Modify the part corresponding to a scalar unknown
!!$                      !     
!!$                      do idime = 1,ndime
!!$                         worma(idime,1) = 0.0_rp
!!$                         do kdime = 1,ndime
!!$                            worma(idime,1) = worma(idime,1) &
!!$                                 + rotma(kdime,idime) * A12(kdime,izdom)
!!$                         end do
!!$                      end do
!!$                      do idime = 1,ndime
!!$                         A12(idime,izdom) = worma(idime,1)
!!$                      end do
!!$
!!$                   end do
!!$                   !
!!$                   ! Rotate RHS: bu
!!$                   !
!!$                   do idime = 1,ndime
!!$                      worma(idime,1) = 0.0_rp
!!$                      do kdime = 1,ndime
!!$                         worma(idime,1) = worma(idime,1) &
!!$                              + rotma(kdime,idime) * b1(kdime,ipoin)
!!$                      end do
!!$                   end do
!!$                   do idime = 1,ndime
!!$                      b1(idime,ipoin) = worma(idime,1)
!!$                   end do
!!$
!!$                end if
!!$             end if
!!$          end do
!!$       end if
!!$
!!$       !----------------------------------------------------------------------
!!$       !
!!$       ! Impose first degree of freedom
!!$       !
!!$       !----------------------------------------------------------------------
!!$
!!$       do ipoin = 1,npoin
!!$
!!$          do idof1 = 1,ndof1
!!$
!!$             if( solve_sol(1) % kfl_fixno(idof1,ipoin) > 0 ) then
!!$                !
!!$                ! Eliminate dof of IPOIN from other equations (JPOIN)
!!$                !
!!$                do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
!!$                   jpoin = c_dom(izdom)
!!$                   if( ipoin /= jpoin ) then
!!$
!!$                      do jzdom = r_dom(jpoin),r_dom(jpoin+1) - 1
!!$                         kpoin = c_dom(jzdom)
!!$                         if( kpoin == ipoin ) then
!!$                            do jdof1 = 1,ndof1
!!$                               b1(jdof1,jpoin)        = b1(jdof1,jpoin) - A11(idof1,jdof1,jzdom) * solve_sol(1) % bvess(idof1,ipoin)
!!$                               A11(idof1,jdof1,jzdom) = 0.0_rp
!!$                            end do
!!$                            b2(jpoin)        = b2(jpoin) - A21(idof1,jzdom) * solve_sol(1) % bvess(idof1,ipoin)
!!$                            A21(idof1,jzdom) = 0.0_rp
!!$                         end if
!!$                      end do
!!$
!!$                   end if
!!$                end do
!!$                !
!!$                ! IZDOD: Diagonal
!!$                !
!!$                izdod = r_dom(ipoin) - 1
!!$                jpoin = 0
!!$                do while( jpoin /= ipoin )
!!$                   izdod = izdod + 1
!!$                   jpoin = c_dom(izdod)
!!$                end do
!!$                A11d(idof1) = A11(idof1,idof1,izdod)
!!$                if( abs(A11d(idof1)) < zeror ) A11d(idof1) = 1.0_rp
!!$                !
!!$                ! Set line to zero
!!$                !
!!$                do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
!!$                   do jdof1 = 1,ndof1
!!$                      A11(jdof1,idof1,izdom) = 0.0_rp
!!$                   end do
!!$                   A12(idof1,izdom) = 0.0_rp
!!$                end do
!!$                !
!!$                ! Prescribe value 
!!$                !
!!$                idofn                  = (ipoin-1)*ndof1 + idof1
!!$                A11(idof1,idof1,izdod) = A11d(idof1)
!!$                b1(idof1,ipoin)        = solve_sol(1) % bvess(idof1,ipoin) * A11d(idof1)
!!$
!!$             end if
!!$
!!$          end do
!!$
!!$       end do
!!$
!!$       !----------------------------------------------------------------------
!!$       !
!!$       ! Impose Schur complement preconditioner for second system
!!$       !
!!$       !----------------------------------------------------------------------
!!$
!!$       if( present(P) ) then
!!$
!!$          if( solve(2) % kfl_symme == 1 ) then
!!$
!!$             do ipoin = 1,npoin
!!$                do izdom = r_sym(ipoin),r_sym(ipoin+1) - 2
!!$                   jpoin = c_sym(izdom)
!!$                   if( solve_sol(2) % kfl_fixno(1,jpoin) > 0 ) then
!!$                      P(izdom) = 0.0_rp
!!$                   end if
!!$                end do
!!$             end do
!!$             do ipoin = 1,npoin
!!$                if( solve_sol(2) % kfl_fixno(1,ipoin) > 0 ) then
!!$                   !
!!$                   ! IZDOD: Diagonal
!!$                   !
!!$                   izdod = r_sym(ipoin+1) - 1
!!$                   Qd = P(izdod)
!!$                   if( abs(Qd) < zeror ) Qd = 1.0_rp
!!$                   !
!!$                   ! Set line to zero
!!$                   !
!!$                   do izdom = r_sym(ipoin),r_sym(ipoin+1) - 1
!!$                      P(izdom) = 0.0_rp
!!$                   end do
!!$                   !
!!$                   ! Presrcibe value 
!!$                   !
!!$                   P(izdod) = Qd   
!!$                end if
!!$             end do
!!$
!!$          else
!!$
!!$             do ipoin = 1,npoin
!!$                do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
!!$                   jpoin = c_dom(izdom)
!!$                   if( ipoin /= jpoin ) then
!!$                      if( solve_sol(2) % kfl_fixno(1,jpoin) > 0 ) then
!!$                         P(izdom) = 0.0_rp
!!$                      end if
!!$                   end if
!!$                end do
!!$             end do
!!$
!!$             do ipoin = 1,npoin
!!$                if( solve_sol(2) % kfl_fixno(1,ipoin) > 0 ) then
!!$                   !
!!$                   ! IZDOD: Diagonal
!!$                   !
!!$                   izdod = r_dom(ipoin) - 1
!!$                   jpoin = 0
!!$                   do while( jpoin /= ipoin )
!!$                      izdod = izdod + 1
!!$                      jpoin = c_dom(izdod)
!!$                   end do
!!$                   Qd = P(izdod) 
!!$                   if( abs(Qd) < zeror ) Qd = 1.0_rp
!!$                   !
!!$                   ! Set line to zero
!!$                   !
!!$                   do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
!!$                      P(izdom) = 0.0_rp
!!$                   end do
!!$                   !
!!$                   ! Presrcibe value 
!!$                   ! 
!!$                   P(izdod) = Qd
!!$                end if
!!$             end do
!!$
!!$          end if
!!$
!!$       end if
!!$
!!$    end if
!!$
!!$    !----------------------------------------------------------------------
!!$    !
!!$    ! Impose pressure
!!$    !
!!$    !----------------------------------------------------------------------
!!$
!!$    if( kfl_matdi_nsi == 1 .and. INOTMASTER ) then
!!$
!!$       if ( kfl_confi_nsi == 1 .and. nodpr_nsi > 0 ) then
!!$
!!$          ipoin = nodpr_nsi
!!$
!!$          if( solve(2) % kfl_symme == 1 ) then
!!$             call runend('NOT CODED: CHECK IT')
!!$             !
!!$             ! Eliminate pressure at IPOIN on all lines
!!$             !
!!$             do jpoin = 1,npoin
!!$                if( ipoin /= jpoin ) then
!!$                   izdom = r_sym(jpoin)
!!$                   do while( izdom < r_sym(jpoin+1) )
!!$                      kpoin = c_sym(izdom)
!!$                      if( kpoin == ipoin ) then
!!$                         b2(jpoin)  = b2(jpoin) - A22(izdom) * valpr_nsi
!!$                         A22(izdom) = 0.0_rp
!!$                         P(izdom)   = 0.0_rp
!!$                         izdom      = r_sym(jpoin+1)
!!$                      end if
!!$                      izdom = izdom + 1
!!$                   end do
!!$                end if
!!$             end do
!!$             !
!!$             ! Diagonal
!!$             !
!!$             izdod = r_sym(ipoin+1) - 1
!!$             Qd   = P(izdod)
!!$             Appd = A22(izdod)
!!$             if( abs(Qd)   < zeror )   Qd = 1.0_rp
!!$             if( abs(Appd) < zeror ) Appd = 1.0_rp
!!$             !
!!$             ! Set line to zero
!!$             !
!!$             do izdom = r_sym(ipoin),r_sym(ipoin+1) - 1
!!$                P(izdom)   = 0.0_rp
!!$                A22(izdom) = 0.0_rp
!!$             end do
!!$             do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
!!$                do idof1 = 1,ndof1
!!$                   A21(idof1,izdom) = 0.0_rp
!!$                end do
!!$             end do
!!$             !
!!$             ! Presrcibe value 
!!$             !
!!$             P(izdod)   = Qd
!!$             A22(izdod) = Appd   
!!$             b2(ipoin)  = Appd * valpr_nsi
!!$
!!$          else
!!$             !
!!$             ! Eliminate pressure at IPOIN on all lines
!!$             !
!!$             do jpoin = 1,npoin
!!$                if( ipoin /= jpoin ) then
!!$                   izdom = r_dom(jpoin)
!!$                   do while( izdom < r_dom(jpoin+1) )
!!$                      kpoin = c_dom(izdom)
!!$                      if( kpoin == ipoin ) then
!!$                         b2(jpoin)  = b2(jpoin) - A22(izdom) * valpr_nsi
!!$                         A22(izdom) = 0.0_rp
!!$                         P(izdom)   = 0.0_rp
!!$                         izdom      = r_dom(jpoin+1)
!!$                      end if
!!$                      izdom = izdom + 1
!!$                   end do
!!$                end if
!!$             end do
!!$             !
!!$             ! Diagonal
!!$             !
!!$             izdod = r_dom(ipoin) - 1
!!$             jpoin = 0
!!$             do while( jpoin /= ipoin )
!!$                izdod = izdod + 1
!!$                jpoin = c_dom(izdod)
!!$             end do
!!$             Appd = A22(izdod)
!!$             Qd   = P(izdod)
!!$             if( abs(Appd) < zeror ) Appd = 1.0_rp
!!$             !
!!$             ! Set line to zero
!!$             !
!!$             do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
!!$                A22(izdom) = 0.0_rp
!!$                P(izdom)   = 0.0_rp
!!$                do idof1 = 1,ndof1
!!$                   A21(idof1,izdom) = 0.0_rp
!!$                end do
!!$             end do
!!$             !
!!$             ! Presrcibe value 
!!$             !
!!$             P(izdod)   = Qd   
!!$             A22(izdod) = Appd   
!!$             b2(ipoin)  = Appd * valpr_nsi
!!$
!!$          end if
!!$          !
!!$          ! Eliminate prescribed pressure from momentum equation
!!$          !
!!$          do jpoin = 1,npoin
!!$             izdom = r_dom(jpoin) 
!!$             do while( izdom < r_dom(jpoin+1) )
!!$                kpoin = c_dom(izdom)
!!$                if( kpoin == ipoin ) then
!!$                   do idof1 = 1,ndof1
!!$                      b1(idof1,jpoin)  = b1(idof1,jpoin) - A12(idof1,izdom) * valpr_nsi
!!$                      A12(idof1,izdom) = 0.0_rp
!!$                   end do
!!$                   izdom = r_dom(jpoin+1)
!!$                end if
!!$                izdom = izdom + 1
!!$             end do
!!$          end do
!!$
!!$       end if
!!$
!!$    end if
!!$
!!$  end subroutine solver_preprocess_dirichlet_2by2

  subroutine solver_pastix(ndof,nbrows,rhsid,unkno,amatr,ia,ja)

    !-----------------------------------------------------------------------
    !
    ! This routine solves linear systems using a direct method. It is
    ! assumed that the mesh graph has been properly renumbered.      
    !
    !-----------------------------------------------------------------------

    use mod_parall, only : par_omp_num_threads

    integer(ip),  intent(in)    :: nbrows
    integer(ip),  intent(in)    :: ndof
    real(rp),     intent(inout) :: rhsid(*)
    real(rp),     intent(out)   :: unkno(*)
    real(rp),     intent(in)    :: amatr(ndof,ndof,*)
    integer(ip),  intent(in)    :: ia(*)
    integer(ip),  intent(in)    :: ja(*)
    integer(ip)                 :: ii

#ifdef PASTIX
#include "pastix_fortran.h"
    pastix_data_ptr_t                         :: pastix_data ! Structure to keep information in PaStiX (0 for first call)
    integer(4)                                :: pastix_comm ! MPI communicator used by pastix
    pastix_int_t   ,dimension(:), allocatable :: perm        ! permutation tabular
    pastix_int_t   ,dimension(:), allocatable :: invp        ! reverse permutation tabular
    pastix_int_t                              :: nrhs        ! right hand side number (only one possible)
    pastix_int_t                              :: iparm(IPARM_SIZE) ! Integer parameters
    real(8)                                   :: dparm(DPARM_SIZE) ! Floating poin parameters

    !iparm(IPARM_END_TASK)            = API_TASK_CLEAN   ! solution <-
    !iparm(IPARM_END_TASK)            = API_TASK_REFINE   ! solution <-


    integer(ip), pointer :: ia_ndof(:)
    integer(ip), pointer :: ja_ndof(:)
    real(rp),    pointer :: amatr_ndof(:)
    integer(ip)          :: iz,jj,ii_ndof,iz_ndof
    integer(ip)          :: kk1,kk2,ndof2,nz,neqn

    if( ndof > 1 ) then

       neqn = nbrows*ndof
       nz   = ia(nbrows+1)-1
       allocate( ia_ndof(neqn+1) )
       allocate( ja_ndof(nz*ndof*ndof) )
       allocate( amatr_ndof(nz*ndof*ndof) )

       ii_ndof = 0
       iz_ndof = 0

       do ii = 1,nbrows
          do kk2 = 1,ndof
             do iz = ia(ii),ia(ii+1)-1
                jj = ja(iz)
                do kk1 = 1,ndof
                   iz_ndof = iz_ndof + 1
                   amatr_ndof(iz_ndof) = amatr(kk1,kk2,iz)
                   ja_ndof(iz_ndof)    = (jj-1)*ndof + kk1
                end do
             end do
          end do
          nz = ia(ii+1)-ia(ii)
          do kk1 = 1,ndof
             ii_ndof = ii_ndof + 1
             ia_ndof(ii_ndof) = nz*ndof
          end do
       end do

       kk1        = ia_ndof(1)
       ia_ndof(1) = 1 
       do ii = 2,neqn+1
          kk2         = ia_ndof(ii)
          ia_ndof(ii) = ia_ndof(ii-1) + kk1
          kk1         = kk2
       end do

    end if

    do ii = 1,nbrows*ndof
       do iz = ia_ndof(ii),ia_ndof(ii+1)-1
          jj = ja_ndof(iz)
          if( ii == jj ) then
             amatr_ndof(iz) = 0.5_rp*real(ii,rp)
          else
             amatr_ndof(iz) = 0.0_rp
          end if
       end do
       rhsid(ii) = real(ii,rp)
    end do
    !print*,' '
    !print*,' '
    !do ii = 1,neqn
    !   write(*,'(i2,a,10(1x,i9))')   ii,': ',ja_ndof(ia_ndof(ii):ia_ndof(ii+1)-1)
    !   write(*,'(i2,a,10(1x,e9.3))') ii,': ',amatr_ndof(ia_ndof(ii):ia_ndof(ii+1)-1)
    !end do

    pastix_data = 0
    ! splitter le communicateur en morceaux quand je tourne en parallele en nombre de MPI process!)
    pastix_comm = 0
    nrhs        = 1

    allocate(perm(nbrows))
    allocate(invp(nbrows))

    do ii = 1,nbrows*ndof
       unkno(ii) = rhsid(ii)
    end do
    !
    ! Initialization
    !
    iparm(IPARM_MODIFY_PARAMETER)    = API_NO            ! Use default parameters
    iparm(IPARM_START_TASK)          = API_TASK_INIT
    iparm(IPARM_END_TASK)            = API_TASK_INIT

print*,'a'
    call pastix_fortran(pastix_data,pastix_comm,&
         nbrows,ia,ja,amatr,perm,invp,unkno,nrhs,iparm,dparm)
print*,'b'
    !
    ! Ordering
    !
    iparm(IPARM_THREAD_NBR)          = max(1_ip,par_omp_num_threads)
    iparm(IPARM_VERBOSE)             = API_VERBOSE_YES
    iparm(IPARM_SYM)                 = API_SYM_NO 
    iparm(IPARM_FACTORIZATION)       = API_FACT_LU    
    iparm(IPARM_MATRIX_VERIFICATION) = API_NO
    iparm(IPARM_DOF_NBR)             = ndof
    iparm(IPARM_RHS_MAKING)          = API_RHS_B
    iparm(IPARM_START_TASK)          = API_TASK_ORDERING ! -> One shot
    iparm(IPARM_END_TASK)            = API_TASK_ORDERING ! -> One shot

    call pastix_fortran(pastix_data,pastix_comm,&
         nbrows,ia,ja,amatr,perm,invp,unkno,nrhs,iparm,dparm)
print*,'c=',perm(1:nbrows)
    !
    ! Solve
    !
    iparm(IPARM_DOF_NBR)             = ndof
    iparm(IPARM_START_TASK)          = API_TASK_SYMBFACT  ! -> One shot
    iparm(IPARM_END_TASK)            = API_TASK_SOLVE     ! solution <-

    if( ndof > 1 ) then
       call pastix_fortran(pastix_data,pastix_comm,&
            nbrows*ndof,ia_ndof,ja_ndof,amatr_ndof,perm,invp,unkno,nrhs,iparm,dparm)
    else
       call pastix_fortran(pastix_data,pastix_comm,&
            nbrows,ia,ja,amatr,perm,invp,unkno,nrhs,iparm,dparm)
    end if

    do ii = 1,nbrows*ndof
       print*,ii,unkno(ii)
    end do

call runend('O.K.!')
    iparm(IPARM_START_TASK)          = API_TASK_CLEAN       ! -> Deallocate
    iparm(IPARM_END_TASK)            = API_TASK_CLEAN       ! memory <-
    if( ndof > 1 ) then
       call pastix_fortran(pastix_data,pastix_comm,&
            nbrows,ia_ndof,ja_ndof,amatr_ndof,perm,invp,unkno,nrhs,iparm,dparm)
    else
       call pastix_fortran(pastix_data,pastix_comm,&
            nbrows,ia,ja,amatr,perm,invp,unkno,nrhs,iparm,dparm)
    end if

    deallocate(perm)
    deallocate(invp)
    !
    ! Deallocate
    !
    if( ndof > 1 ) then
       deallocate( ia_ndof )
       deallocate( ja_ndof )
       deallocate( amatr_ndof )
    end if

#endif

  end subroutine solver_pastix
  
  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Direct solvers
  !> @details Direct solvers
  !>
  !----------------------------------------------------------------------
  
  subroutine solver_direct_solver_initialize(direct_solver)
    type(direct_solver_typ) :: direct_solver 
#ifdef PASTIX
    pastix_data_ptr_t :: pastix_data       ! Structure to keep information in PaStiX (0 for first call)
    integer(4)        :: pastix_comm       ! MPI communicator used by pastix
    pastix_int_t      :: nrhs              ! right hand side number (only one possible)
    pastix_int_t      :: iparm(IPARM_SIZE) ! Integer parameters
    real(8)           :: dparm(DPARM_SIZE) ! Floating poin parameters
#endif
    real(rp)          :: unkno(2)
    real(rp)          :: amatr(2)

    direct_solver = direct_solver_init

    if( direct_solver % kfl_solver == SOL_DIRECT_SOLVER_ALYA ) then
       !
       ! Alya
       !  
       call graphs_copyij(                               &
            direct_solver % nn   ,direct_solver % ia_in, &
            direct_solver % ja_in,direct_solver % ia,    &
            direct_solver % ja)
       call graphs_permut_metis_postordering(            &
            direct_solver % nn   ,direct_solver % nz,    &
            direct_solver % ia   ,direct_solver % ja,    &
            direct_solver % permr,direct_solver % invpr  )
    
    else if( direct_solver % kfl_solver == SOL_DIRECT_SOLVER_PASTIX ) then
       !
       ! Pastix
       !
#ifdef PASTIX
       pastix_data = 0
       ! Split communicator into MPI tasks
       pastix_comm = 0
       nrhs        = 1 
       call memory_alloca(memit,'DIRECT_SOLVER % PERMR','solver_direct_solver_initialize',direct_solver % permr,direct_solver % nn)
       call memory_alloca(memit,'DIRECT_SOLVER % INVPR','solver_direct_solver_initialize',direct_solver % invpr,direct_solver % nn)
       direct_solver % ia => direct_solver % ia_in
       direct_solver % ja => direct_solver % ja_in
       !
       ! Initialization
       !
       iparm(IPARM_MODIFY_PARAMETER)    = API_NO            ! Use default parameters
       iparm(IPARM_START_TASK)          = API_TASK_INIT
       iparm(IPARM_END_TASK)            = API_TASK_INIT
       call pastix_fortran(pastix_data,pastix_comm,&
            direct_solver % nn,direct_solver % ia,direct_solver % ja,&
            amatr,direct_solver % permr,direct_solver % invpr,&
            unkno,nrhs,iparm,dparm)       
       !
       ! Ordering
       !
       !iparm(IPARM_THREAD_NBR)          = max(1_ip,par_omp_num_threads)
       iparm(IPARM_THREAD_NBR)          = 1_ip
       iparm(IPARM_VERBOSE)             = API_VERBOSE_YES
       iparm(IPARM_SYM)                 = API_SYM_NO 
       iparm(IPARM_FACTORIZATION)       = API_FACT_LU    
       iparm(IPARM_MATRIX_VERIFICATION) = API_NO
       iparm(IPARM_DOF_NBR)             = direct_solver % ndof
       iparm(IPARM_RHS_MAKING)          = API_RHS_B
       iparm(IPARM_START_TASK)          = API_TASK_ORDERING 
       iparm(IPARM_END_TASK)            = API_TASK_ORDERING 
       
       call pastix_fortran(pastix_data,pastix_comm,&
            direct_solver % nn,direct_solver % ia,direct_solver % ja,&
            amatr,direct_solver % permr,direct_solver % invpr,&
            unkno,nrhs,iparm,dparm)

#else
       call runend('MACRO PASTIX IS REQUIRED: RECOMPILE ALYA WITH -DPASTIX')
#endif
    end if
  end subroutine solver_direct_solver_initialize

end module mod_solver
!> @} 
!-----------------------------------------------------------------------
