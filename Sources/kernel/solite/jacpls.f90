subroutine jacpls( &
     nbnodes, nbvar, idprecon, maxiter, kfl_resid, &
     eps, an, pn, kfl_cvgso, lun_cvgso, kfl_solve, &
     lun_outso, ja, ia, bb, xx )


  !-----------------------------------------------------------------------
  ! Objective: Solve    [A] [M]^-1  [M] xx = bb  
  !                        [A']       xx'  = bb'   by the CG method. 
  !
  !            Three preconditioners are possible:
  !            idprecon = 0 => [M]^-1 = [M] = I
  !            idprecon = 1 => Diag. Scaling
  !
  !            [A] [M]^-1  must be symmetric positive defined. 
  !            [A] can be full or half stored (lower triangle).
  !            The elements in each row are stored in increasing column order
  !
  !            If Diag. Scaling is selected the preconditioned system is:
  !                    D^-1/2 [A] D^-1/2   D^1/2 x  =  D^-1/2 b
  !                          [A']              x'   =      b'
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg
  use def_master, only       :  IMASTER,INOTMASTER,kfl_paral,npoi1,&
       &                        IPARALL,kfl_async,parre,ISLAVE,&
       &                        INOTSLAVE,funin,NPOIN_TYPE
  use def_solver, only       :  memit,SOL_NO_PRECOND,SOL_LINELET,&
       &                        SOL_SQUARE,SOL_DIAGONAL,SOL_MATRIX,&
       &                        resin,resfi,resi1,resi2,iters,solve_sol
  use mod_memchk
use mod_postpr, only : postpr
use def_master, only : gesca,ittim
use def_parame
  implicit none
  integer(ip), intent(in)    :: nbnodes, nbvar, idprecon, maxiter, kfl_resid
  integer(ip), intent(in)    :: kfl_cvgso, lun_cvgso
  integer(ip), intent(in)    :: kfl_solve, lun_outso
  real(rp),    intent(in)    :: eps
  real(rp),    intent(in)    :: an(nbvar,nbvar,*), pn(*)
  integer(ip), intent(in)    :: ja(*), ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(inout) :: xx(nbvar*nbnodes)
  integer(ip)                :: ii, nrows, ierr, npoin
  integer(4)                 :: istat
  real(rp)                   :: alpha, beta, rho, stopcri, resid
  real(rp)                   :: invnb, newrho, dummr
  real(rp),    pointer       :: rr(:), pp(:), zz(:), invdiag(:)
  real(rp),    pointer       :: ww(:), p0(:)
character(5) :: wopos(3)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif 

  if(  IMASTER ) then
     nrows   = 1                ! Minimum memory for working arrays
     npoin   = 0                ! master does not perform any loop
  else
     nrows   = nbnodes * nbvar
     npoin   = nbnodes
  end if
  !
  ! Sequential and slaves: Working arrays
  !
  allocate(rr(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'RR','deflcg',rr)

  allocate(invdiag(nrows),stat=istat)
  call memchk(0_ip,istat,memit,'INVDIAG','deflcg',invdiag)

  !----------------------------------------------------------------------
  !
  ! INVDIAG = D^-1/2 or D^-1
  !
  !----------------------------------------------------------------------
  
  call diagon(nbnodes,nbvar,solve_sol(1) % kfl_symme,ia,ja,an,invdiag)
  
  do ii = 1,nrows
     invdiag(ii) = 1.0_rp / invdiag(ii)
  end do

  do while( iters < maxiter .and. resid > 1.0e-12_rp )
     !
     ! x^{i+1} = x^i + D^-1 ( b - Ax^i)
     !
     call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )
     do ii = 1,nrows
        rr(ii) = bb(ii) - rr(ii)
        xx(ii) = xx(ii) + invdiag(ii) * rr(ii)
     end do
     call norm2x(nbvar,xx,resid)
     resid = sqrt(resid)
     iters = iters + 1
     print*,iters,resid

  end do

  !-----------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !-----------------------------------------------------------------

  call memchk(2_ip,istat,memit,'INVDIAG','deflcg',invdiag)
  deallocate(invdiag,stat=istat)
  if(istat/=0) call memerr(2_ip,'INVDIAG','deflcg',0_ip)

  call memchk(2_ip,istat,memit,'RR','deflcg',rr)
  deallocate(rr,stat=istat)
  if(istat/=0) call memerr(2_ip,'RR','deflcg',0_ip)

110 format(i5,18(2x,e12.6))
120 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = rho^k / <p^{k+1},q^{k+1}>')

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine jacpls
