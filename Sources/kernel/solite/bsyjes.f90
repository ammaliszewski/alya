subroutine bsyjes(itask,nbnodes,nbvar,an,ja,ia,qq,wa1,rr,ngrou,newrho,mu)
  !-----------------------------------------------------------------------
  ! Objective:  Multiply a symmetric matrix stored in BCSR by a vector.
  !                                wa1 = [A] qq
  !
  !             Only the LOWER triangle is stored. 
  !             JA elements are stored in increasing column order
  !-----------------------------------------------------------------------
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  NPOIN_REAL_12DI,parr1,icoml,&
       &                                parre,nparr,npoi1,npoi2,npoi3,&
       &                                INOTMASTER,IPARALL,ISEQUEN,ISLAVE,&
       &                                NPOIN_TYPE,kfl_paral
  use def_solver, only               :  solve_sol
  implicit none
  integer(ip), intent(in)            :: itask,nbnodes,nbvar,ngrou
  real(rp),    intent(in)            :: an(nbvar,nbvar,*)
  integer(ip), intent(in)            :: ja(*),ia(*)
  real(rp),    intent(in)            :: qq(nbvar,nbnodes),rr(nbvar,nbnodes)
  real(rp),    intent(inout), target :: wa1(nbvar,nbnodes)
  real(rp),    intent(out),   target :: mu(ngrou+1)
  real(rp),    intent(out)           :: newrho
  real(rp),                   target :: dummr_par(1) 
  integer(ip)                        :: ii,jj,kk,mm,igrou,ipoin
  real(rp)                           :: raux,raux2

  !----------------------------------------------------------------
  !
  ! BSYMAX, PRODXY
  !
  !----------------------------------------------------------------

#ifdef TRACING
  call extrae_eventandcounters(401,4)
#endif
  newrho = 0.0_rp

  if( INOTMASTER ) then

     if( nbvar > 1 ) then

        call runend('programme le faignasse')

     else if( nbvar == 1 ) then
        !
        ! Lower triangle + diag
        !
        do ii= 1, nbnodes
           wa1(1,ii) = 0.0_rp
        end do

        if( ISEQUEN ) then

           do ii= 1, nbnodes
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 mm            = ja(jj)
                 raux          = raux   + an(1,1,jj) * qq(1,mm)
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux   + an(1,1,kk) * qq(1,ja(kk))
              newrho    = newrho + rr(1,ii)   * qq(1,ii)
           end do

        else

           do ii= 1, npoi1
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 mm            = ja(jj)
                 raux          = raux          + an(1,1,jj) * qq(1,mm)
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux + an(1,1,kk) * qq(1,ja(kk))
              newrho    = newrho + rr(1,ii) * qq(1,ii)
           end do
           do ii= npoi1+1,npoi2-1
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 raux          = raux          + an(1,1,jj) * qq(1,ja(jj))
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux + an(1,1,kk) * qq(1,ja(kk))
           end do
           do ii= npoi2,npoi3
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 mm            = ja(jj)
                 raux          = raux          + an(1,1,jj) * qq(1,mm)
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux + an(1,1,kk) * qq(1,ja(kk))
              newrho    = newrho + rr(1,ii) * qq(1,ii)
           end do
           do ii= npoi3+1,nbnodes
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 raux          = raux          + an(1,1,jj) * qq(1,ja(jj))
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux + an(1,1,kk) * qq(1,ja(kk))
           end do
        end if

     else
        call runend('bcsrmvsym_d: Wrong NBVAR parameter')
     end if
     !
     ! Modify WA1 due do periodicity and Parall service
     !
     if(itask==1) call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,wa1)

  end if

  !----------------------------------------------------------------
  !
  ! WTVECT
  !
  !----------------------------------------------------------------

  do igrou=1,solve_sol(1)%ngrou*nbvar
     mu(igrou)=0.0_rp
  end do

  if( ISEQUEN ) then

     if( nbvar == 1 )then  
        do ipoin=1,nbnodes
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do
     end if

  else if( ISLAVE ) then

     if( nbvar == 1 ) then  

        do ipoin=1,npoi1
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do

        do ipoin=npoi2,npoi3
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do

     end if

  end if

  if( IPARALL ) then

     if( solve_sol(1)%kfl_gathe == 1 ) then
        !
        ! All reduce for rho and all gatherv for mu
        !     
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        icoml        =  solve_sol(1)%icoml
        call Parall(34_ip)

     else if( solve_sol(1)%kfl_gathe == 0 ) then
        !
        ! All reduce for mu and rho
        !
        mu(solve_sol(1)%ngrou*nbvar+1) = newrho
        nparr  =  solve_sol(1)%ngrou*nbvar+1
        parre  => mu
        call Parall(9_ip)
        newrho =  mu(solve_sol(1)%ngrou*nbvar+1)

     else if( solve_sol(1)%kfl_gathe == 2 ) then
        !
        ! All reduce for rho and send/receive for mu
        !
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        call Parall(803_ip)

     end if

  end if

#ifdef TRACING
  call extrae_eventandcounters(401,0)
#endif

end subroutine bsyjes

subroutine bsyje2(itask,nbnodes,nbvar,an,ja,ia,qq,wa1,rr,ngrou,newrho,mu)
  !-----------------------------------------------------------------------
  ! Objective:  Multiply a symmetric matrix stored in BCSR by a vector.
  !                                wa1 = [A] qq
  !
  !             Only the LOWER triangle is stored. 
  !             JA elements are stored in increasing column order
  !-----------------------------------------------------------------------
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  NPOIN_REAL_12DI,parr1,icoml,&
       &                                parre,nparr,npoi1,npoi2,npoi3,&
       &                                INOTMASTER,IPARALL,ISEQUEN,ISLAVE,&
       &                                NPOIN_TYPE,kfl_paral
  use def_solver, only               :  solve_sol
  implicit none
  integer(ip), intent(in)            :: itask,nbnodes,nbvar,ngrou
  real(rp),    intent(in)            :: an(nbvar,nbvar,*)
  integer(ip), intent(in)            :: ja(*),ia(*)
  real(rp),    intent(in)            :: qq(nbvar,nbnodes),rr(nbvar,nbnodes)
  real(rp),    intent(inout), target :: wa1(nbvar,nbnodes)
  real(rp),    intent(out),   target :: mu(ngrou+1)
  real(rp),    intent(out)           :: newrho
  real(rp),                   target :: dummr_par(1) 
  integer(ip)                        :: ii,jj,mm,igrou,ipoin
  real(rp)                           :: raux2

  !----------------------------------------------------------------
  !
  ! BSYMAX, PRODXY
  !
  !----------------------------------------------------------------

#ifdef TRACING
  call extrae_eventandcounters(401,2)
#endif
  newrho = 0.0_rp

  if( INOTMASTER ) then

     if( nbvar > 1 ) then

        call runend('programme le faignasse')

     else if( nbvar == 1 ) then
        !
        ! Lower triangle + diag
        !
        do ii= 1, nbnodes
           wa1(1,ii) = 0.0_rp
        end do

        if( ISEQUEN ) then

           !do ii= 1, nbnodes
           !   raux2 = qq(1,ii)
           !   do jj= ia(ii), ia(ii+1)-1
           !      mm        = ja(jj)
           !      wa1(1,mm) = wa1(1,mm) + an(1,1,jj) * raux2
           !   end do
           !   newrho = newrho + rr(1,ii) * raux2
           !end do
           do ii= 1, nbnodes
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
              newrho = newrho + rr(1,ii) * qq(1,ii)
           end do

        else

           !do ii= 1, npoi1
           !   raux2 = qq(1,ii)
           !   do jj= ia(ii), ia(ii+1)-1
           !      mm        = ja(jj)
           !      wa1(1,mm) = wa1(1,mm) + an(1,1,jj) * raux2
           !   end do
           !   newrho = newrho + rr(1,ii) * raux2
           !end do
           do ii= 1, npoi1
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
              newrho = newrho + rr(1,ii) * qq(1,ii)
           end do

           !do ii= npoi1+1,npoi2-1
           !   raux2 = qq(1,ii)
           !   do jj= ia(ii), ia(ii+1)-1
           !      mm        = ja(jj)
           !      wa1(1,mm) = wa1(1,mm) + an(1,1,jj) * raux2
           !   end do
           !end do
           do ii= npoi1+1,npoi2-1
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
           end do

           !do ii= npoi2,npoi3
           !   raux2 = qq(1,ii)
           !   do jj= ia(ii), ia(ii+1)-1
           !      mm        = ja(jj)
           !      wa1(1,mm) = wa1(1,mm) + an(1,1,jj) * raux2
           !   end do
           !   newrho = newrho + rr(1,ii) * raux2
           !end do
           do ii= npoi2,npoi3
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
              newrho = newrho + rr(1,ii) * qq(1,ii)
           end do

           do ii= npoi3+1,nbnodes
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
           end do
        end if

     else
        call runend('bcsrmvsym_d: Wrong NBVAR parameter')
     end if
     !
     ! Modify WA1 due do periodicity and Parall service
     !
     if( itask == 1 ) call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,wa1)

  end if

  !----------------------------------------------------------------
  !
  ! WTVECT
  !
  !----------------------------------------------------------------

  do igrou=1,solve_sol(1)%ngrou*nbvar
     mu(igrou)=0.0_rp
  end do

  if( ISEQUEN ) then

     if( nbvar == 1 )then  
        do ipoin=1,nbnodes
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do
     end if

  else if( ISLAVE ) then

     if( nbvar == 1 ) then  

        do ipoin=1,npoi1
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do

        do ipoin=npoi2,npoi3
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do

     end if

  end if

  if( IPARALL ) then

     if( solve_sol(1)%kfl_gathe == 1 ) then
        !
        ! All reduce for rho and all gatherv for mu
        !     
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        icoml        =  solve_sol(1)%icoml
        call Parall(34_ip)

     else if( solve_sol(1)%kfl_gathe == 0 ) then
        !
        ! All reduce for mu and rho
        !
        mu(solve_sol(1)%ngrou*nbvar+1) = newrho
        nparr  =  solve_sol(1)%ngrou*nbvar+1
        parre  => mu
        call Parall(9_ip)
        newrho =  mu(solve_sol(1)%ngrou*nbvar+1)

     else if( solve_sol(1)%kfl_gathe == 2 ) then
        !
        ! All reduce for rho and send/receive for mu
        !     
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        call Parall(803_ip)

     end if

  end if

#ifdef TRACING
  call extrae_eventandcounters(401,0)
#endif
end subroutine bsyje2

subroutine bsyje3(itask,nbnodes,nbvar,an,ja,ia,qq,wa1,rr,ngrou,newrho,mu)

  !-----------------------------------------------------------------------
  ! Objective:  Multiply a symmetric matrix stored in BCSR by a vector.
  !                                wa1 = [A] qq
  !
  !             Only the LOWER triangle is stored. 
  !             JA elements are stored in increasing column order
  !-----------------------------------------------------------------------
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  NPOIN_REAL_12DI,parr1,icoml,&
       &                                parre,nparr,npoi1,npoi2,npoi3,&
       &                                INOTMASTER,IPARALL,ISEQUEN,ISLAVE,&
       &                                kfl_paral,NPOIN_TYPE
  use def_solver, only               :  solve_sol
  implicit none
  integer(ip), intent(in)            :: itask,nbnodes,nbvar,ngrou
  real(rp),    intent(in)            :: an(nbvar,nbvar,*)
  integer(ip), intent(in)            :: ja(*),ia(*)
  real(rp),    intent(in)            :: qq(nbvar,nbnodes),rr(nbvar,nbnodes)
  real(rp),    intent(inout), target :: wa1(nbvar,nbnodes)
  real(rp),    intent(out),   target :: mu(ngrou+1)
  real(rp),    intent(out)           :: newrho
  real(rp),                   target :: dummr_par(1) 
  integer(ip)                        :: ii,jj,mm,igrou,ipoin
  real(rp)                           :: raux2

  !----------------------------------------------------------------
  !
  ! BSYMAX, PRODXY
  !
  !----------------------------------------------------------------

#ifdef TRACING
  call extrae_eventandcounters(401,3)
#endif
  newrho = 0.0_rp

  if( INOTMASTER ) then

     if( nbvar > 1 ) then

        call runend('programme le faignasse')

     else if( nbvar == 1 ) then
        !
        ! Lower triangle + diag
        !
        do ii = 1,nbnodes
           wa1(1,ii) = 0.0_rp
        end do

        if( ISEQUEN ) then

           do ii= 1, nbnodes
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-1
                 mm        = ja(jj)
                 wa1(1,mm) = wa1(1,mm) + an(1,1,jj) * raux2
              end do
              newrho = newrho + rr(1,ii) * qq(1,ii)
           end do

        else

           !-------------------------------------------------------------------
           !
           ! Boundary nodes
           !
           !-------------------------------------------------------------------

           do ii = npoi1+1,npoi2-1
              do jj = ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
           end do
           !
           ! Own boundary nodes: scalar product must be computed
           !
           do ii = npoi2,npoi3
              do jj = ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
              newrho = newrho + rr(1,ii) * qq(1,ii)
           end do

           do ii = npoi3+1,nbnodes
              do jj = ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
           end do

           !-------------------------------------------------------------------
           !
           ! Modify WA1 due do periodicity and Parall service
           !
           !-------------------------------------------------------------------

           if( itask == 1 .and. IPARALL ) then
              call pararr('SLA',NPOIN_TYPE,nbnodes*nbvar,wa1)
           end if

           !-------------------------------------------------------------------
           !
           ! Interior nodes
           !
           !-------------------------------------------------------------------

           do ii = 1, npoi1
              do jj = ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
              newrho = newrho + rr(1,ii) * qq(1,ii)
           end do

           !-------------------------------------------------------------------
           !
           ! Wait all and sum up contributions of boundary nodes
           !
           !-------------------------------------------------------------------

           if( itask == 1 .and. IPARALL ) then
              call pararr('SLA',NPOIN_TYPE,nbnodes*nbvar,wa1)
           end if

        end if

     else
        call runend('bcsrmvsym_d: Wrong NBVAR parameter')
     end if
     
  end if

  !----------------------------------------------------------------
  !
  ! WTVECT
  !
  !----------------------------------------------------------------

  do igrou=1,solve_sol(1)%ngrou*nbvar
     mu(igrou)=0.0_rp
  end do

  if( ISEQUEN ) then

     if( nbvar == 1 )then  
        do ipoin=1,nbnodes
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do
     end if

  else if( ISLAVE ) then

     if( nbvar == 1 ) then  

        do ipoin=1,npoi1
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do

        do ipoin=npoi2,npoi3
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do

     end if

  end if

  if( IPARALL ) then

     if( solve_sol(1)%kfl_gathe == 1 ) then
        !
        ! All reduce for rho and all gatherv for mu
        !     
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        icoml        =  solve_sol(1)%icoml
        call Parall(34_ip)

     else if( solve_sol(1)%kfl_gathe == 0 ) then
        !
        ! All reduce for mu and rho
        !
        mu(solve_sol(1)%ngrou*nbvar+1) = newrho
        nparr  =  solve_sol(1)%ngrou*nbvar+1
        parre  => mu
        call Parall(9_ip)
        newrho =  mu(solve_sol(1)%ngrou*nbvar+1)

     else if( solve_sol(1)%kfl_gathe == 2 ) then
        !
        ! All reduce for rho and send/receive for mu
        !     
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        call Parall(803_ip)

     end if

  end if

#ifdef TRACING
  call extrae_eventandcounters(401,0)
#endif
end subroutine bsyje3

subroutine bsyje5(itask,nbnodes,nbvar,an,ja,ia,qq,wa1,rr,ngrou,newrho,mu)
  !-----------------------------------------------------------------------
  ! Objective:  Multiply a symmetric matrix stored in BCSR by a vector.
  !                                wa1 = [A] qq
  ! Same as bsyjes 2 but
  !
  ! bsyje2:                                         bsyje5:
  ! -------                                         -------
  ! - Az                                            - Az
  ! - Point-to-point                                - W^T.(Az)
  ! - W^T.(Az) only on interior and own boundary    - All reduce
  ! - All reduce
  !
  !             Only the LOWER triangle is stored. 
  !             JA elements are stored in increasing column order
  !-----------------------------------------------------------------------
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  NPOIN_REAL_12DI,parr1,icoml,&
       &                                parre,nparr,npoi1,npoi2,npoi3,&
       &                                INOTMASTER,IPARALL,ISEQUEN,ISLAVE,kfl_paral
  use def_solver, only               :  solve_sol
  implicit none
  integer(ip), intent(in)            :: itask,nbnodes,nbvar,ngrou
  real(rp),    intent(in)            :: an(nbvar,nbvar,*)
  integer(ip), intent(in)            :: ja(*),ia(*)
  real(rp),    intent(in)            :: qq(nbvar,nbnodes),rr(nbvar,nbnodes)
  real(rp),    intent(inout), target :: wa1(nbvar,nbnodes)
  real(rp),    intent(out),   target :: mu(nbvar*ngrou+1)
  real(rp),    intent(out)           :: newrho
  real(rp),                   target :: dummr_par(1) 
  integer(ip)                        :: ii,jj,kk,ll,mm,igrou,ipoin
  real(rp)                           :: raux
  real(rp)                           :: newrho1, newrho2

  !----------------------------------------------------------------
  !
  ! BSYMAX, PRODXY
  !
  !----------------------------------------------------------------
#ifdef TRACING
  call extrae_eventandcounters(401,5)
#endif

  newrho  = 0.0_rp
  newrho1 = 0.0_rp
  newrho2 = 0.0_rp

  if( INOTMASTER ) then

     if( nbvar > 1 ) then
        !
        ! Lower triangle + diag
        !
        do ii = 1,nbnodes
           do kk = 1,nbvar
              wa1(kk,ii) = 0.0_rp
           end do
        end do

        if( ISEQUEN ) then

#ifdef TRACING
           call extrae_eventandcounters(402,1)
#endif
           do ii = 1,nbnodes
              do jj = ia(ii), ia(ii+1)-1
                 do ll = 1,nbvar
                    raux = qq(ll,ja(jj))
                    do kk = 1,nbvar
                       wa1(kk,ii) = wa1(kk,ii) + an(ll,kk,jj) * raux
                    end do
                 end do
              end do
              do kk = 1,nbvar
                 newrho = newrho + rr(kk,ii) * qq(kk,ii)
              end do
           end do

        else

#ifdef TRACING
           call extrae_eventandcounters(402,2)
#endif
           do ii= 1, npoi1
              do jj= ia(ii), ia(ii+1)-1
                 do ll = 1,nbvar
                    raux = qq(ll,ja(jj))
                    do kk = 1,nbvar
                       wa1(kk,ii) = wa1(kk,ii) + an(ll,kk,jj) * raux
                    end do
                 end do
              end do
              do kk = 1,nbvar
                 newrho = newrho + rr(kk,ii) * qq(kk,ii)
              end do
           end do

           do ii= npoi1+1,npoi2-1
              do jj= ia(ii), ia(ii+1)-1
                 do ll = 1,nbvar
                    raux = qq(ll,ja(jj))
                    do kk = 1,nbvar
                       wa1(kk,ii) = wa1(kk,ii) + an(ll,kk,jj) * raux
                    end do
                 end do
              end do
           end do

           do ii= npoi2,npoi3
              do jj= ia(ii), ia(ii+1)-1
                 do ll = 1,nbvar
                    raux = qq(ll,ja(jj))
                    do kk = 1,nbvar
                       wa1(kk,ii) = wa1(kk,ii) + an(ll,kk,jj) * raux
                    end do
                 end do
              end do
              do kk = 1,nbvar
                 newrho = newrho + rr(kk,ii) * qq(kk,ii)
              end do
           end do

           do ii= npoi3+1,nbnodes
              do jj= ia(ii), ia(ii+1)-1
                 do ll = 1,nbvar
                    raux = qq(ll,ja(jj))
                    do kk = 1,nbvar
                       wa1(kk,ii) = wa1(kk,ii) + an(ll,kk,jj) * raux
                    end do
                 end do
              end do
           end do
        end if

     else if( nbvar == 1 ) then
        !
        ! Lower triangle + diag
        !
        do ii= 1, nbnodes
           wa1(1,ii) = 0.0_rp
        end do

        if( ISEQUEN ) then

#ifdef TRACING
           call extrae_eventandcounters(402,3)
#endif
           do ii= 1, nbnodes
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
              newrho = newrho + rr(1,ii) * qq(1,ii)
           end do

        else
           !$OMP PARALLEL                                                         &
           !$OMP SHARED(npoi1, ia, wa1, an, qq, ja, rr, nbvar ) 
           !$OMP DO                                                               &
           !$OMP PRIVATE(jj, ii)                                                  &
           !$OMP REDUCTION(+:newrho1)                                             &
           !$OMP SCHEDULE(DYNAMIC, 1000)
           do ii= 1, npoi1
#if defined(DLB) 
              if( ii == 1 ) then
                 call dlb_enable()
              end if
#endif
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
              newrho1 = newrho1 + rr(1,ii) * qq(1,ii)
              !!newrho = newrho + rr(1,ii) * qq(1,ii)
           end do
           !$OMP END DO NOWAIT

           !$OMP DO                                     &
           !$OMP PRIVATE(jj, ii)                        &
           !$OMP SCHEDULE(DYNAMIC, 1000)
           do ii= npoi1+1,npoi2-1
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
           end do
           !$OMP END DO NOWAIT

           !$OMP DO                                    &
           !$OMP PRIVATE(jj, ii)                       &
           !$OMP REDUCTION(+:newrho2)                  &
           !$OMP SCHEDULE(DYNAMIC, 1000)
           do ii= npoi2,npoi3
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
              newrho2 = newrho2 + rr(1,ii) * qq(1,ii)
              !!newrho = newrho + rr(1,ii) * qq(1,ii)
           end do

           !$OMP END DO NOWAIT
           !$OMP DO                                     &
           !$OMP PRIVATE(jj, ii)                        &
           !$OMP SCHEDULE(DYNAMIC, 1000)                         
           do ii= npoi3+1,nbnodes
              do jj= ia(ii), ia(ii+1)-1
                 wa1(1,ii) = wa1(1,ii) + an(1,1,jj) * qq(1,ja(jj))
              end do
           end do
           !$OMP END DO
           !$OMP END PARALLEL

           newrho = newrho1 + newrho2
        end if

     else
        call runend('bcsrmvsym_d: Wrong NBVAR parameter')
     end if

  end if

#ifdef TRACING
  call extrae_eventandcounters(402,0)
#endif
  !----------------------------------------------------------------
  !
  ! WTVECT
  !
  !----------------------------------------------------------------

  do igrou=1,solve_sol(1)%ngrou*nbvar
     mu(igrou)=0.0_rp
  end do

  if( INOTMASTER ) then

     if( nbvar == 1 )then  
        do ipoin=1,nbnodes
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do
     else
        do ipoin=1,nbnodes
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) then
              ii = (igrou-1)*nbvar
              do kk = 1,nbvar
                 ii = ii + 1
                 mu(ii)=mu(ii)+wa1(kk,ipoin)
              end do
           end if
        end do
     end if

  end if

  if( IPARALL ) then

     if( solve_sol(1)%kfl_gathe == 1 ) then
        !
        ! All reduce for rho and all gatherv for mu
        !     
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        icoml        =  solve_sol(1)%icoml
        call Parall(34_ip)

     else if( solve_sol(1)%kfl_gathe == 0 ) then
        !
        ! All reduce for mu and rho
        !
        mu(solve_sol(1)%ngrou*nbvar+1) = newrho
        nparr  =  solve_sol(1)%ngrou*nbvar+1
        parre  => mu
        call Parall(9_ip)
        newrho =  mu(solve_sol(1)%ngrou*nbvar+1)

     else if( solve_sol(1)%kfl_gathe == 2 ) then
        !
        ! All reduce for rho and send/receive for mu
        !     
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        call Parall(803_ip)

     end if

  end if
#if defined(DLB) 
  call dlb_disable()
#endif

#ifdef TRACING
  call extrae_eventandcounters(401,0)
#endif
end subroutine bsyje5

subroutine bsyje0(itask,nbnodes,nbvar,an,ja,ia,qq,wa1,rr,ngrou,newrho,mu)
  !-----------------------------------------------------------------------
  ! Objective:  Multiply a symmetric matrix stored in BCSR by a vector.
  !                                wa1 = [A] qq
  !
  !             Only the LOWER triangle is stored. 
  !             JA elements are stored in increasing column order
  !-----------------------------------------------------------------------
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  NPOIN_REAL_12DI,parr1,icoml,&
       &                                parre,nparr,npoi1,npoi2,npoi3,&
       &                                INOTMASTER,IPARALL,ISEQUEN,ISLAVE,kfl_paral
  use def_solver, only               :  solve_sol
  implicit none
  integer(ip), intent(in)            :: itask,nbnodes,nbvar,ngrou
  real(rp),    intent(in)            :: an(nbvar,nbvar,*)
  integer(ip), intent(in)            :: ja(*),ia(*)
  real(rp),    intent(in)            :: qq(nbvar,nbnodes),rr(nbvar,nbnodes)
  real(rp),    intent(inout), target :: wa1(nbvar,nbnodes)
  real(rp),    intent(out),   target :: mu(ngrou+1)
  real(rp),    intent(out)           :: newrho
  real(rp),                   target :: dummr_par(1) 
  integer(ip)                        :: ii,jj,kk,mm,igrou,ipoin
  real(rp)                           :: raux,raux2

  !----------------------------------------------------------------
  !
  ! BSYMAX, PRODXY
  !
  !----------------------------------------------------------------

#ifdef TRACING
  call extrae_eventandcounters(401,1)
#endif
  newrho = 0.0_rp

  if( INOTMASTER ) then

     if( nbvar > 1 ) then

        call runend('programme le faignasse')

     else if( nbvar == 1 ) then
        !
        ! Lower triangle + diag
        !
        do ii= 1, nbnodes
           wa1(1,ii) = 0.0_rp
        end do

        if( ISEQUEN ) then

           do ii= 1, nbnodes
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 mm            = ja(jj)
                 raux          = raux   + an(1,1,jj) * qq(1,mm)
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux   + an(1,1,kk) * qq(1,ja(kk))
              newrho    = newrho + rr(1,ii)   * qq(1,ii)
           end do

        else

           do ii= 1, npoi1
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 mm            = ja(jj)
                 raux          = raux          + an(1,1,jj) * qq(1,mm)
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux + an(1,1,kk) * qq(1,ja(kk))
              newrho    = newrho + rr(1,ii) * qq(1,ii)
           end do
           do ii= npoi1+1,npoi2-1
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 raux          = raux          + an(1,1,jj) * qq(1,ja(jj))
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux + an(1,1,kk) * qq(1,ja(kk))
           end do
           do ii= npoi2,npoi3
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 mm            = ja(jj)
                 raux          = raux          + an(1,1,jj) * qq(1,mm)
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux + an(1,1,kk) * qq(1,ja(kk))
              newrho    = newrho + rr(1,ii) * qq(1,ii)
           end do
           do ii= npoi3+1,nbnodes
              raux  = 0.0_rp
              raux2 = qq(1,ii)
              do jj= ia(ii), ia(ii+1)-2
                 raux          = raux          + an(1,1,jj) * qq(1,ja(jj))
                 wa1(1,ja(jj)) = wa1(1,ja(jj)) + an(1,1,jj) * raux2
              end do
              kk        = ia(ii+1)-1
              wa1(1,ii) = raux + an(1,1,kk) * qq(1,ja(kk))
           end do
        end if

     else
        call runend('bcsrmvsym_d: Wrong NBVAR parameter')
     end if

  end if

  !----------------------------------------------------------------
  !
  ! WTVECT
  !
  !----------------------------------------------------------------

  do igrou=1,solve_sol(1)%ngrou*nbvar
     mu(igrou)=0.0_rp
  end do

  if( INOTMASTER ) then

     if( nbvar == 1 ) then  
        do ipoin=1,nbnodes
           igrou=solve_sol(1)%lgrou(ipoin)
           if(igrou>0) mu(igrou)=mu(igrou)+wa1(1,ipoin)
        end do
     end if

  end if

  if( IPARALL ) then

     if( solve_sol(1)%kfl_gathe == 1 ) then
        !
        ! All reduce for rho and all gatherv for mu
        !     
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        icoml        =  solve_sol(1)%icoml
        call Parall(34_ip)

     else if( solve_sol(1)%kfl_gathe == 0 ) then
        !
        ! All reduce for mu and rho
        !
        mu(solve_sol(1)%ngrou*nbvar+1) = newrho
        nparr  =  solve_sol(1)%ngrou*nbvar+1
        parre  => mu
        call Parall(9_ip)
        newrho =  mu(solve_sol(1)%ngrou*nbvar+1)

     else if( solve_sol(1)%kfl_gathe == 2 ) then
        !
        ! All reduce for rho and send/receive for mu
        !
        dummr_par(1) =  newrho
        nparr        =  1
        parre        => dummr_par
        call Parall(9_ip)
        newrho       =  dummr_par(1)

        parre        => mu
        call Parall(803_ip)

     end if

  end if


#ifdef TRACING
  call extrae_eventandcounters(401,0)
#endif
end subroutine bsyje0
