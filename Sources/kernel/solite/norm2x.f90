subroutine norm2x(nbvar,xx,sumxx)
  !------------------------------------------------------------------------
  !****f* solite/norm2x
  ! NAME 
  !    prodxy
  ! DESCRIPTION
  !    This routine computes the 2-norm (Eucledian norm) of a vector XX:
  !    SUMXX = ||XX||_2 = ( sum_i |XX_i|^2 )^{1/2}
  ! INPUT
  !    NBVAR .................... Number of variables per node
  !    XX(NBVAR,NPOIN) .......... Vector
  ! OUTPUT
  !    SUMXX .................... Eucledian norm
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  npoin
  use def_master, only     :  kfl_paral,npoi1,npoi2,npoi3,parre
  use def_master, only     :  nparr,npari,nparc,npoi4,lpoi4

  implicit none
  integer(ip), intent(in)  :: nbvar
  real(rp),    intent(in)  :: xx(*)
  real(rp),    intent(out) :: sumxx
  real(rp),    target      :: dummr_par(1) 
  integer(ip)              :: ii,jj,kk,ll

  sumxx = 0.0_rp

  if( kfl_paral == -1 ) then
     !
     ! Sequential
     !
     do ii = 1,nbvar*npoin
        sumxx = sumxx + xx(ii) * xx(ii)
     end do

  else if( kfl_paral >= 1 ) then
     !
     ! Parallel: slaves
     !
     if( nbvar == 1 ) then  
        do ii = 1,npoi1
           sumxx  = sumxx  + xx(ii) * xx(ii)
        end do
        do ii = npoi2,npoi3
           sumxx  = sumxx  + xx(ii) * xx(ii)
        end do
        do jj = 1,npoi4
           ii = lpoi4(jj)
           sumxx  = sumxx  + xx(ii) * xx(ii)
        end do

     else 
        do ii = 1,npoi1*nbvar
           sumxx  = sumxx  + xx(ii) * xx(ii)
        end do
        do ii = (npoi2-1)*nbvar+1,npoi3*nbvar
           sumxx  = sumxx  + xx(ii) * xx(ii)
        end do

        do jj = 1,npoi4
           ii = lpoi4(jj)
           ll = (ii-1)*nbvar
           do kk = 1,nbvar
              ll = ll + 1
              sumxx  = sumxx  + xx(ll) * xx(ll)
           end do
        end do

     end if
  end if
  !
  ! Parallel: reduce sum
  !
  npari = 0
  nparc = 0
  if( kfl_paral >= 0 ) then
     nparr        =  1
     dummr_par(1) =  sumxx
     parre        => dummr_par
     call Parall(9_ip)           
     sumxx        =  dummr_par(1)
  end if

  sumxx = sqrt(sumxx)

end subroutine norm2x
