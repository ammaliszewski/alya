subroutine prodts(nbvar,nbnodes,tt,ss,sumtt,sumts)
  !------------------------------------------------------------------------
  !****f* solite/prodts
  ! NAME 
  !    prodxy
  ! DESCRIPTION
  !    This routine computes the sclara product of two vectors and the
  !    norm of one of these:
  !    SUMTT = TT.TT
  !    SUMTS = TT.SS
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_master, only     :  kfl_paral,npoi1,npoi2,npoi3,parre,nparr
  use def_master, only     :  npoi4,lpoi4
  implicit none
  integer(ip), intent(in)  :: nbvar,nbnodes
  real(rp),    intent(in)  :: tt(*),ss(*)
  real(rp),    intent(out) :: sumtt,sumts
  real(rp),    target      :: dummr_par(2) 
  integer(ip)              :: kk,ii,jj,ll

  sumtt = 0.0_rp
  sumts = 0.0_rp

  if(kfl_paral==-1) then
     !
     ! Sequential
     !
     do kk=1,nbvar*nbnodes
        sumtt = sumtt + tt(kk) * tt(kk)
        sumts = sumts + tt(kk) * ss(kk)
     end do

  else if(kfl_paral>=1) then
     !
     ! Parallel: Slaves
     !
     if(nbvar==1) then
        do ii=1,npoi1
           sumtt  = sumtt  + tt(ii) * tt(ii)
           sumts  = sumts  + tt(ii) * ss(ii)
        end do
        do ii=npoi2,npoi3
           sumtt  = sumtt  + tt(ii) * tt(ii)
           sumts  = sumts  + tt(ii) * ss(ii)
        end do
        do jj = 1,npoi4
           ii = lpoi4(jj)
           sumtt  = sumtt  + tt(ii) * tt(ii)
           sumts  = sumts  + tt(ii) * ss(ii)           
        end do
     else
        ll=0
        do ii=1,npoi1
           do kk=1,nbvar
              ll=ll+1
              sumtt  = sumtt  + tt(ll) * tt(ll)
              sumts  = sumts  + tt(ll) * ss(ll)
           end do
        end do
        ll=(npoi2-1)*nbvar
        do ii=npoi2,npoi3
           do kk=1,nbvar
              ll=ll+1
              sumtt  = sumtt  + tt(ll) * tt(ll)
              sumts  = sumts  + tt(ll) * ss(ll)
           end do
        end do
        do jj = 1,npoi4
           ii = lpoi4(jj)
           ll = (ii-1)*nbvar
           do kk = 1,nbvar
              ll = ll + 1
              sumtt  = sumtt  + tt(ll) * tt(ll)
              sumts  = sumts  + tt(ll) * ss(ll)
           end do
        end do
     end if
  end if

  if(kfl_paral>=0) then
     !
     ! Parallel: reduce sum
     !
     nparr        =  2
     dummr_par(1) =  sumtt
     dummr_par(2) =  sumts
     parre        => dummr_par
     call Parall(9_ip)           
     sumtt        =  dummr_par(1)
     sumts        =  dummr_par(2)
  end if

end subroutine prodts

