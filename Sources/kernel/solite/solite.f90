subroutine solite(rhsid,unkno,amatr,pmatr)
  !------------------------------------------------------------------------
  !****f* solite/solite
  ! NAME 
  !    solite
  ! DESCRIPTION
  !    This routine drives the library to solve a linear system of equations
  !    by iterative methods.
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_master, only :  INOTSLAVE,IMASTER,ISLAVE,NPOIN_TYPE
  use def_domain, only :  r_sym,c_sym,c_sol,r_sol,r_dom,c_dom,npoin
  use def_solver 
  implicit none
  real(rp), target :: rhsid(*)
  real(rp), target :: unkno(*)
  real(rp), target :: amatr(*)
  real(rp)         :: pmatr(*)
  real(rp)         :: cpre1,cpre2

  call cputim(cpre1)
  if( solve_sol(1) % kfl_schur == 1 ) then
     !
     ! Schur complement solver
     ! 
     if( IMASTER ) then
        call solshu(1_ip,1_ip,1_ip,rhsid,unkno,amatr)
     else
        call solshu(solve_sol(1) % nzmat,solve_sol(1) % ndofn,npoin,rhsid,unkno,amatr)
     end if

  else if( solve_sol(1) % kfl_algso == 1 ) then
     !
     ! CG from PLS
     !
     if( solve_sol(1) % kfl_symme == 1 ) then
        call cgrpls( &
             solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
             solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
             solve_sol(1) % kfl_resid,  solve_sol(1) % solco,&
             amatr, pmatr, &
             solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
             solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
             c_sym, r_sym, rhsid, unkno )
     else
        call cgrpls( &
             solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
             solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
             solve_sol(1) % kfl_resid,  solve_sol(1) % solco,&
             amatr, pmatr, &
             solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
             solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
             c_dom, r_dom, rhsid, unkno )
     end if

  else if( solve_sol(1) % kfl_algso == 2 ) then
     !
     ! Deflated CG 
     !
     if( solve_sol(1) % kfl_symme == 1 ) then
        call deflcg( &
             solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
             solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
             solve_sol(1) % kfl_resid, solve_sol(1) % solco, &
             amatr, pmatr, solve_sol(1) % kfl_cvgso, &
             solve_sol(1) % lun_cvgso, solve_sol(1) % kfl_solve, &
             solve_sol(1) % lun_solve, c_sym, r_sym, rhsid, unkno )
     else
        call deflcg( &
             solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
             solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
             solve_sol(1) % kfl_resid, solve_sol(1) % solco, & 
             amatr, pmatr, solve_sol(1) % kfl_cvgso, &
             solve_sol(1) % lun_cvgso, solve_sol(1) % kfl_solve, &
             solve_sol(1) % lun_solve, c_dom, r_dom, rhsid, unkno )
     end if
#ifdef NINJA
  else if( solve_sol(1) % kfl_algso == 102 ) then
     !
     ! GPU Deflated CG 
     !
     call GPUDEFLCG( &
          solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
          solve_sol(1) % ngrou, solve_sol(1) % nskyl, &
          solve_sol(1) % nzgro, &
          solve_sol(1) % ia, solve_sol(1) % ja, &
          solve_sol(1) % miter, &
          amatr, r_dom, c_dom, rhsid, unkno,solve_sol(1) % lgrou )
#endif
  else if( solve_sol(1) % kfl_algso == 5 ) then
     !
     ! Bi-CGSTAB from PLS
     !
     call bcgpls( &
          solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
          solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
          solve_sol(1) % solco,     amatr, pmatr, &
          solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
          solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
          c_sol, r_sol, rhsid, unkno )

  else if( solve_sol(1) % kfl_algso == 8 ) then
     !
     ! GMRES from PLS 
     !
     call gmrpls( &
          solve_sol(1) % nequa,     solve_sol(1) % ndofn,&
          solve_sol(1) % kfl_preco, solve_sol(1) % miter,&
          solve_sol(1) % nkryd,     solve_sol(1) % kfl_ortho, &
          solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso, &
          solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve, &
          solve_sol(1) % solco, &
          amatr, pmatr, c_sol, r_sol,rhsid, unkno )
#ifdef NINJA
  else if( solve_sol(1) % kfl_algso == 101 ) then
     !
     ! GMRES GPU 
     !
     call GPUGMRES( &
          solve_sol(1) % nequa,     solve_sol(1) % ndofn,&
          solve_sol(1) % miter,& 
          solve_sol(1) % nkryd,&
          amatr, r_sol, c_sol,rhsid, unkno )
#endif

  else if( solve_sol(1) % kfl_algso ==  9 .or. &
       &   solve_sol(1) % kfl_algso == 10 ) then
     !
     ! Richardson solver
     !
     call solric(solve_sol(1) % nequa,solve_sol(1) % ndofn,rhsid,unkno,amatr,pmatr)  

  else if( solve_sol(1) % kfl_algso == 11 ) then
     !
     ! Richardson
     !
     if( solve_sol(1) % kfl_symme == 1 ) then
        call richar( &
             solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
             solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
             solve_sol(1) % solco,     amatr, pmatr, &
             solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
             solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
             c_sym, r_sym, rhsid, unkno )
     else
        call richar( &
             solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
             solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
             solve_sol(1) % solco,     amatr, pmatr, &
             solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
             solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
             c_sol, r_sol, rhsid, unkno )
     end if

  else if( solve_sol(1) % kfl_algso == 12 ) then
     !
     ! Deflated Bi-CGSTAB
     !
     call defbcg( &
          solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
          solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
          solve_sol(1) % solco,     amatr, pmatr, &
          solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
          solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
          c_sol, r_sol, rhsid, unkno )

  else if( solve_sol(1) % kfl_algso == 13 ) then
     !
     ! Deflated GMRES
     !
     call defgmr( &
          solve_sol(1) % nequa,     solve_sol(1) % ndofn,&
          solve_sol(1) % kfl_preco, solve_sol(1) % miter, solve_sol(1) % nkryd,&
          solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
          solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
          solve_sol(1) % solco, amatr, pmatr, c_sol, r_sol, rhsid, unkno )

  else if( solve_sol(1) % kfl_algso == 15 ) then
     !
     ! Steepest descent
     !
     if( solve_sol(1) % kfl_symme == 1 ) then
        call steepe( &
             solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
             solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
             solve_sol(1) % kfl_resid, solve_sol(1) % solco,&
             amatr, pmatr, &
             solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
             solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
             c_sym, r_sym, rhsid, unkno )
     else
        call steepe( &
             solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
             solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
             solve_sol(1) % kfl_resid, solve_sol(1) % solco,&
             amatr, pmatr, &
             solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
             solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
             c_dom, r_dom, rhsid, unkno )
     end if
#ifdef NINJA
  else if(solve_sol(1) % kfl_algso == 100) then
     call GPUCG( &
          solve_sol(1) % nequa,solve_sol(1) % ndofn, &
          solve_sol(1) % miter,&
          amatr,r_dom, c_dom, rhsid, unkno )
#endif
  else if( solve_sol(1) % kfl_algso == SOL_SOLVER_PIPELINED_CG         .or. &
       &   solve_sol(1) % kfl_algso == SOL_SOLVER_PIPELINED_DEFLATED_CG ) then
     !
     ! Pipelined CG
     !
     call pipelined_CG( &
          solve_sol(1) % nequa,     solve_sol(1) % ndofn, &
          solve_sol(1) % kfl_preco, solve_sol(1) % miter, &
          solve_sol(1) % kfl_resid, solve_sol(1) % solco,&
          amatr, pmatr, &
          solve_sol(1) % kfl_cvgso, solve_sol(1) % lun_cvgso,&
          solve_sol(1) % kfl_solve, solve_sol(1) % lun_solve,&
          c_dom, r_dom, rhsid, unkno )
  end if
  !
  ! Force solution continuity across subdomain boundaries
  !
  if( solve_sol(1) % kfl_force == 1 .and. ISLAVE ) then
     call pararr('SLE',NPOIN_TYPE,solve_sol(1) % ndofn*npoin,unkno)
  end if
  !
  ! Solver statistics
  ! 
  solve_sol(1) % itsol(3) = solve_sol(1) % itsol(3) + iters
  if( iters < solve_sol(1) % itsol(1)) solve_sol(1) % itsol(1) = iters
  if( iters > solve_sol(1) % itsol(2)) solve_sol(1) % itsol(2) = iters
  !
  ! Output solver information
  !
  call cputim(cpre2)
  if( solve_sol(1) % kfl_cvgso == 1 .and. INOTSLAVE ) &
       call flush( solve_sol(1) % lun_cvgso )
  if( INOTSLAVE ) then
     if( solve_sol(1) % kfl_solve == 1 ) then
        if( resi2 > 0.0_rp ) then
           if( resi1 /= 0.0_rp ) then
              write(solve_sol(1) % lun_solve,110)&
                   iters,resin,resfi,solve_sol(1) % resi2,solve_sol(1) % resf2,&
                   log10(resi2/resi1),cpre2-cpre1,solve_sol(1) % xorth
           else
              write(solve_sol(1) % lun_solve,110)&
                   iters,resin,resfi,solve_sol(1) % resi2,solve_sol(1) % resf2,&
                   0.0_rp,cpre2-cpre1,solve_sol(1) % xorth   
           end if
        else
           write(solve_sol(1) % lun_solve,110)&
                iters,resin,resfi,solve_sol(1) % resi2,solve_sol(1) % resf2,&
                1.0_rp,cpre2-cpre1,solve_sol(1) % xorth   
        end if
        call flush(solve_sol(1) % lun_solve)
     end if
  end if

110 format(i7,18(2x,e12.6)) 

end subroutine solite
