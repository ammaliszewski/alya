subroutine cgupls( &
     nbnodes, nbvar, idprecon, maxiter, kfl_resid, &
     eps, an, pn, kfl_cvgso, lun_cvgso, kfl_solve, &
     lun_outso, ja, ia, bb, xx )

  !-----------------------------------------------------------------------
  ! Objective: Solve    [A] [M]^-1  [M] xx = bb  
  !                        [A']       xx'  = bb'   by the CG method. 
  !
  !            Three preconditioners are possible:
  !            idprecon = 0 => [M]^-1 = [M] = I
  !            idprecon = 1 => Diag. Scaling
  !
  !            [A] [M]^-1  must be symmetric positive defined. 
  !            [A] can be full or half stored (lower triangle).
  !            The elements in each row are stored in increasing column order
  !
  !            If Diag. Scaling is selected the preconditioned system is:
  !                    D^-1/2 [A] D^-1/2   D^1/2 x  =  D^-1/2 b
  !                          [A']              x'   =      b'
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp,lg
  use def_master, only     :  party,pardi,parki,pard1,parr1,&
       &                      nparr,parre,npoi1,npoi2,npoi3,&
       &                      NPOIN_REAL_12DI,IMASTER
  use def_solver, only     :  memit,SOL_NO_PRECOND,&
       &                      SOL_SQUARE,SOL_DIAGONAL,SOL_MATRIX,&
       &                      resin,resfi,resi1,resi2,iters
  use def_domain, only     :  nzsym
  use mod_memchk
  implicit none
  integer(ip), intent(in)  :: nbnodes, nbvar, idprecon, maxiter, kfl_resid
  integer(ip), intent(in)  :: kfl_cvgso, lun_cvgso
  integer(ip), intent(in)  :: kfl_solve, lun_outso
  real(rp),    intent(in)  :: eps
  real(rp),    intent(in)  :: an(nbvar, nbvar, *),pn(*)
  integer(ip), intent(in)  :: ja(*), ia(*)
  real(rp),    intent(in)  :: bb(*)
  real(rp),    intent(out) :: xx(*)
  integer(ip)              :: ii, jj, kk, ll, nrows, ierr, npoin
  integer(4)               :: istat
  real(rp)                 :: alpha, beta, rho, raux1, raux2, stopcri, resid
  real(rp)                 :: invnb, newrho, resi3, raux3, invn3
  real(rp),    pointer     :: rr(:), pp(:), qq(:), invdiag(:)
  real(rp),    pointer     :: wa1(:), wa2(:)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

  ierr  = 0
  resi1 = 1.0_rp
  if( IMASTER ) then
     nrows = 1                ! Minimum memory for working arrays
     npoin = 0                ! master does not perform any loop
  else
     npoin = nbnodes
     nrows = nbnodes * nbvar
  end if
  !
  ! Sequential and slaves: wWorking arrays
  !
  allocate(rr(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'RR','cgupls',rr)
  allocate(pp(nrows),stat=istat)  
  call memchk(0_ip,istat,memit,'PP','cgupls',pp)
  allocate(qq(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'QQ','cgupls',qq)
  allocate(wa1(nrows),stat=istat) 
  call memchk(0_ip,istat,memit,'WA1','cgupls',wa1)
  allocate(invdiag(nrows),stat=istat)
  call memchk(0_ip,istat,memit,'INVDIAG','cgupls',invdiag)
  if(idprecon==SOL_MATRIX) then
     allocate(wa2(nrows),stat=istat) 
     call memchk(0_ip,istat,memit,'WA2','cgupls',wa2)
  end if
  if( IMASTER ) nrows = 0 ! Master does not perform any loop
  !
  ! RAUX1   = ||L^{-1}b||_2
  ! RAUX2   = ||x||_1
  ! INVDIAG = 1 / sqrt(|diag(A)|)
  !
  if(idprecon==SOL_SQUARE.or.idprecon==SOL_DIAGONAL) then
     !
     ! WA1: Diagonal D
     !
     if(nbvar==1) then
        do ii= 1, npoin 
           jj = ia(ii)
           ll = -1
           do while (jj< ia(ii+1) .and. ll ==-1)
              if(ja(jj)==ii) then
                 ll = jj
              end if
              jj = jj+1
           end do
           if(ll/=-1) then
              wa1(ii)=an(1,1,ll)
           end if
        end do
     else
        do ii= 1, npoin 
           jj = ia(ii)
           ll = -1
           do while (jj< ia(ii+1) .and. ll ==-1)
              if(ja(jj)==ii) then
                 ll = jj
              end if
              jj = jj+1
           end do
           if(ll/=-1) then
              jj = (ii-1) * nbvar
              do kk= 1, nbvar
                 wa1(jj+kk)=an(kk,kk,ll)
              end do
           end if
        end do
     end if
     !
     ! Periodicity and Parallelization
     !
     call rhsmod(nbvar,wa1) 
     !
     ! WA1= |D|^-1/2
     !
     if(idprecon==SOL_SQUARE) then
        if(nbvar==1) then
           do ii= 1, npoin
              wa1(ii) = sqrt( abs(wa1(ii)) )
           end do
        else
           do ii= 1, npoin
              jj = (ii-1) * nbvar
              do kk= 1, nbvar
                 wa1(jj+kk) = sqrt( abs(wa1(jj+kk)) )
              end do
           end do
        end if
     end if
     !
     ! INVDIAG = |D|^-1/2 or D^-1
     ! PP      = |D|^-1/2 b of D^-1 b
     !
     do ii= 1, nrows
        invdiag(ii) = 1.0_rp / wa1(ii)
        pp(ii)      = invdiag(ii) * bb(ii)
     end do
     call norm2x(nbvar,pp,raux1)
     call norm1x(nbvar,xx,raux2)

  else if(idprecon==SOL_MATRIX) then
     !
     ! Matrix preconditioning P
     !
     call bcsrax( 1_ip, npoin, nbvar, pn, ja, ia, bb, pp )
     call norm2x(nbvar,pp,raux1)
     call norm1x(nbvar,xx,raux2)

  else if(idprecon==SOL_NO_PRECOND) then
     !
     ! No preconditioning: raux1 = || b ||
     !
     call norm2x(nbvar,bb,raux1)
     call norm1x(nbvar,xx,raux2)

  else
     !
     ! Wrong preconditioning
     !
     call runend('CGUPLS: WRONG PRECONDITIONER')

  end if
  !
  ! stop criterion = ||L^{-1} b|| * eps
  !
  if(kfl_resid==1) then
     call norm2x(nbvar,bb,raux3)
     stopcri = eps * raux3
     if(raux3/=0.0_rp) invn3 = 1.0_rp / raux3
  else
     stopcri = eps * raux1
     if(raux1/=0.0_rp) invn3 = 1.0_rp / raux1
  end if
  kk = 0
  !
  ! ||L^{-1}b||=0: trivial solution x=0
  !
  if(raux1<=1.0e-12) then
     do ii= 1, nrows
        xx(ii) = 0.0_rp
     end do
     goto 10
  end if

  invnb = 1.0_rp / raux1

  if(raux2==0.0_rp) then
     !
     ! Initial x is zero: r = b
     !
     do ii= 1, nrows
        rr(ii) = bb(ii)
     end do
  else 
     if(idprecon==SOL_MATRIX) then
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx, wa2 )
        call bcsrax( 1_ip, npoin, nbvar, pn, ja, ia, wa2, rr )
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )
     end if
     !
     ! r = b - Ax
     !
     do ii= 1, nrows
        rr(ii) = bb(ii) - rr(ii)
     end do
  end if
  !
  ! r = L^{-1} ( b - Ax )
  !
  if(idprecon==SOL_SQUARE.or.idprecon==SOL_DIAGONAL) then

     do ii= 1, nrows
        rr(ii) = invdiag(ii) * rr(ii)
     end do

  else if(idprecon==SOL_MATRIX) then

     call bcsrax( 1_ip, npoin, nbvar, pn, ja, ia, rr, wa2 )
     do ii= 1, nrows
        rr(ii) = wa2(ii)
     end do

  end if
  !
  ! rho^0 = <r0,r0> = ||r0||^2
  !
  call norm2x(nbvar,rr,resid)
  newrho = resid*resid
  if(kfl_resid==1) then
     call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx,  pp ) 
     do ii= 1, nrows
        pp(ii) = bb(ii) - pp(ii)
     end do
     call norm2x(nbvar,pp,resid)
     resin  = resid*invn3
  else
     resin  = resid*invnb
  end if
  !
  ! Test if the initial guess is the solution
  !
  if ( resid<=stopcri ) goto 10
  !
  ! Initial x = L x 
  !
  if ( idprecon==SOL_SQUARE ) then
     do ii= 1, nrows
        xx(ii) = wa1(ii) * xx(ii)
     end do
  end if

  !-----------------------------------------------------------------
  !
  !  MAIN LOOP
  !
  !-----------------------------------------------------------------

  do while( kk<maxiter .and. resid>stopcri )

     if(kk==0) then       
        !
        ! Initial p^{k+1} = r^k
        !
        do ii= 1, nrows
           pp(ii) = rr(ii)
        end do

     else 
        !
        ! beta  = rho^k / rho^{k-1}  
        !
        beta = newrho / rho
        !
        ! p^{k+1} = r^k + beta*p^k
        !
        do ii= 1, nrows
           pp(ii) = rr(ii) + beta * pp(ii)
        end do

     end if
     !
     ! q^{k+1} = L^{-1} A R^{-1} p^{k+1}
     !
     if (idprecon==SOL_NO_PRECOND) then 

        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, pp, qq )

     else if(idprecon==SOL_SQUARE) then

        do ii= 1, nrows
           wa1(ii) = invdiag(ii) * pp(ii)
        end do
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, wa1, qq )                 
        do ii= 1, nrows
           qq(ii) = invdiag(ii) * qq(ii)
        end do

     else if(idprecon==SOL_DIAGONAL) then

        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, pp, qq )                 
        do ii= 1, nrows
           qq(ii) = invdiag(ii) * qq(ii)
        end do

     else if (idprecon==SOL_MATRIX) then

        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, pp, wa2 )
        call bcsrax( 1_ip, npoin, nbvar, pn, ja, ia, wa2, qq )

     end if
     !
     ! alpha = rho^k / <p^{k+1},q^{k+1}>
     !
     call prodxy(nbvar,npoin,pp,qq,alpha)
     if (alpha==0.0_rp) then
        ierr = 2
        goto 10
     end if
     alpha = newrho / alpha
     !
     ! x^{k+1} = x^k + alpha*p^{k+1}
     !
     do ii= 1, nrows
        xx(ii) = xx(ii) + alpha * pp(ii)
     end do
     !
     ! r^{k+1} = r^k - alpha*q^{k+1}
     !
     do ii= 1, nrows
        rr(ii) = rr(ii) - alpha * qq(ii)
     end do
     !
     ! rho^{k+1} = ||r^{k+1}||^2
     !
     call norm2x(nbvar,rr,resid)
     resi3  = resid*invnb
     rho    = newrho
     newrho = resid*resid
     kk     = kk + 1

     if(kfl_resid==1) then
        if ( idprecon==SOL_SQUARE ) then
           do ii= 1, nrows
              qq(ii) = invdiag(ii) * xx(ii)
           end do
           call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, qq,  wa1 )
        else
           call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx,  wa1 )
        end if
        do ii= 1, nrows
           wa1(ii) = bb(ii) - wa1(ii)
        end do
        call norm2x(nbvar,wa1,resid)        
     end if
     !
     ! Convergence post process:
     ! kk    = iteration number
     ! resi1 = preconditioned or real residual
     ! resi3 = preconditioned residual
     !
     resi2  = resi1
     resi1  = resid*invn3
     if(kfl_cvgso==1) write(lun_cvgso,100) kk,resi1,resi3

  end do

  !-----------------------------------------------------------------
  !
  !  END MAIN LOOP
  !
  !-----------------------------------------------------------------
  !
  ! x = R^-1 x
  !
  if ( idprecon==SOL_SQUARE ) then
     do ii= 1, nrows
        xx(ii) = invdiag(ii) * xx(ii)
     end do
  end if

  !open(unit=68,file='A.txt',status='unknown')
  !open(unit=69,file='b.txt',status='unknown')
  !do ii= 1, nbnodes
  !   do jj= ia(ii), ia(ii+1)-1
  !      write(68,*) ii,ja(jj),an(1,1,jj)
  !   end do
  !   write(69,*) bb(ii)
  !end do
  !close(68)
  !close(69)
  !stop

  !open(unit=70,file='x.txt',status='unknown')
  !do ii= 1, nbnodes
  !   write(70,*) xx(ii)
  !end do
  !close(70)
  !stop

10 resfi = resi1 
  iters = kk

  if(kfl_solve==1) then
     if(ierr/=0) write(lun_outso,120) kk
  end if

  if(idprecon==SOL_MATRIX) then
     call memchk(2_ip,istat,memit,'WA2','cgupls',wa2)
     deallocate(wa2,stat=istat)
     if(istat/=0) call memerr(2_ip,'WA2','cgupls',0_ip)
  end if
  call memchk(2_ip,istat,memit,'INVDIAG','cgupls',invdiag)
  deallocate(invdiag,stat=istat)
  if(istat/=0) call memerr(2_ip,'INVDIAG','cgupls',0_ip)
  call memchk(2_ip,istat,memit,'WA1','cgupls',wa1)
  deallocate(wa1,stat=istat)
  if(istat/=0) call memerr(2_ip,'WA1','cgupls',0_ip)
  call memchk(2_ip,istat,memit,'QQ','cgupls',qq)
  deallocate(qq,stat=istat)
  if(istat/=0) call memerr(2_ip,'QQ','cgupls',0_ip)
  call memchk(2_ip,istat,memit,'PP','cgupls',pp)
  deallocate(pp,stat=istat)
  if(istat/=0) call memerr(2_ip,'PP','cgupls',0_ip)
  call memchk(2_ip,istat,memit,'RR','cgupls',rr)
  deallocate(rr,stat=istat)
  if(istat/=0) call memerr(2_ip,'RR','cgupls',0_ip)

100 format(i7,3(1x,e12.6))
110 format(i5,18(2x,e12.6))
120 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = rho^k / <p^{k+1},q^{k+1}>')

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine cgupls
