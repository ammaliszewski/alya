subroutine coafin(nbvar,alpha,xx,yy)
  !
  ! Solve y = y + alpha * (W.A-1.W^t) x
  !
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  INOTMASTER,kfl_paral
  use def_solver, only       :  solve_sol
  use def_domain, only       :  npoin
  use mod_csrdir
  implicit none
  integer(ip), intent(in)    :: nbvar
  real(rp),    intent(in)    :: alpha
  real(rp),    intent(inout) :: xx(*)
  real(rp),    intent(out)   :: yy(*)
  integer(ip)                :: nrows,ngrou,ii
  real(rp),    pointer       :: mu(:),murhs(:)
  real(rp),    pointer       :: mubig(:)

!return

  ngrou = solve_sol(1) % ngrou
  nrows = npoin * nbvar

  allocate( mu(ngrou*nbvar),murhs(ngrou*nbvar) ) 
  !
  ! murhs = W^T.p
  !
  call wtvect(npoin,ngrou,nbvar,murhs,xx)
  !
  ! mu = A'-1.murhs
  !         
  if( INOTMASTER ) then
     call CSR_LUsol(                   &
          ngrou,nbvar                , &
          solve_sol(1) % invpRpredef , solve_sol(1) % invpCpredef , &
          solve_sol(1) % ILpredef    , solve_sol(1) % JLpredef    , &
          solve_sol(1) % LNpredef    , solve_sol(1) % IUpredef    , &
          solve_sol(1) % JUpredef    , solve_sol(1) % UNpredef    , &
          murhs                      , mu                      )  
     !
     ! x = W.mu
     !
     allocate( mubig(nrows) )
     call wvect(npoin,nbvar,mu,mubig) 
     !
     ! y = y + x: coarse correction
     !
     do ii = 1,nrows
        yy(ii) = yy(ii) + alpha * mubig(ii)
     end do
     deallocate( mubig )
  end if
  deallocate( mu,murhs ) 

end subroutine coafin

subroutine fincoa(nbvar,alpha,xx,yy)
  !
  ! Solve y = y + alpha * (W.A-1.W^t) x
  !
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  INOTMASTER,kfl_paral
  use def_solver, only       :  solve_sol
  use def_domain, only       :  npoin
  use mod_solver, only       :  solver_smvp
  use mod_csrdir
  implicit none
  integer(ip), intent(in)    :: nbvar
  real(rp),    intent(in)    :: alpha
  real(rp),    intent(inout) :: xx(*)
  real(rp),    intent(out)   :: yy(*)
  integer(ip)                :: nrows,ngrou,ii
  real(rp),    pointer       :: mu(:),murhs(:)
  real(rp),    pointer       :: mubig(:)

!return

  ngrou = solve_sol(1) % ngrou
  nrows = npoin * nbvar

  allocate( mu(ngrou*nbvar),murhs(ngrou*nbvar) ) 
  !
  ! murhs = W^T.p
  !
  call wtvect(npoin,ngrou,nbvar,murhs,xx)
  !
  ! A' murhs
  !
  if( INOTMASTER ) then
     call solver_smvp(ngrou,nbvar,nbvar,solve_sol(1) % askylpredef,&
          solve_sol(1) % JLpredef,solve_sol(1) % ILpredef,murhs,mu)
     !
     ! x = W.mu
     !
     allocate( mubig(nrows) )
     call wvect(npoin,nbvar,mu,mubig) 
     !
     ! y = y + x
     !
     do ii = 1,nrows
        yy(ii) = yy(ii) + alpha * mubig(ii)                                   ! Coarse correction
     end do
     deallocate( mubig )
  end if

  deallocate( mu,murhs ) 

end subroutine fincoa
