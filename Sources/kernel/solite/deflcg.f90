!-----------------------------------------------------------------------
!> @addtogroup AlgebraicSolvers
!> @{
!> @file    cgrpls.f90
!> @author  Guillaume Houzeaux
!> @brief   Deflated conjugate gradient solver
!> @details Deflated conjugate gradient solver:
!!          @verbatim
!!          1. Compute r0 := b - Ax0, z0 = M-1r0, and p0 := z0
!!          2. For j = 0, 1,..., until convergence Do:
!!          3. aj   := (rj, zj)/(Apj, pj)
!!          4. xj+1 := xj + ajpj
!!          5. rj+1 := rj - ajApj 
!!          6. zj+1 := M-1 rj+1
!!          7. bj   := (rj+1, zj+1)/(rj, zj)
!!          8. pj+1 := zj+1 + bjpj
!!          9. EndDo
!!          @endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine deflcg( &
     nbnodes, nbvar, idprecon, maxiter, kfl_resid, &
     eps, an, pn, kfl_cvgso, lun_cvgso, kfl_solve, &
     lun_outso, ja, ia, bb, xx )
  use def_kintyp, only       :  ip,rp,lg
  use def_master, only       :  IMASTER,INOTMASTER,kfl_paral,npoi1,&
       &                        IPARALL,kfl_async,parre,ISLAVE,lninv_loc,&
       &                        INOTSLAVE,kfl_timin
  use def_solver, only       :  memit,SOL_NO_PRECOND,SOL_LINELET,&
       &                        SOL_SQUARE,SOL_DIAGONAL,SOL_MATRIX,&
       &                        resin,resfi,resi1,resi2,iters,solve_sol
  use mod_memory
  use mod_csrdir
  use mod_matrix
  use def_coupli,         only : mcoup
  implicit none

  integer(ip), intent(in)    :: nbnodes,nbvar,idprecon,maxiter,kfl_resid
  integer(ip), intent(in)    :: kfl_cvgso,lun_cvgso
  integer(ip), intent(in)    :: kfl_solve,lun_outso
  real(rp),    intent(in)    :: eps
  real(rp),    intent(in)    :: an(nbvar,nbvar,*),pn(*)
  integer(ip), intent(in)    :: ja(*),ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(inout) :: xx(nbvar*nbnodes)
  integer(ip)                :: ii,nrows,ierr,npoin,ngrou
  integer(ip)                :: nskyl,info,igrou,jj,kk,kcoup
  integer(4)                 :: istat
  real(rp)                   :: alpha,beta,rho,stopcri,resid,raux1
  real(rp)                   :: invnb,newrho,dummr,time1,time2
  real(rp),    pointer       :: rr(:),pp(:),zz(:),invdiag(:)
  real(rp),    pointer       :: ww(:),mu(:),p0(:)
  real(rp),    pointer       :: murhs(:),muold(:)
  integer(ip), pointer       :: iagro(:),jagro(:)

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

  ngrou = solve_sol(1) % ngrou
  !
  ! Skyline, CSR or dense format 
  !
  if( ngrou > 0 ) then
     if( solve_sol(1) % kfl_defas == 0 ) then
        nskyl =  solve_sol(1) % nskyl
     else if( solve_sol(1) % kfl_defas == 1 ) then
        nskyl =  solve_sol(1) % nzgro * nbvar * nbvar
        iagro => solve_sol(1) % ia
        jagro => solve_sol(1) % ja
     else if( solve_sol(1) % kfl_defas == 2 ) then
        nskyl =  ngrou * ngrou
     end if
  end if

  if( IMASTER ) then
     nrows = 1                ! Minimum memory for working arrays
     npoin = 0                ! master does not perform any loop
  else
     nrows = nbnodes * nbvar
     npoin = nbnodes
  end if
  !
  ! Sequential and slaves: Working arrays
  ! 
  nullify(rr)
  nullify(pp)
  nullify(p0)
  nullify(zz)
  nullify(ww)
  nullify(invdiag)
  nullify(mu)
  nullify(solve_sol(1) % askyldef)
  nullify(murhs)
  nullify(muold)
  call memory_alloca(memit,'RR'     ,'deflcg',rr     ,nrows)
  call memory_alloca(memit,'PP'     ,'deflcg',pp     ,nrows)
  call memory_alloca(memit,'P0'     ,'deflcg',p0     ,nrows)
  call memory_alloca(memit,'ZZ'     ,'deflcg',zz     ,nrows)
  call memory_alloca(memit,'WW'     ,'deflcg',ww     ,nrows)
  call memory_alloca(memit,'INVDIAG','deflcg',invdiag,nrows)

  if( IMASTER ) nrows = 0 ! Master does not perform any loop

  if( ngrou > 0 ) then

     !----------------------------------------------------------------------
     !
     ! Allocate memory for groups
     !
     !----------------------------------------------------------------------

     call memory_alloca(memit,'MU','deflcg',mu,nbvar*ngrou+1_ip)
     call memory_alloca(memit,'ASKYLDEF','deflcg',solve_sol(1) % askyldef,nskyl)
 
     if( solve_sol(1) % kfl_defas /= 0 ) then
        call memory_alloca(memit,'MURHS','deflcg',murhs,nbvar*ngrou)
     end if
     if( solve_sol(1) % kfl_defso == 1 ) then
        call memory_alloca(memit,'MUOLD','deflcg',muold,nbvar*ngrou)
     end if
 
     !----------------------------------------------------------------------
     !
     ! Compute A'= W^T.A.W
     !
     !----------------------------------------------------------------------

     if( solve_sol(1) % kfl_symme == 1 ) then
        call matgro(ngrou,npoin,nskyl,nbvar,ia,ja,an,solve_sol(1) % askyldef)
     else
        if( solve_sol(1) % kfl_defas == 2 ) then 
           !
           ! Dense
           !
           call matgr3(ngrou,npoin,nskyl,nbvar,ia,ja,an,solve_sol(1) % askyldef)
        else
           !
           ! CSR and Skyline
           !
           call matgr2(ngrou,npoin,nskyl,nbvar,ia,ja,an,solve_sol(1) % askyldef)
        end if
     end if

     !----------------------------------------------------------------------
     !
     ! Factorize A'
     !
     !----------------------------------------------------------------------

     if( solve_sol(1) % kfl_defso == 0 .and. INOTMASTER ) then
        if( solve_sol(1) % kfl_defas == 1 ) then
           !
           ! Inverse CSR matrix ASKYLDEF
           !
           nullify(solve_sol(1) % Ildef)
           nullify(solve_sol(1) % Jldef)
           nullify(solve_sol(1) % Lndef)
           nullify(solve_sol(1) % iUdef)
           nullify(solve_sol(1) % jUdef)
           nullify(solve_sol(1) % Undef)                                ! Permutation arrays
           nullify(solve_sol(1) % invpRdef)
           nullify(solve_sol(1) % invpCdef)
           call CSR_LU_Factorization(&                                  ! CSR LU Factorization  
                ngrou,nbvar,iagro,jagro,solve_sol(1) % askyldef,&
                solve_sol(1) % ILdef,solve_sol(1) % JLdef,&
                solve_sol(1) % LNdef,solve_sol(1) % IUdef,&
                solve_sol(1) % JUdef,solve_sol(1) % UNdef,info) 
           if( info /= 0 ) call runend('DEFLCG: SINGULAR MATRIX')

        else
           !
           ! Inverse skyline matrix ASKYL
           !
           call chofac(&
                nbvar*ngrou,solve_sol(1) % nskyl,&
                solve_sol(1) % iskyl,solve_sol(1) % askyldef,info)
           if( info /= 0 ) call runend('MATGRO: ERROR WHILE DOING CHOLESKY FACTORIZATION')
        end if
     end if 

     !----------------------------------------------------------------------
     !
     ! X_0: Compute pre-initial guess 
     !
     !----------------------------------------------------------------------

     if( solve_sol(1) % kfl_symme == 1 ) then
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )      ! A.x_{-1}
     else if( solve_sol(1) % kfl_schum == 1 ) then
        !if( solve_sol(1) % kfl_schum == 1 .and. INOTMASTER ) then
        !   call matrix_invdia(&
        !        nbnodes,solve_sol(1) % ndofn_A3,solve_sol(1) % kfl_symme,ia,ja,&
        !        solve_sol(1) % A3,solve_sol(1) % invA3,memit)
        !end if
        call bcsrax_schur( 1_ip, npoin, nbvar, solve_sol(1) % ndofn_A3 , &
             solve_sol(1) % A1, solve_sol(1) % A2, solve_sol(1) % invA3, &
             solve_sol(1) % A4, ja, ia, xx, rr )  
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )      ! A.x_{-1}
     end if
     do ii= 1, nrows
        rr(ii) = bb(ii) - rr(ii)                                   ! r_{-1} = b-A.x_{-1}
     end do

     call wtvect(npoin,ngrou,nbvar,mu,rr)                          ! W^T.r_{-1}

     if( solve_sol(1) % kfl_defso == 1 .and. solve_sol(1) % kfl_defas == 2 ) then
        
        if( IMASTER ) print*,'SACAR ESOOOOOOOOO'
        do igrou = 1,ngrou*nbvar
           murhs(igrou) = mu(igrou)
           mu(igrou)    = muold(igrou)
        end do
        !call conjg2( &
        !     ngrou, nbvar, 1000_ip, 1.0e-12_rp,    &
        !     solve_sol(1) % askyldef,              &
        !     murhs, mu )
        do igrou = 1,ngrou*nbvar
           muold(igrou) = mu(igrou)
        end do

     else if( INOTMASTER ) then   

        if( solve_sol(1) % kfl_defso == 1 ) then
           !
           ! Iterative sequential solver
           !
           do igrou = 1,ngrou*nbvar
              murhs(igrou) = mu(igrou)
              mu(igrou)    = muold(igrou)
           end do
           call conjgr( &
                ngrou, nbvar, 1000_ip, 1.0e-12_rp,    &
                solve_sol(1) % askyldef,              &
                solve_sol(1) % ja, solve_sol(1) % ia, &
                murhs, mu )
           do igrou = 1,ngrou*nbvar
              muold(igrou) = mu(igrou)
           end do
        else
           !
           ! Direct solver
           !
           if( solve_sol(1) % kfl_defas == 0 ) then
              call chosol(&
                   solve_sol(1) % ngrou*nbvar,solve_sol(1) % nskyl,&
                   solve_sol(1) % iskyl,1_ip,solve_sol(1) % askyldef,mu,&
                   solve_sol(1) % ngrou*nbvar,info)    
              if( info /= 0 ) &
                   call runend('MATGRO: COULD NOT SOLVE INITIAL SYSTEM')
           else
              do igrou = 1,nbvar*ngrou
                 murhs(igrou) = mu(igrou)
              end do
              call CSR_LUsol(                                          &
                   ngrou                   , nbvar                   , &
                   solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
                   solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
                   solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
                   solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
                   murhs                   , mu                      )  
           end if
        end if
     end if
  
     call wvect(npoin,nbvar,mu,rr)                                 ! W.mu
     do ii = 1,nrows
        xx(ii) = xx(ii) + rr(ii)                                   ! x0 = x_{-1} + W.mu
     end do

  end if

  !----------------------------------------------------------------------
  !
  ! Initial computations
  !
  !----------------------------------------------------------------------

  call solope(&
       1_ip, npoin, nrows, nbvar, idprecon, eps, an, pn, ja, ia, bb, xx , &
       ierr, stopcri, newrho, resid, invnb, rr, zz, pp, ww, &
       invdiag, p0 )

  if( ierr /= 0 ) goto 10

  !----------------------------------------------------------------------
  !
  ! Deflated: Modify initial pp
  !
  !----------------------------------------------------------------------

  if( ngrou > 0 ) then 
     !
     ! Solve A'.mu = W^T.A.r^0
     !
     if( solve_sol(1) % kfl_symme == 1 ) then   
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, zz, ww ) ! A.z^0
     else if( solve_sol(1) % kfl_schum == 1 ) then
        call bcsrax_schur( 1_ip, npoin, nbvar, solve_sol(1) % ndofn_A3 , &
             solve_sol(1) % A1, solve_sol(1) % A2, solve_sol(1) % invA3, &
             solve_sol(1) % A4, ja, ia, zz, ww )  
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, zz, ww ) ! A.z^0
     end if
     call wtvect(npoin,ngrou,nbvar,mu,ww)
 
     if( INOTMASTER ) then
        if( solve_sol(1) % kfl_defso == 1 ) then
           !
           ! Iterative solver
           !
           do igrou = 1,ngrou*nbvar
              murhs(igrou) = mu(igrou)
              mu(igrou)    = muold(igrou)
           end do
           call conjgr( &
                ngrou, nbvar, 1000_ip, 1.0e-12_rp,    &
                solve_sol(1) % askyldef,              &
                solve_sol(1) % ja, solve_sol(1) % ia, &
                murhs, mu )
           do igrou = 1,ngrou*nbvar
              muold(igrou) = mu(igrou)
           end do
        else
           !
           ! Direct solver
           !
           if( solve_sol(1) % kfl_defas == 0 ) then
              call chosol(&
                   solve_sol(1) % ngrou*nbvar,solve_sol(1) % nskyl,&
                   solve_sol(1) % iskyl,1_ip,solve_sol(1) % askyldef,mu,&
                   solve_sol(1) % ngrou*nbvar,info)    
           else
              do igrou = 1,ngrou*nbvar
                 murhs(igrou) = mu(igrou)
              end do
              call CSR_LUsol(                                          &
                   ngrou                   , nbvar                   , &
                   solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
                   solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
                   solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
                   solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
                   murhs                   , mu                      )  
           end if
        end if
     end if
     call wvect(npoin,nbvar,mu,ww)
     !
     ! Initial p^{k+1} = z^k - W.mu^k
     !
     do ii= 1, nrows
        pp(ii) = pp(ii) - ww(ii)
        p0(ii) = pp(ii)
     end do
  end if

  !-----------------------------------------------------------------
  !
  !  MAIN LOOP
  !
  !-----------------------------------------------------------------

  do while( iters < maxiter .and. resid > stopcri )
     !
     ! q^{k+1} =  A R^-1 p^{k+1}
     !
     call precon(&
          4_ip,nbvar,npoin,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,ww,pp,zz)
     !
     ! alpha = rho^k / <p^{k+1},q^{k+1}>
     !
     call prodxy(nbvar,npoin,pp,zz,alpha)

     if( alpha == 0.0_rp ) then
        ierr = 2
        goto 10
     end if
     rho   = newrho

     alpha = newrho / alpha
     !
     ! x^{k+1} = x^k + alpha*p^{k+1}
     !
     do ii= 1, nrows
        xx(ii) = xx(ii) + alpha * pp(ii)
     end do
     !
     ! r^{k+1} = r^k - alpha*q^{k+1}
     !
     do ii= 1, nrows
        rr(ii) = rr(ii) - alpha * zz(ii)
     end do
     !
     !  L z^{k+1} = r^{k+1}
     !
     call precon(&
          3_ip,nbvar,npoin,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,ww,rr,zz)
     !
     ! Solve A'.mu = W^T.A.z^k
     !
     if( ngrou > 0 ) then 
        ! A.z^k, rho=r.z, W^T.A.z^k
        if( solve_sol(1) % kfl_symme == 1 ) then   
           call bsyje0(1_ip, npoin, nbvar, an, ja, ia, zz, ww ,rr, ngrou, newrho, mu)
        else
           call bsyje5(1_ip, npoin, nbvar, an, ja, ia, zz, ww ,rr, ngrou, newrho, mu)
        end if

        if( kfl_timin == 1 ) call Parall(20_ip)
        call cputim(time1)

        if( INOTMASTER ) then
           if( solve_sol(1) % kfl_defso == 1 ) then
              !
              ! Iterative solver
              !
              do igrou = 1,ngrou*nbvar
                 murhs(igrou) = mu(igrou)
                 mu(igrou)    = 0.0_rp
              end do
              call conjgr( &
                   ngrou, nbvar, 1000_ip, 1.0e-12_rp,    &
                   solve_sol(1) % askyldef,              &
                   solve_sol(1) % ja, solve_sol(1) % ia, &
                   murhs, mu )
           else
              !
              ! Direct solver
              !
              if( solve_sol(1) % kfl_defas == 0 ) then
                 call chosol(&
                      solve_sol(1) % ngrou*nbvar,solve_sol(1) % nskyl,&
                      solve_sol(1) % iskyl,1_ip,solve_sol(1) % askyldef,mu,&
                      solve_sol(1) % ngrou*nbvar,info) 
              else
                 do igrou = 1,ngrou*nbvar
                    murhs(igrou) = mu(igrou)
                 end do
                 call CSR_LUsol(                                          &
                      ngrou                   , nbvar                   , &
                      solve_sol(1) % invpRdef , solve_sol(1) % invpCdef , &
                      solve_sol(1) % ILdef    , solve_sol(1) % JLdef    , &
                      solve_sol(1) % LNdef    , solve_sol(1) % IUdef    , &
                      solve_sol(1) % JUdef    , solve_sol(1) % UNdef    , &
                      murhs                   , mu                      )   
              end if
           end if
        end if
        call wvect(npoin,nbvar,mu,ww)

        if( kfl_timin == 1 ) call Parall(20_ip)
        call cputim(time2)
        solve_sol(1) % cputi(4) = solve_sol(1) % cputi(4) + time2 - time1

     else
        call prodxy(nbvar,npoin,rr,zz,newrho)
     end if
     !
     ! beta  = rho^k / rho^{k-1}  
     !
     beta = newrho / rho 
     !
     ! p^{k+1} = z^k + beta*p^k - W.mu^k
     !
     if( ngrou > 0 ) then 
        do ii= 1, nrows
           pp(ii) = zz(ii) + beta * pp(ii) - ww(ii)
        end do
     else
        do ii= 1, nrows
           pp(ii) = zz(ii) + beta * pp(ii)
        end do
     end if

     resid  = sqrt(newrho)
     resi2  = resi1
     resi1  = resid * invnb
     iters  = iters + 1
     !
     ! Convergence post process:
     ! kk    = iteration number
     ! resi1 = preconditioned residual
     !
     if( kfl_cvgso == 1 ) &
          call outcso(nbvar,nrows,npoin,invdiag,an,ja,ia,bb,xx)

  end do

  !-----------------------------------------------------------------
  !
  !  END MAIN LOOP
  !
  !-----------------------------------------------------------------

10 continue
  call solope(&
       2_ip, npoin, nrows, nbvar, idprecon, dummr, an, dummr, ja, ia, bb, xx , &
       ierr, dummr, dummr, resi1, dummr, rr, p0, pp, dummr, &
       invdiag, dummr )

  if( kfl_solve == 1 ) then
     if( ierr > 0 ) write(lun_outso,120) iters
  end if

  !-----------------------------------------------------------------
  !
  ! Deallocate memory
  !
  !-----------------------------------------------------------------

  if( ngrou > 0 ) then

     call memory_deallo(memit,'ASKYLDEF','deflcg',solve_sol(1) % askyldef)
     call memory_deallo(memit,'MU','deflcg',mu)

     if( solve_sol(1) % kfl_defas == 1 ) then
        call memory_deallo(memit,'MURHS','deflcg',murhs)     
     end if
     if( solve_sol(1) % kfl_defso == 1 ) then
        call memory_deallo(memit,'MUOLD','deflcg',muold)
     end if
     if( solve_sol(1) % kfl_defso == 0 .and. solve_sol(1) % kfl_defas == 1 ) then
        if( INOTMASTER ) then         
           call CSR_LUfin(&
                solve_sol(1) % ILdef,solve_sol(1) % JLdef,solve_sol(1) % LNdef,&
                solve_sol(1) % IUdef,solve_sol(1) % JUdef,solve_sol(1) % UNdef)      
        end if
     end if

  end if

  call memory_deallo(memit,'INVDIAG','deflcg',invdiag)
  call memory_deallo(memit,'WW'     ,'deflcg',ww)
  call memory_deallo(memit,'ZZ'     ,'deflcg',zz)
  call memory_deallo(memit,'P0'     ,'deflcg',p0)
  call memory_deallo(memit,'PP'     ,'deflcg',pp)
  call memory_deallo(memit,'RR'     ,'deflcg',rr)

  !mcoup = kcoup

110 format(i5,18(2x,e12.6))
120 format(&
       & '# Error at iteration ',i6,&
       & 'Dividing by zero: alpha = rho^k / <p^{k+1},q^{k+1}>')

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine deflcg
