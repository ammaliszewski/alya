subroutine bcsrax(itask,nbnodes,nbvar,an,ja,ia,xx,yy) 
  !----------------------------------------------------------------------
  !****f* mathru/bcsrax
  ! NAME 
  !     bcsrax
  ! DESCRIPTION
  !     Multiply a non symmetric matrix stored in BCSR by a vector
  !     YY = A XX 
  ! INPUT
  !    NBNODES .... Number of equations
  !    NBVAR ...... Number of variables
  !    AN ......... Matrix
  !    JA ......... List of elements
  !    IA ......... Pointer to list of elements
  !    XX ......... Vector
  ! OUTPUT
  !    YY ......... result vector
  ! USES
  ! USED BY
  !***
  !----------------------------------------------------------------------
  use def_kintyp, only             :  ip,rp
  use def_master, only             :  INOTMASTER,kfl_async,IPARALL
  use def_master, only             :  NPOIN_TYPE,lninv_loc
#ifdef MYTIMING
  use def_master, only             :  time_bcsrax
#endif
  implicit none
  integer(ip), intent(in)          :: itask,nbnodes,nbvar
  real(rp),    intent(in)          :: an(nbvar,nbvar,*)
  integer(ip), intent(in)          :: ja(*),ia(*)
  real(rp),    intent(in)          :: xx(nbvar,*)
  real(rp),    intent(out), target :: yy(nbvar,*)
  integer(ip)                      :: ii,jj,kk,ll,col
  real(rp)                         :: raux,raux1,raux2,raux3

#ifdef MYTIMING
  real(rp)    :: time_begin, time_end                   ! Counters for timing

  ! Start timing counter
  call cputim(time_begin)
#endif

  if( IPARALL .and. kfl_async == 1 .and. itask /= 2 ) then

     call bcsrai(itask,nbnodes,nbvar,an,ja,ia,xx,yy) 

  else if( INOTMASTER ) then

#ifdef EVENT_MATVEC
     ! Compute elements
     call mpitrace_eventandcounters(400,1)
#endif

     if( nbvar == 1 ) then
        !
        ! NBVAR=1
        !
        !$OMP PARALLEL  DO                              &
        !$OMP SCHEDULE ( STATIC )                       &
        !$OMP DEFAULT  ( NONE )                         &
        !$OMP SHARED   ( an, ia, ja, nbnodes, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux )            
        !
        do ii = 1,nbnodes
           yy(1,ii) = 0.0_rp
           do jj   = ia(ii),ia(ii+1)-1
              col  = ja(jj)
              raux = xx(1,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux
           end do
        end do

     else if( nbvar == 2 ) then
        !
        ! NBVAR=2
        !
        !$OMP PARALLEL  DO                              &
        !$OMP SCHEDULE ( STATIC )                       &
        !$OMP DEFAULT  ( NONE )                         &
        !$OMP SHARED   ( an, ia, ja, nbnodes, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux1, raux2 )    
        !
        do ii = 1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
           end do
        end do

     else if( nbvar == 3 ) then
        !
        ! NBVAR=3
        !
        !$OMP PARALLEL  DO                                     &
        !$OMP SCHEDULE ( STATIC )                              &
        !$OMP DEFAULT  ( NONE )                                &
        !$OMP SHARED   ( an, ia, ja, nbnodes, xx, yy )         &
        !$OMP PRIVATE  ( col, ii, jj, raux1, raux2, raux3 )    
        !
        do ii = 1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              raux3    = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux3
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux3
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux1
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux2
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux3
           end do

        end do

     else if( nbvar == 4 ) then
        !
        ! NBVAR=4
        !
        !$OMP PARALLEL  DO                              &
        !$OMP SCHEDULE ( STATIC )                       &
        !$OMP DEFAULT  ( NONE )                         &
        !$OMP SHARED   ( an, ia, ja, nbnodes, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, raux )            
        !
        do ii = 1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           yy(4,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux     = xx(1,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(1,4,jj) * raux
              raux     = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(2,4,jj) * raux
              raux     = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(3,4,jj) * raux
              raux     = xx(4,col)
              yy(1,ii) = yy(1,ii) + an(4,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(4,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(4,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(4,4,jj) * raux
           end do

        end do

     else
        !
        ! NBVAR = whatever
        !
        !$OMP PARALLEL  DO                                     &
        !$OMP SCHEDULE ( STATIC )                              &
        !$OMP DEFAULT  ( NONE )                                &
        !$OMP SHARED   ( an, ia, ja, nbnodes, nbvar, xx, yy )  &
        !$OMP PRIVATE  ( col, ii, jj, kk, ll, raux )           
        !
        do ii = 1,nbnodes
           do kk = 1,nbvar
              yy(kk,ii) = 0.0_rp
           end do
           do jj  = ia(ii),ia(ii+1)-1
              col = ja(jj)
              do ll = 1,nbvar
                 raux = xx(ll,col)
                 do kk = 1,nbvar
                    yy(kk,ii) = yy(kk,ii) + an(ll,kk,jj) * raux
                 end do
              end do
           end do
        end do

     end if

#ifdef EVENT_MATVEC
     ! Send/redv boundary contributions elements
     call mpitrace_eventandcounters(400,2)
#endif

     !
     ! Modify YY due do periodicity and Parall service
     !
!do ii = 1,nbnodes
!   if(lninv_loc(ii)==13)print*,'a=',yy(1,ii)
!end do
!do ii = 1,nbnodes
!   if(    lninv_loc(ii)==1.or.&
!        & lninv_loc(ii)==2.or.&
!        & lninv_loc(ii)==3.or.&
!        & lninv_loc(ii)==4.or.&
!        & lninv_loc(ii)==6.or.&
!        & lninv_loc(ii)==7) yy(1,ii)=1.0_rp
!   if(    lninv_loc(ii)==1) yy(1,ii)=3.0_rp
!   if(    lninv_loc(ii)==4) yy(1,ii)=2.0_rp
!end do

     if( itask == 1 ) call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,yy)

!do ii = 1,nbnodes
!   if(lninv_loc(ii)==13)print*,'b=',yy(1,ii)
!end do

#ifdef EVENT_MATVEC
     ! Send/redv boundary contributions elements
     call mpitrace_eventandcounters(400,0)
#endif

  end if

#ifdef MYTIMING
  ! Stop timing counter
  call cputim(time_end)
  time_bcsrax = time_bcsrax + abs(time_end-time_begin)
#endif

end subroutine bcsrax


subroutine bcsrax2(itask,nbnodes,nbvar,an,ja,ia,xx,yy) 
  !----------------------------------------------------------------------
  !****f* mathru/bcsrax
  ! NAME 
  !     bcsrax
  ! DESCRIPTION
  !     Multiply a non symmetric matrix stored in BCSR by a vector
  !     YY = A XX 
  ! INPUT
  !    NBNODES .... Number of equations
  !    NBVAR ...... Number of variables
  !    AN ......... Matrix
  !    JA ......... List of elements
  !    IA ......... Pointer to list of elements
  !    XX ......... Vector
  ! OUTPUT
  !    YY ......... result vector
  ! USES
  ! USED BY
  !***
  !----------------------------------------------------------------------
  use def_kintyp, only             :  ip,rp
  use def_master, only             :  INOTMASTER,npoi1,npoi2,npoi3
  use def_master, only             :  NPOIN_TYPE
  implicit none
  integer(ip), intent(in)          :: itask,nbnodes,nbvar
  real(rp),    intent(in)          :: an(nbvar,nbvar,*)
  integer(ip), intent(in)          :: ja(*),ia(*)
  real(rp),    intent(in)          :: xx(nbvar,*)
  real(rp),    intent(out), target :: yy(nbvar,*)
  integer(ip)                      :: ii,jj,kk,ll,col
  real(rp)                         :: raux,raux1,raux2,raux3

  if( INOTMASTER ) then

     if( nbvar == 1 ) then
        !
        ! NBVAR=1
        !
        !*OMP   PARALLEL DO SCHEDULE (GUIDED)        & 
        !*OMP   DEFAULT (NONE)                       &
        !*OMP   PRIVATE ( ii, jj, col, raux)         &
        !*OMP   SHARED ( nbnodes, xx, yy, ia, ja, an)
        do ii = npoi1+1,nbnodes
           yy(1,ii) = 0.0_rp
           do jj   = ia(ii),ia(ii+1)-1
              col  = ja(jj)
              raux = xx(1,col)
              yy(1,ii) = yy(1,ii) +an(1,1,jj) * raux
           end do
        end do
        if( itask == 1 ) call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,yy)
        do ii = 1,npoi1
           yy(1,ii) = 0.0_rp
           do jj   = ia(ii),ia(ii+1)-1
              col  = ja(jj)
              raux = xx(1,col)
              yy(1,ii) = yy(1,ii) +an(1,1,jj) * raux
           end do
        end do

     else if( nbvar == 2 ) then
        !
        ! NBVAR=2
        !
        !*OMP   PARALLEL DO SCHEDULE (GUIDED)         & 
        !*OMP   DEFAULT (NONE)                        &
        !*OMP   PRIVATE ( ii, jj, col, raux1, raux2)  &
        !*OMP   SHARED ( nbnodes, xx, yy, ia, ja, an)
        do ii = npoi1+1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
           end do
        end do
        if( itask == 1 ) call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,yy)
        do ii = 1,npoi1
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
           end do
        end do

     else if( nbvar == 3 ) then
        !
        ! NBVAR=3
        !
        !*OMP   PARALLEL DO SCHEDULE (GUIDED)             & 
        !*OMP   DEFAULT (NONE)                            &
        !*OMP   PRIVATE ( ii, jj, col, raux1,raux2,raux3) &
        !*OMP   SHARED ( nbnodes, xx, yy, ia, ja, an)
        do ii = npoi1+1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              raux3    = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux3
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux3
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux1
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux2
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux3
           end do

        end do
        if( itask == 1 ) call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,yy)
        do ii = 1,npoi1
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux1    = xx(1,col)
              raux2    = xx(2,col)
              raux3    = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux1
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux2
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux3
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux1
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux2
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux3
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux1
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux2
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux3
           end do

        end do

     else if( nbvar == 4 ) then
        !
        ! NBVAR=4
        !
        !*OMP   PARALLEL DO SCHEDULE (GUIDED)        & 
        !*OMP   DEFAULT (NONE)                       &
        !*OMP   PRIVATE ( ii, jj, col, raux)         &
        !*OMP   SHARED ( nbnodes, xx, yy, ia, ja, an)
        do ii = npoi1+1,nbnodes
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           yy(4,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux     = xx(1,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(1,4,jj) * raux
              raux     = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(2,4,jj) * raux
              raux     = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(3,4,jj) * raux
              raux     = xx(4,col)
              yy(1,ii) = yy(1,ii) + an(4,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(4,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(4,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(4,4,jj) * raux
           end do

        end do
        if( itask == 1 ) call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,yy)
        do ii = 1,npoi1
           yy(1,ii) = 0.0_rp
           yy(2,ii) = 0.0_rp
           yy(3,ii) = 0.0_rp
           yy(4,ii) = 0.0_rp
           do jj       = ia(ii),ia(ii+1)-1
              col      = ja(jj)
              raux     = xx(1,col)
              yy(1,ii) = yy(1,ii) + an(1,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(1,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(1,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(1,4,jj) * raux
              raux     = xx(2,col)
              yy(1,ii) = yy(1,ii) + an(2,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(2,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(2,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(2,4,jj) * raux
              raux     = xx(3,col)
              yy(1,ii) = yy(1,ii) + an(3,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(3,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(3,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(3,4,jj) * raux
              raux     = xx(4,col)
              yy(1,ii) = yy(1,ii) + an(4,1,jj) * raux
              yy(2,ii) = yy(2,ii) + an(4,2,jj) * raux
              yy(3,ii) = yy(3,ii) + an(4,3,jj) * raux
              yy(4,ii) = yy(4,ii) + an(4,4,jj) * raux
           end do

        end do

     else
        !
        ! NBVAR = whatever
        !
        !*OMP   PARALLEL DO SCHEDULE (GUIDED)               & 
        !*OMP   DEFAULT (NONE)                              &
        !*OMP   PRIVATE ( ii, jj, kk, ll, col, raux)        &
        !*OMP   SHARED ( nbnodes, nbvar, xx, yy, ia, ja, an)
        do ii = npoi1+1,nbnodes
           do kk = 1,nbvar
              yy(kk,ii) = 0.0_rp
           end do
           do jj  = ia(ii),ia(ii+1)-1
              col = ja(jj)
              do ll = 1,nbvar
                 raux = xx(ll,col)
                 do kk = 1,nbvar
                    yy(kk,ii) = yy(kk,ii) + an(ll,kk,jj) * raux
                 end do
              end do
           end do
        end do
        if( itask == 1 ) call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,yy)
        do ii = 1,npoi1
           do kk = 1,nbvar
              yy(kk,ii) = 0.0_rp
           end do
           do jj  = ia(ii),ia(ii+1)-1
              col = ja(jj)
              do ll = 1,nbvar
                 raux = xx(ll,col)
                 do kk = 1,nbvar
                    yy(kk,ii) = yy(kk,ii) + an(ll,kk,jj) * raux
                 end do
              end do
           end do
        end do

     end if
     !
     ! Modify YY due do periodicity and Parall service
     !
     !if( itask == 1 )  call pararr('SLX',NPOIN_TYPE,nbvar*nbnodes,yy)

  end if

end subroutine bcsrax2
