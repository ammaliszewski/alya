!-----------------------------------------------------------------------
!> @addtogroup AlgebraicSolvers
!> @{
!> @file    gmrpls.f90
!> @author  Guillaume Houzeaux
!> @brief   GMRES right preconditioned solver
!> @details GMRES right preconditioned solver:
!!          @verbatim
!!          1. Start: Choose x0 and a dimension m 
!!          2. Arnoldi process:
!!             - Compute r0 = b - Ax0, b = ||r0||_2 and v1 = r0/b. 
!!             - For j = 1,...,m do
!!               - Compute zj := M -1vj 
!!               - Compute w := Azj
!!               - for i = 1,...,j, do : 
!!                   hi,j := (w, vi) 
!!                   w    := w - hi,jvi
!!                 enddo
!!             - hj+1,1 = ||w||2
!!             - vj+1 = w/hj+1,1 
!!             - Define Vm := [v1,....,vm] and Hm = {hi,j}. 
!!          3. Form the approximate solution: xm = x0 + M -1Vmym
!!             where ym = argmin_y||b*e1-Hmy||2 and e1 = [1, 0, . . . , 0]T . 
!!          4. Restart: If satisfied stop, else set x0 <= xm and goto 2.
!!          @endverbatim
!> @} 
!-----------------------------------------------------------------------
subroutine gmrpls( &
     nbnodes, nbvar,&
     idprecon, maxiter, kryldim, kfl_ortho, &
     kfl_cvgso, lun_cvgso,&
     kfl_solve, lun_outso,&
     eps, an, pn, ja, ia, bb, xx )
  !-----------------------------------------------------------------------
  !
  ! Objective: Solve    [L]^-1 [A] [R]^-1  [R] xx  = [L]^-1 bb
  !                            [A']            xx' =        bb'   
  ! by the GMRES(m) method.
  !
  !            Three preconditioners are possible:
  !            idprecon = 0 => [L] = I, [R] = I
  !            idprecon = 1 => [L] = [R] = sqrt(abs(diag([A])))
  !            idprecon = 2 => [L] = diag([A]), [R] = I
  !
  !
  !            If Diag. Scaling is selected the preconditioned system is:
  !                    D^-1/2 [A] D^-1/2   D^1/2 x  =  D^-1/2 b
  !                          [A']              x'   =      b'
  !
  !
  !  kryldim : Dimension of the Krylov basis
  !  kryl    : Working matrix with the Krylov basis.
  !            It must be allocated with:  nbnodes*nbvar*(kryldim+1)
  !  hh      : [H]. It is stored by columns. Its dimension is: 
  !            (kryldim+1) * kryldim. It must be allocated with: 
  !            SUM (i=1, kryldim) {ii + 1} = 
  !                                    kryldim + (kryldim * kryldim+1) / 2
  !  cc, ss  : Givens rotations. They must be allocated with:  kryldim
  !  rs      : Right Hand side of the LSQ problem.
  !            It must be allocated with: kryldim+1
  !
  !  iters : present iteration
  !  jj    : present iteration relative to [1 ... kryldim]
  !
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp,lg
  use def_master, only     :  party,pardi,parki,pard1,parr1,kfl_paral,&
       &                      nparr,parre,npoi1,npoi2,npoi3,intost,&
       &                      kfl_ptask,npari,nparc,modul,IMASTER,modul,&
       &                      INOTSLAVE,kfl_timin
  use def_solver, only     :  memit,SOL_NO_PRECOND,SOL_GAUSS_SEIDEL,&
       &                      SOL_SQUARE,SOL_DIAGONAL,SOL_MATRIX,&
       &                      SOL_LINELET,&
       &                      resin,resfi,resi1,resi2,iters,solve_sol
  use mod_memchk

  use mod_couplings, only  :  COU_INTERPOLATE_NODAL_VALUES
  use mod_couplings, only  :  I_AM_IN_COUPLING
  use mod_couplings, only  :  I_AM_INVOLVED_IN_A_COUPLING_TYPE
  use mod_parall,    only  :  PAR_NODE_NUMBER_OF_PARTITIONS
  use def_coupli,    only  :  mcoup_subdomain
  use def_coupli,    only  :  mcoup
  use def_coupli,    only  :  coupling_type
  use def_coupli,    only  :  DIRICHLET_IMPLICIT
  use def_coupli,    only  :  BETWEEN_SUBDOMAINS
  use def_coupli,    only  :  PROJECTION
  use def_master,    only  :  INOTMASTER
  use def_coupli,    only  :  coupling_type,UNKNOWN
  implicit none
  integer(ip), intent(in)    :: nbnodes,nbvar,idprecon,maxiter,kryldim
  integer(ip), intent(in)    :: kfl_ortho
  integer(ip), intent(in)    :: kfl_cvgso,lun_cvgso,kfl_solve,lun_outso
  real(rp),    intent(in)    :: eps
  real(rp),    intent(inout) :: an(nbvar, nbvar, *)
  real(rp),    intent(inout) :: pn(nbvar, nbvar, *)
  integer(ip), intent(in)    :: ja(*),ia(*)
  real(rp),    intent(inout) :: bb(*)
  real(rp),    intent(out)   :: xx(*)
  logical(lg)                :: convergence,fin2
  integer(ip)                :: ii,jj,kk,jj1,idx,nrows,npoin,ierr
  integer(4)                 :: istat
  real(rp)                   :: resid,gamma,stopcri,invnb,dummr
  real(rp)                   :: time1,time2
  real(rp),    parameter     :: epsmac=1.0e-16_rp

  real(rp),    allocatable   :: wa1(:), wa3(:), kryl(:,:)
  real(rp),    pointer       :: invdiag(:)
  real(rp),    allocatable   :: cc(:), ss(:), rs(:), hh(:)

  integer(ip),  pointer :: lneig(:)
  real(rp),     pointer :: xx_tmp(:)
  real(rp)              :: relax
  integer(ip)  :: icoup

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

  convergence = .false.
  fin2        = .false.

  if( IMASTER ) then
     nrows   = 1                ! Minimum memory for working arrays
     npoin   = 0                ! master does not perform any loop
  else
     npoin   = nbnodes
     nrows   = nbnodes * nbvar
  end if
  !
  ! Allocate memory of arrays with Krylov dimension dependent
  !
  allocate(cc(kryldim),stat=istat)
  call memchk(0_ip,istat,memit,'CC','gmrpls',cc)
  allocate(ss(kryldim),stat=istat)
  call memchk(0_ip,istat,memit,'SS','gmrpls',ss)
  allocate(rs(kryldim+1),stat=istat)
  call memchk(0_ip,istat,memit,'RS','gmrpls',rs)
  allocate(hh(kryldim+((kryldim*(kryldim+1))/2)),stat=istat)
  call memchk(0_ip,istat,memit,'HH','gmrpls',hh)
  !
  ! Allocate memory for large arrays
  !
  allocate(wa1(nrows),stat=istat)
  call memchk(0_ip,istat,memit,'WA1','gmrpls',wa1)
  allocate(invdiag(nrows),stat=istat)
  call memchk(0_ip,istat,memit,'INVDIAG','gmrpls',invdiag)
  allocate(wa3(nrows),stat=istat)
  call memchk(0_ip,istat,memit,'WA3','gmrpls',wa3)
  allocate(kryl(nrows,kryldim+1),stat=istat)
  call memchk(0_ip,istat,memit,'KRYL','gmrpls',kryl)

  if( INOTMASTER .and. I_AM_INVOLVED_IN_A_COUPLING_TYPE(BETWEEN_SUBDOMAINS,DIRICHLET_IMPLICIT) ) then
     nullify(lneig)
     allocate(lneig(nbnodes)) 
     call PAR_NODE_NUMBER_OF_PARTITIONS(nbnodes,lneig)
  end if

  !---------------------------------------------------------------------
  !
  ! Initial computations
  !
  !----------------------------------------------------------------------

  call solope(&
       1_ip, npoin, nrows, nbvar, idprecon, eps, an, pn, ja, ia, bb, xx , &
       ierr, stopcri, dummr, resid, invnb, kryl, dummr, dummr, wa1, &
       invdiag , wa3 )

  if( ierr /= 0 ) goto 20

  !----------------------------------------------------------------------
  ! 
  ! MAIN LOOP
  !
  !----------------------------------------------------------------------

  do while( .not. convergence )
     !
     ! Initial residual: kryl(1) = L^-1 ( b - A x )
     !
#ifdef EVENT_SOLVER
     call mpitrace_eventandcounters(300,1)
#endif
     !
     ! L kryl(1) = A R^-1 x
     !
     call precon(&
          1_ip,nbvar,npoin,nrows,solve_sol(1)%kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,wa1,xx,kryl(1,1))

     do kk= 1, nrows
        kryl(kk,1) = wa3(kk) - kryl(kk,1)
     end do
     ! 
     ! resid = ||kryl(1)||
     !
     call norm2x(nbvar,kryl,resid)

     resi2 = resi1
     resi1 = resid * invnb

     if( kfl_cvgso == 1 ) call outcso(nbvar,nrows,npoin,invdiag,an,ja,ia,bb,xx)

     if( resid <= stopcri ) then
        !
        ! The initial guess is the solution
        !
        convergence = .true.
     else
        !
        ! Initialize 1-st term of the rhs of hessenberg system
        !
        rs(1) = resid
        !
        ! Ortonormalize kryl(*,1)
        !
        resid = 1.0_rp / resid
        do kk= 1, nrows
           kryl(kk,1) = kryl(kk,1) * resid
        end do
     end if

     jj   = 0
     idx  = -1
     fin2 = convergence

#ifdef EVENT_SOLVER
     call mpitrace_eventandcounters(300,0)
#endif
     !
     ! Inner loop. Restarted each kryldim iterations
     !
     do while( .not. fin2 )

        iters = iters + 1
        jj    = jj + 1
        jj1   = jj + 1
        idx   = idx + jj
        !
        !  L kryl(jj1) = A R^-1 kryl(jj)
        !
#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,2)
#endif
       call precon(&
             1_ip,nbvar,npoin,nrows,solve_sol(1)%kfl_symme,idprecon,ia,ja,an,&
             pn,invdiag,wa1,kryl(1,jj),kryl(1,jj1))

#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,0)
#endif
        !
        ! Orthogonalization
        ! For i= 1, j
        !     H(i,j) = <v_i, v_j1>
        !       v_j1 = v_j1 - H(i,j) * v_i
        !
#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,3)
#endif

        if( kfl_timin == 1 ) call Parall(20_ip)
        call cputim(time1)

        if( kfl_ortho == 0 ) then
           !
           ! Gram-Schmidt
           !
           call gmrort(nbvar,npoin,jj,jj1,kryl,hh(idx+1))
           do ii = 1,jj
              resid = hh(idx+ii)
              do kk = 1,nrows
                 kryl(kk,jj1) = kryl(kk,jj1) - resid * kryl(kk,ii)
              end do
           end do
        else
           !
           ! Modified Gram-Schmidt
           !
           do ii = 1,jj              
              call prodxy(nbvar,npoin,kryl(1,ii),kryl(1,jj1),resid)              
              hh(idx+ii) = resid              
              do kk= 1,nrows
                 kryl(kk,jj1) = kryl(kk,jj1) - resid * kryl(kk,ii)
              end do
           end do
        end if

        if( kfl_timin == 1 ) call Parall(20_ip)
        call cputim(time2)
        solve_sol(1) % cputi(5) = solve_sol(1) % cputi(5) + time2 - time1

#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,0)
#endif
#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,4)
#endif
        !
        ! H(jj1,jj) = ||kryl(*,jj1)||
        !
        call norm2x(nbvar,kryl(1,jj1),resid)

        hh(idx+jj1) = resid

        if( resid == 0.0_rp ) then
           fin2        = .true.
           convergence = .true.
           idx         = idx - jj
           jj          = jj - 1
           iters       = iters - 1
           if( kfl_solve /= 0 ) write(lun_outso,*) '||kryl(*,jj1)|| = 0 ',jj1,iters
           goto 10
        else
           !
           ! Ortonormalize kryl(*,jj1)
           !
           resid = 1.0_rp / resid
           do kk = 1,nrows
              kryl(kk,jj1) = kryl(kk,jj1) * resid
           end do
        end if
#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,0)
#endif
        !
        ! Update factorization of H. Perform previous 
        ! transformations on jj-th column of H
        !
#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,5)
#endif
        do ii= 1, jj-1
           kk         =   ii + 1
           resid      =   hh(idx+ii)
           hh(idx+ii) =   cc(ii) * resid + ss(ii) * hh(idx+kk)
           hh(idx+kk) = - ss(ii) * resid + cc(ii) * hh(idx+kk)
        end do
        gamma = hh(idx+jj)*hh(idx+jj) + hh(idx+jj1)*hh(idx+jj1)
        gamma = sqrt(gamma)
        !
        ! if gamma is zero then take any small
        ! value will affect only residual estimate
        !
        if( gamma == 0.0_rp ) then
           gamma = epsmac
           if( kfl_solve /= 0 ) write(lun_outso,*) 'gamma==0.0 ',iters
        end if
#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,0)
#endif
#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,6)
#endif
        !
        ! Get next plane rotation
        !
        gamma      =   1.0_rp / gamma
        cc(jj)     =   hh(idx+jj)  * gamma
        ss(jj)     =   hh(idx+jj1) * gamma
        hh(idx+jj) =   cc(jj) * hh(idx+jj) + ss(jj) * hh(idx+jj1)
        !
        ! Update the rhs of the LS problem
        !
        rs(jj1)    = - ss(jj) * rs(jj)
        rs(jj)     =   cc(jj) * rs(jj)
        !
        ! Convergence Test
        !
        resid      =   abs( rs(jj1) )

        resi2 = resi1
        resi1 = resid * invnb
        if( kfl_cvgso == 1 ) call outcso(nbvar,nrows,npoin,invdiag,an,ja,ia,bb,xx)

        if( resid <= stopcri ) then
           convergence = .true.
           fin2        = .true.
        else
           if( iters >= maxiter ) then
              convergence = .true.
              fin2        = .true.
           else
              if( jj >= kryldim ) then
                 fin2 = .true.
              end if
           end if
        end if

#ifdef EVENT_SOLVER
        call mpitrace_eventandcounters(300,0)
#endif

10      continue
     end do

     !----------------------------------------------------------------------
     ! 
     ! END INNER LOOP 
     !
     !----------------------------------------------------------------------

#ifdef EVENT_SOLVER
     call mpitrace_eventandcounters(300,7)
#endif
     !
     ! Compute y => Solve upper triangular system
     !
     do ii= jj,2,-1
        rs(ii) = rs(ii) / hh(idx+ii)
        resid = rs(ii)

        do kk = 1,ii-1
           rs(kk) = rs(kk) - hh(idx+kk) * resid
        end do

        idx = idx - ii
     end do

     if( hh(1) /= 0.0_rp ) rs(1) = rs(1) / hh(1)
     !
     ! Linear combination of kryl(*,jj)'s to get the solution.
     !
     do ii = 1,jj
        resid = rs(ii)
        do kk = 1,nrows
           xx(kk) = xx(kk) + resid * kryl(kk,ii) 
        end do
     end do
     !
     ! Implicit Dirichlet/Dirichlet
     !
     if( mcoup > 0 ) then
        if( INOTMASTER .and. I_AM_INVOLVED_IN_A_COUPLING_TYPE(BETWEEN_SUBDOMAINS,DIRICHLET_IMPLICIT) ) then
           nullify(xx_tmp)
           allocate(xx_tmp(nrows))
           do ii = 1,nrows
              xx_tmp(ii) = xx(ii)
           end do
           !
           ! Interpolate new value. RHS should be modified as well
           ! and multiplied by its number of partitions (as this is the case
           ! for the matrix)
           !
           do icoup = 1,mcoup
              if(    I_AM_IN_COUPLING(icoup)                            .and. &
                   & coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS  .and. &
                   & coupling_type(icoup) % what == DIRICHLET_IMPLICIT  ) then
                 call COU_INTERPOLATE_NODAL_VALUES(icoup,1_ip,xx,xx_tmp)
                 do kk = 1,coupling_type(icoup) % geome % npoin_wet
                    ii     = coupling_type(icoup) % geome % lpoin_wet(kk)      
                    bb(ii) = xx(ii) * real(lneig(ii),rp)
                 end do
              end if
           end do
           !
           ! wa3 = L^-1 b should be recomputed for proper restart
           !
           call precon(&
                3_ip,nbvar,nbnodes,nrows,solve_sol(1) % kfl_symme,idprecon,ia,ja,an,&
                pn,invdiag,wa1,bb,wa3)
           deallocate(xx_tmp)
        end if
     end if

#ifdef EVENT_SOLVER
     call mpitrace_eventandcounters(300,0)
#endif
  end do

  !-----------------------------------------------------------------
  !
  !  END MAIN LOOP
  !
  !-----------------------------------------------------------------

20 continue
  call solope(&
       2_ip, npoin, nrows, nbvar, idprecon, dummr, an, dummr, ja, ia, &
       bb, xx , ierr, dummr, dummr, resi1, dummr, kryl, dummr, &
       dummr, dummr, invdiag, dummr )

  if( INOTMASTER .and. I_AM_INVOLVED_IN_A_COUPLING_TYPE(BETWEEN_SUBDOMAINS,DIRICHLET_IMPLICIT) ) then
     deallocate(lneig)
  end if

  call memchk(2_ip,istat,memit,'HH','gmrpls',hh)
  deallocate(hh,stat=istat)
  if(istat/=0) call memerr(2_ip,'HH','gmrpls',0_ip)
  call memchk(2_ip,istat,memit,'RS','gmrpls',rs)
  deallocate(rs,stat=istat)
  if(istat/=0) call memerr(2_ip,'RS','gmrpls',0_ip)
  call memchk(2_ip,istat,memit,'SS','gmrpls',ss)
  deallocate(ss,stat=istat)
  if(istat/=0) call memerr(2_ip,'SS','gmrpls',0_ip)
  call memchk(2_ip,istat,memit,'CC','gmrpls',cc)
  deallocate(cc,stat=istat)
  if(istat/=0) call memerr(2_ip,'CC','gmrpls',0_ip)
  call memchk(2_ip,istat,memit,'KRYL','gmrpls',kryl)
  deallocate(kryl,stat=istat)
  if(istat/=0) call memerr(2_ip,'KRYL','gmrpls',0_ip)
  call memchk(2_ip,istat,memit,'WA3','gmrpls',wa3)
  deallocate(wa3,stat=istat)
  if(istat/=0) call memerr(2_ip,'WA3','gmrpls',0_ip)
  call memchk(2_ip,istat,memit,'INVDIAG','gmrpls',invdiag)
  deallocate(invdiag,stat=istat)
  if(istat/=0) call memerr(2_ip,'INVDIAG','gmrpls',0_ip)
  call memchk(2_ip,istat,memit,'WA1','gmrpls',wa1)
  deallocate(wa1,stat=istat)
  if(istat/=0) call memerr(2_ip,'WA1','gmrpls',0_ip)

100 format(i7,3(1x,e12.6))
110 format(i5,18(2x,e12.6))

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine gmrpls
