subroutine coajac(&
     nbvar,npoin,nrows,ia,ja,an,invdiag,pp,qq)
  !------------------------------------------------------------------------
  !****f* solite/coajac
  ! NAME 
  !    coajac
  ! DESCRIPTION
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp
  use def_master, only      :  INOTMASTER,npoi1,kfl_paral
  use def_domain, only      :  r_dom,c_dom,r_sym,c_sym
  use def_solver, only      :  solve_sol,memit
  use def_solver, only      :  SOL_DIAGONAL,SOL_LINELET,SOL_GAUSS_SEIDEL
  use mod_memchk
  use mod_csrdir
  implicit none
  integer(ip), intent(in)   :: nbvar,npoin,nrows
  integer(ip), intent(in)   :: ia(*),ja(*)
  real(rp),    intent(in)   :: an(*),invdiag(*),pp(*)
  real(rp),    intent(out)  :: qq(*) 
  integer(ip)               :: ii,kk,ngrou,igrou,info,nskyl,itpre
  integer(4)                :: istat
  real(rp),    pointer      :: rr(:)
  real(rp),    pointer      :: rrs(:)
  real(rp),    pointer      :: rrrhs(:)
  real(rp),    pointer      :: ww(:)
  real(rp)                  :: dummr

  ngrou = solve_sol(1) % ngrou
  nskyl = solve_sol(1) % nskyl
  itpre = solve_sol(1) % itpre

  !----------------------------------------------------------------------
  !
  ! Alloocate memory
  ! NROWS = 1 for MASTER
  !
  !----------------------------------------------------------------------

  allocate( rr(nrows) , stat = istat ) 
  if( solve_sol(1) % kfl_defpr == SOL_LINELET ) then
     allocate( ww(nrows) , stat = istat ) 
  end if
  allocate( rrs(nbvar*ngrou) , stat = istat )    
  do igrou = 1,nbvar*ngrou
     rrs(igrou)   = 0.0_rp
  end do
  if( solve_sol(1) % kfl_defas /= 0 ) then 
     allocate( rrrhs(nbvar*ngrou) , stat = istat )  
     do igrou = 1,nbvar*ngrou
        rrrhs(igrou) = rrs(igrou)
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! Initial solution QQ and initial residual RR
  !
  !----------------------------------------------------------------------

  !if( solve_sol(1) % kfl_defpr == SOL_DIAGONAL .or. solve_sol(1) % kfl_defpr == SOL_GAUSS_SEIDEL ) then
  if( solve_sol(1) % kfl_defpr == SOL_DIAGONAL ) then
     !
     ! Jacobi with diagonal
     !
     do ii = 1,nrows
        qq(ii) = invdiag(ii) * pp(ii)
     end do

  else if( solve_sol(1) % kfl_defpr == SOL_GAUSS_SEIDEL ) then
     !
     ! SSOR
     !
     do ii = 1,nrows
        qq(ii) = 0.0_rp
     end do
     call bcsrgs(npoin,nbvar,an,ja,ia,invdiag,qq,pp) 

  else if( solve_sol(1) % kfl_defpr == SOL_LINELET ) then
     !
     ! Jacobi with linelet
     !
     do ii = 1,nrows
        qq(ii) = pp(ii)
     end do
     call sollin(nbvar,qq,ww,invdiag)  
  end if

  if( solve_sol(1) % kfl_symme == 1 ) then
     call bsymax( 1_ip, npoin, nbvar, an, ja, ia, qq, rr )
  else
     call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, qq, rr )
  end if
  do ii = 1,nrows
     rr(ii) = pp(ii) - rr(ii)
  end do

  !----------------------------------------------------------------------
  !
  ! Smoothing:
  !
  ! 1. Jacobi smoothing: q^{i+1} = q^i + M^{-1} ( p - A q^i )
  !    1.1 Diagonal preconditioner: M = D
  !    1.2 Linelet preconditioner:  M = Linelet
  ! 2. Sym. Gauss-Seidel: (L+D) D^-1 (U+D) q = r
  !
  !----------------------------------------------------------------------

  do kk = 1,itpre

     if( solve_sol(1) % kfl_defpr == SOL_DIAGONAL ) then
        !
        ! Jacobi with diagonal
        !
        do ii = 1,nrows
           qq(ii) = qq(ii) + invdiag(ii) * rr(ii)
        end do

     else if( solve_sol(1) % kfl_defpr == SOL_GAUSS_SEIDEL ) then
        !
        ! SSOR
        !
        call bcsrgs(npoin,nbvar,an,ja,ia,invdiag,qq,pp) 

     else if( solve_sol(1) % kfl_defpr == SOL_LINELET ) then
        !
        ! Jacobi with linelet
        !
        call sollin(nbvar,rr,ww,invdiag)  
        do ii = 1,nrows
           qq(ii) = qq(ii) + rr(ii) 
        end do

     end if

     if( solve_sol(1) % kfl_symme == 1 ) then
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, qq, rr )
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, qq, rr )
     end if

     do ii = 1,nrows
        rr(ii) = pp(ii) - rr(ii)
     end do

  end do

  !----------------------------------------------------------------------
  !
  ! Coarse system:
  ! 1. r     =  p - A q
  ! 2  r     => r'
  ! 3. A' e' =  r'
  ! 4. e'    => e
  ! 5. q     =  q + e
  !
  !----------------------------------------------------------------------

  call wtvect(npoin,ngrou,nbvar,rrs,rr) 

  if( solve_sol(1) % kfl_defso == 1 ) then
     !
     ! Iterative solver
     !
     do igrou = 1,ngrou*nbvar
        rrrhs(igrou) = rrs(igrou)
        rrs(igrou)   = 0.0_rp
     end do
     call conjgr( &
          ngrou, nbvar, 1000_ip, 1.0e-12_rp,    &
          solve_sol(1) % askylpredef,           &
          solve_sol(1) % ja, solve_sol(1) % ia, &
          rrrhs, rrs )

  else if( INOTMASTER ) then  
     !
     ! Direct solver
     !
     if( solve_sol(1) % kfl_defas == 0 ) then
        call chosol(&
             solve_sol(1) % ngrou*nbvar,solve_sol(1) % nskyl,&
             solve_sol(1) % iskyl,1_ip,solve_sol(1) % askylpredef,rrs,&
             solve_sol(1) % ngrou*nbvar,info)    
        if( info /= 0 ) &
             call runend('MATGRO: COULD NOT SOLVE INITIAL SYSTEM')
     else
        do igrou = 1,ngrou*nbvar
           rrrhs(igrou) = rrs(igrou)
        end do
        call CSR_LUsol(&
             ngrou,nbvar,solve_sol(1) % invpRpredef,solve_sol(1) % invpCpredef,&
             solve_sol(1) % ILpredef,solve_sol(1) % JLpredef,&
             solve_sol(1) % LNpredef,solve_sol(1) % IUpredef,&
             solve_sol(1) % JUpredef,solve_sol(1) % UNpredef,rrrhs,rrs) 
     end if
  end if

  call wvect(npoin,nbvar,rrs,rr)   

  do ii = 1,nrows
     qq(ii) = qq(ii) + rr(ii)   
  end do

  !----------------------------------------------------------------------
  !
  ! Smoothing
  !
  !----------------------------------------------------------------------

  do kk = 1,itpre

     if( solve_sol(1) % kfl_symme == 1 ) then
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, qq, rr )
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, qq, rr )
     end if
     do ii = 1,nrows
        rr(ii) = pp(ii) - rr(ii)
     end do

     if( solve_sol(1) % kfl_defpr == SOL_DIAGONAL ) then
        !
        ! Jacobi with diagonal
        !
        do ii = 1,nrows
           qq(ii) = qq(ii) + invdiag(ii) * rr(ii)
        end do

     else if( solve_sol(1) % kfl_defpr == SOL_GAUSS_SEIDEL ) then        
        !
        ! SSOR
        !
        call bcsrgs(npoin,nbvar,an,ja,ia,invdiag,qq,pp) 

     else if( solve_sol(1) % kfl_defpr == SOL_LINELET ) then
        !
        ! Jacobi with linelet
        !
        call sollin(nbvar,rr,ww,invdiag)  
        do ii = 1,nrows
           qq(ii) = qq(ii) + rr(ii) 
        end do

     end if
  end do

  !----------------------------------------------------------------------
  !
  ! Deallocate
  !
  !----------------------------------------------------------------------

  deallocate( rr , stat = istat )
  if( solve_sol(1) % kfl_defpr == SOL_LINELET ) then
     deallocate( ww , stat = istat )
  end if
  deallocate( rrs , stat = istat )
  if( solve_sol(1) % kfl_defas /= 0 ) then 
     deallocate( rrrhs , stat = istat )
  end if

end subroutine coajac
