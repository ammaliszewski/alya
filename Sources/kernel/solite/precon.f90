!-----------------------------------------------------------------------
!> @addtogroup Solver
!> @{                                                                   
!> @file    precon.f90
!> @author  Guillaume Houzeaux
!> @date    15/07/2015
!> @brief   Apply a preconditioning to basic operations
!> @details Perform the following operations:
!>
!>          ITASK = 1 ... Solve system             L q = A R^-1 p 
!>          ITASK = 2 ... Solve system             L q = q 
!>          ITASK = 3 ... Solve system             L q = p 
!>          ITASK = 4 ... Solve system               q = A R^-1 p
!>          ITASK = 5 ... Recover final solution   R x = x'
!>          ITASK = 6 ... Compute initial solution   x = R x'
!>
!>          L = left preconditioner
!>          R = right preconditioner
!>
!>          INVDIAG ... D^-1
!>          WW ........ Working vector
!>          Q ......... Output vector
!>          P ......... Input vector
!>          AN ........ Matrix
!>          PN ........ Preconditioner Matrix
!>
!>          If there is a coarse grid preconditioner C
!>          Let P = L or P = R
!>          The preconditioner with coarse grid correction is:
!>          M^-1 = P^-1 + C^-1
!>
!>          At some point, we have to solve:
!>          M q = p so that
!>          q    = (P^-1 + C^-1) p = P^-1 p + C^-1 p
!>
!>          1. Compute coarse grid correction: p' = C^-1 p 
!>             then q = P^-1 p + p'
!>          2. Solve: P q1 = p => q1 = P^-1 p
!>          3. Update:  q  = q1 + p'
!>
!> @}                                                                   
!-----------------------------------------------------------------------
subroutine precon(&
     itask,nbvar,npoin,nrows,kfl_symme,idprecon,ia,ja,an,&
     pn,invdiag,ww,pp,qq)
  use def_kintyp, only      :  ip,rp
  use def_master, only      :  INOTMASTER,npoi1,npoi2,npoi3,kfl_timin
  use def_master, only       : NPOIN_TYPE,lninv_loc
  use def_domain, only      :  r_dom_aii,c_dom_aii
  use def_solver, only      :  block_diagonal
  use def_solver, only      :  SOL_NO_PRECOND,SOL_SQUARE,SOL_DIAGONAL,&
       &                       SOL_MATRIX,SOL_GAUSS_SEIDEL,SOL_LINELET,&
       &                       SOL_ORTHOMIN,solve_sol,SOL_AII,SOL_RAS,&
       &                       SOL_MULTIGRID,SOL_NEUMANN,&
       &                       SOL_LEFT_PRECONDITIONING,&
       &                       SOL_RIGHT_PRECONDITIONING,&
       &                       SOL_BLOCK_DIAGONAL
  use mod_csrdir, only      :  CSR_LUsol,CSR_SMVP
  use mod_preconditioner, only  :  preconditioner_gauss_seidel
  use mod_matrix
  implicit none
  integer(ip), intent(in)   :: itask             !< What to do
  integer(ip), intent(in)   :: nbvar             !< Number of dof per node                            
  integer(ip), intent(in)   :: npoin             !< Numnber of nodes                                  
  integer(ip), intent(in)   :: nrows             !< Totoal number of dof=nbvar*npoin                  
  integer(ip), intent(in)   :: kfl_symme         !< If matrix is assembled with symmetric structure   
  integer(ip), intent(in)   :: idprecon          !< Preconditioner ID                                 
  integer(ip), intent(in)   :: ia(*)             !< Matrix graph                                      
  integer(ip), intent(in)   :: ja(*)             !< Matrix graph                                      
  real(rp),    intent(in)   :: an(nbvar,nbvar,*) !< Matrix of system                                  
  real(rp),    intent(in)   :: pn(*)             !< Preconditioner matrix                             
  real(rp),    intent(in)   :: invdiag(*)        !< Inverse diagonal                                  
  real(rp),    intent(in)   :: pp(*)             !< RHS vector                                        
  real(rp),    intent(out)  :: ww(*)             !< Working array                                     
  real(rp),    intent(out)  :: qq(*)             !< Preconditioned vector                             
  integer(ip)               :: ii,jj,kk
  integer(ip)               :: iijj,iikk
  integer(ip)               :: iin,jjn
  integer(ip)               :: ki,kj
  real(rp)                  :: time1,time2
  real(rp),    pointer      :: w2(:),Ap(:)

  if( kfl_timin == 1 ) call Parall(20_ip)
  call cputim(time1)

  select case ( itask )

  case ( 1_ip ) 

     !------------------------------------------------------------------- 
     !
     ! Solve system L q = A R^-1 p
     !
     !------------------------------------------------------------------- 
     !
     ! Left: w = A p
     ! Right w = p
     !
     allocate(Ap(max(1_ip,nrows)))
     if( solve_sol(1) % kfl_leftr == SOL_LEFT_PRECONDITIONING ) then
        if( kfl_symme == 1 ) then 
           call bsymax( 1_ip, npoin, nbvar, an, ja, ia, pp, Ap )
        else
           call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, pp, Ap )  
        end if
     else if( solve_sol(1) % kfl_leftr == SOL_RIGHT_PRECONDITIONING ) then
        do ii = 1,nrows
           Ap(ii) = pp(ii)
        end do
     end if
     !
     ! Preconditioning
     !
     call all_precon(&
          nbvar,npoin,nrows,kfl_symme,idprecon,ia,ja,an,&
          pn,invdiag,ww,Ap,qq)
     !
     ! At this point:
     ! Left:  L^{-1} q = A p
     ! Right:        q = R^{-1} p => then to q = A (R^{-1} p)
     !
     ! Coarse grid correction
     !
     if( solve_sol(1) % kfl_coarse == 1 ) then
        call coafin(nbvar,1.0_rp,Ap,qq)
     end if
     !
     ! Left/Right preconditioning
     !
     deallocate(Ap)
     if( solve_sol(1) % kfl_leftr == SOL_RIGHT_PRECONDITIONING ) then
        do ii = 1,nrows 
           ww(ii) = qq(ii)
        end do
        if( kfl_symme == 1 ) then 
           call bsymax( 1_ip, npoin, nbvar, an, ja, ia, ww, qq )
        else
           call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, ww, qq )  
        end if
     end if

  case ( 2_ip )

     !------------------------------------------------------------------- 
     !
     ! Solve L q = q
     !
     !------------------------------------------------------------------- 

     if( solve_sol(1) % kfl_leftr == SOL_LEFT_PRECONDITIONING ) then
        !
        ! Left preconditioning
        !
        allocate(w2(max(1_ip,nrows)))
        do ii = 1,nrows
           w2(ii) = qq(ii)
        end do
        call all_precon(&
             nbvar,npoin,nrows,kfl_symme,idprecon,ia,ja,an,&
             pn,invdiag,ww,w2,qq)
        !
        ! Coarse grid correction
        !
        if( solve_sol(1) % kfl_coarse == 1 ) then
           call coafin(nbvar,1.0_rp,w2,qq)
        end if
        deallocate(w2)
     end if

  case ( 3_ip )

     !------------------------------------------------------------------- 
     !
     ! Solve L q = p (same as 2 but input is p)
     !
     !------------------------------------------------------------------- 

     if(    idprecon == SOL_NO_PRECOND .or. &
          & solve_sol(1) % kfl_leftr == SOL_RIGHT_PRECONDITIONING ) then
        !
        ! No preconditioner or Right preconditioning: do not do anything
        !
        do ii = 1,nrows
           qq(ii) = pp(ii) 
        end do

     else if( solve_sol(1) % kfl_leftr == SOL_LEFT_PRECONDITIONING ) then
        !
        ! Left preconditioning
        !
        call all_precon(&
             nbvar,npoin,nrows,kfl_symme,idprecon,ia,ja,an,&
             pn,invdiag,ww,pp,qq)
        !
        ! Coarse grid correction
        ! 
        if( solve_sol(1) % kfl_coarse == 1 ) then
           call coafin(nbvar,1.0_rp,pp,qq)
        end if

     end if

  case( 4_ip ) 

     !------------------------------------------------------------------- 
     !
     ! Compute q = A R^-1 p
     !
     !------------------------------------------------------------------- 

     if( solve_sol(1) % kfl_leftr == SOL_LEFT_PRECONDITIONING ) then
        !
        ! Left preconditioning: q = A p
        !
        if( solve_sol(1) % kfl_symme == 1 ) then          
           call bsymax( 1_ip, npoin, nbvar, an, ja, ia, pp, qq )       
        else        
           call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, pp, qq )       
        end if

     else
        !
        ! Right preconditioning
        ! R w2 = p, q = A w
        !
        call all_precon(&
             nbvar,npoin,nrows,kfl_symme,idprecon,ia,ja,an,&
             pn,invdiag,ww,pp,qq)
        !
        ! Coarse grid correction
        !
        if( solve_sol(1) % kfl_coarse /= 0 ) then
           call coafin(nbvar,1.0_rp,pp,qq)
        end if
        !
        ! q = A w
        !
        if( INOTMASTER ) then
           allocate(w2(nrows))
           do ii = 1,nrows
              w2(ii) = qq(ii)
           end do
           if( kfl_symme == 1 ) then
              call bsymax( 1_ip, npoin, nbvar, an, ja, ia, w2, qq ) 
           else
              call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, w2, qq )
           end if
           deallocate(w2)
        end if

     end if

  case ( 5_ip )

     !------------------------------------------------------------------- 
     !
     ! Recover final solution: q <= R^-1 q 
     !
     !------------------------------------------------------------------- 
     !
     ! Coarse grid correction: save q
     !
     if ( solve_sol(1) % kfl_leftr == SOL_RIGHT_PRECONDITIONING ) then
        allocate(w2(max(1_ip,nrows)))
        do ii = 1,nrows
           w2(ii) = qq(ii)
        end do
        call all_precon(&
             nbvar,npoin,nrows,kfl_symme,idprecon,ia,ja,an,&
             pn,invdiag,ww,w2,qq)
        !
        ! Coarse grid correction
        !
        if( solve_sol(1) % kfl_coarse /= 0 ) then
           call coafin(nbvar,1.0_rp,w2,qq)
        end if
        deallocate(w2)
     end if

  case ( 6_ip )

     !------------------------------------------------------------------- 
     !
     ! Compute initial solution: q <= R q
     !
     !------------------------------------------------------------------- 

     if ( idprecon == SOL_SQUARE ) then 

        do ii = 1,nrows
           qq(ii) = qq(ii) / invdiag(ii)
        end do

     else if( solve_sol(1) % kfl_leftr == 1 ) then

        if ( idprecon == SOL_DIAGONAL ) then
           !
           ! Diagonal
           !
           do ii = 1,nrows
              qq(ii) = qq(ii) / invdiag(ii) 
           end do

        else if ( idprecon == SOL_ORTHOMIN ) then
           !
           ! Orthomin(1)
           !
           do ii = 1,nrows
              qq(ii) = qq(ii) / invdiag(ii) 
           end do

        else if ( idprecon == SOL_GAUSS_SEIDEL .and. INOTMASTER ) then
           !
           ! Gauss-Seidel
           !
           do ii = 1,nrows
              ww(ii) = qq(ii)
           end do
           call preconditioner_gauss_seidel(&
                2_ip,npoin,nbvar,an,ja,ia,invdiag,qq,ww) 

        else if( idprecon == SOL_BLOCK_DIAGONAL .and. INOTMASTER ) then
           !
           ! Block diagonal
           !
           allocate( w2(nrows) )
           do ii = 1,nrows
              w2(ii) = qq(ii)
           end do
           do ii = 1,npoin        
              iijj = (ii-1) * nbvar
              do jj = 1,nbvar
                 iijj = iijj + 1
                 qq(iijj) = 0.0_rp
                 do kk = 1,nbvar
                    iikk = (ii-1) * nbvar + kk
                    qq(iijj) = qq(iijj) + block_diagonal(jj,kk,ii) * w2(iikk)
                 end do
              end do
           end do
           deallocate( w2 )

        else if( idprecon == SOL_AII .and. INOTMASTER ) then
           !
           ! Block LU
           !  
           ! +-  -+   +-        -+ +-  -+    
           ! | qi |   | Aii  Aib | | qi |    
           ! |    | = |          | |    | 
           ! | qb |   |  0    D  | | qb |    
           ! +-  -+   +-        -+ +-  -+    
           !
           allocate( w2(nrows) )
           call CSR_SMVP(npoi1,nbvar,r_dom_aii,c_dom_aii,solve_sol(1) % invpR,solve_sol(1) % Aiipre,qq,w2)
           do ii = 1,npoi1
              do kk = ia(ii),ia(ii+1)-1
                 jj = ja(kk)
                 if( jj > npoi1 ) then
                    iin = (ii-1) * nbvar
                    do ki = 1,nbvar
                       iin = iin + 1
                       jjn = (jj-1) * nbvar
                       do kj = 1,nbvar
                          jjn = jjn + 1
                          w2(iin) = w2(iin) + an(kj,ki,kk) * qq(jjn) 
                       end do
                    end do
                 end if
              end do
           end do
           do ii = 1,npoi1*nbvar
              qq(ii) = w2(ii)
           end do
           deallocate( w2 )

           do ii = npoi1*nbvar+1,npoin*nbvar
              qq(ii) = qq(ii) / invdiag(ii) 
           end do

        else if( idprecon == SOL_RAS ) then
           !
           ! RAS
           !
           if( INOTMASTER ) then 
              allocate( w2(nrows) )  
              call CSR_SMVP(npoin,nbvar,solve_sol(1) % ia_ras,solve_sol(1) % ja_ras,solve_sol(1) % invpRras,solve_sol(1) % Aras,qq,w2)
              !call bcsrax( 2_ip, npoin, nbvar, solve_sol(1) % Aras, solve_sol(1) % ja_ras, solve_sol(1) % ia, qq, w2 ) 
              do ii = 1,nrows
                 ww(ii) = qq(ii) 
                 qq(ii) = w2(ii) 
              end do
              do ii = npoi1*nbvar+1,(npoi2-1)*nbvar
                 qq(ii) = 0.0_rp
              end do
              do ii = npoi3*nbvar+1,npoin*nbvar 
                 qq(ii) = 0.0_rp
              end do
              call pararr('SLX',NPOIN_TYPE,nrows,qq)
              deallocate( w2 )
           end if
           !if( solve_sol(1) % kfl_coarse == 1 ) then
           !   call fincoa(nbvar,1.0_rp,ww,qq)
           !end if

        end if

     end if

  end select

  if( kfl_timin == 1 ) call Parall(20_ip)
  call cputim(time2)
  solve_sol(1) % cputi(3) = solve_sol(1) % cputi(3) + time2 - time1

end subroutine precon
