!-----------------------------------------------------------------------
!> @addtogroup Solver
!> @{                                                                   
!> @file    all_precon.f90
!> @author  Guillaume Houzeaux
!> @date    15/07/2015
!> @brief   Solve a preconditioned system                    
!> @details Solve M q = p where M is the preconditioner                          
!> @}                                                                   
!-----------------------------------------------------------------------

subroutine all_precon(&
     nbvar,npoin,nrows,kfl_symme,idprecon,ia,ja,an,&
     pn,invdiag,ww,pp,qq)
  use def_kintyp,          only :  ip,rp
  use def_master,          only :  INOTMASTER,npoi1,npoi2,npoi3
  use def_master,          only :  NPOIN_TYPE,kfl_paral
  use def_solver,          only :  block_invdiagonal
  use def_solver,          only :  SOL_NO_PRECOND,SOL_SQUARE,&
       &                           SOL_DIAGONAL,SOL_MATRIX,&
       &                           SOL_GAUSS_SEIDEL,SOL_LINELET,&
       &                           SOL_ORTHOMIN,solve_sol,&
       &                           SOL_AII,SOL_RAS,&
       &                           SOL_MULTIGRID,SOL_NEUMANN,&
       &                           SOL_LEFT_PRECONDITIONING,&
       &                           SOL_RIGHT_PRECONDITIONING,&
       &                           SOL_BLOCK_DIAGONAL
  use mod_csrdir,         only  :  CSR_LUsol
  use mod_preconditioner, only  :  preconditioner_gauss_seidel
  implicit none
  integer(ip), intent(in)   :: nbvar               !< Number of dof per node
  integer(ip), intent(in)   :: npoin               !< Numnber of nodes
  integer(ip), intent(in)   :: nrows               !< Totoal number of dof=nbvar*npoin
  integer(ip), intent(in)   :: kfl_symme           !< If matrix is assembled with symmetric structure
  integer(ip), intent(in)   :: idprecon            !< Preconditioner ID
  integer(ip), intent(in)   :: ia(*)               !< Matrix graph
  integer(ip), intent(in)   :: ja(*)               !< Matrix graph
  real(rp),    intent(in)   :: an(nbvar,nbvar,*)   !< Matrix of system
  real(rp),    intent(in)   :: pn(*)               !< Preconditioner matrix
  real(rp),    intent(in)   :: invdiag(*)          !< Inverse diagonal
  real(rp),    intent(in)   :: pp(*)               !< RHS vector
  real(rp),    intent(out)  :: ww(*)               !< Working array
  real(rp),    intent(out)  :: qq(*)               !< Preconditioned vector                              
  integer(ip)               :: ii,jj,kk
  integer(ip)               :: iijj,iikk,ki,kj
  integer(ip)               :: iin,jjn
  real(rp)                  :: numer,denom
  real(rp)                  :: alpha
  real(rp),    pointer      :: r0(:)
  real(rp),    pointer      :: d1r0(:)
  real(rp),    pointer      :: Ad1r0(:)

  if( idprecon == SOL_NO_PRECOND ) then

     !------------------------------------------------------------------
     !
     ! No preconditioning
     !
     !------------------------------------------------------------------

     do ii = 1,nrows 
        qq(ii) = pp(ii)
     end do

  else if( idprecon == SOL_DIAGONAL ) then

     !------------------------------------------------------------------
     !
     ! Diagonal preconditioning
     !
     !------------------------------------------------------------------

     do ii = 1,nrows  
        qq(ii) = invdiag(ii) * pp(ii)
     end do

  else if( idprecon == SOL_BLOCK_DIAGONAL ) then

     !------------------------------------------------------------------
     !
     ! Block diagonal
     !
     !------------------------------------------------------------------

     do ii = 1,npoin                
        iijj = (ii-1) * nbvar
        do jj = 1,nbvar
           iijj = iijj + 1
           qq(iijj) = 0.0_rp
           do kk = 1,nbvar
              iikk = (ii-1) * nbvar + kk
              qq(iijj) = qq(iijj) + block_invdiagonal(jj,kk,ii) * pp(iikk)
           end do
        end do
     end do 

  else if( idprecon == SOL_MATRIX ) then

     !------------------------------------------------------------------
     !
     ! Matrix PN preconditioning
     !
     !------------------------------------------------------------------

     if( kfl_symme == 1 ) then
        call bsymax( 1_ip, npoin, nbvar, pn, ja, ia, pp,  qq )  
     else
        call bcsrax( 1_ip, npoin, nbvar, pn, ja, ia, pp,  qq ) 
     end if

  else if( idprecon == SOL_LINELET ) then

     !------------------------------------------------------------------
     !
     ! Linelet
     !
     !------------------------------------------------------------------

     do ii = 1,nrows 
        qq(ii) = pp(ii)
     end do
     call sollin(nbvar,qq,ww,invdiag)    

  else if( idprecon == SOL_GAUSS_SEIDEL .and. INOTMASTER ) then

     !------------------------------------------------------------------
     !
     ! Gauss-Seidel
     !
     !------------------------------------------------------------------

     if( kfl_symme == 1 ) then
        call runend('PRECON: NOT CODED')
     else
        call preconditioner_gauss_seidel(&
             1_ip,npoin,nbvar,an,ja,ia,invdiag,qq,pp) 
     end if

  else if( idprecon == SOL_ORTHOMIN ) then

     !------------------------------------------------------------------
     !
     ! Orthomin(1)
     !
     !------------------------------------------------------------------
     !
     ! q1    = q0 + alpha * D^-1 ( p - A q0 )
     ! q0    = D-1 p
     ! r0    = p - A q0
     !
     !            ( r0 , A D^-1 r0 ) 
     ! alpha = -------------------------
     !         ( A D^-1 r0 , A D^-1 r0 )
     !
     allocate( r0(   max(1_ip,nrows)) )        
     allocate( d1r0( max(1_ip,nrows)) )        
     allocate( Ad1r0(max(1_ip,nrows)) )        

     do ii = 1,nrows
        ww(ii) = invdiag(ii) * pp(ii)                              ! q0 = D-1 p 
     end do

     if( kfl_symme == 1 ) then                                     ! A q0
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, ww, r0 )  
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, ww, r0 )  
     end if
     do ii = 1,nrows
        r0(ii)   = pp(ii) - r0(ii)                                 ! r0 = p - A q0
        d1r0(ii) = invdiag(ii) * r0(ii)                            ! D^-1 r0 
     end do

     if( kfl_symme == 1 ) then                                     ! A D^-1 r0 
        call bsymax( 1_ip, npoin, nbvar, an, ja, ia, d1r0, Ad1r0 )  
     else
        call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, d1r0, Ad1r0 )  
     end if

     call prodxy(nbvar,npoin,r0,   Ad1r0,numer)
     call prodxy(nbvar,npoin,Ad1r0,Ad1r0,denom)
     if( denom == 0.0_rp ) then
        alpha = 1.0_rp
     else
        alpha = numer / denom
     end if

     do ii = 1,nrows
        qq(ii) = ww(ii) + alpha * d1r0(ii)
     end do

     deallocate(r0,d1r0,Ad1r0)

  else if( idprecon == SOL_AII .and. INOTMASTER ) then

     !------------------------------------------------------------------
     !
     ! Block LU 
     !
     !------------------------------------------------------------------
     !
     ! Solve:
     !
     ! +-        -+ +-  -+    +-  -+
     ! | Aii  Aib | | qi |    | pi |
     ! |          | |    | =  |    | => Aii qi = pi - Aib D^{-1} pb
     ! |  0    D  | | qb |    | pb |
     ! +-        -+ +-  -+    +-  -+
     !
     ! NB: for symmetric problems, one should put block Aib to zero
     !
     ! qb = D^{-1} pb
     !
     do ii = npoi1*nbvar+1,npoin*nbvar
        qq(ii) = invdiag(ii) * pp(ii)
     end do
     !
     ! ww <= qb = Aib qb
     ! NB: for symmetric problems, comment this loop
     !
     do ii= 1,npoi1*nbvar
        ww(ii) = 0.0_rp
     end do
     do ii = 1,npoi1
        do kk = ia(ii),ia(ii+1)-1
           jj = ja(kk)
           if( jj > npoi1 ) then
              iin = (ii-1) * nbvar
              do ki = 1,nbvar
                 iin = iin + 1
                 jjn = (jj-1) * nbvar
                 do kj = 1,nbvar
                    jjn = jjn + 1
                    ww(iin) = ww(iin) + an(kj,ki,kk) * qq(jjn) 
                 end do
              end do
           end if
        end do
     end do 
     call pararr('SLX',NPOIN_TYPE,nrows,ww)
     !
     ! w = pi - Aib ( D^{-1} pb )
     !
     do ii= 1,npoi1*nbvar
        ww(ii) = pp(ii) - ww(ii) 
     end do
     !
     ! Solve Aii qi = wi
     !
     call CSR_LUsol(&
          npoi1,nbvar,&
          solve_sol(1) % invpR , solve_sol(1) % invpC , &
          solve_sol(1) % ILpre , solve_sol(1) % JLpre , &
          solve_sol(1) % LNpre , solve_sol(1) % IUpre , &
          solve_sol(1) % JUpre , solve_sol(1) % UNpre , &
          ww,qq)

  else if( idprecon == SOL_RAS .and. INOTMASTER ) then

     !------------------------------------------------------------------
     !
     ! RAS
     !
     !------------------------------------------------------------------

     call CSR_LUsol(&
          npoin                   , nbvar                   , &
          solve_sol(1) % invpRras , solve_sol(1) % invpCras , &
          solve_sol(1) % ILras    , solve_sol(1) % JLras    , &
          solve_sol(1) % LNras    , solve_sol(1) % IUras    , &
          solve_sol(1) % JUras    , solve_sol(1) % UNras    , &
          pp,qq)
     do ii = npoi1*nbvar+1,(npoi2-1)*nbvar
        qq(ii) = 0.0_rp
     end do
     do ii = npoi3*nbvar+1,npoin*nbvar 
        qq(ii) = 0.0_rp
     end do
     call pararr('SLX',NPOIN_TYPE,nrows,qq)

  else if( idprecon == SOL_MULTIGRID ) then

     !------------------------------------------------------------------
     !
     ! Multigrid
     !
     !------------------------------------------------------------------

     call coajac(&
          nbvar,npoin,nrows,ia,ja,an,invdiag,pp,qq) 

  else if( idprecon == SOL_NEUMANN ) then

     !------------------------------------------------------------------
     !
     ! Neumann preconditioner
     !
     !------------------------------------------------------------------
     !call matrix_neumann_preconditioner(&
     !     itask,nbvar,npoin,kfl_symme,ia,ja,an,invdiag,pp,s,w,memit,qq)

  end if

end subroutine all_precon
