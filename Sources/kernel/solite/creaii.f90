!-----------------------------------------------------------------------
!> @file    nsi_inivar.f90
!> @author  Guillaume Houzeaux
!> @date    
!> @brief   Initialize some variables
!> @details Initialize some variables
!-----------------------------------------------------------------------
subroutine creaii(itask)

  use def_master
  use def_domain
  use def_solver
  use mod_memchk
  use mod_graphs
  integer(ip), intent(in) :: itask
  integer(ip)             :: nzgroup

  if( INOTMASTER ) then
     call graphs_groups(& 
          npoi1,solve_sol(1) % ngaii,npoin,r_dom,c_dom,&
          solve_sol(1) % lgaii,solve_sol(1) % kfl_fixno) 
     call graphs_gragro(&
          npoin,solve_sol(1) % ngaii,solve_sol(1) % lgaii,&
          r_dom,c_dom,nzgroup,&
          solve_sol(1) % iaaii,solve_sol(1) % jaaii) 
  end if 

end subroutine creaii
