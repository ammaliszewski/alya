!------------------------------------------------------------------------
!> @addtogroup Solver
!> @{
!> @name    preconditioner
!> @file    mod_parall.f90
!> @author  Guillaume Houzeaux
!> @date    16/09/2015
!> @brief   ToolBox for preconditioners
!> @details ToolBox for preconditioners
!>
!> @{
!------------------------------------------------------------------------

module mod_preconditioner
  use def_kintyp,    only : ip,rp,lg
  use def_master,    only : INOTMASTER,npoi1,npoi2,npoi3
  use def_solver,    only : solve_sol 
  use def_solver,    only : SYMMETRIC_GAUSS_SEIDEL 
  use def_solver,    only : STREAMWISE_GAUSS_SEIDEL
  use def_solver,    only : GAUSS_SEIDEL           
  use def_solver,    only : memit
  use mod_memory,    only : memory_alloca
  use mod_memory,    only : memory_deallo
  use mod_solver,    only : direct_solver_typ

  implicit none   
  private
  real(rp), parameter :: zeror = epsilon(1.0_rp)

  public :: preconditioner_gauss_seidel
  public :: preconditioner_RAS_initialize

contains

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Gauss-Seidel preconditioner
  !> @details According to ITASK:
  !>          ITASK = 1 ... Solve   M x = b
  !>                = 2 ... Compute   b = M x
  !>
  !>          \verbatim
  !>
  !>          Gauss-Seidel: (L+D) x = b => 
  !>          -------------
  !>
  !>                                    i-1
  !>                                    ---
  !>          x_i^{k+1} = 1/a_ii ( bi - \   a_ij * x_j^{k+1} )
  !>                                    /__
  !>                                    j=1
  !>
  !>
  !>          Streamwise Gauss-Seidel: 
  !>          ------------------------
  !>
  !>          Same as Gauss-Seidel but with renumbering
  !>          
  !>
  !>          Symmetric Gauss-Seidel: (L+D) D^-1 (U+D) x = b =>
  !>          -----------------------
  !>
  !>          1. (L+D) y = b
  !>          2. z = D y
  !>          3. (U+D) x = z
  !>      
  !>                                    i-1
  !>                                    ---
  !>          y_i^{k+1} = 1/a_ii ( bi - \   a_ij * y_j^{k+1} )
  !>                                    /__
  !>                                    j=1
  !>
  !>          z_i^{k+1} = D y_i^{k+1}
  !>                                            n
  !>                                           ---
  !>          x_i^{k+1} = 1/a_ii ( z_i^{k+1} - \   a_ij * x_j^{k+1} )
  !>                                           /__
  !>                                          j=i+1
  !>
  !>          \endverbatim
  !>
  !>          In parallel, we consider the following block preconditioning, where
  !>          i refers to internal nodes and b to interface boundary nodes.
  !>
  !>          \verbatim
  !>          
  !>              +-        -+ +-  -+   +-  -+
  !>              | Mii  Mib | | xi |   | bi |
  !>              |          | |    | = |    | where Mbb = Diag(A)
  !>              |  0   Mbb | | xb |   | bb |
  !>              +-        -+ +-  -+   +-  -+
  !>
  !>          \endverbatim
  !> 
  !>           ITASK = 1 .... Solve M x = b:
  !>                          1. Mbb xb = bb 
  !>                          2. Mii xi = bi - Mib xb
  !>                            
  !>           ITASK = 2 .... Compute x = M b
  !>                          1. xb = Mbb b
  !>                          2. xi = Mii xi +  Mib xb
  !>
  !----------------------------------------------------------------------
  subroutine preconditioner_gauss_seidel(&
       itask,nbnodes,nbvar,aa,ja,ia,invdiag,xx,bb) 
    implicit none
    integer(ip), intent(in)          :: itask                !< What to do
    integer(ip), intent(in)          :: nbnodes              !< Number of nodes
    integer(ip), intent(in)          :: nbvar                !< Number of dof per node
    real(rp),    intent(in)          :: aa(nbvar,nbvar,*)    !< Matrix
    integer(ip), intent(in)          :: ja(*)                !< Matrix graph
    integer(ip), intent(in)          :: ia(*)                !< Matrix graph
    real(rp),    intent(in)          :: invdiag(nbvar,*)     !< Inverse diagonal
    real(rp),    intent(in)          :: bb(nbvar,*)          !< RHS
    real(rp),    intent(inout)       :: xx(nbvar,*)          !< Solution
    integer(ip)                      :: ii,jj,kk,ll
    integer(ip)                      :: ii_new,col
    real(rp),    pointer             :: yy(:,:)

    if( INOTMASTER ) then

       !-----------------------------------------------------------------
       !
       ! Subdomain interface treatment and memory allocation: 
       !
       ! Solve xb = Mbb bb
       !
       !-----------------------------------------------------------------

       nullify(yy)        

       if( solve_sol(1) % kfl_renumbered_gs == STREAMWISE_GAUSS_SEIDEL ) then
          if( itask == 1 ) then
             do ii = npoi1+1,nbnodes
                xx(1:nbvar,ii) = bb(1:nbvar,ii) * invdiag(1:nbvar,ii)
             end do
          else          
             do ii = npoi1+1,nbnodes
                xx(1:nbvar,ii) = bb(1:nbvar,ii) / invdiag(1:nbvar,ii)
             end do
          end if
       else
          allocate(yy(nbvar,nbnodes))
          if( itask == 1 ) then
             do ii = npoi1+1,nbnodes
                xx(1:nbvar,ii) = bb(1:nbvar,ii) * invdiag(1:nbvar,ii)
                yy(1:nbvar,ii) = xx(1:nbvar,ii)
             end do
          else          
             do ii = npoi1+1,nbnodes
                xx(1:nbvar,ii) = bb(1:nbvar,ii) / invdiag(1:nbvar,ii)
                yy(1:nbvar,ii) = xx(1:nbvar,ii)
             end do
          end if
       end if
       
       !-----------------------------------------------------------------
       !
       ! Subdomain interface treatment and memory allocation: 
       !
       ! Solve Mii xi = bi - Mib xb
       !
       !-----------------------------------------------------------------

       if( nbvar == 1 ) then

          !-------------------------------------------------------------
          !
          ! NBVAR = 1 
          !
          !-------------------------------------------------------------

          if( solve_sol(1) % kfl_renumbered_gs == STREAMWISE_GAUSS_SEIDEL ) then

             if( itask == 1 ) then

                !-------------------------------------------------------------
                !
                ! Streamwise Gauss-Seidel, NBVAR = 1, ITASK = 1
                !
                !-------------------------------------------------------------
                !
                ! 1. Solve (L+D) x = b
                !
                do ii_new = 1,npoi1
                   ii       = solve_sol(1) % invpr_gs(ii_new)
                   xx(1,ii) = bb(1,ii)
                   do jj  = ia(ii),ia(ii+1)-1
                      col = ja(jj) 
                      if( solve_sol(1) % permr_gs(col) < ii_new ) then
                         xx(1,ii) = xx(1,ii) - aa(1,1,jj) * xx(1,col) 
                      end if
                   end do
                   xx(1,ii) = xx(1,ii) * invdiag(1,ii)
                end do

             else if( itask == 2 ) then

                !-------------------------------------------------------------
                !
                ! Streamwise Gauss-Seidel, NBVAR = 1, ITASK = 2
                !
                !-------------------------------------------------------------
                !
                ! 1. Compute x = (L+D) b
                !
                do ii_new = 1,npoi1
                   ! x = D b
                   ii       = solve_sol(1) % invpr_gs(ii_new)
                   xx(1,ii) = bb(1,ii) / invdiag(1,ii) 
                   ! x = x + L b
                   do jj  = ia(ii),ia(ii+1)-1
                      col = ja(jj) 
                      if( solve_sol(1) % permr_gs(col) < ii_new ) then
                         xx(1,ii) = xx(1,ii) + aa(1,1,jj) * bb(1,col) 
                      end if
                   end do
                end do

             end if

          else 

             !---------------------------------------------------------------
             !
             ! Gauss-Seidel aad symmetric Gauss-Seidel (SSOR), NBVAR = 1, ITASK = 1
             !
             !---------------------------------------------------------------
             !
             ! Gauss-Seidel:
             !
             ! 1. (L+D) y = b
             !
             ! Symmetric Gauss-Seidel:
             !
             ! (L+D) D^-1 (U+D) x = b => x = (U+D)^-1 D (L+D)^-1 b
             !                                          <-------->
             !                                               y
             !                                        <---------->
             !                                             z
             !                               <------------------->
             !                                           x
             ! 1. (L+D) y = b
             ! 2. z = D y
             ! 3. (U+D) x = z
             !
             if( itask == 1 ) then
                !
                ! (L+D) y = b
                !
                do ii = 1,npoi1
                   xx(1,ii) = bb(1,ii)
                   do jj  = ia(ii),ia(ii+1)-1
                      col = ja(jj) 
                      if( col < ii ) then
                         xx(1,ii) = xx(1,ii) - aa(1,1,jj) * xx(1,col) 
                      end if
                   end do
                   xx(1,ii) = xx(1,ii) * invdiag(1,ii)
                end do

                if( solve_sol(1) % kfl_renumbered_gs == SYMMETRIC_GAUSS_SEIDEL ) then
                   !
                   ! 2. z = D y
                   !              
                   do ii = 1,npoi1
                      yy(1,ii) = xx(1,ii) / invdiag(1,ii)
                   end do
                   !
                   ! 3. (U+D) x = z
                   !
                   do ii = npoi1,1,-1
                      xx(1,ii) = yy(1,ii) 
                      do jj  = ia(ii),ia(ii+1)-1
                         col = ja(jj) 
                         if( col > ii ) then
                            xx(1,ii) = xx(1,ii) - aa(1,1,jj) * xx(1,col) 
                         end if
                      end do
                      xx(1,ii) = xx(1,ii) * invdiag(1,ii)
                    end do
                end if

             else if( itask == 2 ) then

                !---------------------------------------------------------------
                !
                ! Gauss-Seidel and symmetric Gauss-Seidel (SSOR), NBVAR = 1, ITASK = 2
                !
                !---------------------------------------------------------------
                !
                ! Gauss-Seidel:
                !
                ! 1. x = (L+D) b
                !
                ! Symmetric Gauss-Seidel:
                !
                ! x = (L+D) D^-1 (U+D) b
                ! 1. y = (L+D) b
                ! 2. y = D^-1 y
                ! 3. x = (U+D) y 
                !
                do ii = 1,npoi1
                   ! y = D b
                   xx(1,ii) = bb(1,ii) / invdiag(1,ii)
                   ! y = y + L b
                   do jj  = ia(ii),ia(ii+1)-1
                      col = ja(jj) 
                      if( col < ii ) then
                         xx(1,ii) = xx(1,ii) + aa(1,1,jj) * bb(1,col) 
                      end if
                   end do
                end do

                if( solve_sol(1) % kfl_renumbered_gs == SYMMETRIC_GAUSS_SEIDEL ) then
                   !
                   ! 2. y = D^-1 y
                   !              
                   do ii = 1,npoi1
                      yy(1,ii) = xx(1,ii) * invdiag(1,ii)
                   end do
                   !
                   ! 3. x = (U+D) y
                   !
                   do ii = npoi1,1,-1
                      xx(1,ii) = yy(1,ii) / invdiag(1,ii)
                      do jj  = ia(ii),ia(ii+1)-1
                         col = ja(jj) 
                         if( col > ii ) then
                            xx(1,ii) = xx(1,ii) + aa(1,1,jj) * yy(1,col)
                         end if
                      end do
                   end do

                end if

             end if

          end if

       else if( nbvar > 1 ) then

          !------------------------------------------------------------------
          !
          ! NBVAR > 1
          !
          !------------------------------------------------------------------

          if( solve_sol(1) % kfl_renumbered_gs == STREAMWISE_GAUSS_SEIDEL ) then

             if( itask == 1 ) then

                !---------------------------------------------------------------
                !
                ! Streamwise Gauss-Seidel, , NBVAR > 1, ITASK = 1
                !
                !---------------------------------------------------------------
                
                do ii_new = 1,npoi1
                   ii = solve_sol(1) % invpr_gs(ii_new)
                   xx(1:nbvar,ii) = bb(1:nbvar,ii)
                   do jj  = ia(ii),ia(ii+1)-1
                      col = ja(jj) 
                      if( solve_sol(1) % permr_gs(col) < ii_new ) then
                         do ll = 1,nbvar
                            do kk = 1,nbvar
                               xx(kk,ii) = xx(kk,ii) - aa(ll,kk,jj) * xx(ll,col) 
                            end do
                         end do
                      end if
                   end do
                   xx(1:nbvar,ii) = xx(1:nbvar,ii) * invdiag(1:nbvar,ii)
                end do

             else if( itask == 2 ) then

                !-------------------------------------------------------------
                !
                ! Streamwise Gauss-Seidel, NBVAR > 1, ITASK = 2
                !
                !-------------------------------------------------------------
                
                do ii_new = 1,npoi1
                   ! x = D b
                   ii             = solve_sol(1) % invpr_gs(ii_new)
                   xx(1:nbvar,ii) = bb(1:nbvar,ii) / invdiag(1:nbvar,ii) 
                   ! x = x + L b
                   do jj  = ia(ii),ia(ii+1)-1
                      col = ja(jj) 
                      if( solve_sol(1) % permr_gs(col) < ii_new ) then
                         do ll = 1,nbvar
                            do kk = 1,nbvar                              
                               xx(kk,ii) = xx(kk,ii) + aa(ll,kk,jj) * bb(ll,col) 
                            end do
                         end do
                      end if
                   end do
                end do

             end if

          else 

             !---------------------------------------------------------------
             !
             ! Gauss-Seidel aad symmetric Gauss-Seidel (SSOR), NBVAR > 1, ITASK = 1
             !
             !---------------------------------------------------------------

             if( itask == 1 ) then
                !
                ! (L+D) y = b
                !
                do ii = 1,npoi1
                   xx(1:nbvar,ii) = bb(1:nbvar,ii)
                   do jj  = ia(ii),ia(ii+1)-1
                      col = ja(jj) 
                      if( col < ii ) then
                         do ll = 1,nbvar
                            do kk = 1,nbvar
                               xx(kk,ii) = xx(kk,ii) - aa(ll,kk,jj) * xx(ll,col) 
                            end do
                         end do
                      end if
                   end do
                   xx(1:nbvar,ii) = xx(1:nbvar,ii) * invdiag(1:nbvar,ii)
                end do

                if( solve_sol(1) % kfl_renumbered_gs == SYMMETRIC_GAUSS_SEIDEL ) then
                   !
                   ! 2. z = D y
                   !              
                   do ii = 1,npoi1
                      yy(1:nbvar,ii) = xx(1:nbvar,ii) / invdiag(1:nbvar,ii)
                   end do
                   !
                   ! 3. (U+D) x = z
                   !
                   do ii = npoi1,1,-1
                      xx(1:nbvar,ii) = yy(1:nbvar,ii) 
                      do jj  = ia(ii),ia(ii+1)-1
                         col = ja(jj) 
                         if( col > ii ) then
                            do ll = 1,nbvar
                               do kk = 1,nbvar
                                  xx(kk,ii) = xx(kk,ii) - aa(ll,kk,jj) * xx(ll,col) 
                               end do
                            end do
                         end if
                      end do
                      xx(1:nbvar,ii) = xx(1:nbvar,ii) * invdiag(1:nbvar,ii)
                   end do
                end if

             else if( itask == 2 ) then

                !---------------------------------------------------------------
                !
                ! Gauss-Seidel aad symmetric Gauss-Seidel (SSOR), NBVAR > 1, ITASK = 2
                !
                !---------------------------------------------------------------

                do ii = 1,npoi1
                   ! y = D b
                   xx(1:nbvar,ii) = xx(1:nbvar,ii) / invdiag(1:nbvar,ii)
                   ! y = y + L b
                   do jj  = ia(ii),ia(ii+1)-1
                      col = ja(jj) 
                      if( col < ii ) then
                         do ll = 1,nbvar
                            do kk = 1,nbvar
                               xx(kk,ii) = xx(kk,ii) + aa(ll,kk,jj) * bb(ll,col) 
                            end do
                         end do
                      end if
                   end do
                end do

                if( solve_sol(1) % kfl_renumbered_gs == SYMMETRIC_GAUSS_SEIDEL ) then
                   !
                   ! 2. y = D^-1 y
                   !              
                   do ii = 1,npoi1
                      yy(1:nbvar,ii) = xx(1:nbvar,ii) * invdiag(1:nbvar,ii)
                   end do
                   !
                   ! 3. x = (U+D) y
                   !
                   do ii = npoi1,1,-1
                      xx(1:nbvar,ii) = yy(1:nbvar,ii) / invdiag(1:nbvar,ii)
                      do jj  = ia(ii),ia(ii+1)-1
                         col = ja(jj) 
                         if( col > ii ) then
                            do ll = 1,nbvar
                               do kk = 1,nbvar
                                  xx(kk,ii) = xx(kk,ii) + aa(ll,kk,jj) * yy(ll,col) 
                               end do
                            end do
                         end if
                      end do
                   end do
                end if

             end if

          end if

       end if

       if( associated(yy) ) deallocate(yy)

    end if

  end subroutine preconditioner_gauss_seidel

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Bidiagonal solver initialization
  !> @details Allocate and compute some arrays for the bidiagonal 
  !>          preconditioner
  !>
  !----------------------------------------------------------------------

  subroutine preconditioner_bidiagonal_initialize(ngrou,npoin,invpr,lgrou,invdiag,idiag1,adiag1)

    integer(ip), intent(in)           :: ngrou
    integer(ip), intent(in)           :: npoin
    integer(ip), intent(in),  pointer :: invpr(:)
    integer(ip), intent(in),  pointer :: lgrou(:)
    real(rp),    intent(in)           :: invdiag(*)
    integer(ip), intent(out), pointer :: idiag1(:)
    real(rp),    intent(out), pointer :: adiag1(:)
    integer(ip)                       :: kpoin,ipoin,igrou,kgrou,jpoin

    call memory_alloca(memit,'IDIAG1','solope',idiag1,npoin)
    call memory_alloca(memit,'ADIAG1','solope',adiag1,npoin)

    do igrou = 1,ngrou
       kgrou = 0      
       do kpoin = 1,npoin
          ipoin = invpr(kpoin) 
          if( lgrou(ipoin) == igrou ) then 
             if( kgrou == 0 ) then
                idiag1(ipoin) = ipoin
                adiag1(ipoin) = 0.0_rp
             else
                idiag1(ipoin) = jpoin
                adiag1(ipoin) = 1.0_rp / invdiag(jpoin)
             end if
             kgrou = kgrou + 1
             jpoin = ipoin
          end if
       end do
    end do
    do ipoin = 1,npoin
       if( lgrou(ipoin) == -1 ) then
          idiag1(ipoin) = ipoin
          adiag1(ipoin) = 0.0_rp          
          call runend('O.K.!')
       end if
    end do

  end subroutine preconditioner_bidiagonal_initialize

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/10/2014
  !> @brief   Initialize RAS preconditioner
  !> @details Initialize RAS preconditioner
  !>
  !----------------------------------------------------------------------
  
  subroutine preconditioner_RAS_initialize(ndof,ia,ja,direct_solver)
    integer(ip),             intent(in)          :: ndof
    integer(ip),             intent(in), pointer :: ia(:) 
    integer(ip),             intent(in), pointer :: ja(:) 
    type(direct_solver_typ), intent(out)         :: direct_solver 

    direct_solver % ia_in => ia
    direct_solver % ja_in => ja
    direct_solver % nn    =  size(ia)-1
    direct_solver % ndof  =  ndof
    direct_solver % nz    =  ia(direct_solver % nn+1)-1
    if( size(ja) /= direct_solver % nz ) call runend('preconditioner_RAS_initialize: WRONG GRAPH')

    !call solver_direct_solver_initialize(direct_solver)
    
  end subroutine preconditioner_RAS_initialize

end module mod_preconditioner
!> @} 
!-----------------------------------------------------------------------
