subroutine outcso(nbvar,nrows,npoin,invdiag,an,ja,ia,bb,xx)
  !------------------------------------------------------------------------
  !****f* solite/outcso
  ! NAME 
  !    outcso
  ! DESCRIPTION
  !    Output solve convergence:
  !    KFL_EXRES = 0 ... Preconditioned residual 
  !              = 1 ... Preconditioned residual and residual
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  IMASTER,INOTSLAVE,INOTMASTER,kfl_paral,ittim
  use def_solver, only       :  resi1,iters,solve_sol
  use def_solver, only       :  SOL_SOLVER_RICHARDSON
  use def_solver, only       :  SOL_SOLVER_MATRIX_RICHARDSON
  use def_solver, only       :  SOL_RIGHT_PRECONDITIONING
  use mod_postpr, only       :  postpr
  implicit none
  integer(ip), intent(in)    :: nbvar,nrows,npoin
  real(rp),    intent(in)    :: invdiag(*)
  real(rp),    intent(in)    :: an(*)
  integer(ip), intent(in)    :: ja(*),ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(in)    :: xx(*)
  integer(4)                 :: istat
  integer(ip)                :: ii,ndofn
  real(rp),    pointer       :: rr(:),xx_tmp(:)
  real(rp)                   :: rnorm,bnorm,dummr
  character(5)               :: wopos(3)
  integer(ip), save          :: ipass = 0

  ndofn = abs(nbvar)
  
  if( solve_sol(1) % kfl_cvgso == 1 ) then

     if( solve_sol(1) % kfl_exres == 0 .or. nbvar < 0 ) then

        if( INOTSLAVE ) write(solve_sol(1) % lun_cvgso,100) iters,resi1

     else
        allocate(rr(max(1,nrows))) 
        !
        ! A R^-1 x
        !
        if(  solve_sol(1) % kfl_algso == SOL_SOLVER_RICHARDSON .or. &
             solve_sol(1) % kfl_algso == SOL_SOLVER_MATRIX_RICHARDSON ) then
           call runend('OUTCSO: OPTION IMPOSSIBLE')
        else
           if ( solve_sol(1) % kfl_leftr == SOL_RIGHT_PRECONDITIONING ) then
              allocate(xx_tmp(max(1,nrows))) 
              do ii = 1,nrows
                 xx_tmp(ii) = xx(ii)
              end do
              call precon(&
                   5_ip,nbvar,npoin,nrows,solve_sol(1) % kfl_symme,solve_sol(1) % kfl_preco,ia,ja,an,&
                   dummr,invdiag,rr,bb,xx_tmp)
              call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx_tmp, rr )   
              deallocate( xx_tmp )
           else
              call bcsrax( 1_ip, npoin, nbvar, an, ja, ia, xx, rr )  
           end if
           do ii = 1,nrows        
              rr(ii) = bb(ii) - rr(ii)
           end do
        end if
        !
        ! Postprocess residual
        ! 
        !ipass=ipass+1;ittim=ipass;wopos(1)='RESID';wopos(2)='SCALA';wopos(3)='NPOIN';call postpr(rr,wopos,ipass,real(ipass,rp))   

        call norm2x(ndofn,rr,rnorm) 
        call norm2x(ndofn,bb,bnorm)
        if( bnorm /= 0.0_rp ) rnorm = rnorm/bnorm

        if( INOTSLAVE ) write(solve_sol(1) % lun_cvgso,100) iters,resi1,rnorm

        deallocate( rr )

     end if

     !call outress(ndofn,nrows,npoin,an,ja,ia,bb,xx)

  end if

100 format(i7,2(1x,e12.6))

end subroutine outcso

subroutine outress(nbvar,nrows,npoin,an,ja,ia,bb,xx)
  !------------------------------------------------------------------------
  !****f* solite/outcso
  ! NAME 
  !    outcso
  ! DESCRIPTION
  !    Output solve convergence:
  !    KFL_EXRES = 0 ... Preconditioned residual 
  !              = 1 ... Preconditioned residual and residual
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  IMASTER,INOTSLAVE,kfl_paral,intost,gesca
  use def_domain, only       :  coord
  use def_solver, only       :  resi1,iters,solve_sol
  implicit none
  integer(ip), intent(in)    :: nbvar,nrows,npoin
  real(rp),    intent(in)    :: an(*)
  integer(ip), intent(in)    :: ja(*),ia(*)
  real(rp),    intent(in)    :: bb(*)
  real(rp),    intent(in)    :: xx(*)
  integer(4)                 :: istat
  integer(ip)                :: ii
  real(rp),    pointer       :: rr(:)
  real(rp)                   :: rnorm,bnorm,x
  
  if(iters==5.or.iters==10.or.iters==50) then
     open(unit=90,file='dcg-'//trim(intost(iters))//'.txt',status='unknown')
     !call memgen(0_ip,npoin,0_ip)
     do ii = 1,npoin
        x=coord(1,ii)
        write(90,*) x,xx(ii)-(0.5_rp*x*(1.0_rp-x))
     end do
     close(90)
     !wopos(1) = 'RESID'
     !wopos(2) = 'SCALA'
     !wopos(3) = 'NPOIN'     
     !call postpr(gesca,wopos,iters,real(ittim,rp))   
     !call memgen(2_ip,npoin,0_ip)
  end if


end subroutine outress
