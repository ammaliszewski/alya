subroutine prodxy(nbvar,nbnodes,xx,yy,sumxx)
  !------------------------------------------------------------------------
  !****f* solite/prodxy
  ! NAME 
  !    prodxy
  ! DESCRIPTION
  !    This routine computes the sclara product of two vectors:
  !    SUMXX=XX^t.YY
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_master, only     :  kfl_paral,npoi1,npoi2,npoi3,parre,nparr
  use def_master, only     :  npoi4,lpoi4
  implicit none
  integer(ip), intent(in)  :: nbvar,nbnodes
  real(rp),    intent(in)  :: xx(*),yy(*)
  real(rp),    intent(out) :: sumxx
  real(rp),    target      :: dummr_par(2) 
  integer(ip)              :: kk,ii,jj,ll

  sumxx = 0.0_rp

  if(kfl_paral==-1) then
     !
     ! Sequential
     !
     do kk=1,nbvar*nbnodes
        sumxx = sumxx + xx(kk) * yy(kk)
     end do
 
  else if(kfl_paral>=1) then
     !
     ! Parallel: Slaves
     !
     if(nbvar==1) then 
        do ii=1,npoi1
           sumxx  = sumxx  + xx(ii) * yy(ii)
        end do
        do ii=npoi2,npoi3
           sumxx  = sumxx  + xx(ii) * yy(ii)
        end do
        do jj = 1,npoi4
           ii = lpoi4(jj)
           sumxx  = sumxx  + xx(ii) * yy(ii)
        end do

     else
        do ii=1,nbvar*npoi1
           sumxx = sumxx + xx(ii) * yy(ii)
        end do
        do ii=(npoi2-1)*nbvar+1,npoi3*nbvar
           sumxx  = sumxx  + xx(ii) * yy(ii)
        end do
        do jj = 1,npoi4
           ii = lpoi4(jj)
           ll = (ii-1)*nbvar
           do kk = 1,nbvar
              ll = ll + 1
              sumxx  = sumxx  + xx(ll) * yy(ll)
           end do
        end do
     end if
  end if

  if(kfl_paral>=0) then
     !
     ! Parallel: reduce sum
     !
     !call pararr('SUM',0_ip,1_ip,sumxx)
     nparr        =  1
     dummr_par(1) =  sumxx
     parre        => dummr_par
     call Parall(9_ip)           
     sumxx        =  dummr_par(1)
  end if

end subroutine prodxy

