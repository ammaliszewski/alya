!------------------------------------------------------------------------
!
! Operations for the deflated CG
!
!------------------------------------------------------------------------

subroutine matgrx1(ngrou,npopo,nskyl,ndofn,ia,ja,an,askyl)
  !
  ! ASKYL: Factorize group matrix
  !
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  kfl_paral,parre,nparr,parcx,nparx,INOTMASTER
  use def_solver, only               :  solve_sol
  use mod_memchk
  implicit none
  integer(ip), intent(in)            :: ngrou,npopo,nskyl,ndofn
  integer(ip), intent(in)            :: ia(*),ja(*)
  complex(rp), intent(in)            :: an(ndofn,ndofn,*)
  complex(rp), intent(inout), target :: askyl(nskyl)
  integer(ip), pointer               :: iskyl(:),lgrou(:)
  integer(ip), pointer               :: iagro(:),jagro(:)
  integer(ip)                        :: igrou,jgrou,ipoin,izsym,jpoin,izgro
  integer(ip)                        :: info,kskyl,idofn,jdofn,igrou1,jgrou1

  if(ngrou==0) return

  if( INOTMASTER ) then

     if( solve_sol(1) % kfl_defas == 1 ) then

        !----------------------------------------------------------------
        !
        ! Fill in sparse matrix ASKYL 
        !       
        !----------------------------------------------------------------

        lgrou => solve_sol(1) % lgrou
        iagro => solve_sol(1) % ia
        jagro => solve_sol(1) % ja
        
        if( ndofn == 1 ) then

           do ipoin = 1,npopo
              if( lgrou(ipoin) > 0 ) then
                 igrou = lgrou(ipoin)
                 do izsym = ia(ipoin),ia(ipoin+1)-1
                    jpoin = ja(izsym)
                    if( lgrou(jpoin) > 0 ) then
                       jgrou = lgrou(jpoin)
                       izgro = iagro(igrou)
                       iifzgro1: do while( izgro <= iagro(igrou)+1-1 )
                          if( jagro(izgro) == jgrou ) exit iifzgro1
                          izgro = izgro + 1
                       end do iifzgro1
                       askyl(izgro) = askyl(izgro) + an(1,1,izsym)
                    end if
                 end do
              end if
           end do

        else

           call runend('MATGRO: NOT CODED')

        end if

     else

        !----------------------------------------------------------------
        !
        ! Fill in skyline matrix ASKYL 
        !       
        !----------------------------------------------------------------

        lgrou => solve_sol(1) % lgrou
        iskyl => solve_sol(1) % iskyl

        if(ndofn==1)then

           do ipoin=1,npopo
              if(lgrou(ipoin)>0) then
                 igrou=lgrou(ipoin)
                 do izsym=ia(ipoin),ia(ipoin+1)-2
                    jpoin=ja(izsym)
                    if(lgrou(jpoin)>0) then
                       jgrou=lgrou(jpoin)
                       if(igrou>jgrou) then
                          kskyl=iskyl(igrou+1)-1-(igrou-jgrou)
                          askyl(kskyl)=askyl(kskyl)+an(1,1,izsym)
                       else if(igrou<jgrou) then
                          kskyl=iskyl(jgrou+1)-1-(jgrou-igrou)
                          askyl(kskyl)=askyl(kskyl)+an(1,1,izsym)
                       else
                          kskyl=iskyl(igrou+1)-1
                          askyl(kskyl)=askyl(kskyl)+2.0_rp*an(1,1,izsym)
                       end if
                    end if
                 end do
                 izsym=ia(ipoin+1)-1
                 kskyl=iskyl(igrou+1)-1
                 askyl(kskyl)=askyl(kskyl)+an(1,1,izsym)  
              end if
           end do

        else

           do ipoin=1,npopo
              if(lgrou(ipoin)>0) then
                 igrou=lgrou(ipoin)

                 do izsym=ia(ipoin),ia(ipoin+1)-2
                    jpoin=ja(izsym)
                    if(lgrou(jpoin)>0) then
                       jgrou=lgrou(jpoin)

                       do idofn=1,ndofn
                          do jdofn=1,idofn

                             igrou1=(igrou-1)*ndofn+idofn
                             jgrou1=(jgrou-1)*ndofn+jdofn

                             if(igrou1>jgrou1) then

                                kskyl=iskyl(igrou1+1)-1-(igrou1-jgrou1)
                                askyl(kskyl)=askyl(kskyl)+an(idofn,jdofn,izsym)

                             else if(igrou1<jgrou1) then

                                kskyl=iskyl(jgrou1+1)-1-(jgrou1-igrou1)
                                askyl(kskyl)=askyl(kskyl)+an(idofn,jdofn,izsym)

                             else

                                kskyl=iskyl(igrou1+1)-1
                                askyl(kskyl)=askyl(kskyl)+2.0_rp*an(idofn,jdofn,izsym)

                             endif
                          enddo
                       enddo
                    end if
                 enddo

                 izsym=ia(ipoin+1)-1

                 do idofn=1,ndofn
                    do jdofn=1,idofn

                       igrou1=(igrou-1)*ndofn+idofn
                       jgrou1=(igrou-1)*ndofn+jdofn
                       kskyl=iskyl(igrou1+1)-1-(igrou1-jgrou1)
                       askyl(kskyl)=askyl(kskyl)+an(idofn,jdofn,izsym) 

                    enddo
                 enddo

              end if
           end do



        end if

     end if

  end if

  if(kfl_paral>=0) then
     !
     ! Parallel: reduce sum
     !
     nparx =  nskyl
     parcx => askyl
     call Parall(9_ip)
  end if
  !
  ! Inverse matrix ASKYL
  !
  if(kfl_paral/=0 .and. solve_sol(1) % kfl_defas == 0 ) then
     call chofac(ngrou*ndofn,nskyl,iskyl,askyl,info)
     if(info/=0) call runend('MATGRO: ERROR WHILE DOING CHOLESKY FACTORIZATION')
  end if

end subroutine matgrx1

subroutine matgrx2(ngrou,npopo,nskyl,ndofn,ia,ja,an,askyl)
  !
  ! ASKYL: Factorize group matrix
  !
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  INOTMASTER,INOTSLAVE,parre,nparr,parcx,nparx,IPARALL,kfl_paral
  use def_solver, only               :  solve_sol
  use def_domain, only               :  r_dom,c_dom
  use mod_memchk
  implicit none
  integer(ip), intent(in)            :: ngrou,npopo,nskyl,ndofn
  real(rp)                           :: cpu_syma1,cpu_syma2,cpu_syma
  integer(ip), intent(in)            :: ia(*),ja(*)
  complex(rp), intent(in)            :: an(ndofn,ndofn,*)
  complex(rp), intent(inout), target :: askyl(nskyl)
  integer(ip), pointer               :: iskyl(:),lgrou(:)
  integer(ip)                        :: igrou,jgrou,ipoin,izdom,jpoin,izgro,izgro1
  integer(ip)                        :: info,kskyl,idofn,jdofn,igrou1,jgrou1
  integer(ip), pointer               :: iagro(:),jagro(:)

  if( ngrou==0 ) return

  if( INOTMASTER ) then

     if( solve_sol(1) % kfl_defas == 1 ) then

        !----------------------------------------------------------------
        !
        ! Fill in sparse matrix ASKYL 
        !       
        !----------------------------------------------------------------

        lgrou => solve_sol(1) % lgrou
        iagro => solve_sol(1) % ia
        jagro => solve_sol(1) % ja

        if( ndofn == 1 ) then
        
           do ipoin = 1,npopo
              if( lgrou(ipoin) > 0 ) then
                 igrou = lgrou(ipoin)
                 do izdom = ia(ipoin),ia(ipoin+1)-1
                    jpoin = ja(izdom)
                    if( lgrou(jpoin) > 0 ) then
                       jgrou = lgrou(jpoin)
                       izgro = iagro(igrou)
                       iifzgro1: do while( jagro(izgro) /= jgrou )
                          izgro = izgro + 1
                       end do iifzgro1
                       askyl(izgro) = askyl(izgro) + an(1,1,izdom)
                    end if
                 end do
              end if
           end do

        else
      
            do ipoin = 1,npopo
              if( lgrou(ipoin) > 0 ) then
                 igrou = lgrou(ipoin)
                 do izdom = ia(ipoin),ia(ipoin+1)-1
                    jpoin = ja(izdom)
                    if( lgrou(jpoin) > 0 ) then
                       jgrou = lgrou(jpoin)
                       izgro = iagro(igrou)
                       iifzgro2: do while( jagro(izgro) /= jgrou )
                          izgro = izgro + 1
                       end do iifzgro2
                       do jdofn = 1,ndofn
                         do idofn = 1,ndofn
                           izgro1 = (izgro-1)*ndofn*ndofn+(jdofn-1)*ndofn+idofn
                           askyl(izgro1) = askyl(izgro1) + an(idofn,jdofn,izdom)
                         end do
                       end do  
                    end if
                 end do
              end if
           end do         
        end if

     else

        !----------------------------------------------------------------
        !
        ! Fill in skyline matrix ASKYL 
        !       
        !----------------------------------------------------------------

        lgrou => solve_sol(1) % lgrou
        iskyl => solve_sol(1) % iskyl

        if( ndofn==1 )then

           !if( kfl_servi(ID_DODEME) /= 0 ) then
           !   do ipoin = 1,npopo
           !      if( lnsub_dod(3,ipoin) >= 2 ) then
           !         lgrou(ipoin) = -1
           !      end if
           !   end do
           !end if

           do ipoin = 1,npopo
              if( lgrou(ipoin) > 0 ) then
                 igrou = lgrou(ipoin)
                 do izdom = ia(ipoin),ia(ipoin+1)-1
                    jpoin = ja(izdom)
                    if( jpoin < ipoin ) then
                       if( lgrou(jpoin) > 0 ) then
                          jgrou = lgrou(jpoin)
                          if( igrou > jgrou ) then
                             kskyl        = iskyl(igrou+1) - 1 - (igrou-jgrou)
                             askyl(kskyl) = askyl(kskyl) + an(1,1,izdom)
                          else if( igrou < jgrou ) then
                             kskyl        = iskyl(jgrou+1) - 1 - (jgrou-igrou)
                             askyl(kskyl) = askyl(kskyl) + an(1,1,izdom)
                          else
                             kskyl        = iskyl(igrou+1) - 1
                             askyl(kskyl) = askyl(kskyl) + 2.0_rp*an(1,1,izdom)
                          end if
                       end if
                    else if( ipoin == jpoin ) then
                       kskyl        = iskyl(igrou+1) - 1
                       askyl(kskyl) = askyl(kskyl) + an(1,1,izdom)  
                    end if
                 end do
              end if
           end do

        else

           do ipoin=1,npopo
              if(lgrou(ipoin)>0) then
                 igrou=lgrou(ipoin)

                 do izdom=ia(ipoin),ia(ipoin+1)-2
                    jpoin=ja(izdom)
                    if(lgrou(jpoin)>0) then
                       jgrou=lgrou(jpoin)

                       do idofn=1,ndofn
                          do jdofn=1,idofn

                             igrou1=(igrou-1)*ndofn+idofn
                             jgrou1=(jgrou-1)*ndofn+jdofn

                             if(igrou1>jgrou1) then

                                kskyl=iskyl(igrou1+1)-1-(igrou1-jgrou1)
                                askyl(kskyl)=askyl(kskyl)+an(idofn,jdofn,izdom)

                             else if(igrou1<jgrou1) then

                                kskyl=iskyl(jgrou1+1)-1-(jgrou1-igrou1)
                                askyl(kskyl)=askyl(kskyl)+an(idofn,jdofn,izdom)

                             else

                                kskyl=iskyl(igrou1+1)-1
                                askyl(kskyl)=askyl(kskyl)+2.0_rp*an(idofn,jdofn,izdom)

                             endif
                          enddo
                       enddo
                    end if
                 enddo

                 izdom=ia(ipoin+1)-1

                 do idofn=1,ndofn
                    do jdofn=1,idofn

                       igrou1=(igrou-1)*ndofn+idofn
                       jgrou1=(igrou-1)*ndofn+jdofn
                       kskyl=iskyl(igrou1+1)-1-(igrou1-jgrou1)
                       askyl(kskyl)=askyl(kskyl)+an(idofn,jdofn,izdom) 

                    enddo
                 enddo

              end if
           end do

        end if

     end if

  end if

  if( IPARALL ) then
     !
     ! Parallel: reduce sum
     !
     nparx =  nskyl
     parcx => askyl
     call Parall(9_ip)
  end if
  !
  ! Inverse matrix ASKYL
  !
  if( INOTMASTER .and. solve_sol(1) % kfl_defas == 0 ) then
     call chofac(ngrou*ndofn,nskyl,iskyl,askyl,info)
     if(info/=0) call runend('MATGR2: ERROR WHILE DOING CHOLESKY FACTORIZATION')
  end if

end subroutine matgrx2

subroutine wtvectx(npopo,ngrou,ndofn,xsmall,xbig)
  !
  ! XSMALL= W^T.XBIG 
  !
  use def_kintyp, only             :  ip,rp
  use def_master, only             :  IPARALL,parre,parcx,nparx,nparr,npoi1,npoi2,npoi3,&
       &                              NPOIN_REAL_12DI,parr1,icoml,ISLAVE,ISEQUEN,kfl_paral
  use def_solver, only             :  solve_sol
  implicit none
  integer(ip), intent(in)          :: npopo,ngrou,ndofn
  complex(rp), intent(in)          :: xbig(*)
  complex(rp), intent(out), target :: xsmall(ngrou*ndofn)
  integer(ip)                      :: ipoin,igrou,ipoin1,igrou1,idofn
  real(rp)                         :: cpu_smal1,cpu_smal2,cpu_smal

  do igrou=1,solve_sol(1) % ngrou*ndofn
     xsmall(igrou)=(0.0_rp,0.0_rp)
  end do

  if( ISEQUEN ) then
  !!call cputim(cpu_smal1)
     if(ndofn==1)then  
        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0) xsmall(igrou)=xsmall(igrou)+xbig(ipoin)
        end do
     else
        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           do idofn=1,ndofn
              igrou1=(igrou-1)*ndofn+idofn
              ipoin1=(ipoin-1)*ndofn+idofn
              if(igrou>0) xsmall(igrou1)=xsmall(igrou1)+xbig(ipoin1)
           enddo
        end do        
     end if
  !!call cputim(cpu_smal2)
  !!cpu_smal = cpu_smal2 - cpu_smal1
  !!write(*,*)'Time to small1:',cpu_smal,kfl_paral

  else if( ISLAVE ) then
  !!call cputim(cpu_smal1)
     if(ndofn==1)then  

        do ipoin=1,npoi1
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0) xsmall(igrou)=xsmall(igrou)+xbig(ipoin)
        end do
        
        do ipoin=npoi2,npoi3
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0) xsmall(igrou)=xsmall(igrou)+xbig(ipoin)
        end do

     else

        do ipoin=1,npoi1
           igrou=solve_sol(1) % lgrou(ipoin)
           igrou1=(igrou-1)*ndofn
           ipoin1=(ipoin-1)*ndofn
           do idofn=1,ndofn 
              igrou1=igrou1+1
              ipoin1=ipoin1+1
              if(igrou>0) xsmall(igrou1)=xsmall(igrou1)+xbig(ipoin1)
           enddo
        end do

        do ipoin=npoi2,npoi3
           igrou=solve_sol(1) % lgrou(ipoin)
              igrou1=(igrou-1)*ndofn
              ipoin1=(ipoin-1)*ndofn
           do idofn=1,ndofn 
              igrou1=igrou1+1
              ipoin1=ipoin1+1
              if(igrou>0) xsmall(igrou1)=xsmall(igrou1)+xbig(ipoin1)
           enddo
        end do

     end if
  !!call cputim(cpu_smal2)
  !!cpu_smal = cpu_smal2 - cpu_smal1
  !!write(*,*)'Time to small1:',cpu_smal,kfl_paral
  end if

  if( IPARALL ) then
  !!call cputim(cpu_smal1)
     nparx =  solve_sol(1) % ngrou*ndofn
     parcx => xsmall 

     if( solve_sol(1) % kfl_gathe == 1 ) then
        !
        ! Parallel: all gather v
        !
        icoml =  solve_sol(1) % icoml
        call Parall(34_ip)
     else if( solve_sol(1) % kfl_gathe == 0 ) then
        !
        ! Parallel: reduce sum
        !
        call Parall(9_ip)        
     else if( solve_sol(1) % kfl_gathe == 2 ) then
        !
        ! Parallel: send/receive
        !
        call Parall(803_ip)        !!!!!!!!Verovatno treba promeniti 
     end if
  !!call cputim(cpu_smal2)
  !!cpu_smal = cpu_smal2 - cpu_smal1
  !!write(*,*)'Time to small exch:',cpu_smal,kfl_paral
  end if

end subroutine wtvectx

subroutine wvectx(npopo,ndofn,xsmall,xbig)
  !
  ! XBIG= W.XSMALL 
  !
  use def_kintyp, only     :  ip,rp
  use def_solver, only     :  solve_sol
  use def_domain, only     :  nimbo
  use def_master, only     :  kfl_servi,ID_DODEME,lnsub_dod,lntib,kfl_coibm,kfl_paral
  implicit none
  integer(ip), intent(in)  :: npopo,ndofn
  complex(rp), intent(in)  :: xsmall(*)
  complex(rp), intent(out) :: xbig(*)
  integer(ip)              :: ipoin,igrou,ipoin1,igrou1,idofn

  if(ndofn==1)then
     
     if( nimbo > 0 .and. kfl_servi(ID_DODEME) /= 0 ) then

        call runend('DEFOPE: UNIFY DODEME AND IMMBOU')

     else if( nimbo > 0 .and. kfl_coibm == 1 ) then

        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0) then
              xbig(ipoin)=xsmall(igrou)
              if( lntib(ipoin) > 0 ) xbig(ipoin)=(0.0_rp,0.0_rp)
           else
              xbig(ipoin)=(0.0_rp,0.0_rp)
           end if
        end do

     else if( kfl_servi(ID_DODEME) /= 0 ) then

        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0.and.lnsub_dod(3,ipoin)<2) then
              xbig(ipoin)=xsmall(igrou)
           else
              xbig(ipoin)=(0.0_rp,0.0_rp)
           end if
        end do

     else

        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0) then
              xbig(ipoin)=xsmall(igrou)
           else
              xbig(ipoin)=(0.0_rp,0.0_rp)
           end if
        end do

     end if

  else

     if( nimbo > 0 ) then
        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           igrou1=(igrou-1)*ndofn
           ipoin1=(ipoin-1)*ndofn
           do idofn=1,ndofn 
              igrou1=igrou1+1
              ipoin1=ipoin1+1
              if(igrou>0) then
                 xbig(ipoin1)=xsmall(igrou1)
                 if( lntib(ipoin) > 0 ) xbig(ipoin1)=(0.0_rp,0.0_rp)
              else
                 xbig(ipoin1)=(0.0_rp,0.0_rp)
              end if
           enddo
        end do
     else
        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           igrou1=(igrou-1)*ndofn
           ipoin1=(ipoin-1)*ndofn
           do idofn=1,ndofn 
              igrou1=igrou1+1
              ipoin1=ipoin1+1
              if(igrou>0) then
                 xbig(ipoin1)=xsmall(igrou1)
              else
                 xbig(ipoin1)=(0.0_rp,0.0_rp)
              end if
           enddo
        end do       
     end if

  endif

end subroutine wvectx

subroutine matgrux(ngrou,npopo,nskyl,ndofn,ia,ja,an,askyl,invdiag)
  !
  ! ASKYL: Factorize group matrix
  !
  use def_kintyp, only               :  ip,rp
  use def_master, only               :  INOTMASTER,IPARALL,parre,nparr
  use def_solver, only               :  solve_sol
  use mod_memchk
  implicit none
  integer(ip), intent(in)            :: ngrou,npopo,nskyl,ndofn
  integer(ip), intent(in)            :: ia(*),ja(*)
  real(rp),    intent(in)            :: an(ndofn,ndofn,*),invdiag(*)
  real(rp),    intent(inout), target :: askyl(nskyl)
  integer(ip), pointer               :: iskyl(:),lgrou(:),idiag(:)
  integer(ip), pointer               :: iagro(:),jagro(:)
  integer(ip)                        :: igrou,jgrou,ipoin,izdom,jpoin,izgro
  integer(ip)                        :: info,kskyl,idofn,jdofn,igrou1,jgrou1

  if( INOTMASTER ) then

     if( solve_sol(1) % kfl_defas == 1 ) then

        !----------------------------------------------------------------
        !
        ! Fill in sparse matrix ASKYL 
        !       
        !----------------------------------------------------------------

        lgrou => solve_sol(1) % lgrou
        iagro => solve_sol(1) % ia
        jagro => solve_sol(1) % ja

        if( ndofn == 1 ) then

           do ipoin = 1,npopo
              if( lgrou(ipoin) > 0 ) then
                 igrou = lgrou(ipoin)
                 do izdom = ia(ipoin),ia(ipoin+1)-1
                    jpoin = ja(izdom)
                    if( lgrou(jpoin) > 0 ) then
                       jgrou = lgrou(jpoin)
                       izgro = iagro(igrou)
                       iifzgro1: do while( izgro <= iagro(igrou)+1-1 )
                          if( jagro(izgro) == jgrou ) exit iifzgro1
                          izgro = izgro + 1
                       end do iifzgro1
                       askyl(izgro) = askyl(izgro) + an(1,1,izdom)
                    end if
                 end do
              end if
           end do

        else

           call runend('MATGRO: NOT CODED')

        end if

     else

        !----------------------------------------------------------------
        !
        ! Fill in skyline matrix ASKYL 
        !       
        !----------------------------------------------------------------

        lgrou => solve_sol(1) % lgrou
        iskyl => solve_sol(1) % iskyl
        idiag => solve_sol(1) % idiag

        if(ndofn==1)then

           do ipoin=1,npopo
              if(lgrou(ipoin)>0) then
                 igrou=lgrou(ipoin)
                 do izdom=ia(ipoin),ia(ipoin+1)-1
                    jpoin=ja(izdom)
                    if(lgrou(jpoin)>0) then
                       jgrou = lgrou(jpoin)
                       if( igrou < jgrou ) then
                          kskyl = iskyl(jgrou+1)-(jgrou-igrou)
                          askyl(kskyl) = askyl(kskyl) + an(1,1,izdom)
                       else     
                          kskyl = idiag(igrou)-(igrou-jgrou)
                          askyl(kskyl) = askyl(kskyl) + an(1,1,izdom)
                       end if
                    end if
                 end do
              end if
           end do

        else

           do ipoin=1,npopo
              if(lgrou(ipoin)>0) then
                 igrou=lgrou(ipoin)

                 do izdom=ia(ipoin),ia(ipoin+1)-1
                    jpoin=ja(izdom)

                    if(lgrou(jpoin)>0) then
                       jgrou=lgrou(jpoin)

                       do idofn=1,ndofn
                          do jdofn=1,ndofn 

                             igrou1=(igrou-1)*ndofn+idofn
                             jgrou1=(jgrou-1)*ndofn+jdofn

                             if(igrou1<jgrou1) then
                                kskyl=iskyl(jgrou1+1)-(jgrou1-igrou1)
                                askyl(kskyl)=askyl(kskyl)+an(idofn,jdofn,izdom)
                             else     
                                kskyl=idiag(igrou1)-(igrou1-jgrou1)
                                askyl(kskyl)=askyl(kskyl)+an(idofn,jdofn,izdom)
                             endif

                          end do
                       end do


                    end if
                 end do
              end if
           end do

        end if

     end if

  end if

  if( IPARALL ) then
     !
     ! Parallel: reduce sum
     !
     nparr =  nskyl
     parre => askyl
     call Parall(9_ip)
  end if
  !
  ! Inverse matrix ASKYL
  !
  if( INOTMASTER .and. solve_sol(1) % kfl_defas == 0 ) then
     call lufact(ngrou*ndofn,nskyl,iskyl,askyl,idiag,info)
     if( info /= 0 ) call runend('MATGRU: ERROR WHILE DOING CHOLESKY FACTORIZATION')
  end if

end subroutine matgrux

subroutine wvectx2(npopo,ndofn,xsmall,xbig)
  !
  ! XBIG= W.XSMALL 
  !
  use def_kintyp, only     :  ip,rp
  use def_solver, only     :  solve_sol
  use def_domain, only     :  nimbo
  use def_master, only     :  kfl_servi,ID_DODEME,lnsub_dod,lntib,kfl_coibm
  implicit none
  integer(ip), intent(in)  :: npopo,ndofn
  real(rp),    intent(in)  :: xsmall(*)
  real(rp),    intent(out) :: xbig(*)
  integer(ip)              :: ipoin,igrou,ipoin1,igrou1,idofn

  if(ndofn==1)then
     
     if( nimbo > 0 .and. kfl_servi(ID_DODEME) /= 0 ) then

        call runend('DEFOPE: UNIFY DODEME AND IMMBOU')

     else if( nimbo > 0 .and. kfl_coibm == 1 ) then

        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0) then
              xbig(ipoin)=xsmall(igrou)
              if( lntib(ipoin) > 0 ) xbig(ipoin)=0.0_rp
           end if
        end do

     else if( kfl_servi(ID_DODEME) /= 0 ) then

        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0.and.lnsub_dod(3,ipoin)<2) then
              xbig(ipoin)=xsmall(igrou)
           end if
        end do

     else

        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           if(igrou>0) then
              xbig(ipoin)=xsmall(igrou)
           end if
        end do

     end if

  else

     if( nimbo > 0 ) then
        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           igrou1=(igrou-1)*ndofn
           ipoin1=(ipoin-1)*ndofn
           do idofn=1,ndofn 
              igrou1=igrou1+1
              ipoin1=ipoin1+1
              if(igrou>0) then
                 xbig(ipoin1)=xsmall(igrou1)
                 if( lntib(ipoin) > 0 ) xbig(ipoin1)=0.0_rp
              end if
           enddo
        end do
     else
        do ipoin=1,npopo
           igrou=solve_sol(1) % lgrou(ipoin)
           igrou1=(igrou-1)*ndofn
           ipoin1=(ipoin-1)*ndofn
           do idofn=1,ndofn 
              igrou1=igrou1+1
              ipoin1=ipoin1+1
              if(igrou>0) then
                 xbig(ipoin1)=xsmall(igrou1)
              end if
           enddo
        end do       
     end if

  endif

end subroutine wvectx2
