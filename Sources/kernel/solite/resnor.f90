subroutine resnor(ndofn,kfl_symme,rhsid,unkno,amatr,xnorm)
  !-----------------------------------------------------------------------
  !****f* master/resnor
  ! NAME 
  !    resnor
  ! DESCRIPTION
  !    This routine calls the resnors
  !    For diagonal solve which uses vmass, amatr must NOT be modified
  ! USES
  !    memchk
  !    mediso
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_master, only       :  NPOIN_REAL_12DI,parr1,INOTMASTER,kfl_paral
  use def_domain, only       :  npoin,c_dom,r_dom,c_sym,r_sym,ndime
  use def_solver, only       :  solve_sol,memma
  use mod_memchk
  implicit none
  integer(ip), intent(in)    :: ndofn,kfl_symme
  real(rp),    intent(in)    :: unkno(*)
  real(rp),    intent(in)    :: amatr(*)
  real(rp),    intent(in)    :: rhsid(*)
  real(rp),    intent(out)   :: xnorm
  real(rp),    pointer       :: rr(:),ww(:)
  integer(ip)                :: iunkn,nunkn
  integer(4)                 :: istat
  real(rp)                   :: raux1,raux2

  if( INOTMASTER ) then
     nunkn = npoin * ndofn
  else
     nunkn = 0
  end if
  allocate(rr(max(nunkn,1_ip)),stat=istat)
  call memchk(0_ip,istat,memma,'RR','resnor',rr)
  allocate(ww(max(nunkn,1_ip)),stat=istat)
  call memchk(0_ip,istat,memma,'RR','resnor',rr)
  !
  ! RR = b - A x
  !
  if( kfl_symme == 0 ) then
     call bcsrax(1_ip,npoin,ndofn,amatr,c_dom,r_dom,unkno,rr)
  else
     call bsymax(1_ip,npoin,ndofn,amatr,c_sym,r_sym,unkno,rr)
  end if
  do iunkn = 1,nunkn
     rr(iunkn) = rhsid(iunkn) - rr(iunkn)
  end do
  !
  ! Diagonal D
  !
  if( kfl_symme == 0 ) then
     call diagon(npoin,ndofn,kfl_symme,r_dom,c_dom,amatr,ww)
  else
     call diagon(npoin,ndofn,kfl_symme,r_sym,c_sym,amatr,ww)
  end if
  do iunkn = 1,nunkn
     ww(iunkn) = 1.0_rp / ww(iunkn) 
  end do
  !
  ! RR = D^-1 ( b - A x )
  !
  do iunkn = 1,nunkn
     rr(iunkn) = rr(iunkn) * ww(iunkn)
  end do
  call norm2x(ndofn,rr,raux2)
  !
  ! RR = D^-1 ( b - A x )
  !
  do iunkn = 1,nunkn
     rr(iunkn) = rhsid(iunkn) * ww(iunkn)
  end do
  call norm2x(ndofn,rr,raux1)
  
  if( raux1 /= 0.0_rp ) then
     xnorm = raux2 / raux1
  else
     xnorm = 0.0_rp
  end if
  !
  ! Deallocate memory
  !
  call memchk(2_ip,istat,memma,'RR','resnor',rr)
  deallocate(rr,stat=istat)
  if(istat/=0) call memerr(2_ip,'RR','resnor',0_ip)
  call memchk(2_ip,istat,memma,'WW','resnor',ww)
  deallocate(ww,stat=istat)
  if(istat/=0) call memerr(2_ip,'WW','resnor',0_ip)
  
end subroutine resnor
