module def_parame

  !-----------------------------------------------------------------------
  !
  !     Parameters
  !
  !-----------------------------------------------------------------------  
  use def_kintyp

  ! Frequently used mathematical constants (with precision to spare):
  real(rp),     parameter :: pi     =  3.141592653589793238462643383279502884197_rp
  real(rp),     parameter :: pio2    = 1.57079632679489661923132169163975144209858_rp
  real(rp),     parameter :: twopi   = 6.283185307179586476925286766559005768394_rp
  real(rp),     parameter :: sqrt2   = 1.41421356237309504880168872420969807856967_rp
  real(rp),     parameter :: zero_rp = epsilon(0.0_rp)
  real(rp),     parameter :: kb      = 1.3806503e-23_rp

  ! Yes/No, Load/Unload and figures
  integer(ip),  parameter :: yes=1_ip,no=0_ip
  integer(ip),  parameter :: mone=-1_ip,zero=0_ip,one=1_ip,two=2_ip,three=3_ip,four=4_ip
  integer(ip),  parameter :: five=5_ip,six=6_ip,seven=7_ip,eight=8_ip,nine=9_ip,ten=10_ip
  integer(ip),  parameter :: eno= -1_ip
  integer(ip),  parameter :: load= 100_ip , unload=200_ip

  ! Null pointers
  integer(ip),  target    :: nulip(1)
  real(rp),     target    :: nulir(1)

  ! Special characters
  character(1), parameter :: wback=achar(92)

end module def_parame
