module def_master

  !-----------------------------------------------------------------------
  !****f* defmod/def_master
  ! NAME
  !   def_master
  ! DESCRIPTION
  !   This module is the header of the master
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_solver, only : solve_sol,eigen_sol

  !------------------------------------------------------------------------
  !
  ! Parameters
  !
  !------------------------------------------------------------------------

  integer(ip), parameter   :: &
       mmodu=30,              & ! Max. # of modules
       mserv=20,              & ! Max. # of services
       mblok=mmodu              ! Max. # of blocks
  !
  ! Task numbering
  !
  integer(ip), parameter   ::   &
       ITASK_REAPRO =  1,     &
       ITASK_TURNON =  2,     &
       ITASK_INIUNK =  3,     &
       ITASK_TIMSTE =  4,     &
       ITASK_BEGSTE =  5,     &
       ITASK_DOITER =  6,     &
       ITASK_CONCOU =  7,     &
       ITASK_CONBLK =  8,     &
       ITASK_NEWMSH =  9,     &
       ITASK_ENDSTE = 10,     &
       ITASK_FILTER = 11,     &
       ITASK_OUTPUT = 12,     &
       ITASK_TURNOF = 13,     &
       ITASK_BEGITE = 14,     &
       ITASK_ENDITE = 15,     &
       ITASK_MATRIX = 16,     &
       ITASK_DOOPTI = 17,     &
       ITASK_ENDOPT = 18,     &
       ITASK_BEGZON = 19,     &
       ITASK_ENDZON = 20,     &
       ITASK_BEFORE =  1,     &    
       ITASK_AFTER  =  2     
  !
  ! CPU
  !
  integer(ip), parameter    ::       &
       CPU_ASSEMBLY            = 31, &
       CPU_SOLVER              = 28, &
       CPU_EIGEN_SOLVER        = 29, &
       CPU_TOTAL_MODULE        = 30, &
       CPU_READ_DOMAIN         =  1, &
       CPU_MESH_MULTIPLICATION =  2, &
       CPU_CONSTRUCT_DOMAIN    =  3, &
       CPU_ADDTIONAL_ARRAYS    =  4
  !
  ! When to solve
  !
  integer(ip), parameter   ::  &
       AT_EACH_TIME_STEP  = 0, &
       AT_BEGINNING       = 1, &   
       AT_FIRST_TIME_STEP = 2
  !
  ! Where are we
  !
  integer(ip), parameter   :: &
       ITASK_INITIA  =  1,    &
       ITASK_ENDTIM  =  2,    &
       ITASK_ENDRUN  =  3,    &
       ITASK_ENDINN  =  4  
  !
  ! Module and service identifiers 
  !
  integer(ip), parameter   :: &
       ID_KERNEL= 0,           & ! 
       ID_NASTIN= 1,           & ! 
       ID_TEMPER= 2,           & ! 
       ID_CODIRE= 3,           & ! 
       ID_TURBUL= 4,           & ! 
       ID_EXMEDI= 5,           & ! 
       ID_NASTAL= 6,           & ! 
       ID_ALEFOR= 7,           & ! 
       ID_LATBOL= 8,           & ! 
       ID_SOLIDZ= 10,          & ! 
       ID_GOTITA= 11,          & ! 
       ID_WAVEQU= 12,          & ! 
       ID_LEVELS= 14,          & ! 
       ID_QUANTY= 15,          & ! 
       ID_PARTIS= 17,          & ! 
       ID_NASEDG= 18,          & ! 
       ID_CHEMIC= 19,          & ! 
       ID_HELMOZ= 20,          & !
       ID_IMMBOU= 21,          & !
       ID_RADIAT= 22,          & !
       ID_CASIMI= 23,          & !
       ID_POROUS= 24,          & !
       ID_PARALL= 5,           & !
       ID_DODEME= 7,           & !
       ID_COUPLI= 25,          & !
       ID_HDFPOS= 12,          & !
       ID_OPTSOL= 13,          & !
       ID_KERMOD= mmodu
  !
  ! Primary variables identifiers
  !
  integer(ip), parameter   :: &
       ID_VELOC= 1,           & ! Velocity             
       ID_PRESS= 2,           & ! Pressure             
       ID_TEMPE= 3,           & ! Temperature          
       ID_DENSI= 4,           & ! Density              
       ID_ENERG= 5,           & ! Energy               
       ID_VISCO= 6,           & ! Viscosity            
       ID_UMOME= 7,           & ! Momentum             
       ID_FMOME= 8,           & ! Fractional momentum  
       ID_UNTUR= 9,           & ! Turbulence unknowns  
       ID_UNCDR=10,           & ! CDR unknown          
       ID_ELMAG=11,           & ! Electromagnetic      
       ID_DISPM=12,           & ! Mesh displacement    
       ID_VELOM=13,           & ! Mesh velocity        
       ID_DISPL=14,           & ! Displacement         
       ID_SPINS=15,           & ! Quantic spins        
       ID_DISFU=16,           & ! Distribution funct.  
       ID_VDROP=17,           & ! Droplet velocity     
       ID_CDROP=18,           & ! Droplet concentration 
       ID_VORTI=19,           & ! Vorticity
       ID_CONCE=20,           & ! Concentration
       ID_CDENS=24,           & ! Classical density
       ID_ENTHA=25,           & ! Enthalpy
       ID_PHION=26,           & ! Phi
       ID_FIBER=27,           & ! Fiber fields
       ID_FISOC=29,           & ! Isochrones nodal field
       ID_GPFIB=31,           & ! Fiber fields in the deformed configuration (elementary)
       ID_RADSO=32,           & ! Heat radiation source term 
       ID_VCONC=33,           & ! Ionic concentrations
       ID_WASAT=34              ! Water Saturation for Porous Flow
  !
  ! Things to do
  !
  integer(ip), parameter   :: &
       WRITE_RESTART_FILE= 2, & ! Restart             
       READ_RESTART_FILE=  1    ! Restart             
  !
  ! Time step and iteration labels
  !
  integer(ip), parameter   :: &
       DT_PHYSICAL                  =   1, & ! 
       DT_PSEUDO                    =   2, & ! 
       ITER_K                       =   1, & ! 
       ITER_AUX                     =   2, & ! useful when coupling modules
       TIME_N                       =   3, & ! 
       TIME_N_MINUS_1               =   4, & ! 
       TIME_N_MINUS_2               =   5     


  !
  ! Input/Output units
  !
  integer(ip)              :: &   
       lun_pdata,             & ! Data file unit
       lun_outpu,             & ! Outlout (log) file unit
       lun_conve,             & ! Convergence file unit
       lun_rstar,             & ! Restart file unit
       lun_rstib,             & ! IB restart file unit
       lun_latex,             & ! Latex file unit: text
       lun_gnupl,             & ! Latex file unit: gnuplot
       lun_syste,             & ! System file unit
       lun_tempo,             & ! Temporary file unit
       lun_commu,             & ! Communication with Alya
       lun_binar,             & ! Geometry binary file
       lun_postp,             & ! Postprocess domain file unit
       lun_posvx,             & ! Postprocess Voxel file unit
       lun_posbs,             & ! Postprocess set file unit
       lun_pospl,             & ! Postprocess plane file unit
       lun_pos00,             & ! Additional output file (VU): Domain 
       lun_pos01,             & ! Additional output file (VU): Domain 
       lun_pos02,             & ! Additional output file (VU): Domain  
       lun_quali,             & ! Mesh quality
       lun_pos04,             & ! Additional output file (VU): Set  
       lun_pos05,             & ! Additional output file (VU): Set  
       lun_pos06,             & ! Additional output file (VU): Plane 
       lun_pos09,             & ! Additional output file (VU): Filter   
       lun_pos10,             & ! Additional output file (VU): Filter  
       lun_rstla,             & ! Lagragian particles: restart file
       lun_posla,             & ! Lagragian particles: positions
       lun_cvgla,             & ! Lagragian particles: convergence
       lun_mshib,             & ! IB mesh
       lun_resib,             & ! IB result
       lun_mshi2,             & ! IB mesh (2)
       lun_resi2,             & ! IB result (2)
       lun_detec                ! Automatic detection unit
  character(150)           :: &
       fil_pdata,             &
       fil_pos00,             &
       fil_pos01,             &
       fil_pos02,             &
       fil_quali,             &
       fil_pos04,             &
       fil_pos05,             &
       fil_pos06,             &
       fil_pos07,             &
       fil_pos08,             &
       fil_pos09,             &
       fil_pos10,             &
       fil_mshib,             & ! IB mesh
       fil_resib,             & ! IB result
       fil_mshi2,             & ! IB mesh (2)
       fil_resi2                ! IB result (2)
  !
  ! Others
  !
  real(rp),   parameter    :: &
       zeror = epsilon(1.0_rp)

  !------------------------------------------------------------------------
  !
  ! Run data: read in rrudat
  !
  !------------------------------------------------------------------------

  integer(ip)              :: &
       current_code,          &      ! Current code treated (my code number!)
       kfl_custo,             &      ! Customer
       kfl_examp,             &      ! Predefined examples
       kfl_preli,             &      ! Problem is preliminary
       nprit,                 &      ! Preliminary output time frequency
       kfl_rstar,             &      ! Problem is a restart
       kfl_rsfil,             &      ! Type of restart file name
       kfl_commu,             &      ! Communication with Alya
       kfl_outfo,             &      ! Output format
       kfl_latex,             &      ! Information file in Latex format 
       kfl_memor,             &      ! Write memory allocation flag
       kfl_timin,             &      ! Timing
       kfl_lotme,             &      ! We have a lot of memory
       kfl_freme,             &      ! Master deallocates mesh-related memory in memgeo
       kfl_outpu,             &      ! Log file
       kfl_vtk                       ! VTK output format way   
  integer(ip)              :: &
       lun_livei
  real(rp)                 :: &
       cpu_limit                     ! CPU limit defined by the user
  character(66)            :: &
       title                         ! Problem title

  !------------------------------------------------------------------------
  !
  ! Problem data: read in readat
  !
  !------------------------------------------------------------------------

  integer(ip)              :: &   
       micou(mblok),          &      ! Max. # of global iter. for each block
       nblok,                 &      ! Number of blocks
       kfl_timco,             &      ! Time coupling strategy
       kfl_timei,             &      ! If there exist a transient problem
       kfl_timef,             &      ! Time step function
       mitim,                 &      ! Maximum number of steps
       mitsm,                 &      ! Mesh refinement: max. # of smoothing iter.
       mitrf,                 &      ! Mesh refinement: max. # of refinement iter.
       kfl_algor_msh,         &      ! Mesh refinement: algorithm
       kfl_error_msh,         &      ! Mesh refinement: error estimation type
       kfl_gover_msh,         &      ! Mesh refinement: governing module
       kfl_block_msh,         &      ! Mesh refinement: Mesh after block
       kfl_outpu_par,         &      ! Parall service output flag
       kfl_postp_par,         &      ! Parall postprocess type flag
       kfl_wwork,             &      ! Who works
       kfl_lumped,            &      ! Lumped evolution matrix
       kfl_dtfun,             &      ! Time step defined as a piecewise function
       nfundt                        ! Number of DeltaT in case of piecewise function for Time step
 
  real(rp)                 :: &
       timei,                 &      ! Initial time
       timef,                 &      ! Final time
       dtime,                 &      ! Time step size dt
       toler_msh                     ! Mesh refinement 
  
  real(rp), pointer       ::  &
       dtfun(:,:)                      ! Vector to allocate the piecewise function for time step

  !
  ! Modules information
  !
  integer(ip)              ::      &
       kfl_modul(0:mmodu),         & ! Existence of module
       kfl_coupl(0:mmodu,0:mmodu), & ! Coupling of modules
       kfl_cowhe(0:mmodu,0:mmodu), & ! Field to couple modules
       lmord(mmodu,mblok),         & ! Order of modules iterations
       kfl_delay(mmodu),           & ! Delay module
       kfl_conve(mmodu),           & ! Convergence required
       kfl_solve(0:mmodu),         & ! When to be solved
       kfl_itask(20,0:mmodu),      & ! Task have been already carried out
       lzone(0:mmodu),             & ! Liste of zones
       ndela(mmodu)                  ! Steps to delay module
  !
  ! Reset system: -1 off, 0 on, 1 on and requested
  ! reset_factor determines how much more than the critical time is allowed
  !
  integer(ip)              :: &
       kfl_reset
  real(rp)                 :: &
       reset_factor
  !
  ! Service information
  ! PARALL: 55
  ! 
  integer(ip)              :: &
       kfl_servi(mserv)              ! Existence of a service

  !------------------------------------------------------------------------
  !
  ! Variables read by one module and shared by others
  !
  !------------------------------------------------------------------------

  integer(ip)              :: &
       kfl_coibm,             &      ! Immbou coupling: Read by IMMBOU. Shared by kernel
       kfl_cofor,             &      ! Immbou coupling: Read by IMMBOU. Shared by kernel
       kfl_advec,             &      ! Mesh advection: Read by IMMBOU
       kfl_async,             &      ! (A)synchronous communications: Read by PARALL
       nzone_par                     ! Zone-wise partition

  ! Two terms for chemical heat source term
  type(r1p),pointer        :: &
       div_enthalpy_transport(:),  &
       chemical_heat(:),           &
       radiative_heat(:),           &
       tfles_factor(:),            &
       tfles_sensor(:),            &
       tfles_sgseff(:)

  type(r2p),pointer        :: &
       enthalpy_transport(:)

  ! tabulation structure for CFI combustion model
  type(base_cfi),pointer   :: &
       table_cfi(:)

  !------------------------------------------------------------------------
  !
  ! Modules and services
  !
  !------------------------------------------------------------------------

  integer(ip)              :: &
       modul,                 &      ! Current module
       nmodu,                 &      ! # modules used
       itinn(0:mmodu)                ! Module inner iteration
  integer(8)               :: &
       mem_modul(2,mmodu)            ! Module memory
  real(rp)                 :: &
       cpu_modul(40,mmodu),   &      ! Module CPU time
       dtc_modul(mmodu),      &      ! Module critical time
       glres(mmodu)                  ! Problem residuals
  character(6)             :: &
       namod(0:mmodu)                ! Module name
  character(3)             :: &
       exmod(0:mmodu)                ! Module extension
  integer(ip)              :: &
       servi,                 &      ! Current service
       nserv                         ! # services used
  integer(8)               :: &
       mem_servi(2,mserv)            ! Service memory
  real(rp)                 :: &
       cpu_servi(2,mserv)            ! Service CPU time
  character(6)             :: &
       naser(mserv)                  ! Service name  
  character(3)             :: &
       exser(mserv)                  ! Service extension

#ifdef MYTIMING
  real(rp)    :: time_sld_elmope, time_bcsrax                   ! Counters for timing
#endif

  !------------------------------------------------------------------------
  !
  ! Global variables
  !
  !------------------------------------------------------------------------
  !
  ! File names
  !
  character(150)           :: &
       fil_rstar,             &      ! Restart file
       fil_rstib,             &      ! IB Restart file
       fil_conve,             &      ! Convergence file
       fil_postp,             &      ! Postprocess domain file
       fil_posvx,             &      ! Postprocess voxel
       fil_posbs,             &      ! Postprocess set file
       fil_pospl,             &      ! Postprocess plane file
       fil_binar                     ! Geometry binary file
  integer(8)               :: &
       memke(2)                      ! Kernel memory
  character(5)             :: &
       wopos_pos(3)                  ! Postprocess variable name
  integer(ip)              :: &
       kfl_split_plus                ! Whether the symbol + should be used as a splitter in ecoute
  !
  ! If arrays should be computed
  !
  integer(ip)              :: &
       kfl_lface,             &      ! If list of faces is required: LFACG
       kfl_lelbf,             &      ! If list of element boundary faces is required: LELBF
       kfl_lelp2,             &      ! If extended node-element graph is needed: PELPO_2, LELPO_2
       kfl_lele2,             &      ! If extended node-element graph is needed: PELEL_2, LELEL_2
       kfl_symgr,             &      ! If symmetric graph is needed
       kfl_conma,             &      ! If consistent mass is needed
       kfl_schur,             &      ! If a Schur solver exists
       kfl_aiipr,             &      ! If a Aii preconditioner exists
       kfl_element_bin               ! If element bin is needed
  !
  ! Integer
  !
  integer(ip)              :: &
       kfl_goblk,             &      ! Block coupling converged
       kfl_gocou,             &      ! Global problem converged
       kfl_gotim,             &      ! Global problem evolving in time
       kfl_goopt,             &      ! Global problem optimization
       kfl_naked,             &      ! If there is an argument 
       kfl_paral,             &      ! -1: Sequential,0: Master,>=1: Slave
       kfl_ptask,             &      ! Partition type
       mem_alloc,             &      ! Number of memory allocations
       ittim,                 &      ! Current time step
       itti2,                 &      ! Current time step (restart not included)
       itcou,                 &      ! Current global iteration
       ittyp,                 &      ! Type of iteration
       iblok,                 &      ! Current block
       iitrf,                 &      ! Current refinement iteration
       nusol,                 &      ! Memory variable
       mxdof,                 &      ! Memory variable
       ioutp(50),             &      ! Integer parameters for output
       nturb,                 &      ! Number of turbulence variables
       ITASK_CURREN                  ! Current task
  !
  ! Current CODE/ZONE/SUBDOMAIN
  !
  integer(ip)              :: &
       current_zone,          &      ! Current zone treated
       current_subd,          &      ! Current subdomain       
       current_color                 ! Current color
  !
  ! Save variables
  !
  integer(ip)              :: &
       lun_postp_old,         &      ! Old postprocess unit
       kfl_outfo_old                 ! Old postprocess format
  !
  ! ADR equation
  !
  integer(ip)              :: & 
       kfl_timei_adr,         &      ! ADR eqn: Time flag  
       kfl_advec_adr,         &      ! ADR eqn: Advection flag
       kfl_diffu_adr,         &      ! ADR eqn: Diffusion flag
       kfl_react_adr,         &      ! ADR eqn: Reaction flag
       kfl_grdif_adr,         &      ! ADR eqn: Diffusion gradient flag
       kfl_tisch_adr,         &      ! ADR eqn: Time scheme flag
       kfl_taust_adr,         &      ! ADR eqn: Stab. strategy
       kfl_sgsti_adr,         &      ! ADR eqn: SGS tracking in time
       kfl_sgsno_adr,         &      ! ADR eqn: SGS tracking in non-linearity
       kfl_tiacc_adr,         &      ! ADR eqn: time scheme accuracy
       kfl_shock_adr,         &      ! ADR eqn: shcok capturing flag
       kfl_ellen_adr,         &      ! ADR eqn: element length strategy
       kfl_stead_adr                 ! ADR eqn: steady state flag
  real(rp)                 :: &
       bemol_adr,             &      ! ADR eqn: Integration by parts convective term
       pabdf_adr(10),         &      ! ADR eqn: BDF coefficients
       staco_adr(4),          &      ! ADR eqn: stability constants
       shock_adr,             &      ! ADR eqn: shock capturing coefficients
       safet_adr                     ! ADR eqn: safety factor
  !
  ! Postprocess
  !
  integer(ip), pointer     :: &
       gefil(:)                      ! Filter list
  real(rp)                 :: &
       pafil(10)                     ! Filter parameters
  integer(ip)              :: &
       ivapo,                 &      ! Postprocess variable
       kfl_filte,             &      ! Filter number in module
       kfilt,                 &      ! Filter number
       isect,                 &      ! Live output sections
       inews,                 &      ! Live output New section
       init_ti,               &      ! Inital time to be used in time maasurament with system_clock
       fini_ti,               &      ! Final time to be used in time measurament with system_clock
       cmax_ti,               &      ! Input for system_cock() function.
       crate_ti                      ! Represents the precission for system_clock() function. Integer = milisecnds // Real = microseconds
  integer(8)               :: &
       vtk_id                        ! vkt_id (double precision)
  logical(lg)              :: & 
       file_opened                   ! If an opened file was openend
  !
  ! Real
  !
  real(rp)                 :: &
       cpu_initi,             &      ! Initial CPU time
       cpu_start(4),          &      ! CPU for starting operations
       cpu_outpu,             &      ! CPU for output operations
       dtinv,                 &      ! 1/dt
       dtold(10),             &      ! dt of previous time steps
       dtinv_old(10),         &      ! 1/dt of all time steps
       cutim,                 &      ! Current time
       oltim,                 &      ! Previous time
       routp(30),             &      ! Real parameters for output
       cpu_other(30),         &      ! CPU time
       ini_tim,               &      ! Initial time to be used in time measurment with cputim
       fin_tim,               &      ! Final time to be used in time measurment with cputim
       rate_time                     ! Time rate computed with system_clock
  !
  ! Character
  !
  character(66)            :: &
       namda                         ! Data file name (naked Z)
  character(100)           :: &
       coutp(20)                     ! Character parameter for output
  !
  ! Primary variables: module unknowns 
  !
  real(rp), pointer        :: &
       veloc(:,:,:),          &      ! 1:  Velocity                NASTIN-NASTAL-POROUS  
       press(:,:),            &      ! 2:  Pressure                NASTIN-NASTAL-SOLIDZ-POROUS
       tempe(:,:),            &      ! 3:  Temperature             TEMPER-NASTAL 
       densi(:,:),            &      ! 4:  Density                 NASTAL 
       energ(:,:),            &      ! 5:  Energy                  NASTAL 
       visco(:,:),            &      ! 6:  Viscosity               NASTAL
       umome(:,:,:),          &      ! 7:  Momentum                NASTAL  
       untur(:,:,:),          &      ! 9:  Turbulence unknowns     TURBUL        
       uncdr(:,:,:),          &      ! 10: CDR unknown             CODIRE 
       elmag(:,:,:),          &      ! 11: Electromagnetic         EXMEDI-MAGNET
       dispm(:,:,:),          &      ! 12: Mesh displacement       ALEFOR, IMMBOU, SOLIDZ
       velom(:,:),            &      ! 13: Mesh velocity           ALEFOR
       displ(:,:,:),          &      ! 14: Displacement            SOLIDZ
       spins(:,:),            &      ! 15: Quantic spins          
       disfu(:,:,:),          &      ! 16: Distribution funct.     LATBOL
       vdrop(:,:,:),          &      ! 17: Droplet velocity        GOTITA
       cdrop(:,:),            &      ! 19: Droplet concentration   GOTITA
       wavam(:,:),            &      ! 20: Wave amplitude          WAVEQU
       fleve(:,:),            &      ! 21: Level set               LEVELS
       erres(:),              &      ! 22: Error estimator         ADAPTI-...
       vorti(:,:),            &      ! 23: Vorticity and relatives NASTAL-NASTIN
       conce(:,:,:),          &      ! 20: Concentration           PARTIS-NASTAL-CHEMIC
       cdens(:,:),            &      ! 24: Classical density       NASTAL
       entha(:,:),            &      ! 25: Enthalpy                NASTAL
       therm(:,:),            &      ! 26: Thermal variable        TEMPER
       phion(:,:),            &      ! 27: Wave function           QUANTY
       fiber(:,:),            &      ! 28: Fiber fields for composites  EXMEDI-SOLIDZ
       fisoc(:,:),            &      ! 29: Isochrones nodal field. fisoc(ipoin,1) has the time and fisoc(ipoin,2-3) has the deformation
       gpfib(:,:,:),          &      ! 30: Fiber fields in the deformed configuration EXMEDI-SOLIDZ
       radso(:,:)  ,          &      ! 31: Radiation Heat source term
       vconc(:,:,:)  ,        &      ! 32: Concentrations EXMEDI-SOLIDZ
       taulo(:),              &      ! 33: Local electrical wave passage time  EXMEDI - SOLIDZ
       wasat(:,:)                    ! 34: Water Saturation        POROUS

  integer(ip), pointer     :: &
       kfl_fixno_ale(:,:)            ! ALEFOR module fixity condition
  real(rp),    pointer     :: &
       bvess_ale(:,:)                ! ALEFOR fixity value
  !
  ! Generic arrays and scalars
  !
  integer(ip)              :: &
       igene,                 &      ! 1st generic integer scalar
       igen2,                 &      ! 2nd generic integer scalar
       iar3p,                 &      ! If gevec allocated
       iasca,                 &      ! If gesca allocated
       iavec,                 &      ! If gevec allocated
       iaten                         ! If geten allocated

  integer(ip), pointer     :: & 
       gisca(:),              &      ! Generic integer array
       givec(:,:),            &      ! Generic integer array
       giscp(:),              &      ! Generic integer array in postprocess
       givep(:,:)                    ! Generic integer array in postprocess
  real(rp)                 :: &
       rgene                         ! Generic real scalar
  real(rp),    pointer     :: &
       geten(:,:,:),          &      ! Generic tensor array
       gevec(:,:),            &      ! Generic vector array
       gesca(:),              &      ! Generic scalar array
       getep(:,:,:),          &      ! Generic tensor array in postprocess
       gevep(:,:),            &      ! Generic vector array in postprocess
       gescp(:)                      ! Generic scalar array in postprocess
  complex(rp), pointer     :: &
       getex(:,:,:),          &      ! Generic complex tensor array
       gevex(:,:),            &      ! Generic complex vector array
       gescx(:)                      ! Generic complex scalar array
  type(r3p),   pointer     :: &
       ger3p(:)                      ! Generic r3p type arrays
  real(rp)                 :: &
       funin(3),              &      ! Function in
       funou(3)                      ! Function out
  !
  ! Secondary variables and auxiliars
  !
  real(rp),    pointer     :: &
       turmu(:),              &      ! Turb. viscosity mu_t       TURBUL
       rhoon(:,:),            &      ! Density function           QUANTY
       forcf(:,:),            &      ! Fluid force on solid       NASTIN-SOLIDZ
       sphea(:),              &      ! Specific Heat
       kcond(:),              &      ! Keat conductivity
       wmean(:,:),            &      ! Mean molecular weight
       visck(:,:),            &      ! Species viscosity
       condk(:,:),            &      ! Species heat conductivity
       sphek(:,:),            &      ! Species specific heat
       advec(:,:,:),          &      ! Advection for convection terms
       sphec(:,:,:),          &      ! specific heat coefficients used in combination with the enthalpy equation
       massk(:,:),            &      ! Mass source term for each species
       lescl(:),              &      ! CFI model: LES closure term for RPV variance (coming from table)
       encfi(:)                      ! Enthalpy scalar for the CFI combustion model

  real(rp)                 :: &
       dpthe,                 &      ! Low-Mach: dp/dt of therm. pressure
       prthe(4),              &      ! Thermodynamics pressure (cst or initial)
       tflux ,                &      ! Low-Mach: Total heat flux=int_S k*grad(T).n ds
       epres ,                &      ! Endo pressure, trigger of initial stimuli EXMEDI - SOLIDZ
       aptim(5700),           &      ! Activation potential starting times  EXMEDI - SOLIDZ
       imass                         ! Total initial mass  for low mach

  integer(ip)              :: &
       nspec,                 &      ! Total number of species around
       kfl_ephys(20),         &      ! Electrophysiology model  EXMEDI - SOLIDZ
       kfl_prthe                     ! type of thpressure calculation

  !
  ! Chemical Species with their properties
  !
  type (typ_speci),pointer          :: &
       speci(:)                     

  !
  ! Subgrid scales of primary variables
  !
  type(r3p),   pointer     :: &
       vesgs(:)                      ! Velocity subgrid scale       NASTIN
  type(r3p),   pointer     :: &
       tesgs(:)                      ! Temperature subgrid scale    TEMPER
  type(r2p),   pointer     :: &
       cosgs(:,:)                    ! Concentration subgrid scale  CHEMIC
  type(r3p),   pointer     :: & 
       statt(:)                      ! State variables   SOLIDZ

  !
  ! Sets values
  !
  real(rp),    pointer     :: &
       veset(:,:),            &      ! Set element values
       vbset(:,:),            &      ! Set boundary values
       vnset(:,:),            &      ! Set node values
       witne(:,:)                    ! Witness point values
  !
  ! Null targets
  !
  real(rp),    target      :: &
       nul1r(1),              &      ! Null vector
       nul2r(1,1)                    ! Null vector
  integer(ip), target      :: &
       nul1i(1),              &      ! Null vector
       nul2i(1,1)                    ! Null vector
  type(r3p),   target      :: &
       nur3p(1)                      ! Null type r3p        

  !------------------------------------------------------------------------
  !
  ! KD-TREE 
  !
  !------------------------------------------------------------------------

  type netyp
     integer(ip)          :: nelem             ! Number of elements for a node
     integer(ip), pointer :: eleme(:)          ! Ids of elements
     integer(ip), pointer :: ltype(:)          ! Types of elements
  end type netyp

  !------------------------------------------------------------------------
  !
  ! Lagrangian particle, needed by Master for coupling
  !
  !------------------------------------------------------------------------

  integer(ip)               :: mlagr             ! Lagrangian particles: Max. number of particle in each subdomain
  integer(ip), parameter    :: mlapr = 10        ! Maximum number of properties in Lagrangian particles
  integer(ip)               :: nlapr             ! Current number of properties requested by modules
  integer(ip), parameter    :: mtyla = 20        ! Max # of lagrangian particle types

  type typlatyp
     integer(ip)            :: kfl_exist         ! If type exists
     integer(ip)            :: kfl_modla         ! Transport model
     integer(ip)            :: kfl_grafo         ! Gravity
     integer(ip)            :: kfl_buofo         ! Buoyancy
     integer(ip)            :: kfl_drafo         ! Drag force model
     integer(ip)            :: kfl_brown         ! Brownian force
     integer(ip)            :: kfl_extfo         ! External force
     integer(ip)            :: kfl_saffm         ! Saffman force
     integer(ip)            :: kfl_analy         ! Analytical approx or numerical approx
     real(rp)               :: denpa             ! Density particle
     real(rp)               :: spher             ! Particle sphericity
     real(rp)               :: diame             ! Particle diameter
     real(rp)               :: calor             ! Calorific capacity
     real(rp)               :: emisi             ! Particle radiation emissivity
     real(rp)               :: scatt             ! Particle radiation scattering factor
     real(rp)               :: diffu             ! Particle diffusion coefficient [m^2/s]
     real(rp)               :: prope(mlapr)      ! Default value of properties for each type
     integer(ip)            :: prova(mlapr)      ! ID of variable in properties vector
  end type typlatyp
  type latyp
     integer(ip)          :: ilagr             ! Absolute ID
     integer(ip)          :: itype             ! Type of particle
     integer(ip)          :: kfl_exist         ! If I have it
     integer(ip)          :: ielem             ! Last interpolation element
     integer(ip)          :: ittim             ! Time iteration
     real(rp)             :: coord(3)          ! Coordinates
     real(rp)             :: veloc(3)          ! Velocity
     real(rp)             :: accel(3)          ! Acceleration
     real(rp)             :: v_fluid_k(3)      ! Fluid velocity at time k
     real(rp)             :: v_fluid_km1(3)    ! Fluid velocity at time k-1
     real(rp)             :: v_fluid_km2(3)    ! Fluid velocity at time k-2
     real(rp)             :: acced(3)          ! Drag acceleration
     real(rp)             :: accee(3)          ! External acceleration
     real(rp)             :: acceg(3)          ! Gravity/buoyancy acceleration
     real(rp)             :: stret             ! Stretching factor
     real(rp)             :: t                 ! Time
     real(rp)             :: dt_k              ! Time step: t^k+1-t^k
     real(rp)             :: dt_km1            ! Time step: t^k  -t^k-1
     real(rp)             :: dt_km2            ! Time step: t^k-1-t^k-2
     real(rp)             :: dtg               ! Guessed time step
     real(rp)             :: Cd                ! Drag coefficient
     real(rp)             :: Re                ! Reynolds number
     real(rp)             :: prope(mlapr)      ! Various properties (managed by modules) 
  end type latyp
  integer(ip)             :: nlagr 
  type(latyp),    pointer :: lagrtyp(:)
  type(typlatyp)          :: parttyp(mtyla)    ! Particle types

  !------------------------------------------------------------------------
  !
  ! Postprocess and modules
  !
  !------------------------------------------------------------------------

  integer(ip)               :: kfl_reawr       ! postprocess(0)/read(1)/write(2) mode
  type(tymod),  target      :: momod(-mserv:mmodu)
  type(typos),  pointer     :: postp(:)
  type(soltyp), pointer     :: solve(:), solad(:)
  type(eigtyp), pointer     :: eigeg(:)

  !------------------------------------------------------------------------
  !
  ! Algebraic system
  !
  !------------------------------------------------------------------------

  real(rp),    pointer     :: &
       unkno(:),              &      ! Working array for inner iter.
       eigen(:),              &      ! Eigen vectors
       eigva(:),              &      ! Eigen values
       amatr(:),              &      ! System matrix
       bmatr(:),              &      ! System RHS matrix  
       rhsid(:),              &      ! Right-hand-side
       cmass(:),              &      ! Consistent mass matrix
       pmatr(:),              &      ! Preconditionner
       pschu(:),              &      ! Schur preconditioner
       aii(:),                &      ! Aii matrix
       aib(:),                &      ! Aib matrix
       abi(:),                &      ! Abi matrix
       abb(:),                &      ! Abb matrix
       xxi(:),                &      ! Ui 
       xxb(:),                &      ! Ub 
       bbi(:),                &      ! bi 
       bbb(:),                &      ! bb
       damatr(:),             &      ! Differentiated System matrix  
       drhsid(:),             &      ! Differentiated Right-hand-side
       aunkno(:),             &      ! Adjoint Unknown
       lumma(:)                      ! lumped mass matrix for dual time stepping
  
  complex(rp), pointer     :: & 
       amatx(:),              &      ! System matrix
       rhsix(:),              &      ! RHS
       unknx(:),              &      ! Unknown
       pmatx(:),              &      ! Preconditionner
       damatx(:),             &      ! Differentiated System matrix  
       drhsix(:),             &      ! Differentiated Right-hand-side
       aunknx(:)                     ! Adjoint Unknown

  !------------------------------------------------------------------------
  !
  ! Parall service
  !
  !------------------------------------------------------------------------

  integer(ip), parameter    :: &
       NELEM_INTE_1DIM = 111,  &
       NELEM_INTE_2DIM = 112,  &
       NELEM_REAL_1DIM = 121,  &
       NELEM_REAL_2DIM = 122,  &
       NELEM_REAL_3DIM = 123,  &
       NBOUN_INTE_1DIM = 211,  &
       NBOUN_INTE_2DIM = 212,  &
       NBOUN_REAL_2DIM = 222,  &
       NBOUN_REAL_3DIM = 223,  &
       NPOIN_INTE_1DIM = 311,  &
       NPOIN_INTE_2DIM = 312,  &
       NPOIN_REAL_1DIM = 321,  &
       NPOIN_REAL_2DIM = 322,  &
       NPOIN_REAL_3DIM = 323,  &
       NPOIN_REAL_12DI = 351,  &
       NBOPO_INTE_1DIM = 411,  &
       NBOPO_REAL_2DIM = 422,  &
       NBOPO_REAL_3DIM = 423,  &
       NELEM_TYPE      =   1,  &
       NBOUN_TYPE      =   2,  &
       NPOIN_TYPE      =   3,  &
       NBOPO_TYPE      =   4,  &
       NFACE_TYPE      =   5,  &
       ITASK_BROADCAST =   2,  &
       ITASK_SEND      =   3,  &
       ITASK_RECEIVE   =   4,  &
       ITASK_MINIMUM   =   5,  &
       ITASK_SUM       =   9,  &
       ITASK_MAXIMUM   =   10, &
       ITASK_GATHER    =  300

  integer(ip)              :: &
       npari,                 &      ! parin buffer size
       npasi,                 &      ! parin buffer size (paris)
       npasr,                 &      ! parin buffer size (parrs)
       nparr,                 &      ! parre buffer size
       nparc,                 &      ! parch buffer size
       nparx,                 &      ! parcx buffer size
       party,                 &      ! array type (e=1,b=2,n=3)
       pardi,                 &      ! array dimension (1,2,3)
       parki,                 &      ! array kind (i=1,r=2,c=3)
       pard1,                 &      ! Dimension 1
       pard2,                 &      ! Dimension 2
       pard3,                 &      ! Dimension 3
       pard4,                 &      ! Dimension 4
       parii,                 &      ! Counter
       npoi1,                 &      ! Internal nodes end node
       npoi2,                 &      ! Own-boundary start node
       npoi3,                 &      ! Own-boundary end node
       npoi4,                 &      ! Own-boundary end node (if additional nodes should be considered)
       gni,                   &      ! Global number of internal nodes
       gnb,                   &      ! Global number of boundary nodes
       lni,                   &      ! Local number of internal nodes
       lnb,                   &      ! Local number of boundary nodes
       icoml,                 &      ! Communication level
       npart,                 &      
       nedgg,                 &      ! Number of edges (mesh subdivision)
       nfacg,                 &      ! Number of faces (mesh subdivision)
       nedgb,                 &      ! Number of boundary edges (mesh subdivision)
       nfacb,                 &      ! Number of face edges     (mesh subdivision)
       kfl_desti_par                 ! Sender/receiver destination

  integer(ip), pointer     :: &
       parin(:),              &      ! Working integer array
       pari1(:),              &      ! Working integer array
       pari2(:,:),            &      ! Working integer array
       pari3(:,:,:),          &      ! Working integer array
       pari4(:),              &      ! Working integer array
       paris(:),              &      ! Working integer array (to send)
       parig(:),              &      ! Working integer array for gather and gatherv
       lnbin(:),              &      ! Working integer array
       lgpar(:),              &      ! Local node to global group 
       lnwit(:),              &      ! Witness nodes
       ledgg(:,:),            &      ! List of edges          (mesh subdivision)
       lfacg(:,:),            &      ! List of edges          (mesh subdivision)
       ledgb(:,:),            &      ! List of boundary edges (mesh subdivision)
       lfacb(:,:),            &      ! List of boundary faces (mesh subdivision)
       ledgp(:),              &      ! List of boundary edges (mesh subdivision)
       pedgp(:),              &      ! List of boundary faces (mesh subdivision)
       nelem_tot(:),          &      ! Number of elements
       npoin_tot(:),          &      ! Number of interior+own nodes
       nboun_tot(:),          &      ! Number of boundaries
       npoia_tot(:),          &      ! Number of all nodes
       npoin_par(:),          &      ! npoin
       nelem_par(:),          &      ! nelem
       nboun_par(:),          &      ! nboun
       lninv_loc(:),          &      ! inverse perm of nodes (1..npoin_total)
       leinv_loc(:),          &      ! global element numbering
       lpoi4(:)                      ! List of node to be added to scalar product
  integer(4)               :: &
       sendcount,             &      ! Arrays to be used by AllGatherv
       recvcount                     ! Arrays to be used by AllGatherv
  integer(ip), pointer     :: &
       recvbuf_ip(:),         &      ! Arrays to be used by AllGatherv
       sendbuf_ip(:)                 ! Arrays to be used by AllGatherv
  real(rp),    pointer     :: &
       recvbuf_rp(:),         &      ! Arrays to be used by AllGatherv
       sendbuf_rp(:)                 ! Arrays to be used by AllGatherv       
  integer(4),  pointer     :: &
       displs(:),             &      ! Arrays to be used by AllGatherv
       recvcounts(:)                 ! Arrays to be used by AllGatherv
  !
  ! Renumbering graphs using METIS
  !
  integer(ip)              :: &
       nnren_par                     ! Size of graph
  integer(ip), pointer     :: &
       iaren_par(:),          &      ! Renumbering graph IA
       jaren_par(:),          &      ! Renumbering graph JA
       permr_par(:),          &      ! Renumbering graph permutation array
       invpr_par(:)                  ! Renumbering graph inv. permutation array  
  !
  ! Others
  !
  type(i1pp),  pointer     :: &
       lelbf(:)                      ! List of element boundary faces 
  type(i1p),   pointer     :: &
       lelfa(:)                      ! Element faces list
  real(rp),    pointer     :: &
       parre(:),              &      ! Working real array
       parr1(:),              &      ! Working real array
       parrs(:),              &      ! Working real array (to send)
       parr2(:,:),            &      ! Working real array
       parr3(:,:,:)                  ! Working real array
  complex(rp), pointer     :: &     
       parcx(:),              &      ! Working complex array
       parx1(:),              &      ! Working complex array
       parx2(:,:),            &      ! Working complex array
       parx3(:,:,:)                  ! Working complex array

  type(r3p),   pointer     :: &
       par3p(:)                      ! Working r3p array
  type(i1p),   pointer     :: &
       pai1p(:)                      ! Working i1p array

  character(10000)          :: &
       parch                         ! Working character array

  character(40)            :: &
       strre,                 &      ! Name of real variable
       strin,                 &      ! Name of integer variable
       strch,                 &      ! Name of character variable
       strcx                         ! Name of complex variable

  logical(lg)              :: &
       ISLAVE,                &      ! I am a slave        (kfl_paral> 0)
       IMASTER,               &      ! I am the master     (kfl_paral= 0)
       ISEQUEN,               &      ! I am sequential     (kfl_paral=-1)
       IPARALL,               &      ! I am a parallel, master or slave     (kfl_paral>=0)
       INOTMASTER,            &      ! I am not the master (kfl_paral/=0)
       INOTSLAVE,              &       ! I am not a slave    (kfl_paral<=0)
       IPARSLAVE                     ! I am a partition pre process slave ()

  !------------------------------------------------------------------------
  !
  ! Dodeme service
  !
  !------------------------------------------------------------------------

  integer(ip)              :: &
       ivari_dod,             &      ! Current interpolation variable
       nzdom_dod,             &      ! Graph numbering
       ninte_dod,             &      ! Number of integral interfaces
       ipoin_dod                     ! IPOIN
  real(rp)                 :: &
       bemol_dod                     ! Robin constant bemol (for Robin bc)
  integer(ip), pointer     :: &
       kfl_fixno_dod(:),      &      ! Fixity
       lnsub_dod(:,:),        &      ! List of node subdomains
       lbsub_dod(:,:)                ! List of boundary subdomains

  !------------------------------------------------------------------------
  !
  ! HDFPOS service
  !
  !------------------------------------------------------------------------

  real(rp),      pointer   :: &
       gesca_hdf(:),          &     ! Scalar
       gevec_hdf(:,:)               ! Vector
  type(r3p),     pointer   :: &
       ger3p_hdf(:)                 ! r3p
  character(5)             :: &
       wopos_hdf(2)                 ! Variable name

  !------------------------------------------------------------------------
  !
  ! IMMBOU module 
  !
  !------------------------------------------------------------------------

  type ibtyp

     integer(ip)          :: npoib         ! Number of boundary nodes
     integer(ip)          :: nboib         ! Number of boundary elements
     integer(ip)          :: npoin         ! Number of nodes
     integer(ip)          :: nelem         ! Number of elements
     integer(ip)          :: kfl_insid     ! =1 if particle inside
     integer(ip)          :: kfl_typeb     ! =0 if embedded particle. -1 for volume type. >0 for body fitted (set number)
     integer(ip)          :: kfl_coupl     ! =0 if fringe. =1 of volume.
     integer(ip)          :: kfl_model     ! =0 no specific model

     real(rp),    pointer :: cooin(:,:)    ! Initial coordinates
     real(rp),    pointer :: cooib(:,:)    ! Coordinates
     real(rp),    pointer :: cooi2(:,:)    ! Coordinates
     integer(ip), pointer :: lnoib(:,:)    ! Boundaries
     integer(ip), pointer :: ltyib(:)      ! Boundary types
     integer(ip), pointer :: lninv(:)      ! Permutation for nodes
     integer(ip), pointer :: lbinv(:)      ! Permutation for boundaries

     real(rp),    pointer :: coord(:,:)    ! Coordinates
     integer(ip), pointer :: lnods(:,:)    ! Element connectivity
     integer(ip), pointer :: ltype(:)      ! Element types

     real(rp)             :: bobox(3,2)    ! Particle boundaring box
     real(rp), pointer    :: fabox(:,:,:)  ! Face boundaring boxes
     real(rp)             :: massa         ! Mass
     real(rp)             :: densi         ! Density
     real(rp)             :: volum         ! Volume
     real(rp)             :: momin(6)      ! Momentum of inertia tensor
     real(rp)             :: posgr(3)      ! Initial position of center of gravity
     !
     ! Linear motion
     !
     real(rp)             :: posil(3,2)    ! Linear position
     real(rp)             :: velol(3,2)    ! Linear velocity
     real(rp)             :: accel(3,3)    ! Linear accelaration
     real(rp)             :: force(3,2)    ! Force
     real(rp)             :: pforce(3)     ! Pressure Force
     real(rp)             :: vforce(3)     ! Viscous Force
     !
     ! Angular motion
     !
     real(rp)             :: posia(3,2)    ! Angular position
     real(rp)             :: veloa(3,2)    ! Angular velocity
     real(rp)             :: accea(3,3)    ! Angular accelaration
     real(rp)             :: rotac(3,3)    ! Rotation matrix
     real(rp)             :: torqu(3,2)    ! Torque
     real(rp)             :: ptorqu(3)     ! Pressure Torque     
     real(rp)             :: vtorqu(3)     ! Viscous Torque     
     real(rp)             :: quate(4,2)    ! quaternion (an alternative representation of the rotation matrix)

     real(rp),    pointer :: sabox(:,:,:)  ! Tree structure of face bounding boxes
     integer(ip), pointer :: blink(:)      ! Index of faces for tree structure 
     integer(ip), pointer :: struc(:)      ! Structure use by tree search     
     real(rp),    pointer :: ldist(:)      ! Structure use by tree search     

     integer(ip)          :: npofb         ! Number of boundaries build with fringes nodes (fringe boundary)
     integer(ip)          :: nbofb         ! Number of boundary nodes build with fringes nodes (fringe boundary)

     integer(ip), pointer :: lnofb(:,:)    ! List of boundaries build with fringes nodes (fringe boundary)
     integer(ip), pointer :: ltyfb(:)      ! List of boundary types build with fringes nodes (fringe boundary)
     integer(ip), pointer :: lelfb(:)      ! List of father elements for the boundaries build with fringes nodes (fringe boundary)

     real(rp)             :: maxdi         ! Maximum distance in the particle measure from his center of gravity
     real(rp)             :: cotim         ! Estimated time of collision

     type(netyp), pointer :: lnele(:)      ! List of elements with a common node    

     integer(ip)          :: cucon         ! Current contact
     integer(ip), pointer :: icont(:)      ! Index of contacts
     real   (rp), pointer :: ncont(:,:)    ! Normal contact
     real   (rp), pointer :: rcont(:,:)    ! Vector from the mass center to the contact point

     integer(ip)          :: npari         ! Number of int. model parameters 
     integer(ip)          :: nparr         ! Number of real model parameters 
     integer(ip), pointer :: ipara(:)      ! Int. parameters
     real(rp),    pointer :: rpara(:)      ! Real parameters     
     !
     ! Conservation mass matrix variables
     !
     real   (rp)          :: mass1         
     real   (rp)          :: mass2         
  end type ibtyp

  !------------------------------------------------------------------------
  !
  ! Rigid body module  - similar to ibtyp but reduced - for rigid body inside ale - 
  ! I do it this way in case I have more than 1 rigid body and also for consistency with cristo
  !
  !------------------------------------------------------------------------

  type rbtyp

     integer(ip)          :: npoib         ! Number of boundary nodes
     integer(ip)          :: nboib         ! Number of boundary elements

     integer(ip)          :: nrbse         ! Number of sets the rigid body is formed by
     integer(ip)          :: lrbse(10)     ! List of sets the rigid body is formed by

     real(rp),    pointer :: cooin(:,:)    ! Initial coordinates
     real(rp),    pointer :: cooib(:,:)    ! Coordinates
     integer(ip), pointer :: lnoib(:,:)    ! Boundaries
     integer(ip), pointer :: ltyib(:)      ! Boundary types
     integer(ip), pointer :: lninv(:)      ! Permutation for nodes
     integer(ip), pointer :: lbinv(:)      ! Permutation for boundaries

     real(rp)             :: massa         ! Mass
     real(rp)             :: densi         ! Density
     real(rp)             :: volum         ! Volume
     real(rp)             :: momin(6)      ! Momentum of inertia tensor
     real(rp)             :: posgr(3)      ! Initial position of center of gravity
     !
     ! Linear motion
     !
     real(rp)             :: posil(3,4)    ! Linear position
     real(rp)             :: velol(3,4)    ! Linear velocity
     real(rp)             :: accel(3,4)    ! Linear accelaration
     real(rp)             :: force(3,4)    ! Force
     real(rp)             :: vpfor(3,4)    ! Viscous + pressure Force
     real(rp)             :: pforce(3)     ! Pressure Force
     real(rp)             :: vforce(3)     ! Viscous Force     
     real(rp)             :: pp_pf(3,10)   ! Pressure Force for post process
     real(rp)             :: pp_vf(3,10)   ! Viscous Force for post process
     !
     ! Angular motion
     !
     real(rp)             :: posia(3,4)    ! Angular position
     real(rp)             :: veloa(3,4)    ! Angular velocity
     real(rp)             :: accea(3,4)    ! Angular accelaration
     real(rp)             :: rotac(3,4)    ! Rotation matrix
     real(rp)             :: torqu(3,4)    ! Torque
     real(rp)             :: vptor(3,4)    ! Viscous + pressure Torque
     real(rp)             :: ptorqu(3)     ! Pressure Torque     
     real(rp)             :: vtorqu(3)     ! Viscous Torque     
     real(rp)             :: pp_pt(3,10)   ! Pressure Torque for post process
     real(rp)             :: pp_vt(3,10)   ! Viscous Torque for post process
     real(rp)             :: quate(4,4)    ! quaternion (an alternative representation of the rotation matrix)
     real(rp)             :: q_dot(4,4)    ! time derivative of the quaternion

  end type rbtyp


  type ibint
     integer(ip), pointer :: lnode(:)
     real(rp),    pointer :: shapl(:)
     integer(ip)          :: limit   
  end type ibint

  type(ibtyp), pointer    :: imbou(:)      ! IB properties
  type(rbtyp), pointer    :: rbbou(:)      ! Rigid body properties
  type(ibint), pointer    :: lnint(:)      ! Interpolation
  type(ibint), pointer    :: lnin2(:)      ! Interpolation

  integer(ip), pointer    :: lntib(:)      ! Node types
  integer(ip), pointer    :: lnti2(:)      ! Node types (previous time stpe)
  integer(ip), pointer    :: lndom(:)      ! Save graph
  integer(ip), pointer    :: lntra(:)      ! List of travesties nodes
  integer(ip), pointer    :: letib(:)      ! Element types
  real(rp),    pointer    :: massc(:,:)    ! Mass conservation matrices (1st column: diagonal mass matrix, 2nd - 4th column: restriccions )

  !------------------------------------------------------------------------
  !
  ! OPTSOL service
  !
  !------------------------------------------------------------------------
  integer(ip)              :: &
       kfl_method_opt,        &  ! optimization method: adjoint
       kfl_maxstp_opt,        &  ! max number of optimization steps
       kfl_maxlin_opt,        &  ! max number of line-search steps
       kfl_maxbar_opt,        &  ! max number of barrier steps
       kfl_maxres_opt,        &  ! max number of restarts for line-search
       kfl_curstp_opt,        &  ! current optimization step
       kfl_curlin_opt,        &  ! current lineasearch step
       kfl_curbar_opt,        &  ! current barrier step
       kfl_curres_opt,        &  ! current number of restarts for line-search
       kfl_first_opt,         &  ! equal to 1 if this is the first opt.iteration, in other case, equal to 0 
       kfl_scheme_opt,        &  ! id number for the set/get scheme
       kfl_constr_opt,        &  ! 1=constraints satisfied, 0=not satisfied 
       kfl_ndvars_opt            ! number of design vars      
  real(rp)              :: &
       kfl_conver_opt,      &  ! tolerance (if grad(cost funct.)<tolerance: exit)
       kfl_conbar_opt,      &  ! tolerance of barrier method
       kfl_scale_opt,       &  ! scale factor percentage
       kfl_steplength_opt      ! initial step length

  integer(ip), pointer   :: &
       diffj_isInside(:)        ! 0 or 1, depending on specific region limits

  real(rp), pointer      :: &
       diffj(:),            &  ! derivative of J wrt design variables (all shots)   
       diffj_shot(:),       &  ! derivative of J wrt design variables (1 shot)   
       diffj_prev(:),       &  ! derivative of J wrt design variables in previous point
       diffj_illum(:),      &  ! illumination factor (preconditioner of the reduced grad)   
       descdir(:),          &  ! descent direction    
       descdir_prev(:)         ! descent direction in previous point

  real(rp)               :: &
       stepj,               &  ! step for line-search using diffj
       stepj_prev              ! step for line-search using diffj in previous iteration

  real(rp), pointer      :: dcostr(:)       ! derivative of J wrt state  variables (real)   
  complex(rp), pointer   :: dcostx(:)       ! derivative of J wrt state  variables (complex)   
  real(rp), pointer      :: modulo_dcost(:)

  real(rp)               :: &
       costf,               &  ! cost function value (1 shot)
       costf_shot,          &  ! cost function value (all shots)
       costf_prev,          &  ! cost function value in previous point
       costf_tmp               ! cost function value, temporal

  real(rp), pointer      :: &
       design_vars(:),      & ! design variable vector
       design_vars_tmp(:),  & ! design variable vector, temporal values
       design_vars_prev(:), & ! design variable vector, previous values
       design_vars_ref(:)     ! reference model for regularization in inversion

  !----------------------------------------------------------------------
  !
  ! COUPLING AND PARALLELIZATION STUFFS
  !
  !----------------------------------------------------------------------

  logical(lg), pointer   :: &
       I_AM_IN_CODE(:),     & ! Am I in code 
       I_AM_IN_ZONE(:),     & ! Am I in zone of my code
       I_AM_IN_SUBD(:)        ! Am I in subd of my code

  !----------------------------------------------------------------------
  !
  ! FUNCTIONS
  !
  !----------------------------------------------------------------------

contains

  function intost(integ)
    !-------------------------------------
    !
    !  Convert an integer(ip) to a string
    !
    !-------------------------------------
    implicit none
    integer(ip)   :: integ
    integer(4)    :: integ4
    character(20) :: intost
    character(20) :: intaux

    integ4=int(integ,4)
    write(intaux,*) integ4
    intost=adjustl(intaux)

  end function intost

  function retost(realn)
    !-------------------------------------
    !
    !  Convert a real(rp) to a string
    !
    !-------------------------------------
    implicit none
    real(rp)   :: realn
    character(20) :: retost
    character(20) :: reaux

    write(reaux,'(e19.12)') realn 
    retost=adjustl(reaux)

  end function retost

  function isanan(a)
    !-------------------------------------
    !
    !  Check if a is an NaN
    !
    !-------------------------------------
    implicit none
    real(rp),    intent(in) :: a
    logical(lg)             :: isanan
    if (a/=a) then
       isanan = .true.
    else
       isanan = .false.
    end if
    return
  end function isanan

  function isainf(a)
    !-------------------------------------
    !
    !  Check if a is a +/-Inf
    !
    !-------------------------------------
    implicit none
    real(rp),    intent(in) :: a
    logical(lg)             :: isainf

    if ((a*0.0_rp)/=0.0_rp) then
       isainf = .true.
    else
       isainf = .false.
    end if
    return

  end function isainf

  function isnain(a)
    !-------------------------------------
    !
    !  Check if a is a Nan or +/-Inf
    !
    !-------------------------------------
    implicit none
    real(rp),    intent(in) :: a
    logical(lg)             :: isnain

    if ((a*0.0_rp)/=0.0_rp.or.a/=a) then
       isnain = .true.
    else
       isnain = .false.
    end if
    return

  end function isnain

  !-------------------------------------
  !
  !  Parallelizaiton mode
  !
  !-------------------------------------

  function PART_AND_WRITE()    
    implicit none
    logical(lg) :: PART_AND_WRITE
    if( kfl_ptask == 0 ) then
       PART_AND_WRITE = .true.
    else
       PART_AND_WRITE = .false.
    end if
    return

  end function PART_AND_WRITE

  function PART_AND_RUN()
    implicit none
    logical(lg) :: PART_AND_RUN
    if( kfl_ptask == 1 ) then
       PART_AND_RUN = .true.
    else
       PART_AND_RUN = .false.
    end if
    return

  end function PART_AND_RUN

  function READ_AND_RUN()
    implicit none
    logical(lg) :: READ_AND_RUN
    if( kfl_ptask == 2 ) then
       READ_AND_RUN = .true.
    else
       READ_AND_RUN = .false.
    end if
    return

  end function READ_AND_RUN

  function THIS_NODE_IS_MINE(ipoin)
    implicit none
    integer(ip), intent(in) :: ipoin
    logical(lg)             :: THIS_NODE_IS_MINE
    if( ipoin <= npoi1 .or. ( ipoin >= npoi2 .and. ipoin <= npoi3 ) ) then
       THIS_NODE_IS_MINE = .true.
    else
       THIS_NODE_IS_MINE = .false.
    end if
    return

  end function THIS_NODE_IS_MINE

  function coupling(wnam1,wnam2)
    !-------------------------------------
    !
    !  Give the coupling function between modules
    !
    !-------------------------------------
    implicit none
    integer(ip)              :: coupling
    character(*), intent(in) :: wnam1
    character(*), intent(in) :: wnam2
    integer(ip)              :: imodu,jmodu

    imodu = idmod(wnam1)
    jmodu = idmod(wnam2)
    if( jmodu == -1 .or. jmodu == -1 ) call runend('WRONG MODULES FOR COUPLING')
    coupling = kfl_coupl(imodu,jmodu)

  end function coupling

  function idmod(wname)
    !-------------------------------------
    !
    !  Convert a module name into its ID
    !
    !-------------------------------------
    implicit none
    integer(ip)              :: idmod
    character(*), intent(in) :: wname
    integer(ip)              :: kmodu

    kmodu = -1
    idmod = -1
    do while( kmodu < mmodu )
       kmodu = kmodu + 1
       if( wname(1:5) == namod(kmodu)(1:5) ) then
          idmod = kmodu
          kmodu = mmodu
       end if
    end do

  end function idmod

end module def_master
