module def_elmope
  !-----------------------------------------------------------------------
  !****f* defmod/def_elmope
  ! NAME
  !   def_elmope
  ! DESCRIPTION
  !   This module is the header of the element operations
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only :  ip,rp
  use def_domain, only :  ndime
  implicit none
  !
  ! Working arrays
  !
  real(rp), pointer    :: wmat1(:,:,:) 
  real(rp), pointer    :: wmat2(:,:,:) 
  real(rp), pointer    :: d2sdx(:,:,:) 
  real(rp), pointer    :: xjaci(:,:) 
  real(rp), pointer    :: xjacm(:,:) 
  !
  ! Element matrix and RHS
  !
  real(rp), pointer    :: elmat(:,:) 
  real(rp), pointer    :: elrhs(:)
  real(rp), pointer    :: wmatr(:,:)
  real(rp), pointer    :: wrhsi(:)

contains 

  subroutine allelm(dim)
    !---------------------------------------------------------------------
    !
    ! Allocate memory for element operation
    !
    !---------------------------------------------------------------------
    implicit none
    integer(ip), intent(in) :: dim
    integer(ip)             :: istat

    allocate(wmat1(ndime,ndime,dim),  stat=istat)
    allocate(wmat2(ndime,ndime,dim),  stat=istat) 
    allocate(d2sdx(ndime,ndime,ndime),stat=istat) 
    allocate(xjaci(ndime,ndime),      stat=istat)
    allocate(xjacm(ndime,ndime),      stat=istat)
    allocate(elmat(dim,dim),          stat=istat)   
    allocate(elrhs(dim),              stat=istat)
    allocate(wmatr(dim,dim),          stat=istat)
    allocate(wrhsi(dim),              stat=istat)

  end subroutine allelm

  subroutine deaelm()
    !---------------------------------------------------------------------
    !
    ! Allocate memory for element operation
    !
    !---------------------------------------------------------------------
    implicit none
    integer(ip) :: istat

    deallocate(wmat1,stat=istat)
    deallocate(wmat2,stat=istat) 
    deallocate(d2sdx,stat=istat) 
    deallocate(xjaci,stat=istat)
    deallocate(xjacm,stat=istat)
    deallocate(elmat,stat=istat)   
    deallocate(elrhs,stat=istat)
    deallocate(wmatr,stat=istat)
    deallocate(wrhsi,stat=istat)

  end subroutine deaelm

end module def_elmope
