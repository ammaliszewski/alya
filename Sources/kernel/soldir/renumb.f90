subroutine renumb(r_sol,c_sol,npoin,nzsol,nzsky,luout)

  !-----------------------------------------------------------------------
  !    
  ! This routine defines the address of the arrays LPNTN and LPONT and
  ! computes the number of components in the skyline matrix NZSKY      
  !
  !-----------------------------------------------------------------------
  use def_parame
  use def_solver
  use mod_memchk  
  implicit none
  integer(ip), intent(in)  :: npoin,nzsol,nzsky
  integer(ip), intent(in)  :: luout
  integer(ip), intent(in)  :: r_sol(npoin+1), c_sol(nzsol)
  integer(ip)              :: nncon,leqns(2)
  integer(ip), allocatable :: lncon(:)
  integer(4)               :: istat
  !
  ! Memory allocation for LPNTN and LPONT and temporary arrays.
  !
  allocate(lpntn(npoin),stat=istat)
  call memchk(zero,istat,memdi,'LPNTN','renumb',lpntn)
  allocate(lpont(npoin),stat=istat)
  call memchk(zero,istat,memdi,'LPONT','renumb',lpont)
  nncon = (nzsol - npoin)/2 
  allocate(lncon(2*nncon),stat=istat)
  call memchk(zero,istat,memdi,'LNCON','renumb',lncon)
  !
  ! Construct the array of connectivities.
  !
  call rencon(lncon,r_sol,c_sol,npoin,nzsol,nncon)
  !
  ! Renumber strategy.
  !
  call renumn(lpntn,lncon,two,nncon,npoin,luout,memdi)
  !
  ! Construct LPONT and compute NZSKY.
  !
  call skyini(lpntn,lpont,lncon,leqns,two,nncon,npoin,nzsky,luout)
  !
  ! Deallocate memory.
  !      
  call memchk(two,istat,memdi,'LNCON','renumb',lncon)
  deallocate(lncon,stat=istat)
  if(istat/=0)  call memerr(two,'LNCON','renumb',0_ip)

end subroutine renumb
