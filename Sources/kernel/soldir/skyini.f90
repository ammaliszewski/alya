subroutine skyini(&
     lpntn,lpont,lncon,leqns,nnode,nelem,&
     npoin,nzsky,lusol)
  
!-----------------------------------------------------------------------
!
! This routine performs the skyline solver initialization when
! renumbering is employed. The output is
! 
! lpont(npoin)        -  position of the term just above the diagonal
!                        in the arrays gstup & gstlo
! nzsky               -  components of the skyline matrix      
! 
! Auxiliary working parameters
! 
! leqns(nnode)        -  auxiliary array to store the equation
!                        numbers of the current element
! 
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none
  integer(ip) :: nnode,nelem,npoin,nzsky
  integer(ip) :: lusol
  integer(ip) :: lpont(npoin), leqns(nnode)
  integer(ip) :: lpntn(npoin), lncon(nnode,nelem)
  integer(ip) :: ieqns,ielem,mneqn,inode,npont,ipoin,meanh
  real(rp)    :: hmean
!
! Find the position of the diagonal terms
!
  do ieqns=1,npoin
     lpont(ieqns)=0
  end do
  
  do ielem = 1,nelem
     mneqn = 0
     do inode = 1,nnode
        ipoin = lncon(inode,ielem)
        ieqns = lpntn(ipoin)
        if(mneqn==0) mneqn = ieqns
        mneqn = min0(mneqn,ieqns)
        leqns(inode) = ieqns
     end do
     do inode = 1,nnode
        ieqns = leqns(inode)
        npont = max0(lpont(ieqns),ieqns-mneqn)
        lpont(ieqns) = npont
     end do
  end do ! ielem = 1,nelem
!
! Compute diagonal pointers for profile and NZSKY
!
  lpont(1) = 0
  if(npoin>1) then
     do ieqns = 2,npoin
        lpont(ieqns) = lpont(ieqns) + lpont(ieqns-1)
     end do
  end if
  nzsky = npoin + 2*lpont(npoin)
!
! Write solver information
!
  hmean = real(lpont(npoin)+npoin)/real(npoin)
  meanh = nint(hmean)
  if(lusol>0) write(lusol,900) meanh,nzsky,rp
!
! Formats
!
900 format(&
         & 5x,'   MEAN HALF-WIDTH    :',i10,/&
         & 5x,'   SIZE OF THE MATRIX :',i10,' (R*',i1,')')
  
end subroutine skyini

