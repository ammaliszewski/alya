subroutine rencon(lncon,r_sol,c_sol,npoin,nzsol,nncon)
!-----------------------------------------------------------------------
!
! This routine constructs the array of nodal connectivities for the
! renumbering strategy  
!
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none
  integer(ip) :: npoin,nzsol,nncon
  integer(ip) :: r_sol(npoin+1), c_sol(nzsol), lncon(2,nncon)
  integer(ip) :: incon,ipoin,jpoin,izsol
  
  incon = 0
  do ipoin = 1,npoin
     do izsol = r_sol(ipoin),r_sol(ipoin+1)-1
        jpoin = c_sol(izsol)
        if(ipoin<jpoin) then
           incon = incon + 1
           lncon(1,incon) = ipoin
           lncon(2,incon) = jpoin
        end if
     end do
  end do
  
end subroutine rencon
