subroutine renumn(lpntn,lpont,nnode,nelem,npoin,lures,smemo)

!-----------------------------------------------------------------------
!    
! This routine sets up array LPNTN (only the nodes are renumbered)
!              
!-----------------------------------------------------------------------
  use def_parame
  use mod_memchk
  implicit none
  integer(ip), intent(in)    :: nnode,npoin,nelem,lures
  integer(8),  intent(inout) :: smemo(2)
  integer(ip), intent(in)    :: lpont(nnode,nelem)
  integer(ip)                :: lpntn(npoin)
  integer(ip)                :: lword
  integer(4)                 :: istat
  integer(ip), allocatable   :: iwork(:)           ! working array

  lword=(nnode*(nnode-1))*nelem*2+3*npoin+1

  allocate(iwork(lword),stat=istat)
  call memchk(zero,istat,smemo,'IWORK','renumn',iwork)

  call renum0(lpont,lpntn,lword,nelem,nnode,npoin,lures,iwork)

  call memchk(two,istat,smemo,'IWORK','renumn',iwork)
  deallocate(iwork,stat=istat)
  if(istat.ne.0)  call memerr(two,'IWORK','renumn',0_ip)


end subroutine renumn
