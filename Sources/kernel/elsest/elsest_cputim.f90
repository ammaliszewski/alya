subroutine elsest_cputim(rtime)
  !-----------------------------------------------------------------------
  !****f* elsest_cputim
  ! NAME
  !    nsi_elmope
  ! DESCRIPTION
  !    Returns the CPU time in seconds
  ! OUTPUT
  !    rtime
  ! USES
  ! USED BY
  !***
  !-----------------------------------------------------------------------
#ifdef _OPENMP
  use omp_lib
#endif
  use def_elsest, only   : rp
  implicit none
  real(rp), intent(out) :: rtime
  real(4)               :: rtim4,elap4(2),etime

#ifdef _OPENMP
  rtime = OMP_GET_WTIME()
#else
  rtim4 = etime(elap4)
  rtime = real(rtim4)
#endif

end subroutine elsest_cputim
