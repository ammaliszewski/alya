!----------------------------------------------------------------------
!> @addtogroup Parall
!> @{
!> @file    par_output_partition.f90
!> @author  Guillaume Houzeaux
!> @date    20/04/2014
!> @brief   Output partition statistics
!> @details Output some informaiton about the partition
!>          The files can be read with GiD
!> @} 
!----------------------------------------------------------------------

subroutine par_output_partition()
  use def_kintyp
  use def_parame
  use mod_parall
  use def_domain
  use def_master
  use def_coupli
  use mod_communications
  implicit none  
  integer(ip)                  :: ielem,ipoin
  integer(ip)                  :: ipart,idime,dummi,ineig
  integer(ip)                  :: icode,ipart_world,icoup
  integer(ip)                  :: npoin_interior,kelem,jelem
  integer(ip)                  :: npoin_boundary,ii
  integer(ip)                  :: number_wet_points,pelty
  integer(ip)                  :: npoin_wet         
  integer(ip)                  :: nboun_wet         
  integer(ip)                  :: nneig_source     
  type(comm_data_par), pointer :: commu
  integer(4)                   :: PAR_COMM_TO_USE
  real(rp)                     :: dummr,xaver,xx
  real(rp)                     :: xmini(3)
  real(rp)                     :: xmaxi(3)
  real(rp)                     :: xmini_send(3)
  real(rp)                     :: xmaxi_send(3)
  real(rp)                     :: xcoor_send(3)

  real(rp)                     :: xcoor(3)
  integer(ip)                  :: nneig
  integer(ip), pointer         :: lneig(:)
  real(rp),    pointer         :: xcoor_gat(:,:)
  integer(ip), pointer         :: nneig_gat(:)
  integer(4),  pointer         :: nneig4_gat(:)
  integer(ip), pointer         :: lneig_gat(:)
  integer(ip), pointer         :: lresu_gat(:)
  integer(ip), pointer         :: lcode_gat(:)
  integer(ip), pointer         :: ncolo_gat(:)
  type(i1p),   pointer         :: ledge(:)
  integer(ip)                  :: element_stat(21)
  integer(ip)                  :: boundary_stat(21)
  integer(ip)                  :: neighbor_stat(21)

  if( ISEQUEN ) return

  nullify( commu      )
  nullify( lneig      )
  nullify( xcoor_gat  )
  nullify( nneig_gat  )
  nullify( nneig4_gat )
  nullify( lneig_gat  )
  nullify( lresu_gat  )
  nullify( lcode_gat  )
  nullify( ncolo_gat  )
  nullify( ledge      )

  call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE,commu)

  element_stat  =  0
  boundary_stat =  0
  neighbor_stat =  0
  xmini         =  huge(1.0_rp)
  xmaxi         = -huge(1.0_rp)
  xmini_send    =  huge(1.0_rp)
  xmaxi_send    = -huge(1.0_rp)
  xcoor         =  0.0_rp
  nneig         =  0

  if( PAR_MY_WORLD_RANK == 0 ) then
     allocate( xcoor_gat (3,0:PAR_WORLD_SIZE-1) )
     allocate( nneig_gat (  0:PAR_WORLD_SIZE-1) )
     allocate( nneig4_gat(  0:PAR_WORLD_SIZE-1) )
     allocate( lresu_gat (  0:PAR_WORLD_SIZE-1) )
     allocate( lcode_gat (  0:PAR_WORLD_SIZE-1) )
     allocate( ncolo_gat (  0:PAR_WORLD_SIZE-1) )
     allocate( ledge     (  0:PAR_WORLD_SIZE-1) )
     do ipart = 0,PAR_WORLD_SIZE-1 
        nullify(ledge(ipart) % l)
     end do
  end if
  !
  ! Partition coordinates
  !
  if( INOTMASTER ) then
     do ipoin = 1,npoin
        xcoor(1:ndime) = xcoor(1:ndime) + coord(1:ndime,ipoin)
        xmini(1:ndime) = min(xmini(1:ndime),coord(1:ndime,ipoin))
        xmaxi(1:ndime) = max(xmaxi(1:ndime),coord(1:ndime,ipoin))
     end do
     xcoor(1:ndime) = xcoor(1:ndime) / real(npoin,rp)
     nneig = commu % nneig 
  end if
  !
  ! Master's coordinates
  ! 
  do icode = 1,mcode
     if( current_code == icode ) then
        xcoor_send = xcoor
     else       
        xcoor_send = 0.0_rp
     end if
     call PAR_SUM(3_ip,xcoor_send,'IN THE WORLD')
     if( IMASTER .and. icode == current_code ) then
        xcoor = xcoor_send / real(PAR_CODE_SIZE,rp)
     end if
  end do
  call PAR_GATHER(xcoor,xcoor_gat,'IN THE WORLD')
  !
  ! Number OpenMP colors
  !
  call PAR_GATHER(par_omp_num_colors,ncolo_gat,'IN THE WORLD')
  !
  ! Number and list of neighbors
  !
  call PAR_GATHER(nneig,nneig_gat,'IN THE WORLD')

  if( PAR_MY_WORLD_RANK == 0 ) then
     dummi = 0
     do ipart = 0,PAR_WORLD_SIZE-1 
        dummi = dummi + nneig_gat(ipart)
        nneig4_gat(ipart) = int(nneig_gat(ipart),4)
     end do
     allocate( lneig_gat(dummi) )
  else
     if( INOTMASTER ) then
        if( commu % nneig > 0 ) allocate( lneig(commu % nneig) )
        do ineig = 1,commu % nneig   
           ipart        = commu % neights(ineig)
           ipart_world  = par_world_rank_of_a_code_neighbor(ipart,current_code)
           lneig(ineig) = ipart_world
        end do
     end if
  end if
  call PAR_GATHERV(lneig,lneig_gat,nneig4_gat,'IN THE WORLD')
  !
  ! Code number
  !
  call PAR_GATHER(current_code,lcode_gat,'IN THE WORLD') 

  !----------------------------------------------------------------------
  !
  ! Geometry
  !
  !----------------------------------------------------------------------

  if( PAR_MY_WORLD_RANK == 0 ) then
     !
     ! Partition graph LEDGE
     !
     ielem = 0
     do ipart = 0,PAR_WORLD_SIZE-1
        if( nneig_gat(ipart) > 0 ) then
           allocate( ledge(ipart) % l(nneig_gat(ipart)) )
           do ineig = 1,nneig_gat(ipart)
              ielem = ielem + 1
              ledge(ipart) % l(ineig) = lneig_gat(ielem)
           end do
        end if
     end do
     !
     ! Coordinates (Master are also present!)
     !
     ipoin = 0
     icode = 1
     write(lun_parti_msh,'(a,i1,a)') 'MESH CODE_'//trim(intost(icode))//' dimension ',ndime,' Elemtype Linear Nnode 2'     
     write(lun_parti_msh,'(a)') 'coordinates'  
     do ipart = 0,PAR_WORLD_SIZE-1 
        ipoin = ipoin + 1
        write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,(xcoor_gat(idime,ipart),idime=1,ndime)
     end do
     write(lun_parti_msh,'(a)') 'end coordinates'
     !
     ! Elements: edges of the communciation
     !
     ielem = 0
     do icode = 1,mcode
        if( icode > 1 ) &
             write(lun_parti_msh,'(a,i1,a)') 'MESH CODE_'//trim(intost(icode))//' dimension ',ndime,' Elemtype Linear Nnode 2'
        write(lun_parti_msh,'(a)') 'elements'
        do ipart = 0,PAR_WORLD_SIZE-1 
           if( lcode_gat(ipart) == icode ) then
              do ineig = 1,nneig_gat(ipart)
                 ielem = ielem + 1
                 write(lun_parti_msh,'(3(1x,i7))') ielem,ipart+1,ledge(ipart) % l(ineig)+1
              end do              
           end if
        end do
        write(lun_parti_msh,'(a)') 'end elements'
     end do
     do ipart = 0,PAR_WORLD_SIZE-1
        if( nneig_gat(ipart) > 0 ) then
           deallocate( ledge(ipart) % l )
        end if
     end do
  end if
  !
  ! Code bounding boxes
  !
  ipoin = PAR_WORLD_SIZE
  do icode = 1,mcode
     if( current_code == icode ) then
        xmini_send = xmini
        xmaxi_send = xmaxi
     else       
        xmini_send =  huge(1.0_rp)
        xmaxi_send = -huge(1.0_rp)
     end if
     call PAR_MIN(3_ip,xmini_send,'IN THE WORLD')
     call PAR_MAX(3_ip,xmaxi_send,'IN THE WORLD')
     if( PAR_MY_WORLD_RANK == 0 ) then
        ielem = ielem + 1
        if( ndime == 2 ) then
           write(lun_parti_msh,'(a)') 'MESH CODE'//trim(intost(icode))//'_BOUNDING_BOX dimension 2 Elemtype Quadrilateral Nnode 4'       
           write(lun_parti_msh,'(a)') 'coordinates'  
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmini_send(1),xmini_send(2)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmaxi_send(1),xmini_send(2)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmaxi_send(1),xmaxi_send(2)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmini_send(1),xmaxi_send(2)
           write(lun_parti_msh,'(a)') 'end coordinates' 
           write(lun_parti_msh,'(a)') 'elements'  
           write(lun_parti_msh,'(5(1x,i7))') ielem,ipoin-3,ipoin-2,ipoin-1,ipoin
           write(lun_parti_msh,'(a)') 'end elements'  
        else
           write(lun_parti_msh,'(a)') 'MESH CODE'//trim(intost(icode))//'_BOUNDING_BOX dimension 3 Elemtype Hexahedra Nnode 8'       
           write(lun_parti_msh,'(a)') 'coordinates'  
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmini_send(1),xmini_send(2),xmini_send(3)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmaxi_send(1),xmini_send(2),xmini_send(3)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmaxi_send(1),xmaxi_send(2),xmini_send(3)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmini_send(1),xmaxi_send(2),xmini_send(3)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmini_send(1),xmini_send(2),xmaxi_send(3)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmaxi_send(1),xmini_send(2),xmaxi_send(3)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmaxi_send(1),xmaxi_send(2),xmaxi_send(3)
           ipoin = ipoin + 1 ; write(lun_parti_msh,'(i5,3(1x,e12.6))') ipoin,xmini_send(1),xmaxi_send(2),xmaxi_send(3)
           write(lun_parti_msh,'(a)') 'end coordinates' 
           write(lun_parti_msh,'(a)') 'elements'  
           write(lun_parti_msh,'(9(1x,i7))') ielem,ipoin-3,ipoin-2,ipoin-1,ipoin-0,&
                &                               ipoin-7,ipoin-6,ipoin-5,ipoin-4
           write(lun_parti_msh,'(a)') 'end elements'  
        end if
     end if
  end do

  !----------------------------------------------------------------------
  !
  ! Results
  !
  !----------------------------------------------------------------------

  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'GiD Post Results File 1.0'
     write(lun_parti_res,'(a)') ' '
     call flush(lun_parti_res)
  end if
  !
  ! Number of neighbors
  !
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result NSUBD ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames NSUBD'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,nneig_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! Number of OpenMP colors
  !
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result OMP_COLOR ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames OMP_COLOR'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,ncolo_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! Element OMP chunk size
  !
  if( IMASTER ) then
     dummi = 0
  else
     dummi = par_omp_nelem_chunk
  end if
  call PAR_GATHER(dummi,lresu_gat,'IN THE WORLD') 
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result OMP_CHUNK_ELEMENT ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames OMP_CHUNK_ELEMENT'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! NODE OMP chunk size
  !
  if( IMASTER ) then
     dummi = 0
  else
     dummi = par_omp_npoin_chunk
  end if
  call PAR_GATHER(dummi,lresu_gat,'IN THE WORLD') 
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result OMP_CHUNK_NODE ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames OMP_CHUNK_NODE'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! Compute neighbors stats
  ! 
  if( PAR_MY_WORLD_RANK == 0 ) then
     !
     ! xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx
     !  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21
     ! xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx
     ! av/2          av/1.5           av           1.5xav          2xav
     !
     xaver = 0.0_rp 
     do ipart = 0,PAR_WORLD_SIZE-1  
        xaver = xaver + real(nneig_gat(ipart),rp)
     end do
     xaver = xaver / real(PAR_WORLD_SIZE-mcode,rp) ! Remove master's contributions
     do ipart = 0,PAR_WORLD_SIZE-1  
        if( nneig_gat(ipart) > 0 ) then
           xx    = real(nneig_gat(ipart),rp)
           dummr = max(xx/xaver,xaver/xx)
           if( xx >= xaver ) then
              ii =   1 + int(10.0_rp*dummr,ip)
           else
              ii =  21 - int(10.0_rp*dummr,ip) 
           end if
           ii = min(max( 1_ip,ii),21_ip)
           neighbor_stat(ii) = neighbor_stat(ii) + 1
        end if
     end do
  end if
  !
  ! My code number
  !
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result CODE ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames CODE'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lcode_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! My rank in my code
  !
  call PAR_GATHER(kfl_paral,lresu_gat,'IN THE WORLD') 
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result CODE_RANK ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames CODE_RANK'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! My world rank 
  !
  call PAR_GATHER(PAR_MY_WORLD_RANK,lresu_gat,'IN THE WORLD') 
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result WORLD_RANK ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames WORLD_RANK'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! Number of interior nodes per subdomain
  !
  if( IMASTER ) then
     npoin_interior = 0
  else
     npoin_interior = npoi1
  end if
  call PAR_GATHER(npoin_interior,lresu_gat,'IN THE WORLD') 
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result NBINT ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames NBINT'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! Number of boundary nodes per subdomain
  !
  if( IMASTER ) then
     npoin_boundary = 0
  else
     npoin_boundary = npoin-npoi2+1_ip
  end if
  call PAR_GATHER(npoin_boundary,lresu_gat,'IN THE WORLD') 
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result NBBOU ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames NBBOU'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! Compute boundary stats
  ! 
  if( PAR_MY_WORLD_RANK == 0 ) then
     !
     ! xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx
     !  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21
     ! xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx
     ! av/2          av/1.5           av           1.5xav          2xav
     !
     xaver = 0.0_rp 
     do ipart = 0,PAR_WORLD_SIZE-1  
        xaver = xaver + real(lresu_gat(ipart),rp)
     end do
     xaver = xaver / real(PAR_WORLD_SIZE-mcode,rp) ! Remove master's contributions
     do ipart = 0,PAR_WORLD_SIZE-1  
        if( lresu_gat(ipart) > 0 ) then
           xx    = real(lresu_gat(ipart),rp)
           dummr = max(xx/xaver,xaver/xx)
           if( xx >= xaver ) then
              ii =   1 + int(10.0_rp*dummr,ip)
           else
              ii =  21 - int(10.0_rp*dummr,ip) 
           end if
           ii = min(max( 1_ip,ii),21_ip)
           boundary_stat(ii) = boundary_stat(ii) + 1
        end if
     end do
  end if
  !
  ! Number of elements per subdomain
  !
  if( IMASTER ) then
     dummi = 0
  else
     dummi = nelem
  end if
  call PAR_GATHER(dummi,lresu_gat,'IN THE WORLD') 
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result NELEM ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames NELEM'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! Number of weighted elements per subdomain
  !
  if( IMASTER ) then
     dummi = 0
  else
     dummi = 0
     do ielem = 1,nelem
        pelty = abs(ltype(ielem))
        dummi = dummi + ngaus(pelty)
     end do
  end if
  call PAR_GATHER(dummi,lresu_gat,'IN THE WORLD') 
  if( PAR_MY_WORLD_RANK == 0 ) then
     write(lun_parti_res,'(a)') 'Result NELEW ANALYSIS 0 Scalar OnNodes'
     write(lun_parti_res,'(a)') 'ComponentNames NELEW'
     write(lun_parti_res,'(a)') 'Values'
     do ipart = 0,PAR_WORLD_SIZE-1  
        write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
     end do
     write(lun_parti_res,'(a)') 'End Values'    
  end if
  !
  ! Compute element stats
  ! 
  if( PAR_MY_WORLD_RANK == 0 ) then
     !
     ! xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx
     !  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21
     ! xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx--+--+--+--xxxx
     ! av/2          av/1.5           av           1.5xav          2xav
     !
     xaver = 0.0_rp 
     do ipart = 0,PAR_WORLD_SIZE-1  
        xaver = xaver + real(lresu_gat(ipart),rp)
     end do
     xaver = xaver / real(PAR_WORLD_SIZE-mcode,rp) ! Remove master's contributions
     do ipart = 0,PAR_WORLD_SIZE-1  
        if( lresu_gat(ipart) > 0 ) then
           xx    = real(lresu_gat(ipart),rp)
           dummr = max(xx/xaver,xaver/xx)
           if( xx >= xaver ) then
              ii =   1 + int(10.0_rp*dummr,ip)
           else
              ii =  21 - int(10.0_rp*dummr,ip) 
           end if
           ii = min(max( 1_ip,ii),21_ip)
           element_stat(ii) = element_stat(ii) + 1
        end if
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! Couplings
  !
  !----------------------------------------------------------------------

  do icoup = 1,mcoup
     color_target      = coupling_type(icoup) % color_target
     color_source      = coupling_type(icoup) % color_source
     number_wet_points = coupling_type(icoup) % geome % number_wet_points
     npoin_wet         = coupling_type(icoup) % geome % npoin_wet
     nboun_wet         = coupling_type(icoup) % geome % nboun_wet
     nneig_source      = coupling_type(icoup) % commd % nneig
     !
     ! Geometry
     !
     call PAR_GATHER(nneig_source,nneig_gat,'IN THE WORLD') 

     if( PAR_MY_WORLD_RANK == 0 ) then        
        dummi = 0
        do ipart = 0,PAR_WORLD_SIZE-1 
           dummi = dummi + nneig_gat(ipart)
           nneig4_gat(ipart) = int(nneig_gat(ipart),4)
        end do
        if( associated(lneig_gat) ) deallocate( lneig_gat )
        allocate( lneig_gat(dummi) )
     else
        if( INOTMASTER ) then
           if( associated(lneig) ) deallocate( lneig )
           allocate( lneig(coupling_type(icoup) % commd % nneig) )
           do ineig = 1,coupling_type(icoup) % commd % nneig   
              ipart        = coupling_type(icoup) % commd % neights(ineig)
              ipart_world  = par_color_coupling_rank_to_world(ipart,color_target,color_source)
              lneig(ineig) = ipart_world
           end do
        end if
     end if

     call PAR_GATHERV(lneig,lneig_gat,nneig4_gat,'IN THE WORLD')

     if( PAR_MY_WORLD_RANK == 0 ) then        
        write(lun_parti_msh,'(a,i1,a)') 'MESH COUPLING'//trim(intost(icoup))//' dimension ',ndime,' Elemtype Linear Nnode 2'
        write(lun_parti_msh,'(a)') 'elements'
        kelem = 0
        jelem = ielem
        do ipart = 0,PAR_WORLD_SIZE-1
           do ineig = 1,nneig_gat(ipart)
              kelem = kelem + 1
              ielem = ielem + 1
              write(lun_parti_msh,'(3(1x,i7))') ielem,ipart+1,lneig_gat(kelem)+1
           end do
        end do
        write(lun_parti_msh,'(a)') 'end elements' 
     end if
     !
     ! Weight
     !
     if( INOTMASTER ) then
        do ineig = 1,coupling_type(icoup) % commd % nneig   
           lneig(ineig) =   coupling_type(icoup) % commd % lrecv_size(ineig+1) &
                &         - coupling_type(icoup) % commd % lrecv_size(ineig) 
        end do
     end if

     call PAR_GATHERV(lneig,lneig_gat,nneig4_gat,'IN THE WORLD')

     if( PAR_MY_WORLD_RANK == 0 ) then        
        write(lun_parti_res,'(a)')  'GaussPoints GP Elemtype Linear'
        write(lun_parti_res,'(a)')  'Number of Gauss Points: 1'
        write(lun_parti_res,'(a)')  'Natural Coordinates: Internal'
        write(lun_parti_res,'(a)')  'End GaussPoints'
        write(lun_parti_res,'(a)')  'Result COUPLING_'//trim(intost(icoup))//'_SIZE ANALYSIS 0 Scalar OnGaussPoints GP'
        write(lun_parti_res,'(a)')  'ComponentNames COUPLING_'//trim(intost(icoup))//'_SIZE'
        write(lun_parti_res,'(a)')  'Values'
        kelem = 0
        do ipart = 0,PAR_WORLD_SIZE-1
           do ineig = 1,nneig_gat(ipart)
              kelem = kelem + 1
              jelem = jelem + 1
              write(lun_parti_res,'(3(1x,i7))') jelem,lneig_gat(kelem)
           end do
        end do
        write(lun_parti_res,'(a)')  'End Values'  
     end if
     !
     ! Results
     !
     call PAR_GATHER(number_wet_points,lresu_gat,'IN THE WORLD') 
     if( PAR_MY_WORLD_RANK == 0 ) then
        write(lun_parti_res,'(a)') 'Result COUPLI'//trim(intost(icoup))//'_WET_POINTS ANALYSIS 0 Scalar OnNodes'
        write(lun_parti_res,'(a)') 'ComponentNames COUPLI'//trim(intost(icoup))//'_WET_POINTS'
        write(lun_parti_res,'(a)') 'Values'
        do ipart = 0,PAR_WORLD_SIZE-1  
           write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
        end do
        write(lun_parti_res,'(a)') 'End Values'    
     end if
     call PAR_GATHER(npoin_wet,lresu_gat,'IN THE WORLD') 
     if( PAR_MY_WORLD_RANK == 0 ) then
        write(lun_parti_res,'(a)') 'Result COUPLI'//trim(intost(icoup))//'_WET_NODES ANALYSIS 0 Scalar OnNodes'
        write(lun_parti_res,'(a)') 'ComponentNames COUPLI'//trim(intost(icoup))//'_WET_NODES'
        write(lun_parti_res,'(a)') 'Values'
        do ipart = 0,PAR_WORLD_SIZE-1  
           write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
        end do
        write(lun_parti_res,'(a)') 'End Values'    
     end if
     call PAR_GATHER(nboun_wet,lresu_gat,'IN THE WORLD') 
     if( PAR_MY_WORLD_RANK == 0 ) then
        write(lun_parti_res,'(a)') 'Result COUPLI'//trim(intost(icoup))//'_WET_BOUNDARIES ANALYSIS 0 Scalar OnNodes'
        write(lun_parti_res,'(a)') 'ComponentNames COUPLI'//trim(intost(icoup))//'_WET_BOUNDARIES'
        write(lun_parti_res,'(a)') 'Values'
        do ipart = 0,PAR_WORLD_SIZE-1  
           write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
        end do
        write(lun_parti_res,'(a)') 'End Values'    
     end if
     call PAR_GATHER(nneig_source,lresu_gat,'IN THE WORLD') 
     if( PAR_MY_WORLD_RANK == 0 ) then
        write(lun_parti_res,'(a)') 'Result COUPLI'//trim(intost(icoup))//'_NEIGHBORS ANALYSIS 0 Scalar OnNodes'
        write(lun_parti_res,'(a)') 'ComponentNames COUPLI'//trim(intost(icoup))//'_NEIGHBORS'
        write(lun_parti_res,'(a)') 'Values'
        do ipart = 0,PAR_WORLD_SIZE-1  
           write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
        end do
        write(lun_parti_res,'(a)') 'End Values'    
     end if
     call PAR_GATHER(coupling_type(icoup) % commd % lrecv_dim,lresu_gat,'IN THE WORLD') 
     if( PAR_MY_WORLD_RANK == 0 ) then
        write(lun_parti_res,'(a)') 'Result COUPLI'//trim(intost(icoup))//'_RECEIVE_SIZE ANALYSIS 0 Scalar OnNodes'
        write(lun_parti_res,'(a)') 'ComponentNames COUPLI'//trim(intost(icoup))//'_RECEIVE_SIZE'
        write(lun_parti_res,'(a)') 'Values'
        do ipart = 1,PAR_WORLD_SIZE-1  
           write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
        end do
        write(lun_parti_res,'(a)') 'End Values'    
     end if
     call PAR_GATHER(coupling_type(icoup) % commd % lsend_dim,lresu_gat,'IN THE WORLD') 
     if( PAR_MY_WORLD_RANK == 0 ) then
        write(lun_parti_res,'(a)') 'Result COUPLI'//trim(intost(icoup))//'_SEND_SIZE ANALYSIS 0 Scalar OnNodes'
        write(lun_parti_res,'(a)') 'ComponentNames COUPLI'//trim(intost(icoup))//'_SEND_SIZE'
        write(lun_parti_res,'(a)') 'Values'
        do ipart = 0,PAR_WORLD_SIZE-1  
           write(lun_parti_res,'(2(1x,i7))') ipart+1,lresu_gat(ipart)
        end do
        write(lun_parti_res,'(a)') 'End Values'    
     end if
  end do
  !
  ! Deallocate
  !
  if( associated(lneig)      ) deallocate( lneig      )
  if( associated(xcoor_gat)  ) deallocate( xcoor_gat  )
  if( associated(nneig_gat)  ) deallocate( nneig_gat  )
  if( associated(nneig4_gat) ) deallocate( nneig4_gat )
  if( associated(lneig_gat)  ) deallocate( lneig_gat  )
  if( associated(lresu_gat)  ) deallocate( lresu_gat  )
  if( associated(lcode_gat)  ) deallocate( lcode_gat  )
  if( associated(ncolo_gat)  ) deallocate( ncolo_gat  )
  if( associated(ledge)      ) deallocate( ledge      )

  if( PAR_MY_WORLD_RANK == 0 ) then
     close(lun_parti_msh)
     close(lun_parti_res)     
  end if

  !----------------------------------------------------------------------
  !
  ! Output statistics: See using gnuplot as follows:
  !
  ! reset
  ! set xtics nomirror; set ytics nomirror; set border front;
  ! set xrange [-1:21]; set yrange [0:100]
  ! set xlabel 'Dispersion'; set ylabel 'Percentage'
  ! set style data histogram; set style histogram cluster gap 0; set style fill solid
  ! set xtics ("<0.5*average" 1, "average" 11, ">2*average" 21)
  ! set ytics ("0" 0, "20" 20, "40" 40, "60" 60, "80" 80, "100" 100)
  ! plot 'caca.txt' u 1 t 'Elements' lc rgb "red", '' u 2 t 'Boundaries' lc rgb "green", '' u 3 t 'Neighbors' lc rgb "blue"
  !
  !----------------------------------------------------------------------

  call PAR_BROADCAST(21_ip,element_stat, 'IN THE WORLD')
  call PAR_BROADCAST(21_ip,boundary_stat,'IN THE WORLD')
  call PAR_BROADCAST(21_ip,neighbor_stat,'IN THE WORLD')
  xaver = real(PAR_WORLD_SIZE-mcode,rp) * 100.0_rp
  call outfor(70_ip,lun_outpu_par,' ')
  do ii = 1,21
     routp(1) = real(element_stat(ii), rp) / xaver 
     routp(2) = real(boundary_stat(ii),rp) / xaver 
     routp(3) = real(neighbor_stat(ii),rp) / xaver 
     call outfor(71_ip,lun_outpu_par,' ')
  end do
  call flush(lun_outpu_par)
 
end subroutine par_output_partition
