!----------------------------------------------------------------------
!> @addtogroup Parall
!> @{
!> @file    par_initialize_nullify.f90
!> @author  Guillaume Houzeaux
!> @date    01/02/2014
!> @brief   Intialize variables
!> @details Initialize and nullify variables of mod_parall
!> @} 
!----------------------------------------------------------------------

subroutine par_initialize_nullify()
  use mod_parall
  implicit none
  !
  ! Initialize variables
  !
  mcolo         = 0  
  mcode         = 0  
  mzone         = 0  
  msubd         = 0  
  ncolo         = 0                         
  par_memor     = 0
  par_bin_boxes = 1
  par_bin_comin = 0.0_rp
  par_bin_comax = 0.0_rp
  PAR_CODE_SIZE = 1

  !
  ! Nullify global pointers 
  !
  nullify(PAR_COMM_COLOR)           
  nullify(PAR_COMM_COLOR_PERM)    
  nullify(I_AM_IN_COLOR)              

  nullify(PAR_COMM_COLOR_ARRAY)       
  nullify(PAR_COMM_MY_CODE_ARRAY)     
   
  nullify(PAR_CPU_TO_COLOR)           
  nullify(PAR_COLOR_TO_CPU)           
  nullify(PAR_COLOR_BIN)              

  nullify(par_bin_part)
  !
  ! OMP stuffs
  !
  par_omp_num_blocks  = 1
  par_omp_granularity = 10
  par_omp_coloring_alg = 0
  par_omp_num_threads = 0
  par_omp_num_colors  = 0
  par_omp_nelem_chunk = 0
  par_omp_nboun_chunk = 0
  par_omp_npoin_chunk = 0
  nullify(par_omp_list_colors) 
  nullify(par_omp_ia_colors)
  nullify(par_omp_ja_colors)

end subroutine par_initialize_nullify
