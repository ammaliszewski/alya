!------------------------------------------------------------------------
!> @addtogroup MetisToolBox
!> @{
!> @name    METIS toolbox
!> @file    mod_parall.f90
!> @author  Guillaume Houzeaux
!> @date    03/11/2015
!> @brief   Bridge to METIS
!> @details Bridge to METIS
!>
!> @{
!------------------------------------------------------------------------

module mod_par_metis
  use def_kintyp, only : ip,rp  
#ifdef V5METIS  
  use iso_c_binding  ! for C_INTPTR_T
#endif
  implicit none 
  private

  public :: par_renumber_nodend
  public :: par_partition_graph

contains

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Renumber a graph using nested dissection
  !> @details Renumber a graph using METIS
  !>
  !-----------------------------------------------------------------------
  
  subroutine par_renumber_nodend(&
       nvert,ia,ja,invpr,permr)
    integer(ip), intent(in)    :: nvert     !< Number of vertices
    integer(ip), intent(in)    :: ia(*)     !< Graph
    integer(ip), intent(in)    :: ja(*)     !< Graph
    integer(ip), intent(out)   :: invpr(*)  !< Inverse permutation
    integer(ip), intent(out)   :: permr(*)  !< Permutation

#ifdef V5METIS
    !
    ! METIS 5.0.2 diferent parameters to metis 4 
    !
    integer(ip)                :: optio(0:39)
    integer(ip), parameter     :: METIS_OPTION_PTYPE=0
    integer(ip), parameter     :: METIS_OPTION_OBJTYPE=1
    integer(ip), parameter     :: METIS_OPTION_CTYPE=2
    integer(ip), parameter     :: METIS_OPTION_IPTYPE=3
    integer(ip), parameter     :: METIS_OPTION_RTYPE=4
    integer(ip), parameter     :: METIS_OPTION_DBGLVL=5
    integer(ip), parameter     :: METIS_OPTION_NITER=6
    integer(ip), parameter     :: METIS_OPTION_NCUTS=7
    integer(ip), parameter     :: METIS_OPTION_SEED=8
    integer(ip), parameter     :: METIS_OPTION_MINCONN=9
    integer(ip), parameter     :: METIS_OPTION_CONTIG=10
    integer(ip), parameter     :: METIS_OPTION_COMPRESS=11
    integer(ip), parameter     :: METIS_OPTION_CCORDER=12
    integer(ip), parameter     :: METIS_OPTION_PFACTOR=13
    integer(ip), parameter     :: METIS_OPTION_NSEPS=14
    integer(ip), parameter     :: METIS_OPTION_UFACTOR=15
    integer(ip), parameter     :: METIS_OPTION_NUMBERING=16
    integer(ip), parameter     :: METIS_OPTION_HELP=17
    integer(ip), parameter     :: METIS_OPTION_TPWGTS=18
    integer(ip), parameter     :: METIS_OPTION_NCOMMON=19
    integer(ip), parameter     :: METIS_OPTION_NOOUTPUT=20
    integer(ip), parameter     :: METIS_OPTION_BALANCE=21
    integer(ip), parameter     :: METIS_OPTION_GTYPE=22
    integer(ip), parameter     :: METIS_OPTION_UBVEC=23
    !options[METIS OPTION PTYPE] !Specifies the partitioning method. Possible values are:
    integer(ip), parameter     :: METIS_PTYPE_RB      = 0   ! Multilevel recursive bisectioning.
    integer(ip), parameter     :: METIS_PTYPE_KWAY    = 1   ! Multilevel k-way partitioning. 
    !options[METIS OPTION OBJTYPE]  !Specifies the type of objective. Possible values are:
    integer(ip), parameter     :: METIS_OBJTYPE_CUT   = 0   ! Edge-cut minimization.
    integer(ip), parameter     :: METIS_OBJTYPE_VOL   = 1   ! Total communication volume minimization. 
    !options[METIS OPTION CTYPE]
    !Specifies the matching scheme to be used during coarsening. Possible values are:
    integer(ip), parameter     :: METIS_CTYPE_RM      = 0   ! Random matching.
    integer(ip), parameter     :: METIS_CTYPE_SHEM    = 1   ! Sorted heavy-edge matching.
    !options[METIS OPTION IPTYPE] Determines the algorithm used during initial partitioning. Possible values are:
    integer(ip), parameter     :: METIS_IPTYPE_GROW    = 0 
    integer(ip), parameter     :: METIS_IPTYPE_RANDOM  = 1 
    integer(ip), parameter     :: METIS_IPTYPE_EDGE    = 2 
    integer(ip), parameter     :: METIS_IPTYPE_NODE    = 3 
    !Grows a bisection using a greedy strategy. Computes a bisection at random followed by a refinement. Derives a separator from an edge cut. Grow a bisection using a greedy node-based strategy.
    !options[METIS OPTION RTYPE] Determines the algorithm used for refinement. Possible values are:
    integer(ip), parameter     :: METIS_RTYPE_FM       = 0 
    integer(ip), parameter     :: METIS_RTYPE_GREEDY   = 1 
    integer(ip), parameter     :: METIS_RTYPE_SEP2SIDED= 2  
    integer(ip), parameter     :: METIS_RTYPE_SEP1SIDED= 3 
    !FM-based cut refinement. Greedy-based cut and volume refinement. Two-sided node FM refinement. 
    !One-sided node FM refinement.
    integer(C_INTPTR_T)        :: nullvar
#endif
#ifdef METIS
    integer(ip)                :: optio(8)
    integer(ip)                :: kfl_fortr
#endif
    
#ifdef V5METIS
    call METIS_SetDefaultOptions(optio)
    optio(METIS_OPTION_NUMBERING) = 1     ! Start by 1; Fortran 
    nullvar = 0
    call metis_nodend(&
         nvert,ia,ja, &
         %val(nullvar),optio,invpr,permr)
#endif

#ifdef METIS
     kfl_fortr = 1_ip
     optio(1)  = 1_ip
     optio(2)  = 3_ip
     optio(3)  = 2_ip
     optio(4)  = 2_ip
     optio(5)  = 0_ip
     optio(6)  = 1_ip 
     optio(7)  = 0_ip
     optio(8)  = 1_ip
     call metis_nodend(&
          nvert,ia,ja,kfl_fortr,&
          optio,invpR,permR)
#endif
    
  end subroutine par_renumber_nodend

  !-----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @brief   Partition a graph
  !> @details Partition a graph using METIS
  !>
  !>          o-----o    o
  !>           \        / <= edge
  !>            o------o
  !>           /\
  !>          /  \
  !>         o    o <= vertex
  !>
  !-----------------------------------------------------------------------
  
  subroutine par_partition_graph(&
       nvert,ia,ja,wvert,wedge,kfl_weigh,npart,lepar)

    integer(ip), intent(in)    :: nvert     !< Number of vertices
    integer(ip), intent(in)    :: ia(*)     !< graph
    integer(ip), intent(in)    :: ja(*)     !< graph
    integer(ip), intent(in)    :: wvert(*)  !< Weight of vertices
    integer(ip), intent(in)    :: wedge(*)  !< Weight of edges
    integer(ip), intent(in)    :: kfl_weigh !< Type of weight  0:No weights,1:edges,2:vertices,3:both
    integer(ip), intent(in)    :: npart     !< Number of partitions/blocks
    integer(ip), intent(out)   :: lepar(*)  !< Partition result
    integer(ip)                :: edgecut
 
#ifdef V5METIS
    !
    ! METIS 5.0.2 diferent parameters to metis 4 
    !
    integer(ip)                :: optio(0:39)
    integer(ip), parameter     :: METIS_OPTION_PTYPE=0
    integer(ip), parameter     :: METIS_OPTION_OBJTYPE=1
    integer(ip), parameter     :: METIS_OPTION_CTYPE=2
    integer(ip), parameter     :: METIS_OPTION_IPTYPE=3
    integer(ip), parameter     :: METIS_OPTION_RTYPE=4
    integer(ip), parameter     :: METIS_OPTION_DBGLVL=5
    integer(ip), parameter     :: METIS_OPTION_NITER=6
    integer(ip), parameter     :: METIS_OPTION_NCUTS=7
    integer(ip), parameter     :: METIS_OPTION_SEED=8
    integer(ip), parameter     :: METIS_OPTION_MINCONN=9
    integer(ip), parameter     :: METIS_OPTION_CONTIG=10
    integer(ip), parameter     :: METIS_OPTION_COMPRESS=11
    integer(ip), parameter     :: METIS_OPTION_CCORDER=12
    integer(ip), parameter     :: METIS_OPTION_PFACTOR=13
    integer(ip), parameter     :: METIS_OPTION_NSEPS=14
    integer(ip), parameter     :: METIS_OPTION_UFACTOR=15
    integer(ip), parameter     :: METIS_OPTION_NUMBERING=16
    integer(ip), parameter     :: METIS_OPTION_HELP=17
    integer(ip), parameter     :: METIS_OPTION_TPWGTS=18
    integer(ip), parameter     :: METIS_OPTION_NCOMMON=19
    integer(ip), parameter     :: METIS_OPTION_NOOUTPUT=20
    integer(ip), parameter     :: METIS_OPTION_BALANCE=21
    integer(ip), parameter     :: METIS_OPTION_GTYPE=22
    integer(ip), parameter     :: METIS_OPTION_UBVEC=23
    !options[METIS OPTION PTYPE] !Specifies the partitioning method. Possible values are:
    integer(ip), parameter     :: METIS_PTYPE_RB      = 0   ! Multilevel recursive bisectioning.
    integer(ip), parameter     :: METIS_PTYPE_KWAY    = 1   ! Multilevel k-way partitioning. 
    !options[METIS OPTION OBJTYPE]  !Specifies the type of objective. Possible values are:
    integer(ip), parameter     :: METIS_OBJTYPE_CUT   = 0   ! Edge-cut minimization.
    integer(ip), parameter     :: METIS_OBJTYPE_VOL   = 1   ! Total communication volume minimization. 
    !options[METIS OPTION CTYPE]
    !Specifies the matching scheme to be used during coarsening. Possible values are:
    integer(ip), parameter     :: METIS_CTYPE_RM      = 0   ! Random matching.
    integer(ip), parameter     :: METIS_CTYPE_SHEM    = 1   ! Sorted heavy-edge matching.
    !options[METIS OPTION IPTYPE] Determines the algorithm used during initial partitioning. Possible values are:
    integer(ip), parameter     :: METIS_IPTYPE_GROW    = 0 
    integer(ip), parameter     :: METIS_IPTYPE_RANDOM  = 1 
    integer(ip), parameter     :: METIS_IPTYPE_EDGE    = 2 
    integer(ip), parameter     :: METIS_IPTYPE_NODE    = 3 
    !Grows a bisection using a greedy strategy. Computes a bisection at random followed by a refinement. Derives a separator from an edge cut. Grow a bisection using a greedy node-based strategy.
    !options[METIS OPTION RTYPE] Determines the algorithm used for refinement. Possible values are:
    integer(ip), parameter     :: METIS_RTYPE_FM       = 0 
    integer(ip), parameter     :: METIS_RTYPE_GREEDY   = 1 
    integer(ip), parameter     :: METIS_RTYPE_SEP2SIDED= 2  
    integer(ip), parameter     :: METIS_RTYPE_SEP1SIDED= 3 
    !FM-based cut refinement. Greedy-based cut and volume refinement. Two-sided node FM refinement. 
    !One-sided node FM refinement.
    integer(C_INTPTR_T)        :: nullvar
#endif
#ifdef METIS
    integer(ip)                :: optio(8)
    integer(ip)                :: kfl_fortr
#endif

#ifdef V5METIS
    !--------------------------------------------------------------------
    !
    ! METIS 5
    !
    !--------------------------------------------------------------------
    !
    ! ! notice that I am passing %val(nullvar) in the place that should be occupied by wedge_par
    !
    if ( wedge(1) /= 0_ip ) then                                        
       call runend('par_metis: wedge_par(1)/=0_ip not ready') 
    end if
    !
    ! Initialization
    !
    call METIS_SetDefaultOptions(optio)
    optio(METIS_OPTION_NUMBERING) = 1     ! Start by 1; Fortran 
    nullvar = 0
    !
    ! IF kfl_weigh_par IS 0 then the wvert_par is nullified
    !
    if (kfl_weigh== 0) then
       if( npart <= 8 ) then           
          call metis_partgraphrecursive( &
               nvert, 1_ip, ia, ja, %val(nullvar), %val(nullvar), &
               %val(nullvar), npart,    &
               %val(nullvar), %val(nullvar), optio, edgecut, lepar )
       else
          call metis_partgraphkway(&
               nvert, 1_ip, ia, ja, %val(nullvar), %val(nullvar), &
               %val(nullvar), npart,    &
               %val(nullvar), %val(nullvar), optio, edgecut, lepar )
       end if
    else
       if( npart <= 8 ) then
          call metis_partgraphrecursive( &
               nvert, 1_ip, ia, ja, wvert, %val(nullvar), &
               %val(nullvar), npart,    &
               %val(nullvar), %val(nullvar), optio, edgecut, lepar )
       else
          call metis_partgraphkway(&
               nvert, 1_ip, ia, ja, wvert, %val(nullvar), &
               %val(nullvar), npart,    &
               %val(nullvar), %val(nullvar), optio, edgecut, lepar )
       end if
    end if
#endif

    !--------------------------------------------------------------------
    !
    ! METIS 4
    !
    !--------------------------------------------------------------------

#ifdef METIS
    kfl_fortr  = 1_ip
    optio(1:8) = 0_ip
    optio(1)   = 0_ip 
    optio(2)   = 3_ip
    optio(3)   = 1_ip
    optio(4)   = 1_ip
    optio(5)   = 0_ip      

    if( npart <= 8 ) then
       !DEC$ ATTRIBUTES C,REFERENCE:: metis_partgraphrecursive
       call metis_partgraphrecursive(     &
            nvert, ia, ja, wvert, wedge,  &
            kfl_weigh, kfl_fortr, npart,  &
            optio, edgecut, lepar )
    else
       !DEC$ ATTRIBUTES C,REFERENCE:: metis_partgraphkway
       call metis_partgraphkway(          &
            nvert, ia, ja, wvert, wedge,  &
            kfl_weigh, kfl_fortr, npart,  &
            optio, edgecut, lepar )
    end if
#endif

  end subroutine par_partition_graph

end module mod_par_metis

