!------------------------------------------------------------------------
!> @addtogroup ParallToolBox
!> @{
!> @name    Parallelization toolbox
!> @file    mod_parall.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!> @brief   ToolBox for parall
!> @details ToolBox for parall
!> @{
!------------------------------------------------------------------------

module mod_communications

  use def_kintyp, only : ip,rp,lg
  use def_domain, only : npoin
  use def_domain, only : npoin_2
  use def_domain, only : nelem_2
  use def_domain, only : nboun_2
  use def_master, only : npoi1
  use def_master, only : IPARALL,IMASTER,ISLAVE,INOTSLAVE,ISEQUEN
  use def_master, only : INOTMASTER
  use def_master, only : comm_data_par
  use def_master, only : current_code,current_zone,current_subd
  use mod_parall, only : par_code_zone_subd_to_color
  use mod_parall, only : PAR_COMM_COLOR
  use mod_parall, only : PAR_COMM_MY_CODE
  use mod_parall, only : PAR_COMM_COLOR_ARRAY
  use mod_parall, only : PAR_COMM_MY_CODE_ARRAY
  use mod_parall, only : PAR_MY_CODE_RANK, PAR_MY_WORLD_RANK
  use mod_parall, only : PAR_COMM_WORLD
  use mod_parall, only : PAR_COMM_CURRENT
  use mod_parall, only : PAR_INTEGER
  use mod_parall, only : commd
  use mod_parall, only : color_target
  use mod_parall, only : color_source
  
#ifdef PARMETIS
  use mod_parall, only: PARMETIS_COMM
#endif

  private 
#ifndef MPI_OFF
  include 'mpif.h'
  integer(4) :: status(MPI_STATUS_SIZE)
#endif

  type non_blocking_typ
     integer(4),  pointer  :: request4(:) 
     integer(4)            :: count4
  end type non_blocking_typ
  logical(lg),            allocatable :: tmp_lsend(:)
  logical(lg),            allocatable :: tmp_lrecv(:)
  integer(ip),            allocatable :: tmp_isend(:)
  integer(ip),            allocatable :: tmp_irecv(:)
  real(rp),               allocatable :: tmp_rsend(:)
  real(rp),               allocatable :: tmp_rrecv(:)
  integer(4),             allocatable :: ireq4(:) 
  integer(4)                          :: ireq41(1)
  type(non_blocking_typ), pointer     :: non_blocking(:)
  integer(ip)                         :: inonblocking
  real(rp),               pointer     :: yy_non_blocking(:)
  !
  ! All reduce operations:
  !
  ! PAR_SUM
  ! PAR_MAX
  ! PAR_MIN
  !
  interface PAR_SUM
     module procedure PAR_SUM_IP_0,PAR_SUM_IP_s,PAR_SUM_IP_1,PAR_SUM_IP_2,PAR_SUM_IP_3,&
          &           PAR_SUM_RP_0,PAR_SUM_RP_s,PAR_SUM_RP_1,PAR_SUM_RP_2,&
          &           PAR_SUM_RP_02,&
          &           PAR_SUM_CX_0,PAR_SUM_CX_s,PAR_SUM_CX_1,PAR_SUM_CX_2
  end interface PAR_SUM
  interface PAR_MAX
     module procedure PAR_MAX_IP_s,PAR_MAX_IP_0,PAR_MAX_IP_1,PAR_MAX_IP_2,&
          &           PAR_MAX_RP_s,PAR_MAX_RP_0,PAR_MAX_RP_1,PAR_MAX_RP_2,PAR_MAX_RP_3,&
          &           PAR_MAX_CX_s,PAR_MAX_CX_0,PAR_MAX_CX_1,PAR_MAX_CX_2
  end interface PAR_MAX
  interface PAR_MIN
     module procedure PAR_MIN_IP_s,PAR_MIN_IP_0,PAR_MIN_IP_1,PAR_MIN_IP_2,&
          &           PAR_MIN_RP_s,PAR_MIN_RP_0,PAR_MIN_RP_1,PAR_MIN_RP_2,&
          &           PAR_MIN_CX_s,PAR_MIN_CX_0,PAR_MIN_CX_1,PAR_MIN_CX_2
  end interface PAR_MIN
  interface PAR_AVERAGE
     module procedure PAR_AVERAGE_IP_s,PAR_AVERAGE_IP_0
  end interface PAR_AVERAGE
  !
  ! Scalar product
  !
  interface PAR_SCALAR_PRODUCT
     module procedure PAR_SCALAR_PRODUCT_RP_2
  end interface PAR_SCALAR_PRODUCT
  !
  ! Reduction operations in the Alya world (PAR_COMM_WORLD) including masters
  !
  interface PAR_MAX_ALL
     module procedure PAR_MAX_ALL_IP_s, PAR_MAX_ALL_IP_1
  end interface PAR_MAX_ALL
  interface PAR_SUM_ALL
     module procedure PAR_SUM_ALL_IP_1, PAR_SUM_ALL_IP_3
  end interface PAR_SUM_ALL
  !
  ! Exchange array between neighbors
  !
  interface PAR_ARRAY_EXCHANGE
     module procedure PAR_ARRAY_EXCHANGE_IP
  end interface PAR_ARRAY_EXCHANGE
  !
  ! Operation on arrays between all partitions
  !
  interface PAR_ALL_TO_ALL_ARRAY_OPERATION
     module procedure PAR_ALL_TO_ALL_ARRAY_OPERATION_IP
  end interface PAR_ALL_TO_ALL_ARRAY_OPERATION
  !
  ! Exchange between slaves:
  ! type:  1. nodes  2. faces    3. fringe nodes
  ! kind:  1. real   2. integer  3. Complex
  ! dim:   1. x(:)   2. x(:,:)   3. x(:,:,:)   4. n,x(n)
  ! what:  1. SUM    2. MAX      3. MIN
  ! where: in my code, in my zone
  !
  interface PAR_INTERFACE_NODE_EXCHANGE
     module procedure PAR_INTERFACE_NODE_EXCHANGE_IP_0,  &
          &           PAR_INTERFACE_NODE_EXCHANGE_IP_1,  &
          &           PAR_INTERFACE_NODE_EXCHANGE_IP_2,  &
          &           PAR_INTERFACE_NODE_EXCHANGE_IP_3,  &
          &           PAR_INTERFACE_NODE_EXCHANGE_IP_2b, &
          &           PAR_INTERFACE_NODE_EXCHANGE_RP_00, &
          &           PAR_INTERFACE_NODE_EXCHANGE_RP_0,  &
          &           PAR_INTERFACE_NODE_EXCHANGE_RP_1,  &
          &           PAR_INTERFACE_NODE_EXCHANGE_RP_2,  &
          &           PAR_INTERFACE_NODE_EXCHANGE_RP_3,  &
          &           PAR_INTERFACE_NODE_EXCHANGE_RP_2b, &
          &           PAR_INTERFACE_NODE_EXCHANGE_LG_1 
  end interface PAR_INTERFACE_NODE_EXCHANGE
  !
  ! Exchange between assymetric
  !
  interface PAR_COUPLING_NODE_EXCHANGE
     module procedure PAR_COUPLING_NODE_EXCHANGE_RP_0,  &
          &           PAR_COUPLING_NODE_EXCHANGE_RP_1,  &
          &           PAR_COUPLING_NODE_EXCHANGE_RP_1b, &
          &           PAR_COUPLING_NODE_EXCHANGE_RP_2,  &
          &           PAR_COUPLING_NODE_EXCHANGE_RP_3,  &
          &           PAR_COUPLING_NODE_EXCHANGE_RP_2b
  end interface PAR_COUPLING_NODE_EXCHANGE
  !
  ! Exchange between slaves on ghost nodes
  !
  interface PAR_GHOST_NODE_EXCHANGE
     module procedure PAR_GHOST_NODE_EXCHANGE_IP_0,  &
          &           PAR_GHOST_NODE_EXCHANGE_IP_1,  &
          &           PAR_GHOST_NODE_EXCHANGE_IP_2,  &
          &           PAR_GHOST_NODE_EXCHANGE_IP_3,  &
          &           PAR_GHOST_NODE_EXCHANGE_IP_2b, &
          &           PAR_GHOST_NODE_EXCHANGE_RP_00, &
          &           PAR_GHOST_NODE_EXCHANGE_RP_0,  &
          &           PAR_GHOST_NODE_EXCHANGE_RP_1,  &
          &           PAR_GHOST_NODE_EXCHANGE_RP_2,  &
          &           PAR_GHOST_NODE_EXCHANGE_RP_3,  &
          &           PAR_GHOST_NODE_EXCHANGE_RP_2b
  end interface PAR_GHOST_NODE_EXCHANGE
  !
  ! Exchange between slaves on ghost elements
  !
  ! +----+----+----+....o
  !             ||   /\
  !             \/   ||
  !           o....+----+----+----+
  !
  !
  interface PAR_GHOST_ELEMENT_EXCHANGE
     module procedure PAR_GHOST_ELEMENT_EXCHANGE_IP_0,  &
          &           PAR_GHOST_ELEMENT_EXCHANGE_IP_1,  &
          &           PAR_GHOST_ELEMENT_EXCHANGE_IP_2,  &
          &           PAR_GHOST_ELEMENT_EXCHANGE_IP_3,  &
          &           PAR_GHOST_ELEMENT_EXCHANGE_IP_2b, &
          &           PAR_GHOST_ELEMENT_EXCHANGE_RP_00, &
          &           PAR_GHOST_ELEMENT_EXCHANGE_RP_0,  &
          &           PAR_GHOST_ELEMENT_EXCHANGE_RP_1,  &
          &           PAR_GHOST_ELEMENT_EXCHANGE_RP_2,  &
          &           PAR_GHOST_ELEMENT_EXCHANGE_RP_3,  &
          &           PAR_GHOST_ELEMENT_EXCHANGE_RP_2b
  end interface PAR_GHOST_ELEMENT_EXCHANGE
  !
  ! Exchange between slaves on ghost elements
  !
  ! +----+----+----+....o
  !             /\   ||
  !             ||   \/
  !           o....+----+----+----+
  !
  !
  interface PAR_FROM_GHOST_ELEMENT_EXCHANGE
     module procedure PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_00, &
          &           PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_0,  &
          &           PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_1,  &
          &           PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_2,  &
          &           PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_3,  &
          &           PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_2b
  end interface PAR_FROM_GHOST_ELEMENT_EXCHANGE
  !
  ! Exchange between slaves on ghost boundaries
  !
  interface PAR_GHOST_BOUNDARY_EXCHANGE
     module procedure PAR_GHOST_BOUNDARY_EXCHANGE_IP_0,  &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_IP_1,  &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_IP_2,  &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_IP_3,  &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_IP_2b, &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_RP_00, &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_RP_0,  &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_RP_1,  &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_RP_2,  &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_RP_3,  &
          &           PAR_GHOST_BOUNDARY_EXCHANGE_RP_2b
  end interface PAR_GHOST_BOUNDARY_EXCHANGE
  !
  ! Send/receive 
  !
  interface PAR_SEND_RECEIVE
     module procedure PAR_SEND_RECEIVE_IP_s,&
          &           PAR_SEND_RECEIVE_IP_0,&
          &           PAR_SEND_RECEIVE_IP_1,&
          &           PAR_SEND_RECEIVE_IP_2,&
          &           PAR_SEND_RECEIVE_IP_3,&
          &           PAR_SEND_RECEIVE_RP_s,&
          &           PAR_SEND_RECEIVE_RP_0,&
          &           PAR_SEND_RECEIVE_RP_1,&
          &           PAR_SEND_RECEIVE_RP_2,&
          &           PAR_SEND_RECEIVE_RP_3
  end interface PAR_SEND_RECEIVE
  interface PAR_SEND_RECEIVE_TO_ALL
     module procedure PAR_SEND_RECEIVE_TO_ALL_RP_s,&
          &           PAR_SEND_RECEIVE_TO_ALL_RP_0,&
          &           PAR_SEND_RECEIVE_TO_ALL_RP_1,&
          &           PAR_SEND_RECEIVE_TO_ALL_RP_2,&
          &           PAR_SEND_RECEIVE_TO_ALL_IP_1
  end interface PAR_SEND_RECEIVE_TO_ALL
  !
  ! Operations with neighbors
  !
  interface PAR_POINT_TO_POINT_ARRAY_OPERATION
     module procedure PAR_POINT_TO_POINT_ARRAY_OPERATION_IP_1
  end interface PAR_POINT_TO_POINT_ARRAY_OPERATION
  !
  ! Split
  !
  interface PAR_COMM_SPLIT
     module procedure PAR_COMM_SPLIT4,PAR_COMM_SPLIT8
  end interface PAR_COMM_SPLIT
  !
  ! Bridge to broadcast data from master to slaves
  !
  interface PAR_EXCHANGE
     module procedure PAR_EXCHANGE_IP_s,PAR_EXCHANGE_IP_1,PAR_EXCHANGE_IP_2,&
          &           PAR_EXCHANGE_RP_s,PAR_EXCHANGE_RP_1,PAR_EXCHANGE_RP_2,&
          &           PAR_EXCHANGE_CH
  end interface PAR_EXCHANGE
  !
  ! Communicator operations
  !
  interface PAR_COMM_RANK_AND_SIZE
     module procedure PAR_COMM_RANK_AND_SIZE_4,PAR_COMM_RANK_AND_SIZE_4W,&
          &           PAR_COMM_RANK_AND_SIZE_41W,&
          &           PAR_COMM_RANK_AND_SIZE_8,PAR_COMM_RANK_AND_SIZE_8W
  end interface PAR_COMM_RANK_AND_SIZE
  !
  ! GATHER
  !
  interface PAR_GATHER
     module procedure PAR_GATHER_CHARACTER,&
          &           PAR_GATHER_IP_s4,PAR_GATHER_IP_s8,PAR_GATHER_IP_14,PAR_GATHER_IP_18,&
          &           PAR_GATHER_RP_1,PAR_GATHER_RP_12
  end interface PAR_GATHER
  !
  ! GATHERV
  !
  interface PAR_GATHERV
     module procedure PAR_GATHERV_RP_1,PAR_GATHERV_RP_21,PAR_GATHERV_RP_22,&
          &           PAR_GATHERV_IP_1,PAR_GATHERV_IP_2
  end interface PAR_GATHERV
  !
  ! ALLGATHERV
  !
  interface PAR_ALLGATHERV
     module procedure PAR_ALLGATHERV_IP4  ,PAR_ALLGATHERV_IP8  ,&
          &           PAR_ALLGATHERV_IP4_2,&
          &           PAR_ALLGATHERV_RP_2
  end interface PAR_ALLGATHERV
  !
  ! ALLGATHER
  !
  interface PAR_ALLGATHER
     module procedure PAR_ALLGATHER_s4,PAR_ALLGATHER_s8,&
          &           PAR_ALLGATHER_IP_14,PAR_ALLGATHER_IP_18,&
          &           PAR_ALLGATHER_RP_0,PAR_ALLGATHER_RP_2,&
          &           PAR_ALLGATHER_RP_02,PAR_ALLGATHER_LG
  end interface PAR_ALLGATHER
  !
  ! SCATTER
  !
  interface PAR_SCATTER
     module procedure PAR_SCATTER_IP_s
  end interface PAR_SCATTER
  !
  ! SCATTERV
  !
  interface PAR_SCATTERV
     module procedure PAR_SCATTERV_IP_1
  end interface PAR_SCATTERV
  !
  ! BORADCAST
  !
  interface PAR_BROADCAST
     module procedure PAR_BROADCAST_IP_s,PAR_BROADCAST_IP_0,PAR_BROADCAST_IP_1,&
          &           PAR_BROADCAST_RP_s,PAR_BROADCAST_RP_0,PAR_BROADCAST_RP_1,&
          &           PAR_BROADCAST_CH
  end interface PAR_BROADCAST
  !
  ! Start non-blocking communications
  !
  interface PAR_START_NON_BLOCKING_COMM
     module procedure PAR_START_NON_BLOCKING_COMM_4,PAR_START_NON_BLOCKING_COMM_8
  end interface PAR_START_NON_BLOCKING_COMM
  !
  ! Public functions
  !
  public :: PAR_SUM                            ! AllReduce SUM
  public :: PAR_MAX                            ! AllReduce MAX
  public :: PAR_MIN                            ! AllReduce MIN
  public :: PAR_AVERAGE                        ! Average value
  public :: PAR_SCALAR_PRODUCT                 ! Scalar product
  public :: PAR_SUM_ALL                        ! AllReduce SUM in the Alya world (PAR_COMM_WORLD)
  public :: PAR_MAX_ALL                        ! AllReduce MAX in the Alya world (PAR_COMM_WORLD)
  public :: PAR_INTERFACE_NODE_EXCHANGE        ! Interface nodal exchange (send/receive)
  public :: PAR_COUPLING_NODE_EXCHANGE         ! Coupling nodal exchange (send/receive)
  public :: PAR_GHOST_NODE_EXCHANGE            ! Ghost nodal exchange (send/receive)
  public :: PAR_GHOST_ELEMENT_EXCHANGE         ! Ghost element exchange (send/receive): from element to ghost
  public :: PAR_FROM_GHOST_ELEMENT_EXCHANGE    ! Ghost element exchange (send/receive): from ghost to element
  public :: PAR_GHOST_BOUNDARY_EXCHANGE        ! Ghost boundary exchange (send/receive)
  public :: PAR_DEFINE_COMMUNICATOR            ! Define the communicator according to some keywords
  public :: PAR_COMM_SPLIT                     ! Split a communicator
  public :: PAR_SEND_RECEIVE                   ! Send and receive arrays to a specific partition
  public :: PAR_SEND_RECEIVE_TO_ALL            ! Send and receive arrays to all
  public :: PAR_EXCHANGE                       ! Bridge to broadcast data from master to slave
  public :: PAR_COMM_RANK_AND_SIZE             ! Give rank (and size) of a communicator
  public :: PAR_GATHER                         ! Gather of integers, reals, characters
  public :: PAR_GATHERV                        ! Gather of integers, reals, characters
  public :: PAR_SCATTER                        ! Scatter of integers, reals, characters
  public :: PAR_SCATTERV                       ! Scatterv of integers, reals, characters
  public :: PAR_ALLGATHERV                     ! All Gatherv
  public :: PAR_ALLGATHER                      ! All Gather
  public :: PAR_INIT                           ! Initialize MPI
  public :: PAR_BROADCAST                      ! Broadcast
  public :: PAR_LENGTH_INTEGER                 ! Length of integers
  public :: PAR_IMASTER_IN_COMMUNICATOR        ! If I am a master of a given communicator
  public :: PAR_BARRIER                        ! Barrier
  public :: PAR_INITIALIZE_NON_BLOCKING_COMM   ! Initialize non-blocking communications variables
  public :: PAR_START_NON_BLOCKING_COMM        ! Start non-blocking communications
  public :: PAR_END_NON_BLOCKING_COMM          ! End non-blocking communications
  public :: PAR_SET_NON_BLOCKING_COMM_NUMBER   ! Set the non-blocking communicator number
  public :: PAR_WAITALL                        ! Waitall
  public :: PAR_ALL_TO_ALL_ARRAY_OPERATION     ! Array operations between all partitions of communicators
  public :: PAR_POINT_TO_POINT_ARRAY_OPERATION ! Array operations between all partitions of communicators

!public :: ireq41
contains 

  !----------------------------------------------------------------------
  !
  ! Define communcator and communciation arrays
  !
  !----------------------------------------------------------------------

  subroutine PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    implicit none

#ifndef MPI_OFF
    include 'mpif.h'
#endif

    character(*),        optional,          intent(in)  :: where 
    integer(4),                             intent(out) :: PAR_COMM_TO_USE
    type(comm_data_par), optional, pointer, intent(out) :: commu
    integer(ip)                                         :: icolo,jcolo

    !if( IPARALL ) then
    if( present(where) ) then
       if( trim(where) == 'IN THE UNIVERSE' ) then
          !
          ! In the universe
          !
#ifndef MPI_OFF
          PAR_COMM_TO_USE = MPI_COMM_WORLD
#endif
       else if( trim(where) == 'IN THE WORLD' ) then
          !
          ! In the world
          !
          PAR_COMM_TO_USE = int(PAR_COMM_WORLD,4_ip)   ! Alya world
          if( present(commu) .and. associated(PAR_COMM_MY_CODE_ARRAY) )   commu => commd

       else if( trim(where) == 'IN MY CODE' ) then
          !
          ! In my code         
          !
          !icolo = par_code_zone_subd_to_color(current_code,0_ip,0_ip)          
          PAR_COMM_TO_USE = int(PAR_COMM_MY_CODE,4_ip)
          !PAR_COMM_COLOR(icolo,icolo)
          if( present(commu) .and. associated(PAR_COMM_MY_CODE_ARRAY) ) commu => PAR_COMM_MY_CODE_ARRAY(1)

       else if( trim(where) == 'IN MY ZONE' .or. trim(where) == 'IN CURRENT ZONE' ) then
          !
          ! In my current zone 
          !         
          icolo = par_code_zone_subd_to_color(current_code,current_zone,0_ip)
          PAR_COMM_TO_USE = int(PAR_COMM_COLOR(icolo,icolo),4)
          if( present(commu) .and. associated(PAR_COMM_MY_CODE_ARRAY) ) commu => PAR_COMM_COLOR_ARRAY(icolo)

       else if( trim(where) == 'IN MY SUBD' ) then
          !
          ! In my current subd
          !
          icolo = par_code_zone_subd_to_color(current_code,0_ip,current_subd)
          PAR_COMM_TO_USE = int(PAR_COMM_COLOR(icolo,icolo),4)
          if( present(commu) .and. associated(PAR_COMM_MY_CODE_ARRAY) ) commu => PAR_COMM_COLOR_ARRAY(icolo)

       else if( trim(where) == 'IN CURRENT COUPLING' ) then
          !
          ! In my current coupling
          !
          icolo = color_target
          jcolo = color_source
          ! REVISAR IN CURRENT COUPLING
          PAR_COMM_TO_USE = int(PAR_COMM_COLOR(icolo,jcolo),4)
          ! print*,"DEBUG: color_target, color_source ", color_target, color_source, PAR_COMM_TO_USE
          if( present(commu) ) then
             call runend('PAR_DEFINE_COMMUNICATOR: WRONG OPTION 1')
          end if

       else if( trim(where) == 'IN CURRENT COLOR' .or. trim(where) == 'IN CURRENT TARGET COLOR' ) then
          !
          ! In my current target color
          !
          icolo = color_target
          PAR_COMM_TO_USE = int(PAR_COMM_COLOR(icolo,icolo),4)
          if( present(commu) .and. associated(PAR_COMM_MY_CODE_ARRAY) ) commu => PAR_COMM_COLOR_ARRAY(icolo)

       else if( trim(where) == 'IN CURRENT SOURCE COLOR' ) then
          !
          ! In my current source color
          !
          icolo = color_source
          PAR_COMM_TO_USE = int(PAR_COMM_COLOR(icolo,icolo),4)
          if( present(commu) .and. associated(PAR_COMM_MY_CODE_ARRAY) ) commu => PAR_COMM_COLOR_ARRAY(icolo)

       else if( trim(where) == 'IN CURRENT' ) then
          !
          ! Uses current communicator
          !
          PAR_COMM_TO_USE = int(PAR_COMM_CURRENT,4)

          if( present(commu) ) then
             call runend('PAR_DEFINE_COMMUNICATOR: WRONG OPTION 2')
          end if
#ifdef PARMETIS       
       else if( trim(where) == 'IN PARAL PARTITION' ) then 
         
			PAR_COMM_TO_USE = int(PARMETIS_COMM,4)
#endif			
       else

          call runend('PAR DEFINE COMMUNICATOR: INVALID COMMUNICATOR OPTION')

       end if
    else
       PAR_COMM_TO_USE =  int(PAR_COMM_WORLD,4)
       commu            => commd
    end if
    !else
    !   PAR_COMM_TO_USE =  0
    !   commu            => commd
    !end if

  end subroutine PAR_DEFINE_COMMUNICATOR

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_INTERFACE_NODE_EXCHANGE_IP
  !
  !----------------------------------------------------------------------

  subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    integer(ip),                    intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_0

  subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_1(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

   if( INOTSLAVE ) return
    ndofn = 1
    if( size(xx,1) /= npoin .and. size(xx,1) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE IP_1')
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_1

  subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_2(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE IP_2')
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_2

  subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_3(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( size(xx,3) <= 2 ) then
       ndofn = size(xx,1)
       if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE IP_3')
    else
       ndofn = size(xx,1)*size(xx,2)
       if( size(xx,3) /= npoin .and. size(xx,3) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE IP_3')
    end if
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_3

  subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),  pointer,  intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
    PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
    call PAR_INTERFACE_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_IP_2b

  !----------------------------------------------------------------------
  !
  ! PAR_INTERFACE_NODE_EXCHANGE_IP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_INTERFACE_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    integer(ip),                    intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,nsize,jj,dom_i
    integer(ip)                                   :: ipoin,ini,kk,idofn
    integer(4)                                    :: istat4,nsize4,count4
    integer(4)                                    :: dom_i4,my_rank4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_isend(commu % bound_dim * ndofn))
             allocate(tmp_irecv(commu % bound_dim * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % bound_dim
                ipoin = commu % bound_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_isend(kk) = xx(idofn,ipoin)
                   tmp_irecv(kk) = 0
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             istat4 = 0_4
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   ini   = ndofn * ( commu % bound_size(ii)   - 1 ) + 1
                   nsize = ndofn * ( commu % bound_size(ii+1) - 1 ) + 1 - ini 

                   nsize4 = int(nsize,4)
                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_isend(ini:ini+nsize-1), nsize4, &
                           PAR_INTEGER,  dom_i4, 0_4,          &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_irecv(ini:ini+nsize-1), nsize4, &
                           PAR_INTEGER,  dom_i4, 0_4,          &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                   else
                      call MPI_Sendrecv(                       &
                           tmp_isend(ini:), nsize4,            &
                           PAR_INTEGER, dom_i4, 0_4,           &
                           tmp_irecv(ini:), nsize4,            &
                           PAR_INTEGER, dom_i4, 0_4,           &
                           PAR_COMM_TO_USE, status, istat4    )
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_INTERFACE_NODE_EXCHANGE_IP: MPI ERROR')
                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % bound_dim
                   ipoin = commu % bound_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) + tmp_irecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % bound_dim
                   ipoin = commu % bound_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = max(xx(idofn,ipoin),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % bound_dim
                   ipoin = commu % bound_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = min(xx(idofn,ipoin),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'TAKE MIN' ) then 
                !
                ! TAKE MIN
                !
                call runend('TAKE MIN NOT CODED')
                call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank4)
                kk = 0
                do ii = 1,commu % nneig
                   dom_i = commu % neights(ii)  
                   if( my_rank4 > dom_i ) then
                      ini   = ndofn * ( commu % bound_size(ii)   - 1 ) + 1
                      nsize = ndofn * ( commu % bound_size(ii+1) - 1 ) + 1 - ini 
                      do jj = commu % bound_size(ii),commu % bound_size(ii+1)-1
                         ipoin = commu % bound_perm(jj)
                         do idofn = 1,ndofn
                            kk = kk + 1
                            xx(idofn,ipoin) = tmp_irecv(kk)
                         end do
                      end do
                   end if
                end do

             else if( trim(what) == 'MIN RANK OR NEGATIVE' ) then 
                !
                ! MIN RANK OR NEGATIVE
                !
                call runend('MIN RANK OR NEGATIVE NOT CODED')
                call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank4)
                kk = 0
                do ii = 1,commu % nneig
                   dom_i = commu % neights(ii) 
                   ini   = ndofn * ( commu % bound_size(ii)   - 1 ) + 1
                   nsize = ndofn * ( commu % bound_size(ii+1) - 1 ) + 1 - ini 
                   do jj = commu % bound_size(ii),commu % bound_size(ii+1)-1
                      ipoin = commu % bound_perm(jj)
                      do idofn = 1,ndofn
                         kk = kk + 1                            
                         if( xx(idofn,ipoin) > 0 .and. tmp_irecv(kk) > 0 ) then
                            if( my_rank4 > dom_i ) then
                               !pard1 = 1
                               xx(idofn,ipoin) = -abs(xx(idofn,ipoin))
                            end if
                         end if
                      end do
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_irecv)
             deallocate(tmp_isend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_IP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_INTERFACE_NODE_EXCHANGE_LG
  !
  !----------------------------------------------------------------------

  subroutine PAR_INTERFACE_NODE_EXCHANGE_LG_1(xx,what,where,wsynch,dom_k)
    implicit none
    logical(lg),          pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

   if( INOTSLAVE ) return
    ndofn = 1
    if( size(xx,1) /= npoin .and. size(xx,1) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_LG(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_LG_1

  !----------------------------------------------------------------------
  !
  ! PAR_INTERFACE_NODE_EXCHANGE_IP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_INTERFACE_NODE_EXCHANGE_LG(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    logical(lg),                    intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,nsize,jj,dom_i
    integer(ip)                                   :: ipoin,ini,kk,idofn
    integer(4)                                    :: istat4,nsize4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_lsend(commu % bound_dim * ndofn))
             allocate(tmp_lrecv(commu % bound_dim * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % bound_dim
                ipoin = commu % bound_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_lsend(kk) = xx(idofn,ipoin)
                   tmp_lrecv(kk) = .false.
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             istat4 = 0_4
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   ini   = ndofn * ( commu % bound_size(ii)   - 1 ) + 1
                   nsize = ndofn * ( commu % bound_size(ii+1) - 1 ) + 1 - ini 

                   nsize4 = int(nsize,4)
                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_lsend(ini:ini+nsize-1), nsize4, &
                           MPI_LOGICAL,  dom_i4, 0_4,          &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_lrecv(ini:ini+nsize-1), nsize4, &
                           MPI_LOGICAL,  dom_i4, 0_4,          &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                   else
                      call MPI_Sendrecv(                       &
                           tmp_lsend(ini:), nsize4,            &
                           MPI_LOGICAL, dom_i4, 0_4,           &
                           tmp_lrecv(ini:), nsize4,            &
                           PAR_INTEGER, dom_i4, 0_4,           &
                           PAR_COMM_TO_USE, status, istat4    )
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_INTERFACE_NODE_EXCHANGE_IP: MPI ERROR')
                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'OR' ) then 
                !
                ! OR
                !
                kk = 0
                do jj = 1,commu % bound_dim
                   ipoin = commu % bound_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) .or. tmp_lrecv(kk)
                   end do
                end do

             else if( trim(what) == 'AND' ) then 
                !
                ! AND
                !
                kk = 0
                do jj = 1,commu % bound_dim
                   ipoin = commu % bound_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) .and. tmp_lrecv(kk)
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_lrecv)
             deallocate(tmp_lsend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_LG

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_INTERFACE_NODE_EXCHANGE_RP
  !
  !----------------------------------------------------------------------

  subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_00(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_00

  subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_0

  subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_1(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = 1
    if( what(len(what):len(what)) /= 'I' ) then
       if( size(xx,1) /= npoin .and. size(xx,1) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE RP_1')
    end if
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_1

  subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_2(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( what(len(what):len(what)) /= 'I' ) then
       if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE RP_2')
    end if
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_2

  subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_3(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    if( what(len(what):len(what)) /= 'I' ) then
       if( size(xx,3) <= 2 ) then
          ndofn = size(xx,1)
          if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE IP_3')
       else
          ndofn = size(xx,1)*size(xx,2)
          if( size(xx,3) /= npoin .and. size(xx,3) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE RP_3')
       end if
    end if
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_INTERFACE_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_3

  subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( what(len(what):len(what)) /= 'I' ) then
       if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
    end if
    PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
    call PAR_INTERFACE_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_RP_2b

  !----------------------------------------------------------------------
  !
  ! PAR_INTERFACE_NODE_EXCHANGE_RP: NODE ASSEMBLY FOR INTEGERS
  ! If last letter of what is I (meaning I interface) then it is assumed
  ! that only the interface part of the vector is passed, from npoi1+1
  ! to npoin.
  !
  !              Interior nodes                    Interface nodes
  ! <----------------------------------------><----------------------->
  ! +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
  ! |  1  |     |     |     |     |     |NPOI1|     |     |     |NPOIN|
  ! +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
  !
  !----------------------------------------------------------------------

  subroutine PAR_INTERFACE_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    real(rp),                       intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,nsize,jj,dom_i
    integer(ip)                                   :: ipoin,ini,kk,idofn
    integer(ip)                                   :: offset,ipoin_offset
    integer(4)                                    :: istat4,nsize4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Offset
       !
       if( what(len(what):len(what)) /= 'I' ) then
          offset = 0
       else
          offset = -npoi1
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_rsend(commu % bound_dim * ndofn))
             allocate(tmp_rrecv(commu % bound_dim * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % bound_dim
                ipoin = commu % bound_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_rsend(kk) = xx(idofn,ipoin+offset)
                   tmp_rrecv(kk) = 0.0_rp
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             istat4 = 0_4
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   ini   = ndofn * ( commu % bound_size(ii)   - 1 ) + 1
                   nsize = ndofn * ( commu % bound_size(ii+1) - 1 ) + 1 - ini 

                   nsize4 = int(nsize,4)
                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_rsend(ini:ini+nsize-1), nsize4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4, &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_rrecv(ini:ini+nsize-1), nsize4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4, &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                   else
                      call MPI_Sendrecv(                       &
                           tmp_rsend(ini:), nsize4,            &
                           MPI_DOUBLE_PRECISION, dom_i4, 0_4,  &
                           tmp_rrecv(ini:), nsize4,            &
                           MPI_DOUBLE_PRECISION, dom_i4, 0_4,  &
                           PAR_COMM_TO_USE, status, istat4    )
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_INTERFACE_NODE_EXCHANGE_RP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' .or. trim(what) == 'SUMI' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % bound_dim
                   ipoin = commu % bound_perm(jj)
                   ipoin_offset = ipoin + offset
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin_offset) = xx(idofn,ipoin_offset) + tmp_rrecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' .or. trim(what) == 'MAXI' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % bound_dim
                   ipoin = commu % bound_perm(jj)
                   ipoin_offset = ipoin + offset
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin_offset) = max(xx(idofn,ipoin_offset),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' .or. trim(what) == 'MINI' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % bound_dim
                   ipoin = commu % bound_perm(jj)
                   ipoin_offset = ipoin + offset
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin_offset) = min(xx(idofn,ipoin_offset),tmp_rrecv(kk))
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_rrecv)
             deallocate(tmp_rsend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_INTERFACE_NODE_EXCHANGE_RP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_INTERFACE_NODE_EXCHANGE_RP
  !
  !----------------------------------------------------------------------

  subroutine PAR_COUPLING_NODE_EXCHANGE_RP_00(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_COUPLING_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_COUPLING_NODE_EXCHANGE_RP_00

  subroutine PAR_COUPLING_NODE_EXCHANGE_RP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_COUPLING_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_COUPLING_NODE_EXCHANGE_RP_0

  subroutine PAR_COUPLING_NODE_EXCHANGE_RP_1(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = 1
    if( size(xx,1) /= npoin .and. size(xx,1) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE RP_1')
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_COUPLING_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_COUPLING_NODE_EXCHANGE_RP_1

  subroutine PAR_COUPLING_NODE_EXCHANGE_RP_1b(xx,what,commu,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = 1
    if( size(xx,1) /= npoin .and. size(xx,1) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE RP_1b')
    PAR_COMM_TO_USE = int(commu % PAR_COMM_WORLD,4)
    call PAR_COUPLING_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_COUPLING_NODE_EXCHANGE_RP_1b

  subroutine PAR_COUPLING_NODE_EXCHANGE_RP_2(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE RP_2')
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_COUPLING_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_COUPLING_NODE_EXCHANGE_RP_2

  subroutine PAR_COUPLING_NODE_EXCHANGE_RP_3(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    if( size(xx,3) <= 2 ) then
       ndofn = size(xx,1)
       if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE IP_3')
    else
       ndofn = size(xx,1)*size(xx,2)
       if( size(xx,3) /= npoin .and. size(xx,3) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE RP_3')
    end if
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_COUPLING_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_COUPLING_NODE_EXCHANGE_RP_3

  subroutine PAR_COUPLING_NODE_EXCHANGE_RP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
    PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
    call PAR_COUPLING_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_COUPLING_NODE_EXCHANGE_RP_2b

  !----------------------------------------------------------------------
  !
  ! PAR_COUPLING_NODE_EXCHANGE_RP: NODE ASSEMBLY FOR REALS
  !
  ! From the receiver point of view
  ! +---+                                                             
  ! | 1 |
  ! +---+                                                    +---+  /\
  ! | 2 |                                                    |   |  ||
  ! +---+   lscat_dim                                        +---+  ||
  ! | 3 |                                                    |   |  ||
  ! +---+      /\  +---+    +---+---+---+---+---+---+---+    +---+  || lrecv_size(2)-lrecv_size(1)
  ! | 4 |      ||  |   |    |   |   |   |   |   |   |   |    |   |  ||
  ! +---+      ||  +---+    +---+---+---+---+---+---+---+    +---+  ||
  ! | 5 |      ||  |   |  = |   |   |   |   |   |   |   |    |   |  ||
  ! +---+      ||  +---+    +---+---+---+---+---+---+---+    +---+  \/
  ! | 6 |      ||  |   |    |   |   |   |   |   |   |   |    +---+  /\                            
  ! +---+      \/  +---+    +---+---+---+---+---+---+---+    |   |  ||                            
  ! | 7 |            |             matrix_ia(:)              +---+  ||                            
  ! +---+  <---------+             matrix_ja(:)              |   |  || lrecv_size(3)-lrecv_size(2)                            
  ! | 8 |    lscat_perm(:)         matrix_aa(:)              +---+  ||
  ! +---+                          matrix_nzdom              |   |  ||
  ! | 9 |                                                    +---+  \/
  ! +---+
  ! 
  ! Nodal array 
  !
  !----------------------------------------------------------------------

  subroutine PAR_COUPLING_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
use def_master, only : lninv_loc
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    real(rp),                       intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,nsize_recv,jj,dom_i,ll
    integer(ip)                                   :: ipoin,ini_recv,kk,idofn
    integer(4)                                    :: istat4,nsize4_send,nsize4_recv,count4
    integer(4)                                    :: ini_send,nsize_send
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_rsend(commu % lsend_dim * ndofn))
             allocate(tmp_rrecv(commu % lrecv_dim * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % lsend_dim
                ipoin = commu % lsend_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_rsend(kk) = xx(idofn,ipoin)
                end do
             end do
             do kk = 1,commu % lrecv_dim * ndofn
                tmp_rrecv(kk) = 0.0_rp
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             istat4 = 0_4
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   ini_send   = ndofn * ( commu % lsend_size(ii)   - 1 ) + 1
                   nsize_send = ndofn * ( commu % lsend_size(ii+1) - 1 ) + 1 - ini_send
                   ini_recv   = ndofn * ( commu % lrecv_size(ii)   - 1 ) + 1
                   nsize_recv = ndofn * ( commu % lrecv_size(ii+1) - 1 ) + 1 - ini_recv

                   nsize4_send = int(nsize_send,4)
                   nsize4_recv = int(nsize_recv,4)

                   if( asynch ) then
                      !if( nsize_send > 0 ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_rsend(ini_send:ini_send+nsize_send-1), nsize4_send, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4, &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                      !end if
                      !if( nsize_recv > 0 ) then
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_rrecv(ini_recv:ini_recv+nsize_recv-1), nsize4_recv, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4, &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                      !end if
                   else
                      call MPI_Sendrecv(                       &
                           tmp_rsend(ini_send:), nsize4_send,  &
                           MPI_DOUBLE_PRECISION, dom_i4, 0_4,  &
                           tmp_rrecv(ini_recv:), nsize4_recv,  &
                           MPI_DOUBLE_PRECISION, dom_i4, 0_4,  &
                           PAR_COMM_TO_USE, status, istat4    )
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_COUPLING_NODE_EXCHANGE_RP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'MATRIX' ) then 
                !
                ! MATRIX 
                !
                kk = 0
                do ii = 1,commu % lscat_dim
                   ipoin = commu % lscat_perm(ii)
                   do kk = commu % matrix_ia(ii),commu % matrix_ia(ii+1)-1
                      jj = commu % matrix_ja(kk)
                      xx(1:ndofn,ipoin) = xx(1:ndofn,ipoin) + commu % matrix_aa(kk) * tmp_rrecv( jj:jj )
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_rrecv)
             deallocate(tmp_rsend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_COUPLING_NODE_EXCHANGE_RP

  subroutine PAR_SCALAR_PRODUCT_RP_2(n,xdoty,xx,yy,where)
    implicit none
    integer(ip),                   intent(in)  :: n
    real(rp),                      intent(out) :: xdoty
    real(rp),                      intent(in)  :: xx(n,*)
    real(rp),                      intent(in)  :: yy(n,*)
    character(*),                  intent(in)  :: where
    type(comm_data_par), pointer               :: commu
    integer(ip)                                :: ipoin,ii
    integer(4)                                 :: PAR_COMM_TO_USE

    xdoty = 0.0_rp          
 
    if( IPARALL ) then
#ifndef MPI_OFF
       !if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       !else 
       !   call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE,commu)  
       !end if
       if( ISLAVE ) then
          do ipoin = 1,commu % npoi1
             do ii = 1,n
                xdoty = xdoty + xx(ii,ipoin)*yy(ii,ipoin)
             end do
          end do
          do ipoin = commu % npoi2,commu % npoi3
             do ii = 1,n
                xdoty = xdoty + xx(ii,ipoin)*yy(ii,ipoin)
             end do
          end do          
       end if
       call PAR_SUM(xdoty,where,commu)
#endif
    else
       do ipoin = 1,npoin
          do ii = 1,n
             xdoty = xdoty + xx(ii,ipoin)*yy(ii,ipoin)
          end do
       end do
    end if

  end subroutine PAR_SCALAR_PRODUCT_RP_2

  !----------------------------------------------------------------------
  !
  ! Norm
  !
  !----------------------------------------------------------------------

  !subroutine PAR_NORM_s(xx,xnorm,where)
  !  implicit none
  !  real(rp),     intent(in)  :: xx
  !  real(rp),     intent(out) :: xnorm
  !  character(*),             :: where    
  !end subroutine PAR_NORM_s

  !----------------------------------------------------------------------
  !
  ! SUM FOR REALS
  !
  !----------------------------------------------------------------------

  subroutine PAR_SUM_RP_s(xx,where,commu)
    implicit none
    real(rp),                      intent(inout) :: xx
    character(*),        optional                :: where
    type(comm_data_par), optional, intent(in)    :: commu
    integer(ip)                                  :: nsize
    integer(4)                                   :: istat4,nsize4
    integer(4)                                   :: PAR_COMM_TO_USE
    real(rp)                                     :: yy

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(commu) ) then
          PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
       else if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       if( PAR_COMM_TO_USE /= -1 ) then
          nsize  = 1
          nsize4 = int(nsize,4)
          if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
             yy = 0.0_rp
          else
             yy = xx
          end if
          call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
               MPI_SUM,PAR_COMM_TO_USE,istat4)
          if( istat4 /= 0_4 ) call runend('PAR_SUM_RP_s: MPI ERROR')
       end if
    end if
#endif

  end subroutine PAR_SUM_RP_s

  subroutine PAR_SUM_RP_0(n,xx,where,wsynch)
    implicit none
    integer(ip),  intent(in)    :: n
    real(rp),     intent(inout) :: xx(n)
    character(*), optional      :: where
    character(*), optional      :: wsynch
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    logical(lg)                 :: asynch
    
#ifndef MPI_OFF    
    istat4 = 0_4
    if( IPARALL .and. n > 0 ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       asynch = .false.
       if( present(wsynch) ) then
          if( trim(wsynch) == 'NON BLOCKING' ) asynch = .true.
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy_non_blocking(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy_non_blocking(ii) = 0.0_rp
          end do
       else
          do ii = 1,nsize
             yy_non_blocking(ii) = xx(ii)
          end do          
       end if
       if( asynch ) then
#ifdef MPI3
          call MPI_IAllReduce(yy_non_blocking,xx,nsize4,MPI_DOUBLE_PRECISION,&
               MPI_SUM,PAR_COMM_TO_USE,ireq41(1),istat4)   
#else
          call runend('THUS FUNCTIONALITY REQUIRES MPI3: COMPILE ALYA WITH -DMPI3')
#endif
       else
          call MPI_AllReduce(yy_non_blocking,xx,nsize4,MPI_DOUBLE_PRECISION,&
               MPI_SUM,PAR_COMM_TO_USE,istat4)
          deallocate(yy_non_blocking)
       end if
       if( istat4 /= 0_4 ) call runend('PAR_SUM_RP_0: MPI ERROR')
    end if
#endif

  end subroutine PAR_SUM_RP_0

  subroutine PAR_SUM_RP_02(n1,n2,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n1
    integer(ip),  intent(in)    :: n2
    real(rp),     intent(inout) :: xx(n1,n2)
    character(*), optional      :: where
    integer(ip)                 :: ii,jj,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    real(rp),     allocatable   :: yy(:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    nsize  = n1*n2
    if( IPARALL .and. nsize > 0 ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize4 = int(nsize,4)
       allocate(yy(n1,n2))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = 1,n2
             do ii = 1,n1             
                yy(ii,jj) = 0.0_rp
             end do
          end do
       else
          do jj = 1,n2
             do ii = 1,n1             
                yy(ii,jj) = xx(ii,jj)
             end do
          end do    
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_RP_0: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_SUM_RP_02

  subroutine PAR_SUM_RP_1(xx,where,wsynch)
    implicit none
    real(rp),     pointer, intent(inout) :: xx(:)
    character(*),          optional      :: where
    character(*),          optional      :: wsynch
    integer(ip)                          :: ii,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    logical(lg)                          :: asynch

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       asynch = .false.
       if( present(wsynch) ) then
          if( trim(wsynch) == 'NON BLOCKING' ) asynch = .true.
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy_non_blocking(lbound(xx,1):ubound(xx,1)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy_non_blocking,1),ubound(yy_non_blocking,1)
             yy_non_blocking(ii) = 0.0_rp
          end do
       else
          do ii = lbound(yy_non_blocking,1),ubound(yy_non_blocking,1)
             yy_non_blocking(ii) = xx(ii)
          end do          
       end if
      if( asynch ) then
#ifdef MPI3
          call MPI_IAllReduce(yy_non_blocking,xx,nsize4,MPI_DOUBLE_PRECISION,&
               MPI_SUM,PAR_COMM_TO_USE,ireq41(1),istat4)         
#else
          call runend('THUS FUNCTIONALITY REQUIRES MPI3: COMPILE ALYA WITH -DMPI3')
#endif
       else
          call MPI_AllReduce(yy_non_blocking,xx,nsize4,MPI_DOUBLE_PRECISION,&
               MPI_SUM,PAR_COMM_TO_USE,istat4)
          deallocate( yy_non_blocking )
       end if
       if( istat4 /= 0_4 ) call runend('PAR_SUM_RP_1: MPI ERROR')
    end if
#endif

  end subroutine PAR_SUM_RP_1

  subroutine PAR_SUM_RP_2(xx,where)
    implicit none
    real(rp),     pointer, intent(inout) :: xx(:,:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,jj,nsize1,nsize2,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    real(rp),     pointer                :: yy(:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = 0.0_rp
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_RP_2: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_SUM_RP_2

  !----------------------------------------------------------------------
  !
  ! SUM FOR COMPLEX
  !
  !----------------------------------------------------------------------

  subroutine PAR_SUM_CX_s(xx,where)
    implicit none
    complex(rp),  intent(inout) :: xx
    character(*), optional      :: where
    integer(ip)                 :: nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    complex(rp)                 :: yy

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = 1
       nsize4 = int(nsize,4)
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          yy = 0.0_rp
       else
          yy = xx
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_CX_s: MPI ERROR')
    end if
#endif

  end subroutine PAR_SUM_CX_s

  subroutine PAR_SUM_CX_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    complex(rp),  intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    complex(rp),  allocatable   :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL .and. n > 0 ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy(ii) = 0.0_rp
          end do
       else
          do ii = 1,nsize
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_CX_0: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_SUM_CX_0

  subroutine PAR_SUM_CX_1(xx,where)
    implicit none
    complex(rp),  pointer, intent(inout) :: xx(:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    complex(rp),  pointer                :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = 0.0_rp
          end do
       else
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_CX_1: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_SUM_CX_1

  subroutine PAR_SUM_CX_2(xx,where)
    implicit none
    complex(rp),  pointer, intent(inout) :: xx(:,:)
    character(*), optional               :: where
    integer(ip)                          :: ii,jj,nsize1,nsize2,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    complex(rp),  pointer                :: yy(:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = 0.0_rp
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_CX_2: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_SUM_CX_2

  !----------------------------------------------------------------------
  !
  ! AVERAGE FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_AVERAGE_IP_s(xx,where)
    implicit none
    integer(ip),  intent(inout) :: xx
    character(*), optional      :: where
    integer(4)                  :: PAR_COMM_TO_USE
    integer(4)                  :: my_rank4,comm_size4

    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
    call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank4,comm_size4)
    call PAR_SUM(xx,where)
    if( IPARALL ) xx = xx / (max(1_ip,comm_size4-1_ip))

  end subroutine PAR_AVERAGE_IP_s

  subroutine PAR_AVERAGE_IP_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    integer(ip),  intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(4)                  :: PAR_COMM_TO_USE
    integer(4)                  :: my_rank4,comm_size4

    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
    call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank4,comm_size4)
    call PAR_SUM(n,xx,where)
    if( IPARALL ) xx(1:n) = xx(1:n) / (max(1_ip,comm_size4-1_ip))

  end subroutine PAR_AVERAGE_IP_0

  !----------------------------------------------------------------------
  !
  ! SUM FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_SUM_IP_s(xx,where)
    implicit none
    integer(ip),  intent(inout) :: xx
    character(*), optional      :: where
    integer(ip)                 :: nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(ip)                 :: yy

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = 1
       nsize4 = int(nsize,4)
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          yy = 0_ip
       else
          yy = xx
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_IP_s: MPI ERROR')
    end if
#endif

  end subroutine PAR_SUM_IP_s

  subroutine PAR_SUM_IP_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    integer(ip),  intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(ip),  allocatable   :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL .and. n > 0 ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy(ii) = 0_ip
          end do
       else
          do ii = 1,nsize
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_IP_0: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_SUM_IP_0

  subroutine PAR_SUM_IP_1(xx,where)
    implicit none
    integer(ip),     pointer, intent(inout) :: xx(:)
    character(*),    optional               :: where
    integer(ip)                             :: ii,nsize
    integer(4)                              :: istat4,nsize4
    integer(4)                              :: PAR_COMM_TO_USE
    integer(ip),     pointer                :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = 0_ip
          end do
       else
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_IP_1: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_SUM_IP_1

  subroutine PAR_SUM_IP_2(xx,where)
    implicit none
    integer(ip),     pointer, intent(inout) :: xx(:,:)
    character(*),    optional               :: where
    integer(ip)                             :: ii,jj,nsize1,nsize2,nsize
    integer(4)                              :: istat4,nsize4
    integer(4)                              :: PAR_COMM_TO_USE
    integer(ip),     pointer                :: yy(:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = 0_ip
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_IP_2: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_SUM_IP_2

  subroutine PAR_SUM_IP_3(xx,where)
    implicit none
    integer(ip),    pointer, intent(inout) :: xx(:,:,:)
    character(*),   optional               :: where
    integer(ip)                            :: ii,jj,kk,nsize1,nsize2,nsize3,nsize
    integer(4)                             :: istat4,nsize4
    integer(4)                             :: PAR_COMM_TO_USE
    integer(ip),    pointer                :: yy(:,:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize3 = size(xx,3)
       nsize  = nsize1*nsize2*nsize3
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2),lbound(xx,3):ubound(xx,3)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do kk = lbound(yy,3),ubound(yy,3)
             do jj = lbound(yy,2),ubound(yy,2)
                do ii = lbound(yy,1),ubound(yy,1)
                   yy(ii,jj,kk) = 0_ip
                end do
             end do
          end do
       else
          do kk = lbound(yy,3),ubound(yy,3)
             do jj = lbound(yy,2),ubound(yy,2)
                do ii = lbound(yy,1),ubound(yy,1)
                   yy(ii,jj,kk) = xx(ii,jj,kk)
                end do
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_IP_3: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_SUM_IP_3

  !----------------------------------------------------------------------
  !
  ! MAX FOR REALS
  !
  !----------------------------------------------------------------------

  subroutine PAR_MAX_RP_s(xx,where,rank_max_owner)
    implicit none
    real(rp),     intent(inout) :: xx
    character(*), optional      :: where
    integer(4),   optional      :: rank_max_owner
    integer(ip)                 :: nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(4)                  :: rank_min
    real(rp)                    :: yy

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = 1
       nsize4 = int(nsize,4)
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          yy = -huge(1.0_rp)
       else
          yy = xx
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_RP_s: MPI ERROR')
       if( present(rank_max_owner) ) then
          rank_min = huge(1_4)
          if( abs(yy-xx) < epsilon(1.0_rp) ) call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,rank_min)
          call MPI_AllReduce(rank_min,rank_max_owner,nsize4,MPI_INTEGER,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       end if
#endif
    else
       if( present(rank_max_owner) ) then
          rank_max_owner = -1
       end if
    end if

  end subroutine PAR_MAX_RP_s

  subroutine PAR_MAX_RP_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    real(rp),     intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    real(rp),     allocatable   :: yy(:)

    if( IPARALL .and. n > 0 ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy(ii) = -huge(1.0_rp)
          end do
       else
          do ii = 1,nsize
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_RP_0: MPI ERROR')
       deallocate(yy)
#endif
    end if

  end subroutine PAR_MAX_RP_0

  subroutine PAR_MAX_RP_1(xx,where)
    implicit none
    real(rp),     pointer, intent(inout) :: xx(:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    real(rp),     pointer                :: yy(:)

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = -huge(1.0_rp)
          end do
       else
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_RP_1: MPI ERROR')
       deallocate( yy )
#endif
    end if

  end subroutine PAR_MAX_RP_1

  subroutine PAR_MAX_RP_2(xx,where)
    implicit none
    real(rp),     pointer, intent(inout) :: xx(:,:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,jj,nsize1,nsize2,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    real(rp),     pointer                :: yy(:,:)

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = -huge(1.0_rp)
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_RP_2: MPI ERROR')
       deallocate( yy )
#endif
    end if

  end subroutine PAR_MAX_RP_2

  subroutine PAR_MAX_RP_3(xx,where)
    implicit none
    real(rp),     pointer, intent(inout) :: xx(:,:,:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,jj,kk,nsize1,nsize2,nsize3,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    real(rp),     pointer                :: yy(:,:,:)

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize3 = size(xx,3)
       nsize  = nsize1*nsize2*nsize3
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2),lbound(xx,3):ubound(xx,3)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do kk = lbound(yy,3),ubound(yy,3)
             do jj = lbound(yy,2),ubound(yy,2)
                do ii = lbound(yy,1),ubound(yy,1)
                   yy(ii,jj,kk) = -huge(1.0_rp)
                end do
             end do
          end do
       else
          do kk = lbound(yy,3),ubound(yy,3)
             do jj = lbound(yy,2),ubound(yy,2)
                do ii = lbound(yy,1),ubound(yy,1)
                   yy(ii,jj,kk) = xx(ii,jj,kk)
                end do
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_RP_2: MPI ERROR')
       deallocate( yy )
#endif
    end if

  end subroutine PAR_MAX_RP_3

  !----------------------------------------------------------------------
  !
  ! MAX FOR COMPLEX
  !
  !----------------------------------------------------------------------

  subroutine PAR_MAX_CX_s(xx,where)
    implicit none
    complex(rp),  intent(inout) :: xx
    character(*), optional      :: where
    integer(ip)                 :: nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    complex(rp)                 :: yy

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = 1
       nsize4 = int(nsize,4)
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          yy = -huge(1.0_rp)
       else
          yy = xx
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_CX_s: MPI ERROR')
#endif
    end if

  end subroutine PAR_MAX_CX_s

  subroutine PAR_MAX_CX_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    complex(rp),  intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    complex(rp),  allocatable   :: yy(:)

    if( IPARALL .and. n > 0 ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy(ii) = -huge(1.0_rp)
          end do
       else
          do ii = 1,nsize
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_CX_0: MPI ERROR')
       deallocate(yy)
#endif
    end if

  end subroutine PAR_MAX_CX_0

  subroutine PAR_MAX_CX_1(xx,where)
    implicit none
    complex(rp),  pointer, intent(inout) :: xx(:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    complex(rp),  pointer                :: yy(:)

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = -huge(1.0_rp)
          end do
       else
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_CX_1: MPI ERROR')
       deallocate( yy )
#endif
    end if

  end subroutine PAR_MAX_CX_1

  subroutine PAR_MAX_CX_2(xx,where)
    implicit none
    complex(rp),  pointer, intent(inout) :: xx(:,:)
    character(*), optional               :: where
    integer(ip)                          :: ii,jj,nsize1,nsize2,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    complex(rp),  pointer                :: yy(:,:)

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = -huge(1.0_rp)
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_CX_2: MPI ERROR')
       deallocate( yy )
#endif  
    end if

  end subroutine PAR_MAX_CX_2

  !----------------------------------------------------------------------
  !
  ! MAX FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_MAX_IP_s(xx,where)
    implicit none
    integer(ip),  intent(inout) :: xx
    character(*), optional      :: where
    integer(ip)                 :: nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(ip)                 :: yy

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = 1
       nsize4 = int(nsize,4)
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          yy = -huge(1_ip)
       else
          yy = xx
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_IP_s: MPI ERROR')
#endif
    end if

  end subroutine PAR_MAX_IP_s

  subroutine PAR_MAX_IP_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    integer(ip),  intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(ip),  allocatable   :: yy(:)

    if( IPARALL .and. n > 0 ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy(ii) = -huge(1_ip)
          end do
       else
          do ii = 1,nsize
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_IP_0: MPI ERROR')
       deallocate(yy)
#endif
    end if

  end subroutine PAR_MAX_IP_0

  subroutine PAR_MAX_IP_1(xx,where)
    implicit none
    integer(ip),     pointer, intent(inout) :: xx(:)
    character(*),    optional               :: where
    integer(ip)                             :: ii,nsize
    integer(4)                              :: istat4,nsize4
    integer(4)                              :: PAR_COMM_TO_USE
    integer(ip),     pointer                :: yy(:)

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = -huge(1_ip)
          end do
       else
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_IP_1: MPI ERROR')
       deallocate( yy )
#endif
    end if

  end subroutine PAR_MAX_IP_1

  subroutine PAR_MAX_IP_2(xx,where)
    implicit none
    integer(ip),     pointer, intent(inout) :: xx(:,:)
    character(*),    optional               :: where
    integer(ip)                             :: ii,jj,nsize1,nsize2,nsize
    integer(4)                              :: istat4,nsize4
    integer(4)                              :: PAR_COMM_TO_USE
    integer(ip),     pointer                :: yy(:,:)

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = -huge(1_ip)
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_IP_2: MPI ERROR')
       deallocate( yy )
#endif
    end if

  end subroutine PAR_MAX_IP_2

  !----------------------------------------------------------------------
  !
  ! MIN FOR REALS
  !
  !----------------------------------------------------------------------

  subroutine PAR_MIN_RP_s(xx,where,rank_max_owner)
    implicit none
    real(rp),     intent(inout) :: xx
    character(*), optional      :: where
    integer(4),   optional      :: rank_max_owner
    integer(ip)                 :: nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(4)                  :: rank_min
    real(rp)                    :: yy

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = 1
       nsize4 = int(nsize,4)
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          yy =  huge(1.0_rp)
       else
          yy = xx
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_RP_s: MPI ERROR')
       if( present(rank_max_owner) ) then
          rank_min = huge(1_4)
          if( abs(yy-xx) < epsilon(1.0_rp) ) call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,rank_min)
          call MPI_AllReduce(rank_min,rank_max_owner,nsize4,MPI_INTEGER,&
               MPI_MIN,PAR_COMM_TO_USE,istat4)
       end if
#endif
    else
       if( present(rank_max_owner) ) then
          rank_max_owner = -1
       end if
    end if

  end subroutine PAR_MIN_RP_s

  subroutine PAR_MIN_RP_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    real(rp),     intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    real(rp),     allocatable   :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL .and. n > 0 ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy(ii) =  huge(1.0_rp)
          end do
       else
          do ii = 1,nsize
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_RP_0: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_MIN_RP_0

  subroutine PAR_MIN_RP_1(xx,where)
    implicit none
    real(rp),     pointer, intent(inout) :: xx(:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    real(rp),     pointer                :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) =  huge(1.0_rp)
          end do
       else
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_RP_1: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_MIN_RP_1

  subroutine PAR_MIN_RP_2(xx,where)
    implicit none
    real(rp),     pointer, intent(inout) :: xx(:,:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,jj,nsize1,nsize2,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    real(rp),     pointer                :: yy(:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) =  huge(1.0_rp)
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_PRECISION,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_RP_2: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_MIN_RP_2

  !----------------------------------------------------------------------
  !
  ! MIN FOR COMPLEX
  !
  !----------------------------------------------------------------------

  subroutine PAR_MIN_CX_s(xx,where)
    implicit none
    complex(rp),  intent(inout) :: xx
    character(*), optional      :: where
    integer(ip)                 :: nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    complex(rp)                 :: yy

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = 1
       nsize4 = int(nsize,4)
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          yy =  huge(1.0_rp)
       else
          yy = xx
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_CX_s: MPI ERROR')
    end if
#endif

  end subroutine PAR_MIN_CX_s

  subroutine PAR_MIN_CX_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    complex(rp),  intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    complex(rp),  allocatable   :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL .and. n > 0 ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy(ii) =  huge(1.0_rp)
          end do
       else
          do ii = 1,nsize
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_CX_0: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_MIN_CX_0

  subroutine PAR_MIN_CX_1(xx,where)
    implicit none
    complex(rp),  pointer, intent(inout) :: xx(:)
    character(*),          optional      :: where
    integer(ip)                          :: ii,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    complex(rp),  pointer                :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) =  huge(1.0_rp)
          end do
       else
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_CX_1: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_MIN_CX_1

  subroutine PAR_MIN_CX_2(xx,where)
    implicit none
    complex(rp),  pointer, intent(inout) :: xx(:,:)
    character(*), optional               :: where
    integer(ip)                          :: ii,jj,nsize1,nsize2,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    complex(rp),  pointer                :: yy(:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) =  huge(1.0_rp)
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,MPI_DOUBLE_COMPLEX,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_CX_2: MPI ERROR')
       deallocate( yy )
    end if
#endif

  end subroutine PAR_MIN_CX_2

  !----------------------------------------------------------------------
  !
  ! MIN FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_MIN_IP_s(xx,where)
    implicit none
    integer(ip),  intent(inout) :: xx
    character(*), optional      :: where
    integer(ip)                 :: nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(ip)                 :: yy

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = 1
       nsize4 = int(nsize,4)
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          yy =  huge(1_ip)
       else
          yy = xx
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_IP_S: MPI ERROR')
    end if
#endif

  end subroutine PAR_MIN_IP_s

  subroutine PAR_MIN_IP_0(n,xx,where)
    implicit none
    integer(ip),  intent(in)    :: n
    integer(ip),  intent(inout) :: xx(n)
    character(*), optional      :: where
    integer(ip)                 :: ii,nsize
    integer(4)                  :: istat4,nsize4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(ip),  allocatable   :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL .and. n > 0 ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = n  
       nsize4 = int(nsize,4)
       allocate(yy(n))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = 1,nsize
             yy(ii) =  huge(1_ip)
          end do
       else
          do ii = 1,nsize
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MIN_IP_0: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_MIN_IP_0

  subroutine PAR_MIN_IP_1(xx,where)
    implicit none
    integer(ip),     pointer, intent(inout) :: xx(:)
    character(*),    optional               :: where
    integer(ip)                             :: ii,nsize
    integer(4)                              :: istat4,nsize4
    integer(4)                              :: PAR_COMM_TO_USE
    integer(ip),     pointer                :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize  = size(xx,1)
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1)) )
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) =  huge(1_ip)
          end do
       else
          do ii = lbound(yy,1),ubound(yy,1)
             yy(ii) = xx(ii)
          end do          
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       deallocate( yy )
       if( istat4 /= 0_4 ) call runend('PAR_MIN_IP_1: MPI ERROR')
    end if
#endif

  end subroutine PAR_MIN_IP_1

  subroutine PAR_MIN_IP_2(xx,where)
    implicit none
    integer(ip),     pointer, intent(inout) :: xx(:,:)
    character(*),    optional               :: where
    integer(ip)                             :: ii,jj,nsize1,nsize2,nsize
    integer(4)                              :: istat4,nsize4
    integer(4)                              :: PAR_COMM_TO_USE
    integer(ip),     pointer                :: yy(:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE)  
       end if
       nsize1 = size(xx,1)
       nsize2 = size(xx,2)
       nsize  = nsize1*nsize2
       nsize4 = int(nsize,4)
       allocate( yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2)))
       if( PAR_MY_CODE_RANK == 0 .and. trim(where) /= 'IN THE UNIVERSE' ) then
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) =  huge(1_ip)
             end do
          end do
       else
          do jj = lbound(yy,2),ubound(yy,2)
             do ii = lbound(yy,1),ubound(yy,1)
                yy(ii,jj) = xx(ii,jj)
             end do
          end do
       end if
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MIN,PAR_COMM_TO_USE,istat4)
       deallocate( yy )
       if( istat4 /= 0_4 ) call runend('PAR_MIN_IP_2: MPI ERROR')
    end if
#endif

  end subroutine PAR_MIN_IP_2
  !
  ! SUM for integers in the Alya world (PAR_COMM_WORLD) including masters
  !
  subroutine PAR_SUM_ALL_IP_1(xx)

    implicit none
    integer(ip),  pointer, intent(inout) :: xx(:)
    integer(ip)                          :: ii,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    integer(ip),  pointer                :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR('IN THE WORLD',PAR_COMM_TO_USE)
       nsize = size(xx,1)
       nsize4 = int(nsize,4)
       allocate(yy(lbound(xx,1):ubound(xx,1)))
       do ii =  lbound(yy,1), ubound(yy,1)
          yy(ii) = xx(ii)
       end do
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_ALL_IP_1: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_SUM_ALL_IP_1

  subroutine PAR_SUM_ALL_IP_3(xx)

    implicit none
    integer(ip),  pointer, intent(inout) :: xx(:,:,:)
    integer(ip)                          :: ii,jj,kk,nsize1,nsize2,nsize3,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    integer(ip),  pointer                :: yy(:,:,:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR('IN THE WORLD',PAR_COMM_TO_USE)
       nsize1  = size(xx,1)  
       nsize2  = size(xx,2)
       nsize3  = size(xx,3)
       nsize   = nsize1 * nsize2 * nsize3
       nsize4 = int(nsize,4)
       allocate(yy(lbound(xx,1):ubound(xx,1),lbound(xx,2):ubound(xx,2),lbound(xx,3):ubound(xx,3)))
       do kk = lbound(yy,3), ubound(yy,3)
          do jj = lbound(yy,2), ubound(yy,2)
             do ii =  lbound(yy,1), ubound(yy,1)
                yy(ii,jj,kk) = xx(ii,jj,kk)
             end do
          end do
       end do
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_SUM,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_ALL_IP_3: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_SUM_ALL_IP_3
  !
  ! MAX for integers in the Alya world (PAR_COMM_WORLD) including masters
  !
  subroutine PAR_MAX_ALL_IP_s(xx)

    implicit none
    integer(ip),  intent(inout) :: xx
    integer(4)                  :: istat4
    integer(4)                  :: PAR_COMM_TO_USE
    integer(ip)                 :: yy

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR('IN THE WORLD',PAR_COMM_TO_USE)
       yy = xx
       call MPI_AllReduce(yy,xx,1_ip,PAR_INTEGER,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_MAX_ALL_IP_s: MPI ERROR')
    end if
#endif
  end subroutine PAR_MAX_ALL_IP_s

  subroutine PAR_MAX_ALL_IP_1(xx)

    implicit none
    integer(ip),  pointer, intent(inout) :: xx(:)
    integer(ip)                          :: ii,nsize
    integer(4)                           :: istat4,nsize4
    integer(4)                           :: PAR_COMM_TO_USE
    integer(ip),  pointer                :: yy(:)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR('IN THE WORLD',PAR_COMM_TO_USE)
       nsize = size(xx,1)
       nsize4 = int(nsize,4)
       allocate(yy(lbound(xx,1):ubound(xx,1)))
       do ii =  lbound(yy,1), ubound(yy,1)
          yy(ii) = xx(ii)
       end do
       call MPI_AllReduce(yy,xx,nsize4,PAR_INTEGER,&
            MPI_MAX,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_SUM_ALL_IP_1: MPI ERROR')
       deallocate(yy)
    end if
#endif

  end subroutine PAR_MAX_ALL_IP_1

  !----------------------------------------------------------------------
  !
  ! Gather to rank = 0
  !
  !----------------------------------------------------------------------
  
  subroutine PAR_GATHER_CHARACTER(character_in,character_out,where)
    implicit none
    character(*),          intent(in)  :: character_in
    character(*), pointer, intent(out) :: character_out(:)
    character(*),          intent(in)  :: where
    character(1), pointer              :: dummi(:)
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: l_character_in
    integer(4)                         :: l_character_out
    integer(4)                         :: istat4

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)

       l_character_in  = len(character_in)
       l_character_out = len(character_out(1))

       if( l_character_in /= l_character_out ) then
          call runend('PAR_GATHER: WRONG CHARACTER LENGTH')
       else
          if( .not. associated(character_out) ) then
             allocate(dummi(l_character_in))
             call MPI_Gather(character_in, l_character_in, MPI_CHARACTER,&
                  &          dummi,        l_character_in, MPI_CHARACTER,&
                  &          0_4,PAR_COMM_TO_USE,istat4)
             deallocate(dummi)
          else
             call MPI_Gather(character_in, l_character_in, MPI_CHARACTER,&
                  &          character_out,l_character_out,MPI_CHARACTER,&
                  &          0_4,PAR_COMM_TO_USE,istat4)
          end if
          if( istat4 /= 0_4 ) call runend('PAR_GATHER_CHARACTER: MPI ERROR')
       end if
    end if
#endif

  end subroutine PAR_GATHER_CHARACTER

  subroutine PAR_GATHER_IP_s4(sendbuf,recvbuf,where)
    implicit none
    integer(4),            intent(in)  :: sendbuf
    integer(4),   pointer, intent(out) :: recvbuf(:)
    character(*),          intent(in)  :: where
    integer(ip)                        :: dummi(2)
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: istat4,my_rank
    integer(ip)                        :: rl

    if( ISEQUEN ) then
       recvbuf(lbound(recvbuf)) = sendbuf
    else if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       call MPI_Comm_rank(PAR_COMM_TO_USE,my_rank,istat4)
       if( .not. associated(recvbuf) ) then
          if( my_rank == 0 ) call runend('PAR_GATHER_IP_s: NOT ASSOCIATED')
          call MPI_Gather(sendbuf,      1_4, MPI_INTEGER,&
               &          dummi,        1_4, MPI_INTEGER,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       else
          rl = lbound(recvbuf,1)
          call MPI_Gather(sendbuf,      1_4, MPI_INTEGER,&
               &          recvbuf(rl:), 1_4, MPI_INTEGER,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       end if
       if( istat4 /= 0_4 ) call runend('PAR_GATHER_IP_s: MPI ERROR')
#endif
    end if

  end subroutine PAR_GATHER_IP_s4

  subroutine PAR_GATHER_IP_s8(sendbuf,recvbuf,where)
    implicit none
    integer(8),            intent(in)  :: sendbuf
    integer(8),   pointer, intent(out) :: recvbuf(:)
    character(*),          intent(in)  :: where
    integer(ip)                        :: dummi(2)
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: istat4,my_rank
    integer(ip)                        :: rl

    if( ISEQUEN ) then
       recvbuf(lbound(recvbuf)) = sendbuf
    else if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       call MPI_Comm_rank(PAR_COMM_TO_USE,my_rank,istat4)
       if( .not. associated(recvbuf) ) then
          if( my_rank == 0 ) call runend('PAR_GATHER_IP_s: NOT ASSOCIATED')
          call MPI_Gather(sendbuf,      1_4, MPI_INTEGER8,&
               &          dummi,        1_4, MPI_INTEGER8,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       else
          rl = lbound(recvbuf,1)
          call MPI_Gather(sendbuf,      1_4, MPI_INTEGER8,&
               &          recvbuf(rl:), 1_4, MPI_INTEGER8,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       end if
       if( istat4 /= 0_4 ) call runend('PAR_GATHER_IP_s: MPI ERROR')
#endif
    end if

  end subroutine PAR_GATHER_IP_s8

  subroutine PAR_GATHER_IP_14(sendbuf,recvbuf,where)
    implicit none
    integer(4),            intent(in)  :: sendbuf(:)
    integer(4),  pointer,  intent(out) :: recvbuf(:)
    character(*),          intent(in)  :: where
    integer(ip)                        :: dummi(2)
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: istat4,my_rank
    integer(4)                         :: sendcount4
    integer(ip)                        :: sl,rl

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       call MPI_Comm_rank(PAR_COMM_TO_USE,my_rank,istat4)
       
       sendcount4 = size(sendbuf)
       sl = lbound(sendbuf,1)
       if( .not. associated(recvbuf) ) then
          if( my_rank == 0 ) call runend('PAR_GATHER_IP_s: NOT ASSOCIATED')
          call MPI_Gather(sendbuf(sl:), sendcount4,MPI_INTEGER,&
               &          dummi,        sendcount4,MPI_INTEGER,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       else
          rl = lbound(recvbuf,1)
          call MPI_Gather(sendbuf(sl:), sendcount4,MPI_INTEGER,&
               &          recvbuf(rl:), sendcount4,MPI_INTEGER,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       end if
       if( istat4 /= 0_4 ) call runend('PAR_GATHER_IP_1: MPI ERROR')
    end if
#endif

  end subroutine PAR_GATHER_IP_14

  subroutine PAR_GATHER_IP_18(sendbuf,recvbuf,where)
    implicit none
    integer(8),            intent(in)  :: sendbuf(:)
    integer(8),  pointer,  intent(out) :: recvbuf(:)
    character(*),          intent(in)  :: where
    integer(ip)                        :: dummi(2)
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: istat4,my_rank
    integer(4)                         :: sendcount4
    integer(ip)                        :: sl,rl

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       call MPI_Comm_rank(PAR_COMM_TO_USE,my_rank,istat4)
       
       sendcount4 = size(sendbuf)
       sl = lbound(sendbuf,1)
       if( .not. associated(recvbuf) ) then
          if( my_rank == 0 ) call runend('PAR_GATHER_IP_s: NOT ASSOCIATED')
          call MPI_Gather(sendbuf(sl:), sendcount4,MPI_INTEGER8,&
               &          dummi,        sendcount4,MPI_INTEGER8,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       else
          rl = lbound(recvbuf,1)
          call MPI_Gather(sendbuf(sl:), sendcount4,MPI_INTEGER8,&
               &          recvbuf(rl:), sendcount4,MPI_INTEGER8,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       end if
       if( istat4 /= 0_4 ) call runend('PAR_GATHER_IP_1: MPI ERROR')
    end if
#endif

  end subroutine PAR_GATHER_IP_18

  subroutine PAR_GATHER_RP_1(sendbuf,recvbuf,where)
    implicit none
    real(rp),              intent(in)  :: sendbuf(:)
    real(rp),     pointer, intent(out) :: recvbuf(:)
    character(*),          intent(in)  :: where
    integer(ip)                        :: dummi(2)
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: istat4,my_rank
    integer(4)                         :: sendcount4

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       call MPI_Comm_rank(PAR_COMM_TO_USE,my_rank,istat4)
       
       sendcount4 = size(sendbuf)
       if( .not. associated(recvbuf) ) then
          if( my_rank == 0 ) call runend('PAR_GATHER_IP_s: NOT ASSOCIATED')
          call MPI_Gather(sendbuf, sendcount4,MPI_DOUBLE_PRECISION,&
               &          dummi,   sendcount4,MPI_DOUBLE_PRECISION,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       else
          call MPI_Gather(sendbuf, sendcount4,MPI_DOUBLE_PRECISION,&
               &          recvbuf, sendcount4,MPI_DOUBLE_PRECISION,&
               &          0_4,PAR_COMM_TO_USE,istat4)
       end if
       if( istat4 /= 0_4 ) call runend('PAR_GATHER_RP_1: MPI ERROR')
    end if
#endif

  end subroutine PAR_GATHER_RP_1

  subroutine PAR_GATHER_RP_12(sendbuf,recvbuf,where)
    implicit none
    real(rp),              intent(in)  :: sendbuf(:)
    real(rp),     pointer, intent(out) :: recvbuf(:,:)
    character(*),          intent(in)  :: where
    integer(ip)                        :: dummi(2)
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: istat4,my_rank
    integer(4)                         :: sendcount4
    integer(ip)                        :: rl1,rl2

#ifndef MPI_OFF
    istat4 = 0_4
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
    call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank)

    sendcount4 = size(sendbuf)
    if( .not. associated(recvbuf) ) then
       if( my_rank == 0 ) call runend('PAR_GATHER_IP_s: NOT ASSOCIATED')
       call MPI_Gather(sendbuf,  sendcount4,MPI_DOUBLE_PRECISION,&
            &          dummi,    sendcount4,MPI_DOUBLE_PRECISION,&
            &          0_4,PAR_COMM_TO_USE,istat4)
    else
       rl1 = lbound(recvbuf,1)
       rl2 = lbound(recvbuf,2)
       call MPI_Gather(sendbuf,           sendcount4,MPI_DOUBLE_PRECISION,&
            &          recvbuf(rl1:,rl2:),sendcount4,MPI_DOUBLE_PRECISION,&
            &          0_4,PAR_COMM_TO_USE,istat4)
    end if
    if( istat4 /= 0_4 ) call runend('PAR_GATHER_RP_12: MPI ERROR')
#endif

  end subroutine PAR_GATHER_RP_12

  !----------------------------------------------------------------------
  !
  ! Scatter from rank = 0
  !
  !----------------------------------------------------------------------
  
  subroutine PAR_SCATTER_IP_s(xx_in,xx_out,where)
    implicit none
    integer(ip),  pointer, intent(in)  :: xx_in(:)
    integer(ip),           intent(out) :: xx_out
    character(*),          intent(in)  :: where
    integer(ip)                        :: dummi(2)
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: istat4

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)   
       if( .not. associated(xx_in) ) then
          call MPI_Scatter(dummi,1_4,PAR_INTEGER,xx_out,1_4,PAR_INTEGER,0_4,PAR_COMM_TO_USE,istat4) 
       else
          call MPI_Scatter(xx_in,1_4,PAR_INTEGER,xx_out,1_4,PAR_INTEGER,0_4,PAR_COMM_TO_USE,istat4)     
       end if
       if( istat4 /= 0_4 ) call runend('PAR_SCATTER_IP_s: MPI ERROR 1')
    end if
#endif

  end subroutine PAR_SCATTER_IP_s

  subroutine PAR_SCATTERV_IP_1(sendbuf,recvbuf,sendcount4,where,displs4)
    implicit none    
    integer(ip),  pointer, intent(in)           :: sendbuf(:)           !< Send buffer
    integer(ip),  pointer, intent(out)          :: recvbuf(:)           !< Recv buffer
    integer(4),   pointer, intent(in)           :: sendcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: recvcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(ip)                                 :: recvbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(4)                                  :: root_rank4
    integer(4)                                  :: my_rank
    integer(ip)                                 :: rl,sl,scl,dl,jpart
    integer(4),   pointer                       :: sendcount4_tmp(:)
    integer(4),   target                        :: sendcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       root_rank4      = 0_4
       sendcount4_null = 0_4
       displs4_null    = 0_4
       recvcount4      = 0_4

       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)

       if( associated(recvbuf) ) then
 
          recvcount4 = size(recvbuf)
          rl         = lbound(recvbuf,1_ip)

       end if

       if( associated(sendbuf)    ) sl = lbound(sendbuf,1_ip)

!       if( associated(sendcount4) ) then
       if( PAR_MY_WORLD_RANK==0_4 ) then

          scl = lbound(sendcount4,1_4)
          sendcount4_tmp => sendcount4(scl:)

       else          

          sendcount4_tmp => sendcount4_null

       end if

       if( present(displs4) ) then

          if( associated(displs4) ) then

             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)

          else          

             displs4_tmp => displs4_null

          end if

          if( recvcount4 == 0_4 ) then

             if( associated(sendbuf) ) then

                call MPI_SCATTERV(sendbuf, sendcount4_tmp, displs4_tmp, PAR_INTEGER,&
                     &            recvbuf_tmp, recvcount4,              PAR_INTEGER,&
                     &            root_rank4, PAR_COMM_TO_USE, istat4)

             else

                call MPI_SCATTERV(sendbuf_tmp, sendcount4_tmp, displs4_tmp, PAR_INTEGER,&
                     &            recvbuf_tmp, recvcount4,                  PAR_INTEGER,&
                     &            root_rank4,  PAR_COMM_TO_USE, istat4)              

             end if

          else

             if( associated(sendbuf) ) then

                call MPI_SCATTERV(sendbuf, sendcount4_tmp, displs4_tmp, PAR_INTEGER,&
                     &            recvbuf, recvcount4,                  PAR_INTEGER,&
                     &            root_rank4, PAR_COMM_TO_USE, istat4)

             else

                call MPI_SCATTERV(sendbuf_tmp, sendcount4_tmp, displs4_tmp, PAR_INTEGER,&
                     &            recvbuf, recvcount4,                      PAR_INTEGER,&
                     &            root_rank4, PAR_COMM_TO_USE, istat4)

             end if

          end if

       else

          call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank,comm_size)

          !if( my_rank /= root_rank4 ) then

             allocate( my_displs4(0_4:comm_size-1_4) )
             jpart = lbound(sendcount4,1_4)
             my_displs4(0_4) = 0_4
             do ipart = 1,comm_size-1
                
                my_displs4(ipart) = my_displs4(ipart-1) + sendcount4(jpart)
                jpart = jpart + 1

             end do
             
          !else
             
          !   allocate( my_displs4(1_4) )
          !   my_displs4(1_4) = 0_4

          !end if

          if( recvcount4 == 0_4 ) then

             if( associated(sendbuf) ) then

                call MPI_SCATTERV(sendbuf, sendcount4_tmp, my_displs4, PAR_INTEGER,&
                     &            recvbuf_tmp, recvcount4,             PAR_INTEGER,&
                     &            root_rank4, PAR_COMM_TO_USE, istat4)

             else 

                call MPI_SCATTERV(sendbuf_tmp, sendcount4_tmp, my_displs4, PAR_INTEGER,&
                     &            recvbuf_tmp, recvcount4,                 PAR_INTEGER,&
                     &            root_rank4, PAR_COMM_TO_USE, istat4)             

             end if

          else

             if( associated(sendbuf) ) then

                call MPI_SCATTERV(sendbuf, sendcount4_tmp, my_displs4, PAR_INTEGER,&
                     &            recvbuf, recvcount4,                 PAR_INTEGER,&
                     &            root_rank4, PAR_COMM_TO_USE, istat4)

             else

                call MPI_SCATTERV(sendbuf_tmp, sendcount4_tmp, my_displs4, PAR_INTEGER,&
                     &            recvbuf, recvcount4,                     PAR_INTEGER,&
                     &            root_rank4, PAR_COMM_TO_USE, istat4)

             end if

          end if

          deallocate( my_displs4 ) 

       end if

       if( istat4 /= 0 ) call runend('PAR_SCATTERV_RP_1: MPI ERROR')

    end if

#endif

  end subroutine PAR_SCATTERV_IP_1

  !----------------------------------------------------------------------
  !
  ! RANK and SIZE of a communicator
  !
  !----------------------------------------------------------------------
  
  subroutine PAR_COMM_RANK_AND_SIZE_4(PAR_COMM_ORIGINAL,my_rank,comm_size)
    implicit none
    integer(4),            intent(in)  :: PAR_COMM_ORIGINAL  !< Communicator
    integer(4),            intent(out) :: my_rank            !< Rank in this communicator
    integer(4), optional,  intent(out) :: comm_size          !< Size of this communicator
    integer(4)                         :: istat4

#ifndef MPI_OFF
    istat4 = 0_4
    if( present(comm_size) ) then
       call MPI_Comm_size(PAR_COMM_ORIGINAL,comm_size,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_4: MPI ERROR 1')
    end if
    call MPI_Comm_rank(PAR_COMM_ORIGINAL,my_rank,istat4)
    if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_4: MPI ERROR 2')
#else
    my_rank = 0
    if( present(comm_size) ) comm_size = 0
#endif

  end subroutine PAR_COMM_RANK_AND_SIZE_4
  subroutine PAR_COMM_RANK_AND_SIZE_8(PAR_COMM_ORIGINAL,my_rank,comm_size)
    implicit none
    integer(8),            intent(in)  :: PAR_COMM_ORIGINAL !< Communicator
    integer(8),            intent(out) :: my_rank           !< Rank in this communicator
    integer(8), optional,  intent(out) :: comm_size         !< Size of this communicator
    integer(4)                         :: istat4
    integer(4)                         :: my_rank4
    integer(4)                         :: comm_size4
    integer(4)                         :: PAR_COMM_ORIGINAL4

#ifndef MPI_OFF
    istat4 = 0_4
    PAR_COMM_ORIGINAL4 = int(PAR_COMM_ORIGINAL,4)
    if( present(comm_size) ) then
       call MPI_Comm_size(PAR_COMM_ORIGINAL4,comm_size4,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_8: MPI ERROR 1')
       comm_size = int(comm_size4,8)
    end if
    call MPI_Comm_rank(PAR_COMM_ORIGINAL4,my_rank4,istat4)
    if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_8: MPI ERROR 2')
    my_rank = int(my_rank4,8)
#else
    my_rank = 0
    if( present(comm_size) ) comm_size = 0
#endif

  end subroutine PAR_COMM_RANK_AND_SIZE_8

  subroutine PAR_COMM_RANK_AND_SIZE_4W(my_rank,comm_size,where)
    implicit none    
    integer(4),             intent(out) :: my_rank           !< Rank in this communicator
    integer(4),             intent(out) :: comm_size         !< Size of this communicator
    character(*),           intent(in)  :: where             !< Where
    integer(4)                          :: istat4
    integer(4)                          :: PAR_COMM_ORIGINAL

#ifndef MPI_OFF
    istat4 = 0_4
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_ORIGINAL)
    call MPI_Comm_size(PAR_COMM_ORIGINAL,comm_size,istat4)
    if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_4W: MPI ERROR 1')
    call MPI_Comm_rank(PAR_COMM_ORIGINAL,my_rank,istat4)
    if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_4W: MPI ERROR 2')
#else
    my_rank   = 0
    comm_size = 0
#endif

  end subroutine PAR_COMM_RANK_AND_SIZE_4W

  subroutine PAR_COMM_RANK_AND_SIZE_41W(my_rank,where)
    implicit none    
    integer(4),             intent(out) :: my_rank           !< Rank in this communicator
    character(*),           intent(in)  :: where             !< Where
    integer(4)                          :: istat4
    integer(4)                          :: PAR_COMM_ORIGINAL

    if( IPARALL ) then
#ifndef MPI_OFF
       istat4 = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_ORIGINAL)
       call MPI_Comm_rank(PAR_COMM_ORIGINAL,my_rank,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_4W: MPI ERROR 2')
#endif
    else
       my_rank = -1
    end if

  end subroutine PAR_COMM_RANK_AND_SIZE_41W

  subroutine PAR_COMM_RANK_AND_SIZE_8W(my_rank,comm_size,where)
    implicit none    
    integer(8),   intent(out) :: my_rank           !< Rank in this communicator
    integer(8),   intent(out) :: comm_size         !< Size of this communicator
    character(*), intent(in)  :: where             !< Where
    integer(4)                :: istat4
    integer(4)                :: PAR_COMM_ORIGINAL

#ifndef MPI_OFF
    istat4 = 0_4
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_ORIGINAL)
    call MPI_Comm_size(PAR_COMM_ORIGINAL,comm_size,istat4)
    if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_8W: MPI ERROR 1')
    call MPI_Comm_rank(PAR_COMM_ORIGINAL,my_rank,istat4)
    if( istat4 /= 0_4 ) call runend('PAR_COMM_RANK_AND_SIZE_8W: MPI ERROR 2')
#else
    my_rank   = 0
    comm_size = 0
#endif

  end subroutine PAR_COMM_RANK_AND_SIZE_8W

  !----------------------------------------------------------------------
  !
  ! SPLIT COMMUNICATOR
  ! IKEY should have the rank
  !
  !----------------------------------------------------------------------
  
  subroutine PAR_COMM_SPLIT4(icolor,PAR_COMM_FINAL,my_new_rank,where)
    implicit none
    integer(4),   intent(in)           :: icolor
    integer(4),   intent(out)          :: PAR_COMM_FINAL
    integer(4),   intent(out)          :: my_new_rank
    character(*), intent(in), optional :: where
    integer(4)                         :: ikey4,istat4,jcolor4
    integer(4)                         :: PAR_COMM_INITIAL

#ifndef MPI_OFF
    istat4 = 0_4
    if( icolor == 0 ) then
       jcolor4 = MPI_UNDEFINED 
    else
       jcolor4 = int(icolor,4)
    end if
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_INITIAL)
    call MPI_COMM_RANK(PAR_COMM_INITIAL,ikey4,istat4)

    if( istat4 /= 0_4 ) call runend('PAR_COMM_SPLIT4: MPI ERROR 1')
    call MPI_COMM_SPLIT(PAR_COMM_INITIAL,jcolor4,ikey4,PAR_COMM_FINAL,istat4)
    if( istat4 /= 0_4 ) call runend('PAR_COMM_SPLIT4: MPI ERROR 2')
    if( PAR_COMM_FINAL /= MPI_COMM_NULL ) then 
       call MPI_COMM_RANK (PAR_COMM_FINAL,my_new_rank,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_COMM_SPLIT4: MPI ERROR 3')
    else
       my_new_rank = -1
    end if
#else
    PAR_COMM_FINAL = 0
#endif

  end subroutine PAR_COMM_SPLIT4

  subroutine PAR_COMM_SPLIT8(icolor,PAR_COMM_FINAL,my_new_rank,where)
    implicit none
    integer(8),   intent(in)           :: icolor
    integer(8),   intent(out)          :: PAR_COMM_FINAL
    integer(8),   intent(out)          :: my_new_rank
    character(*), intent(in), optional :: where
    integer(4)                         :: my_new_rank4
    integer(4)                         :: ikey4,istat4,jcolor4
    integer(4)                         :: PAR_COMM_INITIAL4
    integer(4)                         :: PAR_COMM_FINAL4

#ifndef MPI_OFF
    istat4 = 0_4
    if( icolor == 0 ) then
       jcolor4 = MPI_UNDEFINED 
    else
       jcolor4 = int(icolor,4)
    end if

    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_INITIAL4)
    call MPI_COMM_RANK (PAR_COMM_INITIAL4,ikey4,istat4)

    if( istat4 /= 0_4 ) call runend('PAR_COMM_SPLIT8: MPI ERROR 1')
    call MPI_COMM_SPLIT(PAR_COMM_INITIAL4,jcolor4,ikey4,PAR_COMM_FINAL4,istat4)
    if( istat4 /= 0_4 ) call runend('PAR_COMM_SPLIT8: MPI ERROR 2')
    if( PAR_COMM_FINAL4 /= MPI_COMM_NULL ) then 
       call MPI_COMM_RANK (PAR_COMM_FINAL4,my_new_rank4,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_COMM_SPLIT8: MPI ERROR 3')
       PAR_COMM_FINAL = int(PAR_COMM_FINAL4,8)
       my_new_rank    = int(my_new_rank4,8)
    else
       my_new_rank    = -1
    end if
#else
    PAR_COMM_FINAL = 0
#endif

  end subroutine PAR_COMM_SPLIT8

  !----------------------------------------------------------------------
  !
  ! BROADCAST FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_BROADCAST_IP_s(xx,where)
    implicit none
    integer(ip),  intent(inout) :: xx
    character(*), optional      :: where
    integer(ip)                 :: n
    integer(ip)                 :: yy(2)
    if( ISEQUEN ) return
    yy(1) = xx
    n     = 1
    call PAR_BROADCAST_IP(n,yy,where)
    xx    = yy(1)
  end subroutine PAR_BROADCAST_IP_s

  subroutine PAR_BROADCAST_IP_1(xx,where)
    implicit none
    integer(ip),  pointer, intent(inout) :: xx(:)
    character(*),          optional      :: where
    integer(ip)                          :: n
    if( ISEQUEN ) return
   
    if( associated(xx) ) then
       n = size(xx)
       if( n > 0 ) then
          if( present(where) ) then
             call PAR_BROADCAST_IP(n,xx,where)
          else
             call PAR_BROADCAST_IP(n,xx,'IN MY CODE')
          end if
       end if
    end if

  end subroutine PAR_BROADCAST_IP_1

  subroutine PAR_BROADCAST_IP_0(n,xx,where)
    implicit none
    integer(ip),           intent(in)    :: n
    integer(ip),           intent(inout) :: xx(*)
    character(*),          optional      :: where

    if( ISEQUEN ) return
   
    if( n > 0 ) then
       if( present(where) ) then
          call PAR_BROADCAST_IP(n,xx,where)
       else
          call PAR_BROADCAST_IP(n,xx,'IN MY CODE')
       end if
    end if

  end subroutine PAR_BROADCAST_IP_0

  subroutine PAR_BROADCAST_IP(n,xx,where)
    implicit none
    integer(ip),  intent(in)  :: n
    integer(ip),  intent(out) :: xx(n)
    character(*), optional    :: where
    integer(4)                :: istat4,n4,PAR_COMM_TO_USE
    if( ISEQUEN ) return

    if( n > 0 ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       n4 = int(n,4)
#ifndef MPI_OFF
       istat4 = 0_4
       call MPI_Bcast(xx(1:n),n4,PAR_INTEGER,0_4,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_BROADCAST_IP: MPI ERROR')
#endif
    end if
  end subroutine PAR_BROADCAST_IP

  subroutine PAR_BROADCAST_RP_s(xx,where)
    implicit none
    real(rp),     intent(inout) :: xx
    character(*), optional      :: where
    integer(ip)                 :: n
    real(rp)                    :: yy(2)
    yy(1) = xx
    n     = 1
    call PAR_BROADCAST_RP(n,yy,where)
    xx    = yy(1)
  end subroutine PAR_BROADCAST_RP_s

  subroutine PAR_BROADCAST_RP_1(xx,where)
    implicit none
    real(rp),     pointer, intent(inout) :: xx(:)
    character(*),          optional      :: where
    integer(ip)                          :: n
   
    if( ISEQUEN ) return
    if( associated(xx) ) then
       n = size(xx)
       if( n > 0 ) then
          if( present(where) ) then
             call PAR_BROADCAST_RP(n,xx,where)
          else
             call PAR_BROADCAST_RP(n,xx,'IN MY CODE')
          end if
       end if
    end if

  end subroutine PAR_BROADCAST_RP_1

  subroutine PAR_BROADCAST_RP_0(n,xx,where,root_rank)
    implicit none
    integer(ip),  intent(in)           :: n
    real(rp),     intent(out)          :: xx(n)
    character(*), intent(in)           :: where
    integer(ip),  intent(in), optional :: root_rank
    integer(4)                         :: istat4,n4,PAR_COMM_TO_USE,root_rank4

    if( present(root_rank) ) then
       call PAR_BROADCAST_RP(n,xx,where,root_rank)
    else
       call PAR_BROADCAST_RP(n,xx,where)
    end if

  end subroutine PAR_BROADCAST_RP_0

  subroutine PAR_BROADCAST_RP(n,xx,where,root_rank)
    implicit none
    integer(ip),  intent(in)           :: n
    real(rp),     intent(out)          :: xx(n)
    character(*), intent(in)           :: where
    integer(ip),  intent(in), optional :: root_rank
    integer(4)                         :: istat4,n4,PAR_COMM_TO_USE,root_rank4

    if( ISEQUEN ) return
    if( n > 0 ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       n4 = int(n,4)
       if( present(root_rank) ) then
          root_rank4 = int(root_rank,4)
       else
          root_rank4 = 0_4
       end if
#ifndef MPI_OFF
       istat4 = 0_4
       call MPI_Bcast(xx(1:n),n4,MPI_DOUBLE_PRECISION,root_rank4,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_BROADCAST_RP: MPI ERROR')
#endif
    end if
  end subroutine PAR_BROADCAST_RP

  subroutine PAR_BROADCAST_CH(n,xx,where)
    implicit none
    integer(ip),  intent(in)  :: n
    character(*), intent(out) :: xx
    character(*)              :: where
    integer(4)                :: istat4,n4,PAR_COMM_TO_USE
    if( IPARALL .and. n > 0 ) then
#ifndef MPI_OFF
       istat4 = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       n4 = int(n,4)
       call MPI_Bcast(xx(1:n),n4,MPI_CHARACTER,0_4,PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0_4 ) call runend('PAR_BROADCAST_CP: MPI ERROR')
#endif
    end if
  end subroutine PAR_BROADCAST_CH

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_SEND_RECEIVE_IP
  !
  !----------------------------------------------------------------------

  subroutine PAR_SEND_RECEIVE_IP_s(xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    integer(ip),           intent(in)           :: xx_send
    integer(ip),           intent(out)          :: xx_recv
    character(*),          intent(in), optional :: where
    integer(ip),           intent(in), optional :: dom_i
    character(*),          intent(in), optional :: wsynch
    integer(ip)                                 :: yy_send(2)
    integer(ip)                                 :: yy_recv(2)
    if( ISEQUEN ) return
    yy_send(1) = xx_send
    if( present(wsynch) ) then
       call PAR_SEND_RECEIVE_IP(1_ip,1_ip,yy_send,yy_recv,where,dom_i,wsynch)
    else
       call PAR_SEND_RECEIVE_IP(1_ip,1_ip,yy_send,yy_recv,where,dom_i)
    end if
    xx_recv    = yy_recv(1)
  end subroutine PAR_SEND_RECEIVE_IP_s
  subroutine PAR_SEND_RECEIVE_IP_0(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    integer(ip),           intent(in)           :: nsend
    integer(ip),           intent(in)           :: nrecv
    integer(ip),           intent(in)           :: xx_send(*)
    integer(ip),           intent(out)          :: xx_recv(*)
    character(*),          intent(in)           :: where
    integer(ip),           intent(in)           :: dom_i
    character(*),          intent(in), optional :: wsynch
    integer(ip)                                 :: yy_send(2)
    integer(ip)                                 :: yy_recv(2)
    if( ISEQUEN ) return
    if( present(wsynch) ) then
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,yy_send,xx_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,yy_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
       else
          return
       end if
    else
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,yy_send,xx_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,yy_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i)
       else
          return
       end if
    end if
  end subroutine PAR_SEND_RECEIVE_IP_0
  subroutine PAR_SEND_RECEIVE_IP_1(xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    integer(ip),  pointer, intent(in)           :: xx_send(:)
    integer(ip),  pointer, intent(out)          :: xx_recv(:)
    character(*),          intent(in), optional :: where
    integer(ip),           intent(in), optional :: dom_i
    character(*),          intent(in), optional :: wsynch
    integer(ip)                                 :: nsend,nrecv
    integer(ip)                                 :: yy_send(2)
    integer(ip)                                 :: yy_recv(2)
    if( ISEQUEN ) return
    if( .not. associated(xx_send) ) then
       nsend = 0
    else
       nsend = size(xx_send)
    end if
    if( .not. associated(xx_recv) ) then
       nrecv = 0
    else
       nrecv = size(xx_recv)
    end if
    if( present(wsynch) ) then
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,yy_send,xx_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,yy_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
       else
          return
       end if
    else
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,yy_send,xx_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,yy_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i)
       else
          return
       end if
    end if
  end subroutine PAR_SEND_RECEIVE_IP_1
  subroutine PAR_SEND_RECEIVE_IP_2(xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    integer(ip),  pointer, intent(in)           :: xx_send(:,:)
    integer(ip),  pointer, intent(out)          :: xx_recv(:,:)
    character(*),          intent(in), optional :: where
    integer(ip),           intent(in), optional :: dom_i
    character(*),          intent(in), optional :: wsynch
    integer(ip)                                 :: nsend,nrecv
    integer(ip)                                 :: yy_send(2)
    integer(ip)                                 :: yy_recv(2)

    if( ISEQUEN ) return
    
    if( .not. associated(xx_send) ) then
       nsend = 0
    else
       nsend = size(xx_send,1)*size(xx_send,2)
    end if
    if( .not. associated(xx_recv) ) then
       nrecv = 0
    else
       nrecv = size(xx_recv,1)*size(xx_recv,2)
    end if
    if( present(wsynch) ) then
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,yy_send,xx_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,yy_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
       else
          return
       end if
    else
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,yy_send,xx_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,yy_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i)
       else
          return
       end if
    end if
  end subroutine PAR_SEND_RECEIVE_IP_2
  subroutine PAR_SEND_RECEIVE_IP_3(xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    integer(ip),  pointer, intent(in)           :: xx_send(:,:,:)
    integer(ip),  pointer, intent(out)          :: xx_recv(:,:,:)
    character(*),          intent(in), optional :: where
    integer(ip),           intent(in), optional :: dom_i
    character(*),          intent(in), optional :: wsynch
    integer(ip)                                 :: nsend,nrecv
    integer(ip)                                 :: yy_send(2)
    integer(ip)                                 :: yy_recv(2)
    if( ISEQUEN ) return
    if( .not. associated(xx_send) ) then
       nsend = 0
    else
       nsend = size(xx_send,1)*size(xx_send,2)*size(xx_send,3)
    end if
    if( .not. associated(xx_recv) ) then
       nrecv = 0
    else
       nrecv = size(xx_recv,1)*size(xx_recv,2)*size(xx_recv,3)
    end if
    if( present(wsynch) ) then
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,yy_send,xx_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,yy_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
       else
          return
       end if
    else
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,yy_send,xx_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,yy_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i)
       else
          return
       end if
    end if
  end subroutine PAR_SEND_RECEIVE_IP_3

  !----------------------------------------------------------------------
  !
  ! PAR_SEND_RECEIVE_IP
  !
  !----------------------------------------------------------------------

  subroutine PAR_SEND_RECEIVE_IP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    integer(ip),            intent(in)  :: nsend
    integer(ip),            intent(in)  :: nrecv
    integer(ip),            intent(in)  :: xx_send(*)
    integer(ip),            intent(out) :: xx_recv(*)
    character(*),           intent(in)  :: where
    integer(ip),            intent(in)  :: dom_i
    character(*), optional, intent(in)  :: wsynch
    integer(ip)                         :: kk
    integer(4)                          :: istat4,nsend4,nrecv4,dom_i4
    integer(4)                          :: PAR_COMM_TO_USE
    logical(lg)                         :: asynch
    !
    ! Define communicator
    !
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
    !
    ! BlockinG/non blocking
    !
    if( present(wsynch) ) then
       if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
          asynch = .false.
       else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
          asynch = .true.
          if( .not. associated(non_blocking(inonblocking) % request4) ) then
             call runend('NON-BLOCKING SEND/RECEIVE SHOULD BE STARTED')
          end if
       else
          call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
       end if
    else
       asynch = .false.
    end if
    !
    ! Synchronous Send/receive
    !
    nsend4 = int(nsend,4)
    nrecv4 = int(nrecv,4)
    dom_i4 = int(dom_i,4)
    istat4 = 0

#ifndef MPI_OFF
    if( asynch ) then
       if( nrecv /= 0 ) then
          non_blocking(inonblocking) % count4 = non_blocking(inonblocking) % count4 + 1
          kk = non_blocking(inonblocking) % count4
          call MPI_IRecv(                                 &
               xx_recv(1:nrecv), nrecv4,                  &
               PAR_INTEGER, dom_i4, 0_4,                  &
               PAR_COMM_TO_USE,                           &
               non_blocking(inonblocking) % request4(kk), &
               istat4                                     )
       end if
       if( nsend /= 0 ) then
          non_blocking(inonblocking) % count4 = non_blocking(inonblocking) % count4 + 1
          kk = non_blocking(inonblocking) % count4
          call MPI_ISend(                                 &
               xx_send(1:nsend), nsend4,                  &
               PAR_INTEGER, dom_i4, 0_4,                  &
               PAR_COMM_TO_USE,                           &
               non_blocking(inonblocking) % request4(kk), &
               istat4                                     )  
       end if
    else
       if( nrecv /= 0 .and. nsend == 0 ) then
          call MPI_Recv(                          &
               xx_recv(1:nrecv), nrecv4,          &
               PAR_INTEGER, dom_i4, 0_4,          &
               PAR_COMM_TO_USE, status, istat4    )
          
       else if( nrecv == 0 .and. nsend /= 0 ) then
          call MPI_Send(                          &
               xx_send(1:nsend), nsend4,          &
               PAR_INTEGER, dom_i4, 0_4,          &
               PAR_COMM_TO_USE, istat4            )
          
       else if( nrecv /= 0 .and. nsend /= 0 ) then
          call MPI_Sendrecv(                      &
               xx_send(1:nsend), nsend4,          &
               PAR_INTEGER, dom_i4, 0_4,          &
               xx_recv(1:nrecv), nrecv4,          &
               PAR_INTEGER, dom_i4, 0_4,          &
               PAR_COMM_TO_USE, status, istat4    )
          
       end if
    end if

#endif
    if( istat4 /= 0_4 ) call runend('PAR_SEND_RECEIVE_IP: MPI ERROR')

  end subroutine PAR_SEND_RECEIVE_IP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_SEND_RECEIVE_RP
  !
  !----------------------------------------------------------------------

  subroutine PAR_SEND_RECEIVE_RP_s(xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    real(rp),            intent(in)           :: xx_send
    real(rp),            intent(out)          :: xx_recv
    character(*),        intent(in)           :: where
    integer(ip),         intent(in)           :: dom_i
    character(*),        intent(in), optional :: wsynch
    real(rp)                                  :: yy_send(2)
    real(rp)                                  :: yy_recv(2)
    if( ISEQUEN ) return
    yy_send(1) = xx_send
    if( present(wsynch) ) then
       call PAR_SEND_RECEIVE_RP(1_ip,1_ip,yy_send,yy_recv,where,dom_i,wsynch)
    else
       call PAR_SEND_RECEIVE_RP(1_ip,1_ip,yy_send,yy_recv,where,dom_i)
    end if
    xx_recv    = yy_recv(1)
  end subroutine PAR_SEND_RECEIVE_RP_s
  subroutine PAR_SEND_RECEIVE_RP_0(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    integer(ip),         intent(in)           :: nsend
    integer(ip),         intent(in)           :: nrecv
    real(rp),            intent(in)           :: xx_send(*)
    real(rp),            intent(out)          :: xx_recv(*)
    character(*),        intent(in)           :: where
    integer(ip),         intent(in)           :: dom_i
    character(*),        intent(in), optional :: wsynch
    real(rp)                                  :: yy_send(2)
    real(rp)                                  :: yy_recv(2)
    if( ISEQUEN ) return
    if( present(wsynch) ) then
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,yy_send,xx_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,yy_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
       else
          return
       end if
    else
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,yy_send,xx_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,yy_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i)
       else
          return
       end if
    end if
  end subroutine PAR_SEND_RECEIVE_RP_0
  subroutine PAR_SEND_RECEIVE_RP_1(xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    real(rp),     pointer, intent(in)           :: xx_send(:)
    real(rp),     pointer, intent(out)          :: xx_recv(:)
    character(*),          intent(in)           :: where
    integer(ip),           intent(in)           :: dom_i
    character(*),          intent(in), optional :: wsynch
    integer(ip)                                 :: nsend,nrecv
    real(rp)                                    :: yy_send(2)
    real(rp)                                    :: yy_recv(2)
    if( ISEQUEN ) return
    if( .not. associated(xx_send) ) then
       nsend = 0
    else
       nsend = size(xx_send)
    end if
    if( .not. associated(xx_recv) ) then
       nrecv = 0
    else
       nrecv = size(xx_recv)
    end if
    if( present(wsynch) ) then
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,yy_send,xx_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,yy_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
       else
          return
       end if
    else
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,yy_send,xx_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,yy_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i)
       else
          return
       end if
    end if
  end subroutine PAR_SEND_RECEIVE_RP_1
  subroutine PAR_SEND_RECEIVE_RP_2(xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    real(rp),     pointer, intent(in)           :: xx_send(:,:)
    real(rp),     pointer, intent(out)          :: xx_recv(:,:)
    character(*),          intent(in)           :: where
    integer(ip),           intent(in)           :: dom_i
    character(*),          intent(in), optional :: wsynch
    integer(ip)                                 :: nsend,nrecv
    real(rp)                                    :: yy_send(2)
    real(rp)                                    :: yy_recv(2)
    if( ISEQUEN ) return
    if( .not. associated(xx_send) ) then
       nsend = 0
    else
       nsend = size(xx_send,1)*size(xx_send,2)
    end if
    if( .not. associated(xx_recv) ) then
       nrecv = 0
    else
       nrecv = size(xx_recv,1)*size(xx_recv,2)
    end if
    if( present(wsynch) ) then
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,yy_send,xx_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,yy_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
       else
          return
       end if
    else
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,yy_send,xx_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,yy_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i)
       else
          return
       end if
    end if
  end subroutine PAR_SEND_RECEIVE_RP_2
  subroutine PAR_SEND_RECEIVE_RP_3(xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    real(rp),     pointer, intent(in)           :: xx_send(:,:,:)
    real(rp),     pointer, intent(out)          :: xx_recv(:,:,:)
    character(*),          intent(in)           :: where
    integer(ip),           intent(in)           :: dom_i
    character(*),          intent(in), optional :: wsynch
    integer(ip)                                 :: nsend,nrecv
    real(rp)                                    :: yy_send(2)
    real(rp)                                    :: yy_recv(2)
    if( ISEQUEN ) return
    if( .not. associated(xx_send) ) then
       nsend = 0
    else
       nsend = size(xx_send,1)*size(xx_send,2)*size(xx_send,3)
    end if
    if( .not. associated(xx_recv) ) then
       nrecv = 0
    else
       nrecv = size(xx_recv,1)*size(xx_recv,2)*size(xx_recv,3)
    end if
    if( present(wsynch) ) then
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,yy_send,xx_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,yy_recv,where,dom_i,wsynch)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
       else
          return
       end if
    else
       if( nsend == 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,yy_send,xx_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv == 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,yy_recv,where,dom_i)
       else if( nsend /= 0 .and. nrecv /= 0 ) then
          call PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i)
       else
          return
       end if
    end if
  end subroutine PAR_SEND_RECEIVE_RP_3

  !----------------------------------------------------------------------
  !
  ! PAR_SEND_RECEIVE_RP
  !
  !----------------------------------------------------------------------

  subroutine PAR_SEND_RECEIVE_RP(nsend,nrecv,xx_send,xx_recv,where,dom_i,wsynch)
    implicit none
    integer(ip),            intent(in)  :: nsend
    integer(ip),            intent(in)  :: nrecv
    real(rp),               intent(in)  :: xx_send(*)
    real(rp),               intent(out) :: xx_recv(*)
    character(*),           intent(in)  :: where
    integer(ip),            intent(in)  :: dom_i
    character(*), optional, intent(in)  :: wsynch
    integer(ip)                         :: kk
    integer(4)                          :: istat4,nsend4,nrecv4,dom_i4
    integer(4)                          :: PAR_COMM_TO_USE
    logical(lg)                         :: asynch

    if( IPARALL ) then
       !
       ! Define communicator
       !
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       !
       ! BlockinG/non blocking
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
             if( .not. associated(non_blocking(inonblocking) % request4) ) then
                call runend('NON-BLOCKING SEND/RECEIVE SHOULD BE STARTED')
             end if
          else
             call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if
       !
       ! Synchronous Send/receive
       !
       nsend4 = int(nsend,4)
       nrecv4 = int(nrecv,4)
       dom_i4 = int(dom_i,4)
       istat4 = 0_4

#ifndef MPI_OFF
       if( asynch ) then
          if( nrecv /= 0 ) then
             non_blocking(inonblocking) % count4 = non_blocking(inonblocking) % count4 + 1
             kk = non_blocking(inonblocking) % count4
             call MPI_IRecv(                                 &
                  xx_recv(1:nrecv), nrecv4,                  &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4,         &
                  PAR_COMM_TO_USE,                           &
                  non_blocking(inonblocking) % request4(kk), &
                  istat4                                     )
          end if
          if( nsend /= 0 ) then
             non_blocking(inonblocking) % count4 = non_blocking(inonblocking) % count4 + 1
             kk = non_blocking(inonblocking) % count4
             call MPI_ISend(                                 &
                  xx_send(1:nsend), nsend4,                  &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4,         &
                  PAR_COMM_TO_USE,                           &
                  non_blocking(inonblocking) % request4(kk), &
                  istat4                                     )  
          end if          
       else
          if( nrecv /= 0 .and. nsend == 0 ) then
             call MPI_Recv(                          &
                  xx_recv(1:nrecv), nrecv4,          &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                  PAR_COMM_TO_USE, status, istat4   ) 
          else if( nrecv == 0 .and. nsend /= 0 ) then
             call MPI_Send(                          &
                  xx_send(1:nsend), nsend4,          &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                  PAR_COMM_TO_USE, istat4           )       
          else if( nrecv /= 0 .and. nsend /= 0 ) then
             call MPI_Sendrecv(                      &
                  xx_send(1:nsend), nsend4,          &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                  xx_recv(1:nrecv), nrecv4,          &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                  PAR_COMM_TO_USE, status, istat4   )
          end if
       end if
#endif
       if( istat4 /= 0_4 ) call runend('PAR_SEND_RECEIVE_RP: MPI ERROR')

    end if

  end subroutine PAR_SEND_RECEIVE_RP

  !-----------------------------------------------------------------------
  !
  !> @brief   Bridge to MPI_ALLGATHERV
  !> @details Bridge to MPI_ALLGATHERV. If the displacement is not 
  !>          prescribed the recvbuf are put one after the other 
  !>          automatically
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine PAR_GATHERV_RP_1(sendbuf,recvbuf,recvcount4,where,displs4)
    implicit none    
    real(rp),     pointer, intent(in)           :: sendbuf(:)           !< Send buffer
    real(rp),     pointer, intent(out)          :: recvbuf(:)           !< Recv buffer
    integer(4),   pointer, intent(in)           :: recvcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(ip)                                 :: recvbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(4)                                  :: root_rank4
    integer(4)                                  :: my_rank
    integer(ip)                                 :: rcl,dl,jpart
    integer(4),   pointer                       :: recvcount4_tmp(:)
    integer(4),   target                        :: recvcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)

    if( ISEQUEN ) then
       sendcount4 = size(sendbuf)
       recvbuf    = sendbuf
    else if( IPARALL ) then
#ifndef MPI_OFF
       root_rank4      = 0_4
       recvcount4_null = 0_4
       displs4_null    = 0_4
       sendcount4      = 0_4
 
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
       end if
       if( associated(recvcount4) ) then
          rcl = lbound(recvcount4,1)
          recvcount4_tmp => recvcount4(rcl:)
       else          
          recvcount4_tmp => recvcount4_null
       end if

       if( present(displs4) ) then
          if( associated(displs4) ) then
             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)
          else          
             displs4_tmp => displs4_null
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                    MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                    MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             end if
          end if
       else
          call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank,comm_size)
          if( my_rank == root_rank4 ) then
             allocate( my_displs4(0:comm_size-1) )
             jpart = lbound(recvcount4,1)
             my_displs4(0) = 0
             do ipart = 1,comm_size-1
                my_displs4(ipart) = my_displs4(ipart-1) + recvcount4(jpart)
                jpart = jpart + 1
             end do
          else
             allocate( my_displs4(1) )
             my_displs4 = 0
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                   MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                   MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          end if
          deallocate( my_displs4 ) 
       end if
       if( istat4 /= 0 ) call runend('PAR_GATHERV_RP_1: MPI ERROR')
#endif
    end if

  end subroutine PAR_GATHERV_RP_1

  subroutine PAR_GATHERV_RP_21(sendbuf,recvbuf,recvcount4,where,displs4)
    implicit none    
    real(rp),     pointer, intent(in)           :: sendbuf(:,:)         !< Send buffer
    real(rp),     pointer, intent(out)          :: recvbuf(:)           !< Recv buffer
    integer(4),   pointer, intent(in)           :: recvcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(ip)                                 :: recvbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(4)                                  :: root_rank4
    integer(4)                                  :: my_rank
    integer(ip)                                 :: rcl,dl,jpart,ii,jj,kk
    integer(4),   pointer                       :: recvcount4_tmp(:)
    integer(4),   target                        :: recvcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)

    if( ISEQUEN ) then
       sendcount4 = size(sendbuf)
       kk = lbound(recvbuf,1)
       do jj = lbound(sendbuf,2),ubound(sendbuf,2)
          do ii = lbound(sendbuf,1),ubound(sendbuf,1)
             recvbuf(kk) = sendbuf(ii,jj)
             kk = kk + 1
          end do
       end do
    else if( IPARALL ) then
#ifndef MPI_OFF
       root_rank4      = 0_4
       recvcount4_null = 0_4
       displs4_null    = 0_4
       sendcount4      = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
       end if
       if( associated(recvcount4) ) then
          rcl = lbound(recvcount4,1)
          recvcount4_tmp => recvcount4(rcl:)
       else          
          recvcount4_tmp => recvcount4_null
       end if

       if( present(displs4) ) then
          if( associated(displs4) ) then
             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)
          else          
             displs4_tmp => displs4_null
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                    MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                    MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             end if
          end if
       else
          call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank,comm_size)
          if( my_rank == root_rank4 ) then
             allocate( my_displs4(0:comm_size-1) )
             jpart = lbound(recvcount4,1)
             my_displs4(0) = 0
             do ipart = 1,comm_size-1
                my_displs4(ipart) = my_displs4(ipart-1) + recvcount4(jpart)
                jpart = jpart + 1
             end do
          else
             allocate( my_displs4(1) )
             my_displs4(1) = 0
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                   MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                   MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          end if
          deallocate( my_displs4 ) 
       end if
       if( istat4 /= 0 ) call runend('PAR_GATHERV_RP_2: MPI ERROR')     
#endif
    end if

  end subroutine PAR_GATHERV_RP_21

  subroutine PAR_GATHERV_RP_22(sendbuf,recvbuf,recvcount4,where,displs4)
    implicit none    
    real(rp),     pointer, intent(in)           :: sendbuf(:,:)         !< Send buffer
    real(rp),     pointer, intent(out)          :: recvbuf(:,:)         !< Recv buffer
    integer(4),   pointer, intent(in)           :: recvcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(ip)                                 :: recvbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(4)                                  :: root_rank4
    integer(4)                                  :: my_rank
    integer(ip)                                 :: rcl,dl,jpart,ii,jj,kk
    integer(4),   pointer                       :: recvcount4_tmp(:)
    integer(4),   target                        :: recvcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)

    if( ISEQUEN ) then
       sendcount4 = size(sendbuf)
       kk = lbound(recvbuf,1)
       do jj = lbound(sendbuf,2),ubound(sendbuf,2)
          do ii = lbound(sendbuf,1),ubound(sendbuf,1)
             recvbuf(ii,jj) = sendbuf(ii,jj)
             kk = kk + 1
          end do
       end do
    else if( IPARALL ) then
#ifndef MPI_OFF
       root_rank4      = 0_4
       recvcount4_null = 0_4
       displs4_null    = 0_4
       sendcount4      = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
       end if
       if( associated(recvcount4) ) then
          rcl = lbound(recvcount4,1)
          recvcount4_tmp => recvcount4(rcl:)
       else          
          recvcount4_tmp => recvcount4_null
       end if

       if( present(displs4) ) then
          if( associated(displs4) ) then
             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)
          else          
             displs4_tmp => displs4_null
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                    MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                    MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             end if
          end if
       else
          call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank,comm_size)
          if( my_rank == root_rank4 ) then
             allocate( my_displs4(0:comm_size-1) )
             jpart = lbound(recvcount4,1)
             my_displs4(0) = 0
             do ipart = 1,comm_size-1
                my_displs4(ipart) = my_displs4(ipart-1) + recvcount4(jpart)
                jpart = jpart + 1
             end do
          else
             allocate( my_displs4(1) )
             my_displs4(1) = 0
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                   MPI_DOUBLE_PRECISION,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                   MPI_DOUBLE_PRECISION,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,MPI_DOUBLE_PRECISION,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          end if
          deallocate( my_displs4 ) 
       end if
       if( istat4 /= 0 ) call runend('PAR_GATHERV_RP_2: MPI ERROR')     
#endif
    end if

  end subroutine PAR_GATHERV_RP_22

  subroutine PAR_GATHERV_IP_1(sendbuf,recvbuf,recvcount4,where,displs4)
    implicit none    
    integer(ip),  pointer, intent(in)           :: sendbuf(:)           !< Send buffer
    integer(ip),  pointer, intent(out)          :: recvbuf(:)           !< Recv buffer
    integer(4),   pointer, intent(in)           :: recvcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(ip)                                 :: recvbuf_tmp(2)  
    integer(4)                                  :: ipart
    integer(4)                                  :: root_rank4
    integer(4)                                  :: my_rank
    integer(ip)                                 :: rl,sl,rcl,dl,jpart
    integer(4),   pointer                       :: recvcount4_tmp(:)
    integer(4),   target                        :: recvcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)

#ifndef MPI_OFF
    if( IPARALL ) then
       root_rank4      = 0_4
       recvcount4_null = 0_4
       displs4_null    = 0_4
       sendcount4      = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
          sl = lbound(sendbuf,1)
       end if
       if( associated(recvbuf) )    rl  = lbound(recvbuf,1)
       if( associated(recvcount4) ) then
          rcl = lbound(recvcount4,1)
          recvcount4_tmp => recvcount4(rcl:)
       else          
          recvcount4_tmp => recvcount4_null
       end if

       if( present(displs4) ) then
          if( associated(displs4) ) then
             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)
          else          
             displs4_tmp => displs4_null
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                PAR_INTEGER,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                PAR_INTEGER,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                    PAR_INTEGER,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                    PAR_INTEGER,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             end if
          end if
       else
          call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank,comm_size)
          if( my_rank == root_rank4 ) then
             allocate( my_displs4(0:comm_size-1) )
             jpart = lbound(recvcount4,1)
             my_displs4(0) = 0
             do ipart = 1,comm_size-1
                my_displs4(ipart) = my_displs4(ipart-1) + recvcount4(jpart)
                jpart = jpart + 1
             end do
          else
             allocate( my_displs4(1) )
             my_displs4(1) = 0
           end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               PAR_INTEGER,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               PAR_INTEGER,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                   PAR_INTEGER,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                   PAR_INTEGER,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          end if
          deallocate( my_displs4 ) 
       end if
       if( istat4 /= 0 ) call runend('PAR_GATHERV_IP_1: MPI ERROR')
    end if
#endif

  end subroutine PAR_GATHERV_IP_1

  subroutine PAR_GATHERV_IP_2(sendbuf,recvbuf,recvcount4,where,displs4)
    implicit none    
    integer(ip),  pointer, intent(in)           :: sendbuf(:,:)         !< Send buffer
    integer(ip),  pointer, intent(out)          :: recvbuf(:)           !< Recv buffer
    integer(4),   pointer, intent(in)           :: recvcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(ip)                                 :: recvbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(4)                                  :: root_rank4
    integer(4)                                  :: my_rank
    integer(ip)                                 :: rl,rcl,dl
    integer(4),   pointer                       :: recvcount4_tmp(:)
    integer(4),   target                        :: recvcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)
    integer(ip)                                 :: jpart

#ifndef MPI_OFF
    if( IPARALL ) then
       root_rank4      = 0_4
       recvcount4_null = 0_4
       displs4_null    = 0_4
       sendcount4      = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf,1)*size(sendbuf,2)
       end if
       if( associated(recvbuf) )    rl  = lbound(recvbuf,1)
       if( associated(recvcount4) ) then
          rcl = lbound(recvcount4,1)
          recvcount4_tmp => recvcount4(rcl:)
       else          
          recvcount4_tmp => recvcount4_null
       end if
       if( present(displs4) ) then
          if( associated(displs4) ) then
             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)
          else          
             displs4_tmp => displs4_null
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                PAR_INTEGER,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,                PAR_INTEGER,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                    PAR_INTEGER,&
                     &           recvbuf,recvcount4_tmp,displs4_tmp,    PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                    PAR_INTEGER,&
                     &           recvbuf_tmp,recvcount4_tmp,displs4_tmp,PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             end if
          end if
       else
          call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank,comm_size)
          if( my_rank == root_rank4 ) then
             allocate( my_displs4(0:comm_size-1) )
             jpart = lbound(recvcount4,1)
             my_displs4(0) = 0
             do ipart = 1,comm_size-1
                my_displs4(ipart) = my_displs4(ipart-1) + recvcount4(jpart)
                jpart = jpart + 1
             end do
          else
             allocate( my_displs4(1) )
             my_displs4(1) = 0
          end if
          if( sendcount4 == 0 ) then
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               PAR_INTEGER,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf_tmp,sendcount4,               PAR_INTEGER,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          else
             if( associated(recvbuf) ) then
                call MPI_GATHERV(sendbuf,sendcount4,                   PAR_INTEGER,&
                     &           recvbuf,recvcount4_tmp,my_displs4,    PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)
             else
                call MPI_GATHERV(sendbuf,sendcount4,                   PAR_INTEGER,&
                     &           recvbuf_tmp,recvcount4_tmp,my_displs4,PAR_INTEGER,&
                     &           root_rank4,PAR_COMM_TO_USE,istat4)                
             end if
          end if
          deallocate( my_displs4 ) 
       end if      
       if( istat4 /= 0 ) call runend('PAR_GATHERV_IP_2: MPI ERROR')
    end if
#endif

  end subroutine PAR_GATHERV_IP_2

  !-----------------------------------------------------------------------
  !
  !> @brief   Bridge to MPI_ALLGATHERV
  !> @details Bridge to MPI_ALLGATHERV. If the displacement is not 
  !>          prescribed the recvbuf are put one after the other 
  !>          automatically
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine PAR_ALLGATHERV_IP4(sendbuf,recvbuf,recvcount4,where,displs4)
    implicit none    
    integer(ip),  pointer, intent(in)           :: sendbuf(:)           !< Send buffer
    integer(ip),  pointer, intent(out)          :: recvbuf(:)           !< Recv buffer
    integer(4),   pointer, intent(in)           :: recvcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(ip)                                 :: rl,rcl,dl,jpart
    integer(4),   pointer                       :: recvcount4_tmp(:)
    integer(4),   target                        :: recvcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)

#ifndef MPI_OFF
    if( IPARALL ) then
       recvcount4_null = 0_4
       displs4_null    = 0_4
       sendcount4      = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
       end if
       rl = lbound(recvcount4,1)
       if( associated(recvcount4) ) then
          rcl = lbound(recvcount4,1)
          recvcount4_tmp => recvcount4(rcl:)
       else          
          recvcount4_tmp => recvcount4_null
       end if

       if( present(displs4) ) then
          if( associated(displs4) ) then
             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)
          else          
             displs4_tmp => displs4_null
          end if
          if( sendcount4 == 0 ) then
             CALL MPI_ALLGATHERV(sendbuf_tmp,sendcount4,            PAR_INTEGER,&
                  &              recvbuf,recvcount4_tmp,displs4_tmp,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          else
             CALL MPI_ALLGATHERV(sendbuf,sendcount4,                PAR_INTEGER,&
                  &              recvbuf,recvcount4_tmp,displs4_tmp,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          end if
       else
          call MPI_Comm_size(PAR_COMM_TO_USE,comm_size,istat4)
          allocate( my_displs4(0:comm_size-1) )
          jpart = lbound(recvcount4,1)
          my_displs4(0) = 0
          do ipart = 1,comm_size-1
             my_displs4(ipart) = my_displs4(ipart-1) + recvcount4(jpart)
             jpart = jpart + 1
          end do
          if( sendcount4 == 0 ) then
             CALL MPI_ALLGATHERV(sendbuf_tmp,sendcount4,           PAR_INTEGER,&
                  &              recvbuf,recvcount4_tmp,my_displs4,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          else
             CALL MPI_ALLGATHERV(sendbuf,sendcount4,               PAR_INTEGER,&
                  &              recvbuf,recvcount4_tmp,my_displs4,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          end if
          deallocate( my_displs4 ) 
       end if
       if( istat4 /= 0_4 ) call runend('PAR_ALLGATHERV_IP4: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHERV_IP4

  subroutine PAR_ALLGATHERV_IP8(sendbuf,recvbuf,recvcount8,where,displs8)
    implicit none    
    integer(ip),  pointer, intent(in)           :: sendbuf(:)           !< Send buffer
    integer(ip),  pointer, intent(out)          :: recvbuf(:)           !< Recv buffer
    integer(8),   pointer, intent(in)           :: recvcount8(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(8),   pointer, intent(in), optional :: displs8(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(4),   pointer                       :: displs4(:)
    integer(4),   pointer                       :: recvcount4(:)
    integer(ip)                                 :: jpart

#ifndef MPI_OFF
    if( IPARALL ) then
       sendcount4 = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
       end if
       allocate( recvcount4(max(1_ip,size(recvcount8))) )
       recvcount4 = int(recvcount8,4)
       if( present(displs8) ) then
          if( .not. associated(displs8) ) then
             allocate( displs4(1) )
             displs4 = 0
          else
             allocate( displs4(size(displs8)) )
             displs4 = int(displs8,4)
          end if
          if( sendcount4 == 0 ) then
             CALL MPI_ALLGATHERV(sendbuf_tmp,sendcount4,    PAR_INTEGER,&
                  &              recvbuf,recvcount4,displs4,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          else
             CALL MPI_ALLGATHERV(sendbuf,sendcount4,        PAR_INTEGER,&
                  &              recvbuf,recvcount4,displs4,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          end if
          deallocate(displs4)
       else
          call MPI_Comm_size(PAR_COMM_TO_USE,comm_size,istat4)
          allocate( my_displs4(0:comm_size-1) )
          jpart = lbound(recvcount8,1)
          my_displs4(0) = 0
          do ipart = 1,comm_size-1
             my_displs4(ipart) = my_displs4(ipart-1) + int(recvcount8(jpart),4)
             jpart = jpart + 1
          end do
          if( sendcount4 == 0 ) then
             CALL MPI_ALLGATHERV(sendbuf_tmp,sendcount4,       PAR_INTEGER,&
                  &              recvbuf,recvcount4,my_displs4,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          else
             CALL MPI_ALLGATHERV(sendbuf,sendcount4,           PAR_INTEGER,&
                  &              recvbuf,recvcount4,my_displs4,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          end if
          deallocate( my_displs4 ) 
       end if
       deallocate( recvcount4 )
       if( istat4 /= 0_4 ) call runend('PAR_ALLGATHERV_IP8: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHERV_IP8

  subroutine PAR_ALLGATHERV_IP4_2(sendbuf,recvbuf,recvcount4,where,displs4)
    implicit none    
    integer(ip),  pointer, intent(in)           :: sendbuf(:,:)         !< Send buffer
    integer(ip),  pointer, intent(out)          :: recvbuf(:,:)         !< Recv buffer
    integer(4),   pointer, intent(in)           :: recvcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(ip)                                 :: rl,rcl,dl,jpart
    integer(4),   pointer                       :: recvcount4_tmp(:)
    integer(4),   target                        :: recvcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)

#ifndef MPI_OFF
    if( IPARALL ) then
       recvcount4_null = 0_4
       displs4_null    = 0_4
       sendcount4      = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
       end if
       rl = lbound(recvcount4,1)
       if( associated(recvcount4) ) then
          rcl = lbound(recvcount4,1)
          recvcount4_tmp => recvcount4(rcl:)
       else          
          recvcount4_tmp => recvcount4_null
       end if

       if( present(displs4) ) then
          if( associated(displs4) ) then
             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)
          else          
             displs4_tmp => displs4_null
          end if
          if( sendcount4 == 0 ) then
             CALL MPI_ALLGATHERV(sendbuf_tmp,sendcount4,            PAR_INTEGER,&
                  &              recvbuf,recvcount4_tmp,displs4_tmp,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          else
             CALL MPI_ALLGATHERV(sendbuf,sendcount4,                PAR_INTEGER,&
                  &              recvbuf,recvcount4_tmp,displs4_tmp,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          end if
       else
          call MPI_Comm_size(PAR_COMM_TO_USE,comm_size,istat4)
          allocate( my_displs4(0:comm_size-1) )
          jpart = lbound(recvcount4,1)
          my_displs4(0) = 0
          do ipart = 1,comm_size-1
             my_displs4(ipart) = my_displs4(ipart-1) + recvcount4(jpart)
             jpart = jpart + 1
          end do
          if( sendcount4 == 0 ) then
             CALL MPI_ALLGATHERV(sendbuf_tmp,sendcount4,           PAR_INTEGER,&
                  &              recvbuf,recvcount4_tmp,my_displs4,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          else
             CALL MPI_ALLGATHERV(sendbuf,sendcount4,               PAR_INTEGER,&
                  &              recvbuf,recvcount4_tmp,my_displs4,PAR_INTEGER,&
                  &              PAR_COMM_TO_USE,istat4)
          end if
          deallocate( my_displs4 ) 
       end if
       if( istat4 /= 0_4 ) call runend('PAR_ALLGATHERV_IP4_2: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHERV_IP4_2

  subroutine PAR_ALLGATHERV_RP_2(sendbuf,recvbuf,recvcount4,where,displs4)
    implicit none    
    real(rp),     pointer, intent(in)           :: sendbuf(:,:)         !< Send buffer
    real(rp),     pointer, intent(out)          :: recvbuf(:,:)         !< Recv buffer
    integer(4),   pointer, intent(in)           :: recvcount4(:)        !< Recv counts
    character(*),          intent(in)           :: where                !< Where
    integer(4),   pointer, intent(in), optional :: displs4(:)           !< Displacement 
    integer(4)                                  :: istat4 
    integer(4)                                  :: comm_size 
    integer(4)                                  :: PAR_COMM_TO_USE
    integer(4)                                  :: sendcount4
    integer(4),   pointer                       :: my_displs4(:)
    integer(ip)                                 :: sendbuf_tmp(2)   
    integer(4)                                  :: ipart
    integer(ip)                                 :: rl,rcl,dl,jpart
    integer(4),   pointer                       :: recvcount4_tmp(:)
    integer(4),   target                        :: recvcount4_null(2)
    integer(4),   pointer                       :: displs4_tmp(:)
    integer(4),   target                        :: displs4_null(2)

#ifndef MPI_OFF
    if( IPARALL ) then
       recvcount4_null = 0_4
       displs4_null    = 0_4
       sendcount4      = 0_4
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
       end if
       rl = lbound(recvcount4,1)
       if( associated(recvcount4) ) then
          rcl = lbound(recvcount4,1)
          recvcount4_tmp => recvcount4(rcl:)
       else          
          recvcount4_tmp => recvcount4_null
       end if

       if( present(displs4) ) then
          if( associated(displs4) ) then
             dl = lbound(displs4,1)
             displs4_tmp => displs4(dl:)
          else          
             displs4_tmp => displs4_null
          end if
          if( sendcount4 == 0 ) then
             CALL MPI_ALLGATHERV(sendbuf_tmp,sendcount4,            MPI_DOUBLE_PRECISION,&
                  &              recvbuf,recvcount4_tmp,displs4_tmp,MPI_DOUBLE_PRECISION,&
                  &              PAR_COMM_TO_USE,istat4)
          else
             CALL MPI_ALLGATHERV(sendbuf,sendcount4,                MPI_DOUBLE_PRECISION,&
                  &              recvbuf,recvcount4_tmp,displs4_tmp,MPI_DOUBLE_PRECISION,&
                  &              PAR_COMM_TO_USE,istat4)
          end if
       else
          call MPI_Comm_size(PAR_COMM_TO_USE,comm_size,istat4)
          allocate( my_displs4(0:comm_size-1) )
          jpart = lbound(recvcount4,1)
          my_displs4(0) = 0
          do ipart = 1,comm_size-1
             my_displs4(ipart) = my_displs4(ipart-1) + recvcount4(jpart)
             jpart = jpart + 1
          end do
          if( sendcount4 == 0 ) then
             CALL MPI_ALLGATHERV(sendbuf_tmp,sendcount4,           MPI_DOUBLE_PRECISION,&
                  &              recvbuf,recvcount4_tmp,my_displs4,MPI_DOUBLE_PRECISION,&
                  &              PAR_COMM_TO_USE,istat4)
          else
             CALL MPI_ALLGATHERV(sendbuf,sendcount4,               MPI_DOUBLE_PRECISION,&
                  &              recvbuf,recvcount4_tmp,my_displs4,MPI_DOUBLE_PRECISION,&
                  &              PAR_COMM_TO_USE,istat4)
          end if
          deallocate( my_displs4 ) 
       end if
       if( istat4 /= 0_4 ) call runend('PAR_ALLGATHERV_IP4: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHERV_RP_2

  !-----------------------------------------------------------------------
  !
  !> @brief   Bridge to MPI_ALLGATHER
  !> @details Bridge to MPI_ALLGATHER
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine PAR_ALLGATHER_s4(sendbuf,recvbuf,recvcount4,where)
    implicit none    
    integer(4),            intent(in)  :: sendbuf              !< Send buffer
    integer(4),  pointer,  intent(out) :: recvbuf(:)           !< Recv buffer
    integer(4),            intent(in)  :: recvcount4           !< Recv counts
    character(*),          intent(in)  :: where                !< Where
    integer(4)                         :: istat4 
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: sendcount4

#ifndef MPI_OFF
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       sendcount4 = 1_4       
       call MPI_ALLGATHER(sendbuf,sendcount4,MPI_INTEGER,&
            &             recvbuf,recvcount4,MPI_INTEGER,&
            &             PAR_COMM_TO_USE,istat4)       
       if( istat4 /= 0 ) call runend('PAR_ALLGATHER_s: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHER_s4

  subroutine PAR_ALLGATHER_s8(sendbuf,recvbuf,recvcount4,where)
    implicit none    
    integer(8),            intent(in)  :: sendbuf              !< Send buffer
    integer(8),  pointer,  intent(out) :: recvbuf(:)           !< Recv buffer
    integer(4),            intent(in)  :: recvcount4           !< Recv counts
    character(*),          intent(in)  :: where                !< Where
    integer(4)                         :: istat4 
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: sendcount4

#ifndef MPI_OFF
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       sendcount4 = 1_4       
       call MPI_ALLGATHER(sendbuf,sendcount4,MPI_INTEGER8,&
            &             recvbuf,recvcount4,MPI_INTEGER8,&
            &             PAR_COMM_TO_USE,istat4)       
       if( istat4 /= 0 ) call runend('PAR_ALLGATHER_s: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHER_s8

  subroutine PAR_ALLGATHER_IP_14(sendbuf,recvbuf,recvcount4,where)
    implicit none    
    integer(4),  pointer,  intent(in)  :: sendbuf(:)           !< Send buffer
    integer(4),  pointer,  intent(out) :: recvbuf(:)           !< Recv buffer
    integer(4),            intent(in)  :: recvcount4           !< Recv counts
    character(*),          intent(in)  :: where                !< Where
    integer(4)                         :: istat4 
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: sendcount4
    integer(ip)                        :: sendnul(2)

#ifndef MPI_OFF
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
          call MPI_ALLGATHER(sendbuf,sendcount4,MPI_INTEGER,&
               &             recvbuf,recvcount4,MPI_INTEGER,&
               &             PAR_COMM_TO_USE,istat4)
       else
          sendcount4 = 0_4
          sendnul    = 0_ip
          call MPI_ALLGATHER(sendnul,sendcount4,MPI_INTEGER,&
               &             recvbuf,recvcount4,MPI_INTEGER,&
               &             PAR_COMM_TO_USE,istat4)
       end if
       if( istat4 /= 0 ) call runend('PAR_ALLGATHER_IP: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHER_IP_14

  subroutine PAR_ALLGATHER_IP_18(sendbuf,recvbuf,recvcount4,where)
    implicit none    
    integer(8),  pointer,  intent(in)  :: sendbuf(:)           !< Send buffer
    integer(8),  pointer,  intent(out) :: recvbuf(:)           !< Recv buffer
    integer(4),            intent(in)  :: recvcount4           !< Recv counts
    character(*),          intent(in)  :: where                !< Where
    integer(4)                         :: istat4 
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: sendcount4
    integer(ip)                        :: sendnul(2)

#ifndef MPI_OFF
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       if( associated(sendbuf) ) then
          sendcount4 = size(sendbuf)
          call MPI_ALLGATHER(sendbuf,sendcount4,MPI_INTEGER8,&
               &             recvbuf,recvcount4,MPI_INTEGER8,&
               &             PAR_COMM_TO_USE,istat4)
       else
          sendcount4 = 0_4
          sendnul    = 0_ip
          call MPI_ALLGATHER(sendnul,sendcount4,MPI_INTEGER8,&
               &             recvbuf,recvcount4,MPI_INTEGER8,&
               &             PAR_COMM_TO_USE,istat4)
       end if
       if( istat4 /= 0 ) call runend('PAR_ALLGATHER_IP: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHER_IP_18

  subroutine PAR_ALLGATHER_RP_0(sendbuf,recvbuf,recvcount4,where)
    implicit none    
    real(rp),              intent(in)  :: sendbuf(*)           !< Send buffer
    real(rp),              intent(out) :: recvbuf(*)           !< Recv buffer
    integer(4),            intent(in)  :: recvcount4           !< Recv counts
    character(*),          intent(in)  :: where                !< Where
    integer(4)                         :: istat4 
    integer(4)                         :: PAR_COMM_TO_USE

#ifndef MPI_OFF
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       call MPI_ALLGATHER(sendbuf,recvcount4,MPI_DOUBLE_PRECISION,&
            &             recvbuf,recvcount4,MPI_DOUBLE_PRECISION,&
            &             PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0 ) call runend('PAR_ALLGATHER_IP: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHER_RP_0

  subroutine PAR_ALLGATHER_RP_2(sendbuf,recvbuf,recvcount4,where)
    implicit none    
    real(rp),     pointer, intent(in)  :: sendbuf(:,:)         !< Send buffer
    real(rp),     pointer, intent(out) :: recvbuf(:,:)         !< Recv buffer
    integer(4),            intent(in)  :: recvcount4           !< Recv counts
    character(*),          intent(in)  :: where                !< Where
    integer(4)                         :: istat4 
    integer(4)                         :: PAR_COMM_TO_USE

#ifndef MPI_OFF
    if( IPARALL ) then
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       call MPI_ALLGATHER(sendbuf,recvcount4,MPI_DOUBLE_PRECISION,&
            &             recvbuf,recvcount4,MPI_DOUBLE_PRECISION,&
            &             PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0 ) call runend('PAR_ALLGATHER_IP: MPI ERROR')
    end if
#endif

  end subroutine PAR_ALLGATHER_RP_2

  subroutine PAR_ALLGATHER_RP_02(sendbuf,recvbuf,recvcount4,where)
    implicit none    
    real(rp),              intent(in)  :: sendbuf(*)           !< Send buffer
    real(rp),     pointer, intent(out) :: recvbuf(:,:)         !< Recv buffer
    integer(4),            intent(in)  :: recvcount4           !< Recv counts
    character(*),          intent(in)  :: where                !< Where
    integer(4)                         :: istat4 
    integer(4)                         :: PAR_COMM_TO_USE

    if( IPARALL ) then
#ifndef MPI_OFF
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       call MPI_ALLGATHER(sendbuf,recvcount4,MPI_DOUBLE_PRECISION,&
            &             recvbuf,recvcount4,MPI_DOUBLE_PRECISION,&
            &             PAR_COMM_TO_USE,istat4)
       if( istat4 /= 0 ) call runend('PAR_ALLGATHER_IP: MPI ERROR')
#endif
    end if

  end subroutine PAR_ALLGATHER_RP_02

  subroutine PAR_ALLGATHER_LG(sendbuf,recvbuf,recvcount4,where)
    implicit none    
    logical(lg),           intent(in)  :: sendbuf              !< Send buffer
    logical(lg),  pointer, intent(out) :: recvbuf(:)           !< Recv buffer
    integer(4),            intent(in)  :: recvcount4           !< Recv counts
    character(*),          intent(in)  :: where                !< Where
    integer(4)                         :: istat4 
    integer(4)                         :: PAR_COMM_TO_USE
    integer(4)                         :: sendcount4

    if( IPARALL ) then 
#ifndef MPI_OFF
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE)
       sendcount4 = 1_4       
       call MPI_ALLGATHER(sendbuf,sendcount4,MPI_LOGICAL,&
            &             recvbuf,recvcount4,MPI_LOGICAL,&
            &             PAR_COMM_TO_USE,istat4)       
       if( istat4 /= 0 ) call runend('PAR_ALLGATHER_s: MPI ERROR')
#endif
    end if

  end subroutine PAR_ALLGATHER_LG

  !-----------------------------------------------------------------------
  !
  !> @brief   Initialize MPI
  !> @details Initialize MPI
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine PAR_INIT()
    implicit none
    integer(4) :: istat4

#ifndef MPI_OFF
    call MPI_Init(istat4)
    if( istat4 /= 0_4 ) call runend('COULD NOT INITIALIZE MPI')
#endif

  end subroutine PAR_INIT

  !-----------------------------------------------------------------------
  !
  !> @brief   Define length of integers
  !> @details Define length of integers
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine PAR_LENGTH_INTEGER()
    implicit none

#ifndef MPI_OFF
    if( ip == 4 ) then
       PAR_INTEGER = MPI_INTEGER
    else
       PAR_INTEGER = MPI_INTEGER8
    end if
#endif

  end subroutine PAR_LENGTH_INTEGER

  subroutine PAR_SEND_RECEIVE_TO_ALL_IP(ndofn,xx_send,xx_recv,commu,wsynch)
    implicit none
    integer(ip),                    intent(in)  :: ndofn
    integer(ip),                    intent(in)  :: xx_send(*)
    integer(ip),                    intent(out) :: xx_recv(*)
    type(comm_data_par),            intent(in)  :: commu
    character(*),         optional, intent(in)  :: wsynch
    integer(ip)                                 :: inise,finse,inire,finre,count
    integer(ip)                                 :: dom_i,ineig,nsend,nrecv,kk
    integer(4)                                  :: istat4,nsend4,nrecv4,dom_i4
    integer(4)                                  :: PAR_COMM_TO_USE,count4
    logical(lg)                                 :: asynch
    integer(4),           allocatable           :: ireqq4(:)
    integer(4),           pointer               :: status4(:,:)
    !
    ! Define communicator
    !
    PAR_COMM_TO_USE = int(commu % PAR_COMM_WORLD,4)
    !
    ! Asynchronous
    !
    if( present(wsynch) ) then
       if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
          asynch = .false.
       else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
          asynch = .true.
       else
          call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
       end if
    else
       asynch = .false.
    end if   
    if( asynch ) allocate(ireqq4(commu % nneig*2))
    kk = 0
    !
    ! Synchronous Send/receive
    !
    do ineig = 1,commu % nneig

       dom_i  = commu % neights(ineig)

       inise  = ndofn * ( commu % lsend_size(ineig)   - 1 ) + 1
       nsend  = ndofn * ( commu % lsend_size(ineig+1) - 1 ) + 1 - inise
       finse  = inise + nsend - 1
       inire  = ndofn * ( commu % lrecv_size(ineig)   - 1 ) + 1
       nrecv  = ndofn * ( commu % lrecv_size(ineig+1) - 1 ) + 1 - inire
       finre  = inire + nrecv - 1

       nsend4 = int(nsend,4)
       nrecv4 = int(nrecv,4)
       dom_i4 = int(dom_i,4)

call runend('The interface for integers is not ready. ')

#ifndef MPI_OFF
       if( asynch .and. commu % nneig /= 0_ip ) then
          if( nsend > 0_ip ) then          
             kk = kk + 1
             call MPI_Isend(                          &
                  xx_send(inise:finse), nsend4,       &
                  PAR_INTEGER,  dom_i4, 0_4,          &
                  PAR_COMM_TO_USE, ireqq4(kk), istat4 )
          end if
          if( nrecv > 0_ip ) then
             kk = kk + 1
             call MPI_Irecv(                          &
                  xx_recv(inire:finre), nrecv4,       &
                  PAR_INTEGER,  dom_i4, 0_4,          &
                  PAR_COMM_TO_USE, ireqq4(kk), istat4 )
          end if
       else
          if( nrecv /= 0 .and. nsend == 0 ) then
             call MPI_Recv(                       &
                  xx_recv(inire:finre), nrecv4,   &
                  PAR_INTEGER, dom_i4, 0_4,       &
                  PAR_COMM_TO_USE, status, istat4 )
             
          else if( nrecv == 0 .and. nsend /= 0 ) then
             call MPI_Send(                       &
                  xx_send(inise:finse), nsend4,   &
                  PAR_INTEGER, dom_i4, 0_4,       &
                  PAR_COMM_TO_USE, istat4         )
             
          else if( nrecv /= 0 .and. nsend /= 0 ) then
             call MPI_Sendrecv(                   &
                  xx_send(inise:finse), nsend4,   &
                  PAR_INTEGER, dom_i4, 0_4,       &
                  xx_recv(inire:finre), nrecv4,   &
                  PAR_INTEGER, dom_i4, 0_4,       &
                  PAR_COMM_TO_USE, status, istat4 )
          end if
       end if
#endif

    end do
    !
    ! Wait in case of asynchronous, count is the total of communications actually performe       
    ! and is smaller or equal than the asynchronous communications requested (ireqq)
    !
#ifndef MPI_OFF
    if( asynch .and. commu % nneig /= 0 ) then
       count  = kk 
       count4 = int(count,4)
       allocate( status4(MPI_STATUS_SIZE,count) )
       CALL MPI_WAITALL(count4,ireqq4,status4,istat4)
       deallocate( status4 ) 
       deallocate( ireqq4  )
    end if
#endif

  end subroutine PAR_SEND_RECEIVE_TO_ALL_IP

  !-----------------------------------------------------------------------
  !
  !> @brief   Send receive
  !> @details Send and receive to all my neghbors within communicator COMMU
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine PAR_SEND_RECEIVE_TO_ALL_IP_1(xx_send,xx_recv,where,wsynch)
    implicit none
    integer(ip),                    intent(in)  :: xx_send(*)
    integer(ip),                    intent(out) :: xx_recv(*)
    character(*),                   intent(in)  :: where
    character(*),         optional, intent(in)  :: wsynch
    integer(ip)                                 :: ndofn
    real(rp)                                    :: yy_send(2)
    real(rp)                                    :: yy_recv(2)
    type(comm_data_par),  pointer               :: commu
    integer(4)                                  :: PAR_COMM_TO_USE

    if( ISEQUEN ) return
    ndofn = 1_ip

    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_SEND_RECEIVE_TO_ALL_IP(ndofn,xx_send,xx_recv,commu,wsynch)

  end subroutine PAR_SEND_RECEIVE_TO_ALL_IP_1


  subroutine PAR_SEND_RECEIVE_TO_ALL_RP_s(xx_send,xx_recv,commu,wsynch)
    implicit none
    real(rp),                       intent(in)  :: xx_send
    real(rp),                       intent(out) :: xx_recv
    type(comm_data_par),            intent(in)  :: commu
    character(*),         optional, intent(in)  :: wsynch
    real(rp)                                    :: yy_send(2)
    real(rp)                                    :: yy_recv(2)
    if( ISEQUEN ) return
    yy_send(1) = xx_send
    call PAR_SEND_RECEIVE_TO_ALL_RP(1_ip,yy_send,yy_recv,commu,wsynch)
    xx_recv    = yy_recv(1)
  end subroutine PAR_SEND_RECEIVE_TO_ALL_RP_s
  subroutine PAR_SEND_RECEIVE_TO_ALL_RP_0(ndofn,xx_send,xx_recv,commu,wsynch)
    implicit none
    integer(ip),                    intent(in)  :: ndofn
    real(rp),                       intent(in)  :: xx_send(ndofn,*)
    real(rp),                       intent(out) :: xx_recv(ndofn,*)
    type(comm_data_par),            intent(in)  :: commu
    character(*),         optional, intent(in)  :: wsynch
    if( ISEQUEN ) return
    call PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,xx_send,xx_recv,commu,wsynch)
  end subroutine PAR_SEND_RECEIVE_TO_ALL_RP_0
  subroutine PAR_SEND_RECEIVE_TO_ALL_RP_1(xx_send,xx_recv,commu,wsynch)
    implicit none
    real(rp),             pointer,  intent(in)  :: xx_send(:)
    real(rp),             pointer,  intent(out) :: xx_recv(:)
    type(comm_data_par),            intent(in)  :: commu
    character(*),         optional, intent(in)  :: wsynch
    integer(ip)                                 :: ndofn
    real(rp)                                    :: yy_send(2)
    real(rp)                                    :: yy_recv(2)
    if( ISEQUEN ) return
    ndofn = 1_ip
    if( .not. associated(xx_send) .and. associated(xx_recv) ) then
       call PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,yy_send,xx_recv,commu,wsynch)
    else if( .not. associated(xx_send) .and. .not. associated(xx_recv) ) then
       call PAR_SEND_RECEIVE_TO_ALL_RP (ndofn,yy_send,yy_recv,commu,wsynch)
    else if( associated(xx_send) .and. .not. associated(xx_recv) ) then
       call PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,xx_send,yy_recv,commu,wsynch)
    else 
       call PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,xx_send,xx_recv,commu,wsynch)
    end if
  end subroutine PAR_SEND_RECEIVE_TO_ALL_RP_1
  subroutine PAR_SEND_RECEIVE_TO_ALL_RP_2(xx_send,xx_recv,commu,wsynch)
    implicit none
    real(rp),             pointer,  intent(in)  :: xx_send(:,:)
    real(rp),             pointer,  intent(out) :: xx_recv(:,:)
    type(comm_data_par),            intent(in)  :: commu
    character(*),         optional, intent(in)  :: wsynch
    integer(ip)                                 :: ndofn
    real(rp)                                    :: yy_send(2)
    real(rp)                                    :: yy_recv(2)
    if( associated(xx_send) ) then
       if( size(xx_send,2) < npoin ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
    end if
    if( .not. associated(xx_send) .and. associated(xx_recv) ) then
       ndofn = size(xx_recv,1)
       call PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,yy_send,xx_recv,commu,wsynch)
    else if( .not. associated(xx_send) .and. .not. associated(xx_recv) ) then
       ndofn = 1
       call PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,yy_send,yy_recv,commu,wsynch)
    else if( associated(xx_send) .and. .not. associated(xx_recv) ) then
       ndofn = size(xx_send,1)
       call PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,xx_send,yy_recv,commu,wsynch)
    else 
       ndofn = size(xx_send,1)
       if( ndofn /= size(xx_recv,1) ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
       call PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,xx_send,xx_recv,commu,wsynch)
    end if
  end subroutine PAR_SEND_RECEIVE_TO_ALL_RP_2

  subroutine PAR_SEND_RECEIVE_TO_ALL_RP(ndofn,xx_send,xx_recv,commu,wsynch)
    implicit none
    integer(ip),                    intent(in)  :: ndofn
    real(rp),                       intent(in)  :: xx_send(*)
    real(rp),                       intent(out) :: xx_recv(*)
    type(comm_data_par),            intent(in)  :: commu
    character(*),         optional, intent(in)  :: wsynch
    integer(ip)                                 :: inise,finse,inire,finre,count
    integer(ip)                                 :: dom_i,ineig,nsend,nrecv,kk
    integer(4)                                  :: istat4,nsend4,nrecv4,dom_i4
    integer(4)                                  :: PAR_COMM_TO_USE,count4
    logical(lg)                                 :: asynch
    integer(4),           allocatable           :: ireqq4(:) 
    integer(4),           pointer               :: status4(:,:)
    !
    ! Define communicator
    !
    PAR_COMM_TO_USE = int(commu % PAR_COMM_WORLD,4)
    !
    ! Asynchronous
    !
    if( present(wsynch) ) then
       if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
          asynch = .false.
       else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
          asynch = .true.
       else
          call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
       end if
    else
       asynch = .false.
    end if   
    if( asynch .and. commu % nneig /= 0_ip ) allocate(ireqq4(commu % nneig*2_ip))

    kk = 0
    istat4 = 0_4
    !
    ! Synchronous Send/receive
    !
    do ineig = 1,commu % nneig

       dom_i  = commu % neights(ineig)
       inise  = ndofn * ( commu % lsend_size(ineig)   - 1 ) + 1
       nsend  = ndofn * ( commu % lsend_size(ineig+1) - 1 ) + 1 - inise
       finse  = inise + nsend - 1
       inire  = ndofn * ( commu % lrecv_size(ineig)   - 1 ) + 1
       nrecv  = ndofn * ( commu % lrecv_size(ineig+1) - 1 ) + 1 - inire
       finre  = inire + nrecv - 1
       nsend4 = int(nsend,4)
       nrecv4 = int(nrecv,4)
       dom_i4 = int(dom_i,4)

#ifndef MPI_OFF
       if( asynch .and. commu % nneig /= 0_ip ) then

          if( nsend > 0 ) then
             kk = kk + 1
             call MPI_Isend(                            &
                  xx_send(inise:finse), nsend4,         &
                  MPI_DOUBLE_PRECISION,  dom_i4, 0_4,   &
                  PAR_COMM_TO_USE, ireqq4(kk), istat4   )         
          end if
          if( nrecv > 0 ) then
             kk = kk + 1
             call MPI_Irecv(                            &
                  xx_recv(inire:finre), nrecv4,         &
                  MPI_DOUBLE_PRECISION,  dom_i4, 0_4,   &
                  PAR_COMM_TO_USE, ireqq4(kk), istat4   )
          end if
       else
          if( nrecv /= 0 .and. nsend == 0 ) then
             call MPI_Recv(                          &
                  xx_recv(inire:finre), nrecv4,      &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                  PAR_COMM_TO_USE, status, istat4    )
          else if( nrecv == 0 .and. nsend /= 0 ) then
             call MPI_Send(                          &
                  xx_send(inise:finse), nsend4,      &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                  PAR_COMM_TO_USE, istat4            )             
          else if( nrecv /= 0 .and. nsend /= 0 ) then
             call MPI_Sendrecv(                      &
                  xx_send(inise:finse), nsend4,      &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                  xx_recv(inire:finre), nrecv4,      &
                  MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                  PAR_COMM_TO_USE, status, istat4    )
          end if
       end if
#endif
    end do
    !
    ! Wait in case of asynchronous, count is the total of communications actually performe       
    ! and is smaller or equal than the asynchronous communications requested (ireqq)
    !
#ifndef MPI_OFF
    if( asynch .and. commu % nneig /= 0_ip ) then
       count  = kk     
       count4 = int(count,4)         
       allocate( status4(MPI_STATUS_SIZE,count) )
       CALL MPI_WAITALL(count4,ireqq4,status4,istat4)
       deallocate( status4 ) 
       deallocate( ireqq4  )
    end if
#endif

  end subroutine PAR_SEND_RECEIVE_TO_ALL_RP

  !-----------------------------------------------------------------------
  !
  !> @brief   Bridge to broadcast data
  !> @details Bridge to broadcast data
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine PAR_EXCHANGE_IP_s(xx,xarra,icoun,ipass)
    implicit none
    integer(ip), intent(inout) :: xx
    integer(ip), intent(inout) :: xarra(:)
    integer(ip), intent(out)   :: icoun
    integer(ip), intent(in)    :: ipass
    icoun = icoun + 1
    if( ipass == 2 ) then
       if( IMASTER ) xarra(icoun) = xx
       if( ISLAVE  ) xx           = xarra(icoun)
    end if   
  end subroutine PAR_EXCHANGE_IP_s
  subroutine PAR_EXCHANGE_IP_1(xx,xarra,icoun,ipass)
    implicit none
    integer(ip), intent(inout) :: xx(:)
    integer(ip), intent(inout) :: xarra(:)
    integer(ip), intent(out)   :: icoun
    integer(ip), intent(in)    :: ipass
    integer(ip)                :: ii
    do ii = 1,size(xx)
       icoun = icoun + 1
       if( ipass == 2 ) then
          if( IMASTER ) xarra(icoun) = xx(ii)
          if( ISLAVE  ) xx(ii)       = xarra(icoun)
       end if
    end do
  end subroutine PAR_EXCHANGE_IP_1
  subroutine PAR_EXCHANGE_IP_2(xx,xarra,icoun,ipass)
    implicit none
    integer(ip), intent(inout) :: xx(:,:)
    integer(ip), intent(inout) :: xarra(:)
    integer(ip), intent(out)   :: icoun
    integer(ip), intent(in)    :: ipass
    integer(ip)                :: ii,jj
    do jj = 1,size(xx,2)
       do ii = 1,size(xx,1)
          icoun = icoun + 1
          if( ipass == 2 ) then
             if( IMASTER ) xarra(icoun) = xx(ii,jj)
             if( ISLAVE  ) xx(ii,jj)    = xarra(icoun)
          end if
       end do
    end do
  end subroutine PAR_EXCHANGE_IP_2

  subroutine PAR_EXCHANGE_RP_s(xx,xarra,icoun,ipass)
    implicit none
    real(rp),    intent(inout) :: xx
    real(rp),    intent(inout) :: xarra(:)
    integer(ip), intent(out)   :: icoun
    integer(ip), intent(in)    :: ipass
    icoun = icoun + 1
    if( ipass == 2 ) then
       if( IMASTER ) xarra(icoun) = xx
       if( ISLAVE  ) xx           = xarra(icoun)
    end if   
  end subroutine PAR_EXCHANGE_RP_s
  subroutine PAR_EXCHANGE_RP_1(xx,xarra,icoun,ipass)
    implicit none
    real(rp),    intent(inout) :: xx(:)
    real(rp),    intent(inout) :: xarra(:)
    integer(ip), intent(out)   :: icoun
    integer(ip), intent(in)    :: ipass
    integer(ip)                :: ii
    do ii = 1,size(xx)
       icoun = icoun + 1
       if( ipass == 2 ) then
          if( IMASTER ) xarra(icoun) = xx(ii)
          if( ISLAVE  ) xx(ii)       = xarra(icoun)
       end if
    end do
  end subroutine PAR_EXCHANGE_RP_1
  subroutine PAR_EXCHANGE_RP_2(xx,xarra,icoun,ipass)
    real(rp),    intent(inout) :: xx(:,:)
    real(rp),    intent(inout) :: xarra(:)
    integer(ip), intent(out)   :: icoun
    integer(ip), intent(in)    :: ipass
    integer(ip)                :: ii,jj
    do jj = 1,size(xx,2)
       do ii = 1,size(xx,1)
          icoun = icoun + 1
          if( ipass == 2 ) then
             if( IMASTER ) xarra(icoun) = xx(ii,jj)
             if( ISLAVE  ) xx(ii,jj)    = xarra(icoun)
          end if
       end do
    end do
  end subroutine PAR_EXCHANGE_RP_2

  subroutine PAR_EXCHANGE_CH(xx,xarra,icoun,ipass)
    character(*), intent(inout) :: xx
    character(*), intent(inout) :: xarra
    integer(ip),  intent(out)   :: icoun
    integer(ip),  intent(in)    :: ipass
    integer(ip)                 :: iposi,len_xarra,len_xx
    len_xx    = len(xx)
    len_xarra = len(xarra)   
    iposi     = icoun
    icoun     = icoun + len_xx
    if( ipass == 2 ) then       
       if( iposi+len_xx > len_xarra ) call runend('CEXCHA: MAXIMUM PARCH LENGTH EXCEEDED')
       if( IMASTER ) xarra(iposi+1:iposi+len_xx) = xx(1:len_xx)
       if( ISLAVE  ) xx(1:len_xx) = xarra(iposi+1:iposi+len_xx)
    end if
  end subroutine PAR_EXCHANGE_CH

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    29/04/2014
  !> @brief   Define if I am the master in this comunicatior
  !> @details Answer is TRUE if my rank is zero in communicator
  !
  !----------------------------------------------------------------------

  function PAR_IMASTER_IN_COMMUNICATOR(PAR_COMM_TO_USE)
    integer(4),  intent(in) :: PAR_COMM_TO_USE              !< Communicator
    integer(4)              :: my_rank
    logical(lg)             :: PAR_IMASTER_IN_COMMUNICATOR

    call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank)
    if( my_rank == 0_4 ) then
       PAR_IMASTER_IN_COMMUNICATOR = .true.
    else
       PAR_IMASTER_IN_COMMUNICATOR = .false.
    end if

  end function PAR_IMASTER_IN_COMMUNICATOR

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    16/05/2014
  !> @brief   MPI Barrier
  !> @details MPI Barrier
  !
  !----------------------------------------------------------------------

  subroutine PAR_BARRIER(where)
    character(*), optional, intent(in) :: where 
    integer(4)                         :: PAR_COMM_TO_USE4
    integer(4)                         :: istat4  

#ifndef MPI_OFF
    if( IPARALL ) then
       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE4)
       else
          call PAR_DEFINE_COMMUNICATOR('IN MY ZONE',PAR_COMM_TO_USE4)
       end if
       call MPI_Barrier( PAR_COMM_TO_USE4, istat4 )
    end if
#endif

  end subroutine PAR_BARRIER

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    16/05/2014
  !> @brief   Initialize non-blocking communications
  !> @details Initialize non-blocking communications
  !
  !----------------------------------------------------------------------

  subroutine PAR_INITIALIZE_NON_BLOCKING_COMM()
    integer(ip) :: ii
    nullify(non_blocking)
    allocate(non_blocking(10))
    do ii = 1,size(non_blocking)
       non_blocking(ii) % count4 = 0
       nullify(non_blocking(ii) % request4)
    end do
  end subroutine PAR_INITIALIZE_NON_BLOCKING_COMM

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    16/05/2014
  !> @brief   Start non-blocking communications
  !> @details Start non-blocking communications
  !
  !----------------------------------------------------------------------

  subroutine PAR_START_NON_BLOCKING_COMM_8(knonblocking,max_number_neighbors)
    integer(ip), intent(in) :: knonblocking
    integer(8),  intent(in) :: max_number_neighbors
    if( knonblocking < 0 .or. knonblocking > size(non_blocking) ) then
       call runend('PAR_START_NON_BLOCKING_COMM: WRONG NON-BLOCKING STRUCTURE NUMBER')
    else
       if( non_blocking(knonblocking) % count4 /= 0 ) then
          call runend('PAR_START_NON_BLOCKING_COMM: AN ASYNCHRONOUS SEND/RECEIVE IS ALREADY STARTED')
       else
          if (associated(non_blocking(knonblocking) % request4) ) then
             call runend('PAR_START_NON_BLOCKING_COMM: A NON-BLOCKING COMMUNICATOR NUMBER IS ALREADY ASSOCIATED')
          else
             allocate(non_blocking(knonblocking) % request4(max_number_neighbors*2))
             non_blocking(knonblocking) % count4 = 0
          end if
       end if
    end if
  end subroutine PAR_START_NON_BLOCKING_COMM_8

  subroutine PAR_START_NON_BLOCKING_COMM_4(knonblocking,max_number_neighbors)
    integer(ip), intent(in) :: knonblocking
    integer(4),  intent(in) :: max_number_neighbors
    if( knonblocking < 0 .or. knonblocking > size(non_blocking) ) then
       call runend('PAR_START_NON_BLOCKING_COMM: WRONG NON-BLOCKING STRUCTURE NUMBER')
    else
       if( non_blocking(knonblocking) % count4 /= 0 ) then
          call runend('PAR_START_NON_BLOCKING_COMM: AN ASYNCHRONOUS SEND/RECEIVE IS ALREADY STARTED')
       else
          if (associated(non_blocking(knonblocking) % request4) ) then
             call runend('PAR_START_NON_BLOCKING_COMM: A NON-BLOCKING COMMUNICATOR NUMBER IS ALREADY ASSOCIATED')
          else
             allocate(non_blocking(knonblocking) % request4(max_number_neighbors*2))
             non_blocking(knonblocking) % count4 = 0
          end if
       end if
    end if
  end subroutine PAR_START_NON_BLOCKING_COMM_4

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    16/05/2014
  !> @brief   End non-blocking communications
  !> @details End non-blocking communications
  !
  !----------------------------------------------------------------------

  subroutine PAR_END_NON_BLOCKING_COMM(knonblocking)
    integer(ip), intent(in) :: knonblocking
    integer(4),  pointer    :: status4(:,:)
    integer(4)              :: istat4
    
#ifndef MPI_OFF
    if( non_blocking(knonblocking) % count4 > 0 ) then
       allocate( status4(MPI_STATUS_SIZE,non_blocking(knonblocking) % count4) )
       CALL MPI_WAITALL(non_blocking(knonblocking) % count4,non_blocking(knonblocking) % request4,status4,istat4)
       if( istat4 /= 0 ) call runend('NON BLOCKING SEND/RECEIVE COULD NOT BE COMPLETED')
       deallocate( status4 ) 
       deallocate( non_blocking(knonblocking) % request4 )
       non_blocking(knonblocking) % count4 = 0
    end if
    if( associated(non_blocking(knonblocking) % request4) ) then
       deallocate( non_blocking(knonblocking) % request4  )
    end if
#endif

  end subroutine PAR_END_NON_BLOCKING_COMM

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    16/05/2014
  !> @brief   Set non-blocking communication number
  !> @details Set non-blocking communication number
  !
  !----------------------------------------------------------------------

  subroutine PAR_SET_NON_BLOCKING_COMM_NUMBER(knonblocking)
    integer(ip), intent(in) :: knonblocking
   
    if( knonblocking < 0 .or. knonblocking > size(non_blocking) ) then
       call runend('PAR_START_NON_BLOCKING_COMM: WRONG NON-BLOCKING STRUCTURE NUMBER')
    else 
       inonblocking = knonblocking
    end if

  end subroutine PAR_SET_NON_BLOCKING_COMM_NUMBER

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_GHOST_NODE_EXCHANGE_RP
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_NODE_EXCHANGE_RP_00(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_NODE_EXCHANGE_RP_00

  subroutine PAR_GHOST_NODE_EXCHANGE_RP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_NODE_EXCHANGE_RP_0

  subroutine PAR_GHOST_NODE_EXCHANGE_RP_1(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = 1
    if( associated(xx) ) then
       if( size(xx,1) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_NODE_EXCHANGE_RP_1

  subroutine PAR_GHOST_NODE_EXCHANGE_RP_2(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( associated(xx) ) then
       if( size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_NODE_EXCHANGE_RP_2

  subroutine PAR_GHOST_NODE_EXCHANGE_RP_3(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)*size(xx,2)
       if( size(xx,3) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_NODE_EXCHANGE_RP_3

  subroutine PAR_GHOST_NODE_EXCHANGE_RP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)
       if( size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
       call PAR_GHOST_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_NODE_EXCHANGE_RP_2b

  !----------------------------------------------------------------------
  !
  ! PAR_GHOST_NODE_EXCHANGE_RP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_NODE_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    real(rp),                       intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,jj,dom_i
    integer(ip)                                   :: nsize_send,nsize_recv
    integer(ip)                                   :: ipoin,ini_send,ini_recv,kk,idofn
    integer(4)                                    :: nsize_send4,nsize_recv4
    integer(4)                                    :: istat4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( commu % ghost_send_node_dim == -1 .or. commu % ghost_recv_node_dim == -1 ) &
                  call runend('FRINGE NODE EXCHANGE NOT COMPUTED')
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_rsend(commu % ghost_send_node_dim * ndofn))
             allocate(tmp_rrecv(commu % ghost_recv_node_dim * ndofn))
             istat4 = 0_4 
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % ghost_send_node_dim
                ipoin = commu % ghost_send_node_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_rsend(kk) = xx(idofn,ipoin)
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   dom_i      = commd % neights(ii)
                   ini_send   = ndofn * ( commd % ghost_send_node_size(ii)   -1 ) + 1
                   nsize_send = ndofn * ( commd % ghost_send_node_size(ii+1) -1 ) + 1 - ini_send 
                   ini_recv   = ndofn * ( commd % ghost_recv_node_size(ii)   -1 ) + 1  
                   nsize_recv = ndofn * ( commd % ghost_recv_node_size(ii+1) -1 ) + 1 - ini_recv 

                   nsize_send4 = int(nsize_send,4)
                   nsize_recv4 = int(nsize_recv,4)

                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_rsend(ini_send:ini_send+nsize_send-1), nsize_send4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4,                     &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_rrecv(ini_recv:ini_recv+nsize_recv-1), nsize_recv4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4,                     &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                   else
                      if( nsize_recv /= 0 .and. nsize_send == 0 ) then
                         call MPI_Recv(                          &
                              tmp_rrecv(ini_recv:), nsize_recv4, &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                              PAR_COMM_TO_USE, status, istat4    )
                      else if( nsize_recv == 0 .and. nsize_send /= 0 ) then
                         call MPI_Send(                          &
                              tmp_rsend(ini_send:), nsize_send4, &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                              PAR_COMM_TO_USE, istat4            )                       
                      else if( nsize_recv /= 0 .and. nsize_send /= 0 ) then
                         call MPI_Sendrecv(                         &
                              tmp_rsend(ini_send:), nsize_send4,    &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4,    &
                              tmp_rrecv(ini_recv:), nsize_recv4,    &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4,    &
                              PAR_COMM_TO_USE, status, istat4       )
                      end if
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_GHOST_NODE_EXCHANGE_RP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             if( istat4 /= 0 ) call runend('WRONG SEND/RECEIVE')
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % ghost_recv_node_dim
                   ipoin = commu % ghost_recv_node_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) + tmp_rrecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % ghost_recv_node_dim
                   ipoin = commu % ghost_recv_node_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = max(xx(idofn,ipoin),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % ghost_recv_node_dim
                   ipoin = commu % ghost_recv_node_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = min(xx(idofn,ipoin),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'REPLACE' .or. trim(what) == 'SUBSTITUTE' ) then 
                !
                ! Replace value on my fringe nodes according to what I have received
                !
                kk = 0
                do jj = 1,commu % ghost_recv_node_dim
                   ipoin = commu % ghost_recv_node_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = tmp_rrecv(kk)
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_rrecv)
             deallocate(tmp_rsend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_GHOST_NODE_EXCHANGE_RP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_GHOST_NODE_EXCHANGE_IP
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_NODE_EXCHANGE_IP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    integer(ip),                    intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_NODE_EXCHANGE_IP_0

  subroutine PAR_GHOST_NODE_EXCHANGE_IP_1(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

   if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = 1
       if( size(xx,1) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_NODE_EXCHANGE_IP_1

  subroutine PAR_GHOST_NODE_EXCHANGE_IP_2(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)
       if( size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_NODE_EXCHANGE_IP_2

  subroutine PAR_GHOST_NODE_EXCHANGE_IP_3(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)*size(xx,2)
       if( size(xx,3) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_NODE_EXCHANGE_IP_3

  subroutine PAR_GHOST_NODE_EXCHANGE_IP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),  pointer,  intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)
       if( size(xx,2) /= npoin .and. size(xx,2) /= npoin_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
       call PAR_GHOST_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_NODE_EXCHANGE_IP_2b

 !----------------------------------------------------------------------
  !
  ! PAR_GHOST_NODE_EXCHANGE_RP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_NODE_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    integer(ip),                    intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,jj,dom_i
    integer(ip)                                   :: nsize_send,nsize_recv
    integer(ip)                                   :: ipoin,ini_send,ini_recv,kk,idofn
    integer(4)                                    :: nsize_send4,nsize_recv4
    integer(4)                                    :: istat4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( commu % ghost_send_node_dim == -1 .or. commu % ghost_recv_node_dim == -1 ) &
                  call runend('FRINGE NODE EXCHANGE NOT COMPUTED')
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_isend(commu % ghost_send_node_dim * ndofn))
             allocate(tmp_irecv(commu % ghost_recv_node_dim * ndofn))
             istat4 = 0_4 
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % ghost_send_node_dim
                ipoin = commu % ghost_send_node_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_isend(kk) = xx(idofn,ipoin)
                end do
             end do
             istat4 = 0_4
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   dom_i      = commd % neights(ii)
                   ini_send   = ndofn * ( commd % ghost_send_node_size(ii)   -1 ) + 1
                   nsize_send = ndofn * ( commd % ghost_send_node_size(ii+1) -1 ) + 1 - ini_send 
                   ini_recv   = ndofn * ( commd % ghost_recv_node_size(ii)   -1 ) + 1  
                   nsize_recv = ndofn * ( commd % ghost_recv_node_size(ii+1) -1 ) + 1 - ini_recv 

                   nsize_send4 = int(nsize_send,4)
                   nsize_recv4 = int(nsize_recv,4)

                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_isend(ini_send:ini_send+nsize_send-1), nsize_send4, &
                           PAR_INTEGER,  dom_i4, 0_4,                              &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_irecv(ini_recv:ini_recv+nsize_recv-1), nsize_recv4, &
                           PAR_INTEGER,  dom_i4, 0_4,                              &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                   else 
                      if( nsize_recv /= 0 .and. nsize_send == 0 ) then
                         call MPI_Recv(                          &
                              tmp_irecv(ini_recv:), nsize_recv4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, status, istat4    )
                      else if( nsize_recv == 0 .and. nsize_send /= 0 ) then
                         call MPI_Send(                          &
                              tmp_isend(ini_send:), nsize_send4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, istat4            )                       
                      else if( nsize_recv /= 0 .and. nsize_send /= 0 ) then
                         call MPI_Sendrecv(                      &
                              tmp_isend(ini_send:), nsize_send4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              tmp_irecv(ini_recv:), nsize_recv4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, status, istat4    )
                      end if
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_GHOST_NODE_EXCHANGE_RP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             if( istat4 /= 0 ) call runend('WRONG SEND/RECEIVE')
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % ghost_recv_node_dim
                   ipoin = commu % ghost_recv_node_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) + tmp_irecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % ghost_recv_node_dim
                   ipoin = commu % ghost_recv_node_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = max(xx(idofn,ipoin),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % ghost_recv_node_dim
                   ipoin = commu % ghost_recv_node_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = min(xx(idofn,ipoin),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'REPLACE' .or. trim(what) == 'SUBSTITUTE' ) then 
                !
                ! Replace value on my fringe nodes according to what I have received
                !
                kk = 0
                do jj = 1,commu % ghost_recv_node_dim
                   ipoin = commu % ghost_recv_node_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = tmp_irecv(kk)
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_irecv)
             deallocate(tmp_isend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_GHOST_NODE_EXCHANGE_IP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_GHOST_ELEMENT_EXCHANGE_IP
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    integer(ip),                    intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_ELEMENT_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_0

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_1(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = 1
       if( size(xx,1) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_ELEMENT_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_1

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_2(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( size(xx,2) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_ELEMENT_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_2

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_3(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)*size(xx,2)
       if( size(xx,3) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_ELEMENT_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_3

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),  pointer,  intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)
       if( size(xx,2) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
       call PAR_GHOST_ELEMENT_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP_2b

 !----------------------------------------------------------------------
  !
  ! PAR_GHOST_ELEMENT_EXCHANGE_RP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    integer(ip),                    intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,jj,dom_i
    integer(ip)                                   :: nsize_send,nsize_recv
    integer(ip)                                   :: ipoin,ini_send,ini_recv,kk,idofn
    integer(4)                                    :: nsize_send4,nsize_recv4
    integer(4)                                    :: istat4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_ELEM_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( commu % ghost_send_elem_dim == -1 .or. commu % ghost_recv_elem_dim == -1 ) &
                  call runend('FRINGE NODE EXCHANGE NOT COMPUTED')
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_isend(commu % ghost_send_elem_dim * ndofn))
             allocate(tmp_irecv(commu % ghost_recv_elem_dim * ndofn))
             istat4 = 0_4 
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % ghost_send_elem_dim
                ipoin = commu % ghost_send_elem_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_isend(kk) = xx(idofn,ipoin)
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   dom_i      = commd % neights(ii)
                   ini_send   = ndofn * ( commd % ghost_send_elem_size(ii)   -1 ) + 1
                   nsize_send = ndofn * ( commd % ghost_send_elem_size(ii+1) -1 ) + 1 - ini_send 
                   ini_recv   = ndofn * ( commd % ghost_recv_elem_size(ii)   -1 ) + 1  
                   nsize_recv = ndofn * ( commd % ghost_recv_elem_size(ii+1) -1 ) + 1 - ini_recv 

                   nsize_send4 = int(nsize_send,4)
                   nsize_recv4 = int(nsize_recv,4)

                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_isend(ini_send:ini_send+nsize_send-1), nsize_send4, &
                           PAR_INTEGER,  dom_i4, 0_4,                              &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_irecv(ini_recv:ini_recv+nsize_recv-1), nsize_recv4, &
                           PAR_INTEGER,  dom_i4, 0_4,                              &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                   else 
                      if( nsize_recv /= 0 .and. nsize_send == 0 ) then
                         call MPI_Recv(                          &
                              tmp_irecv(ini_recv:), nsize_recv4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, status, istat4    )
                      else if( nsize_recv == 0 .and. nsize_send /= 0 ) then
                         call MPI_Send(                          &
                              tmp_isend(ini_send:), nsize_send4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, istat4            )                       
                      else if( nsize_recv /= 0 .and. nsize_send /= 0 ) then
                         call MPI_Sendrecv(                      &
                              tmp_isend(ini_send:), nsize_send4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              tmp_irecv(ini_recv:), nsize_recv4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, status, istat4    )
                      end if
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_GHOST_ELEMENT_EXCHANGE_RP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             if( istat4 /= 0 ) call runend('WRONG SEND/RECEIVE')
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % ghost_recv_elem_dim
                   ipoin = commu % ghost_recv_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) + tmp_irecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % ghost_recv_elem_dim
                   ipoin = commu % ghost_recv_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = max(xx(idofn,ipoin),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % ghost_recv_elem_dim
                   ipoin = commu % ghost_recv_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = min(xx(idofn,ipoin),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'REPLACE' .or. trim(what) == 'SUBSTITUTE' ) then 
                !
                ! Replace value on my fringe nodes according to what I have received
                !
                kk = 0
                do jj = 1,commu % ghost_recv_elem_dim
                   ipoin = commu % ghost_recv_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = tmp_irecv(kk)
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_irecv)
             deallocate(tmp_isend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_IP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_GHOST_ELEMENT_EXCHANGE_RP
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_00(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_00

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_0

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_1(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = 1
       if( size(xx,1) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_1

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_2(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( size(xx,2) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_2

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_3(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)*size(xx,2)
       if( size(xx,3) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_3

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)
       if( size(xx,2) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
       call PAR_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP_2b

  !----------------------------------------------------------------------
  !
  ! PAR_GHOST_ELEMENT_EXCHANGE_RP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    real(rp),                       intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,jj,dom_i
    integer(ip)                                   :: nsize_send,nsize_recv
    integer(ip)                                   :: ipoin,ini_send,ini_recv,kk,idofn
    integer(4)                                    :: nsize_send4,nsize_recv4
    integer(4)                                    :: istat4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_ELEMENT_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( commu % ghost_send_elem_dim == -1 .or. commu % ghost_recv_elem_dim == -1 ) &
                  call runend('FRINGE NODE EXCHANGE NOT COMPUTED')
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_rsend(commu % ghost_send_elem_dim * ndofn))
             allocate(tmp_rrecv(commu % ghost_recv_elem_dim * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % ghost_send_elem_dim
                ipoin = commu % ghost_send_elem_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_rsend(kk) = xx(idofn,ipoin)
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             kk = 0
             istat4 = 0_4 
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   dom_i      = commd % neights(ii)
                   ini_send   = ndofn * ( commd % ghost_send_elem_size(ii)   -1 ) + 1
                   nsize_send = ndofn * ( commd % ghost_send_elem_size(ii+1) -1 ) + 1 - ini_send 
                   ini_recv   = ndofn * ( commd % ghost_recv_elem_size(ii)   -1 ) + 1  
                   nsize_recv = ndofn * ( commd % ghost_recv_elem_size(ii+1) -1 ) + 1 - ini_recv 

                   nsize_send4 = int(nsize_send,4)
                   nsize_recv4 = int(nsize_recv,4)

                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_rsend(ini_send:ini_send+nsize_send-1), nsize_send4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4,                     &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_rrecv(ini_recv:ini_recv+nsize_recv-1), nsize_recv4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4,                     &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                   else
                      if( nsize_recv /= 0 .and. nsize_send == 0 ) then
                         call MPI_Recv(                          &
                              tmp_rrecv(ini_recv:), nsize_recv4, &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                              PAR_COMM_TO_USE, status, istat4    )
                      else if( nsize_recv == 0 .and. nsize_send /= 0 ) then
                         call MPI_Send(                          &
                              tmp_rsend(ini_send:), nsize_send4, &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                              PAR_COMM_TO_USE, istat4            )                       
                      else if( nsize_recv /= 0 .and. nsize_send /= 0 ) then
                         call MPI_Sendrecv(                         &
                              tmp_rsend(ini_send:), nsize_send4,    &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4,    &
                              tmp_rrecv(ini_recv:), nsize_recv4,    &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4,    &
                              PAR_COMM_TO_USE, status, istat4       )
                      end if
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_GHOST_ELEMENT_EXCHANGE_RP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             if( istat4 /= 0 ) call runend('WRONG SEND/RECEIVE')
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % ghost_recv_elem_dim
                   ipoin = commu % ghost_recv_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) + tmp_rrecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % ghost_recv_elem_dim
                   ipoin = commu % ghost_recv_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = max(xx(idofn,ipoin),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % ghost_recv_elem_dim
                   ipoin = commu % ghost_recv_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = min(xx(idofn,ipoin),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'REPLACE' .or. trim(what) == 'SUBSTITUTE' ) then 
                !
                ! Replace value on my fringe nodes according to what I have received
                !
                kk = 0
                do jj = 1,commu % ghost_recv_elem_dim
                   ipoin = commu % ghost_recv_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = tmp_rrecv(kk)
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_rrecv)
             deallocate(tmp_rsend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_GHOST_ELEMENT_EXCHANGE_RP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_GHOST_BOUNDARY_EXCHANGE_IP
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    integer(ip),                    intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_BOUNDARY_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_0

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_1(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = 1
    if( associated(xx) ) then
       if( size(xx,1) /= nboun_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_BOUNDARY_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_1

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_2(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( associated(xx) ) then
       if( size(xx,2) /= nboun_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_BOUNDARY_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_2

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_3(xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)*size(xx,2)
    if( associated(xx) ) then
       if( size(xx,3) /= nboun_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_BOUNDARY_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_3

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),  pointer,  intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( associated(xx) ) then
       if( size(xx,2) /= nboun_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
       call PAR_GHOST_BOUNDARY_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP_2b

 !----------------------------------------------------------------------
  !
  ! PAR_GHOST_BOUNDARY_EXCHANGE_RP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    integer(ip),                    intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,jj,dom_i
    integer(ip)                                   :: nsize_send,nsize_recv
    integer(ip)                                   :: ipoin,ini_send,ini_recv,kk,idofn
    integer(4)                                    :: nsize_send4,nsize_recv4
    integer(4)                                    :: istat4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    istat4 = 0_4
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_BOUN_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( commu % ghost_send_boun_dim == -1 .or. commu % ghost_recv_boun_dim == -1 ) &
                  call runend('FRINGE NODE EXCHANGE NOT COMPUTED')
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_isend(commu % ghost_send_boun_dim * ndofn))
             allocate(tmp_irecv(commu % ghost_recv_boun_dim * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % ghost_send_boun_dim
                ipoin = commu % ghost_send_boun_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_isend(kk) = xx(idofn,ipoin)
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             ! 
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   dom_i      = commd % neights(ii)
                   ini_send   = ndofn * ( commd % ghost_send_boun_size(ii)   -1 ) + 1
                   nsize_send = ndofn * ( commd % ghost_send_boun_size(ii+1) -1 ) + 1 - ini_send 
                   ini_recv   = ndofn * ( commd % ghost_recv_boun_size(ii)   -1 ) + 1  
                   nsize_recv = ndofn * ( commd % ghost_recv_boun_size(ii+1) -1 ) + 1 - ini_recv 

                   nsize_send4 = int(nsize_send,4)
                   nsize_recv4 = int(nsize_recv,4)

                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_isend(ini_send:ini_send+nsize_send-1), nsize_send4, &
                           PAR_INTEGER,  dom_i4, 0_4,                              &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_irecv(ini_recv:ini_recv+nsize_recv-1), nsize_recv4, &
                           PAR_INTEGER,  dom_i4, 0_4,                              &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                   else 
                      if( nsize_recv /= 0 .and. nsize_send == 0 ) then
                         call MPI_Recv(                          &
                              tmp_irecv(ini_recv:), nsize_recv4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, status, istat4    )
                      else if( nsize_recv == 0 .and. nsize_send /= 0 ) then
                         call MPI_Send(                          &
                              tmp_isend(ini_send:), nsize_send4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, istat4            )                       
                      else if( nsize_recv /= 0 .and. nsize_send /= 0 ) then
                         call MPI_Sendrecv(                      &
                              tmp_isend(ini_send:), nsize_send4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              tmp_irecv(ini_recv:), nsize_recv4, &
                              PAR_INTEGER, dom_i4, 0_4,          &
                              PAR_COMM_TO_USE, status, istat4    )
                      end if
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_GHOST_BOUNDARY_EXCHANGE_IP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             if( istat4 /= 0 ) call runend('WRONG SEND/RECEIVE')
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % ghost_recv_boun_dim
                   ipoin = commu % ghost_recv_boun_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) + tmp_irecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % ghost_recv_boun_dim
                   ipoin = commu % ghost_recv_boun_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = max(xx(idofn,ipoin),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % ghost_recv_boun_dim
                   ipoin = commu % ghost_recv_boun_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = min(xx(idofn,ipoin),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'REPLACE' .or. trim(what) == 'SUBSTITUTE' ) then 
                !
                ! Replace value on my fringe nodes according to what I have received
                !
                kk = 0
                do jj = 1,commu % ghost_recv_boun_dim
                   ipoin = commu % ghost_recv_boun_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = tmp_irecv(kk)
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_irecv)
             deallocate(tmp_isend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_IP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_GHOST_BOUNDARY_EXCHANGE_RP
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_00(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_BOUNDARY_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_00

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_GHOST_BOUNDARY_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_0

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_1(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = 1
    if( associated(xx) ) then
       if( size(xx,1) /= nboun_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_BOUNDARY_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_1

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_2(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( associated(xx) ) then
       if( size(xx,2) /= nboun_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_BOUNDARY_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_2

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_3(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = size(xx,1)*size(xx,2)
    if( associated(xx) ) then
       if( size(xx,3) /= nboun_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_GHOST_BOUNDARY_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_3

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( associated(xx) ) then
       if( size(xx,2) /= nboun_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
       call PAR_GHOST_BOUNDARY_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP_2b

  !----------------------------------------------------------------------
  !
  ! PAR_GHOST_BOUNDARY_EXCHANGE_RP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    real(rp),                       intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,jj,dom_i
    integer(ip)                                   :: nsize_send,nsize_recv
    integer(ip)                                   :: ipoin,ini_send,ini_recv,kk,idofn
    integer(4)                                    :: nsize_send4,nsize_recv4
    integer(4)                                    :: istat4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_BOUNDARY_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( commu % ghost_send_boun_dim == -1 .or. commu % ghost_recv_boun_dim == -1 ) &
                  call runend('FRINGE NODE EXCHANGE NOT COMPUTED')
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_rsend(commu % ghost_send_boun_dim * ndofn))
             allocate(tmp_rrecv(commu % ghost_recv_boun_dim * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % ghost_send_boun_dim
                ipoin = commu % ghost_send_boun_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_rsend(kk) = xx(idofn,ipoin)
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !    
             istat4 = 0_4 
             kk = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   dom_i      = commd % neights(ii)
                   ini_send   = ndofn * ( commd % ghost_send_boun_size(ii)   -1 ) + 1
                   nsize_send = ndofn * ( commd % ghost_send_boun_size(ii+1) -1 ) + 1 - ini_send 
                   ini_recv   = ndofn * ( commd % ghost_recv_boun_size(ii)   -1 ) + 1  
                   nsize_recv = ndofn * ( commd % ghost_recv_boun_size(ii+1) -1 ) + 1 - ini_recv 

                   nsize_send4 = int(nsize_send,4)
                   nsize_recv4 = int(nsize_recv,4)

                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_rsend(ini_send:ini_send+nsize_send-1), nsize_send4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4,                     &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_rrecv(ini_recv:ini_recv+nsize_recv-1), nsize_recv4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4,                     &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                   else
                      if( nsize_recv /= 0 .and. nsize_send == 0 ) then
                         call MPI_Recv(                          &
                              tmp_rrecv(ini_recv:), nsize_recv4, &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                              PAR_COMM_TO_USE, status, istat4    )
                      else if( nsize_recv == 0 .and. nsize_send /= 0 ) then
                         call MPI_Send(                          &
                              tmp_rsend(ini_send:), nsize_send4, &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                              PAR_COMM_TO_USE, istat4            )                       
                      else if( nsize_recv /= 0 .and. nsize_send /= 0 ) then
                         call MPI_Sendrecv(                         &
                              tmp_rsend(ini_send:), nsize_send4,    &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4,    &
                              tmp_rrecv(ini_recv:), nsize_recv4,    &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4,    &
                              PAR_COMM_TO_USE, status, istat4       )
                      end if
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_GHOST_BOUNDARY_EXCHANGE_RP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             if( istat4 /= 0 ) call runend('WRONG SEND/RECEIVE')
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % ghost_recv_boun_dim
                   ipoin = commu % ghost_recv_boun_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) + tmp_rrecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % ghost_recv_boun_dim
                   ipoin = commu % ghost_recv_boun_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = max(xx(idofn,ipoin),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % ghost_recv_boun_dim
                   ipoin = commu % ghost_recv_boun_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = min(xx(idofn,ipoin),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'REPLACE' .or. trim(what) == 'SUBSTITUTE' ) then 
                !
                ! Replace value on my fringe nodes according to what I have received
                !
                kk = 0
                do jj = 1,commu % ghost_recv_boun_dim
                   ipoin = commu % ghost_recv_boun_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = tmp_rrecv(kk)
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_rrecv)
             deallocate(tmp_rsend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_GHOST_BOUNDARY_EXCHANGE_RP

  !----------------------------------------------------------------------
  !
  ! WAITALL
  !
  !----------------------------------------------------------------------

  subroutine PAR_WAITALL()
    integer(4) :: istat4
    integer(4) :: count4
#ifndef MPI_OFF    
    integer(4) :: status4(MPI_STATUS_SIZE,1_ip)
#endif

#ifndef MPI_OFF    
    if( IPARALL ) then
       count4 = 1_4
       call MPI_WAITALL(count4,ireq41,status4,istat4)
       deallocate(yy_non_blocking)
    end if
#endif
  end subroutine PAR_WAITALL

  !----------------------------------------------------------------------
  !
  ! PAR_ARRAY_EXCHANGE_IP: FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_ARRAY_EXCHANGE_IP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    integer(ip),                    intent(inout) :: xx(*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,nsize,jj,dom_i
    integer(ip)                                   :: ipoin,ini,kk,idofn
    integer(4)                                    :: istat4,nsize4,count4
    integer(4)                                    :: dom_i4,my_rank4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       ! 
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_NODE_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_isend(commu % nneig * ndofn))
             allocate(tmp_irecv(commu % nneig * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do ii = 1,commu % nneig
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_isend(kk) = xx(idofn)
                   tmp_irecv(kk) = 0
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             istat4 = 0_4
             kk     = 0
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   ini   = ( ii - 1 ) * ndofn + 1
                   nsize = ndofn

                   nsize4 = int(nsize,4)
                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_isend(ini:ini+nsize-1), nsize4, &
                           PAR_INTEGER,  dom_i4, 0_4,          &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_irecv(ini:ini+nsize-1), nsize4, &
                           PAR_INTEGER,  dom_i4, 0_4,          &
                           PAR_COMM_TO_USE, ireq4(kk), istat4 )
                   else
                      call MPI_Sendrecv(                       &
                           tmp_isend(ini:), nsize4,            &
                           PAR_INTEGER, dom_i4, 0_4,           &
                           tmp_irecv(ini:), nsize4,            &
                           PAR_INTEGER, dom_i4, 0_4,           &
                           PAR_COMM_TO_USE, status, istat4    )
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_INTERFACE_NODE_EXCHANGE_IP: MPI ERROR')
                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do ii = 1,commu % nneig
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn) = xx(idofn) + tmp_irecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do ii = 1,commu % nneig
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn) = max(xx(idofn),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do ii = 1,commu % nneig
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn) = min(xx(idofn),tmp_irecv(kk))
                   end do
                end do

             else if( trim(what) == 'TAKE MIN' ) then 
                !
                ! TAKE MIN
                !
                call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank4)
                kk = 0
                do ii = 1,commu % nneig
                   dom_i = commu % neights(ii)  
                   if( my_rank4 > dom_i ) then
                      do idofn = 1,ndofn
                         kk = kk + 1
                         xx(idofn) = tmp_irecv(kk)
                      end do
                   else
                      kk = kk + ndofn
                   end if
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_irecv)
             deallocate(tmp_isend)

          end if

       end if

    end if
#endif          
    
  end subroutine PAR_ARRAY_EXCHANGE_IP

  !----------------------------------------------------------------------
  !
  ! PAR_ALL_TO_ALL_ARRAY_OPERATION_IP: FOR INTEGERS
  ! INPUT:  XX(NDOFN) for all slaves
  ! OUTPUT: XX(IDOFN) = XX(IDOFN) if my ranks is the max who have XX(IDOFN) /= 0
  !                   = 0 otherwise
  !         For the master XX(IDOFN) = rank of partition who has XX(IDOFN) /= 0
  ! 
  !
  !----------------------------------------------------------------------

  subroutine PAR_ALL_TO_ALL_ARRAY_OPERATION_IP(ndofn,xx,what,where)
    implicit none
    integer(ip),         intent(in)    :: ndofn
    integer(ip),         intent(inout) :: xx(*)
    character(*),        intent(in)    :: what
    character(*),        optional      :: where
    integer(ip)                        :: idofn
    integer(4)                         :: my_rank4
    integer(4)                         :: PAR_COMM_TO_USE4
    integer(ip),         pointer       :: lranks(:)

#ifndef MPI_OFF
    if( IPARALL ) then

       if( present(where) ) then
          call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE4)
       else 
          call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE4)  
       end if
       call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE4,my_rank4)

       allocate( lranks(ndofn) )

       if( trim(what) == 'CHOOSE ONLY ONE' ) then 
          if( INOTMASTER ) then
             do idofn = 1,ndofn
                if( xx(idofn) > 0 ) then
                   lranks(idofn) = int(my_rank4,ip) 
                else
                   lranks(idofn) = 0_ip
                end if
             end do
          end if
          
          if( present(where) ) then
             call PAR_MAX(ndofn,lranks,where)
          else
             call PAR_MAX(ndofn,lranks)
          end if

          if( INOTMASTER ) then
             do idofn = 1,ndofn
                if( my_rank4 /= lranks(idofn) ) xx(idofn) = 0
             end do
          else
             do idofn = 1,ndofn
                xx(idofn) = lranks(idofn)
             end do
          end if

       end if

       deallocate( lranks )

    end if
#endif

  end subroutine PAR_ALL_TO_ALL_ARRAY_OPERATION_IP

  !----------------------------------------------------------------------
  !
  ! PAR_POINT_TO_POINT_ARRAY_OPERATION_IP: FOR INTEGERS
  ! INPUT/OUTPUT:  XX_SEND_RECV(NDOFN) for all slaves
  ! Operate on arrays between neighbors in the communicator.
  !
  !----------------------------------------------------------------------

  subroutine PAR_POINT_TO_POINT_ARRAY_OPERATION_IP_1(xx_send_recv,where,what)
    implicit none
    integer(ip),          pointer,  intent(inout) :: xx_send_recv(:)
    character(*),                   intent(in)    :: where
    character(*),                   intent(in)    :: what
    integer(ip)                                   :: ndofn
    type(comm_data_par),  pointer                 :: commu
    integer(4)                                    :: PAR_COMM_TO_USE

    if( ISEQUEN ) return

    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)

    if( .not. associated(xx_send_recv) ) then
       return
    else
       ndofn = size(xx_send_recv)
       call PAR_POINT_TO_POINT_ARRAY_OPERATION_IP(ndofn,xx_send_recv,PAR_COMM_TO_USE,commu,what)
    end if
  end subroutine PAR_POINT_TO_POINT_ARRAY_OPERATION_IP_1

  subroutine PAR_POINT_TO_POINT_ARRAY_OPERATION_IP(ndofn,xx_send_recv,PAR_COMM_TO_USE,commu,what)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    integer(ip),                    intent(inout) :: xx_send_recv(*)
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    type(comm_data_par),            intent(in)    :: commu
    character(*),                   intent(in)    :: what
    integer(ip)                                   :: dom_i,ineig,nsend,nrecv,ii
    integer(ip),          pointer                 :: xx_recv(:)
    integer(4)                                    :: istat4,dom_i4,my_rank4
    integer(4)                                    :: ndofn4
    !
    ! Define communicator
    !
    call PAR_COMM_RANK_AND_SIZE(PAR_COMM_TO_USE,my_rank4)
    !
    ! Allocate
    !
    nullify(xx_recv)
    allocate(xx_recv(ndofn))
    ndofn4 = int(ndofn,4)
    !
    ! Send/receive and operate
    !
    do ineig = 1,commu % nneig

       dom_i  = commu % neights(ineig)
       dom_i4 = int(dom_i,4)

#ifndef MPI_OFF
       call MPI_Sendrecv(                      &
            xx_send_recv(1:ndofn), ndofn4,     &
            PAR_INTEGER, dom_i4, 0_4,          &
            xx_recv(1:ndofn), ndofn4,          &
            PAR_INTEGER, dom_i4, 0_4,          &
            PAR_COMM_TO_USE, status, istat4    )
#endif       
       !
       ! Operate on array 
       !
       if(      trim(what) == 'MAX' ) then
          !
          ! MAX
          !
          do ii = 1,ndofn
             xx_send_recv(ii) = max(xx_send_recv(ii),xx_recv(ii))
          end do
       else if( trim(what) == 'MIN' ) then
          !
          ! MIN
          ! 
          do ii = 1,ndofn
             xx_send_recv(ii) = min(xx_send_recv(ii),xx_recv(ii))
          end do
       else if( trim(what) == 'SUM' ) then
          !
          ! SUM
          !
          do ii = 1,ndofn
             xx_send_recv(ii) = xx_send_recv(ii) + xx_recv(ii)
          end do
       else if( trim(what) == 'MIN RANK OR NEGATIVE' ) then 
          !
          ! At the end, only one partition will have the positive sign
          ! if the value is positive
          !
          if( my_rank4 > dom_i ) then
             do ii = 1,ndofn
                if( xx_send_recv(ii) > 0 .and. xx_recv(ii) > 0 ) then
                   xx_send_recv(ii) = -abs(xx_send_recv(ii)) 
                end if
             end do
          end if
       end if

    end do
    !
    ! Deallocate
    !
    deallocate(xx_recv)

  end subroutine PAR_POINT_TO_POINT_ARRAY_OPERATION_IP

  !----------------------------------------------------------------------
  !
  ! Bridges to PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP
  !
  !----------------------------------------------------------------------

  subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_00(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_00

  subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_0(n,xx,what,where,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: n
    real(rp),                       intent(inout) :: xx(n,*)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = n
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_0

  subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_1(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = 1
       if( size(xx,1) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_1

  subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_2(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    ndofn = size(xx,1)
    if( size(xx,2) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
    call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
    call PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)

  end subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_2

  subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_3(xx,what,where,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:,:)
    character(*),                   intent(in)    :: what
    character(*),                   intent(in)    :: where
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE
    type(comm_data_par),               pointer    :: commu

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)*size(xx,2)
       if( size(xx,3) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       call PAR_DEFINE_COMMUNICATOR(where,PAR_COMM_TO_USE,commu)
       call PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_3

  subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_2b(xx,what,commu,wsynch,dom_k)
    implicit none
    real(rp),             pointer,  intent(inout) :: xx(:,:)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ndofn
    integer(4)                                    :: PAR_COMM_TO_USE

    if( INOTSLAVE ) return
    if( associated(xx) ) then
       ndofn = size(xx,1)
       if( size(xx,2) /= nelem_2 ) call runend('PAR_COMMUNICATIONS: WRONG SIZE')
       PAR_COMM_TO_USE = commu % PAR_COMM_WORLD
       call PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    end if

  end subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP_2b

  !----------------------------------------------------------------------
  !
  ! PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP: NODE ASSEMBLY FOR INTEGERS
  !
  !----------------------------------------------------------------------

  subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP(ndofn,xx,what,commu,PAR_COMM_TO_USE,wsynch,dom_k)
    implicit none
    integer(ip),                    intent(in)    :: ndofn
    real(rp),                       intent(inout) :: xx(ndofn,*)
    character(*),                   intent(in)    :: what
    type(comm_data_par),            intent(in)    :: commu
    integer(4),                     intent(in)    :: PAR_COMM_TO_USE
    character(*),         optional, intent(in)    :: wsynch
    integer(ip),          optional, intent(in)    :: dom_k
    integer(ip)                                   :: ii,jj,dom_i
    integer(ip)                                   :: nsize_send,nsize_recv
    integer(ip)                                   :: ipoin,ini_send,ini_recv,kk,idofn
    integer(4)                                    :: nsize_send4,nsize_recv4
    integer(4)                                    :: istat4,count4
    integer(4)                                    :: dom_i4
    logical(lg)                                   :: asynch
    integer(ip),                       save       :: ipass = 0
    integer(4),                        pointer    :: status4(:,:)
    integer(ip)                                   :: dom_j

#ifndef MPI_OFF
    if( IPARALL ) then
       !
       ! Passes
       !
       ipass = ipass + 1 
       if( present(dom_k) ) then
          dom_j = dom_k
       else
          dom_j = 0
       end if
       !
       ! Synchronous or asynchronous
       !
       if( present(wsynch) ) then
          if( trim(wsynch) == 'SYNCHRONOUS' .or. trim(wsynch) == 'BLOCKING' ) then
             asynch = .false.
          else if( trim(wsynch) == 'ASYNCHRONOUS' .or. trim(wsynch) == 'NON BLOCKING' ) then
             asynch = .true.
          else
             call runend('PAR_ELEMENT_ASSMEMBLY: UNKNOWN COMMUNICATION TYPE')
          end if
       else
          asynch = .false.
       end if

       if( ISLAVE ) then

          if( ipass == 1 ) then
             !
             ! Allocate memory
             !
             if( commu % ghost_send_elem_dim == -1 .or. commu % ghost_recv_elem_dim == -1 ) &
                  call runend('FRINGE NODE EXCHANGE NOT COMPUTED')
             if( asynch ) allocate(ireq4(commu % nneig*2))
             allocate(tmp_rsend(commu % ghost_recv_elem_dim * ndofn))
             allocate(tmp_rrecv(commu % ghost_send_elem_dim * ndofn))
             !
             ! Save in temp_send
             !
             kk = 0
             do jj = 1,commu % ghost_recv_elem_dim
                ipoin = commu % ghost_recv_elem_perm(jj)
                do idofn = 1,ndofn
                   kk = kk + 1
                   tmp_rsend(kk) = xx(idofn,ipoin)
                end do
             end do
             !
             ! Send    temp_send
             ! Receive temp_recv     
             !     
             kk = 0
             istat4 = 0_4 
             do ii = 1,commu % nneig

                dom_i  = commu % neights(ii)  
                dom_i4 = int(dom_i,4)

                if( dom_j == 0 .or. dom_j == dom_i ) then

                   dom_i      = commd % neights(ii)
                   ini_send   = ndofn * ( commd % ghost_recv_elem_size(ii)   -1 ) + 1
                   nsize_send = ndofn * ( commd % ghost_recv_elem_size(ii+1) -1 ) + 1 - ini_send 
                   ini_recv   = ndofn * ( commd % ghost_send_elem_size(ii)   -1 ) + 1  
                   nsize_recv = ndofn * ( commd % ghost_send_elem_size(ii+1) -1 ) + 1 - ini_recv 

                   nsize_send4 = int(nsize_send,4)
                   nsize_recv4 = int(nsize_recv,4)

                   if( asynch ) then
                      kk = kk + 1
                      call MPI_Isend(&
                           tmp_rsend(ini_send:ini_send+nsize_send-1), nsize_send4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4,                     &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                      kk = kk + 1
                      call MPI_Irecv(&
                           tmp_rrecv(ini_recv:ini_recv+nsize_recv-1), nsize_recv4, &
                           MPI_DOUBLE_PRECISION,  dom_i4, 0_4,                     &
                           PAR_COMM_TO_USE, ireq4(kk), istat4                      )
                   else
                      if( nsize_recv /= 0 .and. nsize_send == 0 ) then
                         call MPI_Recv(                          &
                              tmp_rrecv(ini_recv:), nsize_recv4, &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                              PAR_COMM_TO_USE, status, istat4    )
                      else if( nsize_recv == 0 .and. nsize_send /= 0 ) then
                         call MPI_Send(                          &
                              tmp_rsend(ini_send:), nsize_send4, &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4, &
                              PAR_COMM_TO_USE, istat4            )                       
                      else if( nsize_recv /= 0 .and. nsize_send /= 0 ) then
                         call MPI_Sendrecv(                         &
                              tmp_rsend(ini_send:), nsize_send4,    &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4,    &
                              tmp_rrecv(ini_recv:), nsize_recv4,    &
                              MPI_DOUBLE_PRECISION, dom_i4, 0_4,    &
                              PAR_COMM_TO_USE, status, istat4       )
                      end if
                   end if
                   if( istat4 /= 0_4 ) call runend('PAR_GHOST_ELEMENT_EXCHANGE_RP: MPI ERROR')

                end if

             end do

          end if
          !
          ! sum,max,min on temp_recv 
          !     
          if( asynch .and. ipass == 2 ) then
             count4 = 2*int(commu % nneig,4)         
             allocate( status4(MPI_STATUS_SIZE,2*commu % nneig) )
             CALL MPI_WAITALL(count4,ireq4,status4,istat4)
             if( istat4 /= 0 ) call runend('WRONG SEND/RECEIVE')
             deallocate( status4 ) 
             deallocate(ireq4)
          end if

          if( ( asynch .and. ipass == 2 ) .or. ( .not. asynch .and. ipass == 1 ) ) then

             if( trim(what) == 'SUM' .or. trim(what) == 'ASSEMBLY' ) then 
                !
                ! SUM
                !
                kk = 0
                do jj = 1,commu % ghost_send_elem_dim
                   ipoin = commu % ghost_send_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = xx(idofn,ipoin) + tmp_rrecv(kk)
                   end do
                end do

             else if( trim(what) == 'MAX' .or. trim(what) == 'MAXIMUM' ) then 
                !
                ! MAX
                !
                kk = 0
                do jj = 1,commu % ghost_send_elem_dim
                   ipoin = commu % ghost_send_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = max(xx(idofn,ipoin),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'MIN' .or. trim(what) == 'MINIMUM' ) then 
                !
                ! MIN
                !
                kk = 0
                do jj = 1,commu % ghost_send_elem_dim
                   ipoin = commu % ghost_send_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = min(xx(idofn,ipoin),tmp_rrecv(kk))
                   end do
                end do

             else if( trim(what) == 'REPLACE' .or. trim(what) == 'SUBSTITUTE' ) then 
                !
                ! Replace value on my fringe nodes according to what I have received
                !
                kk = 0
                do jj = 1,commu % ghost_send_elem_dim
                   ipoin = commu % ghost_send_elem_perm(jj)
                   do idofn = 1,ndofn
                      kk = kk + 1
                      xx(idofn,ipoin) = tmp_rrecv(kk)
                   end do
                end do

             else
                call runend('UNKNOWN ORDER')
             end if

             ipass = 0
             deallocate(tmp_rrecv)
             deallocate(tmp_rsend)

          end if

       end if

    end if
#endif          

  end subroutine PAR_FROM_GHOST_ELEMENT_EXCHANGE_RP



end module mod_communications
!> @}

