!------------------------------------------------------------------------
!> @addtogroup ParallToolBox
!> @{
!> @name    Parallelization toolbox
!> @file    mod_parall.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!> @brief   "The easiest way of making software scalable is to make it sequentially inefficient."
!>          - Bill Gropp, 1999
!> @details ToolBox and definitions for parall. 
!> 
!>          The variables
!>          -------------
!>
!>          \verbatim
!>
!>          PAR_COMM_UNIVERSE........................................ Communicator of the universe  (Always MPI_COMM_WORLD)
!>          PAR_COMM_MY_CODE ........................................ Communicator of current code
!>          PAR_COMM_WORLD .......................................... Alya world communicator (MPI_COMM_WORLD or application split of MPI_COMM_WORLD)
!>          PAR_WORLD_SIZE .......................................... World size  
!>          PAR_MY_UNIVERSE_RANK..................................... My rank in the universe
!>          PAR_MY_WORLD_RANK ....................................... My rank in the world
!>          PAR_UNIVERSE_SIZE........................................ Size of the universe 
!>          PAR_CODE_SIZE ........................................... Size of my code communicator
!>          PAR_MY_CODE_RANK ........................................ My rank in my code
!>          PAR_INTEGER ............................................. Length of integer for MPI (ip)
!>
!>          I_AM_IN_COLOR(ICOLO) .................................... if I have color ICOLO (TRUE/FALSE)
!>          PAR_COMM_COLOR(0:MCOLO,0:MCOLO) ......................... Intercolor communicator
!>          PAR_COMM_COLOR_ARRAY(0:MCOLO) ........................... Color communication arrays
!>          PAR_CPU_TO_COLOR(0:PAR_WORLD_SIZE) % L(:) ............... List of colors for each world partition
!>          PAR_COLOR_TO_CPU(0:MCOLO) % L(:) ........................ List of world partitions for each color
!>          PAR_COMM_COLOR_PERM(0:MCOLO,0:MCOLO,0:PAR_WORLD_SIZE) ... Ranks for each communicator
!>          PAR_COMM_WORLD_TO_CODE_PERM(2,0:PAR_WORLD_SIZE) ......... Rank permutation from world to code
!>
!>          MCOLO ................................................... Nb of colors (codes,zones,subds) in the world
!>          MCODE ................................................... Nb of codes in the world
!>          MZONE ................................................... Nb of zones in the world
!>          MSUBD ................................................... Nb of subds in the world
!>          ncolo ................................................... Nb of colors in my code
!>          MAPPS ................................................... Nb of applications in the universe (MPI_COMM_WORLD)
!>
!>          \endverbatim
!>
!>          The functions
!>          -------------
!>
!>          \verbatim
!>  
!>          PAR_WORLD_RANK_OF_A_CODE_NEIGHBOR ....................... World rank of a code neighbor 
!>          PAR_WORLD_MASTER_RANK_OF_A_CODE ......................... Rank of the master of a code
!>          PAR_COLOR_COUPLING_RANK_TO_WORLD ........................ My rank in the world given my rank in a coupling
!>          PAR_CODE_ZONE_SUBD_TO_COLOR ............................. Mapping (code,zone,subd) => color
!>          PAR_COLOR_TO_CODE ....................................... Mapping color => code
!>          PAR_COLOR_TO_ZONE ....................................... Mapping color => zone
!>          PAR_COLOR_TO_SUBD ....................................... Mapping color => subd
!>          PAR_COLOR_TO_CODE_ZONE_SUBD(3) .......................... Mapping color => (code,zone,subd)
!>          PAR_PART_IN_COLOR ....................................... Check if a partition (rank in world) is in color
!>          PAR_THIS_NODE_IS_MINE ................................... If a node is interior or own boundary
!>          PAR_INITIALIZE_COMMUNICATION_ARRAY ...................... Initialize/Nullify a communication array
!>          PAR_COPY_COMMUNICATION_ARRAY ............................ Copy a communication array to another
!>          PAR_NODE_NUMBER_OF_PARTITIONS ........................... Number of partitions sharing this node
!>          PAR_GLOBAL_TO_LOCAL_NODE ................................ Local to global numbering of a ndoe
!>          par_omp_coloring ........................................ Color the elements for openmp
!>
!>          \endverbatim
!>
!> @{
!------------------------------------------------------------------------

module mod_parall 
  use def_kintyp,         only : ip,rp,lg,i1p,comm_data_par
  use def_domain,         only : npoin,nelem,nboun
  use def_master,         only : ISEQUEN,ISLAVE,IMASTER,INOTMASTER
  use def_master,         only : npoi1,npoi2,npoi3,lninv_loc
  use def_master,         only : intost,gisca,igene,lun_outpu
  use def_master,         only : ioutp
  use mod_memory,         only : memory_copy
  use mod_memory,         only : memory_alloca
  use def_domain,         only : mesh_type
  use mod_graphs,         only : graphs_eleele
  use mod_graphs,         only : graphs_dealep
  use mod_graphs,         only : graphs_coloring
  use mod_graphs,         only : graphs_coloring_greedy
  use mod_graphs,         only : graphs_deallocate
  use mod_postpr,         only : postpr_right_now
  implicit none 
#ifndef MPI_OFF
  include  'mpif.h'
  integer(4) :: status(MPI_STATUS_SIZE)
#endif
  integer(ip)                   :: PAR_COMM_CURRENT                  ! Current communicator
  integer(ip)                   :: PAR_COMM_MY_CODE                  ! Communicator of current code (defined at beginning)
  integer(4)                    :: PAR_COMM_MY_CODE4                 ! Communicator of current code (defined at beginning)
  integer(ip)                   :: PAR_COMM_UNIVERSE                 ! Universe communicator
  integer(ip)                   :: PAR_UNIVERSE_SIZE                 ! Size of the universe communicator
  integer(ip)                   :: PAR_MY_UNIVERSE_RANK              ! My rank in the universe
  integer(ip)                   :: PAR_COMM_WORLD                    ! Alya World communicator
  integer(ip)                   :: PAR_WORLD_SIZE                    ! Size of the world communicator
  integer(ip)                   :: PAR_MY_WORLD_RANK                 ! My communicator in the world
  integer(ip)                   :: PAR_CODE_SIZE                     ! Size of the code communicator
  integer(ip)                   :: PAR_MY_CODE_RANK                  ! My communicator in the world
  integer(ip)                   :: PAR_INTEGER                       ! Integer types for METIS
  integer(ip),         pointer  :: PAR_COMM_COLOR(:,:)               ! Intercolor MPI communicator
  integer(ip),         pointer  :: PAR_COMM_COLOR_PERM(:,:,:)        ! Communicator permutation array
  integer(ip),         pointer  :: PAR_COMM_WORLD_TO_CODE_PERM(:,:)  ! Permutation world to code and rank
  logical(lg),         pointer  :: I_AM_IN_COLOR(:)                  ! If I am in color

  type(comm_data_par), pointer  :: PAR_COMM_COLOR_ARRAY(:)           ! Intercolor communicator
  type(comm_data_par), pointer  :: PAR_COMM_MY_CODE_ARRAY(:)         ! Intercolor communicator
  type(comm_data_par), pointer  :: PAR_COMM_ZONE_ARRAY(:)            ! Interzone communicator
  type(comm_data_par), pointer  :: commd                             ! Generic communication array
  !
  ! Color structure
  !
  type(i1p),           pointer  :: PAR_CPU_TO_COLOR(:)               ! Given a CPU, gives the colors and associated rank
  type(i1p),           pointer  :: PAR_COLOR_TO_CPU(:)               ! Given a color, gives the CPUs
  type(i1p),           pointer  :: PAR_COLOR_BIN(:)                  ! Bin structure for colors
  !
  ! Current CODE/ZONE/SUBDOMAIN
  !
  integer(ip)                   :: mcolo                             ! Maximum number of colors
  integer(ip)                   :: mcode                             ! Maximum number of codes (over all processes)
  integer(ip)                   :: mzone                             ! Maximum number of zones (over all processes)
  integer(ip)                   :: msubd                             ! Maximum number of subds (over all processes) 
  integer(ip)                   :: ncolo                             ! Number of colors of my code
  integer(ip)                   :: mapps                             ! Number of applications in the universe (MPI_COMM_WORLD)
  !
  ! Bin structure to perform subdomain geometrical queries
  ! and subdomain bounding box
  !
  integer(ip)                   :: par_bin_boxes(3)                  ! # boxes in each direction
  real(rp)                      :: par_bin_comin(3)                  ! Minimum box coordinates
  real(rp)                      :: par_bin_comax(3)                  ! Maximum box coordinates
  integer(ip),         pointer  :: par_bin_size(:,:,:)               ! # partitions per box
  type(i1p),           pointer  :: par_bin_part(:,:,:)               ! Bin structure of world partition
  real(rp),            pointer  :: par_part_comin(:,:)               ! Subdomain minimum coordinates
  real(rp),            pointer  :: par_part_comax(:,:)               ! Subdomain maximum coordinates
  !
  ! Others
  !
  integer(8)                    :: par_memor(2)                      ! Memory counter
  integer(ip)                   :: color_target                      ! Target color when using coupling
  integer(ip)                   :: color_source                      ! Source color when using coupling
  integer(ip),        parameter :: lun_outpu_par = 5502              ! Output
  integer(ip),        parameter :: lun_parti_msh = 5507              ! Partition mesh file
  integer(ip),        parameter :: lun_parti_res = 5508              ! Partition result file
  integer(4)                    :: PARMETIS_COMM, PARMETIS_INTERCOMM ! parmetis communicators
  !
  ! OpenMP
  !
  integer(ip)                   :: par_omp_num_blocks                ! Size of blocks to compute chunks n/par_omp_num_blocks  
  integer(ip)                   :: par_omp_granularity               ! Granularity to compute par_omp_num_blocks=par_omp_num_threads*par_omp_granularity
  integer(ip)                   :: par_omp_nelem_chunk               ! Element loop chunk size
  integer(ip)                   :: par_omp_npoin_chunk               ! Node loop chunk size
  integer(ip)                   :: par_omp_nboun_chunk               ! Boundary loop chunk size
  integer(ip)                   :: par_omp_num_threads               ! Number of openmp threads 
  integer(ip)                   :: par_omp_num_colors                ! Number of colors   
  integer(ip),        pointer   :: par_omp_list_colors(:)            ! Element colors
  integer(ip),        pointer   :: par_omp_ia_colors(:)              ! Linked list IA for colors
  integer(ip),        pointer   :: par_omp_ja_colors(:)              ! Linked list JA for colors
  integer(ip)                   :: par_omp_coloring_alg              ! Coloring algorithm
  !
  ! Interfaces
  !
  interface PAR_INITIALIZE_COMMUNICATION_ARRAY
     module procedure PAR_INITIALIZE_COMMUNICATION_ARRAY_s, &
          &           PAR_INITIALIZE_COMMUNICATION_ARRAY_1
  end interface PAR_INITIALIZE_COMMUNICATION_ARRAY

contains

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/03/2014
  !> @brief   Give the world rank of a code neighbor
  !> @details Given a neighbor INEIG, returns its rank in the MPI_COMM_WORLD
  !
  !----------------------------------------------------------------------

  function par_world_rank_of_a_code_neighbor(ipart,icode)
    integer(ip), intent(in) :: ipart
    integer(ip), intent(in) :: icode
    integer(ip)             :: par_world_rank_of_a_code_neighbor
    integer(ip)             :: ipart_world,ipart_code,jcode

    ipart_world = 0
    par_world_rank_of_a_code_neighbor = -1
    do while( ipart_world <= PAR_WORLD_SIZE-1 )
       jcode      = PAR_COMM_WORLD_TO_CODE_PERM(1,ipart_world)
       ipart_code = PAR_COMM_WORLD_TO_CODE_PERM(2,ipart_world)
       if( ipart_code == ipart .and. icode == jcode ) then
          par_world_rank_of_a_code_neighbor = ipart_world
          ipart_world = PAR_WORLD_SIZE-1
       end if
       ipart_world = ipart_world + 1
    end do
    if( par_world_rank_of_a_code_neighbor == -1 ) &
         call runend('par_world_rank_of_a_code_neighbor: WE ARE IN TROUBLE')

  end function par_world_rank_of_a_code_neighbor

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/03/2014
  !> @brief   Give the rank of the master of a code
  !> @details Given a code ICODE, returns the rank of the master of the
  !>          code in the MPI_COMM_WORLD
  !
  !----------------------------------------------------------------------

  function par_world_master_rank_of_a_code(icode)
    integer(ip), intent(in) :: icode
    integer(ip)             :: par_world_master_rank_of_a_code
    integer(ip)             :: icolo,isize

    icolo = par_code_zone_subd_to_color(icode,0_ip,0_ip)
    isize = 0
    do while( isize <= PAR_WORLD_SIZE-1 )
       par_world_master_rank_of_a_code = PAR_COMM_COLOR_PERM(icolo,icolo,isize)
       if( par_world_master_rank_of_a_code == 0 ) then
          isize = PAR_WORLD_SIZE
       end if
       isize = isize + 1
    end do

  end function par_world_master_rank_of_a_code

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/03/2014
  !> @brief   Give my rank in the world given my rank in a coupling
  !> @details Give my rank in the world given my rank in a coupling
  !
  !----------------------------------------------------------------------

  function par_color_coupling_rank_to_world(my_rank,icolo,jcolo)
    integer(ip), intent(in) :: my_rank
    integer(ip), intent(in) :: icolo
    integer(ip), intent(in) :: jcolo
    integer(ip)             :: par_color_coupling_rank_to_world
    integer(ip)             :: isize

    isize = 0
    do while( isize <= PAR_WORLD_SIZE-1 )
       par_color_coupling_rank_to_world = PAR_COMM_COLOR_PERM(icolo,jcolo,isize)
       if( par_color_coupling_rank_to_world == my_rank ) then
          isize = PAR_WORLD_SIZE
       end if
       isize = isize + 1
    end do

  end function par_color_coupling_rank_to_world

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Mapping (code,zone,subd) => color
  !> @details Given a code, zone and subd, compute the corresponding
  !>          color, where:
  !>          code: 0 to mcode
  !>          zone: 0 to mzone
  !>          subd: 0 to msubd
  !>          3D <=> 1D mapping:
  !>          i = x + Lx * (y+Ly*z)
  !>          x = modulo(i,Lx)
  !>          y = modulo(i/Lx,Ly)
  !>          z = i/(Lx*Ly)
  !
  !----------------------------------------------------------------------

  function par_code_zone_subd_to_color(icode,izone,isubd)
    integer(ip), intent(in) :: icode,izone,isubd
    integer(ip)             :: par_code_zone_subd_to_color

    par_code_zone_subd_to_color = isubd+(msubd+1)*(izone+icode*(mzone+1))

  end function par_code_zone_subd_to_color

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Mapping color => code
  !> @details Given a color, returns the code number
  !
  !----------------------------------------------------------------------

  function par_color_to_code(icolo)
    integer(ip), intent(in) :: icolo
    integer(ip)             :: par_color_to_code

    par_color_to_code = icolo / ((msubd+1)*(mzone+1))

  end function par_color_to_code

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Mapping color => code
  !> @details Given a color, returns the zone number
  !
  !----------------------------------------------------------------------

  function par_color_to_zone(icolo)
    integer(ip), intent(in) :: icolo
    integer(ip)             :: par_color_to_zone

    par_color_to_zone = modulo(icolo/(msubd+1),mzone+1)

  end function par_color_to_zone

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Mapping color => code
  !> @details Given a color, returns the subdomain number
  !
  !----------------------------------------------------------------------

  function par_color_to_subd(icolo)
    integer(ip), intent(in) :: icolo
    integer(ip)             :: par_color_to_subd

    par_color_to_subd = modulo(icolo,msubd+1)

  end function par_color_to_subd

  function par_color_to_code_zone_subd(icolo)
    implicit none    
    integer(ip), intent(in) :: icolo
    integer(ip)             :: par_color_to_code_zone_subd(3)

    par_color_to_code_zone_subd(1) = icolo / ((msubd+1)*(mzone+1))
    par_color_to_code_zone_subd(2) = modulo(icolo/(msubd+1),mzone+1)
    par_color_to_code_zone_subd(3) = modulo(icolo,msubd+1)

  end function par_color_to_code_zone_subd

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Check if a partition IPART is in color ICOLO
  !> @details IPART should be given in the world
  !>                                                       ICOLO
  !>                                           +---+---+---+---+---+---+
  !>          PAR_CPU_TO_COLOR(IPART) % L(:) = | 1 | 4 | 7 | 8 | 9 |10 |
  !>                                           +---+---+---+---+---+---+
  !>                                           KCOLO =>
  !>
  !----------------------------------------------------------------------

  function par_part_in_color(ipart,icolo)
    integer(ip), intent(in) :: ipart
    integer(ip), intent(in) :: icolo
    logical(lg)             :: par_part_in_color
    integer(ip)             :: kcolo,jcolo,ksize

    par_part_in_color = .false. 
    if( associated(PAR_CPU_TO_COLOR(ipart) % l) ) then
       jcolo = 1
       kcolo = PAR_CPU_TO_COLOR(ipart) % l(jcolo)
       ksize = size(PAR_CPU_TO_COLOR(ipart) % l)
       do while( jcolo <= ksize .and. kcolo < icolo )
          kcolo = PAR_CPU_TO_COLOR(ipart) % l(jcolo)
          jcolo = jcolo + 1
       end do
       if( kcolo == icolo ) par_part_in_color = .true.
    end if

  end function par_part_in_color

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    06/03/2014
  !> @brief   Returns if a node is interior or inside my own boundary
  !> @details Returns if a node is interior or inside my own boundary
  !>
  !----------------------------------------------------------------------

  function PAR_THIS_NODE_IS_MINE(ipoin,where)
    integer(ip),  intent(in)           :: ipoin
    character(*), intent(in), optional :: where
    logical(lg)                        :: PAR_THIS_NODE_IS_MINE

    if( IMASTER ) then
       PAR_THIS_NODE_IS_MINE = .false.
    else
       if( .not. present(where) ) then
          if( ipoin <= npoi1 .or. ( ipoin >= npoi2 .and. ipoin <= npoi3 ) ) then
             PAR_THIS_NODE_IS_MINE = .true.
          else
             PAR_THIS_NODE_IS_MINE = .false.
          end if
       else
          call runend('PAR_THIS_NODE_IS_MINE: NOT CODED')
       end if
    end if

  end function PAR_THIS_NODE_IS_MINE

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    06/03/2014
  !> @brief   Returns the local node number of a global number
  !> @details Returns the local node number of a global number
  !>          and zero if I do not own the node
  !
  !----------------------------------------------------------------------

  function PAR_GLOBAL_TO_LOCAL_NODE(ipoin_global)
    integer(ip),  intent(in) :: ipoin_global
    integer(ip)              :: ipoin
    integer(ip)              :: PAR_GLOBAL_TO_LOCAL_NODE

    PAR_GLOBAL_TO_LOCAL_NODE = 0

    if( ISEQUEN ) then
       PAR_GLOBAL_TO_LOCAL_NODE = ipoin_global
    else if( ISLAVE ) then
       do ipoin = 1,npoin
          if( lninv_loc(ipoin) == ipoin_global ) then
             PAR_GLOBAL_TO_LOCAL_NODE = ipoin
             return
          end if
       end do
    end if

  end function PAR_GLOBAL_TO_LOCAL_NODE

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    06/03/2014
  !> @brief   Number of partitions for nodes
  !> @details Gives the number of neighbors for a given node or all 
  !>          the nodes. This includes my partition. In next example,
  !>          the middle node (o) has 4 partitions.
  !>
  !>          x----------x----------x
  !>          |          |          |
  !>          |   CPU1   |   CPU2   |
  !>          |          |          |
  !>          x----------o----------x
  !>          |          |          |
  !>          |   CPU3   |   CPU4   |
  !>          |          |          |
  !>          x----------x----------x
  !
  !----------------------------------------------------------------------

  subroutine PAR_NODE_NUMBER_OF_PARTITIONS(npoin,lneig,ipoin)
    integer(ip), intent(in)           :: npoin
    integer(ip), intent(out)          :: lneig(*)
    integer(ip), intent(in), optional :: ipoin
    integer(ip)                       :: kpoin
    integer(ip)                       :: ineig,jj

    if( present(ipoin) ) then
       lneig(1) = 1
       do jj = 1,commd % bound_dim
          kpoin = commd % bound_perm(jj)
          if( kpoin == ipoin ) lneig(ipoin) = lneig(ipoin) + 1
       end do
    else       
       do kpoin = 1,npoin
          lneig(kpoin) = 1
       end do
       do jj = 1,commd % bound_dim
          kpoin = commd % bound_perm(jj)
          lneig(kpoin) = lneig(kpoin) + 1
       end do
    end if

  end subroutine PAR_NODE_NUMBER_OF_PARTITIONS

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    06/03/2014
  !> @brief   Initialize communication array
  !> @details Initialize communication array 
  !
  !----------------------------------------------------------------------

  subroutine PAR_INITIALIZE_COMMUNICATION_ARRAY_s(COMM)
    type(comm_data_par), intent(out) :: COMM     !< Input communicator

    COMM % nneig               =  0      
    COMM % npoi1               =  0    
    COMM % npoi2               =  0         
    COMM % npoi3               = -1         
    COMM % npoi4               =  0       
    COMM % bound_dim           =  0
    COMM % lsend_dim           =  0
    COMM % lrecv_dim           =  0
    COMM % lscat_dim           =  0
    COMM % matrix_nzdom        =  0
    COMM % bface_dim           =  0
    COMM % ghost_send_elem_dim = -1
    COMM % ghost_recv_elem_dim = -1
    COMM % ghost_send_node_dim = -1
    COMM % ghost_recv_node_dim = -1
    COMM % ghost_send_boun_dim = -1
    COMM % ghost_recv_boun_dim = -1
    COMM % PAR_COMM_WORLD      = -1
    nullify(COMM % neights)
    nullify(COMM % bound_size)
    nullify(COMM % bound_perm)
    nullify(COMM % bound_adja)
    nullify(COMM % bound_scal)
    nullify(COMM % lsend_size)
    nullify(COMM % lrecv_size)
    nullify(COMM % lsend_perm)
    nullify(COMM % lrecv_perm)
    nullify(COMM % lscat_perm)
    nullify(COMM % matrix_ia)
    nullify(COMM % matrix_ja)
    nullify(COMM % matrix_aa)
    nullify(COMM % bface_size)
    nullify(COMM % bface_perm)
    nullify(COMM % ghost_send_elem_size)
    nullify(COMM % ghost_send_elem_perm)
    nullify(COMM % ghost_recv_elem_size)
    nullify(COMM % ghost_recv_elem_perm)
    nullify(COMM % ghost_send_node_size)
    nullify(COMM % ghost_send_node_perm)
    nullify(COMM % ghost_recv_node_size)
    nullify(COMM % ghost_recv_node_perm)
    nullify(COMM % ghost_send_boun_size)
    nullify(COMM % ghost_send_boun_perm)
    nullify(COMM % ghost_recv_boun_size)
    nullify(COMM % ghost_recv_boun_perm)

  end subroutine PAR_INITIALIZE_COMMUNICATION_ARRAY_s

  subroutine PAR_INITIALIZE_COMMUNICATION_ARRAY_1(COMM)
    type(comm_data_par), pointer, intent(out) :: COMM(:)     !< Input communicator
    integer(ip)                               :: icomm

    do icomm = lbound(COMM,1),ubound(COMM,1) 
       COMM(icomm) % nneig               =  0      
       COMM(icomm) % npoi1               =  0    
       COMM(icomm) % npoi2               =  0         
       COMM(icomm) % npoi3               = -1         
       COMM(icomm) % npoi4               =  0       
       COMM(icomm) % bound_dim           =  0
       COMM(icomm) % lsend_dim           =  0
       COMM(icomm) % lrecv_dim           =  0
       COMM(icomm) % lscat_dim           =  0
       COMM(icomm) % matrix_nzdom        =  0
       COMM(icomm) % bface_dim           =  0
       COMM(icomm) % ghost_send_elem_dim = -1
       COMM(icomm) % ghost_recv_elem_dim = -1
       COMM(icomm) % ghost_send_node_dim = -1
       COMM(icomm) % ghost_recv_node_dim = -1
       COMM(icomm) % ghost_send_boun_dim = -1
       COMM(icomm) % ghost_recv_boun_dim = -1
       COMM(icomm) % PAR_COMM_WORLD      = -1
       nullify(COMM(icomm) % neights   )
       nullify(COMM(icomm) % bound_size)
       nullify(COMM(icomm) % bound_perm)
       nullify(COMM(icomm) % bound_adja)
       nullify(COMM(icomm) % bound_scal)
       nullify(COMM(icomm) % lsend_size)
       nullify(COMM(icomm) % lrecv_size)
       nullify(COMM(icomm) % lsend_perm)
       nullify(COMM(icomm) % lrecv_perm)
       nullify(COMM(icomm) % lscat_perm)
       nullify(COMM(icomm) % matrix_ia )
       nullify(COMM(icomm) % matrix_ja )
       nullify(COMM(icomm) % matrix_aa )
       nullify(COMM(icomm) % bface_size)
       nullify(COMM(icomm) % bface_perm)
       nullify(COMM(icomm) % ghost_send_elem_size)
       nullify(COMM(icomm) % ghost_send_elem_perm)
       nullify(COMM(icomm) % ghost_recv_elem_size)
       nullify(COMM(icomm) % ghost_recv_elem_perm)
       nullify(COMM(icomm) % ghost_send_node_size)
       nullify(COMM(icomm) % ghost_send_node_perm)
       nullify(COMM(icomm) % ghost_recv_node_size)
       nullify(COMM(icomm) % ghost_recv_node_perm)
       nullify(COMM(icomm) % ghost_send_boun_size)
       nullify(COMM(icomm) % ghost_send_boun_perm)
       nullify(COMM(icomm) % ghost_recv_boun_size)
       nullify(COMM(icomm) % ghost_recv_boun_perm)
    end do

  end subroutine PAR_INITIALIZE_COMMUNICATION_ARRAY_1

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    06/03/2014
  !> @brief   Copy a communication array
  !> @details Copy a communication array to another
  !
  !----------------------------------------------------------------------

  subroutine PAR_COPY_COMMUNICATION_ARRAY(COMM_IN,COMM_OUT)
    type(comm_data_par), intent(in)  :: COMM_IN(:)     !< Input communicator
    type(comm_data_par), intent(out) :: COMM_OUT(:)    !< Output communicator

    COMM_OUT(1) % nneig                 =  COMM_IN(1) % nneig         
    COMM_OUT(1) % npoi1                 =  COMM_IN(1) % npoi1         
    COMM_OUT(1) % npoi2                 =  COMM_IN(1) % npoi2         
    COMM_OUT(1) % npoi3                 =  COMM_IN(1) % npoi3         
    COMM_OUT(1) % npoi4                 =  COMM_IN(1) % npoi4         
    COMM_OUT(1) % bound_dim             =  COMM_IN(1) % bound_dim     
    COMM_OUT(1) % lsend_dim             =  COMM_IN(1) % lsend_dim     
    COMM_OUT(1) % lrecv_dim             =  COMM_IN(1) % lrecv_dim     
    COMM_OUT(1) % lscat_dim             =  COMM_IN(1) % lscat_dim     
    COMM_OUT(1) % matrix_nzdom          =  COMM_IN(1) % matrix_nzdom
    COMM_OUT(1) % bface_dim             =  COMM_IN(1) % bface_dim   
    COMM_OUT(1) % ghost_send_elem_dim   =  COMM_IN(1) % ghost_send_elem_dim
    COMM_OUT(1) % ghost_recv_elem_dim   =  COMM_IN(1) % ghost_recv_elem_dim
    COMM_OUT(1) % ghost_send_node_dim   =  COMM_IN(1) % ghost_send_node_dim
    COMM_OUT(1) % ghost_recv_node_dim   =  COMM_IN(1) % ghost_recv_node_dim
    COMM_OUT(1) % ghost_send_boun_dim   =  COMM_IN(1) % ghost_send_boun_dim
    COMM_OUT(1) % ghost_recv_boun_dim   =  COMM_IN(1) % ghost_recv_boun_dim

    COMM_OUT(1) % PAR_COMM_WORLD        =  COMM_IN(1) % PAR_COMM_WORLD
    COMM_OUT(1) % neights               => COMM_IN(1) % neights   
    COMM_OUT(1) % bound_size            => COMM_IN(1) % bound_size
    COMM_OUT(1) % bound_perm            => COMM_IN(1) % bound_perm
    COMM_OUT(1) % bound_adja            => COMM_IN(1) % bound_adja
    COMM_OUT(1) % bound_scal            => COMM_IN(1) % bound_scal
    COMM_OUT(1) % lsend_size            => COMM_IN(1) % lsend_size
    COMM_OUT(1) % lrecv_size            => COMM_IN(1) % lrecv_size
    COMM_OUT(1) % lsend_perm            => COMM_IN(1) % lsend_perm
    COMM_OUT(1) % lrecv_perm            => COMM_IN(1) % lrecv_perm
    COMM_OUT(1) % lscat_perm            => COMM_IN(1) % lrecv_perm

    COMM_OUT(1) % matrix_ia             => COMM_IN(1) % matrix_ia
    COMM_OUT(1) % matrix_ja             => COMM_IN(1) % matrix_ja 
    COMM_OUT(1) % matrix_aa             => COMM_IN(1) % matrix_aa 

    COMM_OUT(1) % bface_size            => COMM_IN(1) % bface_size
    COMM_OUT(1) % bface_perm            => COMM_IN(1) % bface_perm

    COMM_OUT(1) % ghost_send_elem_size  => COMM_IN(1) % ghost_send_elem_size
    COMM_OUT(1) % ghost_send_elem_perm  => COMM_IN(1) % ghost_send_elem_perm
    COMM_OUT(1) % ghost_recv_elem_size  => COMM_IN(1) % ghost_recv_elem_size
    COMM_OUT(1) % ghost_recv_elem_perm  => COMM_IN(1) % ghost_recv_elem_perm
    COMM_OUT(1) % ghost_send_node_size  => COMM_IN(1) % ghost_send_node_size
    COMM_OUT(1) % ghost_send_node_perm  => COMM_IN(1) % ghost_send_node_perm
    COMM_OUT(1) % ghost_recv_node_size  => COMM_IN(1) % ghost_recv_node_size
    COMM_OUT(1) % ghost_recv_node_perm  => COMM_IN(1) % ghost_recv_node_perm

    COMM_OUT(1) % ghost_send_boun_size  => COMM_IN(1) % ghost_send_boun_size
    COMM_OUT(1) % ghost_send_boun_perm  => COMM_IN(1) % ghost_send_boun_perm
    COMM_OUT(1) % ghost_recv_boun_size  => COMM_IN(1) % ghost_recv_boun_size
    COMM_OUT(1) % ghost_recv_boun_perm  => COMM_IN(1) % ghost_recv_boun_perm

  end subroutine PAR_COPY_COMMUNICATION_ARRAY

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    06/03/2014
  !> @brief   Create a sub communication array
  !> @details Given a communicaiton array structure, create a sub 
  !           communication structure for a subset of nodes
  !
  !----------------------------------------------------------------------

  subroutine PAR_SUB_COMMUNICATION_ARRAY(COMM_IN,COMM_OUT,mask)
    type(comm_data_par),          intent(in)  :: COMM_IN        !< Input communicator
    type(comm_data_par),          intent(out) :: COMM_OUT       !< Output communicator
    integer(ip),         pointer, intent(in)  :: mask(:)        !< mask (=1 to consider node)
    integer(ip),         pointer              :: bound_perm(:)
    integer(ip),         pointer              :: bound_size(:)
    integer(ip),         pointer              :: neights(:)
    integer(ip)                               :: nneig,bound_dim
    integer(ip)                               :: ineig,jj,jneig
    integer(ip)                               :: ipoin

    COMM_OUT % PAR_COMM_WORLD = COMM_IN % PAR_COMM_WORLD

    if( COMM_IN % bound_dim > 0 ) then

       allocate( bound_perm(COMM_IN % bound_dim) )
       allocate( bound_size(COMM_IN % nneig)     )
       allocate( neights   (COMM_IN % nneig)     )

       nneig     = 0
       bound_dim = 0

       do jj = 1,COMM_IN % bound_dim
          bound_perm(jj) = COMM_IN % bound_perm(jj)
       end do
       do ineig = 1,COMM_IN % nneig
          bound_size(ineig) = 0
       end do

       do ineig = 1,COMM_IN % nneig
          do jj = COMM_IN % bound_size(ineig),COMM_IN % bound_size(ineig+1)-1
             ipoin = COMM_IN % bound_perm(jj)
             if( mask(ipoin) > 0 ) then
                bound_size(ineig) = bound_size(ineig) + 1 
             else
                bound_perm(jj) = 0
             end if
          end do
          bound_dim = bound_dim + bound_size(ineig)

          if( bound_size(ineig) > 0 ) nneig = nneig + 1
       end do
       !
       ! Allocate interzone communication array
       !
       COMM_OUT % nneig     = nneig
       COMM_OUT % bound_dim = bound_dim
       COMM_OUT % npoi1     = COMM_IN % npoi1
       COMM_OUT % npoi2     = COMM_IN % npoi2
       COMM_OUT % npoi3     = COMM_IN % npoi3
       allocate( COMM_OUT % neights(nneig) )
       allocate( COMM_OUT % bound_perm(bound_dim) )
       allocate( COMM_OUT % bound_size(nneig+1) )
       !
       ! Permutation array BOUND_PERM(1:BOUND_DIM)
       !
       jneig = 0
       bound_dim = 0
       do ineig = 1,COMM_IN % nneig
          if( bound_size(ineig) > 0 ) then

             jneig = jneig + 1              
             COMM_OUT % neights(jneig) = COMM_IN % neights(ineig)  

             do jj = COMM_IN % bound_size(ineig),COMM_IN % bound_size(ineig+1)-1
                ipoin = bound_perm(jj)
                if( ipoin > 0 ) then
                   bound_dim = bound_dim + 1
                   COMM_OUT % bound_perm(bound_dim) = ipoin
                end if
             end do

          end if
       end do
       !
       ! Construct linked list BOUND_SIZE(1:NNEIG+1)
       !
       COMM_OUT % bound_size(1) = 1
       jneig = 0
       do ineig = 1,COMM_IN % nneig
          if( bound_size(ineig) > 0 ) then       
             jneig = jneig + 1
             COMM_OUT % bound_size(jneig+1) = &
                  COMM_OUT % bound_size(jneig) + bound_size(ineig)
          end if
       end do

    end if

  end subroutine PAR_SUB_COMMUNICATION_ARRAY

  function NODE_IN_NEIGHBOR(ipoin,ineig,commu)
    implicit none
    integer(ip),                  intent(in) :: ipoin  !< Node
    integer(ip),                  intent(in) :: ineig  !< Neighbor
    type(comm_data_par), pointer, intent(in) :: commu  !< Generic communication array
    logical(lg)                              :: NODE_IN_NEIGHBOR 
    integer(ip)                              :: ii

    NODE_IN_NEIGHBOR = .false.
    loop_nodes: do ii = commu % bound_size(ineig),commu % bound_size(ineig+1)-1    
       if( ipoin == commu % bound_perm(ii) )then
          NODE_IN_NEIGHBOR = .true.
          exit loop_nodes
       end if
    end do loop_nodes

  end function NODE_IN_NEIGHBOR

  !------------------------------------------------------------------------
  !> @addtogroup Parall
  !> @{
  !> @file    par_omp_num_blocksl.f90
  !> @author  Guillaume Houzeaux
  !> @date    27/10/2015
  !> @brief   Openm block number
  !> @details Number of blocks for openmp loops to define chunk size
  !>
  !>          OMP PARALLEL DO SCHEDULE (DYNAMIC,n/par_omp_num_blocks) &
  !>          OMP PRIVATE  (i) &                
  !>          OMP SHARED   (n)
  !>          do i = 1,n
  !>          ...
  !>          end do
  !>                                       n
  !>          Chunk size is:   -------------------------
  !>                           num_threads x granularity  (=num_blocks)
  !> @} 
  !------------------------------------------------------------------------

  subroutine par_omp_chunk_sizes(meshe)
    type(mesh_type), intent(in) :: meshe !< Mesh
    integer(ip)                 :: kelem
    integer(ip)                 :: icolo
    integer(ip)                 :: nelem_average
    integer(ip)                 :: nboun_average
    integer(ip)                 :: npoin_average

    if( par_omp_granularity < 1 ) then
       call runend('PAR_OMP_NUMBER_BLOCKS: WRONG GRANULARITY')
    else    
       par_omp_num_blocks  = max(1_ip,par_omp_num_threads) * par_omp_granularity
#ifndef NO_COLORING
       !
       ! With coloring: average number of elements
       !
       if( INOTMASTER ) then
          kelem = 0
          do icolo = 1,par_omp_num_colors   
             kelem = kelem + par_omp_ia_colors(icolo+1)-par_omp_ia_colors(icolo)
          end do
          kelem = kelem / par_omp_num_colors   
          par_omp_nelem_chunk = 1 + kelem / par_omp_num_blocks
       else
          par_omp_nelem_chunk = 1 + meshe % nelem / par_omp_num_blocks
       end if
#else
       !
       ! No coloring
       !
       par_omp_nelem_chunk = 1 + meshe % nelem / par_omp_num_blocks
#endif
       par_omp_nboun_chunk = 1 + meshe % nboun / par_omp_num_blocks
       par_omp_npoin_chunk = 1 + meshe % npoin / par_omp_num_blocks
    end if
 
    call outfor(-72_ip,lun_outpu,'')

  end subroutine par_omp_chunk_sizes

  !------------------------------------------------------------------------
  !> @addtogroup Parall
  !> @{
  !> @file    par_omp_coloring.f90
  !> @author  Guillaume Houzeaux
  !> @date    20/05/2014
  !> @brief   Coloring
  !> @details Color the elements and create a liked list for the colored
  !>          elements
  !> @} 
  !------------------------------------------------------------------------

  subroutine par_omp_coloring(meshe,mepoi,pelpo,lelpo)
    type(mesh_type),          intent(in)    :: meshe       !< Mesh
    integer(ip),              intent(inout) :: mepoi       !< Max nb of nodes per element
    integer(ip),     pointer, intent(inout) :: pelpo(:)    !< Element-node linked list
    integer(ip),     pointer, intent(inout) :: lelpo(:)    !< Element-node linked list
    integer(ip)                             :: nedge
    integer(ip)                             :: medge
    integer(ip)                             :: ielem
    integer(ip),     pointer                :: lelel(:)
    integer(ip),     pointer                :: pelel(:)

    if( INOTMASTER ) then
       if( par_omp_num_threads /= 0 ) then

          call livinf(0_ip,'COLOR ELEMENTS FOR OPENMP',0_ip)
          !
          ! Nullify pointers
          !
          nullify(lelel,pelel)
          !
          ! Element-element graph 
          !
          call graphs_eleele(&
               meshe % nelem,meshe % npoin,meshe % mnode,mepoi,&
               meshe % lnods,meshe % lnnod,pelpo,lelpo,nedge,  &
               medge,pelel,lelel)
          !
          ! Color elements
          !
          !write(*,*)'par_omp_coloring_alg:' , par_omp_coloring_alg
          if( par_omp_coloring_alg == 0 ) then
            call graphs_coloring_greedy(&
               meshe % nelem,pelel,lelel,par_omp_list_colors,&
               par_omp_num_colors,par_omp_ia_colors,par_omp_ja_colors)
          else            
            call graphs_coloring(&
               meshe % nelem,pelel,lelel,par_omp_list_colors,&
               par_omp_num_colors,par_omp_ia_colors,par_omp_ja_colors)
          end if
          !
          ! Element numbering (for postprocess)
          !
          !ielem = 0
          !do inode = 1,ncolo 
          !   ielem = ielem + ia_color(inode+1)-ia_color(inode)
          !   do kelem = ia_color(inode),ia_color(inode+1)-1
          !      lcolo(kelem) = ja_color(kelem)
          !   end do
          !end do
          !print*,'nelem=',ielem
          !call postpr_right_now('LCOLO','SCALA','NELEM',par_omp_list_colors)
          call graphs_deallocate(par_omp_list_colors)
          call graphs_deallocate(pelel)
          call graphs_deallocate(lelel)
          !
          ! Output
          !
          call livinf(0_ip,'NUMBER OF COLORS FOUND= '//trim(intost(par_omp_num_colors)),0_ip)
          igene =  par_omp_num_colors
          gisca => par_omp_ia_colors
          nullify(gisca)

       else 

          par_omp_num_colors = 1
          call memory_alloca(par_memor,'PAR_OMP_IA_COLORS','par_omp_coloring',par_omp_ia_colors,par_omp_num_colors+1_ip)
          call memory_alloca(par_memor,'PAR_OMP_JA_COLORS','par_omp_coloring',par_omp_ja_colors,meshe % nelem)
          par_omp_ia_colors(1) = 1
          par_omp_ia_colors(2) = meshe % nelem + 1
          do ielem = 1,meshe % nelem
             par_omp_ja_colors(ielem) = ielem
          end do

       end if

    end if

  end subroutine par_omp_coloring

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    06/03/2014
  !> @brief   Ghost geometry communicator
  !> @details Ghost geometry communicator
  !>
  !----------------------------------------------------------------------

  !subroutine par_ghost_communication_array(COMM)
  !  type(comm_data_par), intent(inout) :: COMM             !< Input communicator
  !  integer(ip)                        :: PAR_CURRENT_RANK
  !  integer(ip)                        :: PAR_CURRENT_SIZE
  !  integer(ip)                        :: dom_i,dom_j
  !  integer(ip)                        :: ghost_elem_dim_ineig,ii
  !  integer(ip),         pointer       :: permu(:)
  !  PAR_COMM_CURRENT = COMM % PAR_COMM_WORLD
  !  call PAR_COMM_RANK_AND_SIZE(PAR_COMM_CURRENT,PAR_CURRENT_RANK,PAR_CURRENT_SIZE)
  !end subroutine par_ghost_communication_array

end module mod_parall
!
!> @} 
!-----------------------------------------------------------------------
