!------------------------------------------------------------------------
!> @addtogroup ParallToolBox
!> @{
!> @name    Parallelization toolbox
!> @file    mod_parall.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!> @brief   ToolBox for parallel operations
!> @details ToolBox for parallel operations
!>
!> @{
!------------------------------------------------------------------------

module mod_operations
  use def_kintyp,         only : comm_data_par
  use def_kintyp,         only : ip,rp,lg
  use mod_communications, only : PAR_SUM
  implicit none 
  private

  public :: PAR_NORM2X

contains

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    06/03/2014
  !> @brief   Computes the 2-norm (Eucledian norm) of a vector XX
  !> @details Computes the 2-norm (Eucledian norm) of a vector XX
  !
  !----------------------------------------------------------------------

  subroutine PAR_NORM2X(xx,sumxx,COMM_IN)
    implicit none
    real(rp),                      intent(in)  :: xx(:)       !< Vector
    real(rp),                      intent(out) :: sumxx       !< Eucledian norm
    type(comm_data_par), optional, intent(in)  :: COMM_IN     !< Communication array 
    integer(ip)                                :: ii,jj,kk,ll

    sumxx = 0.0_rp

    if( .not. present(COMM_IN) ) then
       !
       ! Sequential
       !
       do ii = 1,size(xx)
          sumxx = sumxx + xx(ii) * xx(ii)
       end do
    else 
       !
       ! Parallel: slaves
       ! 
       do ii = 1,COMM_IN % npoi1
          sumxx  = sumxx  + xx(ii) * xx(ii)
       end do
       do ii = COMM_IN % npoi2,COMM_IN % npoi3
          sumxx  = sumxx  + xx(ii) * xx(ii)
       end do
       do jj = 1,COMM_IN % npoi4
          ii     = COMM_IN % bound_scal(jj)
          sumxx  = sumxx  + xx(ii) * xx(ii)
       end do
       call PAR_SUM(sumxx,'',COMM_IN)
    end if

    sumxx = sqrt(sumxx)

  end subroutine PAR_NORM2X

end module mod_operations
