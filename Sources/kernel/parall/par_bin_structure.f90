!----------------------------------------------------------------------
!> @addtogroup Parall
!> @{
!> @file    par_bin_structure.f90
!> @author  Guillaume Houzeaux
!> @date    01/02/2014
!> @brief   Subdomain bin structure
!> @details Compute the parallel bin structure
!>          Limit the bin size to 100 in each dimension
!> @} 
!----------------------------------------------------------------------

subroutine par_bin_structure()
  use def_kintyp,         only :  ip,rp
  use def_master,         only :  IPARALL,ISEQUEN
  use def_master,         only :  ioutp,lun_outpu
  use def_domain,         only :  xmima_tot
  use mod_communications, only :  PAR_MIN
  use mod_communications, only :  PAR_MAX
  use mod_communications, only :  PAR_SUM
  use mod_communications, only :  PAR_COMM_RANK_AND_SIZE
  use mod_communications, only :  PAR_ALLGATHERV
  use mod_communications, only :  PAR_ALLGATHER
  use mod_parall,         only :  par_memor
  use mod_parall,         only :  PAR_WORLD_SIZE
  use mod_parall,         only :  PAR_MY_CODE_RANK
  use mod_parall,         only :  par_bin_part
  use mod_parall,         only :  par_bin_boxes
  use mod_parall,         only :  par_bin_comin
  use mod_parall,         only :  par_bin_comax
  use mod_parall,         only :  par_bin_size
  use mod_parall,         only :  par_part_comin
  use mod_parall,         only :  par_part_comax
  use def_domain,         only :  coord,npoin,ndime
  use mod_memory,         only :  memory_alloca
  use mod_maths,          only :  maths_mapping_3d_to_1d
  use mod_maths,          only :  maths_mapping_1d_to_3d_x
  use mod_maths,          only :  maths_mapping_1d_to_3d_y
  use mod_maths,          only :  maths_mapping_1d_to_3d_z
  implicit none
  integer(ip)                  :: ipoin,idime
  integer(ip)                  :: nx,ny,nz          ! Number if bins in each directions
  integer(ip)                  :: imin,imax
  integer(ip)                  :: jmin,jmax
  integer(ip)                  :: kmin,kmax
  integer(ip)                  :: ii,jj,kk,iboxe
  integer(ip)                  :: iboxe1,iboxe2
  integer(ip)                  :: nxny
  integer(ip)                  :: nnbox,nzbox,kboxe
  integer(ip)                  :: ipart
  real(rp)                     :: dnx,dny,dnz
  real(rp)                     :: delta(3)          ! Bin size
  real(rp)                     :: comin(3),comax(3) ! Bounding box
  real(rp)                     :: xmibo(3)          ! Minimum box size
  real(rp)                     :: deltx,delty,deltz
  integer(ip), pointer         :: npart_per_box(:)
  integer(ip), pointer         :: list_boxes(:)
  integer(ip), pointer         :: my_list_boxes(:)
  integer(ip), pointer         :: number_boxes(:)
  integer(4),  pointer         :: recvcounts4(:)
  real(rp)                     :: toler
  !
  ! VERY IMPORTANT PARAMETER: tolerance added to the bounding boxes of the subdomains
  !
  toler = 0.05_rp

  call livinf(0_ip,'PARALL: COMPUTE BIN STRUCTURE FOR PARTITIONS',0_ip)
  !
  ! Initialize
  !
  if( IPARALL ) then
     nullify( npart_per_box )
     nullify( list_boxes )
     nullify( my_list_boxes )
     nullify( number_boxes )
     nullify( recvcounts4 )
  end if
  comin(1:3) =  huge(1.0_rp)
  comax(1:3) = -huge(1.0_rp)    

  if( PAR_MY_CODE_RANK /= 0 ) then
     !
     ! COMIN and COMAX: My bounding box
     !
     if( ndime == 1 ) then
        do ipoin = 1,npoin
           comin(1) = min(comin(1),coord(1,ipoin))
           comax(1) = max(comax(1),coord(1,ipoin))
        end do
     else if( ndime == 2 ) then
        do ipoin = 1,npoin
           comin(1:2) = min(comin(1:2),coord(1:2,ipoin))
           comax(1:2) = max(comax(1:2),coord(1:2,ipoin))
        end do
     else if( ndime == 3 ) then
        do ipoin = 1,npoin
           comin(1:3) = min(comin(1:3),coord(1:3,ipoin))
           comax(1:3) = max(comax(1:3),coord(1:3,ipoin))
        end do
     end if
     do idime = 1,ndime
        comin(idime) = comin(idime) - toler * abs(comin(idime))
        comax(idime) = comax(idime) + toler * abs(comax(idime))
     end do
     ! 
     ! XMIBO: minimum partition size in each dimension
     !
     xmibo = huge(1.0_rp)
     do idime = 1,ndime
        xmibo(idime) = min(xmibo(idime),abs(comax(idime)-comin(idime)))
     end do
  end if
  !
  ! All gather the bounding boxes
  !
  if( IPARALL ) then
     call memory_alloca(par_memor,'PAR_PART_COMIN','par_bin_structure',par_part_comin,3_ip,PAR_WORLD_SIZE,'INITIALIZE',1_ip,0_ip)   
     call memory_alloca(par_memor,'PAR_PART_COMAX','par_bin_structure',par_part_comax,3_ip,PAR_WORLD_SIZE,'INITIALIZE',1_ip,0_ip)   
     call PAR_ALLGATHER(comin,par_part_comin,3_4,'IN THE WORLD')
     call PAR_ALLGATHER(comax,par_part_comax,3_4,'IN THE WORLD')
  end if
  !
  ! Minimum subdomain size
  ! PAR_BIN_COMIN(1:NDIME) = minimum position of all-domain bounding box
  ! XMAXI(1:NDIME) = maximum position of all-domain bounding box
  !
  call PAR_MIN(ndime,xmibo(1:ndime),        'IN THE WORLD')
  par_bin_comin(1:ndime) = comin(1:ndime)
  xmima_tot(1,1:ndime)   = comin(1:ndime)
  call PAR_MIN(ndime,par_bin_comin(1:ndime),'IN THE WORLD')
  par_bin_comax(1:ndime) = comax(1:ndime)
  xmima_tot(2,1:ndime)   = comax(1:ndime)
  call PAR_MAX(ndime,par_bin_comax(1:ndime),'IN THE WORLD')

  if( IPARALL ) then
     !
     ! Size of bin
     ! DELTA(1:NDIME) = size of boxes in each dimension
     ! PAR_BIN_BOXES(1:NDIME) = nb of boxes in each dimension
     !
     par_bin_boxes = 1
     do idime = 1,ndime
        delta(idime)         = (par_bin_comax(idime)-par_bin_comin(idime) ) / xmibo(idime)
        par_bin_boxes(idime) = min(100_ip,max(1_ip,int(delta(idime),ip)))
        delta(idime)         = (par_bin_comax(idime)-par_bin_comin(idime))/real(par_bin_boxes(idime),rp)
     end do
     !
     ! Fill in bin structure
     !
     nx    = par_bin_boxes(1)
     ny    = par_bin_boxes(2)
     nz    = par_bin_boxes(3)
     dnx   = real(nx,rp)
     dny   = real(ny,rp)
     dnz   = real(nz,rp)
     nnbox = nx*ny*nz

     allocate( npart_per_box(nnbox) )
     do iboxe = 1,nnbox
        npart_per_box(iboxe) = 0
     end do

     !----------------------------------------------------------------------
     !
     ! Fill in boxes with my subdomain
     !
     !----------------------------------------------------------------------

     if( PAR_MY_CODE_RANK /= 0 ) then

        deltx = 1.0_rp / ( par_bin_comax(1)-par_bin_comin(1) )
        delty = 1.0_rp / ( par_bin_comax(2)-par_bin_comin(2) )
        if( ndime == 3 ) then
           deltz = 1.0_rp / ( par_bin_comax(3)-par_bin_comin(3) )
        end if
        nxny  = par_bin_boxes(1) * par_bin_boxes(2)

        if( ndime == 1 ) then

           imin = int(( ( comin(1) - par_bin_comin(1) ) * deltx ) * dnx , ip ) + 1
           imax = int(( ( comax(1) - par_bin_comin(1) ) * deltx ) * dnx , ip ) + 1  

           imin = max(imin,1_ip)
           imax = min(imax,par_bin_boxes(1))

           do ii = imin,imax
              iboxe = ii
              npart_per_box(iboxe) = 1
           end do

        else if( ndime == 2 ) then

           imin = int(( ( comin(1) - par_bin_comin(1) ) * deltx ) * dnx , ip ) + 1
           imax = int(( ( comax(1) - par_bin_comin(1) ) * deltx ) * dnx , ip ) + 1      

           jmin = int(( ( comin(2) - par_bin_comin(2) ) * delty ) * dny , ip ) + 1
           jmax = int(( ( comax(2) - par_bin_comin(2) ) * delty ) * dny , ip ) + 1      

           imin = max(imin,1_ip)
           imax = min(imax,par_bin_boxes(1))
           jmin = max(jmin,1_ip)
           jmax = min(jmax,par_bin_boxes(2))

           do ii = imin,imax
              do jj = jmin,jmax
                 iboxe = (jj-1_ip) * nx + ii
                 npart_per_box(iboxe) = 1
              end do
           end do

        else if( ndime == 3 ) then

           imin = int(( ( comin(1) - par_bin_comin(1) ) * deltx ) * dnx , ip ) + 1
           imax = int(( ( comax(1) - par_bin_comin(1) ) * deltx ) * dnx , ip ) + 1      

           jmin = int(( ( comin(2) - par_bin_comin(2) ) * delty ) * dny , ip ) + 1
           jmax = int(( ( comax(2) - par_bin_comin(2) ) * delty ) * dny , ip ) + 1      

           kmin = int(( ( comin(3) - par_bin_comin(3) ) * deltz ) * dnz , ip ) + 1
           kmax = int(( ( comax(3) - par_bin_comin(3) ) * deltz ) * dnz , ip ) + 1      

           imin = max(imin,1_ip)
           imax = min(imax,par_bin_boxes(1))
           jmin = max(jmin,1_ip)
           jmax = min(jmax,par_bin_boxes(2))
           kmin = max(kmin,1_ip)
           kmax = min(kmax,par_bin_boxes(3))

           do kk = kmin,kmax
              iboxe2 = nxny * (kk-1)
              do jj = jmin,jmax
                 iboxe1 = iboxe2 + par_bin_boxes(1) * (jj-1)
                 do ii = imin,imax
                    iboxe = iboxe1 + ii
                    npart_per_box(iboxe) = 1
                 end do
              end do
           end do

        end if
     end if
     !
     ! All gather NUMBER_BOXES 
     ! 
     nzbox = 0  
     if( PAR_MY_CODE_RANK /= 0 ) then
        do iboxe = 1,nnbox
           nzbox = nzbox + npart_per_box(iboxe)
        end do
     end if
     if( nzbox > 0 ) then
        allocate( my_list_boxes(nzbox) )
        nzbox = 0  
        do iboxe = 1,nnbox
           if( npart_per_box(iboxe) == 1 ) then
              nzbox = nzbox + 1
              my_list_boxes(nzbox) = iboxe
           end if
        end do
     else
        nullify( my_list_boxes )
     end if
     allocate ( number_boxes(0:PAR_WORLD_SIZE-1) )
     call PAR_ALLGATHER(nzbox,number_boxes,1_4,'IN THE WORLD')
     !
     ! NZBOX = TOTAL NUMBER OF BOXES
     !
     nzbox = 0
     do ipart = 0,PAR_WORLD_SIZE-1
        nzbox = nzbox + number_boxes(ipart)
     end do
     !
     ! LIST_BOXES = All gather list of boxes
     !
     allocate( list_boxes(nzbox) )
     do iboxe = 1,nzbox
        list_boxes(iboxe) = 0
     end do
     allocate( recvcounts4(0:PAR_WORLD_SIZE-1) )  
     do ipart = 0,PAR_WORLD_SIZE-1
        recvcounts4(ipart) = int(number_boxes(ipart),4)
     end do
     call PAR_ALLGATHERV(my_list_boxes,list_boxes,recvcounts4,'IN THE WORLD')
     deallocate( recvcounts4 )

     !----------------------------------------------------------------------
     !
     ! Fill in bin
     ! PAR_BIN_SIZE(I,J,K) = Number of partitions
     ! PAR_BIN_PART(I,J,K) % L = List of partitions in MPI_COMM_WORLD
     !
     !----------------------------------------------------------------------

     call memory_alloca(par_memor,'PAR_BIN_PART','PAR_BIN_STRUCTURE',par_bin_part,nx,ny,nz)
     call memory_alloca(par_memor,'PAR_BIN_SIZE','PAR_BIN_STRUCTURE',par_bin_size,nx,ny,nz)

     nzbox = 0
     do ipart = 0,PAR_WORLD_SIZE-1
        do iboxe = 1,number_boxes(ipart)
           nzbox = nzbox + 1
           kboxe = list_boxes(nzbox)
           ii    = maths_mapping_1d_to_3d_x(nx,ny,nz,kboxe)
           jj    = maths_mapping_1d_to_3d_y(nx,ny,nz,kboxe)
           kk    = maths_mapping_1d_to_3d_z(nx,ny,nz,kboxe)
           par_bin_size(ii,jj,kk) = par_bin_size(ii,jj,kk) + 1
        end do
     end do

     do kk = 1,nz
        do jj = 1,ny
           do ii = 1,nx
              if( par_bin_size(ii,jj,kk) > 0 ) then
                 call memory_alloca(par_memor,'PAR_BIN_PART % L','PAR_BIN_PART % L',par_bin_part(ii,jj,kk) % l,par_bin_size(ii,jj,kk) )
                 par_bin_size(ii,jj,kk) = 0
              else
                 nullify( par_bin_part(ii,jj,kk) % l )
              end if
           end do
        end do
     end do

     nzbox = 0
     do ipart = 0,PAR_WORLD_SIZE-1
        do iboxe = 1,number_boxes(ipart)
           nzbox = nzbox + 1
           kboxe = list_boxes(nzbox)
           ii    = maths_mapping_1d_to_3d_x(nx,ny,nz,kboxe)
           jj    = maths_mapping_1d_to_3d_y(nx,ny,nz,kboxe)
           kk    = maths_mapping_1d_to_3d_z(nx,ny,nz,kboxe)
           par_bin_size(ii,jj,kk) = par_bin_size(ii,jj,kk) + 1
           par_bin_part(ii,jj,kk) % l(par_bin_size(ii,jj,kk)) = ipart
        end do
     end do
     !
     ! Deallocate memory
     !
     if( associated(npart_per_box))  deallocate( npart_per_box )
     if( associated(list_boxes))     deallocate( list_boxes )
     if( associated(my_list_boxes) ) deallocate( my_list_boxes )
     !
     ! Output bin
     !
     ioutp(1) = par_bin_boxes(1) 
     ioutp(2) = par_bin_boxes(2) 
     ioutp(3) = par_bin_boxes(3) 
     call outfor(68_ip,lun_outpu,'')
     !
     ! Code/rank in each bin (II,JJ,KK)
     !
     !if( PAR_MY_WORLD_RANK == 0 ) then
     !   do ii=1,nx;do jj=1,ny ; do kk=1,nz
     !      if( par_bin_size(ii,jj,kk) > 0 ) then
     !         do iboxe = 1,par_bin_size(ii,jj,kk)
     !            ipart = par_bin_part(ii,jj,kk) % l(iboxe)
     !            icode = PAR_COMM_WORLD_TO_CODE_PERM(1,ipart)
     !            ipart = PAR_COMM_WORLD_TO_CODE_PERM(2,ipart)
     !         end do
     !      end if
     !   end do; end do; end do
     !end if
     !call runend('O.K.!')
  end if

end subroutine par_bin_structure
