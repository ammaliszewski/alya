SUBROUTINE MY_DSPEV( JOBZ, UPLO, N, AP, W, Z, LDZ, WORK, INFO )
  !
  !  -- LAPACK driver routine (version 3.2) --
  !  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  !  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  !     November 2006
  !
  !  Purpose
  !  =======
  !
  !  DSPEV computes all the eigenvalues and, optionally, eigenvectors of a
  !  real symmetric matrix A in packed storage.
  !
  !  Arguments
  !  =========
  !
  !  JOBZ    (input) CHARACTER!1
  !          = 'N':  Compute eigenvalues only;
  !          = 'V':  Compute eigenvalues and eigenvectors.
  !
  !  UPLO    (input) CHARACTER!1
  !          = 'U':  Upper triangle of A is stored;
  !          = 'L':  Lower triangle of A is stored.
  !
  !  N       (input) INTEGER
  !          The order of the matrix A.  N >= 0.
  !
  !  AP      (input/output) DOUBLE PRECISION array, dimension (N!(N+1)/2)
  !          On entry, the upper or lower triangle of the symmetric matrix
  !          A, packed columnwise in a linear array.  The j-th column of A
  !          is stored in the array AP as follows:
  !          if UPLO = 'U', AP(i + (j-1)!j/2) = A(i,j) for 1<=i<=j;
  !          if UPLO = 'L', AP(i + (j-1)!(2!n-j)/2) = A(i,j) for j<=i<=n.
  !
  !          On exit, AP is overwritten by values generated during the
  !          reduction to tridiagonal form.  If UPLO = 'U', the diagonal
  !          and first superdiagonal of the tridiagonal matrix T overwrite
  !          the corresponding elements of A, and if UPLO = 'L', the
  !          diagonal and first subdiagonal of T overwrite the
  !          corresponding elements of A.
  !
  !  W       (output) DOUBLE PRECISION array, dimension (N)
  !          If INFO = 0, the eigenvalues in ascending order.
  !
  !  Z       (output) DOUBLE PRECISION array, dimension (LDZ, N)
  !          If JOBZ = 'V', then if INFO = 0, Z contains the orthonormal
  !          eigenvectors of the matrix A, with the i-th column of Z
  !          holding the eigenvector associated with W(i).
  !          If JOBZ = 'N', then Z is not referenced.
  !
  !  LDZ     (input) INTEGER
  !          The leading dimension of the array Z.  LDZ >= 1, and if
  !          JOBZ = 'V', LDZ >= max(1,N).
  !
  !  WORK    (workspace) DOUBLE PRECISION array, dimension (3!N)
  !
  !  INFO    (output) INTEGER
  !          = 0:  successful exit.
  !          < 0:  if INFO = -i, the i-th argument had an illegal value.
  !          > 0:  if INFO = i, the algorithm failed to converge; i
  !                off-diagonal elements of an intermediate tridiagonal
  !                form did not converge to zero.
  !
  !  =====================================================================
  
  use def_kintyp, only : ip,rp,lg
  implicit none

  CHARACTER(1)  :: JOBZ, UPLO
  INTEGER(ip)   :: INFO, LDZ, N
  real(rp)      :: AP( * ), W( * ), WORK( * ), Z( LDZ, * )
  real(rp)      :: ZERO = 0.0_rp, ONE = 1.0_rp

  LOGICAL(lg)   :: WANTZ
  INTEGER(ip)   :: IINFO, IMAX, INDE, INDTAU, INDWRK, ISCALE
  real(rp)      :: ANRM, BIGNUM, EPS, RMAX, RMIN, SAFMIN, SIGMA
  real(rp)      :: SMLNUM

  LOGICAL(lg)   :: LSAME
  real(rp)      :: DLAMCH, DLANSP
  EXTERNAL         LSAME, DLAMCH, DLANSP

  EXTERNAL         DOPGTR, DSCAL, DSPTRD, DSTEQR, DSTERF, XERBLA

  WANTZ = LSAME( JOBZ, 'V' )
  !
  INFO = 0
  IF( .NOT.( WANTZ .OR. LSAME( JOBZ, 'N' ) ) ) THEN
     INFO = -1
  ELSE IF( .NOT.( LSAME( UPLO, 'U' ) .OR. LSAME( UPLO, 'L' ) ) ) THEN
     INFO = -2
  ELSE IF( N < 0 ) THEN
     INFO = -3
  ELSE IF( LDZ < 1 .OR. ( WANTZ .AND. LDZ < N ) ) THEN
     INFO = -7
  END IF
  !
  IF( INFO.NE.0 ) THEN
     CALL MY_XERBLA( 'DSPEV ', -INFO )
     RETURN
  END IF
  !
  !     Quick return if possible
  !
  IF( N == 0 ) RETURN
  !
  IF( N == 1 ) THEN
     W( 1 ) = AP( 1 )
     IF( WANTZ ) Z( 1, 1 ) = ONE
     RETURN
  END IF
  !
  !     Get machine constants.
  !
  SAFMIN = DLAMCH( 'Safe minimum' )
  EPS    = DLAMCH( 'Precision' )

  SMLNUM = SAFMIN / EPS
  BIGNUM = ONE / SMLNUM
  RMIN   = SQRT( SMLNUM )
  RMAX   = SQRT( BIGNUM )
  !
  !     Scale matrix to allowable range, if necessary.
  !
  ANRM   = DLANSP( 'M', UPLO, N, AP, WORK )
  ISCALE = 0
  IF( ANRM > ZERO .AND. ANRM < RMIN ) THEN
     ISCALE = 1
     SIGMA  = RMIN / ANRM
  ELSE IF( ANRM > RMAX ) THEN
     ISCALE = 1
     SIGMA  = RMAX / ANRM
  END IF
  IF( ISCALE == 1 ) THEN
     CALL MY_DSCAL( ( N*( N+1 ) ) / 2, SIGMA, AP, 1_ip )
  END IF
  !
  !     Call My_DSPTRD to reduce symmetric packed matrix to tridiagonal form.
  !
  INDE = 1
  INDTAU = INDE + N
  CALL MY_DSPTRD( UPLO, N, AP, W, WORK( INDE ), WORK( INDTAU ), IINFO )
  !
  !     For eigenvalues only, call my_DSTERF.  For eigenvectors, first call
  !     DOPGTR to generate the orthogonal matrix, then call my_DSTEQR.
  !
  IF( .NOT.WANTZ ) THEN
     CALL MY_DSTERF( N, W, WORK( INDE ), INFO )
  ELSE
     INDWRK = INDTAU + N
     CALL MY_DOPGTR( UPLO, N, AP, WORK( INDTAU ), Z, LDZ,  WORK( INDWRK ), IINFO )
     CALL MY_DSTEQR( JOBZ, N, W, WORK( INDE ), Z, LDZ, WORK( INDTAU ), INFO )
  END IF
  !
  !     If matrix was scaled, then rescale eigenvalues appropriately.
  !
  IF( ISCALE == 1 ) THEN
     IF( INFO == 0 ) THEN
        IMAX = N
     ELSE
        IMAX = INFO - 1
     END IF
     CALL MY_DSCAL( IMAX, ONE / SIGMA, W, 1_ip )
  END IF
  !
  RETURN
  !
  !     End of DSPEV
  !
END SUBROUTINE MY_DSPEV

SUBROUTINE MY_DSCAL(N,DA,DX,INCX)
  !
  !  Purpose
  !  =======
  !
  !     DSCAL scales a vector by a constant.
  !     uses unrolled loops for increment equal to one.
  !
  !  Further Details
  !  ===============
  !
  !     jack dongarra, linpack, 3/11/78.
  !     modified 3/93 to return if incx .le. 0.
  !     modified 12/3/93, array(1) declarations changed to array(*)
  !
  !  =====================================================================
  !
  use def_kintyp, only : ip,rp,lg
  implicit none
  real(rp)      :: DA
  INTEGER(ip)   :: INCX,N
  real(rp)      :: DX(*)
  INTEGER(ip)   :: I,M,MP1,NINCX

  IF ( N <= 0 .OR. INCX <= 0 ) RETURN

  IF (INCX == 1) THEN
     !
     ! code for increment equal to 1
     !
     !
     ! clean-up loop
     !
     M = MOD(N,5)
     IF (M /= 0) THEN
        DO I = 1,M
           DX(I) = DA*DX(I)
        END DO
        IF (N < 5) RETURN
     END IF
     MP1 = M + 1
     DO I = MP1,N,5
        DX(I)   = DA * DX(I)
        DX(I+1) = DA * DX(I+1)
        DX(I+2) = DA * DX(I+2)
        DX(I+3) = DA * DX(I+3)
        DX(I+4) = DA * DX(I+4)
     END DO
  ELSE
     !
     ! code for increment not equal to 1
     !
     NINCX = N*INCX
     DO I = 1,NINCX,INCX
        DX(I) = DA*DX(I)
     END DO
  END IF

END SUBROUTINE MY_DSCAL

FUNCTION DLANSP( NORM, UPLO, N, AP, WORK )
  !
  !  -- LAPACK auxiliary routine (version 3.2) --
  !  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  !  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  !     November 2006
  !
  !     ..
  !
  !  Purpose
  !  =======
  !
  !  DLANSP  returns the value of the one norm,  or the Frobenius norm, or
  !  the  infinity norm,  or the  element of  largest absolute value  of a
  !  real symmetric matrix A,  supplied in packed form.
  !
  !  Description
  !  ===========
  !
  !  DLANSP returns the value
  !
  !     DLANSP = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  !              (
  !              ( norm1(A),         NORM = '1', 'O' or 'o'
  !              (
  !              ( normI(A),         NORM = 'I' or 'i'
  !              (
  !              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  !
  !  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  !  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  !  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  !  squares).  Note that  max(abs(A(i,j)))  is not a consistent matrix norm.
  !
  !  Arguments
  !  =========
  !
  !  NORM    (input) CHARACTER*1
  !          Specifies the value to be returned in DLANSP as described
  !          above.
  !
  !  UPLO    (input) CHARACTER*1
  !          Specifies whether the upper or lower triangular part of the
  !          symmetric matrix A is supplied.
  !          = 'U':  Upper triangular part of A is supplied
  !          = 'L':  Lower triangular part of A is supplied
  !
  !  N       (input) INTEGER
  !          The order of the matrix A.  N >= 0.  When N = 0, DLANSP is
  !          set to zero.
  !
  !  AP      (input) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  !          The upper or lower triangle of the symmetric matrix A, packed
  !          columnwise in a linear array.  The j-th column of A is stored
  !          in the array AP as follows:
  !          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  !          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  !
  !  WORK    (workspace) DOUBLE PRECISION array, dimension (MAX(1,LWORK)),
  !          where LWORK >= N when NORM = 'I' or '1' or 'O'; otherwise,
  !          WORK is not referenced.
  !
  ! =====================================================================
  !
  use def_kintyp, only : ip,rp,lg
  implicit none
  real(rp)     :: DLANSP
  CHARACTER(1) :: NORM, UPLO
  INTEGER(ip)  :: N
  real(rp)     :: AP( * ), WORK( * )
  real(rp)     :: ONE = 1.0_rp, ZERO = 0.0_rp
  INTEGER(ip)  :: I, J, K
  real(rp)     :: ABSA, SCALE, SUM, VALUE
  EXTERNAL        DLASSQ
  LOGICAL(lg)  :: LSAME
  EXTERNAL        LSAME

  IF( N == 0 ) THEN

     VALUE = ZERO

  ELSE IF( LSAME( NORM, 'M' ) ) THEN
     !
     !        Find max(abs(A(i,j))).
     !
     VALUE = ZERO
     IF( LSAME( UPLO, 'U' ) ) THEN
        K = 1
        DO J = 1, N
           DO I = K, K + J - 1
              VALUE = MAX( VALUE, ABS( AP( I ) ) )
           end do
           K = K + J
        end do
     ELSE
        K = 1
        DO J = 1, N
           DO I = K, K + N - J
              VALUE = MAX( VALUE, ABS( AP( I ) ) )
           end do
           K = K + N - J + 1
        end do
     END IF

  ELSE IF( ( LSAME( NORM, 'I' ) ) .OR. ( LSAME( NORM, 'O' ) ) .OR. ( NORM == '1' ) ) THEN
     !
     !        Find normI(A) ( = norm1(A), since A is symmetric).
     !
     VALUE = ZERO
     K = 1
     IF( LSAME( UPLO, 'U' ) ) THEN
        DO J = 1, N
           SUM = ZERO
           DO  I = 1, J - 1
              ABSA = ABS( AP( K ) )
              SUM = SUM + ABSA
              WORK( I ) = WORK( I ) + ABSA
              K = K + 1
           end do
           WORK( J ) = SUM + ABS( AP( K ) )
           K = K + 1
        end do
        DO I = 1, N
           VALUE = MAX( VALUE, WORK( I ) )
        end do
     ELSE
        DO I = 1, N
           WORK( I ) = ZERO
        end do
        DO  J = 1, N
           SUM = WORK( J ) + ABS( AP( K ) )
           K = K + 1
           DO  I = J + 1, N
              ABSA = ABS( AP( K ) )
              SUM = SUM + ABSA
              WORK( I ) = WORK( I ) + ABSA
              K = K + 1
           end do
           VALUE = MAX( VALUE, SUM )
        end do
     END IF

  ELSE IF( ( LSAME( NORM, 'F' ) ) .OR. ( LSAME( NORM, 'E' ) ) ) THEN
     !
     !        Find normF(A).
     !
     SCALE = ZERO
     SUM = ONE
     K = 2
     IF( LSAME( UPLO, 'U' ) ) THEN
        DO  J = 2, N
           CALL MY_DLASSQ( J-1, AP( K ), 1_ip, SCALE, SUM )
           K = K + J
        end do
     ELSE
        DO J = 1, N - 1
           CALL MY_DLASSQ( N-J, AP( K ), 1_ip, SCALE, SUM )
           K = K + N - J + 1
        end do
     END IF
     SUM = 2.0_rp * SUM
     K = 1
     DO  I = 1, N
        IF( AP( K ) /= ZERO ) THEN
           ABSA = ABS( AP( K ) )
           IF( SCALE < ABSA ) THEN
              SUM = ONE + SUM*( SCALE / ABSA )*( SCALE / ABSA )
              SCALE = ABSA
           ELSE
              SUM = SUM + ( ABSA / SCALE )*( ABSA / SCALE )
           END IF
        END IF
        IF( LSAME( UPLO, 'U' ) ) THEN
           K = K + I + 1
        ELSE
           K = K + N - I + 1
        END IF
     end do
     VALUE = SCALE*SQRT( SUM )

  END IF
  !
  DLANSP = VALUE
  RETURN
  !
  !     End of DLANSP
  !
END FUNCTION DLANSP
 

SUBROUTINE MY_DLASSQ( N, X, INCX, SCALE, SUMSQ )
  !
  !  -- LAPACK auxiliary routine (version 3.2) --
  !  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  !  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  !     November 2006
  !
  !
  !  Purpose
  !  =======
  !
  !  DLASSQ  returns the values  scl  and  smsq  such that
  !
  !     ( scl!!2 )!smsq = x( 1 )!!2 +...+ x( n )!!2 + ( scale!!2 )!sumsq,
  !
  !  where  x( i ) = X( 1 + ( i - 1 )!INCX ). The value of  sumsq  is
  !  assumed to be non-negative and  scl  returns the value
  !
  !     scl = max( scale, abs( x( i ) ) ).
  !
  !  scale and sumsq must be supplied in SCALE and SUMSQ and
  !  scl and smsq are overwritten on SCALE and SUMSQ respectively.
  !
  !  The routine makes only one pass through the vector x.
  !
  !  Arguments
  !  =========
  !
  !  N       (input) INTEGER
  !          The number of elements to be used from the vector X.
  !
  !  X       (input) DOUBLE PRECISION array, dimension (N)
  !          The vector for which a scaled sum of squares is computed.
  !             x( i )  = X( 1 + ( i - 1 )!INCX ), 1 <= i <= n.
  !
  !  INCX    (input) INTEGER
  !          The increment between successive values of the vector X.
  !          INCX > 0.
  !
  !  SCALE   (input/output) real(rp) ::
  !          On entry, the value  scale  in the equation above.
  !          On exit, SCALE is overwritten with  scl , the scaling factor
  !          for the sum of squares.
  !
  !  SUMSQ   (input/output) real(rp) ::
  !          On entry, the value  sumsq  in the equation above.
  !          On exit, SUMSQ is overwritten with  smsq , the basic sum of
  !          squares from which  scl  has been factored out.
  !
  ! =====================================================================

  use def_kintyp, only : ip,rp,lg
  implicit none
  INTEGER(ip)         :: INCX, N
  real(rp)            :: SCALE, SUMSQ
  real(rp)            :: X( * )
  real(rp), parameter :: ZERO = 0.0_rp
  INTEGER(ip)         :: IX
  real(rp)            :: ABSXI,XX

  IF( N > 0 ) THEN
     DO IX = 1, 1 + ( N-1 )*INCX, INCX
        IF( X( IX ) /= ZERO ) THEN
           ABSXI = ABS( X( IX ) )
           IF( SCALE < ABSXI ) THEN
              XX    = SCALE / ABSXI
              SUMSQ = 1.0_rp + SUMSQ * XX * XX
              SCALE = ABSXI
           ELSE
              XX    = ABSXI / SCALE 
              SUMSQ = SUMSQ + XX * XX
           END IF
        END IF
     end DO
  END IF

end SUBROUTINE MY_DLASSQ

SUBROUTINE MY_DSPTRD( UPLO, N, AP, D, E, TAU, INFO )
  !
  !  -- LAPACK routine (version 3.2) --
  !  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  !  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  !     November 2006
  !
  !     ..
  !
  !  Purpose
  !  =======
  !
  !  DSPTRD reduces a real symmetric matrix A stored in packed form to
  !  symmetric tridiagonal form T by an orthogonal similarity
  !  transformation: Q!!T ! A ! Q = T.
  !
  !  Arguments
  !  =========
  !
  !  UPLO    (input) CHARACTER*1
  !          = 'U':  Upper triangle of A is stored;
  !          = 'L':  Lower triangle of A is stored.
  !
  !  N       (input) INTEGER
  !          The order of the matrix A.  N >= 0.
  !
  !  AP      (input/output) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  !          On entry, the upper or lower triangle of the symmetric matrix
  !          A, packed columnwise in a linear array.  The j-th column of A
  !          is stored in the array AP as follows:
  !          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  !          if UPLO = 'L', AP(i + (j-1)*(2*n-j)/2) = A(i,j) for j<=i<=n.
  !          On exit, if UPLO = 'U', the diagonal and first superdiagonal
  !          of A are overwritten by the corresponding elements of the
  !          tridiagonal matrix T, and the elements above the first
  !          superdiagonal, with the array TAU, represent the orthogonal
  !          matrix Q as a product of elementary reflectors; if UPLO
  !          = 'L', the diagonal and first subdiagonal of A are over-
  !          written by the corresponding elements of the tridiagonal
  !          matrix T, and the elements below the first subdiagonal, with
  !          the array TAU, represent the orthogonal matrix Q as a product
  !          of elementary reflectors. See Further Details.
  !
  !  D       (output) DOUBLE PRECISION array, dimension (N)
  !          The diagonal elements of the tridiagonal matrix T:
  !          D(i) = A(i,i).
  !
  !  E       (output) DOUBLE PRECISION array, dimension (N-1)
  !          The off-diagonal elements of the tridiagonal matrix T:
  !          E(i) = A(i,i+1) if UPLO = 'U', E(i) = A(i+1,i) if UPLO = 'L'.
  !
  !  TAU     (output) DOUBLE PRECISION array, dimension (N-1)
  !          The scalar factors of the elementary reflectors (see Further
  !          Details).
  !
  !  INFO    (output) INTEGER
  !          = 0:  successful exit
  !          < 0:  if INFO = -i, the i-th argument had an illegal value
  !
  !  Further Details
  !  ===============
  !
  !  If UPLO = 'U', the matrix Q is represented as a product of elementary
  !  reflectors
  !
  !     Q = H(n-1) . . . H(2) H(1).
  !
  !  Each H(i) has the form
  !
  !     H(i) = I - tau * v * v'
  !
  !  where tau is a real scalar, and v is a real vector with
  !  v(i+1:n) = 0 and v(i) = 1; v(1:i-1) is stored on exit in AP,
  !  overwriting A(1:i-1,i+1), and tau is stored in TAU(i).
  !
  !  If UPLO = 'L', the matrix Q is represented as a product of elementary
  !  reflectors
  !
  !     Q = H(1) H(2) . . . H(n-1).
  !
  !  Each H(i) has the form
  !
  !     H(i) = I - tau * v ! v'
  !
  !  where tau is a real scalar, and v is a real vector with
  !  v(1:i) = 0 and v(i+1) = 1; v(i+2:n) is stored on exit in AP,
  !  overwriting A(i+2:n,i), and tau is stored in TAU(i).
  !
  !  =====================================================================
  use def_kintyp, only : ip,rp,lg
  implicit none
  CHARACTER(1)          :: UPLO
  INTEGER(ip)           :: INFO, N
  real(rp)              :: AP( * ), D( * ), E( * ), TAU( * )
  real(rp),   parameter :: ONE = 1.0_rp, ZERO = 0.0_rp, HALF = 0.5_rp
  LOGICAL(lg)           :: UPPER
  INTEGER(ip)           :: I, I1, I1I1, II
  real(rp)              :: ALPHA, TAUI
  EXTERNAL                 DAXPY, DLARFG, DSPMV, DSPR2, XERBLA
  LOGICAL(lg)           :: LSAME
  real(rp)              :: DDOT
  EXTERNAL                 LSAME, DDOT
  !     ..
  !     .. Executable Statements ..
  !
  !     Test the input parameters
  !
  INFO = 0
  UPPER = LSAME( UPLO, 'U' )
  IF( .NOT.UPPER .AND. .NOT.LSAME( UPLO, 'L' ) ) THEN
     INFO = -1
  ELSE IF( N.LT.0 ) THEN
     INFO = -2
  END IF
  IF( INFO.NE.0 ) THEN
     CALL MY_XERBLA( 'DSPTRD', -INFO )
     RETURN
  END IF
  !
  !     Quick return if possible
  !
  IF( N.LE.0 ) RETURN
  !
  IF( UPPER ) THEN
     !
     !        Reduce the upper triangle of A.
     !        I1 is the index in AP of A(1,I+1).
     !
     I1 = N*( N-1 ) / 2 + 1
     DO I = N - 1, 1, -1
        !
        !           Generate elementary reflector H(i) = I - tau * v * v'
        !           to annihilate A(1:i-1,i+1)
        !
        CALL MY_DLARFG( I, AP( I1+I-1 ), AP( I1 ), 1, TAUI )
        E( I ) = AP( I1+I-1 )
        !
        IF( TAUI.NE.ZERO ) THEN
           !
           !              Apply H(i) from both sides to A(1:i,1:i)
           !
           AP( I1+I-1 ) = ONE
           !
           !              Compute  y := tau * A * v  storing y in TAU(1:i)
           !
           CALL MY_DSPMV( UPLO, I, TAUI, AP, AP( I1 ), 1, ZERO, TAU, 1 )
           !
           !              Compute  w := y - 1/2 * tau * (y'*v) * v
           !
           ALPHA = -HALF*TAUI*DDOT( I, TAU, 1, AP( I1 ), 1 )
           CALL MY_DAXPY( I, ALPHA, AP( I1 ), 1, TAU, 1 )
           !
           !              Apply the transformation as a rank-2 update:
           !                 A := A - v * w' - w * v'
           !
           CALL MY_DSPR2( UPLO, I, -ONE, AP( I1 ), 1, TAU, 1, AP )
           !
           AP( I1+I-1 ) = E( I )
        END IF
        D( I+1 ) = AP( I1+I )
        TAU( I ) = TAUI
        I1 = I1 - I
     end do
     D( 1 ) = AP( 1 )
  ELSE
     !
     !        Reduce the lower triangle of A. II is the index in AP of
     !        A(i,i) and I1I1 is the index of A(i+1,i+1).
     !
     II = 1
     DO I = 1, N - 1
        I1I1 = II + N - I + 1
        !
        !           Generate elementary reflector H(i) = I - tau * v * v'
        !           to annihilate A(i+2:n,i)
        !
        CALL MY_DLARFG( N-I, AP( II+1 ), AP( II+2 ), 1, TAUI )
        E( I ) = AP( II+1 )
        !
        IF( TAUI.NE.ZERO ) THEN
           !
           !              Apply H(i) from both sides to A(i+1:n,i+1:n)
           !
           AP( II+1 ) = ONE
           !
           !              Compute  y := tau * A * v  storing y in TAU(i:n-1)
           !
           CALL MY_DSPMV( UPLO, N-I, TAUI, AP( I1I1 ), AP( II+1 ), 1, ZERO, TAU( I ), 1 )
           !
           !              Compute  w := y - 1/2 * tau * (y'*v) * v
           !
           ALPHA = -HALF*TAUI*DDOT( N-I, TAU( I ), 1, AP( II+1 ), 1 )
           CALL MY_DAXPY( N-I, ALPHA, AP( II+1 ), 1, TAU( I ), 1 )
           !
           !              Apply the transformation as a rank-2 update:
           !                 A := A - v * w' - w * v'
           !
           CALL MY_DSPR2( UPLO, N-I, -ONE, AP( II+1 ), 1, TAU( I ), 1, AP( I1I1 ) )
           !
           AP( II+1 ) = E( I )
        END IF
        D( I ) = AP( II )
        TAU( I ) = TAUI
        II = I1I1
     end do
     D( N ) = AP( II )
  END IF
  !
  RETURN
  !
  !     End of DSPTRD
  !
END SUBROUTINE MY_DSPTRD

SUBROUTINE MY_DAXPY(N,DA,DX,INCX,DY,INCY)

  !     ..
  !
  !  Purpose
  !  =======
  !
  !     constant times a vector plus a vector.
  !     uses unrolled loops for increments equal to one.
  !     jack dongarra, linpack, 3/11/78.
  !     modified 12/3/93, array(1) declarations changed to array(*)
  !
  !
  use def_kintyp, only : ip,rp,lg
  implicit none
  real(rp)    :: DA
  INTEGER(ip) :: INCX,INCY,N
  real(rp)    :: DX(*),DY(*)
  INTEGER(ip) :: I,IX,IY,M,MP1
  !     ..
  IF (N.LE.0) RETURN
  IF (DA.EQ.0.0d0) RETURN
  IF (INCX.EQ.1 .AND. INCY.EQ.1) GO TO 20
  !
  !        code for unequal increments or equal increments
  !          not equal to 1
  !
  IX = 1
  IY = 1
  IF (INCX.LT.0) IX = (-N+1)*INCX + 1
  IF (INCY.LT.0) IY = (-N+1)*INCY + 1
  DO I = 1,N
     DY(IY) = DY(IY) + DA*DX(IX)
     IX = IX + INCX
     IY = IY + INCY
  end do
  RETURN
  !
  !        code for both increments equal to 1
  !
  !
  !        clean-up loop
  !
20 M = MOD(N,4)
  IF (M.EQ.0) GO TO 40
  DO I = 1,M
     DY(I) = DY(I) + DA*DX(I)
  end do
  IF (N.LT.4) RETURN
40 MP1 = M + 1
  DO I = MP1,N,4
     DY(I)   = DY(I)   + DA * DX(I)
     DY(I+1) = DY(I+1) + DA * DX(I+1)
     DY(I+2) = DY(I+2) + DA * DX(I+2)
     DY(I+3) = DY(I+3) + DA * DX(I+3)
  end do
  RETURN
END SUBROUTINE MY_DAXPY


SUBROUTINE MY_DGEMV(TRANS,M,N,ALPHA,A,LDA,X,INCX,BETA,Y,INCY)

  !     ..
  !
  !  Purpose
  !  =======
  !
  !  DGEMV  performs one of the matrix-vector operations
  !
  !     y := alpha*A*x + beta*y,   or   y := alpha*A'*x + beta*y,
  !
  !  where alpha and beta are scalars, x and y are vectors and A is an
  !  m by n matrix.
  !
  !  Arguments
  !  ==========
  !
  !  TRANS  - CHARACTER*1.
  !           On entry, TRANS specifies the operation to be performed as
  !           follows:
  !
  !              TRANS = 'N' or 'n'   y := alpha*A*x + beta*y.
  !
  !              TRANS = 'T' or 't'   y := alpha*A'*x + beta*y.
  !
  !              TRANS = 'C' or 'c'   y := alpha*A'*x + beta*y.
  !
  !           Unchanged on exit.
  !
  !  M      - INTEGER.
  !           On entry, M specifies the number of rows of the matrix A.
  !           M must be at least zero.
  !           Unchanged on exit.
  !
  !  N      - INTEGER.
  !           On entry, N specifies the number of columns of the matrix A.
  !           N must be at least zero.
  !           Unchanged on exit.
  !
  !  ALPHA  - DOUBLE PRECISION.
  !           On entry, ALPHA specifies the scalar alpha.
  !           Unchanged on exit.
  !
  !  A      - DOUBLE PRECISION array of DIMENSION ( LDA, n ).
  !           Before entry, the leading m by n part of the array A must
  !           contain the matrix of coefficients.
  !           Unchanged on exit.
  !
  !  LDA    - INTEGER.
  !           On entry, LDA specifies the first dimension of A as declared
  !           in the calling (sub) program. LDA must be at least
  !           max( 1, m ).
  !           Unchanged on exit.
  !
  !  X      - DOUBLE PRECISION array of DIMENSION at least
  !           ( 1 + ( n - 1 )*abs( INCX ) ) when TRANS = 'N' or 'n'
  !           and at least
  !           ( 1 + ( m - 1 )*abs( INCX ) ) otherwise.
  !           Before entry, the incremented array X must contain the
  !           vector x.
  !           Unchanged on exit.
  !
  !  INCX   - INTEGER.
  !           On entry, INCX specifies the increment for the elements of
  !           X. INCX must not be zero.
  !           Unchanged on exit.
  !
  !  BETA   - DOUBLE PRECISION.
  !           On entry, BETA specifies the scalar beta. When BETA is
  !           supplied as zero then Y need not be set on input.
  !           Unchanged on exit.
  !
  !  Y      - DOUBLE PRECISION array of DIMENSION at least
  !           ( 1 + ( m - 1 )!abs( INCY ) ) when TRANS = 'N' or 'n'
  !           and at least
  !           ( 1 + ( n - 1 )!abs( INCY ) ) otherwise.
  !           Before entry with BETA non-zero, the incremented array Y
  !           must contain the vector y. On exit, Y is overwritten by the
  !           updated vector y.
  !
  !  INCY   - INTEGER.
  !           On entry, INCY specifies the increment for the elements of
  !           Y. INCY must not be zero.
  !           Unchanged on exit.
  !
  !
  !  Level 2 Blas routine.
  !
  !  -- Written on 22-October-1986.
  !     Jack Dongarra, Argonne National Lab.
  !     Jeremy Du Croz, Nag Central Office.
  !     Sven Hammarling, Nag Central Office.
  !     Richard Hanson, Sandia National Labs.
  !
  use def_kintyp, only : ip,rp,lg
  implicit none
  real(rp)            :: ALPHA,BETA
  INTEGER(ip)         :: INCX,INCY,LDA,M,N
  CHARACTER(1)        :: TRANS
  real(rp)            :: A(LDA,*),X(*),Y(*)
  real(rp), parameter :: ONE=1.0_rp,ZERO=0.0_rp
  real(rp)            :: TEMP
  INTEGER(ip)         :: I,INFO,IX,IY,J,JX,JY,KX,KY,LENX,LENY
  LOGICAL(lg)         :: LSAME
  EXTERNAL               LSAME
  EXTERNAL               XERBLA
  !     ..
  !
  !     Test the input parameters.
  !
  INFO = 0
  IF (.NOT.LSAME(TRANS,'N') .AND. .NOT.LSAME(TRANS,'T') .AND.&
       &    .NOT.LSAME(TRANS,'C')) THEN
     INFO = 1
  ELSE IF (M.LT.0) THEN
     INFO = 2
  ELSE IF (N.LT.0) THEN
     INFO = 3
  ELSE IF (LDA.LT.MAX(1,M)) THEN
     INFO = 6
  ELSE IF (INCX.EQ.0) THEN
     INFO = 8
  ELSE IF (INCY.EQ.0) THEN
     INFO = 11
  END IF
  IF (INFO.NE.0) THEN
     CALL MY_XERBLA('DGEMV ',INFO)
     RETURN
  END IF
  !
  !     Quick return if possible.
  !
  IF ((M.EQ.0) .OR. (N.EQ.0) .OR. ((ALPHA.EQ.ZERO).AND. (BETA.EQ.ONE))) RETURN
  !
  !     Set  LENX  and  LENY, the lengths of the vectors x and y, and set
  !     up the start points in  X  and  Y.
  !
  IF (LSAME(TRANS,'N')) THEN
     LENX = N
     LENY = M
  ELSE
     LENX = M
     LENY = N
  END IF
  IF (INCX.GT.0) THEN
     KX = 1
  ELSE
     KX = 1 - (LENX-1)*INCX
  END IF
  IF (INCY.GT.0) THEN
     KY = 1
  ELSE
     KY = 1 - (LENY-1)*INCY
  END IF
  !
  !     Start the operations. In this version the elements of A are
  !     accessed sequentially with one pass through A.
  !
  !     First form  y := beta*y.
  !
  IF (BETA.NE.ONE) THEN
     IF (INCY.EQ.1) THEN
        IF (BETA.EQ.ZERO) THEN
           DO I = 1,LENY
              Y(I) = ZERO
           end do
        ELSE
           DO I = 1,LENY
              Y(I) = BETA*Y(I)
           end do
        END IF
     ELSE
        IY = KY
        IF (BETA.EQ.ZERO) THEN
           DO I = 1,LENY
              Y(IY) = ZERO
              IY = IY + INCY
           end do
        ELSE
           DO I = 1,LENY
              Y(IY) = BETA*Y(IY)
              IY = IY + INCY
           end do
        END IF
     END IF
  END IF
  IF (ALPHA.EQ.ZERO) RETURN
  IF (LSAME(TRANS,'N')) THEN
     !
     !        Form  y := alpha*A*x + y.
     !
     JX = KX
     IF (INCY.EQ.1) THEN
        DO J = 1,N
           IF (X(JX).NE.ZERO) THEN
              TEMP = ALPHA*X(JX)
              DO I = 1,M
                 Y(I) = Y(I) + TEMP*A(I,J)
              end do
           END IF
           JX = JX + INCX
        end do
     ELSE
        DO J = 1,N
           IF (X(JX).NE.ZERO) THEN
              TEMP = ALPHA*X(JX)
              IY = KY
              DO I = 1,M
                 Y(IY) = Y(IY) + TEMP*A(I,J)
                 IY = IY + INCY
              end do
           END IF
           JX = JX + INCX
        end do
     END IF
  ELSE
     !
     !        Form  y := alpha*A'*x + y.
     !
     JY = KY
     IF (INCX.EQ.1) THEN
        DO  J = 1,N
           TEMP = ZERO
           DO  I = 1,M
              TEMP = TEMP + A(I,J)*X(I)
           end do
           Y(JY) = Y(JY) + ALPHA*TEMP
           JY = JY + INCY
        end do
     ELSE
        DO  J = 1,N
           TEMP = ZERO
           IX = KX
           DO  I = 1,M
              TEMP = TEMP + A(I,J)*X(IX)
              IX = IX + INCX
           end do
           Y(JY) = Y(JY) + ALPHA*TEMP
           JY = JY + INCY
        end do
     END IF
  END IF
  !
  RETURN
  !
  !     End of DGEMV .
  !
END SUBROUTINE MY_DGEMV


SUBROUTINE MY_DGER(M,N,ALPHA,X,INCX,Y,INCY,A,LDA)

  !     ..
  !
  !  Purpose
  !  =======
  !
  !  DGER   performs the rank 1 operation
  !
  !     A := alpha!x!y' + A,
  !
  !  where alpha is a scalar, x is an m element vector, y is an n element
  !  vector and A is an m by n matrix.
  !
  !  Arguments
  !  ==========
  !
  !  M      - INTEGER.
  !           On entry, M specifies the number of rows of the matrix A.
  !           M must be at least zero.
  !           Unchanged on exit.
  !
  !  N      - INTEGER.
  !           On entry, N specifies the number of columns of the matrix A.
  !           N must be at least zero.
  !           Unchanged on exit.
  !
  !  ALPHA  - DOUBLE PRECISION.
  !           On entry, ALPHA specifies the scalar alpha.
  !           Unchanged on exit.
  !
  !  X      - DOUBLE PRECISION array of dimension at least
  !           ( 1 + ( m - 1 )!abs( INCX ) ).
  !           Before entry, the incremented array X must contain the m
  !           element vector x.
  !           Unchanged on exit.
  !
  !  INCX   - INTEGER.
  !           On entry, INCX specifies the increment for the elements of
  !           X. INCX must not be zero.
  !           Unchanged on exit.
  !
  !  Y      - DOUBLE PRECISION array of dimension at least
  !           ( 1 + ( n - 1 )!abs( INCY ) ).
  !           Before entry, the incremented array Y must contain the n
  !           element vector y.
  !           Unchanged on exit.
  !
  !  INCY   - INTEGER.
  !           On entry, INCY specifies the increment for the elements of
  !           Y. INCY must not be zero.
  !           Unchanged on exit.
  !
  !  A      - DOUBLE PRECISION array of DIMENSION ( LDA, n ).
  !           Before entry, the leading m by n part of the array A must
  !           contain the matrix of coefficients. On exit, A is
  !           overwritten by the updated matrix.
  !
  !  LDA    - INTEGER.
  !           On entry, LDA specifies the first dimension of A as declared
  !           in the calling (sub) program. LDA must be at least
  !           max( 1, m ).
  !           Unchanged on exit.
  !
  !
  !  Level 2 Blas routine.
  !
  !  -- Written on 22-October-1986.
  !     Jack Dongarra, Argonne National Lab.
  !     Jeremy Du Croz, Nag Central Office.
  !     Sven Hammarling, Nag Central Office.
  !     Richard Hanson, Sandia National Labs.
  !
  !
  !     .. Parameters ..
  use def_kintyp, only : ip,rp,lg
  implicit none
  real(rp)            :: ALPHA
  INTEGER(ip)         :: INCX,INCY,LDA,M,N
  real(rp)            :: A(LDA,*),X(*),Y(*)
  real(rp), parameter :: ZERO=0.0_rp
  real(rp)            :: TEMP
  INTEGER(ip)         :: I,INFO,IX,J,JY,KX
  EXTERNAL               XERBLA
  !
  !     Test the input parameters.
  !
  INFO = 0
  IF (M.LT.0) THEN
     INFO = 1
  ELSE IF (N.LT.0) THEN
     INFO = 2
  ELSE IF (INCX.EQ.0) THEN
     INFO = 5
  ELSE IF (INCY.EQ.0) THEN
     INFO = 7
  ELSE IF (LDA.LT.MAX(1,M)) THEN
     INFO = 9
  END IF
  IF (INFO.NE.0) THEN
     CALL MY_XERBLA('DGER  ',INFO)
     RETURN
  END IF
  !
  !     Quick return if possible.
  !
  IF ((M.EQ.0) .OR. (N.EQ.0) .OR. (ALPHA.EQ.ZERO)) RETURN
  !
  !     Start the operations. In this version the elements of A are
  !     accessed sequentially with one pass through A.
  !
  IF (INCY.GT.0) THEN
     JY = 1
  ELSE
     JY = 1 - (N-1)*INCY
  END IF
  IF (INCX.EQ.1) THEN
     DO J = 1,N
        IF (Y(JY).NE.ZERO) THEN
           TEMP = ALPHA*Y(JY)
           DO I = 1,M
              A(I,J) = A(I,J) + X(I)*TEMP
           end do
        END IF
        JY = JY + INCY
     end do
  ELSE
     IF (INCX.GT.0) THEN
        KX = 1
     ELSE
        KX = 1 - (M-1)*INCX
     END IF
     DO J = 1,N
        IF (Y(JY).NE.ZERO) THEN
           TEMP = ALPHA*Y(JY)
           IX = KX
           DO I = 1,M
              A(I,J) = A(I,J) + X(IX)*TEMP
              IX = IX + INCX
           end do
        END IF
        JY = JY + INCY
     end do
  END IF
  !
  RETURN
  !
  !     End of DGER  .
  !
END SUBROUTINE MY_DGER

SUBROUTINE MY_DLAE2( A, B, C, RT1, RT2 )
  !
  !  -- LAPACK auxiliary routine (version 3.2) --
  !  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  !  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  !     November 2006
  !
  !     ..
  !
  !  Purpose
  !  =======
  !
  !  DLAE2  computes the eigenvalues of a 2-by-2 symmetric matrix
  !     [  A   B  ]
  !     [  B   C  ].
  !  On return, RT1 is the eigenvalue of larger absolute value, and RT2
  !  is the eigenvalue of smaller absolute value.
  !
  !  Arguments
  !  =========
  !
  !  A       (input) DOUBLE PRECISION
  !          The (1,1) element of the 2-by-2 matrix.
  !
  !  B       (input) DOUBLE PRECISION
  !          The (1,2) and (2,1) elements of the 2-by-2 matrix.
  !
  !  C       (input) DOUBLE PRECISION
  !          The (2,2) element of the 2-by-2 matrix.
  !
  !  RT1     (output) DOUBLE PRECISION
  !          The eigenvalue of larger absolute value.
  !
  !  RT2     (output) DOUBLE PRECISION
  !          The eigenvalue of smaller absolute value.
  !
  !  Further Details
  !  ===============
  !
  !  RT1 is accurate to a few ulps barring over/underflow.
  !
  !  RT2 may be inaccurate if there is massive cancellation in the
  !  determinant A!C-B!B; higher precision or correctly rounded or
  !  correctly truncated arithmetic would be needed to compute RT2
  !  accurately in all cases.
  !
  !  Overflow is possible only if RT1 is within a factor of 5 of overflow.
  !  Underflow is harmless if the input data is 0 or exceeds
  !     underflow_threshold / macheps.
  !
  ! =====================================================================
  !
  use def_kintyp, only : ip,rp,lg
  implicit none
  real(rp)            ::   A, B, C, RT1, RT2
  real(rp), parameter ::   ONE  = 1.0_rp
  real(rp), parameter ::   TWO  = 2.0_rp
  real(rp), parameter ::   ZERO = 0.0_rp 
  real(rp), parameter ::   HALF = 0.5_rp
  real(rp)            ::   AB, ACMN, ACMX, ADF, DF, RT, SM, TB

  !
  !     Compute the eigenvalues
  !
  SM  = A + C
  DF  = A - C
  ADF = ABS( DF )
  TB  = B + B
  AB  = ABS( TB )
  IF( ABS( A ).GT.ABS( C ) ) THEN
     ACMX = A
     ACMN = C
  ELSE
     ACMX = C
     ACMN = A
  END IF
  IF( ADF.GT.AB ) THEN
     RT = ADF * SQRT( ONE+( AB / ADF )**2 )
  ELSE IF( ADF.LT.AB ) THEN
     RT = AB * SQRT( ONE+( ADF / AB )**2 )
  ELSE
     !
     !        Includes case AB=ADF=0
     !
     RT = AB * SQRT( TWO )
  END IF

  IF( SM.LT.ZERO ) THEN
     RT1 = HALF*( SM-RT )
     !
     !        Order of execution important.
     !        To get fully accurate smaller eigenvalue,
     !        next line needs to be executed in higher precision.
     !
     RT2 = ( ACMX / RT1 )*ACMN - ( B / RT1 )*B
  ELSE IF( SM.GT.ZERO ) THEN
     RT1 = HALF*( SM+RT )
     !
     !        Order of execution important.
     !        To get fully accurate smaller eigenvalue,
     !        next line needs to be executed in higher precision.
     !
     RT2 = ( ACMX / RT1 )*ACMN - ( B / RT1 )*B
  ELSE
     !
     !        Includes case RT1 = RT2 = 0
     !
     RT1 = HALF*RT
     RT2 = -HALF*RT
  END IF
  RETURN
  !
  !     End of DLAE2
  !
END SUBROUTINE MY_DLAE2
