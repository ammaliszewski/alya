subroutine errors(itask,ierro,iwarn,messa)
  !------------------------------------------------------------------------
  !****f* master/errors
  ! NAME 
  !    errors
  ! DESCRIPTION
  !    ITASK == 0 ... Error from master
  !    ITASK == 1 ... From master
  !    ITASK == 2 ... From master
  !    ITASK == 2 ... From module
  ! USES
  ! USED BY
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_domain
  use def_solver
  implicit none
  integer(ip),  intent(in)            :: itask
  integer(ip),  intent(inout), target :: ierro(1)
  integer(ip),  intent(inout), target :: iwarn(1)
  character(*), intent(in)            :: messa
  integer(ip)                         :: kfl_ptask_old
  integer(ip),  target                :: dummi(2)
  character(20)                       :: werro
  
  kfl_ptask_old = kfl_ptask
  kfl_ptask     = 1

  if( itask == 3 ) then

     !-------------------------------------------------------------------
     !
     ! Called by modules to treat errors and/or warnings
     !
     !-------------------------------------------------------------------

     if( IPARALL ) then        
        dummi(1) =  ierro(1)
        dummi(2) =  iwarn(1)
        call parari('BCT',0_ip,2_ip,dummi)
        ierro(1) =  dummi(1)
        iwarn(1) =  dummi(2)
     end if
     !
     ! Warning
     !
     if( iwarn(1) /= 0 ) call outfor( 3_ip,momod(modul)%lun_outpu,' ')
     !
     ! Stop
     !
     werro = intost(ierro(1))
     if( ierro(1) /= 0 ) call outfor(-4_ip,momod(modul)%lun_outpu,trim(werro))

  else

     !-------------------------------------------------------------------
     !
     ! Errors detected by Kernel
     !
     !-------------------------------------------------------------------

     call parari('BCT',0_ip,1_ip,ierro)
     
     if( ierro(1) /= 0 ) then
        if( itask == 1 ) then        
           call outfor(  4_ip,lun_outpu,trim(messa))
        else if( itask == 2 ) then        
           call outfor(-47_ip,lun_outpu,trim(messa))
        end if
     end if
  end if

  kfl_ptask = kfl_ptask_old 

end subroutine errors
