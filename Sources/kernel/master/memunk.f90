subroutine memunk(itask)

  !-----------------------------------------------------------------------
  !****f* master/memunk
  ! NAME
  !    Turnon
  ! DESCRIPTION
  !    This routine allocates memory for all the unknowns of the problem
  !    When using Parall, Master allocates minimum memory
  ! USES
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------

  use def_parame
  use def_master 
  use def_kermod
  use def_domain
  use def_solver
  use mod_memory
  implicit none
  integer(ip), intent(in) :: itask
  !
  ! Deallocate before allocating
  !
  if( itask == 2 ) then
  end if
  !
  ! Algebraic solver
  !
  call memory_alloca(memma,'AMATR','memunk',amatr,nzmat)
  call memory_alloca(memma,'BMATR','memunk',bmatr,nzmbt)
  call memory_alloca(memma,'RHSID','memunk',rhsid,nzrhs)
  call memory_alloca(memma,'UNKNO','memunk',unkno,nzrhs)
  call memory_alloca(memma,'PMATR','memunk',pmatr,nzpre)
  call memory_alloca(memma,'ERRES','memunk',erres,nzerr)
  !
  ! Eigen solver
  !
  call memory_alloca(memma,'EIGEN','memunk',eigen,neige)
  call memory_alloca(memma,'EIGVA','memunk',eigva,neiva)
  !
  ! Algebraic complex solver 
  !
  call memory_alloca(memma,'AMATX','memunk',amatx,nzmax)
  call memory_alloca(memma,'RHSIX','memunk',rhsix,nzrhx)
  call memory_alloca(memma,'UNKNX','memunk',unknx,nzrhx)
  call memory_alloca(memma,'PMATX','memunk',pmatx,nzprx)
  !
  ! lumpped mass matrix - for use in dual time step preconditioner
  !
   if( INOTMASTER ) call memgeo(63_ip) ! not sure if this is the optimal place
  
end subroutine memunk
