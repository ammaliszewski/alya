subroutine setgts(order)
  !-----------------------------------------------------------------------
  !****f* master/setgts
  ! NAME
  !    setgts
  ! DESCRIPTION
  !    This routine computes the time step
  !    KFL_TIMCO = 0 ... Prescribed time step
  !              = 1 ... Module compute their time step
  ! USES
  ! USED BY
  !    Turnon, Timste
  !***
  !-----------------------------------------------------------------------
  use def_master
  implicit none
  integer(ip), intent(in) :: order

  select case (order)

  case (1_ip)
     !
     ! Compute DTINV and the time for the first time step, as well as the
     ! maximum number of time steps allowed    
     !
     dtinv = 0.0_rp
     call timfun(1_ip)
     if( dtime >  0.0_rp ) dtinv = 1.0_rp / dtime
     if( timef <= timei ) then
        dtime = 0.0_rp
        dtinv = 0.0_rp
     end if
     dtold(1) = dtime
     dtinv_old(1) = 1.0_rp / max(dtold(1),zeror)
     oltim    = 0.0_rp
     cutim    = timei 

  case (2_ip)
     ! 
     ! Use minimum of critical time steps
     !
     dtold(4) = dtold(3)
     dtold(3) = dtold(2)
     dtold(2) = dtold(1)
     dtold(1) = dtime
     call timfun(2_ip)
     if( kfl_timco == 1 ) then
        if( dtinv /= 0.0_rp ) dtime = 1.0_rp / dtinv
     end if
     if( dtold(1) == 0.0_rp ) dtold(1) = dtime
     if( dtold(2) == 0.0_rp ) dtold(2) = dtime
     if( dtold(3) == 0.0_rp ) dtold(3) = dtime
     if( dtold(4) == 0.0_rp ) dtold(4) = dtime
     dtinv_old(1:10) = 1.0_rp / max(dtold(1:10),zeror)
     oltim           = cutim
     cutim           = cutim + dtime
     ioutp(1)        = ittim
     routp(1)        = dtime
     routp(2)        = cutim
     call outfor(15_ip,lun_outpu,' ')

  end select


end subroutine setgts
