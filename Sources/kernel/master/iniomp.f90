subroutine iniomp()
  !-----------------------------------------------------------------------
  !****f* master/iniomp
  ! NAME
  !    iniomp
  ! DESCRIPTION
  !    This subroutine prints the number of OpenMP threads
  ! USES
  ! USED BY
  !    Reapro
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only : ip
  use mod_parall, only : par_omp_num_threads
  implicit none  
#ifdef _OPENMP
  integer(4)  :: OMP_GET_MAX_THREADS 
  integer(4)  :: nthr4
  
  nthr4 = OMP_GET_MAX_THREADS()
  par_omp_num_threads = int(nthr4,ip)           
  call livinf(58_ip,' ',par_omp_num_threads) 
#endif

end subroutine iniomp
  
