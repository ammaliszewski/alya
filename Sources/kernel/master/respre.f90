subroutine respre(itask,kfl_gores)
  !------------------------------------------------------------------------
  !****f* kernel/respre
  ! NAME 
  !    respre
  ! DESCRIPTION
  !    Define if a preliminary or restart run should be carried out
  ! USES
  ! USED BY
  !    nsi_turnon
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
use mod_memory
  implicit none
  integer(ip), intent(in)  :: itask
  integer(ip), intent(out) :: kfl_gores
  integer(ip), save        :: kfl_ptask_old,jtask

  if( itask /= 3 ) then
     kfl_gores = 0
     kfl_ptask_old = kfl_ptask
  end if
  jtask = abs(itask)

  select case ( jtask )

  case ( READ_RESTART_FILE )
     !
     ! Read from restart file
     !
     if( kfl_rstar >= 1 ) then
        kfl_gores     = 1
        kfl_reawr     = 1
        if( itask > 0 ) then
           kfl_ptask     = 1
           call livinf(67_ip,' ',modul)
           call moddef(7_ip)
        end if
     end if

  case ( WRITE_RESTART_FILE )
     !
     ! Write to restart file
     !
     if(  kfl_preli        == 1                .and. ( &
          mod(ittim,nprit) == 0                .or.    &
          cutim            >= timef-1.0e-10_rp .or.    &
          ittim            >= mitim            .or.    &
          kfl_timei        == 0                )) then
        kfl_gores     = 1
        kfl_reawr     = 2
        if( itask > 0 ) then
           kfl_ptask     = 1
           call livinf(68_ip,' ',modul)
           call moddef(8_ip)
        end if
     end if

  case ( 3_ip )
     !
     ! Recover old values
     !
     lun_postp = lun_postp_old
     !kfl_outfo = kfl_outfo_old
     if( kfl_gores == 1 ) kfl_ptask = kfl_ptask_old
     kfl_reawr = 0             
     call moddef(6_ip)
     nullify(gesca)
     nullify(gevec)

  end select

  if( kfl_gores == 1 .and. itask <= 2 ) then

     if( IMASTER ) then
        nullify(gevec)
        nullify(gesca)
        nullify(ger3p)
     end if

     lun_postp_old = lun_postp
     !if( modul > 0 ) lun_postp = momod(modul) % lun_rstar
     if( modul > 0 ) lun_postp = momod(modul) % lun_rstpo
     !kfl_outfo_old = kfl_outfo
     !kfl_outfo     = -itask

  end if

end subroutine respre
