subroutine asserr(ielem,nasva,elest)
  !-----------------------------------------------------------------------
  !****f* master/asserr
  ! NAME
  !    asserr
  ! DESCRIPTION
  !    This subroutine assembles error estimators given by the modules
  !    and pass-it to Adapti
  ! USES
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master 
  use def_solver
  use def_inpout
  implicit none  
  integer(ip), intent(in)  :: ielem,nasva
  real(rp), intent(in)     :: elest(nasva)  

  integer(ip)              :: iasva,ierre,jelem
  real(rp)                 :: errmo(10)

  if (nzerr == 1) then
     return
  else
     if (ielem < 0) then
        do jelem= 1,nelem
           do iasva= 1,nasva
              ierre= (jelem-1)*nasva+iasva
              erres(ierre)= 0.0_rp
           end do
        end do
     else
        do iasva= 1,nasva
           ierre= (ielem-1)*nasva+iasva
           erres(ierre) = erres(ierre) +  elest(iasva)* elest(iasva)/real(nelem)
        end do
     end if
     !
     ! Last element done: normalize
     !
     if (ielem == nelem) then
        errmo= 0.0_rp
        do jelem= 1,nelem
           do iasva= 1,nasva
              ierre= (jelem-1)*nasva+iasva
              errmo(iasva)= errmo(iasva) + erres(ierre)*erres(ierre)
           end do
        end do
        do iasva= 1,nasva
           errmo(iasva)= sqrt(errmo(iasva))
           do jelem= 1,nelem
              ierre= (jelem-1)*nasva+iasva
              erres(ierre)= erres(ierre)/errmo(iasva)
           end do
        end do
     end if
  end if

end subroutine asserr
