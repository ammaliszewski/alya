subroutine runend(message)
  !-----------------------------------------------------------------------
  !
  ! This routine stops the run and writes the summary of CPU time.
  !
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  implicit none
  character(*) :: message
  logical(lg)  :: lopen
  integer      :: cy
  integer(ip),parameter     :: kfl_makefail = 0_ip
  character(10)             :: tomakefail
  real(rp)                  :: kfail

  if ( kfl_makefail == 1_ip ) then
     !
     ! To get the line where it has failed 
     !
     tomakefail=' -1'
     read(tomakefail,*) kfail
     write(kfl_paral+6100,*)'message',message
     flush(kfl_paral+6100)
     kfail = log (kfail)
  end if
  !
  ! Write message and stop the run
  !
  if( INOTSLAVE .or. kfl_outpu_par == 1 ) then

     if( message /= 'O.K.!' ) then
        inquire(unit=lun_outpu,opened=lopen)
        if(lopen) call outfor(0_ip,lun_outpu,trim(message))
        if( INOTSLAVE ) then
           call livinf(999_ip,trim(message),zero)
           !
           ! Live a little, a haiku about Alya
           !
           ! Fer
           !
           call system_clock(cy) 
           if (mod(cy,100) == 0 ) then
              call livinf(-1_ip,'.',zero)
              call livinf(-1_ip,'..',zero)
              call livinf(-1_ip,'   Yesterday it worked.',zero)
              call livinf(-1_ip,'   Tomorrow too, today not.',zero)
              call livinf(-1_ip,'   Alya is like that.',zero)
              call livinf(-1_ip,'..',zero)
              call livinf(-1_ip,'.',zero)
           endif
        endif
     else 
        call outfor(14_ip,lun_outpu,' ')
        if( INOTSLAVE ) call livinf(1000_ip,' ',zero)
     end if

  else if( ISLAVE .or. kfl_outpu_par == 1 ) then

     if( message /= 'O.K.!' ) then 
        print*,'RUNEND MESSAGE|'
        print*,'------>',message
        flush(6)
     end if

  end if

  if( IPARALL ) then
     if( message == 'O.K.!' ) then
        call Parall(six)
     else
        call Parall(51_ip)
     end if
  end if
  stop

end subroutine runend
