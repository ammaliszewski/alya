module mod_iofile

  use def_kintyp
  use def_parame
  use def_master
  
contains 
  
  subroutine iofile(itask,nunit,files,messa,stato,formo,posio,conver,lgexi)
    !------------------------------------------------------------------------
    !
    ! This routine opens/closes logical units and treat errors
    !
    !------------------------------------------------------------------------
    implicit none
    integer(ip),   intent(in) :: nunit,itask 
    character*(*), intent(in) :: messa,files
    integer(4)                :: ioerr,nunit4
    character*(*), optional   :: stato,formo,posio,conver
    logical(lg),   optional   :: lgexi
    character(7)              :: statu
    character(6)              :: posit
    character(11)             :: forma
    nunit4=int(nunit,4)

    select case (itask)

    case(0_ip)
       !
       ! Open unit
       !
       if(present(stato)) then
          statu=stato
       else
          statu='unknown'
       end if
       if(present(formo)) then
          forma=formo
       else
          forma='formatted'
       end if
       if(present(posio)) then
          posit=posio
       else
          posit='asis'
       end if

#ifdef BIG_ENDIAN
       ! forces all to big endian
       open(nunit4,file=adjustl(trim(files)),status=statu,form=forma, &
            iostat=ioerr,position=posit,convert='BIG_ENDIAN')
#else
       if( present(conver) ) then
          open(nunit4,file=adjustl(trim(files)),status=statu,form=forma,iostat=ioerr,position=posit,convert=conver)
       else
          open(nunit4,file=adjustl(trim(files)),status=statu,form=forma,iostat=ioerr,position=posit)
       end if
#endif   
       if(ioerr/=0) then
          file_opened = .false.
          !print*,'COULD NOT OPEN'
          call runend('ERROR WHEN OPENING THE '//trim(messa)//' FILE: '//adjustl(trim(files)))
       else
          file_opened = .true.
       end if

    case(2_ip)
       !
       ! Close unit
       !
       if(present(stato)) then
          statu=stato
          close(nunit4,status=statu,iostat=ioerr)
       else
          close(nunit4,iostat=ioerr)
       end if

       if(ioerr/=0) then
          if(trim(messa)=='RESTART') then
             write(lun_outpu,101)&
                  'COULD NOT OPEN THEN FOLLOWING RESTART FILE: '//trim(files)
          else
             call runend('ERROR WHEN CLOSING THE '//trim(messa)//' FILE: '//adjustl(trim(files)))
          end if
       end if

    case(3_ip)
       !
       ! Delete file
       !
       close(nunit4,status='delete',iostat=ioerr)
       if(ioerr/=0) then
          call runend('ERROR WHEN CLOSING THE '//trim(messa)//' FILE: '//adjustl(trim(files)))
       end if

    case(4_ip)
       !
       ! Check if file exist
       !
       if(present(formo)) then
          forma=formo
       else
          forma='formatted'
       end if
       if(present(posio)) then
          posit=posio
       else
          posit='asis'
       end if 
       open(nunit4,file=adjustl(trim(files)),status='old',form=forma,iostat=ioerr,position=posit)

       if( ioerr /= 0 ) then
          file_opened = .false.
          kfl_reawr   = -abs(kfl_reawr)
       else
          file_opened = .true.
          close(nunit4)
       end if
       
    case(5_ip)
       
       call runend('iofile: case 5 no longer valid - now use if def in case 0')

    case(6_ip)
       !
       ! Inquire
       !
       if( present(lgexi) ) then
          inquire(file=adjustl(trim(files)),exist=lgexi)
       end if

    case(7_ip)
       !
       ! Open unit but do not crash if cannot
       !
       if(present(stato)) then
          statu=stato
       else
          statu='unknown'
       end if
       if(present(formo)) then
          forma=formo
       else
          forma='formatted'
       end if
       if(present(posio)) then
          posit=posio
       else
          posit='asis'
       end if

#ifdef BIG_ENDIAN
       ! forces all to big endian
       open(nunit4,file=adjustl(trim(files)),status=statu,form=forma, &
            iostat=ioerr,position=posit,convert='BIG_ENDIAN')
#else
       if( present(conver) ) then
          open(nunit4,file=adjustl(trim(files)),status=statu,form=forma,iostat=ioerr,position=posit,convert=conver)
       else
          open(nunit4,file=adjustl(trim(files)),status=statu,form=forma,iostat=ioerr,position=posit)
       end if
#endif   
       if(ioerr/=0) then
          file_opened = .false.
       else
          file_opened = .true.
       end if

    end select
    !
    ! Format
    !
101 format(5x,'WARNING: ',a)

  end subroutine iofile

end module mod_iofile
