subroutine Concou
  !-----------------------------------------------------------------------
  !****f* master/Concou
  ! NAME
  !    Concou
  ! DESCRIPTION
  !    This routine checks the general convergence of the run.
  ! USES
  !    Nastin
  !    Temper
  !    Codire
  !    Turbul
  !    Exmedi
  !    Nastal
  !    Alefor
  !    Latbol
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_coupli
  use mod_parall
  implicit none
  integer(ip)       :: imodu
  real(rp)          :: cpu_refer
  integer(ip)       :: iorde
  integer(ip), save :: itert = 0

  integer(4)  :: istat
  integer(ip) :: icoup
  integer(ip), save :: caca = 0
  !
  ! Live information
  ! 
  call livinf(seven,' ',zero)
  ! 
  ! Initializations
  !
  do iorde = 1,mmodu-1
     if( lmord(iorde,iblok) > 0 ) glres(lmord(iorde,iblok)) = 0.0_rp
  end do
  kfl_gocou = 0
! *******************
! Added for FSI tests
!  kfl_gocou = 1
! *******************
  call cputim(cpu_refer)
  cpu_refer = cpu_refer - cpu_initi
  routp(1)  = cpu_refer 
  call outfor(10_ip,lun_outpu,' ')
  !
  ! Write convergence in the order of module iterations
  !
  call moduls(ITASK_CONCOU)
  !
  ! Coupling convergence
  !
  !call cou_cvgunk()  
  !
  ! Write to convergence file and keep iterating...
  !
  itert = itert + 1
  if( INOTSLAVE ) then
     if( itert == 1 .and. kfl_rstar /= 2 ) then
        write(lun_conve,11)
        write(lun_conve,12)
        do imodu = 1,mmodu-1
           write(lun_conve,13) namod(imodu),imodu+3
        end do
        write(lun_conve,*)
     end if
     write(lun_conve,14) ittim,itcou,cutim
     do imodu = 1,mmodu-1
        if( kfl_modul(imodu) /= 0 ) then
           write(lun_conve,15) glres(imodu)
        else
           write(lun_conve,16) zero        
        end if
     end do
     write(lun_conve,*)
     call flush(lun_conve) 
  end if
  !
  ! False convergence
  !
  itcou = itcou + 1
  if( itcou > micou(iblok) ) kfl_gocou = 0  
  !
  ! Formats
  !
11 format('# ','       Time','     Global','       Current','  Module residuals -->')
12 format('# ','       step','  iteration','          time',$)
13 format('   ',a6,' (',i2,')',$)
14 format(4x,i9,2x,i9,2x,e12.6,$)
15 format(2x,e12.6,$)
16 format(2x,i12,$)
20 format(&
       & 1x,'Current step',2x,'Maximum steps',/,&
       & 1x,i7,6x,i7)

end subroutine Concou
      
