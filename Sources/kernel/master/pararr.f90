subroutine pararr(wtask,ntype,ndimr,rvarr)
  !------------------------------------------------------------------------
  !****f* Parall/pararr
  ! NAME
  !    pararr
  ! DESCRIPTION
  !    Works with arrays to deal with parallelization
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_kintyp,    only  :  ip,rp,lg
  use def_master,    only  :  npari,nparr,nparc,parin,parre,parr1,kfl_paral
  use def_master,    only  :  party,parki,pardi,IPARALL,pard1,pari1,npasi
  use def_master,    only  :  NPOIN_TYPE,NFACE_TYPE,NBOPO_TYPE,lntra,nfacg
  use def_domain,    only  :  npoin,nbopo,npoin_2
  use mod_couplings, only  :  COU_INTERPOLATE_NODAL_VALUES
  use mod_couplings, only  :  I_AM_INVOLVED_IN_A_COUPLING_TYPE
  use mod_communications, only  :  PAR_INTERFACE_NODE_EXCHANGE
  use def_coupli,    only  :  mcoup_subdomain
  use def_coupli,    only  :  mcoup
  use def_coupli,    only  :  coupling_type
  use def_coupli,    only  :  RESIDUAL,UNKNOWN
  use def_coupli,    only  :  BETWEEN_SUBDOMAINS
  use def_coupli,    only  :  STRESS_PROJECTION
  use def_coupli,    only  :  PROJECTION
  use def_coupli,    only  :  COU_COMM_COUPLING_ARRAY
  use def_master,    only  :  current_zone,kfl_paral,modul,ISLAVE,mmodu
  use def_solver,    only  :  solve_sol
  implicit none 
  character(3), intent(in) :: wtask
  integer(ip),  intent(in) :: ntype,ndimr
  real(rp),     target     :: rvarr(ndimr)

  integer(ip)              :: ii,kk,icoup,jcoup,ipoin,jj,ll
  real(rp),     pointer    :: rvarr_tmp(:),rvarr_int(:,:)
  logical(lg)              :: use_mask

  if( IPARALL ) then

     npari = 0
     nparc = 0

     if( wtask == 'BCT' ) then
        !
        ! Broadcast: master computes something that is broadcasted to the slaves
        !
        nparr =  ndimr
        parre => rvarr
        call Parall(2_ip)

     else if( wtask == 'SND' ) then

        nparr =  ndimr
        parre => rvarr
        call Parall(3_ip)

     else if( wtask == 'RCV' ) then

        nparr =  ndimr
        parre => rvarr
        call Parall(4_ip)

     else if( wtask == 'MIN' ) then

        nparr =  ndimr
        parre => rvarr
        call Parall(5_ip)

     else if( wtask == 'SUM' ) then

        nparr =  ndimr
        parre => rvarr
        call Parall(9_ip)

     else if( wtask == 'MAX' ) then

        nparr =  ndimr
        parre => rvarr
        call Parall(10_ip)

     else if( wtask == 'S2M' ) then

        nparr =  ndimr
        parre => rvarr
        call Parall(24_ip)

     else if( wtask == 'GAT' ) then

        if( ntype == NPOIN_TYPE .or. ntype == NBOPO_TYPE ) then
           if( ntype == NPOIN_TYPE ) pard1 = ndimr/npoin
           if( ntype == NBOPO_TYPE ) pard1 = ndimr/nbopo
           party = ntype
           if( pard1 == 1 ) then
              parki =  2
              pardi =  1
           else
              parki =  6
              pardi =  1
           end if
        else
           party =  ntype
           parki =  2
           pardi =  1
           npari =  ndimr
        end if
        parr1 => rvarr
        call Parall(300_ip)

     else if( wtask == 'IBI' ) then 

        pari1 => lntra
        pard1 =  ndimr/npoin
        parr1 => rvarr(1:ndimr)
        call Parall(405_ip)

     else if( wtask == 'SLX' .or. wtask == 'SLG' ) then
        !
        ! par_slexch
        !
        if( ISLAVE ) then
           if( wtask == 'SLG' ) then
              call Parall(611_ip) 
           end if

           if( ntype == NPOIN_TYPE .or. ntype == NBOPO_TYPE ) then
              if( ntype == NPOIN_TYPE ) pard1 = ndimr/npoin
              if( ntype == NBOPO_TYPE ) pard1 = ndimr/nbopo
              party =  ntype
              if( pard1 == 1 ) then
                 parki =  2
                 pardi =  1
              else
                 parki =  5
                 pardi =  1
              end if

              parr1 => rvarr

              call Parall(400_ip) 

           else if( ntype == NFACE_TYPE ) then

              pard1 = ndimr/nfacg
              party = ntype
              if( pard1 == 1 ) then
                 parki =  2
                 pardi =  1
              else
                 parki =  5
                 pardi =  1
              end if
              parr1 => rvarr
              call Parall(607_ip)
           else
              call runend('PARARR: NOT CODED')
           end if

           if( wtask == 'SLG' ) then
              call Parall(612_ip)
           end if
           !
           ! Subdomain coupling ! OJO CAMBIAR
           !
           if( ntype == NPOIN_TYPE .and. mcoup_subdomain > 0 ) then

              kk = ndimr/npoin
              allocate( rvarr_tmp(ndimr) )

              do ii = 1,ndimr
                 rvarr_tmp(ii) = rvarr(ii)
              end do
              do icoup = 1,mcoup

                 use_mask = .false.

                 if( modul == 0 ) then
                    use_mask = .false.
                 else
                    if( solve_sol(1) % kfl_iffix == 0 ) then
                       use_mask = .false.
                    else 
                       use_mask = .true.
                    end if
                 end if

                 if( coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS ) then
                    if(    coupling_type(icoup) % what  == RESIDUAL          .and. &
                         & coupling_type(icoup) % itype /= STRESS_PROJECTION .and. &
                         & coupling_type(icoup) % itype /= PROJECTION        ) then
                       if( .not. use_mask ) then
                          call COU_INTERPOLATE_NODAL_VALUES(icoup,kk,rvarr,rvarr_tmp)
                       else
                          call COU_INTERPOLATE_NODAL_VALUES(icoup,kk,rvarr,rvarr_tmp,solve_sol(1) % kfl_fixno)
                       end if
                    end if
                 end if
              end do

              if(  I_AM_INVOLVED_IN_A_COUPLING_TYPE(BETWEEN_SUBDOMAINS,RESIDUAL,STRESS_PROJECTION) .or. &
                   I_AM_INVOLVED_IN_A_COUPLING_TYPE(BETWEEN_SUBDOMAINS,RESIDUAL,PROJECTION)        ) then
                 allocate( rvarr_int(kk,npoin) )
                 do ii = 1,ndimr
                    rvarr_tmp(ii) = rvarr(ii)
                 end do
                 do ii = 1,npoin
                    rvarr_int(1:kk,ii) = 0.0_rp
                 end do
                 do icoup = 1,mcoup
                    if( coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS ) then
                       if( coupling_type(icoup) % what == RESIDUAL .and. &
                            ( coupling_type(icoup) % itype == STRESS_PROJECTION .or. coupling_type(icoup) % itype == PROJECTION ) ) then
                          if( .not. use_mask ) then
                             call COU_INTERPOLATE_NODAL_VALUES(icoup,kk,rvarr_int,rvarr_tmp)
                          else
                             call COU_INTERPOLATE_NODAL_VALUES(icoup,kk,rvarr_int,rvarr_tmp,solve_sol(1) % kfl_fixno)
                          end if
                       end if
                    end if
                 end do
                 ! Do the exchange only if I have at least one wet node of with PROJECTION coupling
                 !call PAR_INTERFACE_NODE_EXCHANGE(rvarr_int,'SUM','IN MY ZONE','SYNCHRONOUS')
                 call PAR_INTERFACE_NODE_EXCHANGE(rvarr_int,'SUM',COU_COMM_COUPLING_ARRAY(current_zone),'SYNCHRONOUS')
                 ll = 0
                 do ii = 1,npoin
                    do jj = 1,kk
                       ll = ll + 1
                       rvarr(ll) = rvarr(ll) + rvarr_int(jj,ii)
                    end do
                 end do
                 deallocate(rvarr_int)
              end if

              do ii = 1,ndimr
                 rvarr_tmp(ii) = rvarr(ii)
              end do

              do icoup = 1,mcoup
                 if( coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS ) then
                    if( coupling_type(icoup) % what == UNKNOWN ) then
                       call COU_INTERPOLATE_NODAL_VALUES(icoup,kk,rvarr,rvarr_tmp)
                    end if
                 end if
              end do

              deallocate(rvarr_tmp)

           end if
        end if

     else if( wtask == 'SLE' ) then
        !
        ! par_slequa: Equal values at interface
        !
        if( ntype == NPOIN_TYPE .or. ntype == NBOPO_TYPE ) then
           if( ntype == NPOIN_TYPE ) pard1 = ndimr/npoin
           if( ntype == NBOPO_TYPE ) pard1 = ndimr/nbopo
           party =  ntype
           if( pard1 == 1 ) then
              parki =  2
              pardi =  1
           else
              parki =  5
              pardi =  1
           end if
           parr1 => rvarr
           call Parall(900_ip)
        else
           call runend('PARARR: NOT CODED')
        end if

     else if( wtask == 'SLA' ) then
        !
        ! par_slexca
        !
        if( ntype == NPOIN_TYPE .or. ntype == NBOPO_TYPE ) then
           if( ntype == NPOIN_TYPE ) pard1 = ndimr/npoin
           if( ntype == NBOPO_TYPE ) pard1 = ndimr/nbopo
           party =  ntype
           if( pard1 == 1 ) then
              parki =  2
              pardi =  1
           else
              parki =  5
              pardi =  1
           end if
           parr1 => rvarr
           call Parall(407_ip)
        else
           call runend('PARARR: NOT CODED')
        end if

     else if( wtask == 'SNR' ) then 
        !
        ! par_slaves
        !
        npasi = 0
        npari = 0
        call Parall(411_ip)

     else if( wtask == 'SRA' ) then 
        !
        ! par_slaves: Send receive to any slave
        !
        npasi = 0
        npari = 0
        call Parall(408_ip)

     else if( wtask == 'SMI' ) then
        !
        ! par_slexch
        !
        if( ntype == NPOIN_TYPE .or. ntype == NBOPO_TYPE ) then
           if( ntype == NPOIN_TYPE ) pard1 = ndimr/npoin
           if( ntype == NBOPO_TYPE ) pard1 = ndimr/nbopo
           party =  ntype
           if( pard1 == 1 ) then
              parki =  2
              pardi =  1
           else
              parki =  5
              pardi =  1
           end if
           parr1 => rvarr
           call Parall(424_ip)
        else
           call runend('PARARR: NOT CODED')
        end if

     else if( wtask == 'SMA' ) then
        !
        ! par_slexch
        !
        if( ntype == NPOIN_TYPE .or. ntype == NBOPO_TYPE ) then
           if( ntype == NPOIN_TYPE ) pard1 = ndimr/npoin
           if( ntype == NBOPO_TYPE ) pard1 = ndimr/nbopo
           party =  ntype
           if( pard1 == 1 ) then
              parki =  2
              pardi =  1
           else
              parki =  5
              pardi =  1
           end if
           parr1 => rvarr
           call Parall(425_ip)
        else
           call runend('PARARR: NOT CODED')
        end if

     else if( wtask == 'FRI' ) then
        !
        ! par_slexfr: take fringe geometry nodes
        !
        party = ntype
        if( ntype == NPOIN_TYPE ) then
           pard1 = ndimr/npoin_2
           if( pard1 == 1 ) then
              parki =  1
              pardi =  1
           else
              parki =  5
              pardi =  2
           end if
           parr1 => rvarr
           call Parall(429_ip)
        end if

     else if( wtask == 'SCH' ) then 

        parr1 => rvarr
        call Parall(426_ip)

     else if( wtask == 'SSS' )  then

        parr1 => rvarr
        call Parall(1005_ip)

     else

        call runend('PARARR: WRONG CASE')

     end if

     nparr = 0
     nullify(parre)

  end if

end subroutine pararr
 
