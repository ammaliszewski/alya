subroutine soldef(itask)
  !-----------------------------------------------------------------------
  !****f* master/soldef
  ! NAME
  !    soldef
  ! DESCRIPTION
  !    This subroutine deos the following:
  !    ITASK = 0 ... Initialize the solver type
  !    ITASK = 1 ... Bridge between modules and parall service
  !    ITASK = 4 ... Define matrices, RHS and preconditioner sizes
  ! USES
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master 
  use def_solver
  use def_inpout
  use mod_memchk
  use mod_commdom_alya, only: CPLNG  
  implicit none  
  integer(ip), intent(in) :: itask
  integer(ip)             :: ivari,nvari,iauxi,cont,idime
  integer(ip)             :: ipoin,pos,ndofn_per_block,ii
  integer(4)              :: istat

  if( itask <= 0_ip ) then

     !-------------------------------------------------------------------
     !
     ! Initialization
     !
     !-------------------------------------------------------------------
 
     if( itask < 0 ) then
        nvari = -itask
        momod(modul) % nvari_sol = nvari
        allocate(momod(modul) % solve(nvari))                ! Allocate memory   
        solve     => momod(modul) % solve
        solve_sol => momod(modul) % solve
     else if( itask < -100 ) then
        nvari = -itask-100
        momod(-servi) % nvari_sol = nvari
        allocate(momod(-servi) % solve(nvari))               ! Allocate memory   
        solve     => momod(-servi) % solve
        solve_sol => momod(-servi) % solve       
     end if

     do ivari=1,size(solve_sol)
        solve_sol(ivari) % wprob     = 'PROBLEM NAME'        ! Should be defined in module
        solve_sol(ivari) % wsolv     = 'SOLVER NAME'         ! Defined in reasol
        solve_sol(ivari) % wprec     = 'NONE'                ! Defined in reasol
        solve_sol(ivari) % nprob     = 1                     ! Number of prblem
        solve_sol(ivari) % nzmat     = 1                     ! Matrix size
        solve_sol(ivari) % nzrhs     = npoin                 ! RHS size
        solve_sol(ivari) % nequa     = npoin                 ! Number of equations
        solve_sol(ivari) % nunkn     = npoin                 ! Number of unknowns
        solve_sol(ivari) % ndofn     = 1                     ! D.o.f
        solve_sol(ivari) % ndof2     = 1                     ! D.o.f^2
        solve_sol(ivari) % nsist     = 1                     ! 0 system
        solve_sol(ivari) % nesky     = 0                     ! Skyline size
        solve_sol(ivari) % nseqn     = 1                     ! Number of simultaneous equations
        solve_sol(ivari) % nrhss     = 1                     ! Number of simultaneous RHS
        solve_sol(ivari) % kfl_exist = 0                     ! Solver exists
        solve_sol(ivari) % kfl_algso = -999                  ! No solver: do not modify (used to check errors)
        solve_sol(ivari) % kfl_symme = 0                     ! Solver has an unsymmetric assembly 
        solve_sol(ivari) % kfl_symeq = 0                     ! Equation is non-symmetric
        solve_sol(ivari) % kfl_facto = 1                     ! Factorize matrix
        solve_sol(ivari) % kfl_resid = 0                     ! Residual for iterative solvers
        solve_sol(ivari) % kfl_recov = 0                     ! Recover original residual after slave exchange  
        solve_sol(ivari) % kfl_ortho = 1                     ! Orthogonalization GMRES: Gramm-Schmidt=0, modified=1
        solve_sol(ivari) % kfl_limit = 0                     ! Algebraic limiter
        solve_sol(ivari) % kfl_posgr = 0                     ! Postprocess of groups
        solve_sol(ivari) % kfl_adres = 0                     ! Adaptive residual
        solve_sol(ivari) % kfl_cmplx = 0                     ! Complez solver JELENA
        solve_sol(ivari) % kfl_assem = 0                     ! Matrix not assembled
        solve_sol(ivari) % kfl_schur = 0                     ! Default solver is not of Schur type
        solve_sol(ivari) % kfl_zones = 0                     ! Current zone
        solve_sol(ivari) % kfl_schum = 0                     ! If matrices come from a Schur complement
        solve_sol(ivari) % miter     = 1                     ! Max. number of iterations: should be 1, for Richardson solvers
        solve_sol(ivari) % nkryd     = 10                    ! Krylov dimensions
        solve_sol(ivari) % kfl_coarse  = 0                   ! No coarse solver
        solve_sol(ivari) % kfl_version = 0                   ! Old version
        if( modul == ID_TEMPER ) &
             solve_sol(ivari) % kfl_version = 1 
        if( modul == ID_SOLIDZ ) &
             solve_sol(ivari) % kfl_version = 1 
        if( modul == ID_NASTIN ) &
             solve_sol(ivari) % kfl_version = 1 
        if( modul == ID_ALEFOR ) &
             solve_sol(ivari) % kfl_version = 1
        if( modul == ID_PARTIS ) &
             solve_sol(ivari) % kfl_version = 1
!
!        if( modul == ID_NASTAL ) &
!             solve_sol(ivari) % kfl_version = 1
!
        !
        ! Block Gauss-Seidel
        !
        solve_sol(ivari) % kfl_blogs = 0                     ! Monolithic solution / Block Gauss-Seidel
        solve_sol(ivari) % nblok     = 0                     ! Gauss-Seidel number of blocks
        solve_sol(ivari) % ndofn_per_block = 1               ! dof per block
        nullify(solve_sol(ivari) % lperm_block)
        nullify(solve_sol(ivari) % linvp_block)

        solve_sol(ivari) % kfl_iffix = 0                     ! Fixity not imposed by solver
        solve_sol(ivari) % kfl_bvnat = 0                     ! Natural b.c. not imposed in solver
        nullify(solve_sol(ivari) % kfl_fixno)                ! Fixity array
        nullify(solve_sol(ivari) % bvess)                    ! Prescribed values array
        nullify(solve_sol(ivari) % bvnat)                    ! Prescribed force array
        !
        ! Deflated
        !
        solve_sol(ivari) % ngrou     = -1                    ! Deflated CG: # groups (automatic # groups)
        solve_sol(ivari) % nskyl     = 0                     ! Deflated CG: Skyline A' matrix size
        solve_sol(ivari) % icoml     = 0                     ! Deflated CG: Communication group
        solve_sol(ivari) % ifbop     = 0                     ! Deflated CG: If boundary conditions are imposed on boundary nodes
        solve_sol(ivari) % kfl_gathe = 0                     ! Deflated CG: all reduce
        solve_sol(ivari) % kfl_defas = 0                     ! Deflated CG: skyline
        solve_sol(ivari) % kfl_defso = 0                     ! Deflated CG: drect solver
        nullify(solve_sol(ivari) % limpo)                    ! List of imposed nodes (deflated CG)
        nullify(solve_sol(ivari) % lgrou)                    ! Group list for deflated CG
        nullify(solve_sol(ivari) % iskyl)                    ! Deflated CG: Skyline index
        nullify(solve_sol(ivari) % idiag)                    ! Deflated CG: Pointer to skyline diagonal
        nullify(solve_sol(ivari) % ia)                       ! Deflated CG: Sparse graph
        nullify(solve_sol(ivari) % ja)                       ! Deflated CG: Sparse graph
        nullify(solve_sol(ivari) % amgro)                    ! Deflated CG: Sparse graph
        nullify(solve_sol(ivari) % lcoun)                    ! Deflated CG: All gather strategy           
        nullify(solve_sol(ivari) % lbig)                     ! Deflated CG: All gather strategy           
        nullify(solve_sol(ivari) % displ)                    ! Deflated CG: All gather strategy 
        nullify(solve_sol(ivari) % disp4)                    ! Deflated CG: All gather strategy
        nullify(solve_sol(ivari) % lcou4)                    ! Deflated CG: All gather strategy
        nullify(solve_sol(ivari) % xsmall)                   ! Deflated CG: All gather strategy            
        nullify(solve_sol(ivari) % xbig)                     ! Deflated CG: All gather strategy
        nullify(solve_sol(ivari) % Ildef)                    ! Sparse direct solver deflated CG
        nullify(solve_sol(ivari) % Jldef)                    ! Sparse direct solver deflated CG
        nullify(solve_sol(ivari) % Lndef)                    ! Sparse direct solver deflated CG
        nullify(solve_sol(ivari) % iUdef)                    ! Sparse direct solver deflated CG
        nullify(solve_sol(ivari) % jUdef)                    ! Sparse direct solver deflated CG
        nullify(solve_sol(ivari) % Undef)                    ! Sparse direct solver deflated CG
        nullify(solve_sol(ivari) % invpRdef)                 ! Sparse direct solver deflated CG
        nullify(solve_sol(ivari) % invpCdef)                 ! Sparse direct solver deflated CG
        nullify(solve_sol(ivari) % askyldef)                 ! Deflated CG: skyline coarse matrix   
        !
        ! Renumbered Gauss-Seidel
        !
        solve_sol(ivari) % kfl_renumbered_gs = 0             ! Renumbered Gauss-Seidel permutation
        nullify(solve_sol(ivari) % permr_gs)                 ! Renumbered Gauss-Seidel permutation
        nullify(solve_sol(ivari) % invpr_gs)                 ! Renumbered Gauss-Seidel inverse permutation
        nullify(solve_sol(ivari) % lgrou_gs)                 ! Renumbered Gauss-Seidel groups
        nullify(solve_sol(ivari) % vecto_gs)                 ! Renumbered Gauss-Seidel vetor
        !
        ! Linelet
        !
        solve_sol(ivari) % kfl_factl = 0                     ! Linelet: not factorized
        solve_sol(ivari) % nline     = 0                     ! Linelet: 
        solve_sol(ivari) % nlin1     = 0                     ! Linelet: 
        solve_sol(ivari) % npoin     = 0                     ! Linelet: # of nodes in linelets 
        solve_sol(ivari) % kfl_linty = 0                     ! Linelet: linelet type
        solve_sol(ivari) % toler     = 30.0_rp               ! Linelet: tolerance aspect ratio
        nullify(solve_sol(ivari) % lpntr)                    ! Linelet: tridiag. to original sym. matrix
        nullify(solve_sol(ivari) % lrenu)                    ! Linelet: point position in tridiag.
        nullify(solve_sol(ivari) % lrenup)                   ! Linelet: inverse permutation
        nullify(solve_sol(ivari) % limli)                    ! Linelet: list of imposed node 
        nullify(solve_sol(ivari) % lline)                    ! Linelet: Pointer for each linelet
        nullify(solve_sol(ivari) % trima)                    ! Linelet: Tridiagonal matrix

        solve_sol(ivari) % kfl_penal = 0                     ! Penalization: method
        solve_sol(ivari) % penal     = 1.0_rp                ! Penalization: parameter

        solve_sol(ivari) % kfl_alcsr = 0                     ! Sparse direct solver
        nullify(solve_sol(ivari) % iL)                       ! Sparse direct solver
        nullify(solve_sol(ivari) % jL)                       ! Sparse direct solver
        nullify(solve_sol(ivari) % iU)                       ! Sparse direct solver
        nullify(solve_sol(ivari) % jU)                       ! Sparse direct solver
        nullify(solve_sol(ivari) % Ln)                       ! Sparse direct solver
        nullify(solve_sol(ivari) % Un)                       ! Sparse direct solver

        nullify(solve_sol(ivari) % iLpre)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % jLpre)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % iUpre)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % jUpre)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % Lnpre)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % Unpre)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % permR)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % invpR)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % invpC)                    ! Sparse direct solver precond
        nullify(solve_sol(ivari) % Aiipre)                   ! Sparse direct solver precond

        nullify(solve_sol(ivari) % iLras)                    ! RAS solver precond
        nullify(solve_sol(ivari) % jLras)                    ! RAS solver precond
        nullify(solve_sol(ivari) % iUras)                    ! RAS solver precond
        nullify(solve_sol(ivari) % jUras)                    ! RAS solver precond   
        nullify(solve_sol(ivari) % Lnras)                    ! RAS solver precond   
        nullify(solve_sol(ivari) % Unras)                    ! RAS solver precond     
        nullify(solve_sol(ivari) % ia_ras)                   ! RAS solver precond
        nullify(solve_sol(ivari) % ja_ras)                   ! RAS solver precond
        nullify(solve_sol(ivari) % permRras)                 ! RAS solver precond
        nullify(solve_sol(ivari) % invpRras)                 ! RAS solver precond
        nullify(solve_sol(ivari) % invpCras)                 ! RAS solver precond
        nullify(solve_sol(ivari) % Aras)                     ! RAS solver precond    

        solve_sol(ivari) % kfl_scpre = 0                     ! Schur preconditioner solver: skyline
        solve_sol(ivari) % kfl_scaii = 1                     ! Schur matrix solver: sparse

        nullify(solve_sol(ivari) % iLpredef)
        nullify(solve_sol(ivari) % jLpredef)
        nullify(solve_sol(ivari) % iUpredef)
        nullify(solve_sol(ivari) % jUpredef)    
        nullify(solve_sol(ivari) % Lnpredef)    
        nullify(solve_sol(ivari) % Unpredef)      
        nullify(solve_sol(ivari) % invpRpredef)
        nullify(solve_sol(ivari) % invpCpredef)
        nullify(solve_sol(ivari) % askylpredef)  
        solve_sol(ivari) % kfl_defpr = 2                     ! Multigrid preconditioner: smoother preconditioner

        solve_sol(ivari) % solco     = 1.0e-6_rp             ! Solver tolerance
        solve_sol(ivari) % relso     = 1.0_rp                ! Solver relaxation
        solve_sol(ivari) % dtinv     = 1.0_rp                ! 1/dt for explicit solver
        solve_sol(ivari) % xdiag     = 1.0_rp                ! Coefficient for diagonal solvers
        solve_sol(ivari) % resin     = 1.0_rp                !
        solve_sol(ivari) % resfi     = 1.0_rp                !
        solve_sol(ivari) % resi2     = 1.0_rp                !
        solve_sol(ivari) % resf2     = 1.0_rp                !
        solve_sol(ivari) % reni2     = 1.0_rp                !
        solve_sol(ivari) % cputi     = 0.0_rp                ! Total CPU time
        solve_sol(ivari) % cpu_schur = 0.0_rp                ! Schur complement solver timing
        solve_sol(ivari) % adres     = 0.1_rp                ! Adaptive residual tolerance
        solve_sol(ivari) % solmi     = 1.0e-6_rp             ! Minimum solver tolerance
        solve_sol(ivari) % xorth     = 0.0_rp                ! Orthogonalty check for CG solver

        solve_sol(ivari) % heade     = 0                     ! Header has not been written
        solve_sol(ivari) % itsol(1)  =  huge(1_ip)           ! Max. # of solver iterations: do not modify
        solve_sol(ivari) % itsol(2)  = -huge(1_ip)           ! Min. # of solver iterations: do not modify
        solve_sol(ivari) % itsol(3)  = 0                     ! Ave. # of solver iterations: do not modify
        solve_sol(ivari) % nsolv     = 0                     ! # of solves
        solve_sol(ivari) % kfl_cvgso = 0                     ! Convergence flag
        solve_sol(ivari) % lun_cvgso = 0                     ! Convergence unit
        solve_sol(ivari) % kfl_solve = 0                     ! Output flag
        solve_sol(ivari) % lun_solve = 0                     ! Output unit
        solve_sol(ivari) % kfl_exres = 0                     ! Do not output exact residual
        solve_sol(ivari) % kfl_preco = 0                     ! Preconditioner
        solve_sol(ivari) % kfl_leftr = 0                     ! Left preconditioner
        solve_sol(ivari) % itpre     = 1                     ! Preconditioner iterations
        solve_sol(ivari) % kfl_marke = 0                     ! Output in market format
        solve_sol(ivari) % kfl_force = 0                     ! Do not force solver continuity
        solve_sol(ivari) % nzpre     = 0                     ! Preconditioner # non-null coefficients
        solve_sol(ivari) % lfill     = 0                     ! Filling for ILU preconditioning
        solve_sol(ivari) % thres     = 0.0_rp                ! Threshold for ILU preconditioning
        !
        ! Coarse Aii
        !
        solve_sol(ivari) % ngaii     = 1
        solve_sol(ivari) % kfl_deaii = 1
        nullify(solve_sol(ivari) % iaaii)
        nullify(solve_sol(ivari) % jaaii)
        nullify(solve_sol(ivari) % iLaii)
        nullify(solve_sol(ivari) % jLaii)
        nullify(solve_sol(ivari) % iUaii)
        nullify(solve_sol(ivari) % jUaii)    
        nullify(solve_sol(ivari) % Lnaii)    
        nullify(solve_sol(ivari) % Unaii)      
        nullify(solve_sol(ivari) % invpRaii)
        nullify(solve_sol(ivari) % invpCaii)
        nullify(solve_sol(ivari) % iskylaii)        ! Coarse Aii: Skyline index
        nullify(solve_sol(ivari) % idiagaii)        ! Coarse Aii: Pointer to skyline diagonal
        nullify(solve_sol(ivari) % aiicoarse)         
        nullify(solve_sol(ivari) % lgaii)           ! List of group
        !
        ! Block structure
        !
        solve_sol(ivari) % num_blocks       = 1 
        solve_sol(ivari) % block_num        = 1
        solve_sol(ivari) % block_dimensions = 1 
        solve_sol(ivari) % block_pointers   = 1
        solve_sol(ivari) % kfl_react        = 0
        nullify( solve_sol(ivari) % reaction       )
        nullify( solve_sol(ivari) % lpoin_block    )
        nullify( solve_sol(ivari) % lpoin_reaction )
        nullify( solve_sol(ivari) % bvess          )
        nullify( solve_sol(ivari) % kfl_fixno      )
        nullify( solve_sol(ivari) % bvnat          ) 
        do ii = 1,size(solve_sol(ivari) % block_array)
           nullify( solve_sol(ivari) % block_array(ii) % bvess     )
           nullify( solve_sol(ivari) % block_array(ii) % kfl_fixno )
           nullify( solve_sol(ivari) % block_array(ii) % bvnat     )
        end do
        !
        ! Main matrix comes from a Schur complement
        ! A = A1 + A2*A3^-1*A4
        !
        solve_sol(ivari) % ndofn_A3 = 1             ! Dimension of A3
        nullify(solve_sol(ivari) % A1)
        nullify(solve_sol(ivari) % A2)
        nullify(solve_sol(ivari) % A3)
        nullify(solve_sol(ivari) % A4)
        nullify(solve_sol(ivari) % invA3)

     end do

  else if( itask == 1_ip ) then

     !-------------------------------------------------------------------
     !
     ! Used for Parall service
     !
     !-------------------------------------------------------------------

     do ivari=1,size(solve_sol)
        call iexcha(solve_sol(ivari) % nprob)      ! Number of problems
        !call iexcha(solve_sol(ivari) % nzmat)     ! Local to each slave
        !call iexcha(solve_sol(ivari) % nzrhs)     ! Local to each slave
        !call iexcha(solve_sol(ivari) % nequa)     ! Local to each slave
        !call iexcha(solve_sol(ivari) % nunkn)     ! Local to each slave
        !call iexcha(solve_sol(ivari) % ndofn)     ! Local to each slave
        !call iexcha(solve_sol(ivari) % ndof2)     ! Local to each slave
        call iexcha(solve_sol(ivari) % nsist)
        call iexcha(solve_sol(ivari) % nesky)
        call iexcha(solve_sol(ivari) % nseqn)
        call iexcha(solve_sol(ivari) % nrhss)
        call iexcha(solve_sol(ivari) % kfl_exist)
        call iexcha(solve_sol(ivari) % kfl_algso)
        call iexcha(solve_sol(ivari) % kfl_symme)
        call iexcha(solve_sol(ivari) % kfl_facto)
        call iexcha(solve_sol(ivari) % kfl_resid)
        call iexcha(solve_sol(ivari) % kfl_recov)
        call iexcha(solve_sol(ivari) % kfl_ortho)
        call iexcha(solve_sol(ivari) % kfl_limit)
        call iexcha(solve_sol(ivari) % kfl_posgr)
        call iexcha(solve_sol(ivari) % kfl_adres)
        call iexcha(solve_sol(ivari) % kfl_cmplx) 
        call iexcha(solve_sol(ivari) % kfl_assem) 
        call iexcha(solve_sol(ivari) % kfl_schur)
        call iexcha(solve_sol(ivari) % kfl_zones)
        call iexcha(solve_sol(ivari) % kfl_schum)
        call iexcha(solve_sol(ivari) % miter)
        call iexcha(solve_sol(ivari) % nkryd)
        call iexcha(solve_sol(ivari) % kfl_coarse)
        call iexcha(solve_sol(ivari) % kfl_version)
        call iexcha(solve_sol(ivari) % kfl_blogs)
        call iexcha(solve_sol(ivari) % nblok)
        !call iexcha(solve_sol(ivari) % ndofn_per_block)
        !call iexcha(solve_sol(ivari) % lperm_block)
        !call iexcha(solve_sol(ivari) % linvp_block)
        !
        ! Fixity
        !
        call iexcha(solve_sol(ivari) % kfl_iffix)
        call iexcha(solve_sol(ivari) % kfl_bvnat)
        !
        ! Deflated CG
        !
        call iexcha(solve_sol(ivari) % ngrou)
        !call iexcha(solve_sol(ivari) % nskyl)
        !call iexcha(solve_sol(ivari) % icoml)
        call iexcha(solve_sol(ivari) % ifbop)
        call iexcha(solve_sol(ivari) % kfl_gathe)
        call iexcha(solve_sol(ivari) % kfl_defas)
        call iexcha(solve_sol(ivari) % kfl_defso)
        !call iexcha(solve_sol(ivari) % limpo)       ! List of imposed nodes (deflated CG)
        !call iexcha(solve_sol(ivari) % lgrou)       ! Group list for deflated CG
        !call iexcha(solve_sol(ivari) % iskyl)       ! 
        !call iexcha(solve_sol(ivari) % idiag)       ! 
        !
        ! Linelet
        !        
        !call iexcha(solve_sol(ivari) % npntr)       ! Linelet: tolerance aspect ratio
        !call iexcha(solve_sol(ivari) % nlpntr       ! Linelet: # points in linelet
        !call iexcha(solve_sol(ivari) % nline        ! Linelet: # linelets
        call iexcha(solve_sol(ivari) % kfl_factl)    ! Linelet: Factorization flag
        !call iexcha(solve_sol(ivari) % lpntr(:)     ! Linelet: tridiag. to original sym. matrix
        !call iexcha(solve_sol(ivari) % lrenu(:)     ! Linelet: point position in tridiag.
        !call iexcha(solve_sol(ivari) % lrenup(:)    ! Linelet: inverse permutation
        !call iexcha(solve_sol(ivari) % limli(:)     ! Linelet: list of imposed node 
        !call iexcha(solve_sol(ivari) % lline(:)     ! Linelet: Pointer for each linelet
        !call rexcha(solve_sol(ivari) % trima)       ! Linelet: Tridiagonal matrix
        call rexcha(solve_sol(ivari) % toler)        ! Linelet: tolerance aspect ratio
        call iexcha(solve_sol(ivari) % kfl_linty)    ! Linelet: linelet type
        !
        ! Renumbered Gauss-Seidel
        !
        call iexcha(solve_sol(ivari) % kfl_renumbered_gs) ! Renumbered Gauss-Seidel permutation
        !
        ! Penalization
        !
        call iexcha(solve_sol(ivari) % kfl_penal)    ! Penalization: method
        call rexcha(solve_sol(ivari) % penal)        ! Penalization: parameter

        call iexcha(solve_sol(ivari) % kfl_scpre)    ! Schur preconditioner solver: skyline
        call iexcha(solve_sol(ivari) % kfl_scaii)    ! Schur matrix solver: skyline

        call iexcha(solve_sol(ivari) % kfl_defpr)    ! Multigrid precond: smoother preconditioner

        call rexcha(solve_sol(ivari) % solco)        ! Solver tolerance
        call rexcha(solve_sol(ivari) % relso)        ! Solver relaxation
        call rexcha(solve_sol(ivari) % dtinv)        ! 1/dt for explicit solver
        call rexcha(solve_sol(ivari) % xdiag)        ! Coefficient for diagonal solver
        call rexcha(solve_sol(ivari) % resin)        ! 
        call rexcha(solve_sol(ivari) % resfi)        ! 
        call rexcha(solve_sol(ivari) % resi2)        ! 
        call rexcha(solve_sol(ivari) % resf2)        ! 
        call rexcha(solve_sol(ivari) % adres)            ! Adaptive residual
        call rexcha(solve_sol(ivari) % solmi)        ! Minimum solver tolerance
        call rexcha(solve_sol(ivari) % xorth)        ! Orthogonality
        call iexcha(solve_sol(ivari) % heade)        ! Header
        call iexcha(solve_sol(ivari) % itsol(1))     ! Max. # of solver iterations
        call iexcha(solve_sol(ivari) % itsol(2))     ! Min. # of solver iterations
        call iexcha(solve_sol(ivari) % itsol(3))     ! Ave. # of solver iterations
        call iexcha(solve_sol(ivari) % nsolv)        ! # of solves
        call iexcha(solve_sol(ivari) % kfl_cvgso)    ! Output Convergence flag
        call iexcha(solve_sol(ivari) % lun_cvgso)    ! Convergence unit
        call iexcha(solve_sol(ivari) % kfl_exres)    ! Exact residual
        call iexcha(solve_sol(ivari) % kfl_solve)    ! Output solve flag
        call iexcha(solve_sol(ivari) % lun_solve)    ! Output unit
        call iexcha(solve_sol(ivari) % kfl_preco)    ! Preconditioner type
        call iexcha(solve_sol(ivari) % kfl_leftr)    ! Preconditioner left or right
        call iexcha(solve_sol(ivari) % itpre)        ! Preconditioner iteration
        call iexcha(solve_sol(ivari) % kfl_marke)    ! Output in market format
        call iexcha(solve_sol(ivari) % kfl_force)    ! Force continuity after solver (in Parallel)
        call iexcha(solve_sol(ivari) % nzpre)        ! Preconditioner # non-null coefficients
        call iexcha(solve_sol(ivari) % lfill)        ! Filling for ILU preconditioning
        call rexcha(solve_sol(ivari) % thres)        ! Threshold for ILU preconditioning
        !call iexcha(solve_sol(ivari) % lpdof)       ! 
        !
        ! Coarse Aii
        !
        call iexcha(solve_sol(ivari) % ngaii)        ! Number of groups
        call iexcha(solve_sol(ivari) % kfl_deaii)    ! Assembly
        !
        ! Block structure
        !
        call iexcha(solve_sol(ivari) % num_blocks) 
        call iexcha(solve_sol(ivari) % block_num) 
        do ii = 1,size(solve_sol(ivari) % block_dimensions)
           call iexcha(solve_sol(ivari) % block_dimensions(ii))
        end do
        call iexcha(solve_sol(ivari) % kfl_react)
        nullify(solve_sol(ivari) % reaction)  
        nullify(solve_sol(ivari) % lpoin_block)
        nullify(solve_sol(ivari) % lpoin_reaction)
        !
        ! Dimension of A3
        !
        call iexcha(solve_sol(ivari) % ndofn_A3)     ! Dimension of A3 matrix         
        !
        ! Characters
        !
        !if(parii==2.and.IMASTER) parch(nparc+1:nparc+30)  = solve_sol(ivari) % wprob(1:30)
        !if(parii==2.and.ISLAVE)  solve_sol(ivari) % wprob(1:30) = parch(nparc+1:nparc+30)
        !nparc=nparc+30
        !if(parii==2.and.IMASTER) parch(nparc+1:nparc+30)  = solve_sol(ivari) % wsolv(1:30)
        !if(parii==2.and.ISLAVE)  solve_sol(ivari) % wsolv(1:30) = parch(nparc+1:nparc+30)
        !nparc=nparc+30
        !if(parii==2.and.IMASTER) parch(nparc+1:nparc+30)  = solve_sol(ivari) % wprec(1:30)
        !if(parii==2.and.ISLAVE)  solve_sol(ivari) % wprec(1:30) = parch(nparc+1:nparc+30)
        !nparc=nparc+30
        !if(nparc>len(parch)) call runend('SOLDEF: TOO MANY CHARACTERS')
     end do

  else if( itask == 4 ) then

     !-------------------------------------------------------------------
     !
     ! Solver dimensions
     !
     !-------------------------------------------------------------------

     do ivari = 1,size(solve_sol)

        if(solve_sol(ivari) % kfl_algso==-999) then

           continue
           !call runend('SOLDEF: NO SOLVER HAS BEEN DEFINED '&
           !     //'FOR PROBLEM '//trim(solve_sol(ivari) % wprob)) 

        else

           if( INOTMASTER ) then

              solve_sol(ivari) % nequa = npoin
              solve_sol(ivari) % nunkn = solve_sol(ivari) % ndofn * solve_sol(ivari) % nequa
              solve_sol(ivari) % ndof2 = solve_sol(ivari) % ndofn**2

              !----------------------------------------------------------
              !
              ! Matrix
              !
              !----------------------------------------------------------

              if( solve_sol(ivari) % kfl_algso == SOL_SOLVER_RICHARDSON ) then
                 !
                 ! Richardson solver
                 !
                 continue

              else
                 !
                 ! Iterative solvers
                 !
                 if( solve_sol(ivari) % kfl_symme == 1 ) then
                    solve_sol(ivari) % nzmat = nzsym*solve_sol(ivari) % ndof2
                 else
                    solve_sol(ivari) % nzmat = nzsol*solve_sol(ivari) % ndof2
                 end if

              end if

              !----------------------------------------------------------
              !
              ! RHS
              !
              !----------------------------------------------------------

              solve_sol(ivari) % nzrhs = solve_sol(ivari) % ndofn*npoin

              !----------------------------------------------------------
              !
              ! Block matrix: dimensions of each block
              !
              !----------------------------------------------------------

              do iblok = 1,solve(1) % num_blocks
                 solve(1) % block_dimensions(iblok) = solve(1+iblok-1) % ndofn
              end do
              if( solve(1) % num_blocks == 1 ) then
                 solve(1) % block_array(1) % bvess     => solve(1) % bvess
                 solve(1) % block_array(1) % kfl_fixno => solve(1) % kfl_fixno
                 solve(1) % block_array(1) % bvnat     => solve(1) % bvnat
              end if

              !----------------------------------------------------------
              !
              ! Preconditioning
              !
              !----------------------------------------------------------

              if( solve_sol(ivari) % kfl_preco == SOL_MATRIX ) then
                 !
                 ! Matrix preconditioning
                 !
                 solve_sol(ivari) % nzpre = solve_sol(ivari) % ndof2*nzsol

              else if( solve_sol(ivari) % kfl_preco == SOL_MASS_MATRIX .or. solve_sol(ivari) % kfl_preco == SOL_CLOSE_MASS_MATRIX ) then
                 !
                 ! Diagonal mass
                 !
                 continue

              else if( solve_sol(ivari) % kfl_preco == SOL_LOCAL_DIAGONAL .or. solve_sol(ivari) % kfl_preco == SOL_AUGMENTED_DIAGONAL ) then
                 !
                 ! Diagonal preconditioning for Richardson iterations
                 !
                 solve_sol(ivari) % nzpre = solve_sol(ivari) % ndofn*npoin

              end if

              !----------------------------------------------------------
              !
              ! Block Gauss-Seidel
              !
              !----------------------------------------------------------

              if( solve_sol(1) % kfl_blogs == 1 .and. solve_sol(1) % ndofn > 1 ) then
                 !solve_sol(1) % nblok = abs(solve_sol(1) % nblok)
                 ! Check that ndofn / nblok is an integer
                 if (solve_sol(1) % nblok /= 0) then
                    if (mod(solve_sol(1) % ndofn,solve_sol(1) % nblok)/=0) then
                       call runend('SOLDEF: NDOF/NBLOK NOT AN INTEGER: WRONG NUMBER OF DOF PER BLOCK IN BGS')
                    end if
                 end if
 
                 if( solve_sol(1) % nblok == 0 ) then
                    !
                    ! One block per dof
                    !
                    solve_sol(1) % nblok = solve_sol(1) % ndofn
                    solve_sol(ivari) % ndofn_per_block = solve_sol(1) % ndofn / solve_sol(1) % nblok
                 else if( solve_sol(1) % nblok > 0 ) then
                    !
                    ! Contigous permutation
                    !
                    solve_sol(ivari) % ndofn_per_block = solve_sol(1) % ndofn / solve_sol(1) % nblok
                 else if( solve_sol(1) % nblok < 0 ) then
                    !
                    ! Uncontigous permutation
                    !
                    solve_sol(1) % ndofn_per_block = abs(solve_sol(1) % ndofn / solve_sol(1) % nblok)
                    allocate( solve_sol(1) % lperm_block(2,ndime*npoin) )
                    ndofn_per_block = -solve_sol(1) % ndofn / solve_sol(1) % nblok
                    allocate( solve_sol(1) % linvp_block(ndofn_per_block,nblok) )
                    

                    ! lperm_block
                    do iblok=1,abs(solve_sol(1) % nblok)
                       cont = 0
                       do ipoin=1,npoin
                          do idime=1,ndime
                             pos = (ipoin-1)*ndime + idime
                             solve_sol(1) % lperm_block(iblok,pos) = idime + 2*cont*ndime + 2*(iblok-1)
                          end do
                          cont = cont+1
                       end do
                    end do
                   
                    ! linvp_block            
                 end if
              end if
              !
              ! Size of matrices and RHS 
              !   
              if( solve_sol(ivari) % kfl_cmplx == 0 ) then
                 nzmat = max( nzmat, solve_sol(ivari) % nzmat * solve_sol(ivari) % nseqn )
                 nzrhs = max( nzrhs, solve_sol(ivari) % nzrhs * solve_sol(ivari) % nseqn * solve_sol(ivari) % nrhss )
                 nzpre = max( nzpre, solve_sol(ivari) % nzpre * solve_sol(ivari) % nseqn )
              else
                 nzmax = max( nzmax , solve_sol(ivari) % nzmat * solve_sol(ivari) % nseqn )
                 nzrhx = max( nzrhx , solve_sol(ivari) % nzrhs * solve_sol(ivari) % nseqn * solve_sol(ivari) % nrhss )
                 nzprx = max( nzprx , solve_sol(ivari) % nzpre * solve_sol(ivari) % nseqn )              
              end if
              !
              ! Matrix has a block structure
              !
!!$              if( solve_sol(ivari) % block_number == 1 ) then
!!$                 !solve_sol(ivari) % block_pointers(1,1) = 1
!!$              else
!!$              end if
!!$                 last_pointer = 1 
!!$                 kblok = 0
!!$                 allocate( block_size(solve_sol(ivari) % block_number * solve_sol(ivari) % block_number) )
!!$                 do iblok = 1,solve_sol(ivari) % block_number
!!$                    do jblok = 1,solve_sol(ivari) % block_number       
!!$                       kblok = kblok + 1
!!$                       block_size(kblok) = &
!!$                            solve_sol(ivari) % block_dimensions(iblok) * solve_sol(ivari) % block_dimensions(jblok)
!!$                    end do
!!$                 end do
!!$                 ! block_poinetrs(:,:) or block_poinetrs(:) ??? 
!!$                 do iblok = 1,solve_sol(ivari) % block_number
!!$                    do jblok = 1,solve_sol(ivari) % block_number       
!!$                       if( iblok == 1 .and. jblok == 1 ) then
!!$                          
!!$                       else
!!$
!!$                       end if
!!$                    end do
!!$                 end do
!!$                 deallocate( block_size ) 
!!$              end if

           end if
           !
           ! Name of solver 
           !
           if( solve_sol(ivari) % kfl_algso ==  0 ) solve_sol(ivari) % wsolv = 'DIRECT LDU'                    
           if( solve_sol(ivari) % kfl_algso ==  0 ) solve_sol(ivari) % wsolv = 'DIRECT LDU'                    
           if( solve_sol(ivari) % kfl_algso == -3 ) solve_sol(ivari) % wsolv = 'PASTIX - SPARSE DIRECT'         
           if( solve_sol(ivari) % kfl_algso == -1 ) solve_sol(ivari) % wsolv = 'MUMPS - SPARSE DIRECT'         
           if( solve_sol(ivari) % kfl_algso ==  1 ) then
              if( solve_sol(ivari) % kfl_schur == 0 ) then 
                 solve_sol(ivari) % wsolv = 'CONJUGATE GRADIENT'
              else
                 solve_sol(ivari) % wsolv = 'CONJUGATE GRADIENT FOR SCHUR COMPLEMENT'                 
              end if
           end if
           if( solve_sol(ivari) % kfl_algso ==  2 ) solve_sol(ivari) % wsolv = 'DEFLATED CONJUGATE GRADIENT'   
           if( solve_sol(ivari) % kfl_algso ==  5 ) solve_sol(ivari) % wsolv = 'BiCGSTAB'                   
           if( solve_sol(ivari) % kfl_algso ==  8 ) solve_sol(ivari) % wsolv = 'GMRES'                         
           if( solve_sol(ivari) % kfl_algso ==  9 ) solve_sol(ivari) % wsolv = 'RICHARDSON'          
           if( solve_sol(ivari) % kfl_algso == 10 ) solve_sol(ivari) % wsolv = 'MATRIX_BASED_RICHARDSON'          
           if( solve_sol(ivari) % kfl_algso == 12 ) solve_sol(ivari) % wsolv = 'DEFLATED BiCGSTAB'          
           if( solve_sol(ivari) % kfl_algso == 13 ) solve_sol(ivari) % wsolv = 'DEFLATED GMRES'          
           if( solve_sol(ivari) % kfl_algso == 14 ) solve_sol(ivari) % wsolv = 'SPARSE DIRECT SOLVER'          
           if( solve_sol(ivari) % kfl_algso == 15 ) solve_sol(ivari) % wsolv = 'STEEPEST DESCENT'          
           if( solve_sol(ivari) % kfl_algso == 18 ) solve_sol(ivari) % wsolv = 'PIPELINED CONJUGATE GRADIENT'          
           if( solve_sol(ivari) % kfl_algso == 19 ) solve_sol(ivari) % wsolv = 'PIPELINED DEFLATED CONJUGATE GRADIENT'   
           !
           ! Name of preconditioner
           !
           if( solve_sol(ivari) % kfl_preco == SOL_NO_PRECOND )         solve_sol(ivari) % wprec = 'NONE'
           if( solve_sol(ivari) % kfl_preco == SOL_SQUARE )             solve_sol(ivari) % wprec = 'SYMMETRIC: sqrt[|diag(A)|]'
           if( solve_sol(ivari) % kfl_preco == SOL_DIAGONAL )           solve_sol(ivari) % wprec = 'DIAGONAL diag(A)'
           if( solve_sol(ivari) % kfl_preco == SOL_MATRIX )             solve_sol(ivari) % wprec = 'MATRIX'
           if( solve_sol(ivari) % kfl_preco == SOL_LINELET )            solve_sol(ivari) % wprec = 'LINELET'
           if( solve_sol(ivari) % kfl_preco == SOL_MASS_MATRIX )        solve_sol(ivari) % wprec = 'MASS MATRIX (DIAGONAL)'
           if( solve_sol(ivari) % kfl_preco == SOL_GAUSS_SEIDEL ) then
              if( solve_sol(ivari) % kfl_renumbered_gs == -1 )          solve_sol(ivari) % wprec = 'SYMMETRIC GAUSS-SEIDEL'
              if( solve_sol(ivari) % kfl_renumbered_gs ==  1 )          solve_sol(ivari) % wprec = 'STREAMWISE GAUSS-SEIDEL'
              if( solve_sol(ivari) % kfl_renumbered_gs ==  0 )          solve_sol(ivari) % wprec = 'GAUSS-SEIDEL'
           end if
           if( solve_sol(ivari) % kfl_preco == SOL_CLOSE_MASS_MATRIX )  solve_sol(ivari) % wprec = 'CLOSE MASS MATRIX'
           if( solve_sol(ivari) % kfl_preco == SOL_RICHARDSON )         solve_sol(ivari) % wprec = 'RICHARDSON'
           if( solve_sol(ivari) % kfl_preco == SOL_ORTHOMIN )           solve_sol(ivari) % wprec = 'ORTHOMIN(1)' 
           if( solve_sol(ivari) % kfl_preco == SOL_APPROXIMATE_SCHUR)   solve_sol(ivari) % wprec = 'APPROXIMATE SCHUR=ABB-ABI diag(AII)^{-1} AIB'
           if( solve_sol(ivari) % kfl_preco == SOL_ABB )                solve_sol(ivari) % wprec = 'ABB'
           if( solve_sol(ivari) % kfl_preco == SOL_MOD_DIAGONAL )       solve_sol(ivari) % wprec = 'diag(ABB) - diag( ABI diag(AII)^{-1} AIB)'
           if( solve_sol(ivari) % kfl_preco == SOL_AII )                solve_sol(ivari) % wprec = 'AII^{-1}'
           if( solve_sol(ivari) % kfl_preco == SOL_MULTIGRID )          solve_sol(ivari) % wprec = 'MULTIGRID DEFLATED'
           if( solve_sol(ivari) % kfl_preco == SOL_LOCAL_DIAGONAL )     solve_sol(ivari) % wprec = 'LOCAL DIAGONAL'
           if( solve_sol(ivari) % kfl_preco == SOL_AUGMENTED_DIAGONAL ) solve_sol(ivari) % wprec = 'AUGMENTED DIAGONAL P: (M/dt+P) u = M/dt u+b'
           if( solve_sol(ivari) % kfl_preco == SOL_RAS )                solve_sol(ivari) % wprec = 'RESTRICTIVE ADDITVE SCHWARZ (RAS)'           
           !
           ! Complex solver
           !
           if( solve_sol(ivari) % kfl_cmplx == 1 ) &
                solve_sol(ivari) % wsolv = 'COMPLEX '//trim(solve_sol(ivari) % wsolv)
           !
           ! Zone not defined yet
           !
           if( solve_sol(ivari) % kfl_zones == 0 ) solve_sol(ivari) % kfl_zones = lzone(modul)

        end if
     end do

  end if

end subroutine soldef

subroutine solcpy(ipro1,ipro2)
  use def_master 
  use def_solver
  implicit none  
  integer(ip),  intent(in) :: ipro1,ipro2
  type(soltyp)             :: solve_tmp
  !
  ! Copy solver
  !
  solve_tmp        = solve_sol(ipro2)
  solve_sol(ipro2) = solve_sol(ipro1)
  !
  ! Recover selected orginal values
  !
  solve_sol(ipro2) % wprob = solve_tmp%wprob
  
end subroutine solcpy
