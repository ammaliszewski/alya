subroutine Turnof
  !-----------------------------------------------------------------------
  !****f* master/Turnof
  ! NAME
  !    Turnof
  ! DESCRIPTION
  !    This subroutine stops the run.
  ! USES
  !    Nastin
  !    Temper
  !    Codire
  !    Turbul
  !    Exmedi
  !    Nastal
  !    Alefor
  !    Latbol
  !    Solidz
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  implicit none
  !--------------------------------------
  integer(ip)             :: ipart
  character(150)          :: TMPDIR,my_tmpdir,nunam_pos1,nunam_pos2,nunam_pos3
  integer(ip)             :: values(8),auxi1,auxi2,auxi3,auxi_tar,auxi_cptar,auxi
  integer(ip)             :: auxi4,auxi5,auxi_targz,auxi_cptargz
  !----------------------------------------------

#ifdef EVENT_ITERATION
  call mpitrace_eventandcounters(123456_4,0_4)
#endif
  !
  ! Live information
  !
  call livinf(10_ip,' ',0_ip)
  !
  ! Writes memory used
  !
  call outmem()
  !
  ! Write CPU time heading and master's CPU time
  !
  call outcpu()
  !
  ! Coupling CPU
  !
  call cou_outcpu()
  !
  ! Write latex info file
  !
  call outlat(2_ip)
  !
  ! Turn off modules
  !
  call moduls(ITASK_TURNOF)
  !
  ! Write info about modules and close module files
  !
  call moddef(4_ip)
  !
  ! End-up latex file
  !
  call outlat(3_ip)
  ! 
  ! Turn off Services
  !
  call Hdfpos(10_ip)        ! HDFPOS
  call Parall( 7_ip)        ! PARALL
  call Dodeme(12_ip)        ! DODEME
  !
  ! moving vtk data 
  !
  !call vtkmov(1_ip)
  !
  ! Close postprocess files
  !
  call openfi(4_ip)
  call openfi(6_ip)
  !
  ! Deallocate memory
  !
  call deaker()
  !
  !call finalize coprocessing
  !
#ifdef CATA
  call livinf(204_ip,' ',0_ip)  
  call coprocessorfinalize()
#endif
  !
  ! Stop the run
  !
  call runend('O.K.!')
  !
end subroutine Turnof
