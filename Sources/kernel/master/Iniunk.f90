subroutine Iniunk()
  !-----------------------------------------------------------------------
  !****f* master/Iniunk
  ! NAME
  !    Iniunk
  ! DESCRIPTION
  !    This routine ask the modules to compute their initial condition
  ! USES
  !    moduls
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_master
  use mod_coupling_driver, only :  COU_DRIVER
  implicit none
  !
  ! Call modules
  !
  modul = ID_KERMOD
  call COU_DRIVER(ITASK_BEFORE,ITASK_INIUNK)
  call Kermod(-ITASK_INIUNK)
  modul = ID_KERMOD
  call COU_DRIVER(ITASK_AFTER,ITASK_INIUNK)
  modul = 0 

  call livinf(75_ip,' ',0_ip)
  do iblok = 1,nblok
     call moduls(ITASK_INIUNK)
  end do
  call livinf(76_ip,' ',0_ip)
  !
  ! No time step is performed
  !
  if( mitim == 0 )  then
     kfl_gotim = 0
     cutim     = 0.0_rp
  end if

end subroutine Iniunk
