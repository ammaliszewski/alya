subroutine deaker()
  !-----------------------------------------------------------------------
  !****f* master/deaker
  ! NAME
  !    deaker
  ! DESCRIPTION
  !    This subroutine deallocates memory
  ! USES
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  implicit none
  !
  ! TO DO
  !
  return

  call cshder(4_ip)
  call cshder(2_ip)

end subroutine deaker
