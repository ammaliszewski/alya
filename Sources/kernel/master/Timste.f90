!-----------------------------------------------------------------------
!> @addtogroup Timste 
!> @{
!> @file    Timste.f90
!> @author  Guillaume Houzeaux
!> @brief   Compute time step
!> @details Compute time step:
!!          - Compute the time step for each module
!!          - Compute the global time step 
!!
!> @} 
!-----------------------------------------------------------------------
subroutine Timste()

  use def_master
  use def_parame
  implicit none

#ifdef EVENT_ITERATION
  if( ittim == 0 ) then
     call mpitrace_set_options(63)
  else
     call mpitrace_set_options(55)
  end if
  call mpitrace_eventandcounters(123456_4,int(ittim,4)+1_4)
#endif
  !
  ! Live information
  !
  call livinf(4_ip,' ',zero)
  !
  ! Initializations
  !
  call iniste(1_ip)
  !
  ! Compute the time step for each module
  !           
  call moduls(ITASK_TIMSTE)
  !
  ! Initializations
  !
  call iniste(2_ip)
  !
  ! Computes the global time step 
  !
  call setgts(2_ip)
  !
  ! Live information
  !
  call livinf(18_ip,' ',zero)

end subroutine Timste
