subroutine timfun(itask)
  !------------------------------------------------------------------------
  !****f* master/timfun
  ! NAME 
  !    timfun
  ! DESCRIPTION
  !    This routine computes the time step
  ! OUTPUT
  ! USES
  ! USED BY
  !    setgts
  !***
  !------------------------------------------------------------------------
  use def_master
  implicit none
  integer(ip), intent(in) :: itask

  if(kfl_timef/=0) then

     select case(kfl_timef)

     case(1)
        !
        ! Marek 
        !
        if(cutim<535.0_rp) then
           dtime=(397.91_rp*cutim+2435.9_rp)/10000.0_rp
        else
           dtime=(0.1513_rp*(cutim*cutim)-381.18_rp*cutim+367561.0_rp)/10000.0_rp
        end if

     case(2)
        !
        ! Marek 
        !
        if(cutim<375.84_rp) then
           dtime=(397.91_rp*cutim+2435.9_rp)/10000.0_rp
        else
           dtime=15.0_rp
        end if

     case(3)
        !
        ! Temporal test for catamaran, actually it would me much flexible to give a table in the .dat file 
        !
        if(cutim<(80.0_rp*0.005_rp)) then
           dtime=0.005_rp
        else
           dtime=0.02_rp
        end if

     end select

     if(itask==2.and.dtime>0.0_rp) dtinv = 1.0_rp/dtime

  end if

end subroutine timfun
