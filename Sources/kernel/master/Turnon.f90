!-----------------------------------------------------------------------
!> @addtogroup Turnon
!> @{
!> @file    Turnon.f90
!> @author  Guillaume Houzeaux
!> @brief   Turnon the run
!> @details Initial operatons: read general data, mesh and modules data
!!          Construct the mesh dependent arrays.
!!
!> @} 
!-----------------------------------------------------------------------
subroutine Turnon

  use def_parame
  use def_elmtyp
  use def_master
  use def_inpout
  use def_domain
  use def_kermod
  implicit none
  integer(4) :: count_rate
  real(rp)   :: time1,time2,time3,time4

  !---------------------------------------------------------------------------------
  !
  ! Start run, creates code communicators in case of coupling, and read problem data
  !
  !---------------------------------------------------------------------------------
  !
  ! Compute time rate
  !
  call system_clock(count_rate=count_rate)
  rate_time = 1.0_rp / max(real(count_rate,rp),zeror)
  ! 
  ! Splits the MPI_COMM_WORLD for coupling with other codes. This defines the Alya world
  !
  call par_code_split_universe()
  ! 
  ! Splits the MPI_COMM_WORLD for coupling
  !
  call par_code_split_world()   
  !
  ! Read problem data
  !
  call reapro()
  !
  ! Initialize time counters
  !
  call setgts(1_ip)
  ! 
  ! Initializations
  !
  call inidom()
  ! 
  ! Define element types
  !
  call elmtyp()                            ! Element lists: NNODE, LTOPO, LDIME, LLAPL...
  !
  ! Open module data files
  !
  call moddef(1_ip)
  !
  ! Exceptional things with modules (IMMBOU)
  !
  call excmod(2_ip)
  !
  ! Output optimization options
  !
  call outfor(73_ip,0_ip,' ') 

  call livinf(11_ip,' ',zero)

  !----------------------------------------------------------------------
  !
  ! Read domain data
  !
  !----------------------------------------------------------------------

  call cputim(time1)
  call readom()  
  call cputim(time2)
  cpu_start(CPU_READ_DOMAIN) = time2 - time1

  !----------------------------------------------------------------------
  !
  ! Read kermod data
  !
  !----------------------------------------------------------------------
  
  call Kermod(-ITASK_TURNON)

  !----------------------------------------------------------------------
  !
  ! Create domain data
  !
  !----------------------------------------------------------------------

  call domain()
  call cputim(time1)

  !----------------------------------------------------------------------
  !
  ! Others
  !
  !----------------------------------------------------------------------

  call Adapti(ITASK_TURNON)
  !call Optsol(ITASK_TURNON)  

  call livinf(13_ip,' ',zero)

  !----------------------------------------------------------------------
  !
  ! Read module data
  !
  !----------------------------------------------------------------------

  call livinf( 3_ip,'NULL',zero)
  call moduls(ITASK_TURNON)
  !
  ! Parallel stuffs: send some data computed from modules data
  !
  call Parall(48_ip)
  call livinf(52_ip,'NULL',zero)

  !----------------------------------------------------------------------
  !
  ! Required arrays depending on modules data
  !
  !----------------------------------------------------------------------
  !
  ! List of required arrays: 
  !
  call reqarr()
  !
  ! PELPO_2, LELPO_2, PELEL_2, LELEL_2 extended Graphs
  !
  call lelpo2()
  !
  ! LFCNT, LNCNT: Identify faces of contact elements
  !
  call cntelm()
  !
  ! ZDOM_*, R_DOM_*, C_DOM:*. Schur type solvers and Aii preconditioners. * = Aii, Aib, Abi, Abb
  !
  call solpre()
  !
  ! R_SYM, C_SYM: Symmetric graph if necessary
  !
  call symgra()
  !
  ! CMASS: Consistent mass matrix
  !
  call conmas()
  !
  ! LELBF, LELFA: Global face graph and element boundary face
  !
  call lgface()
  !
  ! Hanging nodes
  !
  call hangin()
  !
  ! Element bin for neighboring elements
  !
  call elebin()

  !----------------------------------------------------------------------
  !
  ! Others
  !
  !----------------------------------------------------------------------
  !
  ! Allocate memory for filters and check them
  !
  call fildef(-1_ip)
  ! 
  ! Allocate memory for all unknowns of the problem and coefficients
  !
  call cputim(time2)
  !call Optsol(ITASK_NDVARS)
  call memunk(1_ip)
  !call Optsol(ITASK_ADJMEM)
  call Optsol(ITASK_TURNON)  
  !
  ! Check warnings and errors
  !
  call outerr(1_ip)
  !
  ! Read restart file
  !
  call restar(1_ip)
  !
  ! Initialization of some variables
  !
  call inivar(2_ip) 
  !
  ! Close module data files and open occasional output files
  !
  call moddef(2_ip)
  !
  ! Close data file/open occasional files
  !
  call openfi(3_ip)
  !
  ! Support geometry
  !
  call ker_submsh() 
  !
  ! Write information
  !
  call outinf()
  call outlat(1_ip)
  !
  ! Transient fields
  !
  call calc_kx_tran_fiel()

  call cputim(time2)
  cpu_start(CPU_ADDTIONAL_ARRAYS) = time2 - time1
  !
  !call initialize coprocessing
  !
#ifdef CATA
  call coprocessorinitializewithpython("new.py",9)
  call livinf(202_ip,' ',0_ip)  
#endif
  !
end subroutine Turnon
