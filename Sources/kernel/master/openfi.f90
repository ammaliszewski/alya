subroutine openfi(itask)
  !------------------------------------------------------------------------
  !****f* master/openfi
  ! NAME 
  !    openfi
  ! DESCRIPTION
  !    This subroutine gets ALL the file names to be used by Alya in two
  !    possible ways, opens or closes them:
  ! 
  !    1. Recalling them from the environment, when Alya is launched
  !    encapsulated in a shell script, or
  ! 
  !    2. Composing the names out of the problem name which is given as
  !     argument when the binary file Alya is launched "naked". 
  !
  ! OUTPUT
  ! USES
  ! USED BY
  !    Turnon
  !    Turnof
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_postpr
  use mod_iofile
  use mod_memory,        only : lun_memor
  use mod_ker_detection, only : ker_detection_open_file
  implicit none
#ifdef DOS
  integer(4),  external   :: GETPID
#else
  integer(4)              :: GETPID
#endif
  integer(ip), intent(in) :: itask
  integer(4)              :: one4=1,pid
  character(150),save     :: fil_outpu,fil_memor,fil_livei,fil_commu
  character(150),save     :: fil_latex,fil_gnupl,fil_syste

  if( INOTSLAVE ) then

     if( itask == 1 ) then
        !
        ! Open input data file: Get file names
        !
        call GETARG(one4,namda)
        if(len(trim(namda))>0) then
           if(      trim(namda)=='--h'.or.trim(namda)=='--help'&
                .or.trim(namda)=='-h' .or.trim(namda)=='-help') then
              call outhel
           else
              kfl_naked=1
           end if
        else
           kfl_naked=0
           call GETENV('ALYA_NAME',namda)
           namda=trim(namda)
        end if
        !
        ! Open data file
        !     
        if (kfl_naked==0) then
           call GETENV('FOR011',fil_pdata)
        else
           if(adjustl(trim(namda))=='connard') then
              call runend('CONNARD TOI-MEME')
           end if
           fil_pdata = adjustl(trim(namda))//'.dat'
        end if
        call iofile(zero,lun_pdata,fil_pdata,'DATA','old')

     else if( itask == 2 ) then
        !
        ! Open output files
        !
        if ( kfl_naked == 0 ) then
           call GETENV('FOR012',fil_outpu) 
           call GETENV('FOR013',fil_memor) 
           call GETENV('FOR014',fil_conve) 
           call GETENV('FOR015',fil_postp) 
           call GETENV('FOR016',fil_livei)    
           call GETENV('FOR017',fil_rstar)    
           call GETENV('FOR018',fil_latex)    
           call GETENV('FOR019',fil_gnupl)
           call GETENV('FOR020',fil_commu)
           call GETENV('FOR024',fil_binar)
           call GETENV('FOR028',fil_syste)
           call GETENV('FOR034',fil_posbs) 
           call GETENV('FOR035',fil_quali) 
           call GETENV('FOR045',fil_rstib)    
           !call GETENV('FOR033',fil_detec)    
           !call GETENV('FOR039',fil_rstla) 
           !call GETENV('FOR040',fil_posla)
           !call GETENV('FOR041',fil_cvgla)
        else if ( kfl_naked == 1 ) then
           fil_outpu = adjustl(trim(namda))//'.log'              ! Unit 12
           fil_memor = adjustl(trim(namda))//'.mem'              ! Unit 13
           fil_conve = adjustl(trim(namda))//'.cvg'              ! Unit 14
           fil_livei = adjustl(trim(namda))//'.liv'              ! Unit 16
           fil_postp = adjustl(trim(namda))                      ! Unit 15
           fil_rstar = adjustl(trim(namda))//'.rst'              ! Unit 17
           fil_latex = adjustl(trim(namda))//'-latex.tex'        ! Unit 18
           fil_gnupl = adjustl(trim(namda))//'-latex.plt'        ! Unit 19
           fil_commu = adjustl(trim(namda))//'.com'              ! Unit 20
           fil_binar = adjustl(trim(namda))//'.dom.bin'          ! Unit 24
           fil_syste = adjustl(trim(namda))//'-system.log'       ! Unit 28
           fil_posbs = adjustl(trim(namda))                      ! Unit?
           fil_quali = adjustl(trim(namda))//'-mesh-quality.res' ! Unit 35
           fil_pospl = adjustl(trim(namda))                      ! Unit?
           fil_rstib = adjustl(trim(namda))//'-IB.rst'           ! Unit?
           !fil_rstla = adjustl(trim(namda))//'-lagrangian.rst'
           !fil_posla = adjustl(trim(namda))//'-lagrangian.res'
           !fil_cvgla = adjustl(trim(namda))//'-lagrangian.cvg'
        end if
        !
        ! Open permanent files
        !
        if( kfl_rstar == 2 ) then
           call iofile(zero,lun_outpu,fil_outpu,'RUN EVOLUTION',      'old','formatted','append')
           call iofile(zero,lun_conve,fil_conve,'CONVERGENCE HISTORY','old','formatted','append')
           call iofile(zero,lun_syste,fil_syste,'SYSTEM INFO',        'old','formatted','append')
        else
           call iofile(zero,lun_outpu,fil_outpu,'RUN EVOLUTION')
           call iofile(zero,lun_conve,fil_conve,'CONVERGENCE HISTORY')
           call iofile(zero,lun_syste,fil_syste,'SYSTEM INFO')
        end if
        !
        ! Open Postprocess file
        !
        if( kfl_postp_par == 1 ) then
           !
           ! Compose mesh and result file names
           !
           call outres()

        end if
        !
        ! Open live file
        !
        if(lun_livei==16) then
           if(kfl_rstar==2) then
              call iofile(zero,lun_livei,fil_livei,'LIVE INFORMATION',   'old','formatted','append')
           else
              call iofile(zero,lun_livei,fil_livei,'LIVE INFORMATION','unknown','formatted')
           end if
           call livinf(-1_ip,' ',zero)
           call livinf(-2_ip,' ',zero)
        end if
        !
        ! Open memory file
        !
        if( kfl_memor == 1 ) then
           if( kfl_rstar == 2 ) then
              call iofile(zero,lun_memor,fil_memor,'MEMORY EVOLUTION',   'old','formatted','append')
           else
              call iofile(zero,lun_memor,fil_memor,'MEMORY EVOLUTION')
           end if
        end if
        !
        ! Open latex file
        ! 
        if( kfl_latex == 1 ) then
           call iofile(zero,lun_latex,fil_latex,'LATEX')
           call iofile(zero,lun_gnupl,fil_gnupl,'GNUPLOT')
        end if
        !
        ! Open communication-with-Alya file
        !
        if( kfl_commu == 1 ) then
           call iofile(zero,lun_commu,fil_commu,'COMMUNICATION')
           write(lun_commu,'(a)') 'WRITE HERE YOUR MESSAGE TO ALYA'
        end if
        !
        ! Write header
        !
        call outfor(16_ip,lun_outpu,' ')
        call outfor(16_ip,lun_syste,' ')

     else if( itask == 3 ) then
        !
        ! Close input data file
        !
        call iofile(two,lun_pdata,' ','DATA')   
        !
        ! Event detection file
        !
        call ker_detection_open_file

     else if( itask == 7 ) then
        !
        ! Open geometry binary file
        !
        call iofile(zero,lun_binar,fil_binar,'GEOMETRY BIN','unknown','unformatted')

     else if( itask == 8 ) then
        !
        ! Close geometry binary file
        !
        call iofile(two,lun_binar,' ','GEOMETRY BIN') 

     else if( itask == 13 ) then
        !
        ! Open Mesh quality file
        ! 
        if( kfl_quali /= 0 ) then
           call iofile(zero,lun_quali,fil_quali,'MESH QUALITY')
        end if

     else if( itask == -13 ) then
        !
        ! Open Mesh quality file
        ! 
        if( kfl_quali /= 0 ) then
           call iofile(two,lun_quali,fil_quali,'MESH QUALITY')
        end if

     end if

  end if


end subroutine openfi
