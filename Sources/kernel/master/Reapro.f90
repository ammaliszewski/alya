subroutine reapro()
  !-----------------------------------------------------------------------
  !****f* master/reapro
  ! NAME
  !    Reapro
  ! DESCRIPTION
  !    This routine starts reading data.
  ! USES
  !    inirun   to perform some initializations.
  !    openfi   to get file names and open them.
  !    rrudat   to read run data.
  !    rproda   to read general problem data.
  !    cputim
  !    Nastin
  !    Temper
  !    Codire
  !    Alefor
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  implicit none
  !
  ! Initializations
  !
  call inirun()
  !
  ! Check if process was initiated by MPI
  !
  call Parall(-1_ip)  
  !
  ! Get data file name and open it
  !
  call openfi(1_ip)
  !
  ! Read run data
  !
  call rrudat()
  !
  ! Live information
  !
  call livinf(1_ip,' ',zero)
  !
  ! Checkpoint for OpenMP
  !
  call iniomp()
  !
  ! Checkpoint for Parall communication
  !
  call Parall(-2_ip)  
  !
  ! Get result file names and open them
  !
  call livinf(2_ip,' ',zero) 
  call openfi(2_ip)
  !
  ! Read general problem data
  !
  call readat()
  !
  ! Modules: read data 
  ! 
  do modul = 1,mmodu
     call reamod()
  end do
  modul = 0
  !
  ! Services: read data 
  !
  call Dodeme(0_ip)
  call Hdfpos(0_ip) 
  call Optsol(ITASK_REAPRO)
  call Parall(0_ip)   
  call Adapti(ITASK_REAPRO)
  
  !PARMETIS init partitioning workers
#ifdef PARMETIS
  call par_initia_part()
#endif

  !
  ! Block ordering 
  !
  call modser()
  !
  ! Initial variable
  !
  call inivar(1_ip) 
  !
  ! Check errors
  !
  call outerr(0_ip)

end subroutine reapro
