subroutine solver(rhsid,unkno,amatr,pmatr)
  !-----------------------------------------------------------------------
  !****f* master/solver
  ! NAME 
  !    solver
  ! DESCRIPTION
  !    This routine calls the solvers
  !    For diagonal solve which uses vmass, amatr must NOT be modified
  !
  !    About residual RHSID:
  !
  !    solve_sol(1) % kfl_recov = 0 ... Do not recover local residual
  !                           = 1 ... Recover local residual
  !                           = 2 ... Residual is already global
  !
  !    About solve tolerance SOLCO:
  !
  !    solve_sol(1) % kfl_adres = 0 ... Solver tolerance is given by user
  !                           = 1 ... Solver tolerance is adaptive
  !
  !    CPU times computed:
  !    SOLVE_SOL(1) % CPUTI(1) ... All operations
  !    SOLVE_SOL(1) % CPUTI(2) ... Initial operations
  !    SOLVE_SOL(1) % CPUTI(3) ... Preconditioning
  !    SOLVE_SOL(1) % CPUTI(4) ... Coarse system solver for DCG
  !    SOLVE_SOL(1) % CPUTI(5) ... Orthonormalization for GMRES
  !
  ! USES
  !    memchk
  !    mediso
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_kintyp,         only :  ip,rp
  use def_master,         only :  NPOIN_REAL_12DI,INOTMASTER,ISLAVE
  use def_master,         only :  kfl_paral,kfl_timin,NPOIN_TYPE,nul1r
  use def_master,         only :  lninv_loc,intost
  use def_domain,         only :  npoin,c_dom,r_dom,c_sym,r_sym
  use def_solver,         only :  solve_sol,memma,cpu_solve
  use def_solver,         only :  SOL_SOLVER_RICHARDSON  
  use def_solver,         only :  SOL_SOLVER_PASTIX
  use def_solver,         only :  SOL_SOLVER_MUMPS
  use mod_couplings,      only :  COU_PRESCRIBE_DIRICHLET_IN_MATRIX
  use mod_communications, only :  PAR_INTERFACE_NODE_EXCHANGE
  use mod_matrix,         only :  matrix_blokgs
  use mod_memory,         only :  memory_alloca
  use mod_memory,         only :  memory_deallo
  use mod_solver,         only :  solver_periodicity
  use mod_solver,         only :  solver_pastix
  use mod_iofile,         only :  iofile
  implicit none
  real(rp),    intent(inout)   :: unkno(*)
  real(rp),    intent(inout)   :: amatr(*)
  real(rp),    intent(in)      :: pmatr(*)
  real(rp),    intent(inout)   :: rhsid(*)
  real(rp)                     :: time1,time2
  integer(ip)                  :: icomp,izdom,ipoin,jpoin
  integer(ip)                  :: iblok,nblok,jzdom
  real(rp)                     :: dummr
  real(rp),    pointer         :: rhscp(:)
  real(rp),    pointer         :: aa_idofn(:)     ! Block Gauss-Seidel
  real(rp),    pointer         :: xx_idofn(:)     ! Block Gauss-Seidel
  real(rp),    pointer         :: bb_idofn(:)     ! Block Gauss-Seidel
  integer(ip)                  :: auxdof          ! Auxiliar variable to recober the amoung of original dof
  character(150)               :: file_market_aa
  character(150)               :: file_market_bb
  character(150)               :: file_market_permu

  nullify(aa_idofn) 
  nullify(xx_idofn) 
  nullify(bb_idofn) 

  !----------------------------------------------------------------------
  !
  ! Periodicity and Dirichlet bc.
  !
  !----------------------------------------------------------------------

  if( solve_sol(1) % kfl_version == 0 ) then
     if( solve_sol(1) % kfl_algso == SOL_SOLVER_RICHARDSON ) then
        call presol(4_ip,solve_sol(1) % ndofn,solve_sol(1) % ndofn,rhsid,amatr,unkno)
     else
        call presol(1_ip,solve_sol(1) % ndofn,solve_sol(1) % ndofn,rhsid,amatr,unkno)
     end if
  end if

  !----------------------------------------------------------------------
  !
  ! Coupling with dirichlet
  !
  !----------------------------------------------------------------------

  call COU_PRESCRIBE_DIRICHLET_IN_MATRIX(solve_sol(1) % ndofn,npoin,r_dom,c_dom,amatr,rhsid,unkno)

  !----------------------------------------------------------------------
  !
  ! Limiting step
  !
  !----------------------------------------------------------------------

  if( solve_sol(1) % kfl_symme == 0 ) then
     call limite(solve_sol(1) % ndofn,r_dom,c_dom,amatr)
  else
     call limite(solve_sol(1) % ndofn,r_sym,c_sym,amatr)     
  end if

  !----------------------------------------------------------------------
  !
  ! Others
  !
  !----------------------------------------------------------------------
  !
  ! Copy of RHS: useful if local (to each slave) RHS is
  ! needed after the solver
  !
  if( INOTMASTER .and. solve_sol(1) % kfl_recov == 1 ) then
     call memory_alloca(memma,'RHSCP','solver',rhscp,solve_sol(1) % nunkn)
     do icomp = 1,solve_sol(1) % nunkn
        rhscp(icomp) = rhsid(icomp)
     end do
  end if
  !
  ! Headers
  !
  if( solve_sol(1) % kfl_algso /= 9 ) then
     if( solve_sol(1) % heade == 0 ) then
        solve_sol(1) % heade = 1
        call outfor(39_ip,0_ip,' ')
        if( solve_sol(1)%kfl_algso == 2 ) call outdef(1_ip)
     end if
     call outfor(5_ip,0_ip,' ')
  end if

  !----------------------------------------------------------------------
  !
  ! Zones: Put 1 on diagonal
  !
  !----------------------------------------------------------------------

  call soldod(solve_sol(1) % ndofn,amatr,rhsid,unkno)

  !----------------------------------------------------------------------
  !
  ! Modify RHS due to Parall service: RHSID
  !
  !----------------------------------------------------------------------

   if( solve_sol(1) % kfl_recov /= 2 ) then
     call pararr('SLX',NPOIN_TYPE,solve_sol(1) % ndofn*npoin,rhsid)
     !call PAR_INTERFACE_NODE_EXCHANGE(solve_sol(1) % ndofn,rhsid,'SUM','IN MY ZONE','SYNCHRONOUS')
  end if
 
  !----------------------------------------------------------------------
  !
  ! Algebraic solver
  !
  !----------------------------------------------------------------------

  call cputim(time1)

  if( solve_sol(1) % kfl_blogs == 1 .and. solve_sol(1) % ndofn > 1 ) then
     !
     ! Block Gauss-Seidel
     !
     auxdof = solve_sol(1) % ndofn 
     nblok                = abs(solve_sol(1) % nblok) 
     if (nblok == solve_sol(1) % ndofn) then
        solve_sol(1) % nzmat = solve_sol(1) % nzmat / solve_sol(1) % ndof2
        solve_sol(1) % nzrhs = solve_sol(1) % nzrhs / solve_sol(1) % ndofn    
        solve_sol(1) % ndofn = 1 
        solve_sol(1) % ndof2 = solve_sol(1) % ndofn * solve_sol(1) % ndofn
     else
        solve_sol(1) %  nzmat = solve_sol(1) % nzmat / (nblok**2)
        solve_sol(1) % nzrhs = solve_sol(1) % nzrhs / nblok
        solve_sol(1) % ndofn = solve_sol(1) % ndofn_per_block
        solve_sol(1) % ndof2 = solve_sol(1) % ndofn * solve_sol(1) % ndofn
     end if

     do iblok = 1,nblok

        if( INOTMASTER ) then

           if( solve_sol(1) % nblok < 0 ) then
              call matrix_blokgs(&
                   1_ip,solve_sol(1) % nequa,solve_sol(1) % ndofn_per_block,nblok,iblok,r_dom,c_dom,amatr,rhsid,unkno,&
                   aa_idofn,bb_idofn,xx_idofn,memma, solve_sol(1) % lperm_block)
           else
              call matrix_blokgs(&
                   1_ip,solve_sol(1) % nequa,solve_sol(1) % ndofn_per_block,nblok,iblok,r_dom,c_dom,amatr,rhsid,unkno,&
                   aa_idofn,bb_idofn,xx_idofn,memma)
           end if

        else
           aa_idofn => nul1r
           bb_idofn => nul1r
           xx_idofn => nul1r
        end if

        if( solve_sol(1) % kfl_algso == -999 ) then
           call runend('SOLVER: NO SOLVER HAS BEEN DEFINED '&
                //'FOR PROBLEM '//trim(solve_sol(1) % wprob))   ! Error

        else if( solve_sol(1) % kfl_algso == SOL_SOLVER_PASTIX ) then 
           call runend('SOLVER: NOT CODED')

        else if( solve_sol(1) % kfl_algso == SOL_SOLVER_MUMPS ) then 
           call Solmum(bb_idofn,xx_idofn,aa_idofn)              ! MUMPS

        else if( solve_sol(1) % kfl_algso == 14 ) then
           call csrluf(solve_sol(1) % ndofn,npoin,&             ! Direct Sparse
                bb_idofn,xx_idofn,aa_idofn,r_dom,c_dom)           

        else
           call solite(bb_idofn,xx_idofn,aa_idofn,pmatr)        ! Iterative
        end if


        if( INOTMASTER ) then

           if( solve_sol(1) % nblok < 0 ) then
              call matrix_blokgs(&
                   2_ip,solve_sol(1) % nequa,solve_sol(1) % ndofn_per_block,nblok,iblok,r_dom,c_dom,amatr,rhsid,unkno,&
                   aa_idofn,bb_idofn,xx_idofn,memma, solve_sol(1) % lperm_block)
           else
              call matrix_blokgs(&
                   2_ip,solve_sol(1) % nequa,solve_sol(1) % ndofn_per_block,nblok,iblok,r_dom,c_dom,amatr,rhsid,unkno,&
                   aa_idofn,bb_idofn,xx_idofn,memma)
           end if
        end if

     end do

     if( INOTMASTER ) then
        if( solve_sol(1) % nblok < 0 ) then
           call matrix_blokgs(&
                3_ip,solve_sol(1) % nequa,solve_sol(1) % ndofn_per_block,nblok,iblok,r_dom,c_dom,amatr,rhsid,unkno,&
                aa_idofn,bb_idofn,xx_idofn,memma, solve_sol(1) % lperm_block)
        else
           call matrix_blokgs(&
                3_ip,solve_sol(1) % nequa,solve_sol(1) % ndofn_per_block,nblok,iblok,r_dom,c_dom,amatr,rhsid,unkno,&
                aa_idofn,bb_idofn,xx_idofn,memma)
        end if
     end if

     if( nblok == solve_sol(1) % ndofn ) then
        solve_sol(1) % ndofn = nblok 
        solve_sol(1) % ndof2 = solve_sol(1) % ndofn * solve_sol(1) % ndofn
        solve_sol(1) % nzmat = solve_sol(1) % nzmat * solve_sol(1) % ndof2
        solve_sol(1) % nzrhs = solve_sol(1) % nzrhs * solve_sol(1) % ndofn    
     else
        solve_sol(1) % ndofn = auxdof
        solve_sol(1) % ndof2 = solve_sol(1) % ndofn * solve_sol(1) % ndofn
        solve_sol(1) % nzmat = solve_sol(1) % nzmat * (nblok**2)
        solve_sol(1) % nzrhs = solve_sol(1) % nzrhs * nblok
     end if

  else
     !
     ! Monolithic
     !
     if( solve_sol(1) % kfl_algso == -999 ) then
        call runend('SOLVER: NO SOLVER HAS BEEN DEFINED '&
             //'FOR PROBLEM '//trim(solve_sol(1) % wprob))   ! Error

     else if( solve_sol(1) % kfl_algso == SOL_SOLVER_PASTIX ) then                        
        call solver_pastix(solve_sol(1) % ndofn,npoin,&      ! PASTIX
             rhsid,unkno,amatr,r_dom,c_dom)                

     else if( solve_sol(1) % kfl_algso == SOL_SOLVER_MUMPS ) then                        
        call Solmum(rhsid,unkno,amatr)                       ! MUMPS

     else if( solve_sol(1) % kfl_algso == 14 ) then
        call csrluf(solve_sol(1) % ndofn,npoin,&             ! Direct Sparse
             rhsid,unkno,amatr,r_dom,c_dom)           

     else                           

        call solite(rhsid,unkno,amatr,pmatr)                 ! Iterative

     end if


  end if

  !----------------------------------------------------------------------
  !
  ! Finish solver: perdiocity, internal force 
  !
  !----------------------------------------------------------------------

  if( solve_sol(1) % kfl_version == 0 ) then
     !
     ! Periodicity: recover solution on slave nodes
     !
     call presol(2_ip,solve_sol(1) % ndofn,solve_sol(1) % ndofn,dummr,dummr,unkno) 
     !
     ! Internal reaction
     !
     call presol(5_ip,solve_sol(1) % ndofn,solve_sol(1) % ndofn,rhsid,amatr,unkno) 

  else if( INOTMASTER ) then
     !
     ! Just in case, see nastin
     !
     call solver_periodicity('SOLUTION',solve_sol,solve_sol(1) % ndofn,solve_sol(1) % ndofn,amatr,rhsid,unkno)

  end if
  !
  ! Assembly is required
  !
  solve_sol(1) % kfl_assem = 0
  !
  ! Recover local residual RHSID in parallel
  !
  if( INOTMASTER  .and. solve_sol(1) % kfl_recov == 1 ) then
     do icomp = 1,solve_sol(1) % nunkn
        rhsid(icomp) = rhscp(icomp)
     end do
     call memory_deallo(memma,'RHSCP','solver',rhscp)
  end if

  !----------------------------------------------------------------------
  !
  ! Output matrix in MatrixMarket format: only in sequential
  !
  !----------------------------------------------------------------------

  if( solve_sol(1) % kfl_marke == 1 .and. INOTMASTER ) then

     solve_sol(1) % kfl_marke = 0
     file_market_aa    = 'MATRIXMARKET_MATRIX'     //trim(intost(kfl_paral))//'.txt'
     file_market_bb    = 'MATRIXMARKET_RHS'        //trim(intost(kfl_paral))//'.txt'

     call iofile(0_ip,90_ip,file_market_aa,   'MATRIXMARKET_MATRIX')
     call iofile(0_ip,91_ip,file_market_bb,   'MATRIXMARKET_RHS')

     write(90,'(a)') '%%MatrixMarket matrix coordinate real general'
     write(91,'(a)') '%%MatrixMarket matrix array real general'
     write(90,'(3(1x,i12))') npoin,npoin,solve_sol(1) % nzmat
     write(91,'(3(1x,i12))') npoin,1_ip
     do ipoin = 1,npoin
        write(91,'(1(1x,i9),1x,e12.6)') ipoin,rhsid(ipoin)
        do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
           jpoin = c_dom(izdom)
           write(90,'(2(1x,i9),1x,e12.6)') ipoin,jpoin,amatr(izdom)
        end do
     end do

     call iofile(2_ip,90_ip,file_market_aa,   'MATRIXMARKET_MATRIX')
     call iofile(2_ip,91_ip,file_market_bb,   'MATRIXMARKET_RHS')

     if( ISLAVE ) then
        file_market_permu = 'MATRIXMARKET_PERMUTATION'//trim(intost(kfl_paral))//'.txt'
        call iofile(0_ip,92_ip,file_market_permu,'MATRIXMARKET_PERMUTATION')
        write(92,'(a)') '%%MatrixMarket matrix array integer general'
        write(92,'(3(1x,i12))') npoin,1_ip
        do ipoin = 1,npoin
           write(92,'(2(1x,i9))') ipoin,lninv_loc(ipoin)
        end do
        call iofile(2_ip,92_ip,file_market_permu,'MATRIXMARKET_PERMUTATION')
     end if

  else if( solve_sol(1) % kfl_marke == 2 .and. INOTMASTER ) then

     solve_sol(1) % kfl_marke = 0
     file_market_aa    = 'MATRIX_ASCII'//trim(intost(kfl_paral))//'.txt'
     call iofile(0_ip,90_ip,file_market_aa,'MATRIX_ASCII')

     do ipoin = 1,npoin        
        do jpoin = 1,npoin
            izdom = r_dom(ipoin)
            jzdom = 0
            izdom_loop: do while( izdom <= r_dom(ipoin+1)-1 )
               if( c_dom(izdom) == jpoin ) then
                  jzdom = izdom
                  exit izdom_loop
               end if
               izdom = izdom + 1
            end do izdom_loop
            if( jzdom /= 0 ) then
               write(90,'(1x,e12.6,$)') amatr(jzdom)
            else
               write(90,'(1x,e12.6,$)') 0.0_rp
            end if
         end do
         write(90,*)
     end do

  end if

  call cputim(time2)
  solve_sol(1) % cputi(1) = solve_sol(1) % cputi(1) + (time2-time1)
  cpu_solve               = cpu_solve               + (time2-time1)
  solve_sol(1) % nsolv    = solve_sol(1) % nsolv    + 1

end subroutine solver
