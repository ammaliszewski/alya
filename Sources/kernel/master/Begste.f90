!-----------------------------------------------------------------------
!> @addtogroup Begste
!> @{
!> @file    Begste.f90
!> @author  Guillaume Houzeaux
!> @brief   Begin a time step
!> @details Begin a time step:
!>          - Modules update boundary conditions, unknowns, etc.
!> @} 
!-----------------------------------------------------------------------
subroutine Begste()
  use def_master
  use def_parame
  use mod_ker_proper
  use def_coupli,    only : mcoup
  use def_coupli,    only : coupling_driver_iteration
  implicit none 

  !
  ! Turn back reset flag to previous step
  !
  if (kfl_reset == 1) then
     !
     ! Initializations
     !
     call iniste(2_ip)
     cutim  = cutim - dtime
     call setgts(2_ip)
     call livinf(201_ip, ' ',1_ip)
  endif
  !
  ! Begin a time step for each module
  !
  call moduls(ITASK_BEGSTE)
  !
  ! Turn back properties if reset
  !
  if (kfl_reset == 1) then
     call ker_updpro()
     ! Deactivate reset request
     kfl_reset = 0
  end if
  !
  ! Coupling: Put counters to zero
  ! 
  if( mcoup > 0 ) then
     coupling_driver_iteration(1:nblok) = 0
  end if

  call coupling_set_dt()
  !
  ! Transient fields
  !
  call calc_kx_tran_fiel()
  
  contains
!-------------------------------------------------------------------------||---!
!                                                                              !
!-------------------------------------------------------------------------||---!

  !-----------------------------------------------------------------------||---!
  !> @author  JM Zavala-Ake
  !> @date    
  !> @brief   
  !> @details 
  !-----------------------------------------------------------------------||---!
  subroutine coupling_set_dt()
  use mod_communications, only: par_min 
  use mod_couplings,      only : THERE_EXISTS_A_ZONE_COUPLING
    use def_coupli,    only : mcoup, coudt
  implicit none 
  real(rp) :: dt_inv(1_ip)
  !
!
!print *, coudt, "<-----"
!
  if( (mcoup > 0).and.(coudt==1_ip).and.THERE_EXISTS_A_ZONE_COUPLING() ) then 
    dt_inv(1_ip) = dtinv  
    call PAR_MIN(1_ip, dt_inv, 'IN CURRENT COUPLING')
!
!    if(imaster) print *, 1.0/dtinv, 1.0/dt_inv(1_ip), "<----"
!
    dtinv = dt_inv(1_ip)      
    !
    cutim  = cutim - dtime          ! \
   !call iniste(2_ip)               ! |__ 2014Dic10, <-- Begste, if(kfl_reset == 1) 
    call setgts(2_ip)               ! |
    call livinf(201_ip, ' ',1_ip)   ! /
    !
  endif
  ! 
  end subroutine
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!

end subroutine Begste
