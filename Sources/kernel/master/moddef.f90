subroutine moddef(itask)
  !-----------------------------------------------------------------------
  !****f* master/moddef
  ! NAME
  !    moddef
  ! DESCRIPTION
  !    This subroutine define units, file names, open and close files
  !
  !    ------------------------------------------------------------
  !    Units #      Unit name     File name     Description
  !    ------------------------------------------------------------
  !
  !    MOMOD(:) %
  !
  !    1 .......... lun_pdata ... fil_pdata ... Data
  !    2 .......... lun_outpu ... fil_outpu ... Output
  !    3 .......... lun_conve ... fil_conve ... Convergence
  !    5 .......... lun_rstar ... fil_rstar ... Restart
  !
  !    POSTP(1) %
  !
  !    6 .......... lun_setse ... fil_setse ... Element set
  !    7 .......... lun_setsb ... fil_setsb ... Boundary set
  !    8 .......... lun_setsn ... fil_setsn ... Node set
  !    9 .......... lun_setsi ... fil_setsi ... IB set
  !    26 ......... lun_witne ... fil_witne ... Witness point
  !
  !    SOLVE(:) %
  !
  !    50 -> 69 ... lun_solve ... fil_solve ... Solver output
  !    70 -> 89 ... lun_cvgso ... fil_cvgso ... Solver convergence
  !
  !    SOLAD(:) %
  !
  !    90 -> 109 ... lun_solve ... fil_solve ... Adjoint Solver output
  !    110 -> 129 ... lun_cvgso ... fil_cvgso ... Adjoint Solver convergence
  !

  !    ------------------------------------------------------------
  !
  ! USES
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master
  use def_kermod
  use def_solver
  use def_inpout
  use mod_memchk
  use mod_memory
  use mod_iofile
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ivari,jtask,imod0,imod1,imod2,nsolv,nadjo
  character(5)            :: wmodu,wfort(mmodu)
  character(7)            :: statu
  character(11)           :: forma
  character(6)            :: posit
  character(150)          :: fil_rstar_new
  character(150)          :: fil_rstib_new

  jtask = abs(itask)
  if( itask > 0 ) then
     !
     ! Treat all modules
     !
     imod0 = 0
     imod1 = 1
     imod2 = mmodu
  else
     !
     ! Treat only module MODUL
     !
     imod0 = modul
     imod1 = modul
     imod2 = modul
  end if

  if ( ( jtask == 1_ip .or. jtask == 2 ) .and. INOTSLAVE ) then
     !
     ! Define unit opening option if this is a restart run
     !
     if ( kfl_rstar == 2 ) then
        statu = 'old'
        forma = 'formatted'
        posit = 'append'
     else
        statu = 'unknown'
        forma = 'formatted'
        posit = 'asis'
     end if
     do modul = imod1,imod2
        wmodu        = intost(modul)
        wfort(modul) = 'FOR'//trim(wmodu)
     end do
  end if

  if ( jtask == 1_ip .and. INOTSLAVE ) then

     !-------------------------------------------------------------------
     !
     ! Open files that are always needed and get file names
     !
     !-------------------------------------------------------------------

     do modul = imod1,imod2

        postp => momod(modul) % postp
        solve => momod(modul) % solve
        eigeg => momod(modul) % eigen
        !
        ! Define all units
        !
        momod(modul) % lun_pdata = modul * 100 +  1     ! Data
        momod(modul) % lun_outpu = modul * 100 +  2     ! Output
        momod(modul) % lun_conve = modul * 100 +  3     ! Convergence
        momod(modul) % lun_rstpo = modul * 100 +  4     ! Restart variables
        momod(modul) % lun_rstar = modul * 100 +  5     ! Restart
        postp(1)     % lun_setse = modul * 100 +  6     ! Element set
        postp(1)     % lun_setsb = modul * 100 +  7     ! Boundary set
        postp(1)     % lun_setsn = modul * 100 +  8     ! Node set
        postp(1)     % lun_setsi = modul * 100 +  9     ! IB set
        postp(1)     % lun_witne = modul * 100 + 26     ! Witness point

        if ( kfl_modul(modul) /= 0 ) then

           if ( kfl_naked == 0 ) then
              !
              !  Encapsulated, then get names from the environment
              !
              call GETENV( trim(wfort(modul)) // '01', momod(modul) % fil_pdata )
              call GETENV( trim(wfort(modul)) // '02', momod(modul) % fil_outpu )
              call GETENV( trim(wfort(modul)) // '03', momod(modul) % fil_conve )
              call GETENV( trim(wfort(modul)) // '05', momod(modul) % fil_rstar )

           else if ( kfl_naked == 1 ) then
              !
              !  Naked, then compose the names
              !
              momod(modul) % fil_pdata = adjustl(trim(namda))//'.'//exmod(modul)//'.dat'
              momod(modul) % fil_outpu = adjustl(trim(namda))//'.'//exmod(modul)//'.log'
              momod(modul) % fil_conve = adjustl(trim(namda))//'.'//exmod(modul)//'.cvg'
              momod(modul) % fil_rstar = adjustl(trim(namda))//'.'//exmod(modul)//'.rst'

           end if

           if( modul == mmodu ) then
              kfl_reawr = 1
              call iofile(4_ip,momod(modul) % lun_pdata,momod(modul) % fil_pdata,namod(modul)//' DATA',       'old')
              if( kfl_reawr == -1 ) momod(modul) % lun_pdata = 0
              kfl_reawr = 0
           end if
           if( momod(modul) % lun_pdata > 0 ) then
              call iofile(zero,momod(modul) % lun_pdata,momod(modul) % fil_pdata,namod(modul)//' DATA',       'old')
           end if
           !
           ! Output (log) and convergence (cvg) files
           !
           if( kfl_rstar == 2 ) then
              kfl_reawr = 1
              call iofile(4_ip,momod(modul) % lun_outpu,momod(modul) % fil_outpu,namod(modul)//' OUTPUT',     statu,forma,posit)
              if( kfl_reawr == 1 ) then
                 call iofile(zero,momod(modul) % lun_outpu,momod(modul) % fil_outpu,namod(modul)//&
                      ' OUTPUT','old','formatted','append')
              else
                 call iofile(zero,momod(modul) % lun_outpu,momod(modul) % fil_outpu,namod(modul)//' OUTPUT')
              end if
              kfl_reawr = 1
              call iofile(4_ip,momod(modul) % lun_conve,momod(modul) % fil_conve,namod(modul)//&
                   ' CONVERGENCE',     statu,forma,posit)
              if( kfl_reawr == 1 ) then
                 call iofile(zero,momod(modul) % lun_conve,momod(modul) % fil_conve,namod(modul)//&
                      ' CONVERGENCE','old','formatted','append')
              else
                 call iofile(zero,momod(modul) % lun_conve,momod(modul) % fil_conve,namod(modul)//' CONVERGENCE')
              end if
              kfl_reawr = 0
           else
              call iofile(zero,momod(modul) % lun_outpu,momod(modul) % fil_outpu,namod(modul)//' OUTPUT',     statu,forma,posit)
              call iofile(zero,momod(modul) % lun_conve,momod(modul) % fil_conve,namod(modul)//' CONVERGENCE',statu,forma,posit)
           end if
           !
           ! Write header
           !
           call outfor(12_ip,momod(modul) % lun_outpu,' ')

        end if
     end do

  else if ( jtask == 2 .and. INOTSLAVE ) then

     !-------------------------------------------------------------------
     !
     ! Close data file and open files needed occasionally
     !
     !-------------------------------------------------------------------

     do modul = imod1,imod2
        if ( kfl_modul(modul) /= 0 ) then
           if( momod(modul) % lun_pdata /= 0 ) &
                call iofile(two,momod(modul) % lun_pdata,momod(modul) % fil_pdata,namod(modul)//' DATA')
        end if
     end do

     do modul = imod1,imod2

        if ( kfl_modul(modul) /= 0 ) then

           postp => momod(modul) % postp
           solve => momod(modul) % solve
           solad => momod(modul) % solad
           eigeg => momod(modul) % eigen

           if( associated(momod(modul) % solve) ) then
              nsolv = size(solve)
           else
              nsolv = 0
           end if

           if( associated(momod(modul) % solve) ) then
              do ivari = 1,nsolv
                 if ( solve(ivari) % kfl_algso /= -999 ) then
                    if ( solve(ivari) % kfl_solve /= 0 ) then
                       solve(ivari) % lun_solve = modul*100 + 49 + ivari ! 50 -> 69
                    end if
                    if ( solve(ivari) % kfl_cvgso /= 0 ) then
                       solve(ivari) % lun_cvgso = modul*100 + 69 + ivari ! 70 -> 89
                    end if
                 end if
              end do
              if ( size(solve) > 20 ) call runend('MODDEF: TOO MANY UNITS FOR SOLVER')
           end if
           if( associated(momod(modul) % solad) ) then
              nadjo = size(solad)
           else
              nadjo = 0
           end if

           if( associated(momod(modul) % solad) ) then
              do ivari = 1,nadjo
                 if ( solad(ivari) % kfl_algso /= -999 ) then
                    if ( solad(ivari) % kfl_solve /= 0 ) then
                       solad(ivari) % lun_solve = modul*100 + 89 + ivari ! 90 -> 109
                    end if
                    if ( solad(ivari) % kfl_cvgso /= 0 ) then
                       solad(ivari) % lun_cvgso = modul*100 + 109 + ivari ! 110 -> 129
                    end if
                 end if
              end do
              if ( size(solad) > 20 ) call runend('MODDEF: TOO MANY UNITS FOR SOLVER (ADJOINT)')
           end if

           if ( kfl_naked == 0 ) then
              !
              !  encapsulated, then get names from the environment
              !
              call GETENV( trim(wfort(modul))//'06', postp(1) % fil_setse )
              call GETENV( trim(wfort(modul))//'07', postp(1) % fil_setsb )
              call GETENV( trim(wfort(modul))//'08', postp(1) % fil_setsn )
              call GETENV( trim(wfort(modul))//'09', postp(1) % fil_setsi )
              call GETENV( trim(wfort(modul))//'26', postp(1) % fil_witne )

              do ivari = 1,nsolv
                 if ( solve(ivari) % kfl_algso /= -999 ) then
                    if ( solve(ivari) % kfl_solve /= 0 ) then
                       wmodu = intost(solve(ivari)%lun_solve)
                       call GETENV( trim(wfort(modul))//trim(wmodu), solve(ivari) % fil_solve )
                    end if
                    if ( solve(ivari) % kfl_cvgso /= 0 ) then
                       wmodu = intost(solve(ivari)%lun_cvgso)
                       call GETENV( trim(wfort(modul))//trim(wmodu), solve(ivari) % fil_cvgso )
                    end if
                 end if
              end do


              do ivari = 1,nadjo
                 if ( solad(ivari) % kfl_algso /= -999 ) then
                    if ( solad(ivari) % kfl_solve /= 0 ) then
                       wmodu = intost(solad(ivari)%lun_solve)
                       call GETENV( trim(wfort(modul))//trim(wmodu), solad(ivari) % fil_solve )
                    end if
                    if ( solad(ivari) % kfl_cvgso /= 0 ) then
                       wmodu = intost(solad(ivari)%lun_cvgso)
                       call GETENV( trim(wfort(modul))//trim(wmodu), solad(ivari) % fil_cvgso )
                    end if
                 end if
              end do


           else

              postp(1) % fil_setse = adjustl(trim(namda)) // '-element.'  // exmod(modul) // '.set'
              postp(1) % fil_setsb = adjustl(trim(namda)) // '-boundary.' // exmod(modul) // '.set'
              postp(1) % fil_setsn = adjustl(trim(namda)) // '-node.'     // exmod(modul) // '.set'
              postp(1) % fil_setsi = adjustl(trim(namda)) // '-IB.'       // exmod(modul) // '.set'
              postp(1) % fil_witne = adjustl(trim(namda)) // '.'          // exmod(modul) // '.wit'

              do ivari = 1,nsolv
                 if ( solve(ivari)%kfl_algso /= -999 ) then
                    if ( solve(ivari)%kfl_solve /= 0 ) then
                       solve(ivari) % fil_solve = adjustl(trim(namda))//'-'//trim(solve(ivari) % wprob)//'.'//exmod(modul)//'.sol'
                       call iofile(&
                            zero,solve(ivari) % lun_solve,solve(ivari) % fil_solve,namod(modul)//'  SOLVER', statu,forma,posit)
                    end if
                    if ( solve(ivari)%kfl_cvgso /= 0 ) then
                       solve(ivari) % fil_cvgso = adjustl(trim(namda))//'-'//trim(solve(ivari) % wprob)//'.'//exmod(modul)//'.cso'
                       call iofile(&
                            zero,solve(ivari) % lun_cvgso,solve(ivari) % fil_cvgso,namod(modul)//'  SOLVER', statu,forma,posit)
                    end if
                 end if
              end do


              do ivari = 1,nadjo
                 if ( solad(ivari)%kfl_algso /= -999 ) then
                    if ( solad(ivari)%kfl_solve /= 0 ) then
                       solad(ivari) % fil_solve = adjustl(trim(namda))//'-'//trim(solad(ivari) % wprob)//'.'//exmod(modul)//'.sol'

                       call iofile(&
                            zero,solad(ivari) % lun_solve,solad(ivari) % fil_solve,namod(modul)//'  SOLVER', statu,forma,posit)
                    end if
                    if ( solad(ivari)%kfl_cvgso /= 0 ) then
                       solad(ivari) % fil_cvgso = adjustl(trim(namda))//'-'//trim(solad(ivari) % wprob)//'.'//exmod(modul)//'.cso'

                       call iofile(&
                            zero,solad(ivari) % lun_cvgso,solad(ivari) % fil_cvgso,namod(modul)//'  SOLVER', statu,forma,posit)
                    end if
                 end if
              end do



           end if
           !
           ! Element set file
           !
           if ( maxval(postp(1) % npp_setse) > 0 ) &
                call iofile(zero,postp(1) % lun_setse,postp(1) % fil_setse,namod(modul)//' ELEMENT SETS ',statu,forma,posit)
           !
           ! Boundary set file
           !
           if ( maxval(postp(1) % npp_setsb) > 0 ) &
                call iofile(zero,postp(1) % lun_setsb,postp(1) % fil_setsb,namod(modul)//' BOUNDARY SETS',statu,forma,posit)
           !
           ! Node set file
           !
           if ( maxval(postp(1) % npp_setsn) > 0) &
                call iofile(zero,postp(1) % lun_setsn,postp(1) % fil_setsn,namod(modul)//' NODE SETS',    statu,forma,posit)
           !
           ! IB set file
           !
           if ( maxval(postp(1) % npp_setsb) > 0 .and. nboib > 0 ) &
                call iofile(zero,postp(1) % lun_setsi,postp(1) % fil_setsi,namod(modul)//' IB SETS',statu,forma,posit)
           !
           ! Witness point
           !
           if ( maxval(postp(1) % npp_witne) > 0 .and. nwitn > 0 ) &
                call iofile(zero,postp(1) % lun_witne,postp(1) % fil_witne,namod(modul)//' WITNESS POINT',statu,forma,posit)

        end if
     end do
     modul = 0
     postp     => momod(modul) % postp
     solve     => momod(modul) % solve
     solad     => momod(modul) % solad
     eigeg     => momod(modul) % eigen
     solve_sol => momod(modul) % solve
     solad_sol => momod(modul) % solad
     veset     => postp(1)     % veset
     vbset     => postp(1)     % vbset
     vnset     => postp(1)     % vnset
     witne     => postp(1)     % witne
     tncod     => momod(modul) % tncod
     tgcod     => momod(modul) % tgcod
     tbcod     => momod(modul) % tbcod

  else if ( jtask == 4_ip .and. INOTSLAVE ) then

     !-------------------------------------------------------------------
     !
     ! Turnof: write info and close result files
     !
     !-------------------------------------------------------------------

     do modul = imod1,imod2
        if ( kfl_modul(modul) /= 0 ) then

           postp => momod(modul) % postp
           solve => momod(modul) % solve
           solad => momod(modul) % solad
           eigeg => momod(modul) % eigen

           if( associated(momod(modul) % solve) ) then
              !
              ! Output solver statistics
              !
              solve_sol => momod(modul) % solve
              call outfor(37_ip,momod(modul) % lun_outpu,' ')
              !
              ! Write tail for formatted files
              !
              call outfor(6_ip,momod(modul) % lun_outpu,' ')
           end if

           if( associated(momod(modul) % solad) ) then
              !
              ! Output soladnt solver statistics
              !
              solve_sol => momod(modul) % solad
              call outfor(37_ip,momod(modul) % lun_outpu,' ')
              !
              ! Write tail for formatted files
              !
              call outfor(6_ip,momod(modul) % lun_outpu,' ')
              solve_sol => momod(modul) % solve
           end if
        end if
     end do

     do modul = imod1,imod2
        if ( kfl_modul(modul) /= 0 ) then

           postp => momod(modul) % postp
           solve => momod(modul) % solve
           solad => momod(modul) % solad
           eigeg => momod(modul) % eigen

           if( associated(momod(modul) % solve) ) then
              !
              ! Close result files
              !
              call iofile(two,momod(modul) % lun_outpu,' ',namod(modul)//' OUTPUT')
              call iofile(two,momod(modul) % lun_conve,' ',namod(modul)//' CONVERGENCE')
              do ivari = 1,size(solve)
                 if ( solve(ivari) % kfl_algso /= -999 ) then
                    if ( solve(ivari) % kfl_solve /= 0 ) then
                       call iofile(two,solve(ivari) % lun_solve,solve(ivari) % fil_solve,namod(modul)//' SOLVER')
                    end if
                    if ( solve(ivari) % kfl_cvgso /= 0 ) then
                       call iofile(two,solve(ivari) % lun_cvgso,solve(ivari) % fil_cvgso,namod(modul)//' SOLVER CONVERGENCE')
                    end if
                 end if
              end do
           end if

           if( associated(momod(modul) % solad) ) then
              !
              ! Close result files
              !
              !call iofile(two,momod(modul) % lun_outpu,' ',namod(modul)//' OUTPUT')
              !call iofile(two,momod(modul) % lun_conve,' ',namod(modul)//' CONVERGENCE')
              do ivari = 1,size(solad)
                 if ( solad(ivari) % kfl_algso /= -999 ) then
                    if ( solad(ivari) % kfl_solve /= 0 ) then
                       call iofile(two,solad(ivari) % lun_solve,solad(ivari) % fil_solve,namod(modul)//' SOLVER')
                    end if
                    if ( solve(ivari) % kfl_cvgso /= 0 ) then
                       call iofile(two,solad(ivari) % lun_cvgso,solad(ivari) % fil_cvgso,namod(modul)//' SOLVER CONVERGENCE')
                    end if
                 end if
              end do
           end if

           if ( maxval(postp(1) % npp_setse) > 0 ) &
                call iofile(two,postp(1) % lun_setse,postp(1) % fil_setse,namod(modul)//' ELEMENT SETS')
           if ( maxval(postp(1) % npp_setsb) > 0 ) &
                call iofile(two,postp(1) % lun_setsb,postp(1) % fil_setsb,namod(modul)//' BOUNDARY SETS')
           if ( maxval(postp(1) % npp_setsn) > 0) &
                call iofile(two,postp(1) % lun_setsn,postp(1) % fil_setsn,namod(modul)//' NODE SETS')
           if ( maxval(postp(1) % npp_setsb) > 0 .and. nboib > 0 ) &
                call iofile(two,postp(1) % lun_setsi,postp(1) % fil_setsi,namod(modul)//' IB SETS')
           if ( maxval(postp(1) % npp_witne) > 0 .and. nwitn > 0 ) &
                call iofile(two,postp(1) % lun_witne,postp(1) % fil_witne,namod(modul)//' WITNESS POINT')

        end if
     end do

  else if ( jtask == 6_ip .and. INOTSLAVE ) then

     !-------------------------------------------------------------------
     !
     ! Close restart file
     !
     !-------------------------------------------------------------------

     if( modul == -1 ) then
        call iofile(two,lun_rstib,fil_rstib,'IB RESTART')
     else if( modul == 0 ) then
        call iofile(two,lun_rstar,fil_rstar,'RESTART')
     else
        call iofile(two,momod(modul) % lun_rstar,momod(modul) % fil_rstar,namod(modul)//' RESTART')
     end if

  else if ( jtask == 7_ip .and. INOTSLAVE ) then

     !-------------------------------------------------------------------
     !
     ! Open restart file for reading
     !
     !-------------------------------------------------------------------

     if( modul == -1 ) then
        call iofile(zero,lun_rstib,fil_rstib,'IB RESTART','old','unformatted')
     else if( modul == 0 ) then
        call iofile(zero,lun_rstar,fil_rstar,'RESTART', 'old','unformatted')
     else
        call iofile(zero,momod(modul) % lun_rstar,momod(modul) % fil_rstar,namod(modul)//' RESTART','old','unformatted')
     end if

  else if ( jtask == 8_ip .and. INOTSLAVE ) then

     !-------------------------------------------------------------------
     !
     ! Open restart file for writing
     !
     !-------------------------------------------------------------------

     if( modul == -1 ) then
        fil_rstib_new = fil_rstib
        if( kfl_rsfil == 1 ) call appnum(ittim,fil_rstib_new)
        call iofile(zero,lun_rstib,fil_rstib_new,'IB RESTART','unknown','unformatted')
     else if( modul == 0 ) then
        fil_rstar_new = fil_rstar
        if( kfl_rsfil == 1 ) call appnum(ittim,fil_rstar_new)
        call iofile(zero,lun_rstar,fil_rstar_new,'RESTART', 'unknown','unformatted')
     else
        fil_rstar_new = momod(modul) % fil_rstar
        if( kfl_rsfil == 1 ) call appnum(ittim,fil_rstar_new)
        call iofile(zero,momod(modul) % lun_rstar,fil_rstar_new,namod(modul)//' RESTART','unknown','unformatted')
     end if

  else if ( jtask == 9_ip ) then

     !-------------------------------------------------------------------
     !
     ! Pointer to current module structures
     !
     !-------------------------------------------------------------------

     postp     => momod(modul) % postp
     solve     => momod(modul) % solve
     solad     => momod(modul) % solad
     eigeg     => momod(modul) % eigen
     solve_sol => momod(modul) % solve
     solad_sol => momod(modul) % solad
     eigen_sol => momod(modul) % eigen
     tncod     => momod(modul) % tncod
     tgcod     => momod(modul) % tgcod
     tbcod     => momod(modul) % tbcod
     veset     => postp(1)     % veset
     vbset     => postp(1)     % vbset
     vnset     => postp(1)     % vnset
     witne     => postp(1)     % witne
     if( modul > 0 ) then
        current_zone = lzone(modul)
     else
        current_zone = 0
     end if
  
  !else if ( jtask == 10_ip ) then

  !   !-------------------------------------------------------------------
  !   !
  !   ! Pointer to current module structures (optsol)
  !   !
  !   !-------------------------------------------------------------------

  !   solad     => momod(modul) % solad
  !   solad_sol => solad
  !   solve_sol => solad_sol

  end if

end subroutine moddef
