subroutine inivar(itask)
  !-----------------------------------------------------------------------
  !****f* master/inivar
  ! NAME
  !    inivar
  ! DESCRIPTION
  !    This routine initialize some variables
  ! USES
  ! USED BY
  !    Reapro
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_elmtyp
  use def_parame
  use def_postpr 
  use def_solver 
  use mod_memory,         only :  memory_alloca_min
  use mod_memory,         only :  memory_alloca
  use mod_parall,         only :  color_target 
  use mod_parall,         only :  color_source
  use mod_parall,         only :  I_AM_IN_COLOR
  use def_coupli,         only :  mcoup
  use def_coupli,         only :  coupling_type
  use def_coupli,         only :  RESIDUAL
  use def_coupli,         only :  BETWEEN_ZONES
  use mod_couplings,      only :  COU_LIST_SOURCE_NODES
  use mod_communications, only :  PAR_INTERFACE_NODE_EXCHANGE
  use mod_graphs,         only :  graphs_permut_metis_postordering
  use mod_graphs,         only :  graphs_copyij
  use mod_commdom_alya,   only :  CPLNG, CC
#ifdef COMMDOM
#if    COMMDOM==1
  use mod_commdom_alya_cht, only: CHT_CPLNG
#elif  COMMDOM==2 
  use mod_commdom_driver,   only: CNT_CPLNG
#endif 
#endif
  implicit none
  integer(ip), intent(in)      :: itask
  integer(ip)                  :: ipoin,idofn,iflin,ivari,ireaction
  integer(ip)                  :: icoup,ndofn,iflim,pelty,num_blocks
  integer(ip)                  :: jzdom,kpoin,ndofn_block
  integer(ip)                  :: jblok,ndofn_iblok,ndofn_jblok
  real(rp)                     :: nu
  logical(lg)                  :: sendrecv(5_ip)

  select case( itask )

  case ( 1_ip )
     !
     ! Postprocess
     !
     varnu_pos=0
     varna_pos='NULL'
     if( kfl_outfo == 50 ) then
        !
        ! HDF
        !
        call Hdfpos(9_ip)
     else if ( kfl_outfo == 40 .or.kfl_outfo == 41 ) then  
        !
        ! VTK
        ! initialise time flag
        !vtk_time=-1.0_rp
        vtk_time=-1_ip
        !#ifdef VTK
        !        if (IMASTER) write(*,*)'---------call coprocessorinitial'
        !        if (IMASTER )write(*,*)'---------kernel/master/inivar'
        !        call coprocessorinitializewithpython("coproc.py",9)
        !#endif
     end if

     if( READ_AND_RUN() ) kfl_latex = 0
     !
     ! Physical problem: compute usref
     !
     !if( u_ref > 0.0_rp .and. h_ref > 0.0_rp .and. k_ref > 0.0_rp .and. usref < 0.0_rp ) then
     !   if( denme > 0.0_rp ) then
     !      nu = visme / denme
     !      call frivel(h_ref,k_ref,u_ref,nu,usref)
     !   end if
     !end if

  case ( 2_ip )
     !
     ! Cancel Hessian calculation
     !
     if( kfl_lapla == 0 ) then
        do pelty = 1,nelty
           llapl(pelty) = 0
        end do
     end if
     !
     ! Prepare module solvers 
     !
     do modul = 1,mmodu
        if( kfl_modul(modul) == 1 ) then
           if( associated(momod(modul) % solve) ) then
              current_zone = lzone(modul)
              do ivari = 1,size(momod(modul) % solve)
                 solve_sol => momod(modul) % solve(ivari:)
                 if( solve_sol(1) % kfl_algso /= -999 ) then
                    iflin     =  0
                    iflim     =  0
                    !
                    ! linelet 
                    !
                    if( solve_sol(1) % kfl_preco == SOL_LINELET .or. &
                         ( solve_sol(1) % kfl_preco == SOL_MULTIGRID .and. solve_sol(1) % kfl_defpr == SOL_LINELET ) ) then
                       iflin = 1
                       if( INOTMASTER ) then
                          ndofn = size(solve_sol(1) % kfl_fixno,1)
                          if( ndofn > 1 ) then
                             call memory_alloca(mem_modul(1:2,modul),'SOLVE_SOL(1) % LIMLI','inivar',solve_sol(1) % limli,npoin)
                             do ipoin = 1,npoin
                                do idofn = 1,ndofn
                                   if( momod(modul) % solve(ivari) % kfl_fixno(idofn,ipoin) > 0 ) solve_sol(1) % limli(ipoin) = 1
                                end do
                             end do
                          else
                             solve_sol(1) % limli => momod(modul) % solve(ivari) % kfl_fixno(1,1:)
                          end if
                       end if
                       call crelin() 
                    end if
                    !
                    ! Possible errors
                    !
                    if( solve_sol(1) % kfl_preco == SOL_RAS .and. kfl_graph == 0 ) then
                       call runend('INIVAR: TO USE RAS, ADD EXTENDED_GRAPH=ON TO THE MESH SECTION OF THE ker.dat FILE')
                    end if
                    !
                    ! Renumbered Gauss-Seidel
                    !
                    if( solve_sol(1) % kfl_preco == SOL_GAUSS_SEIDEL ) then
                       if( solve_sol(1) % kfl_renumbered_gs ==  1 ) solve_sol(1) % vecto_gs => advec(:,:,1)
                    end if
                    !
                    ! Deflated CG / multigrid / RAS preconditioner
                    ! 
                    
                    if(    solve_sol(1) % kfl_algso  == SOL_DEFLATED_CG                  .or. &
                         & solve_sol(1) % kfl_algso  == SOL_GDECG                        .or. &
                         & solve_sol(1) % kfl_preco  == SOL_MULTIGRID                    .or. &
                         & solve_sol(1) % kfl_algso  == SOL_SOLVER_PIPELINED_DEFLATED_CG .or. &
                         & solve_sol(1) % kfl_coarse == 1    ) then
                       if( INOTMASTER ) then 
                          ndofn = size(solve_sol(1) % kfl_fixno,1)
                          if( ndofn > 1 ) then
                             if( iflin == 1 ) then
                                solve_sol(1) % limpo => momod(modul) % solve(ivari) % limli
                             else  
                                call memory_alloca(mem_modul(1:2,modul),'SOLVE_SOL(1) % LIMPO','inivar',solve_sol(1) % limpo,npoin)
                                do ipoin = 1,npoin
                                   solve_sol(1) % limpo(ipoin) = 0
                                   do idofn = 1,ndofn
                                      if( solve_sol(1) % kfl_fixno(idofn,ipoin) > 0 ) then
                                         solve_sol(1) % limpo(ipoin) = 1
                                      end if
                                   end do
                                end do
                             end if
                          else
                             solve_sol(1) % limpo => momod(modul) % solve(ivari) % kfl_fixno(1,1:)
                          end if
                       end if
                       if( ngrou_dom == 0 ) then
                          call runend('INIVAR: GROUPS SHOULD BE DECLARED IN GEOMETRY FIELD')
                       else 
                          !if(modul==ID_PARTIS) print*,'a=',kfl_paral,size(momod(modul) % solve(ivari) % kfl_fixno,2)
                          call cregro()
                          !if(modul==ID_PARTIS) print*,'b'
                       end if
                    end if
                    ! 
                    ! RAS preconditioner
                    ! 
                    if( solve_sol(1) % kfl_preco  == SOL_RAS ) then
                       call livinf(0_ip,trim(namod(modul))//': POSTORDERING FOR RAS PRECONDITIONER',modul)
                       if( INOTMASTER ) then
                          call graphs_copyij(&
                               npoin,&
                               r_dom                   , c_dom                   , &
                               solve_sol(1) % ia_ras   , solve_sol(1) % ja_ras   )
                          call graphs_permut_metis_postordering(&
                               npoin                   , nzdom                   , &
                               solve_sol(1) % ia_ras   , solve_sol(1) % ja_ras   , &
                               solve_sol(1) % permRras , solve_sol(1) % invpRras )
                          solve_sol(1) % invpCras => solve_sol(1) % invpRras
                       end if
                    end if
                 end if
              end do

              !----------------------------------------------------------
              !
              ! Determine if and where reaction forces should be computed
              !
              !----------------------------------------------------------
              !-------------------------------------------------------------------------||---!
              !                                                                              !
              !------------------------------------------------------------------------------!
#ifdef COMMDOM
#if   COMMDOM==1
              call commdom_nodes_to_reaction( CHT_CPLNG )
#elif COMMDOM==2 
              !  call commdom_nodes_to_reaction( CNT_CPLNG ) !< 2015Abr28 -> 2015May10 
#elif COMMDOM==4
              call commdom_nodes_to_reaction( CPLNG(CC) )
#endif
#else
              !-------------------------------------------------------------------------||---!
              !                                                                              !
              !------------------------------------------------------------------------------!
              if( INOTMASTER .and. I_AM_IN_ZONE(current_zone) .and. mcoup > 0 ) then
                 ireaction =  0
                 solve_sol => momod(modul) % solve(1:)

                 if( solve_sol(1) % kfl_algso /= -999 ) then

                    ndofn      = solve_sol(1) % ndofn
                    num_blocks = solve_sol(1) % num_blocks
                    if( solve_sol(1) % block_num == 1 ) then
                       !
                       ! SOLVE_SOL(1) % LPOIN_REACTION(1:NPOIN): Mark the nodes where reaction is required
                       ! They are the source nodes 
                       !
                       do icoup = 1,mcoup
                          if(    coupling_type(icoup) % what     == RESIDUAL      .and. &
                               & coupling_type(icoup) % kind     == BETWEEN_ZONES ) then

                             color_target = coupling_type(icoup) % color_target
                             color_source = coupling_type(icoup) % color_source

                             if( ireaction == 0 ) then
                                if( I_AM_IN_COLOR(color_source) ) then
                                   solve_sol(1) % kfl_react = 1
                                   call memory_alloca(mem_modul(1:2,modul),'SOLVE_SOL(1) % REACTION','inivar',solve_sol(1) % reaction,ndofn,npoin)
                                   do iblok = 2,num_blocks
                                      ndofn_block = solve_sol(1) % block_dimensions(iblok) 
                                      call memory_alloca(mem_modul(1:2,modul),'SOLVE_SOL(IBLOK) % REACTION','inivar',solve_sol(iblok) % reaction,ndofn_block,npoin)
                                   end do
                                end if
                                call memory_alloca(mem_modul(1:2,modul),'SOLVE_SOL(1) % LPOIN_REACTION','inivar',solve_sol(1) % lpoin_reaction,npoin)

                                if( I_AM_IN_COLOR(color_target) ) then
                                   solve_sol(1) % kfl_bvnat = 1
                                   call memory_alloca(mem_modul(1:2,modul),'SOLVE_SOL(1) % BVNAT','inivar',solve_sol(1) % block_array(1) % bvnat,ndofn,npoin)
                                   solve_sol(1) % bvnat => solve_sol(1) % block_array(1) % bvnat
                                   do iblok = 2,num_blocks
                                      ndofn_block = solve_sol(1) % block_dimensions(iblok) 
                                      call memory_alloca(mem_modul(1:2,modul),'SOLVE_SOL(1) % BVNAT','inivar',solve_sol(1) % block_array(iblok) % bvnat,ndofn_block,npoin)
                                   end do
                                end if

                             end if
                             ireaction = ireaction + 1
                             call COU_LIST_SOURCE_NODES(solve_sol(1) % lpoin_reaction,icoup)

                          end if
                       end do

                       if( ireaction > 0 ) then
                          call PAR_INTERFACE_NODE_EXCHANGE(solve_sol(1) % lpoin_reaction,'OR','IN CURRENT ZONE')

                          if( num_blocks == 1 .and. solve_sol(1) % kfl_react > 0 ) then
                             !
                             ! Monolithic system
                             !
                             allocate( solve_sol(1) % lpoin_block(npoin) )
                             ndofn = solve_sol(1) % ndofn
                             do kpoin = 1,npoiz(current_zone)
                                ipoin = lpoiz(current_zone) % l(kpoin)
                                if( solve_sol(1) % lpoin_reaction(ipoin) ) then
                                   jzdom = r_dom(ipoin+1) - r_dom(ipoin)
                                   allocate( solve_sol(1) % lpoin_block(ipoin) % block2_num(1,1)                             )
                                   allocate( solve_sol(1) % lpoin_block(ipoin) % block1_num(1)                               )
                                   allocate( solve_sol(1) % lpoin_block(ipoin) % block1_num(1)   % rhs(ndofn)                )
                                   allocate( solve_sol(1) % lpoin_block(ipoin) % block2_num(1,1) % matrix(ndofn,ndofn,jzdom) )
                                   solve_sol(1) % lpoin_block(ipoin) % block1_num(1)   % rhs    = 0.0_rp
                                   solve_sol(1) % lpoin_block(ipoin) % block2_num(1,1) % matrix = 0.0_rp
                                end if
                             end do

                          else if( solve_sol(1) % kfl_react > 0 ) then
                             !
                             ! ( n x n ) block system
                             !
                             allocate( solve_sol(1) % lpoin_block(npoin) )
                             do kpoin = 1,npoiz(current_zone)
                                ipoin = lpoiz(current_zone) % l(kpoin)
                                if( solve_sol(1) % lpoin_reaction(ipoin) ) then
                                   allocate( solve_sol(1) % lpoin_block(ipoin) % block2_num(num_blocks,num_blocks) )
                                   allocate( solve_sol(1) % lpoin_block(ipoin) % block1_num(num_blocks) )
                                   jzdom = r_dom(ipoin+1) - r_dom(ipoin)
                                   do iblok = 1,num_blocks
                                      ndofn_iblok = solve_sol(1) % block_dimensions(iblok)
                                      allocate( solve_sol(1) % lpoin_block(ipoin) % block1_num(iblok) % rhs(ndofn_iblok) )
                                      solve_sol(1) % lpoin_block(ipoin) % block1_num(iblok) % rhs = 0.0_rp
                                      do jblok = 1,num_blocks
                                         ndofn_jblok = solve_sol(1) % block_dimensions(jblok)
                                         allocate( solve_sol(1) % lpoin_block(ipoin) % block2_num(iblok,jblok) % matrix(ndofn_jblok,ndofn_iblok,jzdom) )
                                         solve_sol(1) % lpoin_block(ipoin) % block2_num(iblok,jblok) % matrix = 0.0_rp
                                      end do
                                   end do
                                end if
                             end do
                          end if
                       end if
                    end if
                 end if
              end if
              !-------------------------------------------------------------------------||---!
              !                                                                              !
              !------------------------------------------------------------------------------!
              !#endif !< 2015Abr28
              !
              ! Allocate minimum memory to some arrays
              !
              do ivari = 1,size(momod(modul) % solve)
                 solve_sol => momod(modul) % solve(ivari:)
                 if( solve_sol(1) % kfl_algso /= -999 ) then
                    call memory_alloca_min(solve_sol(1) % reaction)
                    call memory_alloca_min(solve_sol(1) % bvnat)
                    if( solve_sol(1) % block_num == 1 ) then
                       do iblok = 1,solve_sol(1) % num_blocks
                          call memory_alloca_min(solve_sol(iblok) % reaction)
                          call memory_alloca_min(solve_sol(1) % block_array(iblok) % bvnat)
                       end do
                    end if
                 end if
              end do
              !
#endif 
              !< 2015Abr28
              !
           end if
        end if
     end do
     modul = 0
     current_zone = 0

  end select
end subroutine inivar
!-------------------------------------------------------------------------||---!
!                                                                              !
!------------------------------------------------------------------------------!
subroutine allocate_react_bvnat( mod_module, module_k, react, bvnat) 
  !
  ! CREATE: 2015JAN29 
  ! 
  use def_kintyp, only: ip, rp 
  use def_kintyp, only: soltyp, tymod 
  use def_domain, only: npoin
  use def_postpr, only: mem_modul
  use mod_memory, only: memory_alloca  
  implicit none
  integer(ip),  intent(in)    ::  module_k
  logical(ip),  intent(in)    ::  react, bvnat 
  type(tymod),  intent(inout) :: mod_module 
  type(soltyp), pointer  :: solve_sol(:)
  integer(ip)            :: ndofn
  integer(ip)            :: num_blocks
  integer(ip)            :: i_block, ndofn_block 
  !
  solve_sol  => mod_module % solve(1:)
  if( solve_sol(1) % block_num == 1 .and. solve_sol(1) % kfl_algso /= -999 ) then
    ndofn      = solve_sol(1) % ndofn
    num_blocks = solve_sol(1) % num_blocks
    if( react ) then
      solve_sol(1) % kfl_react = 1
      call memory_alloca(  mem_modul(1:2,module_k), 'SOLVE_SOL(    1) % REACTION',      'inivar',   solve_sol(    1) % reaction,       ndofn, npoin)
      do i_block = 2,num_blocks
        ndofn_block = solve_sol(1) % block_dimensions(i_block)
        call memory_alloca(mem_modul(1:2,module_k), 'SOLVE_SOL(i_block) % REACTION',      'inivar',   solve_sol(i_block) % reaction, ndofn_block, npoin)
      end do
    end if
!    call memory_alloca(    mem_modul(1:2,module_k), 'SOLVE_SOL(    1) % LPOIN_REACTION','inivar', solve_sol(1) % lpoin_reaction,              npoin)
!    solve_sol(1) % lpoin_reaction(1:npoin) = .false.  
    !   
    if( bvnat ) then
      solve_sol(1) % kfl_bvnat = 1
      call memory_alloca(  mem_modul(1:2,module_k), 'SOLVE_SOL(1) % BVNAT', 'inivar',     solve_sol(1) % block_array(1) % bvnat,      ndofn, npoin)
      solve_sol(1) % bvnat => solve_sol(1) % block_array(1) % bvnat
      do i_block = 2,num_blocks
        ndofn_block = solve_sol(1) % block_dimensions(i_block)
        call memory_alloca(mem_modul(1:2,module_k), 'SOLVE_SOL(1) % BVNAT', 'inivar', solve_sol(1) % block_array(i_block) % bvnat, ndofn_block, npoin)
      end do
    end if
    !
    call memory_alloca(    mem_modul(1:2,module_k), 'SOLVE_SOL(    1) % LPOIN_REACTION','inivar', solve_sol(1) % lpoin_reaction,              npoin)
    solve_sol(1) % lpoin_reaction(1:npoin) = .false.  
    !
  endif 
  !
end subroutine allocate_react_bvnat



!-------------------------------------------------------------------------||---!
!                                                                              !
!------------------------------------------------------------------------------!
subroutine  commdom_nodes_to_reaction( CPLNG )
  use def_parame,         only: ip, rp
  use def_master,         only: inotmaster, imaster, isequen
  use def_domain,         only: npoin
  use def_kintyp,         only: soltyp
  use mod_commdom_alya,   only: COMMDOM_COUPLING
  use def_master,         only: momod, modul
  use def_coupli,         only: RESIDUAL
#ifdef COMMDOM
  use mod_commdom_plepp,  only: commdom_plepp_set_mesh, commdom_plepp_set_source_nodes
#endif
  implicit none
  type(COMMDOM_COUPLING), intent(inout) :: CPLNG
  logical(ip) :: sendrecv(5_ip)
  !
  type(soltyp), pointer :: solve_sol(:)
  solve_sol => momod(modul) % solve(1_ip:)
  !
  ! CREATE: 2015JAN30
  !
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  CPLNG%current_what = .false.
  CPLNG%current_what(1_ip) = (CPLNG%what_i==RESIDUAL)
  CPLNG%current_what(2_ip) = (CPLNG%what_j==RESIDUAL)
  CPLNG%current_what(3_ip) = CPLNG%current_what(1_ip).and.CPLNG%current_what(2_ip)
  CPLNG%current_what(4_ip) = CPLNG%current_what(1_ip).or. CPLNG%current_what(2_ip)

  sendrecv = .false.
  sendrecv(1_ip) = (CPLNG%code_i==CPLNG%current_code).and.(CPLNG%module_i==modul).and.(CPLNG%current_what(4_ip)).and.INOTMASTER
  sendrecv(2_ip) = (CPLNG%code_j==CPLNG%current_code).and.(CPLNG%module_j==modul).and.(CPLNG%current_what(4_ip)).and.INOTMASTER
  sendrecv(3_ip) = sendrecv(1_ip).and.sendrecv(2_ip)
  sendrecv(4_ip) = sendrecv(1_ip).or. sendrecv(2_ip)
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  if( sendrecv(4_ip) ) then                   ! OR
    !
    if( sendrecv(2_ip) ) then                 !  CODE_J
      if( CPLNG%current_what(2_ip) ) then     ! BVNAT_J
        call allocate_react_bvnat(  momod(modul), modul, .false., .true.)
      else                                    ! REACT_J
        call allocate_react_bvnat(  momod(modul), modul, .true., .false.)
#ifdef COMMDOM
        call commdom_plepp_set_source_nodes( solve_sol(1) % lpoin_reaction(1:npoin)  )  ! Mark the nodes where reaction is required
#endif
        call allocate_block_system( momod(modul) )
      endif
      print *, "[commdom_plepp_inivar]", " 'RESIDUALj'" !, count( solve_sol(1) % lpoin_reaction(1:npoin)  ), solve_sol(1) % kfl_bvnat, solve_sol(1) % kfl_react
    else&
    if( sendrecv(1_ip) ) then                 !  CODE_I
      if( CPLNG%current_what(1_ip) ) then     ! BVNAT_I
        call allocate_react_bvnat(  momod(modul), modul, .false., .true.)
      else                                    ! REACT_I
        call allocate_react_bvnat(  momod(modul), modul, .true., .false.)
#ifdef COMMDOM
        call commdom_plepp_set_source_nodes( solve_sol(1) % lpoin_reaction(1:npoin)  )  ! Mark the nodes where reaction is required
#endif
        call allocate_block_system( momod(modul) )
      endif
      print *, "[commdom_plepp_inivar]", " 'RESIDUALi'" !, count( solve_sol(1) % lpoin_reaction(1:npoin)  ), solve_sol(1) % kfl_bvnat, solve_sol(1) % kfl_react
    endif
    !
  else
  endif
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
end subroutine
!-------------------------------------------------------------------------||---!
!                                                                              !
!------------------------------------------------------------------------------!
