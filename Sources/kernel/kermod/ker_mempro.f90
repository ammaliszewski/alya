subroutine ker_mempro(itask)
  use def_master
  use def_parame
  use def_kermod
  use def_domain
  use mod_ker_proper  
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: itask

  select case ( itask )

  case ( 1_ip)
     !
     ! Allocate laws
     !
     allocate( densi_ker % wlaws(nmate) )
     allocate( densi_ker % ilaws(nmate) )
     allocate( densi_ker % rlaws(mlapa_ker,nmate) )
     allocate( densi_ker % value_default(nmate) )

     allocate( visco_ker % wlaws(nmate) )
     allocate( visco_ker % ilaws(nmate) )
     allocate( visco_ker % rlaws(mlapa_ker,nmate) )
     allocate( visco_ker % value_default(nmate) )

     allocate( poros_ker % wlaws(nmate) )
     allocate( poros_ker % ilaws(nmate) )
     allocate( poros_ker % rlaws(mlapa_ker,nmate) )
     allocate( poros_ker % value_default(nmate) )

     allocate( condu_ker % wlaws(nmate) )
     allocate( condu_ker % ilaws(nmate) )
     allocate( condu_ker % rlaws(mlapa_ker,nmate) )
     allocate( condu_ker % value_default(nmate) )

     allocate( sphea_ker % wlaws(nmate) )
     allocate( sphea_ker % ilaws(nmate) )
     allocate( sphea_ker % rlaws(mlapa_ker,nmate) )
     allocate( sphea_ker % value_default(nmate) )

     allocate( dummy_ker % wlaws(nmate) )
     allocate( dummy_ker % ilaws(nmate) )
     allocate( dummy_ker % rlaws(mlapa_ker,nmate) )
     allocate( dummy_ker % value_default(nmate) )

     allocate( turmu_ker % wlaws(nmate) )
     allocate( turmu_ker % ilaws(nmate) )
     allocate( turmu_ker % rlaws(mlapa_ker,nmate) )
     allocate( turmu_ker % value_default(nmate) )

     !
     ! Put default values to negative value to identfy if the default
     ! exists or not
     ! 
     densi_ker % value_default(1:nmate) = -1.0_rp
     visco_ker % value_default(1:nmate) = -1.0_rp
     poros_ker % value_default(1:nmate) = -1.0_rp
     condu_ker % value_default(1:nmate) = -1.0_rp
     sphea_ker % value_default(1:nmate) = -1.0_rp
     dummy_ker % value_default(1:nmate) = -1.0_rp
     turmu_ker % value_default(1:nmate) = -1.0_rp

  case ( 2_ip ) 

     if( INOTMASTER ) then
        !
        ! Law name vs law number
        !
        if( densi_ker % kfl_exist == 1 ) call ker_wiprop(densi_ker)
        if( visco_ker % kfl_exist == 1 ) call ker_wiprop(visco_ker)
        if( poros_ker % kfl_exist == 1 ) call ker_wiprop(poros_ker)
        if( condu_ker % kfl_exist == 1 ) call ker_wiprop(condu_ker)
        if( sphea_ker % kfl_exist == 1 ) call ker_wiprop(sphea_ker)
        if( dummy_ker % kfl_exist == 1 ) call ker_wiprop(dummy_ker)
        if( turmu_ker % kfl_exist == 1 ) call ker_wiprop(turmu_ker)
        !
        ! Allocate memory for properties according to law 
        !
        if( densi_ker % kfl_exist == 1 ) call ker_allpro(densi_ker)
        if( visco_ker % kfl_exist == 1 ) call ker_allpro(visco_ker)
        if( poros_ker % kfl_exist == 1 ) call ker_allpro(poros_ker)
        if( condu_ker % kfl_exist == 1 ) call ker_allpro(condu_ker)
        if( sphea_ker % kfl_exist == 1 ) call ker_allpro(sphea_ker)
        if( dummy_ker % kfl_exist == 1 ) call ker_allpro(dummy_ker)
        if( turmu_ker % kfl_exist == 1 ) call ker_allpro(turmu_ker)
     end if

  end select

end subroutine ker_mempro
 
