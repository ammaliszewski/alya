!-----------------------------------------------------------------------
!> @addtogroup KermodInput
!> @{
!> @file    ker_readat.f90
!> @author  Guillaume Houzeaux
!> @date    18/09/2012
!> @brief   Read kermod data.
!> @details Read kermod data.
!> @} 
!-----------------------------------------------------------------------
subroutine ker_readat()
  use def_kintyp
  use def_master
  use def_kermod
  use def_inpout
  use def_domain
  use mod_opebcs
  implicit none
  integer(ip) :: imodu,jmodu,idime,imate,ipara,ipoin,ifunc,kdime
  integer(ip) :: iboun,dummi,ktype,lexis_mm(nelty),ifunp,nfunp
  real(rp)    :: dummr,scawi(3)
  !
  !-----------------------------------------------------------------------
  !
  ! Boundary conditions
  !
  ! Node
  call opnbcs(1_ip,5_ip,dummi,dummi,tncod_ker)              ! Memory for structure
  call opnbcs(2_ip,1_ip, 1_ip, 0_ip,tncod_ker)              ! Memory for mesh deformation/smoothing
  call opnbcs(2_ip,2_ip, 1_ip, 1_ip,tncod_ker)              ! Memory for wall distance
  call opnbcs(2_ip,3_ip, 1_ip, 1_ip,tncod_ker)              ! Memory for deformation
  call opnbcs(2_ip,4_ip, 1_ip, 1_ip,tncod_ker)              ! Memory for mesh support surface
  call opnbcs(2_ip,5_ip, 1_ip, 1_ip,tncod_ker)              ! Memory for wall normal
  ! Boundary
  call opbbcs(0_ip,1_ip, 1_ip,tbcod_ker)                    ! Memory for mesh deformation/smoothing
  call opbbcs(0_ip,2_ip, 1_ip,tbcod_ker)                    ! Memory for wall distance   
  call opbbcs(0_ip,5_ip, 1_ip,tbcod_ker)                    ! Memory for wall normal   
  ! Geometrical
  call opnbcs(0_ip,1_ip, 1_ip, 0_ip,tgcod_ker)              ! Memory for mesh deformation/smoothing
  call opnbcs(0_ip,2_ip, 1_ip, 0_ip,tgcod_ker)              ! Memory for wall distance   
  
  if( INOTSLAVE ) then
     !
     ! Physical problem
     !
     kfl_vefun     =  0
     kfl_rough     = -1                                     ! No roughness 
     kfl_canhe     = -1                                     ! No canopy
     kfl_heiov     = -1                                     ! No height over terrain 
     kfl_delta     =  0                                     ! Ways to calculate wall distance 
     kfl_ustar     =  0                                     ! Ways to calculate U* (=0: Reichart, 1=ABL, 2=ABL2) 
     kfl_walld     =  0                                     ! Wall distance not needed
     kfl_walln     =  0                                     ! Wall normal not needed
     kfl_suppo     =  0                                     ! Support geometry for MM
     kfl_extro     =  0                                     ! Roughness extension
     kfl_prope     =  0                                     ! Kermod in charge of properties    
     kfl_kemod_ker =  0                                     ! kermod k eps modification
     denme         = -1.0_rp                                ! Medium density
     visme         = -1.0_rp                                ! Viscosity medium
     gasco         =  8.3144621_rp                          ! Gas constant
     u_ref         = -1.0_rp                                ! Reference velocity
     h_ref         = -1.0_rp                                ! Reference height
     k_ref         = -1.0_rp                                ! Reference roughness
     usref         = -1.0_rp                                ! Reference ustar
     windg         =  0.0_rp                                ! Geostrophic wind (CFDWind2 model)
     delta_dom     =  0.0_rp                                ! Distance to the wall
     delmu_dom     =  1.0_rp                                ! Multiplier factor for variable wall distance for law of the wall
     rough_dom     =  0.0_rp                                ! Wall roughness
     grnor         =  0.0_rp                                ! Gravity norm
     gravi         =  0.0_rp                                ! Gravity vector
     thicl         = -1.0_rp                                ! Thickness of free surface
     cmu_st        =  0.0_rp                                ! Cmu for turbul k-eps, needed in Nastin b.c
     !
     ! Numerical treatment 
     !
     kfl_renum     = 0                                      ! Renumbering strategy
     ndivi         = 0                                      ! Number of divisions
     kfl_rotation_axe = 0                                   ! Axes of rotation of the mesh
     kfl_graph     = 0                                      ! Extended graph
     kfl_grpro     = 1                                      ! Gradient projection (1=closed,0=open)
     kfl_conbc_ker = 1                                      ! Constant kernel b.c.
     kfl_elses     = 0                                      ! Elsest 
     ielse(1)      = 100                                    ! nx
     ielse(2)      = 100                                    ! ny
     ielse(3)      = 100                                    ! nz
     ielse(4)      = 1                                      ! data format (0=type,1=linked list)
     ielse(5)      = 10                                     ! Maximum number of possible meshes
     ielse(6)      = 1                                      ! Not used 
     ielse(7)      = 0                                      ! Output unit
     ielse(8)      = 1                                      ! Search strategy (0=bin,1=Quad)
     ielse(9)      = 100                                    ! Points per node for Quad/Octtree
     ielse(10)     = 0                                      ! Result output frequency 
     ielse(11)     = 1                                      ! Neighboring-boxes-search radius, 0: search until all boxes finished
     ielse(12)     = 0                                      ! Postprocess mesh
     ielse(13)     = 0                                      ! Postprocess results
     ielse(14)     = 0                                      ! Dont check
     ielse(15)     = 0                                      ! Dont force

     relse(1)      = 0.01_rp                                ! Tolerance for iteration
     rotation_angle = 0.0_rp                                ! Angle of rotation of the mesh (in degrees)

     kfl_cutel     = 0                                      ! If cut elements exist
     kfl_hangi     = 0                                      ! If hanging node exist
     kfl_lapla     = 1                                      ! Laplacians are computed
     kfl_defor     = 0                                      ! Do not deform mesh
     element_bin_boxes(1:3) = 4                             ! Element bin
     npoin_mm      = 0                                      ! Support geometry for MM: number of nodes
     nboun_mm      = 0                                      ! Support geometry for MM: number of boundaries
     mnodb_mm      = ndime                                  ! Support geometry for MM: max number of nodes per boundary
     number_space_time_function = 0                         ! # of space/time functions
     number_time_function       = 0                         ! # of time functions

     do ifunc = 1,max_space_time_function
        space_time_function(ifunc) % ndime = 1              ! Space/time function dimension
        space_time_function(ifunc) % nexpr = 0              ! Space/time function size of the expression
        space_time_function(ifunc) % name  = ' '            ! Space/time function name
     end do
     do ifunc = 1,max_time_function
        time_function(ifunc) % npara = 1                    ! Time function dimension
        time_function(ifunc) % kfl_type = 0                 ! Time function size of the expression
        time_function(ifunc) % name  = ' '                  ! Time function name
     end do
     deformation_steps    = 1                               ! Number of deformation steps (when KFL_DEFOR /= 0 )
     deformation_strategy = 6                               ! Deformation strategy
     kfl_duatss = 0                                         ! No dual time stepping as preconditioner
     fact_duatss = 10                                       ! Make time step 10 times smaller 
     !
     ! Output and postprocess
     !
     nwitn              =  0                                ! # witness points
     mwitn              =  50                               ! Max # witness points
     kfl_posdo          =  1                                ! Domain postprocess (default)
     kfl_posdi          =  0                                ! Postprocess on original level
     kfl_oumes          =  1                                ! Output mesh
     kfl_oustl          =  0                                ! Output boundary mesh STL format
     kfl_quali          =  0                                ! Mesh quality 
     npp_stepo          = -1                                ! Do not step over the defined postprocesing steps
     nfilt              =  0                                ! # filters
     kfl_abovx          =  0                                ! Automatic voxel bounding box
     resvx              =  32                               ! Voxel resolution: 32 x 32 x 32
     kfl_vortx          =  0                                ! Vortex extraction option off
     kfl_vortx_thres    =  0                                ! Vortex threshold option off
     kfl_detection      =  0                                ! Automatic detection 
     kfl_pixel          =  0                                ! Output ppm format
     plane_pixel        =  1                                ! Plane along x,y,z 
     variable_pixel     =  0                                ! Variable to postprocess
     number_pixel       =  50                               ! Numebr of pixels
     thr_veloc          =  0.1_rp                           ! Vortex threshold velocity 
     thr_qvort          =  10.0_rp                          ! Vortex threshold qvort
     travx              =  0.0_rp                           ! Translation
     bobvx              =  0.0_rp                           ! Voxel Bounding box corners 
     scawi              =  1.0_rp                           ! Scale for witness points
     detection_length   =  1.0_rp                           ! Characteristic length for detection
     detection_velocity =  1.0_rp                           ! Characteristic velocity for detection
     coord_plane_pixel  =  0.0_rp                           ! Coordinate of the plane

     !-------------------------------------------------------------------
     !
     ! Read/write unit
     !
     !-------------------------------------------------------------------

     if( momod(modul) % lun_pdata <= 0 ) then
        call livinf(0_ip,'KERMOD FILE DOES NOT EXITS: USE DEFAULT OPTIONS',0_ip)
        return
     end if

     lispa = 0
     lisda = momod(modul) % lun_pdata ! Reading file
     lisre = momod(modul) % lun_outpu ! Writing file
     !--><group>
     !-->       <groupName>Physical_problem</groupName>
     !-->       <subGroup>
     !-->           <inputLine>
     !-->               <inputLineName>PRE_PENALIZATION</inputLineName>
     !-->               <inputLineHelp>Penalize pressure in time. To be used when converging to a steady state when the pressure presents an odd-even decoupling in time. This option helps converging by adding a damping effect.</inputLineHelp>
     !-->               <inputElement>
     !-->                   <inputElementType>edit</inputElementType>
     !-->                   <inputLineEditName>VALUE</inputLineEditName>
     !-->                   <inputLineEditValue>5</inputLineEditValue>
     !-->               </inputElement>
     !-->           </inputLine>
     !-->       </subGroup>
     !-->   </group>
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Physical problem
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> PHYSICAL_PROBLEM
     ! ADOC[d]> <b>PHYSICAL_PROBLEM</b>:
     ! ADOC[d]> Define the physical problem of kermod module. This field contains some variables shared by 
     ! ADOC[d]> the modules, like physical properties, distance to the wall, wall laws, etc.
     !
     call ecoute('ker_readat')
     do while( words(1) /= 'PHYSI' )
        call ecoute('ker_readat')
     end do
     call ecoute('ker_readat')

     do while(words(1) /= 'ENDPH' )

        if( words(1) == 'VELOC' ) then
           !
           ! ADOC[1]> VELOCITY: OFF/HELIC/FUNCTION= int
           ! ADOC[d]> <b>VELOCITY</b>: In case a velocity field is needed, for example to transport particles
           ! ADOC[d]> with PARTIS module, and no module solves for the velocity, one can define a velocity
           ! ADOC[d]> field.
           !
           if( words(2) == 'OFF  ' ) then
              kfl_vefun = 0
           else if( words(2) == 'HELIC' ) then
              kfl_vefun = 7
           else if( words(2) == 'FIELD' ) then
              kfl_vefun = -getint('FIELD',-1_ip,'#VELOCITY FROM A FIELD')
           else 
              if( exists('FUNCT') ) &
                   kfl_vefun = getint('FUNCT',1_ip,'#VELOCITY FUNCTION FOR LAGRANGIAN PARTICLE')
           end if

        else if( words(1) == 'PROPE' ) then
           !
           ! ADOC[1]> PROPERTIES: 
           ! ADOC[2]> MATERIAL= int
           ! ADOC[3]> DENSITY:       CONSTANT/BIFLUID/LOW_MACH/K_LOW_MACH,MIXTURE  PARAMETERS= real1, real2..., DEFAULT_VALUE= real3
           ! ADOC[3]> VISCOSITY:     CONSTANT/BIFLUID/SUTHERLAND/MU_MIXTURE,       PARAMETERS= real1, real2..., DEFAULT_VALUE= real3
           ! ADOC[3]> POROSITY:      CONSTANT/CANOPY,                              PARAMETERS= real1, real2..., DEFAULT_VALUE= real3
           ! ADOC[3]> CONDUCTIVITY:  CONSTANT/SUTHERLAND/K_MIXTURE,                PARAMETERS= real1, real2..., DEFAULT_VALUE= real3
           ! ADOC[3]> SPECIFIC_HEAT: CONSTANT/CP_MIXTURE,                          PARAMETERS= real1, real2..., DEFAULT_VALUE= real3
           ! ADOC[2]> END_MATERIAL
           ! ADOC[1]> END_PROPERTIES: 
           ! ADOC[d]> <b>PROPERTIES</b>: 
           ! ADOC[d]> For each material (MATERIAL= int), the properties laws and values are defined.
           ! ADOC[d]> If the property is constant, then only one parameter is required.
           ! ADOC[d]> For example: DENSITY: CONSTANT, PARAMETERS=1.2047
           ! ADOC[d]> Default values may be required because properties can be needed before
           ! ADOC[d]> for computing the initial values of the unknowns
           ! 
           ! Properties
           !
           call ecoute('ker_readat')
           imate = 1
           do while( words(1) /= 'ENDPR' ) 
              if( words(1) == 'MATER' ) then
                 imate = getint('MATER',1_ip,'#MATERIAL NUMBER')
                 if( imate < 1 .or. imate > nmate ) call runend('KER_READAT: WRONG MATERIAL')
              end if

              if( words(1) == 'DENSI' ) then
                 densi_ker % kfl_exist                = 1
                 if (exists('NASTA')) then
                    densi_ker % wlaws(imate)          = 'DNAST'
                 else if (exists('LOWMA') .and. exists('MIXTU')) then
                    densi_ker % wlaws(imate)          = 'KLOWM'
                 else if (exists('LOWMA') .and. exists('GAUSS')) then
                    densi_ker % wlaws(imate)          = 'LOWMG'
                 else if (exists('LOWMA') .and. exists('GMIXT')) then
                    densi_ker % wlaws(imate)          = 'GKLOW'
                 else
                    densi_ker % wlaws(imate)          = words(2)
                 endif
                 densi_ker % rlaws(1:mlapa_ker,imate) = param(3:2+mlapa_ker)
                 densi_ker % value_default(imate)     = getrea('DEFAU',-1.0_rp,'#DEFAULT DENSITY')

              else if( words(1) == 'VISCO' ) then
                 visco_ker % kfl_exist                = 1
                 if (exists('MIXTU')) then
                    visco_ker % wlaws(imate)          = 'MUMIX'
                 else if (exists('TABLE')) then
                    visco_ker % wlaws(imate)          = 'MUTAB'
                 else if (exists('SUTHE') .and. exists('GAUSS')) then  ! Modifier GAUSS_POINT goes at end of line
                    visco_ker % wlaws(imate)          = 'GPSUT'
                 else if (exists('ABL  ') ) then                       ! Atmospheric boundary layer
                    visco_ker % wlaws(imate)          = 'ABL  '
                 else
                    visco_ker % wlaws(imate)          = words(2)
                 end if
                 visco_ker % rlaws(1:mlapa_ker,imate) = param(3:2+mlapa_ker)
                 visco_ker % value_default(imate)     = getrea('DEFAU',-1.0_rp,'#DEFAULT VISCOSITY')

              else if( words(1) == 'POROS' ) then
                 poros_ker % kfl_exist                = 1
                 poros_ker % wlaws(imate)             = words(2) ! can be const or canopy
                 poros_ker % rlaws(1:mlapa_ker,imate) = param(3:2+mlapa_ker) 
                 poros_ker % value_default(imate)     = getrea('DEFAU',-1.0_rp,'#DEFAULT POROSITY') 
                 !check
                 if ( poros_ker % wlaws(imate)=='CANOP'.and. &
                      (poros_ker % rlaws(4,imate).lt.-epsilon(1.0_rp).or.&
                      poros_ker%rlaws(4,imate).gt.1.0+epsilon(1.0_rp)) ) &
                      call runend('KER_READAT:WRONG CANOPY ZMAX PARAM, SHOULD BE BETWEEN 0.0 AND 1.0' )

              else if( words(1) == 'CONDU' ) then
                 condu_ker % kfl_exist                = 1
                 if (exists('MIXTU')) then
                    condu_ker % wlaws(imate)          = 'KMIXT'
                 else if (exists('TABLE')) then
                    condu_ker % wlaws(imate)          = 'KTABL'
                 else if (exists('SUTHE') .and. exists('GAUSS')) then  ! Modifier GAUSS_POINT goes at end of line
                    condu_ker % wlaws(imate)          = 'GPSUT'
                 else
                    condu_ker % wlaws(imate)          = words(2)
                 end if
                 condu_ker % rlaws(1:mlapa_ker,imate) = param(3:2+mlapa_ker) 
                 condu_ker % value_default(imate)     = getrea('DEFAU',-1.0_rp,'#DEFAULT CONDUCTIVITY')

              else if( words(1) == 'SPECI' ) then
                 sphea_ker % kfl_exist                = 1
                 if (exists('MIXTU')) then
                    sphea_ker % wlaws(imate)             = 'CPMIX'
                 else if (exists('TABLE')) then
                    sphea_ker % wlaws(imate)             = 'CPTAB'
                 else
                    sphea_ker % wlaws(imate)             = words(2)
                 endif
                 sphea_ker % rlaws(1:mlapa_ker,imate) = param(3:2+mlapa_ker) 
                 sphea_ker % value_default(imate)     = getrea('DEFAU',-1.0_rp,'#DEFAULT SPECIFIC HEAT')

              else if( words(1) == 'DUMMY' ) then
                 dummy_ker % kfl_exist                = 1
                 dummy_ker % wlaws(imate)             = words(2)
                 dummy_ker % rlaws(1:mlapa_ker,imate) = param(3:2+mlapa_ker) 
                 dummy_ker % value_default(imate)     = getrea('DEFAU',-1.0_rp,'#DEFAULT DUMMY')
              
              else if( words(1) == 'TURBU' ) then
                 turmu_ker % kfl_exist                = 1
                 if (exists('SMAGO')) then
                    turmu_ker % wlaws(imate)          = 'SMAGO'
                 else if (exists('WALE ')) then
                    turmu_ker % wlaws(imate)          = 'WALE '
                 else
                    turmu_ker % wlaws(imate)          = words(2)
                 end if
                 turmu_ker % rlaws(1:mlapa_ker,imate) = param(3:2+mlapa_ker)
                 turmu_ker % value_default(imate)     = getrea('DEFAU',-1.0_rp,'#DEFAULT TURBULENT VISCOSITY')
              end if

              call ecoute('ker_readat')
           end do

        else if( words(1) == 'DENSI' ) then
           !
           ! Density
           !
           denme = getrea('DENSI',0.0_rp,'#MEDIUM DENSITY')

        else if( words(1) == 'VISCO' ) then
           !
           ! Viscosity
           !
           visme = getrea('VISCO',0.0_rp,'#MEDIUM VISCOSITY')

        else if( words(1) == 'GRAVI' ) then
           !
           ! ADOC[1]> GRAVITY: NORM= real1, GX= real2, GY= real2, GZ= real3                         $ Gravity acceleration
           ! ADOC[d]> <b>GRAVITY:</b> Define the gravity vector 
           ! ADOC[d]> g = real1*(real2,real3,real4)/|(real2,real3,real4)|
           !
           ! Gravity
           !
           grnor    = getrea('NORM ',0.0_rp,'#Gravity norm')
           gravi(1) = getrea('GX   ',0.0_rp,'#x-component of g')
           gravi(2) = getrea('GY   ',0.0_rp,'#y-component of g')
           gravi(3) = getrea('GZ   ',0.0_rp,'#z-component of g')
           call vecuni(3_ip,gravi,dummr)

        else if( words(1) == 'THICK') then
           !
           ! ADOC[1]> THICKNESS: real                                                               $ Thickness of free surface
           ! ADOC[d]> <b>THICKNESS:</b> The thickness is the size over which the properties
           ! ADOC[d]> are smoothed out. It is usually taken as the size of three elements in the
           ! ADOC[d]> zone of interest.
           !
           ! Interface thickness
           !          
           thicl = getrea('THICK',0.0_rp,'#Interface thickness')

        else if( words(1) == 'GASCO' ) then
           !
           ! ADOC[1]> GAS_CONSTANT: real                                                            $ Gas constant (example: 8.3144621 J/K)
           ! ADOC[d]> <b>GAS_CONSTANT:</b> gas constant to be used in perfect gas law.
           ! ADOC[d]> For example, R=8.3144621 J/K.
           !
           ! Gas constant R
           !
           gasco = getrea('GASCO',8.3144621_rp,'#GAS CONSTANT') ! Default in J/K

        else if( words(1) == 'UREFE' ) then
           !
           ! Reference velocity
           !
           u_ref = getrea('UREFE',-1.0_rp,'#Reference velocity')

        else if( words(1) == 'HREFE' ) then
           !
           ! Reference height
           !
           h_ref = getrea('HREFE',-1.0_rp,'#Reference height')

        else if( words(1) == 'KREFE' ) then
           !
           ! Reference roughness
           !
           k_ref = getrea('KREFE',-1.0_rp,'#Reference roughness')

        else if( words(1) == 'USREF' ) then
           !
           ! Reference ustar
           !
           usref = getrea('USREF',-1.0_rp,'#Reference ustar')

        else if( words(1) == 'GEOST' ) then
           !
           ! Geostrophic wind (CFDWind2 model)
           !
           if( nnpar > 3 .or. nnpar < 2 ) call runend('KER_READAT: WRONG GEOSTROPHIC WIND DIMENSION')
           do ipara = 1,nnpar
              windg(ipara) = param(ipara)
           end do

        else if( words(1) == 'ROUGH' ) then
           !
           ! ADOC[1]> ROUGHNESS: CONSTANT, VALUE= real/FIELD=int                                    $ Roughness of wall-like surfaces
           ! ADOC[d]> <b>ROUGHNESS</b>: roughness of the surfaces. This affects the wall law and therefore
           ! ADOC[d]> only boundaries with a wall law condition. The rougness can be a constant value
           ! ADOC[d]> or can be given node-wise by a field to be defined in *.dom.dat file.
           !
           ! Roughness
           !
           if( words(2) == 'CONST' ) then
              kfl_rough = 0
              rough_dom = getrea('VALUE',-1.0_rp,'#Roughness value')
           else if( words(2) == 'FIELD' ) then
              kfl_rough = getint('FIELD',1_ip,'#Roughness field function')
           end if

        else if( words(1) == 'CANOP' ) then
           !
           ! ADOC[1]> CANOPY_HEIGHT: CONSTANT     /FIELD=int     $ Canopy height
           ! ADOC[d]> <b>CANOPY_HEIGHT</b>: Canopy height is used by porous material of type canopy
           ! ADOC[d]> It can be a constant value or can be given node-wise by a field
           ! ADOC[d]> to be defined in *.dom.dat file for nodes of canopy material.
           !
           ! Canopy height
           !
           if( words(2) == 'CONST' ) then
              kfl_canhe = 0
           else if( words(2) == 'FIELD' ) then
              kfl_canhe = getint('FIELD',1_ip,'#Canopy height function')
           end if

        else if( words(1) == 'HEIGH' ) then
           !
           ! ADOC[1]> HEIGHT_OVER_TERRAIN: WALLD  /FIELD=int     $ Height of a node over the terrain
           ! ADOC[d]> <b>HEIGHT_OVER_TERRAIN</b>: Height over the terrain is used by porous material of type canopy.
           ! ADOC[d]> It can be given node-wise by a field to be defined in *.dom.dat file for nodes of canopy material
           ! ADOC[d]> or it can be approximated by the wall distance. The wall distance approximation is a poor one 
           ! ADOC[d]> and should be removed in teh future. It is maintained to make it compatible with te initial canopy implementation. 
           !
           ! Canopy height
           !
           if( words(2) == 'WALLD' ) then
              kfl_heiov = 0
           else if( words(2) == 'FIELD' ) then
              kfl_heiov = getint('FIELD',1_ip,'#Height over the terrain function')
           end if

        else if( words(1) == 'WALLL' ) then
           !
           ! ADOC[1]> WALL_LAW:  ABL/REICHARDT, VARIABLE[MULTIPLIER= real]/WALL_DISTANCE= real      $ Law of the wall and distance to the wall
           ! ADOC[d]> <b>WALL_LAW</b>: 
           ! ADOC[d]> Definition of the law of the wall. Two informations are required. First the kind of
           ! ADOC[d]> wall law, either ABL (atmospheric boundary layer) or REICHARDT (viscous, buffer and log layer):
           ! ADOC[d]> <ul>
           ! ADOC[d]>   <li> Reichardt's law:
           ! ADOC[d]>        u^+ = 1/kap ln(1+0.4y+) + 7.8 [ 1 - exp(-y+/11) - y+/11 exp(-0.33 y+) ] </li> 
           ! ADOC[d]>   <li> ABL wall law: u^+ = 1 / 0.41 ln[ (y+k0) / k0 ] </li>
           ! ADOC[d]> </ul>
           ! ADOC[d]> Then, the distance of the boundary nodes (computational wall) to the physical wall must be prescribed. 
           ! ADOC[d]> It can be constant or variable. In the
           ! ADOC[d]> last case, the distance to the wall is computed as the size of the first element off the wall multiplied
           ! ADOC[d]> by the constant MULTIPLIER= real. By default, the wall distance of wall node is zero, meaning that the
           ! ADOC[d]> computational and physical walls coincide.
           ! 
           !
           ! Wall law
           !
           if(exists('VARIA')) then
              kfl_delta = 1   ! Variable wall distance
              delmu_dom = getrea('MULTI',1.0_rp,'Multiplier factor for variable wall distance')
           else if(exists('WALL2')) then
              kfl_delta = -1  ! convert wall-law boundaries to no slip boundaries, this is an alternative to using a negative wall distance 
           else if(exists('CONST')) then
              kfl_delta = 0   ! Constant wall distance
           end if

           if(exists('ABL  ')) then
              kfl_ustar = 1
           else if(exists('REICH')) then
              kfl_ustar = 0
           else if(exists('ABL2 ')) then !ABL imposing zero gradient of k at the wall
              kfl_ustar = 2         !And imposing ustar from k and vel at nastin
              Cmu_st =getrea('CMU  ',0.0_rp, 'Cmu constant in k turbulent equation')
              if (Cmu_st < 1.0e-6) call runend('ker_readat: Cmu needs to be positive when imposing ABL2 wall law')
           else if(exists('GRADP')) then
              kfl_ustar = 3
           end if

           if(exists('WALLD')) then
              delta_dom = getrea('WALLD',0.0_rp,'#Distance to the wall')
           end if

        else if( words(1) == 'COUPL' ) then
           !
           ! ADOC[1]> COUPLING
           ! ADOC[1]>   char1 char2 int                                                             $ Module1, Module2, coupling number; Module1 <= Module2
           ! ADOC[1]>   ...
           ! ADOC[1]> END_COUPLING
           ! ADOC[d]> <b>COUPLING:</b>
           ! ADOC[d]> This field contains the coupling information between modules.
           ! ADOC[d]> char1 and char2 are the modules and int is a number indicating
           ! ADOC[d]> the coupling procedure if different couplings exist.
           ! ADOC[d]> For example, "char1 char2 2" means that char2 modifies char1 module
           ! ADOC[d]> (char1 <= char2) using the coupling procedure number 2.
           !
           ! Coupling between modules
           ! 
           call ecoute('ker_readat')
           do while( words(1) /= 'ENDCO' )
              imodu = idmod(words(1))
              jmodu = idmod(words(2))
              if( jmodu == -1 .or. jmodu == -1 ) call runend('WRONG MODULES FOR COUPLING')
              kfl_coupl(imodu,jmodu) = max(1_ip,int(param(2)))
              !kfl_coupl(jmodu,imodu) = max(1_ip,int(param(2)))
              if( exists('FIELD') ) then
                 kfl_cowhe(imodu,jmodu) = getint('FIELD',1_ip,'#Field where to couple') 
              else if( exists('ONFIE') ) then  
                 kfl_cowhe(imodu,jmodu) = getint('ONFIE',1_ip,'#Field where to couple') 
              end if
              call ecoute('ker_readat')
           end do

        end if
        call ecoute('ker_readat')
        !
        ! ADOC[0]> END_PHYSICAL_PROBLEM
        !
     end do
     !--><group>
     !-->       <groupName>NUMERICAL_TREATMENT</groupName>
     !-->       <subGroup>
     !-->           <inputLine>
     !-->               <inputLineName>PRE_PENALIZATION</inputLineName>
     !-->               <inputLineHelp>Penalize pressure in time. To be used when converging to a steady state when the pressure presents an odd-even decoupling in time. This option helps converging by adding a damping effect.</inputLineHelp>
     !-->               <inputElement>
     !-->                   <inputElementType>edit</inputElementType>
     !-->                   <inputLineEditName>VALUE</inputLineEditName>
     !-->                   <inputLineEditValue>5</inputLineEditValue>
     !-->               </inputElement>
     !-->           </inputLine>
     !-->       </subGroup>
     !-->   </group>
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Numerical treatment
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> NUMERICAL_TREATMENT
     ! ADOC[d]> <b>NUMERICAL_TREATMENT</b>:
     ! ADOC[d]> Define the numerical treatment of kermod module. This field contains some options
     ! ADOC[d]> like mesh multiplication or about the global arrays governed by PDE's needed by different modules
     ! ADOC[d]> (like the generalized distance to the wall), etc. The difference of KERMOD with other MODULES
     ! ADOC[d]> is that when arrays are computed as a PDE, the boundary conditions are prescribed in the
     ! ADOC[d]> numerical treatment field and not in a separate field.
     !
     ! Reach the section
     !      
     do while( words(1) /= 'NUMER' )
        call ecoute('ker_readat')
     end do
     call ecoute('ker_readat')
     do while( words(1) /= 'ENDNU' )

        if( words(1) == 'MESH ' ) then 
           !
           ! ADOC[1]> MESH
           ! ADOC[d]> <b>MESH</b>: 
           ! ADOC[d]> Options concerning the mesh.
           !
           call ecoute('ker_readat')
           do while(words(1) /= 'ENDME' )

              if( words(1) == 'MULTI' ) then
                 !
                 ! ADOC[2]> MULTIPLICATION= int                                                        $ Number of mesh multiplication levels. 0 means do not multiply
                 ! ADOC[d]> <b>MULTIPLICATION</b>: 
                 ! ADOC[d]> Number of mesh multiplication levels. 0 means do not multiply. Postprocess can be carried
                 ! ADOC[d]> out on original mesh or on the last mesh of the multiplication process. It is strongly
                 ! ADOC[d]> advised to use extrapolated boundary conditions on boundaries so that the boundary conditions
                 ! ADOC[d]> on nodes are uniquely defined.
                 !
                 ! Mesh division
                 !
                 ndivi = getint('MULTI',1_ip,'#Mesh divisions') 

              else if( words(1) == 'DIVIS' ) then

                 ndivi = getint('DIVIS',1_ip,'#Mesh divisions') 

              else if( words(1) == 'ROTAT' ) then

                 rotation_angle = getrea('ANGLE',0.0_rp,'#ANGLE OF ROTATION OF THE MESH')
                 if( exists('X    ') ) then
                    kfl_rotation_axe = 1
                 else if( exists('Y    ') ) then
                    kfl_rotation_axe = 2
                 else if( exists('Z    ') ) then
                    kfl_rotation_axe = 3
                 else
                    call runend('KER_READAT: WRONG ROTATION AXE')
                 end if

              else if( words(1) == 'EXTEN' ) then
                 ! ADOC[2]> EXTEND_GRAPH: OFF/ON
                 if( option('EXTEN') ) kfl_graph = 1

              else if( words(1) == 'RENUM' ) then 
                 !         
                 ! Renumbering
                 !
                 if( option('RENUM') ) kfl_renum = 1

              else if( words(1) == 'ELEME' ) then 
                 !
                 ! Element bin
                 !
                 element_bin_boxes(1)   = getint('NUMBE',1_ip,'#NUMBER OF BINS IN EACH DIRECTION')
                 element_bin_boxes(2:3) = element_bin_boxes(1)

              else if( words(1) == 'SUPPO' ) then
                 !
                 ! ADOC[2]> SUPPORT_GEOMETRY, NODES=int1, BOUNDARIES=int2                              $ Number of mesh multiplication levels. 0 means do not multiply
                 ! ADOC[3]>   COORDINATES
                 ! ADOC[4]>   ...
                 ! ADOC[3]>   END_COORDINATES
                 ! ADOC[3]>   BOUNDARIES
                 ! ADOC[4]>   ...
                 ! ADOC[3]>   END_BOUNDARIES
                 ! ADOC[2]> END_SUPPORT_GEOMETRY   
                 ! ADOC[d]> <b>SUPPORT_GEOMETRY</b>
                 !
                 kfl_suppo = 1
                 npoin_mm  = getint('NODES',0_ip,'#NUMBER OF NODES OF THE SUPPORT GEOMETRY')
                 nboun_mm  = getint('BOUND',0_ip,'#NUMBER OF NODES OF THE BOUNDARIES GEOMETRY')
                 ktype     = 0
                 if( npoin_mm + nboun_mm == 0 ) call runend('KER_READAT: WRONG DIMENSIONS OF THE SUPPORT GEOMETRY')  
                 call ker_memory(5_ip)

                 call ecoute('ker_readat')
                 do while( words(1) /= 'ENDSU' )

                    if( words(1) == 'COORD' ) then
                       !
                       ! Coordinate COORD_MM(:,:)
                       !
                       call ecoute('ker_readat')
                       do while( words(1) /= 'ENDCO')
                          ipoin = int(param(1),ip)
                          if( ipoin < 0 .or. ipoin > npoin_mm ) then
                             call runend('KER_READAT: WRONG SUPPORT GEOMETRY COORDINATES')
                          end if
                          coord_mm(1:ndime,ipoin) = param(2:1+ndime)
                          call ecoute('ker_readat')
                       end do

                    else if( words(1) == 'TYPES' ) then
                       !
                       ! Connectivity LTYPB_MM(:,:)
                       !
                       call reatyp(0_ip,nboun_mm,ktype,ltypb_mm,lexis_mm)

                    else if( words(1) == 'BOUND' ) then
                       !
                       ! Connectivity LNODB_MM(:,:)
                       !
                       call ecoute('ker_readat')
                       do while( words(1) /= 'ENDBO')
                          iboun = int(param(1))
                          if( iboun < 0 .or. iboun > nboun_mm ) then
                             call runend('KER_READAT: WRONG SUPPORT BOUNDARY CONNECTIVITY')
                          end if
                          if( nnpar-1 /= mnodb_mm ) call runend('KER_READAT: WRONG SUPPORT BOUNDARY CONNECTIVITY')
                          lnodb_mm(1:mnodb_mm,iboun) = int(param(2:1+mnodb_mm),ip)
                          call ecoute('ker_readat')
                       end do

                    else if( words(1) == 'ALGEB' ) then
                       !
                       ! Algebraic solver
                       !                       
                       solve_sol => solve(3:)
                       call reasol(1_ip)

                    end if

                    call ecoute('ker_readat')
                 end do

              else if( words(1) == 'DEFOR' ) then
                 !
                 ! ADOC[2]> DEFORMATION
                 ! ADOC[3]>   METHOD: 1/2/3/4/5/6
                 ! ADOC[3]>   STEPS = 1
                 ! ADOC[3]>   ALGEBRAIC_SOLVER
                 ! ADOC[4]>   ...
                 ! ADOC[3]>   END_ALGEBRAIC_SOLVER
                 ! ADOC[2]> END_DEFORMATION
                 ! ADOC[d]> <b>SUPPORT_GEOMETRY</b>
                 !
                 kfl_defor = 1
                 call ecoute('ker_readat')
                 do while( words(1) /= 'ENDDE' )

                    if( words(1) == 'METHO' ) then
                       !
                       ! Method
                       !
                       call runend('KER_READAT: NOT CODED')

                    else if( words(1) == 'STEPS' ) then
                       !
                       ! Steps
                       !
                       deformation_steps = getint('STEPS',1_ip,'#NUMBER OF DEFORMAITON STEPS')

                    else if( words(1) == 'CODES' .and. exists('NODES') ) then
                       !
                       ! ADOC[2]> CODES, NODES
                       ! ADOC[2]>   ...
                       ! ADOC[2]> END_CODES
                       !
                       tncod => tncod_ker(3:)
                       call reacod(1_ip)
                       
                    else if( words(1) == 'ALGEB' ) then
                       !
                       ! Algebraic solver
                       !                       
                       solve_sol => solve(4:)
                       call reasol(1_ip)

                    end if

                    call ecoute('ker_readat')
                 end do

              end if

              call ecoute('ker_readat')

           end do
           !
           ! ADOC[1]> END_MESH
           ! 
        else if( words(1) == 'GRADI' ) then 
           !
           ! ADOC[1]> GRADIENT_PROJECTION: OPEN/CLOSE                                               $ Projection of gradients using open or close rule 
           ! ADOC[d]> <b>GRADIENT_PROJECTION</b>:
           ! ADOC[d]> Gradients are computed using an L2 projection, by solving the diagonal system
           ! ADOC[d]> M gi = \int du/dxi v dx. The option determines if the diagonal mass matrix M
           ! ADOC[d]> and the RHS are computed using a close rule or an open rule. In the last case,
           ! ADOC[d]> the mass matrix is lumped.
           !         
           ! Gradient projection
           !
           if( words(2) == 'OPEN ' .or. words(2) == 'OPENE' ) then
              kfl_grpro = 0
           else
              kfl_grpro = 1
           end if

        else if( words(1) == 'DUALT' ) then 
           !
           ! ADOC[1]> DUAL_TIME_STEP_PRECONDITIONER FACTOR/OFF 10             
           ! ADOC[d]> <b>DUAL_TIME_STEP_PRECONDITIONER </b>:
           !         
           kfl_duatss = 1
           if( words(2) == 'FACTO') then
              fact_duatss = getint('FACTO',10_ip,'#Factor for dual time step precond')
           else
              kfl_duatss = 0
           end if
           print*,'kfl_duatss,fact_duatss',kfl_duatss,fact_duatss

        else if( words(1) == 'ELSES' ) then
           !
           ! ADOC[1]> ELSEST
           ! ADOC[d]> <b>ELSEST</b>:
           ! ADOC[d]> Element search strategy. Alya may need to find the host element of some points,
           ! ADOC[d]> as this is the case of WITNESS_POINTS. The element search strategy can be tuned
           ! ADOC[d]> according to the mesh.
           ! ADOC[d]> Two strategies are available. The bin strategy is adapted to isotropic meshes.
           ! ADOC[d]> It consists in covering the computational volume by a Cartesian grid, and fill
           ! ADOC[d]> each box/cell with the elements which embedding box crosses the cell. The oct-tree strategy is more
           ! ADOC[d]> adapted to anisotropic meshes and creates a tree-like structure of boxes
           ! ADOC[d]> to accelerate the element search. For the bin strategy, the number of boxes
           ! ADOC[d]> in each direction should be prescribed in NUMBER_BOXES. For the oct-tree strategy, 
           ! ADOC[d]> the user must give the maximum number of nodes (MAXIMUM_NUMBER) to fill the boxes. If the number of nodes
           ! ADOC[d]> exceeds this figure, then the box is further divided. If not, the box is filled with
           ! ADOC[d]> the elements  which embedding box crosses the box. The TOLERANCE option gives the tolerance to
           ! ADOC[d]> accept or reject a host element. A velue of 0.01 means that a host element is accepted
           ! ADOC[d]> if the node isoparametric coordinates in the element are within 1% of the size of the element (=1).
           !
           ! Elsest
           !
           kfl_elses=1
           call ecoute('ker_readat')
           do while( words(1) /= 'ENDEL')

              if( words(1) == 'STRAT' ) then
                 !
                 ! ADOC[2]> STRATEGY: BIN/OCT_TREE                                                     $ Bin or quad/oct tree strategy
                 !
                 if( words(2) == 'BIN  ' ) then
                    ielse(8) = 0
                 else
                    ielse(8) = 1
                 end if

              elseif( words(1) == 'NUMBE' ) then
                 !
                 ! ADOC[2]> NUMBER_BOXES: int, int, int                                                $ Number of boxes in x,y,z directions for bin strategy
                 !
                 do idime = 1,3
                    ielse(idime) = max(int(param(idime),ip),1_ip)
                 end do

              else if( words(1) == 'MAXIM' ) then
                 !
                 ! ADOC[2]> MAXIMUM_NUMBER: int                                                        $ Max number of nodes in a box. 
                 !
                 ielse(9) = getint('MAXIM',1_ip,'#MAXIMUM NUMBER OF NODES PER QUAD/OCT')

              else if( words(1) == 'OUTPU' ) then
                 !
                 ! ADOC[2]> OUTPUT: YES/NO                                                             $ Postprocess the bin or oct-tree (in GiD format)
                 !
                 if( words(2) == 'ON   ' .or. words(2) == 'YES  ' ) then
                    if(exists('STATI')) ielse( 7) = lun_elsta_dom
                    if(exists('MESH ')) ielse(12) = lun_elmsh_dom
                    if(exists('RESUL')) ielse(13) = lun_elres_dom
                 end if

              else if( words(1) == 'TOLER' ) then
                 !
                 ! ADOC[2]> TOLERANCE= real                                                            $ Tolerance to accept or reject a host element
                 !
                 relse(1) = getrea('TOLER',0.01_rp,'#TOLERANCE')

              else if( words(1) == 'DATAF' ) then
                 if( words(2) == 'LINKE' ) then
                    ielse(4) = 1
                 else if( words(2) == 'TYPE ' ) then
                    ielse(4) = 0
                 end if

              else if( words(1) == 'MESHE' ) then
                 ielse(5) = getint('MESHE',1_ip,'#MAXIMUM NUMBER OF MESHES')
              end if

              call ecoute('ker_readat')
           end do
           !
           ! ADOC[1]> END_ELSEST
           !
        else if( words(1) == 'WALLD' ) then
           !
           ! ADOC[1]> WALL_DISTANCE
           ! ADOC[d]> <b>WALL_DISTANCE</b>:
           ! ADOC[d]> This field contains the options (solver and boundary conditions) for the computation of the distance to the wall.
           ! ADOC[d]> The wall distance is added to the wall distance to the physical wall set in the PHYSICAL_PROBLEM field.
           ! ADOC[d]> By default, this distance is zero, meaning that the physical and computational walls coincide.
           !
           kfl_walld =  1                 
           solve_sol => solve(2:)
           call ecoute('ker_readat')
           do while( words(1) /= 'ENDWA' )
              if( words(1) == 'ALGEB' ) then
                 !
                 ! ADOC[2]> ALGEBRAIC_SOLVER
                 ! ADOC[2]>   INCLUDE ./sample-solver.dat
                 ! ADOC[2]> END_ALGEBRAIC_SOLVER
                 !
                 call reasol(1_ip)
              else if( words(1) == 'PRECO' ) then 
                 call reasol(2_ip)
              else if( words(1) == 'CODES' .and. exists('NODES') ) then
                 !
                 ! ADOC[2]> CODES, NODES
                 ! ADOC[2]>   INCLUDE ./sample-codes-nodes.dat
                 ! ADOC[2]> END_CODES
                 !
                 if(exists('GEOME')) then
                    tgcod => tgcod_ker(2:)
                    call reacod(4_ip)
                 else
                    tncod => tncod_ker(2:)
                    call reacod(1_ip)
                 end if
              else if( words(1) == 'CODES' .and. exists('BOUND') ) then
                 !
                 ! ADOC[2]> CODES, BOUNDARIES
                 ! ADOC[2]>   INCLUDE ./sample-codes-boundaries.dat
                 ! ADOC[2]> END_CODES
                 !
                 tbcod => tbcod_ker(2:)
                 call reacod(2_ip)
              end if
              call ecoute('ker_readat')
           end do
           !
           ! ADOC[1]> END_WALL_DISTANCE
           !
        else if( words(1) == 'WALLN' ) then
           !
           ! ADOC[1]> WALL_NORMAL
           ! ADOC[d]> <b>WALL_NORMAL</b>:
           !
           kfl_walln =  1                 
           solve_sol => solve(5:)
           call ecoute('ker_readat')
           do while( words(1) /= 'ENDWA' )
              if( words(1) == 'ALGEB' ) then
                 !
                 ! ADOC[2]> ALGEBRAIC_SOLVER
                 ! ADOC[2]>   INCLUDE ./sample-solver.dat
                 ! ADOC[2]> END_ALGEBRAIC_SOLVER
                 !
                 call reasol(1_ip)
              else if( words(1) == 'PRECO' ) then 
                 call reasol(2_ip)
              else if( words(1) == 'CODES' .and. exists('NODES') ) then
                 !
                 ! ADOC[2]> CODES, NODES
                 ! ADOC[2]>   INCLUDE ./sample-codes-nodes.dat
                 ! ADOC[2]> END_CODES
                 !
                 tncod => tncod_ker(5:)
                 call reacod(1_ip)

              else if( words(1) == 'CODES' .and. exists('BOUND') ) then
                 !
                 ! ADOC[2]> CODES, BOUNDARIES
                 ! ADOC[2]>   INCLUDE ./sample-codes-boundaries.dat
                 ! ADOC[2]> END_CODES
                 !
                 tbcod => tbcod_ker(5:)
                 call reacod(2_ip)
              end if
              call ecoute('ker_readat')
           end do
           !
           ! ADOC[1]> END_WALL_DISTANCE
           !
        else if( words(1) == 'CUTEL' ) then
           !
           ! ADOC[1]> CUT_ELEMENTS: ON/OFF                                                          $ If the mesh has cut elements
           ! ADOC[d]> <b>CUT_ELEMENTS</b>:
           ! ADOC[d]> Cut elements are elements who requires a special integration rule
           ! ADOC[d]> when they are crossed by an interface. This option should be activated when
           ! ADOC[d]> simulating cracks with SOLIDZ or when using IMMBOU module to conserve
           ! ADOC[d]> immersed boundary volumes.
           !
           ! Cut elements
           !
           if( words(2) == 'ON   ' .or. words(2) == 'YES  ' ) kfl_cutel = 1

        else if( words(1) == 'HANGI' ) then
           !
           ! Hanging nodes
           !
           if( words(2) == 'ON   ' .or. words(2) == 'YES  ' ) kfl_hangi = 1

        else if( words(1) == 'HESSI' ) then
           !
           ! ADOC[1]> HESSIAN: ON/OFF                                                               $ If the mesh has cut elements
           ! ADOC[d]> <b>HESSIAN</b>:
           ! ADOC[d]> Put OFF if Hessian of function forms should NOT be taken into account. The computational cost of the Hessian
           ! ADOC[d]> can be very high wrt to the gain of accuracy of certain algorithms.
           !
           if( words(2) == 'ON   ' .or. words(2) == 'YES  ' ) then
              kfl_lapla = 1
           else
              kfl_lapla = 0
           end if

        else if( words(1) == 'ROUGH' ) then
           !
           ! ADOC[1]> ROUGHNESS_EXTENSION
           ! ADOC[d]> <b>ROUGHNESS_EXTENSION</b>:
           ! ADOC[d]> Extension of the roughness on the wall to the volume. This can be useful if the roughness
           ! ADOC[d]> of the nearest wall node is needed for an the interior node.
           !
           ! Roughness extension
           !
           kfl_extro = 1                 
           solve_sol => solve(1:)
           call ecoute('ker_readat')
           do while( words(1) /= 'ENDRO' )
              if( words(1) == 'ALGEB' ) then
                 !
                 ! ADOC[2]> ALGEBRAIC_SOLVER
                 ! ADOC[2]>   INCLUDE ./sample-solver.dat
                 ! ADOC[2]> END_ALGEBRAIC_SOLVER
                 !
                 call reasol(1_ip)
              else if( words(1) == 'PRECO' ) then 
                 call reasol(2_ip)
              else if( words(1) == 'CODES' .and. exists('NODES') ) then
                 !
                 ! ADOC[2]> CODES, NODES
                 ! ADOC[2]>   INCLUDE ./sample-codes-nodes.dat
                 ! ADOC[2]> END_CODES
                 !
                 if(exists('GEOME')) then
                    tgcod => tgcod_ker(1:)
                    call reacod(4_ip)
                 else
                    tncod => tncod_ker(1:)
                    call reacod(1_ip)
                 end if
              else if( words(1) == 'CODES' .and. exists('BOUND') ) then
                 !
                 ! ADOC[2]> CODES, BOUNDARIES
                 ! ADOC[2]>   INCLUDE ./sample-codes-boundaries.dat
                 ! ADOC[2]> END_CODES
                 !
                 tbcod => tbcod_ker(1:)
                 call reacod(2_ip)
              end if
              call ecoute('ker_readat')
           end do
           !
           ! ADOC[1]> END_ROUGHNESS_EXTENSION
           !
        else if( words(1) == 'SPACE' ) then
           !
           ! ADOC[1]> SPACE_&_TIME_FUNCTIONS
           !
           call ecoute('ker_readat')
           do while( words(1) /= 'ENDSP' ) 
              if( words(1) == 'FUNCT' ) then

                 !-------------------------------------------------------------
                 !
                 ! Space/Time function definitions
                 !
                 !-------------------------------------------------------------
                 !
                 ! ADOC[2]> FUNCTION=char, DIMENSION=int
                 ! ADOC[3]>    f(x,t)
                 ! ADOC[3]>      ...
                 ! ADOC[2]> END_FUNCTION
                 ! ADOC[d]> <b>SPACE_&_TIME_FUNCTIONS</b>:
                 ! ADOC[d]> Define space/time functions, depending on the coordinates x,y,z and time t.
                 ! ADOC[d]> SPACE_&_TIME_FUNCTIONS
                 ! ADOC[d]>    FUNCTION=RAMP
                 ! ADOC[d]>       tanh(0.6*t) 
                 ! ADOC[d]>    END_FUNCTION
                 ! ADOC[d]> END_SPACE_&_TIME_FUNCTIONS
                 ! 
                 number_space_time_function = number_space_time_function + 1
                 idime = 1
                 if( number_space_time_function > max_space_time_function ) call runend('KER_READAT: TOO MANY FUNCTIONS HAVE BEEN DEFINED')
                 if( exists('DIMEN') ) idime = getint('DIMEN',1_ip,'#Dimension of the function')
                 space_time_function(number_space_time_function) % ndime = idime       
                 space_time_function(number_space_time_function) % name  = getcha('FUNCT','     ','#Space time function name')   
                 igene = number_space_time_function
                 call ker_memory(7_ip)
                 do kdime = 1,idime
                    read(nunit,'(a)',err=3) space_time_function(number_space_time_function) % expression(kdime)
                    space_time_function(number_space_time_function) % expression(kdime) = adjustl(trim(space_time_function(number_space_time_function) % expression(kdime)))
                    !call ecoute('DONT')  
                    !space_time_function(number_space_time_function) % expression(kdime) = adjustl(trim(ccard))
                    space_time_function(number_space_time_function) % nexpr = max( space_time_function(number_space_time_function) % nexpr , &
                         len(trim(space_time_function(number_space_time_function) % expression(kdime))) )
                 end do
                 call ecoute('ker_readat')
                 if( words(1) /= 'ENDFU' ) call runend('KER_READAT: WRONG SPACE/TIME FUNCTION DIMENSION')
              end if
              call ecoute('ker_readat')
           end do
           !
           ! ADOC[1]> END_SPACE_&_TIME_FUNCTIONS
           !
        else if( words(1) == 'TIMEF' ) then

           !-------------------------------------------------------------
           !
           ! Time Function definitions
           !
           !-------------------------------------------------------------
           !
           ! ADOC[1]> TIME_FUNCTIONS
           !
           call ecoute('ker_readat')
           do while(words(1)/='ENDTI')
              !
              ! ADOC[2]> FUNCTION=char1, PARABOLIC | PERIODIC | DISCRETE, PARAMETERS= real1...real6      $ Time function definition
              ! ADOC[3]> ... 
              ! ADOC[2]> END_FUNCTION
              ! ADOC[d]> <b>TIME_FUNCTIONS:</b>
              ! ADOC[d]> Define time function int1. Time functions are functions that multiply the values defined
              ! ADOC[d]> in the field "CODE, NODES", that is real1 real2 real3. Let t be the time
              ! ADOC[d]> and define te the effective time defined as te = min(max(t,real1),real2). 
              ! ADOC[d]> The different options are:
              ! ADOC[d]> <ul>
              ! ADOC[d]> <li>
              ! ADOC[d]>     PARABOLIC: f(t) = real3 * te^2 + real4 * te + real5. 
              ! ADOC[d]> </li>
              ! ADOC[d]> <li>
              ! ADOC[d]>     PERIODIC: f(t) = real3 * cos(real4 * te + real5 ) + real6
              ! ADOC[d]> </li>
              ! ADOC[d]> </ul>
              ! ADOC[d]> According to the definition of te, it means that:
              ! ADOC[d]> <ul>
              ! ADOC[d]> <li>
              ! ADOC[d]>     f(t) = f(real1) for t < real1
              ! ADOC[d]> </li>
              ! ADOC[d]> <li>
              ! ADOC[d]>     f(t) = f(t) for real1 < t < real2
              ! ADOC[d]> </li>
              ! ADOC[d]> <li>
              ! ADOC[d]>     f(t) = f(real2) for t > real2
              ! ADOC[d]> </li>
              ! ADOC[d]> </ul>
              ! ADOC[d]> For PARABOLIC and PERIODIC, no more informaiton is required and the function definition
              ! ADOC[d]> can be finalized by putting END_FUNCTION on the next line.
              !
              number_time_function = number_time_function + 1
              igene                = number_time_function
              ifunc                = number_time_function

              if( ifunc < 0 .or. ifunc > max_time_function ) then
                 call runend('ker_readat: WRONG FUNCTION NUMBER')
              end if

              if(      words(3) == 'PARAB' ) then
                 time_function(ifunc) % kfl_type = 1
                 time_function(ifunc) % npara    = 6

              else if( words(3) == 'PERIO' ) then
                 time_function(ifunc) % kfl_type = 2
                 time_function(ifunc) % npara    = 6

              else if( words(3) == 'SNIFF' ) then
                 time_function(ifunc) % kfl_type = 8
                 time_function(ifunc) % npara    = 9

              else if( words(3) == 'SNIF2' ) then
                 time_function(ifunc) % kfl_type = 9
                 time_function(ifunc) % npara    = 13

              else if( words(3) == 'DISCR' ) then
                 time_function(ifunc) % kfl_type = 3 
                 time_function(ifunc) % npara    = 2 * getint('NUMBE',1_ip,'#Number of data')

              else 
                 
                 time_function(ifunc) % kfl_type = 0
                 number_time_function = number_time_function - 1

              end if

              if( time_function(ifunc) % kfl_type > 0 ) then
                 !
                 ! valid function
                 !
                 time_function(ifunc) % name = getcha('FUNCT','     ','#Time function name')   
                 igene = ifunc
                 call ker_memory(8_ip)
                 if( time_function(ifunc) % kfl_type == 3 ) then
                    ifunp = 0
                    nfunp = time_function(ifunc) % npara / 2
                    if( nfunp < 1 ) call runend('NASTIN: WRONG DISCRETE FUNCTION PARAMETER')
                    call ecoute('ker_readat')
                    do while( words(1) /= 'ENDFU' )
                       ifunp = ifunp + 1
                       if( ifunp > nfunp ) call runend('NASTIN: WRONG DISCRETE FUNCTION DATA')
                       time_function(ifunc) % parameters( (ifunp-1)*2+1 ) = param(1)
                       time_function(ifunc) % parameters( (ifunp-1)*2+2 ) = param(2)
                       call ecoute('ker_readat')
                    end do
                    ! Order the function field
                    call ordena(nfunp,time_function(ifunc) % parameters)                      
                 else
                    dummi = time_function(ifunc) % npara
                    time_function(ifunc) % parameters(1:dummi) = param(4:dummi+3)
                 end if
              end if
              call ecoute('ker_readat')
              !
              ! ADOC[1]> END_FUNCTIONS
              !
           end do

        end if
        call ecoute('ker_readat')
     end do
     !--><group>
     !-->       <groupName>Output_postprocess</groupName>
     !-->       <groupType>oupos</groupType>
     !-->       <postProcess>
     !-->           <var>VELOCITY_MODULE</var>
     !-->           <var>VORTICITY</var>
     !-->           <var>KINETIC_ENERGY</var>
     !-->       </postProcess>
     !-->       <elementSet>
     !-->           <var>VELOCITY_MODULE</var>
     !-->           <var>VORTICITY</var>
     !-->           <var>KINETIC_ENERGY</var>
     !-->       </elementSet>
     !-->       <boundarySet>
     !-->           <var>MEAN_PRESSURE</var>
     !-->           <var>MASS</var>
     !-->           <var>FORCE</var>
     !-->           <var>TORQUE</var>
     !-->           <var>MEAN_YPLUS</var>
     !-->           <var>MEAN_VELOCITY</var>
     !-->           <var>WEAT_FORCE</var>
     !-->           <var>WEAT_SURFACE</var>
     !-->       </boundarySet>
     !-->       <nodeSet>
     !-->           <var>VELOX</var>
     !-->           <var>VELOY</var>
     !-->           <var>VELOZ</var>
     !-->           <var>PRESS</var>
     !-->           <var>YPLUS</var>
     !-->       </nodeSet>
     !-->       <witnessPoints>
     !-->           <var>VELOX</var>
     !-->           <var>VELOY</var>
     !-->           <var>VELOZ</var>
     !-->           <var>PRESS</var>
     !-->       </witnessPoints>
     !-->   </group>
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Output and Postprocess
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> OUTPUT_&_POST_PROCESS   
     ! ADOC[d]> OUTPUT_&_POST_PROCESS:
     ! ADOC[d]> Define the physical problem of kermod module. This field contains some variables shared by 
     ! ADOC[d]> the modules, like physical properties, distance to the wall, wall laws, etc.
     !
     do while( words(1) /= 'OUTPU' )
        call ecoute('ker_readat')
     end do
     call ecoute('ker_readat')
     do while( words(1) /= 'ENDOU' )
        !
        ! ADOC[1]> POSTPROCESS char, STEPS= int                                                  $ Postprocess "char" variable each "int" time step
        !
        call posdef(2_ip,dummi)

        if( words(1) == 'VORTX' .or. words(1) == 'REDVE' ) then
           !
           kfl_vortx = 1
           if( words(2) == 'CELL ')  kfl_vortx = 1
           if( words(2) == 'FACE ')  kfl_vortx = 2
           if( words(3) == 'PARAL')  kfl_vortx_thres=2
           if( words(3) == 'THRES')  kfl_vortx_thres=1

        else if( words(1) == 'MESH ' ) then
           !
           ! Mesh output
           !
           if( option_not_off('MESH ') ) then
              kfl_oumes = 1
           else
              kfl_oumes = 0
           end if

        else if( words(1) == 'NOMES' ) then
           !
           ! Do not output mesh
           !
            kfl_oumes = 0

        else if( words(1) == 'DETEC' ) then
           !
           ! Automatic detection
           !
           if( option('DETEC') ) then
              kfl_detection = 1 
              call ecoute('ker_readat')
              do while( words(1) /= 'ENDDE' )
                 if( words(1) == 'LENGT' ) then
                    detection_length = getrea('LENGT',1.0_rp,'#Detection length')
                 else if( words(1) == 'VELOC' ) then
                    detection_velocity = getrea('VELOC',1.0_rp,'#Detection velocity')
                 end if
                 call ecoute('ker_readat')
              end do
           end if

        else if( words(1) == 'STL  ' ) then
           !
           ! Output boundary mesh STL format
           !
           if( words(2) == 'SUBDO' ) then
              kfl_oustl = 2
           else if( words(2) == 'BOUND' .or. words(1) == 'MASTE' ) then
              kfl_oustl = 1
           else if( words(2) == 'GID  ' ) then
              kfl_oustl = 3
           else
              if( words(2) /= 'OFF  ' .and.  words(2) /= 'NO   ' ) kfl_oustl = 1
           end if

        else if ( words(1) == 'THRES' ) then
           kfl_vortx_thres=1
           if( words(2) == 'VELOC' ) &
                thr_veloc=getrea('VELOC',0.1_rp,'#velocity threshold')
           if( words(3) == 'LAMB2' ) &
                thr_qvort=getrea('LAMB2',10.0_rp,'#lambda 2 threshold')

        else if( words(1) == 'ONLAS' ) then
           kfl_posdi = ndivi

        else if( words(1) == 'ONORI' ) then
           kfl_posdi = 0

        else if( words(1) == 'MESHQ' ) then
           !
           ! Mesh quality
           !
           if( words(2) == 'GAMMA' ) then
              kfl_quali = 1
           else if( words(2) == 'ASPEC' ) then
              kfl_quali = 2
           else if( words(2) == 'CONDI' ) then
              kfl_quali = 3
           else
              kfl_quali = 1
           end if

        else if( words(1) == 'STEPO' ) then
           npp_stepo = getint('STEPO',1_ip,'#Step over')
        else if( words(1) == 'STEPS' ) then
           npp_stepo = getint('STEPS',1_ip,'#Step over')

        else if( words(1) == 'FILTE' ) then  
           !
           ! Filters
           !
           call fildef(2_ip)

        else if( words(1) == 'WITNE' ) then
           !
           ! ADOC[1]> WITNESS_POINTS, NUMBER=int, SCALE : XSCALE= real, YSCALE= real, ZSCALE= real    
           ! ADOC[1]> ...
           ! ADOC[1]> real1,real2,real3                   
           ! ADOC[1]> ...
           ! ADOC[1]> END_WITNESS_POINTS
           ! ADOC[d]> <b>WITNESS</b>: give the list of coordinates of witness points
           !
           ! COWIT: Witness points
           ! If number of witness points not given by user, use default value
           !
           if( words(2) == 'NUMBE' ) &
                mwitn = getint('NUMBE',1_ip,'#NUMBER OF WITNESS POINTS')
           if (exists('SCALE')) then
              scawi(1) = getrea('XSCAL',1.0_rp,'#x-factor')
              scawi(2) = getrea('YSCAL',1.0_rp,'#y-factor')
              scawi(3) = getrea('ZSCAL',1.0_rp,'#z-factor') 
           end if
           call ecoute('ker_readat')
           if( words(1) /= 'ENDWI' ) then
              call memose(7_ip)
              do while( words(1) /= 'ENDWI' )
                 if( nwitn >= mwitn ) then
                    call runend('KER_READAT: TOO MANY WITNESS POINTS')
                 else
                    nwitn = nwitn + 1
                    do idime = 1,3
                       cowit(idime,nwitn) = scawi(idime)*param(idime)
                    end do
                 end if
                 call ecoute('ker_readat')
              end do
           end if

        else if( words(1) == 'PIXEL' ) then
           !
           ! Pixel output
           !
           kfl_pixel = 1
           call ecoute('ker_readat')
           do while( words(1) /= 'ENDPI' )
              if( words(1) == 'VARIA' ) then
                 if(      words(2) == 'VELOC' ) then
                    variable_pixel = ID_VELOC
                 else if( words(2) == 'PRESS' ) then
                    variable_pixel = ID_PRESS
                 else if( words(2) == 'TEMPE' ) then
                    variable_pixel = ID_TEMPE
                 else
                    call runend('KER_READAT: PIXEL VARIABLE NOT CODED, EDIT POSSPM.F90')
                 end if
              else if( words(1) == 'PLANE' ) then
                 if(      words(2) == 'X    ' ) then
                    plane_pixel = 1
                 else if( words(2) == 'Y    ' ) then
                    plane_pixel = 2
                 else if( words(2) == 'Z    ' ) then
                    plane_pixel = 3
                 else
                    call runend('KER_READAT: WRONG PIXEL PLANE')
                 end if
              else if( words(1) == 'COORD' ) then
                 coord_plane_pixel = getrea('COORD',0.0_rp,'#pixel plane coordinate')
              else if( words(1) == 'FREQU' ) then
                 kfl_pixel = getint('FREQU',1_ip,'#pixel output frequency')
              else if( words(1) == 'NUMBE' ) then
                 number_pixel(1) = int(param(1),ip)
                 number_pixel(2) = int(param(2),ip)
                 if( number_pixel(2) == 0 ) number_pixel(2) = number_pixel(1)
              end if
              call ecoute('ker_readat')
            end do

         else if( words(1) == 'VOXEL' ) then
           !
           ! Voxels
           !
           kfl_abovx = 1
           call ecoute('ker_readat')
           do while( words(1) /= 'ENDVO' )
              if( words(1) == 'BOUND' ) then
                 if( exists('AUTOM') ) then
                    kfl_abovx  = 1
                 else
                    kfl_abovx  = 2
                    bobvx(1,1) = param(1)
                    bobvx(1,2) = param(2)
                    bobvx(1,3) = param(3)
                    bobvx(2,1) = param(4)
                    bobvx(2,2) = param(5)
                    bobvx(2,3) = param(6)
                 end if
              else if( words(1) == 'RESOL' ) then
                 resvx(1) = int(param(1),ip)
                 resvx(2) = int(param(2),ip)
                 resvx(3) = int(param(3),ip)
              end if
              call ecoute('ker_readat')
           end do

        end if
        call ecoute('ker_readat')
     end do
     !
     ! ADOC[0]> END_OUTPUT_&_POST_PROCESS   
     !
     if( kfl_timco == 2 ) then
        !        dtime = 1.0_rp
        timei = 0.0_rp
     end if
     !
     ! Types of Support surface have not been given
     !
     if( kfl_suppo == 1 ) then
        if( ktype == 0 ) then
           if( ndime == 2 ) then
              do iboun = 1,nboun_mm
                 ltypb_mm(iboun) = BAR02
              end do
           else
              do iboun = 1,nboun_mm
                 ltypb_mm(iboun) = TRI03
              end do
           end if
        end if
     end if
     !
     ! Kermod takes care of properties
     !
     kfl_prope = &
          &   densi_ker % kfl_exist  &
          & + visco_ker % kfl_exist  &
          & + poros_ker % kfl_exist  &
          & + condu_ker % kfl_exist  &
          & + sphea_ker % kfl_exist  &
          & + dummy_ker % kfl_exist  &
          & + turmu_ker % kfl_exist 

  end if
  return

1 call runend('KER_READAT: WRONG SUPPORT GEOMETRY COORDINATES')
2 call runend('KER_READAT: WRONG SUPPORT GEOMETRY BOUNDARY CONNECTIVITY')
3 call runend('KER_READAT: ERROR READING SPACE TIME FUNCTION')

end subroutine ker_readat
