subroutine ker_outvar(ivari)
  !------------------------------------------------------------------------
  !****f* Master/ker_output
  ! NAME 
  !    ker_output
  ! DESCRIPTION
  !    Output a postprocess variable
  ! USES
  !    postpr
  !    memgen
  ! USED BY
  !    ker_output
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_elmtyp
  use def_kermod
  use def_domain
  use def_coupli
  use mod_ker_proper
  use mod_ker_vortex 
  use mod_parall,         only : par_omp_num_colors   
  use mod_parall,         only : par_omp_ia_colors
  use mod_parall,         only : par_omp_ja_colors
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  implicit none
  integer(ip), intent(in) :: ivari
  integer(ip)             :: ibopo,ipoin,idime
  integer(ip)             :: ielem,icont,iline,jpoin,iboun,inode,izone
  integer(ip)             :: dummi,inodb,imate,incnt,kpoin,imesh,istack
  integer(ip)             :: nstack,izdom,icoup,icolo,kelem
  real(rp)                :: rutim,qmaxi,qmini,dummr,rmate
  character(5)            :: wopos(3)
  character(20)           :: wzone
  integer(ip), pointer    :: lstack(:)
  integer(ip), pointer    :: list_colors(:)

  if( ivari == 0 ) return
  !
  ! Define postprocess variable
  !
  rutim = cutim

  select case ( ivari )  

  case ( 1_ip )
     !
     ! EXNOR
     !
     if( INOTMASTER ) then
        call memgen(0_ip,ndime,npoin) 
        do ipoin = 1,npoin
           if( lpoty(ipoin) > 0 ) then
              ibopo = lpoty(ipoin)
              do idime = 1,ndime
                 gevec(idime,ipoin) = exnor(idime,1,ibopo)
              end do
           else
              do idime = 1,ndime
                 gevec(idime,ipoin) = 0.0_rp
              end do
           end if
        end do
     end if

  case ( 2_ip )
     !
     ! ???
     !
     return

  case ( 3_ip ) 
     !
     ! Geometrical local basis and type of point: LPOIN, SKCOS
     !
     if( kfl_geome == 0 ) return
     if( INOTMASTER ) then
        call memgen(0_ip,ndime,npoin)
        do ipoin = 1,npoin
           if( lpoty(ipoin) > 0 ) then
              ibopo = lpoty(ipoin)
              do idime = 1,ndime
                 gevec(idime,ipoin) = skcos(idime,1,ibopo)
              end do
           else
              do idime = 1,ndime
                 gevec(idime,ipoin) = 0.0_rp
              end do
           end if
        end do
     end if

  case ( 4_ip )
     !
     ! Geometrical local basis SKCOS(:,1,:)
     !
     if( kfl_geome == 0 ) return
     if( INOTMASTER ) then
        call memgen(0_ip,ndime,npoin)
        do ipoin = 1,npoin
           if( lpoty(ipoin) > 0 ) then
              ibopo = lpoty(ipoin)
              do idime = 1,ndime
                 gevec(idime,ipoin) = skcos(idime,1,ibopo)
              end do
           else
              do idime = 1,ndime
                 gevec(idime,ipoin) = 0.0_rp
              end do
           end if
        end do
     end if

  case ( 5_ip )
     !
     ! Geometrical local basis SKCOS(:,2,:)
     !
     if( kfl_geome == 0 ) return
     if( INOTMASTER ) then
        call memgen(0_ip,ndime,npoin)
        do ipoin = 1,npoin
           if( lpoty(ipoin) > 0 ) then
              ibopo = lpoty(ipoin)
              do idime = 1,ndime
                 gevec(idime,ipoin) = skcos(idime,2,ibopo)
              end do
           else
              do idime = 1,ndime
                 gevec(idime,ipoin) = 0.0_rp
              end do
           end if
        end do
     end if

  case ( 6_ip )
     !
     ! Geometrical local basis SKCOS(:,NDIME,:)
     !
     if( kfl_geome == 0 ) return
     if( INOTMASTER ) then
        call memgen(0_ip,ndime,npoin)
        do ipoin = 1,npoin
           if( lpoty(ipoin) > 0 ) then
              ibopo = lpoty(ipoin)
              do idime = 1,ndime
                 gevec(idime,ipoin) = skcos(idime,ndime,ibopo)
              end do
           else
              do idime = 1,ndime
                 gevec(idime,ipoin) = 0.0_rp
              end do
           end if
        end do
     end if

  case ( 7_ip )
     !
     ! Hanging nodes: LHANG
     !
     !if( nhang > 0 .and. INOTMASTER ) then
     !   call memgen(zero,npoin,zero)
     !   do ihang=1,nhang
     !      ipoin=lhang(1,ihang)
     !      if(ipoin<1.or.ipoin>npoin) then
     !         call runend('ERROR IN OUTDOM')
     !      end if
     !      gesca(ipoin)=real(lhang(0,ihang))
     !   end do
     !end if

  case ( 8_ip )
     !
     ! DISPM: Mesh displacement
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        if( associated(dispm) ) then
           do ipoin = 1,npoin
              do idime = 1,ndime
                 gevec(idime,ipoin) = dispm(idime,ipoin,1) 
              end do
           end do
        else
           do ipoin = 1,npoin
              do idime = 1,ndime
                 gevec(idime,ipoin) = 0.0_rp 
              end do
           end do
        end if
        !if( nimbo > 0 ) then
        !   kpoin = npoin
        !   do iimbo = 1,nimbo
        !      do ipoib = 1,imbou(iimbo)%npoib
        !         kpoin = kpoin + 1
        !         do idime = 1,ndime
        !            gevec(idime,kpoin) = imbou(iimbo)%cooi2(idime,ipoib) - imbou(iimbo)%cooin(idime,ipoib)
        !         end do
        !      end do
        !   end do
        !end if
     end if
     !if( kfl_outib == 3 ) kfl_outib = 4

  case ( 9_ip )
     !
     ! Boundary points: LPOTY
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           if(lpoty(ipoin)/=0) gesca(ipoin)=1.0_rp
        end do
     end if

  case ( 10_ip )
     !
     ! Numbering NUMBER: IPOIN
     ! 
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           if( lnlev(ipoin) /= 0 ) &
                gesca(ipoin) = real(meshe(0) % lninv_loc(lnlev(ipoin)))
        end do
     end if

  case ( 11_ip ) 
     !
     ! Boundary codes: KFL_CODNO
     !
     if( kfl_icodn > 0 ) then
        if( INOTMASTER ) then
           call memgen(zero,ndime,npoin)
           do ipoin=1,npoin
              do idime = 1,ndime
                 gevec(idime,ipoin)=real(kfl_codno(idime,ipoin))
              end do
           end do
        end if
     else
        return
     end if

  case ( 12_ip )
     !
     ! YWALP: Wall distance
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           ibopo = lpoty(ipoin)
           if( ibopo /= 0 ) gesca(ipoin)=ywalp(ibopo)
        end do
     end if

  case ( 13_ip )
     !
     ! LNTIB: node type
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           gesca(ipoin)=real(lntib(ipoin))
        end do
     end if

  case ( 14_ip )
     !
     ! HOLES: with node type
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           if( lntib(ipoin) > 0 ) then
              gesca(ipoin) = 1.0_rp
           end if
        end do
     end if

  case ( 15_ip )
     !
     ! DENSI: Density
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call ker_proper('DENSI','NPOIN',dummi,dummi,gesca)
     end if

  case ( 16_ip )
     !
     ! VISCO: viscosity
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call ker_proper('VISCO','NPOIN',dummi,dummi,gesca)
     end if

  case ( 17_ip )
     !
     ! 
     !
     return

  case ( 18_ip )
     !
     ! CONDU: Conductivity
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call ker_proper('CONDU','NPOIN',dummi,dummi,gesca)
     end if

  case ( 19_ip )
     !
     ! SPECI: Specific Heat
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call ker_proper('SPHEA','NPOIN',dummi,dummi,gesca)
     end if

  case ( 20_ip )
     !
     ! DUMMY: Dummy variable
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call ker_proper('DUMMY','NPOIN',dummi,dummi,gesca)
     end if

  case ( 21_ip )
     !
     ! 
     !
     return

  case ( 22_ip )
     !
     ! LEVELS
     !
     if( INOTMASTER ) then
        call memgen(0_ip,nelem,0_ip)
        call meshin(-5_ip)
     end if

  case ( 23_ip )
     !
     ! LGROU_DOM
     !
     if( ngrou_dom <= 0 ) return
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           gesca(ipoin) = real(lgrou_dom(ipoin))
        end do
     end if

  case ( 24_ip )
     !
     ! MASSM
     !
     gesca => vmass

  case ( 25_ip )
     !
     ! MASSC 
     !
     gesca => vmasc

  case ( 26_ip )
     !
     ! KFL_GEONO
     !
     if( kfl_geome == 0 ) return
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           ibopo = lpoty(ipoin)
           if( ibopo /= 0 ) gesca(ipoin)=real(kfl_geono(ibopo),rp)
        end do
     end if

  case ( 27_ip )
     !
     ! Subdomains 
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoi1
           gesca(ipoin) = real(kfl_paral,rp)
        end do
        !do ipoin = npoi1+1,npoin
        do ipoin = npoi2,npoi3
           gesca(ipoin) = real(kfl_paral,rp)
        end do
        call PAR_INTERFACE_NODE_EXCHANGE(gesca,'MAX','IN MY CODE')
     end if

  case ( 28_ip )
     !
     ! WALLD
     !
     if( kfl_walld == 0 ) return
     gesca => walld

  case ( 29_ip )
     !
     ! ROUGH
     !
     if( kfl_rough < 1 ) return
     gesca => rough

  case(30_ip)
     !
     ! LINEL: Linelets of preconditioner CG
     !
     if( INOTMASTER ) then
        icont = 0
        do ipoin = 1,npoin
           rhsid(ipoin) = 0.0_rp
        end do
        do iline = 1,solve(2) % nline
           icont = icont+1
           do ipoin = solve(2) % lline(iline),solve(2) % lline(iline+1)-1
              jpoin = solve(2) % lrenup(ipoin)
              rhsid(jpoin) = real(icont,rp)
           end do
        end do
        gesca => rhsid
     end if

  case ( 31_ip )
     !
     ! Boundary codes: CODBO
     !
     if( INOTMASTER ) then
        call memgen(zero,nboun,zero)
        do iboun = 1,nboun
           gesca(iboun)=real(kfl_codbo(iboun))
        end do
     end if

  case ( 32_ip )
     !
     ! Subdomains
     !
     if( nmate <= 1 ) return
     if( INOTMASTER ) then
        call memgen(zero,nelem,zero)
        do ielem = 1,nelem
           gesca(ielem) = real(lmate(ielem),rp)
        end do
     end if

  case ( 33_ip )
     !
     ! Nodal material (maximum)
     !
     if( nmate <= 0 ) return
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do imate = 1,nmate
           rmate = real(imate,rp)
           do ipoin = 1,size(lmatn(imate)%l)
              gesca(lmatn(imate)%l(ipoin)) = rmate
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 34_ip )
     !
     ! Node characteristic
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           gesca(ipoin) = real(lnoch(ipoin),rp)
        end do
     end if

  case ( 35_ip )
     !
     ! Element material
     !
     if( nmate <= 1 ) return
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)              
              gesca(ipoin) = max(gesca(ipoin),real(lmate(ielem),rp))
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 36_ip )
     !
     ! Element quality
     !
     if( INOTMASTER ) then
        call memgen(zero,nelem,zero) 
        call qualit(gesca,qmaxi,qmini)
     else
        call qualit(dummr,qmaxi,qmini)
     end if

  case ( 37_ip )
     !
     ! Zones
     !
     wopos(2) = postp(1) % wopos(2,37)
     wopos(3) = postp(1) % wopos(3,37)
     do izone = 1,min(9_ip,nzone)
        wzone    = intost(izone)
        wopos(1) = postp(1) % wopos(1,37)(1:4)//trim(wzone)
        if( INOTMASTER ) then
           call memgen(zero,npoin,zero)
           do kpoin = 1,npoiz(izone)
              ipoin = lpoiz(izone) % l(kpoin)
              gesca(ipoin) = real(izone,rp)
           end do
        end if
        call outvar(ivari,ittim,rutim,wopos) 
     end do
     return

  case ( 38_ip )
     !
     ! Boundary codes: CODBB
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do iboun = 1,nboun
           do inodb = 1,nnode(ltypb(iboun))
              ipoin = lnodb(inodb,iboun)
              gesca(ipoin)=real(kfl_codbo(iboun),rp)
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 39_ip )
     !
     ! LETIB
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              gesca(ipoin) = max(real(letib(ielem),rp),gesca(ipoin))
           end do
        end do
     end if

  case ( 40_ip )
     !
     ! LTYPE
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              gesca(ipoin) = real(ltype(ielem),rp)
           end do
        end do
     end if

  case ( 41_ip )
     !
     ! LELCH
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem           
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              dummr = real(lelch(ielem),rp)
              gesca(ipoin) = max(gesca(ipoin),dummr)
           end do
        end do
         call PAR_INTERFACE_NODE_EXCHANGE(gesca,'MAX','IN MY CODE')
     end if

  case ( 42_ip )
     !
     ! Contact
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero) 
        do incnt = 1,nncnt
           ipoin = lncnt(1,incnt)
           jpoin = lncnt(2,incnt)
           gesca(ipoin) = 1.0_rp
           gesca(jpoin) = 2.0_rp
        end do
     end if

  case ( 43_ip )
     !
     ! R_DOM
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           gesca(ipoin) = real(r_dom(ipoin+1)-r_dom(ipoin),rp)
        end do
     end if

  case ( 44_ip )
     !
     ! VORTX: extraction of vortex core 
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
     else
        call memgen(zero,1_ip,1_ip)
     end if
     call ker_vortex(gevec)
     if( IMASTER ) call memgen(two,1_ip,1_ip)

  case ( 45_ip )
     !
     ! Element sets
     !
     if( neset < 1 ) return
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)              
              gesca(ipoin) = max(gesca(ipoin),real(leset(ielem),rp))
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 46_ip )
     !
     ! DISPL_KER
     !
     if( kfl_suppo == 0 ) return
     gevec => displ_ker

  case ( 47_ip )
     !
     ! VELOC
     !
     if( INOTMASTER ) gevec => veloc(:,:,1) 

  case ( 48_ip )
     !
     ! LBSET: boundary sets
     !
     if( nbset < 1 ) return
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do iboun = 1,nboun
           do inodb = 1,nnode(abs(ltypb(iboun)))
              ipoin = lnodb(inodb,iboun)              
              gesca(ipoin) = max(gesca(ipoin),real(lbset(iboun),rp))
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 49_ip ) 
     !
     ! Connected meshes
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)    
        allocate(lstack(npoin))  
        do ipoin = 1,npoin
           lstack(ipoin) = 0
        end do

        imesh        = 0
        kpoin        = 0

        do while( kpoin /= npoin ) 

           imesh = imesh + 1
           ipoin = 1
           do while( gesca(ipoin) /= 0.0_rp )
              ipoin = ipoin + 1
           end do
           nstack       = 1
           lstack(1)    = ipoin
           gesca(ipoin) = real(imesh,rp)
           istack       = 0        
           kpoin        = kpoin + 1

           do 
              if( istack == nstack ) exit
              istack = istack + 1   
              ipoin  = lstack(istack)
              do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                 jpoin = c_dom(izdom)
                 if( gesca(jpoin) == 0.0_rp ) then
                    gesca(jpoin)   = real(imesh,rp)
                    nstack         = nstack + 1
                    lstack(nstack) = jpoin
                    kpoin          = kpoin + 1
                 end if
              end do
           end do
        end do
        deallocate(lstack)  

     end if

  case ( 50_ip )
     !
     ! TURBU: Turbulent viscosity
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call ker_proper('TURBU','NPOIN',dummi,dummi,gesca)
     end if

  case ( 51_ip )
     !
     ! LNSUB: Element subdomain
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)              
              gesca(ipoin) = max(gesca(ipoin),real(lesub(ielem),rp))
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 52_ip )
     !
     ! Wet nodes
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do icoup = 1,mcoup
           do kpoin = 1,coupling_type(icoup) % geome % npoin_wet
              ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
              if( ipoin /= 0 ) gesca(ipoin) = real(icoup,rp)
           end do
        end do
     end if

  case ( 53_ip )
     !
     ! LESUB: Element subdomain
     !
     if( INOTMASTER ) then
        call memgen(zero,nelem,zero)
        do ielem = 1,nelem         
           gesca(ielem) = max(gesca(ielem),real(lesub(ielem),rp))
        end do
     end if

  case ( 54_ip )
     !
     ! CANOPY HEIGHT
     !
     if( kfl_canhe < 1 ) return
     gesca => canhe

  case ( 55_ip )
     !
     ! HEIGHT OVER TERRAIN
     !
     if( kfl_heiov < 1 ) return
     gesca => heiov

  case ( 56_ip )
     !
     ! BEATR: Element subdomain + characteristic + material
     !
     if( INOTMASTER ) then
        call memgen(zero,nelem,zero)
        do ielem = 1,nelem         
           icont = 100 * lmate(ielem) + 10 * lesub(ielem) + lelch(ielem)
           gesca(ielem) = max(gesca(ielem),real(icont,rp))
        end do
     end if

  case ( 57_ip )
     !
     ! COLORING of the openmp stratgy
     !
     if( INOTMASTER ) then
        allocate(list_colors(nelem))
        call memgen(zero,nelem,zero)
        do icolo = 1,par_omp_num_colors  
           do kelem = par_omp_ia_colors(icolo),par_omp_ia_colors(icolo+1)-1
              ielem = par_omp_ja_colors(kelem)        
              list_colors(ielem) = icolo
           end do
        end do
        do ielem = 1,nelem
           gesca(ielem) = list_colors(ielem)
        end do
        !do ipoin = 1,npoin
        !   gesca(ipoin) = huge(1.0_rp)
        !end do
        !do ielem = 1,nelem
        !   do inode = 1,lnnod(ielem)
        !      ipoin = lnods(inode,ielem)
        !      gesca(ipoin) = min(gesca(ipoin),real(list_colors(ielem),rp))
        !   end do
        !end do
        !call PAR_INTERFACE_NODE_EXCHANGE(gesca,'MIN','IN MY CODE')
        deallocate(list_colors)
     end if

  case ( 58_ip )
     !
     ! WALLN
     !
     gevec => walln

  end select

  call outvar(&
       ivari,&
       ittim,rutim,postp(1) % wopos(1,ivari))

end subroutine ker_outvar
