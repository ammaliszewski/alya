subroutine ker_memall()
  !-----------------------------------------------------------------------
  !****f* Kermod/ker_memall
  ! NAME 
  !    ker_memall
  ! DESCRIPTION
  !    This routine allocates memory for the arrays needed to solve the
  !    NS equations
  ! USES
  !    memchk
  !    mediso
  ! USED BY
  !    ker_turnon
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_kermod
  use def_domain
  use def_solver
  use mod_memory
  use mod_maths,  only : maths_equalize_arrays
  use mod_chktyp, only : check_type
  implicit none
  integer(ip) :: kfl_value

  if( INOTMASTER ) then
     !
     ! WALLD: wall distance
     ! 
     if( kfl_walld /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'WALLD'    ,'ker_memall',walld,    npoin)
        call memory_alloca(mem_modul(1:2,modul),'UWALL_KER','ker_memall',uwall_ker,npoin)
        if( kfl_delta == 1 ) then
           call memory_alloca(mem_modul(1:2,modul),'UWAL2_KER','ker_memall',uwal2_ker,npoin)
        end if
     end if
     !
     ! WALLN: wall normal
     ! 
     if( kfl_walln /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'WALLN'    ,'ker_memall',walln,ndime,npoin)
     end if
     !
     ! ROUGH: Allocate roughness
     !
     if( kfl_rough == 0 ) then 
        call memory_alloca(mem_modul(1:2,modul),'ROUGH','ker_memall',rough,npoin)
     end if
     !
     ! CANHE: Allocate canopy heigh 
     !
     if( kfl_canhe == 0 ) then 
        call memory_alloca(mem_modul(1:2,modul),'CANHE','ker_memall',canhe,npoin)
     end if
     !
     ! Support boundary
     !
     if( kfl_suppo == 1 ) then
        call memory_alloca(mem_modul(1:2,modul),'DISPL_KER','ker_memall',displ_ker,ndime,npoin)
     end if
     !
     ! Advection vector
     ! KFL_VEFUN = 0 ... ADVEC points to VELOC 
     !           < 0 ... Copy values from a field
     !           > 0 ... User-defined function
     !
     if( kfl_vefun == 0 ) then
        advec => veloc
     else
        call memory_alloca(mem_modul(1:2,modul),'ADVEC','ker_memall',advec,ndime,npoin,3_ip)
        if( kfl_vefun < 0 ) then 
           kfl_value = -kfl_vefun
           call check_type(xfiel,kfl_value,ndime,npoin) 
           call maths_equalize_arrays(ndime,npoin,xfiel(kfl_value) % a,advec)
           call maths_equalize_arrays(ndime,npoin,advec(1:ndime,1:npoin,1),advec(1:ndime,1:npoin,2))
           call maths_equalize_arrays(ndime,npoin,advec(1:ndime,1:npoin,1),advec(1:ndime,1:npoin,3))
        end if
     end if

  else
     !
     ! Wall distance
     !
     if( kfl_walld /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'WALLD'    ,'ker_memall',walld,    1_ip)
        call memory_alloca(mem_modul(1:2,modul),'UWALL_KER','ker_memall',uwall_ker,1_ip)
        if( kfl_delta == 1 ) then
           call memory_alloca(mem_modul(1:2,modul),'UWAL2_KER','ker_memall',uwal2_ker,1_ip)
        end if
     end if
     !
     ! ROUGH: Allocate roughness
     !
     if( kfl_rough == 0 ) then 
        call memory_alloca(mem_modul(1:2,modul),'ROUGH','ker_memall',rough,1_ip)
     end if
     !
     ! CANHE: Allocate canopy heigh 
     !
     if( kfl_canhe == 0 ) then 
        call memory_alloca(mem_modul(1:2,modul),'CANHE','ker_memall',canhe,1_ip)
     end if
     !
     ! Support boundary
     !
     if( kfl_suppo == 1 ) then
        call memory_alloca(mem_modul(1:2,modul),'DISPL_KER','ker_memall',displ_ker,1_ip,1_ip)
     end if
     !
     ! Fields prescribed by master
     !
     if( kfl_vefun /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'ADVEC','ker_memall',advec,1_ip,1_ip,3_ip)
     end if

  end if

  !----------------------------------------------------------------------
  !
  ! Memory for solver
  !
  !----------------------------------------------------------------------

  if( kfl_extro /= 0 ) then  
     solve_sol => solve(1:1)  ! Roughness extension
     call soldef(4_ip)
  end if
  if( kfl_walld /= 0 ) then
     solve_sol => solve(2:2)  ! Roughness extension
     call soldef(4_ip)
  end if
  if( kfl_suppo /= 0 ) then
     solve_sol => solve(3:3)  ! Support geometry for mesh multiplication 
     call soldef(4_ip)
  end if
  if( kfl_walln /= 0 ) then
     solve_sol => solve(5:5)  ! Wall normal
     call soldef(4_ip)
  end if
 ! if( kfl_defor /= 0 ) then
 !    solve_sol => solve(4:4)  ! Mesh deformation
 !    call soldef(4_ip)
 ! end if

end subroutine ker_memall
