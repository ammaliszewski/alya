module def_kermod

  !-----------------------------------------------------------------------
  !****f* defmod/def_kermod
  ! NAME
  !   def_kermod
  ! DESCRIPTION
  !   This module is the header of kermod
  !   1 material -> 1 law 
  !   1 law      -> 1 responsible module
  !   MATERIAL=1
  !     DENSITY:   LAW=BIFLUID,  PARAMETERS=1.0,2.0,3.0
  !     VISCOSITY: LAW=BIFLUID,  PARAMETERS=1.0,2.0,3.0
  !   END_MATERIAL
  !
  !***
  !-----------------------------------------------------------------------
  use def_kintyp

  integer(ip),   parameter       ::   &
       mlaws_ker                = 20, & ! Max # laws
       mlapa_ker                = 5,  & ! Max # parameters
       mprop_ker                = 7,  & ! Max # of properties tracked by kermod
       mresp_ker                = 5,  & ! Max # responsible modules
       mfilt                    = 10, & ! Max # filters
       nelse                    = 20, & ! # Elsest parameters
       max_space_time_function  = 20, & ! Max # space/time functions
       max_time_function        = 20    ! Max # time functions

  type typ_laws
     integer(ip)                 :: kfl_gradi
     integer(ip)                 :: kfl_deriv
     integer(ip)                 :: kfl_grder
     character(5)                :: wname
     integer(ip)                 :: lresp(mresp_ker)
     character(5)                :: where
  end type typ_laws

  type typ_valpr_ker
     integer(ip)                 :: kfl_exist
     integer(ip)                 :: kfl_nedsm
     character(5),     pointer   :: wlaws(:)
     integer(ip),      pointer   :: ilaws(:)
     real(rp),         pointer   :: rlaws(:,:)
     type(r1p),        pointer   :: value_ielem(:)
     type(r1p),        pointer   :: value_iboun(:)
     type(r2p),        pointer   :: grval_ielem(:)
     type(r2p),        pointer   :: drval_ielem(:)
     type(r3p),        pointer   :: gdval_ielem(:)
     real(rp),         pointer   :: value_ipoin(:)
     real(rp),         pointer   :: grval_ipoin(:,:)
     real(rp),         pointer   :: value_const(:)
     real(rp),         pointer   :: value_default(:)
     type(typ_laws)              :: llaws(mlaws_ker)
  end type typ_valpr_ker

  type lis_prope_ker   !Just a list of the properties to improve readability
     type(typ_valpr_ker),pointer :: prop
  end type lis_prope_ker

  !------------------------------------------------------------------------
  !
  ! Physical problem
  !
  !------------------------------------------------------------------------

  integer(ip)              :: &      
       kfl_vefun,             &      ! Velocity function
       kfl_rough,             &      ! Roughness function: =0: constant,/=0: field
       kfl_canhe,             &      ! Canopy height function: =0: constant,/=0: field
       kfl_heiov,             &      ! Height over terrain function: =0: aprox. by wall distance,/=0: field
       kfl_delta,             &      ! Ways to calculate wall distance (=0 prescribed by delta_dom, =1 depending on element size, -1 fix v=0 instead)
       kfl_ustar,             &      ! Law of the wall type
       kfl_walld,             &      ! Wall distance needed
       kfl_walln,             &      ! Wall normal needed
       kfl_suppo,             &      ! Support geometry for MM 
       kfl_extro,             &      ! Roughness extension
       kfl_prope,             &      ! Properties computed by kermod
       kfl_kemod_ker                 ! k eps modification temporal copy to kernel

  real(rp)                 :: &
       denme,                 &      ! Medium density
       visme,                 &      ! Viscosity medium
       gasco,                 &      ! Gas constant R
       u_ref,                 &      ! Reference velocity
       h_ref,                 &      ! Reference height
       k_ref,                 &      ! Reference roughness
       usref,                 &      ! Reference ustar
       windg(3),              &      ! Geostrophic wind (CFDWind2 model)
       delta_dom,             &      ! Wall distance for law of the wall
       delmu_dom,             &      ! Multiplier factor for variable wall distance for law of the wall
       rough_dom,             &      ! Wall distance for law of the wall
       grnor,                 &      ! Gravity norm
       gravi(3),              &      ! Gravity vector
       thicl,                 &      ! Interface thickness
       cmu_st                        ! cmu to imposse ABL b.c in nastin

  type custo_CFDWind
     integer(ip)  :: kfl_model             ! 1=CFDWind1, 2=CFDWind2
     real(rp)     :: u_ref                 ! Reference velocity
     real(rp)     :: h_ref                 ! Reference height
     real(rp)     :: k_ref                 ! Reference roughness
     real(rp)     :: ustar_ref             ! Reference ustar
     real(rp)     :: l_monin_obukhov       ! Monin-Obukhov mixing length (CFDWind2 model)
     real(rp)     :: u_geostrohpic_wind(3) ! Geostrophic wind (CFDWind2 model)
     real(rp)     :: wind_angle            ! Wind angle
  end type custo_CFDWind
  
  type(typ_valpr_ker), target   :: densi_ker
  type(typ_valpr_ker), target   :: visco_ker
  type(typ_valpr_ker), target   :: poros_ker
  type(typ_valpr_ker), target   :: condu_ker
  type(typ_valpr_ker), target   :: sphea_ker
  type(typ_valpr_ker), target   :: dummy_ker
  type(typ_valpr_ker), target   :: turmu_ker

  !------------------------------------------------------------------------
  !
  ! Numerical treatment
  !
  !------------------------------------------------------------------------

  integer(ip)                   :: &
       kfl_renum,                  & ! Renumbering
       ndivi,                      & ! Number of mesh divisions
       kfl_rotation_axe,           & ! Axes of rotation of the mesh
       kfl_graph,                  & ! Compute extended graph (needed for RAS precon.)
       kfl_grpro,                  & ! Gradient projection (1=closed,0=open)
       kfl_fixsm,                  & ! Default boundary smoothing       
       kfl_conbc_ker,              & ! Kernel functions
       kfl_quali,                  & ! Mesh quality 
       kfl_elses,                  & ! Elsest exists
       ielse(nelse),               & ! Elsest int parameters
       kfl_cutel,                  & ! if cut elements exist
       kfl_hangi,                  & ! If hanging nodes exist
       kfl_lapla,                  & ! If hessian matrices are computed
       kfl_defor,                  & ! Mesh deformation
       npoin_mm,                   & ! Support geometry for MM: number of nodes
       nboun_mm,                   & ! Support geometry for MM: number of boundaries
       mnodb_mm,                   & ! Support geometry for MM: max number of nodes per boundary
       number_space_time_function, & ! Number of space/time functions
       number_time_function,       & ! Number of space/time functions
       deformation_steps,          & ! Number of deformation steps (when KFL_DEFOR /= 0 )
       deformation_strategy,       & ! Deformation strategy
       kfl_duatss,                 & ! Dual time step to precondition transient systems
       fact_duatss                   ! factor for dual time step
  real(rp)                 ::      &
       relse(nelse),               & ! Elsest real parameters
       rotation_angle                ! Angle of rotation of the mesh
  integer(ip),    pointer  ::      &
       lnodb_mm(:,:),              & ! Support geometry for MM: connectivity
       ltypb_mm(:),                & ! Support geometry for MM: connectivity
       kfl_type_time_function(:,:)   ! Time function type
  real(rp),       pointer  ::      &
       coord_mm(:,:)                 ! Support geometry for MM: coordinates
  type(bc_nodes), pointer  ::      &     
       tncod_ker(:)                  ! Node code type
  type(bc_nodes), pointer  ::      &     
       tgcod_ker(:)                  ! Geometrical node code type
  type(bc_bound), pointer  ::      &     
       tbcod_ker(:)                  ! Boundary code type
  type(typ_space_time_function), target :: space_time_function(max_space_time_function)  ! Space time functions
  type(typ_time_function),       target :: time_function(max_time_function)              ! Time function

  !------------------------------------------------------------------------
  !
  ! Output and postprocess
  !
  !------------------------------------------------------------------------

  integer(ip)              :: & 
       mwitn,                 &      ! Max # witness points
       nwitn,                 &      ! # witness points
       kfl_posdo,             &      ! Domain postprocess
       kfl_posdi,             &      ! Postprocess division
       kfl_oumes,             &      ! Output Mesh needs to be written  
       kfl_oustl,             &      ! Output boundary mesh in STL format
       npp_stepo,             &      ! Step over postprocessing frequency
       nfilt,                 &      ! Number of filters
       kfl_abovx,             &      ! Automatic voxel bounding box
       resvx(3),              &      ! Voxel resolution
       kfl_vortx,             &      ! flag of VORTX output  
       kfl_vortx_thres,       &      ! flag of threshold VORTX output 
       kfl_detection,         &      ! Automatic detection of pattern
       kfl_pixel,             &      ! Output pixel 
       plane_pixel,           &      ! Plane along x, y or z
       variable_pixel,        &      ! Variable to postprocess
       number_pixel(2)               ! Pixel number
  real(rp)                 :: &
       bobvx(2,3),            &      ! Voxel Bounding box corners 
       travx(3),              &      ! Translation
       thr_veloc,             &      ! threshold VORTX with velocity
       thr_qvort,             &      ! threshold VORTX with qvorticity
       detection_length,      &      ! Characteristic length for detection
       detection_velocity,    &      ! Characteristic velocity for detection 
       coord_plane_pixel             ! Plane coordinate for pixel value
  real(rp),    pointer     :: &
       cowit(:,:)                    ! Witness point coordinates

  type(tyfil), target      :: &
       filte(mfilt),          &      ! Filter type
       parfi(10)                     ! Number of parameters

  !------------------------------------------------------------------------
  !
  ! Others
  !
  !------------------------------------------------------------------------

  integer(ip)              :: &
       lastm_ker,             &      ! Last solved module
       nvort,                 &      ! number of vortex
       nvort_total,           &      ! number total of vortex
       npoin_total_filt,      &      ! number total of filtered points
       npoin_filt,            &      ! number of filtered points
       nelem_total_filt,      &      ! number total of filtered elements
       nelem_filt,            &      ! number of filtered points
       number_event                  ! Event number
  real(rp),   pointer      :: &
       uwall_ker(:),          &      ! Save wall distance solution
       uwal2_ker(:),          &      ! Save wall distance solution
       shwit(:,:),            &      ! Witness point shape functions
       dewit(:,:,:),          &      ! Witness point shape functions derivatives
       displ_ker(:,:)                ! Displacement support boundary for MM
  integer(ip), pointer     :: &
       lewit(:)                      ! List of witness point element

  !------------------------------------------------------------------------
  !
  ! Boundary conditions of wall distance and wall normal problem
  !
  !------------------------------------------------------------------------

  integer(ip), parameter   ::    &   
       mfunc_walld_ker=10            ! Maximum number of functions
  integer(ip), pointer     ::    &
       kfl_funno_walld_ker(:),   &   ! Nodal function 
       kfl_funbo_walld_ker(:),   &   ! Boundary function 
       kfl_fixno_walld_ker(:,:), &   ! Nodal fixity 
       kfl_fixbo_walld_ker(:),   &   ! Boundary fixity
       kfl_funty_walld_ker(:,:), &   ! Function type and number of parameters
       kfl_fixno_walln_ker(:,:), &   ! Nodal fixity 
       kfl_fixbo_walln_ker(:)        ! Boundary fixity
  type(r1p),   pointer     ::    &
       funpa_walld_ker(:)            ! Function parameters
  real(rp),    pointer     ::    &
       bvess_walld_ker(:,:),     &   ! Nodal value 
       bvnat_walld_ker(:),       &   ! Boundary value
       bvess_walln_ker(:,:)          ! Nodal value 

  !------------------------------------------------------------------------
  !
  ! Boundary conditions of deformation problem
  !
  !------------------------------------------------------------------------

  integer(ip), pointer     ::    &
       kfl_funno_defor_ker(:),   &   ! Nodal function 
       kfl_fixno_defor_ker(:,:), &   ! Nodal fixity 
       kfl_funty_defor_ker(:,:)      ! Function type and number of parameters
  real(rp),    pointer     ::    &
       bvess_defor_ker(:,:)          ! Nodal value 

  !------------------------------------------------------------------------
  !
  ! Boundary conditions of roughness extension problem
  !
  !------------------------------------------------------------------------

  integer(ip), parameter   ::    &   
       mfunc_rough_ker=10            ! Maximum number of functions
  integer(ip), pointer     ::    &
       kfl_funno_rough_ker(:),   &   ! Nodal function 
       kfl_funbo_rough_ker(:),   &   ! Boundary function 
       kfl_fixno_rough_ker(:,:), &   ! Nodal fixity 
       kfl_fixbo_rough_ker(:),   &   ! Boundary fixity
       kfl_funty_rough_ker(:,:)      ! Function type and number of parameters
  type(r1p),   pointer     ::    &
       funpa_rough_ker(:)            ! Function parameters
  real(rp),    pointer     ::    &
       bvess_rough_ker(:,:),     &   ! Nodal value 
       bvnat_rough_ker(:)            ! Boundary value

  !------------------------------------------------------------------------
  !
  ! Boundary conditions of support geometry
  !
  !------------------------------------------------------------------------

  integer(ip), pointer     ::    &
       kfl_fixno_suppo_ker(:,:)      ! Nodal fixity 
  real(rp),    pointer     ::    &
       bvess_suppo_ker(:,:)          ! Nodal value 

  !------------------------------------------------------------------------
  !
  ! Events
  !
  !------------------------------------------------------------------------
  
  character(50) :: events_directory

end module def_kermod
