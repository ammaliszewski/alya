!-----------------------------------------------------------------------
!
!> @addtogroup PropertiesToolBox
!> @{
!> @name    ToolBox for property creations
!> @file    mod_ker_proper.f90
!> @author  Guillaume Houzeaux
!> @brief   ToolBox for properties
!> @details To create a new property:
!>          1. ker_readat.f90: Add the reading of the property 
!>          2. ker_allaws.f90: Decide in where to computer the property
!>          3. ker_updpro.f90: Compute the property and its gradients if
!>             needed
!> @{
!
!------------------------------------------------------------------------

module mod_ker_proper

  use def_kintyp, only :  ip,rp
  use def_master, only :  modul,mem_modul
  use def_domain, only :  nmate,nelem,npoin,ltypb
  use def_domain, only :  ltype,lmate,ngaus,nboun
  use def_kermod !, only :  typ_valpr_ker,mlaws_ker
  implicit none

contains

  subroutine ker_allpro(prope_ker)
    !------------------------------------------------------------------------
    !****f* Master/ker_allpro
    ! NAME 
    !    ker_allpro
    ! DESCRIPTION
    !    Allocate memory
    ! USES
    !    postpr
    !    memgen
    ! USED BY
    !    ker_output
    !***
    !------------------------------------------------------------------------
    use mod_memchk
    use mod_memory
    use def_domain, only :  ndime,nnode
    use def_domain, only :  mgaus
    implicit none
    type(typ_valpr_ker)  :: prope_ker
    integer(ip)          :: kfl_ipoin,kfl_ielem,kfl_const
    integer(ip)          :: ielem,imate,ilaws,pgaus,pgaub
    integer(ip)          :: iboun,kfl_grele,kfl_grpoi,kfl_drele,kfl_gdele,pelty,pnode
    integer(4)           :: istat
    !
    ! Nullify pointers
    !
    nullify( prope_ker % value_ipoin   )
    nullify( prope_ker % grval_ipoin   )
    nullify( prope_ker % value_ielem   )
    nullify( prope_ker % grval_ielem   )
    nullify( prope_ker % value_iboun   )
    nullify( prope_ker % value_const   )
    !
    ! Define some flags
    !
    kfl_ipoin = 0
    kfl_ielem = 0
    kfl_const = 0

    kfl_grele = 0
    kfl_grpoi = 0
    kfl_drele = 0
    kfl_gdele = 0

    do imate = 1,nmate
       ilaws = prope_ker % ilaws(imate)
       if(      prope_ker % llaws(ilaws) % where == 'IPOIN' ) then
          kfl_ipoin = 1
          if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then
             kfl_grpoi = 1
          end if
       else if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then
          kfl_ielem = 1
          if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then
             kfl_grele = 1
          end if
          if( prope_ker % llaws(ilaws) % kfl_deriv == 1 ) then
             kfl_drele = 1
          end if
          if( prope_ker % llaws(ilaws) % kfl_grder == 1 ) then
             kfl_gdele = 1
          end if
       else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then
          kfl_const = 1
       end if
    end do
    !
    ! Allocate memory   
    !
    if ( (kfl_ipoin == 1 ) .or. ( kfl_ielem == 1 ) ) then
       !
       ! On nodes
       !
       ! even if the law is stored at ielem it might be smoothed if at some moment it is need at ipoin on npoin, so we allocate mem
       ! in order to save memory one could find a way to check if it will need to be smoothed -- needs to be thought
       !
       call memory_alloca(mem_modul(1:2,modul),'value_ipoin','ker_allpro',prope_ker % value_ipoin,npoin)
    end if
    if( ( kfl_ipoin == 1 ) .and. ( kfl_grpoi == 1 ) ) then
       call memory_alloca(mem_modul(1:2,modul),'grval_ipoin','ker_allpro',prope_ker % grval_ipoin,ndime,npoin)
    end if

    if( kfl_ielem == 1 ) then
       !
       ! On elements
       !
       call memory_alloca(mem_modul(1:2,modul),'value_ielem','ker_allpro',prope_ker % value_ielem,nelem)
       do ielem = 1,nelem
          pgaus = ngaus(abs(ltype(ielem)))
          if( kfl_cutel /= 0 ) pgaus = mgaus
          call memory_alloca(mem_modul(1:2,modul),'value_ielem(ielem)%a','ker_allpro',&
               prope_ker % value_ielem(ielem)%a,pgaus) 
       end do
       if( kfl_grele == 1 ) then
          call memory_alloca(mem_modul(1:2,modul),'grval_ielem','ker_allpro',prope_ker % grval_ielem,nelem)      
          do ielem = 1,nelem
             pgaus = ngaus(abs(ltype(ielem)))
             if( kfl_cutel /= 0 ) pgaus = mgaus
             call memory_alloca(mem_modul(1:2,modul),'grval_ielem(ielem)%a','ker_allpro',&
                  prope_ker % grval_ielem(ielem)%a,ndime,pgaus) 
          end do
       end if


       if( kfl_drele == 1 ) then
          call memory_alloca(mem_modul(1:2,modul),'drval_ielem','ker_allpro',prope_ker % drval_ielem,nelem) 
          do ielem = 1,nelem
             pgaus = ngaus(abs(ltype(ielem)))
             pelty = ltype(ielem)
             pnode = nnode(pelty)
             call memory_alloca(mem_modul(1:2,modul),'drval_ielem(ielem)%a','ker_allpro',&
                  prope_ker % drval_ielem(ielem)%a,pnode,pgaus) 
          end do
       end if

       if( kfl_gdele == 1 ) then
          call memory_alloca(mem_modul(1:2,modul),'gdval_ielem','ker_allpro',prope_ker % gdval_ielem,nelem) 
          do ielem = 1,nelem
             pgaus = ngaus(abs(ltype(ielem)))
             pelty = ltype(ielem)
             pnode = nnode(pelty)
             call memory_alloca(mem_modul(1:2,modul),'gdval_ielem(ielem)%a','ker_allpro',&
                  prope_ker % gdval_ielem(ielem)%a,ndime,pnode,pgaus) 
          end do
       end if

       !
       ! On boundaries
       !
       call memory_alloca(mem_modul(1:2,modul),'value_iboun','ker_allpro',prope_ker % value_iboun,nboun) 
       do iboun = 1,nboun
          pgaub = ngaus(abs(ltypb(iboun)))
          call memory_alloca(mem_modul(1:2,modul),'value_iboun(iboun)%a','ker_allpro',&
               prope_ker % value_iboun(iboun)%a,pgaub) 
       end do

    end if
    !
    ! Constant properties
    !
    if( kfl_const == 1 ) then
       call memory_alloca(mem_modul(1:2,modul),'value_const','ker_allpro',prope_ker % value_const,nmate)
    end if

  end subroutine ker_allpro

  subroutine ker_wiprop(prope_ker)
    !------------------------------------------------------------------------
    !****f* Master/ker_wippro
    ! NAME 
    !    ker_wippro
    ! DESCRIPTION
    !    Correspondance number vs name of properties
    ! USES
    !    postpr
    !    memgen
    ! USED BY
    !    ker_output
    !***
    !------------------------------------------------------------------------
    implicit none
    type(typ_valpr_ker)  :: prope_ker
    integer(ip)          :: imate,ilaws

    do imate = 1,nmate
       ilaws = 1
       do while( ilaws <=  mlaws_ker )
          if( prope_ker % llaws(ilaws) % wname == prope_ker % wlaws(imate) ) then
             prope_ker % ilaws(imate) = ilaws
             ilaws = mlaws_ker + 1
          end if
          ilaws = ilaws + 1
       end do
       if( ilaws /= mlaws_ker + 2 ) then
          write(*,*) 'KER_WIPROP: PROPERTY LAW DOES NOT EXIST:imate,trim(prope_ker % wlaws(imate))',&
                                                              imate,trim(prope_ker % wlaws(imate))
          ! this can happen if in your geo file you have more materias than those you define in ker.dat 
          call runend('KER_WIPROP: PROPERTY LAW DOES NOT EXIST')
       end if
    end do

  end subroutine ker_wiprop

  subroutine ker_smopro(prope_ker,xvalu)
    !------------------------------------------------------------------------
    !****f* Master/ker_smopro
    ! NAME 
    !    ker_smopro
    ! DESCRIPTION
    !    Smooth a property
    ! USES
    !    postpr
    !    memgen
    ! USED BY
    !    ker_output
    !***
    !------------------------------------------------------------------------
    use def_kintyp, only              :  ip,rp,r1p
    use def_kermod, only              :  typ_valpr_ker
    use def_master, only              :  INOTMASTER
    use def_domain, only              :  ndime,npoin,nelem,nnode,mnode,ntens
    use def_domain, only              :  lnods,ltype,coord,vmass,elmar,vmasc
    use def_domain, only              :  lexis,ngaus,kfl_naxis,mgaus,lelch
    use def_domain, only              :  lmate,lmatn,nmate
    use def_elmtyp  
    use mod_memory
    implicit none
    type(typ_valpr_ker), pointer      :: prope_ker
    real(rp),     intent(out), target :: xvalu(*) 
    integer(ip)                       :: ipoin,idime,inode,ielem,igaus
    integer(ip)                       :: pnode,pelty,pgaus,ilaws,imate
    integer(ip)                       :: jnode,knode
    integer(4)                        :: istat
    real(rp)                          :: detjm,gpvol,gpcar(ndime,mnode)
    real(rp)                          :: elcod(ndime,mnode)
    real(rp)                          :: xjaci(9),xjacm(9),xfact
    integer(ip),  pointer             :: lmele(:)
    real(rp),     pointer             :: vmass_ker(:)

    if( INOTMASTER ) then
       !
       ! Smooth property 
       !
       nullify( lmele )
       nullify( vmass_ker )
       call memory_alloca(mem_modul(1:2,modul),'LMELE'    ,'ker_smopro',lmele,nelem)
       call memory_alloca(mem_modul(1:2,modul),'VMASS_KER','ker_smopro',vmass_ker,npoin)
       do imate = 1,nmate 
          ilaws = prope_ker % ilaws(imate)
          if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then  
             do ielem = 1,nelem
                if( lmate(ielem) == imate ) lmele(ielem) = 1
             end do
          end if
       end do
       !
       ! Initialization
       !
       do ipoin = 1,npoin
          vmass_ker(ipoin) = 0.0_rp
          xvalu(ipoin)     = 0.0_rp
       end do
       !
       ! Loop over elements
       !
       elements: do ielem = 1,nelem
          pelty = ltype(ielem)

          if( pelty > 0 .and. lmele(ielem) == 1 ) then
             pnode = nnode(pelty)
             pgaus = ngaus(pelty)
             !
             ! Gather vectors
             !
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                do idime = 1,ndime
                   elcod(idime,inode) = coord(idime,ipoin)
                end do
             end do
             !
             ! Loop over Gauss points 
             !
             gauss_points: do igaus = 1,pgaus
                call elmder(&
                     pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&
                     elcod,gpcar,detjm,xjacm,xjaci)
                gpvol = elmar(pelty) % weigp(igaus) * detjm
                if( kfl_naxis == 1 ) then
                   call runend('MOD_GRADIE: NOT CODED')
                end if
                !
                ! Extension
                !
                if( lelch(ielem) == ELEXT ) then
                   knode = 1
                else
                   knode = pnode
                end if
                !
                ! Assemble
                !
                do inode = 1,knode
                   ipoin        = lnods(inode,ielem)
                   xfact        = gpvol * elmar(pelty) % shape(inode,igaus)
                   xvalu(ipoin) = xvalu(ipoin) + xfact * prope_ker % value_ielem(ielem) % a(igaus)
                   do jnode = 1,pnode
                      vmass_ker(ipoin) = vmass_ker(ipoin) + xfact * elmar(pelty) % shape(jnode,igaus)
                   end do
                end do

             end do gauss_points
          end if
       end do elements
       !
       ! Parallelization
       !
       call rhsmod(1_ip,xvalu)
       call rhsmod(1_ip,vmass_ker)
       !memory_alloca
       ! Solve diagonal system
       !
       do ipoin = 1,npoin
          if( vmass_ker(ipoin) /= 0.0_rp ) &
               xvalu(ipoin) = xvalu(ipoin) / vmass_ker(ipoin)
       end do
       !
       ! Deallocate memory
       !
       call memory_deallo(mem_modul(1:2,modul),'VMASS_KER','ker_smopro',vmass_ker)
       call memory_deallo(mem_modul(1:2,modul),'LMELE'    ,'ker_smopro',lmele)

    end if

  end subroutine ker_smopro

  subroutine ker_grapro(prope_ker,xvalu)
    !------------------------------------------------------------------------
    !****f* Master/ker_grapro
    ! NAME 
    !    ker_grapro
    ! DESCRIPTION
    !    Smooth the gradient of a property
    ! USES
    !    postpr
    !    memgen
    ! USED BY
    !    ker_output
    !***
    !------------------------------------------------------------------------
    use def_kintyp, only              :  ip,rp,r1p
    use def_kermod, only              :  typ_valpr_ker
    use def_master, only              :  INOTMASTER
    use def_domain, only              :  ndime,npoin,nelem,nnode,mnode,ntens
    use def_domain, only              :  lnods,ltype,coord,vmass,elmar,vmasc
    use def_domain, only              :  lexis,ngaus,kfl_naxis,mgaus,lelch
    use def_domain, only              :  lmate,lmatn,nmate
    use def_elmtyp  
    use mod_memory
    implicit none
    type(typ_valpr_ker), pointer      :: prope_ker
    real(rp),     intent(out), target :: xvalu(ndime,npoin) 
    integer(ip)                       :: ipoin,idime,inode,ielem,igaus
    integer(ip)                       :: pnode,pelty,pgaus,ilaws,imate
    integer(ip)                       :: jnode,knode
    integer(4)                        :: istat
    real(rp)                          :: detjm,gpvol,gpcar(ndime,mnode)
    real(rp)                          :: elcod(ndime,mnode)
    real(rp)                          :: grunk(ndime)
    real(rp)                          :: xjaci(9),xjacm(9),xfact
    integer(ip),  pointer             :: lmele(:)
    real(rp),     pointer             :: vmass_ker(:)

    if( INOTMASTER ) then
       !
       ! Smooth property 
       !
       nullify( lmele )
       nullify( vmass_ker )
       call memory_alloca(mem_modul(1:2,modul),'LMELE'    ,'ker_grapro',lmele,nelem)
       call memory_alloca(mem_modul(1:2,modul),'VMASS_KER','ker_grapro',vmass_ker,npoin)
       do imate = 1,nmate 
          ilaws = prope_ker % ilaws(imate) 
          if(    ( prope_ker % llaws(ilaws) % where == 'IELEM' .and.&
               &   prope_ker % llaws(ilaws) % kfl_gradi == 1 ) .or. &
               & ( prope_ker % llaws(ilaws) % where == 'IPOIN' .and.&
               &   prope_ker % llaws(ilaws) % kfl_gradi == 0 ) ) then  
             do ielem = 1,nelem
                if( lmate(ielem) == imate ) lmele(ielem) = ilaws
             end do
          end if
       end do
       !
       ! Initialization
       !
       do ipoin = 1,npoin
          vmass_ker(ipoin) = 0.0_rp
       end do
       do ipoin = 1,npoin
          do idime = 1,ndime
             xvalu(idime,ipoin) = 0.0_rp
          end do
       end do
       !
       ! Loop over elements
       !
       elements: do ielem = 1,nelem
          pelty = ltype(ielem)

          if( pelty > 0 .and. lmele(ielem) /= 0 ) then
             pnode = nnode(pelty)
             pgaus = ngaus(pelty)
             ilaws = lmele(ielem)
             !
             ! Gather vectors
             !
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                do idime = 1,ndime
                   elcod(idime,inode) = coord(idime,ipoin)
                end do
             end do
             !
             ! Loop over Gauss points 
             !
             gauss_points: do igaus = 1,pgaus
                call elmder(&
                     pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&
                     elcod,gpcar,detjm,xjacm,xjaci)
                gpvol = elmar(pelty) % weigp(igaus) * detjm

                if( kfl_naxis == 1 ) then
                   call runend('MOD_GRADIE: NOT CODED')
                end if
                !
                ! Extension
                !
                if( lelch(ielem) == ELEXT ) then
                   knode = 1
                else
                   knode = pnode
                end if
                !
                ! Assemble
                !
                if(        prope_ker % llaws(ilaws) % where == 'IELEM' &
                     .and. prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then

                   do inode = 1,knode
                      ipoin = lnods(inode,ielem)
                      xfact = gpvol * elmar(pelty) % shape(inode,igaus)
                      do idime = 1,ndime
                         xvalu(idime,ipoin) = xvalu(idime,ipoin) &
                              + xfact * prope_ker % grval_ielem(ielem) % a(idime,igaus)
                      end do
                      do jnode = 1,knode
                         vmass_ker(ipoin) = vmass_ker(ipoin) + xfact * elmar(pelty) % shape(jnode,igaus)
                      end do
                   end do

                else if(   prope_ker % llaws(ilaws) % where == 'IPOIN' &
                     .and. prope_ker % llaws(ilaws) % kfl_gradi == 0 ) then

                   grunk = 0.0_rp
                   do inode = 1,knode
                      ipoin = lnods(inode,ielem)
                      do idime = 1,ndime
                         grunk(idime) = grunk(idime) + prope_ker % value_ipoin(ipoin) * gpcar(idime,inode)
                      end do
                   end do
                   do inode = 1,knode
                      ipoin = lnods(inode,ielem)
                      xfact = gpvol * elmar(pelty) % shape(inode,igaus)
                      do idime = 1,ndime
                         xvalu(idime,ipoin) = xvalu(idime,ipoin) + xfact * grunk(idime)
                      end do
                      do jnode = 1,pnode
                         vmass_ker(ipoin) = vmass_ker(ipoin) + xfact * elmar(pelty) % shape(jnode,igaus)
                      end do
                   end do

                end if

             end do gauss_points
          end if
       end do elements
       !
       ! Parallelization
       !
       call rhsmod(ndime,xvalu)
       call rhsmod( 1_ip,vmass_ker)
       !
       ! Solve diagonal system
       !
       do ipoin = 1,npoin
          if( vmass_ker(ipoin) /= 0.0_rp ) then
             do idime = 1,ndime
                xvalu(idime,ipoin) = xvalu(idime,ipoin) / vmass_ker(ipoin)
             end do
          end if
       end do
       !
       ! Deallocate memory
       !
       call memory_deallo(mem_modul(1:2,modul),'VMASS_KER','ker_grapro',vmass_ker)
       call memory_deallo(mem_modul(1:2,modul),'LMELE'    ,'ker_grapro',lmele)

    end if

  end subroutine ker_grapro

  subroutine ker_proper(&
       wname,where,iposi,ielbo,xvalu,&
       qnode,qgaus,gpsha,gpcar,gpcor)
    !-----------------------------------------------------------------------
    !****f* Kermod/ker_proper
    ! NAME 
    !    ker_proper
    ! DESCRIPTION
    !    Update properties
    !
    !    Property can be defined with 3 formats
    !    It can be required at 8 different places
    !
    !       WHERE   IPOSI  IELBO
    !       --------------------
    !    1. IPOIN   IPOIN  NONSENSE                             
    !    2. NPOIN   DUMMI  Do smoothing => Loop over IELEM
    !    3. IGAUS   IGAUS  IELEM
    !    4. PGAUS   DUMMI  IELEM
    !    5. IGAUB   IGAUB  IBOUN        => IELEM = LBOEL(,IBOUN)
    !    6. PGAUB   DUMMI  IBOUN        => IELEM = LBOEL(,IBOUN)
    !    7. COG     DUMMI  IELEM
    !    8. PNODE   DUMMI  IELEM 
    !
    !    GPSHA is dimensioned as a vector in order to use it as optional 
    !    argument
    !
    ! USED BY
    !    many subroutines
    !***
    !-----------------------------------------------------------------------

    use def_kintyp,      only                 :  ip,rp
    use def_elmtyp,      only                 :  ELCUT
    use def_master,      only                 :  mem_modul,modul
    use def_domain,      only                 :  nmate,npoin,nelem,mnode,lelch
    use def_domain,      only                 :  lnods,lnnod,lmate,lmatn,nmatn
    use def_domain,      only                 :  ngaus,ltype,elmar,ndime,mgaus
    use def_domain,      only                 :  lboel,ltypb,lnodb,nnode,cutel
    use def_kermod,      only                 :  typ_valpr_ker
    use def_kermod,      only                 :  densi_ker,visco_ker 
    use def_kermod,      only                 :  poros_ker,condu_ker
    use def_kermod,      only                 :  sphea_ker,dummy_ker
    use def_kermod,      only                 :  turmu_ker
    use mod_cutele,      only                 :  pgaus_sub   
    use mod_memory
    implicit none
    character(5),        intent(in)           :: wname,where
    integer(ip),         intent(in)           :: iposi,ielbo
    real(rp),            intent(out)          :: xvalu(*) 
    integer(ip),         intent(in), optional :: qnode
    integer(ip),         intent(in), optional :: qgaus
    real(rp),            intent(in), optional :: gpsha(*)
    real(rp),            intent(in), optional :: gpcar(ndime,mnode,*)
    real(rp),            intent(in), optional :: gpcor(ndime,*)
    type(typ_valpr_ker), pointer              :: prope_ker
    integer(ip)                               :: imate,ielem,inode,ipoin,igaus,kauxi
    integer(ip)                               :: kpoin,ilaws,pelty,pgaus,pnode,jpoin
    integer(ip)                               :: pnodb,igaub,inodb,pblty,iboun
    integer(ip)                               :: pgaub,kposi,kfl_gradi,idime,kfl_deriv,kfl_grder
    integer(ip)                               :: lposi
    real(rp)                                  :: elpro(mnode)
    real(rp)                                  :: elgrp(ndime,max(mgaus,mnode))
    real(rp),   pointer                       :: auxva(:)

    nullify(auxva)
    !----------------------------------------------------------------------
    !
    ! Some definitions
    !
    !----------------------------------------------------------------------

    kfl_gradi = 0
    if( wname(1:2) == 'GR' ) kfl_gradi = 1

    kfl_deriv = 0
    if( wname(1:2) == 'DR' ) kfl_deriv = 1

    kfl_grder = 0
    if( wname(1:2) == 'GD' ) kfl_grder = 1

    if(      wname == 'DENSI' .or. wname == 'GRDEN' .or. wname == 'DRDEN' .or. wname == 'GDDEN') then
       prope_ker => densi_ker
    else if( wname == 'VISCO' .or. wname == 'GRVIS' ) then
       prope_ker => visco_ker
    else if( wname == 'POROS' .or. wname == 'GRPOS' ) then
       prope_ker => poros_ker
    else if( wname == 'CONDU' .or. wname == 'GRCON' ) then
       prope_ker => condu_ker
    else if( wname == 'SPHEA' .or. wname == 'GRSPE' ) then
       prope_ker => sphea_ker
    else if( wname == 'DUMMY' .or. wname == 'GRDUM' ) then
       prope_ker => dummy_ker
    else if( wname == 'TURBU' .or. wname == 'GRTUR' ) then
       prope_ker => turmu_ker
    else 
       print *,'--- I DONT UNDERSTAND ',wname
       call runend('KER_PROPER CALLED WITH WRONG PROPERTY') ! This avoids the bug of calling wrongly the ker_proper routine
    end if

    if( prope_ker % kfl_exist == 0 ) then
       !
       ! Put value to zero
       !
       if( where == 'IPOIN' .or. where == 'IGAUS' .or. where == 'IGAUB' .or. where == 'COG' ) then
          kposi = 1
       else if( where == 'NPOIN' ) then
          kposi = npoin
       else if( where == 'PNODE' ) then
          kposi = lnnod(ielbo)
       else if( where == 'PGAUS' ) then 
          if( present(gpsha) ) then
             kposi = qgaus
          else
             kposi = ngaus(abs(ltype(ielbo)))
          end if
       else if( where == 'PGAUB' ) then 
          if( present(gpsha) ) then
             kposi = qgaus
          else
             kposi = ngaus(abs(ltypb(ielbo)))
          end if
       end if
       if( kfl_gradi == 1 ) kposi = kposi * ndime
       do ielem = 1,kposi
          xvalu(ielem) = 0.0_rp
       end do
    else

       if( present(gpsha) ) then
          if( .not. present(qnode) .or. .not. present(qgaus) ) call runend('MOD_KER_PROPER: WRONG ARGUMENTS')
       end if

       !----------------------------------------------------------------------
       !
       ! 1. Property required on IPOIN
       !
       !----------------------------------------------------------------------

       if( where == 'IPOIN' ) then

          if( kfl_gradi == 1 ) then
             call runend('KER_PROPER: DONT KNOW WHAT TO DO WITH THIS OPTION')
          end if

          ipoin = iposi 
          !
          ! Smooth value - If in some material it is stored by ielem it is smoothed 
          ! In the materials where it is not stored by ielem it will step over with erroneous values 
          ! Therefore if is then recalculated in materials where it is stored by IPOIN or CONST 
          !
          kauxi = 0
          if ( prope_ker % kfl_nedsm == 1 ) then
             imat2_loop: do imate = 1,nmate
                ilaws = prope_ker % ilaws(imate)
                if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then
                   call memory_alloca(mem_modul(1:2,modul),'auxva','ker_proper',auxva,npoin)
                   call ker_smopro(prope_ker,auxva)
                   prope_ker % kfl_nedsm = 0
                   kauxi = 1
                   exit imat2_loop
                end if
             end do imat2_loop
          end if

          if (kauxi == 1) then  ! correct the value (auxva) in points where it is calculated by ipoin or const 
             ! and has been steped over when smoothing due to materials whith law at ielem
             do imate = 1,nmate
                ilaws = prope_ker % ilaws(imate)

                if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

                   do kpoin = 1,nmatn(imate) 
                      jpoin = lmatn(imate) % l(kpoin)
                      auxva(jpoin) = prope_ker % value_ipoin(jpoin)
                   end do

                else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

                   do kpoin = 1,nmatn(imate) 
                      jpoin = lmatn(imate) % l(kpoin)
                      auxva(jpoin) = prope_ker % value_const(imate)
                   end do

                end if
             end do
             ! 
             ! Save values obtained in auxva to prope_ker % value_ipoin(jpoin)
             !
             do kpoin =1,npoin
                prope_ker % value_ipoin(kpoin) = auxva(kpoin)
             end do
             call memory_deallo(mem_modul(1:2,modul),'auxva','ker_proper',auxva)
          end if


          do imate = 1,nmate
             ilaws = prope_ker % ilaws(imate)

             if( ( prope_ker % llaws(ilaws) % where == 'IPOIN' ) .or. ( prope_ker % llaws(ilaws) % where == 'IELEM' ) )then  

                xvalu(1) = prope_ker % value_ipoin(ipoin)

             else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

                xvalu(1) = prope_ker % value_const(imate)

             end if
          end do
       end if

       !----------------------------------------------------------------------
       !
       ! 2. Property required on NPOIN
       !
       !----------------------------------------------------------------------

       if( where == 'NPOIN' ) then

          if( kfl_gradi == 1 ) then
             !
             ! Gradient
             !
             imat0_loop: do imate = 1,nmate
                ilaws = prope_ker % ilaws(imate)
                if(    ( prope_ker % llaws(ilaws) % where == 'IELEM' .and.&
                     &   prope_ker % llaws(ilaws) % kfl_gradi == 1 ) .or. &
                     & ( prope_ker % llaws(ilaws) % where == 'IPOIN' .and.&
                     &   prope_ker % llaws(ilaws) % kfl_gradi == 0 ) ) then  
                   call ker_grapro(prope_ker,xvalu)
                   exit imat0_loop
                end if
             end do imat0_loop

             do imate = 1,nmate
                ilaws = prope_ker % ilaws(imate)

                if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

                   if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then

                      do kpoin = 1,nmatn(imate) 
                         ipoin = lmatn(imate) % l(kpoin)
                         kposi = (ipoin-1) * ndime
                         do idime = 1,ndime
                            kposi = kposi + 1
                            xvalu(kposi) = prope_ker % grval_ipoin(idime,ipoin)
                         end do
                      end do

                   end if

                else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

                   do kpoin = 1,nmatn(imate) 
                      ipoin = lmatn(imate) % l(kpoin)
                      kposi = (ipoin-1) * ndime
                      do idime = 1,ndime
                         kposi = kposi + 1
                         xvalu(kposi) = 0.0_rp
                      end do
                   end do

                end if
             end do

          else
             !
             ! Value - If in some material it is stored by ielem it is smoothed 
             ! In the materials where it is not stored by ielem it will step over with erroneous values 
             ! Therefore if is then recalculated in materials where it is stored by IPOIN or CONST 
             !
             kauxi = 0
             if ( prope_ker % kfl_nedsm == 1 ) then
                imat1_loop: do imate = 1,nmate
                   ilaws = prope_ker % ilaws(imate)
                   if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then 
                      call memory_alloca(mem_modul(1:2,modul),'auxva','ker_proper',auxva,npoin) 
                      call ker_smopro(prope_ker,auxva)
                      prope_ker % kfl_nedsm = 0
                      kauxi = 1
                      exit imat1_loop
                   end if
                end do imat1_loop
             end if

             if (kauxi == 1) then  ! correct the value (auxva) in points where it is calculated by ipoin or const 
                ! and has been steped over when smoothing due to materials whith law at ielem
                do imate = 1,nmate
                   ilaws = prope_ker % ilaws(imate)

                   if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

                      do kpoin = 1,nmatn(imate) 
                         jpoin = lmatn(imate) % l(kpoin)
                         auxva(jpoin) = prope_ker % value_ipoin(jpoin)
                      end do

                   else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

                      do kpoin = 1,nmatn(imate) 
                         jpoin = lmatn(imate) % l(kpoin)
                         auxva(jpoin) = prope_ker % value_const(imate)
                      end do

                   end if
                end do
                ! 
                ! Save values obtained in auxva to prope_ker % value_ipoin(jpoin)
                !
                do kpoin =1,npoin
                   prope_ker % value_ipoin(kpoin) = auxva(kpoin)
                end do
                call memory_deallo(mem_modul(1:2,modul),'auxva','ker_proper',auxva)
             end if

             do imate = 1,nmate
                ilaws = prope_ker % ilaws(imate)

                if( ( prope_ker % llaws(ilaws) % where == 'IPOIN' ) .or. ( prope_ker % llaws(ilaws) % where == 'IELEM' ) )then  

                   do kpoin = 1,nmatn(imate) 
                      jpoin = lmatn(imate) % l(kpoin)
                      xvalu(jpoin) = prope_ker % value_ipoin(jpoin)
                   end do

                else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

                   do kpoin = 1,nmatn(imate) 
                      jpoin = lmatn(imate) % l(kpoin)
                      xvalu(jpoin) = prope_ker % value_const(imate)
                   end do

                end if
             end do
          end if

       end if

       !----------------------------------------------------------------------
       !
       ! 3. Property required on IGAUS
       !
       !----------------------------------------------------------------------

       if( where == 'IGAUS' ) then

          igaus = iposi
          ielem = ielbo
          imate = lmate(ielem)
          ilaws = prope_ker % ilaws(imate)

          if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then  

             if( kfl_gradi == 1 ) then
                ! 
                ! Gradient
                !
                if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then
                   do idime = 1,ndime
                      xvalu(idime) = prope_ker % grval_ielem(ielem) % a(idime,igaus)
                   end do
                else                
                   xvalu(1:ndime) = 0.0_rp
                end if
             else
                ! 
                ! Value
                !
                xvalu(1) = prope_ker % value_ielem(ielem) % a(igaus)
             end if

          else if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

             pelty = abs(ltype(ielem))
             pnode = lnnod(ielem)
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                elpro(inode) = prope_ker % value_ipoin(ipoin)
             end do

             if( kfl_gradi == 1 ) then
                ! 
                ! Gradient
                !
                xvalu(1:ndime) = 0.0_rp

                if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then

                   do inode = 1,pnode
                      ipoin = lnods(inode,ielem)
                      do idime = 1,ndime
                         elgrp(idime,inode) = prope_ker % grval_ipoin(idime,ipoin)
                      end do
                   end do
                   if( present(gpsha) ) then
                      kposi = (igaus-1) * qnode
                      do inode = 1,pnode
                         kposi = kposi + 1
                         do idime = 1,ndime
                            xvalu(idime) = xvalu(idime) + elgrp(idime,inode) * gpsha(kposi)
                         end do
                      end do
                   else
                      do inode = 1,pnode
                         do idime = 1,ndime
                            xvalu(idime) = xvalu(idime) + elgrp(idime,inode) * elmar(pelty) % shape(idime,igaus)
                         end do
                      end do
                   end if

                else

                   if( present(gpcar) ) then
                      do inode = 1,pnode
                         do idime = 1,ndime
                            xvalu(idime) = xvalu(idime) + elpro(inode) * gpcar(idime,inode,1)
                         end do
                      end do
                   else
                      call runend('MOD_KER_PROPER: GPCAR NEEDED')
                   end if
                end if

             else
                ! 
                ! Value
                !
                xvalu(1) = 0.0_rp
                if( present(gpsha) ) then
                   kposi    = (igaus-1) * qnode
                   do inode = 1,pnode
                      kposi    = kposi + 1
                      xvalu(1) = xvalu(1) + elpro(inode) * gpsha(kposi)
                   end do
                else
                   do inode = 1,pnode
                      xvalu(1) = xvalu(1) + elpro(inode) * elmar(pelty) % shape(inode,igaus)
                   end do
                end if
             end if

          else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

             if( kfl_gradi == 1 ) then
                xvalu(1:ndime) = 0.0_rp
             else
                xvalu(1) = prope_ker % value_const(imate)
             end if
          end if

       end if

       !----------------------------------------------------------------------
       !
       ! 4. Property required on IGAUS=1,PGAUS
       !
       !----------------------------------------------------------------------

       if( where == 'PGAUS' ) then

          ielem = ielbo
          imate = lmate(ielem)
          ilaws = prope_ker % ilaws(imate)
          pelty = abs(ltype(ielem))
          pgaus = ngaus(pelty) 
          if( lelch(ielem) == ELCUT ) pgaus = cutel(ielem) % nelem * pgaus_sub

          if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then  

             if( kfl_gradi == 1 ) then

                if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then

                   if( present(gpsha) ) then
                      if( qgaus /= pgaus .and. kfl_cutel == 0 ) call runend('MOD_KER_PROPER: PGAUS NOT CODED')
                      do igaus = 1,qgaus
                         kposi = (igaus - 1) * ndime                 
                         do idime = 1,ndime
                            kposi = kposi + 1
                            xvalu(kposi) = prope_ker % grval_ielem(ielem) % a(idime,igaus)
                         end do
                      end do
                   else
                      do igaus = 1,pgaus
                         kposi = (igaus - 1) * ndime                      
                         do idime = 1,ndime
                            kposi = kposi + 1
                            xvalu(kposi) = prope_ker % grval_ielem(ielem) % a(idime,igaus)
                         end do
                      end do
                   end if
                else
                   do kposi = 1,pgaus*ndime
                      xvalu(kposi) = 0.0_rp
                   end do
                end if

             else if( kfl_deriv == 1 ) then

                if( prope_ker % llaws(ilaws) % kfl_deriv == 1 ) then
                   
                   if( present(gpsha) ) then
                      if( qgaus /= pgaus ) call runend('MOD_KER_PROPER: PGAUS NOT CODED')
                      do igaus = 1,qgaus
                         kposi = (igaus - 1) * qnode                 
                         do inode = 1,qnode
                            kposi = kposi + 1
                            xvalu(kposi) = prope_ker % drval_ielem(ielem) % a(inode,igaus)
                         end do
                      end do
                   else
                      do igaus = 1,pgaus
                         kposi = (igaus - 1) * qnode                      
                         do inode = 1,qnode
                            kposi = kposi + 1
                            xvalu(kposi) = prope_ker % drval_ielem(ielem) % a(inode,igaus)
                         end do
                      end do
                   end if
                else
                   do kposi = 1,pgaus*qnode
                      xvalu(kposi) = 0.0_rp
                   end do
                end if
                
             else if( kfl_grder == 1 ) then

                if( prope_ker % llaws(ilaws) % kfl_grder == 1 ) then
                   
                   if( present(gpsha) ) then
                      if( qgaus /= pgaus ) call runend('MOD_KER_PROPER: PGAUS NOT CODED')
                      do igaus = 1,qgaus
                         kposi = (igaus - 1)* qnode*ndime
                         do inode = 1,qnode
                            do idime = 1, ndime
                                kposi = kposi + 1
                                xvalu(kposi) = prope_ker % gdval_ielem(ielem) % a(idime,inode,igaus)
                            enddo
                         end do
                      end do
                   else
                      do igaus = 1,pgaus
                         do inode = 1,qnode
                            kposi = (igaus - 1)*(inode-1) * qnode*ndime
                            do idime = 1, ndime
                                kposi = kposi + 1
                                xvalu(kposi) = prope_ker % gdval_ielem(ielem) % a(idime,inode,igaus)
                            enddo
                         end do
                      end do
                   end if
                else
                   do kposi = 1,pgaus*qnode*ndime
                      xvalu(kposi) = 0.0_rp
                   end do
                end if
                
             else

                if( present(gpsha) .and. kfl_cutel == 0 ) then
                   if( qgaus /= pgaus ) call runend('MOD_KER_PROPER: PGAUS NOT CODED')
                   do igaus = 1,qgaus
                      xvalu(igaus) = prope_ker % value_ielem(ielem) % a(igaus)
                   end do
                else
                   do igaus = 1,pgaus
                      xvalu(igaus) = prope_ker % value_ielem(ielem) % a(igaus)
                   end do
                end if
             end if

          else if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

             pelty = abs(ltype(ielem))
             pnode = lnnod(ielem)
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                elpro(inode) = prope_ker % value_ipoin(ipoin)
             end do

             if( kfl_gradi == 1 ) then
                !
                ! Gradient
                !
                do kposi = 1,pgaus*ndime
                   xvalu(kposi) = 0.0_rp
                end do

                if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then

                   do inode = 1,pnode
                      ipoin = lnods(inode,ielem)
                      do idime = 1,ndime
                         elgrp(idime,inode) = prope_ker % grval_ipoin(idime,ipoin)
                      end do
                   end do
                   if( present(gpsha) ) then
                      do igaus = 1,qgaus
                         kposi = (igaus-1) * qnode
                         do inode = 1,pnode
                            kposi = kposi + 1
                            lposi = (igaus-1) * ndime
                            do idime = 1,ndime
                               lposi = lposi + 1
                               xvalu(lposi) = xvalu(lposi) + elgrp(idime,inode) * gpsha(kposi)
                            end do
                         end do
                      end do
                   else
                      do igaus = 1,qgaus
                         do inode = 1,pnode
                            lposi = (igaus-1) * ndime
                            do idime = 1,ndime
                               lposi = lposi + 1
                               xvalu(lposi) = xvalu(lposi) + elgrp(idime,inode) * elmar(pelty) % shape(inode,igaus)
                            end do
                         end do
                      end do
                   end if

                else

                   if( present(gpcar) ) then
                      do igaus = 1,qgaus
                         kposi = (igaus-1) * ndime
                         do idime = 1,ndime
                            kposi = kposi + 1
                            do inode = 1,pnode
                               xvalu(kposi) = xvalu(kposi) + elpro(inode) * gpcar(idime,inode,igaus)
                            end do
                         end do
                      end do
                   end if

                end if

             else
                !
                ! Value
                !
                if( present(gpsha) ) then
                   do igaus = 1,qgaus
                      xvalu(igaus) = 0.0_rp
                      kposi        = (igaus-1) * qnode
                      do inode = 1,pnode
                         kposi = kposi + 1
                         xvalu(igaus) = xvalu(igaus) + elpro(inode) * gpsha(kposi)
                      end do
                   end do
                else
                   do igaus = 1,ngaus(pelty)
                      xvalu(igaus) = 0.0_rp
                      do inode = 1,pnode
                         xvalu(igaus) = xvalu(igaus) + elpro(inode) * elmar(pelty) % shape(inode,igaus)
                      end do
                   end do
                end if
             end if

          else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

             if( kfl_gradi == 1 ) then
                !
                ! Gradient
                !
                do kposi = 1,pgaus*ndime
                   xvalu(kposi) = 0.0_rp
                end do

             else
                !
                ! Value
                !
                if( present(gpsha) ) then
                   do igaus = 1,qgaus
                      xvalu(igaus) = prope_ker % value_const(imate)
                   end do
                else            
                   do igaus = 1,ngaus(abs(ltype(ielem)))
                      xvalu(igaus) = prope_ker % value_const(imate)
                   end do
                end if

             end if

          end if

       end if

       !----------------------------------------------------------------------
       !
       ! 5. Property required on IGAUB
       !
       !----------------------------------------------------------------------

       if( where == 'IGAUB' ) then

          if( kfl_gradi == 1 )  call runend('MOD_KER_PROPER: GRADIENT NOT AVILABLE ON BOUNDARY')
          igaub = iposi
          iboun = ielbo
          pblty = abs(ltypb(iboun))
          pnodb = nnode(pblty)
          ielem = lboel(pnodb+1,iboun)
          imate = lmate(ielem)
          ilaws = prope_ker % ilaws(imate)

          if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then  

             if( present(gpsha) ) then
                if( qgaus /= ngaus(pblty) ) call runend('MOD_KER_PROPER: IGAUB NOT CODED')
             end if
             xvalu(1) = prope_ker % value_iboun(iboun) % a(igaub)

          else if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

             do inodb = 1,pnodb
                ipoin = lnodb(inodb,iboun)
                elpro(inodb) = prope_ker % value_ipoin(ipoin)
             end do
             xvalu(1) = 0.0_rp
             if( present(gpsha) ) then
                kposi = (igaub-1) * qnode
                do inodb = 1,pnodb
                   kposi = kposi + 1
                   xvalu(1) = xvalu(1) + elpro(inodb) * gpsha(kposi)
                end do
             else
                do inodb = 1,pnodb
                   xvalu(1) = xvalu(1) + elpro(inodb) * elmar(pblty) % shape(inodb,igaub)
                end do
             end if

          else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

             xvalu(1) = prope_ker % value_const(imate)

          end if

       end if

       !----------------------------------------------------------------------
       !
       ! 6. Property required on IGAUB=1,PGAUB
       !
       !----------------------------------------------------------------------

       if( where == 'PGAUB' ) then

          if( kfl_gradi == 1 ) call runend('MOD_KER_PROPER: GRADIENT NOT AVILABLE ON BOUNDARY')

          iboun = ielbo
          pblty = abs(ltypb(iboun))
          pnodb = nnode(pblty)
          ielem = lboel(pnodb+1,iboun)
          imate = lmate(ielem)
          ilaws = prope_ker % ilaws(imate)

          if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then  

             if( present(gpsha) ) then
                if( qgaus /= ngaus(pblty) ) call runend('MOD_KER_PROPER: IGAUB NOT CODED')
             end if
             do igaub = 1,ngaus(pblty)
                xvalu(igaub) = prope_ker % value_iboun(iboun) % a(igaub)
             end do

          else if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

             do inodb = 1,pnodb
                ipoin = lnodb(inodb,iboun)
                elpro(inodb) = prope_ker % value_ipoin(ipoin)
             end do

             if( present(gpsha) ) then
                do igaub = 1,qgaus
                   xvalu(igaub) = 0.0_rp
                   kposi = (igaub-1) * qnode
                   do inodb = 1,pnodb
                      kposi        = kposi + 1
                      xvalu(igaub) = xvalu(igaub) + elpro(inodb) * gpsha(kposi)
                   end do
                end do
             else
                pgaub = ngaus(pblty)
                do igaub = 1,pgaub
                   xvalu(igaub) = 0.0_rp
                   do inodb = 1,pnodb
                      xvalu(igaub) = xvalu(igaub) + elpro(inodb) * elmar(pblty) % shape(inodb,igaub)
                   end do
                end do
             end if

          else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

             do igaub = 1,ngaus(abs(ltypb(iboun)))
                xvalu(igaub) = prope_ker % value_const(imate)
             end do

          end if

       end if

       !----------------------------------------------------------------------
       !
       ! 7. Property required on C.O.G.
       !
       !----------------------------------------------------------------------

       if( where(1:3) == 'COG' ) then

          ielem = ielbo
          imate = lmate(ielem)
          ilaws = prope_ker % ilaws(imate)
          pelty = abs(ltype(ielem))
          pgaus = ngaus(pelty)
          pnode = nnode(pelty)
          if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then  

             if( kfl_gradi == 1 )  then
                !
                ! Gradient
                !
                if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then
                   xvalu(1:ndime) = 0.0_rp
                   do igaus = 1,pgaus
                      do idime = 1,ndime
                         xvalu(idime) = xvalu(idime) + prope_ker % grval_ielem(ielem) % a(idime,igaus)
                      end do
                   end do
                   do idime = 1,ndime
                      xvalu(idime) = xvalu(idime) / real(pgaus)
                   end do
                else
                   call runend('MOD_KER_PROPER: GRDAIENT ON COG NOT CODED')
                end if

             else
                !
                ! Value
                !
                xvalu(1) = 0.0_rp
                if( present(gpsha) ) then               
                   do igaus = 1,qgaus
                      xvalu(1) = xvalu(1) + prope_ker % value_ielem(ielem) % a(igaus)
                   end do
                   xvalu(1) = xvalu(1) / real(qgaus)
                else
                   do igaus = 1,pgaus
                      xvalu(1) = xvalu(1) + prope_ker % value_ielem(ielem) % a(igaus)
                   end do
                   xvalu(1) = xvalu(1) / real(pgaus)
                end if
             end if

          else if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

             if( kfl_gradi == 1 )  then
                !
                ! Gradient
                !
                xvalu(1:ndime) = 0.0_rp

                if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then

                   do inode = 1,pnode
                      ipoin = lnods(inode,ielem)
                      do idime = 1,ndime
                         xvalu(idime) = xvalu(idime) + prope_ker % grval_ipoin(idime,ipoin) 
                      end do
                   end do
                   do idime = 1,ndime
                      xvalu(idime) = xvalu(idime) / real(pnode)  
                   end do

                else

                   if( present(gpcar) ) then
                      do igaus = 1,qgaus
                         do inode = 1,pnode
                            ipoin = lnods(inode,ielem)
                            do idime = 1,ndime
                               xvalu(idime) = xvalu(idime) &
                                    + prope_ker % value_ipoin(ipoin) * gpcar(idime,inode,1)
                            end do
                         end do
                      end do
                      do idime = 1,ndime
                         xvalu(idime) = xvalu(idime) / real(qgaus)
                      end do
                   else
                      call runend('MOD_KER_PROPER: GRADIENT NOT AVAILABLE AT COG')
                   end if

                end if

             else
                !
                ! Value
                !
                xvalu(1) = 0.0_rp
                do inode = 1,pnode
                   ipoin    = lnods(inode,ielem)
                   xvalu(1) = xvalu(1) + prope_ker % value_ipoin(ipoin) 
                end do
                xvalu(1) = xvalu(1) / real(pnode)
             end if

          else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

             if( kfl_gradi == 1 )  then
                xvalu(1:ndime) = 0.0_rp
             else
                xvalu(1) = prope_ker % value_const(imate)
             end if

          end if

       end if

       !----------------------------------------------------------------------
       !
       ! 8. Property required on PNODE
       !
       !----------------------------------------------------------------------

       if( where == 'PNODE' ) then

          ielem = ielbo
          pelty = abs(ltype(ielem))
          imate = lmate(ielem)
          ilaws = prope_ker % ilaws(imate)
          pgaus = ngaus(pelty)
          pnode = nnode(pelty)

          if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then  

             if( present(gpsha) ) then
                if( pgaus /= qgaus ) call runend('MOD_KER_PROPER: COG NOT CODED')
             end if

             if( kfl_gradi == 1 ) then
                !
                ! Gradient
                !
                do inode = 1,ndime*pnode
                   xvalu(inode) = 0.0_rp
                end do

                if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then

                   do igaus = 1,pgaus
                      do inode = 1,pnode
                         kposi = (inode-1) * ndime
                         do idime = 1,ndime
                            kposi = kposi + 1
                            xvalu(kposi) = xvalu(kposi) + prope_ker % grval_ielem(ielem) % a(idime,igaus) &
                                 * elmar(pelty) % shaga(igaus,inode)
                         end do
                      end do
                   end do

                else

                   do inode = 1,pnode
                      ipoin = lnods(inode,ielem)
                      elpro(inode) = 0.0_rp
                      do igaus = 1,pgaus
                         elpro(inode) = elpro(inode) + prope_ker % value_ielem(ielem) % a(igaus) &
                              * elmar(pelty) % shaga(igaus,inode)
                      end do
                   end do
                   do igaus = 1,pgaus
                      do inode = 1,pnode
                         do idime = 1,ndime
                            elgrp(idime,igaus) = elgrp(idime,igaus) + elpro(inode) * elmar(pelty) % shape(inode,igaus)
                         end do
                      end do
                   end do
                   do igaus = 1,pgaus
                      do inode = 1,pnode
                         kposi = (inode-1) * ndime
                         do idime = 1,ndime
                            kposi = kposi + 1
                            xvalu(kposi) = xvalu(kposi) + elgrp(idime,igaus) * elmar(pelty) % shaga(igaus,inode)
                         end do
                      end do
                   end do

                end if

             else
                !
                ! Value
                !
                do inode = 1,pnode
                   xvalu(inode) = 0.0_rp
                   do igaus = 1,pgaus
                      xvalu(inode) = xvalu(inode) + prope_ker % value_ielem(ielem) % a(igaus) &
                           * elmar(pelty) % shaga(igaus,inode)
                   end do
                end do

             end if

          else if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

             if( kfl_gradi == 1 ) then
                !
                ! Gradient
                !
                do inode = 1,ndime*pnode
                   xvalu(inode) = 0.0_rp
                end do
                if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then
                   do inode = 1,pnode
                      kposi = (inode-1) * ndime
                      do idime = 1,ndime
                         kposi = kposi + 1
                         xvalu(kposi) = prope_ker % grval_ipoin(idime,ipoin) 
                      end do
                   end do
                else
                   do igaus = 1,pgaus
                      do inode = 1,pnode
                         ipoin = lnods(inode,ielem)
                         do idime = 1,ndime
                            elgrp(idime,igaus) = elgrp(idime,igaus) + prope_ker % value_ipoin(ipoin) * gpcar(idime,inode,igaus)
                         end do
                      end do
                   end do
                   do igaus = 1,pgaus
                      do inode = 1,pnode
                         kposi = (inode-1) * ndime
                         do idime = 1,ndime
                            kposi = kposi + 1
                            xvalu(kposi) = xvalu(kposi) + elgrp(idime,igaus) * elmar(pelty) % shaga(igaus,inode)
                         end do
                      end do
                   end do
                end if
             else
                !
                ! Value
                !
                do inode = 1,pnode
                   ipoin = lnods(inode,ielem)
                   xvalu(inode) = prope_ker % value_ipoin(ipoin)
                end do
             end if

          else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

             if( kfl_gradi == 1 ) then
                do inode = 1,ndime*pnode
                   xvalu(inode) = 0.0_rp
                end do
             else
                do inode = 1,pnode
                   xvalu(inode) = prope_ker % value_const(imate)
                end do
             end if

          end if

       end if

       !----------------------------------------------------------------------
       !
       ! 9. Property required Anywhere
       !
       !----------------------------------------------------------------------

       if( where == 'ANYWH' ) then

          igaus = iposi
          ielem = ielbo
          imate = lmate(ielem)
          ilaws = prope_ker % ilaws(imate)

          if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then  

             call runend('MOD_KER_PROPER: TEST THIS WITH LAGRANGE')

          else if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then  

             pelty = abs(ltype(ielem))
             pnode = lnnod(ielem)
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                elpro(inode) = prope_ker % value_ipoin(ipoin)
             end do
             xvalu(1) = 0.0_rp
             if( present(gpsha) ) then
                kposi    = (igaus-1) * qnode
                do inode = 1,pnode
                   kposi    = kposi + 1
                   xvalu(1) = xvalu(1) + elpro(inode) * gpsha(kposi)
                end do
             else
                call runend('MOD_KER_PROPER: NOT CODED')
             end if

          else if( prope_ker % llaws(ilaws) % where == 'CONST' ) then

             xvalu(1) = prope_ker % value_const(imate)

          end if

       end if

    end if
    nullify(prope_ker) ! This avoids the bug of calling wrongly the ker_proper routine

  end subroutine ker_proper

  subroutine ker_updpro(itask_xxx) 

    !-----------------------------------------------------------------------
    !****f* Kermod/ker_updpro
    ! NAME 
    !    ker_updpro
    ! DESCRIPTION
    !    Update properties
    !-----------------------------------------------------------------------

    use def_domain
    use def_master
    use def_kermod
    use def_parame, only :  pi
    use def_elmtyp, only :  ELCUT
    use def_elmtyp, only :  TET04
    use def_elmtyp, only :  TRI03
    implicit none
    integer(ip), intent(in), optional  :: itask_xxx
    integer(ip), save            ::  ipass(100) = 0    
    integer(ip)                  :: itask, kfl_force
    integer(ip)                  :: imate,pnode,ielem,ipoin,kpoin
    integer(ip)                  :: pgaus,inode,pelty,inodb,pblty
    integer(ip)                  :: iprop,ilaws,pnodb,iboun,pgaub,igaub
    integer(ip)                  :: iresp,kfl_updpr(10),idime,igaus
    integer(ip)                  :: ispec,jspec,jdime,pelty_sub
    real(rp)                     :: gpsha(mnode,mgaus)  
    real(rp)                     :: gpcar(ndime,mnode,mgaus)
    real(rp)                     :: gphes(ntens,mnode,mgaus)
    real(rp)                     :: gpvol(mgaus)
    real(rp)                     :: dummr(ndime*mgaus)
    real(rp)                     :: elcod(ndime,mnode)
    real(rp)                     :: elvel(ndime,mnode)
    real(rp)                     :: gpcor(ndime,mgaus)
    real(rp)                     :: gp_temperature(mgaus)
    real(rp)                     :: gp_grad_tem(ndime,mgaus), gp_grad_wmean(ndime,mgaus)
    real(rp)                     :: gpvel(ndime,mgaus)
    real(rp)                     :: gpgve(ndime,ndime,mgaus)
    real(rp)                     :: gptur
    real(rp)                     :: gbvel(ndime),gbgve(ndime,ndime)

    real(rp)                     :: ellev(mnode),eltem(mnode),elwmean(mnode)
    real(rp)                     :: prope_wat,prope_air
    real(rp)                     :: hleng(mgaus)

    real(rp)                     :: eps,ooeps,oovpi,f,phi,kap
    real(rp)                     :: densi_phase(2),z0,rho,ustar

    real(rp)                     :: T,T0,mu0,mu,S0,const, heigh, cd, cdlad, auxva , lad

    real(rp)                     :: phiij,sumphi(nspec),gptem,gbtem,gbwme,gphog(mgaus),zmaxi,zmaxf,gpcah(mgaus)
    real (rp)                    :: lamax, zz, n, shaaa, hoveg, gpwmean
    real (rp),save               :: integ

    real (rp),parameter          :: h_can_critical=7.0 ! beware if this value is changed it should also be changed in Windmesh

    integer(ip), save            :: first_entry = 1_ip
    integer(ip)                  :: which_time, icoun, ncoun

    type(typ_valpr_ker), pointer :: prope_ker 
    type(lis_prope_ker), pointer :: listp_ker(:)

    if( IMASTER .or. kfl_prope == 0 ) return

    nullify(prope_ker)
    nullify(listp_ker)

    if (present(itask_xxx)) then
       itask=itask_xxx
       kfl_force=0
    else
       itask=-1000
       kfl_force=1
    endif

    if (kfl_reset == 1) then
       which_time=3
    else
       which_time=1
    endif

    allocate(listp_ker(mprop_ker))

    listp_ker(1) % prop => densi_ker
    listp_ker(2) % prop => visco_ker
    listp_ker(3) % prop => poros_ker
    listp_ker(4) % prop => condu_ker
    listp_ker(5) % prop => sphea_ker   
    listp_ker(6) % prop => dummy_ker   
    listp_ker(7) % prop => turmu_ker

    !----------------------------------------------------------------------
    !
    ! Constant properties: compute only at beginning
    !
    !----------------------------------------------------------------------

    if( itask == ITASK_INIUNK ) then
       do iprop = 1,mprop_ker
          prope_ker =>  listp_ker(iprop) % prop 
          if( prope_ker % kfl_exist == 1 ) then
             do imate = 1,nmate
                if( prope_ker % wlaws(imate) == 'CONST' ) &
                     prope_ker % value_const(imate) = prope_ker % rlaws(1,imate)
             end do
          end if
       end do
    end if

    !----------------------------------------------------------------------
    !
    ! Check if a property depends on the last module solved: LASTM_KER
    !
    !----------------------------------------------------------------------

    do iprop = 1,mprop_ker
       prope_ker => listp_ker(iprop)%prop 
       kfl_updpr(iprop) = 0    
       if( prope_ker % kfl_exist == 1 ) then 
          do imate = 1,nmate            
             if( prope_ker % wlaws(imate) /= 'CONST' ) then  
                ilaws = prope_ker % ilaws(imate)
                do iresp = 1,mresp_ker
                   if( prope_ker % llaws(ilaws) % lresp(iresp) == lastm_ker ) then
                      kfl_updpr(iprop) = 1
                   else if ( prope_ker % llaws(ilaws) % lresp(iresp) == -1 ) then
                      kfl_updpr(iprop) = 1
                   end if
                end do
             end if
          end do
          if ( ( kfl_force == 1 ) .or. ( itask == ITASK_INIUNK ) )  then
             kfl_updpr(iprop) = 1
          endif
       end if
    end do

    !----------------------------------------------------------------------
    !
    ! Variable properties
    !
    !----------------------------------------------------------------------

    do iprop = 1,mprop_ker

       if( kfl_updpr(iprop) == 1 ) then

          prope_ker => listp_ker(iprop)%prop 

          if( prope_ker % kfl_exist == 1 ) then

             do imate = 1,nmate

                if( itask == ITASK_INIUNK .and. prope_ker % value_default(imate) > 0.0_rp ) then

                   !----------------------------------------------------------------------
                   !
                   ! Run is starting (INIUNK): impose a default value if it exists (> 0)
                   !
                   !----------------------------------------------------------------------

                   ilaws = prope_ker % ilaws(imate)

                   if( prope_ker % llaws(ilaws) % where == 'IPOIN' ) then

                      do ipoin = 1,npoin
                         prope_ker % value_ipoin(ipoin) = prope_ker % value_default(imate)
                      end do
                      if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then
                         do ipoin = 1,npoin
                            prope_ker % grval_ipoin(:,ipoin) = 0.0_rp
                         end do
                      end if

                   else if( prope_ker % llaws(ilaws) % where == 'IELEM' ) then

                      do ielem = 1,nelem
                         prope_ker % value_ielem(ielem) % a = prope_ker % value_default(imate)
                      end do
                      do iboun = 1,nboun
                         prope_ker % value_iboun(iboun) % a = prope_ker % value_default(imate)
                      end do

                      if( prope_ker % llaws(ilaws) % kfl_gradi == 1 ) then                         
                         do ielem = 1,nelem
                            prope_ker % grval_ielem(ielem) % a = 0.0_rp
                         end do
                      end if

                   else if( prope_ker % wlaws(imate) == 'CONST' ) then

                      continue ! Nothing to do, it was already imposed
                      !prope_ker % value_const(imate) = prope_ker % value_default(imate)

                   end if

                else if( prope_ker % wlaws(imate) == 'BIFLU' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Bifluid model
                   !
                   !----------------------------------------------------------------------

                   prope_wat = prope_ker % rlaws(1,imate)
                   prope_air = prope_ker % rlaws(2,imate)
                   
                   do ielem = 1,nelem
                      pelty = ltype(ielem)
                      if( lmate(ielem) == imate .and. pelty > 0 ) then
                         pgaus = ngaus(pelty)      
                         pnode = lnnod(ielem)   
                         do inode = 1,pnode
                            ipoin = lnods(inode,ielem)
                            ellev(inode) = fleve(ipoin,which_time)
                            do idime = 1,ndime
                               elcod(idime,inode) = coord(idime,ipoin)
                            end do
                         end do
                         call elmca2(&
                              pnode,pgaus,0_ip,elmar(pelty)%weigp,elmar(pelty)%shape,&
                              elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,gpvol,gpsha,&
                              gpcar,gphes,ielem)                         
                         !call elmcar(& 
                         !     pnode,pgaus,0_ip,elmar(pelty) % weigp,elmar(pelty) % shape,&
                         !     elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpcar,&
                         !     dummr,ielem)
                         call ker_biflui(& 
                              1_ip,pgaus,pnode,gpsha,gpcar,ellev,prope_wat,prope_air, &
                              thicl,hleng,prope_ker % value_ielem(ielem) % a,         &
                              dummr)
                      end if
                   end do

                   do iboun = 1,nboun
                      pblty = abs(ltypb(iboun))
                      pnodb = nnode(pblty)
                      ielem = lboel(pnodb+1,iboun)
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaub = ngaus(pblty)
                            do inodb = 1,pnodb
                               ipoin = lnodb(inodb,iboun)
                               ellev(inodb) = fleve(ipoin,which_time)
                            end do
                            call ker_biflui(& 
                                 2_ip,pgaub,pnodb,elmar(pblty) % shape,gpcar,ellev,prope_wat,prope_air,&
                                 thicl,hleng,prope_ker % value_iboun(iboun) % a,dummr)
                         end if
                      end if
                   end do
                   prope_ker % kfl_nedsm = 1

                else if( prope_ker % wlaws(imate) == 'BIFL2' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Bifluid model - including gradients
                   !
                   !----------------------------------------------------------------------

                   prope_wat = prope_ker % rlaws(1,imate)
                   prope_air = prope_ker % rlaws(2,imate)
                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaus = ngaus(pelty)
                            pnode = lnnod(ielem)   
                            do inode = 1,pnode
                               ipoin = lnods(inode,ielem)
                               ellev(inode) = fleve(ipoin,which_time)
                               do idime = 1,ndime
                                  elcod(idime,inode) = coord(idime,ipoin)
                               end do
                            end do
                            call elmca2(&
                                 pnode,pgaus,0_ip,elmar(pelty)%weigp,elmar(pelty)%shape,&
                                 elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,gpvol,gpsha,&
                                 gpcar,gphes,ielem)                                                     
                            !call elmcar(&
                            !     pnode,pgaus,0_ip,elmar(pelty) % weigp,elmar(pelty) % shape,&
                            !     elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpcar,&
                            !     dummr,ielem)
                            call ker_biflu2(&
                                 1_ip,pgaus,pnode,elmar(pelty) % shape,gpcar,ellev,prope_wat,prope_air, &
                                 thicl,hleng,prope_ker % value_ielem(ielem) % a,         &
                                 prope_ker % grval_ielem(ielem) % a)
                         end if
                      end if
                   end do

                   do iboun = 1,nboun
                      pblty = abs(ltypb(iboun))
                      pnodb = nnode(pblty)
                      ielem = lboel(pnodb+1,iboun)
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaub = ngaus(pblty)
                            do inodb = 1,pnodb
                               ipoin = lnodb(inodb,iboun)
                               ellev(inodb) = fleve(ipoin,which_time)
                            end do
                            call ker_biflu2(& 
                                 2_ip,pgaub,pnodb,elmar(pblty) % shape,gpcar,ellev,prope_wat,prope_air,&
                                 thicl,hleng,prope_ker % value_iboun(iboun) % a,dummr)
                         end if
                      end if
                   end do

                   prope_ker % kfl_nedsm = 1

                else if( prope_ker % wlaws(imate) == 'SUTHE' ) then  

                   !----------------------------------------------------------------------
                   !
                   !                                        T0 + S
                   ! Sutherland's law: mu/mu_0 = (T/T0)^1.5 -------- 
                   !                                        T  + S
                   !
                   !----------------------------------------------------------------------

                   mu0   = prope_ker % rlaws(1,imate)
                   T0    = prope_ker % rlaws(2,imate)
                   S0    = prope_ker % rlaws(3,imate)
                   if (S0.lt. epsilon(1.0_rp)) S0  = 110.0_rp 
                   if (T0.lt. epsilon(1.0_rp)) T0  = 298.0_rp
                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)
                      T     = tempe(ipoin,which_time)                    
                      prope_ker % value_ipoin(ipoin) = mu0 * (T/T0)**1.5_rp * (T0+S0) / (T+S0)
                   end do
                else if( prope_ker % wlaws(imate) == 'CANOP' ) then  
                   !----------------------------------------------------------------------
                   !
                   ! Canopy model
                   !
                   !----------------------------------------------------------------------
                   cd= prope_ker % rlaws(2,imate)
                   lad = prope_ker % rlaws(3,imate)
                   zmaxf =prope_ker % rlaws(4,imate) ! zmaxf = zmax/heigh
                   if ( ipass(imate)==0 .and. kfl_canhe == 0) then   ! When a constant canopy height is used
                      do ielem = 1,nelem
                         if( lmate(ielem) == imate ) then
                            pelty = ltype(ielem)
                            if( pelty > 0 ) then
                               pnode = lnnod(ielem)
                               do inode = 1,pnode
                                  ipoin = lnods(inode,ielem)             
                                  canhe(ipoin) = prope_ker % rlaws(1,imate)
                               end do
                            end if
                         end if
                      end do
                   end if
                   
                   if ( ipass(imate)==0.and.zmaxf.gt.0.01_rp) then ! variable canopy density with height 
                      !
                      ! Obtain the integral for a height = 1   then we will just multiply by the real height
                      !
                      ncoun =1000
                      integ = 0.0_rp
                      heigh = 1.0_rp
                      zmaxi = zmaxf*heigh

                      do icoun = 1, ncoun -1
                         zz = dfloat(icoun)/dfloat(ncoun)*heigh
                         if (zz.lt.zmaxi) then
                            n = 6.0_rp
                         else
                            n = 0.5_rp
                         end if
                         integ = integ + (((heigh -zmaxi) /(heigh - zz))**n )*exp(n*(1.0_rp-(heigh -zmaxi) /(heigh - zz)))
                      end do

                      integ = integ  + 0.5_rp*(((heigh -zmaxi) /(heigh ))**6.0_rp )*exp(6.0_rp*(1.0_rp-(heigh -zmaxi) /(heigh )))

                      integ = integ  + 0.5_rp*(((heigh -zmaxi) /(1.0e-4))**0.5_rp )*exp(0.5_rp*(1.0d0-(heigh -zmaxi) /(1.0e-4)))


                      integ = integ * heigh / dfloat(ncoun)                     
                   end if
                   ipass(imate) = 1

                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaus = ngaus(pelty)
                            pnode = lnnod(ielem)
                            gpvel = 0.0_rp
                            gphog = 0.0_rp
                            gpcah = 0.0_rp
                            do inode = 1,pnode
                               ipoin = lnods(inode,ielem)  
                                
                               do idime = 1,ndime
                                  elvel(idime, inode) = veloc(idime, ipoin,1)                     
                                  do igaus =1, pgaus
                                     gpvel(idime, igaus) = gpvel(idime,igaus) + &
                                          elvel(idime, inode) * elmar(pelty) % shape(inode,igaus)
                                  end do
                               end do                               
                               do igaus =1, pgaus
                                  shaaa = elmar(pelty) % shape(inode,igaus)
                                  gpcah(igaus) = gpcah(igaus) + canhe(ipoin)* shaaa
                                  gphog(igaus) = gphog(igaus) + heiov(ipoin)* shaaa
                               end do
                            end do


                            do igaus =1, pgaus
                               heigh = gpcah(igaus)
                               zmaxi = zmaxf*heigh 
                               hoveg = gphog(igaus)  
                               if ((hoveg.lt.heigh).and.(heigh>h_can_critical)) then ! below forest height and over the critical heig
                                  auxva =0.0_rp
                                  do idime =1, ndime
                                     auxva = auxva + gpvel(idime, igaus)*gpvel(idime, igaus)
                                  end do
                                  if (zmaxf.le.0.01) then ! uniform canopy
                                     cdlad = cd*lad                            
                                  else
                                     !non uniform canopy distribution from LALIC and MIHAILOVIC, 
                                     !An Empirical Relation Describing Leaf-Area Density inside the Forest
                                     !http://journals.ametsoc.org/doi/pdf/10.1175/1520-0450%282004%29043%3C0641%3AAERDLD%3E2.0.CO%3B2
                                     lamax = lad / integ
                                     if (hoveg.gt.0.999*heigh ) then
                                        cdlad = 0.0_rp
                                     else if (hoveg.lt.zmaxi) then 
                                        cdlad =   cd*lamax * (((heigh -zmaxi) /(heigh -hoveg))**6.0 )*&
                                             exp(6.0*(1.0_rp-(heigh -zmaxi) /(heigh -hoveg)))
                                     else
                                        cdlad =   cd*lamax * (((heigh -zmaxi) /(heigh -hoveg))**0.5 )*&
                                             exp(0.5*(1.0_rp-(heigh -zmaxi) /(heigh -hoveg)))
                                     end if
                                  end if
                                  prope_ker % value_ielem(ielem) % a(igaus) = densi_ker % value_const(imate)&
                                       *cdlad* sqrt(auxva)
                               else  ! above forest height
                                  prope_ker % value_ielem(ielem) % a(igaus) = 0.0_rp
                               end if
                            end do
                         end if
                      end if   
                   end do
                else if( prope_ker % wlaws(imate) == 'GPSUT' ) then  

                   !----------------------------------------------------------------------
                   !
                   !                                                         T0 + S
                   ! Sutherland's law in Gauss points : mu/mu_0 = (T/T0)^1.5 -------- 
                   !                                                         T  + S
                   !
                   !----------------------------------------------------------------------

                   mu0   = prope_ker % rlaws(1,imate) ! reference value
                   T0    = prope_ker % rlaws(2,imate) ! referentemperature
                   ! Sutherland Temperature S0
                   S0    = prope_ker % rlaws(3,imate)
                   ! if not given, load default value (air)
                   if (S0.lt. epsilon(1.0_rp)) S0    = 110.0_rp 
            
                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaus = ngaus(pelty)
                            pnode = lnnod(ielem)
                            do inode = 1,pnode
                               ipoin = lnods(inode,ielem)
                               eltem(inode) = tempe(ipoin,which_time)
                               do idime = 1,ndime
                                  elcod(idime,inode) = coord(idime,ipoin)
                               end do
                            end do
                            call elmcar(&
                                 pnode,pgaus,0_ip,elmar(pelty) % weigp,elmar(pelty) % shape,&
                                 elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpcar,&
                                 dummr,ielem)
                            gp_temperature=0.0_rp
                            gp_grad_tem=0.0_rp
                            do igaus = 1,pgaus
                               do inode = 1,pnode
                                  ipoin = lnods(inode,ielem)
                                  gp_temperature(igaus) = gp_temperature(igaus) + eltem(inode) * elmar(pelty) % shape(inode,igaus)
                                  do idime = 1,ndime
                                     gp_grad_tem(idime,igaus) = gp_grad_tem(idime,igaus) + eltem(inode) * gpcar(idime,inode,igaus)
                                  end do
                               end do
                               if (gp_temperature(igaus) <= epsilon(1.0_rp) ) then
                                  ! For initialization we default to reference temperature
                                  gp_temperature(igaus) = T0
                                  gp_grad_tem(1:ndime,igaus) = 0.0_rp
                               endif
                               mu =  mu0 * (gp_temperature(igaus)/T0)**1.5_rp * (T0+S0) / (gp_temperature(igaus)+S0)
                               prope_ker % value_ielem(ielem) % a(igaus) = mu
                               do idime = 1,ndime
                                  prope_ker % grval_ielem(ielem) % a(idime,igaus) = gp_grad_tem(idime,igaus) * &
                                       (3.0_rp*S0 + gp_temperature(igaus)) / (2.0_rp * gp_temperature(igaus)* (gp_temperature(igaus)+S0))*mu                                    
                               enddo
                            end do
                         end if
                      end if
                   end do
                   prope_ker % kfl_nedsm = 1
                   
                   do iboun = 1,nboun
                      pblty = abs(ltypb(iboun))
                      pnodb = nnode(pblty)
                      ielem = lboel(pnodb+1,iboun)
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaub = ngaus(pblty)
                            do inodb = 1,pnodb
                               eltem(inodb) = tempe(lnodb(inodb,iboun),1)
                            end do
                            do igaub = 1,pgaub
                               gbtem = 0.0_rp
                               do inodb = 1,pnodb
                                  gbtem = gbtem + eltem(inodb) * elmar(pblty) % shape(inodb,igaub)
                               end do
                               if (gbtem <= epsilon(1.0_rp) ) then
                                  ! For initialization we default to reference temperature
                                  gbtem = 300.0_rp
                               endif
                               prope_ker % value_iboun(iboun) % a(igaub) =   mu0 * (gbtem/T0)**1.5_rp * (T0+S0) / (gbtem+S0)
                            end do
                         end if
                      end if
                   end do

                else if( prope_ker % wlaws(imate) == 'SMAGO' ) then  

                   !----------------------------------------------------------------------
                   !
                   !                                                        
                   !  SMAGORINSKY MODEL nu_t = (C_s * Vol)^2 * sqrt(2*Sij*Sij)  
                   !                                                         
                   !
                   !----------------------------------------------------------------------


                   const = prope_ker % rlaws(1,imate)

                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaus = ngaus(pelty)
                            pnode = lnnod(ielem)
                            do inode = 1,pnode
                               ipoin = lnods(inode,ielem)
                               do idime = 1,ndime
                                  elcod(idime,inode) = coord(idime,ipoin)
                                  elvel(idime,inode) = veloc(idime,ipoin,which_time)
                               end do
                            end do
                            call elmcar(&
                                 pnode,pgaus,0_ip,elmar(pelty) % weigp,elmar(pelty) % shape,&
                                 elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpcar,&
                                 dummr,ielem)

                            gpvel = 0.0_rp
                            gpgve = 0.0_rp

                            do igaus = 1,pgaus
                               do inode = 1,pnode
                                  do idime = 1,ndime
                                     gpvel(idime,igaus) = gpvel(idime,igaus) + elvel(idime,inode) * elmar(pelty) % shape(inode,igaus)
                                     do jdime = 1,ndime
                                        gpgve(jdime,idime,igaus) = gpgve(jdime,idime,igaus) + elvel(idime,inode) * gpcar(jdime,inode,igaus)
                                     end do
                                  end do
                               end do
                               !
                               ! Computing turbulent viscosity mu_t at gauss points 
                               ! according to Smagorinsky: gptur
                               !
                               call ker_turbul(1_ip,dummr,dummr,const,dummr,gpgve(:,:,igaus),gpvol,gptur,dummr)

                               prope_ker % value_ielem(ielem) % a(igaus) = gptur

                               do idime = 1,ndime
                                  prope_ker % grval_ielem(ielem) % a(idime,igaus) = 0.0_rp 
                               enddo
                            end do
                         end if
                      end if
                   end do

                   prope_ker % kfl_nedsm = 1
                   
                   do iboun = 1,nboun
                      pblty = abs(ltypb(iboun))
                      pnodb = nnode(pblty)
                      ielem = lboel(pnodb+1,iboun)
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaub = ngaus(pblty)
                            do inodb = 1,pnodb
                               do idime = 1,ndime
                                  elvel(idime,inodb) = veloc(idime,lnodb(inodb,iboun),which_time)
                               end do
                            end do
                            do igaub = 1,pgaub
                               gbvel = 0.0_rp
                               gbgve = 0.0_rp
                               do inodb = 1,pnodb
                                  do idime = 1,ndime
                                     gbvel(idime) = gbvel(idime) + elvel(idime,inodb) * elmar(pblty) % shape(inodb,igaub)
                                     do jdime = 1,ndime
                                        gbgve(jdime,idime) = gbgve(jdime,idime) + elvel(idime,inodb) * gpcar(jdime,inodb,igaub)
                                     end do
                                  end do
                               end do
                               !
                               ! Computing turbulent viscosity mu_t at gauss points 
                               ! according to Smagorinsky: gptur
                               !
                               call ker_turbul(1_ip,dummr,dummr,const,dummr,gbgve(:,:),gpvol,gptur,dummr)
            
                               prope_ker % value_iboun(iboun) % a(igaub) =  gptur 
            
                            end do
                         end if
                      end if
                   end do

                else if( prope_ker % wlaws(imate) == 'WALE ' ) then  

                   !----------------------------------------------------------------------
                   !
                   !  WALE  - Wall-adapting local eddy-viscosity - Nicoud-Ducros 99
                   !    
                   !----------------------------------------------------------------------


                   const = prope_ker % rlaws(1,imate)

                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaus = ngaus(pelty)
                            pnode = lnnod(ielem)
                            do inode = 1,pnode
                               ipoin = lnods(inode,ielem)
                               do idime = 1,ndime
                                  elcod(idime,inode) = coord(idime,ipoin)
                                  elvel(idime,inode) = veloc(idime,ipoin,which_time)
                               end do
                            end do
                            call elmcar(&
                                 pnode,pgaus,0_ip,elmar(pelty) % weigp,elmar(pelty) % shape,&
                                 elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpcar,&
                                 dummr,ielem)

                            gpvel = 0.0_rp
                            gpgve = 0.0_rp

                            do igaus = 1,pgaus
                               do inode = 1,pnode
                                  do idime = 1,ndime
                                     gpvel(idime,igaus) = gpvel(idime,igaus) + elvel(idime,inode) * elmar(pelty) % shape(inode,igaus)
                                     do jdime = 1,ndime
                                        gpgve(jdime,idime,igaus) = gpgve(jdime,idime,igaus) + elvel(idime,inode) * gpcar(jdime,inode,igaus)
                                     end do
                                  end do
                               end do
                               !
                               ! Computing turbulent viscosity mu_t at gauss points 
                               ! according to WALE: gptur
                               !
                               call ker_turbul(3_ip,dummr,dummr,const,dummr,gpgve(:,:,igaus),gpvol,gptur,dummr)

                               prope_ker % value_ielem(ielem) % a(igaus) = gptur

                               do idime = 1,ndime
                                  prope_ker % grval_ielem(ielem) % a(idime,igaus) = 0.0_rp 
                               enddo
                            end do
                         end if
                      end if
                   end do

                   prope_ker % kfl_nedsm = 1
                   
                   do iboun = 1,nboun
                      pblty = abs(ltypb(iboun))
                      pnodb = nnode(pblty)
                      ielem = lboel(pnodb+1,iboun)
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaub = ngaus(pblty)
                            do inodb = 1,pnodb
                               do idime = 1,ndime
                                  elvel(idime,inodb) = veloc(idime,lnodb(inodb,iboun),which_time)
                               end do
                            end do
                            do igaub = 1,pgaub
                               gbvel = 0.0_rp
                               gbgve = 0.0_rp
                               do inodb = 1,pnodb
                                  do idime = 1,ndime
                                     gbvel(idime) = gbvel(idime) + elvel(idime,inodb) * elmar(pblty) % shape(inodb,igaub)
                                     do jdime = 1,ndime
                                        gbgve(jdime,idime) = gbgve(jdime,idime) + elvel(idime,inodb) * gpcar(jdime,inodb,igaub)
                                     end do
                                  end do
                               end do
                               !
                               ! Computing turbulent viscosity mu_t at gauss points 
                               ! according to Smagorinsky: gptur
                               !
                               call ker_turbul(1_ip,dummr,dummr,const,dummr,gbgve(:,:),gpvol,gptur,dummr)
            
                               prope_ker % value_iboun(iboun) % a(igaub) =  gptur 
            
                            end do
                         end if
                      end if
                   end do


                else if( prope_ker % wlaws(imate) == 'LOWMA' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Low Mach approximation, density = p(th) / R T
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)    

                      if (tempe(ipoin,which_time) /= 0.0_rp ) then
                         prope_ker % value_ipoin(ipoin) = prthe(which_time) /(gasco * tempe(ipoin,which_time)) 
                      else 
                         prope_ker % value_ipoin(ipoin) = 0.0
                      endif
                   end do

                else if( prope_ker % wlaws(imate) == 'LOWMG' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Low Mach approximation, density = p(th) / R T IN GAUSS POINTS
                   !
                   !----------------------------------------------------------------------
                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaus = ngaus(pelty)
                            pnode = lnnod(ielem)
                            if (associated(tesgs)) then
                               do igaus = 1,pgaus 
                                  gptem = 0.0_rp
                                  do inode = 1,pnode
                                     ipoin = lnods(inode,ielem)
                                     gptem = gptem + tempe(ipoin,which_time) * elmar(pelty) % shape(inode,igaus)                                
                                  end do
                                  gptem = gptem+ tesgs(ielem)%a(1,igaus,1)
                                  if (gptem <= epsilon(1.0_rp) ) then
                                     ! For initialization we default to reference temperature
                                     gptem = 300.0                             
                                  endif
                                  prope_ker % value_ielem(ielem) % a(igaus) = prthe(which_time)/gasco/gptem
                               end do
                            else
                               do igaus = 1,pgaus 
                                  gptem = 0.0_rp
                                  do inode = 1,pnode
                                     ipoin = lnods(inode,ielem)
                                     gptem = gptem + tempe(ipoin,which_time) * elmar(pelty) % shape(inode,igaus)                                
                                  end do
                                  if (gptem <= epsilon(1.0_rp) ) then
                                     ! For initialization we default to reference temperature
                                     gptem = 300.0                             
                                  endif
                                  prope_ker % value_ielem(ielem) % a(igaus) = prthe(which_time)/gasco/gptem
                               end do
                            end if

                         endif
                      end if
                   end do

                   do iboun = 1,nboun
                      pblty = abs(ltypb(iboun))
                      pnodb = nnode(pblty)
                      ielem = lboel(pnodb+1,iboun)
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaub = ngaus(pblty)
                            do inodb = 1,pnodb
                               eltem(inodb) = tempe(lnodb(inodb,iboun),1)
                            end do
                            do igaub = 1,pgaub
                               gbtem = 0.0_rp
                               do inodb = 1,pnodb
                                  gbtem = gbtem + eltem(inodb) * elmar(pblty) % shape(inodb,igaub)
                               end do
                               if (gbtem <= epsilon(1.0_rp) ) then
                                  ! For initialization we default to reference temperature
                                  gbtem = 300.0_rp
                               endif
                               prope_ker % value_iboun(iboun) % a(igaub) =  prthe(1)/gasco/gbtem
                            end do
                         end if
                      end if
                   end do

                else if( prope_ker % wlaws(imate) == 'KLOWM' .or. prope_ker % wlaws(imate) == 'TLOWM' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Low Mach approximation, density = p(th) W / R T  where W is mean molecular density
                   !
                   ! Low Mach approximation for the syncronized CFI combustion model, 
                   ! density is only updated in temper
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)    
                      if (tempe(ipoin,which_time) > 0.0_rp ) then
                         prope_ker % value_ipoin(ipoin) = prthe(which_time) * wmean(ipoin,which_time) /(gasco * tempe(ipoin,which_time)) 
                      else 
                         prope_ker % value_ipoin(ipoin) = 0.0_rp
                      endif
                   end do

                else if( prope_ker % wlaws(imate) == 'GKLOW' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Low Mach approximation, density = p(th) W / R T at gauss_points where W is mean molecular density
                   !                         d(density)/dx = p(th) dW/dx / (R T) - p(th) W R/ (R T)^2
                   !
                   !----------------------------------------------------------------------
                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaus = ngaus(pelty)
                            pnode = lnnod(ielem)
                            do inode = 1,pnode
                               ipoin = lnods(inode,ielem)
                               do idime = 1,ndime
                                  elcod(idime,inode) = coord(idime,ipoin)
                               end do
                            end do
                            call elmcar(&
                                 pnode,pgaus,0_ip,elmar(pelty) % weigp,elmar(pelty) % shape,&
                                 elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpcar,&
                                 dummr,ielem)

                            gp_grad_tem = 0.0_rp
                            gp_grad_wmean = 0.0_rp
   
                            do igaus = 1,pgaus 
      
                              gptem = 0.0_rp
                              gpwmean = 0.0_rp
                              do inode = 1,pnode
                                  ipoin = lnods(inode,ielem)
                                  gptem = gptem + tempe(ipoin,which_time) * elmar(pelty) % shape(inode,igaus)
                                  if (kfl_coupl(ID_TEMPER,ID_CHEMIC) /= 0 ) then
                                    gpwmean = gpwmean + wmean(ipoin,which_time) * elmar(pelty) % shape(inode,igaus)
                                  else
                                    gpwmean = gpwmean + 1.0_rp * elmar(pelty) % shape(inode,igaus)
                                  end if
                                  do idime = 1,ndime
                                    gp_grad_tem(idime,igaus)   = gp_grad_tem(idime,igaus) + tempe(ipoin,which_time) * gpcar(idime,inode,igaus)
                                    if (kfl_coupl(ID_TEMPER,ID_CHEMIC) /= 0 ) then
                                      gp_grad_wmean(idime,igaus) = gp_grad_wmean(idime,igaus) + wmean(ipoin,which_time) * gpcar(idime,inode,igaus)
                                    else
                                      gp_grad_wmean(idime,igaus) = 0.0_rp
                                    end if
                                  end do
                              end do

                              if (associated(tesgs)) then 
                                 gptem = gptem + tesgs(ielem)%a(1,igaus,1)
                              end if
                              if (gptem <= epsilon(1.0_rp) ) then
                              ! For initialization we default to reference temperature
                                gptem = 300.0                             
                              end if
                              rho = prthe(which_time)*gpwmean/(gasco*gptem)
                              prope_ker % value_ielem(ielem) % a(igaus) = rho
                              do idime = 1,ndime
                                 prope_ker % grval_ielem(ielem) % a(idime,igaus) = prthe(which_time) * gp_grad_wmean(idime,igaus)/(gasco*gptem) &
                                                                                 - prthe(which_time)*gpwmean*gp_grad_tem(idime,igaus)/(gasco*gptem**2)
                              end do
     
                              ! derivative of rho (at gauss points) respect to nodal tempe
                              do inode = 1,pnode
                                prope_ker % drval_ielem(ielem) % a(inode,igaus) = -elmar(pelty) % shape(inode,igaus) * rho/gptem
                              end do 
      
                              ! derivative of d(rho)/dx (at gauss points) respect to nodal tempe
                              do inode = 1,pnode
                                do idime = 1, ndime
                                  prope_ker % gdval_ielem(ielem) % a(idime, inode,igaus) = &
                                              - prthe(which_time) * gp_grad_wmean(idime,igaus)/(gasco*gptem**2)*elmar(pelty) % shape(inode,igaus) &
                                              - rho/gptem * gpcar(idime,inode,igaus) &
                                              + 2*rho/(gptem**2)*gp_grad_tem(idime,igaus) * elmar(pelty) % shape(inode,igaus)
                                end do
                              end do 
     
                            end do !igaus    
                         end if
                      end if
                   end do
                   prope_ker % kfl_nedsm = 1

                   do iboun = 1,nboun
                      pblty = abs(ltypb(iboun))
                      pnodb = nnode(pblty)
                      ielem = lboel(pnodb+1,iboun)
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaub = ngaus(pblty)
                            do inodb = 1,pnodb
                               eltem(inodb) = tempe(lnodb(inodb,iboun),which_time)
                               if (kfl_coupl(ID_TEMPER,ID_CHEMIC) /= 0 ) then
                                  elwmean(inodb) = wmean(lnodb(inodb,iboun),which_time)
                               else
                                  elwmean(inodb) = 1.0_rp
                               end if
                            end do
                            do igaub = 1,pgaub
                               gbtem = 0.0_rp
                               gbwme = 0.0_rp
                               do inodb = 1,pnodb
                                  gbtem = gbtem + eltem(inodb) * elmar(pblty) % shape(inodb,igaub)
                                  gbwme = gbwme + elwmean(inodb) * elmar(pblty) % shape(inodb,igaub)
                               end do
                               if (gbtem <= epsilon(1.0_rp) ) then
                                  ! For initialization we default to reference
                                  ! temperature
                                  gbtem = 300.0_rp
                               endif
                               prope_ker % value_iboun(iboun) % a(igaub) = prthe(1)*gbwme/(gasco*gbtem)
                            end do
                         end if
                      end if
                   end do
!                   write(*,*) 'UPDATE GKLOW' 

                else if( prope_ker % wlaws(imate) == 'MIXTU' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Mixture of constant density fluids, 1/density = Sum_k  Y_k / rho_k where Y_k is the mass fraction
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)   
                      prope_ker % value_ipoin(ipoin) = 0.0_rp
                      do ispec = 1,nspec
                         prope_ker % value_ipoin(ipoin) = prope_ker % value_ipoin(ipoin) + conce(ipoin,ispec,which_time)/speci(ispec)%densi(1)
                      enddo
                      if ( prope_ker % value_ipoin(ipoin) .gt. 0.0_rp) then
                         prope_ker % value_ipoin(ipoin) = 1.0_rp /  prope_ker % value_ipoin(ipoin)
                      else
                         call runend("KERMOD: Density evaluated to <= 0, check species properties")
                      endif
                   end do

                else if( prope_ker % wlaws(imate) == 'BIPHA' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Mixture of constant density fluids in two phases
                   ! 1/density = Sum_k  Y_k / rho_k where Y_k is the mass fraction
                   ! If level set is negative, density corresponds to first phase, positive to second phase
                   !
                   !----------------------------------------------------------------------

                   if( thicl > 0.0_rp ) then
                      eps = thicl
                   else
                      call runend('Really need hleng in Ker_updpro.')
                      !eps = -thicl * hleng(1)
                   end if
                   ooeps = 1.0_rp/eps
                   oovpi = 1.0_rp/pi                 

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)    
                      !
                      ! Compute density of phases
                      !
                      densi_phase(1) = 0.0_rp
                      densi_phase(2) = 0.0_rp
                      do ispec = 1,nspec
                         densi_phase(1) = densi_phase(1) + conce(ipoin,ispec,1)/speci(ispec)%densi(1)
                         densi_phase(2) = densi_phase(2) + conce(ipoin,ispec,1)/speci(ispec)%densi(2)
                      enddo
                      if ( densi_phase(1) .gt. 0.0_rp) then
                         densi_phase(1) = 1.0_rp /  densi_phase(1)
                      else 
                         call runend("KERMOD: Density in phase 1 evaluated to <= 0, check species properties")
                      endif
                      if ( densi_phase(2) .gt. 0.0_rp) then
                         densi_phase(2) = 1.0_rp /  densi_phase(2)
                      else
                         call runend("KERMOD: Density in phase 2 evaluated to <= 0, check species properties")
                      endif
                      ! Compute density at point
                      phi=fleve(ipoin,which_time)
                      if( phi < -eps ) then
                         ! First phase
                         prope_ker % value_ipoin(ipoin)=densi_phase(1)
                      else if( phi > eps ) then
                         ! Second phase
                         prope_ker % value_ipoin(ipoin)=densi_phase(2)
                      else 
                         f = 0.5_rp*(1.0_rp+phi*ooeps+sin(pi*phi*ooeps)*oovpi)
                         prope_ker % value_ipoin(ipoin)=densi_phase(1) + (densi_phase(2)-densi_phase(1))*f
                      endif

                   end do

                else if( prope_ker % wlaws(imate) == 'TBIPH' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Mixture of temperature dependent density fluids in two phases
                   ! 1/density = Sum_k  Y_k / rho_k where Y_k is the mass fraction
                   ! If level set is negative, density corresponds to first phase, positive to second phase
                   ! Temperature dependence is assumed linear, rho = rho_a * T + rho_0
                   !
                   !----------------------------------------------------------------------

                   if( thicl > 0.0_rp ) then
                      eps = thicl
                   else
                      call runend('Really need hleng in Ker_updpro.')
                      !eps = -thicl * hleng(1)
                   end if
                   ooeps = 1.0_rp/eps
                   oovpi = 1.0_rp/pi                 

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)    
                      !
                      ! Compute density of phases
                      !
                      densi_phase(1) = 0.0_rp
                      densi_phase(2) = 0.0_rp
                      do ispec = 1,nspec
                         densi_phase(1) = densi_phase(1) + conce(ipoin,ispec,which_time)/ &
                              ( speci(ispec)%densi(1)*tempe(ipoin,which_time)+speci(ispec)%densi(2) )
                         densi_phase(2) = densi_phase(2) + conce(ipoin,ispec,which_time)/ & 
                              ( speci(ispec)%densi(3)*tempe(ipoin,which_time)+speci(ispec)%densi(4) )
                      enddo
                      if ( densi_phase(1) .gt. 0.0_rp) then
                         densi_phase(1) = 1.0_rp /  densi_phase(1)
                      else
                         call runend("KERMOD: Density in phase 1 evaluated to <= 0, check species properties")
                      endif
                      if ( densi_phase(2) .gt. 0.0_rp) then
                         densi_phase(2) = 1.0_rp /  densi_phase(2)
                      else
                         call runend("KERMOD: Density in phase 2 evaluated to <= 0, check species properties")
                      endif
                      ! Compute density at point
                      phi=fleve(ipoin,which_time)
                      if( phi < -eps ) then
                         ! First phase
                         prope_ker % value_ipoin(ipoin)=densi_phase(1)
                      else if( phi > eps ) then
                         ! Second phase
                         prope_ker % value_ipoin(ipoin)=densi_phase(2)
                      else 
                         f = 0.5_rp*(1.0_rp+phi*ooeps+sin(pi*phi*ooeps)*oovpi)
                         prope_ker % value_ipoin(ipoin)=densi_phase(1) + (densi_phase(2)-densi_phase(1))*f
                      endif

                   end do

                else if( prope_ker % wlaws(imate) == 'DNAST' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Density is controlled by NASTAL module
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)    
                      prope_ker % value_ipoin(ipoin) = densi(ipoin,which_time)
                   end do

                else if( prope_ker % wlaws(imate) == 'MUTAB' .or. prope_ker % wlaws(imate) == 'TMUTA') then

                   !----------------------------------------------------------------------
                   !
                   ! Viscosity is read from table in CHEMIC module (CFI combustion model)
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)
                      prope_ker % value_ipoin(ipoin) = visck(ipoin,1)
                   end do

                else if( prope_ker % wlaws(imate) == 'KTABL' .or. prope_ker % wlaws(imate) == 'TKTAB') then

                   !----------------------------------------------------------------------
                   !
                   ! Heat conductivity is read from table in CHEMIC module (CFI combustion model)
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)
                      prope_ker % value_ipoin(ipoin) = condk(ipoin,1)
                   end do

                else if( prope_ker % wlaws(imate) == 'KQUAR') then

                   !----------------------------------------------------------------------
                   !
                   ! Heat conductivity for quartz glass is computed from polynomial function 
                   ! based on temperature
                   ! Source: O. Sergeev, A. Shashkov, A. Umanskii, Thermophysical properties of 
                   !         quartz glass, Journal of engineering physics 43 (1982) 1375–1383
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)
                      T     = tempe(ipoin,which_time)
                      prope_ker % value_ipoin(ipoin) = 2.14749_rp - 298.76_rp * T**(-1) + 20720.0_rp * T**(-2) - 540000.0_rp * T**(-3)
                   end do

                else if( prope_ker % wlaws(imate) == 'CPQUA') then

                   !----------------------------------------------------------------------
                   !
                   ! Specific heat for quartz glass is computed from polynomial function 
                   ! based on temperature
                   ! Source: O. Sergeev, A. Shashkov, A. Umanskii, Thermophysical properties of 
                   !         quartz glass, Journal of engineering physics 43 (1982) 1375–1383
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)
                      T     = tempe(ipoin,which_time)
                      prope_ker % value_ipoin(ipoin) = 931.3_rp + 0.256_rp * T - 24000000.0_rp * T**(-2)
                   end do

                else if( prope_ker % wlaws(imate) == 'KSTAI') then

                   !----------------------------------------------------------------------
                   !
                   ! Heat conductivity for stainless steel is computed from polynomial 
                   ! function based on temperature
                   ! Source: EN1993 1.2 Annex C
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)
                      T     = tempe(ipoin,which_time) - 273.15_rp
                      prope_ker % value_ipoin(ipoin) = 14.6_rp + 0.0127_rp * T
                   end do

                else if( prope_ker % wlaws(imate) == 'CPSTA') then

                   !----------------------------------------------------------------------
                   !
                   ! Specific heat for stainless steel is computed from polynomial 
                   ! function based on temperature
                   ! Source: EN1993 1.2 Annex C
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)
                      T     = tempe(ipoin,which_time) - 273.15_rp
                      prope_ker % value_ipoin(ipoin) = 450.0_rp + 0.28_rp * T - 0.000291_rp * T**2 + 0.000000134 * T**3
                   end do

                else if( prope_ker % wlaws(imate) == 'CPTAB' .or. prope_ker % wlaws(imate) == 'TCPTA') then

                   !----------------------------------------------------------------------
                   !
                   ! Specific heat is read from table in CHEMIC module (CFI combustion model)
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)
                      prope_ker % value_ipoin(ipoin) = sphek(ipoin,1)
                   end do

                else if( prope_ker % wlaws(imate) == 'MUMIX' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Viscosity given by a mixture of species (individual viscosities are computed
                   ! inside CHEMIC, here only the mixture is done)
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin) 
                      !
                      ! Mixture coefficients see Combustion Physics, Chung K. Law, page 155
                      !
                      ! phi(i,j) = 1/sqrt(8) 1/sqrt(1 + Wi/Wj) [1+ sqtr(mui/muj) (Wj/Wi)^1/4 ]^2
                      ! phi(i,i) = 1.0
                      !
                      ! Took out wmean(ipoin,1) from all terms because it simplifies in the final expression
                      !
                      do ispec = 1,nspec
                         sumphi(ispec) = 0.0_rp
                         do jspec = 1,nspec
                            if (jspec /= ispec) then
                               if (visck(ipoin,jspec) /= 0.0_rp) then
                                  phiij = sqrt(0.125_rp/(1.0_rp+speci(ispec)%weigh/speci(jspec)%weigh)) * &
                                       (1.0_rp+sqrt( visck(ipoin,ispec)&
                                       / visck(ipoin,jspec))*(speci(jspec)%weigh/speci(ispec)%weigh)**0.25)**2
                               else
                                  phiij =  sqrt(0.125_rp/(1.0_rp+speci(ispec)%weigh/speci(jspec)%weigh))
                               endif
                               sumphi(ispec) = sumphi(ispec) + phiij * conce(ipoin,jspec,which_time) / speci(jspec)%weigh    
                            endif
                         enddo
                      enddo
                      !
                      prope_ker % value_ipoin(ipoin) = 0.0_rp
                      do ispec = 1,nspec
                         if (conce(ipoin,ispec,which_time) /= 0.0_rp) then
                            prope_ker % value_ipoin(ipoin) = prope_ker % value_ipoin(ipoin) + visck(ipoin,ispec) * & 
                                 (conce(ipoin,ispec,which_time) / speci(ispec)%weigh) & 
                                 / (conce(ipoin,ispec,which_time) / speci(ispec)%weigh + sumphi(ispec))
                         endif

                      enddo
                   end do

                else if( prope_ker % wlaws(imate) == 'KMIXT' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Heat conductivity given by a mixture of species (individual conductivities are computed
                   ! inside CHEMIC, here only the mixture is done)
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin) 
                      !
                      ! Mixture coefficients see Combustion Physics, Chung K. Law, page 155
                      ! Notice that it's ok that viscosities are used...
                      !
                      ! phi(i,j) = 1/sqrt(8) 1/sqrt(1 + Wi/Wj) [1+ sqtr(mui/muj) (Wj/Wi)^1/4 ]^2
                      ! phi(i,i) = 1.0
                      !
                      ! Took out wmean(ipoin,1) from all terms because it simplifies in the final expression
                      !
                      do ispec = 1,nspec
                         sumphi(ispec) = 0.0_rp
                         do jspec = 1,nspec
                            if (jspec /= ispec) then
                               if (visck(ipoin,jspec) /= 0.0_rp) then
                                  phiij = sqrt(0.125_rp/(1.0_rp+speci(ispec)%weigh/speci(jspec)%weigh)) * &
                                       (1.0_rp+sqrt( visck(ipoin,ispec)&
                                       / visck(ipoin,jspec))*(speci(jspec)%weigh/speci(ispec)%weigh)**0.25_rp)**2
                               else
                                  phiij =  sqrt(0.125_rp/(1.0_rp+speci(ispec)%weigh/speci(jspec)%weigh))
                               endif
                               sumphi(ispec) = sumphi(ispec) + phiij * conce(ipoin,jspec,which_time) / speci(jspec)%weigh    
                            endif
                         enddo
                      enddo
                      !
                      prope_ker % value_ipoin(ipoin) = 0.0_rp
                      do ispec = 1,nspec
                         if (conce(ipoin,ispec,which_time) /= 0.0_rp) then
                            prope_ker % value_ipoin(ipoin) = prope_ker % value_ipoin(ipoin) + condk(ipoin,ispec) * & 
                                 (conce(ipoin,ispec,which_time) / speci(ispec)%weigh) &
                                 / (conce(ipoin,ispec,which_time) / speci(ispec)%weigh + 1.065_rp * sumphi(ispec))
                         endif
                      enddo
                   end do

                else if( prope_ker % wlaws(imate) == 'CPMIX' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! Specific heat of a mixture of species (individual speheas are computed
                   ! inside CHEMIC, here only the mixture is done)
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin) 
                      !
                      prope_ker % value_ipoin(ipoin) = 0.0_rp
                      do ispec = 1,nspec
                         prope_ker % value_ipoin(ipoin) = prope_ker % value_ipoin(ipoin) + sphek(ipoin,ispec) * conce(ipoin,ispec,which_time)
                      enddo
                   end do

                else if( prope_ker % wlaws(imate) == 'TEST1' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! TEST1
                   !
                   !----------------------------------------------------------------------

                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = ltype(ielem)
                         if( pelty > 0 ) then
                            pgaus = ngaus(pelty)
                            pnode = lnnod(ielem)
                            gpcor = 0.0_rp
                            do igaus = 1,pgaus
                               do inode = 1,pnode
                                  ipoin = lnods(inode,ielem)
                                  do idime = 1,ndime
                                     gpcor(idime,igaus) = gpcor(idime,igaus) + coord(idime,ipoin) * elmar(pelty) % shape(inode,igaus)
                                  end do
                               end do
                               prope_ker % value_ielem(ielem) % a(igaus)   =  2.0_rp * gpcor(1,igaus) - 3.0_rp * gpcor(2,igaus)
                               prope_ker % grval_ielem(ielem) % a(1,igaus) =  2.0_rp 
                               prope_ker % grval_ielem(ielem) % a(2,igaus) = -3.0_rp 
                            end do
                         end if
                      end if
                   end do
                   prope_ker % kfl_nedsm = 1

                else if( prope_ker % wlaws(imate) == 'TEST2' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! TEST2
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)    
                      prope_ker % value_ipoin(ipoin) = 2.0_rp * coord(1,ipoin) - 3.0_rp * coord(2,ipoin)
                   end do

                else if( prope_ker % wlaws(imate) == 'TEST3' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! TEST3
                   !
                   !----------------------------------------------------------------------

                   do kpoin = 1,nmatn(imate)
                      ipoin = lmatn(imate) % l(kpoin)    
                      prope_ker % value_ipoin(ipoin)   =  2.0_rp * coord(1,ipoin) - 3.0_rp * coord(2,ipoin)
                      prope_ker % grval_ipoin(1,ipoin) =  2.0_rp
                      prope_ker % grval_ipoin(2,ipoin) = -3.0_rp
                   end do

                else if( prope_ker % wlaws(imate) == 'TEST4' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! TEST4
                   !
                   !----------------------------------------------------------------------

                   if( ndime == 2 ) then
                      do kpoin = 1,nmatn(imate)
                         ipoin = lmatn(imate) % l(kpoin)    
                         prope_ker % value_ipoin(ipoin)   = 1.0_rp + 2.0_rp * coord(1,ipoin) + 3.0_rp * coord(2,ipoin)
                         prope_ker % grval_ipoin(1,ipoin) = 2.0_rp
                         prope_ker % grval_ipoin(2,ipoin) = 3.0_rp
                      end do
                   else
                      do kpoin = 1,nmatn(imate)
                         ipoin = lmatn(imate) % l(kpoin)   
                         prope_ker % value_ipoin(ipoin)   = & 
                              1.0_rp + 2.0_rp * coord(1,ipoin) + 3.0_rp * coord(2,ipoin) + 4.0_rp * coord(3,ipoin)
                         prope_ker % grval_ipoin(1,ipoin) = 2.0_rp
                         prope_ker % grval_ipoin(2,ipoin) = 3.0_rp
                         prope_ker % grval_ipoin(3,ipoin) = 4.0_rp
                      end do
                   end if

                else if( prope_ker % wlaws(imate) == 'ABL  ' ) then  

                   !----------------------------------------------------------------------
                   !
                   ! ABL: mu = rho/Cmu*kap*(z+z0)*U_*
                   !
                   !----------------------------------------------------------------------

                   prope_ker % kfl_nedsm = 1
                   do ielem = 1,nelem
                      if( lmate(ielem) == imate ) then
                         pelty = abs(ltype(ielem))
                         pgaus = ngaus(pelty)
                         pnode = lnnod(ielem)
                         gpcor = 0.0_rp
                         do igaus = 1,pgaus
                            do inode = 1,pnode
                               ipoin = lnods(inode,ielem)
                               do idime = 1,ndime
                                  gpcor(idime,igaus) = gpcor(idime,igaus) + coord(idime,ipoin) * elmar(pelty) % shape(inode,igaus)
                               end do
                            end do
                            if( kfl_rough > 0 ) then
                               z0 = 0.0_rp
                               do inode = 1,pnode
                                  ipoin = lnods(inode,ielem)
                                  z0    = z0 + rough(ipoin) * elmar(pelty) % shape(inode,igaus)
                               end do
                            else
                               z0 = rough_dom
                            end if
                            rho   = 1.0_rp
                            ustar = 0.23_rp
                            kap   = 0.41_rp
                            prope_ker % value_ielem(ielem) % a(      igaus) =  rho*ustar*kap*(gpcor(ndime,igaus)+z0+delta_dom)
                            prope_ker % grval_ielem(ielem) % a(    1,igaus) =  0.0_rp
                            prope_ker % grval_ielem(ielem) % a(    2,igaus) =  0.0_rp
                            prope_ker % grval_ielem(ielem) % a(ndime,igaus) =  rho*ustar*kap
                         end do
                      end if
                   end do

                end if
             end do

          end if

       end if

    end do

    deallocate(listp_ker)

  end subroutine ker_updpro


end module mod_ker_proper
