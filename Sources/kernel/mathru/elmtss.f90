subroutine elmtss(&
     pgaus,chale,gpdif,gpden,gpvel,gprea,dtcri)
  !-----------------------------------------------------------------------
  !****f* mathru/elmtss
  ! NAME 
  !    elmtss
  ! DESCRIPTION
  !    This routine computes the element time step
  ! USED BY
  !    elmadr
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime
  use def_master, only       :  kfl_advec_adr,kfl_diffu_adr,&
       &                        kfl_react_adr,kfl_taust_adr,&
       &                        staco_adr
  implicit none
  integer(ip), intent(in)    :: pgaus
  real(rp),    intent(in)    :: chale(2),gpdif(pgaus),gpden(pgaus)
  real(rp),    intent(in)    :: gpvel(ndime,pgaus),gprea(pgaus)
  real(rp),    intent(out)   :: dtcri
  real(rp)                   :: avdif,avden,avrea,avadv,avvel(3)
  real(rp)                   :: rgaus,gpvno
  integer(ip)                :: igaus,idime

  avdif = 0.0_rp
  avden = 0.0_rp
  avrea = 0.0_rp
  avadv = 0.0_rp
  rgaus = 1.0_rp/real(pgaus)
  !
  ! Density
  !
  do igaus=1,pgaus
     avden = avden + gpden(igaus)
  end do
  avden = avden*rgaus  
  !
  ! AVADV: Advection
  !
  if(kfl_advec_adr/=0) then
     avvel = 0.0_rp
     do igaus=1,pgaus
        do idime=1,ndime
           avvel(idime) = avvel(idime) + gpvel(idime,igaus)
        end do
     end do
     do idime=1,ndime
        avvel(idime) = avvel(idime)*rgaus
        gpvno        = gpvno+avvel(idime)*avvel(idime)
     end do
     avadv = avden*sqrt(gpvno)
  end if
  !
  ! AVDIF: Diffusion
  !
  if(kfl_diffu_adr/=0) then
     do igaus=1,pgaus
        avdif = avdif + gpdif(igaus)
     end do
     avdif = avdif*rgaus
  end if
  !
  ! AVREA: Reaction
  !
  if(kfl_react_adr/=0) then
     do igaus=1,pgaus
        avrea = avrea + gprea(igaus)
     end do
     avrea = avrea*rgaus
  end if

  call tauadr(&
       kfl_taust_adr,staco_adr,avadv,avdif,avrea,&
       chale(1),chale(2),dtcri)

  dtcri = avden*dtcri

end subroutine elmtss
