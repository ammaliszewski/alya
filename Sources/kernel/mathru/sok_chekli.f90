subroutine sok_chekli
 
  implicit none
  character(len=8) c_date
  character(len=10) c_time
  character(len=10) c_zone
  integer ivalues(8)

  integer 	     :: lun_licen_sok,lasty,lastm,lastd,ltoda,llast,lrang,bidon(3)

  !Find the today's date
  call date_and_time(date=c_date, &  ! character(len=8) ccyymmdd
                      time=c_time, &  ! character(len=10) hhmmss.sss
                      zone=c_zone, &  ! character(len=10) +/-hhmm (time zone)
                      values=ivalues) ! integer ivalues(8) all of the above 


  !read the license file
  lun_licen_sok=7777
  open(lun_licen_sok,file='/home/howen/svn/Alya2185/Executables/unix/ll.txt')

  read(lun_licen_sok,*) bidon(1)  
  read(lun_licen_sok,*) lasty  !last check up for the key
  read(lun_licen_sok,*) bidon(2)  
  read(lun_licen_sok,*) lastm  !last check up for the key
  read(lun_licen_sok,*) lastd  !last check up for the key
  read(lun_licen_sok,*) bidon(3)  

  !correct to have the uncripted date
  lasty=lasty+562
  lastm=lastm+837
  lastd=lastd-1458

  !convert in day 
  ltoda=(ivalues(1)*365)+(ivalues(2)*30)+ivalues(3)
  llast=(lasty*365)+(lastm*30)+lastd

! print*,'ivalues',ivalues
! print*,'lasty,lastm,lastd',lasty,lastm,lastd

  !Check when the license have been checked 

  lrang = ltoda-llast

  !if the check has been made before the creation of the code or at an incoherente date
  if(llast<733065 .or. lastm<1 .or. lastm>12 .or. lastd<1 .or. lastd>31) then 
     write(6,*) '++| '     
     write(6,*) '++|     SOLVEK LICENSE MANAGER'
     write(6,*) '++|     Erroneous informations find in the licence file'
     write(6,*) '++|     Stop the program'
     write(6,*) '++|     '
     stop

  end if


  !if the check has been made in the futur  
  if(lrang<0) then 
     write(6,*) '++| '     
     write(6,*) '++|     SOLVEK LICENSE MANAGER'
     write(6,*) '++|     Erroneous informations find in the licence file'
     write(6,*) '++|     Stop the program'
     write(6,*) '++|     '
     stop

  end if

  !ask for the pw after 60 days
  if (lrang>90) then  

     write(6,*) '++| '     
     write(6,*) '++|     SOLVEK LICENSE MANAGER'
     write(6,*) '++|     The password has not been checked since 90 days'
     write(6,*) '++|     Stop the program'
     stop

  end if

end subroutine sok_chekli
