!____________________________________________________________________________
! A C-program for MT19937: Real number version
!   genrand() generates one pseudorandom real number (double)
! which is uniformly distributed on [0,1]-interval, for each
! call. sgenrand(seed) set initial values to the working area
! of 624 words. Before genrand(), sgenrand(seed) must be
! called once. (seed is any 32-bit integer except for 0).
! Integer generator is obtained by modifying two lines.
!   Coded by Takuji Nishimura, considering the suggestions by
! Topher Cooper and Marc Rieffel in July-Aug. 1997.
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Library General Public
! License as published by the Free Software Foundation; either
! version 2 of the License, or (at your option) any later
! version.
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
! See the GNU Library General Public License for more details.
! You should have received a copy of the GNU Library General
! Public License along with this library; if not, write to the
! Free Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
! 02111-1307  USA
!
! Copyright (C) 1997 Makoto Matsumoto and Takuji Nishimura.
! When you use this, send an email to: matumoto@math.keio.ac.jp
! with an appropriate reference to your work.
!
!***********************************************************************
! Fortran translation by Hiroshi Takano.  Jan. 13, 1999.
!
!   genrand()      -> double precision function grnd()
!   sgenrand(seed) -> subroutine sgrnd(seed)
!                     integer seed
!
! This program uses the following non-standard intrinsics.
!   ishft(i,n): If n>0, shifts bits in i by n positions to left.
!               If n<0, shifts bits in i by n positions to right.
!   iand (i,j): Performs logical AND on corresponding bits of i and j.
!   ior  (i,j): Performs inclusive OR on corresponding bits of i and j.
!   ieor (i,j): Performs exclusive OR on corresponding bits of i and j.
!
!***********************************************************************
! Fortran version rewritten as an F90 module and mt state saving and getting
! subroutines added by Richard Woloshyn. (rwww@triumf.ca). June 30, 1999

module mod_random
  !
  use def_kintyp, only : rp
  !
  integer, parameter  :: ip4 = 4
  ! Default seed
  integer(ip4), parameter :: defaultsd = 4357
  ! Period parameters
  integer(ip4), parameter :: N = 624, N1 = N + 1
  ! the array for the state vector
  integer(ip4), save, dimension(0:N-1) :: mt
  integer(ip4), save                   :: mti = N1

contains
  !
  ! Initialization subroutine
  !
  subroutine random_sgrnd(seed)
    implicit none
    !
    !      setting initial seeds to mt[N] using
    !      the generator Line 25 of Table 1 in
    !      [KNUTH 1981, The Art of Computer Programming
    !         Vol. 2 (2nd Ed.), pp102]
    !
    integer(ip4), intent(in) :: seed

    mt(0) = iand(seed,-1)
    do mti = 1,N-1
       mt(mti) = iand(69069 * mt(mti-1),-1)
    end do
    return

  end subroutine random_sgrnd
  !
  ! Random number generator
  !
  real(rp) function random_grnd()
    implicit integer(ip4)(a-z)
    ! Period parameters
    integer(ip4), parameter :: M = 397, MATA  = -1727483681 ! constant vector a
    integer(ip4), parameter :: LMASK =  2147483647          ! least significant r bits
    integer(ip4), parameter :: UMASK = -LMASK - 1           ! most significant w-r bits
    ! Tempering parameters
    integer(ip4), parameter :: TMASKB= -1658038656, TMASKC= -272236544

    dimension mag01(0:1)
    data mag01/0, MATA/
    save mag01
    !                                       mag01(x) = x * MATA for x=0,1

    TSHFTU(y) = ishft(y,-11)
    TSHFTS(y) = ishft(y,  7)
    TSHFTT(y) = ishft(y, 15)
    TSHFTL(y) = ishft(y,-18)

    if( mti >= N ) then
       !                                    generate N words at one time
       if( mti == N+1 ) then
          !                                 if sgrnd() has not been called,
          call random_sgrnd( defaultsd )
          !                                 a default initial seed is used
       endif

       do kk = 0,N-M-1
          y      = ior(iand(mt(kk),UMASK),iand(mt(kk+1),LMASK))
          mt(kk) = ieor(ieor(mt(kk+M),ishft(y,-1)),mag01(iand(y,1)))
       enddo
       do kk = N-M,N-2
          y      = ior(iand(mt(kk),UMASK),iand(mt(kk+1),LMASK))
          mt(kk) = ieor(ieor(mt(kk+(M-N)),ishft(y,-1)),mag01(iand(y,1)))
       enddo
       y       = ior(iand(mt(N-1),UMASK),iand(mt(0),LMASK))
       mt(N-1) = ieor(ieor(mt(M-1),ishft(y,-1)),mag01(iand(y,1)))
       mti     = 0
    endif

    y   = mt(mti)
    mti = mti + 1 
    y   = ieor(y,TSHFTU(y))
    y   = ieor(y,iand(TSHFTS(y),TMASKB))
    y   = ieor(y,iand(TSHFTT(y),TMASKC))
    y   = ieor(y,TSHFTL(y))

    if( y < 0 )  then
       random_grnd = (real(y,rp)+2.0_rp**32)/(2.0_rp**32-1.0_rp)
    else
       random_grnd = real(y,rp)/(2.0_rp**32-1.0_rp)
    endif

  end function random_grnd

end module mod_random
