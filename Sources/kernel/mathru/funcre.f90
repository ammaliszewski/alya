function funcre(param,npara,ifuge,timev)

  !------------------------------------------------------------------------
  !
  ! This function yields a parabolic or periodic evolution
  !
  !------------------------------------------------------------------------

  use def_kintyp, only     :  ip,rp
  use def_master, only     :  funin,funou
  use def_domain, only     :  ndime
  use def_parame, only     :  pi
  implicit none
  real(rp)                 :: funcre
  integer(ip), intent(in)  :: npara,ifuge
  real(rp),    intent(in)  :: param(npara),timev
  integer(ip)              :: ipara,idime
  real(rp)                 :: timea,timeb,funca,funcb,zerom,timec
  real(rp)                 :: timei,timef
  real(rp)                 :: coor0(3),betaa,radiu,alpha,deltx,deltz

  funcre = 0.0_rp
  zerom  = epsilon(1.0_rp)

  if( ifuge == 0 ) then
     !
     ! No time dependence 
     !
     funcre = 1.0_rp

  else if( ifuge == 1 ) then
     !
     ! Parabolic evolution
     !
     if( param(1)-zerom <= timev .and. timev <= param(2)+zerom) then 
        funcre = param(3)*timev*timev+param(4)*timev+param(5)
     else if ( timev > param(2)+zerom ) then
        timea  = param(2)
        funcre = param(3)*timea*timea+param(4)*timea+param(5)
     else if ( timev < param(1)-zerom ) then
        timea  = param(1)
        funcre = param(3)*timea*timea+param(4)*timea+param(5)
     end if

  else if( ifuge == 2 ) then
     !
     ! Periodic evolution
     !
     if( param(1)-zerom <= timev .and. timev <= param(2)+zerom) then 
        funcre = param(3)*cos(param(4)*timev+param(5))+param(6)
     else if ( timev > param(2)+zerom ) then
        timea  = param(2)
        funcre = param(3)*cos(param(4)*timea+param(5))+param(6)
     else if ( timev < param(1)-zerom ) then
        timea  = param(1)
        funcre = param(3)*cos(param(4)*timea+param(5))+param(6)
     end if

  else if( ifuge == 3 .or. ifuge == -3 ) then
     !
     ! Discrete evolution
     !
     timei = param(1)
     timef = param((npara/2-1)*2+1)

     if( timev <= timei ) then
        funcre = param(2)
     else
        if( timev >= timef ) then            ! Look for the time inside the period
           timec = timev
           do while( timec > timef )
              timec = timec - (timef-timei)
           end do
        else
           timec = timev
        end if
        ipara = 0
        do while( ipara < npara/2 )
           ipara = ipara+1
           
           if(timec <param((ipara-1)*2+1)) then
              timea  = param((ipara-1)*2+1)
              funca  = param((ipara-1)*2+2)
              timeb  = param((ipara-2)*2+1)
              funcb  = param((ipara-2)*2+2)
              funcre = (funcb-funca)/(timeb-timea)*(timec-timea)+funca
              ipara  = npara/2
           end if
        end do
     end if

  else if( ifuge == 4 ) then
     !
     ! Special function to change boundary values
     !
     funcre = param(2)

  else if( ifuge == 5 ) then
     !
     ! Marek Prymon's function
     !
     if( timev < param(1) ) then
        funcre = param(2)
     else if( timev < param(3) ) then
        funcre = param(4) - param(5) * timev
     else
        funcre = param(6)
     end if

  else if( ifuge == 6 ) then
     !
     ! Translation
     !
     funcre = 1.0_rp
     do idime = 1,ndime
        funou(idime) = param(idime) 
     end do

  else if( ifuge == 7 ) then
     !
     ! Rotation
     !
     funcre   = 1.0_rp
     coor0(1) = param(1)
     coor0(2) = param(2)
     coor0(3) = param(3)
     betaa    = param(4)*pi/180.0_rp
     deltx    = funin(1)-coor0(1)
     deltz    = funin(3)-coor0(3)
     radiu    = sqrt(  (deltx*deltx) + (deltz*deltz) )     
     alpha    = atan2( deltz , deltx )
     
     funou(1) =  ( radiu * cos(alpha+betaa) ) + coor0(1)
     funou(2) =  funin(2)
     funou(3) =  ( radiu * sin(alpha+betaa) ) + coor0(3) 
     do idime = 1,ndime
        funou(idime) = funou(idime) - funin(idime)
     end do
     
  else if( ifuge == 8 ) then
     !
     ! CYCLE function for the nose 'CYCLE'
     !  
     ! periodic evolution with 6 ordre
     !

     funcre = -0.000000582_rp&
          +0.000058387_rp*cos(2.579_rp*timev)+0.000598270_rp*sin(2.579_rp*timev)&
          -0.000004015_rp*cos(5.158_rp*timev)-0.000006084_rp*sin(5.158_rp*timev)&
          -0.000043093_rp*cos(7.737_rp*timev)+0.000111284_rp*sin(7.737_rp*timev)&
          -0.00000087_rp*cos(10.316_rp*timev)-0.000006765_rp*sin(10.316_rp*timev)&
          -0.000014982_rp*cos(12.895_rp*timev)+0.000028346_rp*sin(12.895_rp*timev)&  
          +0.000000056_rp*cos(15.474_rp*timev)-0.000005522_rp*sin(15.474_rp*timev)&
          -0.000002663_rp*cos(18.053_rp*timev)+0.000003572_rp*sin(18.053_rp*timev) 

  else if( ifuge == 9 ) then
     !
     ! SNIFF function for the nose 'SNIF2'
     !  
     ! Parabolic evolution 10th order
     !
     if( param(1)-zerom <= timev .and. timev <= param(2)+zerom) then
        funcre = param(3)*timev**10_rp + param(4)*timev**9_rp + param(5)*timev**8_rp+&
             param(6)*timev**7_rp + param(7)*timev**6_rp + param(8)*timev**5_rp +&
             param(9)*timev**4_rp + param(10)*timev**3_rp + param(11)*timev**2_rp +&
             param(12)*timev + param(13)
        
     else if ( timev > param(2)+zerom ) then
        timea  = param(2)
        funcre = param(3)*timev**10_rp + param(4)*timev**9_rp + param(5)*timev**8_rp+&
             param(6)*timev**7_rp + param(7)*timev**6_rp + param(8)*timev**5_rp +&
             param(9)*timev**4_rp + param(10)*timev**3_rp + param(11)*timev**2_rp +&
             param(12)*timev + param(13)
        
     else if ( timev < param(1)-zerom ) then
        timea  = param(1)
        funcre = param(3)*timev**10_rp + param(4)*timev**9_rp + param(5)*timev**8_rp+&
             param(6)*timev**7_rp + param(7)*timev**6_rp + param(8)*timev**5_rp +&
             param(9)*timev**4_rp + param(10)*timev**3_rp + param(11)*timev**2_rp +&
             param(12)*timev + param(13)
     end if
     
  end if
  
end function funcre
 
