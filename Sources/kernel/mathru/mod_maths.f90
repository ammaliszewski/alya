!-----------------------------------------------------------------------
!
!> @addtogroup MathsToolBox
!> @{
!> @name    ToolBox for mathematics operations
!> @file    mod_maths.f90
!> @author  Guillaume Houzeaux
!> @brief   ToolBox for maths
!> @details ToolBox for maths
!> @{
!
!-----------------------------------------------------------------------

module mod_maths

  use def_kintyp, only : ip,rp,lg
  implicit none

  private 

  interface maths_equalize_arrays
     module procedure maths_equalize_arrays_RP_11,&
          &           maths_equalize_arrays_RP_12,&
          &           maths_equalize_arrays_RP_22,&
          &           maths_equalize_arrays_RP_ndim1,&
          &           maths_equalize_arrays_RP_ndim1_ndim2_22,&
          &           maths_equalize_arrays_RP_ndim1_ndim2_32,&
          &           maths_equalize_arrays_RP_ndim1_ndim2_23,&
          &           maths_equalize_arrays_RP_ndim1_ndim2_33
  end interface maths_equalize_arrays

  public :: maths_mapping_3d_to_1d
  public :: maths_mapping_1d_to_3d_x
  public :: maths_mapping_1d_to_3d_y
  public :: maths_mapping_1d_to_3d_z
  public :: maths_mapping_coord_to_3d
  public :: maths_in_box
  public :: maths_invert_matrix
  public :: maths_equalize_arrays
  public :: maths_heap_sort
  public :: maths_local_orthonormal_basis
  public :: maths_vector_to_new_basis
  public :: maths_vector_from_new_basis

contains 

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/10/2014
  !> @brief   Equalize some arrays
  !> @details Equalize some arrays
  !
  !----------------------------------------------------------------------

  subroutine maths_equalize_arrays_RP_ndim1(ndim1,x_in,x_out)
    integer(ip),       intent(in)  :: ndim1
    real(rp),          intent(in)  :: x_in(*)
    real(rp),          intent(out) :: x_out(*)
    integer(ip)                    :: idim1
    do idim1 = 1,ndim1
       x_out(idim1) = x_in(idim1)
    end do
  end subroutine maths_equalize_arrays_RP_ndim1
  subroutine maths_equalize_arrays_RP_ndim1_ndim2_22(ndim1,ndim2,x_in,x_out)
    integer(ip),       intent(in)  :: ndim1
    integer(ip),       intent(in)  :: ndim2
    real(rp),          intent(in)  :: x_in(ndim1,ndim2)
    real(rp),          intent(out) :: x_out(ndim1,ndim2)
    integer(ip)                    :: idim1,idim2
    do idim2 = 1,ndim2
       do idim1 = 1,ndim1
          x_out(idim1,idim2) = x_in(idim1,idim2)
       end do
    end do
  end subroutine maths_equalize_arrays_RP_ndim1_ndim2_22
  subroutine maths_equalize_arrays_RP_ndim1_ndim2_32(ndim1,ndim2,x_in,x_out)
    integer(ip),       intent(in)  :: ndim1
    integer(ip),       intent(in)  :: ndim2
    real(rp),          intent(in)  :: x_in(ndim1,ndim2,*)
    real(rp),          intent(out) :: x_out(ndim1,ndim2)
    integer(ip)                    :: idim1,idim2
    do idim2 = 1,ndim2
       do idim1 = 1,ndim1
          x_out(idim1,idim2) = x_in(idim1,idim2,1)
       end do
    end do
  end subroutine maths_equalize_arrays_RP_ndim1_ndim2_32
  subroutine maths_equalize_arrays_RP_ndim1_ndim2_23(ndim1,ndim2,x_in,x_out)
    integer(ip),       intent(in)  :: ndim1
    integer(ip),       intent(in)  :: ndim2
    real(rp),          intent(in)  :: x_in(ndim1,ndim2)
    real(rp),          intent(out) :: x_out(ndim1,ndim2,*)
    integer(ip)                    :: idim1,idim2
    do idim2 = 1,ndim2
       do idim1 = 1,ndim1
          x_out(idim1,idim2,1) = x_in(idim1,idim2)
       end do
    end do
  end subroutine maths_equalize_arrays_RP_ndim1_ndim2_23
  subroutine maths_equalize_arrays_RP_ndim1_ndim2_33(ndim1,ndim2,x_in,x_out)
    integer(ip),       intent(in)  :: ndim1
    integer(ip),       intent(in)  :: ndim2
    real(rp),          intent(in)  :: x_in(ndim1,ndim2,*)
    real(rp),          intent(out) :: x_out(ndim1,ndim2,*)
    integer(ip)                    :: idim1,idim2
    do idim2 = 1,ndim2
       do idim1 = 1,ndim1
          x_out(idim1,idim2,1) = x_in(idim1,idim2,1)
       end do
    end do
  end subroutine maths_equalize_arrays_RP_ndim1_ndim2_33

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/10/2014
  !> @brief   Equalize some arrays
  !> @details Equalize some arrays
  !
  !----------------------------------------------------------------------

  subroutine maths_equalize_arrays_RP_11(x_in,x_out)
    real(rp), pointer, intent(in)  :: x_in(:)
    real(rp), pointer, intent(out) :: x_out(:)
    integer(ip)                    :: idim1
    integer(ip)                    :: ndim1

    ndim1 = min(size(x_in),size(x_out))

    do idim1 = 1,ndim1
       x_out(idim1) = x_in(idim1)
    end do

  end subroutine maths_equalize_arrays_RP_11

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/10/2014
  !> @brief   Equalize some arrays
  !> @details Equalize some arrays
  !
  !----------------------------------------------------------------------

  subroutine maths_equalize_arrays_RP_12(x_in,x_out)
    real(rp), pointer, intent(in)  :: x_in(:)
    real(rp), pointer, intent(out) :: x_out(:,:)
    integer(ip)                    :: idim1,idim2,idime
    integer(ip)                    :: ndim1,ndim2

    ndim1 = size(x_out,1)
    ndim2 = size(x_out,2)

     if( size(x_in) /= ndim1*ndim2 ) then
       call runend('maths_equalize_arrays: ARRAYS ARE NOT OF SAME DIMENSIONS')
    else
       idime = 0
       do idim2 = 1,ndim2
          do idim1 = 1,ndim1
             idime = idime + 1
             x_out(idim1,idim2) = x_in(idime)
          end do
       end do
    end if

  end subroutine maths_equalize_arrays_RP_12

  subroutine maths_equalize_arrays_RP_22(x_in,x_out)
    real(rp), pointer, intent(in)  :: x_in(:,:)
    real(rp), pointer, intent(out) :: x_out(:,:)
    integer(ip)                    :: idim1,idim2
    integer(ip)                    :: ndim1,ndim2

    ndim1 = size(x_out,1)
    ndim2 = size(x_out,2)

     if( size(x_in,1) /= ndim1 .and. size(x_in,2) /= ndim2 ) then
       call runend('maths_equalize_arrays: ARRAYS ARE NOT OF SAME DIMENSIONS')
    else
       do idim2 = 1,ndim2
          do idim1 = 1,ndim1
             x_out(idim1,idim2) = x_in(idim1,idim2)
          end do
       end do
    end if

  end subroutine maths_equalize_arrays_RP_22

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    28/06/2012
  !> @brief   Mapping (x,y,z) => s 
  !> @details Mapping 3D array <=> 1D array
  !>          x: 1 to mx
  !>          y: 1 to my
  !>          z: 1 to mz
  !>          s: 1 to mx*my*mx
  !>
  !>          3D to 1D: s = x + mx*[ (y-1) + (z-1)*my ]
  !>          1D to 3D: x = mod(s-1,mx) + 1
  !>                    y = mod((s-1)/mx,my) + 1
  !>                    z = (s-1)/(mx*my) + 1
  !
  !----------------------------------------------------------------------

  function maths_mapping_3d_to_1d(mx,my,mz,xx,yy,zz)
    integer(ip), intent(in) :: mz,my,mx
    integer(ip), intent(in) :: zz,yy,xx
    integer(ip)             :: maths_mapping_3d_to_1d
    maths_mapping_3d_to_1d = xx+mx*( yy-1 + (zz-1)*my)
  end function maths_mapping_3d_to_1d

  function maths_mapping_1d_to_3d_x(mx,my,mz,ss)
    integer(ip), intent(in) :: mx,my,mz,ss
    integer(ip)             :: maths_mapping_1d_to_3d_x
    maths_mapping_1d_to_3d_x = modulo(ss-1,mx)+1
  end function maths_mapping_1d_to_3d_x

  function maths_mapping_1d_to_3d_y(mx,my,mz,ss)
    integer(ip), intent(in) :: mx,my,mz,ss
    integer(ip)             :: maths_mapping_1d_to_3d_y
    maths_mapping_1d_to_3d_y = modulo((ss-1)/mx,my)+1
  end function maths_mapping_1d_to_3d_y

  function maths_mapping_1d_to_3d_z(mx,my,mz,ss)
    integer(ip), intent(in) :: mx,my,mz,ss
    integer(ip)             :: maths_mapping_1d_to_3d_z
    maths_mapping_1d_to_3d_z = (ss-1) / (mx*my)+1
  end function maths_mapping_1d_to_3d_z

  !-----------------------------------------------------------------------
  !
  !> @date    27/02/2014
  !> @author  Guillaume Houzeaux
  !> @brief   Find the box in a bin structure
  !> @details Find the box (II,JJ,KK) the point COORD is located in.
  !>          II = 0 if points is out of the bin
  !
  !-----------------------------------------------------------------------

  subroutine maths_mapping_coord_to_3d(ndime,boxes,comin,comax,coord,ii,jj,kk)
    integer(ip), intent(in)  :: ndime         !< Dimension of problem          
    integer(ip), intent(in)  :: boxes(ndime)  !< # boxes in each dimension
    real(rp),    intent(in)  :: comin(ndime)  !< Minimum bin coordinates
    real(rp),    intent(in)  :: comax(ndime)  !< maximum bin coordinates
    real(rp),    intent(in)  :: coord(ndime)  !< Coordinate of the test point
    integer(ip), intent(out) :: ii            !< Box in x direction
    integer(ip), intent(out) :: jj            !< Box in y direction
    integer(ip), intent(out) :: kk            !< Box in z direction

    ii = int( ( (coord(1)-comin(1)) / (comax(1)-comin(1)) )*real(boxes(1),rp), ip ) + 1
    if( ii < 1 .or. ii > boxes(1) ) ii = 0
    if( ndime >= 2 ) then
       jj = int( ( (coord(2)-comin(2)) / (comax(2)-comin(2)) )*real(boxes(2),rp), ip ) + 1
       if( jj < 1 .or. jj > boxes(2) ) ii = 0
    else
       jj = 1
    end if
    if( ndime >= 3 ) then
       kk = int( ( (coord(3)-comin(3)) / (comax(3)-comin(3)) )*real(boxes(3),rp), ip ) + 1
       if( kk < 1 .or. kk > boxes(3) ) ii = 0
    else
       kk = 1
    end if

  end subroutine maths_mapping_coord_to_3d
 
  !-----------------------------------------------------------------------
  !
  !> @date    23/05/2014
  !> @author  Guillaume Houzeaux
  !> @brief   If in a box
  !> @details If in a box
  !
  !-----------------------------------------------------------------------

  function maths_in_box(ndime,xx,box_comin,box_comax)
    integer(ip), intent(in) :: ndime
    real(rp),    intent(in) :: xx(ndime)
    real(rp),    intent(in) :: box_comin(ndime)
    real(rp),    intent(in) :: box_comax(ndime)
    logical(lg)             :: maths_in_box
    integer(ip)             :: idime

    maths_in_box = .true.
    do idime = 1,ndime
       if( xx(idime) < box_comin(idime) .or. xx(idime) > box_comax(idime) ) then
          maths_in_box = .false.
          return
       end if
    end do

  end function maths_in_box

  !-----------------------------------------------------------------------
  !
  !> @date    27/06/2014
  !> @author  Guillaume Houzeaux
  !> @brief   Inverse a matrix
  !> @details Invert
  !
  !-----------------------------------------------------------------------

  subroutine maths_invert_matrix(nsize,a,deter)
    implicit none
    integer(ip), intent(in)             :: nsize
    real(rp),    intent(inout)          :: a(nsize,nsize)
    real(rp),    intent(out),  optional :: deter
    real(rp)                            :: denom,t1,t2,t3,t4,det
    real(rp),    pointer                :: b(:,:)
    integer(ip)                         :: ii,jj

    allocate( b(nsize,nsize) )
    do jj = 1,nsize
       do ii = 1,nsize
          b(ii,jj) = a(ii,jj)
       end do
    end do

    select case( nsize )

    case ( 1_ip )

       det = b(1,1)
       if( abs(det) < epsilon(1.0_rp) ) goto 10
       a(1,1) = 1.0_rp/b(1,1)

    case( 2_ip )

       det = b(1,1)*b(2,2)-b(2,1)*b(1,2)
       if( abs(det) < epsilon(1.0_rp) ) goto 10
       denom  = 1.0_rp/det
       a(1,1) = b(2,2)*denom
       a(2,2) = b(1,1)*denom
       a(2,1) =-b(2,1)*denom
       a(1,2) =-b(1,2)*denom  

    case(3)
       t1  = b(2,2)*b(3,3) - b(3,2)*b(2,3)
       t2  =-b(2,1)*b(3,3) + b(3,1)*b(2,3)
       t3  = b(2,1)*b(3,2) - b(3,1)*b(2,2)
       det = b(1,1)*t1 + b(1,2)*t2 + b(1,3)*t3
       if( abs(det) < epsilon(1.0_rp) ) goto 10
       denom  = 1.0_rp/det
       a(1,1) = t1*denom
       a(2,1) = t2*denom
       a(3,1) = t3*denom
       a(2,2) = ( b(1,1)*b(3,3) - b(3,1)*b(1,3))*denom
       a(3,2) = (-b(1,1)*b(3,2) + b(1,2)*b(3,1))*denom
       a(3,3) = ( b(1,1)*b(2,2) - b(2,1)*b(1,2))*denom
       a(1,2) = (-b(1,2)*b(3,3) + b(3,2)*b(1,3))*denom
       a(1,3) = ( b(1,2)*b(2,3) - b(2,2)*b(1,3))*denom
       a(2,3) = (-b(1,1)*b(2,3) + b(2,1)*b(1,3))*denom

    case(4)
       t1= b(2,2)*b(3,3)*b(4,4) + b(2,3)*b(3,4)*b(4,2)&
            + b(2,4)*b(3,2)*b(4,3) - b(2,3)*b(3,2)*b(4,4)&
            - b(2,2)*b(3,4)*b(4,3) - b(2,4)*b(3,3)*b(4,2)
       t2=-b(2,1)*b(3,3)*b(4,4) - b(2,3)*b(3,4)*b(4,1)&
            - b(2,4)*b(3,1)*b(4,3) + b(2,4)*b(3,3)*b(4,1)&
            + b(2,3)*b(3,1)*b(4,4) + b(2,1)*b(3,4)*b(4,3)
       t3=+b(2,1)*b(3,2)*b(4,4) + b(2,2)*b(3,4)*b(4,1)&
            + b(2,4)*b(3,1)*b(4,2) - b(2,4)*b(3,2)*b(4,1)&
            - b(2,2)*b(3,1)*b(4,4) - b(2,1)*b(3,4)*b(4,2)
       t4=-b(2,1)*b(3,2)*b(4,3) - b(2,2)*b(3,3)*b(4,1)&
            - b(2,3)*b(3,1)*b(4,2) + b(2,3)*b(3,2)*b(4,1)&
            + b(2,2)*b(3,1)*b(4,3) + b(2,1)*b(3,3)*b(4,2)
       det= b(1,1)*t1 + b(1,2)*t2 + b(1,3)*t3 + b(1,4)*t4
       if( abs(det) < epsilon(1.0_rp) ) goto 10
       denom=1.0_rp/det
       a(1,1) = t1*denom
       a(2,1) = t2*denom
       a(3,1) = t3*denom
       a(4,1) = t4*denom
       a(1,2) =(- b(1,2)*b(3,3)*b(4,4) - b(1,3)*b(3,4)*b(4,2)&
            - b(1,4)*b(3,2)*b(4,3) + b(1,3)*b(3,2)*b(4,4)&
            + b(1,2)*b(3,4)*b(4,3) + b(1,4)*b(3,3)*b(4,2))*denom
       a(2,2) =(  b(1,1)*b(3,3)*b(4,4) + b(1,3)*b(3,4)*b(4,1)&
            + b(1,4)*b(3,1)*b(4,3) - b(1,4)*b(3,3)*b(4,1)&
            - b(1,3)*b(3,1)*b(4,4) - b(1,1)*b(3,4)*b(4,3))*denom
       a(3,2) =(- b(1,1)*b(3,2)*b(4,4) - b(1,2)*b(3,4)*b(4,1)&
            - b(1,4)*b(3,1)*b(4,2) + b(1,4)*b(3,2)*b(4,1)&
            + b(1,2)*b(3,1)*b(4,4) + b(1,1)*b(3,4)*b(4,2))*denom
       a(4,2) =(  b(1,1)*b(3,2)*b(4,3) + b(1,2)*b(3,3)*b(4,1)&
            + b(1,3)*b(3,1)*b(4,2) - b(1,3)*b(3,2)*b(4,1)&
            - b(1,2)*b(3,1)*b(4,3) - b(1,1)*b(3,3)*b(4,2))*denom
       a(1,3) =(  b(1,2)*b(2,3)*b(4,4) + b(1,3)*b(2,4)*b(4,2)&
            + b(1,4)*b(2,2)*b(4,3) - b(1,3)*b(2,2)*b(4,4)&
            - b(1,2)*b(2,4)*b(4,3) - b(1,4)*b(2,3)*b(4,2))*denom
       a(2,3) =(- b(1,1)*b(2,3)*b(4,4) - b(1,3)*b(2,4)*b(4,1)&
            - b(1,4)*b(2,1)*b(4,3) + b(1,4)*b(2,3)*b(4,1)&
            + b(1,3)*b(2,1)*b(4,4) + b(1,1)*b(2,4)*b(4,3))*denom
       a(3,3) =(  b(1,1)*b(2,2)*b(4,4) + b(1,2)*b(2,4)*b(4,1)&
            + b(1,4)*b(2,1)*b(4,2) - b(1,4)*b(2,2)*b(4,1)&
            - b(1,2)*b(2,1)*b(4,4) - b(1,1)*b(2,4)*b(4,2))*denom
       a(4,3) =(- b(1,1)*b(2,2)*b(4,3) - b(1,2)*b(2,3)*b(4,1)&
            - b(1,3)*b(2,1)*b(4,2) + b(1,3)*b(2,2)*b(4,1)&
            + b(1,2)*b(2,1)*b(4,3) + b(1,1)*b(2,3)*b(4,2))*denom
       a(1,4) =(- b(1,2)*b(2,3)*b(3,4) - b(1,3)*b(2,4)*b(3,2)&
            - b(1,4)*b(2,2)*b(3,3) + b(1,4)*b(2,3)*b(3,2)&
            + b(1,3)*b(2,2)*b(3,4) + b(1,2)*b(2,4)*b(3,3))*denom
       a(2,4) =(  b(1,1)*b(2,3)*b(3,4) + b(1,3)*b(2,4)*b(3,1)&
            + b(1,4)*b(2,1)*b(3,3) - b(1,4)*b(2,3)*b(3,1)&
            - b(1,3)*b(2,1)*b(3,4) - b(1,1)*b(2,4)*b(3,3))*denom
       a(3,4) =(- b(1,1)*b(2,2)*b(3,4) - b(1,2)*b(2,4)*b(3,1)&
            - b(1,4)*b(2,1)*b(3,2) + b(1,4)*b(2,2)*b(3,1)&
            + b(1,2)*b(2,1)*b(3,4) + b(1,1)*b(2,4)*b(3,2))*denom
       a(4,4) =(  b(1,1)*b(2,2)*b(3,3) + b(1,2)*b(2,3)*b(3,1)&
            + b(1,3)*b(2,1)*b(3,2) - b(1,3)*b(2,2)*b(3,1)&
            - b(1,2)*b(2,1)*b(3,3) - b(1,1)*b(2,3)*b(3,2))*denom


    case default
       call runend('maths_ivert_matrix not coded')

    end select

10  if( present(deter) ) deter = det
    deallocate( b )

  end subroutine maths_invert_matrix

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/10/2014
  !> @brief   heap sort
  !> @details Equalize some arrays
  !>          Quick sorting. The element in ivin are sorting in:
  !>          ITASK = 1 ... Decreasing value, i.e., ivin(1) > ivin(2) > ...
  !>          ITASK = 2 ... Increasing value, i.e., ivin(1) < ivin(2) < ...
  !
  !----------------------------------------------------------------------

  subroutine maths_heap_sort(itask,nrows,ivin,message)

    integer(ip),  intent(in)           :: itask
    integer(ip),  intent(inout)        :: nrows
    integer(ip),  intent(inout)        :: ivin(*) 
    character(*), intent(in), optional :: message
    integer(ip)                        :: len,ir,ii,jj,iaux,krows

    select case ( itask )

    case ( 1_ip )
       !
       ! Decreasing order
       !
       if( nrows < 2 ) then
          goto 500
       end if

       len = (nrows/2) + 1
       ir  = nrows

100    continue

       if( len > 1 ) then
          len = len - 1
          iaux = ivin(len)
       else
          iaux = ivin(ir)
          ivin(ir) = ivin(1)

          ir = ir - 1

          if( ir == 1 ) then
             ivin(1) = iaux
             goto 500
          end if
       end if

       ii = len
       jj = len + len

200    if( jj <= ir ) then
          if( jj < ir ) then
             if( ivin(jj) > ivin(jj+1) ) then
                jj = jj + 1
             end if
          end if

          if( iaux > ivin(jj) ) then
             ivin(ii) = ivin(jj)
             ii = jj
             jj = jj + jj
          else
             jj = ir + 1
          endif

          goto 200
       end if

       ivin(ii) = iaux

       goto 100

    case ( 2_ip )
       !
       ! Increasing order
       !
       if( nrows < 2 ) then
          goto 500
       end if

       len = (nrows/2) + 1
       ir  = nrows

300    continue

       if( len > 1 ) then
          len = len - 1
          iaux = ivin(len)
       else
          iaux = ivin(ir)
          ivin(ir) = ivin(1)

          ir = ir - 1

          if( ir == 1 ) then
             ivin(1) = iaux
             goto 500
          end if
       end if

       ii = len
       jj = len + len

400    if( jj <= ir ) then
          if( jj < ir ) then
             if ( ivin(jj) < ivin(jj+1) ) then
                jj = jj + 1
             end if
          end if

          if( iaux < ivin(jj) ) then
             ivin(ii) = ivin(jj)
             ii = jj
             jj = jj + jj
          else
             jj = ir + 1
          end if

          goto 400
       end if

       ivin(ii) = iaux

       goto 300

    end select

    return
    !
    ! Eliminate duplicates
    !
500 continue
    if( present(message) ) then
       if( trim(message) == 'ELIMINATE DUPLICATES') then
          krows = nrows
          do ii = 1,krows-1
             if( ivin(ii+1) == ivin(ii) ) then
                do jj = ii+1,krows-1
                   ivin(jj) = ivin(jj+1) 
                end do
                krows = krows - 1
             end if
          end do
          nrows = krows
       end if
    end if


  end subroutine maths_heap_sort

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    04/12/2015
  !> @brief   Computes tangent vectors 
  !> @details Computes tangent vectors given a normal vector which form
  !>          an orthogonal basis
  !>
  !----------------------------------------------------------------------

  subroutine maths_local_orthonormal_basis(ndime,basis,ierro)

    integer(ip), intent(in)            :: ndime               !< Dimension
    real(rp),    intent(inout)         :: basis(ndime,ndime)  !< Local basis: normal is BASIS(1:NDIME,1)
    integer(ip), intent(out), optional :: ierro               !< Error counter
    integer(ip)                        :: idime,kdime
    real(rp)                           :: xnor1,xnor2
    real(rp)                           :: xnor3,xnoma
    real(rp)                           :: exwor(3)

    if( ndime == 2 ) then

       basis(1,2) = -basis(2,1)
       basis(2,2) =  basis(1,1)

    else if( ndime == 3 ) then
       !
       ! Look for e_k such that n x e_k is maximum
       !     
       xnor1    = basis(1,1) * basis(1,1)
       xnor2    = basis(2,1) * basis(2,1)
       xnor3    = basis(3,1) * basis(3,1)
       exwor(1) = xnor2 + xnor3
       exwor(2) = xnor1 + xnor3
       exwor(3) = xnor1 + xnor2
       xnoma    = 0.0_rp
       kdime    = 0
       do idime = 1,3
          if( exwor(idime) > xnoma ) then
             xnoma = exwor(idime)
             kdime = idime
          end if
       end do
       xnoma = 1.0_rp / sqrt(xnoma)
       !
       ! Set t_1 = e_k x n, first tangent vector
       !
       if( kdime == 1 ) then
          basis(1,2) =  0.0_rp
          basis(2,2) = -basis(3,1) * xnoma
          basis(3,2) =  basis(2,1) * xnoma
       else if( kdime == 2 ) then
          basis(1,2) =  basis(3,1) * xnoma
          basis(2,2) =  0.0_rp
          basis(3,2) = -basis(1,1) * xnoma
       else if( kdime == 3 ) then
          basis(1,2) = -basis(2,1) * xnoma
          basis(2,2) =  basis(1,1) * xnoma
          basis(3,2) =  0.0_rp
       else       
          if( present(ierro) ) then
             ierro = ierro + 1
          else
             call runend('MATHS_LOCAL_BASIS: WRONG NORMAL')
          end if
       end if
       !            
       ! Set t_2 = n x t_1, second tangent vector          
       !
       basis(1,3) = basis(2,1) * basis(3,2) - basis(3,1) * basis(2,2)
       basis(2,3) = basis(3,1) * basis(1,2) - basis(1,1) * basis(3,2)
       basis(3,3) = basis(1,1) * basis(2,2) - basis(2,1) * basis(1,2)

    end if

  end subroutine maths_local_orthonormal_basis

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    04/12/2015
  !> @brief   Rotate a vector 
  !> @details Rotate a vector to a new basis
  !>
  !----------------------------------------------------------------------

  subroutine maths_vector_to_new_basis(ndime,basis,vv,ww)

    integer(ip), intent(in)              :: ndime               !< Dimension
    real(rp),    intent(in)              :: basis(ndime,ndime)  !< Local basis: normal is BASIS(1:NDIME,1)
    real(rp),    intent(inout)           :: vv(ndime)           !< Vector 1
    real(rp),    intent(inout), optional :: ww(ndime)           !< Vector 2
    real(rp)                             :: vv_tmp(ndime)
    real(rp)                             :: ww_tmp(ndime)
    integer(ip)                          :: ii,jj
    
    if( present(ww) ) then
       vv_tmp(1:ndime) = vv(1:ndime)
       ww_tmp(1:ndime) = ww(1:ndime)
       do ii = 1,ndime
          vv(ii) = 0.0_rp
          ww(ii) = 0.0_rp
          do jj = 1,ndime
             vv(ii) = vv(ii) + basis(jj,ii) * vv_tmp(jj)
             ww(ii) = ww(ii) + basis(jj,ii) * ww_tmp(jj)
          end do
       end do
    else
       vv_tmp(1:ndime) = vv(1:ndime)
       do ii = 1,ndime
          vv(ii) = 0.0_rp
          do jj = 1,ndime
             vv(ii) = vv(ii) + basis(jj,ii) * vv_tmp(jj)
          end do
       end do
    end if

  end subroutine maths_vector_to_new_basis

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    04/12/2015
  !> @brief   Rotate a vector 
  !> @details Rotate a vector to a new basis
  !>
  !----------------------------------------------------------------------

  subroutine maths_vector_from_new_basis(ndime,basis,vv,ww)

    integer(ip), intent(in)              :: ndime               !< Dimension
    real(rp),    intent(in)              :: basis(ndime,ndime)  !< Local basis: normal is BASIS(1:NDIME,1)
    real(rp),    intent(inout)           :: vv(ndime)           !< Vector 1
    real(rp),    intent(inout), optional :: ww(ndime)           !< Vector 2
    real(rp)                             :: vv_tmp(ndime)
    real(rp)                             :: ww_tmp(ndime)
    integer(ip)                          :: ii,jj
    
    if( present(ww) ) then
       vv_tmp(1:ndime) = vv(1:ndime)
       ww_tmp(1:ndime) = ww(1:ndime)
       do ii = 1,ndime
          vv(ii) = 0.0_rp
          ww(ii) = 0.0_rp
          do jj = 1,ndime
             vv(ii) = vv(ii) + basis(ii,jj) * vv_tmp(jj)
             ww(ii) = ww(ii) + basis(ii,jj) * ww_tmp(jj)
          end do
       end do
    else
       vv_tmp(1:ndime) = vv(1:ndime)
       do ii = 1,ndime
          vv(ii) = 0.0_rp
          do jj = 1,ndime
             vv(ii) = vv(ii) + basis(ii,jj) * vv_tmp(jj)
          end do
       end do
    end if

  end subroutine maths_vector_from_new_basis

end module mod_maths
!> @}
