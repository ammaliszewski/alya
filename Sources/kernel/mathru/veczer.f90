subroutine veczer(n,v,mod)

!-----------------------------------------------------------------------
!
! This routine initializes a vector
!
!-----------------------------------------------------------------------
  use      def_kintyp, only : ip,rp
  implicit none
  integer(ip), intent(in) :: n
  real(rp), intent(in)    :: mod
  real(rp), intent(out)   :: v(n)
  integer(ip)             :: i

  do i=1,n
     v(i) = mod
  end do
 
end subroutine veczer
      
