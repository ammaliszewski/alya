!------------------------------------------------------------------------
!> @addtogroup ProjectionToolBox
!! @{
!> @name    ToolBox for L2 projections 
!! @file    mod_projec.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!! @brief   ToolBox for matrix operations
!! @details ToolBox for matrix operations: fill in, etc.
!! @{
!------------------------------------------------------------------------

module mod_projec

  use def_kintyp
  use def_parame
  use def_master, only         : party,pardi,parki,parr2,IMASTER,&
       &                         NPOIN_REAL_2DIM,INOTMASTER,ISLAVE,&
       &                         INOTSLAVE
  use def_kermod, only         : kfl_grpro
  use def_domain, only         : ndime,npoin,nelem,nnode,mnode,ntens,&
       &                         lnods,ltype,coord,vmass,elmar,vmasc,&
       &                         lexis,ngaus,kfl_naxis,lnodb,mnodb,&
       &                         lboel,mgaus,lelch,nboun,ltypb,ndimb
  use def_domain, only         : mesh_type
  use def_elmtyp
  use mod_communications, only : PAR_SCALAR_PRODUCT
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  use mod_memory  
  implicit none
  private
  integer(ip) :: knode
  integer(ip) :: ipoin,idime,inode,ielem,igaus,jnode
  integer(ip) :: pnode,pelty,pgaus,jdime,itens
  real(rp)    :: gpdet,gpvol
  real(rp)    :: xjaci(9),xjacm(9),xfact
  integer(8)  :: memor(2)

  interface projec_elements_to_nodes
     module procedure projec_elements_to_nodes_r3p,&
          &           projec_elements_to_nodes_rp
  end interface projec_elements_to_nodes

  public :: projec_mass_conservation 
  public :: projec_norm_symmetric_gradient_vector
  public :: projec_elements_to_nodes
  public :: projec_boundaries_to_nodes

contains

  subroutine projec_norm_symmetric_gradient_vector(unkno,grunk)

    !----------------------------------------------------------------------
    !
    !> @author  Guillaume Houzeaux
    !> @date    12/06/2013
    !> @brief   Project the norm of a symmetric gradient of a Tensor
    !> @details Given ui, project P=SijSij, with Sij=1/2(dui/dxj+duj/dxi)
    !
    !----------------------------------------------------------------------

    implicit none
    real(rp),   intent(in)           :: unkno(ndime,npoin)
    real(rp),   intent(out), pointer :: grunk(:)
    real(rp)                         :: elgra(ndime,ndime)
    real(rp)                         :: elunk(ndime,mnode)
    real(rp)                         :: gpcar(ndime,mnode)
    real(rp)                         :: elcod(ndime,mnode)
    real(rp)                         :: S11,S12,S22,S13,S23,S33
    if( INOTMASTER ) then
       !
       ! Allocate memory?
       !
       if( .not. associated(grunk) ) then
          call memory_alloca(memor,'GRUNK','mod_projec',grunk,npoin)
       end if
       !
       ! Initialization
       !
       do ipoin = 1,npoin
          grunk(ipoin) = 0.0_rp
       end do
       !
       ! Loop over elements
       !
       if( kfl_grpro == 0 ) then

          if( ndime == 3 ) then
             !
             ! 3D open rule
             !
             do ielem = 1,nelem
                pelty = ltype(ielem) 
                if( pelty > 0 ) then
                   pnode = nnode(pelty)
                   pgaus = ngaus(pelty)
                   !
                   ! Treat extension elements
                   !
                   if( lelch(ielem) == ELEXT ) then
                      knode = 1
                   else
                      knode = pnode
                   end if
                   !
                   ! Gather vectors
                   !
                   do inode = 1,pnode
                      ipoin          = lnods(inode,ielem)
                      elcod(1,inode) = coord(1,ipoin)
                      elcod(2,inode) = coord(2,ipoin)
                      elcod(3,inode) = coord(3,ipoin)
                      elunk(1,inode) = unkno(1,ipoin)
                      elunk(2,inode) = unkno(2,ipoin)
                      elunk(3,inode) = unkno(3,ipoin)
                   end do
                   !
                   ! Loop over Gauss points 
                   !
                   do igaus = 1,pgaus
                      call elmder(&
                           pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&
                           elcod,gpcar,gpdet,xjacm,xjaci)
                      gpvol = elmar(pelty) % weigp(igaus) * gpdet
                      !
                      ! Velocity strain rates
                      !                
                      elgra(1,1) = 0.0_rp
                      elgra(1,2) = 0.0_rp 
                      elgra(1,3) = 0.0_rp 
                      elgra(2,1) = 0.0_rp 
                      elgra(2,2) = 0.0_rp 
                      elgra(2,3) = 0.0_rp 
                      elgra(3,1) = 0.0_rp 
                      elgra(3,2) = 0.0_rp 
                      elgra(3,3) = 0.0_rp 
                      do jnode = 1,pnode
                         elgra(1,1) = elgra(1,1) + gpcar(1,jnode) * elunk(1,jnode)
                         elgra(1,2) = elgra(1,2) + gpcar(1,jnode) * elunk(2,jnode)
                         elgra(1,3) = elgra(1,3) + gpcar(1,jnode) * elunk(3,jnode)
                         elgra(2,1) = elgra(2,1) + gpcar(2,jnode) * elunk(1,jnode)
                         elgra(2,2) = elgra(2,2) + gpcar(2,jnode) * elunk(2,jnode)
                         elgra(2,3) = elgra(2,3) + gpcar(2,jnode) * elunk(3,jnode)
                         elgra(3,1) = elgra(3,1) + gpcar(3,jnode) * elunk(1,jnode)
                         elgra(3,2) = elgra(3,2) + gpcar(3,jnode) * elunk(2,jnode)
                         elgra(3,3) = elgra(3,3) + gpcar(3,jnode) * elunk(3,jnode)
                      end do
                      !
                      ! Assembly
                      !
                      do inode = 1,knode
                         ipoin = lnods(inode,ielem)
                         xfact = gpvol * elmar(pelty) % shape(inode,igaus) 
                         S11   = 0.5_rp * ( elgra(1,1) + elgra(1,1) )
                         S22   = 0.5_rp * ( elgra(2,2) + elgra(2,2) ) 
                         S12   = 0.5_rp * ( elgra(1,2) + elgra(2,1) ) 
                         S33   = 0.5_rp * ( elgra(3,3) + elgra(3,3) ) 
                         S13   = 0.5_rp * ( elgra(1,3) + elgra(3,1) ) 
                         S23   = 0.5_rp * ( elgra(2,3) + elgra(3,2) ) 
                         grunk(ipoin) = grunk(ipoin) + xfact * ( &
                              &   S11*S11 + S12*S12 + S13*S13    &
                              & + S12*S12 + S22*S22 + S23*S23    &      
                              & + S13*S13 + S23*S23 + S33*S33  )
                      end do
                   end do
                end if
             end do

          else
             !
             ! 2D open rule
             !
             do ielem = 1,nelem
                pelty = ltype(ielem)
                if( pelty > 0 ) then
                   pnode = nnode(pelty)
                   pgaus = ngaus(pelty)
                   !
                   ! Treat extension elements
                   !
                   if( lelch(ielem) == ELEXT ) then
                      knode = 1
                   else
                      knode = pnode
                   end if
                   !
                   ! Gather vectors
                   !
                   do inode = 1,pnode
                      ipoin = lnods(inode,ielem)
                      elcod(1:ndime,inode) = coord(1:ndime,ipoin)
                      elunk(1:ndime,inode) = unkno(1:ndime,ipoin)
                   end do
                   !
                   ! Loop over Gauss points (which are nodes)
                   !
                   do igaus = 1,pgaus
                      call elmder(&
                           pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&
                           elcod,gpcar,gpdet,xjacm,xjaci)
                      gpvol = elmar(pelty)%weigp(igaus)*gpdet
                      !
                      ! Velocity strain rates
                      !                
                      elgra(1,1) = 0.0_rp
                      elgra(1,2) = 0.0_rp 
                      elgra(2,1) = 0.0_rp 
                      elgra(2,2) = 0.0_rp
                      do jnode = 1,pnode
                         elgra(1,1) = elgra(1,1) + gpcar(1,jnode) * elunk(1,jnode)
                         elgra(1,2) = elgra(1,2) + gpcar(1,jnode) * elunk(2,jnode)
                         elgra(2,1) = elgra(2,1) + gpcar(2,jnode) * elunk(1,jnode)
                         elgra(2,2) = elgra(2,2) + gpcar(2,jnode) * elunk(2,jnode)
                      end do
                      !
                      ! Assembly
                      !
                      do inode = 1,knode
                         ipoin = lnods(inode,ielem)
                         xfact = gpvol * elmar(pelty) % shape(inode,igaus) 
                         S11   = 0.5_rp * ( elgra(1,1) + elgra(1,1) )
                         S22   = 0.5_rp * ( elgra(2,2) + elgra(2,2) ) 
                         S12   = 0.5_rp * ( elgra(1,2) + elgra(2,1) ) 
                         grunk(ipoin) = grunk(ipoin) + xfact * ( &
                              &   S11*S11 + S12*S12              &
                              & + S12*S12 + S22*S22            )
                      end do
                   end do
                end if
             end do
          end if

       else
          !
          ! 3D close rule
          !
          if( ndime == 3 ) then

             do ielem = 1,nelem
                pelty = ltype(ielem)

                if( pelty > 0 ) then
                   pgaus = ngaus(pelty)
                   pnode = nnode(pelty)
                   !
                   ! Treat extension elements
                   !
                   if( lelch(ielem) == ELEXT ) then
                      knode = 1
                   else
                      knode = pnode
                   end if
                   !
                   ! Gather vectors
                   !
                   do inode = 1,pnode
                      ipoin = lnods(inode,ielem)
                      elcod(1,inode) = coord(1,ipoin)
                      elcod(2,inode) = coord(2,ipoin)
                      elcod(3,inode) = coord(3,ipoin)
                      elunk(1,inode) = unkno(1,ipoin)
                      elunk(2,inode) = unkno(2,ipoin)
                      elunk(3,inode) = unkno(3,ipoin)
                   end do
                   !
                   ! Loop over Gauss points (which are nodes)
                   !
                   if( pelty /= PYR05 ) then

                      do inode = 1,knode
                         ipoin = lnods(inode,ielem)
                         call elmder(&
                              pnode,ndime,elmar(pelty)%deric(1,1,inode),&
                              elcod,gpcar,gpdet,xjacm,xjaci)
                         gpvol = elmar(pelty)%weigc(inode)*gpdet
                         !
                         ! Velocity strain rates
                         !                
                         elgra(1,1) = 0.0_rp
                         elgra(1,2) = 0.0_rp 
                         elgra(1,3) = 0.0_rp 
                         elgra(2,1) = 0.0_rp 
                         elgra(2,2) = 0.0_rp 
                         elgra(2,3) = 0.0_rp 
                         elgra(3,1) = 0.0_rp 
                         elgra(3,2) = 0.0_rp 
                         elgra(3,3) = 0.0_rp 
                         do jnode = 1,pnode
                            elgra(1,1) = elgra(1,1) + gpcar(1,jnode) * elunk(1,jnode)
                            elgra(1,2) = elgra(1,2) + gpcar(1,jnode) * elunk(2,jnode)
                            elgra(1,3) = elgra(1,3) + gpcar(1,jnode) * elunk(3,jnode)
                            elgra(2,1) = elgra(2,1) + gpcar(2,jnode) * elunk(1,jnode)
                            elgra(2,2) = elgra(2,2) + gpcar(2,jnode) * elunk(2,jnode)
                            elgra(2,3) = elgra(2,3) + gpcar(2,jnode) * elunk(3,jnode)
                            elgra(3,1) = elgra(3,1) + gpcar(3,jnode) * elunk(1,jnode)
                            elgra(3,2) = elgra(3,2) + gpcar(3,jnode) * elunk(2,jnode)
                            elgra(3,3) = elgra(3,3) + gpcar(3,jnode) * elunk(3,jnode)
                         end do
                         !
                         ! Assembly
                         !
                         xfact = gpvol 
                         S11   = 0.5_rp * ( elgra(1,1) + elgra(1,1) )
                         S22   = 0.5_rp * ( elgra(2,2) + elgra(2,2) ) 
                         S12   = 0.5_rp * ( elgra(1,2) + elgra(2,1) ) 
                         S33   = 0.5_rp * ( elgra(3,3) + elgra(3,3) ) 
                         S13   = 0.5_rp * ( elgra(1,3) + elgra(3,1) ) 
                         S23   = 0.5_rp * ( elgra(2,3) + elgra(3,2) ) 
                         grunk(ipoin) = grunk(ipoin) + xfact * ( &
                              & + S11*S11 + S12*S12 + S13*S13    &
                              & + S12*S12 + S22*S22 + S23*S23    &      
                              & + S13*S13 + S23*S23 + S33*S33  )

                      end do

                   else

                      do igaus = 1,pgaus
                         call elmder(&
                              pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&
                              elcod,gpcar,gpdet,xjacm,xjaci)
                         gpvol = elmar(pelty)%weigp(igaus)*gpdet
                         !
                         ! Velocity strain rates
                         !                
                         elgra(1,1) = 0.0_rp
                         elgra(1,2) = 0.0_rp 
                         elgra(1,3) = 0.0_rp 
                         elgra(2,1) = 0.0_rp 
                         elgra(2,2) = 0.0_rp 
                         elgra(2,3) = 0.0_rp 
                         elgra(3,1) = 0.0_rp 
                         elgra(3,2) = 0.0_rp 
                         elgra(3,3) = 0.0_rp 
                         do jnode = 1,pnode
                            elgra(1,1) = elgra(1,1) + gpcar(1,jnode) * elunk(1,jnode)
                            elgra(1,2) = elgra(1,2) + gpcar(1,jnode) * elunk(2,jnode)
                            elgra(1,3) = elgra(1,3) + gpcar(1,jnode) * elunk(3,jnode)
                            elgra(2,1) = elgra(2,1) + gpcar(2,jnode) * elunk(1,jnode)
                            elgra(2,2) = elgra(2,2) + gpcar(2,jnode) * elunk(2,jnode)
                            elgra(2,3) = elgra(2,3) + gpcar(2,jnode) * elunk(3,jnode)
                            elgra(3,1) = elgra(3,1) + gpcar(3,jnode) * elunk(1,jnode)
                            elgra(3,2) = elgra(3,2) + gpcar(3,jnode) * elunk(2,jnode)
                            elgra(3,3) = elgra(3,3) + gpcar(3,jnode) * elunk(3,jnode)
                         end do
                         !
                         ! Assembly
                         !
                         do inode = 1,knode
                            ipoin = lnods(inode,ielem)
                            xfact = gpvol * elmar(pelty) % shape(inode,igaus) 
                            S11   = 0.5_rp * ( elgra(1,1) + elgra(1,1) )
                            S22   = 0.5_rp * ( elgra(2,2) + elgra(2,2) ) 
                            S12   = 0.5_rp * ( elgra(1,2) + elgra(2,1) ) 
                            S33   = 0.5_rp * ( elgra(3,3) + elgra(3,3) ) 
                            S13   = 0.5_rp * ( elgra(1,3) + elgra(3,1) ) 
                            S23   = 0.5_rp * ( elgra(2,3) + elgra(3,2) ) 
                            grunk(ipoin) = grunk(ipoin) + xfact * ( &
                                 & + S11*S11 + S12*S12 + S13*S13    &
                                 & + S12*S12 + S22*S22 + S23*S23    &      
                                 & + S13*S13 + S23*S23 + S33*S33  )
                         end do
                      end do

                   end if

                end if
             end do

          else
             !
             ! 2D close rule
             !
             do ielem = 1,nelem
                pelty = ltype(ielem) 
                if( pelty > 0 ) then
                   pnode = nnode(pelty)
                   pgaus = ngaus(pelty)
                   !
                   ! Treat extension elements
                   !
                   if( lelch(ielem) == ELEXT ) then
                      knode = 1
                   else
                      knode = pnode
                   end if
                   !
                   ! Gather vectors
                   !
                   do inode = 1,pnode
                      ipoin = lnods(inode,ielem)
                      elcod(1:ndime,inode)=coord(1:ndime,ipoin)
                      elunk(1:ndime,inode)=unkno(1:ndime,ipoin)
                   end do
                   !
                   ! Loop over Gauss points (which are nodes)
                   !
                   do inode = 1,knode
                      ipoin = lnods(inode,ielem)
                      call elmder(&
                           pnode,ndime,elmar(pelty)%deric(1,1,inode),&
                           elcod,gpcar,gpdet,xjacm,xjaci)
                      gpvol = elmar(pelty)%weigc(inode)*gpdet
                      !
                      ! Velocity strain rates
                      !                
                      elgra(1,1) = 0.0_rp
                      elgra(1,2) = 0.0_rp 
                      elgra(2,1) = 0.0_rp 
                      elgra(2,2) = 0.0_rp
                      do jnode = 1,pnode
                         elgra(1,1) = elgra(1,1) + gpcar(1,jnode) * elunk(1,jnode)
                         elgra(1,2) = elgra(1,2) + gpcar(1,jnode) * elunk(2,jnode)
                         elgra(2,1) = elgra(2,1) + gpcar(2,jnode) * elunk(1,jnode)
                         elgra(2,2) = elgra(2,2) + gpcar(2,jnode) * elunk(2,jnode)
                      end do
                      !
                      ! Assembly
                      !
                      xfact = gpvol 
                      S11   = 0.5_rp * ( elgra(1,1) + elgra(1,1) )
                      S22   = 0.5_rp * ( elgra(2,2) + elgra(2,2) ) 
                      S12   = 0.5_rp * ( elgra(1,2) + elgra(2,1) ) 
                      grunk(ipoin) = grunk(ipoin) + xfact * ( &
                           & + S11*S11 + S12*S12              &
                           & + S12*S12 + S22*S22            )
                   end do
                end if
             end do

          end if

       end if
       !
       ! Parall and periodicity
       !
       Call rhsmod(1_ip,grunk) 
       !
       ! Solve diagonal system
       !
       if( kfl_grpro == 0 ) then
          do ipoin = 1,npoin
             grunk(ipoin) = grunk(ipoin) / vmass(ipoin)
          end do
       else
          do ipoin = 1,npoin
             grunk(ipoin) = grunk(ipoin) / vmasc(ipoin)
          end do
       end if
    end if

  end subroutine projec_norm_symmetric_gradient_vector

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    12/09/2014
  !> @brief   Conserve mass
  !> @details Given a velocity vector u*, find u so that mass is conserved.
  !>          Solve the following constrained system:
  !>          \verbatim
  !>           +-
  !>           | Minimize int_S | u - u* |^2 dS
  !>           | Under    f(u) = r
  !>           +- 
  !>                        ------
  !>           \partial W = S U S'
  !>
  !>                  S'
  !>              +--------+
  !>              |        |         
  !>           S' |   W    | S        
  !>              |        |         
  !>              +--------+
  !>                  S'
  !>
  !>           where f(u) = int_S(u.n)ds and 
  !>           Total mass: r = -int_S'(u.n)ds 
  !>           Local mass: r =  0
  !>
  !>           In algebraic sense:
  !>           +-
  !>           | Minimize int_S | Nu - Nu* |^2 dS
  !>           | Under    R^t.u = r
  !>           +- 
  !>           which solution is:
  !>           u = u* + (M^-1.R) (R^t.M^-1.R)^-1(r-R^tu*)
  !>           Note that:
  !>           - (r-R^tu*) is the constraint such that u=U* if it is fulfilled
  !>           - (R^t.M^-1.R)^-1 is a scalar
  !>          \endverbatim
  !>
  !----------------------------------------------------------------------
  
  subroutine projec_mass_conservation(xarra,lboun,lpoin,what)
    real(rp),     intent(inout)                   :: xarra(ndime,*)
    logical(lg),  intent(in)                      :: lboun(:)
    logical(lg),  intent(in),   pointer, optional :: lpoin(:)
    character(*), intent(in),            optional :: what
    integer(ip)                                   :: iboun,pblty,pnodb,pgaub,igaub
    integer(ip)                                   :: inodb,ipoin,idime,ndim1
    real(rp)                                      :: baloc(ndime,ndime),gbsur,gbdet
    real(rp)                                      :: bocod(ndime,mnodb)
    real(rp)                                      :: elcod(ndime,mnode)
    real(rp)                                      :: rr,ss
    real(rp),                   pointer           :: restr(:,:)
    real(rp),                   pointer           :: massb(:)
    real(rp),                   pointer           :: zarra(:,:)
    logical(lg),                pointer           :: kpoin(:)
    logical(lg)                                   :: total_mass

    total_mass = .false.
    if( present(what) ) then
       if(      trim(what) == 'LOCAL MASS' ) then
          total_mass = .false.
       else if( trim(what) == 'TOTAL MASS' .or. trim(what) == 'GLOBAL MASS' ) then
          total_mass = .true. 
       end if
    end if

    nullify( restr )
    nullify( massb )
    nullify( zarra )
    nullify( kpoin )

    if( IMASTER ) then

       if( .not. present(lpoin) ) then
          call memory_alloca_min(kpoin)
       end if
       call memory_alloca_min(restr)
       call memory_alloca_min(massb)
       call memory_alloca_min(zarra)

    else if( INOTMASTER ) then

       if( present(lpoin) ) then
          kpoin => lpoin
       else
          allocate( kpoin(npoin) ) 
          do ipoin = 1,npoin
             kpoin(ipoin) = .false.
          end do
          do iboun = 1,nboun
             if( lboun(iboun) ) then
                pblty = abs(ltypb(iboun))
                pnodb = nnode(pblty)
                do inodb = 1,pnodb
                   ipoin = lnodb(inodb,iboun)
                   kpoin(ipoin) = .true.
                end do
             end if
          end do
       end if

       ndim1 = ndime + 1
       allocate( restr(ndime,npoin) ) ! R      = restriction
       allocate( massb(npoin)       ) ! M      = mass
       allocate( zarra(ndime,npoin) ) ! M^-1.R = tmp array
       do ipoin = 1,npoin
          restr(1:ndime,ipoin) = 0.0_rp
          zarra(1:ndime,ipoin) = 0.0_rp
          massb(ipoin)         = 0.0_rp
       end do

       rr = 0.0_rp 

       do iboun = 1,nboun

          if( lboun(iboun) .or. total_mass ) then

             pblty = abs(ltypb(iboun))
             pnodb = nnode(pblty)
             pgaub = ngaus(pblty)
             ielem = lboel(pnodb+1,iboun)
             pelty = ltype(ielem)
             pnode = nnode(pelty)
             do inodb = 1,pnodb
                ipoin = lnodb(inodb,iboun)
                bocod(1:ndime,inodb) = coord(1:ndime,ipoin)
             end do
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                elcod(1:ndime,inode) = coord(1:ndime,ipoin)     
             end do

             do igaub = 1,pgaub
                call bouder(&
                     pnodb,ndime,ndimb,elmar(pblty) % deriv(1,1,igaub),&
                     bocod,baloc,gbdet)
                call chenor(pnode,baloc,bocod,elcod)                      ! Check normal
                gbsur = elmar(pblty) % weigp(igaub) * gbdet              
                do inodb = 1,pnodb
                   ipoin = lnodb(inodb,iboun)
                   restr(1:ndime,ipoin) = restr(1:ndime,ipoin) + gbsur * elmar(pblty) % shape(inodb,igaub) * baloc(1:ndime,ndime) ! Restriction R
                   massb(ipoin)         = massb(ipoin)         + gbsur * elmar(pblty) % shape(inodb,igaub)                        ! Mass matrix M
                end do
                if( total_mass ) then
                   do inodb = 1,pnodb
                      ipoin = lnodb(inodb,iboun)
                      do idime = 1,ndime
                         rr = rr + gbsur * elmar(pblty) % shape(inodb,igaub) * baloc(idime,ndime) * xarra(idime,ipoin)
                      end do
                   end do
                end if
             end do

          end if

       end do
       ! 
       ! Parallel exchange and periodicity
       !        
       call PAR_INTERFACE_NODE_EXCHANGE(restr,'SUM','IN MY CODE')
       call PAR_INTERFACE_NODE_EXCHANGE(massb,'SUM','IN MY CODE')
       !call rhsmod(ndime,restr)
       !call rhsmod(1_ip, massb)
       !
       ! Mass matrix M^-1 and zz = M^-1.R
       !
       ss = 0.0_rp
       do ipoin = 1,npoin
          if( kpoin(ipoin) ) then
             massb(ipoin)         = 1.0_rp / massb(ipoin)
             zarra(1:ndime,ipoin) = restr(1:ndime,ipoin) * massb(ipoin)
          end if
       end do 
    end if
    !
    ! rr = r - R^t.u
    ! ss = R^t.M^-1.R
    !
    call PAR_SCALAR_PRODUCT(ndime,rr,restr,xarra,'IN MY CODE')
    call PAR_SCALAR_PRODUCT(ndime,ss,restr,zarra,'IN MY CODE')
    !call prodxy(ndime,npoin,restr,xarra,rr)
    !call prodxy(ndime,npoin,restr,zarra,ss)

    !if( INOTSLAVE ) print*,'mass before=',rr,ss

    if( INOTMASTER .and. ss /= 0.0_rp ) then
       !
       ! rr <= ( R^t.M^-1.R )^-1 ( r - R^t.u )
       !
       rr = - rr / ss
       !
       ! Actualize solution
       !
       do ipoin = 1,npoin
          if( kpoin(ipoin) ) then
             xarra(1:ndime,ipoin) = xarra(1:ndime,ipoin) + zarra(1:ndime,ipoin) * rr 
          end if
       end do

    end if

    !call PAR_SCALAR_PRODUCT(ndime,rr,restr,xarra,'IN MY CODE')
    !if( INOTSLAVE ) print*,'mass after=',rr

    if( .not. present(lpoin) ) then
       if( associated(kpoin) ) deallocate( kpoin )
    end if
    if( associated(restr) ) deallocate( restr )
    if( associated(massb) ) deallocate( massb )
    if( associated(zarra) ) deallocate( zarra )

  end subroutine projec_mass_conservation

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    09/12/2015
  !> @brief   Boundary projection
  !> @details Project an element type array on the nodes
  !>
  !----------------------------------------------------------------------
  
  subroutine projec_elements_to_nodes_r3p(unkno,xvalu)

    implicit none
    type(r3p),    pointer, intent(in)  :: unkno(:)
    real(rp),     pointer, intent(out) :: xvalu(:) 
    integer(ip)                        :: ipoin,idime,inode,ielem,igaus
    integer(ip)                        :: pnode,pelty,pgaus,ilaws,imate
    integer(ip)                        :: jnode,knode
    real(rp)                           :: detjm,gpvol,gpcar(ndime,mnode)
    real(rp)                           :: elcod(ndime,mnode)
    real(rp)                           :: xjaci(9),xjacm(9),xfact
    real(rp)                           :: elunk(mnode)

    if( INOTMASTER ) then
       !
       ! Initialization
       !
       do ipoin = 1,npoin
          xvalu(ipoin) = 0.0_rp
       end do
       !
       ! Loop over elements
       !
       elements: do ielem = 1,nelem
          pelty = ltype(ielem)

          if( pelty > 0 ) then
             pnode = nnode(pelty)
             pgaus = ngaus(pelty)
             !
             ! Gather vectors
             !
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                elcod(1:ndime,inode) = coord(1:ndime,ipoin)
                elunk(inode) = 0.0_rp
             end do
             !
             ! Loop over Gauss points 
             !
             gauss_points: do igaus = 1,pgaus
                call elmder(&
                     pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&
                     elcod,gpcar,detjm,xjacm,xjaci)
                gpvol = elmar(pelty) % weigp(igaus) * detjm
                if( kfl_naxis == 1 ) then
                   call runend('MOD_GRADIE: NOT CODED')
                end if
                ! 
                ! Extension
                !
                if( lelch(ielem) == ELEXT ) then
                   knode = 1
                else
                   knode = pnode
                end if
                !
                ! Assemble
                !
                do inode = 1,knode
                   xfact        = gpvol * elmar(pelty) % shape(inode,igaus)
                   elunk(inode) = elunk(inode) + xfact * unkno(ielem) % a(1,igaus,1)
                end do

             end do gauss_points
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                xvalu(ipoin) = xvalu(ipoin) + elunk(inode)
             end do

          end if
       end do elements
       !
       ! Parallelization
       !
       call rhsmod(1_ip,xvalu) 
       !
       ! Solve system
       !     
       do ipoin = 1,npoin
          xvalu(ipoin) = xvalu(ipoin) / vmass(ipoin)
       end do
       
    end if

  end subroutine projec_elements_to_nodes_r3p

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    09/12/2015
  !> @brief   Boundary projection
  !> @details Project an element type array on the nodes
  !>
  !----------------------------------------------------------------------
  
  subroutine projec_elements_to_nodes_rp(unkno,xvalu)

    implicit none
    real(rp),     pointer, intent(in)  :: unkno(:,:)
    real(rp),     pointer, intent(out) :: xvalu(:,:) 
    integer(ip)                        :: ipoin,idime,inode,ielem,igaus
    integer(ip)                        :: pnode,pelty,pgaus,ilaws,imate
    integer(ip)                        :: jnode,knode,ndofn
    real(rp)                           :: detjm,gpvol,gpcar(ndime,mnode)
    real(rp)                           :: elcod(ndime,mnode)
    real(rp)                           :: xjaci(9),xjacm(9),xfact
    real(rp),     pointer              :: elunk(:,:)

    if( INOTMASTER ) then
       !
       ! Initialization
       !
       ndofn = size(unkno,1)
       if( size(unkno,1) /= size(xvalu,1) ) call runend('PROJEC_ELEMENTS_TO_NODES: WRONG SIZE') 
       allocate(elunk(ndofn,mnode))
       do ipoin = 1,npoin
          xvalu(1:ndofn,ipoin) = 0.0_rp
       end do
       !
       ! Loop over elements
       !
       elements: do ielem = 1,nelem
          pelty = ltype(ielem)

          if( pelty > 0 ) then
             pnode = nnode(pelty)
             pgaus = ngaus(pelty)
             !
             ! Gather vectors
             !
             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                elcod(1:ndime,inode) = coord(1:ndime,ipoin)
                elunk(1:ndofn,inode) = 0.0_rp
             end do
             !
             ! Loop over Gauss points 
             !
             gauss_points: do igaus = 1,pgaus
                call elmder(&
                     pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&
                     elcod,gpcar,detjm,xjacm,xjaci)
                gpvol = elmar(pelty) % weigp(igaus) * detjm
                if( kfl_naxis == 1 ) then
                   call runend('MOD_GRADIE: NOT CODED')
                end if
                ! 
                ! Extension
                !
                if( lelch(ielem) == ELEXT ) then
                   knode = 1
                else
                   knode = pnode
                end if
                !
                ! Assemble
                !
                do inode = 1,knode
                   xfact                = gpvol * elmar(pelty) % shape(inode,igaus)
                   elunk(1:ndofn,inode) = elunk(1:ndofn,inode) + xfact * unkno(1:ndofn,ielem)
                end do

             end do gauss_points

             do inode = 1,pnode
                ipoin = lnods(inode,ielem)
                xvalu(1:ndofn,ipoin) = xvalu(1:ndofn,ipoin) + elunk(1:ndofn,inode)
             end do

          end if
       end do elements
       !
       ! Parallelization
       !
       call rhsmod(ndofn,xvalu) 
       !
       ! Solve system
       !     
       do ipoin = 1,npoin
          xvalu(1:ndofn,ipoin) = xvalu(1:ndofn,ipoin) / vmass(ipoin)
       end do
       deallocate(elunk)

    end if

  end subroutine projec_elements_to_nodes_rp

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    09/12/2015
  !> @brief   Boundary projection
  !> @details Project an boundary array on the nodes
  !>
  !----------------------------------------------------------------------
  
  subroutine projec_boundaries_to_nodes(xarra_in,meshe_in,elmar_in,xarra_out,boundary_mask)
    real(rp),        intent(in)            :: xarra_in(*)       !< Value on boundaries
    type(mesh_type), intent(in)            :: meshe_in          !< Mesh structure
    type(elm),       intent(in)            :: elmar_in(*)       !< Integration structure
    real(rp),        intent(out)           :: xarra_out(*)      !< Projected value
    logical(lg),     intent(in), optional  :: boundary_mask(*)
    integer(ip)                            :: iboun,pblty,pnodb
    integer(ip)                            :: pgaub,igaub
    integer(ip)                            :: inodb,ipoin,idime
    real(rp)                               :: baloc(ndime,ndime)
    real(rp)                               :: gbsur,gbdet,xvalu,epsil
    real(rp)                               :: bocod(ndime,meshe_in % mnode)
    real(rp)                               :: elmas(27)
    real(rp)                               :: elxar(27)
    logical(lg)                            :: compute_iboun
    real(rp),                   pointer    :: massb(:)

    if( INOTMASTER ) then

       nullify( massb )
       allocate( massb(meshe_in % npoin) )
       epsil = epsilon(1.0_rp)
       do ipoin = 1,meshe_in % npoin
          xarra_out(ipoin) = 0.0_rp
          massb(ipoin)     = 0.0_rp
       end do

       do iboun = 1,meshe_in % nboun

          if( present(boundary_mask) ) then
             compute_iboun = boundary_mask(iboun)
          else
             compute_iboun = .true.
          end if

          if( compute_iboun ) then

             pblty = abs(meshe_in % ltypb(iboun))
             pgaub = elmar_in(pblty) % pgaus
             pnodb = meshe_in % lnnob(iboun)
             xvalu = xarra_in(iboun)

             do inodb = 1,pnodb
                ipoin = meshe_in % lnodb(inodb,iboun)
                bocod(1:ndime,inodb) = meshe_in % coord(1:ndime,ipoin)             
             end do
             elmas(1:pnodb) = 0.0_rp
             elxar(1:pnodb) = 0.0_rp

             do igaub = 1,pgaub
                call bouder(&
                     pnodb,ndime,ndimb,elmar_in(pblty) % deriv(1,1,igaub),&
                     bocod,baloc,gbdet)
                gbsur = elmar_in(pblty) % weigp(igaub) * gbdet              
                do inodb = 1,pnodb
                   elxar(inodb) = elxar(inodb) + gbsur * elmar_in(pblty) % shape(inodb,igaub) * xvalu
                   elmas(inodb) = elmas(inodb) + gbsur * elmar_in(pblty) % shape(inodb,igaub) 
                end do
             end do

             do inodb = 1,pnodb
                ipoin            = meshe_in % lnodb(inodb,iboun)
                xarra_out(ipoin) = xarra_out(ipoin) + elxar(inodb)
                massb(ipoin)     = massb(ipoin)     + elmas(inodb)
             end do
          end if
       end do
       ! 
       ! Parallel exchange and periodicity
       !        
       call PAR_INTERFACE_NODE_EXCHANGE(1_ip,xarra_out,'SUM','IN MY CODE')
       call PAR_INTERFACE_NODE_EXCHANGE(1_ip,massb    ,'SUM','IN MY CODE')
       !
       ! Solve diagonal system
       !
       do ipoin = 1,meshe_in % npoin
          if( massb(ipoin) > 0.0_rp) then
             xarra_out(ipoin) = xarra_out(ipoin) / massb(ipoin)
          end if
       end do
       deallocate(massb)

    end if

  end subroutine projec_boundaries_to_nodes

end module mod_projec
!> @}
