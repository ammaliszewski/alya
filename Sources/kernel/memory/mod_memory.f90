!------------------------------------------------------------------------
!> @addtogroup MemoryToolBox
!> @{
!> @name    ToolBox for memory management
!> @file    mod_memory.f90
!> @author  Guillaume Houzeaux
!> @brief   ToolBox for memory management
!> @details ToolBox for memory management: allocate, deallocate
!> @{
!------------------------------------------------------------------------
module mod_memory

  use def_kintyp, only : ip,rp,lg,i1p,i2p,i3p,r1p,r2p,r3p,i1pi1p,i1pp
  implicit none
  private 

  integer(ip), parameter :: kfl_debug = 0    ! Debugging flag
  integer(8)             :: lbytm            ! Allocated bytes
  integer(8)             :: mem_curre        ! Current memory allocation in bytes
  integer(ip), parameter :: kfl_paral = 0    ! Select the one in def_master instead to debug
  integer(ip)            :: lun_memor        ! Memory file unit
  !
  ! MEMORY_ALLOC: allocate
  !
  interface memory_alloca
     module procedure memory_allrp1   , memory_allrp2   , memory_allrp3   , & ! Pointer:     Real(rp)(:), Real(rp)(:,:), Real(rp)(:,:,:)
          &           memory_allrp1_8 ,                                     & !
          &           memory_alli41   , memory_alli42   , memory_alli43   , & ! Pointer:     Inte(4) (:), Inte(4) (:,:), Inte(4) (:,:,:)
          &           memory_alli418  ,                                     & ! Pointer:     Inte(4) (:) with special argument
          &           memory_alli81   , memory_alli82   , memory_alli83   , & ! Pointer:     Inte(8) (:), Inte(8) (:,:), Inte(8) (:,:,:)
          &           memory_alli1p_1 , memory_alli2p_1 , memory_alli3p_1 , &
          &           memory_alli1p_3 ,                                     &
          &           memory_allr1p_1 , memory_allr2p_1 , memory_allr3p_1 , &
          &                             memory_allr2p_2 ,                   &
          &           memory_alllg1   ,                                     &
          &           memory_alli1pp_1,                                     & 
          &           memory_alli1pi1p,                                     &
          &           memory_allcha_4 , memory_allcha_8,                    &
          &           memory_allxp1,    memory_allxp2                         ! Pointer:     complex(rp)(:), complex(rp)(:,:) 

  end interface memory_alloca

  interface memory_alloca_min
     module procedure memory_alloca_min_rp1,memory_alloca_min_rp2,&
          &           memory_alloca_min_rp3,&
          &           memory_alloca_min_ip1,memory_alloca_min_ip2,&
          &           memory_alloca_min_ip3,&
          &           memory_alloca_min_lg1,memory_alloca_min_lg2,&
          &           memory_alloca_min_lg3
  end interface memory_alloca_min

  interface memory_deallo
     module procedure memory_dearp1   , memory_dearp2   , memory_dearp3   , &
          &           memory_deai41   , memory_deai42   , memory_deai43   , &
          &           memory_deai81   , memory_deai82   , memory_deai83   , &
          &           memory_deai1p_1 , memory_deai2p_1 , memory_deai3p_1 , &
          &           memory_dear1p_1 , memory_dear2p_1 , memory_dear3p_1 , &
          &                             memory_dear2p_2 ,                   &
          &           memory_deai1p_3 ,                                     &
          &           memory_deai1pp_1,                                     & 
          &           memory_dealg1,                                        &
          &           memory_deacha,                                        &
          &           memory_deaxp1   , memory_deaxp2 
  end interface memory_deallo

  interface memory_copy
     module procedure memory_cpyi41   , memory_cpyi42   ,                   & ! Inte(4)   (:),  Inte(4) (:,:)
          &           memory_cpyi81   , memory_cpyi82   ,                   & ! Inte(8)   (:),  Inte(8) (:,:)
          &           memory_cpyi1p_1 ,                                     & ! Type(i1p) (:)
          &           memory_cpyrp1   , memory_cpyrp2                         ! Real(rp)  (:),  Real(rp)(:,:)
  end interface memory_copy

  interface memory_resize
     module procedure memory_newi41   , memory_newi42   ,                   & ! Inte(4)   (:),  Inte(4) (:,:)
          &           memory_newi81   , memory_newi82   ,                   & ! Inte(8)   (:),  Inte(8) (:,:)
          &           memory_newi1p_1                                         ! Type(i1p) (:)  
  end interface memory_resize

  interface memory_renumber
     module procedure memory_renrp1   , memory_renrp2   ,                   & ! Real(rp)  (:),  Real(rp)(:,:)
          &           memory_reni41   , memory_reni81                         ! Inte(4)   (:),  Inte(8) (:)
  end interface memory_renumber

  public :: memory_alloca
  public :: memory_alloca_min
  public :: memory_deallo
  public :: memory_copy
  public :: memory_renumber
  public :: memory_resize
  public :: lbytm             ! Allocated bytes
  public :: mem_curre         ! Current memory allocation in bytes
  public :: lun_memor         ! Output unit

contains

  subroutine memory_allrp1(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Real(rp)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(in)            :: ndim1
    real(rp),     intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(4)                          :: istat
    integer(4)                          :: idim1
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if(kfl_debug == 1_ip) then
          write(kfl_paral+5000,*) 'trim(vanam),trim(vacal) ',trim(vanam),trim(vacal)
          flush(kfl_paral+5000)
       end if

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(kind(varia),8)
          if( lzero ) then
             do idim1 = 1,ndim1
                varia(idim1) = 0.0_rp
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'real')

    else

       nullify(varia)

    end if

  end subroutine memory_allrp1

!!$  subroutine memory_allrp1a(memor,vanam,vacal,varia,ndim1,wzero)
!!$    !
!!$    ! Real(rp)(:)
!!$    !
!!$    implicit none
!!$    character(*), intent(in)               :: vanam
!!$    character(*), intent(in)               :: vacal
!!$    integer(8),   intent(inout)            :: memor(2) 
!!$    integer(4),   intent(in)               :: ndim1
!!$    real(rp),     intent(out), allocatable :: varia(:)
!!$    character(*), intent(in),  optional    :: wzero
!!$    integer(4)                             :: istat
!!$    integer(4)                             :: idim1
!!$    logical(lg)                            :: lzero
!!$
!!$    if( ndim1 > 0 ) then
!!$ 
!!$       if( allocated(varia) ) then
!!$          write(*,*) 'ARRAY ALREADY ALLOCATED: ',kfl_paral,trim(vanam),trim(vacal)
!!$          call runend('MOD_MEMORY: ARRAY ALREADY ALLOCATED') 
!!$       end if
!!$
!!$       allocate( varia(ndim1) , stat = istat )
!!$       lzero = .true.
!!$       if(present(wzero)) then
!!$          if( wzero == 'DO_NOT_INITIALIZE') then
!!$             lzero = .false.
!!$          end if
!!$       end if
!!$
!!$       if( istat == 0 ) then
!!$          lbytm = int(ndim1,8)*int(kind(varia),8)
!!$          if( lzero ) then
!!$             do idim1 = 1,ndim1
!!$                varia(idim1) = 0.0_rp
!!$             end do
!!$          end if
!!$       else
!!$          call memory_error(0_ip,vanam,vacal,istat)
!!$       end if
!!$
!!$       call memory_info(memor,vanam,vacal,'real')
!!$
!!$    end if
!!$
!!$  end subroutine memory_allrp1a

  subroutine memory_allrp1_8(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Real(rp)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(in)            :: ndim1
    real(rp),     intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(4)                          :: istat
    integer(8)                          :: idim1
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if(kfl_debug == 1_ip) then
          write(kfl_paral+5000,*) 'trim(vanam),trim(vacal) ',trim(vanam),trim(vacal)
          flush(kfl_paral+5000)
       end if

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(kind(varia),8)
          if( lzero ) then
             do idim1 = 1,ndim1
                varia(idim1) = 0.0_rp
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'real')

    else

       nullify(varia)

    end if

  end subroutine memory_allrp1_8

  subroutine memory_allrp2(memor,vanam,vacal,varia,ndim1,ndim2,wzero,lboun1,lboun2)
    !
    ! Real(rp)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    integer(ip),  intent(in)            :: ndim2
    real(rp),     intent(out), pointer  :: varia(:,:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun1
    integer(ip),  intent(in),  optional :: lboun2
    integer(4)                          :: istat
    integer(8)                          :: idim1
    integer(8)                          :: idim2
    logical(lg)                         :: lzero

    if( ndim1*ndim2 > 0 ) then

       if(kfl_debug == 1_ip) then
          write(kfl_paral+5000,*) 'trim(vanam),trim(vacal) ',trim(vanam),trim(vacal)
          flush(kfl_paral+5000)
       end if

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) .and. present(lboun2) ) then
          allocate( varia(lboun1:lboun1+ndim1-1,lboun2:lboun2+ndim2-1) , stat = istat )
       else
          allocate( varia(ndim1,ndim2) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(size(varia,1),8)*int(size(varia,2),8)*int(kind(varia),8)
          if( lzero ) then
             do idim2 = lbound(varia,2),ubound(varia,2)
                do idim1 = lbound(varia,1),ubound(varia,1)
                   varia(idim1,idim2) = 0_ip
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'real')

    else

       nullify(varia)

    end if

  end subroutine memory_allrp2

!!$  subroutine memory_allrp2a(memor,vanam,vacal,varia,ndim1,ndim2,wzero)
!!$    !
!!$    ! Real(rp)(:,:)
!!$    !
!!$    implicit none
!!$    character(*), intent(in)               :: vanam
!!$    character(*), intent(in)               :: vacal
!!$    integer(8),   intent(inout)            :: memor(2) 
!!$    integer(ip),  intent(in)               :: ndim1
!!$    integer(ip),  intent(in)               :: ndim2
!!$    real(rp),     intent(out), allocatable :: varia(:,:)
!!$    character(*), intent(in),  optional    :: wzero
!!$    integer(4)                             :: istat
!!$    integer(ip)                            :: idim1
!!$    integer(ip)                            :: idim2
!!$    logical(lg)                            :: lzero
!!$
!!$    if( ndim1*ndim2 > 0 ) then
!!$
!!$       if( allocated(varia) ) then
!!$          write(*,*) 'ARRAY ALREADY ALLOCATED: ',kfl_paral,trim(vanam),trim(vacal)
!!$          call runend('MOD_MEMORY: ARRAY ALREADY ALLOCATED') 
!!$       end if
!!$
!!$       allocate( varia(ndim1,ndim2) , stat = istat )
!!$       lzero = .true.
!!$       if(present(wzero)) then
!!$          if( wzero == 'DO_NOT_INITIALIZE') then
!!$             lzero = .false.
!!$          end if
!!$       end if
!!$
!!$       if( istat == 0 ) then
!!$          lbytm = int(size(varia,1),8)*int(size(varia,2),8)*int(kind(varia),8)
!!$          if( lzero ) then
!!$             do idim2 = lbound(varia,2),ubound(varia,2)
!!$                do idim1 = lbound(varia,1),ubound(varia,1)
!!$                   varia(idim1,idim2) = 0_ip
!!$                end do
!!$             end do
!!$          end if
!!$       else
!!$          call memory_error(0_ip,vanam,vacal,istat)
!!$       end if
!!$
!!$       call memory_info(memor,vanam,vacal,'real')
!!$
!!$    end if
!!$
!!$  end subroutine memory_allrp2a

  subroutine memory_allrp3(memor,vanam,vacal,varia,ndim1,ndim2,ndim3,wzero,lboun1,lboun2,lboun3)

    !
    ! Real(rp)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    integer(ip),  intent(in)            :: ndim2
    integer(ip),  intent(in)            :: ndim3
    real(rp),     intent(out), pointer  :: varia(:,:,:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun1
    integer(ip),  intent(in),  optional :: lboun2
    integer(ip),  intent(in),  optional :: lboun3
    integer(4)                          :: istat
    integer(8)                          :: idim1
    integer(8)                          :: idim2
    integer(8)                          :: idim3
    logical(lg)                         :: lzero

    if( ndim1*ndim2*ndim3 > 0 ) then

       if(kfl_debug == 1_ip) then
          write(kfl_paral+5000,*) 'trim(vanam),trim(vacal) ',trim(vanam),trim(vacal)
          flush(kfl_paral+5000)
       end if

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) .and. present(lboun2) .and. present(lboun3) ) then
          allocate( varia(lboun1:lboun1+ndim1-1,lboun2:lboun2+ndim2-1,lboun3:lboun3+ndim3-1) , stat = istat )
       else
          allocate( varia(ndim1,ndim2,ndim3) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(ndim2,8)*int(ndim3,8)*int(kind(varia),8)
          if( lzero ) then
             do idim3 = lbound(varia,3),ubound(varia,3)
                do idim2 = lbound(varia,2),ubound(varia,2)
                   do idim1 = lbound(varia,1),ubound(varia,1)
                      varia(idim1,idim2,idim3) = 0_8
                   end do
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'real')

    else

       nullify(varia)

    end if

  end subroutine memory_allrp3

  subroutine memory_alli41(memor,vanam,vacal,varia,ndim1,wzero,lboun)
    !
    ! Integer(4)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(in)            :: ndim1
    integer(4),   intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(4),   intent(in),  optional :: lboun
    integer(4)                          :: istat
    integer(8)                          :: idim1
    logical(lg)                         :: lzero
    integer(4)                          :: xvalu

    if( ndim1 > 0 ) then

       if( present(wzero) ) then
          if( wzero == 'REALLOCATE') then
             call memory_deallo(memor,vanam,vacal,varia)
          end if
       end if

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun) ) then
          allocate( varia(lboun:lboun+ndim1-1) , stat = istat )
       else
          allocate( varia(ndim1) , stat = istat )
       end if
       lzero = .true.
       xvalu = 0_4
       if( present(wzero) ) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          else if( wzero == 'HUGE') then
             xvalu = huge(0_4)
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(kind(varia),8)
          if( lzero ) then
             do idim1 = lbound(varia,1),ubound(varia,1)
                varia(idim1) = xvalu
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'integer(4)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli41

!!$  subroutine memory_alli41a(memor,vanam,vacal,varia,ndim1,wzero)
!!$    !
!!$    ! Integer(4)(:)
!!$    !
!!$    implicit none
!!$    character(*), intent(in)               :: vanam
!!$    character(*), intent(in)               :: vacal
!!$    integer(8),   intent(inout)            :: memor(2) 
!!$    integer(4),   intent(in)               :: ndim1
!!$    integer(4),   intent(out), allocatable :: varia(:)
!!$    character(*), intent(in),  optional    :: wzero
!!$    integer(4)                             :: istat
!!$    integer(8)                             :: idim1
!!$    logical(lg)                            :: lzero
!!$
!!$    if( ndim1 > 0 ) then
!!$
!!$       if( present(wzero) ) then
!!$          if( wzero == 'REALLOCATE') then
!!$             call memory_deallo(memor,vanam,vacal,varia)
!!$          end if
!!$       end if
!!$
!!$       if( allocated(varia) ) then
!!$          write(*,*) 'ARRAY ALREADY ALLOCATED: ',kfl_paral,trim(vanam),trim(vacal)
!!$          call runend('MOD_MEMORY: ARRAY ALREADY ALLOCATED') 
!!$       end if
!!$
!!$       allocate( varia(ndim1) , stat = istat )
!!$       lzero = .true.
!!$       if( present(wzero) ) then
!!$          if( wzero == 'DO_NOT_INITIALIZE') then
!!$             lzero = .false.
!!$          end if
!!$       end if
!!$
!!$       if( istat == 0 ) then
!!$          lbytm = int(ndim1,8)*int(kind(varia),8)
!!$          if( lzero ) then
!!$             do idim1 = lbound(varia,1),ubound(varia,1)
!!$                varia(idim1) = 0_ip
!!$             end do
!!$          end if
!!$       else
!!$          call memory_error(0_ip,vanam,vacal,istat)
!!$       end if
!!$
!!$       call memory_info(memor,vanam,vacal,'integer(4)')
!!$
!!$    end if
!!$
!!$  end subroutine memory_alli41a

  subroutine memory_alli418(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Integer(4)(:): wrt memory_alli41, the dimension is integer(8)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(in)            :: ndim1
    integer(4),   intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(4)                          :: istat
    integer(8)                          :: idim1
    logical(lg)                         :: lzero
    integer(4)                          :: xvalu

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       xvalu = 0_4
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          else if( wzero == 'HUGE') then
             xvalu = huge(0_4)
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(kind(varia),8)
          if( lzero ) then
             do idim1 = 1,ndim1
                varia(idim1) = xvalu
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'integer(4)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli418

  subroutine memory_alli42(memor,vanam,vacal,varia,ndim1,ndim2,wzero,lboun1,lboun2)
    !
    ! Integer(4)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(in)            :: ndim1
    integer(4),   intent(in)            :: ndim2
    integer(4),   intent(out), pointer  :: varia(:,:)
    character(*), intent(in),  optional :: wzero
    integer(4),   intent(in),  optional :: lboun1
    integer(4),   intent(in),  optional :: lboun2
    integer(4)                          :: istat
    integer(4)                          :: idim1
    integer(4)                          :: idim2
    logical(lg)                         :: lzero

    if( ndim1*ndim2 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) .and. present(lboun2) ) then
          allocate( varia(lboun1:lboun1+ndim1-1,lboun2:lboun2+ndim2-1) , stat = istat )
       else
          allocate( varia(ndim1,ndim2) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(ndim2,8)*int(kind(varia),8)
          if( lzero ) then
             do idim2 = lbound(varia,2),ubound(varia,2)
                do idim1 = lbound(varia,1),ubound(varia,1)
                   varia(idim1,idim2) = 0_ip
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'integer(4)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli42

!!$  subroutine memory_alli42a(memor,vanam,vacal,varia,ndim1,ndim2,wzero,lboun1,lboun2)
!!$    !
!!$    ! Integer(4)(:,:)
!!$    !
!!$    implicit none
!!$    character(*), intent(in)               :: vanam
!!$    character(*), intent(in)               :: vacal
!!$    integer(8),   intent(inout)            :: memor(2) 
!!$    integer(4),   intent(in)               :: ndim1
!!$    integer(4),   intent(in)               :: ndim2
!!$    integer(4),   intent(out), allocatable :: varia(:,:)
!!$    character(*), intent(in),  optional    :: wzero
!!$    integer(4),   intent(in),  optional    :: lboun1
!!$    integer(4),   intent(in),  optional    :: lboun2
!!$    integer(4)                             :: istat
!!$    integer(4)                             :: idim1
!!$    integer(4)                             :: idim2
!!$    logical(lg)                            :: lzero
!!$
!!$    if( ndim1*ndim2 > 0 ) then
!!$
!!$       if( allocated(varia) ) then
!!$          write(*,*) 'ARRAY ALREADY ALLOCATED: ',kfl_paral,trim(vanam),trim(vacal)
!!$          call runend('MOD_MEMORY: ARRAY ALREADY ALLOCATED') 
!!$       end if
!!$
!!$       if( present(lboun1) .and. present(lboun2) ) then
!!$          allocate( varia(lboun1:lboun1+ndim1-1,lboun2:lboun2+ndim2-1) , stat = istat )
!!$       else
!!$         allocate( varia(ndim1,ndim2) , stat = istat )
!!$       end if
!!$       lzero = .true.
!!$       if(present(wzero)) then
!!$          if( wzero == 'DO_NOT_INITIALIZE') then
!!$             lzero = .false.
!!$          end if
!!$       end if
!!$
!!$       if( istat == 0 ) then
!!$          lbytm = int(ndim1,8)*int(ndim2,8)*int(kind(varia),8)
!!$          if( lzero ) then
!!$             do idim2 = lbound(varia,2),ubound(varia,2)
!!$                do idim1 = lbound(varia,1),ubound(varia,1)
!!$                   varia(idim1,idim2) = 0_ip
!!$                end do
!!$             end do
!!$          end if
!!$       else
!!$          call memory_error(0_ip,vanam,vacal,istat)
!!$       end if
!!$
!!$       call memory_info(memor,vanam,vacal,'integer(4)')
!!$
!!$    end if
!!$
!!$  end subroutine memory_alli42a

  subroutine memory_alli43(memor,vanam,vacal,varia,ndim1,ndim2,ndim3,wzero,lboun1,lboun2,lboun3)
    !
    ! Integer(4)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(in)            :: ndim1
    integer(4),   intent(in)            :: ndim2
    integer(4),   intent(in)            :: ndim3
    integer(4),   intent(out), pointer  :: varia(:,:,:)
    character(*), intent(in),  optional :: wzero
    integer(4),   intent(in),  optional :: lboun1
    integer(4),   intent(in),  optional :: lboun2
    integer(4),   intent(in) , optional :: lboun3
    integer(4)                          :: istat
    integer(8)                          :: idim1
    integer(8)                          :: idim2
    integer(8)                          :: idim3
    logical(lg)                         :: lzero

    if( ndim1*ndim2*ndim3 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) .and. present(lboun2) .and. present(lboun3) ) then
          allocate( varia(lboun1:lboun1+ndim1-1,lboun2:lboun2+ndim2-1,lboun3:lboun3+ndim3-1) , stat = istat )
       else
          allocate( varia(ndim1,ndim2,ndim3) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(ndim2,8)*int(ndim3,8)*int(kind(varia),8)
          if( lzero ) then
             do idim3 = lbound(varia,3),ubound(varia,3)
                do idim2 = lbound(varia,2),ubound(varia,2)
                   do idim1 = lbound(varia,1),ubound(varia,1)
                      varia(idim1,idim2,idim3) = 0_4
                   end do
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'integer(4)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli43

  subroutine memory_alllg1(memor,vanam,vacal,varia,ndim1,wzero,lboun)
    !
    ! Logical(lg)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    logical(lg),  intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun
    integer(4)                          :: istat
    integer(8)                          :: idim1 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun) ) then
          allocate( varia(lboun:lboun+ndim1-1) , stat = istat )
       else
          allocate( varia(ndim1) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(kind(varia),8)
          if( lzero ) then
             do idim1 = lbound(varia,1),ubound(varia,1)
                varia(idim1) = .false.
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'logical')

    else

       nullify(varia)

    end if

  end subroutine memory_alllg1

  subroutine memory_allr1p_1(memor,vanam,vacal,varia,ndim1,wzero,lboun1)
    !
    ! Type(r1p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    type(r1p),    intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun1
    integer(ip)                         :: idim1 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) ) then
          allocate( varia(lboun1:lboun1+ndim1-1) , stat = istat )
       else
          allocate( varia(ndim1) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ip
          if( lzero ) then
             do idim1 = lbound(varia,1),ubound(varia,1)
                nullify(varia(idim1) % a)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(r1p)')

    else

       nullify(varia)

    end if

  end subroutine memory_allr1p_1

  subroutine memory_allr2p_1(memor,vanam,vacal,varia,ndim1,wzero,lboun1)
    !
    ! Type(r2p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    type(r2p),    intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun1
    integer(ip)                         :: idim1 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) ) then
          allocate( varia(lboun1:lboun1+ndim1-1) , stat = istat )
       else
          allocate( varia(ndim1) , stat = istat )
       end if

       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ip
          if( lzero ) then
             do idim1 = lbound(varia,1),ubound(varia,1)
                nullify(varia(idim1) % a)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(r2p)')

    else

       nullify(varia)

    end if

  end subroutine memory_allr2p_1

  subroutine memory_allr2p_2(memor,vanam,vacal,varia,ndim1,ndim2,wzero)
    !
    ! Type(r2p)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    integer(ip),  intent(in)            :: ndim2
    type(r2p),    intent(out), pointer  :: varia(:,:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1 
    integer(ip)                         :: idim2
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1*ndim2 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1,ndim2) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ndim2*ip
          if( lzero ) then
             do idim2 = 1,ndim2
                do idim1 = 1,ndim1
                   nullify(varia(idim1,idim2) % a)
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(r2p)')

    else

       nullify(varia)

    end if

  end subroutine memory_allr2p_2

  subroutine memory_allr3p_1(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Type(r3p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    type(r3p),    intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ip
          if( lzero ) then
             do idim1 = 1,ndim1
                nullify(varia(idim1) % a)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(r3p)')

    else

       nullify(varia)

    end if

  end subroutine memory_allr3p_1

  subroutine memory_alli1p_3(memor,vanam,vacal,varia,ndim1,ndim2,ndim3,wzero,lboun1,lboun2,lboun3)
    !
    ! Type(i1p)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    integer(ip),  intent(in)            :: ndim2
    integer(ip),  intent(in)            :: ndim3
    type(i1p),    intent(out), pointer  :: varia(:,:,:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun1
    integer(ip),  intent(in),  optional :: lboun2
    integer(ip),  intent(in),  optional :: lboun3
    integer(ip)                         :: idim1 
    integer(ip)                         :: idim2
    integer(ip)                         :: idim3 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1*ndim2*ndim3 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) .and. present(lboun2) .and. present(lboun3) ) then
          allocate( varia(lboun1:lboun1+ndim1-1,lboun2:lboun2+ndim2-1,lboun3:lboun3+ndim3-1) , stat = istat )
       else
          allocate( varia(ndim1,ndim2,ndim3) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ndim2*ndim3*ip
          if( lzero ) then
             do idim3 = lbound(varia,3),ubound(varia,3)
                do idim2 = lbound(varia,2),ubound(varia,2)
                   do idim1 = lbound(varia,1),ubound(varia,1)
                      nullify(varia(idim1,idim2,idim3) % l)
                   end do
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(i1p)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli1p_3

  subroutine memory_alli1p_1(memor,vanam,vacal,varia,ndim1,wzero,lboun)
    !
    ! Type(i1p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    type(i1p),    intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun
    integer(ip)                         :: idim1 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun) ) then
          allocate( varia(lboun:lboun+ndim1-1) , stat = istat )
       else
          allocate( varia(ndim1) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ip
          if( lzero ) then
             do idim1 = lbound(varia,1),ubound(varia,1)
                nullify(varia(idim1) % l)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(i1p)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli1p_1

  subroutine memory_alli1pp_1(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Type(i1p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    type(i1pp),   intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ip
          if( lzero ) then
             do idim1 = 1,ndim1
                varia(idim1) % n = 0
                nullify(varia(idim1) % l)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(i1p)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli1pp_1

  subroutine memory_alli2p_1(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Type(i2p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    type(i2p),    intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ip
          if( lzero ) then
             do idim1 = 1,ndim1
                nullify(varia(idim1) % l)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(i2p)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli2p_1

  subroutine memory_alli3p_1(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Type(i3p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    type(i3p),    intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ip
          if( lzero ) then
             do idim1 = 1,ndim1
                nullify(varia(idim1) % l)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(i3p)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli3p_1

  subroutine memory_alli1pi1p(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Type(i1p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    type(i1pi1p), intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1 
    integer(4)                          :: istat 
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = ndim1*ip
          if( lzero ) then
             do idim1 = 1,ndim1
                nullify(varia(idim1) % l)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'type(i1p)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli1pi1p

  subroutine memory_deai41(memor,vanam,vacal,varia)
    !
    ! Integer(4)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   pointer               :: varia(:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(kind(varia),8)

       deallocate( varia , stat = istat )

       nullify(varia)

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)

       end if
       call memory_info(memor,vanam,vacal,'integer(4)')

    end if

  end subroutine memory_deai41

!!$  subroutine memory_deai41a(memor,vanam,vacal,varia)
!!$    !
!!$    ! Integer(4)(:)
!!$    !
!!$    implicit none
!!$    character(*), intent(in)            :: vanam
!!$    character(*), intent(in)            :: vacal
!!$    integer(8),   intent(inout)         :: memor(2) 
!!$    integer(4),   allocatable           :: varia(:)
!!$    integer(4)                          :: istat
!!$
!!$    if( allocated(varia) ) then
!!$
!!$       lbytm = -int(size(varia,1),8)*int(kind(varia),8)
!!$
!!$       deallocate( varia , stat = istat )
!!$
!!$       if( istat /= 0 ) then
!!$          call memory_error(2_ip,vanam,vacal,istat)
!!$       end if
!!$       call memory_info(memor,vanam,vacal,'integer(4)')
!!$
!!$    end if
!!$
!!$  end subroutine memory_deai41a

  subroutine memory_deai42(memor,vanam,vacal,varia)
    !
    ! Integer(4)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   pointer               :: varia(:,:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(size(varia,2),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'integer(4)')

    end if

  end subroutine memory_deai42

  subroutine memory_deai43(memor,vanam,vacal,varia)
    !
    ! Integer(4)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   pointer               :: varia(:,:,:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(size(varia,2),8)*int(size(varia,3),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'integer(4)')

    end if

  end subroutine memory_deai43

  subroutine memory_dearp1(memor,vanam,vacal,varia)
    !
    ! Real(rp)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    real(rp),     pointer               :: varia(:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'real')

    end if

  end subroutine memory_dearp1

  subroutine memory_dearp2(memor,vanam,vacal,varia)
    !
    ! Real(rp)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    real(rp),     pointer               :: varia(:,:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(size(varia,2),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'real')

    end if

  end subroutine memory_dearp2

  subroutine memory_dearp3(memor,vanam,vacal,varia)
    !
    ! Real(rp)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    real(rp),     pointer               :: varia(:,:,:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(size(varia,2),8)*int(size(varia,3),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'real')
    end if

  end subroutine memory_dearp3

  subroutine memory_dealg1(memor,vanam,vacal,varia)
    !
    ! Logical(lg)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    logical(lg),  pointer               :: varia(:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'real')

    end if

  end subroutine memory_dealg1

  subroutine memory_dear1p_1(memor,vanam,vacal,varia)
    !
    ! Type(r1p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(r1p),    pointer               :: varia(:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1

    if( associated(varia) ) then

       lbytm = 0
       do idim1 = lbound(varia,1),ubound(varia,1)
          if( associated(varia(idim1) % a) ) then
             lbytm = lbytm-int(size(varia(idim1) % a,1),8)*rp
             deallocate( varia(idim1) % a , stat = istat )
          end if
       end do
       lbytm = lbytm-int(size(varia,1),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(r1p)')
    end if

  end subroutine memory_dear1p_1

  subroutine memory_dear2p_2(memor,vanam,vacal,varia)
    !
    ! Type(r2p)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(r2p),    pointer               :: varia(:,:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1,idim2

    if( associated(varia) ) then

       lbytm = 0
       do idim1 = lbound(varia,1),ubound(varia,1)
          do idim2 = lbound(varia,2),ubound(varia,2)
             if( associated(varia(idim1,idim2) % a) ) then
                lbytm = lbytm-int(size(varia(idim1,idim2) % a,1)*size(varia(idim1,idim2) % a,2),8)*rp
                deallocate( varia(idim1,idim2) % a , stat = istat )
             end if
          end do
       end do
       lbytm = lbytm-int(size(varia,1),8)*int(size(varia,2),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(r2p)')
    end if

  end subroutine memory_dear2p_2

  subroutine memory_dear2p_1(memor,vanam,vacal,varia)
    !
    ! Type(r2p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(r2p),    pointer               :: varia(:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1

    if( associated(varia) ) then

       lbytm = 0
       do idim1 = lbound(varia,1),ubound(varia,1)
          if( associated(varia(idim1) % a) ) then
             lbytm = lbytm-int(size(varia(idim1) % a,1),8)*rp
             deallocate( varia(idim1) % a , stat = istat )
          end if
       end do

       lbytm = lbytm-int(size(varia,1),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(r2p)')
    end if

  end subroutine memory_dear2p_1

  subroutine memory_dear3p_3(memor,vanam,vacal,varia)
    !
    ! Type(r3p)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(r3p),    pointer               :: varia(:,:,:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1
    integer(ip)                         :: idim2
    integer(ip)                         :: idim3

    if( associated(varia) ) then

       lbytm = 0
       do idim3 = lbound(varia,3),ubound(varia,3)
          do idim2 = lbound(varia,2),ubound(varia,2)
             do idim1 = lbound(varia,1),ubound(varia,1)
                if( associated(varia(idim1,idim2,idim3) % a) ) then
                   lbytm = lbytm-int(size(varia(idim1,idim2,idim3) % a,1),8)*rp
                   deallocate( varia(idim1,idim2,idim3) % a , stat = istat )
                end if
             end do
          end do
       end do

       lbytm = lbytm-int(size(varia,1),8)*int(size(varia,2),8)*int(size(varia,3),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(r3p)')
    end if

  end subroutine memory_dear3p_3

  subroutine memory_dear3p_1(memor,vanam,vacal,varia)
    !
    ! Type(r3p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(r3p),    pointer               :: varia(:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1

    if( associated(varia) ) then

       lbytm = 0
       do idim1 = lbound(varia,1),ubound(varia,1)
          if( associated(varia(idim1) % a) ) then
             lbytm = lbytm-int(size(varia(idim1) % a,1),8)*ip
             deallocate( varia(idim1) % a , stat = istat )
          end if
       end do
       lbytm = lbytm-int(size(varia,1),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(r3p)')
    end if

  end subroutine memory_dear3p_1

  subroutine memory_deai1p_1(memor,vanam,vacal,varia)
    !
    ! Type(i1p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(i1p),    pointer               :: varia(:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1

    if( associated(varia) ) then

       lbytm = 0
       do idim1 = lbound(varia,1),ubound(varia,1)
          if( associated(varia(idim1) % l) ) then
             lbytm = lbytm-int(size(varia(idim1) % l,1),8)*ip
             deallocate( varia(idim1) % l , stat = istat )
          end if
       end do
       lbytm = lbytm-int(size(varia,1),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(i1p)')

    end if

  end subroutine memory_deai1p_1

  subroutine memory_deai1p_3(memor,vanam,vacal,varia)
    !
    ! Type(i1p)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(i1p),    pointer               :: varia(:,:,:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1
    integer(ip)                         :: idim2
    integer(ip)                         :: idim3

    if( associated(varia) ) then

       lbytm = 0
       do idim3 = lbound(varia,3),ubound(varia,3)
          do idim2 = lbound(varia,2),ubound(varia,2)
             do idim1 = lbound(varia,1),ubound(varia,1)
                if( associated(varia(idim1,idim2,idim3) % l) ) then
                   lbytm = lbytm-int(size(varia(idim1,idim2,idim3) % l,1),8)*ip
                   deallocate( varia(idim1,idim2,idim3) % l , stat = istat )
                end if
             end do
          end do
       end do
       lbytm = lbytm-int(size(varia,1),8)*ip*int(size(varia,2),8)*ip*int(size(varia,3),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(i1p)')

    end if

  end subroutine memory_deai1p_3

  subroutine memory_deai1pp_1(memor,vanam,vacal,varia)
    !
    ! Type(i1pp)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(i1pp),   pointer               :: varia(:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1

    if( associated(varia) ) then

       lbytm = 0
       do idim1 = lbound(varia,1),ubound(varia,1)
          if( associated(varia(idim1) % l) ) then
             lbytm = lbytm-int(size(varia(idim1) % l,1),8)*ip
             deallocate( varia(idim1) % l , stat = istat )
          end if
       end do
       lbytm = lbytm-int(size(varia,1),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(i1p)')

    end if

  end subroutine memory_deai1pp_1

  subroutine memory_deai2p_2(memor,vanam,vacal,varia)
    !
    ! Type(i2p)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(i2p),    pointer               :: varia(:,:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1,idim2

    if( associated(varia) ) then

       lbytm = 0
       do idim1 = 1,size(varia,1)
          do idim2 = 1,size(varia,2)
             if( associated(varia(idim1,idim2) % l) ) then
                lbytm = lbytm-int(size(varia(idim1,idim2) % l,1)*size(varia(idim1,idim2) % l,2),8)*ip
                deallocate( varia(idim1,idim2) % l , stat = istat )
             end if
          end do
       end do

       lbytm = lbytm-int(size(varia,1),8)*int(size(varia,2),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(i2p)')

    end if

  end subroutine memory_deai2p_2

  subroutine memory_deai2p_1(memor,vanam,vacal,varia)
    !
    ! Type(i2p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(i2p),    pointer               :: varia(:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1

    if( associated(varia) ) then

       lbytm = 0
       do idim1 = 1,size(varia,1)
          if( associated(varia(idim1) % l) ) then
             lbytm = lbytm-int(size(varia(idim1) % l,1)*size(varia(idim1) % l,2),8)*ip
             deallocate( varia(idim1) % l , stat = istat )
          end if
       end do

       lbytm = lbytm-int(size(varia,1),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(i2p)')

    end if

  end subroutine memory_deai2p_1

  subroutine memory_deai3p_1(memor,vanam,vacal,varia)
    !
    ! Type(i3p)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(i3p),    pointer               :: varia(:,:,:)
    integer(4)                          :: istat
    integer(ip)                         :: idim1
    integer(ip)                         :: idim2
    integer(ip)                         :: idim3

    if( associated(varia) ) then

       lbytm = 0
       do idim3 = lbound(varia,3),ubound(varia,3)
          do idim2 = lbound(varia,2),ubound(varia,2)
             do idim1 = lbound(varia,1),ubound(varia,1)
                if( associated(varia(idim1,idim2,idim3) % l) ) then
                   lbytm = lbytm-int(size(varia(idim1,idim2,idim3) % l,1),8)*ip
                   deallocate( varia(idim1,idim2,idim3) % l , stat = istat )
                end if
             end do
          end do
       end do

       lbytm = lbytm-int(size(varia,1),8)*int(size(varia,2),8)*int(size(varia,3),8)*ip
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'type(i3p)')

    end if

  end subroutine memory_deai3p_1

  subroutine memory_deaxp1(memor,vanam,vacal,varia)
    !
    ! complex(rp)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    complex(rp),     pointer            :: varia(:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(kind(varia),8)*2
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'real')

    end if

  end subroutine memory_deaxp1

  subroutine memory_deaxp2(memor,vanam,vacal,varia)
    !
    ! complex(rp)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    complex(rp),  pointer               :: varia(:,:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(size(varia,2),8)*int(kind(varia),8)*2
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'real')

    end if

  end subroutine memory_deaxp2

  subroutine memory_alli81(memor,vanam,vacal,varia,ndim1,wzero,lboun)
    !
    ! Integer(8)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(in)            :: ndim1
    integer(8),   intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun
    integer(4)                          :: istat
    integer(8)                          :: idim1
    logical(lg)                         :: lzero
    integer(8)                          :: xvalu

    if( ndim1 > 0 ) then

       if( present(wzero) ) then
          if( wzero == 'REALLOCATE') then
             call memory_deallo(memor,vanam,vacal,varia)
          end if
       end if

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun) ) then
          allocate( varia(lboun:lboun+ndim1-1) , stat = istat )          
       else
          allocate( varia(ndim1) , stat = istat )
       end if
       lzero = .true.
       xvalu = 0_8
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          else if( wzero == 'HUGE') then
             xvalu = huge(0_8)
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(kind(varia),8)
          if( lzero ) then
             do idim1 = lbound(varia,1),ubound(varia,1)
                varia(idim1) = xvalu
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'integer(4)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli81

  subroutine memory_alli82(memor,vanam,vacal,varia,ndim1,ndim2,wzero,lboun1,lboun2)
    !
    ! Integer(8)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(in)            :: ndim1
    integer(8),   intent(in)            :: ndim2
    integer(8),   intent(out), pointer  :: varia(:,:)
    character(*), intent(in),  optional :: wzero
    integer(ip),  intent(in),  optional :: lboun1
    integer(ip),  intent(in),  optional :: lboun2
    integer(4)                          :: istat
    integer(8)                          :: idim1
    integer(8)                          :: idim2
    logical(lg)                         :: lzero

    if( ndim1*ndim2 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) .and. present(lboun2) ) then
          allocate( varia(lboun1:lboun1+ndim1-1,lboun2:lboun2+ndim2-1) , stat = istat )
       else
          allocate( varia(ndim1,ndim2) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(ndim2,8)*int(kind(varia),8)
          if( lzero ) then
             do idim2 = lbound(varia,2),ubound(varia,2)
                do idim1 = lbound(varia,1),ubound(varia,1)
                   varia(idim1,idim2) = 0_ip
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'integer(4)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli82

  subroutine memory_alli83(memor,vanam,vacal,varia,ndim1,ndim2,ndim3,wzero,lboun1,lboun2,lboun3)
    !
    ! Integer(8)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(in)            :: ndim1
    integer(8),   intent(in)            :: ndim2
    integer(8),   intent(in)            :: ndim3
    integer(8),   intent(out), pointer  :: varia(:,:,:)
    character(*), intent(in),  optional :: wzero
    integer(8),   intent(in),  optional :: lboun1
    integer(8),   intent(in),  optional :: lboun2
    integer(8),   intent(in),  optional :: lboun3
    integer(4)                          :: istat
    integer(8)                          :: idim1
    integer(8)                          :: idim2
    integer(8)                          :: idim3
    logical(lg)                         :: lzero

    if( ndim1*ndim2*ndim3 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       if( present(lboun1) .and. present(lboun2) .and. present(lboun3) ) then
          allocate( varia(lboun1:lboun1+ndim1-1,lboun2:lboun2+ndim2-1,lboun3:lboun3+ndim3-1) , stat = istat )
       else
          allocate( varia(ndim1,ndim2,ndim3) , stat = istat )
       end if
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(ndim2,8)*int(ndim3,8)*int(kind(varia),8)
          if( lzero ) then
             do idim3 = lbound(varia,3),ubound(varia,3)
                do idim2 = lbound(varia,2),ubound(varia,2)
                   do idim1 = lbound(varia,1),ubound(varia,1)
                      varia(idim1,idim2,idim3) = 0_8
                   end do
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'integer(4)')

    else

       nullify(varia)

    end if

  end subroutine memory_alli83

  subroutine memory_deai81(memor,vanam,vacal,varia)
    !
    ! Integer(8)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   pointer               :: varia(:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'integer(4)')

    end if

  end subroutine memory_deai81

  subroutine memory_deai82(memor,vanam,vacal,varia)
    !
    ! Integer(8)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   pointer               :: varia(:,:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(size(varia,2),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'integer(4)')

    end if

  end subroutine memory_deai82

  subroutine memory_deai83(memor,vanam,vacal,varia)
    !
    ! Integer(8)(:,:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   pointer               :: varia(:,:,:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)*int(size(varia,2),8)*int(size(varia,3),8)*int(kind(varia),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'integer(4)')

    end if

  end subroutine memory_deai83

  subroutine memory_allcha_4(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Character(*)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(in)            :: ndim1
    character(*), intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(4)                          :: istat
    integer(8)                          :: idim1
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(kind(varia),8)
          if( lzero ) then
             do idim1 = 1,ndim1
                varia(idim1) = ''
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'character')

    else

       nullify(varia)

    end if

  end subroutine memory_allcha_4

  subroutine memory_allcha_8(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Character(*)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(in)            :: ndim1
    character(*), intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(4)                          :: istat
    integer(8)                          :: idim1
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)
          if( lzero ) then
             do idim1 = 1,ndim1
                varia(idim1) = ''
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'character')

    else

       nullify(varia)

    end if

  end subroutine memory_allcha_8

  subroutine memory_deacha(memor,vanam,vacal,varia)
    !
    ! Character(*)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    character(*), pointer               :: varia(:)
    integer(4)                          :: istat

    if( associated(varia) ) then

       lbytm = -int(size(varia,1),8)
       deallocate( varia , stat = istat )

       if( istat /= 0 ) then
          call memory_error(2_ip,vanam,vacal,istat)
       else
          nullify (varia)
       end if
       call memory_info(memor,vanam,vacal,'character')

    end if

  end subroutine memory_deacha

  !----------------------------------------------------------------------
  !
  ! Copy arrays
  !
  ! 1. Allocate varia if it is null
  ! 2. varia <= vacpy
  ! 3. Deallocate vacpy if 'DO_NOT_DEALLOCATE' not present
  !
  !----------------------------------------------------------------------

  subroutine memory_cpyi41(memor,vanam,vacal,vacpy,varia,wzero)
    !
    ! Integer(4)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(out), pointer  :: varia(:)
    integer(4),   intent(out), pointer  :: vacpy(:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1
    logical(lg)                         :: lzero
    integer(ip)                         :: ndim1

    ndim1 = size(vacpy,1_4) 

    if( ndim1 > 0 ) then

       if( .not. associated(varia) ) then
          call memory_alloca(memor,vanam,vacal,varia,ndim1,'DO_NOT_INITIALIZE')
       else
          ndim1 = min(ndim1,int(size(varia),ip))
       end if

       do idim1 = 1,ndim1
          varia(idim1) = vacpy(idim1)
       end do

    else

       nullify(varia)

    end if

    lzero = .true.
    if(present(wzero)) then
       if( wzero == 'DO_NOT_DEALLOCATE') lzero = .false.
    end if
    if( lzero ) call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_cpyi41

  subroutine memory_cpyi81(memor,vanam,vacal,vacpy,varia,wzero)
    !
    ! Integer(8)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(out), pointer  :: varia(:)
    integer(8),   intent(out), pointer  :: vacpy(:)
    character(*), intent(in),  optional :: wzero
    integer(8)                          :: idim1
    logical(lg)                         :: lzero
    integer(8)                          :: ndim1

    ndim1 = size(vacpy,1_4)

    if( ndim1 > 0 ) then

       if( .not. associated(varia) ) then
          call memory_alloca(memor,vanam,vacal,varia,ndim1,'DO_NOT_INITIALIZE')
       else
          ndim1 = min(ndim1,int(size(varia),8))
       end if

       do idim1 = 1,ndim1
          varia(idim1) = vacpy(idim1)
       end do

    else

       nullify(varia)

    end if

    lzero = .true.
    if(present(wzero)) then
       if( wzero == 'DO_NOT_DEALLOCATE') lzero = .false.
    end if
    if( lzero ) call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_cpyi81

  subroutine memory_cpyi42(memor,vanam,vacal,vacpy,varia,wzero)
    !
    ! Integer(4)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(out), pointer  :: varia(:,:)
    integer(4),   intent(out), pointer  :: vacpy(:,:)
    character(*), intent(in),  optional :: wzero
    integer(4)                          :: idim1,idim2
    logical(lg)                         :: lzero
    integer(4)                          :: ndim1,ndim2

    ndim1 = size(vacpy,1_ip)
    ndim2 = size(vacpy,2_ip)

    if( ndim1 > 0 .and. ndim2 > 0 ) then

       if( .not. associated(varia) ) then
          call memory_alloca(memor,vanam,vacal,varia,ndim1,ndim2,'DO_NOT_INITIALIZE')
       else
          ndim1 = min(ndim1,int(size(varia,1),4))
          ndim2 = min(ndim2,int(size(varia,2),4))
       end if

       do idim2 = 1,ndim2
          do idim1 = 1,ndim1
             varia(idim1,idim2) = vacpy(idim1,idim2)
          end do
       end do

    else

       nullify(varia)

    end if

    lzero = .true.
    if(present(wzero)) then
       if( wzero == 'DO_NOT_DEALLOCATE') lzero = .false.
    end if
    if( lzero ) call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_cpyi42

  subroutine memory_cpyi82(memor,vanam,vacal,vacpy,varia,wzero)
    !
    ! Integer(8)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(out), pointer  :: varia(:,:)
    integer(8),   intent(out), pointer  :: vacpy(:,:)
    character(*), intent(in),  optional :: wzero
    integer(8)                          :: idim1,idim2
    logical(lg)                         :: lzero
    integer(8)                          :: ndim1,ndim2

    ndim1 = size(vacpy,1_ip)
    ndim2 = size(vacpy,2_ip)

    if( ndim1 > 0 .and. ndim2 > 0 ) then

       if( .not. associated(varia) ) then
          call memory_alloca(memor,vanam,vacal,varia,ndim1,ndim2,'DO_NOT_INITIALIZE')
       else
          ndim1 = min(ndim1,int(size(varia,1),8))
          ndim2 = min(ndim2,int(size(varia,2),8))
       end if

       do idim2 = 1,ndim2
          do idim1 = 1,ndim1
             varia(idim1,idim2) = vacpy(idim1,idim2)
          end do
       end do

    else

       nullify(varia)

    end if

    lzero = .true.
    if(present(wzero)) then
       if( wzero == 'DO_NOT_DEALLOCATE') lzero = .false.
    end if
    if( lzero ) call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_cpyi82

  subroutine memory_cpyi1p_1(memor,vanam,vacal,vacpy,varia,wzero)
    !
    ! Type(i1p)(4)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(i1p),    intent(out), pointer  :: varia(:)
    type(i1p),    intent(out), pointer  :: vacpy(:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1,idim2
    logical(lg)                         :: lzero
    integer(ip)                         :: ndim1
    integer(ip)                         :: ndim2

    ndim1 = size(vacpy,1_ip)

    if( ndim1 > 0 ) then

       if( .not. associated(varia) ) then
          call memory_alloca(memor,vanam,vacal,varia,ndim1,'DO_NOT_INITIALIZE')
          do idim1 = 1,ndim1
             ndim2 = size(vacpy(idim1) % l,1_ip)
             nullify(varia(idim1) % l)
             call memory_alloca(memor,vanam,vacal,varia(idim1) % l,ndim2,'DO_NOT_INITIALIZE')
          end do
       else
          ndim1 = min(ndim1,int(size(varia),ip))
       end if

       do idim1 = 1,ndim1
          ndim2 = min(int(size(vacpy(idim1) % l,1_ip),ip),int(size(varia(idim1) % l,1_ip),ip))
          do idim2 = 1,ndim2
             varia(idim1) % l(idim2) = vacpy(idim1) % l(idim2) 
          end do
       end do

    else

       nullify(varia)

    end if

    lzero = .true.
    if(present(wzero)) then
       if( wzero == 'DO_NOT_DEALLOCATE') lzero = .false.
    end if
    if( lzero ) call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_cpyi1p_1

  subroutine memory_cpyrp1(memor,vanam,vacal,vacpy,varia,wzero)
    !
    ! Real(rp)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    real(rp),     intent(out), pointer  :: varia(:)
    real(rp),     intent(out), pointer  :: vacpy(:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1
    logical(lg)                         :: lzero
    integer(ip)                         :: ndim1

    ndim1 = size(vacpy,1_4) 

    if( ndim1 > 0 ) then

       if( .not. associated(varia) ) then
          call memory_alloca(memor,vanam,vacal,varia,ndim1,'DO_NOT_INITIALIZE')
       else
          ndim1 = min(ndim1,int(size(varia),ip))
       end if

       do idim1 = 1,ndim1
          varia(idim1) = vacpy(idim1)
       end do

    else

       nullify(varia)

    end if

    lzero = .true.
    if(present(wzero)) then
       if( wzero == 'DO_NOT_DEALLOCATE') lzero = .false.
    end if
    if( lzero ) call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_cpyrp1

  subroutine memory_cpyrp2(memor,vanam,vacal,vacpy,varia,wzero)
    !
    ! Real(rp)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    real(rp),     intent(out), pointer  :: varia(:,:)
    real(rp),     intent(out), pointer  :: vacpy(:,:)
    character(*), intent(in),  optional :: wzero
    integer(ip)                         :: idim1,idim2
    logical(lg)                         :: lzero
    integer(ip)                         :: ndim1,ndim2

    ndim1 = size(vacpy,1_ip)
    ndim2 = size(vacpy,2_ip)

    if( ndim1 > 0 .and. ndim2 > 0 ) then

       if( .not. associated(varia) ) then
          call memory_alloca(memor,vanam,vacal,varia,ndim1,ndim2,'DO_NOT_INITIALIZE')
       else
          ndim1 = min(ndim1,int(size(varia,1),4))
          ndim2 = min(ndim2,int(size(varia,2),4))
       end if

       do idim2 = 1,ndim2
          do idim1 = 1,ndim1
             varia(idim1,idim2) = vacpy(idim1,idim2)
          end do
       end do

    else

       nullify(varia)

    end if

    lzero = .true.
    if(present(wzero)) then
       if( wzero == 'DO_NOT_DEALLOCATE') lzero = .false.
    end if
    if( lzero ) call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_cpyrp2

  !----------------------------------------------------------------------
  !
  ! Resize arrays
  !
  ! 1. Nullify(vacpy)
  ! 2. vacpy <= varia, deallocate varia
  ! 3. Allocate varia with new dimension
  ! 4. varia <= vacpy, deallocate vacpy
  !
  !----------------------------------------------------------------------

  subroutine memory_newi41(memor,vanam,vacal,varia,ndim1)
    !
    ! Integer(4)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(out), pointer  :: varia(:)
    integer(4),   intent(in)            :: ndim1
    integer(4),                pointer  :: vacpy(:)
    logical(lg)                         :: varia_associated

    nullify(vacpy)

    if( associated(varia) ) then
       varia_associated = .true.
    else
       varia_associated = .false.
    end if

    if( varia_associated ) call memory_copy(  memor,vanam,vacal,varia,vacpy)
    !call memory_deallo(memor,vanam,vacal,varia)
    !varia => vacpy
    call memory_alloca(memor,vanam,vacal,varia,ndim1)
    if( varia_associated ) call memory_copy(  memor,vanam,vacal,vacpy,varia)

  end subroutine memory_newi41

  subroutine memory_newi42(memor,vanam,vacal,varia,ndim1,ndim2)
    !
    ! Integer(4)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(4),   intent(out), pointer  :: varia(:,:)
    integer(4),   intent(in)            :: ndim1
    integer(4),   intent(in)            :: ndim2
    integer(4),                pointer  :: vacpy(:,:)
    logical(lg)                         :: varia_associated

    nullify(vacpy)

    if( associated(varia) ) then
       varia_associated = .true.
    else
       varia_associated = .false.
    end if

    if( varia_associated ) call memory_copy(  memor,vanam,vacal,varia,vacpy)
    !call memory_deallo(memor,vanam,vacal,varia)
    !varia => vacpy
    call memory_alloca(memor,vanam,vacal,varia,ndim1,ndim2)
    if( varia_associated ) call memory_copy(  memor,vanam,vacal,vacpy,varia)

  end subroutine memory_newi42

  subroutine memory_newi81(memor,vanam,vacal,varia,ndim1)
    !
    ! Integer(8)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(out), pointer  :: varia(:)
    integer(8),   intent(in)            :: ndim1
    integer(8),                pointer  :: vacpy(:)
    logical(lg)                         :: varia_associated

    nullify(vacpy)

    if( associated(varia) ) then
       varia_associated = .true.
    else
       varia_associated = .false.
    end if

    if( varia_associated ) call memory_copy(  memor,vanam,vacal,varia,vacpy)
    !call memory_deallo(memor,vanam,vacal,varia)
    !varia => vacpy
    call memory_alloca(memor,vanam,vacal,varia,ndim1)
    if( varia_associated ) call memory_copy(  memor,vanam,vacal,vacpy,varia)

  end subroutine memory_newi81

  subroutine memory_newi82(memor,vanam,vacal,varia,ndim1,ndim2)
    !
    ! Integer(8)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(8),   intent(out), pointer  :: varia(:,:)
    integer(8),   intent(in)            :: ndim1
    integer(8),   intent(in)            :: ndim2
    integer(8),                pointer  :: vacpy(:,:)
    logical(lg)                         :: varia_associated

    nullify(vacpy)

    if( associated(varia) ) then
       varia_associated = .true.
    else
       varia_associated = .false.
    end if

    if( varia_associated ) call memory_copy(  memor,vanam,vacal,varia,vacpy)
    call memory_deallo(memor,vanam,vacal,varia)
    varia => vacpy
    !call memory_alloca(memor,vanam,vacal,varia,ndim1,ndim2)
    !if( varia_associated ) call memory_copy(  memor,vanam,vacal,vacpy,varia)

  end subroutine memory_newi82

  subroutine memory_newi1p_1(memor,vanam,vacal,varia,ndim1)
    !
    ! Type(I1P)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    type(i1p),    intent(out), pointer  :: varia(:)
    integer(ip),  intent(in)            :: ndim1
    type(i1p),                 pointer  :: vacpy(:)
    logical(lg)                         :: varia_associated

    nullify(vacpy)

    if( associated(varia) ) then
       varia_associated = .true.
    else
       varia_associated = .false.
    end if

    if( varia_associated ) call memory_copy(  memor,vanam,vacal,varia,vacpy)
    !call memory_deallo(memor,vanam,vacal,varia)
    !varia => vacpy
    call memory_alloca(memor,vanam,vacal,varia,ndim1)
    if( varia_associated ) call memory_copy(  memor,vanam,vacal,vacpy,varia)

  end subroutine memory_newi1p_1

  subroutine memory_allxp1(memor,vanam,vacal,varia,ndim1,wzero)
    !
    ! Complex(rp)(:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    complex(rp),  intent(out), pointer  :: varia(:)
    character(*), intent(in),  optional :: wzero
    integer(4)                          :: istat
    integer(ip)                         :: idim1
    logical(lg)                         :: lzero

    if( ndim1 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          lbytm = int(ndim1,8)*int(kind(varia),8)*2
          if( lzero ) then
             do idim1 = 1,ndim1
                varia(idim1) = CMPLX(0.0_rp,0.0_rp,kind=rp)
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if

       call memory_info(memor,vanam,vacal,'real')

    else

       nullify(varia)

    end if

  end subroutine memory_allxp1

  subroutine memory_allxp2(memor,vanam,vacal,varia,ndim1,ndim2,wzero)
    !
    ! Complex(rp)(:,:)
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    integer(ip),  intent(in)            :: ndim1
    integer(ip),  intent(in)            :: ndim2
    complex(rp),  intent(out), pointer  :: varia(:,:)
    character(*), intent(in),  optional :: wzero
    integer(4)                          :: istat
    integer(ip)                         :: idim1,idim2
    logical(lg)                         :: lzero

    if( ndim1*ndim2 > 0 ) then

       if( associated(varia) ) then
          write(*,*) 'POINTER ALREADY ASSOCIATED: ',kfl_paral,trim(vanam),trim(vacal)
          call runend('MOD_MEMORY: POINTER ALREADY ASSOCIATED') 
       end if

       allocate( varia(ndim1,ndim2) , stat = istat )
       lzero = .true.
       if(present(wzero)) then
          if( wzero == 'DO_NOT_INITIALIZE') then
             lzero = .false.
          end if
       end if

       if( istat == 0 ) then
          !lbytm = int(ndim1,8)*int(kind(varia),8)*2
          lbytm = int(size(varia,1),8)*int(size(varia,2),8)*int(kind(varia),8)
          if( lzero ) then
             do idim2 = 1,ndim2
                do idim1 = 1,ndim1
                   varia(idim1,idim2) = CMPLX(0.0_rp,0.0_rp,kind=rp)
                end do
             end do
          end if
       else
          call memory_error(0_ip,vanam,vacal,istat)
       end if
       call memory_info(memor,vanam,vacal,'real')

    else

       nullify(varia)

    end if

  end subroutine memory_allxp2

  !----------------------------------------------------------------------
  !
  ! Renumber arrays
  !
  ! 1. Nullify vacpy
  ! 2. Copy vacpy <= varia, deallocate varia
  ! 3. Reallocate varia
  ! 4. Renumber varia using vacpy
  ! 5. Deallocate vacpy
  !
  !----------------------------------------------------------------------

  subroutine memory_reni41(memor,vanam,vacal,varia,lrenu)
    !
    ! Integer(4)(:)
    !
    implicit none
    character(*), intent(in)           :: vanam
    character(*), intent(in)           :: vacal
    integer(8),   intent(inout)        :: memor(2) 
    integer(4),   intent(out), pointer :: varia(:)
    integer(ip),  intent(in),  pointer :: lrenu(:)
    integer(4),                pointer :: vacpy(:)
    integer(ip)                        :: idim1_new,idim1_old
    integer(ip)                        :: ndim1_new,ndim1_old

    nullify(vacpy)
    ndim1_old = size(varia)
    call memory_copy(memor,vanam,vacal,varia,vacpy)
    ndim1_new = 0
    do idim1_old = 1,ndim1_old
       if( lrenu(idim1_old) /= 0 ) ndim1_new = ndim1_new + 1
    end do
    call memory_alloca(memor,vanam,vacal,varia,ndim1_new)
    do idim1_old = 1,ndim1_old
       if( lrenu(idim1_old) /= 0 ) then
          idim1_new        = lrenu(idim1_old)
          varia(idim1_new) = vacpy(idim1_old)
       end if
    end do
    call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_reni41

  subroutine memory_reni81(memor,vanam,vacal,varia,lrenu)
    !
    ! Integer(8)(:)
    !
    implicit none
    character(*), intent(in)           :: vanam
    character(*), intent(in)           :: vacal
    integer(8),   intent(inout)        :: memor(2) 
    integer(8),   intent(out), pointer :: varia(:)
    integer(8),   intent(in),  pointer :: lrenu(:)
    integer(8),                pointer :: vacpy(:)
    integer(8)                         :: idim1_new,idim1_old
    integer(8)                         :: ndim1_new,ndim1_old

    ndim1_old = size(varia)
    call memory_copy(memor,vanam,vacal,varia,vacpy)
    ndim1_new = 0
    do idim1_old = 1,ndim1_old
       if( lrenu(idim1_old) /= 0 ) ndim1_new = ndim1_new + 1
    end do
    call memory_alloca(memor,vanam,vacal,varia,ndim1_new)
    do idim1_old = 1,ndim1_old
       if( lrenu(idim1_old) /= 0 ) then
          idim1_new        = lrenu(idim1_old)
          varia(idim1_new) = vacpy(idim1_old)
       end if
    end do
    call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_reni81

  subroutine memory_renrp1(memor,vanam,vacal,varia,lrenu)
    !
    ! Real(rp)(:)
    !
    implicit none
    character(*), intent(in)           :: vanam
    character(*), intent(in)           :: vacal
    integer(8),   intent(inout)        :: memor(2) 
    real(rp),     intent(out), pointer :: varia(:)
    integer(ip),  intent(in),  pointer :: lrenu(:)
    real(rp),                  pointer :: vacpy(:)
    integer(ip)                        :: idim1_new,idim1_old
    integer(ip)                        :: ndim1_new,ndim1_old

    ndim1_old = size(varia)
    call memory_copy(memor,vanam,vacal,varia,vacpy)
    ndim1_new = 0
    do idim1_old = 1,ndim1_old
       if( lrenu(idim1_old) /= 0 ) ndim1_new = ndim1_new + 1
    end do
    call memory_alloca(memor,vanam,vacal,varia,ndim1_new)
    do idim1_old = 1,ndim1_old
       if( lrenu(idim1_old) /= 0 ) then
          idim1_new        = lrenu(idim1_old)
          varia(idim1_new) = vacpy(idim1_old)
       end if
    end do
    call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_renrp1

  subroutine memory_renrp2(memor,vanam,vacal,varia,lrenu,idime)
    !
    ! Real(rp)(:,:)
    ! If renumbering LRENU is present, it assumes by default that
    ! the second dimension is the one that should be renumbered 
    !
    implicit none
    character(*), intent(in)            :: vanam
    character(*), intent(in)            :: vacal
    integer(8),   intent(inout)         :: memor(2) 
    real(rp),     intent(out), pointer  :: varia(:,:)
    integer(ip),  intent(in),  pointer  :: lrenu(:)
    real(rp),                  pointer  :: vacpy(:,:)
    integer(ip),  intent(in),  optional :: idime   
    integer(ip)                         :: idim1_new,idim1_old
    integer(ip)                         :: ndim1_new,ndim1_old
    integer(ip)                         :: ndim2,kdime_1,kdime_2
    !
    ! If IDIME is not present: KDIME_1=2, KDIME_2=1
    !
    if( present(idime) ) then
       kdime_1 = idime
       call runend('MEMORY_RENRP2: NOT CODED')
    else
       kdime_1 = 2
    end if
    if( kdime_1 == 1 ) then
       kdime_2 = 2
    else if( kdime_1 == 2 ) then
       kdime_2 = 1
    else
       call runend('MEMORY_RENRP2: WRONG DIMENSION')
    end if
    !
    ! VARIA(KDIME_2,KDIME_1)
    ! NDIM1_OLD <= size(VARIA,2)
    !
    ndim1_old = size(varia,kdime_1)
    ndim2     = size(varia,kdime_2)
    call memory_copy(memor,vanam,vacal,varia,vacpy)
    ndim1_new = 0
    do idim1_old = 1,ndim1_old
       if( lrenu(idim1_old) /= 0 ) ndim1_new = ndim1_new + 1
    end do
    call memory_alloca(memor,vanam,vacal,varia,ndim2,ndim1_new)
    do idim1_old = 1,ndim1_old
       if( lrenu(idim1_old) /= 0 ) then
          idim1_new = lrenu(idim1_old)
          varia(1:ndim2,idim1_new) = vacpy(1:ndim2,idim1_old)
       end if
    end do
    call memory_deallo(memor,vanam,vacal,vacpy)

  end subroutine memory_renrp2

  !----------------------------------------------------------------------
  !
  ! Allocate minimum size if pointers are not associated
  !
  !----------------------------------------------------------------------
  !
  ! Real(rp)(:)
  !
  subroutine memory_alloca_min_rp1(varia)
    real(rp), intent(inout), pointer :: varia(:)
    if( .not. associated(varia) ) then
       allocate( varia(1) )
       varia(1) = 0.0_rp
    end if
  end subroutine memory_alloca_min_rp1
  !
  ! Real(rp)(:,:)
  !
  subroutine memory_alloca_min_rp2(varia)
    real(rp), intent(inout), pointer :: varia(:,:)
    if( .not. associated(varia) ) then
       allocate( varia(1,1) )
       varia(1,1) = 0.0_rp
    end if
  end subroutine memory_alloca_min_rp2
  !
  ! Real(rp)(:,:,:)
  !
  subroutine memory_alloca_min_rp3(varia)
    real(rp), intent(inout), pointer :: varia(:,:,:)
    if( .not. associated(varia) ) then
       allocate( varia(1,1,1) )
       varia(1,1,1) = 0.0_rp
    end if
  end subroutine memory_alloca_min_rp3
  !
  ! integer(ip)(:)
  !
  subroutine memory_alloca_min_ip1(varia)
    integer(ip), intent(inout), pointer :: varia(:)
    if( .not. associated(varia) ) then
       allocate( varia(1) )
       varia(1) = 0_ip
    end if
  end subroutine memory_alloca_min_ip1
  !
  ! integer(ip)(:,:)
  !
  subroutine memory_alloca_min_ip2(varia)
    integer(ip), intent(inout), pointer :: varia(:,:)
    if( .not. associated(varia) ) then
       allocate( varia(1,1) )
       varia(1,1) = 0_ip
    end if
  end subroutine memory_alloca_min_ip2
  !
  ! integer(ip)(:,:,:)
  !
  subroutine memory_alloca_min_ip3(varia)
    integer(ip), intent(inout), pointer :: varia(:,:,:)
    if( .not. associated(varia) ) then
       allocate( varia(1,1,1) )
       varia(1,1,1) = 0_ip
    end if
  end subroutine memory_alloca_min_ip3
  !
  ! logical(lg)(:)
  !
  subroutine memory_alloca_min_lg1(varia)
    logical(lg), intent(inout), pointer :: varia(:)
    if( .not. associated(varia) ) then
       allocate( varia(1) )
       varia(1) = .false.
    end if
  end subroutine memory_alloca_min_lg1
  !
  ! logical(lg)(:,:)
  !
  subroutine memory_alloca_min_lg2(varia)
    logical(lg), intent(inout), pointer :: varia(:,:)
    if( .not. associated(varia) ) then
       allocate( varia(1,1) )
       varia(1,1) = .false.
    end if
  end subroutine memory_alloca_min_lg2
  !
  ! logical(lg)(:,:,:)
  !
  subroutine memory_alloca_min_lg3(varia)
    logical(lg), intent(inout), pointer :: varia(:,:,:)
    if( .not. associated(varia) ) then
       allocate( varia(1,1,1) )
       varia(1,1,1) = .false.
    end if
  end subroutine memory_alloca_min_lg3

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    19/11/2015
  !> @brief   Errro message
  !> @details Write an error message when memory could not be allocated
  !>          or deallocated
  !>
  !-----------------------------------------------------------------------

  subroutine memory_error(itask,vanam,vacal,istat)
    implicit none
    integer(ip),   intent(in) :: itask
    integer(4),    intent(in) :: istat
    integer(ip)               :: ibyte
    real(rp)                  :: rbyte
    character(6)              :: lbyte
    character*(*), intent(in) :: vanam,vacal
    character(200)            :: wmess
    character(20)             :: wmes2,wmes3

    if( itask == 0 ) then
       !
       ! Allocation
       !
       if( lbytm >= 1024*1024*1024 ) then
          rbyte = 1024.0_rp*1024.0_rp*1024.0_rp
          lbyte = 'Gbytes'
       else if( lbytm >= 1024*1024 ) then 
          rbyte = 1024.0_rp*1024.0_rp
          lbyte = 'Mbytes'     
       else if( lbytm >= 1024 ) then 
          rbyte = 1024.0_rp
          lbyte = 'kbytes'          
       else  
          rbyte = 1.0_rp
          lbyte = ' bytes'     
       end if
       ibyte = int(real(lbytm)/rbyte)
       wmes2 = memory_intost(ibyte)
       wmes3 = memory_intost(int(istat,ip))
       wmess = trim(vacal)//': MEMORY FOR '//trim(vanam)//' COULD NOT BE ALLOCATED.'&
            //' RUN TIME ERROR: '//trim(wmes3)
       call runend(trim(wmess))

    else if( itask == 1 ) then
       !
       ! Reallocation
       !
       call runend(trim(vacal)//': MEMORY FOR '//trim(vanam)//' COULD NOT BE REALLOCATED')

    else
       !
       ! Deallocation
       !
       call runend(trim(vacal)//': MEMORY FOR '//trim(vanam)//' COULD NOT BE DEALLOCATED')

    end if

  end subroutine memory_error

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    19/11/2015
  !> @brief   Integer to string
  !> @details Convert an integer(ip) to a string
  !>
  !-----------------------------------------------------------------------

  function memory_intost(integ)

    integer(ip)   :: integ
    integer(4)    :: integ4
    character(20) :: memory_intost
    character(20) :: intaux

    integ4 = int(integ,4)
    write(intaux,*) integ4
    memory_intost = adjustl(intaux)

  end function memory_intost

  !-----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    19/11/2015
  !> @brief   Memory info
  !> @details This routine computes some info on memory
  !>
  !-----------------------------------------------------------------------

  subroutine memory_info(memor,vanam,vacal,vatyp)

    character(*), intent(in)    :: vanam,vacal
    integer(8),   intent(inout) :: memor(2)
    integer(4)                  :: lun_memor4
    real(rp)                    :: rbyte,rbyt2
    character(6)                :: lbyte,lbyt2
    character(*)                :: vatyp
    !
    ! Actualize memory
    !
    memor(1) = memor(1)+lbytm
    memor(2) = max(memor(2),memor(1))
    !
    ! Current total memory
    !
    mem_curre = mem_curre+lbytm
    !
    ! Write memory onfo
    !
    call memctr(memor,vanam,vacal,vatyp)

  end subroutine memory_info

end module mod_memory
!> @}
