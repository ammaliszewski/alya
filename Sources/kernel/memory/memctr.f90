subroutine memctr(memor,vanam,vacal,vatyp)
  !-----------------------------------------------------------------------
  !****f* memory/memctr
  ! NAME 
  !    memctr
  ! DESCRIPTION
  !    This routine writes some info on memory
  ! USES
  !    runend
  ! USED BY
  !    Mod_memchk
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only        :  ip,rp
  use mod_memory, only        :  lun_memor
  use mod_memory, only        :  lbytm,mem_curre
  use def_master, only        :  kfl_memor,mem_alloc
  use def_master, only        :  INOTSLAVE,ISEQUEN,IPARALL,kfl_outpu_par
  implicit none
  character(*), intent(in)    :: vanam,vacal
  integer(8),   intent(inout) :: memor(2)
  integer(4)                  :: lun_memor4
  real(rp)                    :: rbyte,rbyt2
  character(6)                :: lbyte,lbyt2
  character(*)                :: vatyp
  !
  ! Write memory onfo
  !
  if( kfl_memor == 1 .and. ( INOTSLAVE .or. kfl_outpu_par == 1 ) ) then 

     if( mem_curre >= 1024*1024*1024 ) then
        rbyte = 1024.0_rp*1024.0_rp*1024.0_rp
        lbyte = 'Gbytes'
     else if( mem_curre >= 1024*1024 ) then 
        rbyte = 1024.0_rp*1024.0_rp
        lbyte = 'Mbytes'     
     else if( mem_curre >= 1024 ) then 
        rbyte = 1024.0_rp
        lbyte = 'kbytes'          
     else  
        rbyte = 1.0_rp
        lbyte = ' bytes'     
     end if

     if( abs(lbytm) >= 1024*1024*1024 ) then
        rbyt2 = 1024.0_rp*1024.0_rp*1024.0_rp
        lbyt2 = 'Gbytes'
     else if( abs(lbytm) >= 1024*1024 ) then 
        rbyt2 = 1024.0_rp*1024.0_rp
        lbyt2 = 'Mbytes'     
     else if( abs(lbytm) >= 1024 ) then 
        rbyt2 = 1024.0_rp
        lbyt2 = 'kbytes'          
     else  
        rbyt2 = 1.0_rp
        lbyt2 = ' bytes'     
     end if

     lun_memor4 = int(lun_memor,4)
     mem_alloc  = mem_alloc + 1
     if( mem_alloc == 1 .and. ISEQUEN ) write(lun_memor4,1) 

     if( IPARALL ) then
        !
        ! In parallel: only write memory in bytes
        !
        write(lun_memor4,*) mem_curre
     else
        !
        ! In sequantial: write more information
        !
        write(lun_memor4,2) &
             mem_alloc,&
             real(mem_curre,rp),real(mem_curre,rp)/rbyte,lbyte,&
             real(lbytm,rp),real(lbytm,rp)/rbyt2,lbyt2,trim(vanam),&
             trim(vatyp),trim(vacal)
        write(88,3) &
             real(lbytm),trim(vanam),trim(vacal)
     end if
     call flush(lun_memor4)

  end if

1 format('# Operation |  Cur. memory (b) |   Cur. memory    |  Ope. memory (b)',&
       & ' |   Ope. memory     | Variable name  |    Type    | Subroutine',/,&
       & '#-----------+------------------+------------------+-----------------',&
       & '-+-------------------+----------------+------------+-----------' )
2 format(1x,i10,'   ',&
       & e16.8E3,'   ','(',f7.2,1x,a6,')','   ',&
       & e16.8E3,'   ','(',f8.2,1x,a6,')','   ',&
       a14,'   ',a10,'   ',a)
3 format(1x,e16.8E3,1x,a10,1x,a10)

end subroutine memctr
