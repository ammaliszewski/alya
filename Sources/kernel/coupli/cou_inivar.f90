!-----------------------------------------------------------------------
!> @addtogroup Coupli
!> @{
!> @file    ker_inivar.f90
!> @author  Guillaume Houzeaux
!> @date    08/10/2014
!> @brief   Initialize coupling variables
!> @details Initialize coupling variables
!> @} 
!-----------------------------------------------------------------------

subroutine cou_inivar(itask)
  use def_kintyp
  use def_master
  use def_kermod
  use def_inpout
  use def_domain
  use def_coupli
  use mod_parall,         only : I_AM_IN_COLOR
  use mod_parall,         only : PAR_COMM_COLOR
  use mod_communications, only : PAR_INITIALIZE_NON_BLOCKING_COMM
  use mod_couplings,      only : MIRROR_COUPLING
  implicit none
  integer(ip), intent(in) :: itask 
  integer(ip)             :: icoup
  integer(ip)             :: color_target,color_source

  select case ( itask )

  case ( 1_ip )

     !-------------------------------------------------------------------
     !
     ! Initialize variables before reading data
     !
     !-------------------------------------------------------------------

     mcoup           = 0
     memor_cou       = 0
     mcoup_subdomain = 0
     cputi_cou       = 0.0_rp
     !
     ! Driver
     !  
     coupling_driver_couplings     = 0
     coupling_driver_max_iteration = 1      
     coupling_driver_iteration     = 0   
     coupling_driver_tolerance     = 1.0e-12_rp    
     kfl_gozon                     = 1
     !
     ! Others
     !
     nullify(coupling_type)
     call PAR_INITIALIZE_NON_BLOCKING_COMM()
     !
     ! Boundaries (useful if we have holes)
     !
     number_of_holes =  0
     nboun_cou       =  nboun
     lnodb_cou       => lnodb
     ltypb_cou       => ltypb
     lboch_cou       => lboch
     lnnob_cou       => lnnob
     lboel_cou       => lboel

  case ( 2_ip ) 

     !-------------------------------------------------------------------
     !
     ! Initialize variables after reading data
     !
     !-------------------------------------------------------------------

     do icoup = 1,mcoup
        
        color_target = coupling_type(icoup) % color_target
        color_source = coupling_type(icoup) % color_source        
        !
        ! Define kind of coupling
        !
        if( coupling_type(icoup) % zone_target + coupling_type(icoup) % zone_source == 0 ) then
           coupling_type(icoup) % kind = BETWEEN_SUBDOMAINS
        else           
           coupling_type(icoup) % kind = BETWEEN_ZONES
        end if
        !
        ! Define mirror coupling
        !
        coupling_type(icoup) % mirror_coupling = MIRROR_COUPLING(icoup) 
        !
        ! Subdomain coupling
        !
        if( coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS ) then
           if( I_AM_IN_COLOR(color_target) .or. I_AM_IN_COLOR(color_source) ) then
              mcoup_subdomain = mcoup_subdomain + 1
           end if
        end if
        !
        ! Communicators
        !
        coupling_type(icoup) % commd % PAR_COMM_WORLD = PAR_COMM_COLOR(color_target,color_source)
     end do

  end select

end subroutine cou_inivar
