!-----------------------------------------------------------------------
!> @addtogroup CoupliInput
!> @{
!> @file    cou_initialize_coupling.f90
!> @author  Guillaume Houzeaux
!> @date    03/03/2014
!> @brief   Read coupling data
!> @details Initialize coupling data
!> @} 
!-----------------------------------------------------------------------

subroutine cou_initialize_coupling()
  use def_kintyp,         only :  ip,rp,lg
  use def_master,         only :  INOTMASTER,kfl_paral,igene
  use def_master,         only :  kfl_timin
  use def_master,         only :  intost
  use def_domain,         only :  ndime,npoin
  use def_domain,         only :  coord
  use def_domain,         only :  nelem,lelez
  use def_domain,         only :  lesub,nelez
  use def_coupli,         only :  mcoup
  use def_coupli,         only :  PROJECTION
  use def_coupli,         only :  STRESS_PROJECTION
  use def_coupli,         only :  coupling_type
  use def_coupli,         only :  memor_cou
  use def_coupli,         only :  BETWEEN_SUBDOMAINS
  use def_coupli,         only :  BOUNDARY_INTERPOLATION
  use def_coupli,         only :  cputi_cou
  use def_coupli,         only :  nboun_cou
  use def_coupli,         only :  lnodb_cou
  use def_coupli,         only :  ltypb_cou
  use def_coupli,         only :  lnnob_cou
  use def_coupli,         only :  lboel_cou
  use mod_parall,         only :  I_AM_IN_COLOR
  use mod_parall,         only :  color_target
  use mod_parall,         only :  color_source
  use mod_communications, only :  PAR_MAX
  use mod_communications, only :  PAR_BARRIER
  use mod_interpolation,  only :  COU_SOURCE_INTERFACE_MASS_MATRIX
  use mod_interpolation,  only :  COU_TARGET_INTERFACE_MASS_MATRIX
  use mod_couplings,      only :  COU_INIT_INTERPOLATE_POINTS_VALUES

  use mod_couplings,      only :  COU_TRANSMISSION_ARRAYS

  use mod_couplings,      only :  I_AM_IN_COUPLING
  use mod_kdtree,         only :  kdtree_construct
  use mod_memory,         only :  memory_alloca
  use mod_memory,         only :  memory_deallo
  implicit none
  integer(ip)                  :: icoup
  integer(ip)                  :: ipoin,kpoin,jcoup,zone_source
  integer(ip)                  :: subdomain_source,ierro,code_target
  integer(ip)                  :: pnodb,kboun,iboun,ielem,kelem
  logical(lg), pointer         :: lesou(:)
  integer(ip), pointer         :: lbsou(:)
  real(rp)                     :: time1,time2

  nullify(lesou)
  nullify(lbsou)

  if( mcoup > 0 ) then

     call livinf(0_ip,'COUPLI: INITIALIZE COUPLING',0_ip)

     !-------------------------------------------------------------------
     !     
     ! Loop over couplings
     !
     !-------------------------------------------------------------------

     do icoup = 1,mcoup

        if( I_AM_IN_COUPLING(icoup) ) then

           code_target      = coupling_type(icoup) % code_target
           zone_source      = coupling_type(icoup) % zone_source
           subdomain_source = coupling_type(icoup) % subdomain_source
           color_target     = coupling_type(icoup) % color_target
           color_source     = coupling_type(icoup) % color_source
           jcoup            = coupling_type(icoup) % mirror_coupling 
           !
           ! Compute KDTree of the sources if necessary
           !
           if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
           call cputim(time1) 

           if( INOTMASTER ) then

              if(    coupling_type(icoup) % itype == STRESS_PROJECTION .or. &
                   & coupling_type(icoup) % itype == PROJECTION        ) then
                 !
                 ! Get mirror kdtree
                 !
                 if( jcoup /= 0 ) then
                    call kdtree_construct(&
                         nboun_cou,npoin,lnodb_cou,ltypb_cou,coord,coupling_type(icoup) % geome % kdtree,&
                         coupling_type(jcoup) % geome % lboun_wet)
                 end if

              else if( coupling_type(icoup) % itype == BOUNDARY_INTERPOLATION ) then
                 !
                 ! Consider only boundaries involved as a source
                 !
                 call memory_alloca(memor_cou,'LESOU','cou_initialize_coupling',lesou,nelem)
                 if( coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS ) then
                    do ielem = 1,nelem
                       if( lesub(ielem) == coupling_type(icoup) % subdomain_source ) lesou(ielem) = .true.
                    end do
                 else         
                    do kelem = 1,nelez(coupling_type(icoup) % zone_source)
                       ielem = lelez(coupling_type(icoup) % zone_source) % l(kelem)
                       lesou(ielem) = .true.
                    end do
                 end if
                 kboun = 0
                 do iboun = 1,nboun_cou
                    pnodb = lnnob_cou(iboun)
                    ielem = lboel_cou(pnodb+1,iboun)
                    if( lesou(ielem) ) kboun = kboun + 1
                 end do
                 call memory_alloca(memor_cou,'LBSOU','cou_initialize_coupling',lbsou,kboun)
                 kboun = 0
                 do iboun = 1,nboun_cou
                    pnodb = lnnob_cou(iboun)
                    ielem = lboel_cou(pnodb+1,iboun)
                    if( lesou(ielem) ) then
                       kboun = kboun + 1
                       lbsou(kboun) = iboun
                    end if
                 end do
                 call kdtree_construct(nboun_cou,npoin,lnodb_cou,ltypb_cou,coord,coupling_type(icoup) % geome % kdtree,lbsou)
                 call memory_deallo(memor_cou,'LBSOU','cou_initialize_coupling',lbsou)
                 call memory_deallo(memor_cou,'LESOU','cou_initialize_coupling',lesou)
              end if
           end if

           call cputim(time2) 
           cputi_cou(2) = + cputi_cou(2) + time2-time1
           !
           ! Get the CPUs in charge of my wet points
           !
           igene = icoup
           call livinf(0_ip,'COUPLI: DEFINE INTERPOLATION OF WET NODES FOR COUPLING '//trim(intost(icoup)),0_ip)
           call COU_INIT_INTERPOLATE_POINTS_VALUES(coupling_type(icoup) % geome % coord_wet,color_target,color_source,coupling_type(icoup))
           
           !!!!!!!!igene=icoup;call COU_TRANSMISSION_ARRAYS(coupling_type(icoup) % geome % coord_wet,color_target,color_source,coupling_type(icoup))
           !
           ! Check if all points have been found
           !
           ierro = 0
           do kpoin = 1,coupling_type(icoup) % geome % number_wet_points
              ipoin = coupling_type(icoup) % geome % status(kpoin)  
              if( ipoin == 0 ) then
                 if( INOTMASTER ) write(*,*) coupling_type(icoup) % geome % coord_wet(1:ndime,kpoin)
                 ierro = ierro + 1
              end if
           end do
           call PAR_MAX(ierro,'IN CURRENT COUPLING')
           if( ierro > 0 ) call runend('COU_INITIALIZE_COUPLING: SOME WET NODES ARE LOST')

        end if

!         call PAR_SCHEDULING(coupling_type(icoup))

     end do
     !
     ! Compute mass matrix if required
     !     
     if( INOTMASTER ) then
        do icoup = 1,mcoup
           if( coupling_type(icoup) % itype == STRESS_PROJECTION ) then
              jcoup = coupling_type(icoup) % mirror_coupling 
              if( jcoup /= 0 ) then
                 call COU_SOURCE_INTERFACE_MASS_MATRIX(coupling_type(icoup),coupling_type(jcoup))
              end if
           else if( coupling_type(icoup) % itype == PROJECTION ) then
              call COU_TARGET_INTERFACE_MASS_MATRIX(coupling_type(icoup))
           end if
        end do
     end if

  end if

end subroutine cou_initialize_coupling
