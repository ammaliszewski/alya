!------------------------------------------------------------------------
!> @addtogroup Coupling
!> @{
!> @name    Coupling arrays
!> @file    def_coupli.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!> @brief   ToolBox for coupling
!> @details ToolBox for coupling
!>          \verbatim
!>
!>          o----o----o----o----o----o----o----o Source
!>
!>          x-----------x----------x-----------x Target
!>        
!>          x ................... Wet nodes
!>          x-----x ............. Wet boundaries
!>
!>          NBOUN_WET ........... Number of wet boundaries
!>          LBOUN_WET(:) ........ List of wet boundaries
!>
!>          =>
!>
!>          NPOIN_WET ........... Number of wet nodes
!>          LPOIN_WET(:) ........ List of wet nodes
!>          
!>          NUMBER_WET_POINTS ... Number of wet points (nodes or Gauss points)
!>          COORD_WET(:,:) ...... Coordinates of wet points
!>          
!>          VMASB_WET(:) ........ Boundary mass matrix of wet nodes
!>          WEIGHT_WET(:) ....... Weight of values of wet nodes
!>
!>          \endverbatim
!>
!> @{
!------------------------------------------------------------------------

module def_coupli
  use def_kintyp, only : ip,rp,comm_data_par
  use mod_kdtree, only : typ_kdtree
  implicit none  

  !----------------------------------------------------------------------
  !
  ! Types and parameters
  !
  !----------------------------------------------------------------------

  integer(ip),        parameter     :: ON_SET                     = 1  ! Where
  integer(ip),        parameter     :: ON_FIELD                   = 2  ! Where
  integer(ip),        parameter     :: ON_CODE                    = 3  ! Where
  integer(ip),        parameter     :: ON_WHOLE_MESH              = 4  ! Where
  integer(ip),        parameter     :: ON_CHIMERA_MESH            = 5  ! Where
  integer(ip),        parameter     :: ELEMENT_INTERPOLATION      = 1  ! Type
  integer(ip),        parameter     :: NEAREST_BOUNDARY_NODE      = 2  ! Type
  integer(ip),        parameter     :: BOUNDARY_INTERPOLATION     = 3  ! Type
  integer(ip),        parameter     :: PROJECTION                 = 4  ! Type
  integer(ip),        parameter     :: NEAREST_ELEMENT_NODE       = 5  ! Type
  integer(ip),        parameter     :: BOUNDARY_VECTOR_PROJECTION = 6  ! Type
  integer(ip),        parameter     :: STRESS_PROJECTION          = 7  ! Type
  integer(ip),        parameter     :: UNKNOWN                    = 1  ! What
  integer(ip),        parameter     :: RESIDUAL                   = 2  ! What
  integer(ip),        parameter     :: DIRICHLET_EXPLICIT         = 3  ! What
  integer(ip),        parameter     :: DIRICHLET_IMPLICIT         = 4  ! What
  integer(ip),        parameter     :: INTRINSIC                  = 5  ! What
  integer(ip),        parameter     :: RELAXATION_SCHEME          = 1  ! Scheme (relaxation)
  integer(ip),        parameter     :: AITKEN_SCHEME              = 2  ! Scheme (relaxation)
  integer(ip),        parameter     :: BETWEEN_ZONES              = 1  ! Kind
  integer(ip),        parameter     :: BETWEEN_SUBDOMAINS         = 2  ! Kind
  integer(ip),        parameter     :: I_SOURCE                   = 1  ! Source/target
  integer(ip),        parameter     :: I_TARGET                   = 2  ! Source/target
  integer(ip),        parameter     :: FIXED_UNKNOWN              = 11 ! Fixity
  integer(ip),        parameter     :: INTERFACE_MASS             = 1  ! Conservation
  integer(ip),        parameter     :: GLOBAL_MASS                = 2  ! Conservation
  !
  ! Driver
  !
  integer(ip),        parameter     :: max_block_cou = 10
  integer(ip),        parameter     :: max_coupl_cou = 10
  integer(ip)                       :: coupling_driver_couplings(max_coupl_cou,max_block_cou)  
  integer(ip)                       :: coupling_driver_number_couplings(max_block_cou)  
  integer(ip)                       :: coupling_driver_max_iteration(max_block_cou)       
  integer(ip)                       :: coupling_driver_iteration(max_block_cou)           
  real(rp)                          :: coupling_driver_tolerance(max_block_cou)
  integer(ip)                       :: kfl_gozon 
  !
  ! Useful boundary: could be different from original one when
  ! using Chimera. In this case the fringe boundaries are only
  ! modified here
  !
  integer(ip)                       :: number_of_holes                ! Number of holes
  integer(ip)                       :: nboun_cou                      ! Number of boundaries
  integer(ip),      pointer         :: lnodb_cou(:,:)                 ! Boundary connectivity
  integer(ip),      pointer         :: ltypb_cou(:)                   ! Boundary type
  integer(ip),      pointer         :: lboch_cou(:)                   ! Boundary characteristics
  integer(ip),      pointer         :: lnnob_cou(:)                   ! Number of boundary nodes
  integer(ip),      pointer         :: lboel_cou(:,:)                 ! Boundary/element connectivity
  !
  ! When adding a variables, modify the following subroutine
  ! INITIALIZE_COUPLING_STRUCTURE
  ! DEALLOCATE_COUPLING_STRUCTURE
  !
  !
  ! Projection type to be used on target
  !
  type typ_proje
     integer(ip)          :: pgaub
     integer(ip), pointer :: permu(:)
     real(rp),    pointer :: shapb(:,:)
     real(rp),    pointer :: gbsur(:)
  end type typ_proje

  type typ_coupling_geometry
     !
     ! Information used as a source
     !
     !
     ! Unused yet
     !
     integer(ip)                    :: ndime                          ! Problem dimension
     !
     ! Memory allocation or integer definition in COU_INIT_INTERPOLATE_POINTS_VALUES
     !
     integer(ip)                    :: nelem_source                   ! Number of source elements
     integer(ip)                    :: nboun_source                   ! Number of source boundaries
     integer(ip)                    :: npoin_source                   ! Number of source nodes
     integer(ip),      pointer      :: lelem_source(:)                ! List of source elements
     integer(ip),      pointer      :: lboun_source(:)                ! List of source boundaries
     integer(ip),      pointer      :: lpoin_source(:)                ! List of source nodes
     integer(ip),      pointer      :: status(:)                      ! Results of the interpolation
     real(rp),         pointer      :: shapf(:,:)                     ! Shape functions for interpolations
     !
     ! Definition in COU_INITIALIZE_COUPLING
     !
     type(typ_kdtree)               :: kdtree                         ! Kd-tree
     !
     ! Definition in COU_SOURCE_INTERFACE_MASS_MATRIX
     !
     real(rp),         pointer      :: vmasb(:)                       ! Mass matrix used for projection
     !
     ! Wet information used as a target. 
     !
     ! Memory allocation or values initialization 
     ! in cou_define_wet_geometry
     !
     integer(ip)                    :: number_wet_points              ! Number of wet points (nodes or Gauss points)
     integer(ip)                    :: npoin_wet                      ! Number of wet nodes
     integer(ip)                    :: nboun_wet                      ! Number of wet boundaries
     integer(ip),      pointer      :: lboun_wet(:)                   ! List of wet boundaries
     integer(ip),      pointer      :: lpoin_wet(:)                   ! List of wet nodes
     real(rp),         pointer      :: coord_wet(:,:)                 ! Weight nodes coordinates
     real(rp),         pointer      :: weight_wet(:)                  ! Weight for force like
     !
     ! Unused yet, DEFINED COU_WET_INTERFACE_MASS_MATRIX
     !
     real(rp),         pointer      :: vmasb_wet(:)                   ! Boundary mass matrix
     !
     ! Unused yet
     !
     real(rp),         pointer      :: mass_ratio_wet(:)              ! Mass matrix around wet nodes
     integer(ip),      pointer      :: sched_perm(:)                  ! Scheduling permutation of status
     ! 
     ! Memory allocation or values initialization in COU_CREATE_A_SUBMESH unused yet
     !
     integer(ip)                    :: mnodb_target
     integer(ip)                    :: nboun_target
     integer(ip)                    :: npoin_target
     real(rp),         pointer      :: coord_target(:,:)
     integer(ip),      pointer      :: lnodb_target(:,:)
     integer(ip),      pointer      :: ltypb_target(:)

     integer(ip),      pointer      :: lperm_target(:)
     type(typ_proje),  pointer      :: proje_target(:)
  end type typ_coupling_geometry

  type typ_transmission
     !integer(ip),      pointer      :: lperm_target(:)
     !(1ip),        pointer      :: lperm_source(:)
     integer(ip),      pointer      :: ia_matrix(:)
     integer(ip),      pointer      :: ja_matrix(:)
     real(rp),         pointer      :: aa_matrix(:)
  end type typ_transmission

  type typ_color_coupling
     integer(ip)                    :: itype
     integer(ip)                    :: code_source
     integer(ip)                    :: code_target
     integer(ip)                    :: zone_source
     integer(ip)                    :: zone_target
     integer(ip)                    :: color_source
     integer(ip)                    :: color_target
     integer(ip)                    :: module_source
     integer(ip)                    :: module_target
     integer(ip)                    :: subdomain_source
     integer(ip)                    :: subdomain_target
     integer(ip)                    :: where_type
     integer(ip)                    :: where_number
     integer(ip)                    :: what
     integer(ip)                    :: scheme
     integer(ip)                    :: itera
     integer(ip)                    :: conservation
     integer(ip)                    :: overlap                        ! Number of overlapping elements (0 for disjoint)
     integer(ip)                    :: ngaus                          ! Gauss points strategy when using projection

     integer(ip)                    :: kind                           ! Kind of coupling: subdomain/zone type
     integer(ip)                    :: mirror_coupling                ! Number of the mirror coupling
     integer(ip)                    :: task_compute_and_send
     integer(ip)                    :: when_compute_and_send
     integer(ip)                    :: task_recv_and_assemble
     integer(ip)                    :: when_recv_and_assemble
     integer(ip)                    :: frequ_send                     ! Frequency of sends in coupling
     integer(ip)                    :: frequ_recv                     ! Frequency of recvs in coupling
     integer(ip)                    :: when_update_wet_geometry
     integer(ip)                    :: when_update_coupling
     character(5)                   :: variable
     real(rp)                       :: relax
     real(rp)                       :: aitken                         ! Aitken relaxation factor calculated in cou_update_values
     real(rp),    pointer           :: values(:,:,:)                  ! Relaxed exchanged quantity, allocated in mod_couplings
     real(rp),    pointer           :: values_frequ(:,:,:)            ! Values for the exchanged quantities in frequency mode
     real(rp),    pointer           :: values_predicted(:,:)          ! Unrelaxed exchanged quantity, allocated in mod_couplings
     type(typ_coupling_geometry)    :: geome
     type(comm_data_par)            :: commd
     type(typ_transmission)         :: transmission
  end type typ_color_coupling

  !----------------------------------------------------------------------
  !
  ! Read data
  !
  !----------------------------------------------------------------------

  integer(ip)                       :: mcoup                          ! Number of couplings
  integer(ip)                       :: mcoup_subdomain                ! Couplings involving subdomains
  integer(ip)                       :: coudt = -1_ip

  !----------------------------------------------------------------------
  !
  ! Definitions
  !
  !----------------------------------------------------------------------

  type(typ_color_coupling), pointer :: coupling_type(:)               ! Coupling array
  integer(ip)                       :: lun_coupl_dat                  ! Coupling data file
  integer(ip)                       :: lun_coupl_res                  ! Coupling result file
  integer(ip)                       :: lun_coupl_cvg                  ! Coupling result file
  integer(8)                        :: memor_cou(2)                   ! Memory
  real(rp),                 pointer :: resid_cou(:,:)                 ! residual
  type(typ_kdtree)                  :: kdtree_typ                     ! SKD-Tree structure
  type(comm_data_par),      pointer :: COU_COMM_COUPLING_ARRAY(:)     ! Sub communicator for projection type coupling
  real(rp)                          :: cputi_cou(20)                  ! CPU time

end module def_coupli
!> @} 
!-----------------------------------------------------------------------
