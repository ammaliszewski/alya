!------------------------------------------------------------------------
!> @addtogroup Coupling
!> @{
!> @name    Coupling functions
!> @file    mod_coupling_driver.f90
!> @author  Guillaume Houzeaux
!> @date    11/06/2014
!> @brief   Driver for coupling
!> @details Driver for coupling
!> @{
!------------------------------------------------------------------------

module mod_coupling_driver
  use def_kintyp, only : ip,rp,lg
  use def_master, only : modul
  use def_master, only : ittim
  use def_master, only : ID_NASTIN
  use def_master, only : ID_TEMPER
  use def_master, only : ID_NASTAL,IMASTER,ITASK_INIUNK
  use def_master, only : ID_SOLIDZ
  use def_master, only : ID_ALEFOR
  use def_master, only : ID_PARTIS
  use def_master, only : ID_KERMOD
  use def_master, only : ITASK_AFTER
  use mod_parall, only : I_AM_IN_COLOR
  use def_coupli, only : coupling_type
  use def_coupli, only : BETWEEN_ZONES
  use def_coupli, only : mcoup
  use mod_commdom_alya, only: CPLNG, CHT, CC
#ifdef COMMDOM 
#if    COMMDOM==1
  use mod_commdom_alya_cht, only: CHT_CPLNG, commdom_cht_driver_plepp 
#elif  COMMDOM==2
  use mod_commdom_driver,   only: CNT_CPLNG, commdom_driver_sendrecv
#elif  COMMDOM==3
  use mod_commdom_lmc,      only: LMC_CPLNG, commdom_lmc_driver_plepp
#elif  COMMDOM==4
  use mod_commdom_cc,       only: commdom_cc_driver_plepp
#endif 
#endif

#ifdef PRECICE 
  use mod_precice_driver, only: PRCC  
  use mod_precice_driver, only: precice_driver_sendrecv  
#endif 

  implicit none

contains

  subroutine COU_DRIVER(current_when,current_task)
    integer(ip),  intent(in)  :: current_when
    integer(ip),  intent(in)  :: current_task
    integer(ip)               :: icoup
    integer(ip)               :: ITASK_COUPL
    integer(ip)               :: module_source
    integer(ip)               :: module_target
    integer(ip)               :: color_source
    integer(ip)               :: color_target
    logical(lg)               :: i_compute_and_send
    logical(lg)               :: i_recv_and_assemble
    !
    ! Loop over couplings
    !
#ifndef COMMDOM 
#ifndef PRECICE  
    do icoup = 1,mcoup

       if( coupling_type(icoup) % kind == BETWEEN_ZONES ) then

          module_source = coupling_type(icoup) % module_source
          module_target = coupling_type(icoup) % module_target
          color_source  = coupling_type(icoup) % color_source
          color_target  = coupling_type(icoup) % color_target
          ITASK_COUPL   = icoup + 1000
          !
          ! Should I stay or should I go
          !
          ! Only activate coupling if the current time step is a multiple of the coupling frequency 
          !
          if(    current_task == coupling_type(icoup) % task_compute_and_send .and. &
               & current_when == coupling_type(icoup) % when_compute_and_send .and. &
               & I_AM_IN_COLOR(color_source).and. modul == module_source      .and. &
               & mod( ittim,coupling_type(icoup) % frequ_send ) == 0_ip )   then
             i_compute_and_send  = .true.
          else
             i_compute_and_send  = .false.
          end if
          if(    current_task == coupling_type(icoup) % task_recv_and_assemble .and. &
               & current_when == coupling_type(icoup) % when_recv_and_assemble .and. &
               & I_AM_IN_COLOR(color_target).and. modul == module_target       .and. &
               & mod( ittim,coupling_type(icoup) % frequ_recv ) == 0_ip)     then
             i_recv_and_assemble = .true.
          else
             i_recv_and_assemble = .false.
          end if
          !
          ! Call corresponding module (should call the plugin of the module_source/target and that's it)
          ! 
          if( module_source == modul .or. module_target == modul ) then
             if(  ( i_compute_and_send  .and. .not. i_recv_and_assemble ) .or. &
                & ( i_recv_and_assemble .and. .not. i_compute_and_send  ) ) then
                !
                ! I am source or target
                !
                if( i_compute_and_send ) then
                   call livinf(-14_ip,'-> SEND '//trim(coupling_type(icoup) % variable)//' AS A SOURCE FOR COUPLING ',icoup)
                else if( i_recv_and_assemble ) then
                   call livinf(-14_ip,'<- RECV '//trim(coupling_type(icoup) % variable)//' AS A TARGET FOR COUPLING ',icoup)
                end if

                select case ( modul )
                case ( ID_NASTIN )
                   call Nastin(ITASK_COUPL) 
                case ( ID_TEMPER )
                   call Temper(ITASK_COUPL)
                case ( ID_NASTAL )
                   call Nastal(ITASK_COUPL)
                case ( ID_SOLIDZ ) 
                   call Solidz(ITASK_COUPL)
                case ( ID_ALEFOR ) 
                   call Alefor(ITASK_COUPL)
                case ( ID_PARTIS )
                   call Partis(ITASK_COUPL)
                case ( ID_KERMOD ) 
                   call Kermod(ITASK_COUPL)
                case default
                   call runend('COU_DRIVER: MODULE NOT CODED')
                end select

             else if( i_compute_and_send .and. i_recv_and_assemble ) then

                call runend('COU_DRIVER: WE ARE IN TROUBLE')

             end if

          end if

       end if

    end do

#else 
  call precice_driver_sendrecv( PRCC, current_when, current_task )
#endif

#else 
#if    COMMDOM==1
  call commdom_cht_driver_plepp(CHT_CPLNG, current_when, current_task)
#elif  COMMDOM==2
  call commdom_driver_sendrecv(CNT_CPLNG, current_when, current_task)
  if(current_when==ITASK_AFTER) call Solidz(-1_ip) !-> call commdom_sld_plugin()
 !if(current_when==ITASK_AFTER) call Temper(-1_ip) !-> call commdom_xxx_plugin()
#elif  COMMDOM==3
  call commdom_lmc_driver_plepp(LMC_CPLNG, current_when, current_task)
#elif  COMMDOM==4
  call commdom_cc_driver_plepp(  CPLNG(CC), current_when, current_task)
#endif 
#endif 

  end subroutine COU_DRIVER


end module mod_coupling_driver
!> @} 
!-----------------------------------------------------------------------
