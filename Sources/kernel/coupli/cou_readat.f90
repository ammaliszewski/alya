!-----------------------------------------------------------------------
!> @addtogroup CoupliInput
!> @{
!> @file    ker_readat.f90
!> @author  Guillaume Houzeaux
!> @date    03/03/2014
!> @brief   Read coupling data
!> @details Read coupling data
!>          Three types of Dirichlet-type coupling, which lead
!>          to the same solution if meshes i and j coincide.
!>          \verbatim
!>
!>          Dirichlet explicit   Dirichlet implicit   Unknown
!>
!>          => x_i = x_j 
!>
!>          +-- do iter          +-- do iter          +-- do iter         
!>          |                    |                    |
!>          | Ax, x.y            ! A.x                ! A.x
!>          ! x.y                ! x.y                ! x.y
!>          |                    | x_i = x_j          | A.x|_i = A.x|_j
!>          |                    |                    |
!>          +-- end do           +-- end do           +-- end do
!>
!>          \endverbatim
!> @} 
!-----------------------------------------------------------------------

subroutine cou_readat()
  use def_kintyp
  use def_master
  use def_kermod
  use def_inpout
  use def_domain
  use def_coupli
  use mod_parall, only :  par_code_zone_subd_to_color
  implicit none
  integer(ip)          :: icoup,ipara,kblok
  character(5)         :: where_type_char

  if( INOTSLAVE .and. lun_coupl_dat /= 0 ) then
     !
     ! Physical problem
     !
     mcoup = 0
     !
     ! Numerical treatment 
     !

     !-------------------------------------------------------------------
     !
     ! Read/write unit
     !
     !-------------------------------------------------------------------

     lispa = 0
     lisda = lun_coupl_dat 
     lisre = lun_coupl_res
     rewind(lisda)

     !-------------------------------------------------------------------
     !
     ! Physical problem
     !
     !-------------------------------------------------------------------

     call ecoute('cou_readat')
     do while( words(1) /= 'PHYSI' )
        call ecoute('cou_readat')
     end do
     call ecoute('cou_readat')

     do while( words(1) /= 'ENDPH' )
        !
        ! Physical problem
        !
        if( words(1) == 'NUMBE' ) then

           mcoup = getint('NUMBE',1_ip,'#NUMBER OF COUPLINGS')
           call cou_memory(1_ip)

        else if( words(1) == 'COUPL' ) then

           icoup = getint('COUPL',1_ip,'#Number of the set or the field')
           if( icoup < 1 .or. icoup > mcoup )    call runend('COU_READAT: WRONG COUPLING NUMBER')
           if( .not. associated(coupling_type) ) call runend('COU_READAT: NUMBER_COUPLING TYPE IS MISSING')

           call ecoute('cou_readat')
           do while( words(1) /= 'ENDCO' )

              if(      words(1) == 'TARGE' .and. words(2) == 'MODUL' ) then
                 !
                 ! MODULE_TARGET
                 !
                 coupling_type(icoup) % module_target = idmod(words(3))
                 coupling_type(icoup) % zone_target   = lzone(coupling_type(icoup) % module_target)

              else if( words(1) == 'SOURC' .and. words(2) == 'MODUL' ) then
                 !
                 ! MODULE_SOURCE 
                 !
                 coupling_type(icoup) % module_source = idmod(words(3))

              else if( words(1) == 'VARIA' ) then
                 !
                 ! VARIABLE
                 !
                 coupling_type(icoup) % variable = trim(words(2))

              else if( words(1) == 'CONSE' ) then
                 !
                 ! CONSERVATION
                 !
                 if( words(2) == 'LOCAL' .or. words(2) == 'INTER' ) then
                    coupling_type(icoup) % conservation = INTERFACE_MASS
                 else if( words(2) == 'GLOBA' .or. words(2) == 'TOTAL' ) then
                    coupling_type(icoup) % conservation = GLOBAL_MASS
                 end if

              else if( words(1) == 'OVERL' ) then
                 !
                 ! VARIABLE
                 !
                 if( words(2) == 'DISJO' .or. words(2) == 'NO' ) then
                    coupling_type(icoup) % overlap = 0
                 else
                    coupling_type(icoup) % overlap = getint('OVERL',0_ip,'#OVERLAP FOR CHIMERA-TYPE COUPLING')
                 end if

              else if(      words(1) == 'TARGE' .and. words(2) == 'SUBDO' ) then
                 !
                 ! SUBDOMAIN_TARGET
                 !
                 coupling_type(icoup) % subdomain_target = getint('SUBDO',1_ip,'#TARGET SUBDOMAIN')

              else if( words(1) == 'SOURC' .and. words(2) == 'SUBDO' ) then
                 !
                 ! SUBDOMAIN_SOURCE
                 !
                 coupling_type(icoup) % subdomain_source = getint('SUBDO',1_ip,'#SOURCE SUBDOMAIN')

              else if( words(1) == 'TARGE' .and. words(2) == 'CODE ' ) then
                 !
                 ! CODE_TARGET
                 !
                 coupling_type(icoup) % code_target   = int(param(2),ip)

              else if( words(1) == 'SOURC' .and. words(2) == 'CODE ' ) then
                 !
                 ! CODE_SOURCE
                 !
                 coupling_type(icoup) % code_source   = int(param(2),ip)

              else if( words(1) == 'WHERE' ) then
                 !
                 ! WHERE
                 !
                 where_type_char = getcha('WHERE','     ','#Where')
                 coupling_type(icoup) % where_number = getint('NUMBE',1_ip,'#Number of the set or the field')
                 if( where_type_char == 'SET  ' ) then
                    coupling_type(icoup) % where_type = ON_SET
                 else if( where_type_char == 'FIELD' ) then
                    coupling_type(icoup) % where_type = ON_FIELD
                 else if( where_type_char == 'CODE ' ) then
                    coupling_type(icoup) % where_type = ON_CODE
                 else if( where_type_char == 'WHOLE' ) then
                    coupling_type(icoup) % where_type = ON_WHOLE_MESH
                 else if( where_type_char == 'CHIME' ) then
                    coupling_type(icoup) % where_type = ON_CHIMERA_MESH
                 else 
                    call runend('COU_READAT: WRONG WHERE')
                 end if

              else if( words(1) == 'WHAT ' ) then
                 !
                 ! WHAT
                 !
                 if(      words(2) == 'UNKNO' ) then
                    coupling_type(icoup) % what = UNKNOWN
                 else if( words(2) == 'DIRIC' ) then
                    if( words(3) == 'IMPLI' ) then
                       coupling_type(icoup) % what = DIRICHLET_IMPLICIT
                    else
                       coupling_type(icoup) % what = DIRICHLET_EXPLICIT
                    end if
                 else
                    coupling_type(icoup) % what = RESIDUAL
                 end if

              else if( words(1) == 'TYPE ' ) then
                 !
                 ! TYPE
                 !
                 if(      words(2) == 'ELEME' ) then
                    coupling_type(icoup) % itype = ELEMENT_INTERPOLATION
                 else if( words(2) == 'NEARE' ) then
                    if( words(3) == 'ELEME' ) then
                       coupling_type(icoup) % itype = NEAREST_ELEMENT_NODE
                    else
                       coupling_type(icoup) % itype = NEAREST_BOUNDARY_NODE
                    end if
                 else if( words(2) == 'BOUND' ) then
                    coupling_type(icoup) % itype = BOUNDARY_INTERPOLATION
                 else if( words(2) == 'STRES' ) then
                    coupling_type(icoup) % itype = STRESS_PROJECTION
                    if( exists('GAUSS') ) then
                       if( exists('DEFAU') ) then
                          coupling_type(icoup) % ngaus = 0
                       else if( exists('AUTOM') ) then
                          coupling_type(icoup) % ngaus = -1
                          call runend('COU_READAT: AUTOMATIC NOT CODED YET')
                       else
                          coupling_type(icoup) % ngaus = getint('GAUSS',1_ip,'#Number of Gauss points')
                       end if
                    end if
                 else if( words(2) == 'PROJE' ) then
                    coupling_type(icoup) % itype = PROJECTION
                    if( exists('GAUSS') ) then
                       if( exists('DEFAU') ) then
                          coupling_type(icoup) % ngaus = 0
                       else if( exists('AUTOM') ) then
                          coupling_type(icoup) % ngaus = -1
                          call runend('COU_READAT: AUTOMATIC NOT CODED YET')
                       else
                          coupling_type(icoup) % ngaus = getint('GAUSS',1_ip,'#Number of Gauss points')
                       end if
                    end if
                 else
                    call runend('COU_READAT: NON-EXISTING TYPE OF COUPLING')
                 end if

              else if( words(1) == 'RELAX' ) then
                 !
                 ! RELAX
                 !
                 coupling_type(icoup) % relax = getrea('RELAX',1.0_rp,'#Relaxation') 

              else if( words(1) == 'SCHEM' ) then
                 !
                 ! SCHEME
                 !
                 if( words(2) == 'RELAX' ) then
                    coupling_type(icoup) % scheme = RELAXATION_SCHEME
                 else if( words(2) == 'AITKE' ) then
                    coupling_type(icoup) % scheme = AITKEN_SCHEME
                 else
                    call runend('COU_READAT: NOT CODED')
                 end if

              else if( words(1) == 'SENDA' )then
                 !
                 ! COMPUTE & SEND
                 !
                 coupling_type(icoup) % frequ_send = 1_ip ! Initialization of frequency of send
                 if(      words(2) == 'INIUN' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_INIUNK
                 else if( words(2) == 'DOITE' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_DOITER
                 else if( words(2) == 'BEGIT' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_BEGITE
                 else if( words(2) == 'ENDIT' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_ENDITE
                 else if( words(2) == 'TURNO' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_TURNON
                 else if( words(2) == 'TURNF' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_TURNOF
                 else if( words(2) == 'CONCO' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_CONCOU
                 else if( words(2) == 'BEGZO' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_BEGZON
                 else if( words(2) == 'ENDZO' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_ENDZON
                 else if( words(2) == 'BEGST' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_BEGSTE
                 else if( words(2) == 'ENDST' ) then
                    coupling_type(icoup) % task_compute_and_send = ITASK_ENDSTE
                 end if

                 if( words(3) == 'BEFOR' ) then
                    coupling_type(icoup) % when_compute_and_send = ITASK_BEFORE
                 else if( words(3) == 'AFTER' ) then
                    coupling_type(icoup) % when_compute_and_send = ITASK_AFTER
                 end if

                 if( words(4) == 'FREQU' )then
                    !
                    ! Frequency of send for coupling
                    !
                    coupling_type(icoup) % frequ_send = getint('FREQU',1_ip,'#Frequency of send')
                 end if

              else if( words(1) == 'RECEI' ) then
                 !
                 ! RECEIVE & ASSEMBLE
                 !
                 coupling_type(icoup) % frequ_recv = 1_ip ! Initialization of frequency of recv
                 if(      words(2) == 'INIUN' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_INIUNK
                 else if( words(2) == 'DOITE' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_DOITER
                 else if( words(2) == 'BEGIT' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_BEGITE
                 else if( words(2) == 'ENDIT' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_ENDITE
                 else if( words(2) == 'TURNO' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_TURNON
                 else if( words(2) == 'TURNF' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_TURNOF
                 else if( words(2) == 'BEGZO' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_BEGZON
                 else if( words(2) == 'ENDZO' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_ENDZON
                 else if( words(2) == 'BEGST' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_BEGSTE
                 else if( words(2) == 'ENDST' ) then
                    coupling_type(icoup) % task_recv_and_assemble = ITASK_ENDSTE
                 end if

                 if( words(3) == 'BEFOR' ) then
                    coupling_type(icoup) % when_recv_and_assemble = ITASK_BEFORE
                 else if( words(3) == 'AFTER' ) then
                    coupling_type(icoup) % when_recv_and_assemble = ITASK_AFTER
                 end if
                 if( words(4) == 'FREQU' )then
                    !
                    ! Frequency of receive for coupling
                    !
                    coupling_type(icoup) % frequ_recv = getint('FREQU',1_ip,'#Frequency of recv')
                 end if

              end if
              call ecoute('cou_readat')
           end do

        end if
        call ecoute('cou_readat')

     end do

     !-------------------------------------------------------------------
     !
     ! Numerical treatment
     !
     !-------------------------------------------------------------------

     do while( words(1) /= 'NUMER' )
        call ecoute('cou_readat')
     end do
     call ecoute('cou_readat')
     do while( words(1) /= 'ENDNU' )

        if( words(1) == 'BLOCK' ) then
           !
           ! Block iterations
           !
           if( exists('NUMBE') ) then
              kblok = getint('NUMBE',1_ip,'#Number of the block')
           else
              kblok = int(param(1),ip)
           end if
           if( kblok < 1 .or. kblok > nblok ) call runend('COU_READAT: WRONG BLOCK NUMBER')
           if( kblok > max_block_cou )        call runend('COU_READAT: WRONG BLOCK NUMBER')

           call ecoute('cou_readat')
           do while( words(1) /= 'ENDBL' )

              if(      words(1) == 'COUPL' ) then            
                 if( nnpar > max_coupl_cou ) call runend('COU_READAT: WRONG NUMBER OF COUPLING FOR BLOCK DEFINITION')
                 coupling_driver_number_couplings(kblok) = nnpar
                 do ipara = 1,nnpar
                    coupling_driver_couplings(ipara,kblok) = int(param(ipara),ip)
                 end do
              else if( words(1) == 'ITERA' ) then
                 coupling_driver_max_iteration(kblok) = getint('ITERA',1_ip,'#Number of iterations')
              else if( words(1) == 'TOLER' ) then
                 coupling_driver_tolerance(kblok) = getrea('TOLER',1.0e-2_rp,'#Tolerance')
              end if

              call ecoute('cou_readat')
           end do

!        else if( words(1) == 'TIME' ) then
!           if( exists('NUMBE') ) then
!              kblok = getint('NUMBE',1_ip,'#Number of the block')
        end if

           if( exists('TIMES') ) coudt = 1

        call ecoute('cou_readat')
     end do

  end if

end subroutine cou_readat
 
