!------------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @name    Interpolaiton toolbox
!> @file    mod_parall.f90
!> @author  Guillaume Houzeaux
!> @date    03/03/2014
!> @brief   Interpolate values at some coordinates
!> @details Interpolate values at some coordinates. 
!>          Manage the color splitting.
!> @{
!------------------------------------------------------------------------

module mod_interpolation

  use def_kintyp,         only : ip,rp,lg,r1p,r2p,i1p
  use def_master,         only : ISEQUEN,INOTMASTER,kfl_paral
  use def_master,         only : current_code,kfl_timin
  use def_domain,         only : ndime,mnode,nbono
  use def_domain,         only : npoin,nelem,nnode
  use def_domain,         only : lnods,ltype,ltopo
  use def_domain,         only : coord,lnnod,lbono
  use def_domain,         only : mnodb,ltopo,ndimb
  use def_domain,         only : elmar,ngaus,lesub
  use def_domain,         only : lelez,nelez
  use def_domain,         only : lnnob
  use def_master,         only : I_AM_IN_SUBD
  use def_master,         only : I_AM_IN_ZONE
  use def_coupli,         only : typ_color_coupling
  use def_coupli,         only : ELEMENT_INTERPOLATION
  use def_coupli,         only : NEAREST_BOUNDARY_NODE
  use def_coupli,         only : BOUNDARY_INTERPOLATION
  use def_coupli,         only : PROJECTION
  use def_coupli,         only : STRESS_PROJECTION
  use def_coupli,         only : NEAREST_ELEMENT_NODE
  use def_coupli,         only : BOUNDARY_VECTOR_PROJECTION
  use def_coupli,         only : BETWEEN_SUBDOMAINS
  use def_coupli,         only : UNKNOWN
  use def_coupli,         only : DIRICHLET_EXPLICIT
  use def_coupli,         only : DIRICHLET_IMPLICIT
  use def_coupli,         only : memor_cou
  use def_coupli,         only : mcoup
  use def_coupli,         only : mcoup_subdomain
  use def_coupli,         only : cputi_cou
  use def_coupli,         only : nboun_cou
  use def_coupli,         only : lnodb_cou 
  use def_coupli,         only : ltypb_cou
  use def_coupli,         only : lnnob_cou
  use def_coupli,         only : lboel_cou
  use mod_parall,         only : par_bin_part
  use mod_parall,         only : par_bin_size
  use mod_parall,         only : par_bin_boxes
  use mod_parall,         only : par_bin_comin
  use mod_parall,         only : par_bin_comax
  use mod_parall,         only : PAR_COMM_CURRENT 
  use mod_parall,         only : PAR_COMM_COLOR_PERM
  use mod_parall,         only : PAR_MY_CODE_RANK
  use mod_parall,         only : PAR_COMM_COLOR
  use mod_parall,         only : par_part_in_color
  use mod_parall,         only : color_target
  use mod_parall,         only : color_source
  use mod_parall,         only : par_color_to_subd
  use mod_parall,         only : par_color_to_zone
  use mod_parall,         only : par_color_to_code
  use mod_parall,         only : I_AM_IN_COLOR
  use mod_memory,         only : memory_alloca 
  use mod_memory,         only : memory_deallo
  use mod_communications, only : PAR_COMM_RANK_AND_SIZE
  use mod_communications, only : PAR_SEND_RECEIVE
  use mod_communications, only : PAR_SEND_RECEIVE_TO_ALL
  use mod_communications, only : PAR_MAX
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  use mod_communications, only : PAR_BARRIER
  use mod_communications, only : PAR_START_NON_BLOCKING_COMM
  use mod_communications, only : PAR_SET_NON_BLOCKING_COMM_NUMBER
  use mod_communications, only : PAR_END_NON_BLOCKING_COMM
  use mod_integration_rules, only : integration_rules_trapezoidal
  use mod_elmgeo,            only : elmgeo_shapf_deriv_heslo
  implicit none
  
  private 
  !
  ! Interpolate point values from anywhere
  !
  interface COU_GET_INTERPOLATE_POINTS_VALUES
     module procedure COU_GET_INTERPOLATE_POINTS_VALUES_RP_0,&
          &           COU_GET_INTERPOLATE_POINTS_VALUES_RP_1,&
          &           COU_GET_INTERPOLATE_POINTS_VALUES_RP_2,&
          &           COU_GET_INTERPOLATE_POINTS_VALUES_RP_3
  end interface COU_GET_INTERPOLATE_POINTS_VALUES
  
  public :: COU_GET_INTERPOLATE_POINTS_VALUES                 ! Interpolate values afetr initialization
  public :: COU_TARGET_INTERFACE_MASS_MATRIX
  public :: COU_SOURCE_INTERFACE_MASS_MATRIX
  public :: COU_INITIALIZE_COUPLING_STRUCTURE
  public :: COU_DEALLOCATE_COUPLING_STRUCTURE_GEOME
  public :: COU_PROJECTION_TYPE

contains 
 
  subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP_0(ndofn,xx_in,xx_recv,coupling)
    implicit none
    integer(ip),                       intent(in)    :: ndofn
    real(rp),                          intent(in)    :: xx_in(ndofn,*)
    real(rp),                          intent(inout) :: xx_recv(ndofn,*)
    type(typ_color_coupling),          intent(inout) :: coupling

    call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,xx_in,xx_recv,coupling)       

  end subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP_0

  subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP_1(xx_in,xx_recv,coupling)
    implicit none
    real(rp),                 pointer, intent(in)    :: xx_in(:)
    real(rp),                 pointer, intent(inout) :: xx_recv(:)
    type(typ_color_coupling),          intent(inout) :: coupling
    real(rp)                                         :: yy_in(2)
    real(rp)                                         :: yy_recv(2)
    integer(ip)                                      :: ndofn
    ndofn = 1

    if( associated(xx_in) ) then
       if( size(xx_in) < npoin .and. PAR_MY_CODE_RANK /= 0 ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
    end if
    if( associated(xx_recv) ) then
       if( size(xx_recv) < coupling % geome % number_wet_points ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
       if( size(xx_recv) < coupling % commd % lrecv_dim         ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
    end if
    
       if( .not. associated(xx_in) .and. associated(xx_recv) ) then
          call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,yy_in,xx_recv,coupling)
       else if( .not. associated(xx_in) .and. .not. associated(xx_recv) ) then
          call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,yy_in,yy_recv,coupling)
       else if( associated(xx_in) .and. .not. associated(xx_recv) ) then
          call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,xx_in,yy_recv,coupling)
       else 
          call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,xx_in,xx_recv,coupling)
       end if
  end subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP_1

  subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP_2(xx_in,xx_recv,coupling)
    implicit none
    real(rp),                 pointer, intent(in)    :: xx_in(:,:)
    real(rp),                 pointer, intent(out)   :: xx_recv(:,:)
    type(typ_color_coupling),          intent(inout) :: coupling
    real(rp)                                         :: yy_in(2)
    real(rp)                                         :: yy_recv(2)
    integer(ip)                                      :: ndofn

    if( associated(xx_in) ) then
       if( size(xx_in,2) < npoin .and. PAR_MY_CODE_RANK /= 0 ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
    end if
    if( associated(xx_recv) ) then
       if( size(xx_recv,2) < coupling % geome % number_wet_points ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
       if( size(xx_recv,2) < coupling % commd % lrecv_dim         ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
    end if

    if( .not. associated(xx_in) .and. associated(xx_recv) ) then
       ndofn = size(xx_recv,1)
       call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,yy_in,xx_recv,coupling)
    else if( .not. associated(xx_in) .and. .not. associated(xx_recv) ) then
       ndofn = 1
       call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,yy_in,yy_recv,coupling)
    else if( associated(xx_in) .and. .not. associated(xx_recv) ) then
       ndofn = size(xx_in,1)
       call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,xx_in,yy_recv,coupling)
    else 
       ndofn = size(xx_in,1)
       if( ndofn /= size(xx_recv,1) .and. PAR_MY_CODE_RANK /= 0 ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
       call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,xx_in,xx_recv,coupling)
    end if

  end subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP_2

  subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP_3(xx_in,xx_recv,coupling)
    implicit none
    real(rp),                 pointer, intent(in)    :: xx_in(:,:,:)
    real(rp),                 pointer, intent(out)   :: xx_recv(:,:)
    type(typ_color_coupling),          intent(inout) :: coupling
    real(rp)                                         :: yy_in(2)
    real(rp)                                         :: yy_recv(2)
    integer(ip)                                      :: ndofn

    if( associated(xx_in) ) then
       if( size(xx_in,2) < npoin .and. PAR_MY_CODE_RANK /= 0 ) then
          call runend('WRONG SIZE: CANNOT INTERPOLATE')
       end if
    end if
    if( associated(xx_recv) ) then
       if( size(xx_recv,2) < coupling % geome % number_wet_points ) then
          call runend('WRONG SIZE: CANNOT INTERPOLATE')
       end if
       if( size(xx_recv,2) < coupling % commd % lrecv_dim  ) then
          call runend('WRONG SIZE: CANNOT INTERPOLATE')
       end if
    end if

    if( .not. associated(xx_in) .and. associated(xx_recv) ) then
       ndofn = size(xx_recv,1)
       call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,yy_in,xx_recv,coupling)
    else if( .not. associated(xx_in) .and. .not. associated(xx_recv) ) then
       ndofn = 1
       call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,yy_in,yy_recv,coupling)
    else if( associated(xx_in) .and. .not. associated(xx_recv) ) then
       ndofn = size(xx_in,1)
       call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,xx_in,yy_recv,coupling)
    else 
       ndofn = size(xx_in,1)
       if( ndofn /= size(xx_recv,1) .and. PAR_MY_CODE_RANK /= 0 ) call runend('WRONG SIZE: CANNOT INTERPOLATE')
       call COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,xx_in,xx_recv,coupling)
    end if

  end subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP_3

  !----------------------------------------------------------------------------------------------------------------
  !
  !             NPOIN                                NPOIN_WET              LRECV_DIM                   LRECV_DIM
  ! <----------------------------->                <----------->            <-------->                  <-------->
  ! +--+--+--+--+--+--+--+--+--+--+   LPOIN_WET    +--+--+--+--+   STATUS   +--+--+--+   SCHED_PERM     +--+--+--+
  ! |  |x |x |  |  |x |  |x |  |  |   <========    |1 |2 |0 |3 |  <=======  |1 |2 |3 |  <===========    |3 |1 | 2|
  ! +--+--+--+--+--+--+--+--+--+--+                +--+--+--+--+            +--+--+--+                  +--+--+--+
  !                                                   XX_OUT                XX_RECV_TMP                 XX_RECV_AUX
  !                
  !----------------------------------------------------------------------------------------------------------------

  subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP(ndofn,xx_in,xx_out,coupling,what_to_do)
    use def_master, only : kfl_paral
    implicit none
    integer(ip),              intent(in)           :: ndofn
    real(rp),                 intent(in)           :: xx_in(ndofn,*)
    real(rp),                 intent(out)          :: xx_out(ndofn,*)
    type(typ_color_coupling), intent(inout)        :: coupling
    character(*),             intent(in), optional :: what_to_do
    integer(ip)                                    :: kelem,ielem,kpoin
    integer(ip)                                    :: ipoin,idofn,inode
    integer(ip)                                    :: iboun,kboun,inodb
    integer(ip)                                    :: pnode,pnodb,jboun
    integer(ip)                                    :: pblty,igaub,kgaub
    integer(ip)                                    :: pgaub,ipoin_wet
    integer(ip)                                    :: dom_i,jelem,ineig
    integer(ip)                                    :: inise,nsend,finse
    integer(ip)                                    :: inire,nrecv,finre
    integer(ip)                                    :: jpoin
    integer(ip), pointer                           :: inv_perm(:)
    real(rp),    pointer                           :: xx_send(:,:)
    real(rp),    pointer                           :: xx_recv_tmp(:,:),xx_recv_aux(:,:)
    real(rp)                                       :: baloc(3,3),gbsur,gbdet
    real(rp)                                       :: bocod(ndime,mnodb),size_iboun
    real(rp)                                       :: dummr(2,2)
    logical(lg)                                    :: I_interpolate
    logical(lg)                                    :: I_send
    logical(lg)                                    :: I_receive
    logical(lg)                                    :: I_assemble
    real(rp)                                       :: time1,time2,time3,time4
    logical(lg)                                    :: non_blocking
    !
    ! Nullify pointers
    !
    nullify(xx_send)
    nullify(xx_recv_tmp)
    nullify(xx_recv_aux)
    !
    ! Decide what to do
    !
    I_interpolate = .false.
    I_receive     = .false.
    I_assemble    = .false.
    I_send        = .false.
    if( .not. present(what_to_do) ) then
       I_interpolate = .true.
       I_receive     = .true.
       I_assemble    = .true.
       I_send        = .true.       
    end if
    !
    ! Target and source colors
    !
    color_target = coupling % color_target
    color_source = coupling % color_source
    !
    ! Allocate
    !
    allocate( xx_recv_tmp(ndofn,max(1_ip,coupling % commd % lrecv_dim)))
    non_blocking = .true.
    if( non_blocking ) then
       call PAR_START_NON_BLOCKING_COMM(1_ip,coupling % commd % nneig)
       call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
    end if

    !--------------------------------------------------------------------
    !
    ! Compute values on wet nodes
    !
    !--------------------------------------------------------------------

    call cputim(time1) 

    if( coupling % itype == ELEMENT_INTERPOLATION ) then
       !
       ! Element interpolation
       !
       allocate( xx_send(ndofn,max(1_ip,coupling % geome % nelem_source)))  
       if( coupling % geome % nelem_source > 0 ) then
          if( non_blocking ) then
             kelem = 0
             do ineig = 1,coupling % commd % nneig
                dom_i = coupling % commd % neights(ineig)          
                inise = coupling % commd % lsend_size(ineig) 
                finse = coupling % commd % lsend_size(ineig+1) - 1
                nsend = finse - inise + 1
                do jelem = 1,nsend
                   kelem = kelem + 1
                   ielem = coupling % geome % lelem_source(kelem)
                   pnode = lnnod(ielem)
                   xx_send(1:ndofn,kelem) = 0.0_rp
                   do inode = 1,pnode
                      ipoin = lnods(inode,ielem)
                      xx_send(1:ndofn,kelem) = xx_send(1:ndofn,kelem) &
                           + coupling % geome % shapf(inode,kelem) * xx_in(1:ndofn,ipoin)
                   end do
                end do
                if( nsend /= 0 ) &
                     call PAR_SEND_RECEIVE(nsend*ndofn,0_ip,xx_send(1:ndofn,inise),dummr(1:2,1),'IN CURRENT COUPLING',dom_i,'NON BLOCKING')
             end do
          else
             do kelem = 1,coupling % geome % nelem_source
                ielem = coupling % geome % lelem_source(kelem)
                pnode = lnnod(ielem)
                xx_send(1:ndofn,kelem) = 0.0_rp
                do inode = 1,pnode
                   ipoin = lnods(inode,ielem)
                   xx_send(1:ndofn,kelem) = xx_send(1:ndofn,kelem) &
                        + coupling % geome % shapf(inode,kelem) * xx_in(1:ndofn,ipoin)
                end do
             end do
          end if
       end if

    else if( coupling % itype == NEAREST_BOUNDARY_NODE .or. coupling % itype == NEAREST_ELEMENT_NODE ) then
       !
       ! Nearest boundary/element node
       !
       allocate( xx_send(ndofn,max(1_ip,coupling % geome % npoin_source)))  
       if( coupling % geome % npoin_source > 0 ) then
          if( non_blocking ) then
             kpoin = 0
             do ineig = 1,coupling % commd % nneig
                dom_i = coupling % commd % neights(ineig)          
                inise = coupling % commd % lsend_size(ineig) 
                finse = coupling % commd % lsend_size(ineig+1) - 1
                nsend = finse - inise + 1
                do jpoin = 1,nsend
                   kpoin = kpoin + 1
                   ipoin = coupling % geome % lpoin_source(kpoin)
                   xx_send(1:ndofn,kpoin) = xx_in(1:ndofn,ipoin)
                end do
                if( nsend /= 0 ) &
                     call PAR_SEND_RECEIVE(nsend*ndofn,0_ip,xx_send(1:ndofn,inise),dummr(1:2,1),'IN CURRENT COUPLING',dom_i,'NON BLOCKING')
             end do
          else
             do kpoin = 1,coupling % geome % npoin_source
                ipoin = coupling % geome % lpoin_source(kpoin)
                xx_send(1:ndofn,kpoin) = xx_in(1:ndofn,ipoin)
             end do
          end if
       end if

    else if( coupling % itype == BOUNDARY_INTERPOLATION .or. coupling % itype == BOUNDARY_VECTOR_PROJECTION ) then
       !
       ! Boundary interpolation
       !
       allocate( xx_send(ndofn,max(1_ip,coupling % geome % nboun_source)))
       if( coupling % geome % nboun_source > 0 ) then
          if( non_blocking ) then
             kboun = 0
             do ineig = 1,coupling % commd % nneig
                dom_i = coupling % commd % neights(ineig)          
                inise = coupling % commd % lsend_size(ineig) 
                finse = coupling % commd % lsend_size(ineig+1) - 1
                nsend = finse - inise + 1
                do jboun = 1,nsend
                   kboun = kboun + 1
                   iboun = coupling % geome % lboun_source(kboun)
                   pnodb = lnnob_cou(iboun)
                   xx_send(1:ndofn,kboun) = 0.0_rp
                   do inodb = 1,pnodb
                      ipoin = lnodb_cou(inodb,iboun)
                      xx_send(1:ndofn,kboun) = xx_send(1:ndofn,kboun) &
                           + coupling % geome % shapf(inodb,kboun) * xx_in(1:ndofn,ipoin)
                   end do
                end do
                if( nsend /= 0 ) &
                   call PAR_SEND_RECEIVE(nsend*ndofn,0_ip,xx_send(1:ndofn,inise),dummr(1:2,1),'IN CURRENT COUPLING',dom_i,'NON BLOCKING')
             end do
          else
             do kboun = 1,coupling % geome % nboun_source
                iboun = coupling % geome % lboun_source(kboun)
                pnodb = lnnob_cou(iboun)
                xx_send(1:ndofn,kboun) = 0.0_rp
                do inodb = 1,pnodb
                   ipoin = lnodb_cou(inodb,iboun)
                   if( ipoin < 0 .or. ipoin > npoin ) then
                      call runend('COU_GET_INTERPOLATE_POINTS_VALUES_RP: WE ARE IN TROUBLE')
                   end if
                   xx_send(1:ndofn,kboun) = xx_send(1:ndofn,kboun) &
                        + coupling % geome % shapf(inodb,kboun) * xx_in(1:ndofn,ipoin)
                end do
             end do
          end if
       end if

    else if( coupling % itype == STRESS_PROJECTION ) then
       !
       ! Projection
       !
       !
       ! XX_IN                   XX_IN
       !  ||                       ||
       !  \/      XX_SEND          \/
       !  o---------x--------------o     <= IBOUN
       !         
       !  <------------------------>
       !          SIZE_IBOUN
       !
       !           PNODB
       !           +---
       !           \
       !  XX_SEND= /    XX_IN(INODB) * N_INODB(x) / MASS(IPOIN)
       !           +---
       !           INODB=1
       !
       !  N_INODB(x) is the shape function at the wet point position in boundary IBOUN
       !  N_INODB(x) = COUPLING % GEOME % SHAPF(INODB,KBOUN)
       !  KBOUN      = Local nummbering of global boundary IBOUN
       !  SIZE_IBOUN = Surface of IBOUN
       !
       allocate( xx_send(ndofn,max(1_ip,coupling % geome % nboun_source)) )
       if( coupling % geome % nboun_source > 0 ) then
          if( non_blocking ) then
             kboun = 0
             kgaub = 0
             do ineig = 1,coupling % commd % nneig
                dom_i = coupling % commd % neights(ineig)          
                inise = coupling % commd % lsend_size(ineig) 
                finse = coupling % commd % lsend_size(ineig+1) - 1
                nsend = finse - inise + 1
                do jboun = 1,nsend
                   kboun = kboun + 1
                   kgaub = kgaub + 1
                   iboun = coupling % geome % lboun_source(kboun)
                   pnodb = lnnob_cou(iboun)
                   xx_send(1:ndofn,kgaub) = 0.0_rp
                   do inodb = 1,pnodb
                      ipoin = lnodb_cou(inodb,iboun)
                      xx_send(1:ndofn,kgaub) = xx_send(1:ndofn,kgaub) &
                           + coupling % geome % shapf(inodb,kboun) &
                           * xx_in(1:ndofn,ipoin) / coupling % geome % vmasb(ipoin) 
                      !xx_send(1:ndofn,kgaub) = xx_send(1:ndofn,kgaub) &
                      !     + coupling % geome % shapf(inodb,kboun) &
                      !     * 1.0_rp / coupling % geome % vmasb(ipoin) 
                   end do
                end do
                if( nsend > 0 ) &
                     call PAR_SEND_RECEIVE(nsend*ndofn,0_ip,xx_send(1:ndofn,inise),dummr(1:2,1),'IN CURRENT COUPLING',dom_i,'NON BLOCKING')
             end do
          else
             kgaub = 0
             do kboun = 1,coupling % geome % nboun_source
                iboun = coupling % geome % lboun_source(kboun)
                pnodb = lnnob_cou(iboun)
                kgaub = kgaub + 1
                xx_send(1:ndofn,kgaub) = 0.0_rp
                do inodb = 1,pnodb
                   ipoin = lnodb_cou(inodb,iboun)
                   xx_send(1:ndofn,kgaub) = xx_send(1:ndofn,kgaub) &
                        + coupling % geome % shapf(inodb,kboun) &
                        * xx_in(1:ndofn,ipoin) / coupling % geome % vmasb(ipoin) 
                end do
             end do
          end if
       end if

    else if( coupling % itype == PROJECTION ) then
       !
       ! Projection
       !
       allocate( xx_send(ndofn,max(1_ip,coupling % geome % nboun_source)) )
       if( coupling % geome % nboun_source > 0 ) then
          if( non_blocking ) then
             kboun = 0
             kgaub = 0
             do ineig = 1,coupling % commd % nneig
                dom_i = coupling % commd % neights(ineig)          
                inise = coupling % commd % lsend_size(ineig) 
                finse = coupling % commd % lsend_size(ineig+1) - 1
                nsend = finse - inise + 1
                do jboun = 1,nsend
                   kboun = kboun + 1
                   kgaub = kgaub + 1
                   iboun = coupling % geome % lboun_source(kboun)
                   pnodb = lnnob_cou(iboun)
                   xx_send(1:ndofn,kgaub) = 0.0_rp
                   do inodb = 1,pnodb
                      ipoin = lnodb_cou(inodb,iboun)
                      xx_send(1:ndofn,kgaub) = xx_send(1:ndofn,kgaub) &
                           + coupling % geome % shapf(inodb,kboun) &
                           * xx_in(1:ndofn,ipoin) 
                   end do
                end do
                if( nsend > 0 ) &
                     call PAR_SEND_RECEIVE(nsend*ndofn,0_ip,xx_send(1:ndofn,inise),dummr(1:2,1),'IN CURRENT COUPLING',dom_i,'NON BLOCKING')
             end do
          else
             kgaub = 0
             do kboun = 1,coupling % geome % nboun_source
                iboun = coupling % geome % lboun_source(kboun)
                pnodb = lnnob_cou(iboun)
                kgaub = kgaub + 1
                xx_send(1:ndofn,kgaub) = 0.0_rp
                do inodb = 1,pnodb
                   ipoin = lnodb_cou(inodb,iboun)
                   xx_send(1:ndofn,kgaub) = xx_send(1:ndofn,kgaub) &
                        + coupling % geome % shapf(inodb,kboun) &
                        * xx_in(1:ndofn,ipoin) 
                end do
             end do
          end if
       end if

    end if

    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time2) 
    cputi_cou(7) = cputi_cou(7) + time2-time1

    !--------------------------------------------------------------------
    !
    ! Send and receive 
    !
    !--------------------------------------------------------------------

    if( non_blocking ) then
       do ineig = 1,coupling % commd % nneig
          dom_i = coupling % commd % neights(ineig)
          inire = coupling % commd % lrecv_size(ineig) 
          nrecv = ndofn * ( coupling % commd % lrecv_size(ineig+1) - coupling % commd % lrecv_size(ineig) ) 

          if( nrecv /= 0 ) &
               call PAR_SEND_RECEIVE(0_ip,nrecv,dummr(1:2,1),xx_recv_tmp(1:ndofn,inire),'IN CURRENT COUPLING',dom_i,'NON BLOCKING')
       end do
       call PAR_END_NON_BLOCKING_COMM(1_ip)
    else
       call PAR_SEND_RECEIVE_TO_ALL(ndofn,xx_send,xx_recv_tmp,coupling % commd,'ASYNCHRONOUS')
    end if

    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time3) 
    cputi_cou(8) = cputi_cou(8) + time3-time2
    !
    ! Scheduling permutation
    !
!!$    do ipoin = 1_ip, coupling % commd % lrecv_dim
!!$       kpoin = coupling % geome % sched_perm(ipoin)
!!$       if( kpoin > 0 ) then
!!$          xx_recv_aux(1:ndofn,ipoin) = xx_recv_tmp(1:ndofn,kpoin)
!!$       end if
!!$    end do
!!$    xx_recv_tmp = xx_recv_aux
    !--------------------------------------------------------------------
    !
    ! Copy solutions from what I've received from neighbors
    !
    !--------------------------------------------------------------------
    if( coupling % itype == STRESS_PROJECTION ) then
       call COU_PROJECTION(1_ip,coupling,ndofn,xx_recv_tmp,xx_out)
    else if( coupling % itype == PROJECTION ) then
       call COU_PROJECTION(2_ip,coupling,ndofn,xx_recv_tmp,xx_out)
    else
       ! Here wet point = wet node
       do ipoin = 1,coupling % geome % npoin_wet
          kpoin = coupling % geome % status(ipoin)
          if( kpoin > 0 ) then
             xx_out(1:ndofn,ipoin) = xx_recv_tmp(1:ndofn,kpoin)
          end if
       end do
    end if
    !
    ! Deallocate
    !
    if( associated(xx_send)     ) deallocate( xx_send   )
    if( associated(xx_recv_tmp) ) deallocate( xx_recv_tmp )

    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time4) 
    cputi_cou(9) = cputi_cou(9) + time4-time3

    ! call extrae_user_function(0_4)

  end subroutine COU_GET_INTERPOLATE_POINTS_VALUES_RP
 
  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Residual projection
  !> @details Residual projection. There are two possibilities.
  !>          F = nodal force
  !>          f = stress = F/S
  !>          
  !>          1. Find nodal stress on source fi,s = Fi,s / Mii
  !>
  !>          2. Compute force Fi = 
  !>                            +-
  !>                      Fi =  |  ( Ni,s f_source ) ds
  !>                           -+ source/target
  !>                            +-
  !>             Option 1:   =  |  ( Ni,t f_source ) ds  (f_source interpolated on target)
  !>                           -+ target
  !>                            +-
  !>             Option 2:   =  |  ( Ni,t Nj,s fj,s ) ds (Ni interpolated on source)
  !>                           -+ source
  !>
  !>          \verbatim
  !>
  !>          1. All on target
  !>          ----------------
  !>
  !>             1. On source: compute nodal stress fj_source = Fj_s / Mjj 
  !>             2. On source: interpolate f_source at target Gauss points
  !>             3. On target:
  !>             +-                   +-
  !>             | ( Ni Nj fj ) ds =  |  ( Ni f_source ) ds
  !>            -+ target            -+ target 
  !>                                  +-                      +--  +--  +--
  !>                            Fi =  |  ( Ni f_source ) ds = \    \    \   wg |J| Ni f_source
  !>                                 -+ source                /__  /__  /__
  !>                                                         iboun  i    g
  !>
  !>            NB: If a closed rule is selected and mesh coincide
  !>            
  !>                  +-                      
  !>            Fi =  |  ( Ni Nj Fj,s / Mjj ) ds = Ni Ni Fi,s / Mii * Mii = Fi,s
  !>                 -+ source            
  !>          
  !>
  !>             o-------o-------o-------o-------o source
  !>                  ||    ||        ||    ||  
  !>                  \/    \/        \/    \/  
  !>             x----g-----g----x----g-----g----x target
  !>
  !>              <--- iboun ---> <--- iboun --->
  !>                          
  !>          2. On source and target
  !>          -----------------------
  !>
  !>             +-                   +-
  !>             | ( Ni Nj fj ) ds =  |  ( Ni f_source ) ds
  !>            -+ target            -+ source 
  !>                                  +-
  !>                        Mii fi =  |  ( Ni F_source / M_source ) ds
  !>                                 -+ source 
  !>
  !>
  !>             o-g---g-o-------o-------o-------o source <= RHS computed here
  !>               /\
  !>             i || Ni
  !>             x---------------x---------------x target <= Mii computed here
  !>
  !>             The problem with this option is that in the latter
  !>             example all the CPUs of the source would require
  !>             the boundaries of the target as they all have
  !>             influence on the middle node
  !>
  !>             In this case, wet points are Gauss points
  !>
  !>          -------------------
  !>          1. Compute residual
  !>          -------------------
  !>
  !>                    CPU3                  CPU4
  !>          RA                  RB3' RB4'               RC
  !>          o---------------------o o-------------------o  source
  !>
  !>
  !>          --------------------
  !>          2. Exchange residual: Rb=Rb3+Rb4 
  !>          --------------------
  !>
  !>                    CPU3                  CPU4
  !>          RA                  RB3 RB4                 RC
  !>          o---------------------o o-------------------o  source
  !>          <=====================>
  !>                   SAB
  !>
  !>
  !>          ---------------------
  !>          3. Interpolate stress (at target Gauss points x)
  !>          --------------------- 
  !>
  !>                    CPU3                  CPU4
  !>
  !>          o----x------x---------o ox-----x-----------o  source
  !>           I(R)/SAB I(R)/SAB   I(R)/SBC I(R)/SBC                          
  !>
  !>
  !>          ------------------
  !>          4. Assemble stress (integrate on target)
  !>          ------------------ 
  !>                       
  !>                 CPU1               CPU2
  !>          ra            rb1' rb2'            rc
  !>          o----x------x----o o----x------x----o         target
  !>
  !>
  !>          ------------------
  !>          5. Exchange stress: rb = rb1' + rb2'
  !>          ------------------ 
  !>                       
  !>                 CPU1               CPU2
  !>          ra              rb rb            rc
  !>          o----x------x----o o----x------x----o         target
  !>
  !>          \endverbatim
  !>
  !----------------------------------------------------------------------

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Assemble Gauss point contributions
  !> @details 
  !>           X_RECV_TMP
  !>              ||
  !>              \/        XX_OUT(NPOIN_WET)
  !>          o---g-----g---o
  !>
  !>
  !----------------------------------------------------------------------

  subroutine COU_PROJECTION(itask,coupling,ndofn,xx_recv_tmp,xx_out)
    integer(ip),              intent(in)  :: itask
    type(typ_color_coupling), intent(in)  :: coupling
    integer(ip),              intent(in)  :: ndofn
    real(rp),        pointer, intent(in)  :: xx_recv_tmp(:,:)
    real(rp),                 intent(out) :: xx_out(ndofn,*)
    integer(ip)                           :: iboun_global,iboun_wet,nboun_wet
    integer(ip)                           :: pblty,pnodb,pgaub,igaub
    integer(ip)                           :: inodb,ipoin,kgaub,jgaub
    integer(ip)                           :: ipoin_wet,npoin_wet,ipoin_global
    real(rp)                              :: baloc(3,3),gbsur,gbdet
    real(rp)                              :: bocod(ndime,mnodb)

!!$    real(rp), pointer :: xx(:,:)
!!$    if( coupling % geome % npoin_wet > 0 ) then
!!$       !
!!$       ! Initalize residual
!!$       !
!!$       allocate(xx(ndofn,npoin))
!!$       npoin_wet = coupling % geome % npoin_wet
!!$       do ipoin = 1,npoin
!!$          xx(1:ndofn,ipoin) = 0.0_rp
!!$       end do
!!$       jgaub = 0      
!!$       do iboun_wet = 1,coupling % geome % nboun_wet          
!!$          iboun_global = coupling % geome % lboun_wet(iboun_wet)
!!$          pblty        = abs(ltypb_cou(iboun_global))
!!$          pnodb        = lnnob_cou(iboun_global)
!!$          pgaub        = coupling % geome % proje_target(iboun_wet) % pgaub
!!$          do igaub = 1,pgaub
!!$             jgaub = jgaub + 1
!!$             kgaub = coupling % geome % status(jgaub) ! Permutation of wet point             
!!$             gbsur = coupling % geome % proje_target(iboun_wet) % gbsur(igaub)
!!$             do inodb = 1,pnodb
!!$                ipoin = lnodb_cou(inodb,iboun_global)
!!$                ipoin_wet = coupling % geome % proje_target(iboun_wet) % permu(inodb)
!!$                xx(1:ndofn,ipoin) = xx(1:ndofn,ipoin) &
!!$                     & + gbsur * xx_recv_tmp(1:ndofn,kgaub) &
!!$                     & * coupling % geome % proje_target(iboun_wet) % shapb(inodb,igaub)
!!$             end do
!!$          end do          
!!$       end do       
!!$       if( current_code == 2 ) then
!!$          call PAR_INTERFACE_NODE_EXCHANGE(xx,'SUM','IN MY CODE')
!!$       end if       
!!$       do ipoin_wet = 1,npoin_wet
!!$          ipoin = coupling % geome % lpoin_wet(ipoin_wet)
!!$          xx_out(1:ndofn,ipoin_wet) = xx(1:ndofn,ipoin)
!!$       end do
!!$       deallocate( xx )
!!$       if( itask == 2 ) then
!!$          do ipoin_wet = 1,npoin_wet
!!$             xx_out(1:ndofn,ipoin_wet) = xx_out(1:ndofn,ipoin_wet) / coupling % geome % vmasb_wet(ipoin_wet)
!!$          end do
!!$       end if
!!$    end if

    if( coupling % geome % npoin_wet > 0 ) then
       !
       ! Initalize residual
       !
       npoin_wet = coupling % geome % npoin_wet
       do ipoin_wet = 1,npoin_wet
          xx_out(1:ndofn,ipoin_wet) = 0.0_rp
       end do

       jgaub = 0

       do iboun_wet = 1,coupling % geome % nboun_wet
          
          iboun_global = coupling % geome % lboun_wet(iboun_wet)
          pblty        = abs(ltypb_cou(iboun_global))
          pnodb        = lnnob_cou(iboun_global)
          pgaub        = coupling % geome % proje_target(iboun_wet) % pgaub
          do igaub = 1,pgaub
             jgaub = jgaub + 1
             kgaub = coupling % geome % status(jgaub) ! Permutation of wet point             
             gbsur = coupling % geome % proje_target(iboun_wet) % gbsur(igaub)
             do inodb = 1,pnodb
                ipoin_wet = coupling % geome % proje_target(iboun_wet) % permu(inodb)
                xx_out(1:ndofn,ipoin_wet) = xx_out(1:ndofn,ipoin_wet) &
                     & + gbsur * xx_recv_tmp(1:ndofn,kgaub) &
                     & * coupling % geome % proje_target(iboun_wet) % shapb(inodb,igaub)
             end do
          end do
          
       end do

       if( itask == 2 ) then
          do ipoin_wet = 1,npoin_wet
             xx_out(1:ndofn,ipoin_wet) = xx_out(1:ndofn,ipoin_wet) / coupling % geome % vmasb_wet(ipoin_wet)
          end do
       end if

    end if

  end subroutine COU_PROJECTION

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Compute mass matrix for this coupling
  !> @details Compute mass matrix for this coupling
  !>
  !----------------------------------------------------------------------

  subroutine COU_TARGET_INTERFACE_MASS_MATRIX(coupling)
    type(typ_color_coupling), intent(inout) :: coupling
    integer(ip)                             :: iboun,kboun,nboun_wet
    integer(ip)                             :: pblty,pnodb,pgaub,igaub
    integer(ip)                             :: inodb,ipoin,npoin_wet,kpoin
    integer(ip)                             :: ipoin_wet
    real(rp)                                :: baloc(3,3),gbsur,gbdet
    real(rp)                                :: bocod(ndime,mnodb)
    real(rp),                 pointer       :: vmasb(:)

    nboun_wet = coupling % geome % nboun_wet
    npoin_wet = coupling % geome % npoin_wet

    if( associated(coupling % geome % vmasb_wet) ) then
       call memory_deallo(memor_cou,'VMASB','cou_interface_mass_matrix',&
            coupling % geome % vmasb_wet)
    end if
    call memory_alloca(memor_cou,'VMASB','cou_interface_mass_matrix',&
         coupling % geome % vmasb_wet,coupling % geome % npoin_wet)

    allocate( vmasb(npoin) )
    do ipoin = 1,npoin
       vmasb(ipoin) = 0.0_rp
    end do

    do kboun = 1,nboun_wet
 
       iboun = coupling % geome % lboun_wet(kboun)
       pblty = ltypb_cou(iboun)

       if( pblty > 0 ) then

          pnodb = lnnob_cou(iboun)
          pgaub = ngaus(pblty)
          do inodb = 1,pnodb
             ipoin = lnodb_cou(inodb,iboun)
             bocod(1:ndime,inodb) = coord(1:ndime,ipoin)
          end do

          do igaub = 1,pgaub
             call bouder(&
                  pnodb,ndime,ndimb,elmar(pblty) % deriv(1,1,igaub),&
                  bocod,baloc,gbdet)
             gbsur = elmar(pblty) % weigp(igaub) * gbdet 
             do inodb = 1,pnodb
                ipoin = lnodb_cou(inodb,iboun)
                vmasb(ipoin) = vmasb(ipoin) + gbsur * elmar(pblty) % shape(inodb,igaub)
                !kpoin = list_wet_nodes(ipoin)
                !coupling % geome % vmasb_wet(kpoin) = coupling % geome % vmasb_wet(kpoin) &
                !     + gbsur * elmar(pblty) % shape(inodb,igaub)
             end do
          end do

       end if

    end do
    !
    ! OJO: mejorar para que el intercambio se haga solamente en este color
    ! y que sea del tamano npoin-wet...mmmm
    !
    call PAR_INTERFACE_NODE_EXCHANGE(vmasb,'SUM','IN MY CODE')
    do ipoin_wet = 1,coupling % geome % npoin_wet
       ipoin = coupling % geome % lpoin_wet(ipoin_wet)
       coupling % geome % vmasb_wet(ipoin_wet) = vmasb(ipoin) 
    end do

    deallocate(vmasb)
    
  end subroutine COU_TARGET_INTERFACE_MASS_MATRIX

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Compute mass matrix for this coupling
  !> @details Compute mass matrix for this coupling
  !>
  !----------------------------------------------------------------------

  subroutine COU_SOURCE_INTERFACE_MASS_MATRIX(coupling,mirror_coupling)
    type(typ_color_coupling), intent(inout) :: coupling
    type(typ_color_coupling), intent(inout) :: mirror_coupling
    integer(ip)                             :: iboun,kboun,nboun_wet
    integer(ip)                             :: pblty,pnodb,pgaub,igaub
    integer(ip)                             :: inodb,ipoin,npoin_wet,kpoin
    integer(ip)                             :: ipoin_wet,iboun_wet
    real(rp)                                :: baloc(3,3),gbsur,gbdet
    real(rp)                                :: bocod(ndime,mnodb)
    !
    ! Allocate memory
    !
    if( associated(coupling % geome % vmasb) ) then
       call memory_deallo(memor_cou,'VMASB','cou_interface_mass_matrix',coupling % geome % vmasb)
    end if
    call memory_alloca(memor_cou,'VMASB','cou_interface_mass_matrix',coupling % geome % vmasb,npoin) 

    do iboun_wet = 1,mirror_coupling % geome % nboun_wet
       iboun = mirror_coupling % geome % lboun_wet(iboun_wet) 
       pblty = abs(ltypb_cou(iboun))
       pgaub = ngaus(pblty) 
       pnodb = lnnob_cou(iboun)
       do inodb = 1,pnodb
          ipoin = lnodb_cou(inodb,iboun)
          bocod(1:ndime,inodb) = coord(1:ndime,ipoin)
       end do
       do igaub = 1,pgaub
          call bouder(&
               pnodb,ndime,ndimb,elmar(pblty) % deriv(1,1,igaub),&
               bocod,baloc,gbdet)                   
          gbsur = elmar(pblty) % weigp(igaub) * gbdet 
          do inodb = 1,pnodb
             ipoin = lnodb_cou(inodb,iboun)
             coupling % geome % vmasb(ipoin) = coupling % geome % vmasb(ipoin) &
                  + gbsur * elmar(pblty) % shape(inodb,igaub) 
          end do
       end do
    end do
    !
    ! OJO: mejorar para que el intercambio se haga solamente en este color
    ! y que sea del tamano npoin-wet...mmmm
    !
    call PAR_INTERFACE_NODE_EXCHANGE(coupling % geome % vmasb,'SUM','IN MY CODE')
    
  end subroutine COU_SOURCE_INTERFACE_MASS_MATRIX

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Initialize coupling structure
  !> @details Initialize coupling structure
  !>
  !----------------------------------------------------------------------

  subroutine COU_INITIALIZE_COUPLING_STRUCTURE(coupling)
    type(typ_color_coupling), intent(out) :: coupling
    
    coupling % geome % nelem_source = 0
    coupling % geome % nboun_source = 0
    coupling % geome % npoin_source = 0
    coupling % commd % nneig        = 0
    coupling % commd % lsend_dim    = 0
    coupling % commd % lrecv_dim    = 0
    nullify( coupling % commd % neights      )
    nullify( coupling % commd % lsend_size   )
    nullify( coupling % commd % lrecv_size   )
    nullify( coupling % geome % lelem_source )
    nullify( coupling % geome % lboun_source )
    nullify( coupling % geome % lpoin_source )
    nullify( coupling % geome % shapf        )
    nullify( coupling % geome % status       )
    
  end subroutine COU_INITIALIZE_COUPLING_STRUCTURE

  !---------------------------------------------------------------------- 
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Deallocate coupling structure
  !> @details Deallocate coupling structure
  !>
  !----------------------------------------------------------------------

  subroutine COU_DEALLOCATE_COUPLING_STRUCTURE(coupling)
    type(typ_color_coupling), intent(out) :: coupling
    
    coupling % geome % nelem_source = 0
    coupling % geome % nboun_source = 0
    coupling % geome % npoin_source = 0
    coupling % commd % nneig        = 0
    coupling % commd % lsend_dim    = 0
    coupling % commd % lrecv_dim    = 0
    call memory_deallo(memor_cou,'NEIGHTS'   ,'DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % neights      )
    call memory_deallo(memor_cou,'LSEND_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % lsend_size   )
    call memory_deallo(memor_cou,'LRECV_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % lrecv_size   )
    call memory_deallo(memor_cou,'LELEM'     ,'DEALLOCATE_COUPLING_STRUCTURE',coupling % geome % lelem_source )
    call memory_deallo(memor_cou,'LBOUN'     ,'DEALLOCATE_COUPLING_STRUCTURE',coupling % geome % lboun_source )
    call memory_deallo(memor_cou,'LPOIN'     ,'DEALLOCATE_COUPLING_STRUCTURE',coupling % geome % lpoin_source )
    call memory_deallo(memor_cou,'SHAPF'     ,'DEALLOCATE_COUPLING_STRUCTURE',coupling % geome % shapf        )
    call memory_deallo(memor_cou,'STATUS'    ,'DEALLOCATE_COUPLING_STRUCTURE',coupling % geome % status       )
    
  end subroutine COU_DEALLOCATE_COUPLING_STRUCTURE
  
  !---------------------------------------------------------------------- 
  !
  !> @author  Matias Rivero
  !> @date    16/12/2014
  !> @brief   Deallocate coupling structure (geome)
  !> @details Deallocate coupling structure (geome)
  !>
  !----------------------------------------------------------------------

  subroutine COU_DEALLOCATE_COUPLING_STRUCTURE_GEOME(coupling)
    type(typ_color_coupling), intent(out) :: coupling
    integer(ip)                           :: iboun_wet

    coupling % geome % ndime     = 0

    coupling % geome % nelem_source = 0
    coupling % geome % nboun_source = 0
    coupling % geome % npoin_source = 0

    call memory_deallo(memor_cou,'LELEM' , 'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % lelem_source )
    call memory_deallo(memor_cou,'LBOUN' , 'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % lboun_source )
    call memory_deallo(memor_cou,'LPOIN' , 'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % lpoin_source )
    call memory_deallo(memor_cou,'STATUS', 'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % status       )
    call memory_deallo(memor_cou,'SHAPF' , 'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % shapf        )
    
    call memory_deallo(memor_cou,'VMASB' , 'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % vmasb        )

    coupling % geome % number_wet_points = 0
    coupling % geome % npoin_wet = 0
    coupling % geome % nboun_wet = 0
    call memory_deallo(memor_cou,'VMASB_WET','DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % vmasb_wet  )
    call memory_deallo(memor_cou,'LBOUN',    'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % lboun_wet  )
    call memory_deallo(memor_cou,'LPOIN',    'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % lpoin_wet  )
    call memory_deallo(memor_cou,'COORD',    'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % coord_wet  )
    call memory_deallo(memor_cou,'WEIGH',    'DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % weight_wet )

    call memory_deallo(memor_cou,'SCHED','DEALLOCATE_COUPLING_STRUCTURE_GEOME',coupling % geome % sched_perm )
    
    if( associated(coupling % geome % proje_target) ) then
       do iboun_wet = 1,size(coupling % geome % proje_target)
          if( associated(coupling % geome % proje_target(iboun_wet) % permu) ) deallocate(coupling % geome % proje_target(iboun_wet) % permu)
          if( associated(coupling % geome % proje_target(iboun_wet) % shapb) ) deallocate(coupling % geome % proje_target(iboun_wet) % shapb)
          if( associated(coupling % geome % proje_target(iboun_wet) % gbsur) ) deallocate(coupling % geome % proje_target(iboun_wet) % gbsur)
       end do
       deallocate(coupling % geome % proje_target)
    end if

  end subroutine COU_DEALLOCATE_COUPLING_STRUCTURE_GEOME
 
  !---------------------------------------------------------------------- 
  !
  !> @author  Matias Rivero
  !> @date    16/12/2014
  !> @brief   Deallocate coupling structure (commd)
  !> @details Deallocate coupling structure (commd)
  !>
  !----------------------------------------------------------------------

  subroutine COU_DEALLOCATE_COUPLING_STRUCTURE_COMMD(coupling)
    type(typ_color_coupling), intent(out) :: coupling

    coupling % commd % nneig     = 0
    coupling % commd % npoi1     = 0
    coupling % commd % npoi2     = 0
    coupling % commd % npoi3     = 0
    coupling % commd % npoi4     = 0

    coupling % commd % bound_dim     = 0
    coupling % commd % lsend_dim     = 0
    coupling % commd % lrecv_dim     = 0
    coupling % commd % bface_dim     = 0
    coupling % commd % ghost_send_node_dim     = 0
    coupling % commd % ghost_recv_node_dim     = 0
    coupling % commd % ghost_send_elem_dim     = 0
    coupling % commd % ghost_recv_elem_dim     = 0
    coupling % commd % ghost_send_boun_dim     = 0
    coupling % commd % ghost_recv_boun_dim     = 0
    coupling % commd % PAR_COMM_WORLD          = 0

    call memory_deallo(memor_cou,'NEIGHTS'   ,'DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % neights      )
    call memory_deallo(memor_cou,'BOUND_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % bound_size   )
    call memory_deallo(memor_cou,'BOUND_PERM','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % bound_perm   )
    call memory_deallo(memor_cou,'BOUND_ADJA','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % bound_adja   )
    call memory_deallo(memor_cou,'BOUND_SCAL','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % bound_scal   )
    call memory_deallo(memor_cou,'LSEND_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % lsend_size   )
    call memory_deallo(memor_cou,'LRECV_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % lrecv_size   )

    call memory_deallo(memor_cou,'BFACE_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % bface_size   )
    call memory_deallo(memor_cou,'BFACE_PERM','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % bface_perm   )

    call memory_deallo(memor_cou,'GHOST_SEND_NODE_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_send_node_size   )
    call memory_deallo(memor_cou,'GHOST_SEND_NODE_PERM','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_send_node_perm   )
    call memory_deallo(memor_cou,'GHOST_RECV_NODE_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_recv_node_size   )
    call memory_deallo(memor_cou,'GHOST_RECV_NODE_PERM','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_recv_node_perm   )
    call memory_deallo(memor_cou,'GHOST_SEND_ELEM_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_send_elem_size   )
    call memory_deallo(memor_cou,'GHOST_SEND_ELEM_PERM','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_send_elem_perm   )
    call memory_deallo(memor_cou,'GHOST_RECV_ELEM_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_recv_elem_size   )
    call memory_deallo(memor_cou,'GHOST_RECV_ELEM_PERM','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_recv_elem_perm   )
    call memory_deallo(memor_cou,'GHOST_SEND_BOUN_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_send_boun_size   )
    call memory_deallo(memor_cou,'GHOST_SEND_BOUN_PERM','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_send_boun_perm   )
    call memory_deallo(memor_cou,'GHOST_RECV_BOUN_SIZE','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_recv_boun_size   )
    call memory_deallo(memor_cou,'GHOST_RECV_BOUN_PERM','DEALLOCATE_COUPLING_STRUCTURE',coupling % commd % ghost_recv_boun_perm   )

  end subroutine COU_DEALLOCATE_COUPLING_STRUCTURE_COMMD

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    11/03/2014
  !> @brief   Check in which elements are the test points
  !> @details For each partition whcih have received some points,
  !>          check which of them host them
  !>
  !>          1. MASK(IPOIN) = 1 (x): they are the nodes the source 
  !>             is in charge of
  !>
  !>          o---------x---------x---------o---------o
  !>            iboun=1   iboun=2   iboun=3   iboun=4
  !>
  !>          2. Mark boundaries connected to x: iboun=1,2,3
  !>
  !>          3. Mark nodes (X) connected to marked boundaries
  !>
  !>          X---------X---------X---------X---------o
  !>            iboun=1   iboun=2   iboun=3   iboun=4
  !>
  !
  !----------------------------------------------------------------------

  subroutine COU_CREATE_A_SUBMESH(mask,coupling)

    integer(ip),              intent(in)  :: mask(:)
    type(typ_color_coupling), intent(out) :: coupling
    integer(ip)                           :: nboun_wet,iboun,ipoin,kboun
    integer(ip)                           :: pblty,inodb,pnodb,kpoin
    integer(ip)                           :: iboun_wet
    integer(ip),              pointer     :: mask_nboun(:)
    integer(ip),              pointer     :: mask_npoin(:)

    nboun_wet = coupling % geome % nboun_wet

    nullify(mask_nboun)
    nullify(mask_npoin)

    if( nboun_wet > 0 ) then
       !
       ! allocate memory
       !
       allocate( mask_nboun(nboun_wet) )
       allocate( mask_npoin(npoin) )
       do ipoin = 1,npoin
          mask_npoin(ipoin) = 0
       end do
       do iboun_wet = 1,nboun_wet
          mask_nboun(iboun) = 0
       end do
       !
       ! If node is marked, mark connected boundaries and corresponding nodes
       !
       kboun = 0
       kpoin = 0
       do iboun_wet = 1,nboun_wet
          iboun = coupling % geome % lboun_wet(iboun_wet) 
          pnodb = nnode(abs(ltypb_cou(iboun)))
          inodb = 0
          do while( inodb < pnodb )
             inodb = inodb + 1
             ipoin = lnodb_cou(inodb,iboun)
             if( mask(ipoin) == 1 ) then
                do inodb = 1,pnodb
                   ipoin = lnodb_cou(inodb,iboun)
                   if( mask_npoin(ipoin) == 0 ) then
                      kpoin = kpoin + 1
                      mask_npoin(ipoin) = kpoin
                   end if
                end do
                inodb = pnodb
                kboun = kboun + 1
                mask_nboun(iboun_wet) = kboun
             end if
          end do
       end do
       !
       ! Allocate memory
       !
       coupling % geome % npoin_target = kpoin
       coupling % geome % nboun_target = kboun
       coupling % geome % mnodb_target = mnodb
       call memory_alloca(memor_cou,'LNODB_TARGET','COU_CREATE_A_SUBMESH',coupling % geome % lnodb_target,mnodb,kboun)
       call memory_alloca(memor_cou,'LNODB_TARGET','COU_CREATE_A_SUBMESH',coupling % geome % ltypb_target,kboun)
       call memory_alloca(memor_cou,'COORD_TARGET','COU_CREATE_A_SUBMESH',coupling % geome % coord_target,ndime,npoin)
       !
       ! Copy marked boundary mesh
       !
       kpoin = 0
       do ipoin = 1,npoin
          kpoin = mask_npoin(ipoin) 
          if( kpoin /= 0 ) then
             coupling % geome % coord_target(1:ndime,kpoin) = coord(1:ndime,ipoin)
          end if
       end do

       kboun = 0
       do iboun_wet = 1,nboun_wet
          if( mask_nboun(iboun_wet) /= 0 ) then
             kboun = kboun + 1
             iboun = coupling % geome % lboun_wet(iboun_wet)
             pblty = ltypb_cou(iboun)
             pnodb = nnode(abs(pblty))
             coupling % geome % ltypb_target(kboun) = pblty
             do inodb = 1,pnodb
                ipoin = lnodb_cou(inodb,iboun)
                coupling % geome % lnodb_target(inodb,kboun) = mask_npoin(ipoin)
             end do
          end if
       end do
       !
       ! Deallocate memory
       !
       deallocate(mask_nboun)
       deallocate(mask_npoin)

    end if

  end subroutine COU_CREATE_A_SUBMESH

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    17/04/2014
  !> @brief   Give the list of elements in a given color
  !> @details Give the list of elements in a given color
  !>
  !----------------------------------------------------------------------

  subroutine COU_ELEMENTS_IN_COLOR(icolo,necol,lecol)
    integer(ip),          intent(in)  :: icolo 
    integer(ip),          intent(out) :: necol
    logical(lg), pointer, intent(out) :: lecol(:)
    integer(ip)                       :: isubd,izone,icode
    integer(ip)                       :: kelem,ielem

    if( INOTMASTER ) then
       isubd = par_color_to_subd(icolo)
       izone = par_color_to_zone(icolo)
       icode = par_color_to_code(icolo)
       necol = 0
       do ielem = 1,nelem
          lecol(ielem) = .false.
       end do

       if( I_AM_IN_SUBD(isubd) .and. icode == current_code .and. I_AM_IN_ZONE(izone) ) then
          do kelem = 1,nelez(izone)
             ielem = lelez(izone) % l(kelem)
             lecol(ielem) = .true.
          end do
          do ielem = 1,nelem
             if( lesub(ielem) == isubd .and. lecol(ielem) ) then
                necol = necol + 1
             else
                lecol(ielem) = .false.
             end if
          end do
       end if
    end if

  end subroutine COU_ELEMENTS_IN_COLOR

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    17/04/2014
  !> @brief   Give the list of nodes in a given color
  !> @details Give the list of nodes in a given color
  !>
  !----------------------------------------------------------------------

  subroutine COU_NODES_IN_COLOR(icolo,nncol,lncol)
    integer(ip),          intent(in)  :: icolo 
    integer(ip),          intent(out) :: nncol
    logical(lg), pointer, intent(out) :: lncol(:)
    integer(ip)                       :: isubd,izone,icode
    integer(ip)                       :: kelem,ielem,necol
    integer(ip)                       :: inode,ipoin
    logical(lg), pointer              :: lecol(:)

    if( INOTMASTER ) then

       isubd = par_color_to_subd(icolo)
       izone = par_color_to_zone(icolo)
       icode = par_color_to_code(icolo)
       nncol = 0
       do ipoin = 1,npoin
          lncol(ipoin) = .false.
       end do

       if( I_AM_IN_SUBD(isubd) .and. icode == current_code .and. I_AM_IN_ZONE(izone) ) then
          allocate( lecol(nelem) )
          necol = 0
          do ielem = 1,nelem
             lecol(ielem) = .false.
          end do
          do kelem = 1,nelez(izone)
             ielem = lelez(izone) % l(kelem)
             lecol(ielem) = .true.
          end do
          do ielem = 1,nelem
             if( lesub(ielem) == isubd .and. lecol(ielem) ) then
                necol = necol + 1
             else
                lecol(ielem) = .false.
             end if
          end do
          deallocate( lecol )
          if( necol > 0 ) then
             do ielem = 1,nelem
                if( lecol(ielem) ) then
                   do inode = 1,lnnod(ielem)
                      ipoin = lnods(inode,ielem)
                      lncol(ipoin) = .true.
                   end do
                end if
             end do
          end if
       end if
    end if

  end subroutine COU_NODES_IN_COLOR

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    17/04/2014
  !> @brief   Give the list of boundaries in a given color
  !> @details Give the list of boundaries in a given color
  !>
  !----------------------------------------------------------------------

  subroutine COU_BOUNDARIES_IN_COLOR(icolo,nbcol,lbcol)
    integer(ip),          intent(in)  :: icolo 
    integer(ip),          intent(out) :: nbcol
    logical(lg), pointer, intent(out) :: lbcol(:)
    integer(ip)                       :: isubd,izone,icode
    integer(ip)                       :: kelem,ielem,necol
    integer(ip)                       :: inode,iboun,pnodb
    logical(lg), pointer              :: lecol(:)

    if( INOTMASTER ) then

       call runend('LBOEL_COU DOES NOT EXIST!')
       isubd = par_color_to_subd(icolo)
       izone = par_color_to_zone(icolo)
       icode = par_color_to_code(icolo)
       nbcol = 0
       do iboun = 1,nboun_cou
          lbcol(iboun) = .false.
       end do

       if( I_AM_IN_SUBD(isubd) .and. icode == current_code .and. I_AM_IN_ZONE(izone) ) then
          allocate( lecol(nelem) )
          necol = 0
          do ielem = 1,nelem
             lecol(ielem) = .false.
          end do
          do kelem = 1,nelez(izone)
             ielem = lelez(izone) % l(kelem)
             lecol(ielem) = .true.
          end do
          do ielem = 1,nelem
             if( lesub(ielem) == isubd .and. lecol(ielem) ) then
                necol = necol + 1
             else
                lecol(ielem) = .false.
             end if
          end do
          deallocate( lecol )
          if( necol > 0 ) then
             do iboun = 1,nboun_cou
                pnodb = lnnob_cou(iboun)
                ielem = lboel_cou(pnodb+1,iboun)
                lbcol(iboun) = lecol(ielem)
             end do
          end if
       end if
    end if
 
  end subroutine COU_BOUNDARIES_IN_COLOR

  subroutine COU_PROJECTION_TYPE(coupling)
    type(typ_color_coupling), intent(inout)    :: coupling
    integer(ip)                                :: nboun_wet,number_wet_points
    integer(ip)                                :: iboun_wet,iboun_global,pblty
    integer(ip), allocatable                   :: inv_perm(:) 
    integer(ip)                                :: ipoin_wet,ipoin_global,pgaub
    integer(ip)                                :: inodb,pnodb,igaub
    real(rp)                                   :: gbsur,baloc(9),bocod(ndime,mnodb)
    real(rp)                                   :: shapb(mnodb),derib(ndimb,mnodb)
    real(rp)                                   :: gbdet
    real(rp),    allocatable                   :: posgp(:,:),weigp(:)
    !
    ! INV_PERM: temporary permutation array
    !
    allocate( inv_perm(npoin) )
    do ipoin_global = 1,npoin
       inv_perm(ipoin_global) = 0
    end do
    do ipoin_wet = 1,coupling % geome % npoin_wet
       ipoin_global = coupling % geome % lpoin_wet(ipoin_wet)
       inv_perm(ipoin_global) = ipoin_wet
    end do
    !
    ! Compute projection type for each wet boundary
    !
    nboun_wet = coupling % geome % nboun_wet
    allocate(coupling % geome % proje_target(nboun_wet) )

    if( coupling % ngaus == 0 ) then
       !
       ! Use kernel default number of Gauss points
       !
       do iboun_wet = 1,nboun_wet
          iboun_global = coupling % geome % lboun_wet(iboun_wet)
          pblty        = abs(ltypb_cou(iboun_global))
          pgaub        = ngaus(pblty)
          pnodb        = nnode(pblty)

          coupling % geome % proje_target(iboun_wet) % pgaub = pgaub
          allocate(coupling % geome % proje_target(iboun_wet) % shapb(pnodb,pgaub) )
          allocate(coupling % geome % proje_target(iboun_wet) % gbsur(pgaub) )
          allocate(coupling % geome % proje_target(iboun_wet) % permu(pnodb) )

          do inodb = 1,pnodb
             ipoin_global = lnodb_cou(inodb,iboun_global)
             bocod(1:ndime,inodb) = coord(1:ndime,ipoin_global)
          end do
          do igaub = 1,pgaub
             call bouder(&
                  pnodb,ndime,ndimb,elmar(pblty) % deriv(1,1,igaub),bocod,baloc,gbdet)                
             gbsur = elmar(pblty) % weigp(igaub) * gbdet 
             coupling % geome % proje_target(iboun_wet) % gbsur(igaub) = gbsur
             do inodb = 1,pnodb
                ipoin_global = lnodb_cou(inodb,iboun_global)
                if( ipoin_global == 0 ) call runend('COU_DEFINE_WET_GEOMETRY: WE ARE IN TROUBLE')
                ipoin_wet = inv_perm(ipoin_global)
                coupling % geome % proje_target(iboun_wet) % permu(inodb) = ipoin_wet
                coupling % geome % proje_target(iboun_wet) % shapb(inodb,igaub) = elmar(pblty) % shape(inodb,igaub)
             end do
          end do
       end do
    else
       !
       ! Use a user-prescribed number of Gauss points
       !
       pgaub = coupling % ngaus
       allocate(posgp(ndimb,pgaub),weigp(pgaub)) 
       call integration_rules_trapezoidal(ndimb,pgaub,posgp,weigp)
       do iboun_wet = 1,nboun_wet
          iboun_global = coupling % geome % lboun_wet(iboun_wet)
          pblty        = abs(ltypb_cou(iboun_global))
          pnodb        = nnode(pblty)
          do inodb = 1,pnodb
             ipoin_global = lnodb_cou(inodb,iboun_global)
             bocod(1:ndime,inodb) = coord(1:ndime,ipoin_global)
          end do

          coupling % geome % proje_target(iboun_wet) % pgaub = pgaub
          allocate(coupling % geome % proje_target(iboun_wet) % shapb(pnodb,pgaub) )
          allocate(coupling % geome % proje_target(iboun_wet) % gbsur(pgaub) )
          allocate(coupling % geome % proje_target(iboun_wet) % permu(pnodb) )

          do igaub = 1,pgaub
             call elmgeo_shapf_deriv_heslo(ndimb,pnodb,posgp(1,igaub),shapb,derib)
             call bouder(pnodb,ndime,ndimb,derib,bocod,baloc,gbdet)         
             gbsur = weigp(igaub) * gbdet 
             coupling % geome % proje_target(iboun_wet) % gbsur(igaub) = gbsur
             do inodb = 1,pnodb
                ipoin_global = lnodb_cou(inodb,iboun_global)
                if( ipoin_global == 0 ) call runend('COU_DEFINE_WET_GEOMETRY: WE ARE IN TROUBLE')
                ipoin_wet = inv_perm(ipoin_global)
                coupling % geome % proje_target(iboun_wet) % permu(inodb) = ipoin_wet
                coupling % geome % proje_target(iboun_wet) % shapb(inodb,igaub) = shapb(inodb)
             end do
          end do

       end do
       deallocate(posgp,weigp)
    end if
   
    deallocate(inv_perm)
    
  end subroutine COU_PROJECTION_TYPE

end module mod_interpolation
!> @}
