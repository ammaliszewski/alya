!------------------------------------------------------------------------
!> @addtogroup Coupling
!> @{
!> @name    Coupling arrays
!> @file    cou_memory.f90
!> @author  Guillaume Houzeaux
!> @date    03/03/2014
!> @brief   Allocate memory
!> @details Allocate memory and nullify pointers
!> @}
!------------------------------------------------------------------------

subroutine cou_memory(itask)
  use def_kintyp, only : ip,rp
  use def_master, only : AT_BEGINNING
  use def_coupli, only : RELAXATION_SCHEME
  use def_coupli, only : UNKNOWN
  use def_coupli, only : mcoup
  use def_coupli, only : coupling_type
  use def_coupli, only : kfl_gozon
  use def_coupli, only : resid_cou
  use def_coupli, only : memor_cou
  use def_coupli, only : coupling_driver_couplings
  use def_coupli, only : coupling_driver_max_iteration       
  use def_coupli, only : coupling_driver_iteration           
  use def_coupli, only : coupling_driver_tolerance            
  use mod_memory, only : memory_alloca
  use mod_kdtree, only : kdtree_initialize
  use mod_parall, only : PAR_INITIALIZE_COMMUNICATION_ARRAY  
  implicit none 
  integer(ip), intent(in) :: itask
  integer(ip)             :: icoup

  select case ( itask )

  case ( 1_ip ) 

     allocate( coupling_type(mcoup) )
     call memory_alloca(memor_cou,'RESID_COU','cou_memory',resid_cou,2_ip,mcoup)

     do icoup = 1,mcoup
        !
        ! Variables read in cou_readat.f90
        !
        coupling_type(icoup) % itype            = 0
        coupling_type(icoup) % code_source      = 1
        coupling_type(icoup) % code_target      = 1
        coupling_type(icoup) % zone_source      = 0
        coupling_type(icoup) % zone_target      = 0
        coupling_type(icoup) % color_source     = 0
        coupling_type(icoup) % color_target     = 0
        coupling_type(icoup) % module_source    = 0
        coupling_type(icoup) % module_target    = 0
        coupling_type(icoup) % subdomain_source = 0
        coupling_type(icoup) % subdomain_target = 0
        coupling_type(icoup) % where_type       = 0
        coupling_type(icoup) % where_number     = 0       
        coupling_type(icoup) % what             = UNKNOWN      
        coupling_type(icoup) % scheme           = RELAXATION_SCHEME     
        coupling_type(icoup) % relax            = 1.0_rp      
        coupling_type(icoup) % itera            = 0 
        coupling_type(icoup) % conservation     = 0 
        coupling_type(icoup) % overlap          = 0    ! Disjoint subdomains for Chimera-type 
        coupling_type(icoup) % ngaus            = 0    ! 0: kernel default integration, >0: number of Gauss points

        coupling_type(icoup) % task_compute_and_send    = -100
        coupling_type(icoup) % when_compute_and_send    = -100
        coupling_type(icoup) % task_recv_and_assemble   = -100
        coupling_type(icoup) % when_recv_and_assemble   = -100
        coupling_type(icoup) % when_update_wet_geometry =  AT_BEGINNING
        coupling_type(icoup) % when_update_coupling     =  AT_BEGINNING
        coupling_type(icoup) % variable                 =  '     '
        !
        ! Variables computed
        !
        coupling_type(icoup) % kind             = 0 
        coupling_type(icoup) % mirror_coupling  = 0 

        nullify(coupling_type(icoup) % values)
        nullify(coupling_type(icoup) % values_predicted)
        nullify(coupling_type(icoup) % values_frequ)
 
        coupling_type(icoup) % geome % ndime        = 0
        coupling_type(icoup) % geome % nelem_source = 0
        coupling_type(icoup) % geome % nboun_source = 0
        coupling_type(icoup) % geome % npoin_source = 0

        nullify(coupling_type(icoup) % geome % lelem_source) 
        nullify(coupling_type(icoup) % geome % lboun_source) 
        nullify(coupling_type(icoup) % geome % lpoin_source) 
        nullify(coupling_type(icoup) % geome % shapf) 
        nullify(coupling_type(icoup) % geome % vmasb) 

        call kdtree_initialize(coupling_type(icoup) % geome % kdtree)

        coupling_type(icoup) % geome % number_wet_points = 0
        coupling_type(icoup) % geome % npoin_wet  = 0
        coupling_type(icoup) % geome % nboun_wet  = 0

        nullify(coupling_type(icoup) % geome % lboun_wet)
        nullify(coupling_type(icoup) % geome % lpoin_wet)
        nullify(coupling_type(icoup) % geome % coord_wet)
        nullify(coupling_type(icoup) % geome % weight_wet)
        nullify(coupling_type(icoup) % geome % vmasb_wet)
        nullify(coupling_type(icoup) % geome % mass_ratio_wet)
        nullify(coupling_type(icoup) % geome % status)
        nullify(coupling_type(icoup) % geome % sched_perm)
        
        coupling_type(icoup) % geome % mnodb_target = 0
        coupling_type(icoup) % geome % nboun_target = 0
        coupling_type(icoup) % geome % npoin_target = 0

        nullify(coupling_type(icoup) % geome % coord_target)
        nullify(coupling_type(icoup) % geome % lnodb_target)
        nullify(coupling_type(icoup) % geome % ltypb_target)
        nullify(coupling_type(icoup) % geome % lperm_target)
        nullify(coupling_type(icoup) % geome % proje_target)
        !
        ! Communicator
        !
        call PAR_INITIALIZE_COMMUNICATION_ARRAY(coupling_type(icoup) % commd)
        !
        ! Transmission arrays
        !
        !nullify(coupling_type(icoup) % transmission % lperm_target )
        !nullify(coupling_type(icoup) % transmission % lperm_source )
        nullify(coupling_type(icoup) % transmission % ia_matrix    )
        nullify(coupling_type(icoup) % transmission % ja_matrix    )
        nullify(coupling_type(icoup) % transmission % aa_matrix    )
     end do

  end select

end subroutine cou_memory
