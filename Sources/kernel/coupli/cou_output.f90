!----------------------------------------------------------------------
!> @addtogroup Coupli
!> @{
!> @file    cou_output.f90
!> @author  Guillaume Houzeaux
!> @date    18/06/2014
!> @brief   Output coupling 
!> @details Output some information about the couplinga
!> @} 
!----------------------------------------------------------------------

subroutine cou_output()
  use def_kintyp
  use def_parame
  use mod_parall
  use def_domain
  use def_master
  use def_coupli
  use mod_communications
  implicit none  
  integer(ip)                  :: ielem,ipoin
  integer(ip)                  :: ipart,idime,dummi,ineig
  integer(ip)                  :: icode,ipart_world,icoup
  integer(ip)                  :: npoin_interior,kelem,jelem
  integer(ip)                  :: npoin_boundary,ii,jneig
  integer(ip)                  :: number_wet_points,mneig
  integer(ip)                  :: npoin_wet         
  integer(ip)                  :: nboun_wet         
  integer(ip)                  :: nneig_source     
  integer(ip)                  :: istep,nstep,part1,part2     
  type(comm_data_par), pointer :: commu
  integer(4)                   :: PAR_COMM_TO_USE
  real(rp)                     :: dummr,xaver,xx
  real(rp)                     :: xmini(3)
  real(rp)                     :: xmaxi(3)
  real(rp)                     :: y

  real(rp)                     :: xcoor(3)
  integer(ip)                  :: nneig
  integer(ip), pointer         :: lneig(:)
  real(rp),    pointer         :: xcoor_gat(:,:)
  integer(ip), pointer         :: nneig_gat(:)
  integer(4),  pointer         :: nneig4_gat(:)
  integer(ip), pointer         :: lneig_gat(:)
  integer(ip), pointer         :: lresu_gat(:)
  integer(ip), pointer         :: lcode_gat(:)
  type(i1p),   pointer         :: ledge(:)
  integer(ip), pointer         :: comm_last(:)
  integer(ip), pointer         :: comm_matrix(:,:)

  integer(ip) :: lun_coupli_msh
  integer(ip) :: lun_coupli_res

  return

  lun_coupli_msh = 101
  lun_coupli_res = 102

  if( ISEQUEN ) return

  call PAR_DEFINE_COMMUNICATOR('IN MY CODE',PAR_COMM_TO_USE,commu)

  nneig         =  0
  nullify( lneig      )
  nullify( xcoor_gat  )
  nullify( nneig_gat  )
  nullify( nneig4_gat )
  nullify( lneig_gat  )
  nullify( lresu_gat  )
  nullify( lcode_gat  )
  nullify( ledge      )
  nullify( comm_last  )
  nullify( comm_matrix)

  if( PAR_MY_WORLD_RANK == 0 ) then
     allocate( xcoor_gat (3,0:PAR_WORLD_SIZE-1) )
     allocate( nneig_gat (  0:PAR_WORLD_SIZE-1) )
     allocate( nneig4_gat(  0:PAR_WORLD_SIZE-1) )
     allocate( lresu_gat (  0:PAR_WORLD_SIZE-1) )
     allocate( lcode_gat (  0:PAR_WORLD_SIZE-1) )
     allocate( ledge     (  0:PAR_WORLD_SIZE-1) )
     do ipart = 0,PAR_WORLD_SIZE-1 
        nullify(ledge(ipart) % l)
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! Couplings
  !
  !----------------------------------------------------------------------

  do icoup = 1,mcoup
     color_target      = coupling_type(icoup) % color_target
     color_source      = coupling_type(icoup) % color_source
     number_wet_points = coupling_type(icoup) % geome % number_wet_points
     npoin_wet         = coupling_type(icoup) % geome % npoin_wet
     nboun_wet         = coupling_type(icoup) % geome % nboun_wet
     nneig_source      = coupling_type(icoup) % commd % nneig
     !
     ! Geometry
     !
     call PAR_GATHER(nneig_source,nneig_gat,'IN THE WORLD') 

     if( PAR_MY_WORLD_RANK == 0 ) then        
        dummi = 0
        do ipart = 0,PAR_WORLD_SIZE-1 
           dummi = dummi + nneig_gat(ipart)
           nneig4_gat(ipart) = int(nneig_gat(ipart),4)
        end do
        if( associated(lneig_gat) ) deallocate( lneig_gat )
        allocate( lneig_gat(dummi) )
     else
        if( INOTMASTER ) then
           if( associated(lneig) ) deallocate( lneig )
           allocate( lneig(coupling_type(icoup) % commd % nneig) )
           do ineig = 1,coupling_type(icoup) % commd % nneig   
              ipart        = coupling_type(icoup) % commd % neights(ineig)
              ipart_world  = PAR_COMM_COLOR_PERM(color_target,color_source,ipart)
              lneig(ineig) = ipart_world
           end do
        end if
     end if

     call PAR_GATHERV(lneig,lneig_gat,nneig4_gat,'IN THE WORLD')

     if( PAR_MY_WORLD_RANK == 0 ) then        
        write(lun_coupli_msh,'(a,i1,a)') 'MESH COUPLING'//trim(intost(icoup))//' dimension ',ndime,' Elemtype Linear Nnode 2'
        write(lun_coupli_msh,'(a)') 'coordinates'
        do ipart = 0,PAR_WORLD_SIZE-1
           mneig = mneig + nneig_gat(ipart)
        end do
        allocate( comm_matrix(mneig,0:PAR_WORLD_SIZE-1) ) 
        allocate( comm_last(0:PAR_WORLD_SIZE-1) )
        do ipart = 0,PAR_WORLD_SIZE-1
           comm_last(ipart) = 1
           do ineig = 1,mneig
              comm_matrix(ineig,ipart) = 0
           end do
        end do

        jneig = 0
        nstep = 0
        do while( jneig < mneig )
           nstep = nstep + 1
           kelem = 0
           do ipart = 0,PAR_WORLD_SIZE-1
              ineig = comm_last(ipart)
              if( ineig <= nneig_gat(ipart) ) then
                 !
                 ! ipart+1 ========= lneig_gat(kelem)+1
                 !
                 kelem = kelem + ineig
                 part1 = ipart 
                 part2 = lneig_gat(kelem) 
                 if( comm_matrix(nstep,part1) == 0 .and. comm_matrix(nstep,part2) == 0 ) then
                    jneig                    = jneig + 2
                    comm_matrix(nstep,part1) = part2
                    comm_matrix(nstep,part2) = part1             
                    comm_last(ipart)         = comm_last(ipart) + 1
                 end if
              end if
           end do
        end do

        write(lun_coupli_msh,'(a)') 'coordinates'
        ipoin = 0
        y     = 0.0_rp
        do istep = 1,nstep
           do ipart = 0,PAR_WORLD_SIZE-1
              if( comm_matrix(nstep,part1) /= 0 ) then
                 part2 = comm_matrix(istep,part1)
                 comm_matrix(istep,part2) = 0
                 y     = y - 1.0_rp
                 ipoin = ipoin + 1 ; write(lun_coupli_msh,'(i5,3(1x,e12.6))') ipoin,real(part1,rp),y
                 ipoin = ipoin + 1 ; write(lun_coupli_msh,'(i5,3(1x,e12.6))') ipoin,real(part2,rp),y
              end if
           end do
           y = y - 2.0_rp
        end do
        write(lun_coupli_msh,'(a)') 'end coordinates'

        write(lun_coupli_msh,'(a,i1,a)') 'MESH COUPLING'//trim(intost(icoup))//' dimension ',ndime,' Elemtype Linear Nnode 2'
        write(lun_coupli_msh,'(a)') 'elements'
        ielem = 0
        ipoin = 0
        do istep = 1,nstep
           do ipart = 0,PAR_WORLD_SIZE-1
              if( comm_matrix(istep,part1) /= 0 ) then
                 part2 = comm_matrix(istep,part1)
                 ielem = ielem + 1
                 write(lun_coupli_msh,'(3(1x,i7))') ielem,ipoin+1,ipoin+2
                 ipoin = ipoin + 2
              end if
           end do
        end do

        write(lun_coupli_msh,'(a)') 'end elements' 

     end if
  end do
  !
  ! Deallocate
  !
  if( associated(lneig)      ) deallocate( lneig      )
  if( associated(xcoor_gat)  ) deallocate( xcoor_gat  )
  if( associated(nneig_gat)  ) deallocate( nneig_gat  )
  if( associated(nneig4_gat) ) deallocate( nneig4_gat )
  if( associated(lneig_gat)  ) deallocate( lneig_gat  )
  if( associated(lresu_gat)  ) deallocate( lresu_gat  )
  if( associated(lcode_gat)  ) deallocate( lcode_gat  )
  if( associated(ledge)      ) deallocate( ledge      )

  if( PAR_MY_WORLD_RANK == 0 ) then
     close(lun_coupli_msh)
     close(lun_coupli_res)     
  end if
 
end subroutine cou_output

subroutine cou_output2(lun_pores_par)
  use def_master
  use def_domain
  use def_coupli
  use mod_parall
  use mod_memory
  use mod_communications
  use mod_interpolation  
  use mod_meshes, only   :  meshes_submesh
  implicit none
  integer(ip), intent(in) :: lun_pores_par
  integer(ip), pointer    :: nneig_gat(:)
  integer(ip)             :: icoup,ipart


  if( mcoup > 0 ) then

     nullify(nneig_gat)
     if( IMASTER ) then
        allocate( nneig_gat(0:PAR_WORLD_SIZE-1) )
     end if

     do icoup = 1,mcoup

        call PAR_GATHER(coupling_type(icoup) % commd % nneig,nneig_gat,'IN THE WORLD')
        if( IMASTER ) then
           write(lun_pores_par,'(a)') 'Result NNEIG_COUPLING'//trim(intost(icoup))//' ANALYSIS 0 Scalar OnNodes'
           write(lun_pores_par,'(a)') 'ComponentNames NNEIG_COUPLING'//trim(intost(icoup))
           write(lun_pores_par,'(a)') 'Values'   
           do ipart = 1,PAR_WORLD_SIZE-1 
              write(lun_pores_par,'(2(1x,i7))') ipart,nneig_gat(ipart)
           end do
           write(lun_pores_par,'(a)') 'End Values'   
        end if
        
!!$        do ineig = 1,coupling_type(icoup) % commd % nneig
!!$           dom_i      = commu % neights(ineig)
!!$           npoin_recv = lrecv_size(ineig+1) - commu % lrecv_size(ineig)
!!$        end do
!!$        write(lun_pores_par,'(a)')  'GaussPoints GP Elemtype Linear'
!!$        write(lun_pores_par,'(a)')  'Number of Gauss Points: 1'
!!$        write(lun_pores_par,'(a)')  'Natural Coordinates: Internal'
!!$        write(lun_pores_par,'(a)')  'End GaussPoints'
!!$        write(lun_pores_par,'(a)')  'Result COUPLING_'//trim(intost(icoup))//' ANALYSIS 0 Scalar OnNodes'
!!$        write(lun_pores_par,'(a)')  'ComponentNames COUPLING_'//trim(intost(icoup))
!!$        write(lun_pores_par,'(a)')  'Values'
!!$        !
!!$        ! Gather number wet nodes/boundaries 
!!$        !
!!$        call PAR_GATHER(npoin_sub,npoin_gat,'IN THE WORLD')
!!$        call PAR_GATHER(nboun_sub,nboun_gat,'IN THE WORLD')
!!$        
!!$        do ipart = 1,npart_par  
!!$           ielem=0 
!!$           do dom_i = 1, nbcol
!!$              if (lcomm_par(dom_i,ipart)/=0) then
!!$                 ielem = ielem+1
!!$              end if
!!$           end do
!!$           write(lun_pores_par,'(2(1x,i7))') ipart,ielem
!!$        end do
!!$        write(lun_pores_par,'(a)') 'End Values'   
 
     end do

  end if

  if( IMASTER ) then
     deallocate( nneig_gat )
  end if

end subroutine cou_output2

subroutine cou_output3()
  use def_master
  use def_domain
  use def_coupli
  use mod_parall
  use mod_memory
  use mod_communications
  use mod_interpolation  
  use mod_meshes, only   :  meshes_submesh
  implicit none
  integer(ip)            :: icoup,kelem,ielem,ipoin,kpoin,ineig
  integer(ip)            :: inode,icolo,dom_i,isize,jcolo,ipart
  integer(ip)            :: npoin_wet,nboun_wet,idime,itotn
  integer(ip)            :: inodb,iboun,kboun
  integer(ip)            :: ipoin_total,iboun_total

  integer(4),    pointer :: recvcounts4(:)
  integer(4),    pointer :: recvcounts42(:)

  integer(ip),   pointer :: npoin_gat(:)
  integer(ip),   pointer :: nboun_gat(:)
  type(r2p),     pointer :: coord_gat(:)
  type(i2p),     pointer :: lnodb_gat(:)
  type(i1p),     pointer :: ltypb_gat(:)
  type(i1p),     pointer :: lnnob_gat(:)
  type(i1p),     pointer :: lnper_gat(:)

  real(rp),      pointer :: coord_recv(:)
  integer(ip),   pointer :: lnodb_recv(:)
  integer(ip),   pointer :: ltypb_recv(:)
  integer(ip),   pointer :: lnnob_recv(:)

  integer(ip)            :: npoin_sub
  integer(ip)            :: nboun_sub
  real(rp),      pointer :: coord_sub(:,:)
  integer(ip),   pointer :: lnodb_sub(:,:)
  integer(ip),   pointer :: ltypb_sub(:)
  integer(ip),   pointer :: lnnob_sub(:)

  character(100)         :: message

  return
  if( mcoup <= 0 ) return

  nullify(recvcounts4)
  nullify(npoin_gat)
  nullify(nboun_gat)
  nullify(coord_gat)
  nullify(lnodb_gat)
  nullify(ltypb_gat)
  nullify(lnnob_gat)
  nullify(lnper_gat)

  nullify(coord_recv)
  nullify(lnodb_recv)
  nullify(ltypb_recv)
  nullify(lnnob_recv)

  nullify(coord_sub)
  nullify(lnodb_sub)
  nullify(coord_sub)
  nullify(lnodb_sub)

  if( IMASTER ) then
     allocate( recvcounts4 (0:PAR_WORLD_SIZE-1) )
     allocate( recvcounts42(0:PAR_WORLD_SIZE-1) )
     allocate( npoin_gat   (0:PAR_WORLD_SIZE-1) )
     allocate( nboun_gat   (0:PAR_WORLD_SIZE-1) )
     allocate( coord_gat   (0:PAR_WORLD_SIZE-1) )
     allocate( lnodb_gat   (0:PAR_WORLD_SIZE-1) )
     allocate( ltypb_gat   (0:PAR_WORLD_SIZE-1) )
     allocate( lnnob_gat   (0:PAR_WORLD_SIZE-1) )
     allocate( lnper_gat   (0:PAR_WORLD_SIZE-1) )
  end if

  ipoin_total = 0
  iboun_total = 0

  do icoup = 1,mcoup

     npoin_sub = 0
     nboun_sub = 0
     if( INOTMASTER ) then
        call meshes_submesh(                                                          &
             & ndime,    npoin    ,nboun    ,coord    ,lnodb    ,ltypb    ,lnnob,     &
             &           npoin_sub,nboun_sub,coord_sub,lnodb_sub,ltypb_sub,lnnob_sub, &
             &           coupling_type(icoup) % geome % lboun_wet )
     end if
     !
     ! Gather number wet nodes/boundaries 
     !
     call PAR_GATHER(npoin_sub,npoin_gat,'IN THE WORLD')
     call PAR_GATHER(nboun_sub,nboun_gat,'IN THE WORLD')
     !
     ! COORD_GAT: Gather wet nodes coordinates
     !
     if( IMASTER ) then 
        itotn = 0
        do ipart = 0,PAR_WORLD_SIZE-1
           recvcounts4(ipart) = int(ndime*npoin_gat(ipart),4)           
           itotn = itotn + recvcounts4(ipart)
        end do
        allocate( coord_recv(itotn) )
     end if
     call PAR_GATHERV(coord_sub,coord_recv,recvcounts4,'IN THE WORLD')
     if( IMASTER ) then
        itotn = 0
        kpoin = 0
        do ipart = 0,PAR_WORLD_SIZE-1
           allocate( coord_gat(ipart) % a(ndime,npoin_gat(ipart)) )
           allocate( lnper_gat(ipart) % l(npoin_gat(ipart)) )
           do ipoin = 1,npoin_gat(ipart)
              kpoin = kpoin + 1
              lnper_gat(ipart) % l(ipoin) = kpoin
              do idime = 1,ndime
                 itotn = itotn + 1
                 coord_gat(ipart) % a(idime,ipoin) = coord_recv(itotn)
              end do
           end do
        end do
     end if
     !
     ! LNODB_GAT: Gather connectivity
     ! LTYPB_GAT: Gather boundary type
     ! LNNOB_GAT: Gather number nodes per per boundary
     !
     if( IMASTER ) then 
        itotn = 0
        do ipart = 0,PAR_WORLD_SIZE-1
           recvcounts4 (ipart) = int(mnodb*nboun_gat(ipart),4)           
           recvcounts42(ipart) = int(nboun_gat(ipart),4)           
           itotn = itotn + recvcounts42(ipart)
        end do
        allocate( lnodb_recv(itotn*mnodb) )
        allocate( ltypb_recv(itotn)       )
        allocate( lnnob_recv(itotn)       )
     end if
     call PAR_GATHERV(lnodb_sub,lnodb_recv,recvcounts4 ,'IN THE WORLD')
     call PAR_GATHERV(ltypb_sub,ltypb_recv,recvcounts42,'IN THE WORLD')
     call PAR_GATHERV(lnnob_sub,lnnob_recv,recvcounts42,'IN THE WORLD')

     if( IMASTER ) then
        itotn = 0        
        do ipart = 0,PAR_WORLD_SIZE-1
           allocate( lnodb_gat(ipart) % l(mnodb,nboun_gat(ipart)) )
           allocate( ltypb_gat(ipart) % l(nboun_gat(ipart))       )
           allocate( lnnob_gat(ipart) % l(nboun_gat(ipart))       )
           do iboun = 1,nboun_gat(ipart)
              ltypb_gat(ipart) % l(iboun) = ltypb_recv(iboun)
              lnnob_gat(ipart) % l(iboun) = lnnob_recv(iboun)
              do inodb = 1,mnodb
                 itotn = itotn + 1
                 lnodb_gat(ipart) % l(inodb,iboun) = lnodb_recv(itotn)
              end do
           end do
        end do
     end if
     !
     ! Output coordinates
     !
     kpoin = 0
     if( IMASTER ) then 
        write(100,'(a)') 'MESH COUPLING dimension 2 Elemtype Linear Nnode  2'
        write(100,'(a)') 'coordinates'
        do ipart = 1,PAR_WORLD_SIZE-1
           do ipoin = 1,npoin_gat(ipart)
              kpoin = kpoin + 1
              write(100,'(i6,3(1x,e12.6))') ipoin_total+kpoin,(coord_gat(ipart) % a(idime,ipoin),idime=1,ndime)
           end do
        end do
        write(100,'(a)') 'end coordinates'

        kboun = 0
        ipoin = 0
        do ipart = 1,PAR_WORLD_SIZE-1
           if( nboun_gat(ipart) > 0 ) then
              message = 'COUPLI_'//trim(intost(icoup))//'_CPU'//intost(ipart)
              write(100,'(a)') 'MESH '//trim(message)//' dimension 2 Elemtype Linear Nnode  2'
              write(100,'(a)') 'elements'
              do iboun = 1,nboun_gat(ipart)
                 kboun = kboun + 1
                 write(100,'(10(1x,i6))') kboun+iboun_total,&
                      (lnodb_gat(ipart) % l(inodb,iboun)+ipoin_total+ipoin,&
                      inodb=1,lnnob_gat(ipart) % l(iboun) )
              end do
              ipoin = ipoin + npoin_gat(ipart)
              write(100,'(a)') 'end elements'
           end if
        end do
        ipoin_total = ipoin_total + kpoin
        iboun_total = iboun_total + kboun
        call flush(100)
     end if
     !
     ! Deallocate
     !
     if( IMASTER ) then
        do ipart = 0,PAR_WORLD_SIZE-1
           if( associated( coord_gat(ipart) % a ) ) deallocate( coord_gat(ipart) % a )
           if( associated( lnodb_gat(ipart) % l ) ) deallocate( lnodb_gat(ipart) % l )
           if( associated( ltypb_gat(ipart) % l ) ) deallocate( ltypb_gat(ipart) % l )
           if( associated( lnnob_gat(ipart) % l ) ) deallocate( lnnob_gat(ipart) % l )
           if( associated( lnper_gat(ipart) % l ) ) deallocate( lnper_gat(ipart) % l )
        end do

        if( associated( coord_recv ) ) deallocate( coord_recv )
        if( associated( lnodb_recv ) ) deallocate( lnodb_recv )
        if( associated( ltypb_recv ) ) deallocate( ltypb_recv )
        if( associated( lnnob_recv ) ) deallocate( lnnob_recv )

     else

        if( associated( coord_sub ) ) deallocate( coord_sub )
        if( associated( lnodb_sub ) ) deallocate( lnodb_sub )
        if( associated( coord_sub ) ) deallocate( coord_sub )
        if( associated( lnodb_sub ) ) deallocate( lnodb_sub )

     end if

  end do

  if( IMASTER ) then
     if( associated( recvcounts4  ) ) deallocate( recvcounts4  )
     if( associated( recvcounts42 ) ) deallocate( recvcounts42 )
     if( associated( coord_gat    ) ) deallocate( coord_gat    )
     if( associated( lnodb_gat    ) ) deallocate( lnodb_gat    )
     if( associated( ltypb_gat    ) ) deallocate( ltypb_gat    )
     if( associated( lnnob_gat    ) ) deallocate( lnnob_gat    )
     if( associated( lnper_gat    ) ) deallocate( lnper_gat    )
  end if

end subroutine cou_output3

subroutine cou_output4()
  use def_master
  use def_domain
  use def_coupli
  use mod_parall
  use mod_memory
  use mod_communications
  use mod_interpolation  
  implicit none
  integer(ip)         :: icoup,kelem,ielem,ipoin,kpoin,ineig
  integer(ip)         :: inode,icolo,dom_i,isize,jcolo
  integer(4)          :: PAR_COMM_TU_USE
  real(rp),   pointer :: xx_send(:,:)

  do icoup = 1,mcoup

     allocate( xx_send(ndime,coupling_type(icoup) % geome % nelem_source) )

     if( coupling_type(icoup) % itype == ELEMENT_INTERPOLATION ) then
        !
        ! Element interpolation
        !
        do kelem = 1,coupling_type(icoup) % geome % nelem_source
           ielem = coupling_type(icoup) % geome % lelem_source(kelem)
           xx_send(1:ndime,kelem) = 0.0_rp
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              xx_send(1:ndime,kelem) = xx_send(1:ndime,kelem) &
                   + coupling_type(icoup) % geome % shapf(inode,kelem) * coord(1:ndime,ipoin)
           end do
        end do

     else if( coupling_type(icoup) % itype == NEAREST_BOUNDARY_NODE ) then
        !
        ! Nearest boundary node
        !
        do kpoin = 1,coupling_type(icoup) % geome % nelem_source
           ipoin = coupling_type(icoup) % geome % lelem_source(kpoin)
           xx_send(1:ndime,kpoin) = coord(1:ndime,ipoin)
        end do
 
     end if

     PAR_COMM_TU_USE = int(coupling_type(icoup) % commd % PAR_COMM_WORLD,4)
     icolo = coupling_type(icoup) % color_source
     jcolo = coupling_type(icoup) % color_target
     kelem = 0

     do ineig = 1,coupling_type(icoup) % commd % nneig
        dom_i = coupling_type(icoup) % commd % neights(ineig)
        dom_i = par_color_coupling_rank_to_world(dom_i,icolo,jcolo)
        if( coupling_type(icoup) % commd % lsend_size(ineig+1) - coupling_type(icoup) % commd % lsend_size(ineig) > 0 ) then
           write(kfl_paral+100,*) 'SUBDOMAIN ',kfl_paral,' SENDS TO ',dom_i
           do isize = coupling_type(icoup) % commd % lsend_size(ineig),coupling_type(icoup) % commd % lsend_size(ineig+1)-1
              kelem = kelem + 1
              write(kfl_paral+100,*) xx_send(1:ndime,kelem)
           end do
        end if
     end do 
     !
     !print*,'caca=',kfl_paral,coupling_type(icoup) % geome % npoin_wet,coupling_type(icoup) % commd % lrecv_size(coupling_type(icoup) % commd % nneig+1)-1
        
     if( kfl_paral == 1 ) then
        
     end if

     kelem = 0
     do ineig = 1,coupling_type(icoup) % commd % nneig
        dom_i = coupling_type(icoup) % commd % neights(ineig)
        dom_i = par_color_coupling_rank_to_world(dom_i,icolo,jcolo)
        if( coupling_type(icoup) % commd % lrecv_size(ineig+1) - coupling_type(icoup) % commd % lrecv_size(ineig) > 0 ) then
           write(kfl_paral+100,*) 'SUBDOMAIN ',kfl_paral,' RECEIVES FROM ',dom_i
           do isize = coupling_type(icoup) % commd % lrecv_size(ineig),coupling_type(icoup) % commd % lrecv_size(ineig+1)-1
              kelem = kelem + 1
              ipoin = coupling_type(icoup) % geome % status(kelem)
              ipoin = coupling_type(icoup) % geome % lpoin_wet(ipoin)
              write(kfl_paral+100,*) coord(1:ndime,ipoin)
           end do
        end if
     end do 
     call flush( kfl_paral+100 )
     deallocate( xx_send)

  end do

end subroutine cou_output4
