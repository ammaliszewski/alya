subroutine cou_outcpu()
  !-----------------------------------------------------------------------
  !> @addtogroup Coupli
  !> @{
  !> @file    cou_outcpu.f90
  !> @author  Guillaume Houzeaux
  !> @date    16/05/2014
  !> @brief   CPU time of coupling
  !> @details CPU time of coupling
  !> @} 
  !-----------------------------------------------------------------------
  use def_kintyp,         only : ip,rp
  use def_master,         only : routp,coutp,IMASTER,zeror,kfl_timin
  use def_coupli,         only : cputi_cou,mcoup,lun_coupl_res
  use mod_communications, only : PAR_MAX
  implicit none
  integer(ip) :: nsize
  real(rp)    :: xfact

  if( mcoup > 0 ) then

     if( kfl_timin == 0 ) then
        nsize = size(cputi_cou)
        call PAR_MAX(nsize,cputi_cou,'IN THE WORLD')
     end if

     if( IMASTER ) then

        routp(1)  = max(sum(cputi_cou),zeror)
        call outfor(29_ip,lun_coupl_res,' ')
        xfact     = 100.0_rp / routp(1)

        coutp(1)  = 'DEFINE WET NODES' 
        routp(1)  = cputi_cou(1)-cputi_cou(10) ! Take off holcut time
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'HOLCUT' 
        routp(1)  = cputi_cou(10)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'COMPUTE KD-TREES' 
        routp(1)  = cputi_cou(2)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'DISTRIBUTE WET POINTS' 
        routp(1)  = cputi_cou(6)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'SEND WET COORD TO CPUS' 
        routp(1)  = cputi_cou(3)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'CPUS CHECK OWNING' 
        routp(1)  = cputi_cou(4)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'CREATE COUPLING COMM.' 
        routp(1)  = cputi_cou(5)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'INTERPOLATE AT SOURCE' 
        routp(1)  = cputi_cou(7)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'SEND/RECV COUPLING' 
        routp(1)  = cputi_cou(8)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

        coutp(1)  = 'ASSEMBLE AT TARGET' 
        routp(1)  = cputi_cou(9)
        routp(2)  = xfact * routp(1)
        call outfor(30_ip,lun_coupl_res,' ')

     end if

  end if

end subroutine cou_outcpu
