!==============================================================================!
!  I am your father...
! 
!< 2014Sep05
!< 2015Abr14: add  'n_max_its' 'dynamic' 
!< 2015Abr28: add  'commdom_driver_nodes_to_reaction' and 'commdom_bvnat' 
!             change inivar.f90 + line 305 'endif'
! 
!< 2015May10 
!< 2015May11 -> unstick  
!< 2015Jul02 -> segmentation fixed!!
!< 2015Jul03 -> exchanged, avoid overlapping 
!< 2015JUL17 ->  
!
!==============================================================================!
  !-----------------------------------------------------------------------||---!
  !   + current_code                                      ___________current_task 
  !   |_Alya                                       ______|_____
  !     |_call Turnon()                            ITASK_TURNON  02  
  !     |_call Iniunk()                            ITASK_INIUNK  03 
  !     |_time: do while
  !       |_call Timste()                          ITASK_TIMSTE  04 
  !       |_reset: do 
  !       | |_call Begste()                        ITASK_BEGSTE  05 
  !       |    |_block: do while                                     
  !       |       |_coupling: do while                              
  !       |         |_call Begzon()                ITASK_BEGZON  19   _
  !       |         |_modules: do while                              / TASK_BEGITE  14 
  !       |           |_call Doiter()              ITASK_DOITER  06-|  
  !       |           |_call Concou()              ITASK_CONCOU  07  \_ITASK_ENDITE 15 
  !       |         |_call Endzon()                ITASK_ENDZON  20
  !       |       |_call Conblk()                  ITASK_CONBLK  08 
  !       |_call Endste()                          ITASK_ENDSTE  10 
  !   |_call Turnof()                              ITASK_TURNOF  13   
  !          __
  ! BLOCK 3_   | 
  !   1 X   |  |--current_block  -> CPLNG%blocks_list 
  !   2 Y Z |  |     
  !   3 W  _|-----current_module -> CPLNG%moduls_list 
  ! END_BLOCK__| 
  !
  !-----------------------------------------------------------------------||---!
  !
  ! <code, block, modul, task, when, send|recv>
  !
  !-----------------------------------------------------------------------||---!
module mod_commdom_driver
  use mod_commdom_alya,     only: INONE 
  use def_parame,           only: ip, rp
  use def_master,           only: inotmaster, imaster
  use def_domain,           only: coord, mnode, ndime, npoin 
  use def_domain,           only: ltype, lnods
  use mod_commdom_alya,     only: COMMDOM_COUPLING
  use def_kintyp,           only: soltyp
  use def_master,           only: momod, modul
#ifdef COMMDOM
  use mod_commdom_plepp,    only: PLEPP_CPLNG
  use mod_commdom_plepp,    only: commdom_plepp_compare_dtinv
  use mod_commdom_plepp,    only: commdom_plepp_set_source_nodes
  use mod_commdom_dynamic,  only: commdom_dynamic_check_fixno
  use mod_commdom_dynamic,  only: commdom_dynamic_set_mesh
  use mod_commdom_dynamic,  only: commdom_dynamic_deallocate
  use mod_commdom_dynamic,  only: commdom_dynamic_exchange02
#endif
  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  implicit none 
  !
  logical(ip), parameter  :: debug = .false.
  !
  character(6) :: name_task(20) = (/ 'REAPRO', 'TURNON', 'INIUNK', &
                                     'TIMSTE', 'BEGSTE', 'DOITER', &
                                     'CONCOU', 'CONBLK', 'NEWMSH', &
                                     'ENDSTE', 'FILTER', 'OUTPUT', &
                                     'TURNOF', 'BEGITE', 'ENDITE', &
                                     'MATRIX', 'DOOPTI', 'ENDOPT', &
                                     'BEGZON', 'ENDZON' /) 

  character(6) :: name_when(2) = (/ 'BEFORE', 'AFTERE'/) 
  !
  integer(ip), parameter   :: n_times = 7
  logical(ip)              :: CNT_SENDRECV(n_times) = .false. 
  character(64)            :: CNT_SMS               = ' '
  type(COMMDOM_COUPLING)   :: CNT_CPLNG
  !
  real(rp)                 :: n_max_its
  logical(ip)              :: dynamic
  !
  logical(ip) :: residual = .false. 
  !
  logical(ip) :: exchanged  = .false.
  !
  type(soltyp), pointer    :: solve(:)
  !
  private
  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  !
  public :: CNT_SENDRECV
  public :: CNT_SMS
  public :: CNT_CPLNG
  public :: commdom_driver_init_cht
  public :: commdom_driver_init_contact
  public :: commdom_driver_sendrecv
  public :: commdom_driver_exchange02 
  public :: commdom_driver_n_fixno
  public :: commdom_driver_get_total_flux
  public :: commdom_driver_get_residual 
  !
  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!

  contains
!-------------------------------------------------------------------------||---!
!-----------------------------------------------------------------| PUBLIC |---!
!-------------------------------------------------------------------------||---!


  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_init_cht( CPLNG )
  use def_coupli,       only: UNKNOWN, RESIDUAL  
  use def_coupli,       only: mcoup  
  use def_master,       only: ID_TEMPER
  use def_master,       only: current_code, modul
  implicit none
  type(COMMDOM_COUPLING), intent(inout) :: CPLNG
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  !
  CPLNG%code_i       =  1_ip        !< CODEi
  CPLNG%module_i     =  ID_TEMPER   !< MODULEi
  CPLNG%fixbo_i      = -1_ip
  CPLNG%what_i       = -RESIDUAL    !<---- never
  !
  CPLNG%code_j       =  2_ip        !< CODEj
  CPLNG%module_j     =  ID_TEMPER   !< MODULEj 
  CPLNG%fixbo_j      = -1_ip
  CPLNG%what_j       =  RESIDUAL  
  !
  CPLNG%tolerance    =  1e-3 
  n_max_its          =  1_ip
  !
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  dynamic            = .false. 
  CPLNG%n_dof        =  1_ip        !< D.O.F.
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  if(CPLNG%code_i==current_code) CPLNG%current_fixbo = CPLNG%fixbo_i
  if(CPLNG%code_j==current_code) CPLNG%current_fixbo = CPLNG%fixbo_j
  !
  CPLNG%current_code = current_code !< *.dat CODE: ID_CODE 
  current_code       = 1_ip         !< trick!! -> PAR_COLOR_COMMUNICATORS: CODE NUMBER EXCEED NUMBER OF CODES 
  mcoup              = 0_ip         !< evoid cou_turnon 
  !
  if(IMASTER) print*, "[commdom_driver_init_cht]"
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!

  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_init_contact(CPLNG)
  use def_coupli,       only: UNKNOWN, RESIDUAL
  use def_coupli,       only: mcoup
  use def_master,       only: ID_NASTIN, ID_TEMPER, ID_SOLIDZ
  use def_master,       only: current_code, modul
  implicit none
  type(COMMDOM_COUPLING), intent(inout) :: CPLNG
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  CPLNG%code_i       =  1_ip        !< CODEi
  CPLNG%module_i     =  ID_SOLIDZ   !< MODULEi
  CPLNG%fixbo_i      = -1_ip
  CPLNG%what_i       =  RESIDUAL    !< 'physical'  or 'numerical' coupling 
  !
  CPLNG%code_j       =  2_ip        !< CODEj
  CPLNG%module_j     =  ID_SOLIDZ   !< MODULEj 
  CPLNG%fixbo_j      = -1_ip
  CPLNG%what_j       = -RESIDUAL    !<---- never 
  !
  CPLNG%tolerance    = -1e-4 
  n_max_its          =  1_ip       !< 2015JUL17  
  !
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  dynamic            = .true.
  CPLNG%n_dof        =  3_ip        !< D.O.F.
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  if(CPLNG%code_i==current_code) CPLNG%current_fixbo = CPLNG%fixbo_i
  if(CPLNG%code_j==current_code) CPLNG%current_fixbo = CPLNG%fixbo_j
  !
  CPLNG%current_code = current_code !< *.dat CODE: ID_CODE 
  current_code       = 1_ip         !< trick!! -> PAR_COLOR_COMMUNICATORS: CODE NUMBER EXCEED NUMBER OF CODES 
  mcoup              = 0_ip         !< evoid cou_turnon 
  !
  if(IMASTER) print*, "[commdom_driver_init_contact]"
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!

  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_sendrecv(CPLNG, current_when, current_task)
  use def_master,           only: iblok, ittim, itcou 
  use def_master,           only: modul, current_code
  use def_master,           only: nblok
  use def_master,           only: namod, mmodu
  use def_master,           only: ITASK_INIUNK, ITASK_TURNOF
  use def_master,           only: ITASK_TIMSTE, ITASK_ENDSTE 
  use def_master,           only: ITASK_BEGZON, ITASK_ENDZON 
  use def_master,           only: ITASK_AFTER,  ITASK_BEFORE 
  use def_master,           only: ITASK_BEGSTE, ITASK_CONBLK 
  use def_master,           only: ID_KERMOD, ITASK_DOITER, ITASK_CONCOU 
  !
  use def_master,           only: dtinv
  use def_master,           only: displ
  use def_domain,           only: coord
  !
  !< ITERATIONs
  use def_coupli,           only: coupling_driver_iteration 
  !
  implicit none
  type(COMMDOM_COUPLING), intent(inout) :: CPLNG
  !
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  integer(ip),  intent(in)  :: current_when
  integer(ip),  intent(in)  :: current_task
  !
 !integer(ip), parameter   :: n_times = 6
  integer(ip)   :: n_dirichlet(3), n_neumann(3)
  integer(ip)              :: itime 
  integer(ip)              ::       now(8_ip) 
  integer(ip)              ::  the_time(8_ip,n_times) = -1_ip 
  logical(ip)              :: sendrecv(n_times) = .false. 
  !
  character(16)            :: saux(8_ip) = ' '
  character(64)            :: sms        = ' '
  character( 4), parameter :: frmt = '(I2)'
  !
  CNT_SMS = '+-+-+-+-+-+-+-+-'
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  !
  !  ittim: Current time step
  !  itcou: Current global iteration
  !  iblok: Current block
  !  is_when = itcou==micou(iblok)
  !
  now = (/ CPLNG%current_code, iblok, modul, current_task, current_when, ittim, -1_ip, -1_ip /) 
  !
  !-----------------------------------------------------------------------||---!
  write(saux(1), frmt) CPLNG%current_code
  write(saux(2), frmt) iblok
  write(saux(3), frmt) ittim
  write(saux(4), frmt) coupling_driver_iteration(iblok)
  !
  if(current_when==ITASK_BEFORE) then     
    saux(5) = "+"
    saux(6) = "-"
  else& 
  if(current_when==ITASK_AFTER ) then 
    saux(5) = "-"
    saux(6) = "+"
  endif 
  !
  sms = "'"//trim(saux(1))//&
        "."//namod(modul)//&
        "."//trim(saux(5))//name_task(current_task)//trim(saux(6))// & 
       !"."//name_when(current_when)//&
        ".B"//trim(saux(2))// & 
        ".T"//trim(saux(3))// &
        ".I"//trim(saux(4))//"'"
  !-----------------------------------------------------------------------||---!
  if(nblok>1) then 
    print *, "[commdom_driver_sendrecv] ", sms 
    print *, "[commdom_driver_sendrecv] ", "nblok==1 !!" 
    call runend("EXIT!!")
  endif 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  !-----------------------------------------------------------| ITERATIONS |---!
  !   +
  !   |_Alya                                       
  !     |_call Turnon()                            
  !     |_call Iniunk()                             
  !     |_time: do while
  !       |_call Timste()                (-1+)
  !       |_reset: do                    
  !         |_call Begste()              (+2-)  
  !           |_block: do while          
  !             |_coupling: do while     
  !               |_call Begzon()        (+4-)  AFTER<-never INTO the module: into 'coupli/mod_coupling_driver.f90' 
  !                                                                           add 'if(current_when==ITASK_AFTER) call Temper(-1_ip)'
  !               |_modules: do while                           
  !                 |_call Doiter()      
  !                 |_call Concou()      (+7-)
  !               |_call Endzon()        (-5+) 
  !             |_call Conblk()          (-6+) 
  !       |_call Endste()                                     
  !   |_call Turnof()                    (-3+)
  !
  !-----------------------------------------------------------------------||---!
  if(CPLNG%current_code==CPLNG%code_i) then 
    the_time(:,1_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_i, ITASK_TIMSTE, ITASK_AFTER,  ittim, -1_ip, -1_ip /)   
    the_time(:,2_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_i, ITASK_BEGSTE, ITASK_BEFORE, ittim, -1_ip, -1_ip /)   
    the_time(:,3_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_i, ITASK_TURNOF, ITASK_AFTER,  ittim, -1_ip, -1_ip /) 
    the_time(:,4_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_i, ITASK_BEGZON, ITASK_AFTER,  ittim, -1_ip, -1_ip /) 
    the_time(:,5_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_i, ITASK_ENDZON, ITASK_AFTER,  ittim, -1_ip, -1_ip /) 
    the_time(:,6_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_i, ITASK_CONBLK, ITASK_BEFORE,  ittim, -1_ip, -1_ip /) 
    !
    the_time(:,7_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_i, ITASK_ENDZON, ITASK_AFTER, ittim, -1_ip, -1_ip /) 
  endif 
  if(CPLNG%current_code==CPLNG%code_j) then 
    the_time(:,1_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_j, ITASK_TIMSTE, ITASK_AFTER,  ittim, -1_ip, -1_ip /)   
    the_time(:,2_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_j, ITASK_BEGSTE, ITASK_BEFORE, ittim, -1_ip, -1_ip /)   
    the_time(:,3_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_j, ITASK_TURNOF, ITASK_AFTER,  ittim, -1_ip, -1_ip /) 
    the_time(:,4_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_j, ITASK_BEGZON, ITASK_AFTER,  ittim, -1_ip, -1_ip /) 
    the_time(:,5_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_j, ITASK_ENDZON, ITASK_AFTER,  ittim, -1_ip, -1_ip /) 
    the_time(:,6_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_j, ITASK_CONBLK, ITASK_BEFORE,  ittim, -1_ip, -1_ip /) 
    !
    the_time(:,7_ip)  = (/ CPLNG%current_code, iblok, CPLNG%module_j, ITASK_BEGZON, ITASK_BEFORE, ittim, -1_ip, -1_ip /) 
  endif 
  !
  sendrecv = (/ (all(the_time(:,itime)==now), itime=1,n_times) /)
  !
  do itime = 1,n_times
    if( sendrecv(itime) ) then 
      if(debug) print *,"[commdom_driver_sendrecv] ",  trim(sms)   
    endif 
  enddo
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-------------------------------------------------------------| -TIMSTE+ |---!
#ifdef COMMDOM
  if( sendrecv(1_ip).and.dynamic ) then 
    !--------------------------------------------------------------| here? |---!
    if(inotmaster) coord = coord + displ(:,:,1)                          !<-- segmentation!!?? !< 2015Jul02  
    call commdom_dynamic_deallocate( PLEPP_CPLNG ) 
    call commdom_dynamic_set_mesh( CPLNG%current_fixbo, CPLNG%n_dof) 
    if(inotmaster) coord = coord - displ(:,:,1)                          !<-- segmentation!!?? !< 2015Jul02 
    !
    if( residual ) then                                                       !< 2015Abr28
!      call commdom_bvnat(n_dirichlet, n_neumann, init=.false., debug=.true.)  !< 2015May11 
    else 
      call commdom_driver_nodes_to_reaction( CPLNG )                          !< 2015May10
!      call commdom_bvnat(n_dirichlet, n_neumann, init=.true., debug=.true.)   !< 2015May11
      residual = .true. 
    endif                                                           !< 2015Abr28 
    !---------------------------------------------------------------------||---!
  endif 
  !-------------------------------------------------------------| +BEGSTE- |---!
  if( sendrecv(2_ip) ) then 
    call commdom_driver_begste()
  endif 
  !-------------------------------------------------------------| -TURNOF+ |---!
  if( sendrecv(3_ip) ) then 
    call commdom_plepp_compare_dtinv(dtinv) 
  endif 
  !-------------------------------------------------------------| -BEGZON+ |---!
  if( sendrecv(4_ip) ) then 
    call commdom_driver_begzon() 
  endif 
  !-------------------------------------------------------------| -ENDZON+ |---!
  if( sendrecv(5_ip) ) then !.and.exchanged ) then 
    call commdom_driver_endzon() 
!    exchanged = .false.                                                        !< 2015Jul03 
  endif 
  !-------------------------------------------------------------| +CONBLK- |---!
  if( sendrecv(6_ip) ) then 
    call commdom_driver_conblk() 
    !-----------------------------------------------------------| or here? |---!
    !---------------------------------------------------------------------||---!
  endif
  !-------------------------------------------------------------| EXCHANGE |---!
  if( sendrecv(7_ip) ) then !.and.(.not.exchanged) ) then 
!    exchanged = .true.                                                         !< 2015Jul03 
  endif
  !-----------------------------------------------------------------------||---!  
#endif 
  !-----------------------------------------------------------------------||---!  
  CNT_SENDRECV = sendrecv
  if(any(sendrecv)) CNT_SMS = trim(sms)
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!

  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_exchange02( CPLNG, debug) 
  implicit none 
  type(COMMDOM_COUPLING), intent(inout) :: CPLNG
  logical(ip), optional,  intent(in   ) :: debug
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
#ifdef COMMDOM
  if( present(debug) ) then 
    call commdom_dynamic_exchange02( CPLNG%var_ij(1_ip:CPLNG%n_dof,1_ip:CPLNG%n_pts), &
                                     CPLNG%var_ji(1_ip:CPLNG%n_dof,1_ip:CPLNG%n_pts), & 
                                     CPLNG%n_dof,                                     &
                                     debug                                            &  
                                   )
  else
    call commdom_dynamic_exchange02( CPLNG%var_ij(1_ip:CPLNG%n_dof,1_ip:CPLNG%n_pts), &
                                     CPLNG%var_ji(1_ip:CPLNG%n_dof,1_ip:CPLNG%n_pts), &
                                     CPLNG%n_dof,                                     &
                                     .True.                                           &
                                   ) 
  endif 
#endif 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!



!-------------------------------------------------------------------------||---!
!----------------------------------------------------------------| PRIVATE |---!
!-------------------------------------------------------------------------||---!

subroutine  commdom_driver_nodes_to_reaction( CPLNG )
  use def_parame,         only: ip, rp
  use def_master,         only: inotmaster, imaster, isequen
  use def_domain,         only: npoin, mcono, mcodb,  kfl_codno
  use def_kintyp,         only: soltyp
  use mod_commdom_alya,   only: COMMDOM_COUPLING
  use def_master,         only: momod, modul
  use def_coupli,         only: RESIDUAL
  implicit none
  type(COMMDOM_COUPLING), intent(inout) :: CPLNG
  logical(ip) :: sendrecv(5_ip)
!
integer(ip) :: ipoin, ndofn, ncono 
logical(ip), pointer :: touched(:) => null() 
logical(ip) :: codno(mcono) 
logical(ip), pointer ::   fixno(:) 
!
  !
  type(soltyp), pointer :: solve_sol(:)
  solve_sol => momod(modul) % solve(1_ip:)
  ndofn = solve_sol(1)%ndofn
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  if(inotmaster) then 
    allocate( touched(npoin) )
    allocate(   fixno(ndofn) )
  else 
    allocate( touched(1) )
    allocate(   fixno(1) )
  endif 
  !
  touched     = .false.
  !
  if(inotmaster) then 
    do ipoin = 1,npoin  
      codno(1:mcono) = kfl_codno(1:mcono,ipoin) /= mcodb+1                     !< Is it at 'boundary'?
      ncono          = count( codno(1:mcono) )                                 !< How many 'codes' are? 
      !
      if( ncono>0 ) then                                                       !< Is a                   'boundary'? 
        fixno(1:ndofn) = abs( solve_sol(1)%kfl_fixno(1:ndofn,ipoin) ) == 0     !< Is it fixed? 
        if( all(fixno(1:ndofn)) ) touched(ipoin) = .true.
      endif
      !
    enddo
  endif 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  CPLNG%current_what = .false.
  CPLNG%current_what(1_ip) = (CPLNG%what_i==RESIDUAL)
  CPLNG%current_what(2_ip) = (CPLNG%what_j==RESIDUAL)
  CPLNG%current_what(3_ip) = CPLNG%current_what(1_ip).and.CPLNG%current_what(2_ip)
  CPLNG%current_what(4_ip) = CPLNG%current_what(1_ip).or. CPLNG%current_what(2_ip)

  sendrecv = .false.
  sendrecv(1_ip) = (CPLNG%code_i==CPLNG%current_code).and.(CPLNG%module_i==modul).and.(CPLNG%current_what(4_ip)).and.INOTMASTER
  sendrecv(2_ip) = (CPLNG%code_j==CPLNG%current_code).and.(CPLNG%module_j==modul).and.(CPLNG%current_what(4_ip)).and.INOTMASTER
  sendrecv(3_ip) = sendrecv(1_ip).and.sendrecv(2_ip)
  sendrecv(4_ip) = sendrecv(1_ip).or. sendrecv(2_ip)
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  if( sendrecv(4_ip) ) then                   ! OR
    !
    if( sendrecv(2_ip) ) then                 !  CODE_J
      if( CPLNG%current_what(2_ip) ) then     ! BVNAT_J
        call allocate_react_bvnat(  momod(modul), modul, .false., .true.)
      else                                    ! REACT_J
        call allocate_react_bvnat(  momod(modul), modul, .true., .false.)
        !
       !call commdom_plepp_set_source_nodes( solve_sol(1)%lpoin_reaction(1:npoin)  )  ! Mark the nodes where reaction is required
        solve_sol(1)%lpoin_reaction(1:npoin) = touched(1:npoin) 
        !
        call allocate_block_system( momod(modul), solve_sol(1)%lpoin_reaction(1:npoin) )
      endif
      print *, "[commdom_plepp_inivar]", " 'RESIDUALj'" 
    else&
    if( sendrecv(1_ip) ) then                 !  CODE_I
      if( CPLNG%current_what(1_ip) ) then     ! BVNAT_I
        call allocate_react_bvnat(  momod(modul), modul, .false., .true.)
      else                                    ! REACT_I
        call allocate_react_bvnat(  momod(modul), modul, .true., .false.)
        !
       !call commdom_plepp_set_source_nodes( solve_sol(1)%lpoin_reaction(1:npoin)  )  ! Mark the nodes where reaction is required
        solve_sol(1)%lpoin_reaction(1:npoin) = touched(1:npoin) 
        !
        call allocate_block_system( momod(modul), solve_sol(1)%lpoin_reaction(1:npoin) )
      endif
      print *, "[commdom_plepp_inivar]", " 'RESIDUALi'"
    endif
    !
  else
  endif
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  deallocate( touched )
  deallocate( fixno   )
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
end subroutine
!-------------------------------------------------------------------------||---!
!                                                                              !
!------------------------------------------------------------------------------!

  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_bvnat( n_dirichlet, n_neumann, init, debug ) 
  use def_domain
  use def_master 
  implicit none 
  integer(ip), optional, intent(out) :: n_dirichlet(:)
  integer(ip), optional, intent(out) :: n_neumann(:)
  logical(ip), optional, intent(in ) :: init 
  logical(ip), optional, intent(in ) :: debug
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  integer(ip) :: idofn, ipoin, itouch 
  integer(ip) :: nfixn   = -1 
  integer(ip) :: ncono   = -1 
  integer(ip) :: ndofn   = -1
  integer(ip) :: n_touch = -1 
  integer(ip), pointer :: n_fixno(:) => null() 
  integer(ip), pointer :: n_react(:) => null() 
  integer(ip), pointer :: n_bvnat(:) => null()  
  !
  logical(ip) ::  init_aux = .false. 
  logical(ip) :: kfl_react = .false. 
  logical(ip) :: kfl_bvnat = .false. 
  logical(ip) :: codno(mcono) 
  logical(ip), pointer ::   fixno(:) 
  logical(ip), pointer :: touched(:) => null() 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  ndofn = solve(1)%ndofn
  idofn = 1
  !
  if(inotmaster) then 
    allocate( touched(npoin) )
    allocate(   fixno(ndofn) )
    allocate( n_fixno(ndofn) )
    allocate( n_react(ndofn) )
    allocate( n_bvnat(ndofn) )
  else 
    allocate( touched(1) )
    allocate(   fixno(1) )
    allocate( n_fixno(1) )
    allocate( n_react(1) )
    allocate( n_bvnat(1) )
  endif 
  !
  touched     = .false.
  !
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------| cach error |---!
  if( present(n_dirichlet).and.( size(n_dirichlet,1)<ndofn) ) then 
    print *, "[commdom_bvnat] ", " shape(n_dirichlet)/=ndofn ", shape(n_dirichlet), "/=", ndofn
    call runend("[commdom_bvnat] ERROR!!") 
  endif 
  !-----------------------------------------------------------| cach error |---!
  if( present(n_neumann).and.( size(n_neumann,1)<ndofn) ) then 
    print *, "[commdom_bvnat] ", " shape(n_neumann)/=ndofn ", shape(n_neumann),  "/=", ndofn
    call runend("[commdom_bvnat] ERROR!!") 
  endif 
  !-----------------------------------------------------------------------||---!
  if( present(n_dirichlet) ) n_dirichlet = 0 
  if( present(n_neumann  ) ) n_neumann   = 0
  if( present(init       ) ) init_aux    = init 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
#ifdef COMMDOM
  inotmaster02: & 
  if(inotmaster) then 
    !---------------------------------------------------------------------||---!
    kfl_react  = solve(1)%kfl_react==1
    kfl_bvnat  = solve(1)%kfl_bvnat==1
    !
    if(kfl_react) then 
      !-------------------------------------------------------------------||---!
      call commdom_dynamic_check_fixno(solve(1)%kfl_fixno, 1_ip, 1_ip, .True.) ! fixno, idofn, fixval, ToDo
      call commdom_dynamic_check_fixno(solve(1)%kfl_fixno, 2_ip, 1_ip, .True.) ! fixno, idofn, fixval, ToDo
      call commdom_dynamic_check_fixno(solve(1)%kfl_fixno, 3_ip, 1_ip, .True.) ! fixno, idofn, fixval, ToDo
      !-------------------------------------------------------------------||---!
    endif 
    !
    call commdom_plepp_set_source_nodes( touched(1:npoin)  )                   !< Is it 'touched'? 
    n_touch = count( touched(1:npoin) )
    !
    !---------------------------------------------------------------------||---!
    n_fixno(1:ndofn) = 0
    n_react(1:ndofn) = 0
    n_bvnat(1:ndofn) = 0
    do ipoin = 1,npoin  
      codno(1:mcono) = kfl_codno(1:mcono,ipoin) /= mcodb+1                     !< Is it at 'boundary'?
      ncono          = count( codno(1:mcono) )                                 !< How many 'codes' are? 
      !
      fixno(1:ndofn) = abs( solve(1)%kfl_fixno(1:ndofn,ipoin) ) /= 0           !< Is it fixed? 
      !
      if( ncono>0 ) then                                                       !< Is a                   'boundary'? 
        if( touched(ipoin).OR.init_aux ) then                                  !< Is a           'touched boundary'?
          n_fixno(1:ndofn) = n_fixno(1:ndofn) + 1                              !< Is a             'fixed boundary'?
          where(     fixno(1:ndofn) ) n_react(1:ndofn) = n_react(1:ndofn) + 1  !< Is a     'fixed touched boundary'?
          where(.not.fixno(1:ndofn) ) n_bvnat(1:ndofn) = n_bvnat(1:ndofn) + 1  !< Is a not 'fixed touched boundary'?
        endif 
      endif
      ! 
    enddo
    !---------------------------------------------------------| cach error |---!
    !
    ! mca_oob_tcp_msg_recv: readv failed: Connection reset by peer (104) ??
    ! ORTE_ERROR_LOG:  A message is attempting to be sent to... ??
    !
    if( .not.all(n_bvnat(1:ndofn) + n_react(1:ndofn) == n_fixno(1:ndofn)) ) then 
      print *, "[commdom_bvnat] ", " 'n_bvnat+n_react /= n_fixno' ", n_bvnat(1:ndofn) + n_react(1:ndofn),"/=", n_fixno(1:ndofn)
      call runend("[commdom_bvnat] ERROR!!") 
    endif 
    !---------------------------------------------------------------------||---!
    !                                                                          !
    !---------------------------------------------------------------------||---!
    if(kfl_react) then 
      !-------------------------------------------------------| cach error |---!
      if( (any(n_bvnat(1:ndofn)>0).and.(.not.init_aux) ) ) then 
        itouch = 0 
        do ipoin = 1,npoin  
          fixno(1:ndofn) = abs( solve(1)%kfl_fixno(1:ndofn,ipoin) ) /= 0
          if( any( .not.fixno(1:ndofn) ).and.( touched(ipoin) ) ) then 
            itouch = itouch + 1 
            print *, "[commdom_react] ", "codno->", itouch, "|", kfl_codno(1:mcono,ipoin), "|", solve(1)%kfl_fixno(1:ndofn,ipoin)
          endif 
        enddo 
        !
        print *, "[commdom_react] ", "'"//trim(title)//"'", " 'n_bvnat>0' (", n_bvnat(1:ndofn), ") ", count( solve(1)%lpoin_reaction(1:npoin) ), &
                                    ", 'CHANGE fixno -> 1'"
        call runend("[commdom_react] ERROR!!") 
      endif 
      !------------------------------------------------------| n_dirichlet |---!
      !
      if(debug) print *, "[kfl_react] ", "'"//trim(title)//"'", n_react(1:ndofn), count( solve(1)%lpoin_reaction(1:npoin) )
      !
      if( present(n_dirichlet) ) n_dirichlet(1:ndofn) = n_react(1:ndofn)       
      !
      !-------------------------------------------------------------------||---!
    endif
    !---------------------------------------------------------------------||---!
    if(kfl_bvnat) then 
      !-------------------------------------------------------| cach error |---!
      if( (any(n_react(1:ndofn)>0).and.(.not.init_aux)) ) then 
        do ipoin = 1,npoin  
          fixno(1:ndofn) = abs( solve(1)%kfl_fixno(1:ndofn,ipoin) ) /= 0
          if( any( fixno(1:ndofn) ).and.( touched(ipoin) ) ) & 
            print *, "[commdom_bvnat] ", "codno->", kfl_codno(1:mcono,ipoin), "|", solve(1)%kfl_fixno(1:ndofn,ipoin)
        enddo 
        !
        print *, "[commdom_bvnat] ", "'"//trim(title)//"'", " 'n_react>0' ", n_react(1:ndofn)
        call runend("[commdom_bvnat] ERROR!!") 
      endif 
      !------------------------------------------------------------| error |---!
      if( count(kfl_fixbo(1:nboun)>0) > 0 ) then 
        print *, "[commdom_bvnat] ", "'"//trim(title)//"'", " 'kfl_fixbo>0', unset 'CODES, BOUNDARIES' " 
        call runend("[commdom_bvnat] ERROR!!") 
      endif 
      !--------------------------------------------------------| n_neumann |---!
      !
      if(debug) print *, "[kfl_bvnat] ", "'"//trim(title)//"'", n_bvnat(1:ndofn), count( solve(1)%lpoin_reaction(1:npoin) )
      !
      if( present(n_neumann) ) n_neumann(1:ndofn) = n_bvnat(1:ndofn)
      !
      !-------------------------------------------------------------------||---!
    endif 
    !---------------------------------------------------------------------||---!
  endif inotmaster02 
#endif 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  deallocate( touched )
  deallocate( fixno   )
  deallocate( n_fixno )
  deallocate( n_react )
  deallocate( n_bvnat )
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!



  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_begste() 
  use def_master,  only: iblok
  use def_coupli,  only: coupling_driver_iteration 
  use def_coupli,  only: coupling_driver_number_couplings
  use def_coupli,  only: coupling_driver_max_iteration
  use def_coupli,  only: max_block_cou
  use def_coupli,  only: kfl_gozon
  use def_master,  only: kfl_gocou 
  !
  use def_master,        only: dtinv, cutim, dtime  
  !
  implicit none 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  coupling_driver_iteration(1:max_block_cou) = 0 
  coupling_driver_number_couplings(iblok)    = 1 
  coupling_driver_max_iteration(iblok)       = n_max_its  
  !-----------------------------------------------------------------------||---!
#ifdef COMMDOM
  call commdom_plepp_compare_dtinv(dtinv) 
#endif 
  cutim  = cutim - dtime       
  call setgts(2_ip)             
  call livinf(201_ip, ' ',1_ip)
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!


  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_begzon() 
  use def_master,    only : iblok
  use def_master,    only : mmodu
  use def_master,    only : lmord
  use def_master,    only : itinn
  use def_coupli,    only : coupling_driver_iteration
  use def_coupli,    only : coupling_driver_number_couplings
  implicit none 
  integer(ip) :: iorde,imodu
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!  
     if( coupling_driver_number_couplings(iblok) /= 0 .and. coupling_driver_iteration(iblok) == 0 ) then
        call livinf(-6_ip,'ZONAL COUPLING FOR BLOCK ', iblok)
     end if
     !
     ! Put inner iterations to zero
     !
     do iorde = 1,mmodu
        imodu = lmord(iorde,iblok)
        itinn(imodu) = 0
     end do
     !
     ! Iteration counter
     !
     coupling_driver_iteration(iblok) = coupling_driver_iteration(iblok) + 1
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!


  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_endzon() 
  use def_master,  only: iblok
  use def_master,  only: kfl_gocou 
  use def_coupli,  only: coupling_driver_iteration 
  use def_coupli,  only: coupling_driver_number_couplings
  use def_coupli,  only: coupling_driver_max_iteration
  use def_coupli,  only: kfl_gozon
  implicit none 
  integer(ip) :: iorde,imodu
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!  
     if( coupling_driver_number_couplings(iblok) /= 0 ) then
        !
        kfl_gozon = 0
        if( coupling_driver_iteration(iblok) >= coupling_driver_max_iteration(iblok) ) then
 kfl_gozon = 0 !< kernel/coupli/mod_couplings.f90 
!          return
        else
! if( resid_cou(1,icoup) > coupling_driver_tolerance(iblok) ) kfl_gozon = 1 !< kernel/coupli/mod_couplings.f90 
          kfl_gozon = 1
        endif  
        !
        if( kfl_gozon == 1 ) kfl_gocou = 1
     else
        kfl_gozon = 0
     end if
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!


  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_conblk() 
  use def_master,  only: iblok
  use def_coupli,  only: coupling_driver_number_couplings
  use def_coupli,  only : coupling_driver_iteration
  implicit none 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  if( coupling_driver_number_couplings(iblok) /= 0 ) then
    call livinf(-13_ip,'END ZONAL COUPLING: ', coupling_driver_iteration(iblok) )
  end if
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!


!-------------------------------------------------------------------------||---!
!------------------------------------------------------------------| TOOLs |---!
!-------------------------------------------------------------------------||---!


  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_get_total_flux( dU, U ) 
  use def_domain
  implicit none
  !-----------------------------------------------------------------------||---!
  !
  !     /
  ! U = | dU dA  ->  U = dU_i \delta A_i \Psi_i  ??
  !     /
  !
  !  [U] = units       <- Flux        : the rate of 'dU' transfer through a given surface A, per unit 'surface' 
  ! [dU] = units/m^2   <- Flux density: the rate per unit area 
  ! 
  !-----------------------------------------------------------------------||---!
  real(rp), intent(inout) :: dU(npoin) 
  real(rp), intent(inout) ::  U(npoin) 
  real(rp)                ::  Ut 
  real(rp)                ::  Area, dA 
  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  real(rp)                 :: baloc(ndime,ndime)
  real(rp)                 :: bocod(ndime,mnodb)
  real(rp)                 :: elcod(ndime,mnode)
  real(rp)                 :: gbcar(ndime,mnode)
  real(rp)                 :: xjaci(ndime,ndime) 
  real(rp)                 :: xjacm(ndime,ndime) 
  real(rp)                 :: gpcar(ndime,mnode,mgaus)
  real(rp)                 :: eucta, tmatr, detjm
  integer(ip)              :: ielem, inode, ipoin
  integer(ip)              :: igaus, idime, igaub, iboun, inodb, pblty
  integer(ip)              :: pnodb, pnode, pelty, pgaus, pgaub
  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  !
  gpcar = 0.0_rp
  Area  = 0.0_rp
  !
  if( INOTMASTER ) then
  !-----------------------------------------------------------------------||---!
  !
  U(1:npoin) = 0.0 
  !
  boundaries: &
  do iboun = 1,nboun
    !---------------------------------------------------------------------||---!
    pblty = ltypb(iboun)
    pnodb = lnnob(iboun)
    pgaub = ngaus(pblty)
    ielem = lboel(pnodb+1,iboun)
    pelty = ltype(ielem)
    pnode = nnode(pelty)
    pgaus = ngaus(pelty)
    !---------------------------------------------------------------------||---!
    do inodb = 1,pnodb
      ipoin = lnodb(inodb,iboun)
      bocod(1:ndime,inodb) = coord(1:ndime,ipoin)
    end do
    !
    do inode = 1,pnode
      ipoin = lnods(inode,ielem)
      elcod(1:ndime,inode) = coord(1:ndime,ipoin)
    end do
    !
    do igaus = 1,pgaus
      call elmder(pnode, ndime, elmar(pelty)%deriv(1,1,igaus), elcod, gpcar(1,1,igaus), detjm, xjacm, xjaci) 
    end do
    !---------------------------------------------------------------------||---!
    gauss_points: &
    do igaub = 1,pgaub
      !
      call bouder(pnodb, ndime, ndimb, elmar(pblty)%deriv(1,1,igaub), bocod, baloc, eucta)
      call chenor(pnode, baloc, bocod, elcod)
      dA   = elmar(pblty)%weigp(igaub) * eucta 
      Area = Area + dA 
      !
      do inodb = 1,pnodb
        ipoin    = lnodb(inodb,iboun)
        U(ipoin) = U(ipoin) + dU(ipoin) * elmar(pblty)%shape(inodb,igaub) * dA  
      end do
      !
    end do gauss_points
    !---------------------------------------------------------------------||---!
  end do boundaries
  !-----------------------------------------------------------------------||---!
  !
  Ut = sum( U(1:npoin) )/Area 
  !
  !-----------------------------------------------------------------------||---!
  endif 
  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!
  end subroutine commdom_driver_get_total_flux 
  !-----------------------------------------------------------------------||---!
  !
  !-----------------------------------------------------------------------||---!



  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  subroutine commdom_driver_get_residual()  
  use def_master, only: gesca, gevec 
  implicit none 
  integer(ip) :: ndofn
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  if(inotmaster) then 
    !
    solve => momod(modul) % solve(1:)
    ndofn =  solve(1) % ndofn
    !
    if(ndofn==ndime) then 
      call memgen(0_ip,ndime,npoin)
      gevec(1:ndime,1:npoin) = 0.0
      !
      if(solve(1)%kfl_bvnat == 1) then  
        gevec(1:ndofn,1:npoin) =  solve(1)%bvnat(   1:ndofn,1:npoin)
      endif 
      if(solve(1)%kfl_react == 1) then 
        gevec(1:ndofn,1:npoin) =  solve(1)%reaction(1:ndofn,1:npoin) 
      endif
    else&
    if(ndofn==1_ip) then 
      call memgen(0_ip,npoin,0_ip)
      gesca(1:npoin) = 0.0
      !
      if(solve(1)%kfl_bvnat == 1) then  
        gesca(1:npoin) =  solve(1)%bvnat(   1,1:npoin)
      endif 
      if(solve(1)%kfl_react == 1) then 
        gesca(1:npoin) =  solve(1)%reaction(1,1:npoin) 
      endif      
    else
      print *, "[commdom_driver_get_residual] ", " 'residue d.o.f='", solve(1) % ndofn
      call runend("[commdom_driver_get_residual] ERROR!!") 
    endif 
    !     
  endif 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine commdom_driver_get_residual 
  !-----------------------------------------------------------------------||---!


  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  subroutine  commdom_driver_n_fixno( n_fixno )
  use def_domain,         only: mcono, mcodb,  kfl_codno
  implicit none
  !
  real(rp), intent(out) :: n_fixno(npoin) 
  !
  integer(ip)          :: ipoin, ndofn, ncono 
  logical(ip)          :: codno(mcono) 
  logical(ip)          :: dirich, neumann 
  !
  type(soltyp), pointer :: solve_sol(:)
  solve_sol => momod(modul) % solve(1_ip:)
  ndofn = solve_sol(1)%ndofn
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  if(inotmaster) then 
    n_fixno(1:npoin) = 0.0 
    do ipoin = 1,npoin  
      codno(1:mcono) = kfl_codno(1:mcono,ipoin) /= mcodb+1                     !< Is it at 'boundary'?
      ncono          = count( codno(1:mcono) )                                 !< How many 'codes' are? 
      !
      if( ncono>0 ) then                                                       !< Is a  'boundary'? 
!n_fixno(ipoin) = count( abs(solve_sol(1)%kfl_fixno(1:ndofn,ipoin)) >= 0 )   !< How many 'boundary' are fixed? 
!
!< How many 'boundary' are fixed? 
dirich  = all( solve_sol(1)%kfl_fixno(1:ndofn,ipoin) /= 0 )
neumann = all( solve_sol(1)%kfl_fixno(1:ndofn,ipoin) == 0 )
n_fixno(ipoin) = count( abs(solve_sol(1)%kfl_fixno(1:ndofn,ipoin))>= 0 )
if(neumann)  n_fixno(ipoin) = -n_fixno(ipoin)
!
      endif
      !
    enddo
  endif 
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine commdom_driver_n_fixno 
  !-----------------------------------------------------------------------||---!


!-------------------------------------------------------------------------||---!
!-------------------------------------------------------------------------||---!
!-------------------------------------------------------------------------||---!
end module mod_commdom_driver 
!==============================================================================!
!==============================================================================!
