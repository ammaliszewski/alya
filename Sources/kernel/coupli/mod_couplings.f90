!------------------------------------------------------------------------
!> @addtogroup Coupling
!> @{
!> @name    Coupling functions
!> @file    mod_parall.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!> @brief   ToolBox for coupli
!> @details ToolBox for coupli
!>          To create a coupling:
!>
!>          1. Initialize the structure:
!>          - call COU_INITIALIZE_COUPLING_STRUCTURE(COUPLING)
!>          2. Compute the following:
!>          - COUPLING % GEOME % NUMBER_WET_POINTS ....................... Number of wet points
!>          - COUPLING % GEOME % NPOIN_WET ............................... Number of wet nodes
!>          - COUPLING % GEOME % COORD_WET(:,1:NUMBER_WET_POINTS) ........ Coordinates of wet points
!>          - COUPLING % GEOME % LPOIN_WET(1:NPOIN_WET) .................. List of wet nodes
!>          - COUPLING % ITYPE ........................................... Vector projection
!>          - COUPLING % KIND ............................................ BETWEEN_SUBDOMAINS/BETWEEN_ZONES
!>          - COUPLING % COLOR_TARGET .................................... Target color
!>          - COUPLING % COLOR_SOURCE .................................... Source color
!>          3. Initialize the coupling:
!>          - call COU_INIT_INTERPOLATE_POINTS_VALUES(coupling_type(icoup) % geome % coord_wet,color_target,color_source,COUPLING)
!> 
!>          Trick, if the NUMBER_WET_POINTS wet points do not have anything to do with the
!>          mesh, do the following:
!>
!>          - KIND = BETWEEN_ZONES
!>          - NPOIN_WET = NUMBER_WET_POINTS
!>          - LPOIN_WET(1:NPOIN_WET) = 1:NPOIN_WET
!> @{
!------------------------------------------------------------------------

module mod_couplings
  use def_kintyp,         only : ip,rp,lg,r1p,r2p,i1p 
  use def_master,         only : ISEQUEN,ittim
  use def_master,         only : igene ! HOY SACAR
  use def_master,         only : kfl_timin
  use def_master,         only : ittim
  use def_master,         only : current_code
  use def_master,         only : current_zone
  use def_master,         only : INOTMASTER 
  use def_master,         only : IMASTER,kfl_paral,lninv_loc
  use def_master,         only : zeror,lzone
  use def_master,         only : I_AM_IN_SUBD
  use def_domain,         only : lnods,lelez
  use def_domain,         only : nelez,lesub,nelem,lnnod
  use def_domain,         only : mnodb,mnode,ndime
  use def_domain,         only : nbono,coord,ltopo,ltype
  use def_domain,         only : lbono,npoin,lnoch
  use def_domain,         only : nboun,lnodb,lnnob
  use def_domain,         only : lelch,meshe
  use def_domain,         only : nnode
  use def_kermod,         only : ielse,relse,ndivi
  use def_elmtyp,         only : NOFRI
  use def_elmtyp,         only : ELHOL
  use mod_maths,          only : maths_mapping_coord_to_3d
  use mod_maths,          only : maths_in_box
  use mod_elmgeo,         only : elmgeo_natural_coordinates
  use mod_elmgeo,         only : elmgeo_natural_coordinates_on_boundaries
  use mod_elsest,         only : elsest_host_element
  use mod_parall,         only : PAR_THIS_NODE_IS_MINE
  use mod_parall,         only : PAR_COMM_COLOR_PERM
  use mod_parall,         only : par_part_in_color
  use mod_parall,         only : par_code_zone_subd_to_color
  use mod_parall,         only : color_target
  use mod_parall,         only : color_source
  use mod_parall,         only : par_bin_comin
  use mod_parall,         only : par_bin_comax
  use mod_parall,         only : par_bin_part
  use mod_parall,         only : par_bin_boxes
  use mod_parall,         only : par_bin_size
  use mod_parall,         only : PAR_COMM_CURRENT
  use mod_parall,         only : PAR_COMM_COLOR
  use mod_parall,         only : I_AM_IN_COLOR
  use mod_parall,         only : PAR_MY_CODE_RANK
  use mod_parall,         only : par_part_comin
  use mod_parall,         only : par_part_comax
  use mod_parall,         only : PAR_GLOBAL_TO_LOCAL_NODE
  use mod_memory,         only : memory_alloca
  use mod_memory,         only : memory_deallo
  use mod_interpolation,  only : COU_GET_INTERPOLATE_POINTS_VALUES
  use mod_interpolation,  only : COU_INITIALIZE_COUPLING_STRUCTURE 
  use mod_kdtree,         only : typ_kdtree
  use mod_kdtree,         only : kdtree_nearest_boundary
  use mod_communications, only : PAR_MIN
  use mod_communications, only : PAR_SUM
  use mod_communications, only : PAR_SEND_RECEIVE
  use mod_communications, only : PAR_COMM_RANK_AND_SIZE
  use mod_communications, only : PAR_BARRIER
  use mod_communications, only : PAR_MAX
  use mod_communications, only : PAR_START_NON_BLOCKING_COMM
  use mod_communications, only : PAR_END_NON_BLOCKING_COMM
  use mod_communications, only : PAR_SET_NON_BLOCKING_COMM_NUMBER
  use def_coupli,         only : kdtree_typ
  use def_coupli,         only : mcoup
  use def_coupli,         only : coupling_type
  use def_coupli,         only : UNKNOWN
  use def_coupli,         only : RESIDUAL
  use def_coupli,         only : DIRICHLET_IMPLICIT
  use def_coupli,         only : DIRICHLET_EXPLICIT 
  use def_coupli,         only : resid_cou
  use def_coupli,         only : ELEMENT_INTERPOLATION
  use def_coupli,         only : BOUNDARY_INTERPOLATION
  use def_coupli,         only : NEAREST_BOUNDARY_NODE
  use def_coupli,         only : NEAREST_ELEMENT_NODE
  use def_coupli,         only : BOUNDARY_VECTOR_PROJECTION
  use def_coupli,         only : ON_WHOLE_MESH
  use def_coupli,         only : typ_color_coupling
  use def_coupli,         only : memor_cou
  use def_coupli,         only : RELAXATION_SCHEME
  use def_coupli,         only : AITKEN_SCHEME
  use def_coupli,         only : STRESS_PROJECTION
  use def_coupli,         only : PROJECTION
  use def_coupli,         only : BETWEEN_SUBDOMAINS
  use def_coupli,         only : BETWEEN_ZONES
  use def_coupli,         only : ON_CHIMERA_MESH
  use def_coupli,         only : FIXED_UNKNOWN
  use def_coupli,         only : INTERFACE_MASS  
  use def_coupli,         only : GLOBAL_MASS    
  use def_coupli,         only : mcoup_subdomain 
  use def_coupli,         only : cputi_cou
  ! use extrae_module
  use def_coupli,         only : coupling_driver_couplings
  use def_coupli,         only : coupling_driver_iteration
  use def_coupli,         only : coupling_driver_max_iteration
  use def_coupli,         only : coupling_driver_number_couplings
  use def_coupli,         only : coupling_driver_tolerance
  use def_coupli,         only : nboun_cou
  use def_coupli,         only : lnodb_cou
  use def_coupli,         only : ltypb_cou
  use def_coupli,         only : lboch_cou
  use def_coupli,         only : lnnob_cou

  implicit none 
  private

!!$  interface COU_INTERPOLATE_NODAL_VALUES
!!$     module procedure COU_INTERPOLATE_NODAL_VALUES_11_real,&
!!$          &           COU_INTERPOLATE_NODAL_VALUES_22_real,&
!!$          &           COU_INTERPOLATE_NODAL_VALUES_33_real,&
!!$          &           COU_INTERPOLATE_NODAL_VALUES_12_real,&
!!$          &           COU_INTERPOLATE_NODAL_VALUES_21_real,&
!!$          &           COU_INTERPOLATE_NODAL_VALUES_1_pointer,&
!!$          &           COU_INTERPOLATE_NODAL_VALUES_2_pointer
!!$  end interface COU_INTERPOLATE_NODAL_VALUES

  interface COU_PUT_VALUE_ON_TARGET
     module procedure COU_PUT_VALUE_ON_TARGET_IP_1,&
          &           COU_PUT_VALUE_ON_TARGET_IP_2,&
          &           COU_PUT_VALUE_ON_TARGET_IP_12
  end interface COU_PUT_VALUE_ON_TARGET

  public :: COU_INTERPOLATE_NODAL_VALUES
  public :: COU_RESIDUAL_FORCE
  public :: I_AM_IN_COUPLING
  public :: I_AM_INVOLVED_IN_A_COUPLING_TYPE
  public :: COU_INIT_INTERPOLATE_POINTS_VALUES                ! Initialize color coupling
  public :: COU_PRESCRIBE_DIRICHLET_IN_MATRIX
  public :: COU_LIST_SOURCE_NODES
  public :: COU_CHECK_CONVERGENCE
  public :: THERE_EXISTS_A_ZONE_COUPLING
  public :: I_HAVE_A_FRINGE_ELEMENT
  public :: MIRROR_COUPLING
  public :: I_HAVE_A_CHIMERA_COUPLING
  public :: COU_PUT_VALUE_ON_TARGET
  public :: COU_SET_FIXITY_ON_TARGET

  public :: COU_TRANSMISSION_ARRAYS

contains

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    02/10/2014
  !> @brief   Find mirror coupling
  !> @details Obtain the mirror coupling MIRROR_COUPLING of ICOUP
  !>          It returns zero if no mirror has been found
  !>
  !----------------------------------------------------------------------
  
  function I_HAVE_A_CHIMERA_COUPLING()
    integer(ip) :: icoup 
    logical(lg) :: I_HAVE_A_CHIMERA_COUPLING

    I_HAVE_A_CHIMERA_COUPLING = .false.
    do icoup = 1,mcoup
       if(    I_AM_IN_COUPLING(icoup) .and. &
            & coupling_type(icoup) % where_type == ON_CHIMERA_MESH ) then
          I_HAVE_A_CHIMERA_COUPLING = .true.
          return
       end if
    end do

  end function I_HAVE_A_CHIMERA_COUPLING
 
  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    02/10/2014
  !> @brief   Find mirror coupling
  !> @details Obtain the mirror coupling MIRROR_COUPLING of ICOUP
  !>          It returns zero if no mirror has been found
  !
  !----------------------------------------------------------------------
  
  function MIRROR_COUPLING(icoup)
    integer(ip), intent(in) :: icoup        !< Coupling
    integer(ip)             :: MIRROR_COUPLING
    logical(lg)             :: notfound

    MIRROR_COUPLING = 0
    notfound = .true.
    do while( notfound .and. MIRROR_COUPLING < mcoup )
       MIRROR_COUPLING = MIRROR_COUPLING + 1
       if(  &
            & coupling_type(icoup) % color_target           == coupling_type(MIRROR_COUPLING) % color_source .and. &
            & coupling_type(MIRROR_COUPLING) % color_target == coupling_type(icoup) % color_source ) then
          coupling_type(icoup) % mirror_coupling = MIRROR_COUPLING
          notfound = .false.
       end if
    end do

  end function MIRROR_COUPLING
 
  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    07/03/2014
  !> @brief   If I am in a coupling
  !> @details Check if I am involved in cupling icoup
  !
  !----------------------------------------------------------------------

  function I_AM_IN_COUPLING(icoup)
    integer(ip), intent(in) :: icoup        !< Coupling
    integer(ip)             :: icolo_source
    integer(ip)             :: icolo_target
    logical(lg)             :: I_AM_IN_COUPLING

    icolo_source = coupling_type(icoup) % color_source
    icolo_target = coupling_type(icoup) % color_target
    I_AM_IN_COUPLING = I_AM_IN_COLOR(icolo_source) .or. I_AM_IN_COLOR(icolo_target)
    
  end function I_AM_IN_COUPLING

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    07/03/2014
  !> @brief   If I am in a coupling of a certain type
  !> @details If I am in a coupling of a certain type: RESIDUAL, UNKNOWN
  !>          DIRICHLET_EXPLICIT or DIRICHLET_IMPLICIT
  !
  !----------------------------------------------------------------------
  
  function I_AM_INVOLVED_IN_A_COUPLING_TYPE(ikind,iwhat,itype)
    integer(ip), intent(in)           :: ikind    !< Coupling kind (between zones, between subdomain)
    integer(ip), intent(in)           :: iwhat    !< Coupling what (residual, unknown, dirichlet)
    integer(ip), intent(in), optional :: itype    !< Coupling type (projection, interpolation)
    integer(ip)                       :: icoup
    logical(lg)                       :: I_AM_INVOLVED_IN_A_COUPLING_TYPE

    I_AM_INVOLVED_IN_A_COUPLING_TYPE = .false.

    if( ikind == BETWEEN_SUBDOMAINS .and. mcoup_subdomain == 0 ) then
       continue
    else
       if( present(itype) ) then
          do icoup = 1,mcoup
             if(    I_AM_IN_COUPLING(icoup) .and. &
                  & coupling_type(icoup) % kind  == ikind .and. &
                  & coupling_type(icoup) % what  == iwhat .and. &
                  & coupling_type(icoup) % itype == itype ) then
                I_AM_INVOLVED_IN_A_COUPLING_TYPE = .true.
             end if
          end do
       else
          do icoup = 1,mcoup
             if(    I_AM_IN_COUPLING(icoup) .and. &
                  & coupling_type(icoup) % kind == ikind .and. &
                  & coupling_type(icoup) % what == iwhat ) then
                I_AM_INVOLVED_IN_A_COUPLING_TYPE = .true.
             end if
          end do
       end if
    end if

  end function I_AM_INVOLVED_IN_A_COUPLING_TYPE

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    23/09/2014
  !> @brief   Check if I have a fringe element
  !> @details A fringe element is a hole element with at least one if
  !>          fringe node
  !
  !----------------------------------------------------------------------
  
  function I_HAVE_A_FRINGE_ELEMENT()
    integer(ip) :: ipoin,ielem,inode
    logical(lg) :: I_HAVE_A_FRINGE_ELEMENT

    I_HAVE_A_FRINGE_ELEMENT = .false.
    do ielem = 1,nelem
       if( lelch(ielem) == ELHOL ) then
          do inode = 1,lnnod(ielem)
             ipoin = lnods(inode,ielem)
             if( lnoch(ipoin) == NOFRI ) then
                I_HAVE_A_FRINGE_ELEMENT = .true.
                return
             end if
          end do
       end if
    end do

  end function I_HAVE_A_FRINGE_ELEMENT

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    23/09/2014
  !> @brief   Check if I have a hole element
  !> @details A hole element has only hole nodes
  !
  !----------------------------------------------------------------------
  
  function I_HAVE_A_HOLE_ELEMENT()
    integer(ip) :: ipoin,ielem,inode
    logical(lg) :: I_HAVE_A_HOLE_ELEMENT

    I_HAVE_A_HOLE_ELEMENT = .false.
    do ielem = 1,nelem
       if( lelch(ielem) == ELHOL ) then
          I_HAVE_A_HOLE_ELEMENT = .true.
          return
       end if
    end do

  end function I_HAVE_A_HOLE_ELEMENT

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    30/09/2014
  !> @brief   Check convergence
  !> @details Check convergence of a coupling
  !
  !----------------------------------------------------------------------

  subroutine COU_CHECK_CONVERGENCE(iblok,kfl_gozon)
    integer(ip), intent(in)  :: iblok
    integer(ip), intent(out) :: kfl_gozon
    integer(ip)              :: icoup,kcoup
    integer(ip)              :: idime,ipoin,kpoin

    kfl_gozon = 0

    if( coupling_driver_iteration(iblok) >= coupling_driver_max_iteration(iblok) ) then
       do kcoup = 1,coupling_driver_number_couplings(iblok)
          icoup = coupling_driver_couplings(kcoup,iblok)
          !
          ! Update values for subcycling coupling (frequency of exchanges different of one)
          !
          if( coupling_type(icoup) % frequ_send > 1_ip .or. coupling_type(icoup) % frequ_recv > 1_ip )then
             ! 
             ! Only source code save exchanging values
             !
             if( current_code == coupling_type(icoup) % code_source )then

                do kpoin = 1_ip, coupling_type(icoup) % geome % npoin_source
                   ipoin = coupling_type(icoup) % geome % lpoin_source(kpoin)
                   do idime = 1_ip, ndime
                      coupling_type(icoup) % values_frequ(idime,kpoin,1_ip) = coupling_type(icoup) % values_frequ(idime,kpoin,2_ip)
                   end do
                end do
             end if
          end if
       end do
       return
    else
       do kcoup = 1,coupling_driver_number_couplings(iblok)
          icoup = coupling_driver_couplings(kcoup,iblok)
          !
          ! Only check if the current time step is a multiple of the frequency defined
          !
          if( mod( ittim,coupling_type(icoup) % frequ_send ) == 0_ip .and. current_code == coupling_type(icoup) % code_source )then
             if( resid_cou(1,icoup) > coupling_driver_tolerance(iblok) )then 
                kfl_gozon = 1
             end if
          end if
          if( mod( ittim,coupling_type(icoup) % frequ_recv ) == 0_ip .and. current_code == coupling_type(icoup) % code_target )then
             if( resid_cou(1,icoup) > coupling_driver_tolerance(iblok) )then 
                kfl_gozon = 1
             end if
          end if
       end do
       !
       ! Update values exchanged when convergence is reached and frequency of exchanges is larger than one
       !
       if( coupling_type(icoup) % frequ_send > 1_ip .or. coupling_type(icoup) % frequ_recv > 1_ip )then
          do kcoup = 1,coupling_driver_number_couplings(iblok)
             icoup = coupling_driver_couplings(kcoup,iblok)
             if( mod( ittim,coupling_type(icoup) % frequ_send) == 0_ip .and. kfl_gozon==0 .and. current_code == coupling_type(icoup) % code_source )then
                do kpoin = 1_ip, coupling_type(icoup) % geome % npoin_source

                   ipoin = coupling_type(icoup) % geome % lpoin_source(kpoin)
                   do idime = 1_ip, ndime
                      coupling_type(icoup) % values_frequ(idime,kpoin,1_ip) = coupling_type(icoup) % values_frequ(idime,kpoin,2_ip)
                   end do

                end do

             end if
          end do
       end if
    end if

  end subroutine COU_CHECK_CONVERGENCE

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    07/03/2014
  !> @brief   Actualize unknown according to scheme
  !> @details Actualize the unknown and save previous values according 
  !>          to the scheme (relaxation, Aitken, etc.)
  !
  !----------------------------------------------------------------------
  subroutine COU_UPDATE_POINTS_VALUES(xxnew,coupling,xresi)
    real(rp),      pointer,   intent(inout) :: xxnew(:,:)
    type(typ_color_coupling), intent(inout) :: coupling
    real(rp),                 intent(out)   :: xresi(2)
    integer(ip)                             :: ndofn,ipoin,npoin_wet,ntime_wet
    integer(ip)                             :: idofn,itime,kpoin
    real(rp)                                :: relax,rela1,rip1,rip2
    real(rp)                                :: numer,denom
    
    coupling % itera = coupling % itera + 1

    if( INOTMASTER ) then
       !
       ! Degrees of freedom
       !
       ndofn     = size(xxnew,1)
       npoin_wet = coupling % geome % npoin_wet
       !
       ! Allocate memory if required
       !
       if( .not. associated(coupling % values) )then
          if( coupling % scheme == RELAXATION_SCHEME )then     
             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values,ndofn,npoin_wet,2_ip)
          else if( coupling % scheme == AITKEN_SCHEME )then     
             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values,ndofn,npoin_wet,3_ip)
             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values_predicted,ndofn,npoin_wet)
          end if
        end if
       ntime_wet = size(coupling % values,3)
    else
       ndofn     = 0
       npoin_wet = 0
       ntime_wet = 0
    end if
    !
    ! Save old relaxed values
    !
    do itime = ntime_wet,2,-1
       do ipoin = 1,npoin_wet
          do idofn = 1,ndofn
             coupling % values(idofn,ipoin,itime) = coupling % values(idofn,ipoin,itime-1)
          end do
       end do
    end do
    !
    ! Aitken: copy predicted value
    ! 
    if( coupling % scheme == RELAXATION_SCHEME ) then
       relax = coupling % relax
    else if( coupling % scheme == AITKEN_SCHEME ) then
       !
       ! First two iterations are performed with constant relaxation
       !
       if( coupling_driver_iteration(1_ip) < 3_ip ) then
       ! if( coupling % itera < 3_ip ) then

          coupling % aitken = coupling % relax
          relax             = coupling % aitken
          ! print*, "DEBUG: AITKEN inicial ", relax

       else
          !
          ! Scalar product for aitken relaxation factor
          !
          numer = 0.0_rp
          denom = 0.0_rp
          do ipoin = 1,npoin_wet
             do idofn = 1,ndofn
                !
                ! rip1 = d_{i}   - d_{i+1}'
                !
                rip1  = coupling % values_predicted(idofn,ipoin) - coupling % values(idofn,ipoin,3_ip) 
                ! rip1 = ( coupling % values(idofn,ipoin,2_ip) -  coupling % values(idofn,ipoin,3_ip) ) * xxnew(idofn,ipoin)
                !
                ! rip2 = d_{i+1} - d_{i+2}'
                !
                rip2  =  xxnew(idofn,ipoin) - coupling % values(idofn,ipoin,2_ip)
                ! rip2 = xxnew(idofn,ipoin) - coupling % values_predicted(idofn,ipoin)
                ! rip3 = xxnew(idofn,ipoin) - coupling % values(idofn,ipoin,2_ip)
                numer = numer + rip1 * (rip2-rip1)
                denom = denom + (rip2-rip1)*(rip2-rip1)
                ! numer = numer + rip1
                ! denom = denom + rip2 * rip3
             end do
          end do

          call PAR_SUM(numer,'IN CURRENT COLOR')
          call PAR_SUM(denom,'IN CURRENT COLOR')
          relax =-coupling % aitken * numer / (denom+zeror)
          ! relax = - numer / (denom + zeror)
          ! relax = min(relax, 1._rp)
          ! relax = max(relax,-1._rp)
          coupling % aitken = relax
          ! aux = dabs(relax)
          ! print*, "DEBUG: AITKEN ", relax
       end if
       
    end if
    !
    ! Save unrelaxed results for next aitken calculation (must be performed in all iterations)
    !
    if( coupling % scheme == AITKEN_SCHEME ) then
       do ipoin = 1_ip, npoin_wet
          do idofn = 1_ip, ndofn
             coupling % values_predicted(idofn,ipoin) = xxnew(idofn,ipoin)
          end do
       end do
    end if
    !
    ! Relaxation of the solution
    !
    if( coupling % scheme == AITKEN_SCHEME .and. coupling_driver_iteration(1_ip) > 2_ip ) then
       rela1    = 1.0_rp - relax
       do ipoin = 1,npoin_wet
          do idofn = 1,ndofn
             xxnew(idofn,ipoin) = rela1 * coupling % values(idofn,ipoin,2_ip) + relax * xxnew(idofn,ipoin)
             coupling % values(idofn,ipoin,1) = xxnew(idofn,ipoin)
          end do
       end do
    else
       rela1    = 1.0_rp - relax
       do ipoin = 1,npoin_wet
          do idofn = 1,ndofn
             xxnew(idofn,ipoin) = relax * xxnew(idofn,ipoin) + rela1 * coupling % values(idofn,ipoin,2)
             coupling % values(idofn,ipoin,1) = xxnew(idofn,ipoin)
          end do
       end do
    end if
    !
    ! Residual
    !
    xresi = 0.0_rp
    do kpoin = 1,npoin_wet
       ipoin = coupling % geome % lpoin_wet(kpoin)
       if( PAR_THIS_NODE_IS_MINE(ipoin) ) then
          do idofn = 1,ndofn
             xresi(2) = xresi(2) +  coupling % values(idofn,kpoin,1)**2
             xresi(1) = xresi(1) + (coupling % values(idofn,kpoin,1)-coupling % values(idofn,kpoin,2))**2
          end do
       end if
    end do

  end subroutine COU_UPDATE_POINTS_VALUES
!!$
!!$  subroutine COU_UPDATE_POINTS_VALUES(xxnew,coupling,xresi)
!!$    real(rp),      pointer,   intent(inout) :: xxnew(:,:)
!!$    type(typ_color_coupling), intent(inout) :: coupling
!!$    real(rp),                 intent(out)   :: xresi(2)
!!$    integer(ip)                             :: ndofn,ipoin,npoin_wet,ntime_wet
!!$    integer(ip)                             :: idofn,itime,kpoin
!!$    real(rp)                                :: relax,rela1,rip1,rip2
!!$    real(rp)                                :: numer,denom
!!$
!!$    ! real(rp),      pointer                  :: aitken(coupling % geome % npoin_wet)
!!$    real(rp)                                :: aitsum
!!$
!!$    coupling % itera = coupling % itera + 1
!!$
!!$    if( INOTMASTER ) then
!!$       !
!!$       ! Degrees of freedom
!!$       !
!!$       ndofn     = size(xxnew,1)
!!$       npoin_wet = coupling % geome % npoin_wet
!!$       !
!!$       ! Allocate memory if required
!!$       !
!!$       if( .not. associated(coupling % values) )then
!!$          if( coupling % scheme == RELAXATION_SCHEME )then     
!!$             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values,ndofn,npoin_wet,2_ip)
!!$          else if( coupling % scheme == AITKEN_SCHEME )then     
!!$             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values,ndofn,npoin_wet,3_ip)
!!$             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values_predicted,ndofn,npoin_wet)
!!$          end if
!!$        end if
!!$       ntime_wet = size(coupling % values,3)
!!$    else
!!$       ndofn     = 0
!!$       npoin_wet = 0
!!$       ntime_wet = 0
!!$    end if
!!$    !
!!$    ! Save old relaxed values
!!$    !
!!$    do itime = ntime_wet,2,-1
!!$       do ipoin = 1,npoin_wet
!!$          do idofn = 1,ndofn
!!$             coupling % values(idofn,ipoin,itime) = coupling % values(idofn,ipoin,itime-1)
!!$          end do
!!$       end do
!!$    end do
!!$    !
!!$    ! Aitken: copy predicted value
!!$    ! 
!!$    if( coupling % scheme == RELAXATION_SCHEME ) then
!!$       relax = coupling % relax
!!$    else if( coupling % scheme == AITKEN_SCHEME ) then
!!$       !
!!$       ! First iteration are performed with constant relaxation
!!$       !
!!$       if( coupling % itera < 3_ip ) then
!!$ 
!!$          coupling % aitken = coupling % relax
!!$          relax = coupling % aitken
!!$
!!$       else
!!$          !
!!$          ! Scalar product for aitken relaxation factor
!!$          !
!!$          numer = 0.0_rp
!!$          denom = 0.0_rp
!!$          do ipoin = 1,npoin_wet
!!$             do idofn = 1,ndofn
!!$                !
!!$                ! rip1 = d_{i}   - d_{i+1}'
!!$                !
!!$                rip1  = coupling % values_predicted(idofn,ipoin) - coupling % values(idofn,ipoin,3_ip) 
!!$                !
!!$                ! rip2 = d_{i+1} - d_{i+2}'
!!$                !
!!$                rip2  =  xxnew(idofn,ipoin) - coupling % values(idofn,ipoin,2_ip)
!!$                numer = numer + rip1 * (rip2-rip1)
!!$                denom = denom + (rip2-rip1)*(rip2-rip1)
!!$             end do
!!$          end do
!!$
!!$          call PAR_SUM(numer,'IN CURRENT COLOR')
!!$          call PAR_SUM(denom,'IN CURRENT COLOR')
!!$          relax =-coupling % aitken * numer / (denom+zeror)
!!$          coupling % aitken = relax
!!$
!!$       end if
!!$       
!!$    end if
!!$    !
!!$    ! Save unrelaxed results for next aitken calculation (must be performed in all iterations)
!!$    !
!!$    if( coupling % scheme == AITKEN_SCHEME ) then
!!$       do ipoin = 1_ip, npoin_wet
!!$          do idofn = 1_ip, ndofn
!!$             coupling % values_predicted(idofn,ipoin) = xxnew(idofn,ipoin)
!!$          end do
!!$       end do
!!$    end if
!!$    !
!!$    ! Relaxation of the solution
!!$    !  
!!$    rela1    = 1.0_rp - relax
!!$    do ipoin = 1,npoin_wet
!!$       do idofn = 1,ndofn
!!$          xxnew(idofn,ipoin) = relax * xxnew(idofn,ipoin) + rela1 * coupling % values(idofn,ipoin,2)
!!$          coupling % values(idofn,ipoin,1) = xxnew(idofn,ipoin)
!!$       end do
!!$    end do
!!$    !
!!$    ! Residual
!!$    !
!!$    xresi = 0.0_rp
!!$    do kpoin = 1,npoin_wet
!!$       ipoin = coupling % geome % lpoin_wet(kpoin)
!!$       if( PAR_THIS_NODE_IS_MINE(ipoin) ) then
!!$          do idofn = 1,ndofn
!!$             xresi(2) = xresi(2) +  coupling % values(idofn,kpoin,1)**2
!!$             xresi(1) = xresi(1) + (coupling % values(idofn,kpoin,1)-coupling % values(idofn,kpoin,2))**2
!!$          end do
!!$       end if
!!$    end do
!!$
!!$  end subroutine COU_UPDATE_POINTS_VALUES

!!$  subroutine COU_UPDATE_POINTS_VALUES(xxnew,coupling,xresi)
!!$    real(rp),      pointer,   intent(inout) :: xxnew(:,:)
!!$    type(typ_color_coupling), intent(inout) :: coupling
!!$    real(rp),                 intent(out)   :: xresi(2)
!!$    integer(ip)                             :: ndofn,ipoin,npoin_wet,ntime_wet
!!$    integer(ip)                             :: idofn,itime,kpoin
!!$    real(rp)                                :: relax,rela1,rip1,rip2
!!$    real(rp)                                :: numer,denom
!!$
!!$    coupling % itera = coupling % itera + 1
!!$
!!$    if( INOTMASTER ) then
!!$       !
!!$       ! Degrees of freedom
!!$       !
!!$       ndofn     = size(xxnew,1)
!!$       npoin_wet = coupling % geome % npoin_wet
!!$       !
!!$       ! Allocate memory if required
!!$       !
!!$       if( .not. associated(coupling % values) ) then
!!$          if( coupling % scheme == RELAXATION_SCHEME ) then     
!!$             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values,ndofn,npoin_wet,2_ip)
!!$          else if( coupling % scheme == AITKEN_SCHEME ) then     
!!$             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values,ndofn,npoin_wet,3_ip)
!!$             call memory_alloca(memor_cou,'MOD_COUPLINGS','values',coupling % values_predicted,ndofn,npoin_wet)
!!$          end if
!!$       end if
!!$       ntime_wet = size(coupling % values,3)
!!$    else
!!$       ndofn     = 0
!!$       npoin_wet = 0
!!$       ntime_wet = 0
!!$    end if
!!$    !
!!$    ! Save old values
!!$    !
!!$    do itime = ntime_wet,2,-1
!!$       do ipoin = 1,npoin_wet
!!$          do idofn = 1,ndofn
!!$             coupling % values(idofn,ipoin,itime) = coupling % values(idofn,ipoin,itime-1)
!!$          end do
!!$       end do
!!$    end do
!!$    !
!!$    ! Aitken: copy predicted value
!!$    ! 
!!$    if( coupling % scheme == RELAXATION_SCHEME ) then
!!$       relax = coupling % relax
!!$    else if( coupling % scheme == AITKEN_SCHEME ) then
!!$       if( coupling % itera == 1 ) then
!!$          relax = coupling % relax
!!$       else
!!$          numer = 0.0_rp
!!$          denom = 0.0_rp
!!$          do ipoin = 1,npoin_wet
!!$             do idofn = 1,ndofn
!!$                ! rip1 = d_{i+1}' - d_i
!!$                rip1  = coupling % values_predicted(idofn,ipoin) - coupling % values(idofn,ipoin,3)
!!$                ! rip2 = d_{i+2}' - d_i+1
!!$                rip2  = xxnew(idofn,ipoin) - coupling % values(idofn,ipoin,2)
!!$                numer = numer + rip1 * (rip2-rip1)
!!$                denom = denom + (rip2-rip1)**2
!!$             end do
!!$          end do
!!$          call PAR_SUM(numer,'IN CURRENT COLOR')
!!$          call PAR_SUM(denom,'IN CURRENT COLOR')
!!$
!!$
!!$          relax            = -coupling % relax * numer / (sqrt(denom)+zeror)
!!$          relax            = max(relax,0.01_rp)
!!$
!!$          !if( abs(relax) <= zeror ) relax = sign(0.01_rp,relax)
!!$          !if( relax <= zeror ) relax = 0.001_rp
!!$          rela1            =  1.0_rp-relax
!!$          coupling % relax =  relax
!!$          do ipoin = 1,npoin_wet
!!$             do idofn = 1,ndofn
!!$                coupling % values_predicted(idofn,ipoin) = xxnew(idofn,ipoin) 
!!$             end do
!!$          end do
!!$       end if
!!$    end if
!!$    !
!!$    ! Simple relaxation
!!$    !  
!!$    rela1     = 1.0_rp - relax
!!$    do ipoin = 1,npoin_wet
!!$       do idofn = 1,ndofn
!!$          xxnew(idofn,ipoin) = relax * xxnew(idofn,ipoin) + rela1 * coupling % values(idofn,ipoin,2)
!!$          coupling % values(idofn,ipoin,1) = xxnew(idofn,ipoin)
!!$       end do
!!$    end do
!!$    !
!!$    ! Residual
!!$    !
!!$    xresi = 0.0_rp
!!$    do kpoin = 1,npoin_wet
!!$       ipoin = coupling % geome % lpoin_wet(kpoin)
!!$       if( PAR_THIS_NODE_IS_MINE(ipoin) ) then
!!$          do idofn = 1,ndofn
!!$             xresi(2) = xresi(2) +  coupling % values(idofn,kpoin,1)**2
!!$             xresi(1) = xresi(1) + (coupling % values(idofn,kpoin,1)-coupling % values(idofn,kpoin,2))**2
!!$          end do
!!$       end if
!!$    end do
!!$
!!$  end subroutine COU_UPDATE_POINTS_VALUES
!!$
!!$  subroutine COU_INTERPOLATE_NODAL_VALUES_12_real(icoup,ndofn,xtarget,xsource,mask)
!!$    integer(ip), intent(in)           :: icoup
!!$    integer(ip), intent(in)           :: ndofn
!!$    real(rp),    intent(in)           :: xsource(ndofn,*) 
!!$    real(rp),    intent(out)          :: xtarget(*) 
!!$    integer(ip), intent(in), optional :: mask(ndofn,*)
!!$    if( present(mask) ) then
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource,mask)
!!$    else
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource)
!!$    end if
!!$  end subroutine COU_INTERPOLATE_NODAL_VALUES_12_real
!!$  subroutine COU_INTERPOLATE_NODAL_VALUES_21_real(icoup,ndofn,xtarget,xsource,mask)
!!$    integer(ip), intent(in)           :: icoup
!!$    integer(ip), intent(in)           :: ndofn
!!$    real(rp),    intent(in)           :: xsource(*) 
!!$    real(rp),    intent(out)          :: xtarget(ndofn,*) 
!!$    integer(ip), intent(in), optional :: mask(ndofn,*)
!!$    if( present(mask) ) then
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource,mask)
!!$    else
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource)
!!$    end if
!!$  end subroutine COU_INTERPOLATE_NODAL_VALUES_21_real
!!$  subroutine COU_INTERPOLATE_NODAL_VALUES_11_real(icoup,ndofn,xtarget,xsource,mask)
!!$    integer(ip), intent(in)           :: icoup
!!$    integer(ip), intent(in)           :: ndofn
!!$    real(rp),    intent(in)           :: xsource(*) 
!!$    real(rp),    intent(out)          :: xtarget(*) 
!!$    integer(ip), intent(in), optional :: mask(ndofn,*)
!!$    if( present(mask) ) then
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource,mask)
!!$    else
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource)
!!$    end if
!!$  end subroutine COU_INTERPOLATE_NODAL_VALUES_11_real
!!$  subroutine COU_INTERPOLATE_NODAL_VALUES_22_real(icoup,ndofn,xtarget,xsource,mask)
!!$    integer(ip), intent(in)           :: icoup
!!$    integer(ip), intent(in)           :: ndofn
!!$    real(rp),    intent(in)           :: xsource(ndofn,*) 
!!$    real(rp),    intent(out)          :: xtarget(ndofn,*) 
!!$    integer(ip), intent(in), optional :: mask(ndofn,*)
!!$    if( present(mask) ) then
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource,mask)
!!$    else
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource)
!!$    end if
!!$  end subroutine COU_INTERPOLATE_NODAL_VALUES_22_real
!!$  subroutine COU_INTERPOLATE_NODAL_VALUES_33_real(icoup,ndofn,xtarget,xsource,mask)
!!$    integer(ip), intent(in)           :: icoup
!!$    integer(ip), intent(in)           :: ndofn
!!$    real(rp),    intent(in)           :: xsource(1,1,*) 
!!$    real(rp),    intent(out)          :: xtarget(1,1,*) 
!!$    integer(ip), intent(in), optional :: mask(ndofn,*)
!!$    if( present(mask) ) then
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource,mask)
!!$    else
!!$       call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource)
!!$    end if
!!$  end subroutine COU_INTERPOLATE_NODAL_VALUES_33_real
!!$
!!$  subroutine COU_INTERPOLATE_NODAL_VALUES_2_pointer(icoup,xtarget,xsource,mask)
!!$    integer(ip), intent(in)                       :: icoup
!!$    real(rp),    intent(in),    pointer           :: xsource(:,:) 
!!$    real(rp),    intent(inout), pointer           :: xtarget(:,:) 
!!$    integer(ip), intent(in),    pointer, optional :: mask(:,:)
!!$    real(rp),                   target            :: xtarget_tmp(1,1) 
!!$    real(rp),                   target            :: xsource_tmp(1,1) 
!!$    integer(ip)                                   :: ndofn
!!$    if( associated(xsource) .and. associated(xtarget) ) then
!!$       ndofn = size(xsource,1)
!!$       if( present(mask) ) then
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource,mask)
!!$       else
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource)
!!$       end if
!!$    else if( associated(xsource) .and. .not. associated(xtarget) ) then
!!$       ndofn = size(xsource,1)
!!$       if( present(mask) ) then
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget_tmp,xsource,mask)
!!$       else
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget_tmp,xsource)
!!$       end if
!!$    else if( .not. associated(xsource) .and. associated(xtarget) ) then
!!$       ndofn = size(xtarget,1)
!!$       if( present(mask) ) then
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource_tmp,mask)
!!$       else
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource_tmp)
!!$       end if
!!$    else if( .not. associated(xsource) .and. .not. associated(xtarget) ) then
!!$       ndofn = 1
!!$       if( present(mask) ) then
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget_tmp,xsource_tmp,mask)
!!$       else
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget_tmp,xsource_tmp)
!!$       end if
!!$    end if
!!$  end subroutine COU_INTERPOLATE_NODAL_VALUES_2_pointer
!!$
!!$  subroutine COU_INTERPOLATE_NODAL_VALUES_1_pointer(icoup,xtarget,xsource,mask)
!!$    integer(ip), intent(in)                       :: icoup
!!$    real(rp),    intent(in),    pointer           :: xsource(:) 
!!$    real(rp),    intent(inout), pointer           :: xtarget(:) 
!!$    integer(ip), intent(in),    pointer, optional :: mask(:,:)
!!$    real(rp),                   target            :: xtarget_tmp(1,1) 
!!$    real(rp),                   target            :: xsource_tmp(1,1) 
!!$    integer(ip)                                   :: ndofn
!!$
!!$    ndofn = 1
!!$    if( associated(xsource) .and. associated(xtarget) ) then
!!$       if( present(mask) ) then
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource,mask)
!!$       else
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource)
!!$       end if
!!$    else if( associated(xsource) .and. .not. associated(xtarget) ) then
!!$       if( present(mask) ) then
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget_tmp,xsource,mask)
!!$       else
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget_tmp,xsource)
!!$       end if
!!$    else if( .not. associated(xsource) .and. associated(xtarget) ) then
!!$       if( present(mask) ) then
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource_tmp,mask)
!!$       else
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget,xsource_tmp)
!!$       end if
!!$    else if( .not. associated(xsource) .and. .not. associated(xtarget) ) then
!!$       if( present(mask) ) then
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget_tmp,xsource_tmp,mask)
!!$       else
!!$          call COU_INTERPOLATE_NODAL_VALUES_RP(icoup,ndofn,xtarget_tmp,xsource_tmp)
!!$       end if
!!$    end if
!!$  end subroutine COU_INTERPOLATE_NODAL_VALUES_1_pointer

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Interpolate and modify nodal arrays
  !> @details Do the following
  !>                               Interpolate
  !>          XTARGET(NDOFN,NPOIN)     <=       XSOURCE(NDOFN,NPOIN)
  !>        
  !>          1. Allocate XINTERP(NDOFN,NPOIN_WET) 
  !>
  !>          2. Interpolate XINTERP(NDOFN,NPOIN_WET) FROM XSOURCE:
  !>
  !>                     COU_GET_INTERPOLATE_POINTS_VALUES 
  !>             XINTERP              <=                   XSOURCE
  !>             
  !>
  !>          3. Scatter solution:
  !>                     LPOIN_WET
  !>             XTARGET    <=    XINTERP
  !>
  !----------------------------------------------------------------------

  subroutine COU_INTERPOLATE_NODAL_VALUES(icoup,ndofn,xtarget,xsource,mask)
    integer(ip), intent(in)           :: icoup
    integer(ip), intent(in)           :: ndofn
    real(rp),    intent(in)           :: xsource(ndofn,*) 
    real(rp),    intent(out)          :: xtarget(ndofn,*) 
    integer(ip), intent(in), optional :: mask(ndofn,*)
    real(rp),                pointer  :: xinterp(:,:)
    real(rp)                          :: xnew,xresi(2)
    integer(ip)                       :: ipoin,kpoin,idofn,icolo,npoin_wet
    integer(ip)                       :: jcoup
    logical(lg)                       :: subdo_coupling
    real(rp)                          :: weight
    !
    ! Allocate values
    !
    subdo_coupling = .false.
    if( coupling_type(icoup) % zone_target == 0 ) then
       if( I_AM_IN_SUBD( coupling_type(icoup) % subdomain_target) ) subdo_coupling = .true.
       if( I_AM_IN_SUBD( coupling_type(icoup) % subdomain_source) ) subdo_coupling = .true.
       icolo = par_code_zone_subd_to_color(current_code,0_ip,0_ip)     
    else
       icolo = par_code_zone_subd_to_color(current_code,current_zone,0_ip)
    end if

    color_target = coupling_type(icoup) % color_target
    color_source = coupling_type(icoup) % color_source
    npoin_wet    = coupling_type(icoup) % geome % npoin_wet
    xresi        = 0.0_rp
    if( IMASTER ) npoin_wet = 0
    nullify(xinterp)

    if( icolo == color_target .or. icolo == color_source .or. subdo_coupling ) then

       if( current_code == coupling_type(icoup) % code_target .and. INOTMASTER ) then  
          call memory_alloca(memor_cou,'XINTERP','cou_interpolate_nodal_values',xinterp,ndofn,max(1_ip,npoin_wet))
       else
          call memory_alloca(memor_cou,'XINTERP','cou_interpolate_nodal_values',xinterp,1_ip,1_ip)
       end if
       !
       ! Interpolate values from XSOURCE
       !
       call COU_GET_INTERPOLATE_POINTS_VALUES(ndofn,xsource,xinterp,coupling_type(icoup)) 
       !
       ! Permute value to target XTARGET
       !
       if( current_code == coupling_type(icoup) % code_target ) then
          !
          ! Update solution according to scheme (relaxation, Aitken, etc.)
          !
          if( .not. subdo_coupling ) call COU_UPDATE_POINTS_VALUES(xinterp,coupling_type(icoup),xresi)

          if(    coupling_type(icoup) %  what == UNKNOWN            .or. &
               & coupling_type(icoup) %  what == DIRICHLET_IMPLICIT .or. &
               & coupling_type(icoup) %  what == DIRICHLET_EXPLICIT ) then
             !
             ! Unknown
             !
             do kpoin = 1,npoin_wet
                ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                do idofn = 1,ndofn
                   xtarget(idofn,ipoin) = xinterp(idofn,kpoin)
                end do
             end do

          else if( coupling_type(icoup) % what == RESIDUAL ) then

             if( present(mask) ) then
                !
                ! Force with mask
                !
                do kpoin = 1,npoin_wet
                   ipoin  = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                   weight = coupling_type(icoup) % geome % weight_wet(kpoin)
                   do idofn = 1,ndofn 
                      if( mask(idofn,ipoin) == 0 ) then
                         xtarget(idofn,ipoin) = xtarget(idofn,ipoin) + weight * xinterp(idofn,kpoin)
                      end if
                   end do
                end do
             else
                !
                ! Force without mask
                !
                do kpoin = 1,npoin_wet
                   ipoin  = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                   weight = coupling_type(icoup) % geome % weight_wet(kpoin)
                   do idofn = 1,ndofn
                      xtarget(idofn,ipoin) = xtarget(idofn,ipoin) + weight * xinterp(idofn,kpoin)
                   end do
                end do
             end if
          else
             call runend('WRONG TAG')
          end if
          !
          ! Conservation
          !
          if( present(mask) ) then
             call COU_CONSERVATION(coupling_type(icoup),ndofn,xtarget,mask)
          end if
       end if
       call memory_deallo(memor_cou,'XINTERP','cou_interpolate_nodal_values',xinterp)
       !
       ! It should be in color icolo and jcolo!
       !
       if( .not. subdo_coupling ) then
          call PAR_SUM(2_ip,xresi,'IN CURRENT COUPLING')
          resid_cou(2,icoup) = xresi(2)
          resid_cou(1,icoup) = xresi(1) / ( xresi(2) + zeror )
       end if
       
    end if

  end subroutine COU_INTERPOLATE_NODAL_VALUES

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Conservation
  !> @details Apply a conservation algorithm
  !>
  !----------------------------------------------------------------------

  subroutine COU_CONSERVATION(coupling,ndofn,xtarget,mask)
    use mod_projec, only : projec_mass_conservation
    type(typ_color_coupling), intent(in)    :: coupling
    integer(ip),              intent(in)    :: ndofn
    integer(ip),              intent(in)    :: mask(ndofn,*)
    real(rp),                 intent(inout) :: xtarget(ndofn,*) 
    integer(ip)                             :: kboun,iboun,inodb,ipoin
    integer(ip)                             :: idofn
    logical(lg)                             :: mark_node
    logical(lg),              pointer       :: gboun(:)
    logical(lg),              pointer       :: gpoin(:)

    if( coupling % conservation /= 0 ) then

       if( INOTMASTER ) then
          allocate( gboun(nboun) )
          allocate( gpoin(npoin) )
          gboun = .false.
          gpoin = .false.
          do kboun = 1,coupling % geome % nboun_wet 
             iboun = coupling % geome % lboun_wet(kboun)
             gboun(iboun) = .true.
             do inodb = 1,lnnob(iboun)
                ipoin = lnodb(inodb,iboun) 
                mark_node = .true.
                loop_idofn: do idofn = 1,ndofn
                   if( mask(idofn,ipoin) /= FIXED_UNKNOWN ) then
                      mark_node = .false.
                      exit loop_idofn
                   end if
                end do loop_idofn
                if( mark_node ) gpoin(ipoin) = .true.
             end do
          end do
       else
          allocate( gboun(1) )
          allocate( gpoin(1) )
       end if
       if( coupling % conservation == INTERFACE_MASS ) then
          call projec_mass_conservation(xtarget,gboun,gpoin,'LOCAL MASS')
       else if( coupling % conservation == GLOBAL_MASS ) then
          call projec_mass_conservation(xtarget,gboun,gpoin,'GLOBAL MASS')    
       else
          call runend('COU_CONSERVATION: CONSERVATION NOT CODED')
       end if
       deallocate( gboun )
       deallocate( gpoin )
 
    end if

end subroutine COU_CONSERVATION

  subroutine COU_RESIDUAL_FORCE(ndofn,ia,ja,amatr,rhsid,unkno,force_rhs)
    use def_kintyp, only        :  ip,rp
    use def_master, only        :  INOTMASTER
    use def_domain, only        :  npoin
    implicit none
    integer(ip),    intent(in)  :: ndofn
    integer(ip),    intent(in)  :: ia(*)
    integer(ip),    intent(in)  :: ja(*)
    real(rp),       intent(in)  :: amatr(ndofn,ndofn,*)
    real(rp),       intent(in)  :: rhsid(ndofn,*)
    real(rp),       intent(in)  :: unkno(ndofn,*)
    real(rp),       intent(out) :: force_rhs(ndofn,*)
    integer(ip)                 :: ipoin,iz,jpoin,idofn,jdofn
    !
    ! Marcar unicamente los nodos que tocan los elementos
    !
    if( INOTMASTER ) then
       do ipoin = 1,npoin
          force_rhs(1:ndofn,ipoin) = rhsid(1:ndofn,ipoin)
          do iz = ia(ipoin),ia(ipoin+1)-1
             jpoin = ja(iz)
             do idofn = 1,ndofn
                do jdofn = 1,ndofn
                   force_rhs(idofn,ipoin) = &
                        force_rhs(idofn,ipoin) - amatr(jdofn,idofn,iz)*unkno(jdofn,jpoin)
                end do
             end do
          end do
       end do
       call rhsmod(ndofn,force_rhs)
    end if

  end subroutine COU_RESIDUAL_FORCE

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    13/04/2014
  !> @brief   Interpolation communication arrays
  !> @details Interpolation communication arrays
  !>          Input variables:
  !>          COUPLING ............................... Coupling structure
  !>          XX(NDIME,:) ............................ Coordinates of wet points
  !>          COUPLING % GEOME % NUMBER_WET_POINTS ... SIZE(XX,2)
  !>          COUPLING % ITYPE ....................... Vector projection
  !>          COUPLING % KIND ........................ BETWEEN_SUBDOMAINS/BETWEEN_ZONES
  !>          COUPLING % KDTREE ...................... Kdtree of source 
  !>          
  !>
  !----------------------------------------------------------------------

  subroutine COU_INIT_INTERPOLATE_POINTS_VALUES(xx,icolo,jcolo,coupling)

    use mod_parall, only : par_color_to_code,par_color_to_zone,par_color_to_subd,PAR_MY_WORLD_RANK
    implicit none
    integer(ip),              intent(in)    :: icolo
    integer(ip),              intent(in)    :: jcolo
    real(rp),    pointer,     intent(in)    :: xx(:,:)
    type(typ_color_coupling), intent(inout) :: coupling
    integer(ip)                             :: ii,jj,kk,pp,kpart,ipart
    integer(ip)                             :: cpart_owner,number_wet_points
    integer(ip)                             :: ielem,ierro,cz,ksize,kdtree_wet
    integer(ip)                             :: cpart,ineig,isend,irecv
    integer(ip)                             :: ksend,lsend,lrecv,krecv
    integer(ip)                             :: kelem,inode,idime,inodb,pnodb
    integer(ip)                             :: npoin_to_send,npoin_to_recv
    integer(ip)                             :: nenti,kboun,iboun,ipoin,kpoin
    integer(ip)                             :: jcoup,ipart_world
    integer(ip)                             :: PAR_CURRENT_SIZE
    integer(ip)                             :: PAR_CURRENT_RANK
    integer(ip)                             :: PAR_COMM_SAVE
    real(rp)                                :: dista_min
    type(r2p),   pointer                    :: shapf(:)
    integer(ip), pointer                    :: list_neighbors(:,:)
    integer(ip), pointer                    :: npoin_send(:)
    integer(ip), pointer                    :: npoin_recv(:)
    integer(ip), pointer                    :: mask_npoin(:)
    type(i1p),   pointer                    :: decision_send(:)
    type(i1p),   pointer                    :: decision_recv(:)
    type(r1p),   pointer                    :: distance_send(:)
    type(r1p),   pointer                    :: distance_recv(:)
    type(i1p),   pointer                    :: my_part_to_point(:)
    type(i1p),   pointer                    :: my_point_to_part(:)
    type(r1p),   pointer                    :: coord_send(:)
    type(r1p),   pointer                    :: coord_recv(:)
    type(i1p),   pointer                    :: my_send_permut(:)
    logical(lg)                             :: require_distance
    integer(ip), pointer                    :: lesou(:)
    logical(lg), pointer                    :: lnsou(:)
    logical(lg), pointer                    :: intersection(:)
    logical(lg), pointer                    :: list_source_nodes(:)
    logical(lg), pointer                    :: mark_node(:)
    integer(ip), pointer                    :: PAR_WORLD_RANKS(:)
    real(rp)                                :: time0,time1,time2,time3,time4,time5

    ! call extrae_restart()

    call cputim(time0)
    ! call extrae_eventandcounters(600_4,1_8)
    !
    ! Initialize coupling structure COUPLING
    !
    call COU_INITIALIZE_COUPLING_STRUCTURE(coupling)
    !
    ! Communicator
    !
    PAR_COMM_SAVE    = PAR_COMM_CURRENT
    PAR_COMM_CURRENT = PAR_COMM_COLOR(icolo,jcolo) 
    call PAR_COMM_RANK_AND_SIZE(PAR_COMM_CURRENT,PAR_CURRENT_RANK,PAR_CURRENT_SIZE)
    !
    ! Sizes 
    !
    if( .not. associated(xx) ) then
       number_wet_points = 0 
    else
       number_wet_points = size(xx,2)
       if( coupling % geome % number_wet_points /= 0 ) then
          if( coupling % geome % number_wet_points /= number_wet_points ) &
               call runend('SOMETHING STRANGE HAPPENS!')
       else
          coupling % geome % number_wet_points = number_wet_points 
       end if
    end if
    !
    ! Nullify
    !
    nullify(shapf)
    nullify(list_neighbors)
    nullify(npoin_send)
    nullify(npoin_recv)
    nullify(mask_npoin)
    nullify(decision_send)
    nullify(decision_recv)
    nullify(distance_send)
    nullify(distance_recv)
    nullify(my_part_to_point)
    nullify(my_point_to_part)
    nullify(coord_send)
    nullify(coord_recv)
    nullify(my_send_permut)
    nullify(lesou)
    nullify(lnsou)
    nullify(intersection)
    nullify(list_source_nodes)
    nullify(PAR_WORLD_RANKS)
    !
    ! If the distance criterion is required
    !
    if(     coupling % itype == NEAREST_BOUNDARY_NODE  .or. &
         &  coupling % itype == BOUNDARY_INTERPOLATION .or. &
         &  coupling % itype == ELEMENT_INTERPOLATION  .or. &
         &  coupling % itype == STRESS_PROJECTION      .or. &
         &  coupling % itype == PROJECTION             .or. &
         &  coupling % itype == NEAREST_ELEMENT_NODE   .or. &
         &  coupling % itype == BOUNDARY_VECTOR_PROJECTION ) then
       require_distance = .true.
    else
       require_distance = .false.
    end if
    !
    ! Source elements/boundaries/nodes
    !
    if( INOTMASTER ) then
       call memory_alloca(memor_cou,'NELEM','par_test',lesou,nelem)
       call memory_alloca(memor_cou,'NPOIN','par_test',lnsou,npoin)
       if( coupling % kind == BETWEEN_SUBDOMAINS ) then
          do ielem = 1,nelem
             if( lesub(ielem) == coupling % subdomain_source ) then
                lesou(ielem) = 1
             end if
          end do
       else         
          do kelem = 1,nelez(coupling % zone_source)
             ielem = lelez(coupling % zone_source) % l(kelem)
             lesou(ielem) = 1
          end do
       end if
       do ielem = 1,nelem
          if( lesou(ielem) == 1 ) then
             do inode = 1,lnnod(ielem)
                ipoin = lnods(inode,ielem)
                lnsou(ipoin) = .true.
             end do
          end if
       end do
    end if
    !
    ! Allocate memory
    !
    cz = PAR_CURRENT_SIZE
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',npoin_send,      cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',npoin_recv,      cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',decision_send,   cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',decision_recv,   cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',coord_send,      cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',coord_recv,      cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',my_part_to_point,cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',my_point_to_part,number_wet_points,'INITIALIZE')
    call memory_alloca(memor_cou,'SHAPF'     ,'par_test',shapf,           cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',my_send_permut,  cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',intersection,    cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',PAR_WORLD_RANKS, cz,               'INITIALIZE',0_ip)
    if( require_distance ) then
       call memory_alloca(memor_cou,'MY_SEND_DIST','par_test',distance_send,cz,'INITIALIZE',0_ip)
       call memory_alloca(memor_cou,'MY_RECV_DIST','par_test',distance_recv,cz,'INITIALIZE',0_ip)
    end if
    !
    ! Find list of partitions to which I must send my points
    !
    do pp = 1,number_wet_points
       call maths_mapping_coord_to_3d(ndime,par_bin_boxes,par_bin_comin,par_bin_comax,xx(1:ndime,pp),ii,jj,kk)
       if( ii /= 0 ) then 
          ksize = 0
          do kpart = 1,par_bin_size(ii,jj,kk) 
             ipart = par_bin_part(ii,jj,kk) % l(kpart)
             if( maths_in_box(ndime,xx(1:ndime,pp),par_part_comin(1:ndime,ipart),par_part_comax(1:ndime,ipart)) ) then
                cpart = PAR_COMM_COLOR_PERM(icolo,jcolo,ipart)
                !
                ! Take those partitions only in color JCOLO
                !
                if( par_part_in_color(ipart,jcolo) ) then
                   ksize = ksize + 1
                   npoin_send(cpart) = npoin_send(cpart) + 1  
                end if
             end if
          end do
          if( ksize > 0 ) then
             allocate( my_point_to_part(pp) % l(ksize))
             ksize = 0
             do kpart = 1,par_bin_size(ii,jj,kk) 
                ipart = par_bin_part(ii,jj,kk) % l(kpart)
                if( maths_in_box(ndime,xx(1:ndime,pp),par_part_comin(1:ndime,ipart),par_part_comax(1:ndime,ipart)) ) then
                   if( par_part_in_color(ipart,jcolo) ) then
                      ksize = ksize + 1
                      cpart = PAR_COMM_COLOR_PERM(icolo,jcolo,ipart)
                      my_point_to_part(pp) % l(ksize) = cpart
                   end if
                end if
             end do
          end if
       end if
    end do

    !--------------------------------------------------------------------
    !
    !  1. How many wet points I send to (I) and how many I receive from (J)
    !
    !                            +-------+
    !                        =>  |       |  =>
    !                            |       |
    !          NPOIN_RECV(I) =>  |       |  => NPOIN_SEND(I)
    !                            +-------+
    !
    !  2. Send coordinates and receive coordinates
    !
    !                            +-------+
    !                        =>  |       |  =>
    !                            |       |
    !      COORD_RECV(I) % A =>  |       |  => COORD_SEND(I) % A
    !                            +-------+
    !
    !  3. Check if I have what I receive (find host element, nearest point, etc.) and send it back: DECISION_SEND(I) % L
    !     Receive the decision of the others: DECISION_RECV(I) % L
    !     In case of nearest point, send the minimal distance I have found as well: DISTANCE_SEND(I) % A
    !     Receive the minimal distance of others as well: DISTANCE_RECV(I) % A
    !
    !                            +-------+
    !                        <=  |       |  <=
    !                            |       |
    !   DECISION_SEND(I) % L <=  |       |  <= DECISION_RECV(I) % L
    !                            +-------+
    !                         
    !--------------------------------------------------------------------
    !
    ! Copy my points
    !
    do cpart = 0,PAR_CURRENT_SIZE-1
       if( npoin_send(cpart) > 0 ) allocate( my_part_to_point(cpart) % l(npoin_send(cpart)) )
       npoin_send(cpart) = 0
    end do
    do pp = 1,number_wet_points 
       call maths_mapping_coord_to_3d(ndime,par_bin_boxes,par_bin_comin,par_bin_comax,xx(1:ndime,pp),ii,jj,kk)
       if( ii /= 0 ) then 
          do kpart = 1,par_bin_size(ii,jj,kk) 
             ipart = par_bin_part(ii,jj,kk) % l(kpart)
             if( maths_in_box(ndime,xx(1:ndime,pp),par_part_comin(1:ndime,ipart),par_part_comax(1:ndime,ipart)) ) then
                if( par_part_in_color(ipart,jcolo) ) then
                   cpart = PAR_COMM_COLOR_PERM(icolo,jcolo,ipart)
                   npoin_send(cpart) = npoin_send(cpart) + 1
                   my_part_to_point(cpart) % l(npoin_send(cpart)) = pp
                end if
             end if
          end do
       end if
    end do
    !
    ! Bounding box intersection 
    !
    PAR_WORLD_RANKS(PAR_CURRENT_RANK) = PAR_MY_WORLD_RANK
    call PAR_MAX(PAR_CURRENT_SIZE,PAR_WORLD_RANKS(0:PAR_CURRENT_SIZE-1),'IN CURRENT COUPLING') ! should be optimized
    do cpart = 0,PAR_CURRENT_SIZE-1
       intersection(cpart) = .true. 
       ipart_world = PAR_WORLD_RANKS(cpart)
       do idime = 1,ndime
          if(    par_part_comin(idime,PAR_MY_WORLD_RANK) > par_part_comax(idime,ipart_world) .or. &
               & par_part_comin(idime,ipart_world)       > par_part_comax(idime,PAR_MY_WORLD_RANK) ) then
             intersection(cpart) = .false.
          end if
       end do
    end do
    !
    ! For vector projection, send the wet points to all the partitions as we have no way
    ! to know where the point falls. Idea: consider only the partitions bounding boxes
    ! crossed by the segment formed by the wet node ( x_wet , x_wet + 10000 * projection_vector )  
    ! 
    ! +-----+------+-------+-----+
    ! |     |      | x_wet |     |
    ! |     |      |    x  |     |
    ! +-----+------+----|--+-----+
    ! |     |      |    |  |     |
    ! |     |      |    |  |     |
    ! +-----+------+----|--+-----+
    ! |     |      |    |  |     |
    ! |     |      |    |  |     |
    ! +-----+------+----|--+-----+
    ! ///////////////// | ////////
    !                   |
    !                   | projection_vector
    !                   |
    !                   \/   
    !                   x 
    !
    if( coupling % itype == BOUNDARY_VECTOR_PROJECTION .and. INOTMASTER ) then
       do cpart = 1,PAR_CURRENT_SIZE-1
          intersection(cpart) = .true. 
       end do
    end if
    !
    ! Get how many points I should check if I have them
    !
    call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
    call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
    do cpart = 0,PAR_CURRENT_SIZE-1
       if( intersection(cpart) ) then
          call PAR_SEND_RECEIVE(1_ip,1_ip,npoin_send(cpart:cpart),npoin_recv(cpart:cpart),'IN CURRENT COUPLING',cpart,'NON BLOCKING')
          !call PAR_SEND_RECEIVE(1_ip,1_ip,npoin_send(cpart:cpart),npoin_recv(cpart:cpart),'IN CURRENT COUPLING',cpart)
       end if
    end do
    call PAR_END_NON_BLOCKING_COMM(1_ip)


    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time1) 
    cputi_cou(6) = cputi_cou(6) + time1-time0
    !
    ! Save point coordinates
    !
    do cpart = 0,PAR_CURRENT_SIZE-1
       npoin_to_send = npoin_send(cpart)
       npoin_to_recv = npoin_recv(cpart)
       if( npoin_to_send > 0 ) then
          call memory_alloca(memor_cou,'COORD_SEND',   'par_test',coord_send(cpart) % a,npoin_to_send*ndime)
          call memory_alloca(memor_cou,'DECISION_RECV','par_test',decision_recv(cpart) % l,npoin_to_send)
          if( require_distance ) then
             call memory_alloca(memor_cou,'DISTANCE_RECV','par_test',distance_recv(cpart) % a,npoin_to_send)
          end if
       end if
       if( npoin_to_recv > 0 ) then
          call memory_alloca(memor_cou,'COORD_RECV',    'par_test',coord_recv(cpart) % a,    npoin_to_recv*ndime)
          call memory_alloca(memor_cou,'DECISION_SEND', 'par_test',decision_send(cpart) % l, npoin_to_recv)
          call memory_alloca(memor_cou,'MY_SEND_PERMUT','par_test',my_send_permut(cpart) % l,npoin_to_recv)
          if( require_distance ) then
             call memory_alloca(memor_cou,'DISTANCE_SEND','par_test',distance_send(cpart) % a,npoin_to_recv)
          end if
       end if
    end do
    !
    ! Copy points to send buffer
    !
    do cpart = 0,PAR_CURRENT_SIZE-1
       npoin_to_send = npoin_send(cpart)
       if( npoin_to_send > 0 ) then
          kk = 0
          do ii = 1,npoin_to_send
             pp = my_part_to_point(cpart) % l(ii)
             do idime = 1,ndime
                kk = kk + 1
                coord_send(cpart) % a(kk) = xx(idime,pp)
             end do
          end do
       end if
    end do
    !
    ! Get points coordinates
    !
    call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
    call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
    do cpart = 0,PAR_CURRENT_SIZE-1
       if( intersection(cpart) ) then
          call PAR_SEND_RECEIVE(coord_send(cpart) % a,coord_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
       end if
    end do
    call PAR_END_NON_BLOCKING_COMM(1_ip)
    
    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time2) 
    ! call extrae_eventandcounters(600_4,2_8)
    cputi_cou(3) = cputi_cou(3) + time2-time1

    if( ISEQUEN ) then

    else if( IMASTER ) then
    else if( INOTMASTER ) then

       call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
       call PAR_START_NON_BLOCKING_COMM(2_ip,PAR_CURRENT_SIZE)
       ! 
       ! Check if I own the points
       !       
       if( coupling % itype == ELEMENT_INTERPOLATION ) then
          !
          ! Element interpolation
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call memory_alloca(memor_cou,'SHAPF(CPART)','par_test',shapf(cpart)%a,mnode,pp)
                call COU_WET_POINTS_HOST_ELEMENTS(pp,coord_recv(cpart) % a, distance_send(cpart) % a, shapf(cpart) % a,decision_send(cpart) % l,lesou)
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       else if( coupling % itype == NEAREST_BOUNDARY_NODE ) then
          !
          ! Nearest boundary node
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call COU_WET_POINTS_NEAREST_BOUNDARY_NODE(pp,coord_recv(cpart) % a,distance_send(cpart) % a,decision_send(cpart) % l,lnsou)
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       else if( coupling % itype == NEAREST_ELEMENT_NODE ) then
          !
          ! Nearest element node
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call COU_WET_POINTS_NEAREST_ELEMENT_NODE(pp,coord_recv(cpart) % a,distance_send(cpart) % a,decision_send(cpart) % l,lnsou)
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       else if( coupling % itype == BOUNDARY_INTERPOLATION .or. coupling % itype == STRESS_PROJECTION .or. coupling % itype == PROJECTION ) then
          !
          ! Boundary interpolation and Gauss point interpolation
          ! Wet points are nodes in the first case and Gauss points in the second case
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call memory_alloca(memor_cou,'SHAPF(CPART)','par_test',shapf(cpart)%a,mnodb,pp)
                call COU_WET_POINTS_HOST_BOUNDARIES(pp,coord_recv(cpart) % a,distance_send(cpart) % a,&
                     &                              decision_send(cpart) % l,shapf(cpart) % a,        &
                     &                              coupling % geome % kdtree                         )
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       else if( coupling % itype == BOUNDARY_VECTOR_PROJECTION ) then
          !
          ! Look for the boudnary corssing the projection along a vector
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call memory_alloca(memor_cou,'SHAPF(CPART)','par_test',shapf(cpart)%a,mnodb,pp)
                call COU_WET_POINTS_HOST_BOUNDARY_VECTOR(pp,coord_recv(cpart) % a,distance_send(cpart) % a,&
                     &                                   decision_send(cpart) % l,shapf(cpart) % a,        &
                     &                                   coupling % geome % kdtree                         )
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       end if
       !
       ! Send my result and distance if necessary
       !
       !do cpart = 0,PAR_CURRENT_SIZE-1
       !   if( intersection(cpart) ) then
       !      call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
       !      call PAR_SEND_RECEIVE(decision_send(cpart) % l,   decision_recv(cpart) % l,'IN CURRENT',cpart,'NON BLOCKING')
       !   end if
       !end do
       !if( require_distance ) then
       !   do cpart = 0,PAR_CURRENT_SIZE-1
       !      if( intersection(cpart) ) then
       !         call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
       !         call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT',cpart,'NON BLOCKING')
       !      end if
       !   end do
       !end if
       call PAR_END_NON_BLOCKING_COMM(1_ip)
       call PAR_END_NON_BLOCKING_COMM(2_ip)
    end if

!call runend('O.K.!')

    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time3) 
    ! call extrae_eventandcounters(600_4,3_8)
    cputi_cou(4) = cputi_cou(4) + time3-time2

!if(imaster) call runend('O.K.!')
    if( INOTMASTER ) then
       !
       ! Decide the owner IPART_OWNER of my points  
       !
!!$       if( coupling % itype /= ELEMENT_INTERPOLATION ) then
!!$          !
!!$          ! For element interpolation take element which belongs to subdomain
!!$          ! with highest rank
!!$          !
!!$          do pp = 1,number_wet_points
!!$             cpart_owner = 0
!!$             do cpart = 0,PAR_CURRENT_SIZE-1
!!$                kk = 0
!!$                do while( kk < npoin_send(cpart) )
!!$                   kk = kk + 1                
!!$                   if( pp == my_part_to_point(cpart) % l(kk) .and. decision_recv(cpart) % l(kk) /= 0 ) then
!!$                      if( cpart == PAR_CURRENT_RANK ) then
!!$                         !
!!$                         ! I am the owner  
!!$                         !
!!$                         cpart_owner = cpart
!!$                         kk = npoin_send(cpart) 
!!$                      else
!!$                         !
!!$                         ! CPART has this node and CPART hosts the point
!!$                         !
!!$                         cpart_owner = max(cpart_owner,cpart)     
!!$                      end if
!!$                   end if
!!$                end do
!!$             end do
!!$
!!$             do cpart = 0,PAR_CURRENT_SIZE-1
!!$                do kk = 1,npoin_send(cpart)
!!$                   if( pp == my_part_to_point(cpart) % l(kk) ) then
!!$                      if( cpart /= cpart_owner ) then
!!$                         decision_recv(cpart) % l(kk) = 0
!!$                      end if
!!$                   end if
!!$                end do
!!$             end do
!!$             if( associated(my_point_to_part(pp) % l) ) my_point_to_part(pp) % l(1) = cpart_owner
!!$          end do

       if( require_distance ) then
          !
          ! Look for minimum distance
          !
          do pp = 1,number_wet_points
             cpart_owner = 0
             dista_min = huge(1.0_rp)
             do cpart = 0,PAR_CURRENT_SIZE-1
                kk = 0
                do while( kk < npoin_send(cpart) )
                   kk = kk + 1                
                   if( pp == my_part_to_point(cpart) % l(kk) .and. decision_recv(cpart) % l(kk) /= 0 ) then
                      if( distance_recv(cpart) % a(kk) < dista_min ) then
                         cpart_owner = cpart
                         dista_min   = distance_recv(cpart) % a(kk)
!print*, "DEBUG: pp, cpart_owner, dista_min ", pp, cpart_owner, dista_min
                      end if
                   end if
                end do
             end do
             do cpart = 0,PAR_CURRENT_SIZE-1
                do kk = 1,npoin_send(cpart)
                   if( pp == my_part_to_point(cpart) % l(kk) ) then
                      if( cpart /= cpart_owner ) then
                         decision_recv(cpart) % l(kk) = 0
                      end if
                   end if
                end do
             end do
             if( associated(my_point_to_part(pp) % l) ) my_point_to_part(pp) % l(1) = cpart_owner
          end do

       end if
       !
       ! Send my result back
       !
       call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
       call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
       do cpart = 0,PAR_CURRENT_SIZE-1
          if( intersection(cpart) ) then
             call PAR_SEND_RECEIVE(decision_recv(cpart) % l,decision_send(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
          end if
       end do
       call PAR_END_NON_BLOCKING_COMM(1_ip)

       !-----------------------------------------------------------------
       !
       ! decision_send(cpart) % l(1:npoin_recv(cpart)) /= 0 => I am in charge of this point OF CPART
       ! decision_recv(cpart) % l(1:npoin_send(cpart)) /= 0 => I need point from CPART
       !
       !-----------------------------------------------------------------
       !
       ! Fill in type
       !
       call memory_alloca(memor_cou,'LIST_NEIGHBORS','par_test',list_neighbors,2_ip,PAR_CURRENT_SIZE,'INITIALIZE',1_ip,0_ip)
       !
       ! Number of subdomains to communicate with
       !
       coupling % commd % nneig = 0
       do cpart = 0,PAR_CURRENT_SIZE-1
          do ii = 1,npoin_recv(cpart)
             list_neighbors(1,cpart) = list_neighbors(1,cpart) + min(1_ip,decision_send(cpart) % l(ii)) 
          end do
          do ii = 1,npoin_send(cpart)
             list_neighbors(2,cpart) = list_neighbors(2,cpart) + min(1_ip,decision_recv(cpart) % l(ii))
          end do
          coupling % commd % nneig = coupling % commd % nneig &
               + min(max(list_neighbors(1,cpart),list_neighbors(2,cpart)),1)
       end do
       !
       ! Send and receives sizes of subdomains
       ! COMMD % LSEND_SIZE(:)
       ! COMMD % LRECV_SIZE(:)
       !
       call memory_alloca(memor_cou,'NEIGHTS',   'par_test',coupling % commd % neights,   coupling % commd % nneig)
       call memory_alloca(memor_cou,'LSEND_SIZE','par_test',coupling % commd % lsend_size,coupling % commd % nneig+1)
       call memory_alloca(memor_cou,'LRECV_SIZE','par_test',coupling % commd % lrecv_size,coupling % commd % nneig+1)
       ineig = 0
       do cpart = 0,PAR_CURRENT_SIZE-1
          isend = list_neighbors(1,cpart)
          irecv = list_neighbors(2,cpart)
          if( max(isend,irecv) > 0 ) then
             ineig = ineig + 1
             coupling % commd % neights(ineig)    = cpart
             coupling % commd % lsend_size(ineig) = isend
             coupling % commd % lrecv_size(ineig) = irecv
          end if
       end do
       !
       ! Size to list
       !
       ksend = coupling % commd % lsend_size(1)
       krecv = coupling % commd % lrecv_size(1)
       coupling % commd % lsend_size(1) = 1
       coupling % commd % lrecv_size(1) = 1
       do ineig = 2,coupling % commd % nneig + 1
          lsend = coupling % commd % lsend_size(ineig)
          lrecv = coupling % commd % lrecv_size(ineig)
          coupling % commd % lsend_size(ineig) = coupling % commd % lsend_size(ineig-1) + ksend
          coupling % commd % lrecv_size(ineig) = coupling % commd % lrecv_size(ineig-1) + krecv
          ksend = lsend
          krecv = lrecv
       end do
       coupling % commd % lsend_dim = coupling % commd % lsend_size(coupling % commd % nneig + 1) - 1
       coupling % commd % lrecv_dim = coupling % commd % lrecv_size(coupling % commd % nneig + 1) - 1
       !
       ! Order points
       ! KK is the order I receive 
       ! 
       call memory_alloca(memor_cou,'STATUS','par_test',coupling % geome % status,coupling % geome % number_wet_points)             
       kk = 0
       do cpart = 0,PAR_CURRENT_SIZE-1
          do ii = 1,npoin_send(cpart)
             pp = my_part_to_point(cpart) % l(ii)
             if( my_point_to_part(pp) % l(1) == cpart ) then
                !
                ! CPART has wet point pp
                !
                kk = kk + 1
                coupling % geome % status(pp) = kk
             end if
          end do
       end do
       !
       ! Allocate geometrical information
       ! NENTI= number of entities (elements/boundaries/nodes)
       !
       nenti = 0
       do cpart = 0,PAR_CURRENT_SIZE-1
          nenti = nenti + list_neighbors(1,cpart)
       end do

       if( coupling % itype == ELEMENT_INTERPOLATION ) then
          !
          ! Element interpolation
          !
          coupling % geome % nelem_source = nenti
          call memory_alloca(memor_cou,'SHAPF'            ,'par_test',coupling % geome % shapf,mnode, coupling % geome % nelem_source)
          call memory_alloca(memor_cou,'LELEM'            ,'par_test',coupling % geome % lelem_source,coupling % geome % nelem_source)
          call memory_alloca(memor_cou,'LIST_SOURCE_NODES','par_test',list_source_nodes,npoin)

          kelem = 0
          do cpart = 0,PAR_CURRENT_SIZE-1
             kk = 0
             do ii = 1,npoin_recv(cpart)
                ielem = decision_send(cpart) % l(ii)
                if( ielem > 0 ) then
                   kelem = kelem + 1
                   coupling % geome % lelem_source(kelem)  = ielem
                   do inode = 1,mnode
                      coupling % geome % shapf(inode,kelem) = shapf(cpart) % a(inode,ii)
                   end do
                   do inode = 1,lnnod(ielem)
                      ipoin = lnods(inode,ielem)
                      list_source_nodes(ipoin) = .true.
                   end do
                end if
             end do
          end do

       else if( coupling % itype == NEAREST_BOUNDARY_NODE .or. coupling % itype == NEAREST_ELEMENT_NODE .or. coupling % itype == BOUNDARY_VECTOR_PROJECTION ) then
          !
          ! Nearest boundary node
          !
          coupling % geome % npoin_source = nenti          
          call memory_alloca(memor_cou,'LPOIN','par_test',coupling % geome % lpoin_source,coupling % geome % npoin_source)  

          kpoin = 0
          do cpart = 0,PAR_CURRENT_SIZE-1
             kk = 0
             do ii = 1,npoin_recv(cpart)
                ipoin = decision_send(cpart) % l(ii)
                if( ipoin > 0 ) then
                   kpoin = kpoin + 1
                   coupling % geome % lpoin_source(kpoin)  = ipoin
                end if
             end do
          end do

       else if( coupling % itype == BOUNDARY_INTERPOLATION ) then       
          !
          ! Boundary interpolation
          !  
          coupling % geome % nboun_source = nenti          
          call memory_alloca(memor_cou,'SHAPF'            ,'par_test',coupling % geome % shapf,mnodb, coupling % geome % nboun_source)
          call memory_alloca(memor_cou,'LBOUN'            ,'par_test',coupling % geome % lboun_source,coupling % geome % nboun_source)
          call memory_alloca(memor_cou,'LIST_SOURCE_NODES','par_test',list_source_nodes,npoin)

          kboun = 0
          do cpart = 0,PAR_CURRENT_SIZE-1
             kk = 0
             do ii = 1,npoin_recv(cpart)
                iboun = decision_send(cpart) % l(ii)
                if( iboun > 0 ) then
                   kboun = kboun + 1
                   coupling % geome % lboun_source(kboun)  = iboun
                   do inodb = 1,mnodb
                      coupling % geome % shapf(inodb,kboun) = shapf(cpart) % a(inodb,ii)
                   end do
                   do inodb = 1,lnnob_cou(iboun)
                      ipoin = lnodb_cou(inodb,iboun)
                      list_source_nodes(ipoin) = .true.
                   end do
                end if
             end do
          end do

       else if( coupling % itype == STRESS_PROJECTION .or. coupling % itype == PROJECTION ) then       
          !
          ! Projection
          !  
          coupling % geome % nboun_source = nenti          
          call memory_alloca(memor_cou,'SHAPF',            'par_test',coupling % geome % shapf,mnodb,coupling % geome % nboun_source)
          call memory_alloca(memor_cou,'LBOUN',            'par_test',coupling % geome % lboun_source,coupling % geome % nboun_source)
          call memory_alloca(memor_cou,'LIST_SOURCE_NODES','par_test',list_source_nodes,npoin)

          kboun = 0
          do cpart = 0,PAR_CURRENT_SIZE-1
             kk = 0
             do ii = 1,npoin_recv(cpart)
                iboun = decision_send(cpart) % l(ii)
                if( iboun > 0 ) then
                   kboun = kboun + 1
                   coupling % geome % lboun_source(kboun)  = iboun
                   do inodb = 1,mnodb
                      coupling % geome % shapf(inodb,kboun) = shapf(cpart) % a(inodb,ii)
                   end do
                   do inodb = 1,lnnob_cou(iboun)
                      ipoin = lnodb_cou(inodb,iboun)
                      list_source_nodes(ipoin) = .true.
                   end do
                   !write(*,'(i2,2(1x,e12.6),i5,i5)') kfl_paral,coord_recv(cpart) % a((ii-1)*2+1),coord_recv(cpart) % a((ii-1)*2+2),lninv_loc(lnodb(1:2,iboun))
                end if
             end do
          end do
       end if
       !
       ! Allocate sched_perm for scheduling permutation
       !
       call memory_alloca(memor_cou,'SCHED_PERM','par_test',coupling % geome % sched_perm,coupling % commd % lrecv_dim)
       !
       ! Count number of source nodes if not already done
       !
       if( associated(list_source_nodes) ) then
          coupling % geome % npoin_source = 0
          do ipoin = 1,npoin
             if( list_source_nodes(ipoin) ) coupling % geome % npoin_source = coupling % geome % npoin_source + 1
          end do
          if( associated(coupling % geome % lpoin_source) ) then
             call runend('POINTER ALREADY ASSOCIATED')
          else
             call memory_alloca(memor_cou,'LPOIN','par_test',coupling % geome % lpoin_source,coupling % geome % npoin_source)  
          end if
          kpoin = 0
          do ipoin = 1,npoin
             if( list_source_nodes(ipoin) ) then
                kpoin = kpoin + 1
                coupling % geome % lpoin_source(kpoin) = ipoin
             end if
          end do
       end if
       !
       ! Deallocate memory
       !
       call memory_deallo(memor_cou,'LIST_NEIGHBORS',   'par_test',list_neighbors)
       call memory_deallo(memor_cou,'LIST_SOURCE_NODES','par_test',list_source_nodes)
       !
       ! Allocate memory for previous values
       !
       !call memory_alloca(memor_cou,'VALUES','par_test',coupling % values,coupling % geome % number_wet_points)
       !
       ! Check points that do not have a host partition
       ! If receive:        COUPLI % GEOME % LRECV_DIM values from other subdomains
       ! If need values on: COUPLI % GEOME % NUMBER_WET_POINTS values
       ! COUPLI % GEOME % STATUS(IPOIN) = JPOIN (from 1 to COUPLI % GEOME % LRECV_DIM)
       !
       ierro = 0
       ii    = 0
       do pp = 1,number_wet_points
          if( associated(my_point_to_part(pp) % l) ) then
             if( my_point_to_part(pp) % l(1) == 0 ) then
                ierro = 1
             else
                ii = ii + 1
             end if
          else
             ierro = 1
          end if
       end do

    end if

    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time4)
    ! call extrae_eventandcounters(600_4,0_8)
    ! call extrae_shutdown() 
    cputi_cou(5) = cputi_cou(5) + time4-time3
    !
    ! Allocate coupling % values_frequ for FSI in case of frequency is active for exchange
    !
    call memory_alloca(memor_cou,'VALUES_FREQU','mod_couplings',coupling % values_frequ,ndime,coupling % geome % npoin_source,2_ip)
    do ipoin = 1_ip, coupling % geome % npoin_source
       do idime = 1_ip, ndime
          coupling % values_frequ(idime, ipoin, :) = 0_rp
       end do
    end do
    !
    ! Deallocate memory
    !
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',npoin_send)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',npoin_recv)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',mask_npoin)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',decision_send)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',decision_recv)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',coord_send)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',coord_recv)   
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',my_part_to_point)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',my_point_to_part)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',shapf)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',distance_send)
    call memory_deallo(memor_cou,'NPOIN_SEND'     ,'par_test',distance_recv)
    call memory_deallo(memor_cou,'LESOU     '     ,'par_test',lesou)
    call memory_deallo(memor_cou,'LNSOU     '     ,'par_test',lnsou)
    call memory_deallo(memor_cou,'INTERSECTION'   ,'mod_couplings',intersection)
    call memory_deallo(memor_cou,'PAR_WORLD_RANKS','mod_couplings',PAR_WORLD_RANKS)


    !
    ! Recover communicator
    !
    PAR_COMM_CURRENT = PAR_COMM_SAVE


    !if( PAR_MY_WORLD_RANK == 0 ) then
    !   do ii=1,nx;do jj=1,ny ; do kk=1,nz
    !      if( bin_size(ii,jj,kk) > 0 ) then
    !         do iboxe = 1,bin_size(ii,jj,kk)
    !            ipart = par_bin_part(ii,jj,kk) % l(iboxe)
    !            icode = PAR_COMM_WORLD_TO_CODE_PERM(1,ipart)
    !            ipart = PAR_COMM_WORLD_TO_CODE_PERM(2,ipart)
    !         end do
    !      end if
    !   end do; end do; end do
    !end if


  end subroutine COU_INIT_INTERPOLATE_POINTS_VALUES

  subroutine COU_PRESCRIBE_DIRICHLET_IN_MATRIX(nbvar,npopo,ia,ja,an,bb,xx)
    integer(ip), intent(in)    :: nbvar
    integer(ip), intent(in)    :: npopo
    integer(ip), intent(in)    :: ia(*)
    integer(ip), intent(in)    :: ja(*)
    real(rp),    intent(inout) :: an(nbvar,nbvar,*)
    real(rp),    intent(inout) :: xx(nbvar,*)
    real(rp),    intent(inout) :: bb(nbvar,*)
    integer(ip)                :: nrows,icoup,ii,jj,kk,nn,ll
    integer(ip)                :: izdom
    real(rp),    pointer       :: xx_tmp(:,:)

    if(    INOTMASTER .and. ( &
         & I_AM_INVOLVED_IN_A_COUPLING_TYPE(BETWEEN_SUBDOMAINS,DIRICHLET_IMPLICIT) .or. &
         & I_AM_INVOLVED_IN_A_COUPLING_TYPE(BETWEEN_SUBDOMAINS,DIRICHLET_EXPLICIT) ) ) then
 
       nrows = nbvar * npopo
       if( nbvar > 1 ) call runend('COU_PRESCRIBE_DIRICHLET_IN_MATRIX: NOT CODED')
       nullify( xx_tmp )
       allocate( xx_tmp(nbvar,npopo) )
       do ii = 1,npopo
          do nn = 1,nbvar
             xx_tmp(nn,ii) = xx(nn,ii)
          end do
       end do
       do icoup = 1,mcoup
          if( coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS ) then
             if( coupling_type(icoup) % what == DIRICHLET_EXPLICIT .or. coupling_type(icoup) % what == DIRICHLET_IMPLICIT ) then
                call COU_INTERPOLATE_NODAL_VALUES(icoup,1_ip,xx,xx_tmp)
             end if
          end if
       end do
       deallocate( xx_tmp )
       do icoup = 1,mcoup
          if( coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS ) then
             if( coupling_type(icoup) % what == DIRICHLET_EXPLICIT .or. coupling_type(icoup) % what == DIRICHLET_IMPLICIT ) then
                do kk = 1,coupling_type(icoup) % geome % npoin_wet
                   ii  = coupling_type(icoup) % geome % lpoin_wet(kk)
                   do izdom = ia(ii),ia(ii+1)-1
                      jj = ja(izdom)
                      if( ii == jj ) then
                         an(1,1,izdom) = 1.0_rp
                      else
                         an(1,1,izdom) = 0.0_rp                    
                      end if
                   end do
                   bb(1,ii) = xx(1,ii)
                end do
             end if
          end if
       end do
    end if

  end subroutine COU_PRESCRIBE_DIRICHLET_IN_MATRIX

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    11/03/2014
  !> @brief   Check in which elements are the test points
  !> @details For each partition whcih have received some points,
  !>          check which of them host them
  !
  !----------------------------------------------------------------------

  subroutine COU_WET_POINTS_HOST_ELEMENTS(npoin_test,coord_test,dimin_test,shapf_test,inout_test,lesou)
    integer(ip), intent(in)   :: npoin_test                    !< Number of points to test
    real(rp),    intent(in)   :: coord_test(ndime,npoin_test)  !< Coordinates of test points
    real(rp),    intent(out)  :: dimin_test(npoin_test)        !< Minimum distance to a boundary node
    real(rp),    intent(out)  :: shapf_test(mnode,npoin_test)  !< Shape function in elements
    integer(ip), intent(out)  :: inout_test(npoin_test)        !< In or outside en element
    integer(ip), intent(in)   :: lesou(nelem)
    integer(ip)               :: kpoin,ielem,dummi(2),ielse14
    integer(ip)               :: ielse15,ielse8
    real(rp)                  :: coloc(3),deriv(64)
    real(rp)                  :: dista
    !
    ! Tell elsest that not all the elements should be considered
    ! Consider element only if LESOU(IELEM) = 1
    !
    ielse14   = ielse(14)  
    ielse(14) = 1    
    ielse15   = ielse(15)  
    ielse(15) = 1    
    !
    ! Loop over test points
    !
    do kpoin = 1,npoin_test
       ielem = 0
       dimin_test(kpoin) = huge(1.0_rp)
 
       call elsest_host_element(&
            ielse,relse,1_ip,meshe(ndivi),coord_test(:,kpoin),ielem,&
            shapf_test(:,kpoin),deriv,coloc,dista,lesou)
       !call elsest(&
       !     2_ip,1_ip,ielse,mnode,ndime,npoin,nelem,nnode(1:),&
       !     lnods,ltype,ltopo,coord,coord_test(1,kpoin),relse,&
       !     ielem,shapf_test(1,kpoin),deriv,coloc,lesou)
       !
       if( ielem <= 0 ) then
          inout_test(kpoin) = 0              
       else
          if( lesou(ielem) == 1 ) then
             inout_test(kpoin) = ielem
             dimin_test(kpoin) = dista
          else
             inout_test(kpoin) = 0
          end if
       end if

    end do

    ielse(14) = ielse14
    ielse(15) = ielse15

  end subroutine COU_WET_POINTS_HOST_ELEMENTS

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    11/03/2014
  !> @brief   Check in which elements are the test points
  !> @details For each partition whcih have received some points,
  !>          check which of them host them
  !
  !----------------------------------------------------------------------

  subroutine COU_WET_POINTS_NEAREST_BOUNDARY_NODE(npoin_test,coord_test,dimin_test,inout_test,lnsou)
    integer(ip), intent(in)   :: npoin_test                    !< Number of points to test
    real(rp),    intent(in)   :: coord_test(ndime,npoin_test)  !< Coordinates of test points
    real(rp),    intent(out)  :: dimin_test(npoin_test)        !< Minimum distance to a boundary node
    integer(ip), intent(out)  :: inout_test(npoin_test)        !< Node with minimum distance
    logical(lg), intent(in)   :: lnsou(:)
    integer(ip)               :: ipoin,ibono,ipoin_min
    integer(ip)               :: idime,kpoin
    real(rp)                  :: dista,dista_min

    do kpoin = 1,npoin_test

       dista_min = huge(1.0_rp)
       ipoin_min = 0

       do ibono = 1,nbono
          ipoin = lbono(ibono)
          if( lnsou(ipoin) ) then
             dista = 0.0_rp
             do idime = 1,ndime
                dista = dista + ( coord_test(idime,kpoin) - coord(idime,ipoin) ) ** 2
             end do
             if( dista < dista_min ) then
                dista_min = dista
                ipoin_min = ipoin
             end if
          end if
       end do

       inout_test(kpoin) = ipoin_min
       dimin_test(kpoin) = dista_min

    end do

  end subroutine COU_WET_POINTS_NEAREST_BOUNDARY_NODE


  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    11/03/2014
  !> @brief   Check in which elements are the test points
  !> @details For each partition whcih have received some points,
  !>          check which of them host them
  !
  !----------------------------------------------------------------------

  subroutine COU_WET_POINTS_NEAREST_ELEMENT_NODE(npoin_test,coord_test,dimin_test,inout_test,lnsou)
    integer(ip), intent(in)   :: npoin_test                    !< Number of points to test
    real(rp),    intent(in)   :: coord_test(ndime,npoin_test)  !< Coordinates of test points
    real(rp),    intent(out)  :: dimin_test(npoin_test)        !< Minimum distance to a boundary node
    integer(ip), intent(out)  :: inout_test(npoin_test)        !< Node with minimum distance
    logical(lg), intent(in)   :: lnsou(:)
    integer(ip)               :: ipoin,ibono,ipoin_min
    integer(ip)               :: idime,kpoin
    real(rp)                  :: dista,dista_min

    do kpoin = 1,npoin_test

       dista_min = huge(1.0_rp)
       ipoin_min = 0

       do ipoin = 1,npoin
          if( lnsou(ipoin) ) then
             dista = 0.0_rp
             do idime = 1,ndime
                dista = dista + ( coord_test(idime,kpoin) - coord(idime,ipoin) ) ** 2
             end do
             if( dista < dista_min ) then
                dista_min = dista
                ipoin_min = ipoin
             end if
          end if
       end do

       inout_test(kpoin) = ipoin_min
       dimin_test(kpoin) = dista_min

    end do

  end subroutine COU_WET_POINTS_NEAREST_ELEMENT_NODE

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    11/03/2014
  !> @brief   Check in which elements are the test points
  !> @details For each partition whcih have received some points,
  !>          check which of them host them
  !
  !----------------------------------------------------------------------

  subroutine COU_WET_POINTS_HOST_BOUNDARIES(&
       npoin_test,coord_test,dimin_test,inout_test,shapf_test,kdtree_source)
    integer(ip),                        intent(in)    :: npoin_test                    !< Number of points to test
    real(rp),                           intent(in)    :: coord_test(ndime,npoin_test)  !< Coordinates of test points
    real(rp),                           intent(out)   :: dimin_test(npoin_test)        !< Minimum distance to a boundary node
    integer(ip),                        intent(out)   :: inout_test(npoin_test)        !< Node with minimum distance
    real(rp),                           intent(out)   :: shapf_test(mnodb,npoin_test)  !< Shape function in elements
    type(typ_kdtree),                   intent(inout) :: kdtree_source                 !< KD-tree
    integer(ip)                                       :: ipoin,iboun_min
    integer(ip)                                       :: kpoin,inodb
    integer(ip)                                       :: pnodb,pblty,ifoun,ptopo
    real(rp)                                          :: dista_min,proje(3)
    real(rp)                                          :: deriv(3*mnode)
    real(rp)                                          :: coloc(3),toler
    real(rp)                                          :: bocod(ndime,mnodb)

    if( kdtree_source % nboun /= 0 ) then

       toler = 0.01_rp

       do kpoin = 1,npoin_test 

          dista_min = huge(1.0_rp)
          !
          ! KDTREE returns the boundary number in global numbering because KDTree was constructed 
          ! using a permutation array
          !          
          call kdtree_nearest_boundary(coord_test(1,kpoin),kdtree_source,iboun_min,dista_min,proje)  
! if( dista_min < 0 ) print*, "DEBUG: dista min= ",dista_min,coord_test(:,kpoin),kpoin
          dista_min = abs(dista_min)          
          ifoun     = 0
          pblty     = abs(ltypb_cou(iboun_min))
          pnodb     = lnnob_cou(iboun_min)   
          ptopo     = ltopo(pblty)
          do inodb = 1,pnodb
             ipoin = lnodb_cou(inodb,iboun_min)
             bocod(1:ndime,inodb) = coord(1:ndime,ipoin)
          end do
          !print*,ndime,pblty,ptopo,pnodb

          call elmgeo_natural_coordinates_on_boundaries(&
               ndime,pblty,pnodb,bocod,         &
               shapf_test(1,kpoin),deriv,proje, & 
               coloc,ifoun,toler)
          !
          ! To avoid problems temporarly
          !

          ifoun = 1_ip

          !
!if(ifoun/=0) write(*,'(i2,7(1x,e12.6))') kfl_paral,coord_test(1:2,kpoin),kdtree_typ % coord(1:ndime,kdtree_typ % lnodb_cou1,iboun_min)),kdtree_typ % coord(1:ndime,kdtree_typ % lnodb_cou2,iboun_min))
!if(ifoun/=0) write(*,'(7(1x,e12.6))') kdtree_typ % coord(1:ndime,kdtree_typ % lnodb_cou1,iboun_min)),coord(1:ndime,lnodb_cou1,iboun_min))

          if( ifoun == 0 ) then
!write(6,'(a,3(1x,i5))')             'A=',kfl_paral,kpoin,iboun_min,nboun
!write(6,'(a,2(1x,i4),3(1x,e12.6))') 'B=',kfl_paral,kpoin,coloc

!write(6,'(a,i2,7(1x,i5))')          'C=',kfl_paral,kpoin,lnodb_cou1,iboun_min),lnodb_cou2,iboun_min),lnodb_cou3,iboun_min),lnodb_cou4,iboun_min)
!write(6,'(a,i2,i5,7(1x,e12.6))')    'D=',kfl_paral,kpoin,coord_test(1:3,kpoin)
!write(6,*) '1: ',coord(1:3,lnodb_cou1,iboun_min))
!write(6,*) '2: ',coord(1:3,lnodb_cou2,iboun_min))
!write(6,*) '3: ',coord(1:3,lnodb_cou3,iboun_min))
!write(6,*) '4: ',coord(1:3,lnodb_cou4,iboun_min))
!!coord(1:ndime,kdtree_typ % lnodb_cou1,iboun_min)),kdtree_typ % coord(1:ndime,kdtree_typ % lnodb_cou2,iboun_min))
!call flush(6)
!call runend('O.K.!')
             inout_test(kpoin) = 0
             dimin_test(kpoin) = dista_min
          else
             inout_test(kpoin) = iboun_min
             dimin_test(kpoin) = dista_min    
          end if

       end do

    end if

  end subroutine COU_WET_POINTS_HOST_BOUNDARIES

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    11/03/2014
  !> @brief   Check in which elements are the test points
  !> @details For each partition whcih have received some points,
  !>          check which of them host them
  !
  !----------------------------------------------------------------------

  subroutine COU_WET_POINTS_HOST_BOUNDARY_VECTOR(&
       npoin_test,coord_test,dimin_test,inout_test,shapf_test,kdtree_source)
    integer(ip),                        intent(in)    :: npoin_test                    !< Number of points to test
    real(rp),                           intent(in)    :: coord_test(ndime,npoin_test)  !< Coordinates of test points
    real(rp),                           intent(out)   :: dimin_test(npoin_test)        !< Minimum distance to a boundary node
    integer(ip),                        intent(out)   :: inout_test(npoin_test)        !< Node with minimum distance
    real(rp),                           intent(out)   :: shapf_test(mnodb,npoin_test)  !< Shape function in elements
    type(typ_kdtree),                   intent(inout) :: kdtree_source                 !< KD-tree
    integer(ip)                                       :: ipoin,iboun_min
    integer(ip)                                       :: kpoin,inodb
    integer(ip)                                       :: pnodb,pblty,ifoun,ptopo
    real(rp)                                          :: dista_min,proje(3)
    real(rp)                                          :: deriv(3*mnode)
    real(rp)                                          :: coloc(3),toler
    real(rp)                                          :: bocod(ndime,mnodb)


    call runend('COU_WET_POINTS_HOST_BOUNDARY_VECTOR is not ready')

    if( kdtree_source % nboun /= 0 ) then

       toler = 0.01_rp

       do kpoin = 1,npoin_test 
          !
          ! Look for boundary for wet point coord_test(:,kpoin)
          !
          ! shapf_test(1:mnodb,kpoin)
          ! inout_test(kpoin) = iboun_min
          ! dimin_test(kpoin) = dista_min
          !call elmgeo_natural_coordinates(      &
          !     ndime,pblty,ptopo,pnodb,bocod,   &
          !     shapf_test(1,kpoin),deriv,proje, &
          !     coloc,ifoun,toler)
       end do

    end if

  end subroutine COU_WET_POINTS_HOST_BOUNDARY_VECTOR

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    23/09/2014
  !> @brief   List of wet nodes
  !> @details Give the list of source node for a specific coupling
  !>          of for all couplings
  !
  !----------------------------------------------------------------------

  subroutine COU_LIST_SOURCE_NODES(list_source_nodes,kcoup,what_to_do)
    logical(lg),  intent(out), pointer  :: list_source_nodes(:)
    integer(ip),  intent(in),  optional :: kcoup
    character(*), intent(in),  optional :: what_to_do
    integer(ip)                         :: icoup_ini,icoup_end,icoup
    integer(ip)                         :: kpoin,ipoin
    !
    ! Allocate of necessary
    !
    if( associated(list_source_nodes) ) then
       if( present(what_to_do) ) then
          if( trim(what_to_do) == 'INITIALIZE' ) then
             do ipoin = 1,size(list_source_nodes)
                list_source_nodes(ipoin) = .false.
             end do
          end if
       end if
    else
       call memory_alloca(memor_cou,'COU_LIST_SOURCE_NODES','list_source_nodes',list_source_nodes,npoin)
    end if
    !
    ! Bounds
    !
    if( present(kcoup) ) then
       if( kcoup /= 0 ) then
          icoup_ini = kcoup
          icoup_end = kcoup
       else
          icoup_ini = 1
          icoup_end = mcoup          
       end if
    else
       icoup_ini = 1
       icoup_end = mcoup
    end if
    !
    ! Activate node
    !
    do icoup = icoup_ini,icoup_end
       do kpoin = 1,coupling_type(icoup) % geome % npoin_source
          ipoin = coupling_type(icoup)  % geome % lpoin_source(kpoin)
          list_source_nodes(ipoin) = .true.
       end do
    end do
 
  end subroutine COU_LIST_SOURCE_NODES

  !----------------------------------------------------------------------
  !
  !> @author  Guillaume Houzeaux
  !> @date    07/03/2014
  !> @brief   If there is at least one zone coupling
  !> @details If there is at least one zone coupling
  !
  !----------------------------------------------------------------------

  function THERE_EXISTS_A_ZONE_COUPLING()
    integer(ip) :: icoup
    logical(lg) :: THERE_EXISTS_A_ZONE_COUPLING
    
    THERE_EXISTS_A_ZONE_COUPLING = .false.
    do icoup = 1,mcoup
       if( coupling_type(icoup) % zone_target + coupling_type(icoup) % zone_source /= 0 ) then
          THERE_EXISTS_A_ZONE_COUPLING = .true.
          return
       end if
    end do
  end function THERE_EXISTS_A_ZONE_COUPLING

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    07/03/2014
  !> @brief   Initialize a value on target nodes
  !> @details Initialize a value on target nodes only if the coupling
  !>          is not on whole mesh
  !>
  !----------------------------------------------------------------------

  subroutine COU_PUT_VALUE_ON_TARGET_IP_1(value_in,xarray,kcoup)
    integer(ip), intent(in)            :: value_in
    integer(ip), intent(out), pointer  :: xarray(:)
    integer(ip), intent(in),  optional :: kcoup
    integer(ip)                        :: icoup_ini,icoup_fin
    integer(ip)                        :: kpoin,ipoin,icoup

    if( mcoup > 0 ) then 
       if( present(kcoup) ) then
          icoup_ini = kcoup
          icoup_fin = kcoup
       else
          icoup_ini = 1
          icoup_fin = mcoup
       end if
 
       do icoup = icoup_ini,icoup_fin
          if( coupling_type(icoup) % where_type /= ON_WHOLE_MESH ) then
             do kpoin = 1,coupling_type(icoup) % geome % npoin_wet
                ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                if( ipoin > 0 ) xarray(ipoin) = value_in
             end do
          end if
       end do
    end if

  end subroutine COU_PUT_VALUE_ON_TARGET_IP_1

  subroutine COU_PUT_VALUE_ON_TARGET_IP_2(value_in,xarray,kcoup)
    integer(ip), intent(in)            :: value_in(*)
    integer(ip), intent(out), pointer  :: xarray(:,:)
    integer(ip), intent(in),  optional :: kcoup
    integer(ip)                        :: icoup_ini,icoup_fin
    integer(ip)                        :: ndofn,kpoin,ipoin,icoup

    if( mcoup > 0 ) then
       ndofn = size(xarray,1)
       if( present(kcoup) ) then
          icoup_ini = kcoup
          icoup_fin = kcoup
       else
          icoup_ini = 1
          icoup_fin = mcoup
       end if

       do icoup = icoup_ini,icoup_fin
          if( coupling_type(icoup) % where_type /= ON_WHOLE_MESH ) then
             do kpoin = 1,coupling_type(icoup) % geome % npoin_wet
                ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                if( ipoin > 0 ) xarray(1:ndofn,ipoin) = value_in(1:ndofn)
             end do
          end if
       end do
    end if

  end subroutine COU_PUT_VALUE_ON_TARGET_IP_2

  subroutine COU_PUT_VALUE_ON_TARGET_IP_12(value_in,xarray,kcoup)
    integer(ip), intent(in)            :: value_in
    integer(ip), intent(out), pointer  :: xarray(:,:)
    integer(ip), intent(in),  optional :: kcoup
    integer(ip)                        :: icoup_ini,icoup_fin
    integer(ip)                        :: ndofn,kpoin,ipoin,icoup

    if( mcoup > 0 ) then
       ndofn = size(xarray,1)
       if( present(kcoup) ) then
          icoup_ini = kcoup
          icoup_fin = kcoup
       else
          icoup_ini = 1
          icoup_fin = mcoup
       end if

       do icoup = icoup_ini,icoup_fin
          if( coupling_type(icoup) % where_type /= ON_WHOLE_MESH ) then
             do kpoin = 1,coupling_type(icoup) % geome % npoin_wet
                ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                if( ipoin > 0 ) xarray(1:ndofn,ipoin) = value_in
             end do
          end if
       end do
    end if

  end subroutine COU_PUT_VALUE_ON_TARGET_IP_12
  
  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    14/10/2014
  !> @brief   Change fixity array
  !> @details Modify fixity array on target:
  !>          - Coupling between zones:
  !>            UNKNOWN type: set fixity to FIXED_UNKNOWN whenener it 
  !>            is different from 0. Can be forced by using "FORCE"
  !>          - Coupling between subdomains:
  !>            RESIDUAL type: free the nodes if FORCE SOBDOMAIN 
  !             is present
  !>
  !----------------------------------------------------------------------

  subroutine COU_SET_FIXITY_ON_TARGET(variable,imodu,kfl_fixno,what)
    character(*), intent(in)              :: variable
    integer(ip),  intent(in)              :: imodu
    integer(ip),  intent(inout), pointer  :: kfl_fixno(:,:)
    character(*), intent(in), optional    :: what
    integer(ip)                           :: ipoin,kpoin,npoin_wet
    integer(ip)                           :: ndofn,kdofn,kpoin_fixed
    integer(ip)                           :: icoup,iffix_max,idofn
    integer(ip)                           :: code_target,zone_target
    logical(lg)                           :: force_subdomain

    if( mcoup == 0 ) return
    !
    ! What to do
    !
    iffix_max       = 0
    ndofn           = size(kfl_fixno,1)
    force_subdomain = .false.
    if( present(what) ) then
       if( trim(what) == 'FORCE ZONE' ) then
          iffix_max = huge(1_ip)
       else if( trim(what) == 'FREE BETWEEN SUBDOMAINS' ) then
          force_subdomain = .true.
       end  if
    end if

    do icoup = 1,mcoup

       if( coupling_type(icoup) % where_type /= ON_WHOLE_MESH ) then

          code_target  = coupling_type(icoup) % code_target
          zone_target  = coupling_type(icoup) % zone_target
          color_target = coupling_type(icoup) % color_target
          npoin_wet    = coupling_type(icoup) % geome % npoin_wet

          if( I_AM_IN_COLOR(color_target) ) then

             if( coupling_type(icoup) % kind == BETWEEN_SUBDOMAINS ) then
                !
                ! Between subdomains: just check not all dofs are prescribed
                !
                if( force_subdomain ) then
                   kpoin_fixed = 0
                   do kpoin = 1,coupling_type(icoup) % geome % npoin_wet
                      ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                      kfl_fixno(1:ndofn,ipoin) = 0
                   end do
                else
                   kpoin_fixed = 0
                   do kpoin = 1,coupling_type(icoup) % geome % npoin_wet
                      ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                      if( minval(kfl_fixno(1:ndofn,ipoin)) > 0 ) kpoin_fixed = kpoin_fixed + 1
                   end do
                   if( kpoin_fixed == npoin_wet ) then
                      kpoin = 1
                   else
                      kpoin = 0
                   end if
                   call PAR_MIN(kpoin,'IN CURRENT TARGET COLOR')
                   if( kpoin == 1 ) then
                      call runend('ALL DOFS ARE PRESCIBED ON INTERFACE')
                   end if
                end if

             else if( coupling_type(icoup) % kind == BETWEEN_ZONES .and. lzone(imodu) == zone_target  ) then
                !
                ! Between zones
                !
                ! Unknown type:  force iffix
                ! Residual type: just check not all dofs are prescribed
                !
                if( coupling_type(icoup) % variable == variable(1:5) ) then

                   if( coupling_type(icoup) % what == UNKNOWN  ) then
                      do kpoin = 1,coupling_type(icoup) % geome % npoin_wet
                         ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                         do idofn = 1,ndofn
                            if( kfl_fixno(idofn,ipoin) <= iffix_max ) kfl_fixno(idofn,ipoin) = FIXED_UNKNOWN
                         end do
                      end do

                   else if( coupling_type(icoup) % what == RESIDUAL ) then

                      kpoin_fixed = 0
                      do kpoin = 1,coupling_type(icoup) % geome % npoin_wet
                         ipoin = coupling_type(icoup) % geome % lpoin_wet(kpoin)
                         kdofn = 0
                         if( minval(kfl_fixno(1:ndofn,ipoin)) > 0 ) kpoin_fixed = kpoin_fixed + 1
                      end do
                      if( kpoin_fixed == npoin_wet ) then
                         kpoin = 1
                      else
                         kpoin = 0
                      end if
                      call PAR_MIN(kpoin,'IN CURRENT TARGET COLOR')
                      if( kpoin == 1 ) then
                         print*,'popopo=',current_code,' ',variable
                         call runend('ALL DOFS ARE PRESCIBED ON INTERFACE')
                      end if

                   end if

                end if

             end if

          end if

       end if

    end do

  end subroutine COU_SET_FIXITY_ON_TARGET


















  subroutine COU_TRANSMISSION_ARRAYS(xx,icolo,jcolo,coupling)
    use mod_parall,         only : par_color_to_code,par_color_to_zone,par_color_to_subd,PAR_MY_WORLD_RANK
    use mod_communications, only : PAR_COUPLING_NODE_EXCHANGE
    use def_master,         only : igene,givec
    implicit none
    integer(ip),              intent(in)    :: icolo
    integer(ip),              intent(in)    :: jcolo
    real(rp),    pointer,     intent(in)    :: xx(:,:)
    type(typ_color_coupling), intent(inout) :: coupling
    integer(ip)                             :: ii,jj,kk,pp,kpart,ipart
    integer(ip)                             :: cpart_owner,number_wet_points
    integer(ip)                             :: ielem,ierro,cz,ksize,kdtree_wet
    integer(ip)                             :: cpart,ineig,isend,irecv
    integer(ip)                             :: ksend,lsend,lrecv,krecv
    integer(ip)                             :: kelem,inode,idime,inodb,pnodb
    integer(ip)                             :: npoin_to_send,npoin_to_recv
    integer(ip)                             :: nenti,kboun,iboun,ipoin,kpoin
    integer(ip)                             :: jcoup,ipart_world
    integer(ip)                             :: PAR_CURRENT_SIZE
    integer(ip)                             :: PAR_CURRENT_RANK
    integer(ip)                             :: PAR_COMM_SAVE
    real(rp)                                :: dista_min
    type(r2p),   pointer                    :: shapf(:)
    integer(ip), pointer                    :: list_neighbors(:,:)
    integer(ip), pointer                    :: npoin_send(:)
    integer(ip), pointer                    :: npoin_recv(:)
    integer(ip), pointer                    :: mask_npoin(:)
    type(i1p),   pointer                    :: list_transmission_nodes_part(:)
    type(i1p),   pointer                    :: decision_send(:)
    type(i1p),   pointer                    :: decision_recv(:)
    type(r1p),   pointer                    :: distance_send(:)
    type(r1p),   pointer                    :: distance_recv(:)
    type(i1p),   pointer                    :: my_part_to_point(:)
    type(i1p),   pointer                    :: my_point_to_part(:)
    type(r1p),   pointer                    :: coord_send(:)
    type(r1p),   pointer                    :: coord_recv(:)
    type(i1p),   pointer                    :: my_send_permut(:)
    logical(lg)                             :: require_distance
    integer(ip), pointer                    :: lesou(:)
    logical(lg), pointer                    :: lnsou(:)
    logical(lg), pointer                    :: intersection(:)
    logical(lg), pointer                    :: list_source_nodes(:)
    integer(ip), pointer                    :: PAR_WORLD_RANKS(:)
    real(rp)                                :: time0,time1,time2,time3,time4,time5


    type(i1p),   pointer :: number_nodes_send(:)
    type(i1p),   pointer :: number_nodes_recv(:)
    type(i1p),   pointer :: list_nodes_send(:)
    type(i1p),   pointer :: list_nodes_recv(:)
    type(i1p),   pointer :: permu_nodes_send(:)
    type(i1p),   pointer :: permu_nodes_recv(:)
    type(r1p),   pointer :: matrix_nodes_send(:)
    type(r1p),   pointer :: matrix_nodes_recv(:)
    type(i1p),   pointer :: invpr_nodes_send(:)
    type(i1p),   pointer :: invpr_nodes_recv(:)
    integer(ip), pointer :: permu_lgaub(:,:)
    integer(ip), pointer :: list_different_nodes(:)
    logical(lg), pointer :: mark_node(:)
    integer(ip)          :: inise_list,inire_list,inise,finse,nneig,ll,jpoin
    integer(ip)          :: inode_source,ipoin_source,number_different_nodes
    integer(ip)          :: inire,finre,nsend,nrecv,dom_i,jelem,pnode,inode_target
    integer(ip)          :: npoin_source,ipoin_local,ipoin_global,izdom,inode_local
    integer(ip)          :: jboun,kgaub,pblty,iboun_global,iboun_wet,inodb_target
    integer(ip)          :: igaub,jgaub,pgaub,ipoin_wet,lgaub,inodb_source
    integer(ip)          :: npoin_wet
    real(rp)             :: gbsur,xcoef
    real(rp),    pointer :: rr(:)
    nullify(number_nodes_send)
    nullify(number_nodes_recv)
    nullify(list_nodes_send)
    nullify(list_nodes_recv)
    nullify(permu_nodes_send)
    nullify(permu_nodes_recv)
    nullify(matrix_nodes_send)
    nullify(matrix_nodes_recv)
    nullify(invpr_nodes_send)
    nullify(invpr_nodes_recv)
    nullify(permu_lgaub)
    nullify(list_different_nodes)
    nullify(mark_node)

    ! call extrae_restart()

    call cputim(time0)
    ! call extrae_eventandcounters(600_4,1_8)
    !
    ! Initialize coupling structure COUPLING
    !
    call COU_INITIALIZE_COUPLING_STRUCTURE(coupling)
    !
    ! Communicator
    !
    PAR_COMM_SAVE    = PAR_COMM_CURRENT
    PAR_COMM_CURRENT = PAR_COMM_COLOR(icolo,jcolo) 
    call PAR_COMM_RANK_AND_SIZE(PAR_COMM_CURRENT,PAR_CURRENT_RANK,PAR_CURRENT_SIZE)
    !
    ! Sizes 
    !
    if( .not. associated(xx) ) then
       number_wet_points = 0 
    else
       number_wet_points = size(xx,2)
       if( coupling % geome % number_wet_points /= 0 ) then
          if( coupling % geome % number_wet_points /= number_wet_points ) &
               call runend('SOMETHING STRANGE HAPPENS!')
       else
          coupling % geome % number_wet_points = number_wet_points 
       end if
    end if
    !
    ! Nullify
    !
    nullify(shapf)
    nullify(list_neighbors)
    nullify(npoin_send)
    nullify(npoin_recv)
    nullify(mask_npoin)
    nullify(list_transmission_nodes_part)
    nullify(decision_send)
    nullify(decision_recv)
    nullify(distance_send)
    nullify(distance_recv)
    nullify(my_part_to_point)
    nullify(my_point_to_part)
    nullify(coord_send)
    nullify(coord_recv)
    nullify(my_send_permut)
    nullify(lesou)
    nullify(lnsou)
    nullify(intersection)
    nullify(list_source_nodes)
    nullify(PAR_WORLD_RANKS)
    !
    ! If the distance criterion is required
    !
    if(     coupling % itype == NEAREST_BOUNDARY_NODE  .or. &
         &  coupling % itype == BOUNDARY_INTERPOLATION .or. &
         &  coupling % itype == ELEMENT_INTERPOLATION  .or. &
         &  coupling % itype == STRESS_PROJECTION      .or. &
         &  coupling % itype == PROJECTION             .or. &
         &  coupling % itype == NEAREST_ELEMENT_NODE   .or. &
         &  coupling % itype == BOUNDARY_VECTOR_PROJECTION ) then
       require_distance = .true.
    else
       require_distance = .false.
    end if
    !
    ! Source elements/boundaries/nodes
    !
    if( INOTMASTER ) then
       call memory_alloca(memor_cou,'NELEM','par_test',lesou,nelem)
       call memory_alloca(memor_cou,'NPOIN','par_test',lnsou,npoin)
       if( coupling % kind == BETWEEN_SUBDOMAINS ) then
          do ielem = 1,nelem
             if( lesub(ielem) == coupling % subdomain_source ) then
                lesou(ielem) = 1
             end if
          end do
       else         
          do kelem = 1,nelez(coupling % zone_source)
             ielem = lelez(coupling % zone_source) % l(kelem)
             lesou(ielem) = 1
          end do
       end if
       do ielem = 1,nelem
          if( lesou(ielem) == 1 ) then
             do inode = 1,lnnod(ielem)
                ipoin = lnods(inode,ielem)
                lnsou(ipoin) = .true.
             end do
          end if
       end do
    end if
    !
    ! Allocate memory
    !
    cz = PAR_CURRENT_SIZE
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',npoin_send,      cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',npoin_recv,      cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',decision_send,   cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',decision_recv,   cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',coord_send,      cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',coord_recv,      cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',my_part_to_point,cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',my_point_to_part,number_wet_points,'INITIALIZE')
    call memory_alloca(memor_cou,'SHAPF'     ,'par_test',shapf,           cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',my_send_permut,  cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',intersection,    cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'NPOIN_SEND','par_test',PAR_WORLD_RANKS, cz,               'INITIALIZE',0_ip)
    call memory_alloca(memor_cou,'LIST'      ,'par_test',list_transmission_nodes_part,cz,   'INITIALIZE',0_ip)
    if( require_distance ) then
       call memory_alloca(memor_cou,'MY_SEND_DIST','par_test',distance_send,cz,'INITIALIZE',0_ip)
       call memory_alloca(memor_cou,'MY_RECV_DIST','par_test',distance_recv,cz,'INITIALIZE',0_ip)
    end if
    !
    ! Find list of partitions to which I must send my points
    !
    do pp = 1,number_wet_points
       call maths_mapping_coord_to_3d(ndime,par_bin_boxes,par_bin_comin,par_bin_comax,xx(1:ndime,pp),ii,jj,kk)
       if( ii /= 0 ) then 
          ksize = 0
          do kpart = 1,par_bin_size(ii,jj,kk) 
             ipart = par_bin_part(ii,jj,kk) % l(kpart)
             if( maths_in_box(ndime,xx(1:ndime,pp),par_part_comin(1:ndime,ipart),par_part_comax(1:ndime,ipart)) ) then
                cpart = PAR_COMM_COLOR_PERM(icolo,jcolo,ipart)
                !
                ! Take those partitions only in color JCOLO
                !
                if( par_part_in_color(ipart,jcolo) ) then
                   ksize = ksize + 1
                   npoin_send(cpart) = npoin_send(cpart) + 1  
                end if
             end if
          end do
          if( ksize > 0 ) then
             allocate( my_point_to_part(pp) % l(ksize))
             ksize = 0
             do kpart = 1,par_bin_size(ii,jj,kk) 
                ipart = par_bin_part(ii,jj,kk) % l(kpart)
                if( maths_in_box(ndime,xx(1:ndime,pp),par_part_comin(1:ndime,ipart),par_part_comax(1:ndime,ipart)) ) then
                   if( par_part_in_color(ipart,jcolo) ) then
                      ksize = ksize + 1
                      cpart = PAR_COMM_COLOR_PERM(icolo,jcolo,ipart)
                      my_point_to_part(pp) % l(ksize) = cpart
                   end if
                end if
             end do
          end if
       end if
    end do

    !--------------------------------------------------------------------
    !
    !  1. How many wet points I send to (I) and how many I receive from (J)
    !
    !                            +-------+
    !                        =>  |       |  =>
    !                            |       |
    !          NPOIN_RECV(I) =>  |       |  => NPOIN_SEND(I)
    !                            +-------+
    !
    !  2. Send coordinates and receive coordinates
    !
    !                            +-------+
    !                        =>  |       |  =>
    !                            |       |
    !      COORD_RECV(I) % A =>  |       |  => COORD_SEND(I) % A
    !                            +-------+
    !
    !  3. Check if I have what I receive (find host element, nearest point, etc.) and send it back: DECISION_SEND(I) % L
    !     Receive the decision of the others: DECISION_RECV(I) % L
    !     In case of nearest point, send the minimal distance I have found as well: DISTANCE_SEND(I) % A
    !     Receive the minimal distance of others as well: DISTANCE_RECV(I) % A
    !
    !                            +-------+
    !                        <=  |       |  <=
    !                            |       |
    !   DECISION_SEND(I) % L <=  |       |  <= DECISION_RECV(I) % L
    !                            +-------+
    !                         
    !--------------------------------------------------------------------
    !
    ! Copy my points
    !
    do cpart = 0,PAR_CURRENT_SIZE-1
       if( npoin_send(cpart) > 0 ) allocate( my_part_to_point(cpart) % l(npoin_send(cpart)) )
       npoin_send(cpart) = 0
    end do
    do pp = 1,number_wet_points 
       call maths_mapping_coord_to_3d(ndime,par_bin_boxes,par_bin_comin,par_bin_comax,xx(1:ndime,pp),ii,jj,kk)
       if( ii /= 0 ) then 
          do kpart = 1,par_bin_size(ii,jj,kk) 
             ipart = par_bin_part(ii,jj,kk) % l(kpart)
             if( maths_in_box(ndime,xx(1:ndime,pp),par_part_comin(1:ndime,ipart),par_part_comax(1:ndime,ipart)) ) then
                if( par_part_in_color(ipart,jcolo) ) then
                   cpart = PAR_COMM_COLOR_PERM(icolo,jcolo,ipart)
                   npoin_send(cpart) = npoin_send(cpart) + 1
                   my_part_to_point(cpart) % l(npoin_send(cpart)) = pp
                end if
             end if
          end do
       end if
    end do
    !
    ! Bounding box intersection 
    !
    PAR_WORLD_RANKS(PAR_CURRENT_RANK) = PAR_MY_WORLD_RANK
    call PAR_MAX(PAR_CURRENT_SIZE,PAR_WORLD_RANKS(0:PAR_CURRENT_SIZE-1),'IN CURRENT COUPLING') ! should be optimized
    do cpart = 0,PAR_CURRENT_SIZE-1
       intersection(cpart) = .true. 
       ipart_world = PAR_WORLD_RANKS(cpart)
       do idime = 1,ndime
          if(    par_part_comin(idime,PAR_MY_WORLD_RANK) >= par_part_comax(idime,ipart_world) .or. &
               & par_part_comin(idime,ipart_world)       >= par_part_comax(idime,PAR_MY_WORLD_RANK) ) then
             intersection(cpart) = .false.
          end if
       end do
    end do
    !
    ! For vector projection, send the wet points to all the partitions as we have no way
    ! to know where the point falls. Idea: consider only the partitions bounding boxes
    ! crossed by the segment formed by the wet node ( x_wet , x_wet + 10000 * projection_vector )  
    ! 
    ! +-----+------+-------+-----+
    ! |     |      | x_wet |     |
    ! |     |      |    x  |     |
    ! +-----+------+----|--+-----+
    ! |     |      |    |  |     |
    ! |     |      |    |  |     |
    ! +-----+------+----|--+-----+
    ! |     |      |    |  |     |
    ! |     |      |    |  |     |
    ! +-----+------+----|--+-----+
    ! ///////////////// | ////////
    !                   |
    !                   | projection_vector
    !                   |
    !                   \/   
    !                   x 
    !
    if( coupling % itype == BOUNDARY_VECTOR_PROJECTION .and. INOTMASTER ) then
       do cpart = 1,PAR_CURRENT_SIZE-1
          intersection(cpart) = .true. 
       end do
    end if
    !
    ! Get how many points I should check if I have them
    !
    call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
    call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
    do cpart = 0,PAR_CURRENT_SIZE-1
       if( intersection(cpart) ) then
          call PAR_SEND_RECEIVE(1_ip,1_ip,npoin_send(cpart:cpart),npoin_recv(cpart:cpart),'IN CURRENT COUPLING',cpart,'NON BLOCKING')
          !call PAR_SEND_RECEIVE(1_ip,1_ip,npoin_send(cpart:cpart),npoin_recv(cpart:cpart),'IN CURRENT COUPLING',cpart)
       end if
    end do
    call PAR_END_NON_BLOCKING_COMM(1_ip)


    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time1) 
    cputi_cou(6) = cputi_cou(6) + time1-time0
    !
    ! Save point coordinates
    !
    do cpart = 0,PAR_CURRENT_SIZE-1
       npoin_to_send = npoin_send(cpart)
       npoin_to_recv = npoin_recv(cpart)
       if( npoin_to_send > 0 ) then
          call memory_alloca(memor_cou,'COORD_SEND',   'par_test',coord_send(cpart) % a,npoin_to_send*ndime)
          call memory_alloca(memor_cou,'DECISION_RECV','par_test',decision_recv(cpart) % l,npoin_to_send)
          if( require_distance ) then
             call memory_alloca(memor_cou,'DISTANCE_RECV','par_test',distance_recv(cpart) % a,npoin_to_send)
          end if
       end if
       if( npoin_to_recv > 0 ) then
          call memory_alloca(memor_cou,'COORD_RECV',    'par_test',coord_recv(cpart) % a,    npoin_to_recv*ndime)
          call memory_alloca(memor_cou,'DECISION_SEND', 'par_test',decision_send(cpart) % l, npoin_to_recv)
          call memory_alloca(memor_cou,'MY_SEND_PERMUT','par_test',my_send_permut(cpart) % l,npoin_to_recv)
          if( require_distance ) then
             call memory_alloca(memor_cou,'DISTANCE_SEND','par_test',distance_send(cpart) % a,npoin_to_recv)
          end if
       end if
    end do
    !
    ! Copy points to send buffer
    !
    do cpart = 0,PAR_CURRENT_SIZE-1
       npoin_to_send = npoin_send(cpart)
       if( npoin_to_send > 0 ) then
          kk = 0
          do ii = 1,npoin_to_send
             pp = my_part_to_point(cpart) % l(ii)
             do idime = 1,ndime
                kk = kk + 1
                coord_send(cpart) % a(kk) = xx(idime,pp)
             end do
          end do
       end if
    end do
    !
    ! Get points coordinates
    !
    call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
    call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
    do cpart = 0,PAR_CURRENT_SIZE-1
       if( intersection(cpart) ) then
          call PAR_SEND_RECEIVE(coord_send(cpart) % a,coord_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
       end if
    end do
    call PAR_END_NON_BLOCKING_COMM(1_ip)

    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time2) 
    ! call extrae_eventandcounters(600_4,2_8)
    cputi_cou(3) = cputi_cou(3) + time2-time1

    if( ISEQUEN ) then

    else if( IMASTER ) then
    else if( INOTMASTER ) then

       call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
       call PAR_START_NON_BLOCKING_COMM(2_ip,PAR_CURRENT_SIZE)
       ! 
       ! Check if I own the points
       !       
       if( coupling % itype == ELEMENT_INTERPOLATION ) then
          !
          ! Element interpolation
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call memory_alloca(memor_cou,'SHAPF(CPART)','par_test',shapf(cpart)%a,mnode,pp)
                call COU_WET_POINTS_HOST_ELEMENTS(pp,coord_recv(cpart) % a, distance_send(cpart) % a, shapf(cpart) % a,decision_send(cpart) % l,lesou)
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       else if( coupling % itype == NEAREST_BOUNDARY_NODE ) then
          !
          ! Nearest boundary node
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call COU_WET_POINTS_NEAREST_BOUNDARY_NODE(pp,coord_recv(cpart) % a,distance_send(cpart) % a,decision_send(cpart) % l,lnsou)
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       else if( coupling % itype == NEAREST_ELEMENT_NODE ) then
          !
          ! Nearest element node
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call COU_WET_POINTS_NEAREST_ELEMENT_NODE(pp,coord_recv(cpart) % a,distance_send(cpart) % a,decision_send(cpart) % l,lnsou)
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       else if( coupling % itype == BOUNDARY_INTERPOLATION .or. coupling % itype == STRESS_PROJECTION .or. coupling % itype == PROJECTION ) then
          !
          ! Boundary interpolation and Gauss point interpolation
          ! Wet points are nodes in the first case and Gauss points in the second case
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call memory_alloca(memor_cou,'SHAPF(CPART)','par_test',shapf(cpart)%a,mnodb,pp)
                call COU_WET_POINTS_HOST_BOUNDARIES(pp,coord_recv(cpart) % a,distance_send(cpart) % a,&
                     &                              decision_send(cpart) % l,shapf(cpart) % a,        &
                     &                              coupling % geome % kdtree                         )
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       else if( coupling % itype == BOUNDARY_VECTOR_PROJECTION ) then
          !
          ! Look for the boudnary corssing the projection along a vector
          !
          do cpart = 0,PAR_CURRENT_SIZE-1
             if( npoin_recv(cpart) /= 0 ) then
                pp = npoin_recv(cpart)
                call memory_alloca(memor_cou,'SHAPF(CPART)','par_test',shapf(cpart)%a,mnodb,pp)
                call COU_WET_POINTS_HOST_BOUNDARY_VECTOR(pp,coord_recv(cpart) % a,distance_send(cpart) % a,&
                     &                                   decision_send(cpart) % l,shapf(cpart) % a,        &
                     &                                   coupling % geome % kdtree                         )
             end if
             if( intersection(cpart) ) then
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
                call PAR_SEND_RECEIVE(decision_send(cpart) % l,decision_recv(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
                call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
                call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
             end if
          end do

       end if
       !
       ! Send my result and distance if necessary
       !
       !do cpart = 0,PAR_CURRENT_SIZE-1
       !   if( intersection(cpart) ) then
       !      call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
       !      call PAR_SEND_RECEIVE(decision_send(cpart) % l,   decision_recv(cpart) % l,'IN CURRENT',cpart,'NON BLOCKING')
       !   end if
       !end do
       !if( require_distance ) then
       !   do cpart = 0,PAR_CURRENT_SIZE-1
       !      if( intersection(cpart) ) then
       !         call PAR_SET_NON_BLOCKING_COMM_NUMBER(2_ip)
       !         call PAR_SEND_RECEIVE(distance_send(cpart) % a,distance_recv(cpart) % a,'IN CURRENT',cpart,'NON BLOCKING')
       !      end if
       !   end do
       !end if
       call PAR_END_NON_BLOCKING_COMM(1_ip)
       call PAR_END_NON_BLOCKING_COMM(2_ip)
    end if

    !call runend('O.K.!')

    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time3) 
    ! call extrae_eventandcounters(600_4,3_8)
    cputi_cou(4) = cputi_cou(4) + time3-time2

    if( INOTMASTER ) then

       if( require_distance ) then
          !
          ! Look for minimum distance
          !
          do pp = 1,number_wet_points
             cpart_owner = 0
             dista_min = huge(1.0_rp)
             do cpart = 0,PAR_CURRENT_SIZE-1
                kk = 0
                do while( kk < npoin_send(cpart) )
                   kk = kk + 1                
                   if( pp == my_part_to_point(cpart) % l(kk) .and. decision_recv(cpart) % l(kk) /= 0 ) then
                      if( distance_recv(cpart) % a(kk) < dista_min ) then
                         cpart_owner = cpart
                         dista_min   = distance_recv(cpart) % a(kk)
                      end if
                   end if
                end do
             end do
             do cpart = 0,PAR_CURRENT_SIZE-1
                do kk = 1,npoin_send(cpart)
                   if( pp == my_part_to_point(cpart) % l(kk) ) then
                      if( cpart /= cpart_owner ) then
                         decision_recv(cpart) % l(kk) = 0
                      end if
                   end if
                end do
             end do
             if( associated(my_point_to_part(pp) % l) ) my_point_to_part(pp) % l(1) = cpart_owner
          end do

       end if
       !
       ! Send my result back
       !
       call PAR_START_NON_BLOCKING_COMM(1_ip,PAR_CURRENT_SIZE)
       call PAR_SET_NON_BLOCKING_COMM_NUMBER(1_ip)
       do cpart = 0,PAR_CURRENT_SIZE-1
          if( intersection(cpart) ) then
             call PAR_SEND_RECEIVE(decision_recv(cpart) % l,decision_send(cpart) % l,'IN CURRENT COUPLING',cpart,'NON BLOCKING')
          end if
       end do
       call PAR_END_NON_BLOCKING_COMM(1_ip)

       !-----------------------------------------------------------------
       !
       ! decision_send(cpart) % l(1:npoin_recv(cpart)) /= 0 => I am in charge of this point OF CPART
       ! decision_recv(cpart) % l(1:npoin_send(cpart)) /= 0 => I need point from CPART
       !
       !-----------------------------------------------------------------
       !
       ! Fill in type
       !
       call memory_alloca(memor_cou,'LIST_NEIGHBORS','par_test',list_neighbors,2_ip,PAR_CURRENT_SIZE,'INITIALIZE',1_ip,0_ip)
       !
       ! Number of subdomains to communicate with
       !
       coupling % commd % nneig = 0
       do cpart = 0,PAR_CURRENT_SIZE-1
          do ii = 1,npoin_recv(cpart)
             list_neighbors(1,cpart) = list_neighbors(1,cpart) + min(1_ip,decision_send(cpart) % l(ii)) 
          end do
          do ii = 1,npoin_send(cpart)
             list_neighbors(2,cpart) = list_neighbors(2,cpart) + min(1_ip,decision_recv(cpart) % l(ii))
          end do
          coupling % commd % nneig = coupling % commd % nneig &
               + min(max(list_neighbors(1,cpart),list_neighbors(2,cpart)),1)
       end do
       !
       ! Send and receives sizes of subdomains
       ! COMMD % LSEND_SIZE(:)
       ! COMMD % LRECV_SIZE(:)
       !
       call memory_alloca(memor_cou,'NEIGHTS',   'par_test',coupling % commd % neights,   coupling % commd % nneig)
       call memory_alloca(memor_cou,'LSEND_SIZE','par_test',coupling % commd % lsend_size,coupling % commd % nneig+1)
       call memory_alloca(memor_cou,'LRECV_SIZE','par_test',coupling % commd % lrecv_size,coupling % commd % nneig+1)
       ineig = 0
       do cpart = 0,PAR_CURRENT_SIZE-1
          isend = list_neighbors(1,cpart)
          irecv = list_neighbors(2,cpart)
          if( max(isend,irecv) > 0 ) then
             ineig = ineig + 1
             coupling % commd % neights(ineig)    = cpart
             coupling % commd % lsend_size(ineig) = isend
             coupling % commd % lrecv_size(ineig) = irecv
          end if
       end do
       !
       ! Size to list
       !
       ksend = coupling % commd % lsend_size(1)
       krecv = coupling % commd % lrecv_size(1)
       coupling % commd % lsend_size(1) = 1
       coupling % commd % lrecv_size(1) = 1
       do ineig = 2,coupling % commd % nneig + 1
          lsend = coupling % commd % lsend_size(ineig)
          lrecv = coupling % commd % lrecv_size(ineig)
          coupling % commd % lsend_size(ineig) = coupling % commd % lsend_size(ineig-1) + ksend
          coupling % commd % lrecv_size(ineig) = coupling % commd % lrecv_size(ineig-1) + krecv
          ksend = lsend
          krecv = lrecv
       end do
       coupling % commd % lsend_dim = coupling % commd % lsend_size(coupling % commd % nneig + 1) - 1
       coupling % commd % lrecv_dim = coupling % commd % lrecv_size(coupling % commd % nneig + 1) - 1
       !
       ! Order points
       ! KK is the order I receive 
       ! 
       call memory_alloca(memor_cou,'STATUS','par_test',coupling % geome % status,coupling % geome % number_wet_points)             
       kk = 0
       do cpart = 0,PAR_CURRENT_SIZE-1
          do ii = 1,npoin_send(cpart)
             pp = my_part_to_point(cpart) % l(ii)
             if( my_point_to_part(pp) % l(1) == cpart ) then
                !
                ! CPART has wet point pp
                !
                kk = kk + 1
                coupling % geome % status(pp) = kk
             end if
          end do
       end do
       !
       ! Allocate geometrical information
       ! NENTI= number of entities (elements/boundaries/nodes)
       !
       nenti = 0
       do cpart = 0,PAR_CURRENT_SIZE-1
          nenti = nenti + list_neighbors(1,cpart)
       end do
       call memory_alloca(memor_cou,'MARK_NODE','par_test',mark_node,npoin)

       if( coupling % itype == ELEMENT_INTERPOLATION ) then
          !
          ! Element interpolation
          !
          coupling % geome % nelem_source = nenti
          call memory_alloca(memor_cou,'SHAPF'            ,'par_test',coupling % geome % shapf,mnode, coupling % geome % nelem_source)
          call memory_alloca(memor_cou,'LELEM'            ,'par_test',coupling % geome % lelem_source,coupling % geome % nelem_source)
          call memory_alloca(memor_cou,'LIST_SOURCE_NODES','par_test',list_source_nodes,npoin)

          kelem = 0
          do cpart = 0,PAR_CURRENT_SIZE-1
             kk = 0
             do ii = 1,npoin_recv(cpart)
                ielem = decision_send(cpart) % l(ii)
                if( ielem > 0 ) then
                   kelem = kelem + 1
                   coupling % geome % lelem_source(kelem)  = ielem
                   do inode = 1,mnode
                      coupling % geome % shapf(inode,kelem) = shapf(cpart) % a(inode,ii)
                   end do
                   do inode = 1,lnnod(ielem)
                      ipoin = lnods(inode,ielem)
                      list_source_nodes(ipoin) = .true.
                      if( .not. mark_node(ipoin) ) then
                         mark_node(ipoin) = .true.
                         kk = kk + 1
                      end if
                   end do
                end if
             end do
             call memory_alloca(memor_cou,'LIST_TRANSMISSION_NODES_CPART','par_test',list_transmission_nodes_part(cpart)%l,kk)
             kk = 0
             do ipoin = 1,npoin
                if( mark_node(ipoin) ) then
                   kk = kk + 1
                   list_transmission_nodes_part(cpart) % l(kk) = ipoin
                   mark_node(ipoin) = .false.
                end if
             end do
          end do

       else if( coupling % itype == NEAREST_BOUNDARY_NODE .or. coupling % itype == NEAREST_ELEMENT_NODE .or. coupling % itype == BOUNDARY_VECTOR_PROJECTION ) then
          !
          ! Nearest boundary node
          !
          coupling % geome % npoin_source = nenti          
          call memory_alloca(memor_cou,'LPOIN','par_test',coupling % geome % lpoin_source,coupling % geome % npoin_source)  

          kpoin = 0
          do cpart = 0,PAR_CURRENT_SIZE-1
             kk = 0
             do ii = 1,npoin_recv(cpart)
                ipoin = decision_send(cpart) % l(ii)
                if( ipoin > 0 ) then
                   kpoin = kpoin + 1
                   coupling % geome % lpoin_source(kpoin)  = ipoin
                end if
             end do
          end do

       else if( coupling % itype == BOUNDARY_INTERPOLATION ) then       
          !
          ! Boundary interpolation
          !  
          coupling % geome % nboun_source = nenti          
          call memory_alloca(memor_cou,'SHAPF'            ,'par_test',coupling % geome % shapf,mnodb, coupling % geome % nboun_source)
          call memory_alloca(memor_cou,'LBOUN'            ,'par_test',coupling % geome % lboun_source,coupling % geome % nboun_source)
          call memory_alloca(memor_cou,'LIST_SOURCE_NODES','par_test',list_source_nodes,npoin)

          kboun = 0
          do cpart = 0,PAR_CURRENT_SIZE-1
             kk = 0
             do ii = 1,npoin_recv(cpart)
                iboun = decision_send(cpart) % l(ii)
                if( iboun > 0 ) then
                   kboun = kboun + 1
                   coupling % geome % lboun_source(kboun)  = iboun
                   do inodb = 1,mnodb
                      coupling % geome % shapf(inodb,kboun) = shapf(cpart) % a(inodb,ii)
                   end do
                   do inodb = 1,lnnob_cou(iboun)
                      ipoin = lnodb_cou(inodb,iboun)
                      list_source_nodes(ipoin) = .true.
                   end do
                end if
             end do
          end do

       else if( coupling % itype == STRESS_PROJECTION .or. coupling % itype == PROJECTION ) then       
          !
          ! Projection
          !  
          coupling % geome % nboun_source = nenti          
          call memory_alloca(memor_cou,'SHAPF',            'par_test',coupling % geome % shapf,mnodb,coupling % geome % nboun_source)
          call memory_alloca(memor_cou,'LBOUN',            'par_test',coupling % geome % lboun_source,coupling % geome % nboun_source)
          call memory_alloca(memor_cou,'LIST_SOURCE_NODES','par_test',list_source_nodes,npoin)

          kboun = 0
          do cpart = 0,PAR_CURRENT_SIZE-1
             kk = 0
             do ii = 1,npoin_recv(cpart)
                iboun = decision_send(cpart) % l(ii)
                if( iboun > 0 ) then
                   kboun = kboun + 1
                   coupling % geome % lboun_source(kboun)  = iboun
                   do inodb = 1,mnodb
                      coupling % geome % shapf(inodb,kboun) = shapf(cpart) % a(inodb,ii)
                   end do
                   do inodb = 1,lnnob_cou(iboun)
                      ipoin = lnodb_cou(inodb,iboun)
                      list_source_nodes(ipoin) = .true.
                   end do
                   !write(*,'(i2,2(1x,e12.6),i5,i5)') kfl_paral,coord_recv(cpart) % a((ii-1)*2+1),coord_recv(cpart) % a((ii-1)*2+2),lninv_loc(lnodb(1:2,iboun))
                end if
             end do
          end do
       end if

       call memory_deallo(memor_cou,'MARK_NODE','par_test',mark_node)
       !
       ! Allocate sched_perm for scheduling permutation
       !
       call memory_alloca(memor_cou,'SCHED_PERM','par_test',coupling % geome % sched_perm,coupling % commd % lrecv_dim)
       !
       ! Count number of source nodes if not already done
       !
       if( associated(list_source_nodes) ) then
          coupling % geome % npoin_source = 0
          do ipoin = 1,npoin
             if( list_source_nodes(ipoin) ) coupling % geome % npoin_source = coupling % geome % npoin_source + 1
          end do
          if( associated(coupling % geome % lpoin_source) ) then
             call runend('POINTER ALREADY ASSOCIATED')
          else
             call memory_alloca(memor_cou,'LPOIN','par_test',coupling % geome % lpoin_source,coupling % geome % npoin_source)  
          end if
          kpoin = 0
          do ipoin = 1,npoin
             if( list_source_nodes(ipoin) ) then
                kpoin = kpoin + 1
                coupling % geome % lpoin_source(kpoin) = ipoin
             end if
          end do
       end if
       !
       ! Allocate memory for previous values
       !
       !call memory_alloca(memor_cou,'VALUES','par_test',coupling % values,coupling % geome % number_wet_points)
       !
       ! Check points that do not have a host partition
       ! If receive:        COUPLI % GEOME % LRECV_DIM values from other subdomains
       ! If need values on: COUPLI % GEOME % NUMBER_WET_POINTS values
       ! COUPLI % GEOME % STATUS(IPOIN) = JPOIN (from 1 to COUPLI % GEOME % LRECV_DIM)
       !
       ierro = 0
       ii    = 0
       do pp = 1,number_wet_points
          if( associated(my_point_to_part(pp) % l) ) then
             if( my_point_to_part(pp) % l(1) == 0 ) then
                ierro = 1
             else
                ii = ii + 1
             end if
          else
             ierro = 1
          end if
       end do

    end if

    if( kfl_timin == 1 ) call PAR_BARRIER('IN CURRENT COUPLING')
    call cputim(time4)
    ! call extrae_eventandcounters(600_4,0_8)
    ! call extrae_shutdown() 
    cputi_cou(5) = cputi_cou(5) + time4-time3

    !--------------------------------------------------------------------
    !
    ! Compute transmission arrays
    ! list_transmission_nodes_part(cpart) % l(:) (cpart=dom_i)
    !
    !--------------------------------------------------------------------

    if( INOTMASTER .and. coupling % commd % nneig > 0 ) then
       !
       ! Receive for each target point the list of nodes involved in the source
       !
       nneig = coupling % commd % nneig
       !
       ! Allocate some memory
       !
       call memory_alloca(memor_cou,'NUMBER_NODES_SEND','par_test',number_nodes_send,nneig)  
       call memory_alloca(memor_cou,'NUMBER_NODES_RECV','par_test',number_nodes_recv,nneig)  
       call memory_alloca(memor_cou,'LIST_NODES_SEND'  ,'par_test',list_nodes_send  ,nneig)  
       call memory_alloca(memor_cou,'LIST_NODES_RECV'  ,'par_test',list_nodes_recv  ,nneig)  
       call memory_alloca(memor_cou,'INVPR_NODES_SEND' ,'par_test',invpr_nodes_send ,nneig)  
       call memory_alloca(memor_cou,'INVPR_NODES_RECV' ,'par_test',invpr_nodes_recv ,nneig)  
       call memory_alloca(memor_cou,'MATRIX_NODES_SEND','par_test',matrix_nodes_send,nneig)  
       call memory_alloca(memor_cou,'MATRIX_NODES_RECV','par_test',matrix_nodes_recv,nneig)  
       call memory_alloca(memor_cou,'PERMU_NODES_SEND' ,'par_test',permu_nodes_send ,nneig)  
       call memory_alloca(memor_cou,'PERMU_NODES_RECV' ,'par_test',permu_nodes_recv ,nneig)  
       do ineig = 1,nneig
          nsend = coupling % commd % lsend_size(ineig+1) - coupling % commd % lsend_size(ineig)  ! Number target points
          nrecv = coupling % commd % lrecv_size(ineig+1) - coupling % commd % lrecv_size(ineig)  ! Number target points hosted
          if( nsend > 0 ) allocate( number_nodes_send(ineig) % l(nsend) )
          if( nrecv > 0 ) allocate( number_nodes_recv(ineig) % l(nrecv) )
       end do
       !
       ! Different interpolation schemes
       !
       if( coupling % itype == ELEMENT_INTERPOLATION .and. coupling % geome % nelem_source > 0 ) then
          !
          ! Element interpolation
          !
          kelem = 0
          do ineig = 1,nneig
             nsend = coupling % commd % lsend_size(ineig+1) - coupling % commd % lsend_size(ineig)
             kk    = 0
             do jelem = 1,nsend
                kelem = kelem + 1
                ielem = coupling % geome % lelem_source(kelem)
                pnode = lnnod(ielem)
                kk    = kk + pnode
                number_nodes_send(ineig) % l(jelem) = pnode
             end do
             if( kk > 0 ) allocate( list_nodes_send  (ineig) % l(kk) )
             if( kk > 0 ) allocate( matrix_nodes_send(ineig) % a(kk) )
          end do
          kelem = 0
          do ineig = 1,nneig
             nsend = coupling % commd % lsend_size(ineig+1) - coupling % commd % lsend_size(ineig)
             kk    = 0                
             do jelem = 1,nsend
                kelem = kelem + 1
                ielem = coupling % geome % lelem_source(kelem)
                pnode = lnnod(ielem)
                do inode = 1,pnode
                   ipoin = lnods(inode,ielem)
                   kk    = kk + 1
                   list_nodes_send(ineig)   % l(kk) = lninv_loc(ipoin) ! In global numbering to order them
                   matrix_nodes_send(ineig) % a(kk) = coupling % geome % shapf(inode,kelem)
                end do
             end do
          end do

       else if( ( coupling % itype == NEAREST_BOUNDARY_NODE .or. coupling % itype == NEAREST_ELEMENT_NODE ) .and. coupling % geome % npoin_source > 0 ) then
          !
          ! Nearest boundary/element node
          !
          kpoin = 0
          do ineig = 1,nneig
             nsend = coupling % commd % lsend_size(ineig+1) - coupling % commd % lsend_size(ineig)
             kk    = 0
             do jpoin = 1,nsend
                kpoin = kpoin + 1
                kk    = kk + 1
                ipoin = coupling % geome % lpoin_source(kpoin)
                number_nodes_send(ineig) % l(jpoin) = 1
             end do
             if( kk > 0 ) allocate( list_nodes_send  (ineig) % l(kk) )
             if( kk > 0 ) allocate( matrix_nodes_send(ineig) % a(kk) )
          end do
          kpoin = 0
          do ineig = 1,nneig
             nsend = coupling % commd % lsend_size(ineig+1) - coupling % commd % lsend_size(ineig)
             kk    = 0
             do jpoin = 1,nsend
                kpoin = kpoin + 1
                kk    = kk + 1
                ipoin = coupling % geome % lpoin_source(kpoin)
                list_nodes_send(ineig)   % l(kk) = lninv_loc(ipoin)
                matrix_nodes_send(ineig) % a(kk) = 1.0_rp
             end do
          end do

       else if( coupling % itype == PROJECTION .and. coupling % geome % nboun_source > 0 ) then
          !
          ! Projection
          !
          !        list_nodes_send(ineig) % l(:) = s1,s2 <= for each Gauss point we know how much we receive in number_nodes_send(ineig) % l(kgaub) = pnodb
          !
          !        s1                s2
          ! Source o-----x-----x-----o o-----x-----x-----o
          !              ||
          !              \/
          ! Target o-----x-----x-----o o-----x-----x-----o
          !            kgaub
          !
          kboun = 0
          kgaub = 0
          do ineig = 1,nneig
             nsend = coupling % commd % lsend_size(ineig+1)-coupling % commd % lsend_size(ineig)
             kk    = 0
             do jboun = 1,nsend
                kboun = kboun + 1
                kgaub = kgaub + 1
                iboun = coupling % geome % lboun_source(kboun)
                pnodb = lnnob_cou(iboun)
                kk    = kk + pnodb
                number_nodes_send(ineig) % l(jboun) = pnodb
             end do
             if( kk > 0 ) allocate( list_nodes_send  (ineig) % l(kk) )
             if( kk > 0 ) allocate( matrix_nodes_send(ineig) % a(kk) )
          end do
          kboun = 0
          kgaub = 0
          do ineig = 1,nneig
             nsend = coupling % commd % lsend_size(ineig+1)-coupling % commd % lsend_size(ineig)
             kk    = 0
             do jboun = 1,nsend
                kboun = kboun + 1
                kgaub = kgaub + 1
                iboun = coupling % geome % lboun_source(kboun)
                pnodb = lnnob_cou(iboun)
                do inodb = 1,pnodb
                   ipoin = lnodb_cou(inodb,iboun)                
                   kk    = kk + 1
                   list_nodes_send(ineig)   % l(kk) = lninv_loc(ipoin)
                   matrix_nodes_send(ineig) % a(kk) = coupling % geome % shapf(inodb,kboun)
                end do
             end do
          end do

       end if
       !
       ! Number of nodes and coefficients for each target point
       !
       do ineig = 1,nneig
          dom_i = coupling % commd % neights(ineig)
          call PAR_SEND_RECEIVE(number_nodes_send(ineig) % l,number_nodes_recv(ineig) % l,'IN CURRENT COUPLING',dom_i)
          nrecv = coupling % commd % lrecv_size(ineig+1) - coupling % commd % lrecv_size(ineig)
          kk = 0
          do ii = 1,nrecv
             kk = kk + number_nodes_recv(ineig) % l(ii)
          end do
          if( kk > 0 ) then
             allocate( list_nodes_recv  (ineig) % l(kk) )
             allocate( matrix_nodes_recv(ineig) % a(kk) )
          end if
          call PAR_SEND_RECEIVE(list_nodes_send  (ineig) % l,list_nodes_recv  (ineig) % l,'IN CURRENT COUPLING',dom_i)
          call PAR_SEND_RECEIVE(matrix_nodes_send(ineig) % a,matrix_nodes_recv(ineig) % a,'IN CURRENT COUPLING',dom_i)
       end do
       !
       ! Scatter arrays of the communicator
       ! LSCAT_PERM
       ! LSCAT_DIM
       !
       if( coupling % itype == STRESS_PROJECTION .or. coupling % itype == PROJECTION ) then

          if( coupling % commd % lrecv_dim > 0 ) then
             
!print*,'a=',kfl_paral
             npoin_wet = coupling % geome % npoin_wet 
             coupling % commd % lscat_dim = npoin_wet
             call memory_alloca(memor_cou,'LSCAT_PERM' ,'par_test',coupling % commd % lscat_perm,coupling % commd % lscat_dim)
             call memory_alloca(memor_cou,'MATRIX_IA'  ,'par_test',coupling % commd % matrix_ia, coupling % commd % lscat_dim+1_ip)
             do ipoin_wet = 1,coupling % geome % npoin_wet 
                ipoin = coupling % geome % lpoin_wet(ipoin_wet)
                coupling % commd % lscat_perm(ipoin_wet) = ipoin
             end do
             !
             ! We receive lgaub = 1,...,coupling % commd % lrecv_dim
             ! PERMU_LGAUB(1,lgaub) = jgaub
             ! PERMU_LGAUB(2,lgaub) = iboun_global
             ! PERMU_LGAUB(3,lgaub) = iboun_wet
             !
!print*,'b=',kfl_paral
             allocate( permu_lgaub(3,coupling % commd % lrecv_dim) )
             jgaub = 0
             do iboun_wet = 1,coupling % geome % nboun_wet
                iboun_global = coupling % geome % lboun_wet(iboun_wet)
                pblty        = abs(ltypb_cou(iboun_global))
                pnodb        = lnnob_cou(iboun_global)
                pgaub        = coupling % geome % proje_target(iboun_wet) % pgaub
                do igaub = 1,pgaub
                   jgaub = jgaub + 1
                   lgaub = coupling % geome % status(jgaub)
                   permu_lgaub(1,lgaub) = jgaub
                   permu_lgaub(2,lgaub) = iboun_global
                   permu_lgaub(3,lgaub) = iboun_wet
                end do 
             end do
             !
             ! Elaborate a list of unique dependencies in global numbering
             ! LIST_DIFFERENT_NODES(:) = IPOIN in global numbering
             !
             number_different_nodes = 0
             do ineig = 1,nneig
                do lgaub = coupling % commd % lrecv_size(ineig),coupling % commd % lrecv_size(ineig+1)-1 
                   do inodb_source = 1,number_nodes_recv(ineig) % l(lgaub)
                      iboun_global = permu_lgaub(2,lgaub)
                      pnodb        = lnnob_cou(iboun_global)
                      number_different_nodes = number_different_nodes + pnodb
                   end do
                end do
             end do

             call memory_alloca(memor_cou,'LISTE_DIFFERENT_NODES','par_test',list_different_nodes,number_different_nodes,'HUGE')

             number_different_nodes = 0
             do ineig = 1,nneig
                inode_source = 0
                do lgaub = coupling % commd % lrecv_size(ineig),coupling % commd % lrecv_size(ineig+1)-1 
                   do inodb_source = 1,number_nodes_recv(ineig) % l(lgaub)
                      inode_source = inode_source + 1
                      ipoin_source = list_nodes_recv(ineig) % l(inode_source)
                      kk           = 1 
                      do while( ipoin_source > list_different_nodes(kk) ) 
                         kk = kk + 1
                      end do
                      if( ipoin_source /= list_different_nodes(kk) ) then
                         do ll = number_different_nodes,kk,-1
                            list_different_nodes(ll+1) = list_different_nodes(ll)
                         end do
                         list_different_nodes(kk) = ipoin_source
                         number_different_nodes = number_different_nodes + 1
                      end if

                   end do
                end do
             end do
             !
             ! Count for each wet nodes the number of connections
             !
!             allocate( number_nodes_wet(npoin_wet) )

!             allocate( list_nodes_wet(npoin_wet) )

             call memgen(1_ip,npoin_wet,number_different_nodes)
             do ipoin_wet = 1,npoin_wet
                do kk = 1,number_different_nodes
                   givec(ipoin_wet,kk) = list_different_nodes(kk)
                end do
             end do

             coupling % commd % matrix_nzdom = 0
             do ineig = 1,nneig
                ii = 0
                do lgaub = coupling % commd % lrecv_size(ineig),coupling % commd % lrecv_size(ineig+1)-1 
                   do inodb_source = 1,number_nodes_recv(ineig) % l(lgaub)
                      ii           = ii + 1
                      ipoin_source = list_nodes_recv(ineig) % l(ii)   ! Node in original global numbering
                      xcoef        = matrix_nodes_recv(ineig) % a(ii) ! Coefficient
                      jgaub        = permu_lgaub(1,lgaub)
                      iboun_global = permu_lgaub(2,lgaub)
                      iboun_wet    = permu_lgaub(3,lgaub)
                      pnodb        = lnnob_cou(iboun_global)
                      do inodb_target = 1,pnodb
                         ipoin_wet = coupling % geome % proje_target(iboun_wet) % permu(inodb_target)
                         kk = 1
                         do while( givec(ipoin_wet,kk) /= ipoin_source .and. kk < number_different_nodes .and. kk /= 0 )
                            kk = kk + 1
                         end do
                         if( kk /= 0 ) then
                            if( givec(ipoin_wet,kk) == ipoin_source ) then
                               coupling % commd % matrix_ia(ipoin_wet) = coupling % commd % matrix_ia(ipoin_wet) + 1
                               coupling % commd % matrix_nzdom         = coupling % commd % matrix_nzdom + 1
                               givec(ipoin_wet,kk)                     = -ipoin_source
                            end if
                         end if
                      end do                      
                   end do
                end do
             end do
             !
             ! MATRIX_JA
             !
             call memory_alloca(memor_cou,'MATRIX_JA','par_test',coupling % commd % matrix_ja,coupling % commd % matrix_nzdom)
             call memory_alloca(memor_cou,'MATRIX_AA','par_test',coupling % commd % matrix_aa,coupling % commd % matrix_nzdom)
             do ipoin_wet = 1,npoin_wet
                do kk = 1,number_different_nodes
                   givec(ipoin_wet,kk) = list_different_nodes(kk)
                end do
             end do
             
             coupling % commd % matrix_nzdom = 0
             do ineig = 1,nneig
                ii = 0
                do lgaub = coupling % commd % lrecv_size(ineig),coupling % commd % lrecv_size(ineig+1)-1 
                   do inodb_source = 1,number_nodes_recv(ineig) % l(lgaub)
                      ii           = ii + 1
                      ipoin_source = list_nodes_recv(ineig) % l(ii)   ! Node in original global numbering
                      xcoef        = matrix_nodes_recv(ineig) % a(ii) ! Coefficient
                      jgaub        = permu_lgaub(1,lgaub)
                      iboun_global = permu_lgaub(2,lgaub)
                      iboun_wet    = permu_lgaub(3,lgaub)
                      pnodb        = lnnob_cou(iboun_global)
                      do inodb_target = 1,pnodb
                         ipoin_wet = coupling % geome % proje_target(iboun_wet) % permu(inodb_target)
                         kk = 1
                         do while( abs(givec(ipoin_wet,kk)) /= ipoin_source .and. kk < number_different_nodes .and. kk /= 0 )
                            kk = kk + 1
                         end do
                         if( kk /= 0 ) then
                            if( givec(ipoin_wet,kk) == ipoin_source ) then
                               coupling % commd % matrix_nzdom         = coupling % commd % matrix_nzdom + 1
                               coupling % commd % matrix_ja(coupling % commd % matrix_nzdom) = ipoin_source
                               givec(ipoin_wet,kk)                     = -abs(ipoin_source)
                            end if
                            
                         end if
                         
                      end do                      
                   end do
                end do
             end do
             !
             ! MATRIX_IA: from size to linked list
             !
             kk = coupling % commd % matrix_ia(1)
             coupling % commd % matrix_ia(1) = 1
             do ii = 1,coupling % commd % lscat_dim
                jj = coupling % commd % matrix_ia(ii+1)
                coupling % commd % matrix_ia(ii+1) = coupling % commd % matrix_ia(ii) + kk 
                kk = jj
             end do
!print*,'caca=',coupling % commd % matrix_ja(1:coupling % commd % matrix_nzdom)



!call runend('O.K.!')

!print*,'c=',kfl_paral
             ! 
             ! MATRIX_NZDOM: Matrix size
             !
             coupling % commd % matrix_nzdom = 0
             do ineig = 1,nneig
                !
                ! Gauss point lgaub => lgaub = status(jgaub)
                !
                ii = 0
                do lgaub = coupling % commd % lrecv_size(ineig),coupling % commd % lrecv_size(ineig+1)-1 
                   do inodb_source = 1,number_nodes_recv(ineig) % l(lgaub)
                      ii           = ii + 1
                      ipoin        = list_nodes_recv(ineig) % l(ii)   ! Node in original global numbering
                      xcoef        = matrix_nodes_recv(ineig) % a(ii) ! Coefficient
                      jgaub        = permu_lgaub(1,lgaub)
                      iboun_global = permu_lgaub(2,lgaub)
                      pnodb        = lnnob_cou(iboun_global)
                      do inodb_target = 1,pnodb
                         !ipoin_wet = coupling % geome % proje_target(iboun_wet) % permu(inodb_target)
                         ipoin     = lnodb(inodb_target,iboun_global)
                      end do                      
                   end do
                end do
             end do
             deallocate( permu_lgaub )
             ! 
             ! MATRIX_NZDOM: Matrix size
             !
             do ineig = 1,nneig
                do ii = 1,coupling % commd % lrecv_size(ineig+1)-coupling % commd % lrecv_size(ineig)
                   coupling % commd % matrix_nzdom = coupling % commd % matrix_nzdom + number_nodes_recv(ineig) % l(ii)
                end do
             end do
             
print*,'d=',kfl_paral
call runend('O.K.!')

             !allocate( caca(coupling % geome % npoin_wet,coupling % commd % lrecv_dim
             !coupling % commd % lscat_dim = coupling % geome % npoin_wet 
             !jgaub = 0
             !do iboun_wet = 1,coupling % geome % nboun_wet
             !   iboun_global = coupling % geome % lboun_wet(iboun_wet)
             !   pblty        = abs(ltypb_cou(iboun_global))
             !   pnodb        = lnnob_cou(iboun_global)
             !   pgaub        = coupling % geome % proje_target(iboun_wet) % pgaub
             !   do igaub = 1,pgaub
             !      jgaub = jgaub + 1
             !      lgaub = coupling % geome % status(jgaub) ! Permutation of wet gauss point
             !      gbsur = coupling % geome % proje_target(iboun_wet) % gbsur(igaub)
             !      do inodb = 1,pnodb
             !         ipoin_wet = coupling % geome % proje_target(iboun_wet) % permu(inodb)
             !         coupling % commd % lscat_perm(ipoin_wet) = coupling % geome % lpoin_wet(ipoin_wet)
             !         !print*,ipoin_wet
             !         !kgaub = global_to_local_gauss(1,jgaub)
             !         !ineig = global_to_local_gauss(2,jgaub)
             !         !xx_out(1:ndofn,ipoin_wet) = xx_out(1:ndofn,ipoin_wet) &
             !         !     & + gbsur * xx_recv_tmp(1:ndofn,lgaub) &
             !         !     & * coupling % geome % proje_target(iboun_wet) % shapb(inodb,igaub)
             !      end do
             !   end do
             !end do
             !!deallocate( global_to_local_gauss(coupling % commd % lrecv_dim) )

          end if

       else
          !
          ! Interpolation type coupling
          !
          coupling % commd % lscat_dim = coupling % geome % npoin_wet 
          call memory_alloca(memor_cou,'LSCAT_PERM' ,'par_test',coupling % commd % lscat_perm,coupling % commd % lscat_dim)
          call memory_alloca(memor_cou,'MATRIX_IA'  ,'par_test',coupling % commd % matrix_ia, coupling % commd % lscat_dim+1_ip)
          !
          ! ii    = local numbering of the target node (npoin_wet)
          ! ipoin = global number
          !
          do ineig = 1,nneig
             do ii = coupling % commd % lrecv_size(ineig),coupling % commd % lrecv_size(ineig+1)-1
                kk = 1
                do while( ii /= coupling % geome % status(kk) )
                   kk = kk + 1
                end do
                ipoin = coupling % geome % lpoin_wet(kk)
                coupling % commd % lscat_perm(ii) = ipoin
             end do
          end do
          ! 
          ! MATRIX_NZDOM: Matrix size
          !
          coupling % commd % matrix_nzdom = 0
          do ineig = 1,nneig
             do ii = 1,coupling % commd % lrecv_size(ineig+1)-coupling % commd % lrecv_size(ineig)
                coupling % commd % matrix_nzdom = coupling % commd % matrix_nzdom + number_nodes_recv(ineig) % l(ii)
             end do
          end do
       end if

call runend('O.K.!')
       call memory_alloca(memor_cou,'MATRIX_JA','par_test',coupling % commd % matrix_ja,coupling % commd % matrix_nzdom)
       call memory_alloca(memor_cou,'MATRIX_AA','par_test',coupling % commd % matrix_aa,coupling % commd % matrix_nzdom)
       !
       ! Construct arrays to be able to recompute communication arrays
       !
       izdom        = 0
       inode_target = 0
       inode_local  = 0

       do ineig = 1,nneig

          if( associated(list_nodes_send(ineig) % l) ) then
             nsend = size(list_nodes_send(ineig) % l)
             call memory_alloca(memor_cou,'PERMU_NODES_SEND','par_test',permu_nodes_send(ineig) % l,nsend)
             call memory_alloca(memor_cou,'INVPR_NODES_SEND','par_test',invpr_nodes_send(ineig) % l,nsend,'HUGE')
          end if
          if( associated(list_nodes_recv(ineig) % l) ) then
             nrecv = size(list_nodes_recv(ineig) % l)
             call memory_alloca(memor_cou,'PERMU_NODES_RECV' ,'par_test',permu_nodes_recv(ineig) % l,nrecv)
             call memory_alloca(memor_cou,'INVPR_NODES_RECV' ,'par_test',invpr_nodes_recv(ineig) % l,nrecv,'HUGE') 
          end if

          nsend = coupling % commd % lsend_size(ineig+1) - coupling % commd % lsend_size(ineig)
          nrecv = coupling % commd % lrecv_size(ineig+1) - coupling % commd % lrecv_size(ineig)
          !
          ! Renumber sending unknowns
          ! coupling % commd % lsend_size(ineig) ............. number of nodes to send to ineig after collapse
          ! permu_nodes_send(ineig) % l(jj) = kk ............. permutation, kk is the local
          ! list_nodes_send(ineig)  % l(jj) = ipoin_source ... List of nodes to send in global numbering 
          ! invpr_nodes_send(ineig) % l(kk) = ipoin_source ... in global numbering, kk after collapse
          !
          ! NSEND
          !
          ! /\    +---+ ->
          ! ||    |   | ->
          ! ||    +---+ ->  list_nodes_send(ineig)  = [node1,node2,....,number_nodes_send(ineig)] => 
          ! ||    |   | ->  permu_nodes_send(ineig) = [5,3,2,3,1,4,2...                        ]
          ! \/    +---+ ->  invpr_nodes_send(ineig) = [node1,node2,node3,node4,node5]
          !
          ! /\    +---+ ->
          ! ||    |   | ->
          ! ||    +---+ ->
          ! ||    |   | ->  list_nodes_send(ineig) = [node1,node2,...,number_nodes_send(ineig)]
          ! ||    +---+ ->
          ! ||    |   | ->
          ! \/    +---+ ->
          !          
          number_different_nodes = 0
          jj = 0
          do ii = 1,nsend 
             do inode_source = 1,number_nodes_send(ineig) % l(ii) ! Number of nodes connected to this target point
                jj           = jj + 1
                ipoin_source = list_nodes_send(ineig) % l(jj)
                kk           = 1
                do while( ipoin_source > invpr_nodes_send(ineig) % l(kk) ) 
                   kk = kk + 1
                end do
                if( ipoin_source /= invpr_nodes_send(ineig) % l(kk) ) then
                   do ll = number_different_nodes,kk,-1
                      invpr_nodes_send(ineig) % l(ll+1) = invpr_nodes_send(ineig) % l(ll)
                   end do
                   invpr_nodes_send(ineig) % l(kk) = ipoin_source
                   number_different_nodes = number_different_nodes + 1
                end if
             end do
          end do
          coupling % commd % lsend_size(ineig) = number_different_nodes ! How many different nodes should I send to INEIG       
          !
          ! Compute permutation
          !
          jj = 0
          do ii = 1,nsend 
             do inode_source = 1,number_nodes_send(ineig) % l(ii) ! Number of nodes connected to this target point
                jj           = jj + 1
                ipoin_source = list_nodes_send(ineig) % l(jj)
                kk = 1
                do while( invpr_nodes_send(ineig) % l(kk) /=  ipoin_source )
                   kk = kk + 1
                end do
                permu_nodes_send(ineig) % l(jj) = kk ! Local numbering after collapse
             end do
          end do
          !
          ! Renumber receving unknowns and save transmission matrix
          !
          number_different_nodes = 0
          jj = 0
          do ii = 1,nrecv                                         ! II = target node in INEIG
             inode_target = inode_target + 1                      ! INODE_TARGET = global numbering of target node (over all neighbors)
             do inode_source = 1,number_nodes_recv(ineig) % l(ii) ! Number of nodes connected to this target point
                jj           = jj + 1
                ipoin_source = list_nodes_recv(ineig) % l(jj)
                kk           = 1
                do while( ipoin_source > invpr_nodes_recv(ineig) % l(kk) ) 
                   kk = kk + 1
                end do
                if( ipoin_source /= invpr_nodes_recv(ineig) % l(kk) ) then
                   do ll = number_different_nodes,kk,-1
                      invpr_nodes_recv(ineig) % l(ll+1) = invpr_nodes_recv(ineig) % l(ll)
                   end do
                   invpr_nodes_recv(ineig) % l(kk) = ipoin_source
                   number_different_nodes = number_different_nodes + 1
                end if
             end do
             coupling % commd % matrix_ia(inode_target) = number_nodes_recv(ineig) % l(ii)
          end do
          coupling % commd % lrecv_size(ineig) = number_different_nodes ! How many different nodes should I receive from INEIG
          !
          ! Compute permutation
          !
          jj = 0
          do ii = 1,nrecv 
             do inode_source = 1,number_nodes_recv(ineig) % l(ii) ! Number of nodes connected to this target point
                jj           = jj + 1
                ipoin_source = list_nodes_recv(ineig) % l(jj)
                kk = 1
                do while( invpr_nodes_recv(ineig) % l(kk) /= ipoin_source )
                   kk = kk + 1
                end do
                permu_nodes_recv(ineig) % l(jj) = kk ! Local numbering after collapse
             end do
          end do
          !
          ! Fill in matrix
          !     
          if( coupling % itype == STRESS_PROJECTION .or. coupling % itype == PROJECTION ) then

          else
             jj = 0
             do ii = 1,nrecv                                         ! II = target node in INEIG
                do inode_source = 1,number_nodes_recv(ineig) % l(ii) ! Number of nodes connected to this target point
                   jj                                  = jj + 1
                   izdom                               = izdom + 1
                   ipoin_source                        = list_nodes_recv(ineig)   % l(jj)
                   kpoin                               = permu_nodes_recv(ineig)  % l(jj)
                   coupling % commd % matrix_aa(izdom) = matrix_nodes_recv(ineig) % a(jj)                
                   coupling % commd % matrix_ja(izdom) = kpoin + inode_local 
                end do
             end do
             inode_local = inode_local + number_different_nodes
          end if

       end do
       !
       ! MATRIX_IA: from size to linked list
       !
       kk = coupling % commd % matrix_ia(1)
       coupling % commd % matrix_ia(1) = 1
       do ii = 1,coupling % commd % lscat_dim
          jj = coupling % commd % matrix_ia(ii+1)
          coupling % commd % matrix_ia(ii+1) = coupling % commd % matrix_ia(ii) + kk 
          kk = jj
       end do
       !
       ! Recompute sending arrays
       !
       kk = coupling % commd % lsend_size(1)
       coupling % commd % lsend_size(1) = 1
       do ineig = 1,nneig
          jj = coupling % commd % lsend_size(ineig+1)
          coupling % commd % lsend_size(ineig+1) = coupling % commd % lsend_size(ineig) + kk 
          kk = jj
       end do
       coupling % commd % lsend_dim = coupling % commd % lsend_size(nneig+1) - 1
       call memory_alloca(memor_cou,'LSEND_PERM','par_test',coupling % commd % lsend_perm,coupling % commd % lsend_dim)  
       kk = 0
       do ineig = 1,nneig
          do ii = 1,coupling % commd % lsend_size(ineig+1)-coupling % commd % lsend_size(ineig)
             kk = kk + 1
             ipoin_global = invpr_nodes_send(ineig) % l(ii)
             ipoin_local  = PAR_GLOBAL_TO_LOCAL_NODE(ipoin_global)
             coupling % commd % lsend_perm(kk) = ipoin_local
          end do
       end do
       !
       ! Recompute receving arrays
       !
       kk = coupling % commd % lrecv_size(1)
       coupling % commd % lrecv_size(1) = 1
       do ineig = 1,nneig
          jj = coupling % commd % lrecv_size(ineig+1)
          coupling % commd % lrecv_size(ineig+1) = coupling % commd % lrecv_size(ineig) + kk 
          kk = jj
       end do
       coupling % commd % lrecv_dim = coupling % commd % lrecv_size(nneig+1) - 1
       call memory_alloca(memor_cou,'LRECV_PERM','par_test',coupling % commd % lrecv_perm,coupling % commd % lrecv_dim)  
       kk = 0
       do ineig = 1,nneig
          do ii = 1,coupling % commd % lrecv_size(ineig+1)-coupling % commd % lrecv_size(ineig)
             kk = kk + 1
             ipoin_global = invpr_nodes_recv(ineig) % l(ii)
             ipoin_local  = PAR_GLOBAL_TO_LOCAL_NODE(ipoin_global)
             coupling % commd % lrecv_perm(kk) = ipoin_local
          end do
       end do

      ! if( igene == 2 ) then
      !    allocate( rr(npoin) )
      !    if( I_AM_IN_COLOR(color_target) .and. I_AM_IN_COLOR(color_source) ) then
      !       print*,'MAAAAAAAAAAAAAAAAAAARDE'
      !    end if
      !    if( I_AM_IN_COLOR(color_target) ) then
      !       rr = 0.0_rp
      !    else
      !       do ipoin = 1,npoin
      !          rr(ipoin) = coord(1,ipoin) + coord(2,ipoin)
      !       end do
      !    end if
      !    call PAR_COUPLING_NODE_EXCHANGE(rr,'MATRIX',coupling % commd)
      !    if( I_AM_IN_COLOR(color_target) ) then
      !       do kpoin = 1,coupling % geome % npoin_wet
      !          ipoin = coupling % geome % lpoin_wet(kpoin) 
      !          print*,'result=',kfl_paral,lninv_loc(ipoin),rr(ipoin)-(coord(2,ipoin)+coord(1,ipoin))
      !       end do
      !    end if
      ! end if
       !
       ! Deallocate
       !
       call memory_deallo(memor_cou,'NUMBER_NODES_SEND','par_test',number_nodes_send)  
       call memory_deallo(memor_cou,'NUMBER_NODES_RECV','par_test',number_nodes_recv)  
       call memory_deallo(memor_cou,'LIST_NODES_SEND'  ,'par_test',list_nodes_send  )  
       call memory_deallo(memor_cou,'LIST_NODES_RECV'  ,'par_test',list_nodes_recv  )  
       call memory_deallo(memor_cou,'INVPR_NODES_SEND' ,'par_test',invpr_nodes_send )  
       call memory_deallo(memor_cou,'INVPR_NODES_RECV' ,'par_test',invpr_nodes_recv )  
       call memory_deallo(memor_cou,'MATRIX_NODES_SEND','par_test',matrix_nodes_send)  
       call memory_deallo(memor_cou,'MATRIX_NODES_RECV','par_test',matrix_nodes_recv)  
       call memory_deallo(memor_cou,'PERMU_NODES_SEND' ,'par_test',permu_nodes_send )  
       call memory_deallo(memor_cou,'PERMU_NODES_RECV' ,'par_test',permu_nodes_recv )  

    end if
100 continue
    if(igene==1)call runend('O.K.!')
    !
    ! Deallocate memory
    !
    call memory_deallo(memor_cou,'LIST_NEIGHBORS'   ,'par_test',list_neighbors)
    call memory_deallo(memor_cou,'LIST_SOURCE_NODES','par_test',list_source_nodes)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',npoin_send)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',npoin_recv)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',mask_npoin)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',decision_send)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',decision_recv)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',coord_send)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',coord_recv)   
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',my_part_to_point)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',my_point_to_part)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',shapf)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',distance_send)
    call memory_deallo(memor_cou,'NPOIN_SEND'       ,'par_test',distance_recv)
    call memory_deallo(memor_cou,'LESOU'            ,'par_test',lesou)
    call memory_deallo(memor_cou,'LNSOU'            ,'par_test',lnsou)
    call memory_deallo(memor_cou,'INTERSECTION'     ,'mod_couplings',intersection)
    call memory_deallo(memor_cou,'PAR_WORLD_RANKS'  ,'mod_couplings',PAR_WORLD_RANKS)


    !
    ! Recover communicator
    !
    PAR_COMM_CURRENT = PAR_COMM_SAVE


    !if( PAR_MY_WORLD_RANK == 0 ) then
    !   do ii=1,nx;do jj=1,ny ; do kk=1,nz
    !      if( bin_size(ii,jj,kk) > 0 ) then
    !         do iboxe = 1,bin_size(ii,jj,kk)
    !            ipart = par_bin_part(ii,jj,kk) % l(iboxe)
    !            icode = PAR_COMM_WORLD_TO_CODE_PERM(1,ipart)
    !            ipart = PAR_COMM_WORLD_TO_CODE_PERM(2,ipart)
    !         end do
    !      end if
    !   end do; end do; end do
    !end if


  end subroutine COU_TRANSMISSION_ARRAYS




end module mod_couplings
!> @} 
!-----------------------------------------------------------------------
