subroutine tem_bouope()
  !------------------------------------------------------------------------
  !****f* Temper/tem_bouope
  ! NAME 
  !    tem_bouope
  ! DESCRIPTION
  !    ORDER=1:
  !      Temperature equation, boundary operations
  ! USES
  ! USED BY
  !    tem_matrix 
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use mod_ker_proper 
  use def_domain
  use def_temper
  use mod_ADR, only : ADR_add_sgs_or_bubble
  implicit none
  real(rp)    :: elmat(mnode,mnode),elrhs(mnode)
  real(rp)    :: baloc(ndime,ndime)
  real(rp)    :: elvel(ndime,mnode),gptem(mgaus)
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: bocod(ndime,mnodb),botem(mnodb)
  real(rp)    :: bovel(ndime,mnodb)
  real(rp)    :: bogrc(ndime,mnodb)
  integer(ip) :: ielem,inode,ipoin,kfl_gobou
  integer(ip) :: igaus,igaub,iboun,inodb,pblty,idime
  integer(ip) :: pnodb,pmate,pnode,pelty,pgaus
  integer(ip) :: dummi
  real(rp)    :: eucta,tmatr,gbsur,gpdet,adotn
  real(rp)    :: gbsph,gbden,gbcon,gbtem,gbvel(3)
  real(rp)    :: gpsph(mgaus),gpden(mgaus),gpdif(mgaus)
  real(rp)    :: gprea(mgaus)
  real(rp)    :: gptur(mgaus)                                 ! Turbulent viscosity
  real(rp)    :: arobi,trobi,qrobi,twall,acvis
  real(rp)    :: para1,para2,para3,para4
  real(rp)    :: xmrhs,xmmat
  real(rp)    :: eledd(mnode),gpcar(ndime,mnode,mgaus)
  real(rp)    :: gpcon(mgaus),dummr(ndime*mnode),gpcod, gpvis(mgaus)
  real(rp)    :: xjaci(9),xjacm(9)
  !
  ! Loop over elements  
  !
!  call tem_set_heat_flux( bvnat_tem(3,1:nboun,1) )
  !
  boundaries: do iboun=1,nboun

     kfl_gobou = 1

     Neumann_or_Robin: if( (                &
          &  kfl_fixbo_tem(iboun) == 2 .or. &
          &  kfl_fixbo_tem(iboun) == 3 .or. &
          &  kfl_fixbo_tem(iboun) == 4 .or. &
          &  kfl_fixbo_tem(iboun) == 5 .or. &
          &  bemol_tem /= 0.0_rp )  .and. kfl_gobou == 1 ) then

        pblty = ltypb(iboun)
        pnodb = nnode(pblty)
        ielem = lboel(pnodb+1,iboun)
        pelty = ltype(ielem)

        if( pelty > 0 ) then

           pnode = nnode(pelty)
           pgaus = ngaus(pelty)
           pmate = 1
           if( nmate > 1 ) pmate = lmate(ielem)
           !
           ! Inititalize
           !
           elmat = 0.0_rp
           elrhs = 0.0_rp
           !
           ! Gather operations
           !
           bocod(1:ndime,1:pnodb) = coord(1:ndime,lnodb(1:pnodb,iboun))
           botem(1:pnodb)         = therm(lnodb(1:pnodb,iboun),1)
           elcod(1:ndime,1:pnode) = coord(1:ndime,lnods(1:pnode,ielem))
           if( kfl_fixbo_tem(iboun) == 5 ) then
              bogrc(1:ndime,1:pnodb) = gradc_tem(1:ndime,lnodb(1:pnodb,iboun))
           end if
           !
           ! GPTEM+GPSGS: Temperature at Gauss point
           !
           call gather(&
                1_ip,pgaus,pnode,1_ip,lnods(1,ielem),&
                elmar(pelty)%shape,therm,gptem)
           call ADR_add_sgs_or_bubble(&
                ielem,pgaus,elmar(pelty) % shape_bub,ADR_tem,gptem) 
           !
           ! Cartesian derivatives
           !
           do igaus=1,pgaus
              call elmder(&
                   pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&      ! Cartesian derivative
                   elcod,gpcar(1,1,igaus),gpdet,xjacm,xjaci)        ! and Jacobian
           end do
           !
           ! Properties 
           !
           
           call ker_proper('DENSI','PGAUS',dummi,ielem,gpden,pnode,pgaus,elmar(pelty)%shape,gpcar)
           call ker_proper('CONDU','PGAUS',dummi,ielem,gpcon,pnode,pgaus,elmar(pelty)%shape,gpcar)
           call ker_proper('SPHEA','PGAUS',dummi,ielem,gpsph,pnode,pgaus,elmar(pelty)%shape,gpcar)
           call ker_proper('TURBU','PGAUS',dummi,ielem,gptur,pnode,pgaus,elmar(pelty) % shape,gpcar) 

           if (kfl_fixbo_tem(iboun) == 3) &
                call ker_proper('VISCO','PGAUS',dummi,ielem,gpvis,pnode,pgaus,elmar(pelty)%shape,gpcar)
           ! 
           ! Coupling with turbul
           !
           call tem_turbul(&
                ielem,pnode,pgaus,1_ip,pgaus,eledd,dummr,elmar(pelty)%shape,gpcar,gpcon,gpsph, &
                gpdif,dummr,gpden,gptur) 
           !
           ! Reaction term
           !
           call tem_elmrea( &
                1_ip,pnode,pgaus,1_ip,pgaus,elvel,gpden,gpcar,&
                gprea)

           !
           ! Loop over Gauss points
           !
           gauss_points: do igaub=1,ngaus(pblty)
              !
              ! Jacobian EUCTA
              !
              call bouder(&
                   pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&
                   bocod,baloc,eucta)
              gbsur = elmar(pblty)%weigp(igaub)*eucta 
              call chenor(pnode,baloc,bocod,elcod)                      ! Check normal
              !
              ! Cylindrical coordinates
              !
              if( kfl_naxis == 1 ) then
                 gpcod = 0.0_rp
                 do inodb = 1,pnodb
                    gpcod = gpcod + bocod(1,inodb) * elmar(pblty) % shape(inodb,igaub)
                 end do
                 gbsur = gbsur * gpcod * twopi
              end if
              !
              ! Robin: a.n
              !
              adotn = 0.0_rp
              if( kfl_advec_tem /= 0 .and. bemol_tem /= 0.0_rp ) then
                 if( kfl_advec_tem == 1 ) then
                    do inode=1,pnodb
                       ipoin=lnodb(inodb,iboun)
                       do idime=1,ndime
                          bovel(idime,inodb)=veloc(idime,ipoin,1)
                       end do
                    end do
                 else if(kfl_advec_tem>=2) then
                    call tem_velfun(pnodb,bocod,bovel)
                 end if
                 do idime=1,ndime
                    gbvel(idime)=0.0_rp
                 end do
                 do inodb=1,pnodb
                    do idime=1,ndime
                       gbvel(idime)=gbvel(idime)&
                            +bovel(idime,inodb)*elmar(pblty)%shape(inodb,igaub)
                    end do
                 end do
                 do idime=1,ndime   
                    adotn = adotn + baloc(idime,ndime)*gbvel(idime)
                 end do
              end if

              qrobi = 0.0_rp
              arobi = 0.0_rp
              trobi = 0.0_rp 

              if( kfl_fixbo_tem(iboun) == 2 ) then
                 !
                 ! Robin condition
                 ! k*grad(T).n = qr+ar*(T-Tr)
                 !
                 arobi = bvnat_tem(1,iboun,1)                        ! ar
                 trobi = bvnat_tem(2,iboun,1)                        ! Tr   
                 qrobi = bvnat_tem(3,iboun,1)                        ! qr    

              else if( kfl_fixbo_tem(iboun) == 4 ) then
                 !
                 ! Augmented Robin condition
                 ! k*grad(T).n = P3+P1*T+P2*(exp(T/P4)-1.0)
                 !
                 para1 = bvnat_tem(1,iboun,1)                        ! P1
                 para2 = bvnat_tem(2,iboun,1)                        ! P2   
                 para3 = bvnat_tem(3,iboun,1)                        ! P3    
                 para4 = bvnat_tem(4,iboun,1)                        ! P4 
                 gbtem = 0.0_rp
                 do inodb = 1,pnodb                  
                    gbtem = gbtem + botem(inodb) *elmar(pblty) % shape(inodb,igaub)
                 end do
                 qrobi = -para3-para2*(exp(gbtem/para4)-1.0_rp)
                 arobi = -para1
                 trobi = 0.0_rp 

              else if( kfl_fixbo_tem(iboun) == 3 ) then
                 !
                 ! Law of the wall
                 !              
                 ! k*grad(T).n = -(rho*cp*u/T+)*(T-Tw)
                 !                  
                 call runend("TEMPER: Law of the wall is outdated.")
                 twall = bvnat_tem(1,iboun,1)                          ! Twall
                 gbden = 0.0_rp
                 gbsph = 0.0_rp
                 gbcon = 0.0_rp
                 acvis = 0.0_rp
               

                 do igaus = 1,pgaus
                    do inodb = 1,pnodb                  
                       tmatr =  elmar(pelty) % shaga(igaus,lboel(inodb,iboun)) &
                            &  *elmar(pblty) % shape(inodb,igaub)
                       gbden = gbden + gpden(igaus) * tmatr
                       gbsph = gbsph + gpsph(igaus) * tmatr
                       gbcon = gbcon + gpdif(igaus) * tmatr
                       acvis = acvis + gpvis(igaus) * tmatr
                    end do
                 end do
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    elvel(1:ndime,inode) = veloc(1:ndime,ipoin,1)
                 end do
                 if( kfl_delta == 1 ) then
                    call tem_bouwal(&
                         lboel(1,iboun),elmar(pblty)%shape(1,igaub),pnodb,&
                         pnode,ndime,elvel,ywalb(iboun),gbden,acvis,&
                         gbsph,gbcon,prtur_tem,twall,arobi,trobi,qrobi)
                 else
                    call tem_bouwal(&
                         lboel(1,iboun),elmar(pblty)%shape(1,igaub),pnodb,&
                         pnode,ndime,elvel,delta_tem,gbden,acvis,&
                         gbsph,gbcon,prtur_tem,twall,arobi,trobi,qrobi)
                 end if

              else if( kfl_fixbo_tem(iboun) == 5 ) then
                 !
                 ! Robin condition for water vapor model
                 ! 
                 gbtem = 0.0_rp
                 do inodb = 1,pnodb
                    gbtem = gbtem + botem(inodb) * elmar(pblty) % shape(inodb,igaub)
                 end do
                 call tem_watvap(&
                      pnodb,ndime,baloc(1,ndime),bogrc,&
                      elmar(pblty) % shape(1,igaub),&
                      gbtem,qrobi,arobi,trobi)

              end if

              xmrhs =  qrobi - arobi*trobi                         !  qr - ar * Tr
              xmmat =- arobi + bemol_tem*adotn                     ! -ar + bemol(u.n)

              call tem_boumat(&
                   pnode,pnodb,lboel(1,iboun),xmmat,xmrhs,&
                   elmar(pblty)%shape(1,igaub),gbsur,elmat,elrhs)

           end do gauss_points
           !
           ! Prescribe Dirichlet boundary conditions
           !
           call tem_elmdir(&
                pnode,lnods(1,ielem),elmat,elrhs,ielem)
           !
           ! Assembly
           !
           call assrhs(solve(1)%ndofn,pnode,lnods(1,ielem),elrhs,rhsid)
           call assmat(&
                solve(1)%ndofn,pnode,pnode,npoin,solve(1)%kfl_algso,&
                ielem,lnods(1,ielem),elmat,amatr)

        end if

     end if Neumann_or_Robin

  end do boundaries


contains

  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  subroutine tem_set_heat_flux( param )
  use def_domain,           only: npoin, nboun
  !-----------------------------------------------------------------------||---!
  !@author  JM Zavala-Ake
  !
  ! PHYSICAL_PROBLEM
  !   PROBLEM_DEFINITION
  !     HEAT_FLUX:              FIELD=1, CODBO=2
  !   END_PROBLEM_DEFINITION
  !   ...
  !
  !-----------------------------------------------------------------------||---!
  implicit none
  real(rp), intent(out) :: param(nboun) 
  integer(ip) :: i_boun
  logical(ip) :: is_robin, is_code 

  if( kfl_flux_tem > 0 ) then
    do i_boun  = 1,nboun
      is_robin = kfl_fixbo_tem(i_boun)==2 
      is_code  = kfl_codbo(i_boun)==kfl_code_tem 
      if( is_robin.and.is_code ) then
        param(i_boun) = xfiel( kfl_flux_tem ) % a(1,i_boun)
!       print *, i_boun, param(i_boun)   
      endif 
    end do
  endif  
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  end subroutine
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!

end subroutine tem_bouope
