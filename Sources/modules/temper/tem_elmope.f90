subroutine tem_elmope(order)
  !------------------------------------------------------------------------
  !****f* Temper/tem_elmop2
  ! NAME 
  !    tem_elmop2
  ! DESCRIPTION
  !    ORDER=1:
  !      Temperature equation, elemental operations:
  !      1. Compute elemental matrix and RHS 
  !      2. Impose Dirichlet boundary conditions
  !      3. Assemble them
  !    ORDER=4:
  !      Update the subgrid scale
  ! USES
  ! USED BY
  !    tem_matrix
  !------------------------------------------------------------------------
  use def_parame
  use def_elmtyp
  use def_master
  use def_domain
  use def_kermod
  use mod_ker_proper 
  use def_temper
  implicit none

  integer(ip), intent(in) :: order                     ! =2: compute SGS only

  real(rp)    :: elmat(mnode,mnode),elrhs(mnode)
  integer(ip) :: ielem,igaus,idime                     ! Indices and dimensions
  integer(ip) :: pelty,pmate,pnode,kelem
  integer(ip) :: pgaus,plapl,porde,ptopo
  integer(ip) :: kfl_advec_old

  real(rp)    :: eltem(mnode,ncomp_tem)                ! Gather 
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: elvel(ndime,mnode)
  real(rp)    :: eledd(mnode)
  real(rp)    :: elDik(mnode)
  real(rp)    :: elbub(ncomp_tem)

  real(rp)    :: tragl(ndime,ndime),chave(ndime,2)     ! Stabilization
  real(rp)    :: chale(2),hleng(ndime)

  real(rp)    :: gpdtc                                 ! Values at Gauss points
  real(rp)    :: gpvol(mgaus)                          ! |J|*w
  real(rp)    :: gprea(mgaus,4),gpvel(ndime,mgaus)     ! r, a
  real(rp)    :: gpcon(mgaus),gpcod(ndime,mgaus)       ! k
  real(rp)    :: gpdif(mgaus),gpgrd(ndime,mgaus)       ! k+kt, grad(k+kt)
  real(rp)    :: gppr1(mgaus)                          ! Weighted residual L2-projection
  real(rp)    :: gppr2(mgaus)                          ! Weighted residual L2-projection
  real(rp)    :: gpdiv(mgaus)                          ! Divergence of convection
  real(rp)    :: gprhs(mgaus)                          ! f (all terms)
  real(rp)    :: gpden(mgaus)                          ! rho and then rho*cp
  real(rp)    :: gpsph(mgaus)                          ! cp
  real(rp)    :: gptem(mgaus,ncomp_tem)                ! T
  real(rp)    :: gpsou(mgaus)                          ! Q
  real(rp)    :: gpgrt(ndime,mgaus)                    ! grad(T)
  real(rp)    :: gpsgs(mgaus,3)                        ! T'
  real(rp)    :: gpsgv(ndime,mgaus)                    ! u'
  real(rp)    :: gpsha(mnode,mgaus)                    ! N
  real(rp)    :: gpcar(ndime,mnode,mgaus)              ! dNk/dxj
  real(rp)    :: gphes(ntens,mnode,mgaus)              ! dNk/dxidxj
  real(rp)    :: gplap(mnode,mgaus)                    ! Laplacian
  real(rp)    :: gpsta(mgaus)                          ! tau
  real(rp)    :: gptur(mgaus)                          ! Turbulent viscosity
  real(rp)    :: gpres(mgaus)                          ! Residual
  integer(ip) :: dummi
  real(rp)    :: dummr
  real(rp)    :: dtmin, dtcri

  integer(ip) :: inode
  real(rp)    :: elcod_bub(ndime)                      ! x
  real(rp)    :: gpvol_bub(mgaus)                      ! V
  real(rp)    :: gpsha_bub(mgaus)                      ! Ne
  real(rp)    :: gpcar_bub(ndime,mgaus)                ! dNek/dxj
  real(rp)    :: gphes_bub(ntens,mgaus)                ! dNek/dxidxj

#ifdef EVENT
  call mpitrace_user_function(1)
#endif
  !
  ! Initialization
  !
  gpdif = 0.0_rp
  gpsph = 0.0_rp
  gpden = 0.0_rp
  gprea = 0.0_rp
  gpsta = 0.0_rp
  gppr1 = 0.0_rp
  gpdiv = 0.0_rp
  gprhs = 0.0_rp
  gptur = 0.0_rp
  gptem = 0.0_rp
  gpgrd = 0.0_rp
  gpvel = 0.0_rp
  elbub = 0.0_rp
  !
  ! Loop over elements
  !  
  dtmin = 1.0e6_rp
  elements: do kelem = 1,nelez(lzone(ID_TEMPER))
     ielem = lelez(lzone(ID_TEMPER)) % l(kelem)
     !
     ! Element dimensions
     !
     pelty = ltype(ielem)

     if( pelty > 0 ) then
        pnode = lnnod(ielem)
        pgaus = ngaus(pelty)
        plapl = llapl(pelty)
        porde = lorde(pelty)
        ptopo = ltopo(pelty)
        !
        ! Check if element is a solid
        !
        pmate = 1
        kfl_advec_old = kfl_advec_tem
        if( nmate > 1 ) then
           pmate = lmate(ielem)
           if( pmate > nmatf ) kfl_advec_tem = 0 ! Solid material: no advection
        end if
        !
        ! Subgrid scale. Temperature:to GPSGS 
        !
        !call tem_sgsope(&
        !     1_ip,ielem,pgaus,gpsgs,dummr)
        !
        ! Gather operations 
        !
        call tem_elmgat(&
             ielem,pnode,lnods(1,ielem),eltem,elvel,elcod,eledd,elDik,elbub)
        !
        ! hleng and tragl at center of gravity
        !
        call elmlen(ndime,pnode,elmar(pelty) % dercg,tragl,elcod,&
             hnatu(pelty),hleng)
        !
        ! Compute the characteristic length CHALE
        !
        if( kfl_ellen_tem == -1 ) then 
           call tem_elmchl(&
                ielem,pelty,pnode,plapl,pmate,lnods(1,ielem),elcod,eltem,&
                elvel,gpcar,gphes,chale)
        else
           call elmchl(tragl,hleng,elcod,elvel,chave,chale,pnode,&
                porde,hnatu(pelty),kfl_advec_tem,kfl_ellen_tem)
        end if
        !
        ! Local time step DTINV_TEM
        !
        if(kfl_timco==2) then 
           call tem_elmtss(&
                ielem,pelty,pnode,pmate,elcod,elvel,eledd,&
                eltem,gpcar,chale,gpdtc)        
           dtinv_tem = 1.0_rp/(gpdtc*safet_tem)
           if( kfl_stead_tem == 1 ) dtinv_tem = 0.0_rp
           if( kfl_timei_tem == 0 ) dtinv_tem = 0.0_rp
        end if
        !
        ! Cartesian derivatives, Hessian matrix and volume: GPCAR, GPHES, PGVOL
        !
        if( plapl == 0 ) gphes = 0.0_rp
        call elmca2(&
             pnode,pgaus,plapl,elmar(pelty) % weigp,elmar(pelty) % shape,&
             elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpsha,&
             gpcar,gphes,ielem)
        !
        ! Temperature: GPTEM
        !
        call gather(&
             2_ip,pgaus,pnode,1_ip,dummi,gpsha,eltem,gptem)   
        !
        ! Properties: GPDEN, GPDIF, GPGRD and GPREA
        !
        call ker_proper('DENSI','PGAUS',dummi,ielem,gpden,pnode,pgaus,gpsha,gpcar)
        call ker_proper('CONDU','PGAUS',dummi,ielem,gpcon,pnode,pgaus,gpsha,gpcar)
        call ker_proper('SPHEA','PGAUS',dummi,ielem,gpsph,pnode,pgaus,gpsha,gpcar)
        call ker_proper('GRCON','PGAUS',dummi,ielem,gpgrd,pnode,pgaus,gpsha,gpcar)
        call ker_proper('TURBU','PGAUS',dummi,ielem,gptur,pnode,pgaus,gpsha,gpcar)
        ! 
        ! Coupling with turbul
        !
        call tem_turbul(&
             ielem,pnode,pgaus,1_ip,pgaus,eledd,elDik,gpsha,gpcar,gpcon,gpsph, &
             gpdif,gpgrd,gpden,gptur) 
        !
        ! Reaction term
        !
        call tem_elmrea( &
             1_ip,pnode,pgaus,1_ip,pgaus,elvel,gpden,gpcar,&
             gprea)
        !
        ! Equation coefficients in GP
        !
        call tem_elmpre(&  
             pnode,pgaus,pmate,gpden,gpsph,gpdif,&
             gpsgv,gpsha,gpcar,gphes,elvel,eltem,&
             elcod,eledd,gpvel,gptem,gprhs,gpcod,&
             gpgrt,lnods(1,ielem),ielem)
        !
        ! Exact solution: GPRHS
        !
        call tem_elmexa(&
             pgaus,gpcod,gpden,gpdif,gprea,gpgrd,&
             gpvel,gpsou,gprhs)
        !
        ! Coupling with RADIAT
        !   
        call tem_radiat(&
             ielem,pnode,pgaus,lnods(1,ielem),gpsha,gprhs)
        !
        ! Coupling with CHEMIC
        !
        call tem_chemic(&
             ielem, pgaus, gprhs )      
        !
        ! Reset check
        !
        if( kfl_reset == 0 ) then
           call tem_adr_critical_time(pgaus,chale,gpvel,gpdif,gprea,gprhs,gpden,gpsph,gptem,dtcri)
           if (dtcri /= 0.0 ) then
              dtmin= min(dtmin,dtcri)
           endif
        endif
        ! 
        ! End reset check
        !
        if( kfl_regim_tem == 4 ) then ! for the moment we use elmadr for the enthalpy equation
           call elmadr(& 
                order,ielem,pnode,pgaus,ptopo,plapl,pelty,1_ip,1_ip,lnods(1,ielem),kfl_shock_tem,&
                kfl_taust_tem,kfl_ortho_tem,0_ip,kfl_sgsti_tem,kfl_limit_tem,staco_tem,gpdif,&
                gprea,gpden,gpvel,gppr1,gpvol,gpgrd,gprhs,gpsha,gpcar,gphes,&
                eltem,elcod,chale,gpsta,dtinv_tem,shock_tem,bemol_tem,gpdiv,gplap,gpsgs,&
                elmat,elrhs)
        else
           call runend('TEM_ELMOPE: OBSOLETE')
           !call tem_elmmul(&
           !     order     ,kfl_ortho_tem,kfl_shock_tem,shock_tem,kfl_taust_tem,kfl_sgsti_tem, &
           !     staco_tem ,pnode     ,pgaus ,gpvel, gpdif,gprea       ,&
           !     gptem     ,gpgrd     ,gprhs ,gpden, gpsha,gpcar     ,gpvol,&
           !     elmat     ,elrhs     ,chale ,gphes, eltem,dtinv_tem ,pabdf_tem, pabds_tem, &
           !     nbdfp_tem ,nbdfs_tem ,plapl ,gpsgs, kfl_sgsli_tem, &
           !     misgs_tem ,kfl_tibub_tem, tosgs_tem ,gpsph, resgs_tem(1), itsgm_tem, gppr1, gpres, ielem,&
           !     gpsha_bub ,gpcar_bub ,elbub,eltem)
        end if

        if( order == 1 ) then
           !
           ! Prescribe Dirichlet boundary conditions
           !
           if( solve(1) % kfl_iffix == 0 ) &
                call tem_elmdir(&
                pnode,lnods(1,ielem),elmat,elrhs,ielem)
           if( solve_sol(1)  %  kfl_algso == 9 ) then
              call tem_assdia(&
                   pnode,elmat,eltem,elrhs)
              call assrhs(&
                   solve(1) % ndofn,pnode,lnods(1,ielem),elrhs,rhsid)      
           else
              !
              ! Assembly
              !
              call assrhs(&
                   solve(1) % ndofn,pnode,lnods(1,ielem),elrhs,rhsid)
              call assmat(&
                   solve(1) % ndofn,pnode,pnode,npoin,solve(1) % kfl_algso,&
                   ielem,lnods(1,ielem),elmat,amatr)   
           end if

        else if ( order == 4 ) then
           call runend('TEM_ELMOPE: OBSOLETE')
           
        end if       

        kfl_advec_tem=kfl_advec_old
     end if

  end do elements

  if (kfl_reset == 0 ) then
     if (dtmin /= 0.0_rp ) then
        dtcri_tem = min(dtcri_tem, dtmin)
     endif
  endif

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine tem_elmope
