subroutine tem_elmgat(&
     ielem,pnode,lnods,eltem,elvel,elcod,eledd,elDik)
  !------------------------------------------------------------------------
  !****f* Temper/tem_elmgat
  ! NAME 
  !    tem_elmgat
  ! DESCRIPTION
  !    This routine performs the gather operations
  ! USES
  ! USED BY
  !    tem_elmope
  !    tem_bouope
  !***
  !------------------------------------------------------------------------ 
  use def_kintyp, only     :  ip,rp 
  use def_domain, only     :  ndime,mnode,npoin,coord
  use def_master, only     :  therm,turmu,advec
  use def_temper, only     :  kfl_cotur_tem,kfl_regim_tem
  use def_temper, only     :  ADR_tem,kfl_advec_tem
  use mod_ADR,    only     :  BDF
  use mod_ker_proper
  implicit none
  integer(ip), intent(in)  :: pnode 
  integer(ip), intent(in)  :: ielem
  integer(ip), intent(in)  :: lnods(pnode)
  real(rp),    intent(out) :: eltem(pnode,*)
  real(rp),    intent(out) :: elcod(ndime,mnode),elvel(ndime,mnode)
  real(rp),    intent(out) :: eledd(mnode)
  real(rp),    intent(out) :: elDik(mnode)
  integer(ip)              :: inode,ipoin,idime,itime,dummi
  real(rp)                 :: dummr(3)
  real(rp)                 :: elhco(mnode),elsph(mnode)
  !
  ! Current temperature and coordinates
  !
  do inode = 1,pnode
     ipoin = lnods(inode)
     eltem(inode,1) = therm(ipoin,1)
     do idime = 1,ndime
        elcod(idime,inode) = coord(idime,ipoin)
     end do  
  end do
  !
  ! Turbulent viscosity mut from RANS
  !
  if( kfl_cotur_tem > 0 ) then
     do inode = 1,pnode
        ipoin = lnods(inode)
        eledd(inode) = turmu(ipoin)
     end do
  end if
  !
  ! Time integration
  !
  if( ADR_tem % kfl_time_integration /= 0 ) then
     do inode = 1,pnode
        ipoin = lnods(inode)
        eltem(inode,2) = therm(ipoin,3)
     end do
     if( ADR_tem % kfl_time_scheme == BDF ) then
        do itime = 3,ADR_tem % kfl_time_order + 1
           do inode = 1,pnode 
              ipoin = lnods(inode)
              eltem(inode,itime) = therm(ipoin,itime+1)
           end do
        end do
     end if
  end if 
  !
  ! Coupling with flow equations
  !
  if( kfl_advec_tem == 1 ) then
     do inode = 1,pnode
        ipoin = lnods(inode)
        do idime = 1,ndime
           elvel(idime,inode) = advec(idime,ipoin,1)
        end do
     end do
  else if( kfl_advec_tem >= 2 ) then
     call tem_velfun(pnode,elcod,elvel)
  end if

  if( kfl_regim_tem == 4 ) then
     elDik = 0.0_rp
     call ker_proper('CONDU','PNODE',dummi,ielem,elhco,pnode,dummi,dummr,dummr)
     call ker_proper('SPHEA','PNODE',dummi,ielem,elsph,pnode,dummi,dummr,dummr)
     do inode = 1,pnode    !! for CFI model elDik is lambda/cp and only used for gpgrd
       elDik(inode) = elhco(inode) / elsph(inode)
     end do
  end if

end subroutine tem_elmgat
