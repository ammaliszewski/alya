subroutine tem_elmope_new(order)
  !------------------------------------------------------------------------
  !****f* Temper/tem_elmop2
  ! NAME 
  !    tem_elmop2
  ! DESCRIPTION
  !    ORDER=1:
  !      Temperature equation, elemental operations:
  !      1. Compute elemental matrix and RHS 
  !      2. Impose Dirichlet boundary conditions
  !      3. Assemble them
  !    ORDER=4:
  !      Update the subgrid scale
  ! USES
  ! USED BY
  !    tem_matrix
  !------------------------------------------------------------------------
  use def_parame
  use def_elmtyp
  use def_master
  use def_domain
  use def_kermod
  use mod_ker_proper 
  use def_temper
  use mod_ADR, only : ADR_element_assembly
  use mod_ADR, only : ADR_bubble_assembly
  use mod_ADR, only : ADR_projections_and_sgs_assembly
  use mod_ADR, only : ADR_add_sgs_or_bubble
  use mod_ADR, only : ELEMENT_ASSEMBLY             ! 1
  use mod_ADR, only : PROJECTIONS_AND_SGS_ASSEMBLY ! 4
  use mod_ADR, only : BUBBLE_ASSEMBLY              ! 5
  use mod_ADR, only : mreac_adr

  implicit none

  integer(ip), intent(in) :: order                     ! =2: compute SGS only

  real(rp)    :: elmat(mnode,mnode),elrhs(mnode)
  integer(ip) :: ielem,igaus,idime                     ! Indices and dimensions
  integer(ip) :: pelty,pmate,pnode,kelem
  integer(ip) :: pgaus,plapl,porde,ptopo
  integer(ip) :: kfl_advec_old

  real(rp)    :: eltem(mnode,ADR_tem % ntime)          ! Gather 
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: elvel(ndime,mnode)
  real(rp)    :: eledd(mnode)
  real(rp)    :: elDik(mnode)

  real(rp)    :: tragl(ndime,ndime),chave(ndime,2)     ! Stabilization
  real(rp)    :: chale(2),hleng(ndime)

  real(rp)    :: gpdtc                                 ! Values at Gauss points
  real(rp)    :: gpvol(mgaus)                          ! |J|*w
  real(rp)    :: gprea(mgaus,mreac_adr)                ! r
  real(rp)    :: gpvel(ndime,mgaus)                    ! a
  real(rp)    :: gpcon(mgaus),gpcod(ndime,mgaus)       ! k
  real(rp)    :: gpdif(mgaus),gpgrd(ndime,mgaus)       ! k+kt, grad(k+kt)
  real(rp)    :: gpdiv(mgaus)                          ! Divergence of convection
  real(rp)    :: gprhs(mgaus)                          ! f (all terms)
  real(rp)    :: gpden(mgaus)                          ! rho and then rho*cp
  real(rp)    :: gpsph(mgaus)                          ! cp
  real(rp)    :: gptem(mgaus,ADR_tem % ntime)          ! T
  real(rp)    :: gpsou(mgaus)                          ! Q
  real(rp)    :: gpgrt(ndime,mgaus)                    ! grad(T)
  real(rp)    :: gpsgs(mgaus,3)                        ! T'
  real(rp)    :: gpsgv(ndime,mgaus)                    ! u'
  real(rp)    :: gpsha(mnode,mgaus)                    ! N
  real(rp)    :: gpcar(ndime,mnode,mgaus)              ! dNk/dxj
  real(rp)    :: gphes(ntens,mnode,mgaus)              ! dNk/dxidxj
  real(rp)    :: gplap(mnode,mgaus)                    ! Laplacian
  real(rp)    :: gpsta(mgaus)                          ! tau
  real(rp)    :: gptur(mgaus)                          ! Turbulent viscosity
  real(rp)    :: gpres(mgaus)                          ! Residual
  integer(ip) :: dummi
  real(rp)    :: dummr
  real(rp)    :: dtmin, dtcri

  integer(ip) :: inode
  real(rp)    :: elcod_bub(ndime)                      ! x
  real(rp)    :: gpvol_bub(mgaus)                      ! V
  real(rp)    :: gpsha_bub(mgaus)                      ! Ne
  real(rp)    :: gpcar_bub(ndime,mgaus)                ! dNek/dxj
  real(rp)    :: gphes_bub(ntens,mgaus)                ! dNek/dxidxj


real(rp) :: xjaci(2,2),xjacm(2,2),gpdet,xfunc(mnode+1)
integer(ip) :: jdime

#ifdef EVENT
  call mpitrace_user_function(1)
#endif
  !
  ! Initialization
  !
  gpdif = 0.0_rp
  gpsph = 0.0_rp
  gpden = 0.0_rp
  gprea = 0.0_rp
  gpsta = 0.0_rp
  gpdiv = 0.0_rp
  gprhs = 0.0_rp
  gptur = 0.0_rp
  gptem = 0.0_rp
  gpgrd = 0.0_rp
  gpvel = 0.0_rp
  !
  ! Loop over elements
  !  
  dtmin = 1.0e6_rp
  elements: do kelem = 1,nelez(lzone(ID_TEMPER))
     ielem = lelez(lzone(ID_TEMPER)) % l(kelem)
     !
     ! Element dimensions
     !
     pelty = ltype(ielem)

     if( pelty > 0 ) then
        pnode = lnnod(ielem)
        pgaus = ngaus(pelty)
        plapl = llapl(pelty)
        porde = lorde(pelty)
        ptopo = ltopo(pelty)
        !
        ! Check if element is a solid
        !
        pmate = 1
        kfl_advec_old = kfl_advec_tem
        if( nmate > 1 ) then
           pmate = lmate(ielem)
           if( pmate > nmatf ) kfl_advec_tem = 0 ! Solid material: no advection
        end if
        !
        ! Gather operations 
        !
        call tem_elmgat(&
             ielem,pnode,lnods(1,ielem),eltem,elvel,elcod,eledd,elDik)
        !
        ! hleng and tragl at center of gravity
        !
        call elmlen(ndime,pnode,elmar(pelty) % dercg,tragl,elcod,&
             hnatu(pelty),hleng)
        !
        !Compute the characteristic length CHALE
        !
        if( kfl_ellen_tem == -1 ) then 
           call tem_elmchl(&
                ielem,pelty,pnode,plapl,pmate,lnods(1,ielem),elcod,eltem,&
                elvel,gpcar,gphes,chale)
        else
           call elmchl(tragl,hleng,elcod,elvel,chave,chale,pnode,&
                porde,hnatu(pelty),kfl_advec_tem,kfl_ellen_tem)
        end if
        !
        ! Local time step DTINV_TEM
        !
        if(kfl_timco==2) then 
           call tem_elmtss(&
                ielem,pelty,pnode,pmate,elcod,elvel,eledd,&
                eltem,gpcar,chale,gpdtc)        
           dtinv_tem = 1.0_rp/(gpdtc*safet_tem)
           if( kfl_stead_tem == 1 ) dtinv_tem = 0.0_rp
           if( kfl_timei_tem == 0 ) dtinv_tem = 0.0_rp
        end if
        !
        ! Cartesian derivatives, Hessian matrix and volume: GPCAR, GPHES, PGVOL
        !
        if( plapl == 0 ) gphes = 0.0_rp
        call elmca2(&
             pnode,pgaus,plapl,elmar(pelty) % weigp,elmar(pelty) % shape,&
             elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpsha,&
             gpcar,gphes,ielem)
        !
        ! Temperature: GPTEM
        !
        call gather(&
             2_ip,pgaus,pnode,1_ip,dummi,gpsha,eltem,gptem)   
        !
        ! Add SGS or bubble to current unknown
        !
        call ADR_add_sgs_or_bubble(&
             ielem,pgaus,elmar(pelty) % shape_bub,ADR_tem,gptem)
        !
        ! Properties: GPDEN, GPDIF, GPGRD and GPREA
        !
        call ker_proper('DENSI','PGAUS',dummi,ielem,gpden,pnode,pgaus,gpsha,gpcar)
        call ker_proper('CONDU','PGAUS',dummi,ielem,gpcon,pnode,pgaus,gpsha,gpcar)
        call ker_proper('SPHEA','PGAUS',dummi,ielem,gpsph,pnode,pgaus,gpsha,gpcar)
        call ker_proper('GRCON','PGAUS',dummi,ielem,gpgrd,pnode,pgaus,gpsha,gpcar)
        call ker_proper('TURBU','PGAUS',dummi,ielem,gptur,pnode,pgaus,gpsha,gpcar)
        ! 
        ! Coupling with turbul
        !
        call tem_turbul(&
             ielem,pnode,pgaus,1_ip,pgaus,eledd,elDik,gpsha,gpcar,gpcon,gpsph, &
             gpdif,gpgrd,gpden,gptur) 
        !
        ! Reaction term
        !
        call tem_elmrea( &
             1_ip,pnode,pgaus,1_ip,pgaus,elvel,gpden,gpcar,&
             gprea)
        !
        ! Equation coefficients in GP
        !
        call tem_elmpre(&  
             pnode,pgaus,pmate,gpden,gpsph,gpdif,&
             gpsgv,gpsha,gpcar,gphes,elvel,eltem,&
             elcod,eledd,gpvel,gptem,gprhs,gpcod,&
             gpgrt,lnods(1,ielem),ielem)
        !
        ! Coupling with RADIAT
        !   
        call tem_radiat(&
             ielem,pnode,pgaus,lnods(1,ielem),gpsha,gprhs)
        !
        ! Coupling with CHEMIC
        !
        call tem_chemic(ielem,pgaus,gprhs)      
        !
        ! Reset check
        !
        if( kfl_reset == 0 ) then
           call tem_adr_critical_time(pgaus,chale,gpvel,gpdif,gprea,gprhs,gpden,gpsph,gptem,dtcri)
           if (dtcri /= 0.0 ) then
              dtmin= min(dtmin,dtcri)
           end if
        end if

        if( order == ELEMENT_ASSEMBLY ) then
           ! 
           ! Assemble equation
           ! 
           call ADR_element_assembly(&
                ielem,pnode,pgaus,elcod,gpsha,gpcar,elmar(pelty) % deriv,gphes,gpvol,chale,&
                elmar(pelty) % shape_bub,elmar(pelty) % deriv_bub,ADR_tem,&
                cutim,gpden,gpvel,gpdif,gpgrd,gprea,gprhs,&
                gptem,eltem,elmat,elrhs)
           !
           ! Prescribe Dirichlet boundary conditions
           !
           if( solve(1) % kfl_iffix == 0 ) &
                call tem_elmdir(&
                pnode,lnods(1,ielem),elmat,elrhs,ielem)
           if( solve_sol(1)  %  kfl_algso == 9 ) then
              call tem_assdia(&
                   pnode,elmat,eltem,elrhs)
              call assrhs(&
                   solve(1) % ndofn,pnode,lnods(1,ielem),elrhs,rhsid)      
           else
              !
              ! Assembly
              !
              call assrhs(&
                   solve(1) % ndofn,pnode,lnods(1,ielem),elrhs,rhsid)
              call assmat(&
                   solve(1) % ndofn,pnode,pnode,npoin,solve(1) % kfl_algso,&
                   ielem,lnods(1,ielem),elmat,amatr)   
           end if

        else if ( order == PROJECTIONS_AND_SGS_ASSEMBLY ) then
           !
           ! Assemble residual projection
           !
           call ADR_projections_and_sgs_assembly(&
                ielem,pnode,pgaus,elcod,gpsha,gpcar,gphes,gpvol,chale,ADR_tem,&
                cutim,gpden,gpvel,gpdif,gpgrd,gprea,&
                gprhs,gptem,eltem)

        else if ( order == BUBBLE_ASSEMBLY ) then
           !
           ! Update bubble
           !
           call ADR_bubble_assembly(&
                ielem,pnode,pgaus,elcod,gpsha,gpcar,elmar(pelty) % deriv,gphes,gpvol,chale,&
                elmar(pelty) % shape_bub,elmar(pelty) % deriv_bub,ADR_tem,&
                cutim,gpden,gpvel,gpdif,gpgrd,gprea,gprhs,&
                gptem,eltem,elmat,elrhs)

        end if       

        kfl_advec_tem=kfl_advec_old
     end if

  end do elements

  if( kfl_reset == 0 ) then
     if( dtmin /= 0.0_rp ) then
        dtcri_tem = min(dtcri_tem,dtmin)
     end if
  end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine tem_elmope_new
