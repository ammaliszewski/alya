subroutine tem_matrix()
  !------------------------------------------------------------------------
  !
  ! Compute elemental matrix and RHS of the following problem:
  !   _                             _
  !  |                             |
  !  |  rho*cp/(theta*dt) T*v dw + |  rho*cp*[u.grad(T)] dw
  ! _|W                           _|W
  !
  !    _                         _               _
  !   |                         |               |
  ! + |  k*grad(T).grad(v) dw + |  ar*Tr*v ds = |  Q*v dw
  !  _|W                       _|S             _|
  !
  !   _                               _
  !  |                               |
  !  |  rho*cp/(theta*dt) T^n*v dw - |  (qr-ar*Tr) v ds
  ! _|W                             _|S
  ! 
  ! All terms are calculated at current time step, i.e. n+theta
  !
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_temper
  use def_domain
  use def_coupli
  use mod_interpolation
  use mod_couplings
  use mod_communications, only : PAR_MIN
  implicit none
  integer(ip) :: izone,kpoin,ipoin  
  real(rp)    :: time1,time2
  !
  ! Initializations
  !
  call inisol()
  resgs_tem(1) = 0.0_rp
  resgs_tem(2) = 0.0_rp

  call cputim(time1) 

  if( INOTMASTER ) then 
     !
     ! Element assembly
     !
     !call tem_elmope(1_ip) 
     call tem_elmope_new(1_ip) 
     !
     ! Boundary assembly
     !
     call tem_bouope()
     !
     ! Impose non-uniform heat flux (from FIELDS)
     !    
     if( kfl_flux_tem > 0_ip ) then
        izone = lzone(ID_TEMPER)
        do kpoin = 1,npoiz(izone)
           ipoin = lpoiz(izone) % l(kpoin)
           if( ipoin <= npoi1 .or. ( ipoin >= npoi2 .and. ipoin <= npoi3 ) ) then
              rhsid(ipoin) = rhsid(ipoin) + heat_flux(1,ipoin)
           end if
        end do
     endif

  end if

  call cputim(time2) 
  cpu_modul(CPU_ASSEMBLY,modul) = cpu_modul(CPU_ASSEMBLY,modul) + time2 - time1

  if( kfl_reset /= -1 ) then
     !
     ! Look for minimum over subdomains
     ! 
     call PAR_MIN(dtcri_tem,'IN MY ZONE')
     !
     !
     if (dtcri_tem /= 0.0) dtinv_tem = 1.0_rp/(dtcri_tem*safet_tem)
     !
     ! If changing dtinv is necessary, activate reset mode
     if (dtinv_tem > reset_factor * dtinv) then
        dtinv     = dtinv_tem
        kfl_reset = 1
        if( INOTSLAVE ) call livinf(-12_ip,'REQUESTED RESET OF TIME STEP', 1_ip)
     endif
  end if

end subroutine tem_matrix
