subroutine tem_comtem
  !-----------------------------------------------------------------------
  !****f* Temper/solve_tem
  ! NAME 
  !    solve_tem
  ! DESCRIPTION
  !    This routine computes the temperature from the enthalpy using the 
  !    polynomial coefficients for the specific heat
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_temper
  use mod_ker_proper
  implicit none
  integer(ip)             :: ipoin,dummi,nte,xt, ivalu
  real(rp)                :: dummy(ndime,ndime),teloc(2), delta
  real(rp)                :: cploc(6,2), deltaDash

  !
  ! total enthalpy equation, source term implicit in cp coefficients
  !
!!DMM  if (kfl_prope_tem == 1) call tem_reatab()

  do ipoin=1,npoin
    teloc(1) = tempe(ipoin,1)
    nte = 0
    do ivalu = 1,6
      cploc(ivalu,1) = sphec(ipoin,ivalu,1)
      cploc(ivalu,2) = sphec(ipoin,ivalu,2)
    end do
    do while(nte < 50)
      call tem_comput(2_ip,teloc(1),therm(ipoin,1),cploc,delta)
      nte = nte + 1
      if (abs(delta) < 0.00001_rp) then
        !
        ! update tempe and cp and exit loop
        !
        tempe(ipoin,1) = min(max(teloc(1),200.0_rp),3000.0_rp)
        call tem_comput(1_ip,tempe(ipoin,1),therm(ipoin,1),cploc,sphek(ipoin,1))
        exit
      else
        !
        ! residual still too high, compute new temperature for next loop
        !
        call tem_comput(1_ip,teloc(1),therm(ipoin,1),cploc,deltaDash)
        teloc(2) = teloc(1)
        teloc(1) = teloc(2) - (delta/deltaDash)
      end if
    end do
  end do

end subroutine tem_comtem
