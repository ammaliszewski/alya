subroutine tem_intphy(luni2)
  !------------------------------------------------------------------------
  !****f* Temper/tem_intphy
  ! NAME 
  !    tem_intphy
  ! DESCRIPTION
  !    Interpolate velocity from a file
  ! USES
  ! USED BY
  !    tem_reaphy
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_kermod
  use def_temper
  use def_domain
  use def_inpout
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: luni2

  call runend('TEM_INTPHY: OBSOLETE')

end subroutine tem_intphy

