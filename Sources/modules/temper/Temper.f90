subroutine Temper(order)
  !-----------------------------------------------------------------------
  !****f* Temper/temper
  ! NAME
  !   Temper
  ! DESCRIPTION
  !   This routine deals with the temperature equation. The task done
  !   corresponds to the order given by the master.
  ! USES
  !    tem_turnon
  !    tem_timste
  !    tem_begste
  !    tem_doiter
  !    tem_concon
  !    tem_conblk
  !    tem_newmsh
  !    tem_endste
  !    tem_turnof
  ! USED BY
  !    Reapro
  !    Turnon
  !    Timste
  !    Begste
  !    Doiter
  !    Concon
  !    Conblk
  !    Newmsh
  !    Endste
  !    Turnof
  !***
  !-----------------------------------------------------------------------
  use def_master
  !
  use mod_commdom_alya, only: INONE
  use mod_tem_commdom,  only: tem_commdom_plugin  
  !
  implicit none
  integer(ip), intent(in) :: order
  integer(ip)             :: icoup
  !
  ! Normal order
  !
  select case (order)

  case( ITASK_TURNON )
     call tem_turnon()
  case( ITASK_TIMSTE ) 
     call tem_timste()
  case( ITASK_INIUNK ) 
     call tem_iniunk()
  case( ITASK_BEGSTE ) 
     call tem_begste()
  case( ITASK_DOITER )
     call tem_doiter()
  case( ITASK_CONCOU )
     call tem_concou()
  case( ITASK_CONBLK )
     call tem_conblk()
  case( ITASK_NEWMSH )
     call tem_newmsh()
  case( ITASK_ENDSTE )
     call tem_endste()
  case( ITASK_OUTPUT )
     call tem_output()
  case( ITASK_TURNOF )
     call tem_turnof()
  end select
  !
  ! Coupling
  !
  if( order > 1000 ) call tem_plugin(order-1000_ip) 

  call tem_commdom_plugin()

end subroutine Temper
