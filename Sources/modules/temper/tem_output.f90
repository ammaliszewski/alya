subroutine tem_output()
  !------------------------------------------------------------------------
  !****f* Temper/tem_output
  ! NAME 
  !    tem_output
  ! DESCRIPTION
  !    Output and postprocess of solution
  ! USES
  ! USED BY
  !    tem_iniunk
  !    tem_endite
  !    tem_endste
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_temper
  use mod_iofile
  use mod_matrix, only : matrix_output_gid_format
  use mod_ADR,    only : ADR_manufactured_error
  implicit none
  integer(ip)           :: ivari,ivarp
  integer(ip)           :: ipoin,jpoin,izdom
  real(rp), allocatable :: aa(:)
  !
  ! Initial solution, end of a time step and and of run
  !
  do ivarp = 1,nvarp
     ivari = ivarp
     call posdef(11_ip,ivari)
     call tem_outvar(ivari)
  end do

  if( postp(1) % npp_stepi(5) /= 0 ) then
     if( mod(ittim, postp(1) % npp_stepi(5) ) == 0 ) then  ! AVTEM frequency  
        avtim_tem = cutim  ! Update reference time for time-averaging
     endif
  endif

  if( ittyp == ITASK_INITIA .or. ittyp == ITASK_ENDTIM ) then
     !
     ! View factor, only first iteration
     !
     if( kfl_radia_tem == 1 .and. ittim == 1 ) then
        call tem_radpos()
     end if
     !
     ! Boundary conditions
     !
     if( npp_bound_tem == 1 ) then 
        npp_bound_tem = 0
        call tem_outbcs()
     end if
     !
     ! Calculations on sets
     !
     call tem_outset()
     !
     ! Calculations on witness points
     !
     call tem_outwit()
     !
     ! Error w/r exact solution
     !
     if( kfl_exacs_tem /= 0 .and. kfl_timei_tem /= 0 ) then
        call ADR_manufactured_error(ADR_tem,ittim,cutim,therm)
        !call tem_exaerr(1_ip)
     end if

  else if( ittyp == ITASK_ENDRUN ) then
     !
     ! End of the run
     !
     if( kfl_splot_tem == 1 .and. ISEQUEN ) then
        call suplot(1,therm(1:npoin,1),lun_splot_tem)
        close(lun_splot_tem)
     end if

     if( kfl_psmat_tem /= 0 .and. INOTMASTER ) then
        if( kfl_psmat_tem > 0 ) then
           call pspltm(&
                npoin,npoin,1_ip,0_ip,c_dom,r_dom,amatr,&
                trim(title)//': '//namod(modul),0_ip,18.0_rp,'cm',&
                0_ip,0_ip,2_ip,lun_psmat_tem)
           call tem_openfi(4_ip)
        else
            call matrix_output_gid_format(npoin,1_ip,r_dom,c_dom,amatr,momod(modul) % solve(1) % invpr_gs)
        end if
        kfl_psmat_tem = 0
     end if
     !
     ! Residual
     !
     !if( postp(1) % npp_stepi(8) > 0 ) call tem_outvar(8_ip)

  end if

end subroutine tem_output
