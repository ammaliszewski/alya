subroutine tem_turbul(&
     ielem,pnode,pgaus,igaui,igauf,eledd,elDik,gpsha,gpcar,gpcon,gpsph, &
     gpdif,gpgrd,gpden,gptur)   
  !-----------------------------------------------------------------------
  !****f* Temper/tem_turbul
  ! NAME 
  !    tem_turbul
  ! DESCRIPTION
  !  Coupling with TURBUL
  !    Compute Total thermal conductivity k+kt
  ! USES
  ! USED BY
  !    tem_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  ndime,mnode,lnods
  use def_temper, only     :  prtur_tem,kfl_cotur_tem,kfl_grdif_tem,kfl_tfles_tem, &
                              kfl_regim_tem
  use def_master, only     :  kfl_paral,tfles_factor,tfles_sensor,tfles_sgseff

  implicit none
  integer(ip), intent(in)  :: ielem,pnode,pgaus,igaui,igauf
  real(rp),    intent(in)  :: eledd(pnode)
  real(rp),    intent(in)  :: elDik(pnode)
  real(rp),    intent(in)  :: gpsha(pnode,pgaus), gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)  :: gpcon(pgaus)
  real(rp),    intent(in)  :: gpsph(pgaus)
  real(rp),    intent(in)  :: gpden(pgaus)
  real(rp),    intent(out) :: gpdif(pgaus)
  real(rp),    intent(inout)   :: gpgrd(ndime,pgaus)
  real(rp),    intent(inout)   :: gptur(pgaus)
  integer(ip)              :: igaus,inode,idime
  
  !
  ! Total thermal diffusion GPDIF=k+kt; kt=mut*Cp/Prt
  !
  if (kfl_regim_tem == 4) then
    gpgrd = 0.0_rp
    do igaus=igaui,igauf
       gpdif(igaus)=gpcon(igaus) / gpsph(igaus)
       do inode = 1,pnode
          do idime=1,ndime
             gpgrd(idime,igaus) = gpgrd(idime,igaus) + gpcar(idime,inode,igaus) * elDik(inode)
          end do
       end do
    end do
  else
    do igaus=igaui,igauf
       gpdif(igaus)=gpcon(igaus)
    end do
  end if

  if(kfl_cotur_tem/=0) then

     if(kfl_cotur_tem > 0) then  !! RANS
        gptur=0.0_rp
        do igaus=igaui,igauf 
          do inode=1,pnode
             gptur(igaus)=gptur(igaus)+gpsha(inode,igaus)*eledd(inode)
          end do
        end do
     elseif(kfl_cotur_tem < 0) then  !! LES
        do igaus=igaui,igauf 
           gptur(igaus) = gptur(igaus)*gpden(igaus)
        end do
     endif 

     if (kfl_regim_tem == 4) then
       do igaus=igaui,igauf
         gpdif(igaus) = gpdif(igaus) + gptur(igaus)/prtur_tem
       end do
     else if (kfl_tfles_tem == 1_ip) then
        do igaus=igaui,igauf
           gpdif(igaus) = gpdif(igaus)*tfles_factor(ielem)%a(igaus)*tfles_sgseff(ielem)%a(igaus) &
                          + (1.0_rp - tfles_sensor(ielem)%a(igaus))*gpsph(igaus)*gptur(igaus)/prtur_tem
        end do
     else
       do igaus=igaui,igauf        
          gpdif(igaus)=gpdif(igaus)+gpsph(igaus)*gptur(igaus)/prtur_tem
       end do
     end if

     if(kfl_grdif_tem/=0) then
        !
        ! GPGRD=grad(k+kt)
        ! 
        if(kfl_cotur_tem==1) then
           do igaus=1,pgaus
              do inode=1,pnode
                 do idime=1,ndime
                    gpgrd(idime,igaus)=gpgrd(idime,igaus)&
                         +gpcar(idime,inode,igaus)*eledd(inode)
                 end do
              end do
              do idime=1,ndime
                 gpgrd(idime,igaus)=gpgrd(idime,igaus)*gpsph(igaus)/prtur_tem
              end do
           end do
        end if
     endif

  end if

end subroutine tem_turbul
