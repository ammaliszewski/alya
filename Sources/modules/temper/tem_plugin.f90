!-----------------------------------------------------------------------
!> @addtogroup Temper
!> @{
!> @file    tem_plugin.f90
!> @author  Guillaume Houzeaux
!> @date    13/04/2014
!> @brief   Plugin with coupling
!> @details Plugin for the zonal coupling
!> @} 
!-----------------------------------------------------------------------

subroutine tem_plugin(icoup)
  !
  ! Obligatory variables 
  !
  use def_kintyp,    only :  ip,rp
  use def_coupli,    only :  coupling_type
  use def_master,    only :  solve_sol
  use mod_couplings, only :  COU_INTERPOLATE_NODAL_VALUES
  use mod_memory,    only :  memory_deallo
  use mod_memory,    only :  memory_alloca
  use mod_matrix,    only :  matrix_initialize
  !
  ! Possible variables 
  !
  use def_master,    only :  tempe
  use def_temper,    only :  bvess_tem
  use mod_parall,    only :  PAR_GLOBAL_TO_LOCAL_NODE

use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
use def_master, only : current_code,inotmaster,kfl_paral
use def_domain, only : npoin

  implicit none 
  integer(ip), intent(in) :: icoup
  character(5)            :: variable

real(rp), pointer :: xx(:,:)

  variable = coupling_type(icoup) % variable 

  if( variable == 'TEMPE' ) then   
     !
     ! Temperature 
     ! 
     !allocate( xx(1,max(npoin,1)) )
     call COU_INTERPOLATE_NODAL_VALUES(icoup,1_ip,bvess_tem,tempe)
     if( current_code == 2 .and. inotmaster ) then
        !call PAR_INTERFACE_NODE_EXCHANGE(bvess_tem,'SUM','IN MY CODE')
     end if
     !deallocate( xx )

  else if( variable == 'RESID' ) then
     !
     ! Residual
     !
     call matrix_initialize(solve_sol(1) % bvnat)
!solve_sol(1) % reaction = -solve_sol(1) % reaction
     call COU_INTERPOLATE_NODAL_VALUES(icoup,1_ip,solve_sol(1) % bvnat,solve_sol(1) % reaction)
     !if( current_code == 1 .and. inotmaster ) then
     !   call PAR_INTERFACE_NODE_EXCHANGE(solve_sol(1) % bvnat,'SUM','IN MY CODE')
     !end if
  else if( variable == 'FAKE ' ) then
     !! This variable is defined to allow for the use of the coupling tools
 
  end if

  !if( variable == 'TEMPE' ) then   
  !   !
  !   ! Temperature 
  !   ! 
  !   call COU_INTERPOLATE_NODAL_VALUES(icoup,1_ip,bvess_tem,tempe)
  !   if( current_code == 2 .and. inotmaster ) then
  !      !call PAR_INTERFACE_NODE_EXCHANGE(bvess_tem,'SUM','IN MY CODE')
  !   end if
  !
  !else if( variable == 'RESID' ) then
  !   !
  !   ! Residual
  !   !
  !   call matrix_initialize(solve_sol(1) % bvnat)
  !   call COU_INTERPOLATE_NODAL_VALUES(icoup,1_ip,solve_sol(1) % bvnat,solve_sol(1) % reaction)
  !
  !end if

end subroutine tem_plugin

