subroutine tem_bouwal(&
     lboel,shapb,pnodb,pnode,ndime,elvel,&
     delta,acden,acvis,acsph,actco,prtur,&
     twall,arobi,trobi,qrobi)
  
!------------------------------------------------------------------------
!
! Wall law for temperature
!
! This routine updates the surface parameters for the heat equation at
! a given integration point of a boundary IBOUN received by argument.
! The parameters will be saved in the array TPARA, as follows:
!
! q_presc=alfa*(T-Ta)+q
!
! Parameters alfa, Ta and q are computed using the law of the wall
! for the temperature
!
! Pr    = Prandtl number
! Prt   = Turbulent Prandtl number
! Rt    = Pr/Prt
! Dt    = exp(-y+/11 Rt^beta)      
!          +-      
!          | 1.1   for Rt<0.1
! beta  = -+
!          | 0.333 for Rt>0.1
!          +-
! Pt    = 9.24(Rt^{0.75}-1)(1+0.28 exp(-0.007Rt))      
! T+    = Pr u+ Dt + Prt(1-Dt)(u+ + Pt)
!
! The wall heat flux qw is given by:
! 
!         rho cp(Tw-T)u*
! qw    = --------------, therefore
!              T+
!
! alfa  = -rho cp u*/T+
! Ta    =  Tw
! q     =  0      
!
! NB: In the limit y+ -> 0, we have T+=Pr y+=Pr y u*/nu so that
!         rho cp(Tw-T)nu
! qw    = --------------
!             Pr y
! 
!------------------------------------------------------------------------
  use def_kintyp
  use def_kermod, only : rough_dom,kfl_rough
  implicit  none
  integer(ip), intent(in)  :: ndime,pnodb,pnode
  integer(ip), intent(in)  :: lboel(pnodb)
  real(rp),    intent(in)  :: actco,prtur,acvis,acden,twall
  real(rp),    intent(in)  :: shapb(pnodb)
  real(rp),    intent(in)  :: elvel(ndime,pnode)     
  real(rp),    intent(out) :: arobi,trobi,qrobi
  real(rp)                 :: velfr,zero3,yplus,uplus,Pr,Rt
  real(rp)                 :: alpha,Tw,Dt,Pt,Tplus,delta,vonka
  real(rp)                 :: frico,tveno,vikin,qwall,acsph
  real(rp)                 :: veloc
  integer(ip)              :: idime,inodb

  arobi = 0.0_rp
  trobi = 0.0_rp
  tveno = 0.0_rp
  do idime=1,ndime
     veloc = dot_product(shapb(1:pnodb),elvel(idime,lboel(1:pnodb)))
     tveno = tveno+veloc*veloc
  end do
  if(tveno>1.0e-12) tveno=sqrt(tveno)
  vonka=0.41_rp
  vikin=acvis/acden
  if( kfl_rough > 0 ) then
     call runend('TEM_BOUWAL: NOT CODED')
     !rough_dom = rough(ipoin)
  end if
  call frivel(delta,rough_dom,tveno,vikin,velfr)             ! U*
  yplus = delta*velfr/vikin
  if(yplus<1.0) then
     Pr    =  acsph*acvis/actco
     qwall = -vikin/(Pr*delta)
  else
     if(velfr>1.0e-12) then
        uplus = tveno/velfr
     else
        uplus = 0.0_rp
     end if
     Pr    = acsph*acvis/actco
     Rt    = Pr/prtur
     if(Rt<0.1) then
        alpha = 1.100_rp
     else
        alpha = 0.333_rp
     end if
     Dt    = exp(-yplus/11.0_rp*Rt**alpha)
     Pt    = 9.24*(Rt**0.75_rp-1.0_rp)*(1.0_rp+0.28_rp*exp(-0.007_rp*Rt))
     Tplus = Pr*uplus*Dt+prtur*(1.0_rp-Dt)*(uplus+Pt)
     if(Tplus>1.0e-12) then
        qwall  = -acden*acsph*velfr/Tplus
     else
        qwall  = 0.0_rp
     end if
  end if

  arobi=qwall
  trobi=twall
  qrobi=0.0_rp

end subroutine tem_bouwal
