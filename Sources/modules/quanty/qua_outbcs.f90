subroutine qua_outbcs
!-----------------------------------------------------------------------
!****f* Quanty/qua_outbcs
! NAME 
!    qua_outbcs
! DESCRIPTION
!    Postprocess boundary conditions. This could be useful to include
!    a new file when running the same problem over again.
! como no se para que sirve, la dejo vacia!
! USES
! USED BY
!    qua_output
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_quanty
  use      mod_iofile
  implicit none


end subroutine qua_outbcs
