module mod_root_finding
  ! ==============================================================================
  ! INIT   
  !
  use def_parame, only                     :  &
      ip, rp
  ! -----------------------------------------------------------------------------
  implicit none 
  ! -----------------------------------------------------------------------------
  interface
    real(rp) function function_template(x, arg)
      use def_parame, only                 :  &
          ip, rp
      implicit none
      real(rp), intent(in)                 :: x
      real(rp), intent(inout), optional    :: arg(:)
    end function function_template  
  end interface
  !=============================================================| init |=========
  !==============================================================================
  ! PUBLIC
  !
  public                                   :: &
      rootfind_bracke,                        & !
      rootfind_bisect,                        & !
      rootfind_secant
  !=============================================================| public |=======
  !==============================================================================
  ! PRIVATE
  !
  private
  !=============================================================| private |======
  !==============================================================================
  ! CONTAINS
  !
  contains

  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine rootfind_bracke(low, upp, fac, ite, suc, fun, arg)
  
    use def_kintyp, only                    :  &  
        ip, rp, lg
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    procedure(function_template)            :: &
         fun  
    real(rp),       intent(inout), optional :: &
         arg(:)              
    integer(ip),    intent(in)              :: &
         ite                                     !
    real(rp),       intent(in)              :: &
         fac                                     !
    logical(lg),    intent(out)             :: &
         suc                                
    real(rp),       intent(inout)           :: &
         low, upp
    ! ---------------------------------------------------------------------------
    integer(ip)                             :: &
         i                                       !
    real(rp)                                :: & 
         fx1, fx2, x1, x2, xold
    ! ---------------------------------------------------------------------------
    
    xold = low
    x1   = low
    x2   = upp
    
    fx1 = fun(x1, arg)
    fx2 = fun(x2, arg)
    
    suc = .TRUE.
    
    do i = 1, ite
      if (fx1*fx2 < 0.0_rp) then
        low =  xold
        upp = x2
        return
      end if
      if (abs(fx1) < abs(fx2)) then
        xold = x1
        x1   = x1 + fac*(x1 - x2)
        fx1  = fun(x1, arg)
      else
        xold = x2
        x2   = x2 + fac*(x2 - x1)
        fx2  = fun(x2, arg)
      end if
    end do
    
    suc = .FALSE.
    return  
    
  end subroutine rootfind_bracke
  
  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine rootfind_bisect(low, upp, acc, ite, roo, fun, arg)

    use def_kintyp, only                    :  &  
        ip, rp, lg
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    procedure(function_template)            :: &
         fun  
    real(rp),       intent(inout), optional :: &
         arg(:)                               
    integer(ip),    intent(in)              :: &
         ite                                     ! Maximum iteration allowed
    real(rp),       intent(in)              :: &
         low, upp, acc                           ! Lower limit, upper limit and accuracy
    real(rp),       intent(out)             :: &
         roo                                     ! Result
    ! ---------------------------------------------------------------------------
    integer(ip)                             :: &
         i                                       !
    real(rp)                                :: & 
         fx1, fx2, x1, x2, dx
    ! ---------------------------------------------------------------------------
        
    x1 = low
    x2 = upp
    
    fx1 = fun(x1, arg)
    fx2 = fun(x2, arg)
    
    if(fx1*fx2 > 0.0_rp) then
      print*,"root must be bracketed in rtbis"
    end if
    
    if(fx1 < 0.0_rp) then
      roo = x1
      dx  = x2 - x1
    else
      roo = x2
      dx  = x1 - x2
    end if
    
    do i = 1, ite
      dx  = dx*0.5_rp
      x2  = roo + dx
      fx2 = fun(x2, arg)
      if (fx2 <= 0.0_rp) then
        roo = x2
      end if
      if ((abs(dx) < acc) .OR. (fx2 == 0.0_rp)) then
        return
      end if
    end do      
    
  end subroutine rootfind_bisect
  
 
  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine rootfind_secant(low, upp, acc, ite, roo, fun, arg)

    use def_kintyp, only                    :  &  
        ip, rp, lg
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    procedure(function_template)            :: &
         fun  
    real(rp),       intent(inout), optional :: &
         arg(:)                               
    integer(ip),    intent(in)              :: &
         ite                                     ! Maximum iteration allowed
    real(rp),       intent(in)              :: &
         low, upp, acc                           ! Lower limit, upper limit and accuracy
    real(rp),       intent(out)             :: &
         roo                                     ! Result
    ! ---------------------------------------------------------------------------
    integer(ip)                             :: &
         i                                       !
    real(rp)                                :: & 
         fx1, fx2, swa, x1, x2, dx, xl
    ! ---------------------------------------------------------------------------
    
    x1 = low
    x2 = upp
    
    fx1 = fun(x1, arg)
    fx2 = fun(x2, arg)
    
    if( abs(fx1) < abs(fx2) ) then
      roo = x1
      xl  = x2
      swa = fx1
      fx1 = fx2
      fx2 = swa
    else
      xl  = x1
      roo = x2
    end if
    
    do i = 1, ite
      dx  = (xl - roo)*fx2/(fx2 - fx1)
      xl  = roo
      fx1 = fx2
      roo = roo + dx
      fx2 = fun(roo, arg)
      if ((abs(dx) < acc) .OR. (fx2 == 0.0_rp)) then
        return 
      end if
    end do
    
    print*,'rootfind_secant exceed maximum iterations'
    
  end subroutine rootfind_secant
  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 !=============================================================| contains |=====
 
end module mod_root_finding
