!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_elmope.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966   
!> @brief   Elemental operations
!> @details Elemental operations
!>          1. Compute elemental matrix and RHS
!>          2. Impose Dirichlet boundary conditions
!>          3. Assemble them
!> @} 
!-----------------------------------------------------------------------
subroutine sld_elmope(itask)
  use def_master                                                ! general global variables
  use def_elmtyp                                                ! type of elements
  use def_domain                                                ! geometry information
  use def_solidz                                                ! general solidz module information
  use def_kermod,         only : kfl_detection                  ! Event detection flag
  use mod_ker_detection,  only : ker_detection_min_max_value    ! Event detection output
  use mod_communications, only : PAR_MAX
  implicit none
  integer(ip) :: itask,ielem,pevat,ievin                ! Indices and dimensions
  integer(ip) :: kelem,dummi,idofn,jdofn,ipoin
  integer(ip) :: ielem_min,inode
  integer(ip) :: pelty,pmate,pnode,pgaus,plapl,mephy
  real(rp)    :: elrhs(ndofn_sld*mnode+arcint_sld)
  real(rp)    :: elmat(ndofn_sld*mnode+arcint_sld,ndofn_sld*mnode)
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: eldis(ndime,mnode,ncomp_sld)
  real(rp)    :: eldix(ndime,mnode,ncomp_sld)
  real(rp)    :: elvel(ndime,mnode,ncomp_sld)
  real(rp)    :: elrst(    6,mnode)
  real(rp)    :: elvex(ndime,mnode,ncomp_sld)
  real(rp)    :: elepo(ndime,ndime,mnode)
  real(rp)    :: elfrx(ndofn_sld,mnode)
  real(rp)    :: gpvol(mgaus),gpdet(mgaus)
  real(rp)    :: gpdis(ndime,mgaus,ncomp_sld)           ! Displacement at time n
  real(rp)    :: gpvel(ndime,mgaus,ncomp_sld)           ! Velocity at time n
  real(rp)    :: gprat(ndime,ndime,mgaus)               ! Rate of deformation Fdot at time n
  real(rp)    :: gpgdi(ndime,ndime,mgaus)               ! Displacement Gradient F
  real(rp)    :: gpigd(ndime,ndime,mgaus)               ! Displacement Gradient Inverse F^{-1}
  real(rp)    :: gpcau(ndime,ndime,mgaus)               ! Cauchy tensor C
  real(rp)    :: gpstr(ndime,ndime,mgaus)               ! Stress tensor S
  real(rp)    :: gprst(ndime,ndime,mgaus)               ! Residual stress tensor 
  real(rp)    :: nfibe(ndime,mgaus)                     ! Fibers length vector (relative to its ini. length)
  real(rp)    :: gpene(mgaus)                           ! Energy
  real(rp)    :: gptlo(mgaus)                           ! Local tau (COUPLING WITH EXMEDI)
  real(rp)    :: gpsha(mnode,mgaus)                     ! Ni
  real(rp)    :: gpmof(mgaus)                           ! Modulator fields
  real(rp)    :: gphes(ntens,mnode,mgaus)               ! dNk/dxidxj
  real(rp)    :: gpcar(ndime,mnode,mgaus)               ! dNk/dxj
  real(rp)    :: gpcod(ndime,mgaus)                     ! Gauss points' physical coordinates
  real(rp)    :: gphea(mnode,mgaus)                     ! Shifted Heaviside (XFEM)
  real(rp)    :: gptmo(ndime,ndime,ndime,ndime,mgaus)   ! Tangent moduli
  real(rp)    :: elmof(mnode)                           ! Modulating fields 
  real(rp)    :: elmuu(mnode)
  real(rp)    :: elmaa(mnode)
  real(rp)    :: gpdds(ndime,ndime,ndime,ndime,mgaus)
  logical(lg) :: debugging
  real(rp)    :: gpdet_min,xcoor_min(3)

#ifdef MYTIMING
  real(rp)    :: time_begin, time_end                   ! Counters for timing
#endif

  gpdet_min = huge(1.0_rp)
  ielem_min = 0
  xcoor_min = 0.0_rp 

  if( INOTMASTER ) then

     !----------------------------------------------------------------------
     !
     ! RHS assembly (r) and Jacobian matrix (J) for implicit schemes 
     !
     !----------------------------------------------------------------------

     if( itask <= 2 ) then
        !
        ! Coupling with exmedi: 1. computing the activation vector
        !
        mephy= 0
        if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
           do pmate= 1,nmate
              mephy= mephy+kfl_ephys(pmate)
           end do
           if (mephy == 0) then
              call sld_exmedi(0_ip,0_ip,0_ip,gpsha,gptlo,0_ip)
           end if
           call sld_parall(4) 
        end if
        do ipoin = 1,ndofn_sld*npoin
           frxid_sld(ipoin) = 0.0_rp
        end do
     end if

#ifdef MYTIMING
     ! Start timing counter
     call cputim(time_begin)
#endif
     !
     !$OMP PARALLEL  DO                                                                 &
     !$OMP SCHEDULE (GUIDED)                                                            &
     !$OMP DEFAULT  (NONE)                                                              &
     !$OMP SHARED   (amatr, dedef_sld, dummi, elmar, fibde_sld,                         &
     !$OMP           nopio_sld, frxid_sld, gpgdi_sld, itask, kfl_exacs_sld,             &
     !$OMP           kfl_intva_sld, kfl_xfeme_sld, lelch, lelez, llapl,                 &
     !$OMP           lmate_sld, lnods, ltype, lzone, ndofn_sld, ndime, nelez,           &
     !$OMP           ngaus, nmate, nnode, rhsid, solve, gppio_sld, vinte_sld,           &
     !$OMP           vmass_sld, vmasx_sld, kfl_ephys ,taulo , kfl_gdepo_sld ,           &
     !$OMP           gdepo_sld, current_zone)                                           &
     !$OMP PRIVATE  (debugging, elcod, eldis, eldix, elmat, elmaa, elmuu,               &
     !$OMP           elrhs, elvel, elvex, elrst, gpcar, gpcau, gpcod, gpdds, gpdet,     &
     !$OMP           gpdis, gpene, gpgdi, gphea, gphes, gpigd, gprat, gpsha,            &
     !$OMP           gpstr, gptmo, gpvel, gpvol, gprst, idofn, ielem, ievin, jdofn,     &
     !$OMP           kelem, nfibe, pelty, pevat, pgaus, plapl, pmate, pnode,            &
     !$OMP           gptlo ,gpmof, elfrx, elmof,                                        &
     !$OMP           elepo, gpdet_min, ielem_min, xcoor_min )                                               

     elements: do kelem = 1,nelez(current_zone)

        ielem = lelez(current_zone) % l(kelem)

        debugging = .false.
        !
        ! Element dimensions
        !
        pelty = ltype(ielem)
        if ( pelty > 0 .and. pelty /= SHELL .and. pelty /= BAR3D ) then
           pnode = nnode(pelty)
           pgaus = ngaus(pelty)
           plapl = llapl(pelty)
           pevat = ndofn_sld*pnode
           pmate = 1
           gptlo = 0.0_rp
           gpmof = 1.0_rp

           if( nmate > 1 ) then
              pmate = lmate_sld(ielem)
           end if
           !
           ! Gather operations
           !
           call sld_elmgat(&
                pnode,lnods(1,ielem),eldis,elvel,elcod,eldix,elvex,elepo,elmof,elrst)
           !
           ! Cartesian derivatives, Hessian matrix and volume: GPCAR, GPHES, GPVOL
           !
           call elmca2(&
                pnode,pgaus,plapl,elmar(pelty)%weigp,elmar(pelty)%shape,&
                elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,gpvol,gpsha,&
                gpcar,gphes,ielem)
           !
           ! Variables at Gauss points
           !
           if (kfl_xfeme_sld == 1) then
              call gather(2_ip,pgaus,pnode,ndime,dummi,gpsha,elcod,gpcod)
              call sld_xfemic(2_ip,pnode,pgaus,gphea,gpcod,elcod,ielem,dummi)
           end if


           call sld_elmpre(&
                ielem,pnode,pgaus,pmate,eldis,elvel,eldix,elvex,elcod,&
                elmof,elrst,gpsha,gpcar,gphea,gpgdi_sld(ielem)%a,gpdet,gpvol,elepo,gpvel,&
                gprat,gpdis,gpcau,gpigd,gpcod,gpgdi,gpmof,gprst,gpdet_min,ielem_min)       

           !
           ! Coupling with exmedi: 2. computing the activation vector
           !
           if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
              if (kfl_ephys(pmate)== 0) then
                 call sld_exmedi(1,pnode,pgaus,gpsha,gptlo,ielem) ! find tau (local time)
              else if( kfl_ephys(pmate) == 1 ) then
                 gptlo = 0.0_rp
                 taulo = 0.0_rp
              end if
           end if
           !
           ! Constitutive law
           !
           ievin = 1
           if (kfl_intva_sld > 0) ievin = ielem  ! internal variables label for this element

           call sld_elmcla(&
                itask,pgaus,pmate,gpcau,gpgdi_sld(ielem)%a,gpgdi,gpene,gpstr,gppio_sld(ielem)%a,&
                gpdet,gprat,gpigd,gptlo,vinte_sld(1,1,ievin),gptmo,&
                nfibe,ielem,elcod,pnode,lnods(1,ielem),gpsha,gpdds,gpmof)
           


           !
           ! This if statement is moved here, because elmpre, elmcla, etc are also
           ! required for post-processing of stress, strain and internal variables
           ! (the call to sld_bitcul with itask = 3)
           !
           if( itask <= 2 ) then
              !
              ! RHS and MATRIX
              !
              call sld_elmmat(itask,&
                   ielem,pgaus,pmate,pnode,lnods(1,ielem),gpgdi,gppio_sld(ielem)%a,gpstr,&
                   gpvol,gpvel,gptmo,gprat,gpsha,gpcar,gphea,gpdis,gpdds,gprst,eldis,elcod,&
                   elrhs,elmat,elmuu,elmaa,elfrx)        
              if (debugging) then
                 write(*,*)''
                 write(*,*)'ielem = ',ielem
                 write(*,*)'elmat and elrhs (in sld_elmope)'
                 do idofn = 1,ndofn_sld*pnode
                    write(*,'(100(1x,e10.3))')(elmat(idofn,jdofn),jdofn=1,ndofn_sld*pnode),elrhs(idofn)
                 enddo
              end if
              !
              ! Add the stabilization terms (multiscales, rayleigh damping, etc.)
              ! DONE IN ELMMAT SUBROUTINE!!!
              !           call sld_stabil(&
              !                pgaus,pmate,pnode,pelty,gpgdi,gprat,gppio_sld(ielem)%a,gpvol,gpvel,gpstr,&
              !                gpcar,gphes,gpsta,shodi,elrhs,elmat)
              !
              ! Assemble push forward
              !
              if( kfl_gdepo_sld /= 0 ) then
                 call assrhs(&
                      ndime*ndime,pnode,lnods(1,ielem),elepo,gdepo_sld)              
              end if
              !
              ! Assembly
              !
              call assrhs(&
                   ndofn_sld,pnode,lnods(1,ielem),elrhs,rhsid)

              call assrhs(&
                   ndofn_sld,pnode,lnods(1,ielem),elfrx,frxid_sld)
                   
              if( itask == 2 ) then
                 call assmat(&
                      solve(1)%ndofn,pnode,pevat,solve(1)%nunkn,&
                      solve(1)%kfl_algso,ielem,lnods(1,ielem),elmat,amatr)
!                 call assrhs(&
!                      ndofn_sld,pnode,lnods(1,ielem),elfrx,frxid_sld)
              end if
              !
              ! Lumped mass matrices: VMASS_SLD and VMASX_SLD
              !
              if( itask == 1 ) then
                 call assrhs(&
                      1_ip,pnode,lnods(1,ielem),elmuu,vmass_sld)            
                 if( kfl_xfeme_sld == 1 ) then
                    call assrhs(&
                         1_ip,pnode,lnods(1,ielem),elmaa,vmasx_sld)
                 end if
              end if

           else if( itask == 3 ) then
              !
              ! Compute caust, green and lepsi, called in sld_fotens
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)

           else if( itask == 4 ) then
              !
              ! Fibers on element,
              ! needs nfibe, computed in sld_elmcla
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)

           else if( itask == 5 ) then
              !
              ! Fibers on nodes,
              ! needs nfibe, computed in sld_elmcla
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)

           else if( itask == 6 ) then
              !
              ! Update crack elements based on maximum tensile stress criteria
              !
              call sld_crapro(&
                   ielem,pgaus,pnode,gpcod,elcod,gpgdi_sld(ielem)%a,&
                   gppio_sld(ielem)%a,dedef_sld(ielem)%a)

           else if( itask == 7 ) then
              !
              ! Compute nopio_sld (first Piola--Kirchhoff stress) at nodes
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)

           else if( itask == 8 ) then
              !
              ! Exact solution: compute errors
              !
              if( kfl_exacs_sld /= 0 ) then
                 call sld_elmexa(&
                      2_ip,pgaus,pnode,elcod,gpsha,gpcar,gpdds,gpvol,&
                      eldis,gpdis,gpgdi,elrhs)
              end if

           else if( itask == 9 ) then
              !
              ! Exact solution: Assemble force term
              !
              if( kfl_exacs_sld /= 0 ) then
                 elrhs = 0.0_rp
                 call sld_elmexa(&
                      1_ip,pgaus,pnode,elcod,gpsha,gpcar,gpdds,gpvol,&
                      eldis,gpdis,gpgdi,elrhs)
                 call assrhs(ndofn_sld,pnode,lnods(1,ielem),elrhs,rhsid)
              end if

           else if( itask == 10 ) then
              !
              ! Compute element-wise cauchy Schwarz
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)


           else if( itask == 11 ) then
              !
              ! Compute recovered stresses: element-wise cauchy stress
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)
              
           else if( itask == 12 ) then
              !
              ! LOCAL CAUCHY STRESS TENSOR                                                  ( *AQU* ) 
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)

           else if( itask == 13 ) then
              !
              ! INFINITESIMAL STRAIN TENSOR
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)

           else if( itask == 14 ) then
              !
              ! INFINITESIMAL STRESS TENSOR (CAUCHY)
              !
              call sld_bitcul(&
                   itask,ielem,pnode,lnods(1,ielem),pgaus,pmate,gpsha,gpvol,&
                   gpgdi_sld(ielem)%a,gppio_sld(ielem)%a,nfibe,dedef_sld(ielem)%a)

           end if

        end if

     end do elements
  !$OMP END PARALLEL DO

  end if

  !----------------------------------------------------------------------
  !
  ! Negative Jacobian has been detected during assembly
  !
  !----------------------------------------------------------------------

  if( itask == 1 .or. itask == 2 ) then

     kelem = ielem_min
     !
     ! When using event detection, all partitions should be aware something's happening
     !
     if( kfl_detection /= 0 ) then
        call PAR_MAX(kelem,'IN MY ZONE') 
     end if

     if( kelem > 0 ) then
        !
        ! A partition has detected a negative Jacobian:
        ! look for element IELEM_MIN with lowest Jacobian GPDET_MIN
        !
        if( kfl_detection /= 0 ) then
           !
           ! Event detection: it occurs at the center of gravity of the element
           !
           if( INOTMASTER .and. ielem_min > 0 ) then
              do inode = 1,lnnod(ielem_min)
                 xcoor_min(1:ndime) = xcoor_min(1:ndime) + coord(1:ndime,lnods(inode,ielem_min))
              end do
              xcoor_min(1:ndime) = xcoor_min(1:ndime) / real(lnnod(ielem_min),rp)
           end if
           call ker_detection_min_max_value(&
                'MIN',gpdet_min,ndime,displ,xcoor_min,&
                'NEGATIVE_JACOBIAN','DISPL','AN_ELEMENT_HAS_NEGATIVE_JACOBIAN')
           call livinf(10000_ip,'A NEGATIVE JACOBIAN HAS BEEN DETECTED AS AN EVENT',0_ip)
           call runend('O.K.!')

        elseif ( kfl_detection == 0 .and. ledam_sld(kelem) == 0 ) then 
           ! 
           ! Print the information on screen
           !
           if( INOTMASTER .and. gpdet_min < 0.0_rp ) then
              write(*,*) '----> SLD_ELMope:  **ERROR** NEGATIVE GPDET. Check element ',ielem_min,' subdomain ',kfl_paral
              write(*,*) '      Value: ',gpdet_min
              write(*,*) '      Nodal coordinates: '
              if( kfl_xfeme_sld == 1 ) write(*,*) 'The element is enriched'
              do inode = 1,lnnod(ielem_min)
                 write(*,*) '      Node=',inode,'  Position=',elcod(1:ndime,inode)
              end do

              print *, "    ALYA:'"//trim(title)//"'"
              print *, " Element:", ielem_min 
              print *, "   Nodes:", lnods(:,ielem_min)
!             print *, " Coords:", coord(1:ndime,lnods(:,ielem_min))

           end if
           call runend('SLD_ELMOPE: NEGATIVE JACOBIAN(S) HAS(HAVE) BEEN FOUND')
         
        elseif ( kfl_detection == 0 .and. ledam_sld(kelem) > 0 ) then
           ! 
           ! Warning that there is a negative jacobian but coincides with a damaged element
           !
           if( INOTMASTER .and. gpdet_min < 0.0_rp ) then
              write(*,*) '----> SLD_ELMope:  **WARNING** NEGATIVE GPDET. Check damaged element ',ielem_min,' subdomain ',kfl_paral
              write(*,*) '      Value: ',gpdet_min
              write(*,*) '      Nodal coordinates: '

              print *, "    ALYA:'"//trim(title)//"'"
              print *, " Element:", ielem_min
              print *, "   Nodes:", lnods(:,ielem_min)
!             print *, " Coords:", coord(1:ndime,lnods(:,ielem_min))

           end if 
          
        end if

     end if

  end if

#ifdef MYTIMING
  ! Stop timing counter
  call cputim(time_end)
  time_sld_elmope = time_sld_elmope + abs(time_end-time_begin)
#endif

end subroutine sld_elmope
