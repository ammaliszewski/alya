subroutine sld_velsnd(pmate)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_updtss
  ! NAME
  !    sld_updtss
  ! DESCRIPTION
  !    This routine computes the velocity of sound for each material
  ! USED BY
  !    sld_
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  implicit none
  integer(ip), intent(in)    :: pmate
  real(rp)                   :: young,poiss,lame1,lame2,lambda0,mu0,K0


  if (ittim == 0_ip .or. kfl_rstar /= 0 ) then

     if (lawst_sld(pmate)==100) then
        young = parco_sld(1,pmate)
        poiss = parco_sld(2,pmate)
        lame1 = parco_sld(3,pmate) !lambda
        lame2 = parco_sld(4,pmate) !mu
        velas_sld(1,pmate) = sqrt((lame1+2.0_rp*lame2)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==101) then
        lambda0 = parco_sld(1,pmate)
        mu0     = parco_sld(2,pmate)
        velas_sld(1,pmate) = sqrt((lambda0+2.0_rp*mu0)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==102) then
        mu0  = parco_sld(1,pmate)
        K0   = parco_sld(2,pmate)
        velas_sld(1,pmate) = sqrt((K0+4.0_rp*mu0/3.0_rp)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==103) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==104) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==105) then
        lambda0 = parco_sld(1,pmate)
        velas_sld(1,pmate) = sqrt(lambda0/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==133) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==134) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==135) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==136) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==151) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==152) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     else if (lawst_sld(pmate)==153) then
        velas_sld(1,pmate) = sqrt(parco_sld(1,pmate)/densi_sld(1,pmate))
     end if

  end if

end subroutine sld_velsnd
