subroutine sld_travo4(irotype,tenold,Amatr,tennew,ndime)

   !----------------------------------------------------------------------------
   !> @addtogroup Solidz
   !> @{
   !> @file    sld_travo4.f90
   !> @author  Adria Quintanas (u1902492@campus.udg.edu)
   !> @date    Desember, 2014
   !> @brief   This subroutine performs a transformation of a tensor from a old
   !> @        basis to a new basis. 
   !> @details This subroutine performs a transformation of a tensor
   !> @           Compliance from Local CSYS to Global CSYS:
   !> @              H _{ij} = T _{pi}*T _{qj}*H'_{pq}
   !> @           Compliance from Global CSYS to Local CSYS:
   !> @              H'_{ij} = Tg_{ip}*Tg_{jq}*H _{pq}
   !> @           Stiffness  from Local CSYS to Global CSYS:
   !> @              C _{ij} = Tg_{pi}*Tg_{qj}*C'_{pq}
   !> @           Stiffness  from Global CSYS to Local CSYS:
   !> @              C'_{ij} = T _{ip}*T _{jq}*C _{pq}
   !> @
   !> @        Reference: E.J. Barbero - Finite Element Analysis of Composite 
   !> @           Materials
   !> @} 
   !----------------------------------------------------------------------------

   
   use      def_kintyp  
   
   implicit none
   
   integer(ip),    intent(in)     ::    irotype,ndime
   real(rp),       intent(in)     ::    Amatr(3, 3), tenold(ndime*2_ip, ndime*2_ip)
   real(rp),       intent(out)    ::    tennew(ndime*2_ip, ndime*2_ip)
   integer(ip)                    ::    i,j,p,q
   real(rp)                       ::    Tmatr(6, 6)
   
   ! -------------------------------
   ! INITIALIZE VARIABLES
   !
   tennew = 0.0_rp
   
   
   ! -------------------------------
   ! TRANSFORMATION MATRIX
   !    [T] and [Tg] are almost equal, only change some positions, then, the common part
   !    is defined here.
   Tmatr(1, 1) = Amatr(1, 1)*Amatr(1, 1)
   Tmatr(1, 2) = Amatr(1, 2)*Amatr(1, 2)
   Tmatr(1, 3) = Amatr(1, 3)*Amatr(1, 3)
   Tmatr(1, 4) = Amatr(1, 2)*Amatr(1, 3)
   Tmatr(1, 5) = Amatr(1, 1)*Amatr(1, 3)
   Tmatr(1, 6) = Amatr(1, 1)*Amatr(1, 2)
   Tmatr(2, 1) = Amatr(2, 1)*Amatr(2, 1)
   Tmatr(2, 2) = Amatr(2, 2)*Amatr(2, 2)
   Tmatr(2, 3) = Amatr(2, 3)*Amatr(2, 3)
   Tmatr(2, 4) = Amatr(2, 2)*Amatr(2, 3)
   Tmatr(2, 5) = Amatr(2, 1)*Amatr(2, 3)
   Tmatr(2, 6) = Amatr(2, 1)*Amatr(2, 2)
   Tmatr(3, 1) = Amatr(3, 1)*Amatr(3, 1)
   Tmatr(3, 2) = Amatr(3, 2)*Amatr(3, 2)
   Tmatr(3, 3) = Amatr(3, 3)*Amatr(3, 3)
   Tmatr(3, 4) = Amatr(3, 2)*Amatr(3, 3)
   Tmatr(3, 5) = Amatr(3, 1)*Amatr(3, 3)
   Tmatr(3, 6) = Amatr(3, 1)*Amatr(3, 2)
   Tmatr(4, 1) = Amatr(2, 1)*Amatr(3, 1)
   Tmatr(4, 2) = Amatr(2, 2)*Amatr(3, 2) 
   Tmatr(4, 3) = Amatr(2, 3)*Amatr(3, 3)
   Tmatr(4, 4) = Amatr(2, 2)*Amatr(3, 3) + Amatr(2, 3)*Amatr(3, 2)
   Tmatr(4, 5) = Amatr(2, 1)*Amatr(3, 3) + Amatr(2, 3)*Amatr(3, 1)
   Tmatr(4, 6) = Amatr(2, 1)*Amatr(3, 2) + Amatr(2, 2)*Amatr(3, 1)
   Tmatr(5, 1) = Amatr(1, 1)*Amatr(3, 1)
   Tmatr(5, 2) = Amatr(1, 2)*Amatr(3, 2)
   Tmatr(5, 3) = Amatr(1, 3)*Amatr(3, 3)
   Tmatr(5, 4) = Amatr(1, 2)*Amatr(3, 3) + Amatr(1, 3)*Amatr(3, 2)
   Tmatr(5, 5) = Amatr(1, 1)*Amatr(3, 3) + Amatr(1, 3)*Amatr(3, 1)
   Tmatr(5, 6) = Amatr(1, 1)*Amatr(3, 2) + Amatr(1, 2)*Amatr(3, 1)
   Tmatr(6, 1) = Amatr(1, 1)*Amatr(2, 1)
   Tmatr(6, 2) = Amatr(1, 2)*Amatr(2, 2)
   Tmatr(6, 3) = Amatr(1, 3)*Amatr(2, 3)
   Tmatr(6, 4) = Amatr(1, 2)*Amatr(2, 3) + Amatr(1, 3)*Amatr(2, 2)
   Tmatr(6, 5) = Amatr(1, 1)*Amatr(2, 3) + Amatr(1, 3)*Amatr(2, 1)
   Tmatr(6, 6) = Amatr(1, 1)*Amatr(2, 2) + Amatr(1, 2)*Amatr(2, 1)

   !
   ! SELECT CASE, COMPLIANCE / STIFFNESS FROM G->L / L->G
   !
   
   if ( irotype == 1_ip) then
      !
      ! Compliance tensor from LCSYS to GCSYS
      !    H_ {ij} = T_{pi}*T_{qj}*H'_{pq}
      !

      !
      ! Transformation matrix [T]
      !
      Tmatr(1, 4) = Tmatr(1, 4)*2.0_rp
      Tmatr(1, 5) = Tmatr(1, 5)*2.0_rp
      Tmatr(1, 6) = Tmatr(1, 6)*2.0_rp
      Tmatr(2, 4) = Tmatr(2, 4)*2.0_rp
      Tmatr(2, 5) = Tmatr(2, 5)*2.0_rp
      Tmatr(2, 6) = Tmatr(2, 6)*2.0_rp
      Tmatr(3, 4) = Tmatr(3, 4)*2.0_rp
      Tmatr(3, 5) = Tmatr(3, 5)*2.0_rp
      Tmatr(3, 6) = Tmatr(3, 6)*2.0_rp
	  

      !
      ! Matrix multiplication
      !
      do i = 1, 2_ip*ndime
         do j = 1, 2_ip*ndime
            do p = 1, 2_ip*ndime
               do q = 1, 2_ip*ndime
                  tennew(i, j) = tennew(i, j) +  Tmatr(p, i)*Tmatr(q, j)*tenold(p, q)
               end do 
             end do
         end do
      end do

   else if ( irotype == 2_ip) then
      !
      ! Compliance tensor from GCSYS to LCSYS
      !    H'_{ij} = Tg_{ip}*H _{pq}*Tg_{jq}
      !

      !
      ! Transformation matrix [Tg]
      !
      Tmatr(4, 1) = Tmatr(4, 1)*2.0_rp
      Tmatr(4, 2) = Tmatr(4, 2)*2.0_rp
      Tmatr(4, 3) = Tmatr(4, 3)*2.0_rp
      Tmatr(5, 1) = Tmatr(5, 1)*2.0_rp
      Tmatr(5, 2) = Tmatr(5, 2)*2.0_rp
      Tmatr(5, 3) = Tmatr(5, 3)*2.0_rp
      Tmatr(6, 1) = Tmatr(6, 1)*2.0_rp
      Tmatr(6, 2) = Tmatr(6, 2)*2.0_rp
      Tmatr(6, 3) = Tmatr(6, 3)*2.0_rp

      !
      ! Matrix multiplication
      !
      do i = 1, 2_ip*ndime
         do j = 1, 2_ip*ndime
            do p = 1, 2_ip*ndime
               do q = 1, 2_ip*ndime
                  tennew(i, j) = tennew(i, j) +  Tmatr(i, p)*Tmatr(j, q)*tenold(p, q)
               end do 
             end do
         end do
      end do

   else if ( irotype == 3_ip) then
      !
      ! Stiffness tensor from LCSYS to GCSYS
      !    C _{ij} = Tg_{pi}*C'_{pq}*Tg_{qj}
      !

      !
      ! Transformation matrix [Tg]
      !
      Tmatr(4, 1) = Tmatr(4, 1)*2.0_rp
      Tmatr(4, 2) = Tmatr(4, 2)*2.0_rp
      Tmatr(4, 3) = Tmatr(4, 3)*2.0_rp
      Tmatr(5, 1) = Tmatr(5, 1)*2.0_rp
      Tmatr(5, 2) = Tmatr(5, 2)*2.0_rp
      Tmatr(5, 3) = Tmatr(5, 3)*2.0_rp
      Tmatr(6, 1) = Tmatr(6, 1)*2.0_rp
      Tmatr(6, 2) = Tmatr(6, 2)*2.0_rp
      Tmatr(6, 3) = Tmatr(6, 3)*2.0_rp

      !
      ! Matrix multiplication
      !
      do i = 1, 2_ip*ndime
         do j = 1, 2_ip*ndime
            do p = 1, 2_ip*ndime
               do q = 1, 2_ip*ndime
                  tennew(i, j) = tennew(i, j) +  Tmatr(p, i)*Tmatr(q, j)*tenold(p, q)
               end do 
             end do
         end do
      end do

   else if ( irotype == 4_ip) then
      !
      ! Stiffness tensor from GCSYS to LCSYS
      !    C'_ {ij} = T _{ip}*T _{jq}*C_{pq}
      !
      
      !
      ! Transformation matrix [T]
      !
      Tmatr(1, 4) = Tmatr(1, 4)*2.0_rp
      Tmatr(1, 5) = Tmatr(1, 5)*2.0_rp
      Tmatr(1, 6) = Tmatr(1, 6)*2.0_rp
      Tmatr(2, 4) = Tmatr(2, 4)*2.0_rp
      Tmatr(2, 5) = Tmatr(2, 5)*2.0_rp
      Tmatr(2, 6) = Tmatr(2, 6)*2.0_rp
      Tmatr(3, 4) = Tmatr(3, 4)*2.0_rp
      Tmatr(3, 5) = Tmatr(3, 5)*2.0_rp
      Tmatr(3, 6) = Tmatr(3, 6)*2.0_rp
      
      !
      ! Matrix multiplication
      !
      do i = 1, 2_ip*ndime
         do j = 1, 2_ip*ndime
            do p = 1, 2_ip*ndime
               do q = 1, 2_ip*ndime
                  tennew(i, j) = tennew(i, j) +  Tmatr(i, p)*Tmatr(j, q)*tenold(p, q)
               end do 
             end do
         end do
      end do

   end if

end subroutine sld_travo4

