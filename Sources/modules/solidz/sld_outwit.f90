subroutine sld_outwit()
  !------------------------------------------------------------------------
  !****f* Nastin/sld_outwit
  ! NAME 
  !    sld_outwit
  ! DESCRIPTION
  !    Output results on witness points
  ! USES
  ! USED BY
  !    sld_output
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_solidz
  implicit none
  integer(ip) :: iwitn,ielem,inode,pnode,pelty,ipoin,ivawi,dummi,ifwit
  real(rp)    :: fb(3),st(6),stfib,fmod

  ! Check: write witness or not?
  ifwit= 0
  call posdef(5_ip,ifwit)

!  if( nwitn > 0 .and. maxval(postp(1) % npp_witne) > 0 ) then
!!  if (volst_sld(2,1) < 15.0) ifwit=1

  if (ifwit == 1) then
  
     if( INOTMASTER ) then 

        !
        ! SIGFI
        !
        if( postp(1) % npp_witne(11) == 1 ) then
           kfote_sld= kfote_sld + 1
           if (kfote_sld == 1) then   ! fotens computes caust, green and lepsi, so do it only the first kfote
              call sld_fotens
           end if
           call sld_fidbes()
        end if

        do iwitn = 1, nwitn
           ielem = lewit(iwitn)
           if( ielem > 0 ) then

              pelty = ltype(ielem)
              pnode = nnode(pelty)
              do ivawi = 1,postp(1) % nvawi
                 witne(ivawi,iwitn) = 0.0_rp
              end do
              !
              ! U
              !
              if( postp(1) % npp_witne(1) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(1,iwitn) = witne(1,iwitn) &
                         + shwit(inode,iwitn) *  displ(1,ipoin,1) 
                 end do
              end if
              !
              ! V
              !
              if( postp(1) % npp_witne(2) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(2,iwitn) = witne(2,iwitn) &
                         + shwit(inode,iwitn) *  displ(2,ipoin,1) 
                 end do
              end if
              !
              ! W
              !
              if( postp(1) % npp_witne(3) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(3,iwitn) = witne(3,iwitn) &
                         + shwit(inode,iwitn) *  displ(3,ipoin,1) 
                 end do
              end if

              !
              ! SIGXX
              !
              if( postp(1) % npp_witne(4) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(4,iwitn) = witne(4,iwitn) + shwit(inode,iwitn) * caust_sld(1,ipoin)
                 end do
              end if
              !
              ! SIGYY
              !
              if( postp(1) % npp_witne(5) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(5,iwitn) = witne(5,iwitn) + shwit(inode,iwitn) * caust_sld(2,ipoin)
                 end do
              end if
              !
              ! SIGXY 
              !
              if( postp(1) % npp_witne(9) == 1 ) then
                 if( ndime == 2 ) then
                    do inode = 1,pnode
                       ipoin = lnods(inode,ielem)
                       witne(9,iwitn) = witne(9,iwitn) + shwit(inode,iwitn) * caust_sld(3,ipoin)
                    end do
                 else 
                    do inode = 1,pnode
                       ipoin = lnods(inode,ielem)
                       witne(9,iwitn) = witne(9,iwitn) + shwit(inode,iwitn) * caust_sld(6,ipoin)
                    end do
                 end if
              end if
              !
              ! SIGZZ
              !
              if( postp(1) % npp_witne(6) == 1 .and. ndime == 3 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(6,iwitn) = witne(6,iwitn) + shwit(inode,iwitn) * caust_sld(3,ipoin)
                 end do
              end if
              !
              ! SIGYZ
              !
              if( postp(1) % npp_witne(7) == 1 .and. ndime == 3 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(7,iwitn) = witne(7,iwitn) + shwit(inode,iwitn) * caust_sld(4,ipoin)
                 end do
              end if
              !
              ! SIGZX
              !
              if( postp(1) % npp_witne(8) == 1 .and. ndime == 3 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(8,iwitn) = witne(8,iwitn) + shwit(inode,iwitn) * caust_sld(5,ipoin)
                 end do
              end if
              !
              ! SEQVM
              !
              if( postp(1) % npp_witne(10) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(10,iwitn) = witne(10,iwitn) + shwit(inode,iwitn) * seqvm_sld(ipoin)
                 end do
              end if
              !
              ! SIGFI
              !
              if( postp(1) % npp_witne(11) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    st(1:6)= caust_sld(1:6,ipoin)
                    fb(1:3)= fibde_sld(1:3,ipoin)
                    fmod= fb(1)*fb(1)+fb(2)*fb(2)+fb(3)*fb(3)
                    stfib= 0.0_rp
                    if (fmod > 1.0e-10) then
                       stfib= (st(1)*fb(1)+st(6)*fb(2)+st(5)*fb(3))*fb(1)&
                            + (st(6)*fb(1)+st(2)*fb(2)+st(4)*fb(3))*fb(2)&
                            + (st(5)*fb(1)+st(4)*fb(2)+st(3)*fb(3))*fb(3)
                       stfib= stfib/fmod
                    end if
                    witne(11,iwitn) = witne(11,iwitn) + shwit(inode,iwitn) * stfib
                 end do
              end if


           end if
        end do

     end if
     !
     ! Parall
     !
     call posdef(24_ip,dummi)

  end if

end subroutine sld_outwit

