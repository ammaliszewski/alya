!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_solexp.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Explicit time advance
!> @details Explicit time advance
!> @} 
!-----------------------------------------------------------------------
subroutine sld_solexpviejuno()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_solexp
  ! NAME
  !    sld_solexp
  ! DESCRIPTION
  !    This routine advances time explicitly
  ! USES
  ! USED BY
  !    sld_solite
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_elmtyp
  use def_domain
  use def_solidz
  implicit none
  integer(ip) :: ipoin,kpoin,idime,itott,jpoin,jtott,incnt,iplot,izone
  real(rp)    :: dt,dt2,fact1,fact2,xnuma,vauxi(3)
  logical     :: plotDebug

  plotDebug = .false.

  if( kfl_timet_sld == 1 ) then    ! Explicit scheme

     if( INOTMASTER ) then

        dt  = dtime
        dt2 = dtime * dtime
        !
        ! Initialize mass matrix
        !
        if (kfl_xfeme_sld == 1) then
           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
              vmass_sld(ipoin) = 0.0_rp
              vmasx_sld(ipoin) = 0.0_rp
           end do
        else
           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
              vmass_sld(ipoin) = 0.0_rp
           end do
        end if

     end if

     if( kfl_tisch_sld == 1 ) then

        !----------------------------------------------------------------
        !
        ! Update the unknowns, i.e. the displacements using an explicit
        ! centered differences scheme:
        !
        ! ( u(:,:,1) - 2 * u(:,:,3) + u(:,:,4) ) / dt^2 = RHS^n
        !
        !----------------------------------------------------------------
        !
        ! Important !!
        ! Calculation of rhsid in solite has been moved for the central diff.
        !
        call sld_matrix(1_ip)
        !
        ! Paralle exchange of mass matrix
        !
        if( INOTMASTER ) then

           call rhsmod(1_ip,vmass_sld)
           if (kfl_xfeme_sld == 1) call rhsmod(1_ip,vmasx_sld)
           !
           ! Sum up residual contribution of slave neighbors in parallel runs
           !
           call sld_parall(3_ip)
           !
           ! Enrichement: define RHS
           !
           call sld_enrich(2_ip)
           !
           ! Update nodes
           !


           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)


              !**DT: warning, solidz explicit only works with ONE material definition
              ! in the further implementation for more than one material definition,
              ! the densi_sld(1,pmate) should go into vmass(ipoin) such that different
              ! material definition goes directly into the lumped-mass calculation and
              ! automatically included within the vmass(ipoin)

              !           fact1 = dt2 / (densi_sld(1,1)*vmass(ipoin))
              fact1 = dt2 / vmass_sld(ipoin)
              if (kfl_xfeme_sld == 1) fact2 = dt2 / vmasx_sld(ipoin)
              itott = (ipoin-1) * ndofn_sld

              if ( kfl_fixno_sld(1,ipoin) == 2 ) then          ! fixed displacements (local curvilinear framework)
                 call sld_rotunk( 1_ip ,ipoin,ipoin,rhsid(itott+1:itott+ndime))  ! rotate
                 rhsid(itott+1) = 0.0_rp                                         ! fix normal value
                 call sld_rotunk(-1_ip ,ipoin,ipoin,rhsid(itott+1:itott+ndime))  ! rotate back
              end if

              do idime = 1,ndime
                 itott        = itott+1
                 xnuma        = 2.0_rp * displ(idime,ipoin,3) - displ(idime,ipoin,4)

                 unkno(itott) = xnuma + rhsid(itott) * fact1

                 if( kfl_fixno_sld(idime,ipoin) == 1 ) then          ! fixed displacements (cartesian framework)
                    unkno(itott) = bvess_sld(idime,ipoin,1)
                 end if

                 accel_sld(idime,ipoin,1) = ( unkno(itott) - xnuma) / dt2
                 displ(idime,ipoin,1)     =   unkno(itott)
                 

              end do


              !**DT: central difference scheme for enriched unknowns
              if (kfl_xfeme_sld == 1) then
                 do idime = 1,ndime

                    itott        = itott+1
                    xnuma        = 2.0_rp * dxfem_sld(idime,ipoin,3) - dxfem_sld(idime,ipoin,4)
                    unkno(itott) = xnuma + rhsid(itott) * fact2
                    if( lnenr_sld(ipoin) == 0 ) then  ! standard nodes (not enriched)
                       unkno(itott) = 0.0_rp
                    end if
                    axfem_sld(idime,ipoin,1) = ( unkno(itott) - xnuma) / dt2
                    dxfem_sld(idime,ipoin,1) =   unkno(itott)
                 end do
              end if

              if (plotDebug) then
                 write(lun_livei,'(6(e12.4,x),x)')(displ(iplot,ipoin,1),iplot=1,ndime),&
                      (dxfem_sld(iplot,ipoin,1),iplot=1,ndime)
              end if


           end do

        end if

     else if( kfl_tisch_sld == 2 ) then

        !----------------------------------------------------------------
        !
        ! Update the unknowns, i.e. the displacements using an explicit
        ! alpha-generalized Newmark scheme:
        !
        ! u^n+1 = u^n + dt v^n + 0.5 dt^2 a^n
        ! a^n+1 = 1/(1-alphaM)/Mlump  RHS^n+1 - alphaM/(1-alphaM) a^n
        ! v^n+1 = v^n + dt (1-gamma) a^n + gamma a^n+1
        !
        !----------------------------------------------------------------
        !
        ! Update displacements:
        !
        ! u^n+1 = u^n + dt v^n + 0.5 dt^2 a^n
        !
        if( INOTMASTER ) then

           fact1 = tifac_sld(1)
           fact2 = 0.5_rp - tifac_sld(1)

           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)

              itott = (ipoin-1) * ndofn_sld

              do idime = 1,ndime
                 itott = itott + 1
                 unkno(itott) = displ(idime,ipoin,3) + dt * veloc_sld(idime,ipoin,3) &
                      + 0.5_rp * dt2 * accel_sld(idime,ipoin,3)
                 if( kfl_fixno_sld(idime,ipoin) == 1 ) then     ! fixed displacements (cartesian framework)
                    unkno(itott) = bvess_sld(idime,ipoin,1)
                 end if
                 displ(idime,ipoin,1) = unkno(itott)

                 if(kfl_prest_sld == 1) then                         !Save displacements if prestress
                      ddisp_sld(idime,ipoin,2) = ddisp_sld(idime,ipoin,1)
                      ddisp_sld(idime,ipoin,1) = unkno(itott)
                      unkno(itott)= 0.0_rp
                 end if 
            end do

              !**DT: explicit Newmark scheme for enriched unknowns
              if (kfl_xfeme_sld == 1) then
                 do idime = 1,ndime
                    itott = itott + 1
                    unkno(itott) = dxfem_sld(idime,ipoin,3) + dt * vxfem_sld(idime,ipoin,3) &
                         + 0.5_rp * dt2 * axfem_sld(idime,ipoin,3)
                    if( lnenr_sld(ipoin) == 0 )then     ! standard nodes
                       unkno(itott) = 0.0_rp
                    end if
                    dxfem_sld(idime,ipoin,1) = unkno(itott)
                 end do
              end if

           end do
        end if
        !
        ! Contact: Impose displacement(ALE/fluid side) = displacement(solid side)
        !

        !
        ! Calculate RHS and update acceleration:
        !
        ! a^n+1 = 1/(1-alphaM)/Mlump  RHS^n+1 - alphaM/(1-alphaM) a^n
        !
        ! Important!!!!
        ! Calculation of rhsid has to be done here after the update of displacement!
        ! but before acceleration, because it is required for updating the acceleration
        ! compute the forces, store them in rhsid  (step a, bely, p324)
        !
        call sld_matrix(1_ip)

        if( INOTMASTER ) then
           !
           ! Paralle exchange of mass matrix
           !
           call rhsmod(1_ip,vmass_sld)
           if (kfl_xfeme_sld == 1) call rhsmod(1_ip,vmasx_sld)
           !
           ! Sum up residual contribution of slave neighbors in parallel runs
           !
           call sld_parall(3_ip)
           !
           ! Enrichement: define RHS
           !
           call sld_enrich(2_ip)
           !
           fact1 = 1.0_rp       / (1.0_rp - tifac_sld(3))
           fact2 = tifac_sld(3) / (1.0_rp - tifac_sld(3))

           !**DT: warning, solidz explicit only works with ONE material definition
           ! in the further implementation for more than one material definition,
           ! the densi_sld(1,pmate) should go into vmass(ipoin) such that different
           ! material definition goes directly into the lumped-mass calculation and
           ! automatically included within the vmass(ipoin)

           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)

              itott = (ipoin-1) * ndofn_sld

              if ( kfl_fixno_sld(1,ipoin) == 2 ) then          ! fixed displacements (local curvilinear framework)
                 call sld_rotunk( 1_ip ,ipoin,ipoin,rhsid(itott+1:itott+ndime))  ! rotate
                 rhsid(itott+1) = 0.0_rp                                         ! fix normal value
                 call sld_rotunk(-1_ip ,ipoin,ipoin,rhsid(itott+1:itott+ndime))  ! rotate back
              end if

              do idime = 1,ndime
                 itott = itott+1
                 !              accel_sld(idime,ipoin,1)= rhsid(itott) * fact1 / vmass_sld(ipoin) &
                 !                   - fact2*accel_sld(idime,ipoin,2)
                 accel_sld(idime,ipoin,1) = fact1 * rhsid(itott) / &
                      vmass_sld(ipoin) &
                      + fact2 * accel_sld(idime,ipoin,3)
              end do

              !**DT: explicit Newmark scheme for enriched unknowns
              if (kfl_xfeme_sld == 1) then
                 do idime = 1,ndime
                    itott = itott+1
                    axfem_sld(idime,ipoin,1) = fact1 * rhsid(itott) / &
                         vmasx_sld(ipoin) &
                         + fact2 * axfem_sld(idime,ipoin,3)
                 end do
              end if

           end do

!        write(6,*) 'v',rhsid(1:6)
!        write(6,*) 'v',fact2
!        write(6,*) 'v',accel_sld(1:3,1,1)
!        stop



           !
           ! Update velocity (and boundary conditions):
           !
           ! v^n+1 = v^n + dt (1-gamma) a^n + gamma a^n+1
           !
           fact1 = tifac_sld(2)
           fact2 = 1.0_rp - tifac_sld(2)

           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)

              itott = (ipoin-1) * ndofn_sld
              do idime = 1,ndime
                 itott = itott + 1
                 veloc_sld(idime,ipoin,1) = veloc_sld(idime,ipoin,3) &
                      + dt * fact1 * accel_sld(idime,ipoin,1)        &
                      + dt * fact2 * accel_sld(idime,ipoin,3)
                 !
                 ! Velocity and acceleration at fixed displacements
                 !
                 if( kfl_fixno_sld(idime,ipoin) == 1 ) then
                    veloc_sld(idime,ipoin,1) = (unkno(itott) - displ(idime,ipoin,3)) / dt
                    accel_sld(idime,ipoin,1) = (veloc_sld(idime,ipoin,1) - &
                         veloc_sld(idime,ipoin,3)) / dt
                 end if
              end do

              !**DT: explicit Newmark scheme for enriched unknowns
              if (kfl_xfeme_sld == 1) then
                 do idime = 1,ndime
                    itott = itott + 1
                    vxfem_sld(idime,ipoin,1) = vxfem_sld(idime,ipoin,3) &
                         + dt * fact1 * axfem_sld(idime,ipoin,1)        &
                         + dt * fact2 * axfem_sld(idime,ipoin,3)
                 end do
              end if

           end do

        end if

     end if

  end if
  !
  ! Update boundary conditions (*,3) <- (*,2)
  !
  if( INOTMASTER ) call sld_updbcs(4_ip)

end subroutine sld_solexpviejuno
