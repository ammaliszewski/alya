subroutine sld_iniunk()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_iniunk
  ! NAME
  !    sld_iniunk
  ! DESCRIPTION
  !    This routine sets up the initial condition.
  !    If this is a restart, initial condition are loaded somewhere else.
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use mod_cutele 
  use mod_memchk
  implicit none
  integer(ip) :: ipoin,kpoin,idime,icomp,pelty,ielem,pnode,pgaus,ktask
  integer(ip) :: inode,kelem,dummi,izone
  integer(4)  :: istat
  real(rp)    :: dummr
  real(rp)    :: gpcod(ndime,mgaus)                    ! Gauss points' physical coordinates
  real(rp)    :: gphea(mnode,mgaus)                    ! Shifted Heaviside (XFEM)

  if( kfl_rstar == 0 ) then

     ittot_sld=0   ! Total iterations counter
     
     if( INOTMASTER ) then
        !
        ! Assign initial values
        !
        icomp=min(3,ncomp_sld)
        izone = lzone(ID_SOLIDZ)
        do kpoin=1,npoiz(izone); ipoin=lpoiz(izone) % l(kpoin)
           do idime=1,ndime
              ! If (kfl_indis_sld(2) == 1) an initial displacement is given as a field              
              if (kfl_indis_sld(2) == 0) &
                   displ(idime,ipoin,icomp)=bvess_sld(idime,ipoin,1)
              veloc_sld(idime,ipoin,icomp) = 0.0_rp
              accel_sld(idime,ipoin,icomp) = 0.0_rp
           end do
        end do

        if (kfl_xfeme_sld > 0) then
           !
           ! Allocate memory
           !
           allocate(lcrkf_sld(nfacg),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'LCRKF_SLD','sld_iniunk',lcrkf_sld)
           allocate(cockf_sld(ndime,nfacg),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'COCKF_SLD','sld_iniunk',cockf_sld)
           allocate(crtip_sld(ndime,nfacg),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'CRTIP_SLD','sld_iniunk',crtip_sld)
           !
           ! XFEM: initialize crapx: element center and cranx: preexisting crack
           !
           call sld_craini(1_ip)
           call sld_enrich(1_ip)
           !
           ! If the element is CUT
           !
           call cutele(1_ip,cranx_sld,crapx_sld,lcrkf_sld)
           !
           ! Initialize internal variables of cohesive laws and contact/friction
           !
           call sld_updcoh(0_ip)

        end if
      
        if (kfl_elcoh > 0) then
           !
           ! Initialize vector of nodes where the cohesive law is activated
           !
           call sld_updcoh(0_ip)

        end if
   
     end if

  else
     !
     ! Read restart file
     !
     call sld_restar(1_ip)

  end if  

  !
  ! Actualize current iteration DISPL(:,:,1) <= DISPL(:,:,icomp)
  !
  call sld_updunk(6_ip)

  !
  ! Coupling initializations
  !
  
  call sld_coupli(ITASK_INIUNK)

end subroutine sld_iniunk
