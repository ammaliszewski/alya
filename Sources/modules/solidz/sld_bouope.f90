!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_bouope.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   This routine computes boundary operations
!> @details This routine computes boundary operations
!> @} 
subroutine sld_bouope(itask)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_bouope
  ! NAME 
  !    sld_bouope
  ! DESCRIPTION
  !    boundary operations:
  !    1. Compute elemental matrix and RHS 
  !    2. Impose Dirichlet boundary conditions
  !    3. Assemble them
  ! USES
  !    sld_bouave
  !    sld_bougat
  !    sld_elmpro
  !    bouder
  !    chenor
  !    sld_bouwal
  !    elmder
  !    cartbo
  !    sld_bouopb
  !    sld_elmdir
  !    sld_assrhs
  !    assmat
  ! USED BY
  !    sld_matrix
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_solidz
  use mod_commdom_sld, only: commdom_sld_space_funcion
  implicit none

  integer(ip), intent(in) :: itask   !< who is calling sld_bouope
  integer(ip) :: ielem,ipoin,idime,iboun,igaub,inodb,idofn
  integer(ip) :: pelty,pblty,pnodb,pgaub,pnode,inode,ipass
  real(rp)    :: elrhs(nevat_sld)
  real(rp)    :: baloc(ndime,ndime)
  real(rp)    :: bocod(ndime,mnodb),elcod(ndime,mnode)
  real(rp)    :: gbsur,eucta,tract(ndime),gppre
  !
  ! Loop over boundaries
  !

!  call commdom_sld_space_funcion( bou_prop=bvnat_sld(1,1:nboun,1), fixbo=7_ip, idime=1_ip )  !< 2015Jul30  



  if (kfl_icodb .ne. 0) then

     boundaries: do iboun=1,nboun

        if(kfl_fixbo_sld(iboun)==2 &
             .or. kfl_fixbo_sld(iboun)==3 &
             .or. kfl_fixbo_sld(iboun)==6 &
             .or. kfl_fixbo_sld(iboun)==7 &                                                  !< 2015Jul30 
             .or. ( kfl_fixbo_sld(iboun)==4 .and. kfl_cycle_sld==1 ) ) then
           !
           ! fixbo=2  --> Nodal pressure (in press)
           ! fixbo=3  --> Boundary element pressure (in bvnat, as a traction on a LOCAL basis)
           ! fixbo=5  --> Nodal traction coming from a field (in bvess)
           ! fixbo=6  --> Boundary element traction (in bvnat, as a traction on a GLOBAL basis)
           !
           ! Element properties and dimensions
           !
           pblty=ltypb(iboun) 
           pnodb=nnode(pblty)
           pgaub=ngaus(pblty)
           ielem=lboel((pnodb+1),iboun)
           pelty=ltype(ielem)
           pnode=nnode(pelty)
           !
           ! Initialize
           !
           do idofn=1,ndime*pnode
              elrhs(idofn)=0.0_rp
           end do
           !
           ! Gather operations: BOCOD
           !
           do inodb=1,pnodb
              ipoin=lnodb(inodb,iboun)
              do idime=1,ndime
                 bocod(idime,inodb) = coord(idime,ipoin)! + displ(idime,ipoin,1)
              end do
           end do
           do inode=1,pnode
              ipoin=lnods(inode,ielem)
              do idime=1,ndime
                 elcod(idime,inode)=coord(idime,ipoin)! + displ(idime,ipoin,1)
              end do
           end do
           !
           ! Loop over Gauss points
           !
           gauss_points: do igaub=1,pgaub
              call bouder(&
                   pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&
                   bocod,baloc,eucta)    
              gbsur=elmar(pblty)%weigp(igaub)*eucta 
              call chenor(&                                           ! Check normal
                   pnode,baloc,bocod,elcod)           
              if(kfl_fixbo_sld(iboun)==2) then              
                 !              
                 ! pressure force(igaus) = n_i * p(igaus) * gbsur
                 !                       = baloc(1:ndime,ndime) * p(igaus) * gbsur
                 !
                 ! baloc(1:ndime,ndime) --> exterior normal vector
                 !
                 gppre= 0.0_rp
                 do inodb = 1,pnodb
                    ipoin = lnodb(inodb,iboun)
                    gppre = gppre + elmar(pblty)%shape(inodb,igaub) * press(ipoin,1) 
                 end do
                 
                 tract(1:ndime) = baloc(1:ndime,ndime) * gppre
                 
              else if (kfl_fixbo_sld(iboun)==3) then         
                 
                 ! 1_ip  means "boundary elements"
                 if (nfunc_sld > 0) call sld_funbou( 1_ip, iboun,bvnat_sld(1,iboun,1))   
                 
                 gppre = bvnat_sld(1,iboun,1)                 
                 
                 tract(1:ndime) = baloc(1:ndime,ndime) * gppre
                 
              else if (kfl_fixbo_sld(iboun)==4 .and. kfl_cycle_sld==1) then  
                 !The CCMT is called from sld_endite    
                 if (kfl_codbo(iboun)==1) then
                    gppre=ptota_sld(1)  !LV
                 else 
                    gppre=ptota_sld(1)/2.0_rp  !RV: simply scale the pressure calculated bu the LV
                    ptota_sld(2)=gppre !for writing
                    !gppre=0.0_rp
                 end if
                 
                 tract(1:ndime) = baloc(1:ndime,ndime) * gppre
                 
              else if (kfl_fixbo_sld(iboun)==6) then         
                 
                 
                 tract(1:ndime) = bvnat_sld(1:ndime,iboun,1)   
                 

              else if (kfl_fixbo_sld(iboun)==7) then                               !< 2015Jul30 
                 ! 
                 !  bvnat_sld(1,iboun,1) == F(x)
                 ! 
                 tract(1:ndime) = baloc(1:ndime,ndime) * bvnat_sld(1,iboun,1)  


                 
              end if  !kfl_fixbo_sld(iboun)  
              
              
              do inodb=1,pnodb
                 idofn=(lboel(inodb,iboun)-1)*ndime
                 ipoin=lnodb(inodb,iboun) 
                 do idime=1,ndime
                    idofn=idofn+1
                    if(kfl_fixno_sld(idime,ipoin)/=1) then
                       elrhs(idofn)=elrhs(idofn) + gbsur*tract(idime) * elmar(pblty)%shape(inodb,igaub)
                    end if
                 end do
              end do
              
           end do gauss_points
           !
           ! Assembly 
           !
           call assrhs(&
                ndofn_sld,pnode,lnods(1,ielem),elrhs,rhsid)
           
        end if  !kfl_fixbo_sld(iboun)==2 .or. 3 .or. kfl_codbo(iboun) /= 100
        
     end do boundaries
     
  end if



end subroutine sld_bouope
