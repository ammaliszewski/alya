subroutine sld_user_materials(pgaus,pmate,temp0,temp1,&
        gpgi0,gpgi1,gpigd,gpdet,gprat,gppio,gpivo,gpene,flagt,&
        gptmo,flags,ggsgo,ielem)

!-----------------------------------------------------------------------
!****f* Solidz/sld_user_materials
! NAME
!    sld_user_materials
! DESCRIPTION
!    Interface for calling user material constitutive laws
! INPUT
!    PGAUS ... Number of Gauss points 
!    PMATE ... Material number
!    TEMP0 ... Initial temperature ............................. temp(n)
!    TEMP1 ... Updated temperature ............................. temp(n+1)
!    GPGI0 ... Initial deformation gradient tensor ............. F(n)
!    GPGI1 ... Updated deformation gradient tensor ............. F(n+1)
!    GPIGD ... Inverse of updated deformation gradient tensor .. F^{-1}(n+1)
!    GPDET ... Updated jacobian ................................ J = det(F(n+1))
!    GPRAT ... Rate of Deformation tensor ...................... Fdot = grad(phidot)
!    FLAGT ... Flag for tangent moduli calculations ............ 1 if yes; 0 if no
!    FLAGS ... Flag for strain gradient calculations ........... 1 if yes; 0 if no
!    IELEM ... Number of the element ........................... I_e
! INPUT/OUTPUT
!    GPPIO ... 1st Piola-Kirchhoff stress tensor at t(n)/t(n+1)  P(n)/P(n+1)
!    GPIVO ... Internal variable array at t(n)/t(n+1) .......... Q(n)/Q(n+1)
!    GPENE ... Stored energy function .......................... W
! OUTPUT
!    GPTMO ... Tangent moduli at t(n+1) ........................ dP/dF(n+1)
!    GGSGO ... Deformation gradient gradient tensor ............ dF/dX(n+1)
! USES
!    elmcla_umat .............. Interface subroutine for UMAT
!    elmcla_vumat ............. Interface subroutine for VUMAT
!    (other commercial subroutines to be added as necessary)
! USED BY
!    sld_elmcla
!***
!-----------------------------------------------------------------------

  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime
  use def_solidz
  use def_master, only       :  ittim
  
  implicit none
  integer(ip), intent(in)    :: pgaus,pmate,ielem
  real(rp),    intent(in)    :: temp0(pgaus)
  real(rp),    intent(in)    :: temp1(pgaus)
  real(rp),    intent(in)    :: gpgi0(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpgi1(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpigd(ndime,ndime,pgaus)    
  real(rp),    intent(in)    :: gpdet(pgaus)    
  real(rp),    intent(in)    :: gprat(ndime,ndime,pgaus)
  integer(ip), intent(in)    :: flagt,flags
  real(rp),    intent(inout) :: gppio(ndime,ndime,pgaus)
  real(rp),    intent(inout) :: gpivo(nvint_sld,pgaus)
  real(rp),    intent(inout) :: gpene(pgaus)  
  real(rp),    intent(out)   :: gptmo(ndime,ndime,ndime,ndime,pgaus)
  real(rp),    intent(out)   :: ggsgo(ndime,ndime)


  ! call interface for user material subroutines

  ! Compute sound velocity only at time-step 0 (initialization)
  if (ittim == 0_ip) then
     velas_sld(1,pmate) = sqrt((parco_sld(2,pmate)+4.0_rp*parco_sld(1,pmate))/densi_sld(1,pmate))
  end if

  ! UMAT: 1
  if (lawco_sld(pmate) == 6) then
!     call sld_umat_bridge(pgaus,pmate,temp0,temp1,gpgi0,gpgi1,gpigd,&
!     gpdet,gprat,gppio,gpivo,gpene,flagt,gptmo,flags,ggsgo,ielem)


  ! VUMAT: 2
  else if (lawco_sld(pmate) == 5) then
     call sld_vumat_bridge(pgaus,pmate,temp0,temp1,gpgi0,gpgi1,gpigd,&
     gpdet,gprat,gppio,gpivo,gpene,flagt,gptmo,flags,ggsgo,ielem)
     
  end if

end subroutine sld_user_materials
