subroutine sld_endite(itask)
!-----------------------------------------------------------------------
!****f* Solidz/sld_endite
! NAME 
!    sld_endite
! DESCRIPTION
!    This routine checks convergence and updates unknowns at:
!    - itask=1 The end of an internal iteration
!    - itask=2 The end of the internal loop iteration
! USES
!    sld_cvgunk
!    sld_updunk
! USED BY
!    sld_doiter
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solidz
  use      def_solver, only : iters,resi1
  implicit none
  integer(ip) :: itask,idime,ipoin,jdime
  real(rp)    :: dummr
  character(300)   :: messa

  select case(itask)

  case(one) 

     !Calculate the volume inside the myocardium
     if (kfl_volca_sld==1_ip)  then
!        call sld_volume()
        call sld_volume_bound()
     end if

     !
     !  Compute convergence residual of the internal iteration (that is,
     !  || u(n,i,j) - u(n,i,j-1)|| / ||u(n,i,j)||) and update unknowns:
     !  u(n,i,j-1) <-- u(n,i,j)
     !
     call sld_cvgunk(1_ip)
     call sld_updunk(3_ip)  
     call sld_updunk(7_ip)  

     !if (kfl_arcle_sld == 1) then
     !   if (itinn(modul) >= miinn_sld) then
     !      deltaE_sld = (0.5_rp**0.25_rp)*enerG_sld
     !   end if
     !end if

     !  if (kfl_goite_sld == 1) call sld_updunk(8_ip)  ! relaxation... seems to be more robust

     call livinf(56_ip,' ',modul)
     call livinf(16_ip,' ',itinn(modul))
     messa = ' (SUBIT: '//trim(intost(itinn(modul)))//'  IT: '//trim(intost(iters))//')'

  case(two)

     !
     !  Compute convergence residual of the external iteration (that is,
     !  || u(n,i,*) - u(n,i-1,*)|| / ||u(n,i,*)||) and update unknowns:
     !  u(n,i-1,*) <-- u(n,i,*)
     !
     call sld_cvgunk( two)
     call sld_updunk(4_ip)  

     call sld_coupli(ITASK_ENDITE)

     if (kfl_cohes_sld > 0 .and. kfl_xfeme_sld > 0) call sld_updcoh(two)
     !
     ! CARDIAC CYCLE MANAGEMENT TOOL (CCMT). Return ptota_sld
     !  
     if (kfl_cycle_sld==1) then 
         call sld_ccmtol()
     end if

  end select


end subroutine sld_endite
