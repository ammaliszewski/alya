subroutine sld_updbcs(itask)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_updbcs
  ! NAME 
  !    sld_updbcs
  ! DESCRIPTION
  !    This routine updates the displacement boundary conditions:
  !    1. Before a time step
  !    2. After a time step
  ! USED BY
  !    sld_begste
  !***
  !-----------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      def_solidz
  use      def_parame
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,kpoin,idime,iboun,idumm,idofn,kfunn,itime,kfuty,izone
  real(rp)                :: dinew(ndime),diold(ndime),rdumm,forcv(2)


  if (itask == 0 .or. itask==1) then
     !  
     ! Before a time step
     !  
     ! Compute endo pressure        
     if (nfunc_sld > 0) then
        call sld_funbou(1_ip,-1_ip,forcv)
        ! pressure is negative when it is against the wall, so the pressure is computed with the opposite sign
        epres = - forcv(1)
        ptota_sld(1) = - forcv(1)
        ptota_sld(2) = - forcv(1)
     end if

     if( INOTMASTER ) then

        if (kfl_conbc_sld==0) then       
           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
              !
              ! Function: U(t)=f(t)*U(0)
              !
              kfunn = kfl_funno_sld(ipoin)           
              if( kfunn /= 0 ) then 
                 dinew(1:ndime) = 0.0_rp
                 diold(1:ndime) = bvess_sld(1:ndime,ipoin,2)
                 call sld_funbou( 2_ip, ipoin,dinew)             
                 bvess_sld(1:ndime,ipoin,1) = dinew(1:ndime)
              end if
           end do
        end if
        if (kfl_bodyf_sld > 0) then
           dinew(1:ndime) = 0.0_rp
           call sld_funbou( 3_ip, ipoin,dinew)
           bodyf_sld(1:ndime) = dinew(1:ndime)
        end if
     end if

  else if (itask == 2) then

  else if (itask == 4) then
     !
     ! After a time step
     !  
     if( INOTMASTER ) then
        ! update bvess
        !izone = lzone(ID_SOLIDZ)
        !do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
        !   do idime=1,ndime
        !      bvess_sld(idime,ipoin,3)= bvess_sld(idime,ipoin,2)
        !   end do
        !end do
     end if

  end if

end subroutine sld_updbcs
