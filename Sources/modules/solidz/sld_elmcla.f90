subroutine sld_elmcla(&
     itask,pgaus,pmate,gpcau,gpgi0,gpgi1,gpene,gpstr,gppio,gpdet,gprat,&
     gpigd,gptlo,gpivo,gptmo,nfibe,ielem,elcod,pnode,lnods,gpsha,gpdds,gpmof)

!-----------------------------------------------------------------------
!****f* Solidz/sld_elmcla
! NAME
!    sld_elmcla
! DESCRIPTION
!    Call built-in or user materials interface
! INPUT
!    PGAUS ... Number of Gauss points
!    PMATE ... Material number
!    GPCAU ... Updated right Cauchy-Green tensor ............... C(n+1)
!    GPGI1 ... Updated deformation gradient tensor ............. F(n+1)
!    GPDET ... Updated jacobian ................................ J = det(F(n+1))
!    GPRAT ... Rate of Deformation tensor ...................... Fdot = grad(phidot)
!    GPIGD ... Inverse of updated deformation gradient tensor .. F^{-1}(n+1)
!    IELEM ... Number of the element ........................... I_e
! INPUT/OUTPUT
!    GPENE ... Stored energy function .......................... W(n)/W(n+1)
!    GPSTR ... 2nd Piola-Kirchhoff stress tensor at t(n)/t(n+1)  S(n)/S(n+1)
!    GPPIO ... 1st Piola-Kirchhoff stress tensor at t(n)/t(n+1)  P(n)/P(n+1)
!    GPLEP ... Log strain in the {f s n} system
!
! FUTURE INPUT
!    TEMP0 ... Initial temperature ............................. temp(n)
!    TEMP1 ... Updated temperature ............................. temp(n+1)
!    GPGI0 ... Previous deformation gradient tensor ............ F(n)
!    FLAGT ... Flag for tangent moduli calculations ............ 1 if yes; 0 if no
!    FLAGS ... Flag for strain gradient calculations ........... 1 if yes; 0 if no
! FUTURE INPUT/OUTPUT
!    GPIVO ... Internal variable array at t(n)/t(n+1) .......... Q(n)/Q(n+1)
! FUTURE OUTPUT
!    GPTMO ... Tangent moduli at t(n+1) ........................ dP/dF(n+1)
!    GGSGO ... Deformation gradient gradient tensor ............ dF/dX(n+1)
!
! USES
!    sld_builtin_materials ... Interface subroutine for built-in materials
!    sld_user_materials ...... Interface subroutine for user materials
! USED BY
!    sld_elmope
!***
!-----------------------------------------------------------------------

  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,mnode
  use def_solidz

  implicit none
  integer(ip), intent(in)    :: itask,pgaus,pmate,ielem,pnode,lnods(pnode)
  real(rp)                   :: temp0(pgaus)
  real(rp)                   :: temp1(pgaus)
  real(rp),    intent(in)    :: gpcau(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpgi0(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpgi1(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpdet(pgaus),gpmof(pgaus)
  real(rp),    intent(in)    :: gprat(ndime,ndime,pgaus)
  integer(ip)                :: flagt,flags,idime,jdime,igaus
  real(rp),    intent(in)    :: gpigd(ndime,ndime,pgaus)
  real(rp),    intent(inout) :: gpene(pgaus)
  real(rp),    intent(inout) :: gpstr(ndime,ndime,pgaus)
  real(rp),    intent(inout) :: nfibe(ndime,pgaus)
  real(rp),    intent(inout) :: gppio(ndime,ndime,pgaus)
  real(rp),    intent(inout) :: gpivo(nvint_sld,pgaus)
  real(rp),    intent(out)   :: gptmo(ndime,ndime,ndime,ndime,pgaus)
  real(rp)                   :: ggsgo(ndime,ndime),gptlo(pgaus)
  real(rp),    intent(in)    :: elcod(ndime,mnode)
  real(rp),    intent(out)   :: gpdds(ndime,ndime,ndime,ndime,pgaus)
  !
  ! For now implicit methods and strain gradient calculations are not
  ! implemented
  !
  if( itask == 2 .or. kfl_exacs_sld /= 0 ) then
     flagt = 1_ip
  else
     flagt = 0_ip
  end if
  flags = 0_ip
  !
  ! Call interface for either built-in or user materials
  !
  if ( lawco_sld(pmate) == 1 ) then
     !
     ! Built-in materials
     !
     ! Check tangent moduli (only for implicit computations) 
     if (kfl_tange_sld==1) then
       call sld_dertan(pgaus,pmate,temp0,temp1,&
         gpgi0,gpgi1,gpigd,gpcau,gpdet,gprat,gppio,gpstr,gpivo,&
         gpene,flagt,gptmo,flags,ggsgo,ielem,gptlo,nfibe,elcod,pnode,lnods,gpsha,gpdds,gpmof)
       call runend('SLD_ELMCLA: Checked for one element numerical tangent moduli vs. analytical one')
     end if

     call sld_builtin_materials(pgaus,pmate,temp0,temp1,&
          gpgi0,gpgi1,gpigd,gpcau,gpdet,gprat,gppio,gpstr,gpivo,gpene,&
          flagt,gptmo,flags,ggsgo,ielem,gptlo,nfibe,elcod,pnode,lnods,gpsha,gpdds,gpmof)
  else if ( lawco_sld(pmate) >= 5 ) then
     !
     ! User materials
     !
     call sld_user_materials(pgaus,pmate,temp0,temp1,&
          gpgi0,gpgi1,gpigd,gpdet,gprat,gppio,gpivo,gpene,flagt,gptmo,&
          flags,ggsgo,ielem)

  end if
  !
  ! Stock F1 in gpgdi_sld and detF in dedef_sld
  !
  do igaus = 1,pgaus
     do idime = 1,ndime
        do jdime = 1,ndime
           gpgdi_sld(ielem) % a(idime,jdime,igaus) = gpgi1(idime,jdime,igaus)
        end do
     end do
     dedef_sld(ielem) % a(igaus) = gpdet(igaus)
  end do

end subroutine sld_elmcla
