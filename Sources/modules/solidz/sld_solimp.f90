!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_solimp.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Implicit time advance
!> @details Implicit time advance
!> @} 
!-----------------------------------------------------------------------
subroutine sld_solimp()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_solimp
  ! NAME
  !    sld_solimp
  ! DESCRIPTION
  !    This routine advances time implicitly
  ! USES
  ! USED BY
  !    sld_solite
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use mod_solver, only : solver_solve

  implicit none

  integer(ip) :: ipoin,kpoin,idime,itott,izrhs,ifpoi,izone
  real(rp)    :: dt,dt2,fact1,fact2,fact3,fact4,xnuma
  logical     :: debuggingMode

  !
  ! Sum up residual contribution of slave neighbors in parallel runs
  ! call sld_parall(3_ip)

  debuggingMode = .false.

  dt  = dtime
  dt2 = dtime*dtime

  if (kfl_tisch_sld == 2 ) then

     !-------------------------------------------------------------------
     !
     ! Newmark Initialization
     !
     !-------------------------------------------------------------------

     !do ipoin = 1,npoin
     !   displ(1,ipoin,1:3)=1.0_rp*coord(1,ipoin)+2.0_rp*coord(2,ipoin)
     !   displ(2,ipoin,1:3)=3.0_rp*coord(1,ipoin)+4.0_rp*coord(2,ipoin)
     !   unkno( (ipoin-1)*2+1 ) = 1.0_rp*coord(1,ipoin)+2.0_rp*coord(2,ipoin)
     !   unkno( (ipoin-1)*2+2 ) = 3.0_rp*coord(1,ipoin)+4.0_rp*coord(2,ipoin)
     !end do

     if( INOTMASTER ) then
        !
        ! Update the unknowns, i.e. the displacements using an implicit
        ! alpha-generalized Newmark scheme:
        !
        ! initial guess for displacement, velocity and acceleration

        izone= lzone(ID_SOLIDZ)        

        if (itinn(modul) == 1) then

           if (kfl_probl_sld /= 0_ip) then    ! in case of dynamic problem


              fact2 = 0.5_rp - tifac_sld(1)
              do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
                 itott =(ipoin-1)*ndofn_sld
                 do idime=1,ndime
                    itott= itott+1
                    if(kfl_fixno_sld(idime,ipoin) /= 1_ip) then  ! nodes with dof
                       unkno(itott) = displ(idime,ipoin,3) + veloc_sld(idime,ipoin,3)*dt + &
                            fact2*accel_sld(idime,ipoin,3)*dt2
                    else                                         ! at fixed nodes
                       unkno(itott) = bvess_sld(idime,ipoin,1)
                    end if
                 end do

                 !**DT: initial guess of displacement-like enrichment dofs
                 if (kfl_xfeme_sld == 1) then
                    do idime=1,ndime
                       itott= itott+1
                       if(lnenr_sld(ipoin) /= 0_ip) then          ! nodes with enrichment dof
                          unkno(itott) = dxfem_sld(idime,ipoin,3) + vxfem_sld(idime,ipoin,3)*dt + &
                               fact2*axfem_sld(idime,ipoin,3)*dt2
                       else                                        ! standard nodes
                          unkno(itott) = 0.0_rp
                       end if
                    end do
                 end if
              end do

              fact3 = (1.0_rp - tifac_sld(2))
              do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
                 itott=(ipoin-1)*ndofn_sld
                 do idime=1,ndime
                    itott= itott+1
                    if(kfl_fixno_sld(idime,ipoin) /= 1_ip) then  ! nodes with dof
                       veloc_sld(idime,ipoin,1) = veloc_sld(idime,ipoin,3) + &
                            fact3*dt*accel_sld(idime,ipoin,3)
                       accel_sld(idime,ipoin,1) = 0.0_rp
                    else                                         ! at fixed nodes
                       veloc_sld(idime,ipoin,1) = (unkno(itott) - displ(idime,ipoin,3))/dt
                       accel_sld(idime,ipoin,1) = (veloc_sld(idime,ipoin,1) - &
                            veloc_sld(idime,ipoin,3))/dt
                    end if
                 end do

                 !**DT: initial guess of velocity and acceleration-like enrichment dofs
                 if (kfl_xfeme_sld == 1) then
                    do idime=1,ndime
                       itott= itott+1
                       if(lnenr_sld(ipoin) /= 0_ip) then           ! nodes with enrichment dof
                          vxfem_sld(idime,ipoin,1) = vxfem_sld(idime,ipoin,3) + &
                               fact3*dt*axfem_sld(idime,ipoin,3)
                          accel_sld(idime,ipoin,1) = 0.0_rp
                       else                                        ! standard nodes
                          vxfem_sld(idime,ipoin,1) = 0.0_rp ! (unkno(itott) - dxfem_sld(idime,ipoin,3))/dt
                          axfem_sld(idime,ipoin,1) = 0.0_rp ! (vxfem_sld(idime,ipoin,1) - &
                          !  vxfem_sld(idime,ipoin,3))/dt
                       end if
                    end do
                 end if

              end do

           else                               ! in case of static problem, only displacement

              do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)                 
                 itott=(ipoin-1)*ndofn_sld
                 do idime=1,ndime
                    itott= itott+1
                    unkno(itott) = displ(idime,ipoin,3)
                    if(kfl_fixno_sld(idime,ipoin) == 1) then           ! fixed displacements
                       unkno(itott)= bvess_sld(idime,ipoin,1)
                    end if
                 end do

                 if (kfl_xfeme_sld == 1) then
                    do idime=1,ndime
                       itott= itott+1
                       unkno(itott) = dxfem_sld(idime,ipoin,3)
                       if(lnenr_sld(ipoin) == 0) then         ! no enrichment
                          unkno(itott)= 0.0_rp
                       end if
                    end do
                 end if

              end do

           end if

        end if


        ! Compute and assemble matrix and rhs

        volum_sld = 0.0_rp

     end if
     !
     ! Assemble Jacobian matrix (J) and RHS (r)
     !
     call sld_matrix(2_ip)

     !-------------------------------------------------------------------
     !
     ! Solving linearized equations for displacement update:
     ! J Du = r
     !
     !-------------------------------------------------------------------

     ! New solver module
     call solver_solve(momod(modul) % solve,amatr,rhsid,dunkn_sld,pmatr)  
     ! call solver(rhsid,dunkn_sld,amatr,pmatr)  

     if (INOTMASTER) then
        if (kfl_prest_sld == 1) then   !Save diplacements if prestress
           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
              itott=(ipoin-1)*ndofn_sld
              do idime=1,ndime
                 itott= itott+1
                 ddisp_sld(idime,ipoin,2) =  ddisp_sld(idime,ipoin,1)
                 ddisp_sld(idime,ipoin,1) =  dunkn_sld(itott)
                 dunkn_sld(itott) = 0.0_rp
              end do
           end do
        end if
     end if

     !
     ! ARC-LENGTH update: ECR: REVISAR SI ESTO ESTA DUPLICADO
     !
     if (INOTMASTER) then 
       if (kfl_arcle_sld == 1) then
          ! update lambda
          lambda_sld(2) = lambda_sld(1)
          lambda_sld(1) = dunkn_sld(ndofn_sld*npoin + 1) 
       end if
     end if

     ! finding the global maximum residual force
     if (INOTMASTER) then
        rsmax_sld = -1.0_rp
        do izrhs=1,solve_sol(1) % nzrhs * solve_sol(1) % nseqn
           rsmax_sld = max(abs(rhsid(izrhs)),rsmax_sld)
        end do
     end if
     call pararr('MAX',0_ip,1_ip,rsmax_sld)

     ! computing the L2-norm of the global residual force vector
     call norm2x(ndime,rhsid,rsn2x_sld)

     if (INOTMASTER) then

        ! exchange of entries of frxid_sld (the reaction forces) at sub-domain boundaries
        call pararr('SLX',NPOIN_TYPE,ndofn_sld*npoin,frxid_sld)
        ! finding the global maximum reaction force at Dirichlet nodes
        !         (see sld_elmmat.f90 for calculation of reaction force)
        fimax_sld = -1.0_rp
        do izrhs=1,solve_sol(1) % nzrhs * solve_sol(1) % nseqn
           fimax_sld = max(abs(frxid_sld(izrhs)),fimax_sld)
        end do
     end if

     call pararr('MAX',0_ip,1_ip,fimax_sld)

     !-------------------------------------------------------------------
     !
     ! Newmark update
     !
     !-------------------------------------------------------------------

     if( INOTMASTER ) then
        !
        ! Correcting displacement and imposing boundary conditions
        !
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)

           itott=(ipoin-1)*ndofn_sld
           do idime=1,ndime
              itott= itott+1
              unkno(itott) = unkno(itott) + dunkn_sld(itott)
              if(kfl_fixno_sld(idime,ipoin) == 1) then    ! fixed displacements
                 unkno(itott)= bvess_sld(idime,ipoin,1)
              end if
           end do

           !**DT: correcting displacement-like enrichment dofs
           if (kfl_xfeme_sld == 1) then
              do idime=1,ndime
                 itott= itott+1
                 unkno(itott) = unkno(itott) + dunkn_sld(itott)
                 if(lnenr_sld(ipoin) == 0) then         ! standard nodes
                    unkno(itott)= 0.0_rp
                 end if
              end do
           end if

           ! ECR:  ARC-LENGTH: correcting lambda
           if (kfl_arcle_sld == 1) then
              itott = itott + 1
              unkno(itott) = unkno(itott) + dunkn_sld(itott)
           end if

        end do

        if (kfl_probl_sld /= 0_ip) then
           !
           ! Correcting velocity and acceleration
           !
           fact2 = 1.0_rp/(tifac_sld(1))
           fact3 = 0.5_rp-tifac_sld(1)
           fact4 = tifac_sld(2)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)

              itott=(ipoin-1)*ndofn_sld
              do idime=1,ndime
                 itott= itott+1
                 if(kfl_fixno_sld(idime,ipoin) /= 1_ip) then  ! nodes with dof
                    accel_sld(idime,ipoin,1) = &
                         fact2*(unkno(itott) - displ(idime,ipoin,3) - &
                         dt*veloc_sld(idime,ipoin,3) - &
                         dt2*fact3*accel_sld(idime,ipoin,3))/dt2
                    veloc_sld(idime,ipoin,1) = veloc_sld(idime,ipoin,3) + &
                         dt*((1.0_rp - fact4)*accel_sld(idime,ipoin,3) + &
                         fact4*accel_sld(idime,ipoin,1))
                 else                                          ! at fixed nodes
                    veloc_sld(idime,ipoin,1) = (unkno(itott) - displ(idime,ipoin,3))/dt
                    accel_sld(idime,ipoin,1) = (veloc_sld(idime,ipoin,1) - &
                         veloc_sld(idime,ipoin,3))/dt
                 end if
              end do
           end do
        end if

        if (debuggingMode) then
           write(lun_livei,'(/,a3,x,2(6x,3(2x,a13)),$)')'ipt','displ','veloc','accel',&
                'dxfem','vxfem','axfem'
           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)

              itott=(ipoin-1)*ndofn_sld
              do idime=1,ndime
                 itott= itott+1
                 write(lun_livei,'(/,i3,x,2(6x,3(2x,e13.6)),$)')ipoin,unkno(itott),&
                      veloc_sld(idime,ipoin,1),accel_sld(idime,ipoin,1),unkno(itott+2),&
                      vxfem_sld(idime,ipoin,1),axfem_sld(idime,ipoin,1)
              enddo
           enddo
        endif

     end if

  end if
  !
  ! Update boundary conditions (*,3) <- (*,2)
  !
  if( INOTMASTER ) call sld_updbcs(4_ip)

end subroutine sld_solimp
