subroutine sld_activf(gptlo,ielem,pgaus)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_activf
  ! NAME
  !    sld_activf
  ! DESCRIPTION
  !    Used for the coupling between the SOLIDZ and EXMEDI modules using Peterson's method.
  !    The routine calulates the active FORCE created by the cross-bridges. 
  !
  !    Solve {bridg}dot = [A]{bridg} by finite diference. 
  !    where {bridg}={U' O' U  O}^T, the state (unattached (U) or attached (O)) of the cross-bridges
  !    and [A] function of constants and [Ca2+]_f
  !    b^n = b^(n-1) + delta_t * A * b^(n-1)
  !
  ! REFERENCES
  !    Watanabe et al. (2004), eqns 3-4-5
  ! INPUT
  !
  !    GPTLO ... Time since the depolarization of the membrane (give by EXMEDI via SLD_EXMEDI)
  !  
  ! OUTPUT (global) 
  !    bridg_sld ... State of the bridges 
  !    forcm_sld ... Max force (will be use for normalization)
  ! USES
  !
  ! USED BY
  ! sld_elmope   
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_domain   ! geometry information
  use def_solidz
  use def_master, only       :  dtime,ittim
  implicit none
  
  integer(ip), intent(in)    :: ielem,pgaus
  real(rp),    intent(in)    :: gptlo(mgaus)
  real(rp)                   :: K1,K2,K3,K4,Ka,Kd,Kdp,ca2pm,tca2p
  real(rp)                   :: A(4,4),bridg_new(4),bridg_old(4),bidon(4),tfals,&
                                cca2p,deltt,forcb
  integer(ip)                :: i,j,k,igaus
 
 
 
  
 !constants (units: microM et sec)
! K1= 39.0_rp 
! K2= K1*40.0_rp 
! K3= 19.6_rp 
! K4= 19.6_rp 
! Ka= 0.95_rp 
! Kd= 2.04_rp 
! Kdp=15.0_rp  

! ca2pm=1.2_rp   !MAX[Ca2+]_f (in microM)
! tca2p=0.08_rp  !thau_Ca (sec)




 !constants (units: microM et milisec)
 K1= 0.039_rp 
 K2= K1*40.0_rp 
 K3= 0.0196_rp 
 K4= 0.0196_rp 
 Ka= 0.00095_rp 
 Kd= 0.00204_rp 
 Kdp=0.015_rp  

 ca2pm=1.2_rp   !MAX[Ca2+]_f (in microM)
 tca2p=80.0_rp  !thau_Ca (sec)



 A=0.0_rp
  
 !
 ! find Fmax (forcm_sld)
 !
 
 if (ittim==1 .AND. ielem==1) then 

    forcm_sld=0.0_rp

    ! initial cdn (b=[U' O' U O])
    bridg_old=0.0_rp
    bridg_old(1)=1.0_rp !value is not important since the force will be normalized        
    
    deltt=1000.0/4999.0
    do k=1,5000
         
       tfals=(real(k)-1.0)*deltt 
         
       !calculate [Ca2+] 
       cca2p = ca2pm*(tfals/tca2p)*exp(-tfals/tca2p)  
         
       ! Define [A]             
       A(1,1) =  deltt*(-cca2p*K1) 
       A(1,2) =  deltt*Kdp 
       A(1,3) =  deltt*K3 
       A(1,4) =  0.0_rp 
       A(2,1) =  0.0_rp 
       A(2,2) =  deltt*(-cca2p*K2-Kdp) 
       A(2,3) =  0.0_rp 
       A(2,4) =  deltt*K4 
       A(3,1) =  deltt*(cca2p*K1) 
       A(3,2) =  0.0_rp 
       A(3,3) =  deltt*(-(Ka+K3)) 
       A(3,4) =  deltt*Kd 
       A(4,1) =  0.0_rp  
       A(4,2) =  deltt*(cca2p*K2) 
       A(4,3) =  deltt*Ka 
       A(4,4) =  deltt*(-(Kd+K4)) 

       !b_new = b_old + A*b_old;
       bidon=0.0_rp
       do i=1,4
          do j=1,4
             bidon(i)=bidon(i) + A(i,j)*bridg_old(j)
          end do 
       end do 

       do i=1,4
         bridg_new(i)=bridg_old(i)+bidon(i)
       end do    
        
       bridg_old = bridg_new
         
       forcb = bridg_new(2)+bridg_new(4) !Force developed by the bridge => use to calibrate the active parameters
       
       if (forcb>forcm_sld) then
         forcm_sld=forcb
       end if  
        
   end do 
  
  
   

end if 

do igaus=1,pgaus

    !calculate [Ca2+] 
    cca2p = ca2pm*(gptlo(igaus)/tca2p)*exp(-gptlo(igaus)/tca2p) 
           
    ! Define [A]             
    A(1,1) =  dtime*(-cca2p*K1) 
    A(1,2) =  dtime*Kdp 
    A(1,3) =  dtime*K3 
    A(1,4) =  0.0_rp 
    A(2,1) =  0.0_rp 
    A(2,2) =  dtime*(-cca2p*K2-Kdp) 
    A(2,3) =  0.0_rp 
    A(2,4) =  dtime*K4 
    A(3,1) =  dtime*(cca2p*K1) 
    A(3,2) =  0.0_rp 
    A(3,3) =  dtime*(-(Ka+K3)) 
    A(3,4) =  dtime*Kd 
    A(4,1) =  0.0_rp  
    A(4,2) =  dtime*(cca2p*K2) 
    A(4,3) =  dtime*Ka 
    A(4,4) =  dtime*(-(Kd+K4)) 

    ! initial cdn (b=[U' O' U O]) ! OJO: TO PUT ELSEWHERE
    !bridg_sld=0.0_rp
    !bridg_sld(1)=1.0_rp !value is not important sinbridg_sld = b_oldce the force will be normalized => now in def_solidz
    
    !b_new = b_old + A*b_old;
    !here, bridg_sld = b_old
     bidon=0.0_rp
     do i=1,4
        do j=1,4
           bidon(i)=bidon(i) + A(i,j)*bridg_sld(j,igaus,ielem)
        end do 
     end do 

     do i=1,4
       bridg_new(i)=bridg_sld(i,igaus,ielem)+bidon(i)
     end do    
       
    ! 
    ! normalized the force with the maxF 
    !
    !forcb=forcb/forcm_sld  
      
      
    ! 
    ! update {b}
    !
    do i=1,4  
      bridg_sld(i,igaus,ielem) = bridg_new(i)  
    end do 
    

   
end do 

end subroutine sld_activf
