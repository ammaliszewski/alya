subroutine sld_tistep
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_tistep
  ! NAME 
  !    sld_tistep
  ! DESCRIPTION
  !    This routine sets the time step
  ! USES
  ! USED BY
  !    sld_begite
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_solidz
  implicit none

  integer(ip) :: idata

  if(kfl_timco/=2) then
     if (kfl_dtfun == 1) then
       do idata = 1,nfundt-1
         if ((cutim >= 0.0_rp) .and. (cutim < dtfun(1,1))) then
           dtime = dtfun(2,1)
         else if ((cutim >= dtfun(1,idata)) .and. (cutim < dtfun(1,idata+1))) then
           dtime = dtfun(2,idata+1)
         else if (cutim > dtfun(1,nfundt)) then
           dtime = dtfun(2,nfundt) 
         end if
         if (dtime /= 0.0_rp) then
           dtinv_sld = 1.0_rp / dtime
         end if
       end do
     else
       dtinv_sld = dtinv
       dtime     = 1.0_rp / dtinv
  !      if(kfl_stead_sld == 1 .or. kfl_timei_sld == 0) then
  !         dtinv_sld = 0.0_rp
  !      end if
     end if
  end if

  routp(1) = dtcri_sld
  ioutp(1) = kfl_timei_sld
  ioutp(2) = kfl_stead_sld
  routp(2) = 0.0_rp
  routp(3) = 0.0_rp
  call outfor(8_ip,lun_outpu,' ')


end subroutine sld_tistep
