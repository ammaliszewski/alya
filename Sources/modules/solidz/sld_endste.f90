subroutine sld_endste
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_endste
  ! NAME 
  !    sld_endste
  ! DESCRIPTION
  !    This routine ends a time step of SOLIDZ
  ! USES
  !    sld_cvgunk
  !    sld_updunk
  !    sld_output
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_solidz

  !!!!! DEBUG
  use def_domain ! Added to use npoin
  !!!!! END_DEBUG
  implicit none

  integer(ip) :: ipoin
  !
  ! Compute convergence residual of the time evolution (that is,
  ! || d(n,*,*) - d(n-1,*,*)|| / ||d(n,*,*)||) and update unknowns
  ! d(n-1,*,*) <-- d(n,*,*) 
  !
  call sld_cvgunk(3_ip)
  call sld_updunk(5_ip)
  call sld_updunk(7_ip)
  call sld_isochr
  !
  ! Write restart file
  !
  call sld_restar(two)
  !
  ! If not steady, go on
  !

  if(kfl_stead_sld==0.and.kfl_timei_sld==1.and.kfl_conve(modul)==1) kfl_gotim = 1


!!!! DEBUG
    if(.false.) then
        if(INOTMASTER) then
            do ipoin=1, npoin
                if ((coord(1,ipoin).eq.0.0_rp).and.(coord(2,ipoin).lt.-0.079_rp).and.(coord(2,ipoin).gt.-0.081_rp).and.(coord(3,ipoin).eq.0.0_rp)) then
                    write(6,*) ' '
                    write(6,*) '|---ENDSTE_SLD--|'
                    ! write(6,*) 'XYZ:', coord(1,ipoin), coord(2,ipoin), coord(3,ipoin)
                    write(6,*) 'displ:',  displ(1,ipoin, TIME_N)
                endif
            enddo
        endif
    endif
!!!! END_DEBUG

end subroutine sld_endste
