module def_solidz
  !------------------------------------------------------------------------
  !****f* Solidz/sld_temper
  ! NAME
  !    def_solidz
  ! DESCRIPTION
  !    Heading for the module routines
  ! USES
  ! USED BY
  !    Almost all
  !***
  !------------------------------------------------------------------------
  use def_kintyp

  !------------------------------------------------------------------------
  !
  ! Parameters
  !
  !------------------------------------------------------------------------

  integer(ip), parameter              :: &
       nvarp_sld=45,                     & ! # postprocess variables
       nvart_sld=10,                     & ! # postprocess times x
       !       nvint_sld=10,                     & ! # number of internal variables (for damage OR vumat)
       ncoef_sld=30                        ! # coefficient for density law

  !------------------------------------------------------------------------
  !
  ! Physical problem: read in sld_reaphy
  !
  !------------------------------------------------------------------------
!--BEGIN REA GROUP
  integer(ip)                         :: &
       kfl_model_sld,                    & ! Elastic/visco-elastic/elasto-plastic
       kfl_timei_sld,                    & ! Existence of dd/dt
       kfl_probl_sld,                    & ! Type of problem: static or dynamic
       kfl_vumat_sld,                    & ! Vumat model
       kfl_fiber_sld,                    & ! Anisotropic media with fibers
       kfl_restr_sld,                    & ! Residual Stress in voigt notation
       kfl_indis_sld(2),                 & ! Initial displacements
       kfl_prdef_sld,                    & ! Fibers field nodal- or element- wise
       kfl_damag_sld,                    & ! Damage model
       kfl_cohes_sld,                    & ! Cohesive model
       kfl_cohft_sld,                    & ! Friction in cohesive model (at the momento in always 0)
       kfl_intva_sld,                    & ! Internal variables
       kfl_vofor_sld,                    & ! Applied external volume force
       kfl_quali_sld,                    & ! Check quality of deformed mesh
       kfl_therm_sld,                    & ! Thermal effects in the constitutive model
       kfl_moduf_sld(5),                 & ! Modulator fields for properties
       kfl_tange_sld,                    & ! 1: Check tangent moduli, 2: Use numerical tangent moduli  (only for implicit computations) 
       nvint_sld,                        & ! # of internal variables
       ivert_sld                           ! Vertical coordinate from gravity definition
  real(rp)                            :: &
       gravi_sld(3),                     & ! Gravity vector
       grnor_sld,                        & ! Gravity norm
       gfibe_sld(2,3),                   & ! Global fiber orientation
       aleso_sld(2)                        ! ALE Tezduyar rigidization parameters
  integer(ip),  pointer               :: &
       modfi_sld(:),                     & ! Type of model used to define the fibers
       modor_sld(:,:),                   & ! Fibers with sheet-like structure (orthotropy)
       kusmo_sld(:),                     & ! User-defined models number
       kfl_aleso_sld(:),                 & ! ALE Solid
       kfl_dampi_sld(:),                 & ! Rayleigh damping
       kfl_coupt_sld(:),                 & ! Coupling tensor (transversally zero, trans isotropic o trans aniso)
       kfl_eccty_sld(:),                 & ! Type of excitation-contraction coupling (Hunter or Rice) *for now**
       lawco_sld(:),                     & ! Constitutive law
       lawde_sld(:),                     & ! Law for rho [Kg/m^3]
       lawst_sld(:),                     & ! Law for the stress model
       lawch_sld(:),                     & ! Law for cohesive model
       nocoh_sld(:),                     & ! List of nodes where the tractions exceed the cohesive limitIt has to be reinitializated and updated at ecah time step
       lawmo_sld(:)                        ! Auxiliar when some versions of one constitutive model are defined
  real(rp),     pointer               :: &
       cocof_sld(:),                     & ! Control coupling factor
       timec_sld(:),                     & ! Time constant for the [Ca] fct      
       hillc_sld(:),                     & ! Hill coefficient
       cal50_sld(:),                     & ! Ca 50 coefficient
       deltt_sld(:),                     & ! tfin of thermal effects and DeltaT
       trans_sld(:,:),                   & ! Transversal coupling force ratio
       parsp_sld(:,:),                   & ! Parameter of the spheroid-geometry model
       densi_sld(:,:),                   & ! Reference Density (rho)
       dampi_sld(:,:),                   & ! Rayleigh damping coefficients
       parco_sld(:,:),                   & ! Parameters built-in constitutive law
       parcc_sld(:,:),                   & ! Parameters CCMT       
       parch_sld(:,:),                   & ! Parameters cohesive law
       parcf_sld(:,:),                   & ! Parameters contact and friction
       velas_sld(:,:)                      ! Reference velocity of sound

  !------------------------------------------------------------------------
  !
  ! Numerical scheme: read in sld_reanut
  !
  !------------------------------------------------------------------------

  integer(ip)                           :: &
       kfl_xfeme_sld,                      & ! Enrichment strategy (XFEM)
       kfl_xfcra_sld,                      & ! Crack/discontinuity definition (XFEM)
       kfl_normc_sld,                      & ! Norm of convergence
       kfl_tiacc_sld,                      & ! Temporal accuracy
       kfl_timet_sld,                      & ! Time treatment
       kfl_tisch_sld,                      & ! Time integration scheme
       kfl_linea_sld,                      & ! Non-linear treatment scheme (none, picard, n-r, etc.)
       kfl_serei_sld,                      & ! Selective reduced integration
       kfl_limit_sld,                      & ! Limiter on
       kfl_volca_sld,                      & ! Calculate the volume of the cavity
       kfl_prest_sld,                      & ! Calculate prestress
       kfl_plane_sld,                      & ! Plane-strain 2D assumption
       kfl_arcle_sld,                      & ! Arc-length technique
       mcavi_sld,                          & ! 
       kcavi_sld(4),                       & ! 
       iocav_sld(4),                       & ! 
       nisaf_sld,                          &
       miinn_sld,                          & ! Max # of iterations
       arcint_sld                            ! Auxiliar integer to define size of structures for arc-length technique

  real(rp)                              :: &
       ocavi_sld(3,4),                     & ! 
       tifac_sld(5),                       & ! Time integration factors (for CN, newmark, etc.)
       spect_sld(2),                       & ! Spectral ratio and stability limit
       cotol_sld,                          & ! Convergence tolerance
       safet_sld,                          & ! Safety factor for time step
       safex_sld,                          & ! Safety factor for time step
       safma_sld,                          & ! Safety factor for time step
       relfa_sld,                          & ! Relaxation factor
       meanf_sld,                          & ! Meaningful value of force/stress
       sstol_sld                             ! Steady state tolerance
                                             ! Achtung: a priori different from 'time step'
  !------------------------------------------------------------------------
  !
  ! Boundary conditions: read in sld_reabcs
  !
  !------------------------------------------------------------------------

  integer(ip)                           :: &
       kfl_conbc_sld,                      & ! Constant b.c.
       kfl_cycle_sld,                      & ! To activate the cardiac cycle management 
       kfl_bodyf_sld,                      & ! Body force function id
       kfl_inifi_sld(3),                   & ! Initial fields
       kfl_funty_sld(20,20),               & ! Function type and number of paremeters
       nbcbo_sld,                          & ! Number of boundary conditions on surface elements
       mtloa_sld(10),                      & ! Number of data points for the transient boundary functions
       nfunc_sld,                          & ! Number of transient boundary conditions function
       ncrak_sld                             ! Number of cracks                        
  real(rp) ::&
       rtico_sld(2 ,20),                  &  ! Time counters for the transient boundary conditions
       fubcs_sld(20,20)
  type(bc_nodes), pointer              :: &
       tncod_sld(:)                          ! Node code type
  type(bc_nodes), pointer              :: &
       tgcod_sld(:)                          ! Geometrical node code type
  type(bc_bound), pointer              :: &
       tbcod_sld(:)                          ! Boundary code type
  type(r2p),      pointer              :: &
       tload_sld(:)                          ! time dependent boundary functions (to be read in a file)
  type(r1p),      pointer              :: &
       funpa_sld(:)                          ! Function parameters
  real(rp),       pointer              :: &
       crkco_sld(:,:,:)                      ! Initial crack coordinates

  !------------------------------------------------------------------------
  !
  ! Output and Postprocess: read in sld_reaous
  !
  !------------------------------------------------------------------------

  integer(ip)                           :: &
       kfl_exacs_sld,                      & ! Exact solution 
       kfl_rotei_sld,                      & ! Rotate and correct sigma in prismatic shells
       kfl_foten_sld                         ! Tensors of forces are used (caust_sld, green_sld and lepsi_sld)
  real(rp) ::&
       rorig_sld(3),                       & ! Postprocess rotations origin
       thiso_sld(2)                          ! Threashold for isochrones
  real(rp),    pointer  ::                 &
       cpual_master_sld(:),                &
       cpual_slave_sld(:),                 &
       isoch_sld(:,:)                        ! Isochrones for solidz
                  
!--END REA GROUP
  !------------------------------------------------------------------------
  !
  ! Other
  !
  !------------------------------------------------------------------------
  !
  ! Physical problem: read, modified or defined in exm_reaphy SECOND CALL
  !
  real(rp),     pointer ::&
       fiber_sld(:,:),                     & ! Fiber corientation field
       fiemo_sld(:,:),                     & ! Modulator fields
       restr_sld(:,:),                     & ! Residual Internal stresses in voigt notation
       vofor_sld(:,:),                     & ! Volume external force in the reference frame
       ddisp_sld(:,:,:)                      ! Save delta displacements for Prestress computations

  integer(ip),  pointer                 :: &
       kfl_fixbo_sld(:),                   & ! Element boundary fixity
       kfl_fixno_sld(:,:),                 & ! Load fixity
       kfl_funno_sld(:),                   & ! Functions for node bc
       kfl_fixrs_sld(:),                   & ! Reference system for the BV
       kfl_funbo_sld(:),                   & ! Functions for boundary bc
       lmate_sld(:),                       & ! Materials (element-wise)
       lcrkf_sld(:),                       & ! List of cracked faces
       iswav_sld(:,:)                        ! Displacement and stress wave

  real(rp),     pointer                 :: &
       dxfem_sld(:,:,:),                   & ! XFEM Displacement enrichment global function (alpha in denny's report)
       vxfem_sld(:,:,:),                   & ! XFEM Velocity enrichment global function (alpha in denny's report)
       axfem_sld(:,:,:),                   & ! XFEM Acceleration enrichment global function (alpha in denny's report)
       crapx_sld(:,:),                     & ! XFEM Crack/discontinuity position (per element)
       cranx_sld(:,:),                     & ! XFEM Crack/discontinuity normal (per element)
       sgmax_sld(:),                       & ! XFEM Maximum tensile stress (per element)
       bvess_sld(:,:,:),                   & ! Essential bc values (on dd/dt)
       bvnat_sld(:,:,:),                   & ! Natural bc values   (on traction)
       skcos_sld(:,:,:),                   & ! Cosine matrices of skew systems
       cockf_sld(:,:),                     & ! List of cracked faces coordinates
       crtip_sld(:,:),                     & ! List of actual crack tip faces
       covar_sld(:),                       & ! Control coupling factor variable parameter
       frxid_sld(:),                       & ! Global/assembled reaction force
       cohnx_sld(:,:),                     & ! Normal to the midplane of cohesive element (per element)
       forc_fsi(:,:),                      & ! force to be used in FSI problems
       lambda_sld(:)                         ! arc-length control parameter

  integer(ip)                           :: &
       kfote_sld,                          & ! flag for foten 
       nmate_sld,                          & ! # of materials
       ittot_sld,                          & ! Total number of iteration
       kfl_goite_sld,                      & ! Keep iterating
       kfl_tiaor_sld,                      & ! Original time accuracy
       kfl_stead_sld,                      & ! Steady-state has been reached
       kfl_gdepo_sld,                      & ! If GDEPO is needed
       nvgij_sld(6,2),                     & ! Voigt - indicial conversion table
       nvgij_inv_sld(3,3),                 & ! Voigt - inverse of the indicial conversion table
       ntens_sld,                          & ! Stress and strain dimension
       ncomp_sld,                          & ! # of d.o.f. of the problem
       ndofn_sld,                          & ! # of d.o.f. of the problem
       ndof2_sld,                          & ! ndofn*ndofn
       nvoig_sld,                          & ! voigt dimension
       nshea_sld,                          & ! shear components
       nevab_sld,                          & ! Element velocity dof=ndime*nnode
       nevat_sld,                          & ! Element matrix dimension = ndofn_sld*mnode
       nunkn_sld,                          & ! # unknowns
       nzmat_sld,                          & ! Matrix size
       nzrhs_sld,                          & ! RHS size
       lasti_sld,                          & ! use to calculate the isovolumetric phases of the cardiac cycle
       ifase_sld(4),                       & ! flag for the cardiac cycle phases (detect the first time the phase is entered)
       kfase_sld,                          & ! Pĥase ID for the cardiac cycle
       iwave_sld,                          & ! Polarization wave
       kfl_ielem,                          & ! Witness element
       nstat_sld,                          & ! Total number of state variables 
       itarc_sld                             ! Optimum number of iterations of arc-length NR method (nd=5 refernce Gutierrez (2004))

  real(rp), pointer                     :: &
       dunkn_sld(:),                       & ! corrector for Newton--Raphson
       accel_sld(:,:,:),                   & ! Displacement acceleration
       veloc_sld(:,:,:),                   & ! Displacement velocity
       vinte_sld(:,:,:),                   & ! Internal variables for user-defined models
       bridg_sld(:,:,:),                   & ! Cross bridge state (EXMEDI COUPLING) with Peterson's method
       ddism_sld(:,:),                     & ! FSI movemente variation (ALEFOR COUPLING)
       gpsl0_sld(:,:,:),                   & ! Initial Logarithmic strain (BRIDGE)
       rstr0_sld(:,:,:),                   & ! Rotation matrix (F=RU) (BRIDGE)
       dfric_sld(:,:,:,:),                 & ! Friction force for contact/friction
       dslip_sld(:,:,:,:),                 & ! Crack tangential jump/surface slip
       dceff_sld(:,:,:,:),                 & ! Effective scalar opening for cohesive law
       dcmax_sld(:,:,:),                   & ! Maximum scalar opening for cohesive law
       treff_sld(:)                          ! Effective traction for cohesive law

  real(rp),       pointer              :: &
       crkpo_sld(:,:),                    &  ! Crack positions
       crkno_sld(:,:),                    &  ! Crack normals
       eleng_sld(:)                          ! Element characteristic length

  integer(ip), pointer                 :: &
       kacti_sld(:),                      & ! Activation flag (EXMEDI COUPLING)
       lnenr_sld(:),                      & ! List of enriched nodes
       lecoh_sld(:),                      & ! List of cohesive elements
       leenr_sld(:),                      & ! List of enriched elements
       ledam_sld(:)                         ! List of damaged elements

  real(rp),     pointer               :: &
       fibts_sld(:,:),                   & ! Orthotropic fiber direction s
       fibtn_sld(:,:),                   & ! Orthotropic fiber direction n
       gdepo_sld(:,:,:),                 & ! Deformation gradient defined per node (for nodal push-forward)
       quali_sld(:),                     & ! Deformed mesh quality (per element)
       nopio_sld(:,:),                   & ! First Piola--Kirchhoff Stress tensor on nodes
       caust_sld(:,:),                   & ! Cauchy Stress tensor
       roloc_sld(:,:,:),                 & ! Local rotation tensor
       sigei_sld(:),                     & ! Largest eigenvector of the Cauchy Stress tensor (sigma)
       lepsi_sld(:,:),                   & ! Log strain in a local system define in the const. law (ex:fiber-sheet-normal)
       fibde_sld(:,:),                   & ! Fiber postproc vector in the current configuration (relative to ini. lenght)
       seqvm_sld(:),                     & ! Combined stress: Von Mises
       seqin_sld(:),                     & ! Combined stress: Intensity
       caunn_sld(:),                     & ! n . Cauchy . n: different than zero in the surface
       green_sld(:,:),                   & ! Green strain tensor
       vmass_sld(:),                     & ! Weighted mass matrix
       vmasx_sld(:)                        ! Enriched mass matrix

  real(rp), target                    :: &
       volum_sld(2),                     & ! Reference (1) and deformed configuration (2) volumes
       fimax_sld,                        & ! Maximum reaction force at Dirichlet nodes
       deltaE_sld                          ! arc-length energy release incremental 


  type(r3p),   pointer                :: &
       cause_sld(:)                        ! Element-wise Cauchy Stress tensor

  real(rp)                              :: &
!        maxrx_sld,                          & ! Maximum of reaction force
!        maxrs_sld,                          & ! Maximum of residual
       dtinv_sld,                          & ! 1/dt
       dtcri_sld,                          & ! Critical time step
!       eleng_sld,                          & ! Typical element length
       sgult_sld,                          & ! Ultimate maximum level of tensile stress
       pabdf_sld(10),                      & ! BDF paremeters
       shagp_sld(8,8,5),                   & ! Extrapolation gauss-nodes matrix (inverse of gpsha)
       resid_sld,                          & ! Residual, internal iterations
       resti_sld,                          & ! Residual, time steps
       volst_sld(12,2),                    & ! Volume of the preceeding step for calculation of pression in the heart
       volvr_sld(2),                       & ! Reference ventricle volumes
       ejrat_sld(2),                       & ! Ejection rate
       dltap_sld(12,2),                    & ! dP for calculation of pression in the heart
       timst_sld(12),                      & ! time for calculation of pression in the heart       
       sidev_sld(2,2),                     & ! ...for cardiac isovolumetric phase
       forcm_sld,                          & ! Max force for EC coupling
       velov_sld,                          & ! rate of change of volume - for cardiac isovolumetric phase
       bodyf_sld(3),                       & ! Body force vector
       ptota_sld(2),                       & ! The pressure to add in the cardiac isovolumetric phase   
       tactv_sld,                          & ! Volumetric active force along the fibers
       rsmax_sld,                          & ! Maximum residual
       rsn2x_sld                             ! L2-norm residual

  real(rp),  pointer :: &
       du_sld(:,:)

! Global vectors defined per element and per gauss point
  type(r3p),   pointer                  :: &
       gppio_sld(:),                       & ! 1st PK stress tensor on elements
       gpgdi_sld(:),                       & ! Deformation gradient tensor on elements
       fibeg_sld(:)                          ! Fiber orientation field

  type(r1p),   pointer                  :: &
       dedef_sld(:)                          ! Determinant of the deformation gradient tensor grdef

  !
  ! Exact solution
  !
  real(rp)                              :: &
       err01_sld(2),                       &
       err02_sld(2),                       & 
       err0i_sld(2),                       &
       err11_sld(2),                       &
       err12_sld(2),                       & 
       err1i_sld(2)

  !
  ! Aitken relaxation factors, computed by the master and passed to the slaves
  ! 
  real(rp)                              :: &
       relpa_sld ,                         &  ! Solver
       relco_sld                              ! ALEFOR - NASTIN coupling

  !
  !Contact stuff
  !
  real(rp),  pointer                    :: &
       rhscont(:),                         &
       neumanncb(:),                       &
       contact_info(:,:),                  &
       res(:),                             &
       lagmult(:),                         &
       lagmult_ant(:)

  integer(ip)                           :: &
       uzawa_count,                        &
       kfl_conta_sld
   
  !
  ! Composite stuff                                                                                 ( *AQU* )
  !	   	
  real(rp), pointer                     :: &
       state_sld(:, :, :, :),              & ! Internal state variables: dimension( 7, mgauss,nelem, 2)
                                             ! (:,:,:,1) values  at iteration 0, (:,:,:,2) updated at last iteration 
       stiff0_sld(:, :, :),                & ! Stiffness matrix without damage
       fibeb_sld(:, :),                    & ! Orientation. Axis X' or b
       fibes_sld(:, :),                    & ! Orientation: Axis Y' or s
       fiben_sld(:, :)                       ! Orientation: Axis Z' or n

  type(r3p),   pointer                  :: &
       caunp_sld(:),                       & ! Recovered stresses at nodes, element-wise Cauchy Stress tensor      
       caulo_sld(:)                          ! Recovered stresses at nodes, element-wise Cauchy Stress tensor 
                                             ! in the local coordinate system

   real(rp), pointer :: aux_sld(:,:,:) => null() 

contains

  function sld_zonenode(ipoin)
    use def_kintyp
    use def_domain
    use def_master
    implicit none

    logical :: sld_zonenode
    integer(ip) :: izone,ipoin

    sld_zonenode = .true.
    izone= lzone(ID_SOLIDZ)

    if (nzone > 1) then
   !    if (lpoiz(izone,ipoin) == 0) sld_zonenode = .false.
    end if

  end function sld_zonenode

end module def_solidz

