subroutine sld_volume()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_volume
  ! NAME
  !    sld_volume
  ! DESCRIPTION
  ! 
  !    ** WARNING ** Only for tetra element 
  !    ** WARNING ** The normal of the bnd element of the epicardium must point outward (PRECISER)- can do that in icem   
  !    ** WARNING ** a DELTA_V can be calculated, not the actual VOLUME
  ! INPUT
  !       LNODB(MNODB,NBOUN)       list of boundary elements 
  !       NBOUN                    Amount of boundary elements
  !       MNODB                    dim of boundary elements  
  ! USES
  ! USED BY
  !    sld_endite
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain
  use def_solidz
  use def_master
  implicit none

  integer(ip)                :: ipoin,itask,ivolu

  integer(ip)                ::  iboun,jboun,ibope,inodb,idime,jdime,inode,ielem,icoun,linee(2)
  integer(ip)                ::  opene(2,nboun*mnodb),lilin,try,edge1,edge2,tmp,ielty,icavi,prueba

  real(rp)                   ::  xcent,ycent,zcent,node1(3),node2(3),node3(3),ax,ay,az,bx,by,bz
  real(rp)                   ::  gpele(3),norme,venor(3),super,volum(mcavi_sld),fonct,bidon
  real(rp)                   ::  dummr(4),super_total,deltv,deltp,compl
  real(rp)                   ::  coord_vol(3,3)


  if( INOTMASTER ) then

     do ielty = iesta_dom,iesto_dom
        if(lexis(ielty)/=0.and.lorde(ielty)>1) then
           call runend('SLD_VOLUME: ONLY CODED FOR TRIANGULAR FACES')
        end if
     end do

  end if

  volum       = 0.0_rp

  if( INOTMASTER ) then

     !
     ! Calculte the VOLUMES (ONLY WORKS FOR TRIANGULAR FACES)
     ! 

     super_total = 0.0_rp
     super       = 0.0_rp
     do iboun=1,nboun
        ivolu= 0
        do icavi= 1,mcavi_sld
           if (lbset(iboun) == iocav_sld(icavi)) then
              ivolu= icavi
           end if
        end do

        if ((ivolu > 0)) then
           do inodb= 1,3
              ipoin= lnodb(inodb,iboun)
              do idime=1,ndime                    
                 coord_vol(idime,inodb)=coord(idime,ipoin)+displ(idime,ipoin,1) - ocavi_sld(idime,ivolu)
              end do
           end do
           
           !centroid of the element (use as integration point)
           do idime= 1,ndime
              gpele(idime)=(1.0_rp/3.0_rp)*&
                   (coord_vol(idime,1) + coord_vol(idime,2)+ coord_vol(idime,3))
           end do
           
           !Normal vectors (node1node2 x node1node3 , should point outward if the mesh is well oriented)
           
           ax=coord_vol(1,2) - coord_vol(1,1)
           ay=coord_vol(2,2) - coord_vol(2,1)
           az=coord_vol(3,2) - coord_vol(3,1)  
           
           bx=coord_vol(1,3) - coord_vol(1,1)
           by=coord_vol(2,3) - coord_vol(2,1)
           bz=coord_vol(3,3) - coord_vol(3,1)
           
           venor(1)=-(ay*bz)+(az*by)
           venor(2)=-(az*bx)+(ax*bz) 
           venor(3)=-(ax*by)+(ay*bx)                        
           
           norme=sqrt(venor(1)**2.0_rp + venor(2)**2.0_rp + venor(3)**2.0_rp)  !|axb|
           
           !area 
           super=abs(norme)*0.5_rp   
           
           super_total=super_total+super 
           
           
           !Make the normal vector a unit vect. 
           venor(1)=-venor(1)/norme
           venor(2)=-venor(2)/norme
           venor(3)=-venor(3)/norme  

           
           ! V = (1/3) int_S [coord . normal] dS
           !   we define F = (1/3)*coord . normal
           ! V = F int_S [dA] = F SUM[A . w_i]
           
!!           fonct=(1.0_rp/3.0_rp)*(gpele(1)*venor(1) + gpele(2)*venor(2) + gpele(3)*venor(3))
           fonct=(gpele(1)*venor(1) + gpele(2)*venor(2) + gpele(3)*venor(3))
           volum(ivolu) = volum(ivolu) + fonct*super           
           
        end if
     end do !iboun
     
  end if !NOTMASTER

  call pararr('SUM',0_ip,mcavi_sld,volum)

  call pararr('SUM',0_ip,1_ip,super_total)

  do ivolu=1,mcavi_sld !swap all the cavities defined
     
     if (ittim==1) then
        !write(6,*) "hola",kfl_paral, volum(ivolu), ivolu
        volst_sld(2,ivolu) =   volum(ivolu)
        volvr_sld(ivolu)   =   volum(ivolu)
        
     else
     
        !update variables
        volst_sld(1,ivolu)=volst_sld(2,ivolu)  !V
        
        !deltaV
        volst_sld(2,ivolu)=volum(ivolu)
        volst_sld(4,ivolu) = volst_sld(2,ivolu)-volst_sld(1,ivolu) !deltaV new
        
        
     end if

     if (mcavi_sld == 1) then
        !
        ! When there is one ventricle only, put 1.0 in the second one volume
        !
        volst_sld(1,2)=1.0_rp
        
        !deltaV
        volst_sld(2,2) = 1.0_rp
        volst_sld(4,2) = 1.0_rp

     end if

     !
     ! Broadcast values to the slaves
     !  
     call pararr('BCT',0_ip,12_ip,volst_sld(1,ivolu))

  end do

100 format (2(F16.8,','))  
101 format (6(F19.11,',')) 
end subroutine sld_volume
