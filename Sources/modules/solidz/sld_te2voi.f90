subroutine sld_te2voi(trans, tenso, voigt, ndime)
    !-----------------------------------------------------------------------
    !****f* Solidz/sld_te2voi.f90
    ! NAME
    !    sld_te2voi
    ! DESCRIPTION
    !    Transformation from Tensor -> Voigt / Voigt -> Tensor
    ! IMPLEMENTED BY
    !    Adrià Quintanas (u1902492@campus.udg.edu)
    ! VERSION
    !    October 2014 
    !***
    !-----------------------------------------------------------------------
    
    use      def_kintyp  
    
    implicit none
    
	character(2),   intent(in)     ::    trans
	integer(ip),    intent(in)     ::    ndime
    real(rp),       intent(inout)  ::    tenso(ndime,ndime,ndime,ndime), voigt(ndime*2,ndime*2)   
	integer(ip)                    ::    i,j,k,l,a,b                                              ! Tensor index: i,j,k,l // Voigt index: a,b
	real(rp)                       ::    tkron(ndime,ndime)
    
	

	! -------------------------------
    ! INITIALIZE VARIABLES
    !
    
    !
    ! Kronecker delta
    !
    tkron = 0.0_rp
    do i = 1, ndime
    	tkron(i,i) = 1.0_rp
    end do
	
	
    ! -------------------------------
    ! TRANSFORMATION
    !    The tensor index with the Voigt index relationship is:
	!        11-1, 22-2, 33-3, 23-4, 13-5, 12-6
    !    Then the mapping ij -> a and kl -> b is: 
	!        a = i*delta_{ij} + (1 - delta_{ij})*(9 - i - j)
    !        b = k*delta_{kl} + (1 - delta_{kl})*(9 - k - l)
    !    
	
	!
	! Stiffness: Voigt -> tensor
	!    C_{ijkl} = C_{ab}
	if(trans == 'CV') then

        do l = 1, ndime
            do k = 1, ndime
                do j = 1, ndime
                    do i = 1, ndime
                        a = i*tkron(i, j) + (1 - tkron(i, j))*(9 - i - j)
                        b = k*tkron(k, l) + (1 - tkron(k, l))*(9 - k - l)
                        tenso(i, j, k, l) = voigt(a, b)
                    end do
                end do
            end do
        end do

	!
	! Stiffness: tensor -> Voigt
	!    C_{ab} = C_{ijkl}
    else if (trans == 'CT') then
	    
		do l = 1, ndime
            do k = 1, ndime
                do j = 1, ndime
                    do i = 1, ndime
                        a = i*tkron(i, j) + (1 - tkron(i, j))*(9 - i - j)
                        b = k*tkron(k, l) + (1 - tkron(k, l))*(9 - k - l)
                        voigt(a, b) = tenso(i, j, k, l)
                    end do
                end do
            end do
        end do

    !
	! Compliance: Voigt -> tensor
	!    H_{ijkl} = (1/K)*H_{ab} w/ K = 1 if a and b E [1,2,3]
	!                               K = 2 if a or  b E [4,5,6]
	!                               K = 4 if a and b E [4,5,6]
	else if (trans == 'SV') then
	
	    do l = 1, ndime
            do k = 1, ndime
                do j = 1, ndime
                    do i = 1, ndime
                        a = i*tkron(i, j) + (1 - tkron(i, j))*(9 - i - j)
                        b = k*tkron(k, l) + (1 - tkron(k, l))*(9 - k - l)
						if (a <= 3_ip .AND. b <= 3_ip) then
						    tenso(i, j, k, l) = voigt(a, b)
						else if (a > 3_ip .OR. b > 3_ip) then
						    tenso(i, j, k, l) = 0.50_rp*voigt(a, b)
						else if (a > 3_ip .AND. b > 3_ip) then
						    tenso(i, j, k, l) = 0.25_rp*voigt(a, b)
                        end if
                    end do
                end do
            end do
        end do

    !
	! Compliance: tensor -> Voigt
	!    H_{ijkl} = K*H_{ab}  where K = 1 if a and b E [1,2,3]
	!                               K = 2 if a or  b E [4,5,6]
	!                               K = 4 if a and b E [4,5,6]
	else if (trans == 'SV') then
	
	    do l = 1, ndime
            do k = 1, ndime
                do j = 1, ndime
                    do i = 1, ndime
                        a = i*tkron(i, j) + (1 - tkron(i, j))*(9 - i - j)
                        b = k*tkron(k, l) + (1 - tkron(k, l))*(9 - k - l)
						if (a <= 3_ip .AND. b <= 3_ip) then
						    voigt(a, b) = tenso(i, j, k, l)
						else if (a > 3_ip .OR. b > 3_ip) then
						    voigt(a, b) = 2.0_rp*tenso(i, j, k, l)
						else if (a > 3_ip .AND. b > 3_ip) then
						    voigt(a, b) = 4.0_rp*tenso(i, j, k, l)
                        end if
                    end do
                end do
            end do
        end do
		
    end if
    	
end subroutine sld_te2voi
