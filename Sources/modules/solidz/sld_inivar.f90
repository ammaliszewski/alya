!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_inivar.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Initialize some variables
!> @details Initialize some variables
!> @} 
!-----------------------------------------------------------------------
subroutine sld_inivar(itask)
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solidz
  use      def_solver
  implicit none
  integer(ip), intent(in) :: itask   !> when is the subroutine called

  integer(ip)  :: ipoin,kpoin,idime,imate,ivari,izone,icomp,ielem,inode
  integer(4)   :: istat
  real(rp)     :: rkeps,fimod,dummr(3),vauxi(3),vmodu

  select case(itask)

  case(0)
     !
     ! Postprocess variable names
     !
     !postp(1) % wopos  = ''

     postp(1) % wopos ( 1, 1) = 'DISPL'
     postp(1) % wopos ( 1, 2) = 'VELOC'
     postp(1) % wopos ( 1, 3) = 'ACCEL'
     postp(1) % wopos ( 1, 4) = 'SIGMA'
     postp(1) % wopos ( 1, 5) = 'EPSIL'
     postp(1) % wopos ( 1, 6) = 'LNEPS'
     postp(1) % wopos ( 1, 7) = 'FIBER'
     postp(1) % wopos ( 1, 8) = 'SEQVM'
     postp(1) % wopos ( 1, 9) = 'SEQIN'
     postp(1) % wopos ( 1,10) = 'FIBEL'
     postp(1) % wopos ( 1,11) = 'DXFEM'
     postp(1) % wopos ( 1,12) = 'VXFEM'
     postp(1) % wopos ( 1,13) = 'AXFEM'
     postp(1) % wopos ( 1,14) = 'LINEL'
     postp(1) % wopos ( 1,15) = 'NSIGN'
     postp(1) % wopos ( 1,16) = 'XYROT'
     postp(1) % wopos ( 1,17) = 'ZXROT'
     postp(1) % wopos ( 1,18) = 'QUALI'
     postp(1) % wopos ( 1,19) = 'LNENR'
     postp(1) % wopos ( 1,20) = 'ERROR'
     postp(1) % wopos ( 1,21) = 'CRACK'
     postp(1) % wopos ( 1,22) = 'GROUP'
     postp(1) % wopos ( 1,23) = 'SIGEI'
     postp(1) % wopos ( 1,24) = 'DISPX'
     postp(1) % wopos ( 1,25) = 'DISPY'
     postp(1) % wopos ( 1,26) = 'DISPZ'
     postp(1) % wopos ( 1,33) = 'FIXNO'
     postp(1) % wopos ( 1,34) = 'EXAFO'
     postp(1) % wopos ( 1,35) = 'FORCF'
     postp(1) % wopos ( 1,36) = 'SEGMA'
     postp(1) % wopos ( 1,37) = 'FRXID'
     postp(1) % wopos ( 1,38) = 'SIGNP'
     postp(1) % wopos ( 1,39) = 'SIGLO'   ! Cauchy stress (sigma) according to the local coordinate system 
     postp(1) % wopos ( 1,40) = 'STAT1'
     postp(1) % wopos ( 1,41) = 'STAT2'
     postp(1) % wopos ( 1,42) = 'STAT3'
     postp(1) % wopos ( 1,43) = 'STAT4'
     postp(1) % wopos ( 1,44) = 'RESID'
     postp(1) % wopos ( 1,45) = 'NSIGM'
     postp(1) % wopos ( 1,46) = 'STATT'
     postp(1) % wopos ( 1,47) = 'TOUCH'
     postp(1) % wopos ( 1,48) = 'NFIXN'
     postp(1) % wopos ( 1,49) = 'TSIGM'
     postp(1) % wopos ( 1,50) = 'DIISO'
     postp(1) % wopos ( 1,51) = 'VMISO'

     postp(1) % wopos ( 1,52) = 'TRACE'
     postp(1) % wopos ( 1,53) = 'NOMIN'
     postp(1) % wopos ( 1,54) = 'NNSIG'
     postp(1) % wopos ( 1,55) = 'NNOMI'
     postp(1) % wopos ( 1,56) = 'KDTRE'

     postp(1) % wopos ( 1,57) = 'EPSIS'   ! Infinitesimal stresses
     postp(1) % wopos ( 1,58) = 'PK2'     ! Second Piola Kirchoff
     
     postp(1) % wopos ( 1,59) = 'INTLI'   ! PLEPP interior list points

     postp(1) % wopos ( 3,18) = 'NELEM'   ! quali is given per-element
     postp(1) % wopos ( 3,36) = 'NELEM'   ! Element-wise sig

     postp(1) % wopos ( 2, 1) = 'VECTO'
     postp(1) % wopos ( 2, 2) = 'VECTO'
     postp(1) % wopos ( 2, 3) = 'VECTO'
     postp(1) % wopos ( 2, 4) = 'TENSO'
     postp(1) % wopos ( 2, 5) = 'TENSO'
     postp(1) % wopos ( 2, 6) = 'TENSO'
     postp(1) % wopos ( 2, 7) = 'VECTO'
     postp(1) % wopos ( 2, 8) = 'SCALA'
     postp(1) % wopos ( 2, 9) = 'SCALA'
     postp(1) % wopos ( 2,10) = 'R3PVE'
     postp(1) % wopos ( 2,11) = 'VECTO'
     postp(1) % wopos ( 2,12) = 'VECTO'
     postp(1) % wopos ( 2,13) = 'VECTO'
     postp(1) % wopos ( 2,14) = 'SCALA'
     postp(1) % wopos ( 2,15) = 'SCALA'
     postp(1) % wopos ( 2,16) = 'SCALA'
     postp(1) % wopos ( 2,17) = 'SCALA'
     postp(1) % wopos ( 2,18) = 'SCALA'
     postp(1) % wopos ( 2,19) = 'SCALA'
     postp(1) % wopos ( 2,20) = 'VECTO'
     postp(1) % wopos ( 2,21) = 'SCALA'
     postp(1) % wopos ( 2,22) = 'SCALA'
     postp(1) % wopos ( 2,23) = 'SCALA'
     postp(1) % wopos ( 2,24) = 'SCALA'
     postp(1) % wopos ( 2,25) = 'SCALA'
     postp(1) % wopos ( 2,26) = 'SCALA'
     postp(1) % wopos ( 2,33) = 'VECTO'
     postp(1) % wopos ( 2,34) = 'VECTO'
     postp(1) % wopos ( 2,35) = 'VECTO'
     postp(1) % wopos ( 2,36) = 'R3PVE'
     postp(1) % wopos ( 2,37) = 'VECTO'
     postp(1) % wopos ( 2,38) = 'R3PVE'
     postp(1) % wopos ( 2,39) = 'R3PVE'
     postp(1) % wopos ( 2,40) = 'SCALA'
     postp(1) % wopos ( 2,41) = 'SCALA'
     postp(1) % wopos ( 2,42) = 'SCALA'
     postp(1) % wopos ( 2,43) = 'SCALA'
     postp(1) % wopos ( 2,44) = 'VECTO'
     postp(1) % wopos ( 2,45) = 'VECTO'
     postp(1) % wopos ( 2,46) = 'R3PVE'
     postp(1) % wopos ( 2,47) = 'SCALA'
     postp(1) % wopos ( 2,48) = 'SCALA'
     postp(1) % wopos ( 2,49) = 'VECTO'
     postp(1) % wopos ( 2,50) = 'SCALA'
     postp(1) % wopos ( 2,51) = 'SCALA'

     postp(1) % wopos ( 2,52) = 'SCALA'
     postp(1) % wopos ( 2,53) = 'VECTO'
     postp(1) % wopos ( 2,54) = 'SCALA'
     postp(1) % wopos ( 2,55) = 'SCALA'
     postp(1) % wopos ( 2,56) = 'VECTO'
    
     postp(1) % wopos ( 2,57) = 'TENSO'
     postp(1) % wopos ( 2,58) = 'TENSO'

     postp(1) % wopos ( 2,59) = 'SCALA'

     postp(1) % wobse (1)     = 'FORCE'       ! Force
     postp(1) % wobse (2)     = 'F_y'
     postp(1) % wobse (3)     = 'F_z'
     postp(1) % wobse (4)     = 'NORMA'       ! Normal Force
     postp(1) % wobse (5)     = 'FRBOX'       ! Reaction forces at boundaries
     postp(1) % wobse (6)     = 'FRBOY'
     postp(1) % wobse (7)     = 'FRBOZ'

     postp(1) % wonse (1)     = 'DISPX'       ! x-Displacement
     postp(1) % wonse (2)     = 'DISPY'       ! y-Displacement
     postp(1) % wonse (3)     = 'DISPZ'       ! z-Displacement
     postp(1) % wonse (4)     = 'TREFF'       ! Effective traction
     postp(1) % wonse (5)     = 'FRXIX'       ! x-Reaction forces
     postp(1) % wonse (6)     = 'FRXIY'       ! y-Reaction forces
     postp(1) % wonse (7)     = 'FRXIZ'       ! z-Reaction forces


     !
     ! Witness variables
     !
     postp(1) % wowit (1)     = 'DISPX'
     postp(1) % wowit (2)     = 'DISPY'
     postp(1) % wowit (3)     = 'DISPZ'
     postp(1) % wowit (4)     = 'SIGXX'
     postp(1) % wowit (5)     = 'SIGYY'
     postp(1) % wowit (6)     = 'SIGZZ'
     postp(1) % wowit (7)     = 'SIGYZ'
     postp(1) % wowit (8)     = 'SIGZX'
     postp(1) % wowit (9)     = 'SIGXY'
     postp(1) % wowit (10)    = 'SEQVM'     
     postp(1) % wowit (11)    = 'SIGFI'

     !
     ! Solvers
     !
     call soldef(-2_ip)                       ! Allocate memory for solvers
     solve(1) % kfl_solve = 1                 ! Output flag
     solve(1) % wprob     = 'DISPLACEMENT'
     solve(1) % ndofn     = ndime

     solve(1) % kfl_algso = 9
     solve(1) % kfl_preco  = SOL_LOCAL_MASS_MATRIX  ! explicit is the default

     solve(2) % kfl_solve = 1                 ! Output flag ! OJO CAMBIAR
     solve(2) % wprob     = 'DISP-ROT'
     solve(2) % ndofn     = 6
     !solve(3) % kfl_solve = 1                ! Output flag

     !
     ! Materials
     !
     nmate_sld =  nmate
     lmate_sld => lmate
     nvoig_sld = 3
     if (ndime == 3) nvoig_sld = 6

     rorig_sld(1) = 0.0_rp
     rorig_sld(2) = 0.0_rp
     rorig_sld(3) = 0.0_rp

     if(.not.(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 ))) then
        ! when there is no coupling with exmedi
        kfl_ephys= 0
     end if

     !
     ! State variables
     !
     nstat_sld = 12_ip

     !
     ! Relaxation parameters
     !
     relpa_sld = 0.0_rp ! for solvers
     relco_sld = 1.0_rp ! for coupling

     !
     ! Nullify pointers
     !
     nullify(cause_sld)

     !
     ! Composite stuff                                                                              ( *AQU* )
     !
     nullify(state_sld)                          ! Internal state variables
     nullify(fibeb_sld)                          ! Local coordinate system
     nullify(fibes_sld)
     nullify(fiben_sld)
     nullify(caunp_sld)                          ! Ouput
     nullify(caulo_sld)

  case(1)
     if (kfl_xfeme_sld == 1) then
        solve(1) % ndofn = 2*ndime
        !solve(1) % kfl_xfeme = 1
        kfl_lface = 1                      ! Requires global list of faces
        !
        ! Second solver: the enrichment field (X-FEM or alike)
        !
        !        solve(2) % ndofn     = ndime
        !        solve(2) % wprob     = 'DISP_XFEM'
     end if

     !
     ! Dimensions
     !
     ndofn_sld = ndime
     if (kfl_xfeme_sld == 1) ndofn_sld = 2*ndime
     nevat_sld = ndofn_sld*mnode
     nevab_sld = ndime*mnode
     ndof2_sld = ndofn_sld*ndofn_sld
     nunkn_sld = ndofn_sld*npoin
     ntens_sld = 2*ndime
     !
     ! Push
     !
     ivari = 7
     call posdef(25_ip,ivari)
     if( ivari /= 0 ) kfl_gdepo_sld = 1
!      if( kfl_coupl(ID_SOLIDZ,ID_NASTIN) /= 0 ) then
!         kfl_gdepo_sld = 1
!      end if
     !
     ! Number of temporal displacement components
     !
     !      if(kfl_timet_sld==1) then
     !         ncomp_sld=4                         ! Explicit Newmark scheme
     !      else
     !         if(kfl_tisch_sld==1 .or. kfl_tisch_sld==2) then
     !            ncomp_sld=4                      ! Trapezoidal-like rules
     !         else if(kfl_tisch_sld==5) then
     !            ncomp_sld=3+kfl_tiacc_sld        ! BDF scheme
     !         end if
     !      end if
     ncomp_sld = 4
     !
     ! Time accuracy: save original value
     !
     kfl_tiaor_sld=kfl_tiacc_sld
     if( kfl_timei_sld /= 0 ) kfl_timei = 1
     !
     ! Initial (REFERENCE) displacements field: COORD IS CHANGED when not stressed
     !
!!$     inode= 4
!!$     write (1932,'(a)') 'NODES_PER_ELEMENT'
!!$     do ielem=1,nelem
!!$        write (1932,*) ielem,inode
!!$     end do
!!$     write (1932,'(a)') 'END_NODES_PER_ELEMENT'
!!$     write (1932,'(a)') 'ELEMENTS'
!!$     do ielem=1,nelem
!!$        write (1932,200) ielem,lnods(1:4,ielem)        
!!$     end do
!!$     write (1932,'(a)') 'END_ELEMENTS'
!!$     write (1932,'(a)') 'COORDINATES'

     if (kfl_indis_sld(1) < 0) then
        if( associated(xfiel)) then
           if (INOTMASTER) then
              icomp=min(3,ncomp_sld)
              izone = lzone(ID_SOLIDZ)
              if (kfl_indis_sld(2) == 0) then
                 do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
                    do idime= 1,ndime
                       coord(idime,ipoin) = coord(idime,ipoin) &
                            + xfiel(-kfl_indis_sld(1)) % a(idime,ipoin)
                    end do
!!                    write (1932,100) ipoin,10.0_rp * coord(1:3,ipoin)
                 end do
              end if
           end if
        else
           call runend('SLD_INIVAR: NO INITIAL DISPLACEMENTS FIELD ASSOCIATED.')
        end if
     end if        
!!     write (1932,'(a)') 'END_COORDINATES'
!!100  format(3x,i8,3(3x,e12.5))
!!200  format(3x,i8,4(3x,i8))
!!     stop
     

     !
     ! Fibers
     !

     if (kfl_fiber_sld > 0 .and. kfl_fiber_sld < 5_ip) then        
        if( associated(xfiel) ) then
           do imate=1,nmate_sld
              if( modfi_sld(imate) < 0 .and. INOTMASTER ) then
                 fiber => xfiel(-modfi_sld(imate)) % a
                 print*, size(fiber,1), size(fiber,2)
                 izone = lzone(ID_SOLIDZ)
                 do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
                    fimod= 0.0_rp
                    do idime=1,ndime
                       fimod= fimod + fiber(idime,ipoin)*fiber(idime,ipoin)
                    end do
                    ! fibers are always normalized
                    fimod= sqrt(fimod)                 
                    if (fimod == 0.0_rp) fimod= 1.0_rp
                    do idime=1,ndime
                       fiber(idime,ipoin)= fiber(idime,ipoin) / fimod
                    end do
                 end do
              end if
              
              if( modor_sld(1,imate) < 0 .and. INOTMASTER ) then
                 fibts_sld => xfiel(-modor_sld(1,imate)) % a
                 do ipoin=1,npoin
                    fimod= 0.0_rp
                    do idime=1,ndime
                       fimod= fimod + fibts_sld(idime,ipoin)*fibts_sld(idime,ipoin)
                    end do
                    ! fibers are always normalized
                    fimod= sqrt(fimod)
                    if (fimod == 0.0_rp) fimod= 1.0_rp
                    do idime=1,ndime
                       fibts_sld(idime,ipoin)= fibts_sld(idime,ipoin) / fimod
                    end do
                 end do
              end if
              
              if( modor_sld(2,imate) < 0 .and. INOTMASTER ) then
                 fibtn_sld => xfiel(-modor_sld(2,imate)) % a
                 izone = lzone(ID_SOLIDZ)
                 do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
                    fimod= 0.0_rp
                    do idime=1,ndime
                       fimod= fimod + fibtn_sld(idime,ipoin)*fibtn_sld(idime,ipoin)
                    end do
                    ! fibers are always normalized
                    fimod= sqrt(fimod)
                    if (fimod == 0.0_rp) fimod= 1.0_rp
                    do idime=1,ndime
                       fibtn_sld(idime,ipoin)= fibtn_sld(idime,ipoin) / fimod
                    end do
                 end do
              end if
           end do
        end if
     end if

     !
     ! Material coordinate system                                                                     (*AQU*)
     !
     if (kfl_fiber_sld == 5_ip) then        
           fibeb_sld => xfiel(1_ip) % a
           fibes_sld => xfiel(2_ip) % a
           fiben_sld => xfiel(3_ip) % a
     end if

     !
     ! Volume force field
     !
     if( kfl_vofor_sld > 0 ) then
        vofor_sld => xfiel(kfl_vofor_sld) % a
     end if

     !
     ! Stresses field
     !
     if (kfl_restr_sld < 0) then
        if( associated(xfiel) ) then
           restr_sld => xfiel(-kfl_restr_sld) % a
        else
           call runend('SLD_INIVAR: NO STRESS FIELD ASSOCIATED.')
        end if
     end if

     !
     ! Rigidity modulator field
     !
     if( kfl_moduf_sld(1) > 0 ) then
        fiemo_sld => xfiel(kfl_moduf_sld(1)) % a
     end if

     !
     ! Precalculate constants for model 152: precalculate stiff without damage
     !
     do imate=1,nmate_sld
       if (lawst_sld(imate)==152) call sm152_precalculus(imate) 
     end do   

     !
     ! Polarization wave index
     !
     iwave_sld = 0

     !
     ! Voigt conversion
     !
     if (ndime==2) then
        nvgij_sld(1,1)= 1
        nvgij_sld(1,2)= 1
        nvgij_sld(2,1)= 2
        nvgij_sld(2,2)= 2
        nvgij_sld(3,1)= 1
        nvgij_sld(3,2)= 2
!        nvgij_inv_sld(1,1)= 1
!        nvgij_inv_sld(1,2)= 2
!        nvgij_inv_sld(2,1)= 2
!        nvgij_inv_sld(2,2)= 3
        nvgij_inv_sld(1,1)= 1
        nvgij_inv_sld(2,2)= 2
        nvgij_inv_sld(1,2)= 3
        nvgij_inv_sld(2,1)= 3
     else if (ndime==3) then
        nvgij_sld(1,1)= 1
        nvgij_sld(1,2)= 1
        nvgij_sld(2,1)= 2
        nvgij_sld(2,2)= 2
        nvgij_sld(3,1)= 3
        nvgij_sld(3,2)= 3
        nvgij_sld(4,1)= 2
        nvgij_sld(4,2)= 3
        nvgij_sld(5,1)= 1
        nvgij_sld(5,2)= 3
        nvgij_sld(6,1)= 1
        nvgij_sld(6,2)= 2
!        nvgij_inv_sld(1,1)= 1
!        nvgij_inv_sld(1,2)= 2
!        nvgij_inv_sld(1,3)= 3
!        nvgij_inv_sld(2,1)= 2
!        nvgij_inv_sld(2,2)= 4
!        nvgij_inv_sld(2,3)= 5
!        nvgij_inv_sld(3,1)= 3
!        nvgij_inv_sld(3,2)= 5
!        nvgij_inv_sld(3,3)= 6
        nvgij_inv_sld(1,1)= 1
        nvgij_inv_sld(1,2)= 6
        nvgij_inv_sld(1,3)= 5
        nvgij_inv_sld(2,1)= 6
        nvgij_inv_sld(2,2)= 2
        nvgij_inv_sld(2,3)= 4
        nvgij_inv_sld(3,1)= 5
        nvgij_inv_sld(3,2)= 4
        nvgij_inv_sld(3,3)= 3
     end if
     !
     ! Define [A]^{-1} for the extrapolation gauss-node (inverse of [A])
     ! jelty = 1 (P1-2D), 2 (Q1-2d), 3 (P1-3D) , 4 (Q1-3d)
     !
     ! t_{nodes} = [A]^{-1} * t_{Gauss points}
     !    where  [A]^{-1} = shagp_sld
     !

     !hexa
     shagp_sld(1,1,4) =  2.549038
     shagp_sld(1,2,4) = -0.683013
     shagp_sld(1,3,4) = -0.683013
     shagp_sld(1,4,4) =  0.183013
     shagp_sld(1,5,4) = -0.683013
     shagp_sld(1,6,4) =  0.183013
     shagp_sld(1,7,4) =  0.183013
     shagp_sld(1,8,4) = -0.049038

     shagp_sld(2,1,4) = -0.683013
     shagp_sld(2,2,4) =  0.183013
     shagp_sld(2,3,4) =  0.183013
     shagp_sld(2,4,4) = -0.049038
     shagp_sld(2,5,4) =  2.549038
     shagp_sld(2,6,4) = -0.683013
     shagp_sld(2,7,4) = -0.683013
     shagp_sld(2,8,4) =  0.183013

     shagp_sld(3,1,4) =  0.183013
     shagp_sld(3,2,4) = -0.049038
     shagp_sld(3,3,4) = -0.683013
     shagp_sld(3,4,4) =  0.183013
     shagp_sld(3,5,4) = -0.683013
     shagp_sld(3,6,4) =  0.183013
     shagp_sld(3,7,4) =  2.549038
     shagp_sld(3,8,4) = -0.683013

     shagp_sld(4,1,4) = -0.683013
     shagp_sld(4,2,4) =  0.183013
     shagp_sld(4,3,4) =  2.549038
     shagp_sld(4,4,4) = -0.683013
     shagp_sld(4,5,4) =  0.183013
     shagp_sld(4,6,4) = -0.049038
     shagp_sld(4,7,4) = -0.683013
     shagp_sld(4,8,4) =  0.183013

     shagp_sld(5,1,4) = -0.683013
     shagp_sld(5,2,4) =  2.549038
     shagp_sld(5,3,4) =  0.183013
     shagp_sld(5,4,4) = -0.683013
     shagp_sld(5,5,4) =  0.183013
     shagp_sld(5,6,4) = -0.683013
     shagp_sld(5,7,4) = -0.049038
     shagp_sld(5,8,4) =  0.183013

     shagp_sld(6,1,4) =  0.183013
     shagp_sld(6,2,4) = -0.683013
     shagp_sld(6,3,4) = -0.049038
     shagp_sld(6,4,4) =  0.183013
     shagp_sld(6,5,4) = -0.683013
     shagp_sld(6,6,4) =  2.549038
     shagp_sld(6,7,4) =  0.183013
     shagp_sld(6,8,4) = -0.683013

     shagp_sld(7,1,4) = -0.049038
     shagp_sld(7,2,4) =  0.183013
     shagp_sld(7,3,4) =  0.183013
     shagp_sld(7,4,4) = -0.683013
     shagp_sld(7,5,4) =  0.183013
     shagp_sld(7,6,4) = -0.683013
     shagp_sld(7,7,4) = -0.683013
     shagp_sld(7,8,4) =  2.549038

     shagp_sld(8,1,4) =  0.183013
     shagp_sld(8,2,4) = -0.683013
     shagp_sld(8,3,4) = -0.683013
     shagp_sld(8,4,4) =  2.549038
     shagp_sld(8,5,4) = -0.049038
     shagp_sld(8,6,4) =  0.183013
     shagp_sld(8,7,4) =  0.183013
     shagp_sld(8,8,4) = -0.683013

     !thetra
     shagp_sld(1,1,3) =  1.927051
     shagp_sld(1,2,3) = -0.309017
     shagp_sld(1,3,3) = -0.309017
     shagp_sld(1,4,3) = -0.309017

     shagp_sld(2,1,3) = -0.309017
     shagp_sld(2,2,3) =  1.927051
     shagp_sld(2,3,3) = -0.309017
     shagp_sld(2,4,3) = -0.309017

     shagp_sld(3,1,3) = -0.309017
     shagp_sld(3,2,3) = -0.309017
     shagp_sld(3,3,3) =  1.927051
     shagp_sld(3,4,3) = -0.309017

     shagp_sld(4,1,3) = -0.309017
     shagp_sld(4,2,3) = -0.309017
     shagp_sld(4,3,3) = -0.309017
     shagp_sld(4,4,3) =  1.927051

     !prism
     shagp_sld(1,1,5) = -0.455343
     shagp_sld(1,2,5) = -0.455343
     shagp_sld(1,3,5) =  2.276711
     shagp_sld(1,4,5) =  0.122008
     shagp_sld(1,5,5) =  0.122008
     shagp_sld(1,6,5) = -0.610042

     shagp_sld(2,1,5) =  2.276711
     shagp_sld(2,2,5) = -0.455343
     shagp_sld(2,3,5) = -0.455343
     shagp_sld(2,4,5) = -0.610042
     shagp_sld(2,5,5) =  0.122008
     shagp_sld(2,6,5) =  0.122008

     shagp_sld(3,1,5) = -0.455343
     shagp_sld(3,2,5) =  2.276711
     shagp_sld(3,3,5) = -0.455343
     shagp_sld(3,4,5) =  0.122008
     shagp_sld(3,5,5) = -0.610042
     shagp_sld(3,6,5) =  0.122008

     shagp_sld(4,1,5) =  0.122008
     shagp_sld(4,2,5) =  0.122008
     shagp_sld(4,3,5) = -0.610042
     shagp_sld(4,4,5) = -0.455343
     shagp_sld(4,5,5) = -0.455343
     shagp_sld(4,6,5) =  2.276711

     shagp_sld(5,1,5) = -0.610042
     shagp_sld(5,2,5) =  0.122008
     shagp_sld(5,3,5) =  0.122008
     shagp_sld(5,4,5) =  2.276711
     shagp_sld(5,5,5) = -0.455343
     shagp_sld(5,6,5) = -0.455343

     shagp_sld(6,1,5) =  0.122008
     shagp_sld(6,2,5) = -0.610042
     shagp_sld(6,3,5) =  0.122008
     shagp_sld(6,4,5) = -0.455343
     shagp_sld(6,5,5) =  2.276711
     shagp_sld(6,6,5) = -0.455343

     !triangle

     shagp_sld(1,1,1) = -0.333333
     shagp_sld(1,2,1) =  1.666667
     shagp_sld(1,3,1) = -0.333333

     shagp_sld(2,1,1) = -0.333333
     shagp_sld(2,2,1) = -0.333333
     shagp_sld(2,3,1) =  1.666667

     shagp_sld(3,1,1) =  1.666667
     shagp_sld(3,2,1) =  -0.333333
     shagp_sld(3,3,1) =  -0.333333

     !
     ! Witness element (debugging)
     !

     kfl_ielem= 0

     rkeps= 0.00001

     !     comin(1,1)=0.764095799999999992 -rkeps
     !     comin(2,1)=4.46877600000000008 -rkeps
     !     comin(3,1)=9.41417099999999962-rkeps
     !
     !     comin(1,2)=0.666665999999999981 -rkeps
     !     comin(2,2)=4.48533929999999970 -rkeps
     !     comin(3,2)=9.32023560000000018-rkeps
     !
     !     comin(1,3)=0.565172400000000019 -rkeps
     !     comin(2,3)=4.46982120000000016 -rkeps
     !     comin(3,3)=9.45249629999999996-rkeps
     !
     !     comin(1,4)=0.685612200000000005-rkeps
     !     comin(2,4)= 4.55427960000000009-rkeps
     !     comin(3,4)= 9.47345879999999951-rkeps
     !
     !     comax(1,1)=0.764095799999999992 +rkeps
     !     comax(2,1)=4.46877600000000008 +rkeps
     !     comax(3,1)=9.41417099999999962+rkeps
     !
     !     comax(1,2)=0.666665999999999981 +rkeps
     !     comax(2,2)=4.48533929999999970 +rkeps
     !     comax(3,2)=9.32023560000000018+rkeps
     !
     !     comax(1,3)=0.565172400000000019 +rkeps
     !     comax(2,3)=4.46982120000000016 +rkeps
     !     comax(3,3)=9.45249629999999996+rkeps
     !
     !     comax(1,4)=0.685612200000000005+rkeps
     !     comax(2,4)= 4.55427960000000009+rkeps
     !     comax(3,4)= 9.47345879999999951+rkeps

     !     if (INOTMASTER) then
     !
     !        do ielem= 1,nelem
     !           ielco= 0
     !
     !           do inode= 1,4
     !              ipoin= lnods(inode,ielem)
     !              if ((coord(1,ipoin) > comin(1,inode)).and.(coord(1,ipoin) < comax(1,inode))) then
     !                 if ((coord(2,ipoin) > comin(2,inode)).and.(coord(1,ipoin) < comax(2,inode))) then
     !                    if ((coord(3,ipoin) > comin(3,inode)).and.(coord(1,ipoin) < comax(3,inode))) then
     !                       ielco= ielco+1
     !                    end if
     !                 end if
     !              end if
     !           end do
     !
     !           if (ielco== 4) then
     !              kfl_ielem = ielem
     !           end if
     !        end do
     !     end if
     !
     !     if (kfl_ielem > 0) then
     !
     !        write(6,*)
     !        write(6,*) '----------------------Listo calisto:',kfl_ielem,kfl_paral
     !        write(6,*)
     !
     !     end if

  case (2)

     !
     ! Correct fibers when required
     !

     if (kfl_fiber_sld == 3 .and. INOTMASTER) then
        
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           
           vauxi   = 0.0_rp
           vauxi(1)= 1.0_rp
           call vecpro(fiber(1,ipoin),vauxi,fibts_sld(1,ipoin),ndime)
           dummr(1:ndime) = fibts_sld(1:ndime,ipoin)
           vmodu= sqrt(dummr(1)*dummr(1) + dummr(2)*dummr(2) + dummr(3)*dummr(3))
           if (vmodu < 1.0e-10) then
              vauxi   = 0.0_rp
              vauxi(2)= 1.0_rp
              call vecpro(fiber(1,ipoin),vauxi,fibts_sld(1,ipoin),ndime)
              dummr(1:ndime) = fibts_sld(1:ndime,ipoin)
              vmodu= sqrt(dummr(1)*dummr(1) + dummr(2)*dummr(2) + dummr(3)*dummr(3))                    
           end if
           
           fibts_sld(1:ndime,ipoin) = fibts_sld(1:ndime,ipoin)/vmodu
           
           call vecpro(fiber(1,ipoin),fibts_sld(1,ipoin),fibtn_sld(1,ipoin),ndime)
           dummr(1:ndime) = fibtn_sld(1:ndime,ipoin)
           vmodu= sqrt(dummr(1)*dummr(1) + dummr(2)*dummr(2) + dummr(3)*dummr(3))
           
           fibtn_sld(1:ndime,ipoin) = fibtn_sld(1:ndime,ipoin)/vmodu
           
        end do

     end if

     !
     ! Initial (REFERENCE) displacements field: DISPL IS SET when stressed
     !
     if (kfl_indis_sld(1) < 0) then
        if( associated(xfiel)) then
           if (INOTMASTER) then
              icomp=min(3,ncomp_sld)
              izone = lzone(ID_SOLIDZ)
              if (kfl_indis_sld(2) == 1) then
                 do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
                    do idime= 1,ndime
                       displ(idime,ipoin,icomp) = xfiel(-kfl_indis_sld(1)) % a(idime,ipoin)
                    end do
                 end do
              end if
           end if
        else
           call runend('SLD_INIVAR: NO INITIAL DISPLACEMENTS FIELD ASSOCIATED.')
        end if
     end if        

  case(3)
     !
     ! Solver
     !
     solve_sol            => solve(1:)
     solve(1) % bvess     => bvess_sld(:,:,1)
     solve(1) % bvnat     => bvnat_sld(:,:,1)
     solve(1) % kfl_fixno => kfl_fixno_sld



     resti_sld = 0.0_rp
     
  end select

end subroutine sld_inivar



