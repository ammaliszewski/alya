subroutine sld_timste
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_timste
  ! NAME 
  !    sld_timste
  ! DESCRIPTION
  !    This routine prepares for a new time step of SOLIDZ
  !    equation      
  ! USES
  !    sld_iniunk
  !    sld_updtss
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  implicit none
  !
  ! Latex output format
  !
  if(ittim==0) call sld_outlat(one)
  !
  ! For the first time step, set up the initial condition
  !
  if(ittim==0) then
     !      kfl_stead_sld = 0
     call sld_updbcs(zero)
  end if
  !
  ! Time step size
  !
  !   if(kfl_stead_sld/=1) then
  call sld_updtss()
  !   end if

end subroutine sld_timste

