subroutine sld_builtin_materials(pgaus,pmate,temp0,temp1,&
        gpgi0,gpgi1,gpigd,gpcau,gpdet,gprat,gppio,gpstr,gpivo,&
        gpene,flagt,gptmo,flags,ggsgo,ielem,gptlo,nfibe,elcod,pnode,lnods,gpsha,gpdds,gpmof)

!-----------------------------------------------------------------------
!****f* Solidz/sld_builtin_materials
! NAME
!    sld_builtin_materials
! DESCRIPTION
!    Interface for calling built-in subroutines
! INPUT
!    PGAUS ... Number of Gauss points
!    PMATE ... Material number
!    TEMP0 ... Previous temperature ............................ temp(n)
!    TEMP1 ... Updated temperature ............................. temp(n+1)
!    GPGI0 ... Previous deformation gradient tensor ............ F(n)
!    GPGI1 ... Updated deformation gradient tensor ............. F(n+1)
!    GPIGD ... Inverse of updated deformation gradient tensor .. F^{-1}(n+1)
!    GPCAU ... Updated right Cauchy-Green tensor ............... C(n+1)
!    GPDET ... Updated jacobian ................................ J = det(F(n+1))
!    GPRAT ... Rate of Deformation tensor ...................... Fdot = grad(phidot)
!    FLAGT ... Flag for tangent moduli calculations ............ 1 if yes; 0 if no
!    FLAGS ... Flag for strain gradient calculations ........... 1 if yes; 0 if no
! INPUT/OUTPUT
!    GPPIO ... 1st Piola-Kirchhoff stress tensor at t(n)/t(n+1)  P(n)/P(n+1)
!    GPSTR ... 2nd Piola-Kirchhoff stress tensor at t(n)/t(n+1)  S(n)/S(n+1)
!    GPIVO ... Internal variable array at t(n)/t(n+1) .......... Q(n)/Q(n+1)
!    GPENE ... Stored energy function .......................... W
! OUTPUT
!    GPTMO ... Tangent moduli at t(n+1) ........................ dP/dF(n+1)
!    GGSGO ... Deformation gradient gradient tensor ............ dF/dX(n+1)
! USES
!    sld_stress_model_xxx ................... constitutive law xxx
!    (other subroutines to be added as necessary)
! USED BY
!    sld_elmcla
!***
!-----------------------------------------------------------------------

  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,mnode
  use def_solidz

  implicit none
  integer(ip), intent(in)    :: pgaus,pmate,ielem,pnode,lnods(pnode)
  real(rp),    intent(in)    :: temp0(pgaus)
  real(rp),    intent(in)    :: temp1(pgaus)
  real(rp),    intent(in)    :: gpgi0(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpgi1(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpcau(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpdet(pgaus),gpmof(pgaus)
  real(rp),    intent(in)    :: gprat(ndime,ndime,pgaus)
  integer(ip), intent(in)    :: flagt,flags
  real(rp),    intent(in)    :: gpigd(ndime,ndime,pgaus)
  real(rp),    intent(inout) :: gppio(ndime,ndime,pgaus)
!   real(rp),    intent(out)   :: gpgre(ndime,ndime,pgaus)
  real(rp),    intent(inout) :: gpstr(ndime,ndime,pgaus)
!   real(rp),    intent(inout) :: gplep(ndime,ndime,pgaus)
  real(rp),    intent(out)   :: nfibe(ndime,pgaus)
  real(rp),    intent(inout) :: gpivo(nvint_sld,pgaus)
  real(rp),    intent(inout) :: gpene(pgaus)
  real(rp),    intent(out)   :: gptmo(ndime,ndime,ndime,ndime,pgaus)
  real(rp),    intent(out)   :: ggsgo(ndime,ndime)
  real(rp),    intent(in)    :: gptlo(pgaus)
  real(rp),    intent(out)   :: gpdds(ndime,ndime,ndime,ndime,pgaus)
  integer(ip)                :: inode,igaus,idime,jdime,kdime,ldime,mm,nn
  real(rp)                   :: detf0
  real(rp)                   :: tkron(ndime,ndime)
!   real(rp)                   :: gpcrt(ndime,ndime),eval(ndime),evec(ndime,ndime)
  real(rp),    intent(in)    :: elcod(ndime,mnode)
  real(rp),    intent(in)    :: gpsha(pnode,pgaus)

  logical                    :: debuggingMode

  ! declaration of flags for additional inputs/outputs

  integer(ip)  :: flgs1,flgs3

  flgs1 = 0_ip ! flag for 1st P-K Stress tensor from 2nd P-K in output P(n+1)
  flgs3 = 0_ip ! flag for tangent moduli from material stress tangent  dP/dF

  ! touch the values of the "output" so that the compiler does think it is used
  ! (to be erased as soon as there exists one subroutine that uses it)

  ggsgo(1,1)       = 0.0_rp
  gptmo(1,1,1,1,1) = 0.0_rp

  debuggingMode = .false. ! (ielem == 1_ip)

  ! Kronecker delta

  tkron = 0.0_rp
  do idime = 1, ndime
      tkron(idime,idime) = 1.0_rp
  end do


  ! define additional inputs/outputs when needed

  if (lawst_sld(pmate)==100) then                     ! isolinear elastic
        flgs1 = 1_ip                                  
        flgs3 = 1_ip                                  
  else if (lawst_sld(pmate)==101) then                ! Neo-Hookean belytschko (textbook)
        flgs1 = 1_ip                                  
        flgs3 = 1_ip                                  
  else if (lawst_sld(pmate)==102) then                ! Neo-Hookean Ansys
        flgs1 = 1_ip                                  
        flgs3 = 1_ip                                  
  else if (lawst_sld(pmate)==103) then                ! Mooney-Rivlin
        flgs1 = 1_ip                                  
        flgs3 = 1_ip                                  
  else if (lawst_sld(pmate)==105) then                ! constant spring
        flgs1 = 1_ip                                  
  else if (lawst_sld(pmate)==133) then                ! Yin & Lin
        flgs1 = 1_ip                                  
  else if (lawst_sld(pmate)==1331) then                ! Yin & Lin
        flgs1 = 1_ip                                  
  else if (lawst_sld(pmate)==134) then                ! Holza & Ogden
        flgs1 = 1_ip                                  
        flgs3 = 1_ip                                  
  else if (lawst_sld(pmate)==135) then                ! Pole-Zero
        flgs1 = 1_ip                                   
  else if (lawst_sld(pmate)==136) then                ! Guccione
        flgs1 = 1_ip
        flgs3 = 1_ip
  else if (lawst_sld(pmate)==151) then                ! Ortholinear elastic
        flgs1 = 1_ip                                  
        flgs3 = 1_ip                                  
  else if (lawst_sld(pmate)==152) then                ! Transversally isotropic damage (Maimi 2016)
        flgs1 = 1_ip                                  
        flgs3 = 1_ip                                  
  else if (lawst_sld(pmate)==153) then                ! Isolinear damage law (Oliver 1990)
        flgs1 = 1_ip
        flgs3 = 1_ip
  end if


  ! call built-in material constitutive laws

  ! Use tangent moduli (only for implicit computations)
   if (kfl_tange_sld==2_ip .and. flagt==1_ip ) then
     call sld_dertan(pgaus,pmate,temp0,temp1,&
       gpgi0,gpgi1,gpigd,gpcau,gpdet,gprat,gppio,gpstr,gpivo,&
       gpene,flagt,gptmo,flags,ggsgo,ielem,gptlo,nfibe,elcod,pnode,lnods,gpsha,gpdds,gpmof)
     
  else

    if (lawst_sld(pmate)==100) then                                         ! isolinear elastic
       !call sld_stress_model_100(pgaus,pmate,gpgi1,gpstr)
       call sld_stress_model_100(pgaus,pmate,gpgi1,gpstr,gpcau,gpdet,gptlo,gpigd,ielem,elcod,flagt,gpdds,gpmof)    !with activation
    else if (lawst_sld(pmate)==101) then                                    ! Neo-Hookean belytschko (textbook)
       !call sld_stress_model_101(pgaus,pmate,gpcau,gpgi1,gpene,gpstr,gpdet)
       call sld_stress_model_101(pgaus,pmate,gpcau,gpgi1,gpene,gpstr,gpdet,flagt,gpdds,gpmof)
    else if (lawst_sld(pmate)==102) then                                    ! Neo-Hookean Ansys
       call sld_stress_model_102(pgaus,pmate,gpgi1,gpstr,gpdet,flagt,gpdds,gpmof)
    else if (lawst_sld(pmate)==103) then
       call sld_stress_model_103(pgaus,pmate,gpgi1,gpstr,gpdet,flagt,gpdds,gpmof) ! Mooney-Rivlin (Belytschko notation)
    else if (lawst_sld(pmate)==105) then                                    ! constant spring
       call sld_stress_model_105(pgaus,pmate,gpstr,flagt,gpdds,gpmof)
    else if (lawst_sld(pmate)==133) then                                    ! Yin & Lin with Peteron's EC coupling integrated
       if (flagt == 1_ip)   &
         call runend('SLD_BUILTIN_MATERIALS: STRESS TANGENT NOT PROGRAMMED YET FOR THIS MODEL')
         call sld_stress_model_133(pgaus,pmate,gpcau,gpstr,gpdet,gptlo,ielem,elcod,gpmof)
       else if (lawst_sld(pmate)==1331) then                                    ! Yin & Lin only passive properties
         if (flagt == 1_ip)   &
          call runend('SLD_BUILTIN_MATERIALS: STRESS TANGENT NOT PROGRAMMED YET FOR THIS MODEL')
          call sld_stress_model_133a(pgaus,pmate,gpgi1,gpigd,gpcau,gpstr,gpdet,gptlo,ielem,elcod,gpmof)
       else if (lawst_sld(pmate)==134) then                                    ! Holza & Ogden
         call sld_stress_model_134(pgaus,pmate,gpcau,gpgi1,gpigd,gpstr,gpdet,gptlo,nfibe,ielem,elcod,&
           pnode,lnods,gpsha,flagt,gpdds,gpmof)
       else if (lawst_sld(pmate)==135) then
         if (flagt == 1_ip)   &
           call runend('SLD_BUILTIN_MATERIALS: STRESS TANGENT NOT PROGRAMMED YET FOR THIS MODEL')
           call sld_stress_model_135(pgaus,pmate,gpcau,gpgi1,gpigd,gpstr,gpdet,pnode,elcod,lnods,ielem,gpsha,nfibe,gpmof)
       else if (lawst_sld(pmate)==136) then
           call sld_stress_model_136(pgaus,pmate,gpcau,gpgi1,gpigd,gpstr,gpdet,gptlo,nfibe,ielem,elcod,&
             pnode,lnods,gpsha,flagt,gpdds,gpmof)
       else if (lawst_sld(pmate)==151) then
           call sld_stress_model_151(pgaus,pmate,gpgi1,gpstr,ielem,flagt,gpdds)
       else if (lawst_sld(pmate)==152) then
           call sld_stress_model_152(pgaus,pmate,gpgi1,gpstr,ielem,flagt,gpdds)
       else if (lawst_sld(pmate)==153) then
           call sld_stress_model_153(pgaus,pmate,gpgi1,gpstr,ielem,flagt,gpdds)
       end if
    end if

  ! calculate required outputs from additional outputs when needed

  if ( flgs1 == 1_ip ) then
     !
     ! Calculate P (1st Piola-Kirchhoff stress tensor)
     !
     ! P_jK = F_jI * S_IK
     !
     do igaus=1,pgaus
        do kdime=1,ndime 
           do jdime=1,ndime
              gppio(jdime,kdime,igaus)=0.0_rp
              do idime=1,ndime
                 gppio(jdime,kdime,igaus)= &
                      gppio(jdime,kdime,igaus) + &
                      gpgi1(jdime,idime,igaus)*gpstr(idime,kdime,igaus)
              enddo
           enddo
        enddo
     end do


  endif


   if ( flgs3*flagt == 1_ip ) then

     !Calculate dPdF (tangent of 1st Piola-Kirchhoff stress tensor wrt. deformation gradient)
     !
     ! dPdF_iJkL = delta_ik * S_JL + F_iM * FkN * dSdE_MJNL
     !
     do igaus=1,pgaus
        do idime=1,ndime; do jdime=1,ndime; do kdime=1,ndime; do ldime=1,ndime
           gptmo(idime,jdime,kdime,ldime,igaus) = 0.0_rp
           do mm=1,ndime; do nn=1,ndime
              gptmo(idime,jdime,kdime,ldime,igaus)= &
                   gptmo(idime,jdime,kdime,ldime,igaus) + &
                   gpdds(mm,jdime,nn,ldime,igaus)*gpgi1(idime,mm,igaus)*gpgi1(kdime,nn,igaus)
           enddo; enddo
           gptmo(idime,jdime,kdime,ldime,igaus)=&
                   gptmo(idime,jdime,kdime,ldime,igaus) + &
                   tkron(idime,kdime)*gpstr(jdime,ldime,igaus)
        enddo; enddo; enddo; enddo
     enddo

     if (debuggingMode) then
        write(*,*) ''
        write(*,*) 'gpdds = '
        do idime=1,ndime
           write(*,*)''
           do kdime=1,ndime
             write(*,'(x,3(x,3(x,e10.3)))')((gpdds(idime,jdime,kdime,ldime,1),ldime=1,ndime),jdime=1,ndime)
           enddo
        enddo
        write(*,*) ''
        write(*,*) 'gptmo = '
        do idime=1,ndime
           write(*,*)''
           do kdime=1,ndime
             write(*,'(x,3(x,3(x,e10.3)))')((gptmo(idime,jdime,kdime,ldime,1),ldime=1,ndime),jdime=1,ndime)
           enddo
        enddo
     endif

  endif


end subroutine sld_builtin_materials
