subroutine sld_begste()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_begste
  ! NAME 
  !    sld_begste
  ! DESCRIPTION
  !    This routine prepares for a new time step of Solidz
  !    equation      
  ! USES
  !    sld_iniunk
  !    sld_updtss
  !    sld_updbcs
  !    sld_updcra
  !    sld_updunk
  !    sld_radvuf
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use mod_cutele
  implicit none
  integer(ip), save :: ipass=0
  logical(lg)       :: debugging
  
  debugging = .false.
  !
  ! Initial guess fo the displacement: d(n,0,*) <-- d(n-1,*,*).
  !
  call sld_updunk(1_ip)
  !
  ! Update boundary conditions
  !
  call sld_updbcs(one)
  !
  ! Update crack path propagation
  !
  call sld_updcra()
  !
  ! Enrichement: define enriched nodes
  !
  call sld_enrich(one)
  !
  ! Compute effecteife traction for cohesive elements
  !
  call sld_uptcoh()

end subroutine sld_begste

