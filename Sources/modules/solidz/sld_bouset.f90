subroutine sld_bouset(ibsec,ibset)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_bouset
  ! NAME 
  !    sld_bouset
  ! DESCRIPTION
  !    This routine computes variables on a boundary set W.
  !    The variable are: 
  !     0       SETSU: surface            =  int_W  = meas(W)      
  !     6 -> 8  SETFP: Force              =  int_W  [-pI].n                 
  !
  !    Values of SETMP and SETYP are averaged further on in sld_outset
  !
  !    GPGDI ... Deformation tensor ...................... F = grad(u) + I
  !    GPCAU ... Right Cauchy-Green deformation tensor ... C = F^t x F
  !    GPGDH ... Displacement gradient ....................H = grad(u) 
  !    GPRAT ... Rate of Deformation tensor .............. Fdot = grad(phidot)
  !    GPSTR ... 2nd P-K Stress tensor ........................... S
  !    vgpsr ... 2nd P-K Stress tensor voigt vector...............{S}
  !    GPPIO ... 1st Piola-Kirchhoff stress tensor ....... P = F.S
  !    GPENE ... Stored energy function .................. W
  !    GPGRE ... Green strain tensor .............................E
  !    vgpgr ... Green strain voigt vector........................{E}
  ! USES 
  !    bouder
  !    chenor
  ! USED BY
  !    sld_outset
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  implicit none

  integer(ip), intent(in)  :: ibsec,ibset
  real(rp),    pointer     :: setsu(:),setfo(:),setnf(:), setfr(:)
  integer(ip)              :: ielem,inode,ipoin,igaus,idime,kdime,nn,kfl_force
  integer(ip)              :: pnode,pgaus,iboun,igaub,inodb,dummi
  integer(ip)              :: pelty,pblty,pnodb,pgaub,pmate,jdime
  integer(ip)              :: izone,kpoin,idofn
  real(rp)                 :: baloc(ndime,ndime),bopre(mnodb)
  real(rp)                 :: bovel(ndime,mnodb),eldis(ndime,mnode,ncomp_sld)
  real(rp)                 :: bocod(ndime,mnodb),elcod(ndime,mnode)
  real(rp)                 :: cartb(ndime,mnode),gpcar(ndime,mnode,mgaus)
  real(rp)                 :: xjaci(ndime,ndime),xjacm(ndime,ndime) 
  real(rp)                 :: shapp(mnode),gbcoo(3),grave(ndime,ndime)
  real(rp)                 :: gpgdi(ndime,ndime,mgaus)
  real(rp)                 :: gpigd(ndime,ndime,mgaus),gpdet(mgaus),gbdet
  real(rp)                 :: gpcau(ndime,ndime,mgaus)              ! Right Cauchy-Green deformation tensor ... C = F^t x F
  real(rp)                 :: gpene(mgaus)                          ! Energy
  real(rp)                 :: gpstr(ndime,ndime,mgaus)              ! Stress tensor S
  real(rp)                 :: gppio(ndime,ndime,mgaus)              ! Piola  tensor P
  real(rp)                 :: gprat(ndime,ndime,mgaus)              ! Rate of deformation Fdot at time n
  real(rp)                 :: gpgdh(ndime,ndime,mgaus)              ! Displacement gradient tensor H
  real(rp)                 :: gbsur,eucta
  real(rp)                 :: dummr(3),tragl(9),tracf(3),tract(3),hleng(3),tmatr,bidon
  real(rp)                 :: frxid(ndime,npoin), elfrx(ndime,mnode), gbfrx(ndime,mgaus), bofrx(ndime,mnodb)

  !----------------------------------------------------------------------
  !
  ! Initialization
  !
  !----------------------------------------------------------------------


  nn    =  postp(1) % nvabs + 1
  setsu => vbset( nn:nn , ibset ) ! Surface   
  setfo => vbset(  1: 3 , ibset ) ! Force    
  setnf => vbset(  4:   , ibset ) ! Normal Force    
  setfr => vbset(  5: 7 , ibset ) ! Reaction Forces
  setsu =  0.0_rp
  setfo =  0.0_rp
  setnf =  0.0_rp
  setfr =  0.0_rp
  tracf =  0.0_rp
  bofrx =  0.0_rp
 
  !

  !  write (6,*) nboun
  !  stop

  !
  ! Transform frxid as a vector frxid(idime,ipoin)
  !
  izone = lzone(ID_SOLIDZ)
  idofn = 0.0_ip
  do kpoin = 1,npoiz(izone)
     ipoin = lpoiz(izone)%l(kpoin)
     do idime=1,ndime
        idofn = idofn+1
        frxid(idime,ipoin) = -frxid_sld(idofn)
     end do
  end do

  boundaries: do iboun = 1,nboun

     if( lbset(iboun) == ibsec ) then

        !----------------------------------------------------------------
        !
        ! Element properties and dimensions and gather
        !
        !----------------------------------------------------------------

        pblty = ltypb(iboun) 
        pnodb = nnode(pblty)
        pgaub = ngaus(pblty)
        pmate = 1

        do inodb = 1,pnodb
           ipoin = lnodb(inodb,iboun)
           do idime = 1,ndime
              bocod(idime,inodb  ) = coord(idime,ipoin)
              bofrx(idime,inodb  ) = frxid(idime,ipoin)
           end do
        end do

        ielem = lboel(pnodb+1,iboun)
        pelty = ltype(ielem)

        if( pelty > 0 ) then

           pnode = nnode(pelty)
           pgaus = ngaus(pelty)
           do inode = 1,pnode
              ipoin = lnods(inode,ielem)
              do idime = 1,ndime
                 elcod(idime,inode) = coord(idime,ipoin)
                 eldis(idime,inode,1) = displ(idime,ipoin,1)
                 elfrx(idime,inode) = frxid(idime,ipoin)
              end do
           end do
           !
           ! Element length HLENG
           !
           call elmlen(&
                ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&
                hnatu(pelty),hleng)

           do igaub = 1,pgaub
              do jdime = 1,ndime
                 do idime = 1,ndime
                    gppio(idime,jdime,igaub) = 0.0_rp
                 end do
              end do
           end do

           !----------------------------------------------------------------
           !
           ! Loop over Gauss points: GBFRX
           !
           !----------------------------------------------------------------

           do igaub = 1,pgaub

              do idime = 1,ndime
                 gbfrx(idime,igaub) = 0.0_rp
              end do

              !call bouder(&
              !     pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&    ! Cartesian derivative
              !     bocod,baloc,eucta)                                   ! and Jacobian
              !gbsur = elmar(pblty)%weigp(igaub)*eucta 
              !setsu = setsu + gbsur
              !call chenor(pnode,baloc,bocod,elcod)                      ! Check normal

              do inodb = 1,pnodb
                 do idime = 1,ndime
                    gbfrx(idime,igaub) = gbfrx(idime,igaub) & 
                         + elmar(pblty)%shape(inodb,igaub) * bofrx(idime,inodb) 
                 end do
              end do
            end do

            gauss_points: do igaub = 1,pgaub

              call bouder(&
                   pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&    ! Cartesian derivative
                   bocod,baloc,eucta)                                   ! and Jacobian
              gbsur = elmar(pblty)%weigp(igaub)*eucta
              setsu = setsu + gbsur
              call chenor(pnode,baloc,bocod,elcod)                      ! Check normal


              !-------------------------------------------------------------
              !
              ! Compute traction
              ! t = P . N  (1st piola . normal in the reference system)
              !
              !-------------------------------------------------------------

              if( postp(1) % npp_setsb(1) /= 0 .or. postp(1) % npp_setsb(4) /= 0 ) then
                 !
                 ! Extrapolate from Gauss points igauss to node inode and 
                 ! then interpolation at boundary Gauss points igaub
                 !         
                 do igaus = 1,pgaus
                    do inodb = 1,pnodb                 
                       tmatr =  elmar(pelty)%shaga(igaus,lboel(inodb,iboun))&
                            & * elmar(pblty)%shape(inodb,igaub)
                       do jdime = 1,ndime
                          do idime = 1,ndime
                             gppio(idime,jdime,igaub) = gppio(idime,jdime,igaub) &
                                  + gppio_sld(ielem) % a(idime,jdime,igaus)*tmatr
                          end do
                       end do
                       !do idime = 1,ndime
                       !   ipoin = lnodb(inodb,iboun)
                       !   gbfrx(idime,igaub) = gbfrx(idime,igaub) + elmar(pblty)%shape(inodb,igaub) &
                       !                     & *bofrx(idime,inodb)
                       !end do
                    end do
                 end do

                 do idime = 1,ndime
                    tract(idime)= 0.0_rp
                    do jdime = 1,ndime
                       tract(idime) = tract(idime) &
                            + gppio(jdime,idime,igaub) * baloc(jdime,ndime)
                    end do
                 end do

                 if( postp(1) % npp_setsb(1) /= 0  ) then
                 ! OPTION 1 : (x y z) component of the force
                   setfo(1) = setfo(1) + gbsur * tract(1)
                   setfo(2) = setfo(2) + gbsur * tract(2)
                   setfo(3) = setfo(3) + gbsur * tract(3)
                end if
                if( postp(1) % npp_setsb(4) /= 0  ) then
                   dummr(1) = 0.0_rp
                   do idime = 1,ndime
                     dummr(1) = dummr(1) + tract(idime) * baloc(idime,ndime)
                   end do
                   setnf(1) = setnf(1) + dummr(1) * gbsur
                end if
                if( postp(1) % npp_setsb(5) /= 0  ) then
                   setfr(1) = setfr(1) + gbsur * gbfrx(1,igaub)
                   setfr(2) = setfr(2) + gbsur * gbfrx(2,igaub)
                   setfr(3) = setfr(3) + gbsur * gbfrx(3,igaub)
                end if                
              end if

           end do gauss_points

        end if

     end if

  end do boundaries


end subroutine sld_bouset
