  !----------------------------------------------------------------------
  !> @addtogroup Solidz
  !> @{
  !> @file    sld_fsiexch.f90
  !> @author  J.C. Cajas
  !> @date    17/04/2014
  !> @brief   Receive force from Nastin and send displacement to Alefor
  !> @details Receive force from Nastin and send displacement to Alefor
  !> @        usind the coupling structures and functions.
  !> @} 
  !----------------------------------------------------------------------

subroutine sld_fsiexch(itask,icoup)
  use def_coupli,        only :  coupling_type
  use def_domain,        only :  ndime
  use def_domain,        only :  npoin
  use def_domain,        only :  coord
  use def_kintyp,        only :  ip,rp
  use def_master,        only :  current_code
  use def_master,        only :  displ
  use def_master,        only :  INOTMASTER
  use mod_couplings,     only :  COU_INTERPOLATE_NODAL_VALUES
  use def_solidz,        only :  gdepo_sld, forc_fsi
  use def_master,        only :  rhsid

  implicit none
  integer(ip), intent(in)     :: itask, icoup
  integer(ip)                 :: idofn
  integer(ip)                 :: ipoin
  integer(ip)                 :: idime
  integer(ip)                 :: jdime

  real(rp),    pointer        :: xvalu(:,:)
  real(rp),    pointer        :: svalu(:,:)
  real(rp),    pointer        :: force(:,:)
  real(rp)                    :: foref

  nullify(xvalu) 
  nullify(svalu)
  nullify(force)

  if ( INOTMASTER ) then

     allocate(xvalu(ndime,npoin))
     allocate(svalu(ndime,npoin))
     allocate(force(ndime,npoin))

  else

     allocate(xvalu(1_ip,1_ip))
     allocate(svalu(1_ip,1_ip))
     allocate(force(1_ip,1_ip))

  end if

  if ( itask == 1 .and. icoup == 2_ip ) then ! Send displacement to NASTIN 

     ! Arguments for the coupling function: 
     ! COU_INTERPOLATE_NODAL_VALUES(coupling label, number of dimensions of the variable to interpolate, 
     ! array to store the results, variable to interpolate )
     ! The setup of the communicators, coordinates and everything else is done in COU_INITIALIZE_COUPLING

     ! displacement
     if ( INOTMASTER ) then

        svalu = 0_rp
        do ipoin = 1, npoin

           do idime= 1, ndime

              svalu(idime,ipoin) = displ(idime,ipoin,1) - displ(idime,ipoin,3) !

           end do

        end do

     end if

     call COU_INTERPOLATE_NODAL_VALUES(2_ip,ndime,xvalu,svalu)

  else if ( itask == 2 .and. icoup == 1_ip  ) then ! Receive force from nastin

     xvalu    = 0_rp
     call COU_INTERPOLATE_NODAL_VALUES(1_ip,ndime,xvalu,force)
     if (INOTMASTER ) then

        forc_fsi = 0_rp
        if( associated(xvalu) ) then

           do ipoin = 1, npoin
              
              do idime = 1,ndime

                 forc_fsi(idime,ipoin) = xvalu(idime,ipoin)

              end do

           end do

        end if

     end if

  end if

  if( associated(xvalu) ) deallocate( xvalu )
  if( associated(svalu) ) deallocate( svalu )
  if( associated(force) ) deallocate( force )

end subroutine sld_fsiexch
!> @} 
!-----------------------------------------------------------------------
