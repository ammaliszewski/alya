subroutine sld_membcs(itask)
!-----------------------------------------------------------------------
!****f* Solidz/sld_membcs
! NAME 
!    sld_memcbs
! DESCRIPTION
!    This routine allocates memory for the boundary conditions arrays
! USES
!    ecoute
! USED BY
!    sld_reabcs
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use mod_memchk
  use def_solidz
  implicit none
  integer(ip), intent(in) :: itask
  integer(4)              :: istat
  integer(ip)             :: ifunc,dummi

  if (itask == 1_ip) then

     allocate(kfl_fixno_sld(ndime,npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FIXNO_SLD','sld_reabcs',kfl_fixno_sld)
     allocate(kfl_fixbo_sld(nboun),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FIXBO_SLD','sld_reabcs',kfl_fixbo_sld)
     allocate(kfl_fixrs_sld(nbopo),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FIXRS_SLD','sld_membcs',kfl_fixrs_sld)

  else if (itask == 2_ip) then
     !
     ! BVESS_SLD, BVNAT_SLD, and KFL_FUNNO_SLD, KFL_FUNBO_SLD
     !
     allocate(bvess_sld(ndime,npoin,2),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'BVESS_SLD',    'sld_reabcs',bvess_sld)
     allocate(bvnat_sld(ndime,nboun,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'BVNAT_SLD',    'sld_reabcs',bvnat_sld)
     if(kfl_conbc_sld==0) then     ! non-constant bc
        allocate(kfl_funno_sld(npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FUNNO_SLD','sld_reabcs',kfl_funno_sld)
        allocate(kfl_funbo_sld(nboun),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FUNBO_SLD','sld_reabcs',kfl_funbo_sld)
     end if

  else if (itask == 3_ip) then

     allocate(tload_sld(10),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'TLOAD_SLD','sld_reabcs',tload_sld)

  else if ((itask > 10_ip).and.(itask < 21_ip)) then
     ifunc = itask - 10
     allocate(tload_sld(ifunc)%a(ndime+1,mtloa_sld(ifunc)),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'TLOAD_SLD','sld_reabcs',tload_sld(ifunc)%a)

  else if( itask ==  30 ) then

     allocate(crkpo_sld(ndime,ncrak_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'CRKPO_SLD','sld_reabcs',crkpo_sld)
     allocate(crkno_sld(ndime,ncrak_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'CRKNO_SLD','sld_reabcs',crkno_sld)

  else if( itask == -30 ) then

     call memchk(two,istat,mem_modul(1:2,modul),'CRKPO_SLD','sld_membcs',crkpo_sld)
     deallocate(crkpo_sld,stat=istat)
     if( istat /= 0 ) call memerr(two,'CRKPO_SLD','sld_membcs',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'CRKNO_SLD','sld_membcs',crkno_sld)
     deallocate(crkno_sld,stat=istat)
     if( istat /= 0 ) call memerr(two,'CRKNO_SLD','sld_membcs',0_ip)

  else if( itask ==  31 ) then

     if( ndime == 2 ) then
        dummi = 2
     else
        dummi = 4
     end if
     allocate(crkco_sld(ndime,dummi,ncrak_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'CRKCO_SLD','sld_reabcs',crkco_sld)

  else if( itask == -31 ) then

     call memchk(two,istat,mem_modul(1:2,modul),'CRKCO_SLD','sld_membcs',crkco_sld)
     deallocate(crkco_sld,stat=istat)
     if( istat /= 0 ) call memerr(two,'CRKCO_SLD','sld_membcs',0_ip)

  end if

end subroutine sld_membcs



