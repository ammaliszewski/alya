module mod_sld_stress_model_comput
  ! ==============================================================================
  ! INIT   
  !
  use def_parame, only                     :  &
      ip, rp
  ! -----------------------------------------------------------------------------
  implicit none 
  !
  !=============================================================| init |=========
  !==============================================================================
  ! PUBLIC
  !
  public                                   :: &
      SM_strain_tensor,                       & !
      SM_stress_tensor,                       & !
      SM_tensor_to_voigt_second,              & !
      SM_tensor_to_voigt_fourth,              & !
      SM_rotate_basis_creation,               & !
      SM_rotate_tensor_second,                & !
      SM_rotate_voigt_second,                 & !
      SM_rotate_voigt_fourth    
   !
  !=============================================================| public |=======
  !==============================================================================
  ! PRIVATE
  !
  private                                 :: &
      priv_transformation_matrix,            &
      priv_polar_decomposition
  !
  !=============================================================| private |======
  !==============================================================================
  ! CONTAINS
  !
  contains

  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine SM_strain_tensor(task, gpgdi, gpgre)
  
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         task 
    real(rp),       intent(in)              :: &
         gpgdi(3,3)                              !
    real(rp),       intent(out)             :: &
         gpgre(3,3)                              !
    ! ---------------------------------------------------------------------------
    integer(ip)                             :: &
         i, j, k                                 !
    real(rp)                                :: &
         auxS1, tkron(3,3)                    
    ! ---------------------------------------------------------------------------
    !
    tkron = 0.0_rp
    do i = 1, 3
      tkron(i, i) = 1.0_rp
    enddo
    !
    ! ---------------------------------------------------------------------------
    select case (task)
      !
      case(0_ip)
        !
        ! Infinitesimal strain tensor
        !
        gpgre = 0.0_rp
        do i = 1, 3
          do j = 1, 3
            gpgre(i,j) = gpgre(i,j) + 0.5_rp*(gpgdi(i,j) + gpgdi(j,i)) - tkron(i,j)
          enddo
        enddo
      !
      case(1_ip)
        !
        ! Green-Lagrange strain tensor
        !
        gpgre = 0.0_rp
        do i = 1, 3
          do j = 1, 3
            auxS1 = 0.0_rp
            do k = 1, 3
              auxS1 = auxS1 + gpgdi(k,i)*gpgdi(k,j)
            enddo
            gpgre(i,j) = 0.5_rp*(auxS1 - tkron(i,j))
          enddo
        enddo
      !
      case default
        return
    end select
    !
    return  

  end subroutine SM_strain_tensor

  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine SM_stress_tensor(task, strain, stiff, stress)

    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         task 
    real(rp),       intent(in)              :: &
         strain(6), stiff(6,6)                   !
    real(rp),       intent(out)             :: &
         stress(6)                               !
    ! ---------------------------------------------------------------------------
    integer(iP)                             :: &
         i, j                                    !               
    !
    ! ---------------------------------------------------------------------------
    !
    select case (task)
      !
      case(0_ip)
        !
        ! 2nd Piola-Kirchhoff stress tensor
        !   {S}_i = [C]_ij*{E}_j   / (1)-(1,1), (2)-(2,2), (3)-(3,3)
        !                            (4)-(2,3), (5)-(1,3), (6)-(1,2)
        !
        stress = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            stress(i) = stress(i) + stiff(i,j)*strain(j)
          enddo
        enddo
      !
      case default
        return
    end select
    !
    return  

  end subroutine SM_stress_tensor

  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine SM_tensor_to_voigt_second(task, tensor, voigt)

    use def_solidz, only                    :  &
        nvgij_inv_sld
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         task 
    real(rp),       intent(inout)           :: &
         tensor(:,:) , voigt(6)
    !
    integer(ip)                             :: &
         i, j, ivoig
    !
    ! ---------------------------------------------------------------------------
    select case (task)  
      !
      case(0_ip)
        !
        ! Tensor to voigt
        !
        voigt = 0.0_rp
        do i = 1, 3
          do j = 1, 3
            ivoig = nvgij_inv_sld(i,j)
            voigt(ivoig) = voigt(ivoig) + tensor(i,j)
          enddo
        enddo
      !
      case(1_ip)
        !
        ! Voigt to tensor
        !
        tensor = 0.0_rp
        do i = 1, 3
          do j = 1, 3
            ivoig = nvgij_inv_sld(i, j)
            tensor(i,j) = voigt(ivoig)
          enddo
        enddo
      !
      case default
        return
    end select

    return

  end subroutine SM_tensor_to_voigt_second
  
  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine SM_tensor_to_voigt_fourth(task, tensor, voigt)

    use def_solidz, only                    :  &
        nvgij_inv_sld
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         task 
    real(rp),       intent(inout)           :: &
         tensor(:,:,:,:), voigt(6,6)
    !
    integer(ip)                             :: &
         i, j, k, l, p, q, tkron(3,3)
    !
    ! ---------------------------------------------------------------------------
    !
    tkron = 0_ip
    do i = 1, 3
      tkron(i, i) = 1_ip
    enddo
    !
    ! ---------------------------------------------------------------------------
    select case (task)  
      !
      case(0_ip)
      !
      case(1_ip)
        !
        ! From Voigt notation to tensorial notation
        !    The mapping ij -> a and kl -> b is: 
        !        a = i*delta_{ij} + (1 - delta_{ij})*(9 - i - j)
        !        b = k*delta_{kl} + (1 - delta_{kl})*(9 - k - l)
        tensor = 0.0_rp
        do l = 1, 3
          do k = 1, 3
            do j = 1, 3
              do i = 1, 3
                p = i*tkron(i, j) + (1_ip - tkron(i, j))*(9_ip - i - j)
                q = k*tkron(k, l) + (1_ip - tkron(k, l))*(9_ip - k - l)
                tensor(i, j, k, l) = voigt(p, q)
              enddo
            enddo
          enddo
        enddo
      !
      case default
        return
    end select

    return

  end subroutine SM_tensor_to_voigt_fourth

  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine SM_rotate_basis_creation(ielem, gpgdi, rotmat)

    use def_solidz, only                    :  &
        kfl_fiber_sld, fibeb_sld,              &
        fibes_sld, fiben_sld
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         ielem
    real(rp),       intent(in)              :: &
         gpgdi(3,3)                              ! Deformation gradient
    real(rp),       intent(out)             :: &
         rotmat(3,3)
    !
    integer(ip)                             :: &
         i, j, k
    real(rp)                                :: &
         rotrig(3,3),                          & ! Solid rigid rotation matrix
         rotmcs(3,3),                          & ! Material coordinate system rotation matrix
         auxM1(3,3)                              ! Auxiliar
    !
    ! ---------------------------------------------------------------------------
    !
    ! Compute solid rigid rotation matrix
    !
    !rotrig = 0.0_rp
    !auxM1  = 0.0_rp
    !call priv_polar_decomposition(gpgdi(:,:), rotrig(:,:), auxM1(:,:))
    rotrig = 0.0_rp
    do i = 1, 3
      rotrig(i,i) = 1.0_rp
    enddo
    !
    ! Read material coordinate system rotation matrix from fields
    !
    rotmcs = 0.0_rp
    if ( kfl_fiber_sld == 5_ip ) then
      do i = 1, 3
        rotmcs(1,i) = real(fibeb_sld(i,ielem),rp)
        rotmcs(2,i) = real(fibes_sld(i,ielem),rp)
        rotmcs(3,i) = real(fiben_sld(i,ielem),rp)
      enddo
    else
      do i = 1, 3
        rotmcs(i,i) = 1.0_rp
      enddo
    end if
    !
    ! Compute rotation matrix
    !
    rotmat = 0.0_rp
    do i = 1, 3 
      do j = 1, 3
        do k = 1, 3
          rotmat(i,j) = rotmat(i,j) + rotmcs(i,k)*rotrig(k,j)
         enddo
      enddo
    enddo 

   return

  end subroutine SM_rotate_basis_creation

  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine SM_rotate_tensor_second(task, rotmat, tenGCS, tenMCS)

    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         task 
    real(rp),       intent(in)              :: &
         rotmat(3,3)
    real(rp),       intent(inout)           :: &
         tenGCS(3,3), tenMCS(3,3)
    !
    integer(ip)                             :: &
         i, j, p, q
    !
    ! ---------------------------------------------------------------------------
    !
    select case (task)  
      !
      case(0_ip)
        !
        ! From global to local coordinate system
        !     Alcs = Q Agcs Q^{T}
        ! 
        tenMCS = 0.0_rp
        do i = 1, 3
          do j = 1, 3
            do p = 1, 3
              do q = 1, 3
                tenMCS(i,j) = tenMCS(i, j) + rotmat(i, p)*rotmat(j, q)*tenGCS(p, q)
              end do
            end do
          end do
        end do
      !  
      case(1_ip)
        !
        ! From local to global coordinate system
        !     Agcs = Q^{T} Alcs Q
        tenGCS = 0.0_rp
        do i = 1, 3
          do j = 1, 3
            do p = 1, 3
               do q = 1, 3
                  tenGCS(i,j) = tenGCS(i, j) + rotmat(i, p)*rotmat(j, q)*tenMCS(p, q)
               end do
            end do
          end do
        end do
      !  
      case default
        return
    end select
 
    return
 
  end subroutine SM_rotate_tensor_second

  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
   !> @author  Adria Quintanas (adria.quintanas@udg.edu)
   !> @date    November, 2015
   !> @brief   This subroutine performs a transformation of a tensor in the Voigt
   !> @        from a old basis to a new basis. 
   !> @details This subroutine performs a transformation of a tensor
   !> @           Strain from Local CSYS to Global CSYS:
   !> @              E _{i} = T _{ij}^T*E'_{j}
   !> @           Strain from Global CSYS to Local CSYS:
   !> @              E'_{i} = Tg_{ij}  *E _{j}
   !> @           Stress  from Local CSYS to Global CSYS:
   !> @              S _{i} = Tg_{ij}^T*S'_{j}
   !> @           Stress  from Global CSYS to Local CSYS:
   !> @              S'_{i} = T _{ij}  *S _{j}
   !> @
   !> @        Reference: E.J. Barbero - Finite Element Analysis of Composite 
   !> @           Materials
   !> @} 
  !------------------------------------------------------------------------------
  subroutine SM_rotate_voigt_second(task, rotmat, voigtGCS, voigtLCS)

    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         task 
    real(rp),       intent(in)              :: &
         rotmat(3,3)
    real(rp),       intent(inout)           :: &
         voigtGCS(6), voigtLCS(6)
    !
    integer(ip)                             :: &
         i, j
    real(rp)                                :: &
         rotvoi(6,6)
    !
    ! ---------------------------------------------------------------------------
    !
    ! Different cases
    !
    select case (task)  
      !
      case(0_ip)
        !
        ! Strain from Local CSYS to Global CSYS:
        !    E _{i} = T _{ij}^T*E'_{j}
        !
        call priv_transformation_matrix(0_ip, rotmat, rotvoi)
        !
        voigtGCS = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            voigtGCS(i) = voigtGCS(i) +  rotvoi(j,i)*voigtLCS(j)
          end do
        end do
      !  
      case(1_ip)
        !
        ! Strain from Global CSYS to Local CSYS:
        !    E'_{i} = Tg_{ij}  *E _{j}
        !
        call priv_transformation_matrix(1_ip, rotmat, rotvoi)
        !
        voigtLCS = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            voigtLCS(i) = voigtLCS(i) +  rotvoi(i,j)*voigtGCS(j)
          end do
        end do
      !
      case(2_ip)
        !
        ! Stress  from Local CSYS to Global CSYS:
        !    S _{i} = Tg_{ij}^T*S'_{j}
        !
        call priv_transformation_matrix(1_ip, rotmat, rotvoi)
        !
        voigtGCS = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            voigtGCS(i) = voigtGCS(i) +  rotvoi(j,i)*voigtLCS(j)
          end do
        end do
      !
      case(3_ip)
        !
        ! Stress  from Global CSYS to Local CSYS:
        !    S'_{i} = T _{ij}  *S _{j}
        !
        call priv_transformation_matrix(0_ip, rotmat, rotvoi)
        !
        voigtLCS = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            voigtLCS(i) = voigtLCS(i) +  rotvoi(i,j)*voigtGCS(j)
          end do
        end do
      !  
      case default
        return
    end select
 
    return
 
  end subroutine SM_rotate_voigt_second  
  
  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
   !> @author  Adria Quintanas (adria.quintanas@udg.edu)
   !> @date    November, 2015
   !> @brief   This subroutine performs a transformation of a tensor in the Voigt
   !> @        from a old basis to a new basis. 
   !> @details This subroutine performs a transformation of a tensor
   !> @           Strain from Local CSYS to Global CSYS:
   !> @              E _{i} = T _{ij}^T*E'_{j}
   !> @           Strain from Global CSYS to Local CSYS:
   !> @              E'_{i} = Tg_{ij}  *E _{j}
   !> @           Stress  from Local CSYS to Global CSYS:
   !> @              S _{i} = Tg_{ij}^T*S'_{j}
   !> @           Stress  from Global CSYS to Local CSYS:
   !> @              S'_{i} = T _{ij}  *S _{j}
   !> @
   !> @        Reference: E.J. Barbero - Finite Element Analysis of Composite 
   !> @           Materials
   !> @} 
  !------------------------------------------------------------------------------
  subroutine SM_rotate_voigt_fourth(task, rotmat, voigtGCS, voigtLCS)

    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         task 
    real(rp),       intent(in)              :: &
         rotmat(3,3)
    real(rp),       intent(inout)           :: &
         voigtGCS(6,6), voigtLCS(6,6)
    !
    integer(ip)                             :: &
         i, j, p, q
    real(rp)                                :: &
         rotvoi(6,6)
    !
    ! ---------------------------------------------------------------------------
    !
    ! Different cases
    !
    select case (task)  
      !
      case(0_ip)
        !
        ! Compliance tensor from LCSYS to GCSYS
        !    H_ {ij} = T_{pi}*T_{qj}*H'_{pq}
        !
        call priv_transformation_matrix(0_ip, rotmat, rotvoi)
        !
        voigtGCS = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            do p = 1, 6
              do q = 1, 6
                voigtGCS(i,j) = voigtGCS(i,j) + rotvoi(p,i)*rotvoi(q,j)*voigtLCS(p,q)
              end do 
            end do
          end do
        end do
      !  
      case(1_ip)
        !
        ! Compliance tensor from GCSYS to LCSYS
        !    H'_{ij} = Tg_{ip}*H _{pq}*Tg_{jq}
        !
        call priv_transformation_matrix(1_ip, rotmat, rotvoi)
        !
        voigtLCS = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            do p = 1, 6
              do q = 1, 6
                voigtLCS(i,j) = voigtLCS(i,j) + rotvoi(i,p)*rotvoi(j,q)*voigtGCS(p,q)
              end do 
            end do
          end do
        end do
      !
      case(2_ip)
        !
        ! Stiffness tensor from LCSYS to GCSYS
        !    C _{ij} = Tg_{pi}*C'_{pq}*Tg_{qj}
        !
        call priv_transformation_matrix(1_ip, rotmat, rotvoi)
        !
        voigtGCS = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            do p = 1, 6
              do q = 1, 6
                voigtGCS(i,j) = voigtGCS(i,j) + rotvoi(p,i)*rotvoi(q,j)*voigtLCS(p,q)
              end do 
            end do
          end do
        end do
      !
      case(3_ip)
        !
        ! Stiffness tensor from GCSYS to LCSYS
        !    C'_ {ij} = T _{ip}*T _{jq}*C_{pq}
        !
        call priv_transformation_matrix(0_ip, rotmat, rotvoi)
        !
        voigtLCS = 0.0_rp
        do i = 1, 6
          do j = 1, 6
            do p = 1, 6
              do q = 1, 6
                voigtLCS(i,j) = voigtLCS(i,j) + rotvoi(p,i)*rotvoi(j,q)*voigtGCS(p,q)
              end do 
            end do
          end do
        end do
      !  
      case default
        return
    end select
 
    return
 
  end subroutine SM_rotate_voigt_fourth
  
  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author
  !> @date    
  !> @brief   
  !> @details 
  !> @} 
  !------------------------------------------------------------------------------
  subroutine priv_transformation_matrix(task, rotmat, rotvoi)

    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    integer(ip),    intent(in)              :: &
         task
    real(rp),       intent(in)              :: &
         rotmat(3,3)
    real(rp),       intent(out)             :: &
         rotvoi(6,6)
    !
    ! ---------------------------------------------------------------------------
    !
    rotvoi = 0.0_rp
    !
    ! TRANSFORMATION MATRIX
    !    [T] and [Tg] are almost equal, only change some positions, then, the common part
    !    is defined here.

    rotVoi(1,1) = rotmat(1,1)*rotmat(1,1)
    rotVoi(1,2) = rotmat(1,2)*rotmat(1,2)
    rotVoi(1,3) = rotmat(1,3)*rotmat(1,3)
    rotVoi(1,4) = rotmat(1,2)*rotmat(1,3)
    rotVoi(1,5) = rotmat(1,1)*rotmat(1,3)
    rotVoi(1,6) = rotmat(1,1)*rotmat(1,2)
    rotVoi(2,1) = rotmat(2,1)*rotmat(2,1)
    rotVoi(2,2) = rotmat(2,2)*rotmat(2,2)
    rotVoi(2,3) = rotmat(2,3)*rotmat(2,3)
    rotVoi(2,4) = rotmat(2,2)*rotmat(2,3)
    rotVoi(2,5) = rotmat(2,1)*rotmat(2,3)
    rotVoi(2,6) = rotmat(2,1)*rotmat(2,2)
    rotVoi(3,1) = rotmat(3,1)*rotmat(3,1)
    rotVoi(3,2) = rotmat(3,2)*rotmat(3,2)
    rotVoi(3,3) = rotmat(3,3)*rotmat(3,3)
    rotVoi(3,4) = rotmat(3,2)*rotmat(3,3)
    rotVoi(3,5) = rotmat(3,1)*rotmat(3,3)
    rotVoi(3,6) = rotmat(3,1)*rotmat(3,2)
    rotVoi(4,1) = rotmat(2,1)*rotmat(3,1)
    rotVoi(4,2) = rotmat(2,2)*rotmat(3,2) 
    rotVoi(4,3) = rotmat(2,3)*rotmat(3,3)
    rotVoi(4,4) = rotmat(2,2)*rotmat(3,3) + rotmat(2,3)*rotmat(3,2)
    rotVoi(4,5) = rotmat(2,1)*rotmat(3,3) + rotmat(2,3)*rotmat(3,1)
    rotVoi(4,6) = rotmat(2,1)*rotmat(3,2) + rotmat(2,2)*rotmat(3,1)
    rotVoi(5,1) = rotmat(1,1)*rotmat(3,1)
    rotVoi(5,2) = rotmat(1,2)*rotmat(3,2)
    rotVoi(5,3) = rotmat(1,3)*rotmat(3,3)
    rotVoi(5,4) = rotmat(1,2)*rotmat(3,3) + rotmat(1,3)*rotmat(3,2)
    rotVoi(5,5) = rotmat(1,1)*rotmat(3,3) + rotmat(1,3)*rotmat(3,1)
    rotVoi(5,6) = rotmat(1,1)*rotmat(3,2) + rotmat(1,2)*rotmat(3,1)
    rotVoi(6,1) = rotmat(1,1)*rotmat(2,1)
    rotVoi(6,2) = rotmat(1,2)*rotmat(2,2)
    rotVoi(6,3) = rotmat(1,3)*rotmat(2,3)
    rotVoi(6,4) = rotmat(1,2)*rotmat(2,3) + rotmat(1,3)*rotmat(2,2)
    rotVoi(6,5) = rotmat(1,1)*rotmat(2,3) + rotmat(1,3)*rotmat(2,1)
    rotVoi(6,6) = rotmat(1,1)*rotmat(2,2) + rotmat(1,2)*rotmat(2,1)
    ! 
    ! T or Tg
    !
    select case (task)  
      !
      case(0_ip)
        !
        ! Transformation matrix [T]
        !
        rotVoi(1,4) = rotVoi(1,4)*2.0_rp
        rotVoi(1,5) = rotVoi(1,5)*2.0_rp
        rotVoi(1,6) = rotVoi(1,6)*2.0_rp
        rotVoi(2,4) = rotVoi(2,4)*2.0_rp
        rotVoi(2,5) = rotVoi(2,5)*2.0_rp
        rotVoi(2,6) = rotVoi(2,6)*2.0_rp
        rotVoi(3,4) = rotVoi(3,4)*2.0_rp
        rotVoi(3,5) = rotVoi(3,5)*2.0_rp
        rotVoi(3,6) = rotVoi(3,6)*2.0_rp
      !  
      case(1_ip)
        !
        ! Transformation matrix [Tg]
        !
        rotVoi(4,1) = rotVoi(4,1)*2.0_rp
        rotVoi(4,2) = rotVoi(4,2)*2.0_rp
        rotVoi(4,3) = rotVoi(4,3)*2.0_rp
        rotVoi(5,1) = rotVoi(5,1)*2.0_rp
        rotVoi(5,2) = rotVoi(5,2)*2.0_rp
        rotVoi(5,3) = rotVoi(5,3)*2.0_rp
        rotVoi(6,1) = rotVoi(6,1)*2.0_rp
        rotVoi(6,2) = rotVoi(6,2)*2.0_rp
        rotVoi(6,3) = rotVoi(6,3)*2.0_rp
      !  
      case default
        return
    end select
 
    return

  end subroutine priv_transformation_matrix
  
  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author
  !> @date    
  !> @brief   
  !> @details This subroutine performs the right polar decomposition F=R*U, of the
  !           deformation gradient F (3x3 matrix) into a rotation tensor, R, and 
  !           the right stretch tensor, U. Requires subroutine "invmtx".
  !> @} 
  !------------------------------------------------------------------------------
  subroutine priv_polar_decomposition(F, R, U)

    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    real(rp),       intent(in)              :: &
         F(3,3)
    real(rp),       intent(out)             :: &
         R(3,3), U(3,3)
    !
    integer(ip)                             :: &
        i, j, k, p, q, NROT
    real(rp)                                :: &
        detU, C(3,3), omega(3),                &
        eigvec(3,3), Uinv(3,3), temp(3,3)
    !
    ! ---------------------------------------------------------------------------
    !
    ! Create identity matrix
    !
    R = 0.0_rp
    U = 0.0_rp
    Uinv = 0.0_rp
    temp = 0.0_rp
    !
    ! Calculate right cauchy-green strain tensor, C = F^T * F
    !
    C = 0.0_rp
    do i = 1,3
      do j = 1,3
         do k = 1,3
           C(i,j) = C(i,j) + F(k,i)*F(k,j)
         enddo
      enddo
    enddo
    !
    ! Compute eigenvalues and eigenvectors
    !
    call spcdec(C, omega(:), eigvec(:,:), NROT, 1_ip)
    !  
    ! Calculate the principal values of U
    !
    do i=1,3
       U(i,i) = sqrt(omega(i))
    end do
    !  
    ! Calculate complete tensor U = eigvec*U*eigvec^T
    !
    temp = U
    U = 0.0_rp
    do i = 1, 3
      do j = 1, 3
        do p = 1, 3
          do q = 1, 3
            U(i,j) = U(i, j) + eigvec(i, p)*eigvec(j, q)*temp(p, q)
          end do
        end do
      end do
    end do
    !
    ! calculate inverse of U
    !
    call invmtx(U, Uinv, detU, 3_ip)
    !  
    ! calculate R = F*U^{-1}
    !
    do i = 1,3
      do j = 1,3
        do k = 1,3
           R(i,j) = R(i,j) + F(i,k)*Uinv(k,j)
        end do
      end do
    end do 

    return

  end subroutine priv_polar_decomposition
 
 !=============================================================| contains |=====
 
end module mod_sld_stress_model_comput
