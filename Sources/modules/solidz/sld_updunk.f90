!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_updunk.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Perform displacement updates
!> @details Perform displacement updates
!> @} 
!-----------------------------------------------------------------------
subroutine sld_updunk(itask)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_updunk
  ! NAME
  !    sld_updunk
  ! DESCRIPTION
  !    This routine performs several types of updates for the displacement
  ! USED BY
  !    sld_begste (itask=1)
  !    sld_begite (itask=2)
  !    sld_endite (itask=3, inner loop)
  !    sld_endite (itask=4, outer loop)
  !    sld_endste (itask=5)
  !    sld_endste (itask=6)               Updates Pressure
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,kpoin,itotv,idime,icomp,itime,ielem,igaus,izone
  integer(ip)             :: pelty,pgaus

  real(rp)                :: relax_old, relax_new

  if( INOTMASTER ) then

     select case (itask)

     case(1)
        !
        ! Assign u(n,0,*) <-- u(n-1,*,*), initial guess for outer iterations
        !
        icomp=min(3,ncomp_sld)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           do idime=1,ndime
              displ(idime,ipoin,ITER_AUX) = displ(idime,ipoin,icomp)
              veloc_sld(idime,ipoin,ITER_AUX) = veloc_sld(idime,ipoin,icomp)
              accel_sld(idime,ipoin,ITER_AUX) = accel_sld(idime,ipoin,icomp)
           end do
        end do

!            write(*,*)'case 1: dxfem_sld =',dxfem_sld(:,:,ITER_AUX)

        if (kfl_xfeme_sld == 1) then
           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
              do idime=1,ndime
                 dxfem_sld(idime,ipoin,ITER_AUX) = dxfem_sld(idime,ipoin,icomp)
                 vxfem_sld(idime,ipoin,ITER_AUX) = vxfem_sld(idime,ipoin,icomp)
                 axfem_sld(idime,ipoin,ITER_AUX) = axfem_sld(idime,ipoin,icomp)
                 !            write(*,*)'case 1: dxfem_sld =',dxfem_sld(:,:,ITER_AUX)
                 !            write(*,*)'case 1: displ =',displ(:,:,ITER_AUX)
              end do
           end do
        end if

        !if (kfl_arcle_sld == 1) then
        !   lambda_sld(ITER_AUX) = lambda_sld(icomp)
        !end if

     case(2)
        !
        ! Assign u(n,i,0) <-- u(n,i-1,*), initial guess for inner iterations
        !
!       izone = lzone(ID_SOLIDZ)
!       do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
!           do idime=1,ndime
!              displ(idime,ipoin,ITER_K) = displ(idime,ipoin,ITER_AUX)
!              veloc_sld(idime,ipoin,ITER_K) = veloc_sld(idime,ipoin,ITER_AUX)
!              accel_sld(idime,ipoin,ITER_K) = accel_sld(idime,ipoin,ITER_AUX)
!           end do
!        end do
        displ(:,:,ITER_K)     = displ(:,:,ITER_AUX)
        veloc_sld(:,:,ITER_K) = veloc_sld(:,:,ITER_AUX)
        accel_sld(:,:,ITER_K) = accel_sld(:,:,ITER_AUX)

        if (kfl_xfeme_sld == 1) then
           dxfem_sld(:,:,ITER_K) = dxfem_sld(:,:,ITER_AUX)
           vxfem_sld(:,:,ITER_K) = vxfem_sld(:,:,ITER_AUX)
           axfem_sld(:,:,ITER_K) = axfem_sld(:,:,ITER_AUX)
!            write(*,*)'case 2: dxfem_sld =',dxfem_sld(:,:,ITER_K)
!            write(*,*)'case 2: displ =',displ(:,:,ITER_K)
        end if

        !if (kfl_arcle_sld == 1) then
           !lambda_sld(ITER_K) = lambda_sld(ITER_AUX)
        !end if

        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           itotv=(ipoin-1)*ndofn_sld
           do idime=1,ndime
              itotv=itotv+1
              unkno(itotv)=displ(idime,ipoin,ITER_AUX)
           end do

           if (kfl_xfeme_sld == 1) then
              do idime=1,ndime
                 itotv=itotv+1
                 unkno(itotv)=dxfem_sld(idime,ipoin,ITER_AUX)
              end do
           end if
 
           !if (kfl_arcle_sld == 1) then
           !   itotv=itotv+1
           !   unkno(itotv)=lambda_sld(ITER_AUX)
           !end if

        end do

     case(3)
        !
        ! Assign u(n,i,j-1) <-- u(n,i,j), update of the displacement
        !
        relax_new = relfa_sld
        relax_old = 1.0_rp - relfa_sld

        if (kfl_tisch_sld == 1) then     ! CD and CN (0.5 relaxation required for CN)
           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
              itotv=(ipoin-1)*ndofn_sld
              do idime=1,ndime
                 itotv=itotv+1
                 displ(idime,ipoin,ITER_K)=(relax_new * unkno(itotv) + relax_old * displ(idime,ipoin,ITER_K))
              end do

              if (kfl_xfeme_sld == 1) then
                 do idime=1,ndime
                    itotv=itotv+1
                    dxfem_sld(idime,ipoin,ITER_K)=(relax_new * unkno(itotv) + relax_old * dxfem_sld(idime,ipoin,ITER_K))
                 end do
!                  write(*,*)'case 3: dxfem_sld =',dxfem_sld(:,:,ITER_K)
              end if
 
              !if (kfl_arcle_sld == 1) then
              !   itotv=itotv+1
              !   lambda_sld(ITER_K)=(relax_new * unkno(itotv) + relax_old * lambda_sld(ITER_K))
              !end if
           end do

        else if (kfl_tisch_sld == 2) then ! Newmark
           izone = lzone(ID_SOLIDZ)
           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
              itotv=(ipoin-1)*ndofn_sld
              do idime=1,ndime
                 itotv=itotv+1
                 displ(idime,ipoin,ITER_K)= unkno(itotv)
              end do

              if (kfl_xfeme_sld == 1) then
                 do idime=1,ndime
                    itotv=itotv+1
                    dxfem_sld(idime,ipoin,ITER_K)= unkno(itotv)
                 end do
!                  write(*,*)'case 3: dxfem_sld =',dxfem_sld(:,:,ITER_K)
              end if

              !if (kfl_arcle_sld == 1) then
              !   itotv=itotv+1
              !   lambda_sld(ITER_K)= unkno(itotv) 
              !end if
           end do

        end if

     case(4)
        !
        ! Assign u(n,i-1,*) <-- u(n,i,*)
        !
!       izone = lzone(ID_SOLIDZ)
!        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
!           do idime=1,ndime
!              displ(idime,ipoin,ITER_AUX)=displ(idime,ipoin,ITER_K)
!              veloc_sld(idime,ipoin,ITER_AUX)=veloc_sld(idime,ipoin,ITER_K)
!              accel_sld(idime,ipoin,ITER_AUX)=accel_sld(idime,ipoin,ITER_K)
!           end do
!        end do
        displ(:,:,ITER_AUX)     = displ(:,:,ITER_K)
        veloc_sld(:,:,ITER_AUX) = veloc_sld(:,:,ITER_K)
        accel_sld(:,:,ITER_AUX) = accel_sld(:,:,ITER_K)

        if (kfl_xfeme_sld == 1) then
           dxfem_sld(:,:,ITER_AUX) = dxfem_sld(:,:,ITER_K)
           vxfem_sld(:,:,ITER_AUX) = vxfem_sld(:,:,ITER_K)
           axfem_sld(:,:,ITER_AUX) = axfem_sld(:,:,ITER_K)
!            write(*,*)'case 4: dxfem_sld =',dxfem_sld(:,:,ITER_AUX)
        end if

        !if (kfl_arcle_sld == 1) then
        !   lambda_sld(ITER_AUX) = lambda_sld(ITER_K)
        !end if

     case(5)
        !
        ! u(n-1,*,*) <-- u(n,*,*)
        !
        do itime=2+kfl_tiacc_sld,4,-1
!	    izone = lzone(ID_SOLIDZ)
!           do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
!              do idime=1,ndime
!                 displ(idime,ipoin,itime) = displ(idime,ipoin,itime-1)
!                 veloc_sld(idime,ipoin,itime) = veloc_sld(idime,ipoin,itime-1)
!                 accel_sld(idime,ipoin,itime) = accel_sld(idime,ipoin,itime-1)
!              end do
!           end do
           displ(:,:,itime)     = displ(:,:,itime-1)
           veloc_sld(:,:,itime) = veloc_sld(:,:,itime-1)
           accel_sld(:,:,itime) = accel_sld(:,:,itime-1)

           if (kfl_xfeme_sld == 1) then
              dxfem_sld(:,:,itime) = dxfem_sld(:,:,itime-1)
              vxfem_sld(:,:,itime) = vxfem_sld(:,:,itime-1)
              axfem_sld(:,:,itime) = axfem_sld(:,:,itime-1)
           end if

           !if (kfl_arcle_sld == 1) then
           !   lambda_sld(itime) = lambda_sld(itime-1)
           !end if

        end do

!        izone = lzone(ID_SOLIDZ)
!        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
!           do idime=1,ndime
!              displ(idime,ipoin,TIME_N) = displ(idime,ipoin,ITER_K)
!              veloc_sld(idime,ipoin,TIME_N) = veloc_sld(idime,ipoin,ITER_K)
!              accel_sld(idime,ipoin,TIME_N) = accel_sld(idime,ipoin,ITER_K)
!           end do
!        end do

        displ(:,:,TIME_N)     = displ(:,:,ITER_K)
        veloc_sld(:,:,TIME_N) = veloc_sld(:,:,ITER_K)
        accel_sld(:,:,TIME_N) = accel_sld(:,:,ITER_K)

        if (kfl_xfeme_sld == 1) then
           dxfem_sld(:,:,TIME_N) = dxfem_sld(:,:,ITER_K)
           vxfem_sld(:,:,TIME_N) = vxfem_sld(:,:,ITER_K)
           axfem_sld(:,:,TIME_N) = axfem_sld(:,:,ITER_K)
        end if

        !if (kfl_arcle_sld == 1) then
        !   lambda_sld(TIME_N) = lambda_sld(ITER_K)
        !end if


     case(6)
        !
        ! Assign u(n,i,*) <-- u(n-1,*,*), initial guess after reading restart
        !
        icomp=min(3,ncomp_sld)
!        izone = lzone(ID_SOLIDZ)
!        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
!           do idime=1,ndime
!              displ(idime,ipoin,ITER_K) = displ(idime,ipoin,icomp)
!              veloc_sld(idime,ipoin,ITER_K) = veloc_sld(idime,ipoin,icomp)
!              accel_sld(idime,ipoin,ITER_K) = accel_sld(idime,ipoin,icomp)
!           end do
!        end do
        displ(:,:,ITER_K)     = displ(:,:,icomp)
        veloc_sld(:,:,ITER_K) = veloc_sld(:,:,icomp)
        accel_sld(:,:,ITER_K) = accel_sld(:,:,icomp)


     case(7)
       !
       ! update state variables state_sld(n,i-1,*) <-- u(n,i,*)
       !
       state_sld(:,:,:,ITER_K) = state_sld(:,:,:,ITER_AUX)

     case(8)
        !
        ! Assign u(n,i-1,*) <-- mean value u(n,i,*)
        !
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           do idime=1,ndime
              displ(idime,ipoin,ITER_AUX)=0.8_rp * displ(idime,ipoin,ITER_K) + 0.2_rp * displ(idime,ipoin,ITER_AUX)
           end do
        end do


     end select

  end if

end subroutine sld_updunk

