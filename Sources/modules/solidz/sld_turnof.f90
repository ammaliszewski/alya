subroutine sld_turnof()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_turnof
  ! NAME 
  !    sld_turnof
  ! DESCRIPTION
  !    This routine closes SOLIDZ run 
  ! USES
  !    sld_output
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_solidz
  implicit none

end subroutine sld_turnof

