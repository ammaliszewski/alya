subroutine sld_memphy(itask)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_memphy
  ! NAME
  !    sld_memphy
  ! DESCRIPTION
  !    Allocate memory for the physical problem
  ! OUTPUT 
  ! USES
  ! USED BY
  !    sld_reaphy
  !    sld_sendat
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_solidz
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ielem,nsize
  integer(4)              :: istat

  select case(itask)
     
  case( 1_ip)
     !
     ! Density: DENSI_SLD, LAWDE_SLD, and velocity of sound: VELAS_SLD
     !
     allocate(densi_sld(ncoef_sld,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DENSI_SLD','sld_memphy',densi_sld)
     allocate(velas_sld(ncoef_sld,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VELAS_SLD','sld_memphy',velas_sld)
     allocate(lawde_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LAWDE_SLD','sld_memphy',lawde_sld)
     !
     ! Rayleigh damping
     !
     allocate(dampi_sld(2,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DAMPI_SLD','sld_memphy',dampi_sld)
     allocate(kfl_dampi_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_DAMPI_SLD','sld_memphy',kfl_dampi_sld)
     kfl_dampi_sld = 0                          ! Numerical damping
     dampi_sld     = 0.0_rp                     ! Rayleigh damping parameters alpha and beta
     !
     ! Thermal effects
     !
     allocate(deltt_sld(2),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DELTT_SLD','sld_memphy',deltt_sld)
     deltt_sld = 0.0_rp

     !
     ! Constitutive law: PARCO_SLD, LAWCO_SLD, LAWST_CO, LAWMO_SLD
     !
     allocate(parco_sld(ncoef_sld,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'PARCO_SLD','sld_memphy',parco_sld)
     allocate(parcc_sld(ncoef_sld,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'PARCC_SLD','sld_memphy',parcc_sld)     
     allocate(lawco_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LAWCO_SLD','sld_memphy',lawco_sld)
     allocate(lawst_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LAWST_SLD','sld_memphy',lawst_sld)
     allocate(lawch_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LAWCH_SLD','sld_memphy',lawch_sld)
     allocate(lawmo_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LAWMO_SLD','sld_memphy',lawmo_sld)
     allocate(kusmo_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KUSMO_SLD','sld_memphy',kusmo_sld)
     allocate(modfi_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'MODFI_SLD','sld_memphy',modfi_sld)
     modfi_sld=-1
     allocate(modor_sld(2,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'MODOR_SLD','sld_memphy',modor_sld)
     modor_sld= 0
     allocate(cocof_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'COCOF_SLD','sld_memphy',cocof_sld)
     cocof_sld=1.0_rp !value for human    
     allocate(timec_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'TIMEC_SLD','sld_memphy',timec_sld)
     timec_sld=0.06_rp  !value for human - in sec         
     allocate(hillc_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'HILLC_SLD','sld_memphy',hillc_sld)
     hillc_sld=3.0_rp  !default value
     allocate(cal50_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'CAL50_SLD','sld_memphy',cal50_sld)
     cal50_sld=0.5_rp  !default value
     allocate(trans_sld(2,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'TRANS_SLD','sld_memphy',trans_sld)
     trans_sld=0.0_rp
     allocate(kfl_coupt_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_COUPT_SLD','sld_memphy',kfl_coupt_sld)
     kfl_coupt_sld=0
     allocate(kfl_eccty_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_ECCTY_SLD','sld_memphy',kfl_eccty_sld)
     kfl_eccty_sld=1
     allocate(parsp_sld(ncoef_sld,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'PARSP_SLD','sld_memphy',parsp_sld)
     allocate(parch_sld(ncoef_sld,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'PARCH_SLD','sld_memphy',parch_sld)
     allocate(parcf_sld(ncoef_sld,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'PARCF_SLD','sld_memphy',parcf_sld)
     allocate(kfl_aleso_sld(nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_ALESO_SLD','sld_memphy',kfl_aleso_sld)
     kfl_aleso_sld = 0                          
     !
     ! Constitutive law :Bridge (added by PIERRE OJO: ADD CHECK MEMORY )
     !
     allocate(gpsl0_sld(ndime,ndime,30),stat=istat)  !30 being the max possible nb of GP    
     gpsl0_sld=0.0_rp 
     allocate(rstr0_sld(ndime,ndime,30),stat=istat)     
     rstr0_sld=0.0_rp      
     !
     ! Stiffness matrix for undamage model
     !
     allocate(stiff0_sld(ndime*2,ndime*2,nmate_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'STIFF0_SLD','sld_memphy',stiff0_sld)
     
     ! INTERNAL STATE VARIABLES                                                    ( *AQU* )
     !    a vector of 8 variables for each gauss point of each element
     !
     allocate(state_sld(nstat_sld, mgaus, nelem, 2), stat = istat)
     call memchk(zero, istat, mem_modul(1:2,modul), 'STATE_SLD', 'sld_memall', state_sld)
         
  end select

end subroutine sld_memphy
