!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_output.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Output 
!> @details Output 
!> @} 
!-----------------------------------------------------------------------
subroutine sld_output()
  !------------------------------------------------------------------------
  !****f* Solidz/sld_output
  ! NAME 
  !    sld_output
  ! DESCRIPTION
  !    End of a SOLIDZ time step 
  ! USES
  !    sld_outvar
  !    postpr
  !    sld_outbcs
  !    sld_outset
  !    sld_exaerr
  ! USED BY
  !    sld_endste (itask=1)
  !    sld_turnof (itask=2)
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use mod_postpr
  use mod_iofile
  implicit none
  integer(ip) :: ivari,ivarp,idime,jdime,ivoig,ipoin
  real(rp)    :: dummr
  real(rp)  ,target  :: xauxi(1)
  !
  ! Fibers on element
  !
  ivari = 10
  call posdef(11_ip,ivari)
  if( ivari /= 0 ) call sld_elmope(4_ip)
  !
  ! Postprocess
  !
  kfote_sld = 0
  do ivarp = 1,nvarp
   ivari = ivarp
     call posdef(11_ip,ivari)
     call sld_outvar(ivari)  
  end do

!!   write(923,*) cutim,displ(3,6,1),elmag(1,6,1),dtime
!!   write(923,*) cutim,displ(3,49199,1),elmag(1,6,1),dtime
!!   write(923,*) cutim,displ(3,80461,1),elmag(1,80461,1),fisoc(80461),dtime

   !write(923,*) cutim,displ(1,337,1)
   !write(924,*) cutim,displ(2,337,1)
   !write(925,*) cutim,displ(3,337,1)
   !write(926,*) cutim,elmag(1,337,1)

 if( ittyp == ITASK_INITIA .or. ittyp == ITASK_ENDTIM ) then
     !
     ! Calculations on sets
     !
     call sld_outset()
  end if

  if( ittyp == ITASK_INITIA .or. ittyp == ITASK_ENDTIM ) then
     !
     ! Postprocess on witness points
     !
     call sld_outwit()
  end if

  if( ittyp == ITASK_ENDRUN ) then
     !
     ! End of the run
     !
     call sld_exaerr(2_ip)

  end if  

  !if( ittyp == ITASK_ENDRUN ) then
  !   print*,'a=',ittyp
  !    call pspltm(&
  !        npoin,npoin,ndofn_sld,0_ip,c_dom,r_dom,amatr,&
  !        trim(title)//': '//namod(modul),0_ip,18.0_rp,'cm',&
  !        0_ip,0_ip,2_ip,13)
  !    stop
  ! end if

end subroutine sld_output
