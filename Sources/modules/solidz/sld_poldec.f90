subroutine sld_poldec(F,R,U)

!-----------------------------------------------------------------------
!
! This subroutine performs the right polar decomposition F=R*U, of the
! deformation gradient F (3x3 matrix) into a rotation tensor, R, and 
! the right stretch tensor, U. Requires subroutine "invmtx".
!
!-----------------------------------------------------------------------

  use def_kintyp

  implicit none
  real(rp),    intent(in)  :: F(3,3) 
  real(rp),    intent(out) :: R(3,3), U(3,3)
  real(rp)                 :: detF, detU, C(3,3), omega(3)
  real(rp)                 :: eigvec(3,3)
  real(rp)                 :: Uinv(3,3), temp(3,3)
  integer(ip)              :: idime, jdime, kdime,NROT
  
  
  do idime=1,3
     do jdime=1,3

        ! store identity matrix in R, U, and Uinv
        if (idime==jdime) then
           R(idime,jdime)=1.D0
           U(idime,jdime)=1.D0
           Uinv(idime,jdime)=1.D0
        else
           R(idime,jdime)=0.D0
           U(idime,jdime)=0.D0
           Uinv(idime,jdime)=0.D0
        end if

        ! store zero matrix in R, C, and temp
        R(idime,jdime)=0.D0
        C(idime,jdime)=0.D0
        temp(idime,jdime)=0.D0


        ! calculate right cauchy-green strain tensor, C = F^T * F
        do kdime=1,3
           C(idime,jdime)=C(idime,jdime)+F(kdime,idime)*F(kdime,jdime)
        end do
      
     end do
  end do

  ! compute eigenvalues and eigenvectors

  call spcdec(C,omega,eigvec,NROT,1_ip)


  ! calculate the principal values of U

  do idime=1,3
     U(idime,idime) = SQRT(omega(idime))
  end do
  

  ! calculate complete tensor U = eigvec*U*eigvec^T
  do idime=1,3
     do jdime=1,3
        do kdime=1,3
           temp(idime,jdime)=temp(idime,jdime)+eigvec(idime,kdime)&
           *U(kdime,jdime)
        end do
     end do
  end do
  
  do idime=1,3
     do jdime=1,3
        U(idime,jdime)=0.D0
        do kdime=1,3
           U(idime,jdime)=U(idime,jdime)+temp(idime,kdime)*eigvec(jdime,kdime)
        end do
     end do
  end do


  ! calculate inverse of U

  call invmtx(U,Uinv,detU,3)


  ! calculate R=F*U^-1

  do idime=1,3
     do jdime=1,3
        do kdime=1,3
           R(idime,jdime)=R(idime,jdime) + F(idime,kdime)*Uinv(kdime,jdime)
        end do
     end do
  end do 
  

end subroutine sld_poldec
