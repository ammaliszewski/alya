subroutine sld_vumat_bridge(pgaus,pmate,temp0,temp1,gpgi0,gpgi1,gpigd,&
     gpdet,gprat,gppio,gpivo,gpene,flagt,gptmo,flags,ggsgo,ielem)

!-----------------------------------------------------------------------
!****f* Solidz/sld_vumat_bridge
! NAME
!    sld_vumat_bridge
! DESCRIPTION
!    Interface for calling VUMAT subroutines
! INPUT
!    PGAUS ... Number of Gauss points 
!    PMATE ... Material number
!    GPGI0 ... Initial deformation gradient tensor ............. F(n)
!    GPGI1 ... Updated deformation gradient tensor ............. F(n+1)
!    GPDET ... Updated jacobian ................................ J = det(F(n+1))
!    GPIGD ... Inverse of updated deformation gradient tensor .. F^{-1}(n+1)
!    GPRAT ... Rate of Deformation tensor ...................... Fdot = grad(phidot)
!    FLAGT ... Flag for tangent moduli calculations (1 if yes; 0 if no)
!    FLAGS ... Flag for Strain Gradient calculations (1 if yes; 0 if no)
!    IELEM ... Number of the element ........................... I_e
! INPUT/OUTPUT
!    GPENE ... Stored energy function .......................... W(n)/W(n+1)
!    GPPIO ... 1st Piola-Kirchhoff stress tensor at t(n)/t(n+1)  P(n)/P(n+1)
!    GPIVO ... Internal variable array at t(n)/t(n+1) .......... Q(n)/Q(n+1)
! OUTPUT
!    GPTMO ... Tangent moduli at t(n+1) ........................ dP/dF(n+1)
!    GGSGO ... Deformation gradient gradient tensor ............ dF/dX(n+1)
! USES
!    vumat_nh  .................... 3D Neo-Hookean constitutive law
!    RSTR1     ... R in F=VR = RU (Updated (n+1))
!    RSTR0_SLD ... R in F=VR = RU (Initial (n))
!    GPFIN     ... Finger strain tensor (or left Cauchy).............b=F F^T
!    GPSL0_SLD ... Initial Logarithmic strain .......................eps_L(n) = sum(ln(lambda)*n*n^T)
!    GPSL1     ... Updated Logarithmic strain .......................eps_L(n+1) = sum(ln(lambda)*n*n^T)
!    STRIN     ... Strain Increment (strainInc) in Voigt notation
!    GPSTI     ... Strain Increment (strainInc) 
!    GPOME     ... Angulat velocity (omega) .........................Omega = R^dot * R^T= [(R_1-R_0)/dt]*R^T
!    GPDER     ....delatR
! USED BY
!    sld_user_materials
!***
!-----------------------------------------------------------------------

  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime
  use def_solidz
  use def_master, only       :  dtime,ittim,cutim

  implicit none
  integer(ip), intent(in)    :: pgaus,pmate,ielem
  real(rp),    intent(in)    :: temp0(pgaus)
  real(rp),    intent(in)    :: temp1(pgaus)
  real(rp),    intent(in)    :: gpgi0(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpgi1(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpigd(ndime,ndime,pgaus)    
  real(rp),    intent(in)    :: gpdet(pgaus)  
  real(rp),    intent(in)    :: gprat(ndime,ndime,pgaus)  
  integer(ip), intent(in)    :: flagt,flags
  real(rp),    intent(inout) :: gppio(ndime,ndime,pgaus)
  real(rp),    intent(inout) :: gpivo(nvint_sld,pgaus)
  real(rp),    intent(inout) :: gpene(pgaus)    


! Flag to use VUMAT in single or double precision (1 for double)  
  integer(ip)     :: flgd

! Additional variables needed to call VUMAT
  real(rp)        :: gpgi1_inv(ndime,ndime),&
                     cauchy(ndime,ndime,pgaus), &
                     rstr1(ndime,ndime,pgaus),&
                     temp(ndime,ndime,pgaus),udefo(ndime,ndime,pgaus),vdefo(ndime,ndime,pgaus),gpdeb,bidon,& 
                     temm1(ndime,ndime),temm2(ndime,ndime)
  !  OJO OJO The dimension has changed. gptmo has to be addapted to Alya format                  
  real(rp)        :: gptmo(ndime,ndime,ndime,ndime,pgaus)
  ! ! ! ! ! ! 
  real(rp)        :: ggsgo(ndime,ndime)
  real(rp)        :: gpfin(ndime,ndime,pgaus)
  real(rp)        :: gpeva(ndime,pgaus),gpeve(ndime,ndime,pgaus)   
  real(rp)        :: gpsl1(ndime,ndime,pgaus) 
  real(rp)        :: gpome(ndime,ndime,pgaus)                     
  real(rp)        :: gpder(ndime,ndime,pgaus)                     
  real(rp)        :: gpsti(ndime,ndime,pgaus)                     
                     
  integer(ip)     :: idime,jdime,kdime,mdime,igaus,istat,k,p,i,j

  
  !Variable to give to the usermat (format is "free")
  
  integer(ip)   :: idum,pgauf,ndimf,nshef,nnvif,ncoef,modelnumber
  character     :: cdum
  
  real(rp)      :: dum_d,props_d(ncoef_sld),gpgi0_tmp_d(pgaus,ndime+2*nshea_sld),&
                   cauchy_tmp_d(pgaus,nvoig_sld),udefo_tmp_d(pgaus,nvoig_sld),vdefo_tmp_d(pgaus,nvoig_sld),&
                   temp0_tmp_d(pgaus),temp1_tmp_d(pgaus),gpene_tmp_d(pgaus),&
                   dtime_tmp_d,gpgi1_tmp_d(pgaus,ndime+2*nshea_sld),&
                   gpivo_tmp_d(pgaus,nvint_sld),stime_d,strin_d(pgaus,nvoig_sld)
                   
  real*4        :: dum,props(ncoef_sld),gpgi0_tmp(pgaus,ndime+2*nshea_sld),&
                   cauchy_tmp(pgaus,nvoig_sld),udefo_tmp(pgaus,nvoig_sld),vdefo_tmp(pgaus,nvoig_sld),&
                   temp0_tmp(pgaus),temp1_tmp(pgaus),gpene_tmp(pgaus),dtime_tmp,&
                   gpgi1_tmp(pgaus,ndime+2*nshea_sld),gpivo_tmp(pgaus,nvint_sld),stime,strin(pgaus,nvoig_sld)
  

  if (ndime == 2) call runend('SLD_VUMAT_BRIDGE: ONLY 3D MODELS ALLOWED!!!')

  ! Size of arrays given to VUMAT   
  ndimf=3 ! (whether ndime=3 or 2)
  pgauf=int(pgaus)
  nshef=int(nshea_sld)
  nnvif=int(nvint_sld)
  ncoef=int(ncoef_sld)

  idum= 0
  dum= 0.0_rp

 write(*,*) '' 
 write(*,*) 'gpivo Begin Bridge'      
 write(*,*) gpivo

  !
  !OJO: WORKS FOR ONLY ONE LOAD STEP FUNCTION (IFUNC IN SLD_REABCS)
  !
  !time of the load step   
  if (flgd == 1) then
      stime_d =fubcs_sld(12,1)-fubcs_sld(11,1)
  else 
      stime =sngl(fubcs_sld(12,1))-sngl(fubcs_sld(11,1))  
  end if

  dtime_tmp_d = dtime
  dtime_tmp   = sngl(dtime)

  ! Flag to use VUMAT in double precision by default 
  flgd = 0_ip

  ! convert 1st PK to Cauchy stress (sigma=1/J * P * F^t)

  do igaus=1,pgaus
     do idime=1,ndime
        do jdime=1,ndime
           cauchy(idime,jdime,igaus)=0.0_rp
           do kdime=1,ndime
              cauchy(idime,jdime,igaus)=cauchy(idime,jdime,igaus)+&
              gppio(idime,kdime,igaus)*gpgi1(jdime,kdime,igaus)/gpdet(igaus)
           end do
        end do
     end do
  end do


 
  ! calculate deformation gradient gradient (ggsgo)

  if (flags==1) then
  !**********************************************************************
  !
  ! note: GGSGO not calculated in this version of sld_vumat_bridge
  !       Code can be added here if needed 
  !
  !**********************************************************************
        call runend('SLD_VUMAT_BRIDGE: Deformation gradient gradient not available')
  end if
  


 ! calculate tangent moduli (gptmo)

  if (flagt==1) then
  !**********************************************************************
  !
  ! note: GPTMO not calculated in this version of sld_vumat_bridge
  !       Code can be added here if needed 
  !       (numerical tangent moduli to be added) 
  !       (analytical tangent moduli not available for VUMAT)
  !
  !**********************************************************************
        call runend('SLD_VUMAT_BRIDGE: Tangent moduli not implemented yet for VUMAT')
  end if  


  ! 
  ! 1. Convert from Alya to Abaqus vumat formats
  !      

  do igaus=1,pgaus


     ! Intial deformation gradient
     if (flgd == 1) then
        gpgi0_tmp_d(igaus,1) = gpgi0(1,1,igaus)
        gpgi0_tmp_d(igaus,2) = gpgi0(2,2,igaus)
        gpgi0_tmp_d(igaus,3) = gpgi0(3,3,igaus)
        gpgi0_tmp_d(igaus,4) = gpgi0(1,2,igaus)
        gpgi0_tmp_d(igaus,5) = gpgi0(2,3,igaus)
        gpgi0_tmp_d(igaus,6) = gpgi0(3,1,igaus)
        gpgi0_tmp_d(igaus,7) = gpgi0(2,1,igaus)
        gpgi0_tmp_d(igaus,8) = gpgi0(3,2,igaus)
        gpgi0_tmp_d(igaus,9) = gpgi0(1,3,igaus)
     else
        gpgi0_tmp(igaus,1) = sngl(gpgi0(1,1,igaus))
        gpgi0_tmp(igaus,2) = sngl(gpgi0(2,2,igaus))
        gpgi0_tmp(igaus,3) = sngl(gpgi0(3,3,igaus))
        gpgi0_tmp(igaus,4) = sngl(gpgi0(1,2,igaus))
        gpgi0_tmp(igaus,5) = sngl(gpgi0(2,3,igaus))
        gpgi0_tmp(igaus,6) = sngl(gpgi0(3,1,igaus))
        gpgi0_tmp(igaus,7) = sngl(gpgi0(2,1,igaus))
        gpgi0_tmp(igaus,8) = sngl(gpgi0(3,2,igaus))
        gpgi0_tmp(igaus,9) = sngl(gpgi0(1,3,igaus))          
     end if          


     ! Final deformation gradient 
     if (flgd == 1) then
        gpgi1_tmp_d(igaus,1) = gpgi1(1,1,igaus)
        gpgi1_tmp_d(igaus,2) = gpgi1(2,2,igaus)
        gpgi1_tmp_d(igaus,3) = gpgi1(3,3,igaus)
        gpgi1_tmp_d(igaus,4) = gpgi1(1,2,igaus)
        gpgi1_tmp_d(igaus,5) = gpgi1(2,3,igaus)
        gpgi1_tmp_d(igaus,6) = gpgi1(3,1,igaus)
        gpgi1_tmp_d(igaus,7) = gpgi1(2,1,igaus)
        gpgi1_tmp_d(igaus,8) = gpgi1(3,2,igaus)
        gpgi1_tmp_d(igaus,9) = gpgi1(1,3,igaus)
     else
        gpgi1_tmp(igaus,1) = sngl(gpgi1(1,1,igaus))
        gpgi1_tmp(igaus,2) = sngl(gpgi1(2,2,igaus))
        gpgi1_tmp(igaus,3) = sngl(gpgi1(3,3,igaus))
        gpgi1_tmp(igaus,4) = sngl(gpgi1(1,2,igaus))
        gpgi1_tmp(igaus,5) = sngl(gpgi1(2,3,igaus))
        gpgi1_tmp(igaus,6) = sngl(gpgi1(3,1,igaus))
        gpgi1_tmp(igaus,7) = sngl(gpgi1(2,1,igaus))
        gpgi1_tmp(igaus,8) = sngl(gpgi1(3,2,igaus))
        gpgi1_tmp(igaus,9) = sngl(gpgi1(1,3,igaus))     
     end if


     ! Initial internal variable array
     ! (Careful: some local variables of Alya might actually be an internal
     !           variable in the vumat, and thus will need to be stored in both
     !           places) 

  
     do idime=1,nvint_sld
         if (flgd == 1) then
            gpivo_tmp_d(igaus,idime) = gpivo(idime,igaus)
         else
            gpivo_tmp(igaus,idime) = sngl(gpivo(idime,igaus))
         end if
     end do


     ! 
     ! rotate Cauchy stress tensor before calling VUMAT
     !
     
     ! Get R and U (F=R*U ; gpgi1 = rstr1 * udefo)
     call sld_poldec(gpgi1(1,1,igaus),rstr1(1,1,igaus),udefo(1,1,igaus))

     if (ndime<3) then 
        write(*,*) 'SLD_VUMAT_BRIDGE:  sld_poldec only available in 3D '
        stop
     end if


     ! cauchy_vumat = R^T * cauchy * R (VUMAT uses the corotated Cauchy stress)
     
     do idime=1,ndime
        do jdime=1,ndime
           temp(idime,jdime,igaus)=0.0_rp
           do kdime=1,ndime
              temp(idime,jdime,igaus)=temp(idime,jdime,igaus)&
              + rstr1(kdime,idime,igaus) * cauchy(kdime,jdime,igaus)
           end do
        end do
     end do

     do idime=1,ndime
        do jdime=1,ndime
           cauchy(idime,jdime,igaus)=0.0_rp
           do kdime=1,ndime
              cauchy(idime,jdime,igaus)=cauchy(idime,jdime,igaus)&
              + temp(idime,kdime,igaus) * rstr1(kdime,jdime,igaus)
           end do
        end do
     end do

     !
     ! Calculate the strain increment (straiInc) !PUT A FLAG THERE MAYBE?
     ! strainInc = delta_epsilon = ln(delta_V)  
     !
       
          !Calculate b=F F^T
     
          do kdime=1,ndime
           do jdime=1,ndime
              gpfin(jdime,kdime,igaus)=0.0_rp
              do idime=1,ndime
                 gpfin(jdime,kdime,igaus)= gpfin(jdime,kdime,igaus) + gpgi1(jdime,idime,igaus)*gpgi1(kdime,idime,igaus)
              end do
           end do
         end do
      
          ! Find eigenvalues and eigenvectors of b  
      
         call spcdec(gpfin(1,1,igaus),gpeva(1,igaus),gpeve(1,1,igaus),bidon,1_ip)



!write(*,*) '  ' 
!write(*,*) 'gpfin '     
!write(*,*) gpfin(1,:,1)  
!write(*,*) gpfin(2,:,1)  
!write(*,*) gpfin(3,:,1) 


!write(*,*) '  ' 
!write(*,*) 'gpeve '     
!write(*,*) gpeve(1,:,1)  
!write(*,*) gpeve(2,:,1)  
!write(*,*) gpeve(3,:,1) 

!write(*,*) '  ' 
!write(*,*) 'gpeva '     
!write(*,*) gpeva  
 
         
         if (ndime<3) then 
            write(*,*) 'SLD_VUMAT_BRIDGE:  spcdec only available in 3D '
            stop
         end if 


        !Calculate updated epsilon_log
        do idime=1,ndime
          do jdime=1,ndime
             gpsl1(idime,jdime,igaus)=0.0_rp
          end do 
        end do
        
        do k=1,ndime
          do idime=1,ndime
             do jdime=1,ndime
                   gpsl1(idime,jdime,igaus)=gpsl1(idime,jdime,igaus) +&   
                   log(sqrt(gpeva(k,igaus)))*gpeve(idime,k,igaus)*gpeve(jdime,k,igaus) !OJO: Check the sqrt
                 end do 
             end do             
          end do 


!write(*,*) '  ' 
!write(*,*) 'gpsl1 '     
!write(*,*) gpsl1(1,:,1)  
!write(*,*) gpsl1(2,:,1)  
!write(*,*) gpsl1(3,:,1) 
         
         !Calculate the angular velocity omega 
          do idime=1,ndime
             do jdime=1,ndime         
                  temp(idime,jdime,igaus)= (rstr1(idime,jdime,igaus)-rstr0_sld(idime,jdime,igaus))/dtime
             end do 
          end do 
           !temp*R^T
          do idime=1,ndime 
             do jdime=1,ndime 
                gpome(idime,jdime,igaus)=0.0_rp
                do kdime=1,ndime                 
                  gpome(idime,jdime,igaus)= gpome(idime,jdime,igaus)+&
                                            temp(idime,kdime,igaus)*rstr1(jdime,kdime,igaus)
                end do
             end do 
          end do 
         
         !Archive R
         do idime=1,ndime
             do jdime=1,ndime
               rstr0_sld(idime,jdime,igaus)=rstr1(idime,jdime,igaus)
             end do 
         end do 
         
          !Calculate deltaR 
          do idime=1,ndime 
             do jdime=1,ndime 
                  temm1(idime,jdime)= -0.5_rp*dtime*gpome(idime,jdime,igaus)
             end do 
             temm1(idime,idime)= 1.0 + temm1(idime,idime) 
          end do          
          
          call invmtx(temm1(1,1),temm2(1,1),bidon,ndime)  
         
           do idime=1,ndime 
             do jdime=1,ndime 
                  temm1(idime,jdime)= 0.5_rp*dtime*gpome(idime,jdime,igaus)
             end do 
             temm1(idime,idime)= 1.0 + temm1(idime,idime) 
          end do              
  
           do idime=1,ndime 
             do jdime=1,ndime 
                gpder(idime,jdime,igaus)=0.0_rp
                do kdime=1,ndime 
                  gpder(idime,jdime,igaus)=gpder(idime,jdime,igaus)+&
                                           temm2(idime,kdime)*temm1(kdime,jdime)
                end do 
             end do 
           end do                         
         
          !Calculate StrainInc (gpsti in Alya and )
                   
          do idime=1,ndime 
            do jdime=1,ndime
               temm1(idime,jdime)=0.0_rp          
               do kdime=1,ndime 
                  do mdime=1,ndime                   
                     temm1(idime,jdime)=temm1(idime,jdime) &
                          + gpder(idime,kdime,igaus) * gpder(jdime,mdime,igaus)&
                          * gpsl0_sld(kdime,mdime,igaus)                  
                  end do 
                end do
            end do 
          end do 

          do idime=1,ndime 
            do jdime=1,ndime
                  gpsti(idime,jdime,igaus) = 0.0_rp
            end do 
          end do    
         
          do idime=1,ndime 
            do jdime=1,ndime
                  gpsti(idime,jdime,igaus) = gpsl1(idime,jdime,igaus)-temm1(idime,jdime)
            end do 
          end do          

!write(*,*) '  ' 
!write(*,*) 'gpsti '     
!write(*,*) gpsti(1,:,1)  
!write(*,*) gpsti(2,:,1)  
!write(*,*) gpsti(3,:,1)  
         
         !Archive strain_L 
          do idime=1,ndime 
            do jdime=1,ndime
                gpsl0_sld(idime,jdime,igaus) = gpsl1(idime,jdime,igaus)
            end do 
          end do  
 
        !Transform delta_epsilon to the Voigt notation   

        if (flgd == 1) then
           strin_d(igaus,1) = gpsti(1,1,igaus)
           strin_d(igaus,2) = gpsti(2,2,igaus)
           strin_d(igaus,3) = gpsti(3,3,igaus)
           strin_d(igaus,4) = gpsti(1,2,igaus)
           strin_d(igaus,5) = gpsti(2,3,igaus)
           strin_d(igaus,6) = gpsti(1,3,igaus)
        else
           strin(igaus,1) = sngl(gpsti(1,1,igaus))
           strin(igaus,2) = sngl(gpsti(2,2,igaus))
           strin(igaus,3) = sngl(gpsti(3,3,igaus))
           strin(igaus,4) = sngl(gpsti(1,2,igaus))
           strin(igaus,5) = sngl(gpsti(2,3,igaus))
           strin(igaus,6) = sngl(gpsti(1,3,igaus))
        end if         
         
        !
        ! End strainInc
        !     

!write(*,*) '  ' 
!write(*,*) 'strin '     
!write(*,*) strin(1,:)     
     !
     ! Change initial Cauchy stress from elmcla to VUMAT standards
     !
     if (flgd == 1) then
        cauchy_tmp_d(igaus,1) = cauchy(1,1,igaus)
        cauchy_tmp_d(igaus,2) = cauchy(2,2,igaus)
        cauchy_tmp_d(igaus,3) = cauchy(3,3,igaus)
        cauchy_tmp_d(igaus,4) = cauchy(1,2,igaus)
        cauchy_tmp_d(igaus,5) = cauchy(2,3,igaus)
        cauchy_tmp_d(igaus,6) = cauchy(1,3,igaus)
     else
        cauchy_tmp(igaus,1) = sngl(cauchy(1,1,igaus))
        cauchy_tmp(igaus,2) = sngl(cauchy(2,2,igaus))
        cauchy_tmp(igaus,3) = sngl(cauchy(3,3,igaus))
        cauchy_tmp(igaus,4) = sngl(cauchy(1,2,igaus))
        cauchy_tmp(igaus,5) = sngl(cauchy(2,3,igaus))
        cauchy_tmp(igaus,6) = sngl(cauchy(1,3,igaus))
     end if

     !
     ! Get temperatures/internal energy when needed
     !
     if (flgd == 1) then
        temp0_tmp_d(igaus) = temp0(igaus)
        temp1_tmp_d(igaus) = temp1(igaus)
        gpene_tmp_d(igaus) = gpene(igaus)
     else
        temp0_tmp(igaus) = sngl(temp0(igaus))
        temp1_tmp(igaus) = sngl(temp1(igaus))
        gpene_tmp(igaus) = sngl(gpene(igaus))
     end if     

  ! Change from elmcla standards into VUMAT standards for U
     if (flgd == 1) then
        udefo_tmp_d(igaus,1) = udefo(1,1,igaus)
        udefo_tmp_d(igaus,2) = udefo(2,2,igaus)
        udefo_tmp_d(igaus,3) = udefo(3,3,igaus)
        udefo_tmp_d(igaus,4) = udefo(1,2,igaus)
        udefo_tmp_d(igaus,5) = udefo(2,3,igaus)
        udefo_tmp_d(igaus,6) = udefo(1,3,igaus)
     else
        udefo_tmp(igaus,1) = sngl(udefo(1,1,igaus))
        udefo_tmp(igaus,2) = sngl(udefo(2,2,igaus))
        udefo_tmp(igaus,3) = sngl(udefo(3,3,igaus))
        udefo_tmp(igaus,4) = sngl(udefo(1,2,igaus))
        udefo_tmp(igaus,5) = sngl(udefo(2,3,igaus))
        udefo_tmp(igaus,6) = sngl(udefo(1,3,igaus))
     end if

  ! Change from elmcla standards into VUMAT standards for V
     if (flgd == 1) then
        vdefo_tmp_d(igaus,1) = vdefo(1,1,igaus)
        vdefo_tmp_d(igaus,2) = vdefo(2,2,igaus)
        vdefo_tmp_d(igaus,3) = vdefo(3,3,igaus)
        vdefo_tmp_d(igaus,4) = vdefo(1,2,igaus)
        vdefo_tmp_d(igaus,5) = vdefo(2,3,igaus)
        vdefo_tmp_d(igaus,6) = vdefo(1,3,igaus)
     else
        vdefo_tmp(igaus,1) = sngl(vdefo(1,1,igaus))
        vdefo_tmp(igaus,2) = sngl(vdefo(2,2,igaus))
        vdefo_tmp(igaus,3) = sngl(vdefo(3,3,igaus))
        vdefo_tmp(igaus,4) = sngl(vdefo(1,2,igaus))
        vdefo_tmp(igaus,5) = sngl(vdefo(2,3,igaus))
        vdefo_tmp(igaus,6) = sngl(vdefo(1,3,igaus))
     end if

 
  end do

  ! Get material parameters

  do i=1,ncoef_sld
       if (flgd == 1) then
          props_d(i) = parco_sld(i,pmate)
       else
          props(i) = sngl(parco_sld(i,pmate))               
       end if
  end do

 

  ! 
  ! 2. Call the models
  !      
  !ABAQUS Notation:
  ! 0  model number (needed)
  ! 1  nblock, ndi, nshr,
  ! 2  nstatev, nfieldv, nprops,
  ! 3  lanneal, stepTime, totTime,
  ! 4  dt, cmname, coordMp,
  ! 5  charLen, props, density,
  ! 6  Dstrain, rSpinInc, temp0,
  ! 7  U0, F0, field0,
  ! 8  stressVec0, state0, intEne0,
  ! 9  inelaEn0, temp1,U1,
  ! 10 F1, field1, stressVec1,
  ! 11 state1, intEne1, inelaEn1
  ! 
     
  modelnumber= kusmo_sld(pmate)

    if (flgd == 1) then !double
       call sld_vumat(&
            modelnumber,&                             !  0
            pgauf,ndimf,nshef,&                       !  1
            nnvif,idum,ncoef,&                        !  2
            idum,stime_d,dum_d,&                      !  3
            dtime_tmp_d,cdum,dum_d,&                  !  4   
            idum,props_d,dum_d,&                      !  5
            strin_d,dum_d,temp0_tmp_d,&                 !  6
            dum_d,gpgi0_tmp_d,dum_d,&                 !  7
            cauchy_tmp_d,gpivo_tmp_d,gpene_tmp_d,&    !  8
            dum_d,temp1_tmp_d,udefo_tmp_d,&           !  9
            gpgi1_tmp_d,dum_d,cauchy_tmp_d,&          ! 10   
            gpivo_tmp_d,gpene_tmp_d,dum_d)            ! 11
    else                 !single
       call sld_vumat(&
            modelnumber,&                       !  0
            pgauf,ndimf,nshef,&                 !  1  1 2 3
            nnvif,idum,ncoef,&                  !  2  4 5 6
            idum,stime,dum,&                    !  3  7 8 9
            dtime_tmp,cdum,dum,&                !  4  10 11 12 
            idum,props,dum,&                    !  5  13 14 15
            strin,dum,temp0_tmp,&               !  6  16 17 18
            dum,gpgi0_tmp,dum,&                 !  7  19 20 21
            cauchy_tmp,gpivo_tmp,gpene_tmp,&    !  8  22 23 24
            dum,temp1_tmp,udefo_tmp,&           !  9  25 26 27
            gpgi1_tmp,dum,cauchy_tmp,&          ! 10  28 29 30 
            gpivo_tmp,gpene_tmp,dum)            ! 11  31 32 33
    end if
     
  ! 
  ! 3. Convert from Abaqus to Alya formats
  !      


     
     ! Change from VUMAT standards back into elmcla standards

  do igaus=1,pgaus

     ! Final Cauchy stress
  
     if (flgd == 1) then
        cauchy(1,1,igaus) = cauchy_tmp_d(igaus,1)
        cauchy(2,2,igaus) = cauchy_tmp_d(igaus,2)
        cauchy(3,3,igaus) = cauchy_tmp_d(igaus,3)
        cauchy(1,2,igaus) = cauchy_tmp_d(igaus,4)
        cauchy(2,1,igaus) = cauchy_tmp_d(igaus,4)
        cauchy(2,3,igaus) = cauchy_tmp_d(igaus,5)
        cauchy(3,2,igaus) = cauchy_tmp_d(igaus,5)
        cauchy(1,3,igaus) = cauchy_tmp_d(igaus,6)
        cauchy(3,1,igaus) = cauchy_tmp_d(igaus,6)
     else
        cauchy(1,1,igaus) = dble(cauchy_tmp(igaus,1))
        cauchy(2,2,igaus) = dble(cauchy_tmp(igaus,2))
        cauchy(3,3,igaus) = dble(cauchy_tmp(igaus,3))
        cauchy(1,2,igaus) = dble(cauchy_tmp(igaus,4))
        cauchy(2,1,igaus) = dble(cauchy_tmp(igaus,4))
        cauchy(2,3,igaus) = dble(cauchy_tmp(igaus,5))
        cauchy(3,2,igaus) = dble(cauchy_tmp(igaus,5))
        cauchy(1,3,igaus) = dble(cauchy_tmp(igaus,6))
        cauchy(3,1,igaus) = dble(cauchy_tmp(igaus,6))
     end if


!write(*,*) '  ' 
!write(*,*) 'cauchy '     
!write(*,*) cauchy(1,:,1)  
!write(*,*) cauchy(2,:,1)  
!write(*,*) cauchy(3,:,1) 

     ! cauchy = R * cauchy_vumat * R^T (VUMAT uses the corotated Cauchy stress)

     do k=1,ndime
         do p=1,ndime 
            temp(k,p,igaus)=0.0_rp         
            do i=1,ndime 
               do j=1,ndime 
                  temp(k,p,igaus)=temp(k,p,igaus)+&
                        rstr1(k,i,igaus)*cauchy(i,j,igaus)*rstr1(p,j,igaus)
               end do 
            end do 
         end do 
     end do 
     do k=1,ndime
        do p=1,ndime 
            cauchy(k,p,igaus)=temp(k,p,igaus)
        end do 
     end do

     ! convert Cauchy stress back to 1st PK (P=J * sigma * F^-t)

     do idime=1,ndime
        do jdime=1,ndime
           gppio(idime,jdime,igaus)=0.0_rp
           do kdime=1,ndime
              gppio(idime,jdime,igaus)=gppio(idime,jdime,igaus)+&
              cauchy(idime,kdime,igaus)*gpigd(jdime,kdime,igaus)*gpdet(igaus)
           end do
        end do
     end do


     ! Final internal variable array

     do idime=1,nvint_sld
        if (flgd == 1) then
                gpivo(idime,igaus) = gpivo_tmp_d(igaus,idime)
        else
                gpivo(idime,igaus) = dble(gpivo_tmp(igaus,idime))
        end if
     end do 

  end do


 write(*,*) '' 
 write(*,*) 'gpivo Final Bridge'      
 write(*,*) gpivo

end subroutine sld_vumat_bridge
