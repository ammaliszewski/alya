!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_solite.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   This routine solves an iteration of the module equations.
!> @details This routine solves an iteration of the module equations.
!> @} 
!-----------------------------------------------------------------------
subroutine sld_solite()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_solite
  ! NAME
  !    sld_solite
  ! DESCRIPTION
  !    This routine solves an iteration of the module equations.
  ! USES
  !    sld_matrix
  ! USED BY
  !    sld_doiter
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use mod_communications, only : PAR_GATHER
  implicit none
  real(rp)    :: cpu_refe1,cpu_refe2

  call cputim(cpu_refe1)
  !
  ! Update inner iteration counter and write headings in solver file
  !
  itinn(modul) = itinn(modul) + 1
  !
  ! Update boundary conditions
  !
  !  call sld_updbcs(three)
  !
  !  important!!!!!!
  !  the call to sld_matrix has been removed because sld_matrix has to be done after
  !                  displacement update but before the velocity and acceleration update for the
  !                  Nemwark scheme (the call to sld_matrix(one) is now put inside sld_solexp
  !                  and the call to sld_matrix(two) is inside sld_solimp)
  if(kfl_timet_sld == 1) then

     ! Explicit scheme
     call sld_solexp()           ! compute displacement, velocity and acceleration
!     call sld_solexpviejuno()           ! compute displacement, velocity and acceleration


  else if(kfl_timet_sld == 2) then

     ! Implicit scheme
     call sld_solimp()           ! compute displacement, velocity and acceleration

  end if
  !
  ! Shell elements
  !
  !call sld_solshe()

  call cputim(cpu_refe2)
  cpu_modul(3,modul) =  cpu_modul(3,modul) + cpu_refe2 - cpu_refe1


!!!! ESTA ES LA POSTA
!
!  if (ISLAVE) then
!     cpual_slave_sld(1)= cpu_modul(3,modul)
!     cpual_slave_sld(2)= cpu_refe2 - cpu_refe1
!  end if
!
!  call PAR_GATHER(cpual_slave_sld,cpual_master_sld,'IN THE WORLD')
!
!  if (IMASTER) then
!     cpual_master_sld(0)= cpu_refe2 - cpu_refe1
!     cpual_master_sld(1)= cpu_refe2
!  end if
!
!  if (kfl_paral == 0) then
!
!     write(6,*)''
!     write(6,*) 'master',cpual_master_sld(0),cpual_master_sld(1)
!     write(6,*) 'ninyo',cpual_master_sld(2),cpual_master_sld(4),cpual_master_sld(6)
!     write(6,*) 'ninyo',cpual_master_sld(3),cpual_master_sld(5),cpual_master_sld(7)
!     write(6,*)''
!     
!  end if


end subroutine sld_solite
