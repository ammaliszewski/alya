module mod_numerical_integration
  ! ==============================================================================
  ! INIT
  !
  use def_parame, only                     :  &
      ip, rp
  ! -----------------------------------------------------------------------------
  implicit none 
  ! ----------------------------------------------------------------------------
  interface
    real(rp) function function_template(x, arg)
      use def_parame, only                 :  &
          ip, rp
      implicit none
      real(rp), intent(in)                 :: x
      real(rp), intent(inout), optional    :: arg(:)
    end function function_template  
  end interface
  !=============================================================| init |=========
  !==============================================================================
  ! PUBLIC
  !
  public                                   :: &
      numint_trapzd,                          &
      numint_qsimps
  !=============================================================| public |=======
  !==============================================================================
  ! PRIVATE
  !
  private
  !=============================================================| private |======
  !==============================================================================
  ! CONTAINS
  !
  contains
  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !------------------------------------------------------------------------------
  !> @addtogroup 
  !> @{
  !> @file    
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    
  !> @brief   
  !> @details This routine computes the nth stage of refinement of an extended 
  !           trapezoidal rule. f is input as the name of the function to be 
  !           integrated between limits a and b, also input. When called with n=1,
  !           the routine returns as s the crudest estimate of int(f(x)) from a to
  !           b. Subsequent calls with n=2,3,... (in that sequential order) will 
  !           improve the accuracy of s by adding 2n-2 additional interior points.
  !           s should not be modified between sequential calls.
  !> @} 
  !------------------------------------------------------------------------------
  subroutine numint_trapzd(low, upp, tra, num, fun, arg)

    use def_kintyp, only                    :  &  
        ip, rp, lg
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    procedure(function_template)            :: &
         fun  
    real(rp),       intent(inout), optional :: &
         arg(:)
    integer(ip),    intent(in)              :: &
         num                                  
    real(rp),       intent(in)              :: &
         low, upp                                !
    real(rp),       intent(inout)           :: &
         tra
    ! ---------------------------------------------------------------------------
    integer(ip)                             :: &
         i                                       ! Current and maximum allowed number of steps        
    real(rp)                                :: &
         itt,                                  & ! Maximum allowed number of steps
         del,                                  & !   
         are,                                  & !
         x                                       ! Desired fractional accuracy
    ! ---------------------------------------------------------------------------
    
    if (num == 1.0_ip) then
       tra = 0.5_rp*(upp - low)*(fun(low, arg) + fun(upp, arg))
    else
       itt = int(2.0_rp**(num - 2.0_rp),ip) ! OJO
       del = (upp - low)/itt
       x   = low + 0.5_rp*del
       are = 0.0_rp
       do i = 1, int(itt,ip)
          are = are + fun(x, arg)
          x   = x + del
       end do
       tra = 0.5_rp*(tra + (upp - low)*are/itt)
    end if

    
    return

  end subroutine numint_trapzd

  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  subroutine numint_qsimps(low, upp, sim, eps, ite, fun, arg)
  
    !----------------------------------------------------------------------------
    !> @addtogroup 
    !> @{
    !> @file    
    !> @author  Adria Quintanas (adria.quintanas@udg.edu)
    !> @date    
    !> @brief   
    !> @details Returns as s the integral of the function func from a to b. The 
    !           parameters EPS can be set to the desired fractional accuracy and
    !           JMAX so that 2 to the power JMAX-1 is the maximum allowed number 
    !           of steps. Integration is performed by Simpson’s rule.
    !> @} 
    !----------------------------------------------------------------------------
  
    use def_kintyp, only                    :  &  
        ip, rp, lg
    ! ---------------------------------------------------------------------------
    implicit none
    ! ---------------------------------------------------------------------------
    procedure(function_template)            :: &
         fun  
    real(rp),       intent(inout), optional :: &
         arg(:)   
    integer(ip),    intent(in)              :: &
         ite                                
    real(rp),       intent(in)              :: &
         low, upp, eps                           !
    real(rp),       intent(out)             :: &
         sim                                     ! Area
    ! ---------------------------------------------------------------------------
    integer(ip)                             :: &
         i                                       !
    real(rp)                                :: & 
         oldAre, oldTra, tra                     ! 
    ! ---------------------------------------------------------------------------

    oldAre = -1.E30_rp    
    oldTra = -1.E30_rp

    do i = 1, ite
       call numint_trapzd(low, upp, tra, i, fun, arg)
       sim = (4.0_rp*tra - oldTra)/3.0_rp
       if ( i > 5_ip ) then
          if ((abs(sim - oldAre) < eps*abs(oldAre)) .OR. (sim == 0.0_rp .AND. oldAre == 0.0_rp)) then
             return
          end if
       end if
       oldAre = sim
       oldTra = tra
    end do
  
  end subroutine numint_qsimps
 
 !=============================================================| contains |=====
 
end module mod_numerical_integration
