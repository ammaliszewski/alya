subroutine sld_inibcs()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_inibcs
  ! NAME
  !    sld_inibcs
  ! DESCRIPTION
  !    This routine reads the boundary conditions
  ! OUTPUT 
  ! USES
  ! USED BY
  !    sld_turnon
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use mod_memchk
  use mod_memory,         only :  memory_alloca_min
  use def_solidz
  implicit none

  integer(ip)  :: ipoin,kpoin,ibopo,idofn,idime,izone

  if( INOTMASTER ) then
     !
     ! Allocate memory
     !
     call sld_membcs(1_ip)
     call sld_membcs(2_ip)

     !-------------------------------------------------------------
     !
     ! Node codes
     !
     !-------------------------------------------------------------

     if( kfl_icodn > 0 ) then
        if( kfl_conbc_sld == 0 ) then
           iffun      =  1
           kfl_funno  => kfl_funno_sld
        else
           iffun      =  0
        end if
        ifbop     =  0
        ifloc     =  1
        kfl_fixrs => kfl_fixrs_sld
        kfl_fixno => kfl_fixno_sld
        bvess     => bvess_sld(:,:,1)
        tncod     => tncod_sld(:)
        call reacod(10_ip)

     end if

     !-------------------------------------------------------------
     !
     ! Boundary codes
     !
     !-------------------------------------------------------------

     if( kfl_icodb > 0 ) then
        if( kfl_conbc_sld == 0 ) then
           iffun      =  1
           kfl_funbo  => kfl_funbo_sld
        end if
        nparb     =  1
        kfl_fixbo => kfl_fixbo_sld
        bvnat     => bvnat_sld(:,:,1)
        tbcod     => tbcod_sld(1:)
        !tncod     => momod(ID_NASTIN) % tncod(1:)    
        call reacod(20_ip)
     end if

     !-------------------------------------------------------------
     !
     ! Final corrections
     !
     !-------------------------------------------------------------
    
     izone = lzone(ID_SOLIDZ)
     do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
        !
        ! Correct errors of reacod assignement on interior nodes (which we don't want to have!!)
        !
        ibopo= lpoty(ipoin)
        if (ibopo == 0) then                     ! ibopo=0 means this is an inner node
           do idofn= 1,ndime
              kfl_fixno_sld(idofn,ipoin) = 0
           end do
        end if
     end do

     !-------------------------------------------------------------
     !
     ! Exact solution
     !
     !-------------------------------------------------------------

     call sld_exaerr(1_ip)

  else

     call memory_alloca_min(bvess_sld) 

  end if

end subroutine sld_inibcs
