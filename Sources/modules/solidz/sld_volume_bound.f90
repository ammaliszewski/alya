subroutine sld_volume_bound()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_volume
  ! NAME
  !    sld_volume
  ! DESCRIPTION
  ! 
  !    ** WARNING ** Only for tetra element 
  !    ** WARNING ** The normal of the bnd element of the epicardium must point outward (PRECISER)- can do that in icem   
  !    ** WARNING ** a DELTA_V can be calculated, not the actual VOLUME
  ! INPUT
  !       LNODB(MNODB,NBOUN)       list of boundary elements 
  !       NBOUN                    Amount of boundary elements
  !       MNODB                    dim of boundary elements  
  ! USES
  ! USED BY
  !    sld_endite
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain
  use def_solidz
  use def_master
  implicit none


  integer(ip) :: ivolu,icavi,ielem,ipoin,idime,iboun,igaub,inodb,idofn
  integer(ip) :: pelty,pblty,pnodb,pgaub,pnode,inode,ipass

  real(rp)                   ::  xcent,ycent,zcent,node1(3),node2(3),node3(3),ax,ay,az,bx,by,bz
  real(rp)                   ::  gpele(3),norme,venor(3),super,volum(mcavi_sld),fonct,bidon
  real(rp)                   ::  dummr(4),super_total,deltv,deltp,compl
  real(rp)                   ::  coord_vol(3,3)
  real(rp)    :: baloc(ndime,ndime)
  real(rp)    :: bocod(ndime,mnodb),elcod(ndime,mnode)
  real(rp)    :: gbsur,eucta,tract(ndime),gppre



  volum       = 0.0_rp

  if( INOTMASTER ) then

     super_total = 0.0_rp
     super       = 0.0_rp

     do iboun=1,nboun
        ivolu= 0
        do icavi= 1,mcavi_sld
           if (lbset(iboun) == iocav_sld(icavi)) then
              ivolu= icavi
           end if
        end do

        if ((ivolu > 0)) then
                      !
           ! Element properties and dimensions
           !
           pblty=ltypb(iboun) 
           pnodb=nnode(pblty)
           pgaub=ngaus(pblty)
           ielem=lboel((pnodb+1),iboun)
           pelty=ltype(ielem)
           pnode=nnode(pelty)

           !
           ! Gather operations: BOCOD
           !
           do inodb=1,pnodb
              ipoin=lnodb(inodb,iboun)
              do idime=1,ndime
                 bocod(idime,inodb) = coord(idime,ipoin) + displ(idime,ipoin,1)
              end do
           end do
           do inode=1,pnode
              ipoin=lnods(inode,ielem)
              do idime=1,ndime
                 elcod(idime,inode)=coord(idime,ipoin) + displ(idime,ipoin,1)
              end do
           end do
           !
           ! Loop over Gauss points
           !
           gauss_points: do igaub=1,pgaub
              call bouder(&
                   pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),bocod,baloc,eucta)    
              gbsur=elmar(pblty)%weigp(igaub)*eucta 
              call chenor(pnode,baloc,bocod,elcod)        ! Check normal
              
              gpele= 0.0_rp
              do inodb = 1,pnodb
                 ipoin = lnodb(inodb,iboun)
                 do idime= 1,ndime
                    gpele(idime) = gpele(idime) + elmar(pblty)%shape(inodb,igaub) * (coord(idime,ipoin)+displ(idime,ipoin,1) - ocavi_sld(idime,ivolu))
                 end do
              end do
              
              fonct= 0.0_rp
              do idime= 1,ndime
                 fonct= fonct + gpele(idime) * baloc(idime,ndime)
              end do

              volum(ivolu) = volum(ivolu) - fonct * gbsur / real(ndime)
              
           end do gauss_points
                      
           
        end if

     end do !iboun
     
  end if !NOTMASTER

  call pararr('SUM',0_ip,mcavi_sld,volum)

  call pararr('SUM',0_ip,1_ip,super_total)

  do ivolu=1,mcavi_sld !swap all the cavities defined
     
     if (ittim==1) then
        !write(6,*) "hola",kfl_paral, volum(ivolu), ivolu
        volst_sld(2,ivolu) =   volum(ivolu)
        volvr_sld(ivolu)   =   volum(ivolu)
        
     else
     
        !update variables
        volst_sld(1,ivolu)=volst_sld(2,ivolu)  !V
        
        !deltaV
        volst_sld(2,ivolu)=volum(ivolu)
        volst_sld(4,ivolu) = volst_sld(2,ivolu)-volst_sld(1,ivolu) !deltaV new
        
        
     end if

     if (mcavi_sld == 1) then
        !
        ! When there is one ventricle only, put 1.0 in the second one volume
        !
        volst_sld(1,2)=1.0_rp
        
        !deltaV
        volst_sld(2,2) = 1.0_rp
        volst_sld(4,2) = 1.0_rp

     end if

     !
     ! Broadcast values to the slaves
     !  
     call pararr('BCT',0_ip,12_ip,volst_sld(1,ivolu))

  end do

100 format (2(F16.8,','))  
101 format (6(F19.11,',')) 
end subroutine sld_volume_bound
