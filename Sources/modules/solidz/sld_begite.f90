!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_begite.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   This routine initializes the linearization loop
!> @details This routine initializes the linearization loop
!> @} 
subroutine sld_begite()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_begite
  ! NAME 
  !    sld_begite
  ! DESCRIPTION
  !    This routine initializes the linearization loop
  ! USES
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solver
  use def_solidz
  implicit none
  integer(ip) :: idime
  !
  ! Initializations
  !
  kfl_goite_sld = 1 
  itinn(modul)  = 0
  if(itcou==1) call sld_tistep()
  !
  ! Update boundary conditions
  !
  call sld_updbcs(2_ip)
  !
  ! Initialization (rhsid and amatr set to zero)
  !
  call inisol() 
  !
  ! Couplings
  !
  call sld_coupli(ITASK_BEGITE)
  !
  ! Obtain the initial guess for inner iterations: unkno --> last iteration unknown
  !
  call sld_updunk(two)
  !
  ! Clear counts
  !
  volum_sld = 0.0_rp
  tactv_sld = 0.0_rp
  !
  ! Update cohesive laws
  !
  if (kfl_xfeme_sld > 0 .or. kfl_elcoh > 0)  call sld_updcoh(one)


end subroutine sld_begite
