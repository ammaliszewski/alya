subroutine sld_trate4(irotype,tenold,baslo,tennew,ndime)

   !----------------------------------------------------------------------------
   !> @addtogroup Solidz
   !> @{
   !> @file    sld_trate4.f90
   !> @author  Adria Quintanas (u1902492@campus.udg.edu)
   !> @date    Desember, 2014
   !> @brief   This subroutine performs a transformation of a tensor from a old
   !> @        basis to a new basis. 
   !> @details This subroutine performs a transformation of a tensor
   !> @           From Local CSYS to Global CSYS:
   !> @              S _{ijkl} = R_{pi}*R_{qj}*R_{rk}*R_{sl}*S'_{pqrs}
   !> @           From Global CSYS to Local CSYS:
   !> @              S'_{ijkl} = R_{ip}*R_{jq}*R_{kr}*R_{ls}*S _{pqrs}
   !> @} 
   !----------------------------------------------------------------------------

    
   use      def_kintyp  
    
   implicit none
    
   integer(ip),    intent(in)     ::    irotype,ndime
   real(rp),       intent(in)     ::    tenold(ndime,ndime,ndime,ndime), baslo(ndime,ndime)
   real(rp),       intent(out)    ::    tennew(ndime,ndime,ndime,ndime)
   integer(ip)                    ::    i,j,k,l,p,q,r,s
    
   ! -------------------------------
   ! INITIALIZE VARIABLES
   !
   tennew = 0.0_rp
    
    
   ! -------------------------------
   ! ROTATING THE TENSOR IN FUNCTION OF THE BASIS
   !
    
   if ( irotype == 1_ip ) then
      !
      ! From LCSYS to GCSYS
      !    S _{ijkl} = R_{pi}*R_{qj}*R_{rk}*R_{sl}*S'_{pqrs}
      !
       
      do i = 1, ndime
         do j = 1, ndime
            do k = 1, ndime
               do l = 1, ndime
                  do p = 1, ndime
                     do q = 1, ndime
                        do r = 1, ndime
                           do s = 1, ndime
                              tennew(i, j, k, l) = tennew(i, j, k, l) + baslo(p, i)*baslo(q, j)*baslo(r, k)*baslo(s, l)*tenold(p, q, r, s)
                           end do
                        end do
                     end do
                  end do
               end do
            end do
         end do
      end do


   else if ( irotype == 2_ip ) then
      !
      ! From GCSYS to LCSYS
      !    S'_{ijkl} = R_{ip}*R_{jq}*R_{kr}*R_{ls}*S _{pqrs}
      !
	   
      do i = 1, ndime
         do j = 1, ndime
            do k = 1, ndime
               do l = 1, ndime
                  do p = 1, ndime
                     do q = 1, ndime
                        do r = 1, ndime
                           do s = 1, ndime
                              tennew(i, j, k, l) = tennew(i, j, k, l) + baslo(i, q)*baslo(j, q)*baslo(k, r)*baslo(l, s)*tenold(p, q, r, s)
                           end do
                        end do
                     end do
                  end do
               end do
            end do
         end do
      end do

   end if
    	
end subroutine sld_trate4
