subroutine sld_defcon(itask)

  use def_kintyp
  use def_kermod
  use def_parame
  use def_master
  use def_domain
  use def_solver
  use def_solidz
  use mod_kdtree, only : typ_kdtree
  use mod_kdtree, only : kdtree_construct
  use mod_kdtree, only : kdtree_deallocate
  use mod_kdtree, only : kdtree_nearest_boundary
  use mod_memory

  implicit none

  integer(ip) :: ielem,ipoin,idime,iboun,igaub,inodb,idofn,itask
  integer(ip) :: pelty,pblty,pnodb,pgaub,pnode,inode,ipass,pevat
  integer(ip) :: iboun2,pblty2,pnodb2,inodb2,ipoin2,ipoin2_min 

  integer(ip) :: icont,jcont,kcont
  integer(ip), pointer :: lnods_tmp(:,:),nodmaster(:),nodslave(:)
  real(rp) :: isss,dist_min,dist,penet
  real(rp), pointer :: contelmat(:,:),gap(:,:)

  !definitions needed to implement kdtree and dpopar
  integer(ip), pointer :: npoin_tmp(:),npoin_tmp2(:),local2global(:),ltypb_perset(:),lnodb_perset(:,:)
  integer(ip)          :: npoib_perset,nboun_perset,tag_master,tag_slave
  real(rp), pointer    :: coord_perset(:,:)
  real(rp)             :: chkdi,vec_proje(3),proje(3)
  !!real(rp)             :: bobox_loc(3,2)
  !!real(rp),    pointer :: fabox_loc(:,:,:) 
  !!real(rp),    pointer :: sabox_loc(:,:,:) 
  !!integer(ip), pointer :: blink_loc(:)     
  !!integer(ip), pointer :: stru2_loc(:)     
  !!real(rp),    pointer :: ldist_loc(:)      
  !!type(netyp), pointer :: lnele_loc(:)     
  type(typ_kdtree)     :: kdtree_perset

  !definitions needed to implement parametric coordinates calculation
  integer(ip)          :: imin,ifoun
  real(rp)             :: dmin,lmini,lmaxi,tmp1,tmp2,tmp3,nu,eta
  real(rp), pointer    :: bobox(:,:)
  real(rp)             :: deriv(2),shapf(2),elcod(1,2),coglo(1),tau(2),coloc
  !definitions needed to perform contact residual and stiffness
  real(rp)             :: N(6),N1(6),T(6),m11,m22,m12,m21,D1(6),xx(3)
  integer(ip)          :: slave_ipoin,local2global_1,local2global_2

  !!nullify(fabox_loc)
  !!nullify(sabox_loc)
  !!nullify(blink_loc)
  !!nullify(stru2_loc)
  !!nullify(ldist_loc)
  !!nullify(lnele_loc)

  allocate(npoin_tmp(npoin))
  allocate(npoin_tmp2(npoin))
  allocate(local2global(npoin))
  allocate(ltypb_perset(nboun))  
  allocate(lnodb_perset(mnodb,nboun))
  allocate(coord_perset(ndime,npoin))
  allocate(gap(npoin,3))
  allocate(bobox(ndime,2))

  pnode = 3
  pevat = ndofn_sld*pnode
  allocate(nodmaster(npoin))
  allocate(nodslave(npoin))
  allocate(contelmat(ndofn_sld*pnode,ndofn_sld*pnode))
  allocate(lnods_tmp(pnode,nelem))

  select case(itask)

  case(one) !initialization

     !    lagmult=0
     !    rhscont=0 
     kfl_conta_sld = -1
     uzawa_count = 0
     !    first_it = 1
     neumanncb = 0
     do iboun =1,nboun
        if (xfiel(1)%a(1,iboun) .eq. 5) then 
           pblty=ltypb(iboun) 
           pnodb=nnode(pblty) 
           do inodb = 1,pnodb
              !neumanncb(lnodb(inodb,iboun)*2) = -2000
              neumanncb(lnodb(inodb,iboun)*2) = 0.0
           end do
        end if
     end do

  case(two) !kdtree construction and contact searching

     write(*,*) "searching contact..."
     tag_master = 2
     tag_slave = 1
     npoin_tmp = 0
     do iboun = 1,nboun
        if (xfiel(1)%a(1,iboun) .eq. tag_master) then !master surface
           pblty=ltypb(iboun) 
           pnodb=nnode(pblty) 
           do inodb = 1,pnodb
              npoin_tmp(lnodb(inodb,iboun)) = tag_master
           end do
        else if (xfiel(1)%a(1,iboun) .eq. tag_slave) then !slave surface
           pblty=ltypb(iboun) 
           pnodb=nnode(pblty) 
           do inodb = 1,pnodb
              npoin_tmp(lnodb(inodb,iboun)) = tag_slave
           end do
        end if
     end do

     local2global = 0
     npoib_perset = 0
     do ipoin = 1,npoin
        if (npoin_tmp(ipoin) == 2) then !ipoin belongs to master surface
           npoib_perset = npoib_perset + 1
           local2global(npoib_perset) = ipoin 
        end if
     end do

     npoin_tmp2 = 0
     do ipoin = 1,npoib_perset
        npoin_tmp2(local2global(ipoin)) = ipoin
     end do

     nboun_perset = 0
     do iboun =1,nboun
        if (xfiel(1)%a(1,iboun) .eq. tag_master) then !master surface
           nboun_perset = nboun_perset + 1
           ltypb_perset(nboun_perset) = ltypb(iboun)
           pblty=ltypb(iboun) 
           pnodb=nnode(pblty) 
           do inodb = 1,pnodb
              lnodb_perset(inodb,nboun_perset) = npoin_tmp2(lnodb(inodb,iboun))
           end do
        end if
     end do

     do idime = 1,ndime
        do ipoin = 1,npoib_perset
           coord_perset(idime,ipoin) = coord(idime,local2global(ipoin))+displ(idime,local2global(ipoin),1)
        end do
     end do

     !build kd-tree
     call kdtree_construct(&
          nboun_perset,npoib_perset,lnodb_perset,ltypb_perset,coord_perset,kdtree_perset)

     !!call kdtree(&
     !!  1_ip,mnodb,npoib_perset,nboun_perset,&
     !!  coord_perset,lnodb_perset,ltypb_perset,&
     !!  fabox_loc,bobox_loc,sabox_loc,blink_loc,stru2_loc,ldist_loc,lnele_loc)
     !compute distances with dpopar
     kfl_conta_sld = 0
     chkdi = 1.0e9_rp

     !if(first_it .eq. 1) then
     !  first_it = 0
     penet = 0.0
     !else if(first_it .ne. 1) then
     !  penet = 0.01
     !end if

     do ipoin = 1,npoin
        if(npoin_tmp(ipoin) .eq. tag_slave) then !slave node
           xx(1:ndime) = coord(1:ndime,ipoin)+displ(1:ndime,ipoin,1)
           call kdtree_nearest_boundary(xx,kdtree_perset,iboun,gap(ipoin,1),proje,vec_proje)
           !call dpopar(&
           !     1_ip,(coord(1:ndime,ipoin)+displ(1:ndime,ipoin,1)),npoib_perset,mnodb,&
           !     nboun_perset,chkdi,ltypb_perset,lnodb_perset,&
           !     coord_perset,gap(ipoin,1),vec_proje,proje,iboun) 
           gap(ipoin,1) = -gap(ipoin,1) 
           if (gap(ipoin,1) .gt. penet) then
              kfl_conta_sld = kfl_conta_sld + 1
              do idime = 1,ndime
                 bobox(idime,1) =  1.0e12_rp
                 bobox(idime,2) = -1.0e12_rp
              end do
              pblty=ltypb(iboun) 
              pnodb=nnode(pblty)
              do icont = 1,pnodb
                 do idime = 1,ndime
                    bobox(idime,1) = min(bobox(idime,1),coord(idime,local2global(lnodb_perset(icont,iboun)))+displ(idime,local2global(lnodb_perset(icont,iboun)),1))
                    bobox(idime,2) = max(bobox(idime,2),coord(idime,local2global(lnodb_perset(icont,iboun)))+displ(idime,local2global(lnodb_perset(icont,iboun)),1))
                 end do
              end do
              dmin = 9999999
              do idime = 1,ndime
                 if (abs(bobox(idime,1)-bobox(idime,2)) < dmin) then
                    dmin = abs(bobox(idime,1)-bobox(idime,2))
                    imin = idime
                 end if
              end do

              icont = 0
              !inputs: elcod(1,2),coglo(1): proyecciones
              do idime = 1,ndime
                 if (idime .ne. imin) then
                    icont = icont + 1
                    coglo(icont) = proje(idime)
                    elcod(icont,1) = coord(idime,local2global(lnodb_perset(1,iboun))) + displ(idime,local2global(lnodb_perset(1,iboun)),1)
                    elcod(icont,2) = coord(idime,local2global(lnodb_perset(2,iboun))) + displ(idime,local2global(lnodb_perset(2,iboun)),1)
                 end if
              end do

              !parametric coordinate calculation
              !write(*,*) ipoin,coglo(1),elcod(1,1),elcod(1,2),local2global(lnodb_perset(1,iboun)),local2global(lnodb_perset(2,iboun))
              !! if ( .not. (((coglo(1) .ge. elcod(1,1)) .and. (coglo(1) .le. elcod(1,2))) .or. &
              !!    ((coglo(1) .ge. elcod(1,2)) .and. (coglo(1) .le. elcod(1,1))))) call runend('HAY NODOS CUYA PROYECCION NO PERTENECEN AL ELEMENTO') 

              if (elcod(1,2) .gt. elcod(1,1)) then
                 coloc = (coglo(1) - elcod(1,1))/(elcod(1,2) - elcod(1,1))
              else if (elcod(1,1) .gt. elcod(1,2)) then
                 coloc = 1 - ((coglo(1) - elcod(1,2))/(elcod(1,1) - elcod(1,2)))
              end if

              shapf(1) = 1-coloc
              shapf(2) = coloc
              deriv(1) = -1
              deriv(2) = 1
              tau(1) = vec_proje(2)
              tau(2) = -vec_proje(1)

              contact_info(kfl_conta_sld,1) = ipoin !slave point identification
              write(*,*) 'ipoin',ipoin
              contact_info(kfl_conta_sld,2) = gap(ipoin,1) !gap
              contact_info(kfl_conta_sld,3) = local2global(lnodb_perset(1,iboun)) !node 1 of master surface
              contact_info(kfl_conta_sld,4) = local2global(lnodb_perset(2,iboun)) !node 2 of master surface
              contact_info(kfl_conta_sld,5) = proje(1) !x coordinate of projected point (in master surface)
              contact_info(kfl_conta_sld,6) = proje(2) !y coordinate of projected point (in master surface)
              contact_info(kfl_conta_sld,7) = vec_proje(1) !x component of normal projection
              contact_info(kfl_conta_sld,8) = vec_proje(2) !y component of normal projection
              contact_info(kfl_conta_sld,9) = coloc !parametric coordinate 
              contact_info(kfl_conta_sld,10) = shapf(1) !shape function 1 evaluated at coloc
              contact_info(kfl_conta_sld,11) = shapf(2) !shape function 2 evaluated at coloc
              contact_info(kfl_conta_sld,12) = deriv(1) !derivative of the shape function 1
              contact_info(kfl_conta_sld,13) = deriv(2) !derivative of the shape function 2
              contact_info(kfl_conta_sld,14) = tau(1) !x component of tangent vector
              contact_info(kfl_conta_sld,15) = tau(2) !y component of tangent vector
              contact_info(kfl_conta_sld,16) = 0.5_rp * 5.0e8

           end if
        end if
     end do

     if (kfl_conta_sld .gt. 0) then 
        write(*,*) "contact detected... ",ittim
     else
        write(*,*) "no contact detected... ",ittim
     end if

     !deallocate kd-tree

     call kdtree_deallocate(kdtree_perset)
     !!call kdtree(&
     !!  2_ip,mnodb,npoib_perset,nboun_perset,&
     !!  coord_perset,lnodb_perset,ltypb_perset,&
     !!  fabox_loc,bobox_loc,sabox_loc,blink_loc,stru2_loc,ldist_loc,lnele_loc)

     write(999,*) "***********************" 
     write(999,*) ittim,kfl_conta_sld
     do icont=1,kfl_conta_sld
        write(999,*) contact_info(icont,1),contact_info(icont,2)
     end do

  case(three)

     write(*,*) "applying loads... ",ittim

     do icont = 1,kfl_conta_sld
        N(1) = contact_info(icont,7) !vec_proje(1), x direction of normal projection 
        N(2) = contact_info(icont,8) !vec_proje(2), y direction of normal projection
        N(3) = -contact_info(icont,10)*contact_info(icont,7) !N1*nu_x 
        N(4) = -contact_info(icont,10)*contact_info(icont,8) !N1*nu_y
        N(5) = -contact_info(icont,11)*contact_info(icont,7) !N2*nu_x
        N(6) = -contact_info(icont,11)*contact_info(icont,8) !N2*nu_y

        N1(1) = 0
        N1(2) = 0
        N1(3) = -contact_info(icont,12)*contact_info(icont,7)
        N1(4) = -contact_info(icont,12)*contact_info(icont,8)
        N1(5) = -contact_info(icont,13)*contact_info(icont,7)
        N1(6) = -contact_info(icont,13)*contact_info(icont,8)

        T(1) = contact_info(icont,14)
        T(2) = contact_info(icont,15)
        T(3) = -contact_info(icont,10)*contact_info(icont,14)
        T(4) = -contact_info(icont,10)*contact_info(icont,15)
        T(5) = -contact_info(icont,11)*contact_info(icont,14)
        T(6) = -contact_info(icont,11)*contact_info(icont,15)

        m11 = contact_info(icont,14)*contact_info(icont,14) + contact_info(icont,15)*contact_info(icont,15)

        D1(1) = (1/m11)*(T(1) + contact_info(icont,2)*N1(1))
        D1(2) = (1/m11)*(T(2) + contact_info(icont,2)*N1(2))
        D1(3) = (1/m11)*(T(3) + contact_info(icont,2)*N1(3))
        D1(4) = (1/m11)*(T(4) + contact_info(icont,2)*N1(4))
        D1(5) = (1/m11)*(T(5) + contact_info(icont,2)*N1(5))
        D1(6) = (1/m11)*(T(6) + contact_info(icont,2)*N1(6))

        slave_ipoin = int(contact_info(icont,1))
        local2global_1 = int(contact_info(icont,3))
        local2global_2 = int(contact_info(icont,4))

        !if (uzawa_count .eq. 1) then
        !  lagmult(slave_ipoin) = lagmult_ant(slave_ipoin)
        !else
        !  lagmult(slave_ipoin) = lagmult_ant(slave_ipoin) + isss*contact_info(icont,2)
        !end if

        lagmult(slave_ipoin) = lagmult(slave_ipoin) + contact_info(icont,16)*abs(contact_info(icont,2))

        rhscont((slave_ipoin*2)-1) = lagmult(slave_ipoin)*N(1)
        rhscont(slave_ipoin*2)  = lagmult(slave_ipoin)*N(2)
        rhscont((local2global_1*2)-1) = lagmult(slave_ipoin)*N(3)
        rhscont(local2global_1*2)   = lagmult(slave_ipoin)*N(4)
        rhscont((local2global_2*2)-1) = lagmult(slave_ipoin)*N(5)
        rhscont(local2global_2*2)   = lagmult(slave_ipoin)*N(6)

        !      rhscont(slave_ipoin*2) = rhscont(slave_ipoin*2) + lagmult(slave_ipoin)
        !      rhscont(local2global_1*2) = rhscont(local2global_1*2) + lagmult(slave_ipoin)*(-1)
        !      rhscont(local2global_2*2) = rhscont(local2global_2*2) + lagmult(slave_ipoin)*(-1)

        contelmat = 0
        do jcont = 1,6
           do kcont = 1,6
              contelmat(jcont,kcont) = contelmat(jcont,kcont) + isss*N(jcont)*N(kcont) &
                   + lagmult(slave_ipoin)*((contact_info(icont,2)/m11)*N1(jcont)*N1(kcont) &
                   - D1(jcont)*N1(kcont) - N1(jcont)*D1(kcont))
           end do
        end do

        lnods_tmp(1,1) = slave_ipoin
        lnods_tmp(2,1) = local2global_1
        lnods_tmp(3,1) = local2global_2

        !      call assmat(&
        !                  solve(1)%ndofn,pnode,pevat,solve(1)%nunkn,&
        !                  solve(1)%kfl_algso,ielem,lnods_tmp(1,1),contelmat,amatr)

     end do

  end select

  deallocate(contelmat)
  deallocate(lnods_tmp)
  deallocate(nodmaster)
  deallocate(nodslave)

  deallocate(npoin_tmp)
  deallocate(npoin_tmp2)
  deallocate(local2global)
  deallocate(ltypb_perset)  
  deallocate(lnodb_perset)
  deallocate(coord_perset)
  deallocate(gap)
  deallocate(bobox)

end subroutine sld_defcon
