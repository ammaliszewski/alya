subroutine sld_outset()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_outset
  ! NAME 
  !    sld_outset
  ! DESCRIPTION
  !    Compute and write results on sets:
  ! USES
  ! USED BY
  !    sld_output
  !***
  !----------------------------------------------------------------------- 
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use mod_iofile
  use def_elmtyp 
  use mod_memory, only : memory_alloca
  use mod_memory, only : memory_deallo
  implicit none
  integer(ip)         :: ieset,ibset,inset,iiset,idime,ipoin,ibopo,dummi,iboun
  integer(ip)         :: idofn, kpoin, izone
  real(rp)            :: tveno,ustar,yplus,nu
  real(rp)            :: sumnod_frxid(3)
  real(rp),   pointer :: veaux(:,:)

  nullify(veaux)

  !----------------------------------------------------------------------
  !
  ! Element sets
  !
  !----------------------------------------------------------------------

  if( maxval(postp(1) % npp_setse) > 0 ) then

     if( INOTMASTER ) then
        do ieset = 1,neset
           !call sld_elmset(lesec(ieset),ieset)
        end do
     end if
     call posdef(21_ip,dummi)

  end if

  !----------------------------------------------------------------------
  !
  ! Boundary sets
  !
  !----------------------------------------------------------------------

  if( maxval(postp(1) % npp_setsb) > 0 ) then

     if( INOTMASTER ) then
        do ibset = 1,nbset 
           call sld_bouset(lbsec(ibset),ibset)
        end do
     end if
     call posdef(22_ip,dummi)


  end if

  !----------------------------------------------------------------------
  !
  ! Node sets
  !
  !----------------------------------------------------------------------
  if( maxval(postp(1) % npp_setsn) > 0 ) then

     if( INOTMASTER ) then
        !
        ! Put into a matrix (ndime,nnode) the frxid vector
        !
        call memory_alloca(mem_modul(1:2,modul),'VEAUX','sld_outset',veaux,ndime,npoin)

        izone = lzone(ID_SOLIDZ)
        idofn = 0 
        sumnod_frxid = 0.0_rp 
        do kpoin = 1,npoiz(izone); 
           ipoin = lpoiz(izone) % l(kpoin)
           do idime = 1,ndime
              idofn = idofn + 1
              veaux(idime,ipoin) = -frxid_sld(idofn)
           end do
        end do

        do inset= 1,nnset  
           ipoin=lnsec(inset) 
           if (ipoin/=0) then
              do idime=1,ndime
                 if( postp(1) % npp_setsn(idime) /= 0 ) vnset(idime,inset)= displ(idime,ipoin,1)
              end do
              if( postp(1) % npp_setsn(4) /= 0 ) vnset(4,inset) = treff_sld(ipoin)
              do idime=1,ndime
                 if (postp(1) % npp_setsn(5) /= 0)  vnset(5,inset) = vnset(5,inset) + veaux(idime,ipoin) 
                 if (postp(1) % npp_setsn(6) /= 0)  vnset(6,inset) = vnset(6,inset) + veaux(idime,ipoin)
                 if (postp(1) % npp_setsn(7) /= 0)  vnset(7,inset) = vnset(7,inset) + veaux(idime,ipoin)
                 sumnod_frxid(idime) = sumnod_frxid(idime) + veaux(idime,ipoin)
              end do
           end if
        end do

        call memory_deallo(mem_modul(1:2,modul),'VEAUX','sld_outset',veaux)

        !write(10,*) ittim,sumnod_frxid(1),sumnod_frxid(2),sumnod_frxid(3)

        !1 format(1x,i1,f2.6,f2.6,f2.6)

     end if
     call posdef(23_ip,dummi)
  end if


end subroutine sld_outset
