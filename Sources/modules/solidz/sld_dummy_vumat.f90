subroutine sld_vumat(idum0,&
     idumm1 ,idumm2 ,idumm3,idumm4 ,idumm5 ,idumm6,idumm7 ,&
     rdumm1 ,rdumm2, rdumm3 ,&
     cdumm  ,&
     rdumm4 ,&
     idumm8 ,&
     rdumm5 ,rdumm6,rdumm7 ,rdumm8 ,rdumm9,rdumm10,rdumm11,rdumm12,rdumm13,rdumm14,rdumm15,&
     rdumm16,rdumm17,rdumm18,rdumm19,rdumm20,rdumm21,rdumm22,rdumm23,rdumm24,&
     idumm9)

  use def_kintyp, only       :  ip,rp
  implicit none
  integer(ip) :: idum0, &  
     idumm1 ,idumm2 , idumm3,idumm4 ,idumm5 , idumm6,idumm7 ,idumm8 , idumm9
  real(rp)    :: &
     rdumm1 ,rdumm2,rdumm3 ,rdumm4,rdumm5 ,rdumm6,rdumm7 ,rdumm8 ,rdumm9,&
     rdumm10,rdumm11,rdumm12,rdumm13,rdumm14,rdumm15,rdumm16,rdumm17,rdumm18,&
     rdumm19,rdumm20,rdumm21,rdumm22,rdumm23,rdumm24
  character   :: cdumm
  
  call runend("SLD_VUMAT: NO VUMAT USER DEFINED SUBROUTINE IS LINKED TO ALYA")
  
end subroutine sld_vumat
