!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
    !> @file    sld_outvar.f90
        !> @author  Mariano Vazquez
        !> @date    16/11/1966
        !> @brief   Output for postprocess
        !> @details Output for postprocess
        !> @} 
    !-----------------------------------------------------------------------
subroutine sld_outvar(ivari)
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use def_elmtyp
  !
  use mod_commdom_sld,     only: commdom_sld_outvar, commdom_sld_sigma_dot_n
  use mod_commdom_sld,     only: commdom_sld_nominal_stress_dot_n, commdom_sld_trace_sigma, commdom_sld_n_sigma_n !< 2015JUL17 
  use mod_commdom_driver,  only: commdom_driver_n_fixno
  use mod_commdom_driver, only : commdom_driver_get_total_flux
#ifdef COMMDOM
  use mod_commdom_dynamic, only: commdom_dynamic_outvar
  use mod_commdom_dynamic, only: commdom_dynamic_kdtree
  use mod_commdom_sld,     only: commdom_sld_interior_list
#endif  
  !
  implicit none
  integer(ip), intent(in) :: ivari !> checking variable ivari
  integer(ip)             :: ibopo,ivoig,icont,ipoin,jdime,pgaus,pelty,pnode
  integer(ip)             :: izale,izsld,jpoin,idime,iline,inodf,iface
  integer(ip)             :: ielem,ifacg,inode,ielty,idofn,kpoin,izone,igaus,i
  real(rp)                :: xauxi(2),anpoi,andis,angul,dummr,foref
  real(rp)                :: u(3),dudx(9),d2udx2(27) 
  !
  real(rp), pointer       :: aux(:,:) => null()
  !
  !
  ! Define postprocess variable
  !


  select case (ivari)  

  case(0_ip)

     return

  case(1_ip)
     !
     ! Displacement
     !
     if( INOTMASTER) then
!!$        if( coupling('ALEFOR','SOLIDZ') >= 1 ) then
!!$           call memgen(zero,ndime,npoin)
!!$           if( nncnt > 0 ) then
!!$              izone = lzone(ID_SOLIDZ)
!!$              do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)    
!!$                 do idime = 1,ndime
!!$                    gevec(idime,ipoin)= displ(idime,ipoin,1) 
!!$                 end do
!!$              end do
!!$           else
!!$              !
!!$              ! this case is not very frequent, but it could be... it is alefor-solidz but no nastin.
!!$              ! you must NOT USE contacts here!   
!!$              !
!!$              call memgen(1_ip,npoin,0_ip)
!!$              izale = lzone(ID_ALEFOR)
!!$              do kpoin = 1,npoiz(izale)
!!$                 ipoin = lpoiz(izale) % l(kpoin)
!!$                 gisca(ipoin) = 1
!!$              end do              
!!$              izsld = lzone(ID_SOLIDZ)
!!$              do kpoin = 1,npoiz(izsld)
!!$                 ipoin = lpoiz(izsld) % l(kpoin)
!!$                 gisca(ipoin) = gisca(ipoin) + 1
!!$              end do
!!$              izone = lzone(ID_SOLIDZ)
!!$              do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)    
!!$                 do idime = 1,ndime
!!$                    gevec(idime,ipoin) = displ(idime,ipoin,1) + dispm(idime,ipoin,1) 
!!$                 end do
!!$                 if( gisca(ipoin) == 2 ) then
!!$                    do idime = 1,ndime
!!$                       gevec(idime,ipoin) = displ(idime,ipoin,1)
!!$                    end do
!!$                 end if
!!$              end do
!!$              call memgen(3_ip,npoin,0_ip)
!!$           end if
!!$        else
        gevec => displ(:,:,1) 
!!$        end if
     end if

  case(2_ip)
     !
     ! Displacement velocity
     !
     gevec => veloc_sld(:,:,1) 

  case(3_ip)
     !
     ! Displacement acceleration
     !
     gevec => accel_sld(:,:,1) 

  case(4_ip)
     !
     ! Postprocessing symmetric tensors:
     !
     ! Tensors are stored in gevec with a voigt notation. They are defined as "TENSO" in 
     ! inivar. Finally, the subroutine outvar spread the tensor components in scalars, 
     ! dumped in "...XX", "...YY", "...XY", ...
     !
     !
     ! Stress tensor
     !
     if (INOTMASTER) then
        kfote_sld= kfote_sld + 1
        ! fotens computes caust, green and lepsi, so do it only the first kfote
        if (kfote_sld == 1) then   
           call sld_fotens
        end if

        call memgen(zero,nvoig_sld,npoin)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)    
           do ivoig = 1,nvoig_sld
              gevec(ivoig,ipoin)= caust_sld(ivoig,ipoin) 
           end do
        end do

     end if

  case(5_ip)
     !
     ! Strain tensor
     !
     if (INOTMASTER) then
        kfote_sld= kfote_sld + 1
        if (kfote_sld == 1) then   ! fotens computes caust, green and lepsi, so do it only the first kfote
           call sld_fotens
        end if

        call memgen(zero,nvoig_sld,npoin)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)    
           do ivoig = 1,nvoig_sld
              gevec(ivoig,ipoin)= green_sld(ivoig,ipoin) 
           end do
        end do

     end if

  case(6_ip)
     !
     ! Logarithmic strain tensor
     !
     if (INOTMASTER) then
        kfote_sld= kfote_sld + 1
        if (kfote_sld == 1) then   ! fotens computes caust, green and lepsi, so do it only the first kfote
           call sld_fotens
        end if

        call memgen(zero,nvoig_sld,npoin)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)    
           do ivoig = 1,nvoig_sld
              gevec(ivoig,ipoin)= lepsi_sld(ivoig,ipoin) 
           end do
        end do
     end if

  case(7_ip)
     !
     ! Fibres
     !
     if (kfl_fiber_sld == 0) call runend('SLD_OUTVAR: CANNOT POSTPROCESS A FIBER FIELD WHEN UNDEFINED')
     call sld_fidbes()
     gevec => fibde_sld     

  case(8_ip)
     !
     ! Fibres
     !
     gesca => seqvm_sld   

  case(9_ip)
     !
     ! Fibres
     !
     gesca => seqin_sld    

  case(10_ip)
     !
     ! Fibres on elements
     !
     ger3p => fibeg_sld    

  case(11_ip)
     !
     ! XFEM Displacement
     !
     gevec => dxfem_sld(:,:,1) 

  case(12_ip)
     !
     ! XFEM Velocity
     !
     gevec => vxfem_sld(:,:,1) 

  case(13_ip)
     !
     ! XFEM Acceleration
     !
     gevec => axfem_sld(:,:,1) 

  case(14_ip)
     !
     ! LINEL: Linelets of preconditioner CG
     !
     if( INOTMASTER ) then
        icont=0
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           rhsid(ipoin)=0.0_rp
        end do
        do iline=1,solve(1)%nline
           icont=icont+1
           do ipoin=solve(1)%lline(iline),solve(1)%lline(iline+1)-1
              jpoin=solve(1)%lrenup(ipoin)
              rhsid(jpoin)=real(icont)
           end do
        end do
        gesca => rhsid
     end if

  case(15_ip)
     !
     ! Stress tensor
     !
     if (INOTMASTER) then
        kfote_sld= kfote_sld + 1
        if (kfote_sld == 1) then   ! fotens computes caust, green and lepsi, so do it only the first kfote
           call sld_fotens
        end if
     end if

     gesca => caunn_sld

  case(16_ip)
     !
     ! Rotation on plane XY
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)

           anpoi= 0.0_rp  ! coordinates angle in xy
           if (abs(coord(1,ipoin)) > 0) anpoi= atan(coord(2,ipoin)/coord(1,ipoin))
           andis= 0.0_rp  ! displacement angle in xy
           if (abs(displ(1,ipoin,1)) > 0) anpoi= atan(displ(2,ipoin,1)/displ(1,ipoin,1))
           !!acaaaaaaaaa
           angul= andis - anpoi     ! displacement angle with respect to coord      

           gesca(ipoin)= angul

        end do
     end if
  case(17_ip)
     !
     ! Rotation on plane XZ
     !
     if( INOTMASTER .and. ndime==3) then
        call memgen(zero,npoin,zero)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           xauxi(1)= displ(3,ipoin,1) - rorig_sld(3)
           xauxi(2)= displ(2,ipoin,1) - rorig_sld(2)
           gesca(ipoin)= 0.0_rp
           if (xauxi(1) > 0.0_rp) gesca(ipoin)= atan(xauxi(2)/xauxi(1))
        end do
     end if

  case(18_ip)
     !
     ! Mesh quality
     !
     gesca => quali_sld    

  case(19_ip)
     !
     ! LNENR_sld
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           gesca(ipoin) = real(lnenr_sld(ipoin),rp)
        end do
     end if

  case(20_ip)
     !
     ! Error
     !
     if( INOTMASTER) then
        call memgen(zero,ndime,npoin)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           call sld_exacso(1_ip,coord(1,ipoin),u,dudx,d2udx2,dummr,dummr)
           do idime = 1,ndime
              gevec(idime,ipoin) = displ(idime,ipoin,1)-u(idime)
           end do
        end do
     end if

  case(21_ip)
     !
     ! Cracked nodes
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ifacg = 1,nfacg
           if( lcrkf_sld(ifacg) /= 0 ) then
              iface = lfacg(3,ifacg)
              ielem = lfacg(1,ifacg)
              ielty = ltype(ielem)
              do inodf = 1,nnodf(ielty) % l(iface)
                 inode = lface(ielty) % l(inodf,iface) 
                 ipoin = lnods(inode,ielem)
                 gesca(ipoin) = 1.0_rp
              end do
           end if
        end do
        call pararr('SLX',NPOIN_TYPE,npoin,gesca)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           gesca(ipoin) = min(1.0_rp,gesca(ipoin))
        end do
     end if

  case(22_ip)
     !
     ! Groups
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           gesca(ipoin) = real( solve_sol(1) % lgrou(ipoin) ,rp)
        end do
     end if

  case(23_ip)
     !
     ! Sigma largest eigenvalue
     !
     if (INOTMASTER) then
        kfote_sld= kfote_sld + 1
        if (kfote_sld == 1) then   ! fotens computes caust, green and lepsi, so do it only the first kfote
           call sld_fotens
        end if
     end if

     gesca => sigei_sld

  case(33_ip)
     !
     ! FIXNO_SLD
     !
     if( INOTMASTER) then
        call memgen(zero,ndime,npoin)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)    
           do idime = 1,ndime
              gevec(idime,ipoin) = real(kfl_fixno_sld(idime,ipoin),rp)
           end do
        end do
     end if

  case(34_ip)
     !
     ! Exact force
     !
     if( INOTMASTER) then
        call memgen(zero,ndime,npoin)
        do idofn = 1,npoin*ndime
           rhsid(idofn) = 0.0_rp
        end do
        call sld_elmope(9_ip)
        call pararr('SLX',NPOIN_TYPE,ndime*npoin,rhsid)
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)    
           idofn = (ipoin-1)*ndime
           do idime = 1,ndime
              idofn = idofn + 1
              gevec(idime,ipoin) = rhsid(idofn) / vmass(ipoin)
           end do
        end do
     end if

  case(35_ip)
     !
     ! Fluid force
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        do ipoin = 1,npoin
           if( lnoch(ipoin) ==  NODE_CONTACT_SOLID ) then
              do idime = 1,ndime
                 if( kfl_fixno_sld(idime,ipoin) /= 1 ) then
                    foref = 0.0_rp
                    do jdime = 1,ndime
                       foref = foref + gdepo_sld(idime,jdime,ipoin) * forcf(jdime,ipoin)
                    end do
                    gevec(idime,ipoin) = foref
                 end if
              end do
           end if
        end do
     end if

  case(36_ip)
     !
     ! Element-wise Cauchy stress
     !
     if( INOTMASTER ) then
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           cause_sld(ielem) % a(1:ndime,1:pgaus,1) = 0.0_rp
        end do
        call sld_elmope(10_ip)
        ger3p => cause_sld
     end if

  case(37_ip)
     !
     ! nodal reactions (equilibrium)
     !
     if( INOTMASTER) then
        call memgen(zero,ndime,npoin)
        izone = lzone(ID_SOLIDZ)
        idofn = 0_ip
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           do idime = 1,ndime
              idofn = idofn + 1
              gevec(idime,ipoin) = -frxid_sld(idofn) !opposite sign 
           end do
        end do
     end if

  case(38_ip)
     !
     ! Recovered stresses
     !
     if( INOTMASTER ) then
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           caunp_sld(ielem) % a(1:ndime,1:pgaus,1) = 0.0_rp
        end do
        call sld_elmope(11_ip)
        ger3p => caunp_sld
     end if

  case(39_ip)
     !
     ! Element-wise Cauchy stress according to the local coordinate system        ( *AQU* )
     !
     if( INOTMASTER ) then
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           caulo_sld(ielem) % a(:,:,1) = 0.0_rp
        end do
        call sld_elmope(12_ip)
        ger3p => caulo_sld
     end if

  case ( 40_ip )
     !
     ! Element damage
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pnode = nnode(pelty)
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              if (mgaus <= 1_ip) then
                 gesca(ipoin) = max(gesca(ipoin),real(state_sld(4,1,ielem,1),rp))
              else if (mgaus == pnode) then
                 gesca(ipoin) = max(gesca(ipoin),real(state_sld(4,inode,ielem,1),rp))
              else
                 print*, 'not implemented for mgaus different of 1 or of pnode ', ielem, ' ', mgaus, ' ', pnode
              end if
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 41_ip )
     !
     ! Element damage
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pnode = nnode(pelty)
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              if (mgaus <= 1_ip) then
                 gesca(ipoin) = max(gesca(ipoin),real(state_sld(5,1,ielem,1),rp))
              else if (mgaus == pnode) then
                 gesca(ipoin) = max(gesca(ipoin),real(state_sld(5,inode,ielem,1),rp))
              else
                 print*, 'not implemented for mgaus different of 1 or of pnode ', ielem, ' ', mgaus, ' ', pnode
              end if
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 42_ip )
     !
     ! Element damage
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pnode = nnode(pelty)
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              if (mgaus <= 1_ip) then
                 gesca(ipoin) = max(gesca(ipoin),real(state_sld(6,1,ielem,1),rp))
              else if (mgaus == pnode) then
                 gesca(ipoin) = max(gesca(ipoin),real(state_sld(6,inode,ielem,1),rp))
              else
                 print*, 'not implemented for mgaus different of 1 or of pnode ', ielem, ' ', mgaus, ' ', pnode
              end if
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 43_ip )
     !
     ! Element damage
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pnode = nnode(pelty)
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              if (mgaus <= 1_ip) then
                 gesca(ipoin) = max(gesca(ipoin),real(state_sld(7,1,ielem,1),rp))
              else if (mgaus == pnode) then
                 gesca(ipoin) = max(gesca(ipoin),real(state_sld(7,inode,ielem,1),rp))
              else
                 print*, 'not implemented for mgaus different of 1 or of pnode ', ielem, ' ', mgaus, ' ', pnode
              end if
           end do
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case ( 46_ip ) 
     !
     ! State variables
     !
     if( INOTMASTER ) then
   !     do ielem = 1,nelem
   !        pelty = abs(ltype(ielem))
   !        pgaus = ngaus(pelty)
   !        statt(ielem) % a(1:7,1:pgaus,1:2) = 0.0_rp
   !     end do

   !     do ielem = 1,nelem 
   !        do igaus = 1,pgaus
              !
              ! Compute state variables
              !
   !           do i=1,7
   !              statt(ielem)%a(i,igaus,1) = state_sld(i,igaus,ielem,1)
   !           end do
   !        end do
           ger3p => statt
   !     end do
     end if

  case ( 44_ip )
     call commdom_sld_outvar()   ! RESID 


  case ( 45_ip )
     ! 'NSIGM'
     if( INOTMASTER ) then
        call memgen(0_ip, ndime, npoin)
        call commdom_sld_sigma_dot_n( gevec(1:ndime,1:npoin) ) 
     endif


  case ( 47_ip )
#ifdef COMMDOM
     call commdom_dynamic_outvar( ) ! TOUCH
#else 
     call memgen(0_ip,npoin,0_ip)
     gesca(1:npoin) = -69.69   
#endif

  case ( 48_ip )
     call memgen(0_ip,npoin,0_ip)
     gesca(1:npoin) = 0.0
     call commdom_driver_n_fixno( gesca ) ! NFIXN


  case( 49_ip )
     ! 'TSIGN'
     if( INOTMASTER ) then
        allocate( aux(ndime,npoin) )
        call commdom_sld_sigma_dot_n( aux(1:ndime,1:npoin) )  
        !
        call memgen(0_ip, ndime, npoin)
        do idime = 1,ndime
           call commdom_driver_get_total_flux( aux(idime,1:npoin), gevec(idime,1:npoin) )
        enddo
        !   
        deallocate( aux )
     endif

  case ( 50_ip )
     ! 'DIISO'
     if( INOTMASTER ) then
        gesca => isoch_sld(:,1)
     endif

  case ( 51_ip )
     ! 'VMISO'
     if( INOTMASTER ) then
        gesca => isoch_sld(:,2)
     endif


  case ( 52_ip )
     ! 'TRACE'
     if( INOTMASTER ) then
        call memgen(0_ip, npoin, 0_ip)
        call commdom_sld_trace_sigma( gesca(1:npoin) ) 
     endif


  case ( 53_ip )
     ! 'NOMIN'
     if( INOTMASTER ) then
        call memgen(0_ip, ndime, npoin)
        call commdom_sld_nominal_stress_dot_n( vprop=gevec(1:ndime,1:npoin) )
     endif


  case ( 54_ip )
     ! 'NNSIG'
     if( INOTMASTER ) then
        call memgen(0_ip, npoin, 0_ip)
        call commdom_sld_n_sigma_n( gesca(1:npoin) )
     endif


  case ( 55_ip )
     ! 'NNOMI'
     if( INOTMASTER ) then
        call memgen(0_ip, npoin, 0_ip)
        call commdom_sld_nominal_stress_dot_n( sprop=gesca(1:npoin) )
     endif


  case ( 56_ip )
     ! 'KDTRE'
     if( INOTMASTER ) then
       call memgen(zero, ndime, npoin)
       gevec(1:ndime,1:npoin) = 0.0
#ifdef COMMDOM
       call commdom_dynamic_kdtree( gevec(1:ndime,1:npoin) ) 
#endif  
     endif

  case( 57_ip )
     !
     ! Infinitessimal strain tensor
     !
     if (INOTMASTER) then
       !green_sld = 0.0_rp
       call sld_elmope(13_ip)
       call memgen(0.0_rp, 6_ip, npoin)
       izone = lzone(ID_SOLIDZ)
       do kpoin = 1,npoiz(izone)
         ipoin = lpoiz(izone) % l(kpoin)    
         do ivoig = 1,nvoig_sld
           gevec(ivoig,ipoin) = green_sld(ivoig,ipoin) 
        end do
       end do
     end if

  case( 58_ip )
     !
     ! Infinitesimal stress tensor
     !
     if (INOTMASTER) then
       !caust_sld = 0.0_rp
       call sld_elmope(14_ip)
       call memgen(0.0_rp, 6_ip, npoin)
       izone = lzone(ID_SOLIDZ)
       do kpoin = 1,npoiz(izone)
         ipoin = lpoiz(izone) % l(kpoin)    
         do ivoig = 1,nvoig_sld
           gevec(ivoig,ipoin) = caust_sld(ivoig,ipoin) 
        end do
       end do
     end if

  case( 59_ip )
     ! 'INTLI'
     if( INOTMASTER ) then
        call memgen(0_ip, npoin, 0_ip)
#ifdef COMMDOM
        call commdom_sld_interior_list( gesca(1:npoin) )
#endif  
     endif

  case default

     return

  end select
  !
  ! Postprocess
  !
  call outvar(&
       ivari,&
       ittim,cutim,postp(1) % wopos(1,ivari))

end subroutine sld_outvar

