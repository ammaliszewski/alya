subroutine sld_stabil(&
     pgaus,pmate,pnode,pelty,gpgdi,gprat,gppio,gpvol,gpvel,gpstr,gpcar,gphes,gpsta,&
     shodi,elrhs,elmat)
  !------------------------------------------------------------------------
  !****f* Solidz/sld_stabil
  ! NAME 
  !    sld_stabil
  ! DESCRIPTION
  !    this subroutine computes stabilization terms
  ! USES
  ! USED BY
  !    sld_elmope
  !------------------------------------------------------------------------

  use def_master   ! general global variables
  use def_domain   ! geometry information
  use def_solidz   ! general solidz module information

  implicit none
  integer(ip) :: pgaus,pmate,pnode,pelty,inode,idime,jdime,kdime,igaus,idofn
  real(rp)    :: gppio(ndime,ndime,mgaus)              ! Piola  tensor P
  real(rp)    :: gpstr(ndime,ndime,mgaus)              ! Stress tensor S (evaluated at gauss points)
  real(rp)    :: gpcar(ndime,mnode,mgaus)              ! dNk/dxj
  real(rp)    :: gphes(ntens,mnode,mgaus)              ! dNk/dxidxj
  real(rp)    :: gpsta(ndime,mgaus)                    ! stabilization terms (including shock capturing)
  real(rp)    :: gpgdi(ndime,ndime,mgaus)              ! Gradient of displacement
  real(rp)    :: gprat(ndime,ndime,mgaus)              ! Rate of deformation Fdot at time n
  real(rp)    :: gpvel(ndime,pgaus)                    ! Velocity at the gauss point (first ncomp_nsa)
  real(rp)    :: gpvol(mgaus)                          ! w*|J|
  real(rp),    intent(out)   :: elrhs(ndofn_sld*pnode)
  real(rp)    :: elmat(ndofn_sld*pnode,ndofn_sld*pnode)

  real(rp)    :: pnstr(ndime,ndime,mnode)              ! Stress tensor S (extrapolated from gauss to nodes)
  real(rp)    :: gpgst(ndime,mgaus)                    ! dS_IJ / dx_J
  real(rp)    :: shano,hlele,grno2,vepar,reloc,shodi(ndime,mgaus),volux,adjoi,taupa


  if (kfl_dampi_sld(pmate) == 1) then
     

     ! rayleigh damping terms alpha*M + beta*K
     ! mass matrix term is computed in sld_elmmat
     if (dampi_sld(1,pmate) > 0.0_rp) then
        
        do igaus=1,pgaus
           volux= gpvol(igaus)
           do inode=1,pnode
              shano= elmar(pelty)%shaga(igaus,inode) 
              idofn=(inode-1)*ndime
              do idime=1,ndime
                 idofn=idofn+1
                 elrhs(idofn)= elrhs(idofn)                     &
                      - shano*densi_sld(1,pmate)*volux &
                      * dampi_sld(1,pmate) * gpvel(idime,igaus)        ! mass matrix term of the rayleigh damping
              end do
           end do
        end do

     end if

!     if (dampi_sld(2,pmate) > 0.0_rp) then
!        
!        do igaus=1,pgaus
!           volux= gpvol(igaus)
!           do inode=1,pnode
!              shano= elmar(pelty)%shaga(igaus,inode) 
!              idofn=(inode-1)*ndime
!              do idime=1,ndime
!                 idofn=idofn+1
!                 do jdime=1,ndime
!                    do kdime=1,ndime
!                       elrhs(idofn)= elrhs(idofn) &
!                            - dampi_sld(2,pmate) &
!                            * gpcar(jdime,inode,igaus) &
!                            * gprat(kdime,jdime,igaus) &
!                            * gpstr(jdime,kdime,igaus) &
!                            * volux
!                    end do
!                 end do
!              end do
!           end do
!        end do
!
!     end if

  end if

  return

  !
  !   THIS PART MUST BE REVIEWED, THEN IT IS ALWAYS SKIPPED
  !


  do igaus=1,pgaus
     hlele= gpvol(igaus) ** (1.0_rp/real(ndime))
     do idime=1,ndime
        gpgst(idime,igaus)= 0.0_rp
        do inode=1,pnode
           do jdime=1,ndime
              gpgst(idime,igaus)= gpgst(idime,igaus)+pnstr(idime,jdime,inode)*gpcar(jdime,inode,igaus)
           end do
        end do
        
        grno2= 0.0_rp
        reloc= 0.0_rp
        do jdime=1,ndime
           reloc= reloc + gpgst(jdime,igaus)*gpgdi(jdime,idime,igaus)
           grno2= grno2+gpgdi(jdime,idime,igaus)*gpgdi(jdime,idime,igaus)           
        end do

        reloc= reloc + gpgst(idime,igaus)

! falta el termino con gphes!!!!
        
        !        
        ! discontinuities treatment
        !
        reloc= sqrt(reloc*reloc)
        grno2= sqrt(grno2)

        if((reloc>1.0d-8).and.(grno2>1.0d-8)) then
           vepar=reloc/grno2              
           shodi(idime,igaus)= 0.5_rp*hlele*0.7_rp*vepar
        end if
        
        !        
        ! stabilization (falta!!)
        !
        gpsta(idime,igaus)= 0.0_rp

     end do
     
  end do

  

end subroutine sld_stabil
