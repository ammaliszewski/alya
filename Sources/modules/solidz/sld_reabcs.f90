!-----------------------------------------------------------------------
!> @addtogroup SolidzInput
!> @{
!> @file    sld_reabcs.f90
!> @author  Mariano Vazquez
!> @brief   Read boundary conditions
!> @details Read boundary conditions
!> @} 
!-----------------------------------------------------------------------
subroutine sld_reabcs()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_reabcs
  ! NAME
  !    sld_reabcs
  ! DESCRIPTION
  !    This routine reads the boundary conditions
  ! OUTPUT 
  ! USES
  ! USED BY
  !    sld_turnon
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use mod_memchk
  use mod_opebcs
  use def_solidz
  implicit none
  integer(ip)  :: itloa,ifunc,nauxi,icrak,idime,dummi,ipoin,kzone,imate,izone
  real(rp)     :: rapoi, raboc, vmean, a(3), origp(3), rauxi(2)
  !
  ! Allocate memory
  !
  if( kfl_icodn > 0 ) then
     call opnbcs(0_ip,1_ip,ndime,0_ip,tncod_sld) 
  end if
  if( kfl_icodb > 0 ) then
     call opbbcs(0_ip,1_ip,ndime,tbcod_sld)      
  end if

  !
  ! Initializations
  !
  nfunc_sld     = 0                    ! No time-dependent boundary condition types
  kfl_conbc_sld = 1                    ! Constant boundary conditions
  nbcbo_sld     = 0                    ! No boundary conditions set on boundary elements
  mtloa_sld     = 0
  kfl_cycle_sld = 0                    ! cardiac cycle management is off
  kfl_bodyf_sld = 0                    ! no body forces

  if( INOTSLAVE ) then

     !
     ! Reach section BOUNDARY_CONDITIONS
     !
     call ecoute('sld_reabcs')
     do while(words(1)/='BOUND')
        call ecoute('sld_reabcs')
     end do
     !
     ! Read header
     !
     if(exists('CONST')) then
        kfl_conbc_sld = 1
     else if(exists('TRANSI').or.(exists('NONCO'))) then
        kfl_conbc_sld = 0
     end if
     !
     ! Allocate the prescription time function vector of vectors
     !
     call sld_membcs(3_ip)        

     !
     ! Read data
     !
     call ecoute('sld_reabcs')

     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Physical properties definition
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> BOUNDARY_CONDITIONS, TRANSIENT
     do while(words(1)/='ENDBO')

        if(words(1)=='CODES') then 
 
           if (exists('NODES')) then

              !----------------------------------------------------------
              !
              ! User-defined codes on nodes 
              !
              !----------------------------------------------------------

              tncod => tncod_sld(:)
              call reacod(1_ip)           

           else if( exists('BOUND') ) then
              
              !-------------------------------------------------------------
              !
              ! User-defined codes on boundaries
              !          
              !-------------------------------------------------------------
              
              tbcod => tbcod_sld(1:)
              call reacod(2_ip)

           end if

        ! ADOC[2]> CYCLE: 8xreal                                         $ Simple cardiac cycle managment (AlyaRED) 
        ! ADOC[d]> CYCLE: 
        ! ADOC[d]> <li> CYCLE: tfinf,pfinf,pfins,pfinr,pfirf,C,R1,R2.
        ! ADOC[d]> tfinf=time end inflation; pfinf=pressure end inflation; pfins=pressure end systole; pfinr= pressure end iso relaxation; 
        ! ADOC[d]> pfirf= pressure end rapid fillig; C R1 R2 = windkessel model parameters</li>           
        else if (words(1)=='CYCLE') then
             
              imate = 1
              kfl_cycle_sld = 1
              parcc_sld(1:ncoef_sld,imate)=param(1:ncoef_sld)


        else if( words(1) == 'CRACK' ) then   

           !-------------------------------------------------------------
           !
           ! Cracks
           !          
           !-------------------------------------------------------------
           
           if( exists('NUMBE') ) then
              ncrak_sld = getint('NUMBE',1_ip,'#Number of cracks')
           else
              call runend('SLD_RABCS: NUMBER OF CRACKS MISSING')
           end if
           call ecoute('sld_reabcs')
           call sld_membcs(31_ip)
           if( ndime == 2 ) then
              dummi = 2
           else
              dummi = 4
           end if
           do while( words(1) /= 'ENDCR' )
              if( words(1) == 'CRACK') then
                 icrak = getint('CRACK',1_ip,'#CRACK NUMBER')                           
                 if( icrak > ncrak_sld .or. icrak < 1 ) call runend('SLD_REABCS: WRONG NUMBER OF CRACKS')
                 call ecoute('sld_reabcs')
                 ipoin = 0
                 do while( words(1) /= 'ENDCR' )         
                    ipoin = ipoin + 1
                    if( ipoin > dummi ) call runend('SLD_REABCS: WRONG CRACK DEFINITION')
                    do idime = 1,ndime
                       crkco_sld(idime,ipoin,icrak) = param(idime)
                    end do
                    call ecoute('sld_reabcs')
                 end do
              end if
              call ecoute('sld_reabcs')
           end do
        else if( words(1) == 'BODYF' ) then
           kfl_bodyf_sld = int(param(1))           
           
        else if( words(1) == 'FUNCT' ) then

           !----------------------------------------------------------
           !
           ! Functions
           !
           !----------------------------------------------------------

           call ecoute('sld_reabcs')
           nauxi = 0
           nfunc_sld= 0
           if (words(1)=='TOTAL') nfunc_sld =  int(param(1))
           if (nfunc_sld == 0) then
              call runend('SLD_REABCS: PROVIDE A TOTAL_NUMBER OF FUNCTIONS')
           else if (nfunc_sld > 9) then
              call runend('SLD_REABCS: THE TOTAL_NUMBER OF FUNCTIONS MUST BE LOWER THAN 10')
           end if

           do while(words(1)/='ENDFU')
              if(kfl_conbc_sld==0) then                   ! non-constant bc
                 do while(words(1)/='ENDCO')
                    if (words(1)=='FUNCT') then
                       ifunc=getint('FUNCT',1_ip,'#FUNCTION NUMBER')                       
                       kfl_funty_sld(8,ifunc)= 1        ! default: only apply function to fixity codes 1 
                       if(ifunc<0.or.ifunc>10) then
                          call runend('SLD_REABCS: WRONG FUNCION NUMBER, MUST BE GT.0 AND LT.10')
                       else
                          nauxi = nauxi + 1
                          if (nauxi > nfunc_sld) call runend('SLD_REABCS: MORE FUNCTIONS THAN THE TOTAL_NUMBER ARE GIVEN')
                       end if
                       
                    else if (words(1)=='TIMES') then      ! time shape

                       if(words(2)=='PARAB' .or. words(2)=='LINEA' .or. words(2)=='POLYN') then
                          kfl_funty_sld(1,ifunc)=1
                          if (words(2) == 'LINEA') kfl_funty_sld(1,ifunc)=10 
                          kfl_funty_sld(2,ifunc)=20
                          kfl_funty_sld(3:7,ifunc)=1      ! default: apply to all equations  
                       else if(words(2)=='PERIO') then
                          kfl_funty_sld(1,ifunc)=2
                          kfl_funty_sld(2,ifunc)=20
                          kfl_funty_sld(3:7,ifunc)=1      ! default: apply to all equations  
                       else if(words(2)=='DISCR') then
                          kfl_funty_sld(1,ifunc)=3
                          call ecoute('sld_reabcs')
                          rauxi = 0.0_rp
                          if (words(1) == 'SHAPE') then   ! defining the shape by discrete points
                             rauxi(1)= getrea('REFER', 0.0_rp, 'Reference value, sometimes useful')
                             rauxi(2)= getrea('TSTAR', 0.0_rp, 'Startint time, sometimes useful')
                             call ecoute('sld_reabcs')
                             kfl_funty_sld(2,ifunc)= int(param(1))
                             mtloa_sld(ifunc) = kfl_funty_sld(2,ifunc)
                             itloa = 0

                             call sld_membcs(10_ip + ifunc)  ! allocate the prescription time function vector for ifunc

                             call ecoute('sld_reabcs')

                             do while(words(1)/='ENDSH')
                                itloa= itloa + 1
                                tload_sld(ifunc)%a(ndime+1,itloa)= param(1) - rauxi(2)         ! time
                                tload_sld(ifunc)%a(1:ndime,itloa)= param(2:ndime+1) - rauxi(1) ! prescribed value
                                call ecoute('sld_reabcs')
                             end do
                             
                          end if
                          kfl_funty_sld(3:7,ifunc)=1      ! default: apply to all equations  
                       else
                          kfl_funty_sld(1,ifunc)=0
                       end if

                       !
                       ! OLD WAY: funcrion without an input file                      
                       !
                       if(kfl_funty_sld(1,ifunc)>0) then
                          if(kfl_funty_sld(1,ifunc)==3) then
                       !      ifunp=0
                       !      nfunp=kfl_funty_sld(2,ifunc)/2
                       !      if(nfunp<1) call runend('SOLIDZ: WRONG DISCRETE FUNCTION PARAMETER')
                       !      call ecoute('sld_reabcs')
                            ! do while(words(1)/='ENDFU')
                            !    ifunp=ifunp+1
                            !    if(ifunp>nfunp) call runend('SOLIDZ: WRONG DISCRETE FUNCTION DATA')
                            !    fubcs_sld((ifunp-1)*2+1,ifunc)=param(1)
                            !    fubcs_sld((ifunp-1)*2+2,ifunc)=param(2)
                            !    call ecoute('sld_reabcs')
                            ! end do
                             ! Order the function field
                       !      call ordena(nfunp,fubcs_sld(1,ifunc))
                          else
                             fubcs_sld(1:10,ifunc)=param(2:11)
                          end if
                       end if
                       
                    else if (words(1)=='REFVA') then    ! reference values      
                 
                       fubcs_sld(15,ifunc)=param(1)
                       fubcs_sld(16,ifunc)=param(2)
                       fubcs_sld(17,ifunc)=param(3)
                       fubcs_sld(18,ifunc)=param(4)
                       fubcs_sld(19,ifunc)=param(5)

                    else if (words(1)=='TIMEL') then    ! time lapse

                       fubcs_sld(11,ifunc)   =param(1)  !   start
                       fubcs_sld(12,ifunc)   =param(2)  !   end             
                       fubcs_sld(13,ifunc)   =1.0e10    !   when repeat (default value, very high)
                       rtico_sld( 1,ifunc)    =param(1)  !   initial start time for all eqs.
                       rtico_sld( 2,ifunc)    =param(2)  !   initial final time for all eqs.
                       if (kfl_funty_sld(1,ifunc)==10) then   ! LINEAR: divide the load interval in equispaced steps
                          kfl_funty_sld(1,ifunc)=1
                          fubcs_sld(3:10,ifunc)= 0.0_rp
                          fubcs_sld(2,ifunc)   = 1.0_rp/(fubcs_sld(12,ifunc)-fubcs_sld(11,ifunc))
                       end if

                    else if (words(1)=='TIMER') then    ! time repeat

                       fubcs_sld(13,ifunc)=param(1)     !   when repeat

                    else if (words(1)=='FIXIT') then    ! special behavior controlled by the fixity code

                       if (exists('CONST')) then        
                          kfl_funty_sld(8,ifunc) = 1    ! constrained in the direction with fixity code other than 1
                          call runend('SLD_REABCS: CONSTRAINED IS A DEPRECATED OPTION')
                       else if (exists('UNCON')) then   ! free in the direction with fixity code other than 1
                          kfl_funty_sld(8,ifunc) = 2
                          call runend('SLD_REABCS: UNCONSTRAINED IS A DEPRECATED OPTION')
                       else if (exists('ALLDI')) then   ! apply function to all dimensions
                          kfl_funty_sld(8,ifunc) = 3                          
                          call runend('SLD_REABCS: ALLDIMENSIONS IS A DEPRECATED OPTION')
                       else if (exists('RELEA')) then   ! release constrained fixities when the cycle finishes
                          kfl_funty_sld(8,ifunc) = 4
                       end if

                    end if

                    call ecoute('sld_reabcs')
                 end do
              end if
              call ecoute('sld_reabcs')
           end do !while

        else
           !
           ! classical, iffix  boundary conditions
           !
           if (exists('INITI')) then
              kfl_inifi_sld = 1               ! an initial field is given
              if (exists('PRESS')) then
                 kfl_inifi_sld(2) = 2         ! given initial pressure instead of density
              end if
           end if
        end if

        call ecoute('sld_reabcs')

     end do

     ! give a default size to non-defined tloads
     do ifunc= 1,10
        if (mtloa_sld(ifunc) == 0) then
           mtloa_sld(ifunc) = 1            ! done to allocate a default memory space
           call sld_membcs(10_ip + ifunc)  ! allocate the prescription time function vector for ifunc           
        end if
     end do

  end if
  ! ADOC[0]> END_BOUNDARY_CONDITIONS


!!!!!!! FOR DEBUGING PURPOSES TO CREATE A PARABOLIC PROFILE AT THE INFLOW
!  izone= lzone(ID_SOLIDZ)
!  raboc= 0.8_rp
!  vmean= 1.0_rp
!  origp(1)= 0.0_rp
!  origp(2)= 0.0_rp
!  origp(3)= 0.5445_rp
!
!  izone = lzone(ID_SOLIDZ)
!        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
!     if (lpoiz(izone,ipoin)> 0) cycle
!     ! do this only for zones other than solidz's
!     if (coord(1,ipoin) < 0.0001) then
!        rapoi= sqrt((coord(2,ipoin)-origp(2))*(coord(2,ipoin)-origp(2)) + (coord(3,ipoin)-origp(3))*(coord(3,ipoin)-origp(3)))
!        a(1)= 2.0_rp * vmean * (1.0_rp - rapoi*rapoi/raboc/raboc)
!        a(2)= 0.0
!        a(3)= 0.0
!        write(932, 10) ipoin, a(1:3)
!     end if
!  end do
!10 format (i8,2x,3(e12.5))
!  stop
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


end subroutine sld_reabcs
