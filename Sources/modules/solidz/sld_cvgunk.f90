subroutine sld_cvgunk(itask)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_cvgunk
  ! NAME
  !    sld_cvgunk
  ! DESCRIPTION
  !    This routine performs several convergence checks for the
  !    balance of momentum equation of solidz
  ! USES
  !    sld_endite (itask=1,2)
  !    sld_endste (itask=3)
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip), save       :: kpass = 0
  real(rp),    save       :: cpuit_sld
  real(rp)                :: risld,time1
  real(rp)                :: ejtot, forcv(ndime)

  select case(itask)

  case(1)
     !
     ! Check convergence of the inner iterations:
     ! || d(n,i,j) - d(n,i,j-1)|| / ||d(n,i,j)||
     !
     if( kfl_normc_sld == 3_ip ) then
        risld = rsmax_sld/(fimax_sld+meanf_sld)
        ! risld = rsn2x_sld/fimax_sld
     else if( kfl_normc_sld == 4_ip) then
        risld = solve(1) % resin
     else
        call residu(kfl_normc_sld,ndofn_sld,ndofn_sld,unkno,displ,one,one,ndofn_sld,1.0_rp,risld)
     end if

     if (kfl_prest_sld == 1) then
        risld= 0.0_rp
        call residu(kfl_normc_sld,ndime,ndime,ddisp_sld(1,1,1), ddisp_sld(1,1,2),one,one,ndime,1.0_rp,risld)
     end if

     if (kfl_timet_sld == 1) then         ! Explicit: no internal iterations
        kfl_goite_sld = 0
     else if (kfl_timet_sld == 2) then    ! Impliciti

        if  (itinn(modul) /= 1 .and. & 
             (risld < cotol_sld .or. itinn(modul) >= miinn_sld)) kfl_goite_sld = 0
        ! if  (itinn(modul) >= miinn_sld) then
        !     write(lun_livei,*)''
        !     write(lun_livei,'(A52)')'--| ALYA     NEWTON ITERATION EXCEEDS MAXIMUM NUMBER' 
        ! end if
        !if ((risld<cotol_sld.or.itinn(modul)>=miinn_sld)) kfl_goite_sld = 0
     end if
     !
     ! Sum up slaves contribution for volum_sld and tactv_sld
     !
     call pararr('SUM',0_ip,2_ip,volum_sld)
     call pararr('SUM',0_ip,1_ip,tactv_sld)

     if( INOTSLAVE ) then
        !
        ! Write convergence
        !
        call cputim(time1)
        if( kpass == 1 ) then
           time1 = time1 - cpuit_sld
        else
           time1 = time1 - cpu_initi
        end if

        if( kpass == 0 .and. kfl_rstar /= 2 ) write(momod(modul) % lun_conve,100)
        ! solid body volume
        if( volum_sld(1) /= 0.0_rp ) then
           volum_sld(2) = volum_sld(2) / volum_sld(1)   ! write the ratio        
        else 
           volum_sld(2) = 0.0_rp
        end if

        !        if (nfunc_sld > 0) then
        !           call sld_funbou(1_ip,-1_ip,forcv)
        !           ! pressure is negative when it is against the wall, 
        !           ! so the pressure is computed with the opposite sign
        !           ptota_sld(1) = - forcv(1)
        !           ptota_sld(2) = - forcv(1)
        !        end if

        ejrat_sld(1) = 0.0_rp
        ejrat_sld(2) = 0.0_rp
        ejtot        = 0.0_rp
        if (kfl_volca_sld > 0 .and. ittim > 1) then
           ! cavity volumes for the ejection rate
           ejrat_sld(1) = volst_sld(2,1)/volvr_sld(1) * 100.0_rp
           ejrat_sld(mcavi_sld) = volst_sld(2,mcavi_sld)/volvr_sld(mcavi_sld) * 100.0_rp
           ejtot = (volst_sld(2,mcavi_sld)+volst_sld(2,mcavi_sld)) / (volvr_sld(1)+volvr_sld(mcavi_sld)) * 100.0_rp
        end if
        
        


        write(momod(modul) % lun_conve,101) &
             ittim,itcou,itinn(modul),cutim,risld,volum_sld(1),volum_sld(2),1.0_rp/dtinv_sld,time1,&
             ejrat_sld(1),ejrat_sld(2),ejtot,&
             ptota_sld(1),ptota_sld(2),real(kfase_sld),resti_sld,volst_sld(2,1),volst_sld(2,2),&
             tactv_sld/volum_sld
        call cputim(cpuit_sld)
        call flush(momod(modul) % lun_conve)
     end if

     kpass = 1

  case(2)
     !
     ! Check convergence of the outer iterations:
     ! || d(n,i,*) - d(n,i-1,*)|| / ||d(n,i,*)||
     !
     call residu(kfl_normc_sld,ndime,ndime,displ(1,1,1),displ(1,1,2),one,one,ndime,1.0_rp,resid_sld)

  case(3)
     !
     ! Check residual of the time iterations:
     ! || d(n,*,*) - d(n-1,*,*)|| / ||d(n,*,*)||
     !

     call residu(kfl_normc_sld,ndime,ndime,displ(1,1,1),displ(1,1,3),one,one,ndime,1.0_rp,resti_sld)

     if( resti_sld <= sstol_sld ) then
        kfl_stead_sld = 1
        call outfor(28_ip,momod(modul) % lun_outpu,' ')
     end if

  end select
  !
  ! Formats
  !
100 format('# --| ALYA Convergence'  ,/,&
       &   '# --| Columns displayed:' ,/,&
       &   '# --| 1. Time step         2. Global Iteration   3. Inner Iteration   ' ,/,&
       &   '# --| 4. Current time      5. Displacement       6. Reference volume  ' ,/,&
       &   '# --| 7. Defo/Ref Volume   8. Critical dt        9. CPU time          ' ,/,&
       &   '# --| 10. Ejection Rate 1 11. Ejection Rate 2   12. Total Ejection Rate ' ,/,&
       &   '# --| 13. Pres. Cavity 1  14. Pres. Cavity 2    15. Cycle ID    ',/,'#',/,&
       &   '# --| 16. Time residual   17. Vol. Cavity 1     18. Vol. Cavity 2    ',/,'#',/,&
       &   '# --| 19. Fiber active tension  ',/,'#',/,&
       &   '# ','          1','          2','          3',&
       &        '               4','               5','               6','               7',&
       &        '               8','               9','              10','              11',&
       &        '              12','              13','              14','              15',&
       &        '              16','              17','              18','              19')
101 format(4x,i9,2x,i9,2x,i9,20(2x,e14.6),2x,i9,20(2x,e14.6))

end subroutine sld_cvgunk


