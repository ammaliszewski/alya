subroutine sld_matrix(itask)
  !-----------------------------------------------------------------------
  !****f* solidz/sld_matrix
  ! NAME
  !    sld_matrix
  ! DESCRIPTION
  !    This routine computes the matrix and right hand side
  ! USES
  !    sld_elmope
  !    sld_bouope
  !    sld_assloa
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solver
  use def_solidz
  use def_domain

  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: izrhs,ipoin,izone,idime,jdime,itott
  integer(ip)             :: kpoin,ifpoi,ipoin_solid,incnt,idofn
  integer(ip)             :: icont
  real(rp)                :: time1,time2

  if( INOTMASTER ) then
     !
     ! Initialize solver
     !
     if (itask == 1) then        ! explicit

        do izrhs=1,nzrhs_sld
           rhsid(izrhs)=0.0_rp
        end do

        call inisol()
        

     else if(itask==2) then      ! implicit

        call inisol()

     end if
     !
     ! Initialize push back
     !
     if(kfl_gdepo_sld /= 0 ) call veczer(ndime*ndime*npoin,gdepo_sld,0.0_rp)
  end if

  call cputim(time1)
  !
  ! Element assembly
  !
  call sld_elmope(itask)

  if( INOTMASTER ) then
     !
     ! Boundary assembly
     !
     call sld_bouope(itask)
     !
     ! Cohesive laws
     !
     if (kfl_cohes_sld > 0) call sld_cohesi(itask)

     if (kfl_cohes_sld > 0 .and. kfl_elcoh > 0) call sld_cohele(itask)
     !
     ! Solve push forward (slaves)
     !
     if( kfl_gdepo_sld /= 0 .and. INOTMASTER ) then
        call rhsmod(ndime*ndime,gdepo_sld)
        do ipoin = 1,npoin
           do idime = 1,ndime
              do jdime = 1,ndime
                 gdepo_sld(jdime,idime,ipoin) = gdepo_sld(jdime,idime,ipoin) / vmass(ipoin)
              end do
           end do
        end do
     end if
     !
     ! Coupling
     !
     call sld_coupli(ITASK_MATRIX)
     !
     ! Limiter
     !
     if (kfl_limit_sld==1) call sld_limite()
     !
     ! Add external volume forces 
     !
     if( kfl_vofor_sld > 0 ) then
        !
        ! Internal nodes
        !
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone)
           ipoin = lpoiz(izone) % l(kpoin)
           if( ipoin <= npoi1 .or. ( ipoin >= npoi2 .and. ipoin <= npoi3 ) ) then
              itott = (ipoin-1) * ndofn_sld
              do idime = 1,ndime
                 itott = itott + 1
                 rhsid(itott) = rhsid(itott) + vofor_sld(idime,ipoin)
              end do
           end if
        end do

     end if

     !     write(6,*) 'ooooooo'
     !     write(6,*) 'ooooooo'
     !     pimod= pipix(1)*pipix(1) + pipix(2)*pipix(2)
     !     write(6,*) 'ooooooo', pipix,pimod     
     !     pimod= pipix(1)*pipox(1) + pipox(2)*pipox(2)
     !     write(6,*) 'ooooooo', pipox,pimod


     !contact contribution to rhsid matrix
     do icont = 1,(npoin*ndime)
        rhsid(icont) = rhsid(icont) + rhscont(icont) + neumanncb(icont)
     end do

  end if

  call cputim(time2) 
  cpu_modul(CPU_ASSEMBLY,modul) = cpu_modul(CPU_ASSEMBLY,modul) + time2 - time1

100 format (5(F16.8,','))  
end subroutine sld_matrix
