subroutine sld_trate2(irotype,tenold,baslo,tennew,ndime)

   !----------------------------------------------------------------------------
   !> @addtogroup Solidz
   !> @{
   !> @file    sld_trate2.f90
   !> @author  Adria luintanas (u1902492@campus.udg.edu)
   !> @date    Desember, 2014
   !> @brief   This subroutine performs a transformation of a tensor from a old
   !> @        basis to a new basis. 
   !> @details This subroutine performs a transformation of a tensor
   !> @           S'_{ij} = R_{ik}*R_{jl}*S _{kl}
   !> @           S _{ij} = R_{ki}*R_{lj}*S'_{kl}
   !> @} 
   !----------------------------------------------------------------------------

    
    use      def_kintyp  
    
    implicit none
    
    integer(ip),    intent(in)     ::    irotype,ndime
    real(rp),       intent(in)     ::    baslo(ndime,ndime), tenold(ndime,ndime)
    real(rp),       intent(out)    ::    tennew(ndime,ndime)
    integer(ip)                    ::    i,j,k,l
    
    ! -------------------------------
    ! INITIALIZE VARIABLES
    !
    tennew = 0.0_rp
    
    
    ! -------------------------------
    ! ROTATING THE TENSOR IN FUNCTION OF THE BASIS
    !
    
    if ( irotype == 1_ip ) then
       !
       ! From LCSYS to GCSYS
       !    S _{ijkl} = R_{ki}*R_{lj}*R_{rk}*R_{sl}*S'_{mn}
       !
    
       do i = 1, ndime
          do j = 1, ndime
             do k = 1, ndime
                do l = 1, ndime
                   tennew(i, j) = tennew(i, j) + baslo(k, i)*baslo(l, j)*tenold(k, l)
                end do
             end do
          end do
       end do
		

    else if ( irotype == 2_ip ) then
       !
       ! From GCSYS to LGCSYS
       !    S'_{ijkl} = R_{ik}*R_{jl}*R_{kr}*R_{ls}*S _{kl}

       do i = 1, ndime
          do j = 1, ndime
             do k = 1, ndime
                do l = 1, ndime
                   tennew(i, j) = tennew(i, j) + baslo(i, k)*baslo(j, l)*tenold(k, l)
                end do
             end do
          end do
       end do

    end if

end subroutine sld_trate2
