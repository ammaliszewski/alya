!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_memall.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   General memory allocation 
!> @details General memory allocation 
!> @} 
subroutine sld_memall()
  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use def_solver
  use mod_memchk
  use mod_memory
  use def_solidz
  implicit none
  integer(ip) :: lodof,ielem,pgaus,pelty,ncstr,ngdef,ntaul,pnode,nqual
  integer(ip) :: ncouv,npiol,idime,igaus,ivari,imate,npoin_roloc
  integer(4)  :: istat

  !
  ! Slave time counters for the module  (TO BE FINISHED)
  !
  nullify(cpual_master_sld)
  allocate(cpual_slave_sld(2),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'CPUAL_SLAVE_SLD','sld_memall',cpual_slave_sld)     
  if (IMASTER) then
     allocate(cpual_master_sld(0:20))
     cpual_master_sld = 0.0_rp
  end if

  if( INOTMASTER ) then
     !
     ! Allocate memory for velocity and pressure: VELOC, PRESS
     ! displ(:,:,1)=u^{n,i}
     ! displ(:,:,2)=u^{n,i-1}
     ! displ(:,:,3)=u^{n-1}
     ! displ(:,:,4)=u^{n-2}
     ! displ(:,:,5)=u^{n-3}, etc.
     !
     allocate(displ(ndime,npoin,ncomp_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DISPL','sld_memall',displ)
     allocate(press(npoin,ncomp_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'PRESS','sld_memall',press)
     allocate(gpfib(ndime,mgaus,nelem),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'GPFIB','sld_memall',gpfib)     
! **********************************
     allocate(forc_fsi(ndime,npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'FORFI','sld_memall',forc_fsi)
! **********************************          

      allocate( aux_sld(ndime,npoin,2)  )
      aux_sld(1:ndime,1:npoin,1:2) = 0.0 


     !
     ! COUPLING
     !
     ngdef= 1
     ntaul= 1
     ncouv= 1
     if( coupling('SOLIDZ','NASTIN') >= 1 ) then
        ! the mesh velocity is the solid deformation velocity
        !velom => veloc_sld(:,:,1)
        allocate(ddism_sld(ndime,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DDISM_SLD','sld_memall',ddism_sld)
     end if
     if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
        ngdef= nelem
        ntaul= npoin
        do imate= 1,nmate_sld
           !only one variable coupling is enough to allocate covar_sld
           if (kfl_coupt_sld(imate) == 11) ncouv= nelem  
        end do
     else
        if (kfl_modul(ID_EXMEDI) == 1) &
             call runend('SLD_MEMALL: COUPLING WITH EXMEDI NOW IN THE KER.DAT FILE!! CHECK DOCS.')
     end if

     npiol= 1
     if (kfl_linea_sld == 2) then   ! newton methods
        npiol= nelem
     end if
     !
     ! Deformation gradient inverse
     allocate(bridg_sld(4,mgaus,ngdef),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'BRIDG_SLD','sld_memall',bridg_sld)
     allocate(kacti_sld(ntaul),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KACTI_SLD','sld_memall',kacti_sld)
     allocate(covar_sld(ncouv),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'COVAR_SLD','sld_memall',covar_sld)

     bridg_sld= 0.0_rp
     bridg_sld(1,1:mgaus,1:ngdef)= 1.0_rp
     covar_sld(1:ncouv) = 1.0_rp

     !allocate(du_sld(ndime,npoin),stat=istat)
     !call memchk(zero,istat,mem_modul(1:2,modul),'VELOC_SLD','sld_memall',du_sld)

     !
     ! Displacement velocity and acceleration
     !
     allocate(veloc_sld(ndime,npoin,ncomp_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VELOC_SLD','sld_memall',veloc_sld)
     if( coupling('SOLIDZ','NASTIN') >= 1 ) then
        ! the mesh velocity is the solid deformation velocity
        !!!!!velom => veloc_sld(:,:,1)
     end if
     allocate(accel_sld(ndime,npoin,ncomp_sld),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'ACCEL_SLD','sld_memall',accel_sld)
     !
     ! Quality of the deformed mesh
     !
     nqual= 1
     if (kfl_quali_sld == 1) nqual= nelem
     allocate(quali_sld(nqual),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'QUALI_SLD','sld_memall',quali_sld)
     quali_sld = 0.0_rp
     if( kfl_gdepo_sld /= 0 ) then
        allocate(gdepo_sld(ndime,ndime,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'GDEPO_SLD','sld_memall',gdepo_sld)        
     end if
     !
     ! Displacement correction for Newton--Raphson
     !
     allocate(dunkn_sld(ndofn_sld*npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DUNKN_SLD','sld_memall',dunkn_sld)

     allocate(vmass_sld(npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VMASS_SLD','sld_memall',vmass_sld)

     !
     ! Displacement enrichment function for XFEM / GFEM
     !
     if (kfl_xfeme_sld > 0) then
        allocate(dxfem_sld(ndime,npoin,ncomp_sld),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DXFEM_SLD','sld_memall',dxfem_sld)
        allocate(vxfem_sld(ndime,npoin,ncomp_sld),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'VXFEM_SLD','sld_memall',vxfem_sld)
        allocate(axfem_sld(ndime,npoin,ncomp_sld),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'AXFEM_SLD','sld_memall',axfem_sld)
        allocate(crapx_sld(ndime,nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CRAPX_SLD','sld_memall',crapx_sld)
        allocate(cranx_sld(ndime,nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CRANX_SLD','sld_memall',cranx_sld)
        allocate(sgmax_sld(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'SGMAX_SLD','sld_memall',sgmax_sld)

        allocate(eleng_sld(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'ELENG_SLD','sld_memall',eleng_sld)

        allocate(leenr_sld(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LEENR_SLD','sld_memall',leenr_sld)
        allocate(lnenr_sld(npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LNENR_SLD','sld_memall',lnenr_sld)
        allocate(lecoh_sld(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LECOH_SLD','sld_memall',lecoh_sld)
 
        if (kfl_timet_sld == 1) then
           allocate(vmasx_sld(npoin),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'VMASX_SLD','sld_memall',vmasx_sld)
        end if

     else if (kfl_xfeme_sld == 0) then
        allocate(dxfem_sld(1,    1,ncomp_sld),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DXFEM_SLD','sld_memall',dxfem_sld)
        allocate(vxfem_sld(1,    1,ncomp_sld),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'VXFEM_SLD','sld_memall',vxfem_sld)
        allocate(axfem_sld(1,    1,ncomp_sld),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'AXFEM_SLD','sld_memall',axfem_sld)
        allocate(crapx_sld(1,    1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CRAPX_SLD','sld_memall',crapx_sld)
        allocate(cranx_sld(1,    1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CRANX_SLD','sld_memall',cranx_sld)
        allocate(sgmax_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'SGMAX_SLD','sld_memall',sgmax_sld)

        allocate(eleng_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'ELENG_SLD','sld_memall',eleng_sld)

        allocate(leenr_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LEENR_SLD','sld_memall',leenr_sld)
        allocate(lnenr_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LNENR_SLD','sld_memall',lnenr_sld)
        allocate(lecoh_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LECOH_SLD','sld_memall',lecoh_sld)
        
     end if
     !
     ! Postprocess of element wise Cauchy stress
     !
     ivari = 36
     call posdef(25_ip,ivari) 
     if( ivari /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'CAUSE_SLD','sld_memall',cause_sld,nelem)
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           call memory_alloca(mem_modul(1:2,modul),'CAUSE_SLD(IELEM)','sld_memall',cause_sld(ielem)%a,ndime,pgaus,1_ip)
        end do     
     end if
     !
     ! Cohesive law and friction/contact
     !
     if (kfl_cohes_sld > 0) then
        allocate(dfric_sld(nelem,2,mgaus,2),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DFRIC_SLD','sld_memall',dfric_sld)
        allocate(dslip_sld(nelem,2,mgaus,2),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DSLIP_SLD','sld_memall',dslip_sld)
        allocate(dceff_sld(nelem,2,mgaus,2),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DCEFF_SLD','sld_memall',dceff_sld)
        allocate(dcmax_sld(nelem,2,mgaus),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DCMAX_SLD','sld_memall',dcmax_sld)
        allocate(nopio_sld(ndime*ndime,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'NOPIO_SLD','sld_memall',nopio_sld)
        allocate(lecoh_sld(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LECOH_SLD','sld_memall',lecoh_sld)
        allocate(treff_sld(npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'TREFF_SLD','sld_memall',treff_sld)
        
     else if (kfl_cohes_sld == 0) then
        allocate(dfric_sld(1,1,1,2),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DFRIC_SLD','sld_memall',dfric_sld)
        allocate(dslip_sld(1,1,1,2),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DSLIP_SLD','sld_memall',dslip_sld)
        allocate(dceff_sld(1,1,1,2),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DCEFF_SLD','sld_memall',dceff_sld)
        allocate(dcmax_sld(1,1,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DCMAX_SLD','sld_memall',dcmax_sld)
        allocate(nopio_sld(1,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'NOPIO_SLD','sld_memall',nopio_sld)
        allocate(lecoh_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LECOH_SLD','sld_memall',lecoh_sld)
        allocate(treff_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'TREFF_SLD','sld_memall',treff_sld)

     end if


     ! 
     ! Cohesive elements
     !
     if (kfl_elcoh > 0 ) then
       allocate(nocoh_sld(npoin),stat=istat)
       call memchk(zero,istat,mem_modul(1:2,modul),'NOCOH_SLD','sld_memall',nocoh_sld) 
       allocate(cohnx_sld(ndime,nelem),stat=istat)
       call memchk(zero,istat,mem_modul(1:2,modul),'COHNX_SLD','sld_memall',cohnx_sld)
     else
       allocate(nocoh_sld(1),stat=istat)
       call memchk(zero,istat,mem_modul(1:2,modul),'NOCOH_SLD','sld_memall',nocoh_sld) 
       allocate(cohnx_sld(1,1),stat=istat)
       call memchk(zero,istat,mem_modul(1:2,modul),'COHNX_SLD','sld_memall',cohnx_sld) 
     end if


     !
     ! Internal variables for the damage model
     !
     if (kfl_intva_sld > 0) then
        allocate(vinte_sld(nvint_sld,mgaus,nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'VINTE_SLD','sld_memall',vinte_sld)
     else if (kfl_intva_sld == 0) then
        allocate(vinte_sld(1,1,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'VINTE_SLD','sld_memall',vinte_sld)
     end if

     !
     ! Damage model
     !
     if (kfl_damag_sld > 0) then
        allocate(ledam_sld(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LEDAM_SLD','sld_memall',ledam_sld)
     else if (kfl_damag_sld == 0) then
        allocate(ledam_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LEDAM_SLD','sld_memall',ledam_sld)
     end if

     !
     ! Arc-length parameters
     !
     if (kfl_arcle_sld > 0) then
        allocate(lambda_sld(3),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LAMBDA_SLD','sld_memall',lambda_sld)
     else if (kfl_arcle_sld == 0) then
        allocate(lambda_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LAMBDA_SLD','sld_memall',lambda_sld)
     end if

     !
     ! Allocate memory for stress (1st PK tensor)
     !
     allocate(gppio_sld(nelem),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'GPPIO_SLD','sld_memall',gppio_sld)
     if (kfl_xfeme_sld > 0) then
        do ielem = 1,nelem
           allocate(gppio_sld(ielem)%a(ndime,ndime,mgaus),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'GPPIO_SLD','sld_memall',gppio_sld(ielem)%a)
        end do
     else
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           allocate(gppio_sld(ielem)%a(ndime,ndime,pgaus),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'GPPIO_SLD','sld_memall',gppio_sld(ielem)%a)
        end do
     end if
     
     !
     ! Allocate memory for F (F1 stocked in gpgdi_sld)
     !
     allocate(gpgdi_sld(nelem),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'GPGDI_SLD','sld_memall',gpgdi_sld)
     if (kfl_xfeme_sld > 0) then
        do ielem = 1,nelem
           allocate(gpgdi_sld(ielem)%a(ndime,ndime,mgaus),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'GPGDI_SLD','sld_memall',gpgdi_sld(ielem)%a)
        end do
     else
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           allocate(gpgdi_sld(ielem)%a(ndime,ndime,pgaus),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'GPGDI_SLD','sld_memall',gpgdi_sld(ielem)%a)
        end do
     end if
     !
     ! Allocate memory for detF 
     !
     allocate(dedef_sld(nelem),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DEDEF_SLD','sld_memall',dedef_sld)
     if (kfl_xfeme_sld > 0) then
        do ielem = 1,nelem
           allocate(dedef_sld(ielem)%a(mgaus),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'DEDEF_SLD','sld_memall',dedef_sld(ielem)%a)
        end do
     else
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           allocate(dedef_sld(ielem)%a(pgaus),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'DEDEF_SLD','sld_memall',dedef_sld(ielem)%a)
        end do
     end if
     !
     ! Dimensions required for the solver
     !
     lodof     = ndofn_sld
     nusol     = max(nusol,1)
     mxdof     = max(mxdof,lodof)
     nusol     = 1
     nzmat_sld = 0
     nzrhs_sld = ndime*npoin
     if (kfl_xfeme_sld == 1) nzrhs_sld = nzrhs_sld + ndime*npoin
     if (kfl_arcle_sld == 1) nzrhs_sld = nzrhs_sld + 1_ip
     
     if(kfl_timet_sld==2) then
        if (kfl_xfeme_sld == 1) then
           solve_sol => solve(1:2) ! OJO CAMBIAR
           call soldef(4_ip)
        else
           solve_sol => solve(1:2) ! OJO CAMBIAR
           call soldef(4_ip)
        end if
     end if
     !
     ! Initialized F to [1]
     !
     if (kfl_xfeme_sld > 0) then
        do ielem = 1,nelem
           do idime=1,ndime
              do igaus=1,mgaus
                 gpgdi_sld(ielem)%a(idime,idime,igaus)=1.0_rp
              end do
           end do
        end do
     else
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           do idime=1,ndime
              do igaus=1,pgaus
                 gpgdi_sld(ielem)%a(idime,idime,igaus)=1.0_rp
              end do
           end do
        end do
     end if

     !
     ! Cauchy stress and Green strain: CAUST_SLD, GREEN_SLD, LSFSN_SLD, SEQVM_SLD, SEQIN_SLD' (for postprocessing stage)
     !
     if (kfl_foten_sld == 1) then
        allocate(caust_sld(nvoig_sld,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CAUST_SLD','sld_memall',caust_sld)
        allocate(caunn_sld(npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CAUNN_SLD','sld_memall',caunn_sld)
        allocate(green_sld(nvoig_sld,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'GREEN_SLD','sld_memall',green_sld)
        allocate(lepsi_sld(nvoig_sld,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LSFSN_SLD','sld_memall',lepsi_sld)
        allocate(seqvm_sld(npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'SEQVM_SLD','sld_memall',seqvm_sld)
     else
        allocate(caust_sld(nvoig_sld,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CAUST_SLD','sld_memall',caust_sld)
        allocate(caunn_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CAUNN_SLD','sld_memall',caunn_sld)
        allocate(green_sld(nvoig_sld,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'GREEN_SLD','sld_memall',green_sld)
        allocate(lepsi_sld(nvoig_sld,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LSFSN_SLD','sld_memall',lepsi_sld)        
        allocate(seqvm_sld(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'SEQVM_SLD','sld_memall',seqvm_sld)
     end if

     if (kfl_restr_sld == 0) then
        ! No restr field read
        allocate(restr_sld(6,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'RESTR_SLD','sld_memall',restr_sld)        
     end if
     !restr_sld = 0.0_rp

     if (kfl_prest_sld == 1) then
        allocate(ddisp_sld(ndime,npoin,1:2),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DDISP_SLD','sld_memall',ddisp_sld)
     end if 

     if (kfl_fiber_sld > 0) then
        allocate(fibde_sld(ndime,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'FIBDE_SLD','sld_memall',fibde_sld)
        if (kfl_fiber_sld == 3) then
           !
           ! sheet and normal fields to be computed, not read
           !
           allocate(fibts_sld(ndime,npoin),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'FIBTS_SLD','sld_memall',fibts_sld)
           allocate(fibtn_sld(ndime,npoin),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'FIBTN_SLD','sld_memall',fibtn_sld)           
        end if
     else
        allocate(fibde_sld(ndime,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'FIBDE_SLD','sld_memall',fibde_sld)
     end if

     allocate(seqin_sld(npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'SEQIN_SLD','sld_memall',seqin_sld)
!!     npoin_roloc = 1
!!     if (kfl_rotei_sld == 1) npoin_roloc = npoin     
     npoin_roloc = npoin     
     allocate(sigei_sld(npoin_roloc),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'SIGEI_SLD','sld_memall',sigei_sld)
     allocate(roloc_sld(ndime,ndime,npoin_roloc),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'ROLOC_SLD','sld_memall',roloc_sld)

     !
     ! Elemental fibers
     !
     ivari = 10
     call posdef(25_ip,ivari)
     if( ivari /= 0 ) then
        allocate(fibeg_sld(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'FIBEG_SLD','sld_memall',fibeg_sld)
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           allocate(fibeg_sld(ielem)%a(ndime,pgaus,1),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'FIBEG_SLD','sld_memall',fibeg_sld(ielem)%a)
        end do
     end if
     !
     ! Allocate memory for global reaction forces
     !
     allocate(frxid_sld(ndofn_sld*npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'FRXID_SLD','sld_memall',frxid_sld)

     !
     ! Actualize maximum sizes of matrix and RHS
     !
     nzmat=max(nzmat,nzmat_sld)
     nzrhs=max(nzrhs,nzrhs_sld)
 
    !
    ! CAUCHY STRESS TENSOR 
    !   The values are not average at nodes so they are stored for each 
    !   element.
    ivari = 38
    call posdef(25_ip,ivari) 
    if( ivari /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'CAUNP_SLD','sld_memall',caunp_sld,nelem)
        do ielem = 1,nelem
            pelty = abs(ltype(ielem))
            pgaus = ngaus(pelty)
            call memory_alloca(mem_modul(1:2,modul),'CAUNP_SLD(IELEM)','sld_memall',caunp_sld(ielem)%a,nvoig_sld,pgaus,1_ip)
        end do
    end if
 
    !
    ! INTERNAL STATE VARIABLES                                                    ( *AQU* )
    !	 a vector of 8 variables for each gauss point of each element
    !
    !allocate(state_sld(nstat_sld, mgaus, nelem, 2), stat = istat)
    !call memchk(zero, istat, mem_modul(1:2,modul), 'STATE_SLD', 'sld_memall', state_sld)

    !
    ! LOCAL CAUCHY STRESS TENSOR                                                  ( *AQU* )
    !   Local stress tensor according to the global coordinate system. 
    !   The values are not average at nodes so they are stored for each 
    !   element.
    ivari = 39
    call posdef(25_ip,ivari) 
    if( ivari /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'CAULO_SLD','sld_memall',caulo_sld,nelem)
        do ielem = 1,nelem
            pelty = abs(ltype(ielem))
            pgaus = ngaus(pelty)
            call memory_alloca(mem_modul(1:2,modul),'CAULO_SLD(IELEM)','sld_memall',caulo_sld(ielem)%a,nvoig_sld,pgaus,1_ip)
        end do
    end if

    ! 
    ! INTERNAL STATT VARIABLES FOR POSTPROCESS
    !   State variables. There are 7 and they are stored at gauss points
    !   of each element
       call memory_alloca(mem_modul(1:2,modul),'STATT','sld_memall',statt,nelem)
       do ielem = 1,nelem
          pelty = abs(ltype(ielem))
          pgaus = ngaus(pelty)
          call memory_alloca(mem_modul(1:2,modul),'STATT(IELEM)','sld_memall',statt(ielem)%a,7_ip,pgaus,2_ip)
       end do

    !
    ! Isochrones
    !
    allocate(isoch_sld(npoin,2), stat=istat)
    call memchk(zero,istat,mem_modul(1:2,modul),'isoch_sld','sld_memall',isoch_sld)
    isoch_sld=-1.0_rp

    allocate(iswav_sld(npoin,2), stat=istat)
    call memchk(zero,istat,mem_modul(1:2,modul),'iswav_sld','sld_memall',iswav_sld)
    iswav_sld=0_ip

  else

     allocate(displ(1,1,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DISPL','sld_memall',displ)
     allocate(veloc_sld(1,1,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VELOC_SLD','sld_memall',veloc_sld)
     allocate(accel_sld(1,1,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'ACCEL_SLD','sld_memall',accel_sld)

     allocate(dunkn_sld(1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DUNKN_SLD','sld_memall',dunkn_sld)

     allocate(quali_sld(1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'QUALI_SLD','sld_memall',quali_sld)

     allocate(dfric_sld(1,1,1,2),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DFRIC_SLD','sld_memall',dfric_sld)
     allocate(dslip_sld(1,1,1,2),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DSLIP_SLD','sld_memall',dslip_sld)
     allocate(dceff_sld(1,1,1,2),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DCEFF_SLD','sld_memall',dceff_sld)
     allocate(dcmax_sld(1,1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DCMAX_SLD','sld_memall',dcmax_sld)
     
     allocate(dxfem_sld(1,1,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DXFEM_SLD','sld_memall',dxfem_sld)
     allocate(vxfem_sld(1,1,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VXFEM_SLD','sld_memall',vxfem_sld)
     allocate(axfem_sld(1,1,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'AXFEM_SLD','sld_memall',axfem_sld)
     allocate(crapx_sld(1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'CRAPX_SLD','sld_memall',crapx_sld)
     allocate(cranx_sld(1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'CRANX_SLD','sld_memall',cranx_sld)

     allocate(gpfib(ndime,mgaus,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'GPFIB','sld_memall',gpfib)

     allocate(bridg_sld(4,mgaus,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'BRIDG_SLD','sld_memall',bridg_sld)

     allocate(nopio_sld(1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'NOPIO_SLD','sld_memall',nopio_sld)
     allocate(caust_sld(1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'CAUST_SLD','sld_memall',caust_sld)
     allocate(green_sld(1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'GREEN_SLD','sld_memall',green_sld)
     allocate(lepsi_sld(1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LSFSN_SLD','sld_memall',lepsi_sld)
     allocate(restr_sld(6,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'RESTR_SLD','sld_memall',restr_sld)
     allocate(ddisp_sld(6,1,2),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DDISP_SLD','sld_memall',ddisp_sld)
     allocate(fibde_sld(1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'FIBDE_SLD','sld_memall',fibde_sld)
     allocate(seqvm_sld(1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'SEQVM_SLD','sld_memall',seqvm_sld)
     allocate(seqin_sld(1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'SEQIN_SLD','sld_memall',seqin_sld)
     allocate(gdepo_sld(ndime,ndime,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'GDEPO_SLD','sld_memall',gdepo_sld)        
     
     allocate(dceff_sld(nelem,1,1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DCEFF_SLD','sld_memall',dceff_sld)
     allocate(dcmax_sld(nelem,1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DCMAX_SLD','sld_memall',dcmax_sld)
     allocate(iswav_sld(npoin,2), stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'iswav_sld','sld_memall',iswav_sld)

     allocate(vmass_sld(1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VMASS_SLD','sld_memall',vmass_sld)


  end if

  dxfem_sld=0.0_rp
  vxfem_sld=0.0_rp
  axfem_sld=0.0_rp
  crapx_sld=0.0_rp
  cranx_sld=0.0_rp
  sidev_sld=0.0_rp
  dltap_sld=0.0_rp

  ifase_sld=0_ip
  kfase_sld=9_ip

  !
  ! Contact stuff
  !
  !all this memory is allocated only once, at the beginning of Alya execution
  allocate(contact_info(npoin,28))
  allocate(rhscont(npoin*ndime))
  rhscont=0
  allocate(neumanncb(npoin*ndime))
  neumanncb=0
  allocate(res(npoin*ndime))
  res=0
  allocate(lagmult(npoin))
  lagmult=0
  allocate(lagmult_ant(npoin))
  lagmult_ant=0

end subroutine sld_memall
