!-----------------------------------------------------------------------
!> @addtogroup SolidzInput
!> @{
!> @file    sld_reaphy.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Read physical data
!> @details Read physical data
!> @} 
!-----------------------------------------------------------------------
subroutine sld_reaphy()
  !------------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_domain

  use def_solidz

  implicit none
  integer(ip) :: imate,nmatm,nrema,nrefi,irefi,iauxi,irema,iusem,idime
  real(rp)    :: xrefi(12),xauxi,xbalo(3,3),xnorm(3),young,poiss,lame1,lame2,dummr

  if( INOTSLAVE ) then
     !
     ! Initializations (defaults)
     !
     kfl_model_sld = 1                        ! Elastic(1) - visco-elastic (2) - elasto-plastic (3)
     kfl_timei_sld = 1                        ! Transient term
     kfl_probl_sld = 1                        ! Transient static (0) or dynamic (1)
     kfl_vumat_sld = 0                        ! Vumat model
     kfl_fiber_sld = 0                        ! Fiber field (0 if none)
     kfl_restr_sld = 0                        ! Initial stress fields (ndime size)
     kfl_indis_sld = 0                        ! Initial displacements
     kfl_prdef_sld = 0                        ! Fibers field nodal- or element- wise
     kfl_damag_sld = 0                        ! No damage model
     kfl_cohes_sld = 0                        ! No cohesive law
     kfl_intva_sld = 0                        ! No internal variables
     kfl_vofor_sld = 0                        ! External volume force
     kfl_moduf_sld = 0                        ! No modulating fields
     kfl_plane_sld = 0
     nvint_sld     = 1                        ! Number of internal variables
     ivert_sld     = 0                        ! Gravity dimension
     nshea_sld     = 1                        ! Shear components

     aleso_sld(1)  = 0.0_rp                   ! ALE solidz: Tezduyar rigidization smallest element size
     aleso_sld(2)  = 0.0_rp                   ! ALE solidz: Tezduyar rigidization exponent

     thiso_sld     = -10000000.0_rp

     !
     ! Reach the section
     !
     call ecoute('sld_reaphy')
     do while(words(1)/='PHYSI')
        call ecoute('sld_reaphy')
     end do
     !
     ! Begin to read data
     !
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Physical properties definition
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> PHYSICAL_PROBLEM
     !
     do while(words(1)/='ENDPH')
        call ecoute('sld_reaphy')

        if(words(1)=='PROBL') then
           ! ADOC[1]> PROBLEM_DEFINITION
           call ecoute('sld_reaphy')
           do while(words(1)/='ENDPR')
              if(words(1)=='TEMPO') then       
                 ! 
                 ! ADOC[2]> TEMPORAL_DERIVATIVES: DYNAM                          $ Temporal problem: Dynamic (default)                         
                 !
                 if(words(2)=='DYNAM') then
                    kfl_probl_sld = 1
                 else
                    kfl_probl_sld = 0
                 end if
              end if
              call ecoute('sld_reaphy')
           end do
           ! ADOC[1]> END_PROBLEM_DEFINITION

        else if(words(1)=='TANGE') then
           ! ADOC[1]> CHECK ANALYTICAL TANGENT MODULI VS. NUMERICAL TANGENT MODULI
           kfl_tange_sld = getint('TANGE',1_ip,'Use numerical tangent')

        else if(words(1)=='PROPE') then

           call sld_memphy(1_ip)

           call ecoute('sld_reaphy')
           imate = 1
           ! ADOC[1]> PROPERTIES 
           do while(words(1)/='ENDPR')
              if( words(1) == 'MATER' ) then
                 imate = getint('MATER',1_ip,'#Current material')
                 if( imate > nmate_sld ) then
                    call runend('SLD_REAPHY: THIS MODULE HAS MORE MATERIALS THAN THOSE DEFINED IN DOM.DAT')
                 end if
              ! ADOC[2]> DENSITY: real                                           $ Density (rho)                   
              else if(words(1)=='DENSI') then                    
                 densi_sld(1:ncoef_sld,imate)=param(1:ncoef_sld)

              ! ADOC[2]> THERMAL_EFFECTS: THFIN=real, DELTT=real                 $ Thermal effects in the constitutive model
              else if(words(1)=='THERM') then
                 kfl_therm_sld = 1
                 deltt_sld(1) = getrea('THFIN',0.00_rp,'Final time for thermal effects')
                 deltt_sld(2) = getrea('DELTT',0.00_rp,'Increment of temperature DeltaT')

              ! ADOC[2]> RAYLEIGH_DUMPING: RAYALPHA=real,RAYBETA=real            $ Rayleigh damping coefficients: C = alpha*M + beta*K                     
              else if(words(1)=='RAYLE') then                          
                 kfl_dampi_sld(imate)= 1
                 dampi_sld(1,imate)  = getrea('RAYAL',0.00_rp,'Rayleigh dumping alpha factor')
                 dampi_sld(2,imate)  = getrea('RAYBE',0.00_rp,'Rayleigh dumping beta factor')

              ! ADOC[2]> GRAVITY: MODULE=real ,XCOMP=real YCOMP=real ZCOMP=real  $ Gravity acceleration  
              else if(words(1)=='GRAVI') then
              
                 grnor_sld    = getrea('MODUL',0.0_rp,'#Gravity norm')
                 gravi_sld(1) = getrea('XCOMP',0.0_rp,'#x-component of g')
                 gravi_sld(2) = getrea('YCOMP',0.0_rp,'#y-component of g')
                 gravi_sld(3) = getrea('ZCOMP',0.0_rp,'#z-component of g')
                 call vecuni(3_ip,gravi_sld,dummr)

              else if(words(1)=='LAWDE') then               ! Law Density (rho)
                 lawde_sld(imate)=0

              ! ADOC[2]> CONSTITUTIVE_MODEL: string real real ...                $ Material law.  
              ! ADOC[d]> CONSTITUTIVE_MODELS: 
              ! ADOC[d]> <li> ISOLIN (isotropic linear elastic): YOUNG, POISSON </li>
              ! ADOC[d]> <li> HOLZA (Transversaly isotropic hyperelastic): E_eq,k,a,b,af,bf,as,bs,afs,bfs,scalf.
              ! ADOC[d]> E_eq is an equivalent rigidity used to calculate the time step. k is the compresibility factor. 
              ! ADOC[d]> scalf scales the stress tensor (defualt to 1) </li>                         
              
              else if(words(1)=='CONST') then               ! Constitutive model
                 if(words(2)=='NEOHO') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=101   ! default, belytschko
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)

                    if(words(3)=='BELYT') then
                       lawst_sld(imate)=101
                       parco_sld(1:ncoef_sld,imate)=param(3:ncoef_sld+2)
                    else if(words(3)=='ANSYS') then
                       lawst_sld(imate)=102
                       parco_sld(1:ncoef_sld,imate)=param(3:ncoef_sld+2)
                    else if(words(3)=='CHAPE') then
                       lawst_sld(imate)=121
                       parco_sld(1:ncoef_sld,imate)=param(3:ncoef_sld+2)
                    else if(words(3)=='WATAN') then
                       lawst_sld(imate)=125
                       parco_sld(1:ncoef_sld,imate)=param(3:ncoef_sld+2)
                    end if
                    
                   
                 else if(words(2)=='ALESO') then         ! Tezduyar ALE strategy
                    kfl_aleso_sld(imate)= 1
                    aleso_sld(1)  = getrea('MINSI',voale(1),'Tezduyar rigidization smallest element size')
                    aleso_sld(2)  = getrea('EXPON',1.2_rp,'Tezduyar rigidization exponent')
                    ! ALEsolids are always  NEOHOOKEAN   BELYTSCHKO   1.0  1.0   lambda and mu
                    lawco_sld(imate)=1
                    lawst_sld(imate)=101         ! belytschko
!!                    lawst_sld(imate)=100         ! isolin
                    parco_sld(1,imate)=1.0_rp    ! lambda = 1
                    parco_sld(2,imate)=1.0_rp    ! mu = 1
!                    parco_sld(1,imate)=1728395.06172839    ! lambda = 1
!                    parco_sld(2,imate)=740740.740740741    ! mu = 1
                    ! ALEsolids are always    DENSITY= 1
                    densi_sld(1,imate)  = 1.0_rp
                    ! ALEsolids are always    LAW_DENSITY= CONSTANT
                    lawde_sld(imate)    = 0
                    
                 else if(words(2)=='ISOLI') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=100   ! isotropic linear elastic
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
 
                    young = parco_sld(1,imate)
                    poiss = parco_sld(2,imate)
                    
                    !Lame Constants:
                    lame1 = (young*poiss)/((1.0_rp+poiss)*(1.0_rp-2.0_rp*poiss))  !lambda
                    lame2 = young/(2.0_rp*(1.0_rp+poiss))  !mu
                    
                    parco_sld(3,imate) = lame1
                    parco_sld(4,imate) = lame2
                 
                 else if(words(2)=='MOONE') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=103   ! Mooney-Rivlin material (formulation Xiao and Belystcho by default)
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                    if(words(3)=='MR1') then
                      lawmo_sld(imate)=1
                      parco_sld(1:ncoef_sld,imate)=param(3:ncoef_sld+2)
                    else if(words(3)=='MR2') then
                      lawmo_sld(imate)=2
                      parco_sld(1:ncoef_sld,imate)=param(3:ncoef_sld+2)
                    else if(words(3)=='MR3') then
                      lawmo_sld(imate)=3
                      parco_sld(1:ncoef_sld,imate)=param(3:ncoef_sld+2)
                    end if

                 else if(words(2)=='ORTLI') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=151   ! orthotropic linear elastic
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                    !compute stiff0 without damage
                    call sm151_precalculus(imate)

                  else if(words(2)=='MAIMI') then
                    !
                    ! Transversely isotropic damage model (Maimi 2016)
                    !
                    lawco_sld(imate)=1
                    lawst_sld(imate)=152   ! 3D damage model for transversely isotropic material
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                    kfl_damag_sld = 1
                    !call sm152_precalculus(imate)

                  else if(words(2)=='OLIVE') then
                    !
                    ! Isotropic damage model (Oliver 1990)
                    !
                    lawco_sld(imate)=1
                    lawst_sld(imate)=153
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                    kfl_damag_sld = 1
                    call sm153_precalculus(imate)

                 else if(words(2)=='SPRIN') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=105   ! constant spring
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                    
                 else if(words(2)=='LINYI') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=133   ! isotropic linear elastic
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                    
                 else if(words(2)=='UCSAN') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=135
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                    
                 else if(words(2)=='HOLZA') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=134
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                    if (nnpar < 11) parco_sld(11,imate)= 1.0   ! required for retrocompatibility

                 else if(words(2)=='GUCCI') then
                    lawco_sld(imate)=1
                    lawst_sld(imate)=136
                    parco_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)

                 else if(words(2)=='USERD') then   ! USER DEFINED MODELS
                    lawco_sld(1)=5
                    call ecoute('sld_reaphy')
                    if (words(1)=='USERM') then              ! user models list
                       do while(words(1)/='ENDUS')
                          if (words(1)=='MODNU') then            ! model number
                             kusmo_sld(imate)= int(param(1))
                          else if (words(1)=='INTER') then       ! model parameters
                             nvint_sld = int(param(1))
                             kfl_intva_sld = 1
                          else if (words(1)=='PARAM') then       ! model parameters
                             parco_sld(1:nnpar,imate) = param(1:nnpar)
                          end if
                          call ecoute('sld_reaphy')
                       end do
                    end if
                 end if


              else if(words(1)=='CONTA') then               ! Contact and friction model
                 parcf_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)

              else if(words(1)=='COHES') then               ! Cohesive model
                 kfl_cohes_sld = 1
                 
                 if(words(2)=='DECRE') then
                    lawch_sld(imate)=900
                    parch_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                 end if                    

                 if(words(2)=='SMITH') then
                    lawch_sld(imate)=901
                    parch_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                 end if                    

                 if(words(2)=='EXPON') then
                    lawch_sld(imate)=902
                    parch_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                 end if                    

                 if(words(2)=='POWER') then
                    lawch_sld(imate)=903
                    parch_sld(1:ncoef_sld,imate)=param(2:ncoef_sld+1)
                 end if                    

              ! ADOC[2]> FIBER_MODEL: FIELD=int | XALIG | SPHEROID | FILE        $ Fiber field. 
              ! ADOC[d]> FIBER_MODEL: 
              ! ADOC[d]> <li> FIELD: int. Defined as a vectorial field in the .dom.dat </li>
              ! ADOC[d]> <li> FIELD: XALIG | YALIG | ZALIG. Oriented along the cartesian axis. </li>
              ! ADOC[d]> <li> FIELD: SPHEROID, a_epi,b_epi,a_endo,b_endo, helix. 
              ! ADOC[d]> Oriented along the cardiac prolate spheroidal coordinate system. 
              ! ADOC[d]> The long (a) and short (b) diameters for the endocardium and epicardium, and the transmural 
              ! ADOC[d]> angle (helix) are given for the endocardium and epicardium (AlyaRED) </li>
                 
              else if(words(1)=='FIBER') then               ! FIBER_MODEL
                 kfl_fiber_sld=  1                      ! fibers field, this means at least one material has fibers
                 if(words(2)=='XALIG') then
                     modfi_sld(imate)=1
                 else if(words(2)=='YALIG') then
                     modfi_sld(imate)=2
                 else if(words(2)=='ZALIG') then
                     modfi_sld(imate)=3
                 else if(words(2)=='SPHER') then
                     modfi_sld(imate)=4
                     parsp_sld(1:5,imate)=param(2:6)
                 else if(words(2)=='FILE') then
                     modfi_sld(imate)=5
                 else if(words(2)=='FIELD') then                
                     modfi_sld(imate) = -getint('FIELD',1_ip,'#Field Number fo fibers')                    
                 end if

              ! ADOC[2]> STRESS_RESIDUAL_FIELD: FIELD= int
              ! ADOC[d]> STRESS_RESIDUAL_FIELD: 
              ! ADOC[d]> <li> Residual initial stresses field in Voigt notation
              else if(words(1)=='STRES') then               ! STRESS_RESIDUAL_FIELD
                 if(words(2)=='FIELD') then
                    kfl_restr_sld = - getint('FIELD',-1_ip,'#Field number for the X row of the initial stress tensor')   
                 end if                 

              ! ADOC[2]> IN_DISPLACEMENT: FIELD= int | STRESSED
              ! ADOC[d]> IN_DISPLACEMENT: 
              ! ADOC[d]> <li> Initial displacements field
              ! ADOC[d]> <li> STRESSED means that the initial displacement field goes to the initial value of
              ! ADOC[d]> <li> the displacement unknown, thus generating stress.
              ! ADOC[d]> <li> Otherwise, when no additional tags, the displacement field goes to the 
              ! ADOC[d]> <li> adds to the coordinates (i.e. the reference system), with no stress generation.
              else if(words(1)=='INDIS') then
                 if(words(2)=='FIELD') then
                    kfl_indis_sld(1) = - getint('FIELD',-1_ip,'#Field number for the initial displacements')   
                    if (exists('STRES')) kfl_indis_sld(2)= 1 ! stressed displacement
                 end if                 

              ! ADOC[2]> ORTHOTROPIC_FIBER                                       $ Orthotropic fiber field (3 vectors must be defined). 
              else if(words(1)=='ORTHO') then               ! ORTHOTROPIC FIBER_MODEL
                 if (kfl_fiber_sld==0) then                 ! fiber model must be already set 
                    call runend('SLD_REAPHY: A TANGENT FIBER FIELD MUST BE GIVEN, NOT ONLY ORTHO')
                 end if                 
                 kfl_fiber_sld = 2
                 modor_sld(1,imate) = -int(param(2))
                 modor_sld(2,imate) = -int(param(3))
                 if (exists('ISOTR')) then
                    kfl_fiber_sld = 3
                 end if
              else if(words(1)=='FORCE') then               ! 
                 kfl_vofor_sld = getint('FIELD',1_ip,'#Field Number of the external volume force')   

              ! ADOC[2]> MODULATING_FIELDS                                    $ Fields that modulate a given property
              ! ADOC[3]> RIGIDITY                                             $ Modulating material rigidity
              ! ADOC[3]> FIELD:  integer                                      $ Number of the modulating field

              else if(words(1)=='MODUL') then               ! MODULATING FIELDS
                 if (words(2) =='RIGID') then
                    kfl_moduf_sld(1) = getint('FIELD',1_ip,'#Field Number of the external volume force')   
                 end if

              else if(words(1)=='STATE') then
                 if (words(2)=='STRAI')  kfl_plane_sld = 1
                 


              ! ADOC[2]> COUPLING_SCHEME                                         $ Paremeters of the electro-mechanical coupling (AlyaRED) 
              ! ADOC[3]> CONTROL_FACTOR: real                                 $ Scale the active stress component 
              ! ADOC[3]> TIME_CONSTANT:  real                                 $ Time constant for the [Ca++] fonction 
              ! ADOC[2]> END_COUPLING_SCHEME                         
              ! ADOC[d]> COUPLING_SCHEME: 
              ! ADOC[d]> <li> CONTROL_FACTOR: scale the active stress equation: stress_tot = stress_passive + (CONTROL_FACTOR*stress_active).
              ! ADOC[d]> The default value = 1.0 correspond to human (Hunter et al. 1998)   </li>     
              ! ADOC[d]> <li> TIME_CONSTANT: thau in the [Ca++] fonction.   
              ! ADOC[d]> The default value = 0.06 sec. correspond to human (Hunter et al. 1998)   </li>                      
              
              
              else if(words(1)=='COUPL') then               ! Coupling model (SOLIDZ parameters for EXMEDI coupling)
                 call ecoute('sld_reaphy')
                 do while(words(1)/='ENDCO')
                    kfl_coupt_sld(imate) = 1
                    if(words(1)=='ECCTY') then
                       if (words(2)=='HUNTE') then
                           kfl_eccty_sld(imate)= 1_ip
                       else if (words(2)=='RICEJ') then
                           kfl_eccty_sld(imate)= 2_ip
                       end if
                    end if
                    if(words(1)=='CONTR') cocof_sld(imate)= param(1)  !Coupling Factor always for exmedi-solidz coupling
                    if(words(1)=='TIMEC') timec_sld(imate)= param(1)                    
                    if(words(1)=='HILLC') hillc_sld(imate)= param(1)                    
                    if(words(1)=='CAL50') cal50_sld(imate)= param(1)                    
                    if(exists('VARIA')) kfl_coupt_sld(imate) = 11  !never the case
                    if(words(1)=='TRISO') then
                       kfl_coupt_sld(1:nmate_sld) = 2         !not for exmedi-solidz    ! transversally isotropic coupling
                       trans_sld(1,1:nmate_sld)= param(1)  ! ratio 
                    end if
                    call ecoute('sld_reaphy')
                 end do


              else if(words(1) =='CSYSM') then               ! Material coordinate system defined by 3 vectors (*AQU*)
                 kfl_fiber_sld = 5_ip

              else if(words(1) =='ISOCH') then
                 if(words(2)=='DIISO') then !Displacement isochrones
                    thiso_sld(1)=param(2)
                 elseif(words(2)=='VMISO') then
                    thiso_sld(2)=param(2)
                 endif

              end if

              call ecoute('sld_reaphy')

           end do
           ! ADOC[1]> END_PROPERTIES 

           if (imate < nmate_sld) then
              ! When the total number of materials used by this module
              ! is smaller than the kernel materials:
              ! 1. if the nmatm == 1, this is the default one, so assign all properties to it
              ! 2. otherwise if the nmatm > 1 stop the problem and demand to reassign properties
              !    for this module

              nmatm = imate
              
              if (nmatm > 1) then
                 call runend('SLD_REAPHY: THIS MODULE HAS MORE THAN ONE MATERIAL, BUT LESS THAN THOSE AT DOM.DAT')
              end if
              
              do imate= nmatm, nmate_sld
                 densi_sld(1:ncoef_sld,imate) = densi_sld(1:ncoef_sld,1) 
                 kfl_dampi_sld(imate)         = kfl_dampi_sld(1)        
                 dampi_sld(1,imate)           = dampi_sld(1,1)          
                 dampi_sld(2,imate)           = dampi_sld(2,1)          
                 lawde_sld(imate)             = lawde_sld(1)            
                 lawco_sld(imate)             = lawco_sld(1)            
                 lawst_sld(imate)             = lawst_sld(1)            
                 parco_sld(1:ncoef_sld,imate) = parco_sld(1:ncoef_sld,1)
                 kfl_aleso_sld(imate)         = kfl_aleso_sld(1)        
                 lawde_sld(imate)             = lawde_sld(1)            
                 kusmo_sld(imate)             = kusmo_sld(1)            
                                                                            
                 parcf_sld(1:ncoef_sld,imate) = parcf_sld(1:ncoef_sld,1)
                                                                            
                 lawch_sld(imate)             = lawch_sld(1)            
                 parch_sld(1:ncoef_sld,imate) = parch_sld(1:ncoef_sld,1)
                                                                            
                 modfi_sld(imate)             = modfi_sld(1)                  
                 modor_sld(1,imate)           = modor_sld(1,1)          
                 modor_sld(2,imate)           = modor_sld(2,1)          
                                                                            
                 kfl_coupt_sld(imate)         = kfl_coupt_sld(1)        
                 cocof_sld(imate)             = cocof_sld(1)            
                 timec_sld(imate)             = timec_sld(1)            
                 trans_sld(1,imate)           = trans_sld(1,1)          
              end do

           end if
        end if
     end do
     ! ADOC[0]> END_PHYSICAL_PROBLEM 
     !
     ! Identify the gravity dimension
     !
     do idime= 1,ndime
        if ((gravi_sld(idime) > 1.0e-10) &
             .or.(gravi_sld(idime) < -1.0e-10)) then
           ivert_sld= idime
        end if
     end do
     !
     ! Shear components
     !
     if (ndime == 3) nshea_sld = 3


  end if

end subroutine sld_reaphy

