!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_elmgat.f90
!> @author  Guillaume Houzeaux
!> @date    07/05/2013
!> @brief   Element gather
!> @details Gather some global arrays to elemental arrays
!> @} 
!-----------------------------------------------------------------------

subroutine sld_elmgat(pnode,lnods,eldis,elvel,elcod,eldix,elvex,elepo,elmof,elrst)
  use def_kintyp, only     :  ip,rp
  use def_master, only     :  displ
  use def_domain, only     :  ndime,coord
  use def_solidz, only     :  veloc_sld,dxfem_sld,vxfem_sld
  use def_solidz, only     :  kfl_xfeme_sld,fiemo_sld,kfl_restr_sld
  use def_solidz
  implicit none
  integer(ip), intent(in)  :: pnode                    !< Number of element nodes
  integer(ip), intent(in)  :: lnods(pnode)             !< Element node connectivity
  real(rp),    intent(out) :: elcod(ndime,pnode)       !< Element node coordinates
  real(rp),    intent(out) :: eldis(ndime,pnode,*)     !< Element node displacement
  real(rp),    intent(out) :: elvel(ndime,pnode,*)     !< Element node velocity
  real(rp),    intent(out) :: eldix(ndime,pnode,*)     !< Element XFEM node displacement
  real(rp),    intent(out) :: elvex(ndime,pnode,*)     !< Element XFEM node velocity
  real(rp),    intent(out) :: elepo(ndime,ndime,pnode) !< Element node push forward
  real(rp),    intent(out) :: elmof(pnode)             !< Element node modulator field
  real(rp),    intent(out) :: elrst(6,pnode)           !< Element node residual stresses
  integer(ip)              :: inode,ipoin,idime,jdime,nauxi,ivoig
  !
  ! Current global values
  !
  do inode = 1,pnode
     ipoin = lnods(inode)
     do idime = 1,ndime
        elcod(idime,inode)   = coord(idime,ipoin)
        elvel(idime,inode,1) = veloc_sld(idime,ipoin,1)
        eldis(idime,inode,2) = displ(idime,ipoin,3)
        eldis(idime,inode,3) = displ(idime,ipoin,4)
        eldis(idime,inode,1) = displ(idime,ipoin,1)
        if(kfl_prest_sld ==1 ) eldis(idime,inode,1) = ddisp_sld(idime,ipoin,1)   !if prestres

        eldix(idime,inode,1) = 0.0_rp
        eldix(idime,inode,2) = 0.0_rp
        eldix(idime,inode,3) = 0.0_rp
        elvex(idime,inode,1) = 0.0_rp
        do jdime = 1,ndime
           elepo(idime,jdime,inode) = 0.0_rp
        end do
        do ivoig= 1, 6
           elrst(ivoig,inode) = 0.0_rp
        end do
     end do
  end do

  !
  ! XFEM
  !
  if( kfl_xfeme_sld == 1 ) then 
     do inode = 1,pnode
        ipoin = lnods(inode)
        do idime = 1,ndime
           eldix(idime,inode,1) = dxfem_sld(idime,ipoin,1)
           eldix(idime,inode,2) = dxfem_sld(idime,ipoin,3)
           eldix(idime,inode,3) = dxfem_sld(idime,ipoin,4)
           elvex(idime,inode,1) = vxfem_sld(idime,ipoin,1)
        end do
     end do
  end if
  !
  ! Rigidity modulator field
  !
  if( kfl_moduf_sld(1) > 0 ) then
     do inode = 1,pnode
        ipoin = lnods(inode)
        elmof(inode) = fiemo_sld(1,ipoin)
     end do     
  end if
  !
  ! Residual stresses
  !
  if( kfl_restr_sld < 0 ) then
     nauxi= 3
     if (ndime==3) nauxi=6
     do ivoig= 1,nauxi
        do inode = 1,pnode
           ipoin = lnods(inode)
           elrst(ivoig,inode) = restr_sld(ivoig,ipoin)
        end do
     end do
  end if

end subroutine sld_elmgat
