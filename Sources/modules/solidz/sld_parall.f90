subroutine sld_parall(itask)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_parall
  ! NAME
  !    sld_parall
  ! DESCRIPTION
  !    This routine is a bridge to Parall service  
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solidz
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipart,istar,istop

  if ( IPARALL ) then

     select case(itask)

     case(1_ip)
        !
        ! Exchange data read in sld_reaphy, sld_reanut and sld_reaous
        ! always using MPI, even if this is a partition restart
        !
        call sld_sendat(one)

     case(2_ip)
        !
        ! Exchange data read in sld_reabcs
        !
        call Parall(21_ip)
        istar = pard1
        istop = pard2
        do ipart = 0,istar,istop
           if( kfl_paral >= ipart .and. kfl_paral < ipart+istop ) then
              call sld_sendat(two)
           end if
           call Parall(20_ip)
        end do

     case(3_ip)
        !
        ! Sum up residual contribution of slave neighbors
        !
        if ( ISLAVE ) then
           call vocabu(NPOIN_REAL_12DI,ndofn_sld,0_ip)
           parr1 => rhsid(1:ndofn_sld*npoin)
           call Parall(400_ip)
        end if

     case(4_ip)
        !
        ! Sum up kacti activation vector of slave neighbors
        !
        if ( ISLAVE ) then
           call vocabu(NPOIN_INTE_1DIM,0_ip,0_ip)
           pari1 => kacti_sld(1:npoin)
           !write(*,*) "Im in", kfl_paral, kacti_sld(3:6)
           call Parall(400_ip)
        end if

     end select

  end if

end subroutine sld_parall
