subroutine sld_outinf()
  !------------------------------------------------------------------------
  !****f* Solidz/sld_outinf
  ! NAME 
  !    sld_outinf
  ! DESCRIPTION
  !    This routine writes information in SOLIDZ files
  ! OUTPUT
  ! USES
  ! USED BY
  !    sld_turnon
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_solidz
  implicit none

end subroutine sld_outinf
      
