 subroutine sld_reanut()
  !-----------------------------------------------------------------------
  !****f* Temper/sld_reanut
  ! NAME
  !    sld_reanut
  ! DESCRIPTION
  !    This routine reads the numerical treatment for SOLIDZ  module
  ! USES
  !    ecoute
  ! USED BY
  !    sld_turnon
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_solidz
  use def_domain
  implicit none

  real(rp)     :: freq1,freq2,damp1,damp2,vauxi,ishel
  integer(ip)  :: icavi,i,imate

  if(kfl_paral<=0) then
     !
     !  Initializations (defaults)
     !
     kfl_xfeme_sld = 0                          ! XFEM family of enrichment strategies
     kfl_normc_sld = 2                          ! Norm of convergence
     kfl_tiacc_sld = 2                          ! Temporal accuracy
     kfl_timet_sld = 1                          ! Time treatment (explicit=1, implicit=2)
     kfl_tisch_sld = 2                          ! Time integration scheme
     kfl_linea_sld = 0                          ! No linearization method chosen
     kfl_serei_sld = 0                          ! Selective reduced integration is off
     kfl_limit_sld = 0
     kfl_volca_sld = 0
     kfl_prest_sld = 0                          ! Do not compute pre-stress
     kfl_quali_sld = 0                          ! No deformed mesh quality is checked
     kfl_gdepo_sld = 0                          ! Push backwards to the reference configuration
     kfl_arcle_sld = 0                          ! Arc-length method for non-linear equations chosen
     miinn_sld     = 10                         ! Max # of linearization iterations
     cotol_sld     = -1.0d-5                     ! Convergence tolerance
     safet_sld     = 1.0_rp                     ! Safety factor for time step
     safex_sld     = 1.0_rp                     ! Safety factor for time step
     meanf_sld     = 1.0d-6                     ! Meaningful value of stress/force
     sstol_sld     = -1.0d-6                     ! Steady state tolerance
     spect_sld(1)  = 0.0_rp                     ! spectral radius
     spect_sld(2)  = 1.0_rp                     ! stability limit (default value)
     relfa_sld     = 1.0_rp                     ! relaxation factor
     itarc_sld     = 6_ip                       ! Optimal number of iterations for NR in arc-length methods (van del Meer 2012)
     mcavi_sld     = 1                          ! number of cavities (ventricles) - only to compute ejection rate
     kcavi_sld     = 0
     ocavi_sld     = 0.0_rp
     arcint_sld     = 0.0_ip                     ! By default no arclength technique
     !
     ! Reach the section
     !
     call ecoute('sld_reanut')
     do while(words(1)/='NUMER')
        call ecoute('sld_reanut')
     end do
     ishel = 0
     !
     ! Begin to read data
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Numerical treatment definition
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> NUMERICAL_TREATMENT        
     do while(words(1)/='ENDNU')
        call ecoute('sld_reanut')


  
        ! ADOC[1]> TIME_TREATMENT:       IMPLICIT  | EXPLICIT                        $ temporal differencing scheme    
        if(words(1)=='TIMET') then              ! Time treatment
           if(words(2)=='EXPLI') then
              kfl_timet_sld=1
           else if(words(2)=='IMPLI') then
              kfl_timet_sld=2
              !call runend('SLD_REANUT: IMPLICIT TREATMENT NOT PROGRAMMED')
           end if

        ! ADOC[1]> PRESTRESS
        ! ADOC[d]> Compute the pre-stress field
        else if(words(1)=='PREST') then         ! Compute pre-stress
           kfl_prest_sld = 1

        ! ADOC[1]> TIME_INTEGRATION: NEWMARK | CENTRAL_DIFFERENCE | CRANK-NICHILSON  $ temporal differencing scheme method
        ! ADOC[d]> TIME_INTEGRATION: 
        ! ADOC[d]> <li> CRANK-NICHILSON: STABI, LIMIT (stability limit, relaxation factor (default=0.5)) </li>
        ! ADOC[d]> <li> NEWMARK: NEWBE,NEWGA,NEWAL    (Beta, Gamma and Alpha factors) </li>        
        else if(words(1)=='TIMEI') then         ! Time integration scheme
           if(exists('CENTR')) then
              kfl_tisch_sld=1                            ! central differences
           else if(exists('CRANK')) then
              spect_sld(2)  = 1.55_rp                    ! stability limit (this is the good one for CN)
              relfa_sld     = 0.5_rp                     ! relaxation factor for CN is 0.5 (default)
              call runend('SLD_REANUT: CRANK-NICHOLSON NOT PROGRAMMED YET')
           else if(exists('NEWMA')) then
              kfl_tisch_sld=2
              
              
              ! suggestion: for alpha-gerenalized Newmark scheme, we use key-word NEWMA
              !             instead of ALPHA.
              !             Key-word ALPHA, BETA and GAMMA can be reserved for
              !             numerical parameters below
              
              tifac_sld(1)  = getrea('NEWBE',0.00_rp,'Beta factor')
              tifac_sld(2)  = getrea('NEWGA',0.50_rp,'Gamma factor')
              tifac_sld(3)  = getrea('NEWAL',0.00_rp,'Alpha factor')
              
              ! 1) Undamped trapezoidal rule (orden 2)
              ! TIME_INTEGRATION:     NEWMARK, NEWBE=0.25, NEWGA=0.5, NEWAL=0.0
              !
              ! 2) Numerically damped integrator with damping proportional to NEWGA-0.5
              ! TIME_INTEGRATION:     NEWMARK, NEWBE=0.65, NEWGA=0.9, NEWAL=0.0 
              
              if (exists('DAMPE')) then
                 tifac_sld(1)  = 0.65_rp
                 tifac_sld(2)  = 0.90_rp
                 tifac_sld(3)  = 0.00_rp
              end if

!              write (lun_livei,'(1x,a,e10.3)')'tifac_sld(1)/beta  = ',tifac_sld(1)
!              write (lun_livei,'(1x,a,e10.3)')'tifac_sld(2)/gamma = ',tifac_sld(2)
!              write (lun_livei,'(1x,a,e10.3)')'tifac_sld(3)/alpha = ',tifac_sld(3)



! ESO? commented by Pierre 
! Note: set kfl_timet_sld = 1 (explicit) if beta = 0.0
!              if (tifac_sld(1) == 0.0_rp) kfl_timet_sld = 1_ip

           end if
        ! ADOC[1]> [PLANE_STRAIN]                                        $ Plane strain in 2D
        ! ADOC[d]> PLANE_STRAIN: only works for 2D cases, and for the ISOLIN material model

        else if(words(1)=='QUALI') then         ! Check mesh quality
           kfl_quali_sld = 1


        ! ADOC[1]> SAFETY_FACTOR:        real                                        $ Temporal Safety factor
        ! ADOC[d]> SAFETY_FACTOR: Safety factor that multiplicates the calculated critical time step. SF<1 in explicit and >1 in implicit.   
        ! ADOC[d]> Decrease this value can help convergence when a "NEGATIVE GPDET" error is returned.           
        else if(words(1)=='SAFET') then         ! Safety factor
           safet_sld = param(1)
           if (kfl_probl_sld == 0) safet_sld = 1e12_ip
           safex_sld  = getrea('EXPON',1.0_rp,'#Safety Factor Exponential time function')
           safma_sld  = getrea('MAXIM',1.0e9_rp,'#Maximum safety factor function')
           nisaf_sld  = getint('INITI',0_ip    ,'#Initial step for variable CFL')

        else if(words(1)=='SELEC') then         ! Selective reduced integration enabled
           kfl_serei_sld = 1

        ! ADOC[1]> STEADY_STATE_TOLERANCE:        real                               $ Tolerance for steady-state 
        ! ADOC[d]> STEADY_STATE_TOLERANCE: detect that the steady state is reach in a transient problem when |d_{n} - d_{n-1}| < TOL  OJO: esta bien eso? Only for explicit?? 
        else if(words(1)=='STEAD') then         ! Steady state tolerance
           sstol_sld = param(1)

        else if(words(1)=='NOSTE') then         ! Force no steady state check 
           sstol_sld = -1.0_rp

        else if(words(1)=='RAYLE') then         ! Rayleigh damping

           call runend('SLD_REANUT: DEPRECATED - RAYLEIGH NOW GOES TO PROPERTIES SECTION')

!!!           kfl_dampi_sld= 1
!!!           dampi_sld(1)  = getrea('RAYAL',0.00_rp,'Rayleigh dumping alpha factor')
!!!           dampi_sld(2)  = getrea('RAYBE',0.00_rp,'Rayleigh dumping beta factor')

!           freq1  = getrea('SHORT',0.00_rp,'Long wavelength')
!           freq2  = getrea('LONGW',0.00_rp,'Short wavelength')
!           freq1  = 1.0_rp / freq1
!           freq2  = 1.0_rp / freq2
!           damp1  = getrea('DAMP1',0.00_rp,'First critical damping')
!           damp2  = getrea('DAMP2',0.00_rp,'Second critical damping')
!
!           dampi_sld(2) = (damp2 - damp1) / (freq2 - freq1)
!           dampi_sld(1) = damp1 - dampi_sld(2) * freq1
!

        else if(words(1)=='ARCLE') then
           kfl_arcle_sld = 1                    ! Arc-length
           deltaE_sld = 0.1                     ! deltaE_0 (van der Meer 2012 in open-hole example) 0.1Nm 
           arcint_sld = 1_ip                     ! Auxiliar integer to define size of structures in arc-length method 

        else if(words(1)=='ENRIC') then         ! Enrichement strategy: XFEM/GFEM
           kfl_xfeme_sld= 1
           kfl_xfcra_sld= 1                     ! Local crack definition (as opposed to level set definition)
           if(exists('X-FEM')) kfl_xfeme_sld= 1     ! Default value
           if(exists('LOCAL')) kfl_xfcra_sld= 1     ! Local crack definition
           if(exists('LEVEL')) kfl_xfcra_sld= 2     ! Level set crack definition
!           call ecoute('sld_reanut')
!           do while(words(1)/='ENDEN')
!              !
!              ! ...
!              !
!              call ecoute('sld_reanut')
!           end do
           
        else if(words(1)=='NORMO') then         ! Norm
           if(exists('L1   ')) then
              kfl_normc_sld = 1
           else if(exists('L-inf')) then
              kfl_normc_sld = 0
           else if(exists('MOMEN')) then
              kfl_normc_sld = 3
           else if(exists('ALGEB')) then
              kfl_normc_sld = 4
           end if

        else if(words(1)=='MEANI') then         ! Meaningful value of stress/force
           meanf_sld = param(1)

!        else if(words(1)=='LINEA') then
!           if(exists('PICAR')) then
!              kfl_linea_sld = 1
!              miinn_sld     = getint('PICAR',0_ip,'Number of Picard Linearization iterations')
!              cotol_sld     = getrea('TOLER',0.0001_rp,'Tolerance of Picard Linearization iterations')
!           else if(words(2)=='NEWTO') then
!              kfl_linea_sld = 2
!              miinn_sld     = getint('NEWTO',0_ip,'Number of Newton Linearization iterations')
!           end if


        else if(words(1)=='RELAX') then         ! Relaxation factor
           relfa_sld  = param(1)

        else if(words(1)=='LIMIT') then         ! Limiter
           kfl_limit_sld = 1_ip

        ! ADOC[1]> CAVITY_VOLUME int int int ...                                     $ AlyaRED: calculate the volume of the ventricular cavities
        ! ADOC[d]> CAVITY_VOLUME: NB ID1 ID2  :NB=total number of cavity ; ID=id number of the cavity, has defined with the SETS
        else if(words(1)=='CAVIT') then         ! Calculate the volume of the cavity (e.g. for Winkessel) (CAVITY_VOLUME)
           kfl_volca_sld = 1_ip
           mcavi_sld = getint('TOTAL',1_ip,'Number of cavities to compute the volume')
           call ecoute('sld_reanut')
           do while(words(1)/='ENDCA')
!              if (words(1)=='VENTR') then  
!                 mcavi_sld= int(param(1))               ! Number of cavities 
              if (words(1)=='ORIGI') then  
                 do icavi=1,mcavi_sld
                    i= (icavi-1) * ndime + 1
                    ocavi_sld(1:ndime,icavi)= param(i:i+ndime-1)
                 end do
              else if (words(1)=='BOUND') then          ! Boundary set that defines de inner surface 
                 do icavi=1,mcavi_sld
                    iocav_sld(icavi)= int(param(icavi))
                 end do
              else if (words(1)=='MESHN') then          ! MESH_NODE
                 call runend('SLD_REANUT: MESH NODE FOR CAVITIES IS DEPRECATED. USE ORIGIN.')
              end if

              call ecoute('sld_reanut')
           end do

        ! ADOC[1]> MAXIMUM_ITERATION:    int                                         $ Max nb of Newton-Raphson iterations (only for implicit)
        else if(words(1)=='MAXIM') then         ! Linearization iterations
           miinn_sld  = int(param(1))

        ! ADOC[1]> CONVERGENCE:   OJO. "CONVERGENCE_TOLER:    1.0E-3"  or "CONVERGENCE:ITERA=1000,TOLER=1.0e-06,ADAPTI,RATIO=0.01"  ¿?¿?¿?
        else if(words(1)=='CONVE') then         ! Convergence tolerance
           cotol_sld = param(1)

        else if(words(1)=='SHELL') then
           do while( words(1) /= 'ENDSH' )
              if(words(1)=='ALGEB') then
                 solve_sol => solve(2:)
                 call reasol(1_ip)
              end if
              call ecoute('SLD_REANUT')
           end do
           solve_sol => solve(1:)

        else if(words(1)=='ALGEB') then
           call reasol(1_ip)

        else if(words(1)=='PRECO') then
           call reasol(2_ip)

        ! ADOC[1]> ROTATION TO REFERENCE CONFIGURATION (PUSH FORWARD): ON/OFF
        else if(words(1)=='ROTAT')then
           if(words(2)=='ON') kfl_gdepo_sld = 1_ip

        end if

     end do
     ! ADOC[0]> END_NUMERICAL_TREATMENT        
     
     !
     ! Final compatibility corrections
     !

     if (kfl_timet_sld==1) then
        miinn_sld     = 1
     end if

     if (kfl_plane_sld == 1) then
        do imate= 1,nmate
           if (lawst_sld(imate) .ne. 100) then
              call runend("SLD_REANUT: PLANE STRAIN MODEL ONLY PROGRAMMED FOR ISOLIN MATERIALS.")
           end if
        end do
     end if


  end if


end subroutine sld_reanut
