subroutine sld_elmpre(&
     ielem,pnode,pgaus,pmate,eldis,elvel,eldix,elvex,elcod,&
     elmof,elrst,gpsha,gpcar,gphea,gpgi0,gpdet,gpvol,elepo,gpvel,&
     gprat,gpdis,gpcau,gpigd,gpcod,gpgdi,gpmof,gprst,gpdet_min,&
     ielem_min)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_elmpre
  ! NAME
  !    sld_elmpre
  ! DESCRIPTION
  !    Compute some Gauss values
  ! INPUT
  !    GPGI0 ... Previous deformation gradient tensor ............ F(n)
  ! OUTPUT
  !    GPDIS ... Deformation ..................................... phi (phidot is elvel)
  !    GPVEL ... Deformation velocity............................. phidot
  !    GPGDI ... Updated deformation gradient .....................F(n+1) = grad(phi)
  !    GPIGD ... Inverse of updated deformation gradient tensor .. F^{-1}(n+1)
  !    GPRAT ... Rate of Deformation   ........................... Fdot = grad(phidot)
  !    GPCAU ... Right Cauchy-Green deformation tensor ........... C = F^t x F
  !    GPCOD ... Physical coordinates of the gauss points
  !    GPRST ... Residual stresses
  ! USES
  ! USED BY
  !    sld_elmope
  !***
  !-----------------------------------------------------------------------

  use def_kintyp, only       :  ip,rp,lg
  use def_domain, only       :  mnode,ndime,mgaus,lnods
  use def_solidz, only       :  volum_sld, kfl_aleso_sld,aleso_sld
  use def_solidz, only       :  kfl_quali_sld,quali_sld,kfl_xfeme_sld
  use def_solidz, only       :  kfl_gdepo_sld,covar_sld,kfl_coupt_sld
  use def_solidz, only       :  kfl_moduf_sld,nvgij_inv_sld,kfl_restr_sld,kfl_prest_sld
  use def_master, only       :  kfl_paral,coupling, ittim
  implicit none
  integer(ip), intent(in)    :: ielem
  integer(ip), intent(in)    :: pnode
  integer(ip), intent(in)    :: pgaus
  integer(ip), intent(in)    :: pmate
  real(rp),    intent(in)    :: eldis(ndime,pnode,*)     
  real(rp),    intent(in)    :: elvel(ndime,pnode)
  real(rp),    intent(in)    :: elrst(    6,pnode)
  real(rp),    intent(in)    :: eldix(ndime,pnode,*)
  real(rp),    intent(in)    :: elvex(ndime,pnode)
  real(rp),    intent(in)    :: elcod(ndime,pnode)
  real(rp),    intent(in)    :: elmof(pnode)
  real(rp),    intent(in)    :: gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)    :: gphea(pnode,pgaus)
  real(rp),    intent(in)    :: gpgi0(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpdet(pgaus)
  real(rp),    intent(inout) :: gpvol(pgaus)
  real(rp),    intent(out)   :: elepo(ndime,ndime,pnode)
  real(rp),    intent(out)   :: gpvel(ndime,pgaus,*)
  real(rp),    intent(out)   :: gprat(ndime,ndime,pgaus)
  real(rp),    intent(out)   :: gpdis(ndime,pgaus,*)       !< Deformation: phi
  real(rp),    intent(out)   :: gpcau(ndime,ndime,pgaus)   !< Right Cauchy-Green deformation tensor: C = F^t x F
  real(rp),    intent(out)   :: gpigd(ndime,ndime,pgaus)
  real(rp),    intent(out)   :: gpcod(ndime,mgaus)
  real(rp),    intent(out)   :: gpgdi(ndime,ndime,pgaus)
  real(rp),    intent(out)   :: gpmof(pgaus)
  real(rp),    intent(out)   :: gprst(ndime,ndime,pgaus)
  real(rp),    intent(out)   :: gpdet_min
  integer(ip), intent(out)   :: ielem_min
  real(rp)                   :: detme,volel,bidon,xmean
  integer(ip)                :: inode,igaus,idime,jdime 
  integer(ip)                :: kdime,ipoin,kauxi,ivoig
  real(rp)                   :: gpig0(ndime,ndime), gpgd1(ndime,ndime)
  real(rp)                   :: gpde0
  !
  ! GPDIS and GPVEL: displacement and velocity
  !
  kauxi= 0
  do idime = 1,ndime
     do igaus = 1,pgaus
        gpcod(idime,igaus)   = 0.0_rp
        gpdis(idime,igaus,1) = 0.0_rp
        gpdis(idime,igaus,2) = 0.0_rp
        gpdis(idime,igaus,3) = 0.0_rp
        gpvel(idime,igaus,1) = 0.0_rp
        do inode = 1,pnode
           gpdis(idime,igaus,1) = gpdis(idime,igaus,1) + eldis(idime,inode,1)*gpsha(inode,igaus)
           gpdis(idime,igaus,2) = gpdis(idime,igaus,2) + eldis(idime,inode,2)*gpsha(inode,igaus)
           gpvel(idime,igaus,1) = gpvel(idime,igaus,1) + elvel(idime,inode)*gpsha(inode,igaus)
           gpcod(idime,igaus)   = gpcod(idime,igaus)   + elcod(idime,inode)*gpsha(inode,igaus)
        end do
        do jdime = 1,ndime
           gprst(idime,jdime,igaus) = 0.0_rp
           gpgdi(idime,jdime,igaus) = 0.0_rp
           gprat(idime,jdime,igaus) = 0.0_rp
        end do
     end do
  end do
  !
  ! GPDIS and GPVEL: displacement and velocity
  ! to include the contribution of the enrichment dofs
  !
  if (kfl_xfeme_sld == 1) then
     do idime=1,ndime
        do igaus=1,pgaus
           do inode=1,pnode
              gpdis(idime,igaus,1)=gpdis(idime,igaus,1)&
                   +eldix(idime,inode,1)*gpsha(inode,igaus)*gphea(inode,igaus)
              gpdis(idime,igaus,2)=gpdis(idime,igaus,2)&
                   +eldix(idime,inode,2)*gpsha(inode,igaus)*gphea(inode,igaus)
              gpvel(idime,igaus,1)=gpvel(idime,igaus,1)&
                   +elvex(idime,inode)*gpsha(inode,igaus)*gphea(inode,igaus)
           end do
        end do
     end do
  end if

  !
  ! GPGDI: Deformation gradients 
  !
  if (kfl_prest_sld == 0) then
     do igaus=1,pgaus
        do idime=1,ndime
           do jdime=1,ndime
              do inode=1,pnode
                 gpgdi(idime,jdime,igaus)=gpgdi(idime,jdime,igaus)&
                      +eldis(idime,inode,1)*gpcar(jdime,inode,igaus)
                 gprat(idime,jdime,igaus)=gprat(idime,jdime,igaus)&
                      +elvel(idime,inode)*gpcar(jdime,inode,igaus)
              end do
           end do
        end do
     end do
  end if

  !
  !PRESTRESS
  !
  if (kfl_prest_sld == 1) then
     do igaus=1,pgaus
        if (ittim < 3) then
           do idime=1,ndime
              do jdime=1,ndime
                 do inode=1,pnode
                    gpgdi(idime,jdime,igaus)=gpgdi(idime,jdime,igaus)&
                         +eldis(idime,inode,1)*gpcar(jdime,inode,igaus)
                 end do
              end do
           end do
           gpgdi(1,1,igaus)= gpgdi(1,1,igaus) + 1.0_rp
           gpgdi(2,2,igaus)= gpgdi(2,2,igaus) + 1.0_rp
           if (ndime==3) gpgdi(3,3,igaus)= gpgdi(3,3,igaus) + 1.0_rp
        else
           call invmtx(gpgi0(1,1,igaus),gpig0(1,1),gpde0,ndime)
           do idime=1,ndime
              do jdime=1,ndime
                 gpgd1(idime,jdime) =0.0_rp
                 do inode=1,pnode
                    gpgd1(idime,jdime)=gpgd1(idime,jdime)&
                         +eldis(idime,inode,1)*gpcar(jdime,inode,igaus)/gpde0
                 end do
              end do
           end do
           gpgd1(1,1)= gpgd1(1,1) + 1.0_rp
           gpgd1(2,2)= gpgd1(2,2) + 1.0_rp
           if (ndime==3) gpgd1(3,3)= gpgd1(3,3) + 1.0_rp
           do idime=1,ndime
              do jdime=1,ndime
                  do kdime=1,ndime  
                     gpgdi(idime,jdime,igaus) = gpgdi(idime,jdime,igaus) + gpgd1(idime,kdime)*gpgi0(kdime,jdime,igaus)  
                  end do
              end do
           end do
        end if
     end do
  endif

  !
  ! GPRST: Residual stresses
  !
  if (kfl_restr_sld < 0) then
     do igaus=1,pgaus
        do idime=1,ndime
           do jdime=1,ndime
              do inode=1,pnode
                 ivoig= nvgij_inv_sld(idime,jdime)
                 gprst(idime,jdime,igaus)=gprst(idime,jdime,igaus)&
                      +elrst(ivoig,inode)*gpsha(inode,igaus)
              end do
           end do
        end do
     end do
  end if
  !
  ! GPGDI: Deformation gradients for xfem case
  ! to include the contribution of the enrichment dofs  !
  if (kfl_xfeme_sld == 1) then
     do igaus=1,pgaus
        do idime=1,ndime
           do jdime=1,ndime
              do inode=1,pnode
                 gpgdi(idime,jdime,igaus)=gpgdi(idime,jdime,igaus)&
                      +eldix(idime,inode,1)*gpcar(jdime,inode,igaus)*gphea(inode,igaus)
                 gprat(idime,jdime,igaus)=gprat(idime,jdime,igaus)&
                      +elvex(idime,inode)*gpcar(jdime,inode,igaus)*gphea(inode,igaus)
              end do
           end do
        end do
     end do
  end if
  !
  ! GPMOF: Modulating fields, elmof is set to 1 when there are no fields (then, gpmof will be 1)
  !
  if( kfl_moduf_sld(1) > 0 ) then
     do igaus = 1,pgaus
        gpmof(igaus) = 0.0_rp
        do inode = 1,pnode
           gpmof(igaus) = gpmof(igaus) + elmof(inode) * gpsha(inode,igaus)
        end do
     end do
  end if

  detme= 0.0_rp
  volel= 0.0_rp
  gpigd= 0.0_rp

  if (kfl_quali_sld == 1) quali_sld(ielem)= 0.0_rp

  do igaus=1,pgaus
     if(kfl_prest_sld == 0) then   !without prestress 
        gpgdi(1,1,igaus)= gpgdi(1,1,igaus) + 1.0_rp
        gpgdi(2,2,igaus)= gpgdi(2,2,igaus) + 1.0_rp
        if (ndime==3) gpgdi(3,3,igaus)= gpgdi(3,3,igaus) + 1.0_rp
     end if
     !Compute J
     call invmtx(gpgdi(1,1,igaus),gpigd(1,1,igaus),gpdet(igaus),ndime)

     !if( kfl_exacs_sld == 0 ) then

     if( gpdet(igaus) < 1.0e-12_rp ) then
        !write(*,*) 'SLD_ELMPRE:  **WARNING** GPDET < 1.0E-12. Check element ', ielem,' subdomain ', kfl_paral
        !write(*,*) '      value: ',gpdet(igaus)
        !if( kfl_xfeme_sld == 1 ) then
        !   write(*,*) 'The element is enriched'
        !end if
     else if( gpdet(igaus) < 1.0e-4 ) then
        kauxi = 1
     end if

     gpdet_min = min(gpdet_min,gpdet(igaus))

     if( gpdet(igaus) < 0.0_rp ) then
        if( gpdet(igaus) <= gpdet_min ) then
           gpdet_min = gpdet(igaus)
           ielem_min = ielem
        end if
        !write(*,*) &
        !     '----> SLD_ELMPRE:  **ERROR** NEGATIVE GPDET. Check element ', ielem, &
        !     ' subdomain ', kfl_paral
        !write(*,*) &
        !     '      value: ',gpdet(igaus)
        !write(*,*) &
        !     '      Nodal coordinates: '
        !if (kfl_xfeme_sld==1) then
        !   write(*,*) &
        !        'The element is enriched'
        !end if
        !do inode=1,pnode
        !   write(*,*) &
        !        '      node=',inode,'  position=',elcod(1:ndime,inode)
        !end do
        !call runend ('SLD_ELMPRE: NEGATIVE JACOBIAN DETERMINANT')
     end if
     !end if
     !
     ! Witness element (debugging) connected with sld_inivar
     !
     !     if (ielem==kfl_ielem) then
!!!     if (ielem == 1167) then        ! ALE Tezduyar rigidization parameters
!!!        bidon= gpvol(igaus)
!!!        gpvol(igaus) = bidon * (1.0_rp/gpdet(igaus))**2.0_rp
!!!     end if
!!!          if (ielem==1167) then
!!!             write(4112,*) cutim,gpdet(igaus),gpvol(igaus)
!!!          end if

     if (kfl_aleso_sld(pmate) == 1) then        ! ALE Tezduyar rigidization parameters
        bidon= gpvol(igaus)
        gpvol(igaus) = bidon * (aleso_sld(1) / bidon)**aleso_sld(2)
     end if

     volel = volel + gpvol(igaus)
     detme = detme + gpvol(igaus) * gpdet(igaus)

     if (kfl_quali_sld == 1) then
        quali_sld(ielem)= quali_sld(ielem) + gpvol(igaus) * gpdet(igaus)
     end if

     !
     ! GDEPO_SLD: Push forward
     !
     if( kfl_gdepo_sld /= 0 ) then
        do inode = 1,pnode
           ipoin = lnods(inode,ielem)
           xmean = gpsha(inode,igaus) * gpvol(igaus) 
           do idime= 1,ndime
              do jdime= 1,ndime
                 bidon = xmean * gpgi0(idime,jdime,igaus) 
                 elepo(idime,jdime,inode) = elepo(idime,jdime,inode) + bidon
              end do
           end do
        end do
     end if

  end do
  volum_sld(1) = volum_sld(1) + volel         ! reference
  volum_sld(2) = volum_sld(2) + detme         ! deformed

  detme= detme / volel                        ! local ratio

  !
  ! GPCAU: Cauchy tensor
  !

  do igaus=1,pgaus
     do kdime=1,ndime
        do jdime=1,ndime
           gpcau(jdime,kdime,igaus)=0.0_rp
           do idime=1,ndime
              gpcau(jdime,kdime,igaus)= gpcau(jdime,kdime,igaus) + gpgdi(idime,jdime,igaus)*gpgdi(idime,kdime,igaus)
           end do
        end do
     end do
  end do

  if (kfl_coupt_sld(pmate) == 11) then 
     covar_sld(ielem)= 1.0_rp
     if (kauxi==1) covar_sld(ielem)= 0.0_rp
  end if

100 format (2(F16.8,','))
end subroutine sld_elmpre

