subroutine sld_restar(itask)
!------------------------------------------------------------------------
!****f* Solidz/sld_restar
! NAME 
!    sld_restar
! DESCRIPTION
!    This routine reads the initial values from the restart file
! USES
! USED BY
!    sld_turnon
!***
!------------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solidz
  use      mod_postpr
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: icomp,iwopo,kfl_gores
  integer(ip)             :: ielem, ipoin, inode, idama, igaus, i
  integer(ip)             :: pelty, pgaus
  !
  ! Check if restart file should be read or written
  !
  call respre(itask,kfl_gores)
  if( kfl_gores == 0 ) return

  if( itask == READ_RESTART_FILE ) then
     icomp = min(ncomp_sld, 3_ip) !ncomp_sld
  else
     icomp = 1
  end if

  !----------------------------------------------------------------------
  !
  ! Displacement
  !
  !----------------------------------------------------------------------

  iwopo = 1
  gevec => displ(:,:,icomp)
  call postpr(gevec,postp(1)%wopos(1:3,iwopo),ittim,cutim)


  iwopo = 4
  gevec => caust_sld
  call postpr(gevec,postp(1)%wopos(1:3,iwopo),ittim,cutim)

  !----------------------------------------------------------------------
  !
  ! Velocity
  !
  !----------------------------------------------------------------------  
  
  iwopo = 2
  gevec => veloc_sld(:,:,icomp)
  call postpr(gevec,postp(1)%wopos(1:3,iwopo),ittim,cutim)
  !----------------------------------------------------------------------
  !
  ! Acceleration
  !
  !----------------------------------------------------------------------  
  
  iwopo = 3
  gevec => accel_sld(:,:,icomp)
  call postpr(gevec,postp(1)%wopos(1:3,iwopo),ittim,cutim)
  !
  ! STATT variables (for damage models only in 3-D)
  !
  if( ndime > 2 )then
     do iwopo = 5, size(state_sld,1)
        gevec => state_sld(1,:,:,1)
        call postpr(gevec,postp(1)%wopos(1:3,iwopo),ittim,cutim)
     end do
  end if


!  if (kfl_damag_sld == 1) then

!    print*, 'AAAAAAAAA'
!    call postpr(statt,postp(1)%wopos(1:3,46),ittim,cutim,1_ip)  !statt  
!    print*, 'BBBBBBBBB'

!    if(itask == READ_RESTART_FILE .and. INOTMASTER ) then
!      do ielem = 1,nelem
!        pelty = abs(ltype(ielem))
!        pgaus = ngaus(pelty)
!        do igaus = 1,pgaus
!          do i=1,7
!            state_sld(i,igaus,ielem,1) = statt(ielem)%a(i,igaus,1)
!          end do
!        end do
!      end do
!    end if
!   end if


  !----------------------------------------------------------------------
  !
  ! Finish
  !
  !----------------------------------------------------------------------
  call respre(3_ip,kfl_gores)


end subroutine sld_restar
