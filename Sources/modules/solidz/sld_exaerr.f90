subroutine sld_exaerr(itask)
  !-----------------------------------------------------------------------
  !****f* Nstinc/sld_exaerr
  ! NAME 
  !    sld_exacso
  ! DESCRIPTION
  !    This routine computes the FEM errors (referred to an analytical
  !    solution defined in exacso.f90). The errors are normalized by the 
  !    appropriate norm of the exact solution, except when this norm is zero.      
  ! USES
  ! USED BY
  !    sld_output
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master
  use def_solidz
  use def_elmtyp
  implicit none
  integer(ip), intent(in) :: itask
  real(rp)    :: gpcar(ndime,mnode) 
  real(rp)    :: xjaci(ndime,ndime) 
  real(rp)    :: xjacm(ndime,ndime) 
  real(rp)    :: elvel(ndime,mnode)                          ! Gather 
  real(rp)    :: elpre(mnode)
  real(rp)    :: ellev(mnode)
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: gpvol(mgaus),detjm                          ! Values at Gauss points
  real(rp)    :: gpvis(mgaus),gpden(mgaus),gppor(mgaus)
  real(rp)    :: gpgvi(ndime,mgaus)
  real(rp)    :: gpvel(ndime),grave(ndime,ndime)
  real(rp)    :: prgau,gradp(ndime),gpcod(ndime)
  real(rp)    :: exvel(ndime),exveg(ndime,ndime)
  real(rp)    :: expre,exprg(ndime),sgsno,velno
  real(rp)    :: difeu,abvel,diffp,abpre,dummr(3)
  real(rp)    :: erp01(2),erp02(2),erp0i(2),erp11(2),erp12(2),erp1i(2)
  real(rp)    :: eru01(2),eru02(2),eru0i(2),eru11(2),eru12(2),eru1i(2) 
  real(rp)    :: gphes(ntens,mnode,mgaus)                    ! d2N/dxidxj
  integer(ip) :: ielem,inode,ipoin,igaus,idime,jdime,kpoin,izone         ! Indices and dimensions
  integer(ip) :: pnode,pgaus,pelty,dummi,ibopo
  real(rp)    :: gpdum(max(mnode,mgaus))
  real(rp)    :: u(ndime)                             ! u(i) = ui
  real(rp)    :: dudx(ndime,ndime)                    ! dudx(i,j) = duj/dxi
  real(rp)    :: d2udx2(ndime,ndime,ndime)            ! d2udx2(i,j,k) = d^2uk/dxidxj

  if( kfl_exacs_sld /= 0 ) then

     if( itask == 1 .and. INOTMASTER ) then
        !
        ! Impose exact Dirichlet boundary and initial condition
        !
        izone = lzone(ID_SOLIDZ)
        do kpoin = 1,npoiz(izone); ipoin = lpoiz(izone) % l(kpoin)
           ibopo = lpoty(ipoin)
           call sld_exacso(1_ip,coord(1,ipoin),u,dudx,d2udx2,dummr,dummr)
           !if( kfl_fixno_sld(idime,ipoin) > 0 ) then
           !   do idime = 1,ndime
           !      bvess_sld(idime,ipoin,1) = u(idime)
           !      bvess_sld(idime,ipoin,2) = u(idime)
           !   end do
           !end if
           do idime = 1,ndime
              if( kfl_fixno_sld(idime,ipoin) > 0 ) then
                 bvess_sld(idime,ipoin,1) = u(idime)
                 bvess_sld(idime,ipoin,2) = u(idime)
                 if( ibopo > 0 ) kfl_fixno_sld(idime,ipoin) = 1
              end if
           end do
        end do

     else
        !
        ! Compute errors
        !
        err01_sld = 0.0_rp
        err02_sld = 0.0_rp
        err0i_sld = 0.0_rp
        err11_sld = 0.0_rp
        err12_sld = 0.0_rp
        err1i_sld = 0.0_rp
        if( INOTMASTER ) call sld_elmope(8_ip)

        call pararr('SUM',0_ip,2_ip,err01_sld)
        call pararr('SUM',0_ip,2_ip,err02_sld)
        call pararr('MAX',0_ip,2_ip,err0i_sld)
        call pararr('SUM',0_ip,2_ip,err11_sld)
        call pararr('SUM',0_ip,2_ip,err12_sld)
        call pararr('MAX',0_ip,2_ip,err1i_sld)

        if( INOTSLAVE ) then

           err02_sld(1) = sqrt(err02_sld(1))
           err12_sld(1) = sqrt(err12_sld(1))
           err02_sld(2) = sqrt(err02_sld(2))
           err12_sld(2) = sqrt(err12_sld(2))

           if( err01_sld(2) > 0.0_rp ) err01_sld(1) = err01_sld(1) / err01_sld(2) 
           if( err02_sld(2) > 0.0_rp ) err02_sld(1) = err02_sld(1) / err02_sld(2)
           if( err0i_sld(2) > 0.0_rp ) err0i_sld(1) = err0i_sld(1) / err0i_sld(2) 
           if( err11_sld(2) > 0.0_rp ) err11_sld(1) = err11_sld(1) / err11_sld(2) 
           if( err12_sld(2) > 0.0_rp ) err12_sld(1) = err12_sld(1) / err12_sld(2)
           if( err1i_sld(2) > 0.0_rp ) err1i_sld(1) = err1i_sld(1) / err1i_sld(2) 

           write(momod(modul)%lun_outpu,100)                    &
                &       ittim,itcou,                            &
                &       err01_sld(1),err02_sld(1),err0i_sld(1), &
                &       err11_sld(1),err12_sld(1),err1i_sld(1)
        end if

     end if
  end if

100 format(///,10X,'FINITE ELEMENT ERRORS',                                    &
       &              /,10X,'=====================',//,                        &
       &  '          TIME STEP NO.',I5,',  ITERATION NO. ',I5,/,10X,20('-'),/, &
       &  '          NORM    DISPLACEMENT               ',/,10X,20('-'),/,     &
       &  '          W(0,1) ',E12.5,/, &
       &  '          W(0,2) ',E12.5,/, &
       &  '          W(0,i) ',E12.5,/, &
       &  '          W(1,1) ',E12.5,/, &
       &  '          W(1,2) ',E12.5,/, &
       &  '          W(1,i) ',E12.5,/,10X,20('-'))

end subroutine sld_exaerr
