!
!* SUB. SLD_STRESS_MODEL_152
!    31/07/2015 : sub. sm152_get_material_properties to read the material properties
!    31/07/2015 : sub. sm152_get_length to compute the characteristics element length
!    31/07/2015 : sub. sm152_stiffness_tensor to compute the stiffness tensor using
!    23/09/2015 : add HM and SM (where M = LT, LC, G, 6) to avoid errors in eltan computation
!                 COMMENT all strength reduction
!    24/09/2015 : redifinition of Ctan and elimination of auxS variables. 
!
!* SUB. SM152_PRECALCULUS
!    31/07/2015 : creation of the function
!
!* SUB. SM152_GET_MATERIAL_PROPERTIES:
!    31/07/2015 : creation of the function
!    22/09/2015 : add hygrothermal properties
!    23/09/2015 : add HM and SM (where M = LT, LC, G, 6) to avoid errors in eltan computation
!
!* SUB. SM152_STIFFNESS_TENSOR
!    31/07/2015 : creation of the function
!
!* SUB. SM152_FIND_A_B
!    29/07/2015 : sub rootfind_bracke to bracketing the solution in the interval [0,X]
!    29/07/2015 : sub rootfind_bisect to refining the solution in the interval [x1,x2]
!    29/07/2015 : sub rootfind_secant to find the A to dissipate g1T for a given l and B
!    28/07/2015 : fun integ_eneDis to compute the difference between the desired g1T and 
!                     the obtained by setting a A, B and element length
!    28/07/2015 : fun ratio_eneDis to compute the ratio of transversal dissipated energy 
!

! ##################################################################################
! SUBROUTINE SLD_STRESS_MODEL_152
!
subroutine sld_stress_model_152(pgaus, pmate, gpgdi, gpstr, ielem, flagt, gpdds)

  !---------------------------------------------------------------------------------
  !> @addtogroup Solidz
  !> @{
  !> @file    sld_stress_model_152.f90
  !> @author  Adria Quintanas (adria.quintanas@udg.edu)
  !> @date    October 2014
  !> @brief   
  !> @details Based on the model of Pere Maimi � (pere.maimi@udg.edu)
  !> @        Uses:
  !> @           stiffnTensor     :: compute the stiffness tensor.
  !> @} 
  !---------------------------------------------------------------------------------

  ! ================================================================================
  ! INIT
  ! --------------------------------------------------------------------------------
  use def_kintyp, only      :  ip, rp, lg
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use def_elmtyp

  implicit none

  ! --------------------------------------------------------------------------------
  integer(ip), intent(in)                  :: &
     pgaus,                                   & ! Number of gauss points
     pmate,                                   & ! Current material number
     ielem,                                   & ! Current element number
     flagt                                      ! Integration scheme flag: 
                                                !   0: explicit / 1: implicit

  real(rp),    intent(in)                  :: &
     gpgdi(ndime,ndime,pgaus)                   ! Displacement gradient

  real(rp),    intent(out)                 :: &
     gpstr(ndime,ndime,pgaus),                & ! 2nd Piola-Kirchoff stresses tensor
     gpdds(ndime,ndime,ndime,ndime,pgaus)       ! 2nd elasticity tensor

  ! --------------------------------------------------------------------------------
  logical(lg)                              :: &
     flaLT, flaLC, flaT                        ! Damage flags
                                             
  integer(ip)                              :: &
     igaus,i,j,k,l,p,q,ivoig                   ! Index
                                             
  real(rp)                                 :: &  
     E11, E22, EKT, EGT, G12, v12, v21, v23,  & ! Material properties (- elastic)
     xT, xC, yT, yC, sL, sT,                  & !   - Strength
     aT, bT, nT, nS, nTQ, nSQ,                & !   - Friction and adjust
     gLT, gLC, g1T, g2T, g2L,                 & !   - Fracture toughens 
     hLT1, hLT2, sLT1, sLT2,                  & !   - Slopes, ordinates and inflection
     hLC1, hLC2, sLC1, sLC2,                  &
     hG1, sG1, h61, s61,                      &
     a11, a22, b11, b22,                      & !   - Hygrothermal
     pT,  tT,  tLT,                           & ! Stress invariants
     phiLT, phiLC, phiT,                      & ! Loading functions
     rLT,  rLC,  rT,                          & ! Internal variables
     rLT1, rLC1,                              &
     d1,  dK,  dG, d6,                        & ! Damage variables 
     hLT, hLC, hG, h6,                        & 
     sLT, sLC, sG, s6,                        &
     A,   B,                                  & ! Auxiliar damage variables
     celen,                                   & ! Element legth
     traMa(3,3),                              & ! Transformation matrix
!    gpgret(ndime,ndime,pgaus),               & ! Strains tensor (hygrothermal)   
     gpgre(ndime,ndime,pgaus),                & ! Strains tensor (mechanical)
     vogre(6),                                & ! Strains tensor in Voigt notation
     vostr(6,2),                              & ! Stresses tensor in Voigt notation:
                                                !   vostr(:,1) -> effective
                                                !   vostr(:,2) -> nominal
     stiff(6,6,2),                            & ! Stiffness tensor in Voigt form:   
                                                !   stiff(:,1) -> undamaged (precal) 
                                                !   stiff(:,2) -> damaged
!    alpha(3,3), beta(3,3),                   & ! Hygrothermal tensor in Voigt form.
     dY1ds11, dYGds22, dYGds33, dYKds22,      & ! 2nd elasticity tensor stuff 
     dYKds33, dYGds23, dY6ds12, dY6ds13,      &
     dd1drL, ddGdrT, ddKdrT, dd6drT,          &
     drLde(6), drTde(6), drLdS(6), drTdS(6),  &
     tanM(6,6), eltan(6,6),                   &
     dummr, auxS1, auxS2, auxS3, auxV1(6),    & ! Auxiliary variables
     auxM1(6,6),                              &
     ca1, ca2, ca3, cb1, cb2, cb3, cc1,       & 
     cb1d, cb2d, cb3d,                        &     
     tkron(ndime*2,ndime*2)                     ! Kronecker delta
  ! =============================================================|    INIT    |=====

  ! ================================================================================
  ! MAIN
  ! --------------------------------------------------------------------------------
  ! INITIALIZE VARIABLES
  ! 
  ! Get the material properties
  !
  call sm152_get_properties( pmate,                                                &
                           & E11, E22, EKT, EGT, G12, v12, v21, v23,               &
                           & xT, xC, yT, yC, sL, sT, aT, bT, nT, nS, nTQ, nSQ,     &
                           & gLT, gLC, g1T, g2T, g2L,                              & 
                           & hLT1, sLT1, hLT2, sLT2,                               &
                           & hLC1, sLC1, hLC2, sLC2,                               &
                           & hG1, sG1, h61, s61,                                   &
                           & a11, a22, b11, b22)
  


!
  ! Get the element length, A and B
  !
  celen = state_sld( 8, 1, ielem, 2)
  A     = state_sld( 9, 1, ielem, 2)
  B     = state_sld(10, 1, ielem, 2)


  !only for debugging arclength
  if (celen < 1.0E-12_rp) then
     ! Only computed once
     call sm153_get_length(ielem, celen)
     state_sld(8, 1, ielem, 2)  = celen
     state_sld(9, 1, ielem, 2)  = 1.0E+9_rp
     state_sld(10, 1, ielem, 2) = EGT/EKT
     A = state_sld( 9, 1, ielem, 2)
     B = state_sld(10, 1, ielem, 2)     
  endif


  ! 
  ! Material coordinate system
  ! 
  if ( kfl_fiber_sld == 5_ip ) then
     traMa = 0.0_rp
     do i = 1, 3
        traMa(1, i) = real(fibeb_sld(i, ielem),rp)
        traMa(2, i) = real(fibes_sld(i, ielem),rp)
        traMa(3, i) = real(fiben_sld(i, ielem),rp)
     enddo
  endif
  !
  ! Undamaged stiffness tensor
  !
  stiff(:, :, 1) = stiff0_sld(:, :, pmate)
  ! 
  ! Thermal expansion tensor 
  !
!  alpha = 0.0_rp
!  alpha(1, 1) = a11
!  alpha(2, 2) = a22
!  alpha(3, 3) = a22
  !
  ! Moisture tensor
  !
!  beta = 0.0_rp
!  beta(1, 1) = b11
!  beta(2, 2) = b22
!  beta(3, 3) = b22
  !
  ! Kronecker delta
  !
  tkron = 0.0_rp
  do i = 1, 6
     tkron(i, i) = 1.0_rp
  enddo
  ! --------------------------------------------------------------------------------
  ! LOOP OVER GAUSS POINTS
  !
  !...| Do gauss points |...........................................................
  do igaus = 1, pgaus  

    ! ------------------------------------------------------------------------------
    ! STRAINS
    !
    ! Green-Lagrange strain tensor
    !   E_ij = 0.5(F_ki*F_kj-Kro_ij)      (Ref: Holzapfel eq. 2.69)
    !
!    do i = 1, 3
!      do j = 1, 3
!         dummr = 0.0_rp
!         do k = 1, 3
!            dummr = dummr + gpgdi(k, i, igaus) * gpgdi(k, j, igaus)
!         enddo
!         gpgre(i, j, igaus) = 0.5_rp*( dummr - tkron(i, j) )
!          ! Add only thermal effects in the first time step
!          if (cutim <= deltt_sld(1)) then
!             gpgre(i, j, igaus) = gpgre(i, j, igaus) - alpha(i, j)*deltt_sld(2) 
!          endif
!      enddo
!   enddo
    !
    ! Small strain tensor
    !
    gpgre(:, :, igaus) = 0.0_rp
    do i = 1, 3
      do j = 1, 3
        gpgre(i, j, igaus) = gpgre(i, j, igaus) + 0.5_rp*(gpgdi(i, j, igaus) +     &
        &                    gpgdi(j, i, igaus)) - tkron(i, j)
      enddo
    enddo
    !
    ! Green-Lagrange strains tensor in compacted form using the Voigt notation
    !   E_a = E_ij             / (1)-(1,1), (2)-(2,2), (3)-(3,3), 
    !                            (4)-(2,3), (5)-(1,3), (6)-(1,2)
    !
    vogre = 0.0_rp
    do i = 1, 3
       do j = 1, 3
          ivoig = nvgij_inv_sld(i, j)
          vogre(ivoig) = vogre(ivoig) + gpgre(i, j, igaus)
       enddo
    enddo  
    !
    ! Rotate from the global to the material coordinate system
    !
    if ( kfl_fiber_sld == 5_ip) then
       auxV1 = vogre
       call sld_travo2(2_ip, auxV1, traMa, vogre, 3_ip)
    endif
    ! ------------------------------------------------------------------------------
    ! EFFECTIVE STRESS TENSOR & STRESS INVARIANTS
    !
    ! 
    ! 2nd Piola-Kirchhoff stress tensor in compacted form using the Voigt notation
    !   {S}_i = [C]_ij*{E}_j   / (1)-(1,1), (2)-(2,2), (3)-(3,3)
    !                            (4)-(2,3), (5)-(1,3), (6)-(1,2)
    !
    vostr = 0.0_rp
    do i = 1, 6
       do j = 1, 6
          vostr(i, 1) = vostr(i, 1) + stiff(i, j, 1)*vogre(j)
       enddo
    enddo
    !
    !  Compute the invariants
    !
    pT  = 0.5_rp*(vostr(2, 1) + vostr(3, 1))
    tT  = 0.5_rp*dsqrt((vostr(2, 1) - vostr(3, 1))**2.0_rp + 4.0_rp*vostr(4, 1)    &
    &     **2.0_rp)
    tLT = dsqrt(vostr(5, 1)**2.0_rp + vostr(6, 1)**2.0_rp)
    ! -----------------------------------------------------------------------------
    ! STRENGTH PROPERTIES REDUCTION
    !
    ! Fiber. Tensile 
    !
    if ((2.0_rp*gLT*E11 - celen*xT**2.0_rp) .lt. 0.001) then
       call runend('SM152: ELEMENT LENGTH TO LARGE TO AVOID SNAPBACK LT')
    endif

    !
    ! Fiber. Compression
    !
    if ((2.0_rp*gLC*E11 - celen*xC**2.0_rp) .lt. 0.001) then
       call runend('SM152: ELEMENT LENGTH TO LARGE TO AVOID SNAPBACK LC')
    endif
  
    !
    ! Matrix. Transversal mode II
    !
    if ((2.0_rp*g1T*E22 - celen*yT**2.0_rp) .lt. 0.001) then
       call runend('SM152: ELEMENT LENGTH TO LARGE TO AVOID SNAPBACK TG')
    endif
  
    !
    ! Matrix. d6
    !
    if ((2.0_rp*g2L*G12 - celen*sL**2.0_rp) .lt. 0.001) then
       call runend('SM152: ELEMENT LENGTH TO LARGE TO AVOID SNAPBACK T6')
    endif
    ! ------------------------------------------------------------------------------
    ! LOADING FAILURE FUNCTION (phi_M)
    !
    ! Longitudinal failure
    !
    phiLT = 0.0_rp
    phiLC = 0.0_rp
    if (vostr(1, 1) .gt. 0.0_rp) then
       !
       ! tensile
       !
       phiLT = E11*vogre(1)/xT
  
    else
       !
       ! Compression
       !
       phiLC = (dsqrt(vostr(1, 1)**2.0_rp + nTQ*pT**2.0_rp + nSQ*tLT**2.0_rp)      &
       &        + nT*pT + nS*tLT)/xC
  
    endif
    !
    ! Transverse failure
    !
    phiT = 0.0_rp
    phiT = dsqrt(((yT + yC)**2.0_rp*(tT**2.0_rp + aT*pT**2.0_rp))/((yT*yC)         &
    &      **2.0_rp*(1.0_rp + aT)) + (bT*tLT/sL)**2.0_rp) + ((yC - yT)*pT)/yT/yC   &
    &      + (1.0_rp - bT)*tLT/sL
    ! ------------------------------------------------------------------------------
    ! INTERNAL DAMAGE LAWS (r_M)
    !
    ! Longitudinal tensile
    !
    flaLT = .false.
    rLT = max(1.0_rp, state_sld(2, igaus, ielem, 1))
    if (phiLT .gt. rLT) then
       rLT = phiLT
       flaLT = .true.
    endif
    !
    ! Longitudinal compression
    !
    flaLC = .false.
    rLC = max(1.0_rp, state_sld(1, igaus, ielem, 1))
    if (phiLC .gt. rLC) then
       rLC = phiLC
       flaLC = .true.
       if (rLC .gt. rLT) then
          rLT = rLC
       endif
    endif
    !
    ! Transverse
    !
    flaT  = .false.
    rT  = max(1.0_rp, state_sld(3, igaus, ielem, 1))
    if (phiT .gt. rT) then
       rT   = phiT
       flaT = .true.
    endif
    ! ------------------------------------------------------------------------------
    ! DAMAGE VARIABLES (d_M)
    !
    ! Fiber (d1)
    !
    d1 = 0.0_rp
    if (vostr(1, 1) .gt. 0.0_rp .and. rLT .gt. 1.0_rp) then
       !
       ! Longitudinal tensile
       !
       rLT1 = ((sLT1 - sLT2)*E11 + (hLT1*sLT2 - hLT2*sLT1)*celen)/((hLT1 - hLT2)   &
       &      *celen*sLT1)
       if (rLT .lt. rLT1) then
           hLT = hLT1
           sLT = sLT1
       else
           hLT = hLT2
           sLT = sLT2
       end if
       d1 = min(1.0_rp, (E11/(E11 - hLT*celen))*(1.0_rp - sLT/(rLT*sLT1)))
    elseif (vostr(1, 1) .le. 0.0_rp .and. rLC .gt. 1.0_rp) then
       !
       ! Longitudinal compression
       !
       ca1 = 1.0_rp/(((hLC1*sLC2 - hLC2*sLC1)*celen)/(E11*(sLC1 - sLC2))+ 1.0_rp)
       ca2 = 2.0_rp*v12*v21 + v23 - 1.0_rp
       rLC1 = -((sLC1 - sLC2)*E11 + (hLC1*sLC2 - hLC2*sLC1)*celen)/((hLC1 - hLC2)  &
       &       *celen*sLC1)*(1.0_rp/ca2)*(dsqrt(ca2**2.0_rp - 4.0_rp*v12*v21*ca2   &
       &       *ca1 + (nTQ + 4.0_rp*v12**2.0_rp)*(v21*ca1)**2.0_rp) - nT*v21*ca1)
       if (rLC .lt. rLC1) then
           hLC = hLC1
           sLC = sLC1
       else
           hLC = hLC2
           sLC = sLC2
       end if
       ca1  = 1.0 - celen*hLC/E11
       ca2  = v23 + 2.0_rp*v12*v21 - 1.0_rp
       ca3  = sLC1/sLC
       cb1  = (nT*v21 + ca1*ca2*ca3*rLC)**2.0_rp - (4.0_rp*(v12**2.0_rp) + nTQ)    &
       &      *(v21**2.0_rp)
       cb2  = 2.0_rp*ca2*(2.0_rp*v12*v21 - (nT*v21 + ca1*ca2*ca3*rLC)*ca3*rLC) 
       cb3  = (ca2**2.0_rp)*((ca3*rLC)**2.0_rp - 1.0_rp)
       d1 = min(1.0_rp,(-cb2 - dsqrt((cb2**2.0_rp) - 4.0_rp*cb1*cb3))/(2.0_rp*cb1))
    endif
    !
    ! Matrix (dG, dK and d6)
    !
    dG = 0.0_rp
    dK = 0.0_rp
    d6 = 0.0_rp
    if (rT .gt. 1.0_rp) then
      !
      ! dG
      !
      hG = hG1
      sG = sG1
      dG = min(1.0_rp, (EGT/(EGT - hG*celen))*(1.0_rp - sG/(rT*sG1)))    
      !
      ! dK
      !
      if (pT .ge. 0.00000001_rp) then
        dK = min(1.0_rp, 1.0_rp - (1.0_rp - dG)*(B*(rT - 1.0_rp) + A + 1.0_rp)     &
        &   /(rT + A))
      endif     
      !
      ! d6
      !
      h6 = h61
      s6 = s61
      d6 = min(1.0_rp, (G12/(G12 - h6*celen))*(1.0_rp - s6/(rT*s61)))
    endif

    ! 
    ! Damaged element? 
    !
    if ((d1>0.0_rp) .or. (dG>0.0_rp) .or. (dK>0.0_rp) .or. (d6>0.0_rp)) then
       ledam_sld(ielem) = 1_ip
    end if

    ! ------------------------------------------------------------------------------
    ! DAMAGED STIFFNESS TENSOR
    !
    !
    call sm152_stiffness_tensor(stiff(:, :, 2), E11, EKT, EGT, G12, v12, d1, dK,   &
    &                           dG, d6)
    ! ------------------------------------------------------------------------------
    ! STRESS TENSOR
    !
    ! 2nd Piola-Kirchhoff stress tensor in compacted form using the Voigt notation
    !   {S}_i = [C]_ij*{E}_j   / (1)-(1,1), (2)-(2,2), (3)-(3,3)
    !                            (4)-(2,3), (5)-(1,3), (6)-(1,2)
    !
    do i = 1, 6
      do j = 1, 6
        vostr(i, 2) = vostr(i, 2) + stiff(i ,j, 2)*vogre(j)
      enddo
    enddo
    
    !
    ! Rotate from material to global coordinate system
    !
    if ( kfl_fiber_sld == 5_ip ) then
       auxV1 = vostr(:, 2)
       call sld_travo2(3_ip, auxV1, traMa, vostr(:, 2), 3_ip)
    endif
    !
    ! From Voigt notation to tensorial notation
    !
    do i = 1, 3
       do j = 1, 3
          ivoig = nvgij_inv_sld(i, j)
          gpstr(i, j, igaus) = vostr(ivoig,2)
       enddo
    enddo
    ! ------------------------------------------------------------------------------
    ! SECOND ELASTICITY TENSOR (dS/dE = C^T)
    !
    ! [dS/dE] = [H]^-1*( [I] - [M])
    ! 
    ! ...| if implicit scheme |......................................................
    if (flagt .eq. 1_ip) then
       !
       ! Computing [M]
       !            
       !   Longitudinal direction
       !     M(1, :) = dY1ds1*dd1/de = dY1ds1*dd1/drLT*drLT/ds*ds/de
       !
       dY1ds11 = 0.0_rp
       dd1drL  = 0.0_rp
       drLdS   = 0.0_rp
       drLde   = 0.0_rp
       if (flaLT) then
         !
         ! dY1/dd1 and dd1/drLT
         !
         if (d1 .lt. 0.99999999_rp) then
           dY1ds11 = vostr(1,2)/E11/((1.0_rp - d1)**2.0_rp)
           dd1drL = (E11*sLT)/(xT*(E11 - hLT*celen)*(rLT**2.0_rp)) 
         endif 
         !
         ! drL/de = drLT/ds*ds/de 
         !
         drLdS(1) = 1.0_rp/xT
         drLdS(2) = -v12/xT
         drLds(3) = -v12/xT
         !
         do i = 1,6
           do j = 1,6
             drLde(i) = drLde(i) + stiff(j,i,1)*drLds(j)
           enddo
         enddo 
       elseif (flaLC) then
         !
         ! dY1/dd1 and dd1/drLC
         !
         if (d1 .lt. 0.99999999_rp) then
           dY1ds11 = vostr(1,2)/(E11*(1.0_rp - d1)**2.0_rp)
           !
           ca1  = 1.0_rp - celen*hLC/E11
           ca2  = v23 + 2.0_rp*v12*v21 - 1.0_rp
           ca3  = sLC1/sLC
           cb1  = (nT*v21 + ca1*ca2*ca3*rLC)**2.0_rp - (4.0_rp*v12**2.0_rp + nTQ)  &
           &      *(v21**2.0_rp)
           cb2  = 2.0_rp*ca2*(2.0_rp*v12*v21 - (nT*v21 + ca1*ca2*ca3*rLC)*ca3*rLC) 
           cb3  = ca2**2.0_rp*((ca3*rLC)**2.0_rp - 1.0_rp)
           cb1d = 2.0_rp*ca1*ca2*ca3*(ca1*ca2*ca3*rLC + nT*v21)
           cb2d = -2.0_rp*ca2*ca3*(2.0_rp*ca1*ca2*ca3*rLC + nT*v21)
           cb3d = 2.0_rp*(ca2**2.0_rp)*(ca3**2.0_rp)*rLC
           cc1  = (cb2d*cb1 - cb2*cb1d)/(2.0_rp*(cb1**2.0_rp))
           dd1drL = - cc1 - (cb2*cc1/cb1 - (cb3d*cb1 - cb3*cb1d)/(cb1**2.0_rp))    &
           &        /(2.0_rp*dsqrt((cb2/(2.0_rp*cb1))**2.0_rp - cb3/cb1))
         endif 
         !
         ! drL/dS = = drL/ds*ds/de 
         !
         auxS1 = dsqrt((vostr(1, 1)**2.0_rp) + nTQ*(pT**2.0_rp) + nSQ*(tLT**2.0_rp))
         if (auxS1 .gt. 0.00000001_rp) then
           drLds(1) = vostr(1, 1)/(xC*auxS1)
           drLds(2) = (1.0_rp/(2.0_rp*xC))*(nT + (nTQ*pT)/auxS1)
           drLds(3) = (1.0_rp/(2.0_rp*xC))*(nT + (nTQ*pT)/auxS1)
           drLds(4) = 0.0_rp
           if (tLT .gt. 0.00000001_rp) then
              drLds(5) = (nS + (nSQ*tLT)/auxS1)*vostr(5,1)/(xC*tLT)
              drLds(6) = (nS + (nSQ*tLT)/auxS1)*vostr(6,1)/(xC*tLT)
           endif
         endif
         !          
         do i = 1,6
           do j = 1,6
             drLde(i) = drLde(i) + stiff(j,i,1)*drLds(j)
           enddo
         enddo 
       endif 
       !  
       !   Transverse direction
       !     M(2, :) = dYGds22*ddG/de + dYKds22*ddK/de
       !     M(3, :) = dYGds33*ddG/de + dYKds33*ddK/de
       !     M(4, :) = dYGds23*ddG/de
       !     M(5, :) = dY6ds13*dd6/de
       !     M(6, :) = dY6ds12*dd6/de
       !     where ddM/de = ddM/drT*drT/de
       !
       dYGds22 = 0.0_rp
       dYGds33 = 0.0_rp
       dYGds23 = 0.0_rp
       dYKds22 = 0.0_rp
       dYKds33 = 0.0_rp
       dY6ds13 = 0.0_rp
       dY6ds12 = 0.0_rp
       ddGdrT  = 0.0_rp
       ddKdrT  = 0.0_rp
       dd6drT  = 0.0_rp
       drTds   = 0.0_rp
       drTde   = 0.0_rp
       if (flaT) then
         !
         ! dYG/ddG and ddG/drT
         !
         if (dG .lt. 0.99999999_rp) then
           dYGds22 = (vostr(2,2) - vostr(3,2))/(4.0_rp*EGT*((1.0_rp - dG)**2.0_rp))
           dYGds33 = (vostr(3,2) - vostr(2,2))/(4.0_rp*EGT*((1.0_rp - dG)**2.0_rp))
           dYGds23 = vostr(4,2)/(EGT*((1.0_rp - dG)**2.0_rp))
           ddGdrT  = (EGT*sG)/(sG1*(EGT - hG*celen)*(rT**2.0_rp)) 
         endif
         !
         ! dYK/ddK and ddK/drT
         !
         if (pT .gt. 0.00000001_rp .and. dK .lt. 0.99999999_rp) then
           dYKds22 = (vostr(2,2) + vostr(3,2))/(4.0_rp*EKT*((1.0 - dK)**2.0_rp))
           dYKds33 = (vostr(2,2) + vostr(3,2))/(4.0_rp*EKT*((1.0 - dK)**2.0_rp))
           ddKdrT = ((A + rT)*(A + B*(rT - 1.0_rp) + 1.0_rp)*ddGdrT + (A + 1.0_rp) &
           &    *(B - 1.0_rp)*dG - (A + 1.0_rp)*(B - 1.0_rp))/((A + rT)**2.0_rp)
         endif
         !
         ! dY6/dd6 and dd6/drT
         !
         if (d6 .lt. 0.99999999_rp) then
           dY6ds13  = vostr(5,2)/(G12*((1.0_rp - d6)**2.0))
           dY6ds12  = vostr(6,2)/(G12*((1.0_rp - d6)**2.0))
           dd6drT = (G12*s6)/(s61*(G12 - h6*celen)*(rT**2.0_rp)) 
         endif 
         !
         ! drT/de = drT/ds*ds/de
         !
         auxS1 = ((yT + yC)/(yT*yC))**2.0_rp
         auxS2 = (yC - yT)/(yT*yC)
         auxS3 = dsqrt(auxS1*((tT**2.0_rp) + aT*(pT**2.0_rp))/(1.0_rp + aT)        &
         &    + (bT*tLT/sL)**2.0_rp)
         if (auxS3 .gt. 0.00000001_rp) then
           drTds(2) = 0.5_rp*(auxS1*(aT*pT + vostr(2, 1) - vostr(3, 1))            &
           &    /(2.0_rp*(aT + 1.0_rp)*auxS3) + auxS2)
           drTds(3) = 0.5_rp*(auxS1*(aT*pT + vostr(3, 1) - vostr(2, 1))            &
           &    /(2.0_rp*(aT + 1.0_rp)*auxS3) + auxS2)
           drTds(4) = auxS1*vostr(4, 1)/((1.0_rp + aT)*auxS3)
           drTds(5) = (bT**2.0_rp)/(sL*auxS3)*(vostr(5, 1)/sL)
           drTds(6) = (bT**2.0_rp)/(sL*auxS3)*(vostr(6, 1)/sL)
           if (tLT .ge. 0.00000001_rp) then
             drTds(5) = drTds(5) + ((1.0_rp - bT)/tLT)*(vostr(5, 1)/sL)
             drTds(6) = drTds(6) + ((1.0_rp - bT)/tLT)*(vostr(6, 1)/sL)
           endif
         endif
         !
         do i = 1,6
           do j = 1,6
             drTde(i) = drTde(i) + stiff(j,i,1)*drTds(j)
           enddo
         enddo          
       endif
       !
       tanM  = 0.0_rp
       do i = 1,6
          tanM(1,i) =  dY1ds11*dd1drL*drLde(i)
          tanM(2,i) = (dYGds22*ddGdrT + dYKds22*ddKdrT)*drTde(i)
          tanM(3,i) = (dYGds33*ddGdrT + dYKds33*ddKdrT)*drTde(i)
          tanM(4,i) =  dYGds23*ddGdrT*drTde(i)
          tanM(5,i) =  dY6ds13*dd6drT*drTde(i)
          tanM(6,i) =  dY6ds12*dd6drT*drTde(i)
       enddo
       !
       ! Compunting [I] - [M]
       !
       do i = 1, 6
          tanM(i,i) = 1.0_rp - tanM(i,i)
       enddo
       !
       ! Computing [H]^-1([I] -[M])
       !
       eltan = 0.0_rp
       do i = 1, 6
         do j = 1, 6
           do k = 1, 6
             eltan(i, j) = eltan(i, j) + stiff(i, k, 2)*tanM(k, j)
           enddo
         enddo
       enddo
      !
      ! Rotate from material to global coordinate system
      !
      if ( kfl_fiber_sld == 5_ip ) then
         auxM1 = eltan
         call sld_travo4(3_ip, auxM1, traMa, eltan, 3_ip)
      endif
      !
      ! From Voigt notation to tensorial notation
      !    The mapping ij -> a and kl -> b is: 
      !        a = i*delta_{ij} + (1 - delta_{ij})*(9 - i - j)
      !        b = k*delta_{kl} + (1 - delta_{kl})*(9 - k - l)
      do l = 1, 3
        do k = 1, 3
          do j = 1, 3
            do i = 1, 3
              p = i*int(tkron(i, j),ip) + (1_ip - int(tkron(i, j),ip))*(9_ip - i-j)
              q = k*int(tkron(k, l),ip) + (1_ip - int(tkron(k, l),ip))*(9_ip - k-l)
              gpdds(i, j, k, l, igaus) = eltan(p, q)
            enddo
          enddo
        enddo
      enddo   
    endif 
    ! .....................................................| if implicit scheme |...
    ! ------------------------------------------------------------------------------
    ! STORING STATE VARIABLES 
    state_sld(1, igaus, ielem, 2) = rLC
    state_sld(2, igaus, ielem, 2) = rLT
    state_sld(3, igaus, ielem, 2) = rT
    state_sld(4, igaus, ielem, 2) = d1
    state_sld(5, igaus, ielem, 2) = dG
    state_sld(6, igaus, ielem, 2) = dK
    state_sld(7, igaus, ielem, 2) = d6

    statt(ielem) % a(:,igaus,1) = state_sld(:,igaus,ielem,2)
  enddo 
  ! ..........................................................| Do gauss points |...
  
!   if (flagt .eq. 0_ip) then
!     print*,'*damage',gpgre(1, 1, 6),vogre(1),gpstr(1, 1, 6)
!     print*,d1,dK,dG,d6
!   endif
  
  
  ! ============================================================|    MAIN     |=====
  
end subroutine sld_stress_model_152
! ##################################################################################

! ##################################################################################
! SUBROUTINE SM152_PRECALCULUS(imate)
subroutine sm152_precalculus(imate)

  ! ================================================================================
  ! INIT
  ! --------------------------------------------------------------------------------
  use def_parame, only                     :  &
      ip, rp, lg
  use def_solidz, only                     :  &
      stiff0_sld
  ! --------------------------------------------------------------------------------
  implicit none 
  ! --------------------------------------------------------------------------------
  integer(ip), intent(in)                  :: &
      imate
  ! -------------------------------------------------------------------------------      
  real(rp)                                 :: & ! Material properties
     E11, E22, EKT, EGT, G12, v12, v21, v23,  & !   - Elastic
     xT, xC, yT, yC, sL, sT,                  & !   - Strength
     aT, bT, nT, nS, nTQ, nSQ,                & !   - Friction and adjust
     gLT, gLC, g1T, g2T, g2L,                 & !   - Fracture toughens 
     hLT1, hLT2, sLT1, sLT2,                  & !   - Slopes, ordinates and inflection
     hLC1, hLC2, sLC1, sLC2,                  &
     hG1, sG1, h61, s61,                      &
     a11, a22, b11, b22                         !   - hygrothermal

  ! =============================================================|    INIT    |=====
  
  ! ================================================================================
  ! MAIN
  ! --------------------------------------------------------------------------------
  ! GET PROPERTIES
  !
  call sm152_get_properties( imate,                                                &
                           & E11, E22, EKT, EGT, G12, v12, v21, v23,               &
                           & xT, xC, yT, yC, sL, sT, aT, bT, nT, nS, nTQ, nSQ,     &
                           & gLT, gLC, g1T, g2T, g2L,                              & 
                           & hLT1, sLT1, hLT2, sLT2,                               &
                           & hLC1, sLC1, hLC2, sLC2,                               &
                           & hG1, sG1, h61, s61,                                   &
                           & a11, a22, b11, b22)
   
  ! --------------------------------------------------------------------------------
  ! UNDAMAGE STIFF TENSOR
  !
  call sm152_stiffness_tensor(stiff0_sld(:, :, imate), E11, EKT, EGT, G12, v12,    &
      &                       0.0_rp, 0.0_rp, 0.0_rp, 0.0_rp)
  ! --------------------------------------------------------------------------------
  ! FIND A & B. INTERPOLATING POLYNOMIAL
  !
  ! comment only for debugging 
  !call sm152_find_A_B(imate, EKT, EGT, v12, v21, v23, aT, yT, yC, g1T, g2T, sT)

  ! ============================================================|    MAIN     |=====
  
end subroutine sm152_precalculus
! ##################################################################################

! ##################################################################################
! SUBROUTINE SM152_STIFFNESS_TENSOR
subroutine sm152_stiffness_tensor(stiff, E11, EKT, EGT, G12, v12, d1, dK, dG, d6)

  ! ================================================================================
  ! INIT
  ! --------------------------------------------------------------------------------
   use def_kintyp, only       :  ip, rp
   use def_domain, only       :  ndime
  ! --------------------------------------------------------------------------------
   implicit none
  ! --------------------------------------------------------------------------------
  real(rp),    intent(in)                 :: &
      E11, EKT, EGT, G12, v12,               & ! Material properties 
      d1,  dK,  dG,  d6                        ! Damage variables
  real(rp),    intent(out)                :: &
      stiff(6,6)                               ! Elastic modulus tensor
  ! --------------------------------------------------------------------------------
  real(rp)                                :: & 
      auxS1, auxS2                             ! Auxiliary variables
  ! =============================================================|    INIT    |=====
  
  ! ================================================================================
  ! MAIN
  ! --------------------------------------------------------------------------------
  ! INITIALIZE VARIABLES
  stiff = 0.0_rp
  auxS1 = 0.0_rp
  auxS2 = 0.0_rp

  ! --------------------------------------------------------------------------------
  ! STIFFNESS TENSOR 
  auxS1 = E11 + 4.0_rp*EKT*v12**2.0_rp*(d1*(1.0_rp - dK) + dK - 1.0_rp)
  stiff(1, 1) = -E11**2.0_rp*(d1 - 1.0_rp)/auxS1
  stiff(1, 2) = 2.0_rp*E11*EKT*v12*(d1 - 1.0_rp)*(dK - 1.0_rp)/auxS1
  stiff(1, 3) = stiff(1,2)
  stiff(2, 1) = stiff(1,2)
  auxS2 = EKT*(1.0_rp - dK) - 4.0_rp*EKT**2.0_rp*v12**2.0_rp*(d1 - 1.0_rp)          &
  &       *(dK - 1.0_rp)**2.0_rp/auxS1
  stiff(2, 2) =  EGT*(1.0_rp - dG) + auxS2 
  stiff(2, 3) = -EGT*(1.0_rp - dG) + auxS2
  stiff(3, 1) = stiff(1, 2)
  stiff(3, 2) = stiff(2, 3)
  stiff(3, 3) = stiff(2, 2)
  stiff(4, 4) = EGT*(1.0_rp - dG)
  stiff(5, 5) = G12*(1.0_rp - d6)
  stiff(6, 6) = stiff(5,5)  

  ! ============================================================|    MAIN     |=====
  
end subroutine sm152_stiffness_tensor
! ##################################################################################

! ##################################################################################
! SUBROUTINE find_A_B
!
subroutine sm152_find_A_B(imate, EKT, EGT, v12, v21, v23, aT, yT, yC, g1T, g2T,    &
      &                   sT)

  ! ================================================================================
  ! INIT
  !
  use def_parame, only                     :  &
      ip, rp, lg
  use def_solidz, only                     :  &
      parco_sld, lmate_sld, state_sld
  use def_master
  use def_domain
  use mod_root_finding
  use mod_numerical_integration
  ! -------------------------------------------------------------------------------
  implicit none 
  ! -------------------------------------------------------------------------------
  integer(ip), intent(in)                  :: &
      imate
  real(rp),    intent(in)                  :: &
      EKT, EGT, v12, v21, v23, aT,            & ! Material properties
       yT,  yC, g2T, g1T,  sT
  ! -------------------------------------------------------------------------------
  integer(ip)                              :: &
      ielem, kelem
  real(rp)                                 :: &
      celen, celenOld, prec,                  & ! Element lengths
      A,  A1,  A2,                            & ! A, Lower and upper limit
      arg(12),                                & ! Function argument   
      emin, emax                                !
  logical(lg)                              :: &
      suc                                       ! Bracketing function  
  ! =============================================================|    INIT    |=====
  
  ! ================================================================================
  ! MAIN
  !  Suppose B
  !     loop l from lmin to lmax
  !         find A to energy_dissipated = g1T 

  write(*,*)"--| ALYA          SM152_Find_A_B material #",imate 

  !
  ! Setting non-length dependent arguments
  !
  arg( 1) = EKT
  arg( 2) = EGT
  arg( 3) = aT
  arg( 4) = huge(1.0_rp)                                          ! A parameter
  arg( 5) = EGT/EKT                                               ! B parameter
  arg( 6) = (2.0_rp*v12*v21)/(1.0_rp - v23 - 2.0_rp*v12*v21)      ! a1
  arg( 7) = (yC + yT)/(yC*yT*dsqrt(1.0_rp + aT))                  ! a2
  arg( 8) = (yC - yT)/(yC*yT)                                     ! a3

  celenOld = huge(1.0_rp) 
  prec     = 1.0E-6
 
  !
  ! Loop over elements
  !
  !

  if (INOTMASTER) then

  do ielem = 1,nelem
 
    ! Only compute the length of the elements
    if (lmate_sld(ielem) == imate) then
      !
      ! Compute element length and store the length and B parameter
      !
      call sm152_get_length(ielem, celen)
      state_sld( 8, 1, ielem, 2) = celen
      state_sld(10, 1, ielem, 2) = arg(5)
     
 
      ! If the element length is equal to the previous element length
      if ( abs(celenOld - celen) < prec) then
          
        state_sld( 9, 1, ielem, 2) = arg(4)

      ! If not
      else
        !
        ! Setting length dependent arguments 
        !
        arg( 9) = 2.0_rp*g2T*EGT/(2.0_rp*g2T*EGT - celen*(sT**2.0_rp))
        arg(10) = celen                                             ! Element length
        arg(11) = 2.0_rp*g2T*EGT/(celen*(sT**2.0_rp))               ! Upper limit (rT where dG = 1)
        arg(12) = g1T/celen                                         ! Objective energy (g1T)
      
        !
        ! Can g1T be achieved?
        !
        arg( 4) = 0.0_rp
        call numint_qsimps(1.0_rp, arg(11), emin, 1.0E-6_rp, 30_ip, ratio_eneDis, arg)
        arg( 4) = 1.0E+9_rp
        call numint_qsimps(1.0_rp, arg(11), emax, 1.0E-6_rp, 30_ip, ratio_eneDis, arg)
        if ((arg(12) < emin) .OR. (arg(12) > emax)) then
          print*,ielem,celen,arg(12),emin,emax
          call runend('SLD_STRESS_MODEL_152: THE G1T DEFINED CANNOT BE ADJUSTED')
        endif
      
        !
        ! Bracketing the solution
        !
        A1 = 0.0_rp
        A2 = 5.0_rp
        call rootfind_bracke(A1, A2, 5.0_rp, 20_ip, suc, integ_eneDis, arg)

        ! 
        ! Closing the bracketing
        !
        call rootfind_bisect(A1, A2, 1.0_rp, 10_ip, A, integ_eneDis, arg)
        A1 = A*(0.99_rp)
        A2 = A*(1.01_rp)
        
        !
        ! Finding the solution
        !
        call rootfind_secant(A1, A2, 1.0E-3_rp, 20_ip, arg(4), integ_eneDis, arg)


        !
        ! Storing the A parameter and setting celenOld
        !
        state_sld( 9, 1, ielem, 2) = arg(4)
        celenOld = celen

      endif
   
    endif

  enddo

endif

  ! ============================================================|    MAIN     |===
  
  ! ==============================================================================
  ! CONTAINS
  !
  contains
    ! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! ENERGY DISSIPATION INTEGRATION
    !
    real(rp) function integ_eneDis(A,arg)
      use def_parame, only                 :  &
          ip, rp
      use mod_numerical_integration
      ! -------------------------------------------------------------------------
      implicit none
      ! -------------------------------------------------------------------------
      real(rp), intent(in)              :: &
          A
      real(rp), intent(inout), optional :: &
          arg(:)
      ! -------------------------------------------------------------------------
      real(rp)                          :: &
          ene
      ! -------------------------------------------------------------------------
      !
      ! Update A parameter to arguments
      !

      arg(4) = A
      
      !
      ! Difference between the objective energy minus the computed energy
      !
      ene = huge(1.0_rp)
      call numint_qsimps(1.0_rp, arg(11), ene, 1.0E-6_rp, 30_ip, ratio_eneDis, arg)
      integ_eneDis = arg(12) - ene
    
    end function integ_eneDis
    ! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    ! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! ENERGY DISSIPATION RATIO
    !
    real(rp) function ratio_eneDis(rT, arg)
      use def_parame, only                 :  &
          ip, rp
      ! -------------------------------------------------------------------------
      implicit none
      ! -------------------------------------------------------------------------
      real(rp),  intent(in)                :: &
          rT

      real(rp),  intent(inout), optional   :: &
          arg(:)
      ! -------------------------------------------------------------------------
      real(rp)                             :: &
          EGT, EKT, aT, a1, a2, a3, a4,       & ! Auxiliary variables
          A, B, celen,                        & 
          dG, dK, ddGdr, ddKdr,               & ! Damage variables
          s22, pT, tT,                        & ! Transversal stress & invariants
          YK, YG                                ! Conjugate thermodynamic forces
      ! -------------------------------------------------------------------------
      !
      ! Assign function arguments 
      !
      EKT   = arg( 1)
      EGT   = arg( 2)
      aT    = arg( 3)
      A     = arg( 4) 
      B     = arg( 5)
      a1    = arg( 6)
      a2    = arg( 7)
      a3    = arg( 8)
      a4    = arg( 9)
      celen = arg(10)     
      
      !
      ! Damage variables 
      !
      dG    = a4*(rT - 1.0_rp)/rT
      ddGdr = a4/(rT**2.0_rp)
      dK    = 1.0 - (1.0 - dG)*(B*(rT - 1.0) + A + 1.0_rp)/(rT + A)
      ddKdr = (A*(1.0_rp + A - B)*a4 + 2.0_rp*(1 + A - B)*rT*a4 + (rT**2.0_rp)* &
     &    (1.0_rp - B + A*(B - 1.0_rp)*(a4 - 1.0_rp) - a4 + 2.0_rp*B*a4))/      &
     &    ((rT**2.0_rp)*((A + rT)**2.0_rp))
      
      !
      ! Transversal stress and invariants
      !
      s22 = 0.0_rp
      if ((dK .lt. 0.99999999_rp) .AND. (dG .lt. 0.99999999_rp)) then
        s22 = rT/(a2*dsqrt(1.0_rp/(4.0_rp*(1.0_rp - dG)**2.0_rp) + (aT*((1.0_rp + a1*dK)**2.0_rp))/&
        &    (4.0_rp*(1.0_rp - dK)**2.0_rp)) + (a3*(1.0_rp + a1*dK))/(2.0_rp*(1.0_rp - dK)))
      endif
      pT  = s22*0.5_rp
      tT  = s22*0.5_rp
      
      !
      ! Conjugate thermodynamic forces
      !
      YK = 0.0_rp
      if (dK .lt. 0.99999999_rp) then
         YK = (pT**2.0_rp)/(2.0_rp*EKT*((1.0_rp - dK)**2.0_rp))
      endif
      YG = 0.0_rp
      if (dG .lt. 0.99999999_rp) then
         YG = (tT**2.0_rp)/(2.0_rp*EGT*((1.0_rp - dG)**2.0_rp))
      endif
      
      !
      ! Dissipated energy
      !
      ratio_eneDis = YK*ddKdr + YG*ddGdr
      
    end function ratio_eneDis
    ! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! ============================================================| CONTAINS  |=====

end subroutine sm152_find_A_B
! ##################################################################################

! ##################################################################################
! SUBROUTINE SM152_GET_PROPERTIES
!
subroutine sm152_get_properties(imate,                                             &
                              & E11, E22, EKT, EGT, G12, v12, v21, v23,            &
                              & xT, xC, yT, yC, sL, sT, aT, bT, nT, nS, nTQ, nSQ,  &
                              & gLT, gLC, g1T, g2T, g2L,                           & 
                              & hLT1, sLT1, hLT2, sLT2,                            &
                              & hLC1, sLC1, hLC2, sLC2,                            &
                              & hG1, sG1, h61, s61,                                &
                              & a11, a22, b11, b22)

  ! ================================================================================
  ! INIT
  ! --------------------------------------------------------------------------------
  use def_parame, only                     :  &
      ip, rp, lg
  use def_solidz, only                     :  &
      parco_sld
  ! --------------------------------------------------------------------------------
  implicit none 
  ! --------------------------------------------------------------------------------
  integer(ip),  intent(in)                 :: &
      imate
  real(rp),     intent(out)                :: &
     E11, E22, EKT, EGT, G12, v12, v21, v23,  & !   - Elastic
     xT, xC, yT, yC, sL, sT,                  & !   - Strength
     aT, bT, nT, nS, nTQ, nSQ,                & !   - Friction and adjust
     gLT, gLC, g1T, g2T, g2L,                 & !   - Fracture toughens 
     hLT1, hLT2, sLT1, sLT2,                  & !   - Slopes, ordinates and inflection
     hLC1, hLC2, sLC1, sLC2,                  &
     hG1, sG1, h61, s61,                      &
     a11, a22, b11, b22                         !   - hygrothermal
  ! --------------------------------------------------------------------------------
  real(rp)                                 :: &
     fxT, fxC, fgT, fgC
  ! ============================================================|    INIT     |=====
  
  ! ================================================================================
  ! MAIN
  ! --------------------------------------------------------------------------------
  !
  ! Elastic properties
  !
  E11 = parco_sld( 1, imate)
  E22 = parco_sld( 2, imate)                  
  G12 = parco_sld( 3, imate)
  v12 = parco_sld( 4, imate)
  v23 = parco_sld( 5, imate)

  EKT = E22/(2.0_rp - 2.0_rp*v23)
  EGT = E22/(2.0_rp + 2.0_rp*v23)
  v21 = v12*(E22/E11)
  
  !
  ! Strength properties
  !
  xT  = parco_sld( 6, imate)
  fxT = parco_sld( 7, imate)
  xC  = parco_sld( 8, imate)
  fxC = parco_sld( 9, imate)
  aT  = parco_sld(10, imate)
  bT  = parco_sld(11, imate) 
  yT  = parco_sld(12, imate)
  yC  = parco_sld(13, imate)
  sL  = parco_sld(14, imate)
  nT  = parco_sld(15, imate)
  nS  = parco_sld(16, imate)
  nTQ = parco_sld(17, imate)
  nSQ = parco_sld(18, imate)

  sT  = (yT*yC*dsqrt(1.0_rp + aT)/(yC + yT)) 
  
  !
  ! Cohesive law
  !
  gLT  = parco_sld(19, imate) 
  fgT  = parco_sld(20, imate)  
  gLC  = parco_sld(21, imate)
  fgC  = parco_sld(22, imate)
  g1T  = parco_sld(23, imate)
  g2T  = parco_sld(24, imate)
  g2L  = parco_sld(25, imate)
  
  hLT1 = (xT**2.0_rp)/(gLT*fgT*2.0_rp)
  sLT1 = xT
  hLT2 = ((fxT*sLT1)**2.0_rp)/((1.0_rp + fgT*(fxT**2.0_rp - 1.0_rp))*2.0_rp*gLT)
  sLT2 = ((1.0_rp + fgT*(fxT - 1.0_rp))*fxT*xT)/(1.0_rp + fgT*(fxT**2.0_rp         &
  &      - 1.0_rp))
  hLC1 = (xC**2.0_rp)/(gLC*fgC*2.0_rp)
  sLC1 = xC
  hLC2 = ((fxC*sLC1)**2.0_rp)/((1.0_rp + fgC*(fxC**2.0_rp - 1.0_rp))*2.0_rp*gLC)
  sLC2 = ((1.0_rp + fgC*(fxC - 1.0_rp))*fxC*xC)/(1.0_rp + fgC*(fxC**2.0_rp         &
  &      - 1.0_rp))
  sG1  = sT
  hG1  = (sG1**2.0_rp)/(g2T*2.0_rp)
  s61  = sL
  h61  = (s61**2.0_rp)/(g2L*2.0_rp)
  
  ! 
  ! Thermal effects
  !
  a11  = parco_sld(25, imate)
  a22  = parco_sld(26, imate)
  b11  = parco_sld(27, imate)
  b22  = parco_sld(28, imate)
 
  ! ============================================================|    MAIN     |=====
  
end subroutine sm152_get_properties

! ##################################################################################
! SUBROUTINE SM152_GET_LENGTH
!
subroutine sm152_get_length(ielem, celen)

  ! ================================================================================
  ! INIT
  ! --------------------------------------------------------------------------------
  use def_parame, only                     :  &
      ip, rp, lg
  use def_domain, only                     :  &
      ltype, lnnod, lnods, ndime,             &
      elmar, hnatu, mnode, coord
  ! --------------------------------------------------------------------------------
  implicit none
  ! --------------------------------------------------------------------------------
  integer(ip),  intent(in)                 :: &
      ielem
  real(rp),     intent(out)                :: &
      celen
  ! --------------------------------------------------------------------------------
  integer(ip)                              :: &
      idime,                                  & ! Index
      pelty, pnode, inode, ipoin                ! Element length required indices 
  real(rp)                                 :: &
      tragl(9), hleng(3), elcod(ndime,mnode)    ! Element length
  ! ============================================================|    INIT     |=====
  
  ! ================================================================================
  ! MAIN
  
  !
  ! Element length
  !
  pelty = ltype(ielem)
  pnode = lnnod(ielem)
  do inode = 1, lnnod(ielem)
     ipoin = lnods(inode, ielem)
     do idime = 1, ndime
        elcod(idime, inode) = coord(idime, ipoin)
     enddo
  enddo
  call elmlen(ndime, pnode, elmar(pelty)%dercg, tragl, elcod, hnatu(pelty), hleng)
  celen = hleng(1)
  ! ============================================================|    MAIN     |=====
  
end subroutine sm152_get_length
