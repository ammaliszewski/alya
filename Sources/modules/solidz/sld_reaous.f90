subroutine sld_reaous()
!-----------------------------------------------------------------------
!****f* Solidz/sld_reaous
! NAME 
!    sld_reaous
! DESCRIPTION
!    This routine reads the output strategy 
! USES
!    ecoute
! USED BY
!    sld_turnon
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_solidz
  use def_domain
  implicit none
  integer(ip) :: ifote,nfote,dummi

  kfl_exacs_sld = 0                        ! Exact solution
  kfl_foten_sld = 0                        ! Tensors of forces caust, green, lepsi (0 if none)
  kfl_rotei_sld = 0                        ! Rotate and correct sigma eigenvalues (ONLY FOR 3D PRISMATIC SHELLS)

  if( INOTSLAVE ) then
     !
     ! Reach the section
     !
     call ecoute('reaous')
     do while(words(1)/='OUTPU')
        call ecoute('reaous')
     end do
     !
     ! Begin to read data
     !
     do while(words(1)/='ENDOU')
        call ecoute('reaous')
        call posdef(2_ip,dummi)
        if(words(1)=='OUTPU') then
           !
           ! Exact solution
           !           
           if(words(2)=='ERROR') then
              kfl_exacs_sld = getint('SOLUT',1_ip,'#Exact solution')
           end if
        else if (words(1) == 'ROTAT') then
           kfl_rotei_sld = 1
        end if
     end do
  end if

  if( INOTSLAVE ) then
     if( postp(1) % npp_setsb(1) == 1 ) then
        postp(1) % npp_setsb(1:3) = 1
     end if
     !
     ! Check if the stress tensors need to be computed (postprocess)
     ! If at least one of them is active, kfl_foten_sld = 1.

!!     do nfote= 4,35
!!        ifote= nfote
!!        call posdef(25_ip,ifote)
!!        if (ifote > 0) kfl_foten_sld = 1    ! postprocess
!!     end do

     kfl_foten_sld = 1    ! do it always
  
   
  end if

end subroutine sld_reaous
