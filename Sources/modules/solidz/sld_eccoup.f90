!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_eccoup.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Calculate the active (2nd P-K and cauchy) active stress 
!> @details Calculate the active (2nd P-K and cauchy) active stress 
!> @} 
!-----------------------------------------------------------------------
subroutine sld_eccoup(&
     pgaus,pmate,igaus,gpdet,gptlo,lamda,nfibt,nshtt,normt,&
     gpigd,castr_active,gpstr_active,gpcac,tactf,ielem)
  
  !-----------------------------------------------------------------------
  !
  !    GPSTR_active ... 2nd P-K Active Stress tensor ....................S_a
  !    CASTR_active ... Cauchy Stress tensor .................... sigma_a
  !
  ! USES
  !
  ! USED BY
  !    sld_stress_model_134
  !    sld_stress_model_xxx  
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime
  use def_master, only       :  cutim,fisoc,elmag,kfl_ephys,ittim
  use def_solidz           
      
  implicit none
  integer(ip), intent(in)    :: pgaus,pmate,igaus,ielem  
  real(rp),    intent(in)    :: gptlo(pgaus),lamda(3),gpigd(ndime,ndime,pgaus),gpdet(pgaus),&
                                nfibt(ndime,pgaus),nshtt(ndime,pgaus),normt(ndime,pgaus),gpcac
  real(rp),    intent(out)   :: castr_active(ndime,ndime),gpstr_active(ndime,ndime),tactf
!  real(rp)                   :: Factive
  
  real(rp)                   :: ca0,cam,thau,cca2p,n,beta,Cal50,tmaxi,tacts,tactn,tacis,k,&
                                fporf_t(ndime,ndime),spors_t(ndime,ndime),nporn_t(ndime,ndime),&
                                nfibe_length
  
  integer(ip)                :: idime,jdime,kdime,ldime
  
  !constants for active stress (units: CGS)
  
  ca0  = 0.0_rp!0.01 !mmole
  cam  = 1.0_rp !mmole
  
  !Default value: 0.06_rp sec (human) ... !0.01268 sec Rat
  thau = timec_sld(pmate) 
    
  !tmaxi= 1.0e6_rp !0.2e6_rp !barye !! 1.0e-3!1.0e-3 !stress in msec, cm
  tmaxi= 1.0e6_rp * cocof_sld(pmate)
!  JAZZY DID THIS TO SPEED THINGS UP AND DEBUG!!!  
  if (kfl_coupt_sld(pmate) == 11) then 
     tmaxi= tmaxi * covar_sld(ielem)
  end if
!!  END 
  beta =1.45_rp  !!1.45_rp  changed to value from paper: JAZ 2.0
  n    =hillc_sld(pmate)
  k    =0.3_rp

   
  castr_active=0.0_rp
  gpstr_active=0.0_rp
  
  ! * * * * * *    
  ! Active stress T(lambda, [Ca++])m x m  (Hunter p 688)
  ! * * * * * * 
   
  !calculate [Ca2+] 
  if (kfl_ephys(pmate) == 1) then   !! Ten Tuscher calcula el Ca2+ dins del propi model
     !
     ! Ten Tuscher model
     !
     cca2p=gpcac    !*1000  Changed by JAZ, was wrong!!
     !C50
     Cal50= cal50_sld(pmate)!0.6 !value by Hung 
     ! Cal50= 4.35_rp/sqrt(exp(4.75_rp*((1.9*lamda(1))-1.58_rp))-1.0_rp) 
  else                       !! FHN, Fenton necessiten un valor Ca2+ 
     !
     ! Otherwise
     !
     cca2p= ca0 + (cam-ca0)*(gptlo(igaus)/thau)*exp(1.0-(gptlo(igaus)/thau))
     !C50
     Cal50= cal50_sld(pmate)!0.6 !value by Hung 
!     Cal50= 0.5_rp!0.6 !value by Hung 
     ! Cal50= 4.35_rp/sqrt(exp(4.75_rp*((1.9*lamda(1))-1.58_rp))-1.0_rp) 
  end if

  !write(990,*) cca2p
!  if (tactf == 1) then  !Hunter model for active force (PAULA)
  !T, active tension in ff
  tactf = (cca2p**n/(cca2p**n + Cal50**n)) * tmaxi * (1.0_rp+beta*(lamda(1)-1.0_rp))
!  tacts = 0.0_rp!tactf!0.0!k*tactf*(lamda(2)/lamda(1))
!  tactn = 0.0_rp!tactf!0.0!k*tactf*(lamda(3)/lamda(1))
!  else                  !Rice myofilament model (PAULA) ------> if (tactf == 1) aún por mejorar!
   !Cal50 here should be 0.5
!     call sld_myofil(cca2p, Factive)
!     tactf = Factive            !Gamma for tensile active force is necessary in this case? (PAULA)
!  end if

  tacts = 0.0_rp
  tactn = 0.0_rp
  tacis = 0.0_rp  ! for transversally isotropic coupling
!!  JAZZY DID THIS TO SPEED THINGS UP AND DEBUG!!!
  if (kfl_coupt_sld(pmate) == 2) then
     tacis = tactf * trans_sld(1,pmate)
     tactf = tactf * (1.0_rp - trans_sld(1,pmate))
  end if
!!!  END OF JAZZY'S MESSUP

  tactv_sld= tactv_sld + tactf/real(pgaus)

  !f_true x f_true (outer products [3x3]) 
  fporf_t=0.0_rp
  spors_t=0.0_rp
  nporn_t=0.0_rp    
  nfibe_length= 0.0_rp
  do idime=1,ndime
     nfibe_length=   nfibe_length + nfibt(idime,igaus)*nfibt(idime,igaus) 
     do jdime=1,ndime
        fporf_t(idime,jdime) = nfibt(idime,igaus)*nfibt(jdime,igaus) 
        spors_t(idime,jdime) = nshtt(idime,igaus)*nshtt(jdime,igaus) 
        nporn_t(idime,jdime) = normt(idime,igaus)*normt(jdime,igaus)
     end do
  end do
  nfibe_length= sqrt(nfibe_length) 
!  if (ielem == 1253) write(6,*) 'poto',ielem,nfibe_length,tactf
!  if (ielem ==    1) write(6,*) 'poto',ielem,nfibe_length,tactf

  ! Cauchy active stress tensor (castr_active)
  do idime=1,ndime
     do jdime=1,ndime          
        castr_active(idime,jdime) = (fporf_t(idime,jdime)*tactf) +&
                                    (spors_t(idime,jdime)*tacts) +&
                                    (nporn_t(idime,jdime)*tactn)
     end do
  end do
!!  JAZZY DID THIS TO SPEED THINGS UP AND DEBUG!!!
  if (kfl_coupt_sld(pmate) == 2) then
     ! add isotropy in the transversally isotropic coupling
      do jdime=1,ndime          
        castr_active(idime,idime) = castr_active(idime,idime) + tacis
     end do
  end if
!! FINISH TO JAZZY'S MESSUP!

101 format (9(e13.6,' ')) 
end subroutine sld_eccoup
