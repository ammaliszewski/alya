subroutine sld_detjac(&
     ielem,pnode,pgaus,eldis,eldix,elcod,gpcar,gphea,gpdet,&
     gpigd,gpgdi,gpdet_min,ielem_min) 
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_detjac
  ! NAME
  !    sld_detjac
  ! DESCRIPTION
  !    Compute some Gauss values
  ! INPUT
  !    GPGI0 ... Previous deformation gradient tensor ............ F(n)
  ! OUTPUT
  !    GPGDI ... Updated deformation gradient .....................F(n+1) = grad(phi)
  ! USES
  ! USED BY
  !    sld_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg 
  use def_domain, only       :  mnode,ndime
  use def_solidz, only       :  kfl_xfeme_sld
  implicit none
  integer(ip), intent(in)    :: ielem
  integer(ip), intent(in)    :: pnode
  integer(ip), intent(in)    :: pgaus
  real(rp),    intent(in)    :: eldis(ndime,pnode,*)   
  real(rp),    intent(in)    :: eldix(ndime,pnode,*)
  real(rp),    intent(in)    :: elcod(ndime,pnode)
  real(rp),    intent(in)    :: gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)    :: gphea(pnode,pgaus)
  real(rp),    intent(in)    :: gpdet(pgaus)
  real(rp),    intent(out)   :: gpigd(ndime,ndime,pgaus)
  real(rp),    intent(out)   :: gpgdi(ndime,ndime,pgaus)
  real(rp),    intent(inout) :: gpdet_min
  integer(ip), intent(inout) :: ielem_min
  integer(ip)                :: inode,igaus,idime,jdime
  !
  ! Initialization
  !
  do idime = 1,ndime
     do igaus = 1,pgaus 
        do jdime = 1,ndime
           gpgdi(idime,jdime,igaus) = 0.0_rp
        end do
     end do
  end do
  !
  ! GPGDI: Deformation gradients 
  !
  do igaus = 1,pgaus
     do idime = 1,ndime
        do jdime = 1,ndime
           do inode = 1,pnode
              gpgdi(idime,jdime,igaus) = gpgdi(idime,jdime,igaus) &
                   + eldis(idime,inode,1) * gpcar(jdime,inode,igaus)
           end do
        end do
     end do
  end do
  !
  ! GPGDI: Deformation gradients for xfem case
  ! to include the contribution of the enrichment dofs  
  !
  if( kfl_xfeme_sld == 1 ) then
     do igaus = 1,pgaus
        do idime = 1,ndime
           do jdime = 1,ndime
              do inode = 1,pnode
                 gpgdi(idime,jdime,igaus) = gpgdi(idime,jdime,igaus) &
                      + eldix(idime,inode,1) * gpcar(jdime,inode,igaus) * gphea(inode,igaus)
              end do
           end do
        end do
     end do
  end if
  !
  ! Compute J
  !
  gpigd = 0.0_rp
  do igaus = 1,pgaus
     gpgdi(1,1,igaus)= gpgdi(1,1,igaus) + 1.0_rp
     gpgdi(2,2,igaus)= gpgdi(2,2,igaus) + 1.0_rp
     if( ndime == 3 ) gpgdi(3,3,igaus)= gpgdi(3,3,igaus) + 1.0_rp

     call invmtx(gpgdi(1,1,igaus),gpigd(1,1,igaus),gpdet(igaus),ndime)

     if( gpdet(igaus) < gpdet_min ) then
        gpdet_min = gpdet(igaus)
        ielem_min = ielem
     end if

  end do

end subroutine sld_detjac 
