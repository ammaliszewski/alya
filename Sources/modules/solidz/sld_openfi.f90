subroutine sld_openfi(itask)

!-----------------------------------------------------------------------
!    
! This subroutine gets ALL the file names and open them to be used by 
! the module in two possible ways:
! 
! 1. Recalling them from the environment, when Alya is launched
! encapsulated in a shell script, or
! 
! 2. Composing the names out of the problem name which is given as argument
! when the binary file Alya is launched "naked". 
!
!-----------------------------------------------------------------------
  use def_master
  use mod_iofile
  use def_solidz
  implicit none
  integer(ip),   intent(in) :: itask  
  integer(ip)               :: ilcha 
  character(150)            :: fil_pdata_sld,fil_outpu_sld
  character(150)            :: fil_conve_sld,fil_solve_sld
  character(7)              :: statu
  character(11)             :: forma
  character(6)              :: posit
  
end subroutine sld_openfi

