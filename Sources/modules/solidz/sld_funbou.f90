subroutine sld_funbou(itask,ifixi,xretu)
  !------------------------------------------------------------------------
  !
  ! This subroutine computes transient boundary conditions for boundary
  ! elements coming from transient data files
  !
  ! counter ifixi can be a boundary element (iboun, itask=1) or a node (ipoin, itask=2)
  !
  !
  !-------------------------------------------------------------------------------------
  use def_master
  use def_domain
  use def_solidz
  implicit none
  
  integer(ip) :: itask,idata,ifunc,ifixi,idime
  real(rp)    :: t1,p1,t2,p2, b, m, cdeti, xretu(ndime)

  if (itask == 1_ip) then
     !
     ! on boundary elements
     !
     if (ifixi < 0) then
        ifunc = - ifixi
     else
        ifunc = kfl_funbo_sld(ifixi)
     end if

     do idata= 1,mtloa_sld(ifunc)-1
        if (cutim >= tload_sld(ifunc)%a(ndime+1,idata)) then
           if (cutim < tload_sld(ifunc)%a(ndime+1,idata+1)) then
              t1= tload_sld(ifunc)%a(ndime+1,idata)
              t2= tload_sld(ifunc)%a(ndime+1,idata+1)
              p1= tload_sld(ifunc)%a(1,idata  )
              p2= tload_sld(ifunc)%a(1,idata+1)
              m=(p2-p1)/(t2-t1)
              b=p1-m*t1
              xretu(1)=m*cutim+b                              ! return value
              exit
           end if
        end if
     end do     
  else   if (itask == 2_ip) then
     !
     ! on boundary nodes
     !
     if (ifixi < 0) then
        ifunc = - ifixi
     else
        ifunc = kfl_funno_sld(ifixi)
     end if
     do idata= 1,mtloa_sld(ifunc)-1
        if (cutim >= tload_sld(ifunc)%a(ndime+1,idata)) then
           if (cutim < tload_sld(ifunc)%a(ndime+1,idata+1)) then
              t1= tload_sld(ifunc)%a(ndime+1,idata)
              t2= tload_sld(ifunc)%a(ndime+1,idata+1)
              do idime=1,ndime
                 if (kfl_fixno_sld(idime,ifixi) == 1) then
                    p1= tload_sld(ifunc)%a(idime,idata  )
                    p2= tload_sld(ifunc)%a(idime,idata+1)
                    m=(p2-p1)/(t2-t1)
                    b=p1-m*t1
                    xretu(idime)=m*cutim+b                              ! return value
                 end if
              end do
              exit
           end if
        end if
     end do     
   else   if (itask == 3_ip) then
     !
     ! on every node (body forces)
     !
     

     ifunc = kfl_bodyf_sld
     if (ifunc == 0) then
        xretu = 0.0_rp
        return
     end if     
     do idata= 1,mtloa_sld(ifunc)-1
        if (cutim >= tload_sld(ifunc)%a(ndime+1,idata)) then
           if (cutim < tload_sld(ifunc)%a(ndime+1,idata+1)) then
              t1= tload_sld(ifunc)%a(ndime+1,idata)
              t2= tload_sld(ifunc)%a(ndime+1,idata+1)             
              do idime=1,ndime          
                ! if (kfl_fixno_sld(idime,ifixi) == 1) then
                    p1= tload_sld(ifunc)%a(idime,idata  )
                    p2= tload_sld(ifunc)%a(idime,idata+1)
                    m=(p2-p1)/(t2-t1)
                    b=p1-m*t1
                    xretu(idime)=m*cutim+b                              ! return value
                ! end if
              end do
              exit
           end if
        end if
     end do     
 end if


end subroutine sld_funbou
