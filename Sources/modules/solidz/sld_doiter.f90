subroutine sld_doiter
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_doiter
  ! NAME
  !    sld_doiter
  ! DESCRIPTION
  !    This routine controls the internal loop of the module equation.
  ! USES
  !    sld_begite
  !    sld_solite
  !    sld_endite
  ! USED BY
  !    Solidz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solver
  use def_solidz
  implicit none

  !   if(kfl_stead_sld==0) then
  call sld_begite()
  do while(kfl_goite_sld==1)
     call sld_solite()
     call sld_endite(one)
  end do
  call sld_endite(two)

  call sld_uptcoh()
  !   end if

  !  write(*,*)'leenr_sld = ',size(leenr_sld),leenr_sld
  !  write(*,*)'leien_sld = ',size(leien_sld),leien_sld

  !!IF CONTACT, UNCOMMENT ALL LINES WITH "!!" AND COMMENT ALL THE REST
!!$  call sld_defcon(one) !initialization
!!$  do while(kfl_conta_sld .ne. 0)
!!$     call sld_begite()
!!$     do while(kfl_goite_sld==1)
!!$        call sld_solite()
!!$        call sld_endite(one)
!!$     end do
!!$     call sld_endite(two)
!!$     call sld_defcon(two) !search for contact
!!$     if (kfl_conta_sld .gt. 0)  then
!!$        uzawa_count = uzawa_count + 1
!!$        if (uzawa_count .gt. 15) then
!!$           kfl_conta_sld = 0
!!$        else
!!$           call sld_defcon(three) !apply loads if contact (calculates the contribution to RHSID term) 
!!$        end if
!!$     end if
!!$  end do
!!$
!!$  write(888,*) cutim,rhscont(512),uzawa_count
!!$  write(889,*) cutim,rhscont(366),uzawa_count

end subroutine sld_doiter
