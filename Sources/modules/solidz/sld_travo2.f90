subroutine sld_travo2(irotype,tenold,Amatr,tennew,ndime)
   
   !----------------------------------------------------------------------------
   !> @addtogroup Solidz
   !> @{
   !> @file    sld_travo2.f90
   !> @author  Adria Quintanas (u1902492@campus.udg.edu)
   !> @date    Desember, 2014
   !> @brief   This subroutine performs a transformation of a tensor in the Voigt
   !> @        from a old basis to a new basis. 
   !> @details This subroutine performs a transformation of a tensor
   !> @           Strain from Local CSYS to Global CSYS:
   !> @              E _{i} = T _{ij}^T*E'_{j}
   !> @           Strain from Global CSYS to Local CSYS:
   !> @              E'_{i} = Tg_{ij}  *E _{j}
   !> @           Stress  from Local CSYS to Global CSYS:
   !> @              S _{i} = Tg_{ij}^T*S'_{j}
   !> @           Stress  from Global CSYS to Local CSYS:
   !> @              S'_{i} = T _{ij}  *S _{j}
   !> @
   !> @        Reference: E.J. Barbero - Finite Element Analysis of Composite 
   !> @           Materials
   !> @} 
   !----------------------------------------------------------------------------

    
   use      def_kintyp  
   
   implicit none
   
   integer(ip),    intent(in)     ::    irotype,ndime
   real(rp),       intent(in)     ::    Amatr(3, 3), tenold(ndime*2_ip)
   real(rp),       intent(out)    ::    tennew(ndime*2_ip)
   integer(ip)                    ::    i,j
   real(rp)                       ::    Tmatr(6, 6)
   
   ! -------------------------------
   ! INITIALIZE VARIABLES
   !
   tennew = 0.0_rp


   ! -------------------------------
   ! TRANSFORMATION MATRIX
   !    [T] and [Tg] are almost equal, only change some positions, then, the common part
   !    is defined here.
   Tmatr(1, 1) = Amatr(1, 1)*Amatr(1, 1)
   Tmatr(1, 2) = Amatr(1, 2)*Amatr(1, 2)
   Tmatr(1, 3) = Amatr(1, 3)*Amatr(1, 3)
   Tmatr(1, 4) = Amatr(1, 2)*Amatr(1, 3)
   Tmatr(1, 5) = Amatr(1, 1)*Amatr(1, 3)
   Tmatr(1, 6) = Amatr(1, 1)*Amatr(1, 2)
   Tmatr(2, 1) = Amatr(2, 1)*Amatr(2, 1)
   Tmatr(2, 2) = Amatr(2, 2)*Amatr(2, 2)
   Tmatr(2, 3) = Amatr(2, 3)*Amatr(2, 3)
   Tmatr(2, 4) = Amatr(2, 2)*Amatr(2, 3)
   Tmatr(2, 5) = Amatr(2, 1)*Amatr(2, 3)
   Tmatr(2, 6) = Amatr(2, 1)*Amatr(2, 2)
   Tmatr(3, 1) = Amatr(3, 1)*Amatr(3, 1)
   Tmatr(3, 2) = Amatr(3, 2)*Amatr(3, 2)
   Tmatr(3, 3) = Amatr(3, 3)*Amatr(3, 3)
   Tmatr(3, 4) = Amatr(3, 2)*Amatr(3, 3)
   Tmatr(3, 5) = Amatr(3, 1)*Amatr(3, 3)
   Tmatr(3, 6) = Amatr(3, 1)*Amatr(3, 2)
   Tmatr(4, 1) = Amatr(2, 1)*Amatr(3, 1)
   Tmatr(4, 2) = Amatr(2, 2)*Amatr(3, 2) 
   Tmatr(4, 3) = Amatr(2, 3)*Amatr(3, 3)
   Tmatr(4, 4) = Amatr(2, 2)*Amatr(3, 3) + Amatr(2, 3)*Amatr(3, 2)
   Tmatr(4, 5) = Amatr(2, 1)*Amatr(3, 3) + Amatr(2, 3)*Amatr(3, 1)
   Tmatr(4, 6) = Amatr(2, 1)*Amatr(3, 2) + Amatr(2, 2)*Amatr(3, 1)
   Tmatr(5, 1) = Amatr(1, 1)*Amatr(3, 1)
   Tmatr(5, 2) = Amatr(1, 2)*Amatr(3, 2)
   Tmatr(5, 3) = Amatr(1, 3)*Amatr(3, 3)
   Tmatr(5, 4) = Amatr(1, 2)*Amatr(3, 3) + Amatr(1, 3)*Amatr(3, 2)
   Tmatr(5, 5) = Amatr(1, 1)*Amatr(3, 3) + Amatr(1, 3)*Amatr(3, 1)
   Tmatr(5, 6) = Amatr(1, 1)*Amatr(3, 2) + Amatr(1, 2)*Amatr(3, 1)
   Tmatr(6, 1) = Amatr(1, 1)*Amatr(2, 1)
   Tmatr(6, 2) = Amatr(1, 2)*Amatr(2, 2)
   Tmatr(6, 3) = Amatr(1, 3)*Amatr(2, 3)
   Tmatr(6, 4) = Amatr(1, 2)*Amatr(2, 3) + Amatr(1, 3)*Amatr(2, 2)
   Tmatr(6, 5) = Amatr(1, 1)*Amatr(2, 3) + Amatr(1, 3)*Amatr(2, 1)
   Tmatr(6, 6) = Amatr(1, 1)*Amatr(2, 2) + Amatr(1, 2)*Amatr(2, 1)

   !
   ! SELECT CASE, COMPLIANCE / STIFFNESS FROM G->L / L->G
   !
   
   if ( irotype == 1_ip) then
      !
      ! Strain from Local CSYS to Global CSYS:
      !    E _{i} = T _{ij}^T*E'_{j}
      !
   
      !
      ! Transformation matrix [T]
      !
      Tmatr(1, 4) = Tmatr(1, 4)*2.0_rp
      Tmatr(1, 5) = Tmatr(1, 5)*2.0_rp
      Tmatr(1, 6) = Tmatr(1, 6)*2.0_rp
      Tmatr(2, 4) = Tmatr(2, 4)*2.0_rp
      Tmatr(2, 5) = Tmatr(2, 5)*2.0_rp
      Tmatr(2, 6) = Tmatr(2, 6)*2.0_rp
      Tmatr(3, 4) = Tmatr(3, 4)*2.0_rp
      Tmatr(3, 5) = Tmatr(3, 5)*2.0_rp
      Tmatr(3, 6) = Tmatr(3, 6)*2.0_rp

      !
      ! Matrix multiplication
      !
      do i = 1, 2_ip*ndime
         do j = 1, 2_ip*ndime
                  tennew(i) = tennew(i) +  Tmatr(j, i)*tenold(j)
         end do
      end do


   else if ( irotype == 2_ip ) then
      !
      ! Strain from Global CSYS to Local CSYS:
      !    E'_{i} = Tg_{ij}  *E _{j}
      !
   
      !
      ! Transformation matrix [Tg]
      !
      Tmatr(4, 1) = Tmatr(4, 1)*2.0_rp
      Tmatr(4, 2) = Tmatr(4, 2)*2.0_rp
      Tmatr(4, 3) = Tmatr(4, 3)*2.0_rp
      Tmatr(5, 1) = Tmatr(5, 1)*2.0_rp
      Tmatr(5, 2) = Tmatr(5, 2)*2.0_rp
      Tmatr(5, 3) = Tmatr(5, 3)*2.0_rp
      Tmatr(6, 1) = Tmatr(6, 1)*2.0_rp
      Tmatr(6, 2) = Tmatr(6, 2)*2.0_rp
      Tmatr(6, 3) = Tmatr(6, 3)*2.0_rp

      !
      ! Matrix multiplication
      !
      do i = 1, 2_ip*ndime
         do j = 1, 2_ip*ndime
                  tennew(i) = tennew(i) +  Tmatr(i, j)*tenold(j)
         end do
      end do
	  
   else if ( irotype == 3_ip ) then
      !
      ! Stress  from Local CSYS to Global CSYS:
      !    S _{i} = Tg_{ij}^T*S'_{j}
      !
      
      !
      ! Transformation matrix [Tg]
      !
      Tmatr(4, 1) = Tmatr(4, 1)*2.0_rp
      Tmatr(4, 2) = Tmatr(4, 2)*2.0_rp
      Tmatr(4, 3) = Tmatr(4, 3)*2.0_rp
      Tmatr(5, 1) = Tmatr(5, 1)*2.0_rp
      Tmatr(5, 2) = Tmatr(5, 2)*2.0_rp
      Tmatr(5, 3) = Tmatr(5, 3)*2.0_rp
      Tmatr(6, 1) = Tmatr(6, 1)*2.0_rp
      Tmatr(6, 2) = Tmatr(6, 2)*2.0_rp
      Tmatr(6, 3) = Tmatr(6, 3)*2.0_rp
      
      !
      ! Matrix multiplication
      !
      do i = 1, 2_ip*ndime
         do j = 1, 2_ip*ndime
                  tennew(i) = tennew(i) +  Tmatr(j, i)*tenold(j)
         end do
      end do

   else if ( irotype == 4_ip ) then  
      !
      ! Stress  from Global CSYS to Local CSYS:
      !    S'_{i} = T _{ij}  *S _{j}
      !
   
      !
      ! Transformation matrix [T]
      !
      Tmatr(1, 4) = Tmatr(1, 4)*2.0_rp
      Tmatr(1, 5) = Tmatr(1, 5)*2.0_rp
      Tmatr(1, 6) = Tmatr(1, 6)*2.0_rp
      Tmatr(2, 4) = Tmatr(2, 4)*2.0_rp
      Tmatr(2, 5) = Tmatr(2, 5)*2.0_rp
      Tmatr(2, 6) = Tmatr(2, 6)*2.0_rp
      Tmatr(3, 4) = Tmatr(3, 4)*2.0_rp
      Tmatr(3, 5) = Tmatr(3, 5)*2.0_rp
      Tmatr(3, 6) = Tmatr(3, 6)*2.0_rp

      !
      ! Matrix multiplication
      !
      do i = 1, 2_ip*ndime
         do j = 1, 2_ip*ndime
                  tennew(i) = tennew(i) +  Tmatr(i, j)*tenold(j)
         end do
      end do
		 
   end if
   
end subroutine sld_travo2

