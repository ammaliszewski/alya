subroutine sld_stress_model_100(pgaus,pmate,gpgdi,gpstr,gpcau,gpdet,gptlo,gpigd,ielem,elcod,flagt,gpdds,gpmof)

  !-----------------------------------------------------------------------
  !****f* Solidz/sld_stress_model_100
  ! NAME
  !    sld_stress_model_100
  ! DESCRIPTION
  !    Compute second Piola-Kirchoff stress tensor S_{IJ} for the ISOLIN
  !
  !    GPGDI ... Deformation tensor .............................. F = grad(phi)
  !    GPPRE ... Green-Lagrange Strain ........................... E = 1/2(F^t.F-delta I)
  !    GPSTR ... 2nd P-K Stress tensor ........................... S
  !    GPDDS ... Tangent moduli in terms of 2nd P-K stress ....... dS/dE
  !    FLAGT ... Flag to activate GPDDS (when implicit)
  ! USES
  ! USED BY
  !    sld_elmcla
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,mnode
  use def_solidz, only       :  lawco_sld,parco_sld,densi_sld,velas_sld,kfl_plane_sld
  use def_master, only       :  kfl_modul,cutim,ittim,postp,ittyp,ITASK_ENDRUN
  use def_master, only       :  lun_livei,coupling


  implicit none
  integer(ip), intent(in)    :: pgaus,pmate,ielem,flagt
  real(rp),    intent(in)    :: gpgdi(ndime,ndime,pgaus),gpmof(pgaus)
  real(rp),    intent(out)   :: gpstr(ndime,ndime,pgaus)
  real(rp),    intent(out)   :: gpdds(ndime,ndime,ndime,ndime,pgaus)
  real(rp)                   :: gpgre(ndime,ndime,pgaus),trgpg
  integer(ip)                :: igaus,i,j,k,l,idime,jdime,kdime,ldime
  real(rp)                   :: lame1,lame2,young,poiss

  real(rp)                   :: i4f,i4n,i4s,lamda(3),&
                                scasta,scastv,scastr,&
                                nfibe0(ndime,pgaus),norma0(ndime,pgaus),nshet0(ndime,pgaus),&
                                nfibe(ndime,pgaus),nfibt(ndime,pgaus),&
                                nshet(ndime,pgaus),nshtt(ndime,pgaus),&
                                norma(ndime,pgaus),normt(ndime,pgaus),tkron(ndime,ndime)

  real(rp)                   :: castr_active(ndime,ndime),gpstr_active(ndime,ndime),bidon2,gpcac
  real(rp),    intent(in)    :: gpcau(ndime,ndime,pgaus),gpdet(pgaus),gptlo(pgaus),gpigd(ndime,ndime,pgaus)

  real(rp)                   :: trace(pgaus),gphyd(ndime,ndime,pgaus),gpdev(ndime,ndime,pgaus),plane,dummr
  real(rp),    intent(in)    :: elcod(ndime,mnode)
  
  logical                    :: debugging
  
  debugging = .false.

  !Lame Constants:

  young = parco_sld(1,pmate)
  poiss = parco_sld(2,pmate)
  lame1 = parco_sld(3,pmate) !lambda
  lame2 = parco_sld(4,pmate) !mu

  
  plane = (1.0_rp-2.0_rp*poiss)/(1.0_rp-poiss)
  
  if (debugging) then
     write(*,*)'lame1 = ',lame1,'   lame2 = ',lame2
  end if

  !Now in sld_velsnd.f90
  !Compute sound velocity only at time-step 0 (initialization)
  !if (ittim == 0_ip) then
  !   velas_sld(1,pmate) = sqrt((lame1+2.0_rp*lame2)/densi_sld(1,pmate))
  !end if

  gpstr = 0.0_rp
  gpgre = 0.0_rp

  gphyd = 0.0_rp
  gpdev = 0.0_rp
  !
  ! Kronecker delta
  !
  tkron = 0.0_rp
  do idime = 1, ndime
      tkron(idime,idime) = 1.0_rp
  end do
  !
  ! Compute the tangent moduli if required (i.e., implicit scheme)
  ! Cijkl: Elastic moduli tensor
  !
  if( flagt == 1_ip ) then

     if (ndime == 3 .or. (ndime == 2 .and. kfl_plane_sld == 1)) then
     
        ! dSdE_{ijkl} = lame1*delta^{-1}_{ij}*delta^{-1}_{kl}
        !               + lame2*[delta^{-1}_{ik}*delta^{-1}_{jl}+
        !                        delta^{-1}_{il}*delta^{-1}_{jk}]
        !forall(igaus=1:pgaus, i=1:ndime, j=1:ndime, k=1:ndime, l=1:ndime)
        !   gpdds(i,j,k,l,igaus)= &
        !                lame1*tkron(i,j)*tkron(k,l)+ &
        !                lame2*((tkron(i,k)*tkron(j,l))+(tkron(i,l)*tkron(j,k)))
        !end forall
        !
        ! Cijkl = lambda * dij * dkl  +  mu * ( dik * djl + dil * djk )
        ! C     = lambda I x I + 2 mu I
        !
        do igaus = 1,pgaus
           do l = 1,ndime
              do k = 1,ndime
                 do j = 1,ndime
                    do i = 1,ndime
                       ! when no modulating fields are present, gpmof is 1.0
                       gpdds(i,j,k,l,igaus)= &
                            lame1 * gpmof(igaus) * tkron(i,j)*tkron(k,l)+ &               
                            lame2 * ( tkron(i,k)*tkron(j,l) + tkron(i,l)*tkron(j,k) )     
                    end do
                 end do
              end do
           end do
        end do
        
     else if (ndime == 2) then
     
        ! dSdE_{ijkl} = lame1*plane_stress_factor*delta^{-1}_{ij}*delta^{-1}_{kl}
        !               + lame2*[delta^{-1}_{ik}*delta^{-1}_{jl}+
        !                        delta^{-1}_{il}*delta^{-1}_{jk}]
        forall(igaus=1:pgaus, i=1:ndime, j=1:ndime, k=1:ndime, l=1:ndime)
           ! when no modulating fields are present, gpmof is 1.0
           gpdds(i,j,k,l,igaus)= &
                        lame1*gpmof(igaus)*plane*tkron(i,j)*tkron(k,l)+ &
                        lame2*((tkron(i,k)*tkron(j,l))+(tkron(i,l)*tkron(j,k)))
        end forall  
         
     end if
     
  end if

  do igaus = 1,pgaus
     !
     ! E_ij = 0.5(F_ki*F_kj-Kro_ij) (Holzapfel eq. 2.69)
     !
     do i = 1,ndime
        do j = 1,ndime
           dummr = 0.0_rp
           do k = 1,ndime
              dummr = dummr + gpgdi(k,i,igaus) * gpgdi(k,j,igaus)
           end do
           gpgre(i,j,igaus) = 0.5_rp*( dummr - tkron(i,j) )
        end do
     end do
     !
     ! trace E
     !
     trace(igaus) = 0.0_rp
     do idime = 1,ndime
        trace(igaus) = trace(igaus) + gpgre(idime,idime,igaus)
     end do
  end do 

  if( ndime == 3 .or. (ndime == 2 .and. kfl_plane_sld == 1)) then
     !
     ! S directly with fourth order tensor (Belytch p. 228)
     ! Sij = Cijkl Ekl = lambda * trace(E) delta_ij + 2 mu * Eij
     !
     do igaus = 1,pgaus
        ! when no modulating fields are present, gpmof is 1.0
        gpstr(:,:,igaus) = lame1 * gpmof(igaus) * trace(igaus) * tkron + 2.0_rp * lame2 * gpgre(:,:,igaus)
     end do

  else if( ndime == 2 ) then

     !
     ! S in 2D (plain stress)
     ! Sij = Cijkl Ekl = lambda * plane_stress_factor * trace(E) delta_ij + 2 mu * Eij
     !
     do igaus = 1,pgaus
        ! when no modulating fields are present, gpmof is 1.0
        gpstr(:,:,igaus) = lame1 * gpmof(igaus) * plane * trace(igaus) * tkron + 2.0_rp * lame2 * gpgre(:,:,igaus)
     end do

  end if

  !EC coupling
  if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
     nfibe=0.0_rp
     do igaus=1,pgaus

        !orientation fiber
        nfibe0(1,igaus) = 1.0_rp
        nfibe0(2,igaus) = 0.0_rp
        nfibe0(3,igaus) = 0.0_rp

        !n0 (norma0) = -nfibe0 x k   (OJO: Doesn't work if "e1" is align with "k")
        norma0(1,igaus) =-nfibe0(2,igaus)
        norma0(2,igaus) = nfibe0(1,igaus)
        norma0(3,igaus) = 0.0_rp

        !s0 nshet0 = nfibe0 x norma0
        nshet0(1,igaus) =  nfibe0(2,igaus)*norma0(3,igaus) - nfibe0(3,igaus)*norma0(2,igaus)
        nshet0(2,igaus) =-(nfibe0(1,igaus)*norma0(3,igaus) - nfibe0(3,igaus)*norma0(1,igaus))
        nshet0(3,igaus) =  nfibe0(1,igaus)*norma0(2,igaus) - nfibe0(2,igaus)*norma0(1,igaus)

        !I_4f = f0 * C_ij * f0 (eq. 5.1)
        i4f = 0.0_rp
        do idime=1,ndime; do jdime=1,ndime
           i4f = i4f + nfibe0(idime,igaus)*gpcau(idime,jdime,igaus)*nfibe0(jdime,igaus)
        end do; end do

        !I_4s = s0 * C_ij * s0
        i4s = 0.0_rp
        do idime=1,ndime; do jdime=1,ndime
           i4s = i4s + nshet0(idime,igaus)*gpcau(idime,jdime,igaus)*nshet0(jdime,igaus)
        end do; end do

        !I_4n = n0 * C_ij * n0
        i4n = 0.0_rp
        do idime=1,ndime; do jdime=1,ndime
           i4n = i4n + norma0(idime,igaus)*gpcau(idime,jdime,igaus)*norma0(jdime,igaus)
        end do; end do

        !Transform f=F·f0 ; s=F·s0
        do idime=1,ndime; do jdime=1,ndime
          nfibe(idime,igaus) = nfibe(idime,igaus) + gpgdi(idime,jdime,igaus)*nfibe0(jdime,igaus)
          nshet(idime,igaus) = nshet(idime,igaus) + gpgdi(idime,jdime,igaus)*nshet0(jdime,igaus)
          norma(idime,igaus) = norma(idime,igaus) + gpgdi(idime,jdime,igaus)*norma0(jdime,igaus)
        end do; end do

        !Calculate the stretch (lamfi) of the fiber
        lamda(1)=sqrt(i4f)
        lamda(2)=sqrt(i4s)
        lamda(3)=sqrt(i4n)

        !Updated fiber direction: lambda_f * f_true = F * f_0 = f
        !=> here f is realy the fiber direction in the ref. conf.
        do idime=1,ndime
           nfibt(idime,igaus) = nfibe(idime,igaus)/lamda(1)
           nshtt(idime,igaus) = nshet(idime,igaus)/lamda(2) !not used
           normt(idime,igaus) = norma(idime,igaus)/lamda(3) !not used
        end do

        gpcac= 0.0_rp   ! set Ten-Tuscher calcium to zero

        call sld_eccoup(pgaus,pmate,igaus,gpdet,gptlo,lamda,nfibt,nshtt,normt,gpigd, &
                     castr_active,gpstr_active,gpcac,ielem)

        !sum the active stress
        gpstr(:,:,igaus)=gpstr(:,:,igaus)+gpstr_active(:,:)

     end do !gaus p. in active stress
  end if ! couplig

100 format (5(E16.8,','))
end subroutine sld_stress_model_100

