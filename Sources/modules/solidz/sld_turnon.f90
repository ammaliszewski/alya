subroutine sld_turnon()
  !-----------------------------------------------------------------------
  !****f* Solids/sld_turnon
  ! NAME 
  !    sld_turnon
  ! DESCRIPTION
  !    This routine performs the following tasks:
  !    - Gets file names and open them.
  !    - Read data for the solid mechanics.
  !    - Write some info
  !    - Check if there are errrors
  !    - Allocate memory
  ! USES
  !    sld_openfi
  !    sld_reaphy
  !    sld_reabcs
  !    sld_reanut
  !    sld_reaous
  !    sld_outinf
  !    sld_memall
  !    sld_restar
  ! USED BY
  !    Solids
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use mod_iofile
  use def_solidz
  implicit none
  !
  ! Initial variables
  !
  call sld_inivar(0_ip)
  !
  ! Read the physical problem
  !
  call sld_reaphy()
  !
  ! Read the numerical treatment
  !
  call sld_reanut()
  !
  ! Read the output strategy
  !
  call sld_reaous()
  !
  ! Read the boundary conditions
  !
  call sld_reabcs()
  !
  ! Service: Parall
  !
  call sld_parall(1_ip)
  !
  ! Errors eand warnings
  !
  call sld_outerr()
  !
  ! Initial variables
  !
  call sld_inivar(1_ip)
  !
  ! Initial boundary conditions
  !
  call sld_inibcs()
  !
  ! Write info
  !
  call sld_outinf()
  !
  ! Allocate info
  !
  call sld_memall()
  !
  ! Correct fibers when needed
  !
  call sld_inivar(2_ip)  
  !
  ! Solvers
  !
  call sld_inivar(3_ip)

end subroutine sld_turnon
