subroutine Solidz(order)
  !-----------------------------------------------------------------------
  !****f* solidz/Solidz
  ! NAME 
  !    Solidz
  ! DESCRIPTION
  !    The task done corresponds to the order given by the master. 
  ! USES
  !    sld_turnon
  !    sld_timste
  !    sld_begste
  !    sld_doiter
  !    sld_concon
  !    sld_conblk
  !    sld_newmsh
  !    sld_endste
  !    sld_turnof
  ! USED BY
  !    Reapro
  !    Turnon
  !    Timste
  !    Begste
  !    Doiter
  !    Concon
  !    Conblk
  !    Newmsh
  !    Endste
  !    Turnof
  !***
  !-----------------------------------------------------------------------
  use      def_master
  !
  use mod_commdom_alya, only: INONE
  use mod_commdom_sld,  only: commdom_sld_plugin
  !
  implicit none
  integer(ip), intent(in) :: order
  integer(ip)             :: icoup

  select case (order)

  case(ITASK_TURNON)
     call sld_turnon()
     
  case(ITASK_OUTPUT)
     call sld_output()
     
  case(ITASK_INIUNK)
     call sld_iniunk()
        
  case(ITASK_TIMSTE) 
     call sld_timste()
     
  case(ITASK_BEGSTE) 
     call sld_begste()
       
  case(ITASK_DOITER)
     call sld_doiter()
 
  case(ITASK_CONCOU)
     call sld_concou()
     
  case(ITASK_CONBLK)
     call sld_conblk()
     
  case(ITASK_NEWMSH)
     !!call sld_newmsh()

  case(ITASK_ENDSTE)
     call sld_endste()
     
  case(ITASK_TURNOF)
     call sld_turnof()

  end select
  !
  ! Coupling
  !
  if( order > 1000_ip ) call sld_plugin(order-1000) ! Compute and send 

  call commdom_sld_plugin()

end subroutine Solidz
