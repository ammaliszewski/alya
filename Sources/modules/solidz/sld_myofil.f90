!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_myofil.f90
!> @author  
!> @date    19/12/2014
!> @brief   Computing the active Force. Rice myofilament model. 
!> @details Active force computed in the myofilament model of Rice. No internal cell shortening Isosarcometric case.
!> @} 
!-----------------------------------------------------------------------
subroutine sld_myofil(&
     pgaus,pmate,igaus,gpdet,gptlo,lamda,nfibt,nshtt,normt,&
     gpigd,castr_active,gpstr_active,gpcac,tactf,ielem)

  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime
  use def_solidz
  use def_master, only       :  dtime,cutim,fisoc,elmag,kfl_ephys,ittim

  implicit none

  integer(ip), intent(in)    :: pgaus,pmate,igaus,ielem  
  real(rp),    intent(in)    :: gptlo(pgaus),lamda(3),gpigd(ndime,ndime,pgaus),gpdet(pgaus),&
                                nfibt(ndime,pgaus),nshtt(ndime,pgaus),normt(ndime,pgaus),gpcac
  real(rp),    intent(out)   :: castr_active(ndime,ndime),gpstr_active(ndime,ndime),tactf
  real(rp)                   :: ca0,cam,thau,cca2p,n,beta,Cal50,tmaxi,tacts,tactn,tacis
  real(rp)                   :: fporf_t(ndime,ndime),spors_t(ndime,ndime),nporn_t(ndime,ndime)
  real(rp)                   :: nfibe_length,Factive
  integer(ip)                :: idime,jdime,kdime,ldime
  real(rp)                   :: Force
  real(rp), dimension(9)     :: iy, y,  f, b, yf
  real(rp)                   :: SLmax, SLmin, lenthin, lenthick, lenhbare, SL0
  real(rp)                   :: Qkon, Qkoff, Qknp, Qkpn, Qfapp, Qgapp
  real(rp)                   :: Qhf, Qhb, Qgxb, QT, TempK
  real(rp)                   :: kon, koffL, koffH, knp, kpn
  real(rp)                   :: konT, koffLT, koffHT, knpT, kpnT, kxb
  real(rp)                   :: permtot, invpermtot, Tropreg
  real(rp)                   :: fapp, gapp, gslmod, hf, hfmdc, hb, gxb, xbmodsp, perm50
  real(rp)                   :: gappslmod, hfmod, gxbmod
  real(rp)                   :: fappT, gappT, hfT, hbT, gxbT
  real(rp)                   :: sigmap, sigman
  real(rp)                   :: x0, phi
  real(rp)                   :: SLrest, PConti, PExpti, PConcol, PExpCol
  real(rp)                   :: SLcol, mass, visc, KSE
  real(rp)                   :: sovrze, sovrcle, lensovr, SOVFthin, SOVFthick
  real(rp)                   :: XBdutyprer, XBdutypostr, XBMPrer, XBMPostr
  real(rp)                   :: t, h, P, N0XB
  real(rp)                   :: nperm
  integer(ip)                :: j, k, i!, l 

                                
  castr_active=0.0_rp
  gpstr_active=0.0_rp
  ca0  = 0.0_rp!0.01 !mmole
  cam  = 1.0_rp !mmole
  !Default value: 0.06_rp sec (human) ... !0.01268 sec Rat
  thau = timec_sld(pmate) 
    
  
    if (kfl_ephys(pmate) == 1) then   !! Ten Tuscher calcula el Ca2+ dins del propi model
     !
     ! Cell model
     !
     cca2p=gpcac*1000  !Changed by JAZ, was wrong!!
     !C50
     Cal50= cal50_sld(pmate)!0.6 !value by Hung 
     ! Cal50= 4.35_rp/sqrt(exp(4.75_rp*((1.9*lamda(1))-1.58_rp))-1.0_rp) 
  else                       !! FHN, Fenton necessiten un valor Ca2+ 
     !
     ! Otherwise
     !
     cca2p= ca0 + (cam-ca0)*(gptlo(igaus)/thau)*exp(1.0-(gptlo(igaus)/thau))
  end if

  !--------------------------------------------------------------
  !             Initial Conditions     
  !--------------------------------------------------------------

  iy(1) = 0.0_rp
  iy(2) = 0.007_rp
  iy(3) = 1.0_rp
  iy(4) = 2.0579e-7_rp
  iy(5) = 8.7571e-7_rp
  iy(6) = (1.0_rp-1.8e-7_rp)
  iy(7) = 1.8e-7_rp
  iy(8) = 0.0196_rp
  iy(9) = 0.1667_rp


  !--------------------------------------------------------------
  !            CONSTANTS                                         
  !--------------------------------------------------------------

  kon          = 50000.0_rp      !(mM^-1 s^-1)
  koffL        = 250.0_rp     !(s^-1)
  koffH        = 25.0_rp      !(s^-1)
  perm50       = 0.6738_rp
  !     perm50       = 0.55
  knp          = 500.0_rp      !(1/s)
  kpn          = 50.0_rp     !(1/s)


  fapp         = 500.0_rp    !(1/s)
  gapp         = 70.0_rp     !(1/s)
  gslmod       = 6.0_rp      
  hf           = 2000.0_rp   !(1/s)
  hfmdc        = 5.0_rp
  hb           = 400.0_rp    !(1/s)
  gxb          = 70.0_rp     !(1/s)
  kxb          = 1200000.0_rp    !(Dynes/cm^2)     
  sigmap       = 8.0_rp
  sigman       = 1.0_rp
  xbmodsp      = 0.2_rp

  !! Temperature Dependence (degrees Kelvin)
  TempK        = 310  !273 + 37
  QT           = (TempK-310_rp)/10_rp
  Qkon         = 1.5**QT
  Qkoff        = 1.3**QT
  Qknp         = 1.6**QT
  Qkpn         = 1.6**QT
  Qfapp        = 6.25**QT
  Qgapp        = 2.5**QT
  Qhf          = 6.25**QT
  Qhb          = 6.25**QT
  Qgxb         = 6.25**QT


  x0           = 0.007_rp * 0.0001_rp    !(cm)
  phi          = 2.0_rp


  SLmax         = 2.4_rp * 0.0001_rp     !(cm)
  SLmin         = 1.4_rp * 0.0001_rp     !(cm)
  lenthick      = 1.65_rp * 0.0001_rp    !(cm)
  lenthin       = 1.2_rp * 0.0001_rp     !(cm)
  lenhbare      = 0.1_rp * 0.0001_rp     !(cm)
  SL0           = 1.85_rp * 0.0001_rp     !(cm)


  SLrest        = 1.85_rp * 0.0001_rp     !(cm)
  SLcol         = 2.25_rp * 0.0001_rp     !(cm)
  PConti        = 0.002_rp     !(Unit normalized force)
  PExpti        = 10.0_rp      
  PConcol       = 0.02_rp      !(Unit normalized force)
  PExpcol       = 70.0_rp
  nperm         = 8.1997_rp
  !    nperm         = 7.5

  mass          = 0.2_rp !([norm Force s^2]/cm)
  visc          = 30.0_rp   !(norm Force s/cm)
  KSE           = 10000.0_rp     !(Unit normalised force)/cm 

  !------------------------------------------------------------
  !         PARAMETERS THAT DEPEND ON OTHER CONSTANTS  
  !------------------------------------------------------------

  konT         = kon*Qkon
  koffLT       = koffL*Qkoff
  koffHT       = koffH*Qkoff

  !--------------------------------------------------------------
  !        SOLVING THE SYSTEM OF ODE's. EULER.
  !--------------------------------------------------------------

  y = iy
  t = cutim
  h=  dtime

!  do i = 1, 150000

     !          l = 1

     !        do while (0.0001>=t+h(l))

     !           l = l + 1

     sovrze       = min(0.5_rp*lenthick,0.5_rp*SL0)
     sovrcle      = max(SL0/2_rp-(SL0-lenthin),lenhbare*0.5_rp)
     lensovr      = sovrze-sovrcle
     SOVFthick    = (2.0_rp*lensovr)/(lenthick-lenhbare)
     SOVFthin     = lensovr/lenthin


     gappslmod    = 1_rp + (1_rp-SOVFthick)*gslmod

     fappT        = fapp*xbmodsp*Qfapp
     gappT        = gapp*gappslmod*xbmodsp*Qgapp


     !if (y(10) >= SLrest) then 

     !            Ftitin       = PConti*(exp(PExpti*(y(10)-SLrest))-1.0)



     !      else if (y(10) < SLrest) then

     !            Ftitin       = -PConti*(exp(PExpti*(SLrest-y(10)))-1.0)

     !      end if

     !       if (y(10) >= SLcol) then

     !           Fcol = PConcol*(exp(PExpcol*(y(10)-SLcol))-1)

     !       else if (y(10) < SLcol) then

     !            Fcol         = 0.0  

     !       end if 

     !       Fpassive     = Ftitin + Fcol
     !       Fafterload   = KSE*(y(10)-SLrest)  
     !       print*, 'Fafterload', Fafterload, 'Fpassive', Fpassive
     !       Fpreload     = 0.0

     !        l = 1

     !        do while (0.0001>=t+h(l))

     !           l = l + 1


     Tropreg      = (1.0_rp-SOVFthin)*y(8) + SOVFthin*y(9)
     permtot      = sqrt(1.0_rp/(1.0_rp+(perm50/Tropreg)**nperm))
     invpermtot   = min(1.0_rp/permtot,100.0_rp)

     knpT         = knp*permtot*Qknp
     kpnT         = kpn*invpermtot*Qkpn
     hfmod        = exp((SIGN(0.5_rp,y(1))-SIGN(0.5_rp,-y(1)))*hfmdc*(y(1)/x0)*(y(1)/x0)) 


     if ( (x0-y(2)) >= 0.0_rp )   then

        gxbmod        = exp(sigman*((x0-y(2))/x0)*((x0-y(2))/x0))

     else 

        gxbmod        = exp(sigmap*((x0-y(2))/x0)*((x0-y(2))/x0))

     end if



     hfT          = hf*hfmod*xbmodsp*Qhf
     hbT          = hb*xbmodsp*Qhb
     gxbT         = gxb*gxbmod*xbmodsp*Qgxb




     XBMPrer      = (fapp*hb+fapp*gxb)/(gxb*hf+fapp*hf+gapp*hb+gapp*gxb+fapp*hb+fapp*gxb)
     XBMPostr     = fapp*hf/(gxb*hf+gapp*hb+fapp*hf+gapp*gxb+fapp*hb+fapp*gxb)

     XBdutyprer   = (fappT*hbT+fappT*gxbT)/(gxbT*hfT+fappT*hfT+gappT*hbT+gappT*gxbT+fappT*hbT+fappT*gxbT)
     XBdutypostr  =(fappT*hfT)/(gxbT*hfT+fappT*hfT+gappT*hbT+gappT*gxbT+fappT*hbT+fappT*gxbT)

     !      Factive = (kxb*SOVFthick*(y(1)*y(4)+y(2)*y(5)))/(kxb*x0*XBMPostr)



     b(1) = -x0*phi*hbT/XBdutyprer 
     b(2) = (phi/XBdutypostr)*x0*hfT 
     b(3) = kpnT
     b(4) = fappT
     b(5) = 0.0_rp
     b(6) = 0.0_rp
     b(7) = 0.0_rp
     b(8) = cca2p*konT
     b(9) = cca2p*konT


     f(1) = phi/XBdutyprer*(-(fappT+hbT)*y(1)+(hbT*y(2)))               + b(1) 
     f(2) = hfT*(phi/XBdutypostr)*(y(1)-y(2))                           + b(2)
     f(3) = -(knpT+kpnT)*y(3)-kpnT*(y(4)+y(5))                          + b(3)
     f(4) = -fappT*y(3)-(gappT+hfT+fappT)*y(4)+(hbT-fappT)*y(5)         + b(4)
     f(5) = hfT*y(4)-(hbT+gxbT)*y(5)                                    + b(5)
     f(6) = -knpT*y(6)+kpnT*y(7)                                        + b(6)
     f(7) = knpT*y(6)-kpnT*y(7)                                         + b(7)
     f(8) =-(cca2p*konT+koffLT)*y(8)                                    + b(8)
     f(9) =-(cca2p*konT+koffHT)*y(9)                                    + b(9)





     P       = 1_rp - y(3) - y(4) - y(5)
     N0XB    = 1_rp - y(7)
     Factive = (kxb*SOVFthick*(y(1)*y(4)+y(2)*y(5)))/(kxb*x0*XBMPostr)
     Force   = (kxb*SOVFthick*(y(1)*y(4)+y(2)*y(5)))


     do j = 1, 9         


        yf = y + h*f         


     end do


     y = yf
     t = t + h

     tactf=Factive * 1.0e6_rp * cocof_sld(pmate) !example to scale

     !       t = t + h(l)
  tacts = 0.0_rp
  tactn = 0.0_rp
  tacis = 0.0_rp  ! for transversally isotropic coupling
!!  JAZZY DID THIS TO SPEED THINGS UP AND DEBUG!!!
  !if (kfl_coupt_sld(pmate) == 2) then
  !   tacis = tactf * trans_sld(1,pmate)
  !   tactf = tactf * (1.0_rp - trans_sld(1,pmate))
  !end if
!!!  END OF JAZZY'S MESSUP

  tactv_sld= tactv_sld + tactf/real(pgaus)

  !f_true x f_true (outer products [3x3]) 
  fporf_t=0.0_rp
  spors_t=0.0_rp
  nporn_t=0.0_rp    
  nfibe_length= 0.0_rp
  do idime=1,ndime
     nfibe_length=   nfibe_length + nfibt(idime,igaus)*nfibt(idime,igaus) 
     do jdime=1,ndime
        fporf_t(idime,jdime) = nfibt(idime,igaus)*nfibt(jdime,igaus) 
        spors_t(idime,jdime) = nshtt(idime,igaus)*nshtt(jdime,igaus) 
        nporn_t(idime,jdime) = normt(idime,igaus)*normt(jdime,igaus)
     end do
  end do
  nfibe_length= sqrt(nfibe_length) 
!  if (ielem == 1253) write(6,*) 'poto',ielem,nfibe_length,tactf
!  if (ielem ==    1) write(6,*) 'poto',ielem,nfibe_length,tactf

  ! Cauchy active stress tensor (castr_active)
  do idime=1,ndime
     do jdime=1,ndime          
        castr_active(idime,jdime) = (fporf_t(idime,jdime)*tactf) +&
                                    (spors_t(idime,jdime)*tacts) +&
                                    (nporn_t(idime,jdime)*tactn)
     end do
  end do

!     write(100, *) t, y(8)!, h 
!     write(200, *) t, y(9)!, h 
!     write(300, *) t, y(1)!, h 
!     write(400, *) t, y(2)!, h
!     write(500, *) t, y(3)!, h 
!     write(600, *) t, y(4)!, h
!     write(700, *) t, y(5)!, h 
!     write(800, *) t, y(6)!, h 
!     write(900, *) t, y(7)!, h
!     write(1000,*) t, P
     write(2000,*) t, Factive
!     write(4000,*) t, N0XB
     write(6000,*) t, Force

!  end do





end subroutine sld_myofil
