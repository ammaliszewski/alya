subroutine sld_updtss()
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_updtss
  ! NAME 
  !    sld_updtss
  ! DESCRIPTION
  !    This routine computes the time step size 
  !    equation.
  ! USED BY
  !    sld_begste
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_solidz
  use def_elmtyp   ! type of elements

  implicit none 
  integer(ip) :: ielem,kelem,idime,inode,ipoin,pnode
  integer(ip) :: pdime,pmate,pelty,porde,eltot 
  real(rp)    :: dtcri,dtmin,hmini
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: tragl(9),hleng(3),eleng,dummr

  dtmin = 1.0e9_rp
  dtcri = 0.0_rp
  eleng = 0.0_rp
  eltot = 0_ip

 
  if( ittim > nisaf_sld ) safet_sld = min(safet_sld*safex_sld, safma_sld)

  if( INOTMASTER ) then
     !
     ! Computing the critical time-step based on the speed of sound and the element characteristic length-scale
     !
     elements: do kelem = 1,nelez(current_zone)
        ielem = lelez(current_zone) % l(kelem)
        pelty = ltype(ielem)
        
        if ( pelty > 0 .and. pelty /= SHELL .and. pelty /= BAR3D ) then
           
           porde = lorde(pelty)
           pnode = nnode(pelty)
           pdime = ldime(pelty)
           pmate = 1

           if( nmate_sld > 1 ) then
              pmate = lmate_sld(ielem)
           end if
           
           call sld_velsnd(pmate)
           
           do inode = 1,pnode
              ipoin = lnods(inode,ielem)
              do idime = 1,ndime
                 elcod(idime,inode) = coord(idime,ipoin)
              end do
           end do
           call elmlen(&
                pdime,pnode,elmar(pelty)%dercg,tragl,elcod,&
                hnatu(pelty),hleng)
           hmini = hleng(ndime) / real(porde,rp)
           dummr = 0.0_rp
           do idime = 1,ndime
              dummr = dummr + hleng(idime)
           end do
           !              eleng = eleng + dummr / real(ndime,rp)
           if (kfl_xfeme_sld > 0) eleng_sld(ielem) = dummr / real(ndime,rp)
           dtcri = spect_sld(2) * hmini / velas_sld(1,pmate)
           dtmin = min(dtmin,dtcri)
           
        end if
        
     end do elements
!     eltot = nelem
     
  end if
  !
  ! Look for minimum over whole mesh
  !
  call pararr('MIN',0_ip,1_ip,dtmin)
  
!  call pararr('SUM',0_ip,1_ip,eleng)
!  call parari('SUM',0_ip,1_ip,eltot)
  
!  eleng_sld = eleng / real(eltot,rp)

  dtcri_sld = dtmin
  if( dtcri_sld /= 0.0_rp ) dtinv_sld = 1.0_rp/((dtcri_sld+zeror)*safet_sld)
  if( kfl_timco == 1 )      dtinv     = max(dtinv,dtinv_sld)


end subroutine sld_updtss
