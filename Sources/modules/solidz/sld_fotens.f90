!-----------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    sld_fotens.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Compute some tensors for postprocess
!> @details Compute some tensors for postprocess
!> @} 
!-----------------------------------------------------------------------
subroutine sld_fotens()
  use def_parame
  use def_elmtyp
  use def_master
  use def_domain
  use def_solidz
  implicit none
  integer(ip)             :: ipoin,ivoig,ibopo,idumm,imeth, i, j 
  real(rp)                :: dummr,nx,ny,nz,vdumm(3,3),G_ij(3,3),G_val(3)

  do ipoin = 1,npoin
     do ivoig = 1,nvoig_sld
        caust_sld(ivoig,ipoin) = 0.0_rp
        green_sld(ivoig,ipoin) = 0.0_rp
        lepsi_sld(ivoig,ipoin) = 0.0_rp
     end do
  end do
 
 
  imeth = 1
  if( imeth == 2 ) call opeclo(1_ip)  
  call sld_elmope(3_ip)
  if( imeth == 2 ) call opeclo(2_ip)  

  call rhsmod(nvoig_sld,caust_sld)
  call rhsmod(nvoig_sld,green_sld)
  call rhsmod(nvoig_sld,lepsi_sld)

  do ipoin = 1,npoin
     dummr = 1.0_rp/vmass(ipoin)
     do ivoig = 1,nvoig_sld
        caust_sld(ivoig,ipoin) = dummr * caust_sld(ivoig,ipoin) 
        green_sld(ivoig,ipoin) = dummr * green_sld(ivoig,ipoin) 
        lepsi_sld(ivoig,ipoin) = dummr * lepsi_sld(ivoig,ipoin)    
     end do

     ibopo= lpoty(ipoin)
     caunn_sld(ipoin)= 0.0_rp
     if (ibopo > 0) then
        if (ndime == 2)  then
           nx = exnor(1,1,ibopo)
           ny = exnor(1,2,ibopo)
           caunn_sld(ipoin)= &
                nx * caust_sld(1,ipoin) * nx + &
                ny * caust_sld(2,ipoin) * ny + &
                2.0_rp * nx * caust_sld(3,ipoin) * ny
        else if (ndime == 3) then


        end if
     end if
  end do
  !
  ! Combined Stresses
  !
  if( ndime == 3 ) then

     do ipoin = 1,npoin
        seqvm_sld(ipoin) = &
             (sqrt(0.5_rp*((caust_sld(1,ipoin)-caust_sld(2,ipoin))**2.0_rp+&
             (caust_sld(2,ipoin)-caust_sld(3,ipoin))**2.0_rp+&
             (caust_sld(3,ipoin)-caust_sld(1,ipoin))**2.0_rp+&               
             6.0_rp*(caust_sld(4,ipoin)**2.0_rp+caust_sld(5,ipoin)**2.0_rp+caust_sld(6,ipoin)**2.0_rp))))

        if (kfl_rotei_sld == 1) call sld_troloc(0_ip,G_ij)     !compute roloc_sld tensor
        
        !G_ij(1,1)= caust_sld(1,ipoin)
        !G_ij(2,2)= caust_sld(2,ipoin)
        !G_ij(3,3)= caust_sld(3,ipoin)
        !G_ij(1,2)= caust_sld(4,ipoin)
        !G_ij(1,3)= caust_sld(5,ipoin)
        !G_ij(2,3)= caust_sld(6,ipoin)
        !G_ij(2,1)= G_ij(1,2)
        !G_ij(3,1)= G_ij(1,3) 
        !G_ij(3,2)= G_ij(2,3) 
        
        do i = 1, ndime
           do j = 1, ndime
              ivoig = nvgij_inv_sld(i, j)
              G_ij(i, j) = caust_sld(ivoig,ipoin)
           end do
        end do


        if (kfl_rotei_sld == 1) call sld_troloc(ipoin,G_ij)     !rotate and correct tensor
       
        call spcdec(G_ij,G_val,vdumm,idumm,0_ip)   ! eigenvalues sorted in ascending order  
        
        G_val(1) = abs(G_val(1))
        G_val(3) = abs(G_val(3))
        if (G_val(3) .gt. G_val(1)) G_val(1)= G_val(3)
        
        sigei_sld(ipoin) = G_val(1)

     end do
  else if (ndime==2) then
     ! ... not coded yet
  end if

end subroutine sld_fotens
