subroutine sld_elmmat(itask,&
     ielem,pgaus,pmate,pnode,lnods,gpgdi,gppio,gpstr,gpvol,gpvel,gptmo,&
     gprat,gpsha,gpcar,gphea,gpdis,gpdds,gprst,eldis,elcod,elrhs,elmat,elmuu,&
     elmaa,elfrx)
  !-----------------------------------------------------------------------
  !****f* Solidz/sld_elmmat
  ! NAME 
  !    sld_elmmat
  ! DESCRIPTION
  !    Compute right hand side
  ! OUTPUT
  ! USES
  ! USED BY
  !    sld_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg
  use def_elmtyp, only       :  ELEXT
  use def_master, only       :  dtime,lun_livei,solve
  use def_domain, only       :  ndime,mnode,mgaus,lelch
  use def_solidz, only       :  ndofn_sld,densi_sld,grnor_sld
  use def_solidz, only       :  gravi_sld,dampi_sld,kfl_fixno_sld
  use def_solidz, only       :  bodyf_sld,ncomp_sld,tifac_sld,accel_sld
  use def_solidz, only       :  kfl_probl_sld,kfl_aleso_sld
  use def_solidz, only       :  axfem_sld,lnenr_sld
  use def_solidz, only       :  kfl_exacs_sld,kfl_xfeme_sld
  use def_solidz, only       :  kfl_arcle_sld, lambda_sld, deltaE_sld, arcint_sld
  implicit none
  integer(ip), intent(in)    :: itask
  integer(ip), intent(in)    :: pgaus
  integer(ip), intent(in)    :: pmate
  integer(ip), intent(in)    :: pnode
  integer(ip), intent(in)    :: ielem
  integer(ip), intent(in)    :: lnods(pnode)
  real(rp),    intent(in)    :: gppio(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpstr(ndime,ndime,pgaus)              ! Stress tensor S
  real(rp),    intent(in)    :: gpvol(pgaus)
  real(rp),    intent(in)    :: gphea(pnode,mgaus)                    ! Shifted heaviside (use mgaus!!)
  real(rp),    intent(in)    :: gpvel(ndime,pgaus)                    ! Velocity at the gauss point (first ncomp_nsa)
  !                                     i     J     k     L
  real(rp),    intent(in)    :: gprst(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gptmo(ndime,ndime,ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpcar(ndime,mnode,mgaus)
  real(rp),    intent(in)    :: gpgdi(ndime,ndime,mgaus)
  real(rp),    intent(in)    :: gprat(ndime,ndime,mgaus)               ! Rate of deformation Fdot at time n
  real(rp),    intent(in)    :: gpdis(ndime,pgaus,*)
  real(rp),    intent(in)    :: gpdds(ndime,ndime,ndime,ndime,mgaus)   ! Cijkl
  real(rp),    intent(in)    :: eldis(ndime,pnode,ncomp_sld)
  real(rp),    intent(in)    :: elcod(ndime,pnode)                     ! Node coordinates
  real(rp),    intent(out)   :: elrhs(ndofn_sld*pnode+arcint_sld)
  real(rp),    intent(out)   :: elmat(ndofn_sld*pnode+arcint_sld,ndofn_sld*pnode+arcint_sld)
  real(rp),    intent(out)   :: elmuu(pnode)
  real(rp),    intent(out)   :: elmaa(pnode)
  real(rp),    intent(out)   :: elfrx(ndofn_sld,pnode)

  real(rp)                   :: elmas(pnode,pnode),elmas_ua(pnode,pnode)
  real(rp)                   :: elmas_au(pnode,pnode),elmas_aa(pnode,pnode)
  integer(ip)                :: igaus,inode,idime,jdime,kdime
  integer(ip)                :: jnode,ipoin,iprin,idofn,jdofn
  integer(ip)                :: anode,bnode,adofn,bdofn,ldime
  real(rp)                   :: adiag,dtfa1,dtfa2,volux,betan,dummr,grale
  real(rp)                   :: betar,alphr,tonta
  logical(lg)                :: debuggingMode

  real(rp)                   :: fext(ndime,pnode)                  ! arc-length variables
  !real(rp)                   :: lambda(3)                              ! lambda^n(i), lambda^n(i-1), lambda^n-1


  debuggingMode = .false. ! (ielem == 617 .and. (ittim == 77 .or. ittim == 76))

!  bidonE=0.0
!  bidonI=0.0

  if (debuggingMode) then
     write(lun_livei,*)' '
     write(lun_livei,*)'ielem =',ielem
  end if

  betan = 0.0_rp
  if( tifac_sld(1) > 0.0_rp ) betan = 1.0_rp / (tifac_sld(1)*dtime*dtime)

  dtfa1 = 1.0_rp - tifac_sld(3)
  dtfa2 = tifac_sld(3)

  alphr = (dampi_sld(1,pmate) / dtime)             ! Rayleigh damping: alpha/dt
  betar = (1.0_rp + dampi_sld(2,pmate) / dtime)    ! Rayleigh damping: 1 + beta/dt

  grale = grnor_sld
  if( kfl_aleso_sld(pmate) == 1 ) grale = 0.0_rp    ! ALE material has no gravity influence

  if( itask == 1 ) then

     !-------------------------------------------------------------------
     !
     ! EXPLICIT SCHEME:
     ! Compute the elemental right hand side 
     !
     !-------------------------------------------------------------------

     do idofn = 1,ndofn_sld*pnode
        elrhs(idofn) = 0.0_rp
     end do

     iprin = 0
     do igaus = 1,pgaus
        volux = gpvol(igaus)
        do inode = 1,pnode
           ipoin = lnods(inode)
           if (ipoin==1) iprin=1
           idofn = (inode-1)*ndofn_sld
           do idime = 1,ndime
              idofn = idofn+1
              do jdime = 1,ndime
                 elrhs(idofn) = elrhs(idofn) &
                      - (gppio(idime,jdime,igaus)+gprst(idime,jdime,igaus))*gpcar(jdime,inode,igaus)*volux
              end do

              elrhs(idofn)= elrhs(idofn)                           &
                   + (gpsha(inode,igaus)*densi_sld(1,pmate)*volux) &
                   *(grale*gravi_sld(idime) + bodyf_sld(idime)   &      ! gravity andforce
                   - dampi_sld(1,pmate) * gpvel(idime,igaus)) ! RHS mass matrix term of the rayleigh damping                

             !if (igaus==1) then 
             !if (inode==1) then 
             !if (idime==2) then                                        
             !   bidonE= bidonE + dampi_sld(1,pmate) * gpvel(2,igaus)   
             !end if
             !end if
             !end if

           end do

           if (kfl_xfeme_sld == 1) then
              do idime = 1,ndime
                 idofn = idofn+1
                 !
                 ! the term corresponding to xfem...
                 !
                 do jdime = 1,ndime
                    elrhs(idofn) = elrhs(idofn) &
                         - (gppio(idime,jdime,igaus)+gprst(idime,jdime,igaus))&
                         *gpcar(jdime,inode,igaus)*volux*gphea(inode,igaus)
                 end do
              end do
           end if

        end do
     end do
              
   ! write(*,*) 'bidonE ',bidonE,gpvel(2,1)
    
    
     if (dampi_sld(2,pmate) > 0.0_rp) then        
        ! RHS stiffness matrix term of the rayleigh damping

        do igaus=1,pgaus
           volux= gpvol(igaus)
           do inode=1,pnode
              idofn= (inode-1)*ndime
              do idime=1,ndime
                 idofn=idofn+1
                 do jdime=1,ndime
                    do kdime=1,ndime
                       elrhs(idofn)= elrhs(idofn) &
                            + dampi_sld(2,pmate) &
                            * gpcar(jdime,inode,igaus) &
                            * gprat(kdime,jdime,igaus) &
                            * gpstr(jdime,kdime,igaus) &
                            * volux
                    end do
                 end do
              end do
           end do
        end do

     end if

     elmuu = 0.0_rp
     elmaa = 0.0_rp
     !
     ! M_ab = int_\Omega /rho * Na * Nb d\Omega
     !
     do igaus = 1,pgaus
        volux = gpvol(igaus)
        do anode = 1,pnode
           do bnode = 1,pnode
              elmuu(anode) = elmuu(anode) &
                   + densi_sld(1,pmate)*volux &
                   *gpsha(anode,igaus)*gpsha(bnode,igaus)
           end do
        end do
     end do

     if (kfl_xfeme_sld == 1) then           
        do igaus = 1,pgaus
        !**DT: Addition of stiffness matrix related to unknown enrichments
           !
           ! the term corresponding to xfem: M_ua_ab, M_au_ab, M_aa_ab
           !
           volux = gpvol(igaus)
           do anode = 1,pnode
              do bnode = 1,pnode
                 elmaa(anode) = elmaa(anode) &
                      + densi_sld(1,pmate)*volux &
                      *gpsha(anode,igaus)*gpsha(bnode,igaus) &
                      *abs(gphea(anode,igaus)*gphea(bnode,igaus))
!                 elmaa(anode) = elmaa(anode) &
!                      + densi_sld(1,pmate)*volux &
!                      *gpsha(anode,igaus)*gpsha(bnode,igaus) &
!                      *abs(gphea(bnode,igaus))
!                 elmaa(anode) = elmaa(anode) &
!                      + densi_sld(1,pmate)*volux &
!                      *gpsha(anode,igaus)*gpsha(bnode,igaus) &
!                      *abs(gphea(anode,igaus))
              end do
           end do
        end do
     end if
     !
     ! Exact solution
     !
     if( kfl_exacs_sld /= 0 ) then
        call sld_elmexa(&
             1_ip,pgaus,pnode,elcod,gpsha,gpcar,gpdds,gpvol,&
             eldis,gpdis,gpgdi,elrhs)
     end if
     !
     ! Compute reaction force (lagged)
     ! 
     elfrx(1:ndofn_sld,1:pnode) = 0.0_rp
     do inode = 1,pnode
        ipoin = lnods(inode)
        do idime = 1,ndime
           if( kfl_fixno_sld(idime,ipoin) > 0 ) then
              idofn = (inode-1)*ndofn_sld + idime
              elfrx(idime,inode) = elrhs(idofn)
           end if
        end do
     end do
     
     if( debuggingMode ) then
        write(lun_livei,'(a8,100(x,e10.3))')'elmuu = ',(elmuu(idofn),idofn = 1,pnode)
        write(lun_livei,'(a8,100(x,e10.3))')'elmaa = ',(elmaa(idofn),idofn = 1,pnode)
     end if

  else if( itask == 2 ) then

     !-------------------------------------------------------------------
     !
     ! IMPLICIT SCHEME:
     ! Compute the elemental right hand side and matrix
     !
     !-------------------------------------------------------------------
     !
     ! critical changes:
     ! initialization: reset all entries of elrhs and elmat to zero
     !
     !-------------------------------------------------------------------

     if (kfl_arcle_sld == 1) then
      do inode = 1,ndofn_sld*pnode + arcint_sld
        elrhs(inode) = 0.0_rp
        do jnode = 1,ndofn_sld*pnode + arcint_sld
           elmat(jnode,inode) = 0.0_rp
        end do
       end do
     else
       do inode = 1,ndofn_sld*pnode
          elrhs(inode) = 0.0_rp
          do jnode = 1,ndofn_sld*pnode
             elmat(jnode,inode) = 0.0_rp
          end do
       end do
     end if
     !
     ! f_kb = int_\Omega P_kL * dNb/dxL d\Omega
     !
     iprin = 0
     do igaus = 1,pgaus
        volux = gpvol(igaus)
        do inode = 1,pnode
           ipoin = lnods(inode)
           if (ipoin==1) iprin=1
           idofn = (inode-1) * ndofn_sld
           do idime = 1,ndime
              idofn = idofn + 1
              do jdime = 1,ndime
                 elrhs(idofn) = elrhs(idofn) &
                      - (gppio(idime,jdime,igaus)+gprst(idime,jdime,igaus)) &
                      * gpcar(jdime,inode,igaus) * volux
              end do
              !
              ! Check this when damping goes to rhs
              !  Implicit with C on the rhs ("explicit"). 
              !  If use this also comment contribution of C in the matrix
              tonta = gpvel(idime,igaus) * dtime  
              !tonta = gpdis(idime,igaus,1)      !Real implicit
         
              !fext = int_\Omega b_0*N d\Omega + int_\dOmega tract*N d\S
              !       b_0 are bodyforces and  gravity
              !       tract are traction forces (Neumann)              

              elrhs(idofn) = elrhs(idofn) &
                   + (gpsha(inode,igaus)*densi_sld(1,pmate)*volux) &
                   * (grale*gravi_sld(idime) - bodyf_sld(idime) &
                    - alphr*tonta)
                  
 
           end do           
           !
           !**DT: Addition of internal force vector related to unknown enrichments
           !
           if (kfl_xfeme_sld == 1) then
              do idime = 1,ndime
                 idofn = idofn + 1
                 !
                 ! the term corresponding to xfem...
                 !
                 do jdime = 1,ndime
                    elrhs(idofn) = elrhs(idofn) &
                         - (gppio(idime,jdime,igaus)+gprst(idime,jdime,igaus))*gpcar(jdime,inode,igaus) &
                         *volux*gphea(inode,igaus)
                 end do
                 elrhs(idofn) = elrhs(idofn) &
                      + gpsha(inode,igaus)*(grale*gravi_sld(idime) - bodyf_sld(idime) )&
                      *densi_sld(1,pmate)*volux*gphea(inode,igaus)
              end do
           end if
           !
           !**ECR: Addition of internal force vector related to arc-length method: constraint equation g
           !       g = 0.5*(lambda0*(u-u0) -(lambda-lambda0)*u0)*fext - deltaE  
           !
           if (kfl_arcle_sld == 1) then
             idofn = idofn + 1
             do idime = 1,ndime
               fext(idime,inode) = fext(idime,inode) + (gpsha(inode,igaus)*densi_sld(1,pmate)*volux) &
                     * (grale*gravi_sld(idime) - bodyf_sld(idime) &
                      - alphr*tonta)
             end do
           end if

        end do
     end do

     !
     !**ECR: Addition of internal force vector related to arc-length method: constraint equation g
     !       g = 0.5*(lambda0*(u-u0) -(lambda-lambda0)*u0)*fext - deltaE  
     !
     if (kfl_arcle_sld == 1) then
       do inode = 1,pnode
         ipoin = lnods(inode)
         idofn = pnode * ndofn_sld + 1
         do idime = 1,ndime
             elrhs(idofn) = elrhs(idofn) &
                      + 0.5_rp*(lambda_sld(1)*(eldis(idime,inode,1)-eldis(idime,inode,2) &
                      - (lambda_sld(1)-lambda_sld(3))*eldis(idime,inode,2))*fext(idime,inode)) 
         end do
       end do
       elrhs(idofn) = elrhs(idofn) - deltaE_sld
     end if

     !
     ! K_iakb = int_\Omega (dP_iJ / dF_kL * dNa/dxJ * dNb/dxL * (1 + beta_rayleigh / dt)  
     !                      + (alpha_rayleigh / dt) Na * Nb  ) d\Omega
     !
     do igaus = 1,pgaus
        volux= gpvol(igaus)
        !
        ! Time matrix, including Rayleigh damping contribution to the system matri
        !
        do anode = 1,pnode
           do idime = 1,ndime
              adofn = ( anode - 1 ) * ndofn_sld + idime
              do bnode = 1,pnode
                 do kdime = 1,ndime
                    bdofn = ( bnode - 1 ) * ndofn_sld + kdime
                    do jdime = 1,ndime
                       do ldime = 1,ndime
                          elmat(adofn,bdofn) = elmat(adofn,bdofn) &
                               + (gptmo(idime,jdime,kdime,ldime,igaus) &
                               *  gpcar(jdime,anode,igaus) * gpcar(ldime,bnode,igaus) * betar) &
                               * volux
                       end do
                    end do
                    !elmat(adofn,bdofn) = elmat(adofn,bdofn) &
                    !     + densi_sld(1,pmate)*alphr * volux &
                    !     * gpsha(anode,igaus)*gpsha(bnode,igaus)                    
                 end do
              end do
           end do
        end do
        !
        !**DT: Addition of stiffness matrix related to unknown enrichments
        !
        if (kfl_xfeme_sld == 1) then
           !
           ! the term corresponding to xfem: K_ua_iakb
           !
           do anode = 1,pnode
              do idime = 1,ndime
                 adofn = ( anode - 1 ) * ndofn_sld + idime
                 do bnode = 1,pnode
                    do kdime = 1,ndime
                       bdofn = ( bnode - 1 ) * ndofn_sld + kdime + ndime
                       do jdime = 1,ndime
                          do ldime = 1,ndime
                             elmat(adofn,bdofn) = elmat(adofn,bdofn) &
                                  + gptmo(idime,jdime,kdime,ldime,igaus) * volux &
                                  * gpcar(jdime,anode,igaus) * gpcar(ldime,bnode,igaus) &
                                  * gphea(bnode,igaus)
                          end do
                       end do
                    end do
                 end do
              end do
           end do
           !
           ! the term corresponding to xfem: K_au_iakb
           !
           do anode = 1,pnode
              do idime = 1,ndime
                 adofn = ( anode - 1 ) * ndofn_sld + idime + ndime
                 do bnode = 1,pnode
                    do kdime = 1,ndime
                       bdofn = ( bnode - 1 ) * ndofn_sld + kdime
                       do jdime = 1,ndime
                          do ldime = 1,ndime
                             elmat(adofn,bdofn) = elmat(adofn,bdofn) &
                                  + gptmo(idime,jdime,kdime,ldime,igaus) * volux &
                                  * gpcar(jdime,anode,igaus) * gpcar(ldime,bnode,igaus) &
                                  * gphea(anode,igaus)
                          end do
                       end do
                    end do
                 end do
              end do
           end do
           !
           ! the term corresponding to xfem: K_aa_iakb
           !
           do anode = 1,pnode
              do idime = 1,ndime
                 adofn = ( anode - 1 ) * ndofn_sld + idime + ndime
                 do bnode = 1,pnode
                    do kdime = 1,ndime
                       bdofn = ( bnode - 1 ) * ndofn_sld + kdime + ndime
                       do jdime = 1,ndime
                          do ldime = 1,ndime
                             elmat(adofn,bdofn) = elmat(adofn,bdofn) &
                                  + gptmo(idime,jdime,kdime,ldime,igaus) * volux &
                                  * gpcar(jdime,anode,igaus) * gpcar(ldime,bnode,igaus) &
                                  * gphea(anode,igaus) * gphea(bnode,igaus)
                          end do
                       end do
                    end do
                 end do
              end do
           end do
        end if

    end do  !do gauss

    !
    !**ECR: Addition of stiffness matrix related to lambda
    !  OJO!!! HAY QUE MIRAR LOS SUBINDICES DE LAS MATRICES Y DIMENSIONES!
    if (kfl_arcle_sld == 1) then
       !
       ! the last column (term ^f=dfint/du in articles notation): K_ulamb_ilambkb
       !
       do anode = 1,pnode
          do idime = 1,ndime
             adofn = ( anode - 1 ) * ndofn_sld + idime
             bdofn = pnode * ndofn_sld + 1
             elmat(adofn,bdofn) = elmat(adofn,bdofn) - fext(idime,anode)
          end do
       end do
       !
       ! the last row (term h=dg/du in articles notation): K_lambu_ilambkb
       !
       do bnode = 1,pnode
          do idime = 1,ndime
             bdofn = ( bnode - 1 ) * ndofn_sld + idime
             adofn = pnode * ndofn_sld + 1 
             elmat(adofn,bdofn) = elmat(adofn,bdofn) + 0.5_rp*lambda_sld(3)*fext(idime,bnode)
          end do
       end do
       !
       ! the term of the corner (w=dg/dlambda): K_lamlam_ilamkb
       !
       do anode = 1,pnode
          do idime = 1,ndime
             adofn = pnode*ndofn_sld + 1
             bdofn = pnode*ndofn_sld + 1
             elmat(adofn,bdofn) = elmat(adofn,bdofn) - 0.5_rp*fext(idime,anode)*eldis(idime,anode,2)
          end do
       end do
    end if !if (arc-length)


     if( debuggingMode ) then
        write(lun_livei,*)'elmat and elrhs'
        if (kfl_arcle_sld==1) then
           do idofn = 1,pnode*ndofn_sld+1
              write(lun_livei,'(100(x,e10.3))')(elmat(idofn,jdofn),jdofn = 1,pnode*ndofn_sld+1),elrhs(idofn)
           enddo
        else
           do idofn = 1,pnode*ndofn_sld
              write(lun_livei,'(100(x,e10.3))')(elmat(idofn,jdofn),jdofn = 1,pnode*ndofn_sld+1),elrhs(idofn)
           enddo
        end if
     end if

     if( kfl_probl_sld == 1_ip ) then       ! This is a dynamic problem (with inertial effect)
        elmas    = 0.0_rp
        elmas_ua = 0.0_rp
        elmas_au = 0.0_rp
        elmas_aa = 0.0_rp
        !
        ! M_ab = int_\Omega /rho * Na * Nb d\Omega
        !
        do igaus = 1,pgaus
           volux = gpvol(igaus)
           do anode = 1,pnode
              do bnode = 1,pnode
                 elmas(anode,bnode) = elmas(anode,bnode) &
                      + densi_sld(1,pmate) * volux       &
                      * gpsha(anode,igaus) * gpsha(bnode,igaus)
              end do
           end do

           !**DT: Addition of stiffness matrix related to unknown enrichments
           if (kfl_xfeme_sld == 1) then
              !
              ! the term corresponding to xfem: M_ua_ab, M_au_ab, M_aa_ab
              !
              do anode = 1,pnode
                 do bnode = 1,pnode
                    elmas_ua(anode,bnode) = elmas_ua(anode,bnode) &
                         + densi_sld(1,pmate)*volux &
                         *gpsha(anode,igaus)*gpsha(bnode,igaus) &
                         *gphea(bnode,igaus)
                    elmas_au(anode,bnode) = elmas_au(anode,bnode) &
                         + densi_sld(1,pmate)*volux &
                         *gpsha(anode,igaus)*gpsha(bnode,igaus) &
                         *gphea(anode,igaus)
                    elmas_aa(anode,bnode) = elmas_aa(anode,bnode) &
                         + densi_sld(1,pmate)*volux &
                         *gpsha(anode,igaus)*gpsha(bnode,igaus) &
                         *gphea(anode,igaus)*gphea(bnode,igaus)
                 end do
              end do
           end if

        end do

        if( debuggingMode ) then
           write(lun_livei,*)'elmas_sld'
           do idofn = 1,pnode
              write(lun_livei,'(100(x,e10.3))')(elmas(idofn,jdofn),jdofn = 1,pnode)
           enddo
           do idofn = 1,pnode
              write(lun_livei,'(100(x,e10.3))')(elmas_ua(idofn,jdofn),jdofn = 1,pnode)
           enddo
           do idofn = 1,pnode
              write(lun_livei,'(100(x,e10.3))')(elmas_au(idofn,jdofn),jdofn = 1,pnode)
           enddo
           do idofn = 1,pnode
              write(lun_livei,'(100(x,e10.3))')(elmas_aa(idofn,jdofn),jdofn = 1,pnode)
           enddo
        end if
        !
        ! inertial contribution to f_kb += M_ba * d2x/dt2_ak
        !
        !!!!if( kfl_exacs_sld == 0 ) then

           do idime = 1,ndime
              do inode = 1,pnode
                 idofn = (inode-1)*ndofn_sld + idime
                 do jnode = 1,pnode
                    ipoin = lnods(jnode)
                    elrhs(idofn) = elrhs(idofn) - elmas(inode,jnode)*&
                         (dtfa1*accel_sld(idime,ipoin,1) - &
                         dtfa2*accel_sld(idime,ipoin,3))
                    !**DT: Addition of inertial contribution to f_kb due to enrichment dof
                    if (kfl_xfeme_sld == 1) then
                       elrhs(idofn) = elrhs(idofn) - elmas_ua(inode,jnode)*&
                            (dtfa1*axfem_sld(idime,ipoin,1) - &
                            dtfa2*axfem_sld(idime,ipoin,3))
                    end if

                 end do
              end do
           end do

        !!!!end if
        !**DT: Inertial contribution of enrichment part of f_kb
        if (kfl_xfeme_sld == 1) then
           do idime = 1,ndime
              do inode = 1,pnode
                 idofn = (inode-1)*ndofn_sld + idime + ndime
                 do jnode = 1,pnode
                    ipoin = lnods(jnode)
                    elrhs(idofn) = elrhs(idofn) - elmas_au(inode,jnode)*&
                         (dtfa1*accel_sld(idime,ipoin,1) - &
                         dtfa2*accel_sld(idime,ipoin,3))

                    elrhs(idofn) = elrhs(idofn) - elmas_aa(inode,jnode)*&
                         (dtfa1*axfem_sld(idime,ipoin,1) - &
                         dtfa2*axfem_sld(idime,ipoin,3))
                 end do
              end do
           end do
        end if

        !
        ! inertial contribution to K_iakb += M_ba * delta_ik / (\beta * dt2)
        !
!print*,'a=',tifac_sld(1),dtime,betan
        do idime = 1,ndime
           do anode = 1,pnode
              adofn = ( anode - 1 )*ndofn_sld + idime
              do bnode = 1,pnode
                 bdofn = ( bnode - 1 ) *ndofn_sld + idime
!write(100,*) elmat(adofn,bdofn),elmas(anode,bnode)*betan
                 elmat(adofn,bdofn) = elmat(adofn,bdofn) + elmas(anode,bnode)*betan
              end do

              !**DT: Addition of inertial contribution to K_iakb due to enrichment dof
              if (kfl_xfeme_sld == 1) then
                 do bnode = 1,pnode
                    bdofn = ( bnode - 1 ) *ndofn_sld + idime + ndime
                    elmat(adofn,bdofn) = elmat(adofn,bdofn) + elmas_ua(anode,bnode)*betan
                 end do
              end if
           end do
        end do
!stop
        !**DT: Inertial contribution of enrichment part of K_iakb
        if (kfl_xfeme_sld == 1) then
           do idime = 1,ndime
              do anode = 1,pnode
                 adofn = ( anode - 1 )*ndofn_sld + idime + ndime
                 do bnode = 1,pnode
                    bdofn = ( bnode - 1 ) *ndofn_sld + idime
                    elmat(adofn,bdofn) = elmat(adofn,bdofn) + elmas_au(anode,bnode)*betan
                 end do
                 do bnode = 1,pnode
                    bdofn = ( bnode - 1 ) *ndofn_sld + idime + ndime
                    elmat(adofn,bdofn) = elmat(adofn,bdofn) + elmas_aa(anode,bnode)*betan
                 end do
              end do
           end do
        end if
     end if

     if( debuggingMode ) then
        write(lun_livei,*)'elmat and elrhs with inertial'
        do idofn = 1,pnode*ndofn_sld
           write(lun_livei,'(100(x,e10.3))')(elmat(idofn,jdofn),jdofn = 1,pnode*ndofn_sld),elrhs(idofn)
        enddo
     end if
     !
     ! Exact solution
     !
     if( kfl_exacs_sld /= 0 ) then
        call sld_elmexa(&
             1_ip,pgaus,pnode,elcod,gpsha,gpcar,gpdds,gpvol,&
             eldis,gpdis,gpgdi,elrhs)
     end if
     !
     ! Extension elements (Dodeme)
     !
     if( lelch(ielem) == ELEXT ) then
        call elmext(&
             4_ip,ndime,pnode,dummr,dummr,dummr,dummr,elmat,dummr,&
             elrhs,dummr)
     end if
     !
     ! Compute reaction force (lagged)
     ! 
     elfrx(1:ndofn_sld,1:pnode) = 0.0_rp
     do inode = 1,pnode
        ipoin = lnods(inode)
        do idime = 1,ndime
           if( kfl_fixno_sld(idime,ipoin) > 0 ) then
              idofn = (inode-1)*ndofn_sld + idime
              elfrx(idime,inode) = elrhs(idofn)
           end if
        end do
     end do
     !
     ! Boundary conditions: Dirichlet (essential B.Cs)
     !
     if( solve(1) % kfl_iffix == 0 ) then
        do inode = 1,pnode
           ipoin = lnods(inode)
           if (ipoin == 1) iprin=1
           do idime = 1,ndime
              if( kfl_fixno_sld(idime,ipoin) > 0 ) then
                 idofn = (inode-1)*ndofn_sld + idime
                 adiag = elmat(idofn,idofn)
                 do jnode = 1,pnode 
                    do jdime = 1,ndime
                       jdofn = (jnode-1)*ndofn_sld + jdime
                       elmat(idofn,jdofn) = 0.0_rp
                       elmat(jdofn,idofn) = 0.0_rp
                    end do
                 end do
                 elmat(idofn,idofn) = adiag
                 elrhs(idofn)       = 0.0_rp ! We work with increments
              end if
           end do
        end do
     end if
     !
     ! 'Boundary conditions' of enrichment dof:
     !  if node is enriched => leave the elmat row at it is
     !  if node is not enriched (standard node) => set off diagonal components of elmat and elrhs to zero
     !
     if( solve(1) % kfl_iffix == 0 .and. kfl_xfeme_sld == 1 ) then
        do inode = 1,pnode
           ipoin = lnods(inode)
           if ( lnenr_sld(ipoin) == 0 ) then
              do idime = 1,ndime
                 idofn = (inode-1)*ndofn_sld + idime + ndime
                 do jnode = 1,pnode
                    do jdime = 1,ndime
                       jdofn = (jnode-1)*ndofn_sld + jdime + ndime
                       elmat(idofn,jdofn) = 0.0_rp
                       elmat(jdofn,idofn) = 0.0_rp
                    end do
                 end do
                 elmat(idofn,idofn) = 1.0_rp
                 elrhs(idofn)       = 0.0_rp
              end do
           end if
        end do
     end if

     if( debuggingMode ) then
        write(lun_livei,*)'elmat and elrhs with boundary conditions'
        do idofn = 1,pnode*ndofn_sld
           write(lun_livei,'(100(x,e10.3))')(elmat(idofn,jdofn),jdofn = 1,pnode*ndofn_sld),elrhs(idofn)
        enddo
     end if

  end if

end subroutine sld_elmmat
