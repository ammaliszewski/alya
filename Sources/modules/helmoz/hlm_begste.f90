subroutine hlm_begste()
  !-----------------------------------------------------------------------
  !****f* Helmoz/hlm_begste
  ! NAME 
  !    hlm_begste
  ! DESCRIPTION
  !    This routine prepares for a new time step 
  ! USES
  ! USED BY
  !    Helmoz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_helmoz
  implicit none
  integer(ip), save :: ipass=0

end subroutine hlm_begste

