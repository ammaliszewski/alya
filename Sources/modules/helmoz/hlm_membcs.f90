subroutine hlm_membcs(itask)

  !-----------------------------------------------------------------------
  ! Sources/modules/helmoz/hlm_membcs.f90
  ! NAME
  !    hlm_membcs
  ! DESCRIPTION
  !    This routine allocates memory for boundary conditions.
  ! OUTPUT 
  ! USES
  ! USED BY
  !    hlm_inibcs
  !-----------------------------------------------------------------------

  use def_master
  use def_domain
  use def_helmoz
  !use mod_memchk
  use mod_memory

  implicit none

  integer(ip), intent(in) :: itask

  integer(ip)             :: ipoin
  integer(4)              :: istat

  select case (itask)
 	  case (1_ip)
 		  !Allocate memory for the codes for boundary values
!     	allocate(kfl_fixno_hlm(1,npoin),stat=istat)
!     	call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FIXNO_HLM','hlm_membcs',kfl_fixno_hlm)

        call memory_alloca(mem_modul(1:2,modul),'KFL_FIXNO_HLM','hlm_membcs',kfl_fixno_hlm,1_ip,npoin)

     	do ipoin = 1,npoin
   		  kfl_fixno_hlm(1,ipoin) = -1_ip
   		enddo
  	case (2_ip)

   	case (3_ip)
 		  !Deallocate memory
     	!call memchk(two,istat,mem_modul(1:2,modul),'KFL_FIXNO_HLM','hlm_membcs',kfl_fixno_hlm)
     	!deallocate(kfl_fixno_hlm,stat=istat)

        call memory_deallo(mem_modul(1:2,modul),'KFL_FIXNO_HLM','hlm_membcs',kfl_fixno_hlm)

     	!if (istat /= 0_ip) call memerr(two,'KFL_FIXNO_HLM','hlm_membcs',0_ip)
  end select

end subroutine hlm_membcs
