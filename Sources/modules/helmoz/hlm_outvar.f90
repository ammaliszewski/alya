subroutine hlm_outvar(ivari)

  !-----------------------------------------------------------------
  ! Sources/modules/helmoz/hlm_outvar.f90
  ! NAME 
  !    hlm_outvar
  ! DESCRIPTION
  !    This routine outputs postprocess variables.
  !    Also, master gets values of FE-computed potentials that 
  !    it needs for computation of field vectors. 
  ! USES
  !    postpr
  !    memgen
  !    hlm_postpr
  ! USED BY
  !    hlm_output
  !-----------------------------------------------------------------

  use def_parame
  use def_master
  use def_domain
  use def_helmoz
  use def_kermod

  implicit none

  integer(ip), intent(in) :: ivari

  !integer(ip)             :: iar3p,ibopo,ipoin
  integer(ip)             :: iascatmp,iavectmp,iar3ptmp
  integer(ip)             :: ibopo,ipoin,jttim
  real(rp)                :: dummr,rutim,normgradinv 
  character(5)            :: wopos(3)
  
  iascatmp = 0_ip
  iavectmp = 0_ip
  iar3ptmp = 0_ip
  !ibopo = 0_ip
  rutim = cutim

 
  !Define postprocess variables
  select case (ivari)  
    case(0_ip)
      return
    case(1_ip)     
      !kfl_cntrl = 1_ip
      !Secondary magnetic vector potential  
      if (INOTMASTER) then
        call memgen(6_ip,ndime,nsite_hlm*nmlsi_hlm)
        gevex => smgvp_hlm
      endif  

    case(2_ip)       
      !kfl_cntrl = 1_ip
      !Secondary electric scalar potential  
      if (INOTMASTER) then       
        call memgen(6_ip,nsite_hlm*nmlsi_hlm,0_ip)
        gescx => selsp_hlm
      endif

    case(5_ip)
      if(INOTMASTER) then
         call memgen(0_ip,npoin,0_ip)
         do ipoin=1,npoin
            diffj_hlm(ipoin)=diffj(lninv_loc(ipoin))
            !design_hlm(ipoin)=design_vars(lninv_loc(ipoin))
         end do
         gesca => diffj_hlm
      end if

    case(6_ip)
      if(INOTMASTER) then
         call memgen(0_ip,npoin,0_ip)
         do ipoin=1,npoin
            !diffj_hlm(ipoin)=diffj_scale(lninv_loc(ipoin))
            design_hlm(ipoin)=design_vars(lninv_loc(ipoin))
            !design_hlm(ipoin)=design_vars_tmp(lninv_loc(ipoin))
         end do
         gesca => design_hlm
      end if

    case(7_ip)
      normgradinv = 1.0_rp / sqrt(dot_product(diffj,diffj))
      diffj = diffj * normgradinv
      if(INOTMASTER) then
         do ipoin=1,npoin
            diffj(lninv_loc(ipoin)) = diffj(lninv_loc(ipoin)) * diffj_illum(lninv_loc(ipoin))
      !      !diffj_hlm(ipoin)=diffj_scale(lninv_loc(ipoin))
      !      !diffj_hlm(ipoin)=(diffj(lninv_loc(ipoin)) / diffj_illum(lninv_loc(ipoin))) * (1.0_rp/(exp(design_vars(lninv_loc(ipoin)))))
      !      !diffj_hlm(ipoin)=exp( 1.0e+10_rp * diffj(lninv_loc(ipoin)) / diffj_illum(lninv_loc(ipoin)) )
      !      !diffj_hlm(ipoin)=exp(-diffj(lninv_loc(ipoin))) * exp(design_vars(lninv_loc(ipoin)))
         end do
      end if
      if(INOTMASTER) then
         call memgen(0_ip,npoin,0_ip)
         do ipoin=1,npoin
            diffj_hlm(ipoin)=exp(-diffj(lninv_loc(ipoin))) * exp(design_vars(lninv_loc(ipoin)))
         end do

         gesca => diffj_hlm
      end if

!      if(INOTMASTER) then
!         do ipoin=1,npoin
!            diffj_hlm(ipoin)= diffj(lninv_loc(ipoin))
!         end do
!
!
!         gesca => diffj_hlm
!      end if

    case(8_ip)
      if(INOTMASTER) then
         call memgen(0_ip,npoin,0_ip)
         do ipoin=1,npoin
            diffj_hlm(ipoin)=descdir(lninv_loc(ipoin))
            !design_hlm(ipoin)=design_vars(lninv_loc(ipoin))
         end do

         gesca => diffj_hlm
      end if

  
  endselect
   
 
  !Output postprocess variables

  wopos(1) = postp(1) % wopos(1,ivari) 
  wopos(2) = postp(1) % wopos(2,ivari) 
  wopos(3) = postp(1) % wopos(3,ivari) 
  
  call outvar(&
       ivari,iascatmp,iavectmp,&
       ittim,rutim,wopos(:))
       !ittim,rutim,postp(1) % wopos(:,ivari))
        
  !call outvax(&
  !     ivari,ittim,rutim,wopos(:))
  !     !ivari,ittim,rutim,postp(1) % wopos(1,ivari))
 
  !Master gets values of FE-computed potentials that it needs for computation of field vectors
  !if (kfl_cntrl /= 0_ip .and. IMASTER) call hlm_postpr(ivari)

  !kfl_cntrl = 0_ip
  !cntrl_hlm = 1_ip
      
  !Deallocate postprocess variables
  select case (ivari)  
    case(0_ip)
      return
    case(1_ip)     
      !Secondary magnetic vector potential  
      if (INOTMASTER) then
        call memgen(7_ip,ndime,nsite_hlm*nmlsi_hlm)
      endif  

    case(2_ip)       
      !kfl_cntrl = 1_ip
      !Secondary electric scalar potential  
      if (INOTMASTER) then       
        call memgen(7_ip,nsite_hlm*nmlsi_hlm,0_ip)
      endif

    case(5_ip)       
      if (INOTMASTER) then       
        call memgen(2_ip,npoin,0_ip)
      endif 

    case(6_ip)       
      if (INOTMASTER) then       
        call memgen(2_ip,npoin,0_ip)
      endif 
 
    case(7_ip)       
      if (INOTMASTER) then       
        call memgen(2_ip,npoin,0_ip)
      endif 

    case(8_ip)       
      if (INOTMASTER) then       
        call memgen(2_ip,npoin,0_ip)
      endif 

  endselect
 
end subroutine hlm_outvar
