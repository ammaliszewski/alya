subroutine hlm_reaous()
  !------------------------------------------------------------------------
  ! Sources/modules/helmoz/hlm_reaous.f90
  ! NAME 
  !    hlm_reaous
  ! DESCRIPTION
  !    This routine reads the postprocess.
  ! USES
  ! USED BY
  !    hlm_turnon
  !------------------------------------------------------------------------

  use def_parame
  use def_inpout
  use def_master
  use def_helmoz
  use def_domain

  implicit none
  integer(ip) :: ii,dummi

  call reaous()

  !if(INOTSLAVE)then

  !   call ecoute('hlm_reaous')
  !   do while(words(1)/='OUTPU')
  !      call ecoute('hlm_reaous')
  !   end do
  !   !
  !   ! Begin to read data
  !   !
  !   do while(words(1)/='ENDOU')
  !      call ecoute('hlm_reaous')

  !      call posdef(2_ip,dummi)

  !   end do
  !end if

end subroutine hlm_reaous
 
