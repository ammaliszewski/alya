subroutine hlm_openfi(itask)
  !------------------------------------------------------------------------
  !****f* Temper/hlm_openfi
  ! NAME 
  !    hlm_openfi
  ! DESCRIPTION
  !    This subroutine opens and closes files
  ! USES
  ! USED BY
  !    hlm_turnon
  !***
  !------------------------------------------------------------------------
  use def_helmoz
  use def_master
  use def_domain
  use mod_iofile
  implicit none
  integer(ip), intent(in) :: itask 

end subroutine hlm_openfi

