subroutine hlm_endste()
  !-----------------------------------------------------------------------
  !****f* Helmoz/hlm_endste
  ! NAME 
  !    hlm_endste
  ! DESCRIPTION
  !    This routine ends a time step of the helmozature equation.
  ! USES
  !    hlm_cvgunk
  !    hlm_updunk
  !    hlm_output
  ! USED BY
  !    Helmoz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_helmoz
  implicit none

end subroutine hlm_endste
