subroutine hlm_inibcs()

  !-----------------------------------------------------------------------
  ! Sources/modules/helmoz/hlm_inibcs.f90
  ! NAME
  !    hlm_inibcs
  ! DESCRIPTION
  !    This routine applies boundary conditions.
  ! OUTPUT 
  ! USES
  !    hlm_membcs
  ! USED BY
  !    hlm_turnon
  !-----------------------------------------------------------------------

  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use def_helmoz
  use mod_opebcs

  implicit none

  integer(ip) :: ipoin

  if (INOTMASTER) then
    !Allocate memory
    call hlm_membcs(1_ip)
    !Node codes
    if (kfl_icodn > 0) then
      ifloc     =  0
      ifbop     =  0
      ifbes     =  0
      iffun     =  0
      kfl_fixno => kfl_fixno_hlm
      tncod     => tncod_hlm
      call reacod(10_ip)
    endif
    do ipoin = 1,npoin
      if (kfl_fixno_hlm(1,ipoin) == -1) kfl_fixno_hlm(1,ipoin) = 0
    enddo
  endif
end subroutine hlm_inibcs
