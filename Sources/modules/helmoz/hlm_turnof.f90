subroutine hlm_turnof()
  !-----------------------------------------------------------------------
  !****f* Helmoz/hlm_turnof
  ! NAME 
  !    hlm_turnof
  ! DESCRIPTION
  !    This routine closes the run for the helmozature equation
  ! USES
  !    hlm_outcpu
  !    hlm_output
  ! USED BY
  !    Helmoz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_solver
  use def_helmoz
  implicit none

end subroutine hlm_turnof

