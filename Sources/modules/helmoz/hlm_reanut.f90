subroutine hlm_reanut()

  !-----------------------------------------------------------------------
  ! Sources/modules/helmoz/hlm_reanut.f90
  ! NAME 
  !    hlm_reanut
  ! DESCRIPTION
  !    This routine reads the numerical problem for 'helmoz' module
  ! USES
  !    ecoute
  ! USED BY
  !    hlm_turnon
  !-----------------------------------------------------------------------

  use def_parame
  use def_inpout
  use def_master
  use def_solver
  use def_helmoz
  use def_domain

  implicit none

  if (INOTSLAVE) then
    !Initializations (defaults)
    solve_sol => solve       !Solver type
    solad_sol => solad
    !Reach the section
    call ecoute('hlm_reanut')
    do while (words(1) /= 'NUMER')
      call ecoute('hlm_reanut')
    enddo
    !Begin to read data
    do while (words(1) /= 'ENDNU')
      call ecoute('hlm_reanut')
      if (words(1) == 'ALGEB') then
        call reasol(1_ip)
      !else if (words(1) == 'PRECO') then 
      !  call reasol(2_ip)
      else if(words(1)=='ADJOI') then ! read Adjoint algebraic solver
        call reasol(3_ip)
        !call reaadj(1_ip)
      !else if(words(1)=='ADJPR') then ! read Adjoint preconditioner
      !  call reaadj(2_ip)
      endif
    enddo
  endif

end subroutine hlm_reanut
