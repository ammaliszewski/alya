subroutine hlm_timste()
  !-----------------------------------------------------------------------
  !****f* Helmoz/hlm_timste
  ! NAME 
  !    hlm_begste
  ! DESCRIPTION
  !    This routine prepares for a new time step
  ! USES
  ! USED BY
  !    Helmoz
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_helmoz
  implicit none

end subroutine hlm_timste

