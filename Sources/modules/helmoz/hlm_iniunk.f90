subroutine hlm_iniunk()
  !-----------------------------------------------------------------------
  !****f* Helmoz/hlm_iniunk
  ! NAME 
  !    hlm_iniunk
  ! DESCRIPTION
  !    This routine sets up the initial condition 
  ! USED BY
  !    hlm_begste
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_helmoz
  implicit none
  integer(ip) :: ipoin,icomp

end subroutine hlm_iniunk
