subroutine tur_elmmat(&
     pnode,plapl,pgaus,gpvel,gpdif,gprea,gptur,gprhs,&
     gpgrd,gpden,gpsta,gpadj,gpsha,gpcar,gphes,gpvol,&
     elmat,elrhs)
  !-----------------------------------------------------------------------
  !****f* Turbul/tur_elmmat
  ! NAME
  !   tur_elmmat
  ! DESCRIPTION
  !    Compute elemental matrix and RHS at Gauss point
  ! OUTPUT
  !    ELMAT ... LHS matrix for current Gauss point
  !    ELRHS ... RHS vector for current Gauss point
  ! USES
  ! USED BY
  !    tur_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,ntens,mnode
  use def_turbul, only       :  nturb_tur,iunkn_tur,kfl_timei_tur,&
       &                        kfl_weigh_tur,dtinv_tur,kfl_advec_tur
  implicit none
  integer(ip), intent(in)    :: pnode,plapl,pgaus
  real(rp),    intent(in)    :: gpvel(ndime,pgaus)
  real(rp),    intent(in)    :: gpdif(pgaus),gprea(pgaus)
  real(rp),    intent(in)    :: gptur(nturb_tur,3,pgaus)
  real(rp),    intent(in)    :: gprhs(pgaus),gpgrd(ndime,pgaus)
  real(rp),    intent(in)    :: gpden(pgaus),gpsta(pgaus)
  real(rp),    intent(inout) :: gpadj(pnode,pgaus)
  real(rp),    intent(in)    :: gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)    :: gphes(ntens,mnode,pgaus),gpvol(pgaus)
  real(rp),    intent(out)   :: elmat(pnode,pnode),elrhs(pnode)
  integer(ip)                :: inode,jnode,idime,igaus
  real(rp)                   :: gpadv(mnode),gpper(mnode)
  real(rp)                   :: fact1,fact2,fact3
  !
  ! Initialization
  !
  do inode=1,pnode
     elrhs(inode)=0.0_rp
     do jnode=1,pnode
        elmat(jnode,inode)=0.0_rp
     end do
  end do

  do igaus=1,pgaus
     !
     ! Pertubated test function: GPPER(j)=Nj-tau*L*(Nj)
     !
     do inode=1,pnode
        gpadj(inode,igaus) = -gpsta(igaus)*gpadj(inode,igaus)
        gpper(inode)       =  gpsha(inode,igaus)+gpadj(inode,igaus)
     end do
     !
     ! Advection: GPADV(j)=rho*a.grad(Nj)
     !
     do jnode=1,pnode
        gpadv(jnode)=0.0_rp
     end do
     if(kfl_advec_tur/=0) then
        do jnode=1,pnode
           do idime=1,ndime
              gpadv(jnode)=gpadv(jnode)+gpden(igaus)*gpvel(idime,igaus)&
                   *gpcar(idime,jnode,igaus)
           end do
        end do
     end if
     !
     ! Advection+reaction:  (rho*a.grad(Nj)+r*Nj)*[Ni-tau*L*(Ni)]
     ! Diffusion:           k*grad(Nj).grad(Ni)
     ! Perturbed diffusion: -[-tau*L*(Ni)]*[grad(k).grad(Nj)]
     !
     fact2=gpvol(igaus)*gpdif(igaus)
     do jnode=1,pnode
        fact1=gpvol(igaus)*(gpadv(jnode)+gprea(igaus)*gpsha(jnode,igaus))
        do inode=1,pnode
           fact3=gpvol(igaus)*gpadj(inode,igaus)
           elmat(inode,jnode)=elmat(inode,jnode)+fact1*gpper(inode)
           do idime=1,ndime
              elmat(inode,jnode)=elmat(inode,jnode)&
                   +gpcar(idime,jnode,igaus)&
                   *(fact2*gpcar(idime,inode,igaus)-fact3*gpgrd(idime,igaus))

           end do
        end do
     end do
     !
     ! Perturbed diffusion: -(-tau*L*(Ni)]*div(grad(Nj))
     !
     if(plapl==1) then
        fact1=gpvol(igaus)*gpdif(igaus)        
        do jnode=1,pnode
           fact2=0.0_rp
           do idime=1,ndime
              fact2=fact2+gphes(idime,jnode,igaus)
           end do
           fact2=fact2*fact1
           do inode=1,pnode
              elmat(inode,jnode)=elmat(inode,jnode)&
                   -gpadj(inode,igaus)*fact2
           end do
        end do
     end if
     !
     ! RHS: f*[Ni-tau*L*(Ni)]
     !
     fact1=gpvol(igaus)*gprhs(igaus)
     do inode=1,pnode
        elrhs(inode)=elrhs(inode)+fact1*gpper(inode)
     end do
     !
     ! Time integration: LHS: rho/(theta*dt)*Nj*[Ni-tau*L*(Ni)]
     !                   RHS: rho/(theta*dt)*u^{n-1}*[Ni-tau*L*(Ni)]
     !
     if(kfl_timei_tur/=0.and.iunkn_tur/=4) then
        fact1=gpden(igaus)*dtinv_tur*gpvol(igaus)
        if(kfl_weigh_tur==1) then
           do jnode=1,pnode
              do inode=1,pnode
                 elmat(inode,jnode)=elmat(inode,jnode)&
                      +fact1*gpsha(jnode,igaus)*gpper(inode)
              end do
              elrhs(jnode)=elrhs(jnode)+gptur(iunkn_tur,3,igaus)&
                   *fact1*gpper(jnode)
           end do
        else
           do jnode=1,pnode
              do inode=1,pnode
                 elmat(inode,jnode)=elmat(inode,jnode)&
                      +fact1*gpsha(jnode,igaus)*gpsha(inode,igaus)
              end do
              elrhs(jnode)=elrhs(jnode)+gptur(iunkn_tur,3,igaus)&
                   *fact1*gpsha(jnode,igaus)
           end do
        end if
     end if

  end do

end subroutine tur_elmmat

