subroutine tur_matrix()
  !------------------------------------------------------------------------
  !
  ! Compute elemental matrix and RHS
  !
  !------------------------------------------------------------------------
  use def_master
  use def_domain
  use def_turbul
  implicit none
  real(rp)    :: time1,time2
  !
  ! Initialization
  !
  call inisol()

  call cputim(time1)

  if( INOTMASTER ) then
     call tur_elmop2(1_ip)
     !call tur_elmope(1_ip)
  end if

  call cputim(time2) 
  cpu_modul(CPU_ASSEMBLY,modul) = cpu_modul(CPU_ASSEMBLY,modul) + time2 - time1

end subroutine tur_matrix
