!-----------------------------------------------------------------------
!> @addtogroup TurbulModel
!> @{
!> @file    tur_stdkep.f90
!> @author  Guillaume Houzeaux
!> @brief   Standard k-eps model
!> @details Compute coefficient of the equation of standard k-eps model           \n
!!                                                                                \n
!!    - \f$ \rho Dk/Dt = P - rho* \varepsilon1 
!!      + div[ (\mu+\mu_t/\sigma_k) \nabla k  ] \f$                               \n
!!                                                                                \n 
!!    - \f$ \rho D\varepsilon/Dt = C_{\varepsilon1} \varepsilon/k P
!!       - \rho C_{\varepsilon2} \varepsilon^2 / k 
!!       + div[ (\mu+\mu_t/\sigma_\varepsilon) \nabla \varepsilon  ]\f$           \n
!!                                                                                \n
!!    - \f$ \mu_t = \rho C_{\mu} k^2/e  \f$                                       \n
!!                                                                                \n
!!    For Non-Neutral atmospheric boundary layer:                                 \n
!!    - \f$ C_{\varepsilon 1} <= [ C_{\varepsilon 1} 
!!      + (C_{\varepsilon 2}-C_{\varepsilon 1}) l_m/l_{max} ]  \f$                \n
!!    - \f$ 1/l_m = \phi_m(z/L)/(k z) + 1/l_{max}   \f$                           \n
!!    - if  (\f$ z/L < 0  \f$)  then
!!       \f$ \phi_m = (1 - 16  z/L )^{-1/4} \f$
!!      else
!!       \f$ \phi_m = 1 + 4.7  z/L \f$
!!      endif
!!
!> @} 
!-----------------------------------------------------------------------
subroutine tur_stdkep(&
     pnode,gptur,gpden,gpvis,gpmut,gpgra,eledd,&
     gpcar,gppro,gppr2,gpwal,gprea,gpdif,gprhs,&
     gpgrd, kfl_kemod, eta, c_mu, gpcan ,sreac, &
     ldiss_material_tur)

  use def_kintyp, only     :  ip,rp 
  use def_domain, only     :  ndime
  use def_turbul, only     :  param_tur,nturb_tur,iunkn_tur, kfl_cotem_tur
  use def_turbul, only     :  kfl_algor_tur,ipara_tur, inv_l_max
  implicit none
  integer(ip), intent(in)  :: pnode                                !< Number of element nodes
  integer(ip), intent(in)  :: kfl_kemod                            !< Modification of ke model
  real(rp),    intent(in)  :: eledd(pnode)                         !< Element mut
  real(rp),    intent(in)  :: gptur(nturb_tur,2)                   !< Turb. varibales at Gauss points
  real(rp),    intent(in)  :: gpgra                                !< Gauss point Gravity production GPGRA=beta*mut/Prt*g.grad(T)
  real(rp),    intent(in)  :: gpden                                !< Gauss point density
  real(rp),    intent(in)  :: gpvis                                !< Gauss point viscosity
  real(rp),    intent(in)  :: gpmut(2)                                !< Gauss point mut
  real(rp),    intent(inout)  :: gppro                                !< Gauss point Shear production GPPRO = mu_t*(dui/dxj+duj/dxi)*dui/dxj
  real(rp),    intent(in)  :: gppr2                                !< Gauss point Shear production GPPRO =      (dui/dxj+duj/dxi)*dui/dxj
  real(rp),    intent(in)  :: gpwal                                !< Gauss point wall distance
  real(rp),    intent(in)  :: gpcar(ndime,pnode)                   !< Gauss point Shape function Cartesian derivatives
  real(rp),    intent(out) :: gpdif                                !< Gauss point diffusion: k
  real(rp),    intent(out) :: gpgrd(ndime)                         !< Gauss point diffusion gradient: grad(k)
  real(rp),    intent(out) :: gprea                                !< Gauss point reaction: r
  real(rp),    intent(in) ::  gpcan                                !< Gauss point canopy cd*LAD*|u|
  real(rp),    intent(out) :: sreac                                !< Gauss point canopy cd*LAD*|u|
  real(rp),    intent(inout) :: gprhs                                !< Gauss point RHS: f
  real(rp),    intent(in)  :: eta, c_mu                                !< Eta parameter for RNG model
  integer(ip), intent(in)  :: ldiss_material_tur
  real(rp)                 :: gpkin,gpeps,Ce1,Ce2,Cmu, DCe1, Ce3, C1p, Ce4
  real(rp)                 :: lm, facto, gpepo, gpkio, alpha, onerp, react, cmu0
  integer(ip)              :: index 
  !
  ! Definitions
  !
  gpkin = gptur(1,1)
  gpeps = gptur(2,1)
  gpepo = gptur(2,2) ! old eps
  gpkio = gptur(1,2) ! old k
  Ce1   = param_tur(3)
  Ce2   = param_tur(4)
  Ce3   = param_tur(5)
  Cmu   = param_tur(6) !constant cmu, from data files
  cmu0  = cmu ! for realizable
  DCe1 = 0.0_rp
  onerp = 1.0_rp
  if (kfl_kemod==2.or.kfl_kemod==3 ) cmu = c_mu ! gets modified cmu
  Ce4 = 0.37_rp

  !
  ! Maximum length scale limitation
  ! Ce1 <= [ Ce1 + (Ce2-Ce1) lm/lmax ]      \n
  ! 


  !
  ! Force and reaction GPRHS and GPREA
  !
  !if( iunkn_tur == 1 ) then 
  !   gprhs = gprhs + gppro + gpgra    
  !   if( gpmut/= 0.0_rp ) gprea = gpden*Cmu*gpkin/gpmut
  !else
  !   gprhs = gprhs + 1.44_rp*0.09_rp*gpden*gpkin*gppr2
  !   if( gpkin /= 0.0_rp ) gprea = 1.92_rp*gpden*gpeps/gpkin
  !end if

  if( iunkn_tur == 1 ) then

     gprea = max(2.0_rp*gpden*gpden*Cmu*gpkin/gpmut(2), 0.0_rp)
     sreac = max(       gpden*gpden*Cmu*gpkin/gpmut(2), 0.0_rp)
     if (gprea.lt.epsilon(1.0_rp)) onerp =0.0_rp
     gprhs = gprhs + max(gppro  +  gpgra,0.0_rp) + onerp*1.0_rp*gpden*gpeps 

  else ! EPSILON EQUATION 
     if (kfl_kemod==2) then  !Realizable model

        Ce1 = max(0.43_rp, eta/(eta+5.0_rp))
        gprea = 2.0_rp*Ce2 * gpden * gpeps / (gpkin + sqrt(gpvis*gpepo)) -   0.0_rp* gpden* sqrt(gppr2)*Ce1

        ! mixing length
        lm = (Cmu*gpkin*gpkin)**(0.75_rp)/gpepo    
        if (lm.lt.0.0_rp) lm=1.0/inv_l_max

        Ce1 = Ce1 + (Ce2*sqrt(Cmu0) - Ce1)*lm*inv_l_max         

        gprhs = gprhs    &
             +  1.0_rp*Ce2 * gpden * gpeps* gpeps / (gpkin+ sqrt(gpvis*gpepo))  &
        +  1.0_rp* gpden*gpeps* sqrt(gppr2)*Ce1

     else 
        ! mixing length
        lm = (Cmu*gpkin*gpkin)**(0.75_rp)/gpepo    
        if (lm.lt.0.0_rp) lm=1.0/inv_l_max
        ! l_max stores the inverse of the maximum (l_max=0 when no mixing model)
        DCe1  =  (Ce2-Ce1) * lm*inv_l_max    !  * min( lm*l_max, 1.0_rp)
        if (kfl_kemod==1) & ! RNG model
             DCe1  = - eta*(1.0_rp-eta/4.377_rp)/(1.0_rp+0.012_rp*eta*eta*eta)      

        C1p  = Ce1 + DCe1  ! C1 modifidied by Apsley and Castro

        react = max(Ce2 * gpden * gpeps / gpkin, 0.0_rp) ! reactive term (linearized)
        gprea = 2.0_rp *react + 12.0_rp*(C1p-Ce2)*sqrt(Cmu)*gpcan 
        sreac = react + 12.0_rp*(C1p-Ce2)*sqrt(Cmu)*gpcan ! reaction in perturbation test function
        
        if (kfl_cotem_tur==2) then ! Thermal coupling using SOGACHEV's
           if (gpgra.lt.0.0) then  ! stable
              alpha = 1.0_rp - lm*inv_l_max   
           else             
              alpha = 1.0_rp - ( 1.0_rp +(Ce2 -1.0_rp)/(Ce2-Ce1) )*lm*inv_l_max              
           end if
           Ce3 = 1.0_rp + (Ce1-Ce2)*alpha
        end if

        gprhs = gprhs +   max(C1p*Cmu*gpden*gpkin* gppr2 + Ce3*gpepo/gpkin*gpgra, 0.0_rp) + react*gpeps 

     end if

     ! Disc dissipation model (dissipation equation)
     if (ldiss_material_tur==1) & 
          gprhs = gprhs + max(Ce4*gppro*gppro/(gpden*gpkio), 0.0_rp)
     
  end if
  !
  ! GPDIF, GPGRD: diffusion and its gradient 
  !
  call tur_elmdif(pnode,gpvis,gpmut(2),gpcar,eledd,gpdif,gpgrd)

end subroutine tur_stdkep
