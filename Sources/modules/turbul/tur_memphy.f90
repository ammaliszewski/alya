subroutine tur_memphy
  !-----------------------------------------------------------------------
  !****f* Turbul/tur_memphy
  ! NAME 
  !    tur_memphy
  ! DESCRIPTION
  !    This routine allocates memory for physical arrays
  ! USES
  !    ecoute
  !    memchk
  !    runend
  ! USED BY
  !    tur_reaphy
  !    tur_sendat
  !***
  !-----------------------------------------------------------------------
  use def_parame 
  use def_inpout
  use def_master
  use def_turbul
  use def_domain
  use mod_memchk
  use mod_memory
  implicit none  
  !
  ! Material extra dissipation wake model 
  !
  call memory_alloca(mem_modul(1:2,modul),'LDISS_MATERIAL_TUR','tur_memall',ldiss_material_tur,nmate)

end subroutine tur_memphy
