subroutine tur_endste()
  !-----------------------------------------------------------------------
  !****f* Turbul/tur_endste
  ! NAME 
  !    tur_endste
  ! DESCRIPTION
  !    This routine ends a time step of the turbulence equations.
  ! USES
  !    tur_cvgunk
  !    tur_updunk
  !    tur_output
  ! USED BY
  !    Turbul
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_turbul
  implicit none
  !
  ! Compute convergence residual of the time evolution (that is,
  ! || u(n,*,*) - u(n-1,*,*)|| / ||u(n,*,*)||) and update unknowns
  ! u(n-1,*,*) <-- u(n,*,*) 
  !
  if(kfl_stead_tur==0.and.kfl_timei_tur==1) then
     call tur_cvgunk(three)
     call tur_updunk( five)
  end if
  !
  ! Compute averaged variables
  !
  call tur_averag()
  !
  ! Write restart file
  !
  call tur_restar(two)
  !
  ! If not steady, go on
  !
  if(kfl_stead_tur==0.and.kfl_timei_tur==1.and.kfl_conve(modul)==1) kfl_gotim = 1

end subroutine tur_endste
