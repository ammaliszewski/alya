subroutine tur_elmma2(&
     pnode,plapl,pgaus,pevat,gpvel,gpdif,gprea,gptur,&
     gprhs,gpgrd,gpden,gpsta,gpadj,gpsha,gpcar,gphes,&
     gpvol,elmat,elrhs)
  !-----------------------------------------------------------------------
  !****f* Turbul/tur_elmma2
  ! NAME
  !   tur_elmma2
  ! DESCRIPTION
  !    Compute elemental matrix and RHS at Gauss point
  ! OUTPUT
  !    ELMAT ... LHS matrix for current Gauss point
  !    ELRHS ... RHS vector for current Gauss point
  ! USES
  ! USED BY
  !    tur_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,ntens,mnode
  use def_turbul, only       :  nturb_tur,iunkn_tur,kfl_timei_tur,&
       &                        kfl_weigh_tur,dtinv_tur,kfl_advec_tur
  implicit none
  integer(ip), intent(in)    :: pnode,plapl,pgaus,pevat
  real(rp),    intent(in)    :: gpvel(ndime,pgaus)
  real(rp),    intent(in)    :: gpdif(2,pgaus)
  real(rp),    intent(in)    :: gprea(2,2,pgaus)
  real(rp),    intent(in)    :: gptur(nturb_tur,2,pgaus)
  real(rp),    intent(in)    :: gprhs(2,pgaus)
  real(rp),    intent(in)    :: gpgrd(2,ndime,pgaus)
  real(rp),    intent(in)    :: gpden(pgaus)
  real(rp),    intent(in)    :: gpsta(2,pgaus)
  real(rp),    intent(inout) :: gpadj(2,pnode,pgaus)
  real(rp),    intent(in)    :: gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)    :: gphes(ntens,mnode,pgaus),gpvol(pgaus)
  real(rp),    intent(out)   :: elmat(pevat,pevat),elrhs(pevat)
  integer(ip)                :: inode,jnode,idime,igaus
  integer(ip)                :: ieva1,ieva2,jeva1,jeva2
  real(rp)                   :: gpadv(mnode),gpper(2,mnode)
  real(rp)                   :: fact1(2),fact2(2),fact3(2),dummr
  !
  ! Initialization
  !
  do inode=1,pevat
     elrhs(inode)=0.0_rp
     do jnode=1,pevat
        elmat(jnode,inode)=0.0_rp
     end do
  end do

  do igaus=1,pgaus
     !
     ! Pertubated test function: GPPER(j)=Nj-tau*L*(Nj)
     !
     do inode=1,pnode
        gpadj(1,inode,igaus) = -gpsta(1,igaus)*gpadj(1,inode,igaus)
        gpadj(2,inode,igaus) = -gpsta(2,igaus)*gpadj(2,inode,igaus)
        gpper(1,inode)       =  gpsha(inode,igaus)+gpadj(1,inode,igaus)
        gpper(2,inode)       =  gpsha(inode,igaus)+gpadj(2,inode,igaus)
     end do
     !
     ! Advection: GPADV(J)=rho*a.grad(Nj)
     !
     do jnode=1,pnode
        gpadv(jnode)=0.0_rp
     end do
     if(kfl_advec_tur/=0) then
        do jnode=1,pnode
           do idime=1,ndime
              gpadv(jnode)=gpadv(jnode)+gpden(igaus)*gpvel(idime,igaus)&
                   *gpcar(idime,jnode,igaus)
           end do
        end do
     end if
     !
     ! Advection+reaction:  (rho*a.grad(Nj)+r*Nj)*[Ni-tau*L*(Ni)]
     ! Diffusion:           k*grad(Nj).grad(Ni)
     ! Perturbed diffusion: -(-tau*L*(Ni)]*[grad(k).grad(Nj)]
     !
     fact2(1)=gpvol(igaus)*gpdif(1,igaus)
     fact2(2)=gpvol(igaus)*gpdif(2,igaus)
     do jnode=1,pnode
        fact1(1)=gpvol(igaus)*(gpadv(jnode)+gprea(1,1,igaus)*gpsha(jnode,igaus))
        fact1(2)=gpvol(igaus)*(gpadv(jnode)+gprea(2,2,igaus)*gpsha(jnode,igaus))
        jeva1=2*jnode-1
        jeva2=2*jnode
        do inode=1,pnode
           fact3(1)=gpvol(igaus)*gpadj(1,inode,igaus)
           fact3(2)=gpvol(igaus)*gpadj(2,inode,igaus)
           ieva1=2*inode-1
           ieva2=2*inode
           elmat(ieva1,jeva1)=elmat(ieva1,jeva1)+fact1(1)*gpper(1,inode)
           elmat(ieva2,jeva2)=elmat(ieva2,jeva2)+fact1(2)*gpper(2,inode)
           do idime=1,ndime
              elmat(ieva1,jeva1)=elmat(ieva1,jeva1)&
                   +gpcar(idime,jnode,igaus)&
                   *(fact2(1)*gpcar(idime,inode,igaus)-fact3(1)*gpgrd(1,idime,igaus))
              elmat(ieva2,jeva2)=elmat(ieva2,jeva2)&
                   +gpcar(idime,jnode,igaus)&
                   *(fact2(2)*gpcar(idime,inode,igaus)-fact3(2)*gpgrd(2,idime,igaus))
           end do
         end do
     end do     
     dummr=gprea(1,2,igaus)*gpvol(igaus)
     do inode=1,pnode
        ieva1=2*inode-1
        fact1(1)=dummr*gpper(1,inode)
        do jnode=1,pnode
           jeva2=2*jnode
           elmat(ieva1,jeva2)=elmat(ieva1,jeva2)+fact1(1)*gpsha(jnode,igaus)
        end do
     end do
     dummr=gprea(2,1,igaus)*gpvol(igaus)
     do inode=1,pnode
        ieva1=2*inode
        fact1(1)=dummr*gpper(2,inode)
        do jnode=1,pnode
           jeva2=2*jnode-1
           elmat(ieva1,jeva2)=elmat(ieva1,jeva2)+fact1(1)*gpsha(jnode,igaus)
        end do
     end do
     !
     ! Perturbed diffusion: -(-tau*L*(Ni)]*div(grad(Nj))
     !
     if(plapl==1) then

        fact1(1)=gpvol(igaus)*gpdif(1,igaus)        
        fact1(2)=gpvol(igaus)*gpdif(2,igaus)        
        do jnode=1,pnode
           jeva1=2*jnode-1
           jeva2=2*jnode
           do inode=1,pnode
              ieva1=2*inode-1
              ieva2=2*inode
              dummr=sum(gphes(1:ndime,jnode,igaus))
              elmat(ieva1,jeva1)=elmat(ieva1,jeva1)&
                   -gpadj(1,inode,igaus)*dummr*fact1(1)
              elmat(ieva2,jeva2)=elmat(ieva2,jeva2)&
                   -gpadj(2,inode,igaus)*dummr*fact1(2)
           end do
        end do
     end if
     !
     ! RHS: f*[Ni-tau*L*(Ni)]
     !
     fact1(1)=gpvol(igaus)*gprhs(1,igaus)
     fact1(2)=gpvol(igaus)*gprhs(2,igaus)
     do inode=1,pnode
        ieva1=2*inode-1
        ieva2=2*inode
        elrhs(ieva1)=elrhs(ieva1)+fact1(1)*gpper(1,inode)
        elrhs(ieva2)=elrhs(ieva2)+fact1(2)*gpper(2,inode)
     end do
     !
     ! Time integration: LHS: rho/(theta*dt)*Nj*[Ni-tau*L*(Ni)]
     !                   RHS: rho/(theta*dt)*u^{n-1}*[Ni-tau*L*(Ni)]
     !
     if(kfl_timei_tur/=0) then
        fact1(1)=gpden(igaus)*dtinv_tur*gpvol(igaus)
        if(kfl_weigh_tur==1) then
           do jnode=1,pnode
              jeva1=2*jnode-1
              jeva2=2*jnode
              do inode=1,pnode
                 ieva1=2*inode-1
                 ieva2=2*inode
                 dummr=fact1(1)*gpsha(jnode,igaus)
                 elmat(ieva1,jeva1)=elmat(ieva1,jeva1)&
                      +dummr*gpper(1,inode)
                 elmat(ieva2,jeva2)=elmat(ieva2,jeva2)&
                      +dummr*gpper(2,inode)
              end do
              elrhs(jeva1)=elrhs(jeva1)+gptur(1,2,igaus)&
                   *fact1(1)*gpper(1,jnode)
              elrhs(jeva2)=elrhs(jeva2)+gptur(2,2,igaus)&
                   *fact1(1)*gpper(2,jnode)
           end do
        else
           do jnode=1,pnode
              jeva1=2*jnode-1
              jeva2=2*jnode
              do inode=1,pnode
                 ieva1=2*inode-1
                 ieva2=2*inode
                 dummr=fact1(1)*gpsha(jnode,igaus)*gpsha(inode,igaus)
                 elmat(ieva1,jeva1)=elmat(ieva1,jeva1)+dummr
                 elmat(ieva2,jeva2)=elmat(ieva2,jeva2)+dummr
              end do
              dummr=fact1(1)*gpsha(jnode,igaus)
              elrhs(jeva1)=elrhs(jeva1)&
                   +gptur(1,2,igaus)*dummr
              elrhs(jeva2)=elrhs(jeva2)&
                   +gptur(2,2,igaus)*dummr
           end do
        end if
     end if

  end do

end subroutine tur_elmma2
