subroutine tur_elmort(&
     gpres, gpcon, gprec,gprhs, ielem,pgaus,pnode, gpsha,gpvol)
  !------------------------------------------------------------------------
  !****f* Turbul/tur_elmort
  ! NAME 
  !    tur_elmort
  ! DESCRIPTION
  !    This subroutine assemble the projected residuals
  ! USES
  ! USED BY
  !    tur_elmop2
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  rhsid
  use def_domain, only       :  ndime,mnode,npoin,lnods
  use def_turbul, only       :  kfl_ortho_tur, tupr2_tur, tuprr_tur
  implicit none
  integer(ip), intent(in)    :: ielem,pgaus,pnode
  real(rp),    intent(in)    :: gpres(pgaus), gpcon(pgaus), gprec(pgaus), gprhs(pgaus)
  real(rp),    intent(in)    :: gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpvol(pgaus)
  real(rp)                   :: gptup(pgaus), gprep(pgaus), gprhp(pgaus)! projection of turbul
  real(rp)                   :: eltup(pnode), eltu2(pnode), eltu3(pnode)
  integer(ip)                :: idime,igaus,inode

  if( kfl_ortho_tur <= 0 ) then ! no orthogonal projection

     return

  else if( kfl_ortho_tur == 1 ) then ! FULL OSS

     !-------------------------------------------------------------------
     !
     ! GPTEP: Projection of turbulence residual
     !
     !-------------------------------------------------------------------

     do igaus =1, pgaus
        gptup(igaus) =  - gpres(igaus) * gpvol(igaus)
     end do   

  else if( kfl_ortho_tur == 2 ) then ! split oss

     !-------------------------------------------------------------------
     !
     ! GPTEP: Projection of rho*(a.grad)u
     !
     !-------------------------------------------------------------------

     do igaus =1, pgaus
        gptup(igaus) =  gpcon(igaus) * gpvol(igaus)
        gprep(igaus) =  gprec(igaus) * gpvol(igaus)
     end do

     do inode = 1,pnode
        eltu2(inode) = 0.0_rp
     end do
     do igaus = 1,pgaus
        do inode = 1,pnode
           eltu2(inode) = eltu2(inode) &
                + gprep(igaus) * gpsha(inode,igaus) 
        end do
     end do
     call assrhs(1_ip,pnode,lnods(1,ielem),eltu2,tuprr_tur)
  end if

  !----------------------------------------------------------------------
  !
  ! Assemble RHS
  !
  !----------------------------------------------------------------------

  do inode = 1,pnode
     eltup(inode) = 0.0_rp
  end do
  do igaus = 1,pgaus
     do inode = 1,pnode
        eltup(inode) = eltup(inode) &
             + gptup(igaus) * gpsha(inode,igaus) 
     end do
  end do

  call assrhs(1_ip,pnode,lnods(1,ielem),eltup,tupr2_tur)

end subroutine tur_elmort
