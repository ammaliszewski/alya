subroutine tur_iniunk()
  !-----------------------------------------------------------------------
  !****f* Turbul/tur_iniunk
  ! NAME 
  !    tur_iniunk
  ! DESCRIPTION
  !    This routine sets up the initial condition for the turbulence
  !    variables
  ! USED BY
  !    tur_begste
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_kermod
  use mod_ker_proper
  use def_domain
  use def_turbul
  use mod_chktyp,           only : check_type
  use mod_ker_space_time_function
  implicit none
  integer(ip)          :: ipoin,iturb,kfl_value,dummi,ifunc
  real(rp)             :: maxtu,rho(1),mu(1),nu,nup,nut
  real(rp),    target  :: turbm(nturb_tur)

  avtim_tur = 0.0_rp  ! Accumulated time for time-averaging variables
!  avkey_tur = 0.0_rp
!  avome_tur = 0.0_rp
!  avtvi_tur = 0.0_rp

  if( kfl_rstar == 0 ) then
        !
        ! TURMU: Initial turbulent viscosity
        !
        if( INOTMASTER ) then
           if ( kfl_prope /= 0_ip ) then
              call ker_proper('VISCO','NPOIN',dummi,dummi,turmu)
              if( nutnu_tur >= 0.0_rp ) then
                 do ipoin = 1,npoin 
                    turmu(ipoin) = turmu(ipoin) * nutnu_tur
                 end do
              end if
           else
              call tur_updpro()
              if( nutnu_tur >= 0.0_rp ) then
                 do ipoin = 1,npoin 
                    call tur_nodpro(ipoin,rho(1),mu(1))
                    turmu(ipoin) = nutnu_tur*mu(1)
                 end do
              else
                 do ipoin = 1,npoin 
                    call tur_nodpro(ipoin,rho(1),mu(1))
                    turmu(ipoin) = mu(1)
                 end do
              end if
           end if
        end if
        !
        ! Treat special cases
        !
        call tur_vortic()                            ! Vorticity magnitude
        call tur_adapti()                            ! Adaptive b.c. according to velocity
        call tur_frivel()                            ! Friction velocity
        call tur_bouave(1_ip)                        ! Average velocity
        call tur_updbcs(TUR_BEFORE_GLOBAL_ITERATION) ! Boundary conditions
        !
        ! Look for maximum values
        !    
        if( TUR_SPALART_ALLMARAS ) then

           if( INOTMASTER ) then 
              do ipoin = 1,npoin
                 if(  kfl_fixno_tur(1,ipoin,1) <= 0 .or.&
                      kfl_fixno_tur(1,ipoin,1) == 3 .or.&
                      kfl_fixno_tur(1,ipoin,1) == 4 ) then                 
                    if ( kfl_prope /= 0_ip ) then
                       call ker_proper('DENSI','IPOIN',ipoin,dummi,rho(1))
                       call ker_proper('VISCO','IPOIN',ipoin,dummi,mu(1))
                    else
                       call tur_nodpro(ipoin,rho(1),mu(1))
                    end if
                    nu  = mu(1)/rho(1) 
                    nut = turmu(ipoin)/rho(1)
                    call tur_spanut(1_ip,nu,nup,nut)
                    untur(1,ipoin,ncomp_tur) = nup
                 else
                    untur(1,ipoin,ncomp_tur) = bvess_tur(1,ipoin,1)
                 end if
              end do
           end if

        else

           if( INOTMASTER ) then
              turbm = 0.0_rp
              do ipoin = 1,npoin
                 do iturb = 1,nturb_tur
                    if( kfl_fixno_tur(1,ipoin,iturb) /= 3 .and. kfl_fixno_tur(1,ipoin,iturb) /=4 &
                         .and. bvess_tur(1,ipoin,iturb) > turbm(iturb) ) &
                         turbm(iturb) = bvess_tur(1,ipoin,iturb)
                 end do
              end do
           end if
           !
           ! Look for maximum over whole mesh
           !
           call pararr('MAX',0_ip,nturb_tur,turbm)
           !
           ! Initial values for turbulence variables
           !
           if( INOTMASTER ) then

              if( kfl_inifi_tur(1) > 0 ) then
                 !
                 ! initialization from fields
                 !
                 do iturb = 1,nturb_tur
                    do ipoin = 1,npoin
                       untur(iturb,ipoin,ncomp_tur) = xfiel(-nfiel_tur(iturb))%a(1,ipoin)
                    end do
                 end do

              else if( kfl_initi_tur == 2 ) then 
                 !
                 ! All from bvess 
                 !
                 do ipoin = 1,npoin
                    do iturb = 1,nturb_tur
                       untur(iturb,ipoin,ncomp_tur) = bvess_tur(1,ipoin,iturb)
                    end do
                 end do 
                  
              else if( kfl_initi_tur == -1 ) then
                 !
                 ! Value from a function
                 ! 
                 do iturb = 1,nturb_tur
                    kfl_value = kfl_valbc_tur(iturb)
                    call check_type(bvcod,kfl_value,1_ip,npoin) ! Check if value function exist
                    do ipoin = 1,npoin
                       untur(iturb,ipoin,ncomp_tur) = bvcod(kfl_value) % a(1,ipoin)
                    end do
                 end do
                  
              else if( kfl_initi_tur == 3 ) then 
                 ! 
                 ! Space time function
                 !
                do iturb = 1,nturb_tur
                   ifunc = int(xinit_tur(iturb),ip)
                   if( ifunc > 0 ) then
                      do ipoin = 1,npoin
                         call ker_space_time_function(&
                              ifunc,coord(1,ipoin),coord(2,ipoin),coord(ndime,ipoin),cutim, untur(iturb,ipoin,ncomp_tur))
                      end do
                   end if
                end do
                
              else 
                 !
                 ! Based on maximum values
                 !
                 do ipoin = 1,npoin
                    do iturb = 1,nturb_tur
                       if(  kfl_fixno_tur(1,ipoin,iturb) <= 0 .or.&
                            kfl_fixno_tur(1,ipoin,iturb) == 3 .or.&
                            kfl_fixno_tur(1,ipoin,iturb) == 4 ) then
                          untur(iturb,ipoin,ncomp_tur) = turbm(iturb)
                       else 
                          untur(iturb,ipoin,ncomp_tur) = bvess_tur(1,ipoin,iturb)
                       end if
                    end do
                 end do

              end if

           end if

        end if
        !
        ! When solving a manufactured solution, impose exact Dirichlet bc
        !
        !if( kfl_exacs_tur /= 0 ) then
        !   call tur_manufactured_Dirichlet_condition()
        !end if

        if( INOTMASTER ) then
           !
           ! Initialize turbulent viscosity
           !
           call tur_updunk(10_ip)
           call tur_updedd()    
           !
           ! If initial values are zero
           !
           maxtu = 0.0_rp
           do iturb = 1,nturb_tur
              if( turbm(iturb) > maxtu ) maxtu = turbm(iturb)
           end do
           if( maxtu == 0.0_rp ) then
              do ipoin = 1,npoin 
                 if ( kfl_prope /= 0_ip ) then
                    call ker_proper('DENSI','IPOIN',ipoin,dummi,rho(1))
                    call ker_proper('VISCO','IPOIN',ipoin,dummi,mu(1))
                 else
                    call tur_nodpro(ipoin,rho(1),mu(1))
                 end if
                 turmu(ipoin) = 10.0_rp*mu(1)
              end do
           end if

        end if

  else
     !
     ! Restart
     !
!     if( INOTMASTER ) call tur_memarr(1_ip)  esta linea lo jodia todo 
     call tur_restar(1_ip)
     avtim_tur = cutim
     call tur_updunk(6_ip)

  end if

  !-------------------------------------------------------------------
  !                                                 
  ! Interpolation from coarse to fine mesh      
  ! 
  !-------------------------------------------------------------------      

  if(kfl_meshi_tur /= 0_ip) call tur_coarfine(1_ip)


end subroutine tur_iniunk
