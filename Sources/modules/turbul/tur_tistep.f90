subroutine tur_tistep()
  !-----------------------------------------------------------------------
  !****f* Turbul/tur_tistep
  ! NAME 
  !    tur_tittim
  ! DESCRIPTION
  !    This routine sets the time step
  ! USES
  ! USED BY
  !    tur_begite
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_turbul
  implicit none

  if(kfl_timco/=2) then
     dtinv_tur=dtinv
     if(ittim<=neule_tur) then
        kfl_tiacc_tur=1
     else
        kfl_tiacc_tur=kfl_tiaor_tur
     end if
     if(kfl_stead_tur==1) dtinv_tur = 0.0_rp
     if(kfl_tiacc_tur==2) dtinv_tur = 2.0_rp*dtinv_tur
     if(kfl_timei_tur==0) dtinv_tur = 0.0_rp 
  end if

  routp(1)=dtcri_tur

  ! minimum time step when using local time step
  routp(2)=dtcri_tur*max(saflo_tur, safet_tur)
  !
  ! Look for maximum (time step) over subdomains
  !
  if(kfl_timco==2.and.kfl_paral>=0)  &                      
       call pararr('MAX',0_ip,1_ip,dtmax_tur)
  routp(3)=dtmax_tur
  ioutp(1)=kfl_timei_tur
  ioutp(2)=kfl_stead_tur
  call outfor(8_ip,lun_outpu,' ')

end subroutine tur_tistep
