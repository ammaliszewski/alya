subroutine tur_solsgs()
  !-----------------------------------------------------------------------
  !****f* Nastin/tur_solsgs
  ! NAME 
  !    tur_solsgs
  ! DESCRIPTION
  !    This routine solves the SGS equation
  ! USES
  ! USED BY
  !    tur_endite
  !***
  !-----------------------------------------------------------------------

  use def_parame
  use def_domain
  use def_master
  use def_turbul
  use mod_memory
  use mod_ADR,     only : PROJECTIONS_AND_SGS_ASSEMBLY ! 4
  use mod_ADR,     only : BUBBLE_ASSEMBLY              ! 5
  use mod_ADR,     only : ADR_initialize_projections
  use mod_ADR,     only : ADR_update_projections
  implicit none
  real(rp)    :: time1,time2

  call cputim(time1)
  if( INOTMASTER ) then

     if(  ADR_tur(iunkn_tur) % kfl_time_sgs      /= 0 .or. &
          ADR_tur(iunkn_tur) % kfl_nonlinear_sgs /= 0 .or. &
          ADR_tur(iunkn_tur) % kfl_stabilization  > 0 ) then

        !-------------------------------------------------------------------
        !
        ! Update subgrid scale and project residuals
        !
        !-------------------------------------------------------------------
        !
        ! Initialize
        !
        Call ADR_initialize_projections(ADR_tur(iunkn_tur))
        !
        ! Update SGS and projections
        !              
        call tur_elmope(PROJECTIONS_AND_SGS_ASSEMBLY)
        !
        ! Residual projections
        ! 
        call ADR_update_projections(ADR_tur(iunkn_tur),vmass)

     else if( ADR_tur(iunkn_tur) % kfl_time_bubble /= 0 ) then

        !-------------------------------------------------------------------
        !
        ! Update bubble
        !
        !-------------------------------------------------------------------

        call tur_elmope(BUBBLE_ASSEMBLY)

     end if

  end if
  call cputim(time2)  

end subroutine tur_solsgs

!!$  use def_parame
!!$  use def_domain
!!$  use def_master
!!$  use def_turbul
!!$  use mod_memory
!!$  implicit none
!!$  integer(ip) :: ipoin
!!$  real(rp)    :: time1,time2
!!$
!!$  if( kfl_ortho_tur >= 1 ) then
!!$     !
!!$     ! Initialization
!!$     !
!!$     call cputim(time1)
!!$     !
!!$     ! Residual projections
!!$     !
!!$     if( INOTMASTER ) then
!!$        call memory_alloca(mem_modul(1:2,modul),'TUPR2_TUR','tur_solsgs',tupr2_tur,npoin)         !
!!$        if ( kfl_ortho_tur==2) then ! Split oss
!!$           call memory_alloca(mem_modul(1:2,modul),'TUPRR_TUR','tur_solsgs',tuprr_tur,npoin)     
!!$        end if
!!$
!!$        ! Update SGS
!!$        ! 
!!$        call tur_elmop2(4_ip)
!!$        !
!!$        ! Residual projections
!!$        !
!!$        call rhsmod(1_ip,tupr2_tur)        
!!$        do ipoin = 1,npoin
!!$           unpro_tur(iunkn_tur,ipoin) = tupr2_tur(ipoin) / vmass(ipoin)
!!$        end do
!!$        call memory_deallo(mem_modul(1:2,modul),'TUPR2_TUR','tur_solsgs',tupr2_tur) 
!!$        
!!$        if( kfl_ortho_tur == 2 ) then ! Split oss
!!$           call rhsmod(1_ip,tuprr_tur)        
!!$           do ipoin = 1,npoin
!!$              unprr_tur(iunkn_tur,ipoin) = tuprr_tur(ipoin) / vmass(ipoin)
!!$           end do
!!$        call memory_deallo(mem_modul(1:2,modul),'TUPRR_TUR','tur_solsgs',tuprr_tur) 
!!$        end if
!!$     end if
!!$
!!$     call cputim(time2)
!!$     !cputi_tur(3) = cputi_tur(3) + time2 - time1
!!$
!!$  end if
!!$
!!$end subroutine tur_solsgs
