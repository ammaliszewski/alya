subroutine nsa_turnof
  !-----------------------------------------------------------------------
  !****f* Nastal/nsa_turnof
  ! NAME 
  !    nsa_turnof
  ! DESCRIPTION
  !    This routine closes the run for the current module
  ! USES
  !    nsa_outcpu
  !    nsa_output
  ! USED BY
  !    Nstinc
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_nastal
  implicit none

end subroutine nsa_turnof

