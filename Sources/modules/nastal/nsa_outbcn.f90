subroutine nsa_outbcn
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      mod_memchk
  
  use      def_nastal
  
  implicit none
  character(5)  :: chaux(mnodb)
  
  integer(ip)   :: idime,ielem,idofn,ipoin,iboun,inode,inodb,ncouf(nbhie_nsa),ibhie,kcoun,nnodb

  if (kfl_dumbo_nsa==0) return
  if (kfl_dumbo_nsa==1) then
     if (nboun==0) call runend('OUTBCN: NO NBOUN DEFINED')

!     write(6,*) 'uieee ',chhie_nsa(1:nbhie_nsa)

     write (lun_dumb1_nsa,*) '# CODES'
     do ibhie=1,nbhie_nsa
        write (lun_dumb1_nsa,*) '# ',ibhie,chhie_nsa(ibhie)(1:ndofn_nsa)
     end do
     write (lun_dumb1_nsa,*) '# END_CODES'
     write (lun_dumb1_nsa,'(a)') 'ON_BOUNDARIES, CODED'
     write (lun_dumb2_nsa,'(a)') 'BOUNDARIES'
     do iboun=1,nboun
        ncouf= 0
        chaux= ''
        nnodb=nnode(ltypb(iboun))
        do inodb=1,nnodb
           ipoin=lnodb(inodb,iboun)
           do idofn=1,ndofn_nsa
              chaux(inodb)(idofn:idofn)= intost(kfl_fixno_nsa(idofn,ipoin))
           end do
           do ibhie=1,nbhie_nsa
              if (chaux(inodb)(1:ndofn_nsa)==chhie_nsa(ibhie)(1:ndofn_nsa)) ncouf(ibhie)=ncouf(ibhie)+1
           end do
        end do
        kcoun=0
        do ibhie=1,nbhie_nsa
           if (ncouf(ibhie) > 0) then
              kcoun=ibhie
              exit
           end if
        end do
        
!             write(6,*) kcoun,ncouf(1:nbhie_nsa),chaux(1:nnodb)
        
        if (kcoun==0) then
           write (6,*) ' | '
           write (6,*) ' |------>   Missing fixity:  ',chaux(1:nnodb)
           write (6,*) ' | '
           call runend('OUTBCN: ALL FIXITIES MUST BE DECLARED IN THE HIERARCHY!')
        end if
        
        write (lun_dumb1_nsa,*) iboun,kcoun
        write (lun_dumb2_nsa,*) iboun,lnodb(1:nnodb,iboun),lboel(nnodb+1,iboun)

     end do
     
     write (lun_dumb1_nsa,'(a)') 'END_ON_BOUNDARIES'
     write (lun_dumb2_nsa,'(a)') 'END_BOUNDARIES'

  else if (kfl_dumbo_nsa==2) then

     call runend('OUTCON: BOUNDARY-TO-NODE NOT PROGRAMMED YET')

  end if


end subroutine nsa_outbcn
