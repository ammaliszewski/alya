!-----------------------------------------------------------------------
!> @addtogroup Nastal
!> @{
!> @file    nsa_output.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Output 
!> @details Output 
!> @} 
!-----------------------------------------------------------------------
subroutine nsa_output()
  use      def_parame
  use      def_master
  use      def_domain
  use      def_nastal
  use      mod_postpr

  implicit none
  integer(ip)             :: ivarp,ivari,ivort

  ivort = 0
  kfl_crens_nsa= 0
  do ivarp = 1,nvarp
     ivari = ivarp
     call posdef(11_ip,ivari)
     if( ivort == 0 .and. ( ivari == 4 .or. ivari == 29 .or. ivari == 31 .or. ivari == 32 ) ) then
        call vortic(-1_ip)
        ivort = 1
     end if
     call nsa_outvar(ivari)
  end do

  if( postp(1) % npp_stepi(55) /= 0 ) then
     if( mod(ittim,  postp(1) % npp_stepi(55) ) == 0 ) then ! AVVEL frequency
        avtim_nsa = cutim  ! Update reference time for time-averaging
     endif
  endif
 
  if( ivort == 1 ) call vortic(3_ip)

  if( ittyp == ITASK_INITIA .or. ittyp == ITASK_ENDTIM ) then
     !
     ! Calculations on sets
     !
     call nsa_outset()

     !
     ! Postprocess on witness points
     !
     call nsa_outwit()
  end if

  !...special stuff:     
  !
  !   if (ivari==36 .and. ndime==2) then
  !      if (kfl_pro2d_nsa == 1) call nsa_outpro(two)
  !      call nsa_outpro(three)
  !   end if

  if (kfl_chkpo_nsa(2) < 0) kfl_chkpo_nsa(2) = - kfl_chkpo_nsa(2) 

end subroutine nsa_output
