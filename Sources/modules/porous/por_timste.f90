!------------------------------------------------------------------------
!> @addtogroup Porous 
!> @{
!> @file    por_timste.f90
!> @date    13/05/2013
!> @author  Herbert Owen
!> @brief   Prepares for a new time step
!> @details Prepares for a new time step
!> @} 
!------------------------------------------------------------------------
subroutine por_timste()
  use def_parame
  use def_master
  use def_domain
  use def_porous
  implicit none
  !
  ! Time step size 
  !
!  call por_updtss() 

end subroutine por_timste

