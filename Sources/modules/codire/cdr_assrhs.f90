subroutine cdr_assrhs(ndofn,pnode,mnode,lnods,elrhs,rhsid)
!-----------------------------------------------------------------------
!
! Assembly of the RHS
!
!-----------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip),  intent(in) :: ndofn,pnode,mnode
  integer(ip),  intent(in) :: lnods(pnode)
  real(rp), intent(in)     :: elrhs(ndofn,mnode)
  real(rp), intent(inout)  :: rhsid(*)
  integer(ip)              :: idofn,inode,ipoin,ixcdr

  do inode=1,pnode
     ipoin=lnods(inode)
     do idofn=1,ndofn
        ixcdr=(ipoin-1)*ndofn+idofn
        rhsid(ixcdr)=rhsid(ixcdr)+elrhs(idofn,inode)
     end do
  end do

end subroutine cdr_assrhs
