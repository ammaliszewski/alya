subroutine cdr_lspics(kpite,glin0,glin1,dglin,glins,slins)
!-----------------------------------------------------------------------
!****f* Codire/cdr_lspics
! NAME 
!    cdr_lspics
! DESCRIPTION
!    This routine performs a line search and backtracking after a Picard
!    iteration. The target function is given by
!
!    g(s) = 0.5 * F( u_i + s d ) * F( u_i + s d )
!
!    The values are stored as:
!    glin0    = g(0)      flin0 = f(0)
!    glin1    = g(1)      flin1 = f(1)
!    glins(3) = g(s)
!    slins(3) = s
!
! USES
! USED BY
!    cdr_linsea
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  implicit none
  real(rp)   , intent(in)  :: glin0,glin1,dglin
  real(rp)   , intent(out) :: glins(10),slins(10)
  integer(ip), intent(out) :: kpite
  integer(ip)              :: iitls,kfoun

  kpite=0
!  slins(3) = 1.0_rp
!  write(lun_linse_cdr,105) itinn(modul),1.0,glin0,glin1,glin1
!  return

  if(glin1 <= glin0 ) then
     ! Accepted step
     slins(3) = 1.0_rp
     write(lun_linse_cdr,105) itinn(modul),1.0,glin0,glin1,glin1
  else

     slins(3) = 1.0_rp
     slins(4) = slins(3)
     glins(4) = glin1

     ! Find the minimum of g over some points
     kfoun=0
     iitls=0
     do while(iitls < mipls_cdr .and. kfoun==0)
        slins(3)=slins(3)/2.0_rp
        uncdr(:,:,1) = geten(:,:,1) + slins(3) * geten(:,:,2)
        if(kfl_modul(modul)==8) call cdr_updtpr(one)
        call cdr_lstarg
        glins(3)=0.5_rp*dot_product(rhsid,rhsid)

        if( glins(3)<=glin0 ) then
           ! Step found
           kfoun=1
        else
           ! Store the minimum
           if( glins(3)<glins(4) ) then
              glins(4)=glins(3)
              slins(4)=slins(3)
           end if
           iitls = iitls +1 
        end if
     end do

     if(kfoun==0) then
        slins(3)=slins(4)
        write(lun_linse_cdr,115) itinn(modul),slins(3),glin0,glin1,glins(3)
     else if(kfoun==1) then
        write(lun_linse_cdr,125) itinn(modul),slins(3),glin0,glin1,glins(3)
     end if

  end if


105 format(10x,i15,4x,4(e13.6,4x),'Accepted step')
115 format(10x,i15,4x,4(e13.6,4x),'Minimum value after Picard')
125 format(10x,i15,4x,4(e13.6,4x),'Step found')


end subroutine cdr_lspics
