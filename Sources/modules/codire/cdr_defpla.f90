subroutine cdr_defpla(ndofn,ndime,phypa,hleng,staco,masma, &
                      dires,cores,sores,dites,cotes,sotes, &
                      tauma,force)
!------------------------------------------------------------------------
!****f* Codire/cdr_defpla
! NAME 
!    cdr_defpla
! DESCRIPTION
!    This routine obtains the coefficients of the CDR equations for the
!    plate problem.      
! USES
! USED BY
!    cdr_defmat
!***
!-----------------------------------------------------------------------
  use def_parame
!  USE NUMERICAL_LIBRARIES
  implicit none

  integer(ip), intent(in)  :: ndofn,ndime
  real(rp),    intent(in)  :: phypa(20),staco(10),hleng(2)
  real(rp),    intent(out) :: masma(ndofn,ndofn)
  real(rp),    intent(out) :: dires(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(out) :: cores(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: sores(ndofn,ndofn)
  real(rp),    intent(out) :: dites(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(out) :: cotes(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: sotes(ndofn,ndofn)
  real(rp),    intent(out) :: tauma(ndofn,ndofn)
  real(rp),    intent(out) :: force(ndofn)

  integer(ip), save        :: ipass
  real(rp)                 :: t,E,nu,k1,k2,k,eps,hdist,tauc,tau1,tau2

!  real(rp)                 :: diffu,react,advec
!  real(rp)                 :: tminv(ndofn,ndofn),xsots(ndofn,ndofn)
!  real(rp)                 :: xiden(ndofn,ndofn),xata1(ndofn,ndofn)
!  real(rp)                 :: xata2(ndofn,ndofn)
!  real(rp)                 :: hstar,xiadi,detit
!  integer(ip)              :: idime,jdime,idofn,jdofn
!  integer(ip)              :: itier,niter


  data ipass/0/

  if(ipass==0) then
     ipass = 1

     ! Coefficients of the temporal derivative.
     masma(1,1) = 0.0_rp
     masma(2,2) = 0.0_rp

     ! Matrix coefficients.
     t   = phypa(1)
     E   = phypa(2)
     nu  = phypa(3)
     k   = phypa(4)
     k1  = E/(24.0_rp*(1.0_rp + nu))
     k2  = E/(24.0_rp*(1.0_rp - nu))
     eps = 2.0_rp*(1.0_rp + nu)*t*t/(E*k)
     dires(1,1,1,1) = k1 + k2
     dires(2,2,1,1) = k1
     dires(3,3,1,1) = 1.0_rp/eps
     dires(1,1,2,2) = k1
     dires(2,2,2,2) = k1 + k2
     dires(3,3,2,2) = 1.0_rp/eps
     dires(1,2,1,2) = 0.5_rp*k2
     dires(2,1,1,2) = 0.5_rp*k2
     dires(1,2,2,1) = 0.5_rp*k2
     dires(2,1,2,1) = 0.5_rp*k2
     cores(1,3,1)   = -1.0_rp/eps
     cores(3,1,1)   =  1.0_rp/eps
     cores(2,3,2)   = -1.0_rp/eps
     cores(3,2,2)   =  1.0_rp/eps
     sores(1,1) = 1.0_rp/eps
     sores(2,2) = 1.0_rp/eps
     force(3)   = phypa(5)

     dites = dires   ! Test functions
     cotes = cores
     sotes = sores

  else

!
! Stabilization parameters.  
!
!     if(staco(1)>=zero_rp) then

        t   = phypa(1)
        E   = phypa(2)
        nu  = phypa(3)
        k   = phypa(4)
        k1  = E/(24.0_rp*(1.0_rp + nu))
        k2  = E/(24.0_rp*(1.0_rp - nu))
        eps = 2.0_rp*(1.0_rp + nu)*t*t/(E*k)
        hdist=hleng(1)

        tau1 = staco(1)*(k1+k2)/(hdist*hdist)
        tau2 = staco(3)/eps
        tauc = tau1 + tau2
        if(tauc.gt.zero_rp) tauc = 1.0_rp/tauc
        tauma(1,1) = tauc
        tauma(2,2) = tauc
!        tauc = hdist*hdist*hdist*hdist/(staco(1)*staco(1))
!        tauc = tauc*(staco(1)*eps/(hdist*hdist) + 1.0_rp/(k1+k2))
!        tauma(3,3) = tauc

!     else

!        xiden=0.0_rp
!        do idofn=1,ndofn
!           xiden(idofn,idofn)=1.0_rp
!        end do

!        xiadi=1.0e-4_rp
!        ! Find max advec max react and min diffu
!        diffu = 1.0e10_rp
!        advec = 0.0_rp
!        react = 0.0_rp
!        do idofn = 1,ndofn
!           do jdofn = 1,ndofn
!              react = max( react,abs(sores(idofn,jdofn)) )
!              do idime = 1,ndime
!                 advec = max(advec,                   &
!                   abs(cores(idofn,jdofn,idime)) )
!                 do jdime = 1,ndime
!                    if(dires(idofn,jdofn,idime,idime) >= zero_rp)  &
!                    diffu = min(diffu,                               &
!                      dires(idofn,jdofn,idime,idime))
!                 end do
!              end do
!           end do
!        end do

!        ! Find the number of iterations
!        hstar=min(2.0_rp*diffu*xiadi/advec,dsqrt(2.0_rp*diffu*xiadi/react))
!        niter=int(dlog(hleng(1)/hstar) / dlog(2.0_rp)) + 1
!        hstar=hleng(1)/(2.0_rp**niter)

!        ! Iteration
!        tauma=0.0_rp
!        do itcou=1,niter
!
!           xsots = xiden-matmul(sores,tauma)
!           xsots = hstar*hstar*xsots
!           xata1 = matmul(tauma,cores(:,:,1))
!           xata1 = matmul(cores(:,:,1),xata1)
!           xata2 = matmul(tauma,cores(:,:,2))
!           xata2 = matmul(cores(:,:,2),xata2)

!           tminv = dires(:,:,1,1)    +  &
!                   dires(:,:,2,2)    +  &
!                   xata1 + xata2  +  &
!                   matmul(xsots,sores)

!           !CALL DLINRG (ndofn, tminv, ndofn, tauma, ndofn)
!           call invmtx(tminv,tauma,detit,ndofn)

!           tauma = matmul(tauma,xsots)
!           hstar=2.0_rp*hstar

!        end do

!     end if


  end if


end subroutine cdr_defpla
