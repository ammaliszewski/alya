subroutine cdr_defgen(ndofn,ndime,staco,hleng,masma,dires,  &
                      cores,sores,tauma)
!-----------------------------------------------------------------------
!
! This routine obtains the coefficients of the CDR equations for the
! Navier-Stokes case, including the matrix of stabilization parameters.
!
!-----------------------------------------------------------------------
  use def_parame
!  USE NUMERICAL_LIBRARIES
  implicit none
  integer(ip), intent(in)  :: ndofn,ndime
  real(rp),    intent(in)  :: staco(10)
  real(rp),    intent(in)  :: hleng(2)
  real(rp),    intent(in)  :: masma(ndofn,ndofn)
  real(rp),    intent(in)  :: dires(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(in)  :: cores(ndofn,ndofn,ndime)
  real(rp),    intent(in)  :: sores(ndofn,ndofn)
  real(rp),    intent(out) :: tauma(ndofn,ndofn)
  integer(ip)              :: idofn,jdofn,idime
  real(rp)                 :: diffu,react,advec,hdisi,tauc1,tauc2
!  real(rp)                 :: tminv(ndofn,ndofn),xsots(ndofn,ndofn)
!  real(rp)                 :: xiden(ndofn,ndofn),xata1(ndofn,ndofn)
!  real(rp)                 :: xata2(ndofn,ndofn)
!  real(rp)                 :: hstar,xiadi,detit
!  integer(ip)              :: jdime
!  integer(ip)              :: itcou,niter
!
!  if(staco_cdr(1)>=zero_rp) then
!
! System treated equation by equation.
!
!     tauc2=1.0e10_rp
     hdisi = 1.0_rp/hleng(1)                   ! Min element length
     do idofn = 1,ndofn
        diffu = 0.0_rp
        advec = 0.0_rp
        do idime = 1,ndime
          diffu = max(diffu,                   &
            dires(idofn,idofn,idime,idime))
          advec = advec                        &
            + cores(idofn,idofn,idime)     &
            * cores(idofn,idofn,idime)
        end do
        advec = sqrt(advec)
        react = abs(sores(idofn,idofn))
        tauc1 = diffu*staco(1)*hdisi*hdisi &
          +     advec*staco(2)*hdisi       &
          +     react*staco(3)
!        tauc2 = min(tauc2,tauc1)
        if(tauc1.gt.zero_rp)                     &
          tauma(idofn,idofn) = 1.0_rp/tauc1
     end do

!  else
!
! Recursion tau
!
!     xiden=0.0_rp
!     do idofn=1,ndofn
!        xiden(idofn,idofn)=1.0_rp
!     end do
!
!     xiadi=1.0e-4_rp
        ! Find max advec max react and min diffu
!        diffu = 1.0e10_rp
!        advec = 0.0_rp
!        react = 0.0_rp
!        do idofn = 1,ndofn
!           do jdofn = 1,ndofn
!              react = max( react,abs(sores(idofn,jdofn)) )
!              do idime = 1,ndime
!                 advec = max(advec,                   &
!                   abs(cores(idofn,jdofn,idime)) )
!                 do jdime = 1,ndime
!                    if(dires(idofn,jdofn,idime,idime) >= zero_rp)  &
!                    diffu = min(diffu,                               &
!                      dires(idofn,jdofn,idime,idime))
!                 end do
!              end do
!           end do
!        end do
!
!     ! Find the number of iterations
!     hstar=min(2.0_rp*diffu*xiadi/advec,dsqrt(2.0_rp*diffu*xiadi/react))
!     niter=int(dlog(hleng(1)/hstar) / dlog(2.0_rp)) + 1
!     hstar=hleng(1)/(2.0_rp**niter)
!
!     ! Iteration
!     tauma_cdr=0.0_rp
!     do itcou=1,niter
!
!        xsots = xiden-matmul(sores,tauma)
!        xsots = hstar*hstar*xsots
!        xata1 = matmul(tauma,cores(:,:,1))
!        xata1 = matmul(cores(:,:,1),xata1)
!        xata2 = matmul(tauma,cores(:,:,2))
!        xata2 = matmul(cores(:,:,2),xata2)
!
!        tminv = dires(:,:,1,1)    +  &
!                dires(:,:,2,2)    +  &
!                xata1 + xata2  +  &
!                matmul(xsots,sores)
!
!        ! CALL DLINRG (ndofn, tminv, ndofn, tauma, ndofn)
!        call invmtx(tminv,tauma,detit,ndofn)
!
!        tauma_cdr = matmul(tauma,xsots)
!        hstar=2.0_rp*hstar
!
!     end do
!
!  end if

end subroutine cdr_defgen

