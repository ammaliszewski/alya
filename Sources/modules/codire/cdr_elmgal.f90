subroutine cdr_elmgal(ndime,nnode,mnode,ntens,ndofn,    &
                      ncomp,llapl,kfl_timei_cdr,theta,  &
                      dires_cdr,cores_cdr,flres_cdr,    &
                      sores_cdr,force_cdr,masma_cdr,    &
                      elunk,olunk,dtinv,shape,cartd,    &
                      zero_rp,wmatr,wrhsi)
!------------------------------------------------------------------------
!****f* Codire/cdr_elmgal
! NAME 
!    cdr_elmgal
! DESCRIPTION
!    This routine computes the Galerkin terms of the CDR equation

! USED BY
!    cdr_elmope
!***
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none

  integer(ip), intent(in)    :: ndime,nnode,mnode,ntens,ndofn,ncomp,llapl
  integer(ip), intent(in)    :: kfl_timei_cdr
  real(rp),    intent(in)    :: theta(3)
  real(rp),    intent(in)    :: dires_cdr(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(in)    :: cores_cdr(ndofn,ndofn,ndime)
  real(rp),    intent(in)    :: flres_cdr(ndofn,ndofn,ndime)
  real(rp),    intent(in)    :: sores_cdr(ndofn,ndofn)
  real(rp),    intent(in)    :: force_cdr(ndofn)
  real(rp),    intent(in)    :: masma_cdr(ndofn,ndofn)
  real(rp),    intent(in)    :: elunk(ndofn,mnode,ncomp)
  real(rp),    intent(out)   :: olunk(ndofn,ncomp-2)
  real(rp),    intent(in)    :: dtinv
  real(rp),    intent(in)    :: shape(nnode)
  real(rp),    intent(in)    :: cartd(ndime,nnode)
  real(rp),    intent(in)    :: zero_rp
  real(rp),    intent(out)   :: wmatr(ndofn,nnode,ndofn,nnode)
  real(rp),    intent(out)   :: wrhsi(ndofn,nnode)

  integer(ip)                :: inode,jnode,idofn,jdofn,idime,jdime,itens,icomp
  real(rp)                   :: wpro1,wpro2

  ! Contribution from the advection term
  do jnode=1,nnode
     do jdofn=1,ndofn
        do idime=1,ndime
           do inode=1,nnode
              wpro1=shape(inode)*cartd(idime,jnode)
              wpro2=cartd(idime,inode)*shape(jnode)
              do idofn=1,ndofn
                 wmatr(idofn,inode,jdofn,jnode) = wmatr(idofn,inode,jdofn,jnode)         &
                   +  wpro1*(cores_cdr(idofn,jdofn,idime)-flres_cdr(idofn,jdofn,idime))  &
                   -  wpro2*flres_cdr(idofn,jdofn,idime)
              end do
           end do
        end do
     end do
  end do

  ! Contribution from the diffusive term
  do jnode=1,nnode
     do jdime=1,ndime
        do idime=1,ndime
           do jdofn=1,ndofn
              do inode=1,nnode
                 wpro1=cartd(idime,inode)*cartd(jdime,jnode)
                 do idofn=1,ndofn
                    wmatr(idofn,inode,jdofn,jnode) = wmatr(idofn,inode,jdofn,jnode) &
                      +   wpro1*dires_cdr(idofn,jdofn,idime,jdime)
                 end do
              end do
           end do
        end do
     end do
  end do

  ! Contribution from the reaction term
  do jnode=1,nnode
     do jdofn=1,ndofn
        do inode=1,nnode
           wpro1=shape(inode)*shape(jnode)
           do idofn=1,ndofn
              wmatr(idofn,inode,jdofn,jnode) = wmatr(idofn,inode,jdofn,jnode)       &
                +   wpro1*sores_cdr(idofn,jdofn)
           end do
        end do
     end do
  end do

  ! Contribution from the temporal derivative        
  if(kfl_timei_cdr==1) then

     olunk=0.0_rp
     do icomp=3,ncomp
        do inode=1,nnode
           do idofn=1,ndofn
              olunk(idofn,icomp-2) = olunk(idofn,icomp-2)                            &
                                   + elunk(idofn,inode,icomp-1)*shape(inode)
           end do
        end do
     end do

     do jdofn=1,ndofn
        wpro1 = ( theta(2)*olunk(jdofn,1)+theta(3)*olunk(jdofn,ncomp-2) )*dtinv
        do inode=1,nnode
           do idofn=1,ndofn
              wrhsi(idofn,inode) = wrhsi(idofn,inode)                                 &
                - wpro1*shape(inode)*masma_cdr(idofn,jdofn)
           end do
        end do
     end do

     wpro1=theta(1)*dtinv
     do jnode=1,nnode
        do jdofn=1,ndofn
           do inode=1,nnode
              wpro2=wpro1*shape(inode)*shape(jnode)
              do idofn=1,ndofn
                 wmatr(idofn,inode,jdofn,jnode) = wmatr(idofn,inode,jdofn,jnode)    &
                   + wpro2*masma_cdr(idofn,jdofn)
              end do
           end do
        end do
     end do

  end if

  ! Contribution from the force term
  do inode=1,nnode
     do idofn=1,ndofn
        wrhsi(idofn,inode) = wrhsi(idofn,inode) + shape(inode)*force_cdr(idofn)
     end do
  end do

end subroutine cdr_elmgal
