subroutine cdr_outsec
!-----------------------------------------------------------------------
!
! This routine writes sections for the CDR problem.
!
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  use mod_iofile
  implicit none
  integer(ip)     :: ipoin,idofn
  integer(ip), save   :: ipass
  data                   ipass/0/

      if(ndime==2) then
        if(ipass==0) then
          call iofile(zero,lun_sectx_cdr,fil_sectx_cdr,'CDR SECTION X')
          call iofile(zero,lun_secty_cdr,fil_secty_cdr,'CDR SECTION Y')
          rewind(lun_sectx_cdr)
          rewind(lun_secty_cdr)
          ipass = 1
          write(lun_sectx_cdr,1) csect_cdr(1)
          write(lun_secty_cdr,2) csect_cdr(2)
        end if

        write(lun_sectx_cdr,10) cutim
        write(lun_secty_cdr,10) cutim
        
        do ipoin = 1,npoin
          if(abs(coord(1,ipoin)-csect_cdr(1))<=zero_rp) then
            write(lun_sectx_cdr,'(10(2x,e15.8))') &
                  coord(2,ipoin), &
                 (uncdr(idofn,ipoin,1),idofn=1,ndofn_cdr)
          end if  
          if(abs(coord(2,ipoin)-csect_cdr(2))<=zero_rp) then
            write(lun_secty_cdr,'(10(2x,e15.8))') &
                  coord(1,ipoin), &
                 (uncdr(idofn,ipoin,1),idofn=1,ndofn_cdr)
          end if
        end do
        call flush(lun_sectx_cdr)
        call flush(lun_secty_cdr)
          
      else if(ndime==3) then
        if(ipass.eq.0) then
          call iofile(zero,lun_sectx_cdr,fil_sectx_cdr,'CDR SECTION X')
          call iofile(zero,lun_secty_cdr,fil_secty_cdr,'CDR SECTION Y')
          call iofile(zero,lun_sectz_cdr,fil_sectz_cdr,'CDR SECTION Z')
          rewind(lun_sectx_cdr)
          rewind(lun_secty_cdr)
          rewind(lun_sectz_cdr)
          ipass = 1
          write(lun_sectx_cdr,3) csect_cdr(1)
          write(lun_secty_cdr,4) csect_cdr(2)
          write(lun_sectz_cdr,5) csect_cdr(3)
        end if
        
        write(lun_sectx_cdr,10) cutim
        write(lun_secty_cdr,10) cutim
        write(lun_sectz_cdr,10) cutim       
        do ipoin = 1,npoin
          if(abs(coord(1,ipoin)-csect_cdr(1))<=zero_rp) then
            write(lun_sectx_cdr,'(10(2x,f15.5))') &
                  coord(2,ipoin),coord(3,ipoin),  &
                 (uncdr(idofn,ipoin,1),idofn=1,ndofn_cdr)
          end if
          if(abs(coord(2,ipoin)-csect_cdr(2))<=zero_rp) then
            write(lun_secty_cdr,'(10(2x,f15.5))') &
                  coord(1,ipoin),coord(3,ipoin),  &
                 (uncdr(idofn,ipoin,1),idofn=1,ndofn_cdr)
          end if
          if(abs(coord(3,ipoin)-csect_cdr(3))<=zero_rp) then
            write(lun_sectz_cdr,'(10(2x,f15.5))') &
                  coord(1,ipoin),coord(2,ipoin),  &
                 (uncdr(idofn,ipoin,1),idofn=1,ndofn_cdr)
          end if
        end do
        call flush(lun_sectx_cdr)
        call flush(lun_secty_cdr)
        call flush(lun_sectz_cdr)
      end if
      
    1 format(1x,'Results at section X = ',e12.6,/,  &
             1x,'    Y       cdr1   cdr2  ... ',/,  &
             1x,'-----------------------------')
    2 format(1x,'Results at section Y = ',e12.6,/,  &
             1x,'    X       cdr1   cdr2  ...',/,   &
             1x,'------------------- --------') 
    3 format(1x,'Results at section X = ',e12.6,/,  &
             1x,' Y    Z     cdr1   cdr2  ...',/,   &
             1x,'----------------------------')
    4 format(1x,'Results at section Y = ',e12.6,/,  &
             1x,' X    Z     cdr1   cdr2  ...',/,   &
             1x,'----------------------------')
    5 format(1x,'Results at section Z = ',e12.6,/,  &
             1x,' X    Y     cdr1   cdr2  ...',/,   &
             1x,'----------------------------') 
   10 format(1x,'Current time :',e12.6) 

      return

end subroutine cdr_outsec
