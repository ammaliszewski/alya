subroutine cdr_forces
!-----------------------------------------------------------------------
!****f* Codire/cdr_forces
! NAME 
!    cdr_forces
! DESCRIPTION
!    This routine computes forces and moments at a given time step.
! USES
! USED BY
!    cdr_output
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  use def_cdrloc
  use mod_iofile
  use mod_memchk
  implicit none

  real(rp)   , allocatable :: force(:,:),momen(:,:)
  real(rp)   , allocatable :: fluxe(:,:,:)
  integer(ip), allocatable :: lpbou(:,:)

  real(rp)    :: cartb(ndime,mnode)
  real(rp)    :: shapp(mnode)
  real(rp)    :: unkgp(ndofn_cdr)
  real(rp)    :: tragp(ndofn_cdr,mgaub)

  integer(ip) :: ielem,inode,ipoin,igaus,idime,jdime      ! Indices and dimensions
  integer(ip) :: pnode,pgaus,iboun,igaub,inodb
  integer(ip) :: pelty,pblty,pnodb,pgaub
  integer(ip) :: idofn,jdofn,ibody
  integer(4)  :: istat

  integer  lenfi
  integer  ipass
  data     ipass/0/
  save     ipass,lenfi
!
! Inicializations.
!
  if(ipass==0) then
     call iofile(zero,lun_force_cdr,fil_force_cdr,'CDR FORCES & MOMENTS')
     if (ndime==2.and.kfl_modul(modul)==2) write (lun_force_cdr,12)
     if (ndime==3.and.kfl_modul(modul)==2) write (lun_force_cdr,13)
     if (ndime==2.and.kfl_modul(modul)==7) write (lun_force_cdr,14)
     if (ndime==3.and.kfl_modul(modul)==7) write (lun_force_cdr,15)
     if(kfl_outfl_cdr==1) then
        lenfi=len_trim(fil_fluxe_cdr)
        bod: do ibody = 1,nbody_cdr
           if(ibody<10) then
              write(fil_fluxe_cdr(lenfi+1:lenfi+2),50) '.',ibody
           else if(ibody<100) then
              write(fil_fluxe_cdr(lenfi+1:lenfi+3),51) '.',ibody
           else if(ibody<1000) then
              write(fil_fluxe_cdr(lenfi+1:lenfi+4),52) '.',ibody
           else
              write(lun_outpu_cdr,60)
              exit bod
           end if
           call iofile(zero,lun_fluxe_cdr,fil_fluxe_cdr,'CDR FLUXES')
           write(lun_fluxe_cdr,60)
           call iofile(two,lun_fluxe_cdr,fil_fluxe_cdr,'CDR FLUXES')
        end do bod
     end if
     ipass = 1
  end if
!
! Allocate volatile memory.
!
  call cdr_memelm(one)
  call cdr_membou(one)
  hleng=1.0_rp

  allocate(force(ndofn_cdr,nbody_cdr),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'FORCE','cdr_force',force)
  allocate(momen(3,nbody_cdr),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'MOMEN','cdr_force',momen)

  if(kfl_outfl_cdr==1) then
     allocate(fluxe(ndofn_cdr,npoin,nbody_cdr),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'FLUXE','cdr_force',fluxe)
     allocate(lpbou(npoin,nbody_cdr),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LPBOU','cdr_force',lpbou)
  end if
!
! Computations.
!
  boundaries: do iboun=1,nboun

     pblty=ltypb(iboun) 
     pnodb=nnode(pblty)
     ielem=lboel(pnodb+1,iboun)
     pelty=ltype(ielem)
     pnode=nnode(pelty)
     ibody=lbody_cdr(iboun)

     do inode=1,pnode
        ipoin=lnods(inode,ielem)
        elunk(:,inode,1) = uncdr(:,ipoin,1)
        elcod(:,inode)   = coord(1:ndime,ipoin)
     end do
     
     if(ibody>0) then

        do inodb=1,pnodb                                   ! Gather operations
           ipoin=lnodb(inodb,iboun)
           bounk(:,inodb) = uncdr(:,ipoin,1)
           bocod(:,inodb) = coord(:,ipoin)
        end do
        
        tragp=0.0_rp
        gauss: do igaub=1,ngaus(pblty)
        
           ! Jacobian
           call bouder(&
                pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&
                bocod,baloc,eucta)
           dsurf=elmar(pblty)%weigp(igaub)*eucta 
           call chenor(pnode,baloc,bocod,elcod)                          ! Check normal
        
           do inode=1,pnode                                              ! Calculate carte-
              do idime=1,ndime                                           ! sian derivates
                 cartb(idime,inode)=0.0_rp                               ! at boundary 
                 do inodb=1,pnodb                                        ! gauss points
                    do igaus=1,ngaus(pelty)
                       call elmder(&
                            pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&  ! Cartesian derivative
                            elcod,cartd,detjm,xjacm,xjaci)               ! and Jacobian
                       cartb(idime,inode) = cartb(idime,inode)&
                                          + elmar(pelty)%shaga(igaus,lboel(inodb,iboun))&
                                          * elmar(pblty)%shape(inodb,igaub)*cartd(idime,inode)
                    end do
                 end do
              end do
           end do
           shapp=0.0_rp                                                    ! Shape at boundary
           do inodb=1,pnodb                                                ! gauss points
              shapp(lboel(inodb,iboun))=elmar(pblty)%shape(inodb,igaub)
           end do
        
           ! Evaluate matrices
           call cdr_defmat(kfl_modul(modul),kfl_syseq_cdr,kfl_fdiff_cdr,   &
                           lawvi_cdr,ncomp_cdr,ndofn_cdr,pnode,mnode,   &
                           ndime,phypa_cdr,visco_cdr,thpre_cdr,         &
                           welin_cdr,staco_cdr,hleng,shapp,             &
                           cartb,elcod,elunk,masma_cdr,dires_cdr,       &
                           cores_cdr,flres_cdr,sores_cdr,dites_cdr,     &
                           cotes_cdr,sotes_cdr,tauma_cdr,force_cdr,grunk)
        
           ! unkno en gp
           do jdofn=1,ndofn_cdr
              unkgp(jdofn)=dot_product(elmar(pblty)%shape(1:pnodb,igaub),bounk(jdofn,1:pnodb))
           end do
        
           ! traction at gp
           do idofn=1,ndofn_cdr
              do jdofn=1,ndofn_cdr
                 do idime=1,ndime
                    tragp(idofn,igaub) = tragp(idofn,igaub) &
                                 + baloc(idime,ndime) * flres_cdr(idofn,jdofn,idime) * unkgp(jdofn)
                    do jdime=1,ndime
                       tragp(idofn,igaub) = tragp(idofn,igaub) &
                                    + baloc(idime,ndime) * dires_cdr(idofn,jdofn,idime,jdime) * grunk(jdime,jdofn)
                    end do
                 end do
              end do
           end do
        
           do idofn=1,ndofn_cdr
              force(idofn,ibody) = force(idofn,ibody) + dsurf * tragp(idofn,igaub)
           end do
        
           if(ndime==2) then
               momen(3,ibody) = momen(3,ibody) &
                              + force(2,ibody)*(dot_product(shapp(1:pnode),elcod(1,1:pnode))-origm_cdr(1)) &
                              - force(1,ibody)*(dot_product(shapp(1:pnode),elcod(2,1:pnode))-origm_cdr(2)) 
           else if(ndime==3) then
               momen(1,ibody) = momen(1,ibody) &
                              + force(3,ibody)*(dot_product(shapp(1:pnode),elcod(2,1:pnode))-origm_cdr(2)) &
                              - force(2,ibody)*(dot_product(shapp(1:pnode),elcod(3,1:pnode))-origm_cdr(3)) 
               momen(2,ibody) = momen(2,ibody) &
                              + force(1,ibody)*(dot_product(shapp(1:pnode),elcod(3,1:pnode))-origm_cdr(3)) &
                              - force(3,ibody)*(dot_product(shapp(1:pnode),elcod(1,1:pnode))-origm_cdr(1)) 
               momen(3,ibody) = momen(3,ibody) &
                              + force(2,ibody)*(dot_product(shapp(1:pnode),elcod(1,1:pnode))-origm_cdr(1)) &
                              - force(1,ibody)*(dot_product(shapp(1:pnode),elcod(2,1:pnode))-origm_cdr(2)) 
           end if


        end do gauss

        if(kfl_outfl_cdr==1) then
           do inodb=1,pnodb
              ipoin=lnodb(inodb,iboun)
              lpbou(ipoin,ibody)=lpbou(ipoin,ibody)+1
              do idofn=1,ndofn_cdr
                 do igaub=1,ngaus(pblty)
                 fluxe(idofn,ipoin,ibody) = fluxe(idofn,ipoin,ibody) &
                                          + elmar(pblty)%shaga(igaub,inodb)*tragp(idofn,igaub)
                 end do
              end do
           end do
        end if

     end if

  end do boundaries
!
! Output forces.
!
  if(ndime.eq.2) then
     do ibody = 1,nbody_cdr
        write(lun_force_cdr,20) cutim,      &
           -force(1,ibody)/adimf_cdr(1),    &
           -force(2,ibody)/adimf_cdr(2),    &
           -momen(3,ibody)/adimm_cdr(3),    &
            (force(idofn,ibody)/adimf_cdr(idofn),idofn=3,ndofn_cdr)
     end do
  else if(ndime.eq.3) then
     do ibody = 1,nbody_cdr
        write(lun_force_cdr,30) cutim,      &
           -force(1,ibody)/adimf_cdr(1),    &
           -force(2,ibody)/adimf_cdr(2),    &
           -force(3,ibody)/adimf_cdr(3),    &
           -momen(1,ibody)/adimm_cdr(1),    &
           -momen(2,ibody)/adimm_cdr(2),    &
           -momen(3,ibody)/adimm_cdr(3),    &
            (force(idofn,ibody)/adimf_cdr(idofn),idofn=3,ndofn_cdr)
     end do
  end if
  call flush(lun_force_cdr)
!
! Output fluxes.
!
  if(kfl_outfl_cdr==1) then
     bodies: do ibody = 1,nbody_cdr
        if(ibody<10) then
           write(fil_fluxe_cdr(lenfi+1:lenfi+2),50) '.',ibody
        else if(ibody<100) then
           write(fil_fluxe_cdr(lenfi+1:lenfi+3),51) '.',ibody
        else if(ibody<1000) then
           write(fil_fluxe_cdr(lenfi+1:lenfi+4),52) '.',ibody
        else
           write(lun_outpu_cdr,53)
           exit bodies
        end if
        call iofile(zero,lun_fluxe_cdr,fil_fluxe_cdr,'CDR FLUXES','old')
        do ipoin=1,npoin
           if(lpbou(ipoin,ibody)>0) then
              write(lun_fluxe_cdr,61) cutim,      &
                 ipoin,(coord(idime,ipoin),idime=1,ndime),  &
                 (fluxe(idofn,ipoin,ibody)/lpbou(ipoin,ibody)/adimf_cdr(idofn),idofn=1,ndofn_cdr)
           end if
        end do
        call iofile(two,lun_fluxe_cdr,fil_fluxe_cdr,'CDR FLUXES')
     end do bodies
  end if
!
! Deallocate volatile memory.
!
  call memchk(two,istat,mem_modul(1:2,modul),'FORCE','cdr_force',force)
  deallocate(force,stat=istat)
  if(istat.ne.0)  call memerr(two,'FORCE','cdr_force',0_ip)
  call memchk(two,istat,mem_modul(1:2,modul),'MOMEN','cdr_force',momen)
  deallocate(momen,stat=istat)
  if(istat.ne.0)  call memerr(two,'MOMEN','cdr_force',0_ip)

  if(kfl_outfl_cdr==1) then
     call memchk(two,istat,mem_modul(1:2,modul),'FLUXE','cdr_force',fluxe)
     deallocate(fluxe,stat=istat)
     if(istat.ne.0)  call memerr(two,'FLUXE','cdr_force',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LPBOU','cdr_force',lpbou)
     deallocate(lpbou,stat=istat)
     if(istat.ne.0)  call memerr(two,'LPBOU','cdr_force',0_ip)
  end if

  call cdr_memelm(two)
  call cdr_membou(two)

  return
!
! Formats.
!
   12   format( &
          '$        Forces and Moments over bodies          ',/,          &
          '$    Time',15x,'Fx',15x,'Fy',15x,'Mz     (repeat for each body) ')
   13   format( &
          '$          Forces and Moments over bodies            ',/,      &
          '$ Time  Fx  Fy  Fz  Mx  My  Mz (repeat for each body)')
   14   format( &
          '$        Forces and Moments over bodies          ',/,          &
          '$    Time',15x,'Fx',15x,'Fy',15x,'Mz',30x,'Q     (repeat for each body) ')
   15   format( &
          '$          Forces and Moments over bodies            ',/,      &
          '$ Time  Fx  Fy  Fz  Mx  My  Mz  Q(repeat for each body)')

   20   format(1x,e12.6,2x,5(2x,e15.8))
   30   format(1x,e12.6,2x,6(2x,e15.8))

   50   format(a1,i1)
   51   format(a1,i2)
   52   format(a1,i3)
   53   format(5x,'WARNING: CANNOT WRITE MORE THAN 999 bodies')

   60   format( &
          '$        Fluxes over bodies          ',/,  &
          '$ Time  point   x    y    Fluxes-->  ')

   61   format(1x,e12.6,2x,i10,2x,10(2x,e15.8))

end subroutine cdr_forces

