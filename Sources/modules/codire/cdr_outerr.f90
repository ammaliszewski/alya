subroutine cdr_outerr
!------------------------------------------------------------------------
!****f* Codire/cdr_outerr
! NAME 
!    cdr_outerr
! DESCRIPTION
!    This routine checks if there are errros and warnings
! USES
! USED BY
!    cdr_turnon
!***
!------------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      def_codire
  implicit none
  integer(ip) :: ierro=0,iwarn=0
!
! Check the transient evolution
!
  if(kfl_timei/=0) then
     if(kfl_timei_cdr == 0) then
        write(lun_outpu_cdr,101) &
              'STEADY CDR EQUATIONS IN A TRANSIENT CALCULATION'
     end if
  end if
!
! Local time step is not available for CDR
!
  if(kfl_timco==2) then
     ierro=ierro+1
     write(lun_outpu_cdr,100)&
          'LOCAL TIME STEP NOT AVAILABLE FOR CDR'
     write(lun_outpu,100)&
          'LOCAL TIME STEP NOT AVAILABLE FOR CDR'
  end if
!
! Stop
!
  if(ierro==1) then
     call runend(adjustl(trim(intost(ierro)))//' ERROR HAS BEEN FOUND')
  else if(ierro>=2) then
     call runend(adjustl(trim(intost(ierro)))//' ERRORS HAVE BEEN FOUND')
  end if
!
! Formats
!
100 format(5x,'ERROR:   ',a)
101 format(5x,'WARNING: ',a)

end subroutine cdr_outerr
