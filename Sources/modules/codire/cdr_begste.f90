subroutine cdr_begste
!-----------------------------------------------------------------------
!****f* Codire/cdr_begste
! NAME 
!    cdr_begste
! DESCRIPTION
!    This routine prepares for a new time step of the CDR equations.
! USES
!    cdr_iniunk
!    cdr_updtss
!    cdr_updbcs
!    cdr_updunk
!    cdr_radvuf
! USED BY
!    Codire
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master
  use def_codire
  implicit none
!
! Update boundary conditions
!
  call cdr_updbcs(one)
!
! Initial guess for the unknown: U(n,0,*) <-- U(n-1,*,*).
!
  call cdr_updunk(one)
!
! Low Mach number flows 
!
  if(ittim==1.and.kfl_modul(modul)==8) call cdr_initpr
!
! Write title in the log and solver files.
!
  call outfor(7_ip,lun_solve_cdr,' ')
  if(kfl_modul(modul)==8) &
     call outfor(7_ip,lun_thpre_cdr,' ')
  if(kfl_linse_cdr > zero) &
       call outfor(7_ip,lun_linse_cdr,' ')

end subroutine cdr_begste

      
