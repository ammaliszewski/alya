subroutine cdr_defmat(kprob,ksyse,kfdif,lawvi,ncomp,ndofn,  &
                      nnode,mnode,ndime,phypa,visco,thpre,  &
                      welin,staco,hleng,shape,cartd,elcod,  &
                      elunk,masma,dires,cores,flres,sores,  &
                      dites,cotes,sotes,tauma,force,grunk)
!------------------------------------------------------------------------
!****f* Codire/cdr_defmat
! NAME 
!    cdr_defmat
! DESCRIPTION
!    This routine obtains the coefficients of the CDR equations due
!    to the coupling with other problems and nonlinearities. It also 
!    computes the TAU matrix for the CDR equations.
! USES
! USED BY
!    cdr_elmope
!***
!-----------------------------------------------------------------------
  use def_parame
  implicit none
  integer(ip), intent(in)  :: kprob,kfdif,ksyse,lawvi,ncomp,ndofn,nnode,mnode,ndime
  real(rp),    intent(in)  :: phypa(20),visco(10),thpre(2),welin(20),staco(10)
  real(rp),    intent(in)  :: hleng(2)
  real(rp),    intent(in)  :: shape(nnode)
  real(rp),    intent(in)  :: cartd(ndime,mnode)
  real(rp),    intent(in)  :: elcod(ndime,mnode)
  real(rp),    intent(in)  :: elunk(ndofn,mnode,ncomp)
  real(rp),    intent(out) :: masma(ndofn,ndofn)
  real(rp),    intent(out) :: dires(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(out) :: cores(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: flres(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: sores(ndofn,ndofn)
  real(rp),    intent(out) :: dites(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(out) :: cotes(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: sotes(ndofn,ndofn)
  real(rp),    intent(out) :: tauma(ndofn,ndofn)
  real(rp),    intent(out) :: force(ndofn)
  real(rp)                 :: grunk(ndime,ndofn)

  select case(kprob)
!
! General case. Default TAU matrix.
!
  case (one)
     call cdr_defgen(ndofn,ndime,staco,hleng,masma,dires,  &
                     cores,sores,tauma)
!
! Navier-Stokes equations as a general system of CDR equations.
!
  case(two)
     call cdr_defnst(kfdif,lawvi,ncomp,ndofn,nnode,mnode,  &
                     ndime,phypa,visco,welin,staco,hleng,  &
                     shape,cartd,elcod,elunk,masma,dires,  &
                     cores,flres,sores,dites,cotes,sotes,  &
                     tauma,force,grunk)
!
! Plate problem.
!
  case(three)
     call cdr_defpla(ndofn,ndime,phypa,hleng,staco,masma, &
                     dires,cores,sores,dites,cotes,sotes, &
                     tauma,force)
!
! Helmholtz problem.
!
  case(four)
!      call cdr_defhel
!
! Contaminant transport problem.
!
  case(five)
!     call cdr_defcta
!
! Concentration problem.
!
  case(six)
!     call cdr_defcon
!
! Boussinesq equations as a general system of CDR equations.
!
  case(seven)
     call cdr_defbou(kfdif,lawvi,ncomp,ndofn,nnode,mnode,  &
                     ndime,phypa,visco,welin,staco,hleng,  &
                     shape,cartd,elcod,elunk,masma,dires,  &
                     cores,flres,sores,dites,cotes,sotes,  &
                     tauma,force,grunk)
!
! Low Mach number equations as a general system of CDR equations.
!
  case(eight)
     call cdr_deflom(kfdif,ksyse,lawvi,ncomp,ndofn,nnode,  &
                     mnode,ndime,phypa,visco,thpre,welin,  &
                     staco,hleng,shape,cartd,elcod,elunk,  &
                     masma,dires,cores,flres,sores,dites,  &
                     cotes,sotes,tauma,force,grunk)
!
! Compressible NS equations as a general system of CDR equations.
!
  case(nine)
!     call cdr_defcom

  end select

end subroutine cdr_defmat
