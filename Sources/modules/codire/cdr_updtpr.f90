subroutine cdr_updtpr(itask)
!-----------------------------------------------------------------------
!****f* Codire/cdr_updtpr
! NAME 
!    cdr_updtpr
! DESCRIPTION
!    This subroutine updates the thermodynamic pressure. The volume of the
!    domain is assumed to be constant. The constants are
!
!    phypa_cdr(14) = cp
!    phypa_cdr(15) = gamma = cp/cv
!    phypa_cdr(17) = (gamma-1) / gamma
!    phypa_cdr(18) = R     = cp-cv
!
! USES
! USED BY
!    cdr_endite
!    cdr_linsea
!***
!-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_codire
  implicit none
  integer(ip) :: itask
  real(rp)    :: derit
!
! Choose equation:
!
  if(kfl_thpeq_cdr==0) then

     ! Calculate volume flow on the boundary
     call cdr_smflow(1,ndofn_cdr,uncdr,s_vfl_cdr(1))

     ! Calculate mass flow on the boundary
     call cdr_smflow(0,ndofn_cdr,uncdr,sitfl_cdr(1))

     ! Write info
     if(itask==2) write(lun_thpre_cdr,100) itinn(modul),s_vfl_cdr(1),sitfl_cdr(1)

  else if(kfl_thpeq_cdr==1) then

     ! Update coefficients
     if(itask==3) then
        if(kfl_twost_cdr==1) then
           vinvt_cdr(3) = vinvt_cdr(2)
           sitfl_cdr(3) = sitfl_cdr(2)
           thpre_cdr(3) = thpre_cdr(2)
        end if
        if(kfl_schem_cdr==2.and.kfl_tiacc_cdr==2) then
           vinvt_cdr(2) = 2.0_rp*vinvt_cdr(1)-vinvt_cdr(2)
           sitfl_cdr(2) = 2.0_rp*sitfl_cdr(1)-sitfl_cdr(2)
           thpre_cdr(2) = 2.0_rp*thpre_cdr(1)-thpre_cdr(2)
        else
           vinvt_cdr(2) = vinvt_cdr(1)
           sitfl_cdr(2) = sitfl_cdr(1)
           thpre_cdr(2) = thpre_cdr(1)
        end if
     end if

     ! Evaluate the integral of the inverse of temperature
     call cdr_vinvte(ndofn_cdr,uncdr,vinvt_cdr(1))

     ! Calculate inverse of temperature flow on the boundary
     sitfl_cdr(1)=0.0_rp
     if(kfl_tmass_cdr==1) call cdr_smflow(0,ndofn_cdr,uncdr,sitfl_cdr(1))

     ! Update thermodynamic pressure
     if(kfl_tiacc_cdr == 2.and.kfl_schem_cdr==1.and.ittim>1) then ! BDF2
        thpre_cdr(1) = (   4.0_rp * thpre_cdr(2) * vinvt_cdr(2)     &
                         -          thpre_cdr(3) * vinvt_cdr(3)  )  &
                     / (   3.0_rp*vinvt_cdr(1) + sitfl_cdr(1) / dtinv_cdr )
     else
        thpre_cdr(1) =   thpre_cdr(2) * vinvt_cdr(2) &
                     / ( vinvt_cdr(1) + sitfl_cdr(1) / dtinv_cdr)
     end if

     ! Update thermodynamic pressure time derivative
     if(kfl_tiacc_cdr == 2.and.kfl_schem_cdr==1.and.ittim>1) then ! BDF2
        derit = dtinv_cdr * (3.0_rp*vinvt_cdr(1)-4.0_rp*vinvt_cdr(2)+vinvt_cdr(3))
     else
        derit = dtinv_cdr * (vinvt_cdr(1)-vinvt_cdr(2))
     end if
     dtpdt_cdr = - thpre_cdr(1) * ( derit + sitfl_cdr(1) ) / vinvt_cdr(1)

     ! Write info
     if(itask==2) write(lun_thpre_cdr,100) itinn(modul),vinvt_cdr(1),sitfl_cdr(1),thpre_cdr(1),dtpdt_cdr

  else if(kfl_thpeq_cdr==2) then

     ! Update coefficients
     if(itask==3) then
        if(kfl_twost_cdr==1) then
           s_vfl_cdr(3) = s_vfl_cdr(2)
           s_hfl_cdr(3) = s_hfl_cdr(2)
           thpre_cdr(3) = thpre_cdr(2)
        end if
        if(kfl_schem_cdr==2.and.kfl_tiacc_cdr==2) then
           s_vfl_cdr(2) = 2.0_rp*s_vfl_cdr(1)-s_vfl_cdr(2)
           s_hfl_cdr(2) = 2.0_rp*s_hfl_cdr(1)-s_hfl_cdr(2)
           thpre_cdr(2) = 2.0_rp*thpre_cdr(1)-thpre_cdr(2)
        else
           s_vfl_cdr(2) = s_vfl_cdr(1)
           s_hfl_cdr(2) = s_hfl_cdr(1)
           thpre_cdr(2) = thpre_cdr(1)
        end if
     end if

     ! Calculate volume flow on the boundary
     call cdr_smflow(1,ndofn_cdr,uncdr,s_vfl_cdr(1))

     ! Calculate heat flow on the boundary
     call cdr_shflow(uncdr,s_hfl_cdr(1),kfl_modul(modul))

     ! Update thermodynamic pressure
     if(kfl_tiacc_cdr == 2.and.kfl_schem_cdr==1.and.ittim>1) then ! BDF2
        derit = (4.0_rp*thpre_cdr(2)-thpre_cdr(3)) * vodom/phypa_cdr(15)
        thpre_cdr(1) = ( derit + s_hfl_cdr(1)*phypa_cdr(17)/dtinv_cdr )       &
                     / ( 3.0_rp*vodom/phypa_cdr(15) + s_vfl_cdr(1)/dtinv_cdr )
     else
        derit = thpre_cdr(2) * vodom/phypa_cdr(15)
        thpre_cdr(1) = ( derit + s_hfl_cdr(1)*phypa_cdr(17)/dtinv_cdr )   &
                     / ( vodom/phypa_cdr(15) + s_vfl_cdr(1)/dtinv_cdr )
     end if

     ! Update thermodynamic pressure time derivative
     dtpdt_cdr = - thpre_cdr(1) * s_vfl_cdr(1) / vodom + phypa_cdr(17) * s_hfl_cdr(1)

     ! Write info
     if(itask==2) write(lun_thpre_cdr,100) itinn(modul),s_vfl_cdr(1),s_hfl_cdr(1),thpre_cdr(1),dtpdt_cdr

  end if

  100 format(10x,i15,4x,e20.13,7x,e20.13,7x,e20.13,7x,e20.13)

end subroutine cdr_updtpr
