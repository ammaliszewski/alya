subroutine cdr_deflom(kfdif,ksyse,lawvi,ncomp,ndofn,nnode,  &
                      mnode,ndime,phypa,visco,thpre,welin,  &
                      staco,hleng,shape,cartd,elcod,elunk,  &
                      masma,dires,cores,flres,sores,dites,  &
                      cotes,sotes,tauma,force,grunk)
!------------------------------------------------------------------------
!****f* Codire/cdr_deflom
! NAME 
!    cdr_deflom
! DESCRIPTION
!    This routine obtains the coefficients of the CDR equations for the
!    case of Low Mach number equations, including the matrix of 
!    stabilization parameters. The linearization parameters are used as in
!     
!    "Finite element approximation of thermal models for low speed flows"
!     R. Codina J. Principe and G. Houzeaux
!     Mecanica Computacional. Vol XXIII (Proceedings of the ENIEF 2004)
!
!     where:
!
!        welin(1)=lambda_11
!        welin(2)=lambda_12
!        welin(3)=lambda_13
!        welin(4)=lambda_21
!        welin(5)=lambda_22
!        welin(6)=lambda_23
!        welin(7)=lambda_31
!        welin(8)=lambda_32
!
! USES
! USED BY
!    cdr_defmat
!***
!-----------------------------------------------------------------------
  use def_parame
  implicit none
  integer(ip), intent(in)  :: kfdif,ksyse,lawvi,ncomp,ndofn,nnode,mnode,ndime
  real(rp),    intent(in)  :: phypa(20),visco(10),thpre,welin(20),staco(10)
  real(rp),    intent(in)  :: hleng(2)
  real(rp),    intent(in)  :: shape(nnode)
  real(rp),    intent(in)  :: cartd(ndime,mnode)
  real(rp),    intent(in)  :: elcod(ndime,mnode)
  real(rp),    intent(in)  :: elunk(ndofn,mnode,ncomp)
  real(rp),    intent(out) :: masma(ndofn,ndofn)
  real(rp),    intent(out) :: dires(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(out) :: cores(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: flres(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: sores(ndofn,ndofn)
  real(rp),    intent(out) :: dites(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(out) :: cotes(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: sotes(ndofn,ndofn)
  real(rp),    intent(out) :: tauma(ndofn,ndofn)
  real(rp),    intent(out) :: force(ndofn)
  real(rp)                 :: grunk(ndime,ndofn)
  integer(ip), save        :: ipass
  real(rp),    save        :: welim,viscp,diffu,perme,react,soun2
  real(rp),    save        :: gnorm,g_dir(3),speci,tdiff,dilat,deref
  real(rp)                 :: veloc(3),tempe,densi,ugrau(3),ugrat,kcoef
  real(rp)                 :: advec,hdisi,tauc1,tauc2,tauc3
  integer(ip)              :: idime,jdime,kdime,ldime,inode,idofn,i
  data ipass/0/
!
! Constant coefficients.
!
  if(ipass==0) then

     welim=0.0_rp                 ! Type of linearization
     do i=1,10
        welim = max(welim,welin(i))
     end do

     deref = phypa( 1)
     perme = phypa( 2)
     react = phypa( 3)            ! = sqrt(phypa(4)**2+phypa(5)**2 + phypa(6)**2)
     tdiff = phypa(13)
     speci = phypa(14)
     soun2 = phypa(8)*phypa(8)

     gnorm    = phypa( 9)
     g_dir(1) = phypa(10)
     g_dir(2) = phypa(11)
     g_dir(3) = phypa(12)

     if(lawvi==0) then
        call cdr_vislaw(zero_rp,viscp,visco,grunk,0.0_rp,lawvi,ndime)
        diffu = viscp
        call cdr_defdif(kfdif,ndofn,ndime,diffu,dires)
     end if

     dires(ndofn,ndofn,1,1) = tdiff
     dires(ndofn,ndofn,2,2) = tdiff
     if(ksyse==2) then
        kcoef = phypa(15)*phypa(13)/phypa(14)/thpre
        dires(ndofn-1,ndofn,1,1) = kcoef
        dires(ndofn-1,ndofn,2,2) = kcoef
     end if

     sores(1,1) =  perme
     sores(2,2) =  perme
     sores(1,2) = -phypa(6)
     sores(2,1) =  phypa(6)

     if(ndime.eq.2) then
        masma(1,1) = 1.0_rp
        masma(2,2) = 1.0_rp
        masma(3,3) = 1.0_rp/soun2
        masma(4,4) = 1.0_rp
        cores(1,3,1) = 1.0_rp
        cores(3,1,1) = 1.0_rp
        cores(2,3,2) = 1.0_rp
        cores(3,2,2) = 1.0_rp
        flres(1,3,1) = 1.0_rp
        flres(3,1,1) = 0.0_rp
        flres(2,3,2) = 1.0_rp
        flres(3,2,2) = 0.0_rp
        sores(3,3) = phypa(7)
     else if(ndime.eq.3) then
        masma(1,1) = 1.0_rp
        masma(2,2) = 1.0_rp
        masma(3,3) = 1.0_rp
        masma(4,4) = 1.0_rp/soun2
        masma(5,5) = 1.0_rp
        dires(5,5,3,3) = tdiff 
        cores(1,4,1) = 1.0_rp
        cores(4,1,1) = 1.0_rp
        cores(2,4,2) = 1.0_rp
        cores(4,2,2) = 1.0_rp
        cores(3,4,3) = 1.0_rp
        cores(4,3,3) = 1.0_rp
        flres(1,4,1) = 1.0_rp
        flres(4,1,1) = 0.0_rp
        flres(2,4,2) = 1.0_rp
        flres(4,2,2) = 0.0_rp
        flres(3,4,3) = 1.0_rp
        flres(4,3,3) = 0.0_rp
        sores(3,3) =  perme
        sores(4,4) =  phypa(7)
        sores(1,3) =  phypa(5)
        sores(3,1) = -phypa(5)
        sores(2,3) = -phypa(4)
        sores(3,2) =  phypa(4)
     end if

     dites = dires   ! Test functions
     cotes = cores
     sotes = sores

     ipass = 1
  end if
!
! Coefficients that depend on the iteration.
!
! 1) Unknowns at the previous iteration
!
  veloc=0.0_rp
  tempe=0.0_rp
  do inode=1,nnode
     do idime=1,ndime
        veloc(idime) = veloc(idime) + shape(inode)*elunk(idime,inode,1)
     end do
     tempe = tempe + shape(inode)*elunk(ndofn,inode,1)
  end do
  densi = thpre / tempe
!  if(lawvi>zero_rp .or. welim>zero_rp) then     ! Gradient of the unknown
     grunk= 0.0_rp
     do inode=1,nnode
        do idofn=1,ndofn
           do idime=1,ndime
              grunk(idime,idofn) = grunk(idime,idofn) + cartd(idime,inode)*elunk(idofn,inode,1)
           end do
        end do
     end do
!  end if
!  if(welim>zero_rp) then                      ! Convective derivatives
     ugrau = 0.0_rp
     do idime=1,ndime
        do jdime=1,ndime
           ugrau(idime) = ugrau(idime) + veloc(jdime)*grunk(jdime,idime)
        end do
     end do
     ugrat = 0.0_rp
     do jdime=1,ndime
        ugrat = ugrat + veloc(jdime)*grunk(jdime,ndofn)
     end do
!  end if
!
! 2) Diffusive terms (temperature and/or velocity dependent viscosity)
!
  if(lawvi>0) then
     call cdr_vislaw(zero_rp,viscp,visco,grunk,tempe,lawvi,ndime)
     diffu = viscp
     call cdr_defdif(kfdif,ndofn,ndime,diffu,dires)
     dites = dires   ! Test function
  end if
!
! 3) Stabilization parameters
!
!  advec = sqrt(vx*vx+vy*vy+vz*vz)
  advec=sqrt(veloc(1)**2+veloc(2)**2+veloc(3)**2)
  hdisi = 1.0_rp/hleng(2)
  tauc1 = staco(1)*diffu*hdisi*hdisi   &
    +     staco(2)*densi*advec*hdisi   &
    +     staco(3)*react               &
    +     perme
  if(tauc1>zero_rp) tauc1 = 1.0_rp/tauc1
  tauc2 = staco(4)*diffu                    &
    +     staco(5)*densi*advec*hleng(2)     &
    +     staco(6)*react*hleng(2)*hleng(2)
  tauc3 = staco(7)*tdiff*hdisi*hdisi  &
    +     staco(8)*densi*speci*advec*hdisi
  if(tauc3>zero_rp) tauc3 = 1.0_rp/tauc3
  tauma(1,1) = tauc1
  tauma(2,2) = tauc1
  if(ndime==2) then
     tauma(3,3) = tauc2
     tauma(4,4) = tauc3
  else if(ndime==3) then
     tauma(3,3) = tauc1
     tauma(4,4) = tauc2
     tauma(5,5) = tauc3
  end if
!
! 4) Convection terms
!
  do jdime=1,ndime
     do idime=1,ndime
        cores(idime,idime,jdime)=densi*veloc(jdime)
     end do
     cores(ndofn,ndofn,jdime)=densi*speci*veloc(jdime)
  end do
  if(ksyse==1) then
     do jdime=1,ndime
        cores(ndofn-1,ndofn,jdime) = 0.0_rp
     end do
  end if
  cotes = cores
  if(ksyse==1) then
     do jdime=1,ndime   ! These terms are not included in the test function
        cores(ndofn-1,ndofn,jdime)= -welin(4)*veloc(jdime)/tempe
     end do
  end if
!
! 5) Reaction terms
!
!        welin(1)=lambda_11
!        welin(2)=lambda_12
!        welin(3)=lambda_13
!        welin(4)=lambda_21
!        welin(5)=lambda_22
!        welin(6)=lambda_23
!        welin(7)=lambda_31
!        welin(8)=lambda_32
!
  if(welim>zero_rp) then
     sores=0.0_rp
     sores(1,1) =  perme
     sores(2,2) =  perme
     sores(1,2) = -phypa(6)
     sores(2,1) =  phypa(6)
     if(ndime==3) then
        sores(3,3) =  perme
        sores(1,3) =  phypa(5)
        sores(3,1) = -phypa(5)
        sores(2,3) = -phypa(4)
        sores(3,2) =  phypa(4)
     end if
     sotes = sores

     do idime=1,ndime   ! These terms are not included in the test function
        do jdime=1,ndime
           sores(idime,jdime) = sores(idime,jdime)  &
                              + welin(1)*densi*grunk(jdime,idime)
        end do
        sores(idime,ndofn) = densi/tempe * ( - welin(2) * ugrau(idime)  &
                                             + welin(3) * gnorm*g_dir(idime) )
     end do
     if(ksyse==1) then
        do jdime=1,ndime
           sores(ndofn-1,jdime) = - welin(5) * grunk(jdime,ndofn)/tempe
        end do
        sores(ndofn-1,ndofn) = welin(6) * ugrat / tempe**2
     end if
     do jdime=1,ndime
        sores(ndofn,jdime) = welin(7)*densi*speci*grunk(jdime,ndofn)
     end do
     sores(ndofn,ndofn) = - welin(8)*densi*speci*ugrat/tempe

  end if
!
! 6) Forces
!
  do idime=1,ndime
     force(idime) = (welin(1)-welin(2))*densi*ugrau(idime)          &
                  + (   1.0_rp+welin(3))*densi*gnorm*g_dir(idime)    &
                  - deref*gnorm*g_dir(idime)
  end do
  if(ksyse==1) &
  force(ndofn-1) = (1.0_rp-welin(4)-welin(5)+welin(6) ) * ugrat / tempe
  force(ndofn  ) = (welin(7)-welin(8))*densi*ugrat
!
end subroutine cdr_deflom

!
! A backup:
!
!
!  ddete = - densi / tempe
!
!  cores(1,1,1) = vx
!  cores(2,2,1) = vx
!  cores(1,1,2) = vy
!  cores(2,2,2) = vy
!  if(ndime.eq.2) then
!     cores(4,4,1) = vx
!     cores(4,4,2) = vy
!  else if(ndime.eq.3) then
!     cores(3,3,1) = vx
!     cores(3,3,2) = vy
!     cores(1,1,3) = vz
!     cores(2,2,3) = vz
!     cores(3,3,3) = vz
!     cores(5,5,1) = vx
!     cores(5,5,2) = vy
!     cores(5,5,3) = vz
!  end if
!  if(ndime.eq.2) then   ! These terms are not included in the test function
!     cores(3,4,1)= -welin(4)*vx/te
!     cores(3,4,2)= -welin(4)*vy/te
!  else if(ndime.eq.3) then
!     cores(3,4,1)= -welin(4)*vx/te
!     cores(3,4,2)= -welin(4)*vy/te
!     cores(3,4,3)= -welin(4)*vz/te
!  end if
!
!  cotes = cores ! Test function
!
!
!  sores(1,1) =  perme    + welin(1)*densi*grunk(1,1)
!  sores(2,2) =  perme    + welin(1)*densi*grunk(2,2)
!  sores(1,2) = -phypa(6) + welin(1)*densi*grunk(2,1)
!  sores(2,1) =  phypa(6) + welin(1)*densi*grunk(1,2)
!  if(ndime==2) then
!     sores(1,4) =   ddete * welin(2)* ( vx*grunk(1,1) + vy*grunk(2,1) )    &
!                  - ddete * welin(3)* gnorm*g_dir(1)
!     sores(2,4) =   ddete * welin(2)* ( vx*grunk(1,2) + vy*grunk(2,2) )    &
!                  - ddete * welin(3)* gnorm*g_dir(2)
!     sores(3,1) = welin(5)*grunk(1,4)/te
!     sores(3,2) = welin(5)*grunk(2,4)/te
!     sores(3,4) = welin(6)*(vx*grunk(1,4)+vy*grunk(2,4)) / te**2
!
!     sores(4,1) = welin(7)*densi*grunk(1,4)
!     sores(4,2) = welin(7)*densi*grunk(2,4)
!     sores(4,4) = welin(8)*ddete*(vx*grunk(1,4)+vy*grunk(2,4))
!  else if(ndime==3) then
!     sores(3,3) =  perme    + welin(1)*densi*grunk(3,3)
!     sores(1,3) =  phypa(5) + welin(1)*densi*grunk(3,1)
!     sores(3,1) = -phypa(5) + welin(1)*densi*grunk(1,3)
!     sores(2,3) = -phypa(4) + welin(1)*densi*grunk(3,2)
!     sores(3,2) =  phypa(4) + welin(1)*densi*grunk(2,3)
!
!     sores(1,5) =   ddete * welin(2)* ( vx*grunk(1,1) + vy*grunk(2,1) )    &
!                  - ddete * welin(3)* gnorm*g_dir(1)
!     sores(2,5) =   ddete * welin(2)* ( vx*grunk(1,2) + vy*grunk(2,2) )    &
!                  - ddete * welin(3)* gnorm*g_dir(2)
!     sores(3,5) =   ddete * welin(2)* ( vx*grunk(1,3) + vy*grunk(2,3) )    &
!                  - ddete * welin(3)* gnorm*g_dir(3)
!
!     sores(4,1) = welin(5)*grunk(1,5)/te
!     sores(4,2) = welin(5)*grunk(2,5)/te
!     sores(4,3) = welin(5)*grunk(2,5)/te
!     sores(4,5) = welin(6)*(vx*grunk(1,5)+vy*grunk(2,5)+vz*grunk(3,5)) / te**2
!
!     sores(5,1) = welin(7)*densi*grunk(1,5)
!     sores(5,2) = welin(7)*densi*grunk(2,5)
!     sores(5,3) = welin(7)*densi*grunk(3,5)
!     sores(5,5) = welin(8)*ddete*(vx*grunk(1,5)+vy*grunk(2,5)+vz*grunk(3,5))
!  end if
!
!
!  force(1) =   (-welin(1)+welin(2))*densi*(vx*grunk(1,1)+vy*grunk(2,1))   &
!             + (   1.0_rp+welin(3))*densi*gnorm*g_dir(1)
!  force(2) =   (-welin(1)+welin(2))*densi*(vx*grunk(1,2)+vy*grunk(2,2))   &
!             + (   1.0_rp+welin(3))*densi*gnorm*g_dir(2)
!
!  if(ndime==2) then
!     force(3) = (1.0_rp-welin(4)-welin(5)+welin(6) ) * (vx*grunk(1,4)+vy*grunk(2,4)) / te
!     force(4) = (-welin(7)+welin(8))*densi*(vx*grunk(1,4)+ vy*grunk(2,4) )
!
!  else if(ndime==3) then
!     force(1) =  force(1) + (-welin(1)+welin(2))*densi*vz*grunk(3,1)
!     force(2) =  force(2) + (-welin(1)+welin(2))*densi*vz*grunk(3,2)
!     force(3) = (-welin(1)+welin(2))*densi*(vx*grunk(1,3)+vy*grunk(2,3)+vz*grunk(3,3))   &
!              + (   1.0_rp+welin(3))*densi*gnorm*g_dir(3)
!     force(4) = (1.0_rp-welin(4)-welin(5)+welin(6) )        *  &
!                (vx*grunk(1,5)+vy*grunk(2,5)+vz*grunk(3,5)) / te
!     force(5) = (-welin(7)+welin(8))*densi*(vx*grunk(1,5)+ vy*grunk(2,5)+vz*grunk(3,5))
!  end if
!
!
! To find bugs:
!
! if(ipoin.eq.209.and.jicdr.eq.maxitcdr) then        
!    write(18,*) &
!       'cores',(((cores(idofncdr,jdofncdr,idime),idofncdr=1,ndofncdr),jdofncdr=1,ndofncdr),idime=1,ndime)
!    write(18,*)
!    write(18,*) &
!       'flres',(((flres(idofncdr,jdofncdr,idime),idofncdr=1,ndofncdr),jdofncdr=1,ndofncdr),idime=1,ndime)
!    write(18,*)
!    write(18,*) &
!       'dires',(((dires(idofncdr,jdofncdr,idime),idofncdr=1,ndofncdr),jdofncdr=1,ndofncdr),idime=1,ndime)
!    write(18,*)
!    write(18,*) &
!       'sotes',((sotes(idofncdr,jdofncdr),idofncdr=1,ndofncdr),jdofncdr=1,ndofncdr)
!    write(18,*)
!    write(18,*) &
!       'sores',((sores(idofncdr,jdofncdr),idofncdr=1,ndofncdr),jdofncdr=1,ndofncdr)
!    write(18,*)
!    write(18,*) &
!       'force',(force(idofncdr),idofncdr=1,ndofncdr)
!    write(18,*)
!    write(18,*) &
!       'masma',(masma(idofncdr),idofncdr=1,ndofncdr)
!        write(18,*) '======================================================='
! end if
