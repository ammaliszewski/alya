subroutine cdr_tittim
  !-----------------------------------------------------------------------
  !****f* Codire/cdr_tittim
  ! NAME 
  !    cdr_tittim
  ! DESCRIPTION
  !    This routine sets the time step
  ! USES
  ! USED BY
  !    cdr_begite
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_codire
  implicit none

  if(kfl_timco/=2) then
     dtinv_cdr=dtinv
     if(kfl_stead_cdr==1) dtinv_cdr = 0.0_rp
     if(kfl_schem_cdr==2.and.kfl_tiacc_cdr==2) dtinv_cdr = 2.0_rp*dtinv_cdr
  end if

  routp(1)=dtcri_cdr
  ioutp(1)=kfl_timei_cdr
  ioutp(2)=kfl_stead_cdr
  call outfor(8_ip,lun_outpu,' ')

end subroutine cdr_tittim
