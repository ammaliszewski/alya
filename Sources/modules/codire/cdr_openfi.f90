subroutine cdr_openfi
!-----------------------------------------------------------------------
!****f* Codire/cdr_openfi
! NAME 
!   cdr_openfi
! DESCRIPTION
!   This subroutine gets ALL the file names and opens some of them to 
!   be used by the module in two possible ways:
!
!   1. Recalling them from the environment, when Alya is launched
!      encapsulated in a shell script, or
!   2. Composing the names out of the problem name which is given as argument
!      when the binary file Alya is launched "naked". 
! USED BY
!   cdr_turnon
!***
!-----------------------------------------------------------------------
  use      def_codire
  use      def_master
  use      mod_iofile
  implicit none
  integer(ip)    :: ilcha 
  character(150) :: fil_pdata_cdr,fil_outpu_cdr,fil_conve_cdr
  character(150) :: fil_solve_cdr,fil_thpre_cdr
!
! kfl_naked is set in the kernel subroutine getnam
!
  if (kfl_naked==0) then
     !  encapsulated, then get names from the environment
     call GETENV('FOR301',fil_pdata_cdr)                        ! loaded by the script
     call GETENV('FOR302',fil_outpu_cdr)                        ! loaded by the script
     call GETENV('FOR303',fil_conve_cdr)                        ! loaded by the script
     call GETENV('FOR304',fil_solve_cdr)                        ! loaded by the script
     call GETENV('FOR305',fil_rstar_cdr)                        ! loaded by the script
     call GETENV('FOR308',fil_thpre_cdr)                        ! loaded by the script
     call GETENV('FOR309',fil_linse_cdr)                        ! loaded by the script
     
     call GETENV('FOR311',fil_sectx_cdr)                        ! Loaded by the script
     call GETENV('FOR312',fil_secty_cdr)                        ! Loaded by the script
     call GETENV('FOR313',fil_sectz_cdr)                        ! Loaded by the script

     call GETENV('FOR321',fil_trap1_cdr)                        ! Loaded by the script
     call GETENV('FOR322',fil_trap2_cdr)                        ! Loaded by the script
     call GETENV('FOR323',fil_trap3_cdr)                        ! Loaded by the script
     call GETENV('FOR324',fil_trap4_cdr)                        ! Loaded by the script
     call GETENV('FOR325',fil_trap5_cdr)                        ! Loaded by the script

     call GETENV('FOR330',fil_fluxe_cdr)                        ! loaded by the script
     call GETENV('FOR331',fil_force_cdr)                        ! loaded by the script
     call GETENV('FOR332',fil_splot_cdr)                        ! loaded by the script

  else if (kfl_naked==1) then
     !  naked, then compose the names
     fil_pdata_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.dat'
     fil_outpu_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.log'
     fil_conve_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.cvg'
     fil_solve_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.sol'
     fil_rstar_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.rst'
     fil_thpre_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.thp'
     fil_linse_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.lse'

     fil_sectx_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.sex'
     fil_secty_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.sey'
     fil_sectz_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.sez'

     fil_fluxe_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.flu'
     fil_force_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.frc'
     fil_splot_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.spl'
     fil_trap1_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.tp1'
     fil_trap2_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.tp2'
     fil_trap3_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.tp3'
     fil_trap4_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.tp4'
     fil_trap5_cdr = adjustl(trim(namda))//'.'//exmod(modul)//'.tp5'

  end if
!
! Open files
!
  call iofile(zero,lun_pdata_cdr,fil_pdata_cdr,'CODIRE DATA','old')
  if(kfl_rstar==2) then
     call iofile(zero,lun_outpu_cdr,fil_outpu_cdr,'CODIRE OUTPUT'     ,'old','formatted','append')
     call iofile(zero,lun_conve_cdr,fil_conve_cdr,'CODIRE CONVERGENCE','old','formatted','append')
     call iofile(zero,lun_solve_cdr,fil_solve_cdr,'CODIRE SOLVER'     ,'old','formatted','append')
     if(kfl_modul(modul) == 8)    &
        call iofile(zero,lun_thpre_cdr,fil_thpre_cdr,'CODIRE P_TH')
  else
     call iofile(zero,lun_outpu_cdr,fil_outpu_cdr,'CODIRE OUTPUT')
     call iofile(zero,lun_conve_cdr,fil_conve_cdr,'CODIRE CONVERGENCE')
     call iofile(zero,lun_solve_cdr,fil_solve_cdr,'CODIRE SOLVER')
     if(kfl_modul(modul) == 8)    &
        call iofile(zero,lun_thpre_cdr,fil_thpre_cdr,'CODIRE P_TH')
  end if

end subroutine cdr_openfi
