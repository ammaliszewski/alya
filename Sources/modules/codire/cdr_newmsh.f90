subroutine cdr_newmsh
!-----------------------------------------------------------------------
!****f* Codire/cdr_newmsh
! NAME 
!    cdr_newmsh
! DESCRIPTION
!    This routine projects data from the old to the new mesh
! USES
!    cdr_openfi
!    cdr_reabcs
!    cdr_memall
! USED BY
!    Codire
!***
!-----------------------------------------------------------------------
  use      def_master
  implicit none

  if(kfl_algor_msh==1) then                  ! SHEAR_SLIP


  elseif(kfl_algor_msh==2) then              ! GLOBAL REFINEMENT

     ! Save data and deallocate vectors

     ! Project data onto new mesh

     ! Read new mesh data (bc's)

     ! Open files
     ! call cdr_openfi
     ! Read the boundary conditions
     ! call cdr_reabcs
     ! Allocate memory (veloc, press and solver)
     ! call cdr_memall

  end if
  
end subroutine cdr_newmsh
