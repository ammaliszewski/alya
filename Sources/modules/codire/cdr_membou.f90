subroutine cdr_membou(itask)
!------------------------------------------------------------------------
!****f* Codire/cdr_membou
! NAME 
!    cdr_membou
! DESCRIPTION
!    This routine allocates memory for local boundary arrays
! USED BY
!    cdr_matrix
!    cdr_lstarg
!    cdr_forces
!***
!-----------------------------------------------------------------------
use def_parame
use def_master
use def_domain
use def_codire
use def_cdrloc
use mod_memchk
implicit none
integer(ip), intent(in) :: itask
integer(4)              :: istat

  if(itask==1) then

     ! Gather operations
     allocate(bounk(ndofn_cdr,mnodb),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_membou',bounk)
     allocate(bocod(ndime,mnodb),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_membou',bocod)

     ! Jacobians and derivatives
     allocate(baloc(ndime,ndime),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_membou',baloc)

  else if(itask==2) then

     ! Gather operations
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_membou',bounk)
     deallocate(bounk,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_membou',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_membou',bocod)
     deallocate(bocod,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_membou',0_ip)

     ! Jacobians and derivatives
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_membou',baloc)
     deallocate(baloc,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_membou',0_ip)

  end if

end subroutine cdr_membou
