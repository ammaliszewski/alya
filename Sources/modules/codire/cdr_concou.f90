subroutine cdr_concou

!-----------------------------------------------------------------------
!    
! This routine checks the CDR convergence of the run.
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_codire    ! Problem 4
  implicit none
  !
  ! Check convergence
  !
  if(kfl_conve(modul)==1) then
     if(resid_cdr>cotol_cdr) kfl_gocou = 1
  end if
  glres(modul) = resid_cdr
  !
  ! Output residuals
  !
  coutp(1)='CDR UNKNOWNS'
  routp(1)=resid_cdr
  call outfor(9_ip,lun_outpu,' ')

end subroutine cdr_concou
