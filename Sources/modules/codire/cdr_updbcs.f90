subroutine cdr_updbcs(itask)
!-----------------------------------------------------------------------
!****f* Codire/cdr_updbcs
! NAME 
!    cdr_updbcs
! DESCRIPTION
!    This routine updates the boundary conditions for the unknown of the
!    CDR equations at each time step.      
! USED BY
!    cdr_begste
!***
!-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_codire
  implicit none
  integer(ip), intent(in) :: itask
  real(rp), external      :: funcre
  integer(ip)             :: ipoin,idofn
  real(rp)                :: unnew,unold

  select case(itask)
  !  
  ! Before a time step
  !     
  case (1)
     if(kfl_conbc_cdr==0) then
        do ipoin = 1,npoin
           if(kfl_funno_cdr(ipoin)/=0) then
!
              if(kfl_funty_cdr(kfl_funno_cdr(ipoin),1)==4) then     ! Special
                 if((kfl_modul(modul)==7.or.kfl_modul(modul)==8).and.ittim==1) then
                    bvess_cdr(ndofn_cdr,ipoin,1)= &
                            funpa_cdr(kfl_funno_cdr(ipoin))%a(1) +          &
                            bvess_cdr(ndofn_cdr,ipoin,2)                    &
                            *funcre(funpa_cdr(kfl_funno_cdr(ipoin))%a,      &
                                    kfl_funty_cdr(kfl_funno_cdr(ipoin),2),  &
                                    kfl_funty_cdr(kfl_funno_cdr(ipoin),1),cutim)
                    uncdr(ndofn_cdr,ipoin,3) = bvess_cdr(ndofn_cdr,ipoin,1)
                    if(kfl_twost_cdr==1) &
                    uncdr(ndofn_cdr,ipoin,4) = bvess_cdr(ndofn_cdr,ipoin,1)
                 end if
              else
                 do idofn=1,ndofn_cdr
                    if(kfl_fixno_cdr(idofn,ipoin)==1) then
                       unnew=bvess_cdr(idofn,ipoin,2)&
                            *funcre(funpa_cdr(kfl_funno_cdr(ipoin))%a,      &
                                    kfl_funty_cdr(kfl_funno_cdr(ipoin),2),  &
                                    kfl_funty_cdr(kfl_funno_cdr(ipoin),1),cutim)
                       if(kfl_timei_cdr/=0.and. &  ! Second order
                          kfl_schem_cdr==2.and. &  ! Crank Nicolson
                          kfl_tiacc_cdr==2) then
                             unold=uncdr(idofn,ipoin,ncomp_cdr)
                             bvess_cdr(idofn,ipoin,1)=0.50_rp*(unnew+unold)
                       else
                          bvess_cdr(idofn,ipoin,1)=unnew
                       end if
                    end if
                 end do
              end if
!
           end if
        end do
     end if
  !
  ! Before a global iteration
  !  
  case (2)

  !
  ! Before an inner iteration
  ! 
  case (3)

  end select

end subroutine cdr_updbcs
!
!
! A backup:
!
! Update of boundary values and prescription of Dirichlet conditions
!
!  if(kfl_modul(modul)==7.or.kfl_modul(modul)==8) then
!     do ipoin = 1,npoin
!        if(fucdr(ipoin).gt.0) then
!           do idofn = 1,ndofncdr-1
!              if(tftyp(fucdr(ipoin)).ne.2)
!     .           bvcdr(idofn,ipoin) = bvcdr(idofn,ipoin) *
!     .                               timefu (tftyp(fucdr(ipoin)),tfpar(1,fucdr(ipoin)))
!           end do
!           if(tftyp(fucdr(ipoin)).eq.2.and.ittim.eq.1) then
!              if(bccdr(ndofncdr,ipoin).eq.1)
!     .        bvcdr(ndofncdr,ipoin)   = tfpar(1,fucdr(ipoin)) + bvcdr(ndofncdr,ipoin) *
!     .                                  timefu (tftyp(fucdr(ipoin)),tfpar(1,fucdr(ipoin)))
!              uncdr(ndofncdr,ipoin,3) = tfpar(1,fucdr(ipoin)) + uncdr(ndofncdr,ipoin,3) *
!     .                                  timefu (tftyp(fucdr(ipoin)),tfpar(1,fucdr(ipoin)))
!              if(bccdr(ndofncdr,ipoin).eq.1) uncdr(ndofncdr,ipoin,3) = bvcdr(idofn,ipoin)
!           else if(tftyp(fucdr(ipoin)).ne.2) then
!              bvcdr(ndofncdr,ipoin) = bvcdr(ndofncdr,ipoin) *
!     .                                timefu (tftyp(fucdr(ipoin)),tfpar(1,fucdr(ipoin)))
!           end if  
!        end if
!        do idofn=1,ndofncdr
!           if(bccdr(idofn,ipoin).eq.1) then
!               uncdr(idofn,ipoin,1) = bvcdr(idofn,ipoin)
!               uncdr(idofn,ipoin,2) = bvcdr(idofn,ipoin)
!           end if
!        end do
!     end do
!  else
!     do ipoin = 1,npoin
!        do idofn = 1,ndofn_cdr
!           if(fucdr(ipoin).gt.0) then
!              bvcdr(idofn,ipoin) = bvcdr(idofn,ipoin) *
!              timefu (tftyp(fucdr(ipoin)),tfpar(1,fucdr(ipoin)))
!           end if
!           if(kfl_fixno_cdr(idofn,ipoin).eq.1) then
!              uncdr(idofn,ipoin,1) = bvess_cdr(idofn,ipoin,1)
!              uncdr(idofn,ipoin,2) = bvess_cdr(idofn,ipoin,1)
!           end if
!        end do
!     end do
!  end if
!
