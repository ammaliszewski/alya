module def_cdrloc
!-----------------------------------------------------------------------
!
! Local variables for integration
!
!-----------------------------------------------------------------------
  use def_kintyp
!
! Assembly matrices
!
  real(rp), allocatable ::&
     wmatr(:,:,:,:),&             ! Working amatr
     wrhsi(:,:),&                 ! Working rhsid
     elmat(:,:,:,:),&             ! Element matrix
     elrhs(:,:),&                 ! Element rhs
     wprod(:)                     ! Working array for products
!
! Element gather operations
!
  real(rp), allocatable ::&
     elunk(:,:,:),&               ! Unknowns
     elcod(:,:)                   ! Coordinates
!
! Element jacobians and derivatives
!
  real(rp), allocatable ::&
     cartd(:,:),&                 ! Cartesian derivatives
     xjaci(:,:),&                 ! Inverse of jacobian
     xjacm(:,:),&                 ! Jacobian
     hessi(:,:),&                 ! Hessian
     wmat1(:,:,:),&               ! Working matrix for hessian calculation
     wmat2(:,:,:),&               ! Working matrix for hessian calculation
     d2sdx(:,:,:)                 ! Working matrix for hessian calculation
!
! Element stabilization variables
!
  real(rp), allocatable ::&
     tragl(:,:),&                 ! Inverse of jacobian at c.o.g
     hleng(:)                     ! Element length
!
! Element values at Gauss points
!
  real(rp) ::&
     dvolu,&                      ! Volume element
     detjm                        ! Jacobian determinant

  real(rp), allocatable ::&
     olunk(:,:),&                 ! Old unknowns
     grunk(:,:),&                 ! Gradient of unknowns
     vlapl(:),&                   ! Diffusion matrix as a vector
     ptest(:,:,:),&               ! Test function
     resma(:,:,:),&               ! Residual matrix
     resfo(:)                     ! Residual force
!
! Boundary gather operations
!
  real(rp), allocatable ::&
     bounk(:,:),&                 ! Boundary element unknowns
     bocod(:,:)                   ! Boundary element coordinates
!
! Boundary jacobians and derivatives
!
  real(rp), allocatable ::&
     baloc(:,:)                   ! Local base of boundary element
!
! Boundary values at Gauss points
!
  real(rp) ::&
     dsurf,&                      ! Surface element
     eucta                        ! Norm of baloc

end module def_cdrloc
