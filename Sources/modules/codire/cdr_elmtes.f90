subroutine cdr_elmtes(ndime,nnode,ntens,ndofn,llapl,   &
                      dites_cdr,cotes_cdr,sotes_cdr,   &
                      shape,cartd,hessi,vlapl,zero_rp,   &
                      ptest)
!------------------------------------------------------------------------
!****f* Codire/cdr_elmtes
! NAME 
!    cdr_elmtes
! DESCRIPTION
!    This routine computes the perturbation of the weight function for
!    the CDR equation 
! USES
! USED BY
!    cdr_elmope
!***
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none

  integer(ip), intent(in)  :: ndime,nnode,ntens,ndofn,llapl
  real(rp),    intent(in)  :: dites_cdr(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(in)  :: cotes_cdr(ndofn,ndofn,ndime)
  real(rp),    intent(in)  :: sotes_cdr(ndofn,ndofn)
  real(rp),    intent(in)  :: shape(nnode)
  real(rp),    intent(in)  :: cartd(ndime,nnode)
  real(rp),    intent(in)  :: hessi(ntens,nnode*llapl)
  real(rp)                 :: vlapl(ntens*llapl)
  real(rp),    intent(in)  :: zero_rp
  real(rp),    intent(out) :: ptest(ndofn,ndofn,nnode)

  integer(ip)              :: inode,idofn,jdofn,idime,itens

  ptest=0.0_rp

! Contribution from the advection term
  do inode=1,nnode
     do jdofn=1,ndofn
        do idofn=1,ndofn
           do idime=1,ndime
              ptest(idofn,jdofn,inode) = ptest(idofn,jdofn,inode) +                                &
                    cotes_cdr(idofn,jdofn,idime)*cartd(idime,inode)
           end do
        end do
     end do
  end do

  if(llapl.ne.0) then
! Contribution from the diffusive term (only K_ij d^2 N / dx_i  dx_j)
! To do: add dK_ij /dx_i dN / dx_j)
     do inode=1,nnode
        do jdofn=1,ndofn
           do idofn=1,ndofn
              call matove(vlapl,dites_cdr(idofn,jdofn,:,:),ndime,ntens)
              do itens=1,ntens
                 ptest(idofn,jdofn,inode) = ptest(idofn,jdofn,inode) +                             &
                       hessi(itens,inode)*vlapl(itens)
              end do
           end do
        end do
     end do
  end if

! Contribution from the reaction term
  do inode=1,nnode
     do jdofn=1,ndofn
        do idofn=1,ndofn
           ptest(idofn,jdofn,inode) = ptest(idofn,jdofn,inode) -                             &
                 shape(inode)*sotes_cdr(idofn,jdofn)
        end do
     end do
  end do

end subroutine cdr_elmtes
