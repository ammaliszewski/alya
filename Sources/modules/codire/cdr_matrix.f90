subroutine cdr_matrix
!------------------------------------------------------------------------
!****f* Codire/cdr_matrix
! NAME 
!    cdr_matrix
! DESCRIPTION
!    Compute elemental matrix and RHS of the CDR problem
! USES
!    cdr_elmope
!    cdr_bouope
! USED BY
!    cdr_solite
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  use def_cdrloc
  implicit none
  integer(ip) :: ielem,pelty,pnode
  integer(ip) :: iboun,pblty,pnodb
!
! Allocate local variables
!
  call cdr_memelm(one)
  call cdr_membou(one)
  call cdr_memass(one)
!
! Initializations
!
  amatr=0.0_rp
  rhsid=0.0_rp
!
! Loop over elements
!
  elements: do ielem=1,nelem

     pelty=ltype(ielem)
     pnode=nnode(pelty)

     ! Element matrix and right hand side
     call cdr_elmope(ielem,pnode,pelty,kfl_modul(modul),ncomp_cdr,ndofn_cdr,uncdr)

     ! Prescribe Dirichlet boundary conditions
     call cdr_elmdir(ndofn_cdr,pnode,mnode,npoin,lnods(1,ielem),          &
                     kfl_fixno_cdr,bvess_cdr,elmat,elrhs)

     ! Assembly
     call cdr_assrhs(ndofn_cdr,pnode,mnode,lnods(1,ielem),elrhs,rhsid)

     call cdr_assmat(ndofn_cdr,pnode,ndofn_cdr*mnode,nunkn_cdr,     &
                     kfl_algso_cdr,lnods(1,ielem),lpcdr,elmat,amatr)

  end do elements
!
! Boundary assembly
!
  if(nboun>0) then

     boundaries: do iboun=1,nboun
     
        if( maxval(kfl_fixbo_cdr(:,iboun)) > 0 .or. &
            (kfl_modul(modul)==8.and.kfl_syseq_cdr==2) ) then

        pblty=ltypb(iboun)
        pnodb=nnode(pblty)
        ielem=lboel(pnodb+1,iboun)
        pelty=ltype(ielem)
        pnode=nnode(pelty)

        call cdr_bouope(iboun,pblty,pnodb,ielem,pelty,pnode, &
                        kfl_modul(modul),ncomp_cdr,ndofn_cdr,uncdr)

        ! Prescribe Dirichlet boundary conditions
        call cdr_elmdir(ndofn_cdr,pnode,mnode,npoin,lnods(1,ielem),          &
                        kfl_fixno_cdr,bvess_cdr,elmat,elrhs)

        ! Assembly
        call cdr_assrhs(ndofn_cdr,pnode,mnode,lnods(1,ielem),elrhs,rhsid)

        call cdr_assmat(ndofn_cdr,nnode,ndofn_cdr*mnode,nunkn_cdr,     &
                        kfl_algso_cdr,lnods(1,ielem),lpcdr,elmat,amatr)

        end if

     end do boundaries

  end if
!
! Deallocate local variables
!
  call cdr_memelm(two)
  call cdr_membou(two)
  call cdr_memass(two)

end subroutine cdr_matrix
