subroutine cdr_solite
  !-----------------------------------------------------------------------
  !****f* Codire/cdr_solite
  ! NAME 
  !    cdr_solite
  ! DESCRIPTION
  !    This routine solves an iteration of the CDR equations.
  ! USES
  !    cdr_matrix
  !    Soldir
  !    Solite
  ! USED BY
  !    cdr_doiter
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_codire
  use def_solver
  implicit none
  integer(ip) :: iztot
  real(rp)                          :: cpu_refe1,cpu_refe2
  !
  ! Update inner iteration counter and write headings in the
  ! solver file.
  !
  itinn(modul) = itinn(modul) + 1
  welin_cdr(1:10) = 0.0_rp
  if((itinn(modul)>npica_cdr).and.(kfl_linme_cdr==2)) then
     welin_cdr(1:10)=welin_cdr(11:20)
  end if
  !
  ! Construct the system matrix and right-hand-side.
  !
  call cdr_matrix
  !
  ! Solve the algebraic system.
  !
  call solver(rhsid,unkno,amatr,pmatr)

end subroutine cdr_solite
