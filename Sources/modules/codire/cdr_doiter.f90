subroutine cdr_doiter
!-----------------------------------------------------------------------
!****f* Codire/cdr_doiter
! NAME 
!    cdr_doiter
! DESCRIPTION
!    This routine controls the internal loop of the CDR equations.
! USES
!    cdr_begite
!    cdr_solite
!    cdr_endite
! USED BY
!    Codire
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_solver
  use def_codire
  implicit none
  real(rp)    :: cpu_refe1,cpu_refe2

  if(kfl_delay(modul)/=0) return 

  cpu_solve = 0.0_rp
  call cputim(cpu_refe1)

  if(kfl_stead_cdr==0) then
     call cdr_begite
     do while(kfl_goite_cdr==1)
        call cdr_solite
        call cdr_endite(one)
     end do
     call cdr_endite(two)
  end if

  call cputim(cpu_refe2)
  cpu_modul(1,modul) =  cpu_modul(1,modul) + cpu_refe2 - cpu_refe1 
  cpu_modul(2,modul) =  cpu_modul(2,modul) + cpu_solve

end subroutine cdr_doiter
