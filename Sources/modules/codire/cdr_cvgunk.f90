subroutine cdr_cvgunk(itask)
!-----------------------------------------------------------------------
!****f* Codire/cdr_cvgunk
! NAME 
!    cdr_cvgunk
! DESCRIPTION
!    This routine checks convergence of the CDR equations at:
!    - itask=1 The end of an internal iteration
!    - itask=2 The end of an outer iteration
!    - itask=3 The end of a time step
! USES
!    cdr_cvgsca
!    cdr_cvgvec
! USED BY
!    cdr_endite
!    cdr_endste
!    cdr_linsea
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  implicit none
  integer(ip)        :: itask
  integer(ip), save  :: ipass=0
  integer(ip)        :: kvcvg,nfout,ipoin,ixcdr,idofn,i
  real(rp)           :: rnume(7),rdeno(7),ricdr(7),rmaxi(7)
  character(len=400) :: formt
  character(len=200) :: form1
  character(len=150) :: form2
!
! Initializations.
!
  rnume = 0.0_rp
  rdeno = 0.0_rp
  ricdr = 0.0_rp
  rmaxi = 0.0_rp
!
! Check convergence of the inner iterations, always in the L2 norm:
! ||U(n,i,j) - U(n,i,j-1)|| / ||U(n,i,j)||.
!
  if(itask==1) then

     select case(kfl_modul(modul))
        case(one)
           kvcvg=0
           form1='$        Time     Global      Inner       Current     Unkno_cdr      Residuals,  &
                  &Norms and Maximum for each field -->'
           form2='$        step  iteration  iteration          time      residual'
        case(two)
           kvcvg=1
           form1='$        Time     Global      Inner       Current     Unkno_cdr      Velocity    &
                 &  Pressure      Velocity      Pressure  Maximum for each field -->'
           form2='$        step  iteration  iteration          time      residual      residual    &
                 &  residual          norm          norm'
        case(three)
           kvcvg=1
           form1='$        Time     Global      Inner       Current     Unkno_cdr    Deflection    &
                 &  Rotation    Deflection      Rotation  Maximum for each field -->'
           form2='$        step  iteration  iteration          time      residual      residual    &
                 &  residual          norm          norm'
        case(four)
           kvcvg=0
           form1='$        Time     Global      Inner       Current     Unkno_cdr      Residuals,  &
                 &Norms and Maximum for each field -->'
           form2='$        step  iteration  iteration          time      residual'
        case(five)
           kvcvg=0
           form1='$        Time     Global      Inner       Current     Unkno_cdr      Residuals,  &
                 &Norms and Maximum for each field -->'
           form2='$        step  iteration  iteration          time      residual'
        case(six)
           kvcvg=0
           form1='$        Time     Global      Inner       Current     Unkno_cdr      Residuals,  &
                 &Norms and Maximum for each field -->'
           form2='$        step  iteration  iteration          time      residual'
        case(seven)
           kvcvg=1
           form1='$        Time     Global      Inner       Current     Unkno_cdr      Velocity    &
                 &  Pressure   Temperature      Velocity      Pressure   Temperature  Maximum for each field -->'
           form2='$        step  iteration  iteration          time      residual      residual    &
                 &  residual      residual          norm          norm          norm'
        case(eight)
           kvcvg=1
           form1='$        Time     Global      Inner       Current     Unkno_cdr      Velocity    &
                 &  Pressure   Temperature      Velocity      Pressure   Temperature  Maximum for each field -->'
           form2='$        step  iteration  iteration          time      residual      residual    &
                 &  residual      residual          norm          norm          norm'
     end select

     if(kvcvg==1) then
        call cdr_cvgvec(ndofn_cdr,ndime,npoin,nunkn_cdr,zero_rp,  &
                        unkno,uncdr,rnume,rdeno,ricdr,rmaxi)
        nfout=2+ndofn_cdr-ndime
     elseif(kvcvg==0) then
        call cdr_cvgsca(ndofn_cdr,npoin,nunkn_cdr,zero_rp,  &
                        unkno,uncdr,rnume,rdeno,ricdr,rmaxi)
        nfout=1+ndofn_cdr
     end if
     if((ricdr(1)<cotol_cdr).or.(itinn(modul)>=miinn_cdr)) kfl_goite_cdr = 0
     if(ipass==0.and.kfl_rstar/=2) then
        ipass=1
        write(lun_conve_cdr,'(a)') adjustl(trim(form1))
        write(lun_conve_cdr,'(a)') adjustl(trim(form2))
     end if
     write(lun_conve_cdr,201) ittim,itcou,itinn(modul),cutim, &
                              (ricdr(i),i=1,nfout), &
                              (rdeno(i)/real(npoin,rp),i=2,nfout), &
                              (rmaxi(i),i=1,ndofn_cdr)
     call flush(lun_conve_cdr)
!
! Check convergence of the outer iterations in the norm selected by the user:
! ||U(n,i,*) - U(n,i-1,*)|| / ||U(n,i,*)||.
!
  else if(itask==2) then
     resid_cdr = 0.0_rp
     select case(kfl_normc_cdr)
     case(0)       
        rnume(1)=maxval(abs(uncdr(:,:,1)-uncdr(:,:,2)) )
        rdeno(1)=maxval(abs(uncdr(:,:,1)) )
        if(rdeno(1)>zero_rp) resid_cdr = 100.0_rp*rnume(1)/rdeno(1)
     case(1)
        rnume(1)=sum(abs(uncdr(:,:,1)-uncdr(:,:,2)))
        rdeno(1)=sum(abs(uncdr(:,:,1)))
        if(rdeno(1)>zero_rp) resid_cdr = 100.0_rp*rnume(1)/rdeno(1)
     case(2)
        rnume(1)=dot_product(reshape((uncdr(:,:,1)-uncdr(:,:,2)),(/nunkn_cdr/)), &
                             reshape((uncdr(:,:,1)-uncdr(:,:,2)),(/nunkn_cdr/)) )
        rdeno(1)=dot_product(reshape( uncdr(:,:,1),(/nunkn_cdr/)), &
                             reshape( uncdr(:,:,1),(/nunkn_cdr/)) )
        if(rdeno(1)>zero_rp) resid_cdr = 100.0_rp*sqrt(rnume(1)/rdeno(1))
     end select
!
! Check residual of the time evolution, always in the L2 norm:
! ||U(n,*,*) - U(n-1,*,*)|| / ||U(n,*,*)||.
!
  else if(itask==3) then
     rnume(1)=dot_product(reshape((uncdr(:,:,1)-uncdr(:,:,3)),(/nunkn_cdr/)), &
                          reshape((uncdr(:,:,1)-uncdr(:,:,3)),(/nunkn_cdr/)) )
     rdeno(1)=dot_product(reshape( uncdr(:,:,1),(/nunkn_cdr/)), &
                          reshape( uncdr(:,:,1),(/nunkn_cdr/)) )
     if(rdeno(1).gt.zero_rp) ricdr(1) = 100.0_rp*sqrt(rnume(1)/rdeno(1))*max(timef,dtime)/dtime
     if(ricdr(1).le.sstol_cdr) then
        kfl_stead_cdr = 1
        write(lun_conve_cdr,301) ittim
     end if
  end if
!
! Formats.
!

201 format(4x,i9,2x,i9,2x,i9,20(2x,e12.6))

301 format(/,5x,'>>>  CDR UNKNOWN IS STATIONARY AT TIME STEP ',i5)

end subroutine cdr_cvgunk
