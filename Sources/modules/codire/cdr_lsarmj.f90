subroutine cdr_lsarmj(kpite,glin0,glin1,dglin,glins,slins)
!-----------------------------------------------------------------------
!****f* Codire/cdr_lsarmj
! NAME 
!    cdr_lsarmj
! DESCRIPTION
!    This routine performs a line search and backtracking using the Armijo
!    rule. The target function is given by
!
!    g(s) = 0.5 * F( u_i + s d ) * F( u_i + s d )
!
!    The values are stored as:
!    glin0    = g(0)      flin0 = f(0)
!    glin1    = g(1)      flin1 = f(1)
!    glins(3) = g(s)
!    slins(3) = s
!    glins(1) = g(s1)
!    glins(2) = g(s2)
!    slins(1) = s1
!    slins(2) = s2
!
!    See numerical recipies in fortran cap 9.
! USES
! USED BY
!    cdr_linsea
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  implicit none
  real(rp)   , intent(in)  :: glin0,glin1,dglin
  real(rp)   , intent(out) :: glins(10),slins(10)
  integer(ip), intent(out) :: kpite
!  real(rp)   , parameter   :: toler=1.0e-7_rp
  real(rp)                 :: slmin,stest,stemp
  real(rp)                 :: rhsg1,rhsg2,amodg,bmodg,sdisc
  integer(ip)              :: iitls,kfoun,idofn,ipoin

     if(glin1 <= glin0 + parls_cdr * dglin ) then
        ! Accepted step
        slins(3) = 1.0_rp
        write(lun_linse_cdr,105) itinn(modul),1.0,glin0,glin1,glin1
     else

        ! First backtrack: quadratic model
        slins(3) = - 0.5_rp * dglin / (glin1 - glin0 - dglin )
        if(slins(3) < 0.1_rp ) slins(3) = 0.1_rp

        uncdr(:,:,1) = geten(:,:,1) + slins(3) * geten(:,:,2)
        if(kfl_modul(modul)==8) call cdr_updtpr(one)
        call cdr_lstarg
        glins(3)=0.5_rp*dot_product(rhsid,rhsid)

        if(glins(3) <= glin0 + slins(3) * parls_cdr * dglin ) then
           ! Step found
           write(lun_linse_cdr,125) itinn(modul),slins(3),glin0,glin1,glins(3)
        else

           ! Compute minimum step
           stest=0.0_rp
           do ipoin=1,npoin
              do idofn=1,ndofn_cdr
                 stemp=abs(geten(idofn,ipoin,2)) / max(abs(geten(idofn,ipoin,1)),1.0_rp)
                 stest=max(stemp,stest)
              end do
           end do
           slmin=tolls_cdr/stest

           ! Backtrack until an acceptable step is found
           slins(2) = 1.0_rp
           glins(2) = glin1
           slins(1) = slins(3)
           glins(1) = glins(3)
           iitls=1
           kfoun=0
           do while( iitls < mitls_cdr .and. kfoun==0 )

              ! compute the step using a cubic model
              rhsg1 = glins(1) - dglin * slins(1) - glin0
              rhsg2 = glins(2) - dglin * slins(2) - glin0

              amodg = (   rhsg1/slins(1)**2 - rhsg2/slins(2)**2 )  &
                    / ( slins(1) - slins(2) )
              bmodg = ( - rhsg1*slins(2)/slins(1)**2 + rhsg2* slins(1) / slins(2)**2 ) &
                    / ( slins(1) - slins(2) )

              if( abs(amodg) <= zero_rp ) then
                 slins(3) = - 0.5_rp * dglin / bmodg
              else
                 sdisc = bmodg**2 - 3.0_rp * amodg * dglin
                 if( sdisc <= 0 ) then
                    slins(3) = 0.5_rp * slins(1)
                 else
                    slins(3) = ( - bmodg + sqrt(sdisc) ) / ( 3.0_rp * amodg )
                 end if
              end if

              ! check the step size
              if(slins(3) > 0.5_rp * slins(1) ) then
                 slins(3) = 0.5_rp * slins(1)
              else if(slins(3) < slmin ) then
                 kfoun=1
              else if(slins(3) < 0.1_rp * slins(1) ) then
                 slins(3) = 0.1_rp * slins(1)
              end if

              ! Evaluate the target function
              uncdr(:,:,1) = geten(:,:,1) + slins(3) * geten(:,:,2)
              if(kfl_modul(modul)==8) call cdr_updtpr(one)
              call cdr_lstarg
              glins(3)=0.5_rp*dot_product(rhsid,rhsid)

              ! Accept the step or continue
              if(glins(3) <= glin0 + slins(3) * parls_cdr * dglin ) then
                 kfoun=2
              else
                 iitls = iitls +1 
                 slins(2) = slins(1)
                 glins(2) = glins(1)
                 slins(1) = slins(3)
                 glins(1) = glins(3)
              end if
           end do

           if(kfoun==0) then      ! maximum number of iterations reached: Try Picard
              write(lun_linse_cdr,115) itinn(modul),slins(3),glin0,glin1,glins(3)
              slins(3)=0.0_rp
              kpite=1
           else if(kfoun==1) then ! the step is smaller than the minimum: Try Picard
              write(lun_linse_cdr,115) itinn(modul),slins(3),glin0,glin1,glins(3)
              slins(3)=0.0_rp
              kpite=1
           else if(kfoun==2) then ! the step has been accepted
              write(lun_linse_cdr,125) itinn(modul),slins(3),glin0,glin1,glins(3)
           end if

        end if
     end if


105 format(10x,i15,4x,4(e13.6,4x),'Accepted step')
110 format(10x,i15,4x,4(e13.6,4x),'Minimum not found. Full step')
115 format(10x,i15,4x,4(e13.6,4x),'Minimum not found. Trying Picard')
125 format(10x,i15,4x,4(e13.6,4x),'Step found')


end subroutine cdr_lsarmj
