subroutine cdr_shflow(uncdr,smflo,kfl_modul)
!-----------------------------------------------------------------------
!****f* Codire/cdr_shflow
! NAME 
!    cdr_shflow
! DESCRIPTION
!    This subroutine computes heat flow through the domain boundary:
! USED BY
!    cdr_initpr
!    cdr_updtpr
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_codire
  use def_cdrloc
  implicit none
  integer(ip), intent(in)  :: kfl_modul
  real(rp),    intent(in)  :: uncdr(ndofn_cdr,npoin)
  real(rp),    intent(out) :: smflo
  real(rp)    :: cartb(ndime,mnode)
  real(rp)    :: shapp(mnode)
  real(rp)    :: hflgp
  integer(ip) :: ielem,pelty,pmate,pnode,igaus,inode
  integer(ip) :: iboun,pblty,pnodb,pgaub,inodb,igaub
  integer(ip) :: ipoin,idime,jdime,idofn
!
! Allocate local variables
!
  call cdr_membou(one)
  call cdr_memelm(one)
!
! Initializations
!
  smflo=0.0_rp

  boundaries: do iboun=1,nboun

     pblty=ltypb(iboun)
     pnodb=nnode(pblty)
     ielem=lboel(pnodb+1,iboun)
     pelty=ltype(ielem)
     pnode=nnode(pelty)

     do inodb=1,pnodb                                      ! Gather operations
        ipoin=lnodb(inodb,iboun)
        bounk(:,inodb) = uncdr(1:ndofn_cdr,ipoin)
        bocod(:,inodb) = coord(1:ndime,ipoin)
     end do
     do inode=1,pnode
        ipoin=lnods(inode,ielem)
        elunk(:,inode,1) = uncdr(:,ipoin)
        elcod(:,inode)   = coord(1:ndime,ipoin)
     end do

     gauss: do igaub=1,ngaus(pblty)

        ! Jacobian
        call bouder(pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&
                    bocod,baloc,eucta)
        dsurf=elmar(pblty)%weigp(igaub)*eucta 
        call chenor(pnode,baloc,bocod,elcod)                          ! Check normal

        do inode=1,pnode                                              ! Calculate carte-
           do idime=1,ndime                                           ! sian derivates
              cartb(idime,inode)=0.0_rp                               ! at boundary 
              do inodb=1,pnodb                                        ! gauss points
                 do igaus=1,ngaus(pelty)
                    call elmder(&
                         pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&  ! Cartesian derivative
                         elcod,cartd,detjm,xjacm,xjaci)               ! and Jacobian
                    cartb(idime,inode) = cartb(idime,inode)&
                                       + elmar(pelty)%shaga(igaus,lboel(inodb,iboun))&
                                       * elmar(pblty)%shape(inodb,igaub)*cartd(idime,inode)
                 end do
              end do
           end do
        end do
        shapp=0.0_rp                                                    ! Shape at boundary
        do inodb=1,pnodb                                                ! gauss points
           shapp(lboel(inodb,iboun))=elmar(pblty)%shape(inodb,igaub)
        end do

        ! Evaluate matrices
        call cdr_defmat(kfl_modul,kfl_syseq_cdr,kfl_fdiff_cdr,       &
                        lawvi_cdr,ncomp_cdr,ndofn_cdr,pnode,mnode,   &
                        ndime,phypa_cdr,visco_cdr,thpre_cdr,         &
                        welin_cdr,staco_cdr,hleng,shapp,             &
                        cartb,elcod,elunk,masma_cdr,dires_cdr,       &
                        cores_cdr,flres_cdr,sores_cdr,dites_cdr,     &
                        cotes_cdr,sotes_cdr,tauma_cdr,force_cdr,grunk)

        ! heat flux at gp
        do idofn=1,ndofn_cdr
           do idime=1,ndime
              do jdime=1,ndime
                 hflgp = hflgp &
                       + baloc(idime,ndime) * dires_cdr(idofn,ndofn_cdr,idime,jdime) * grunk(jdime,ndofn_cdr)
                 end do
              end do
        end do

        smflo = smflo + dsurf*hflgp

     end do gauss

  end do boundaries
!
! Deallocate local variables
!
  call cdr_membou(two)
  call cdr_memelm(two)

end subroutine cdr_shflow
