subroutine cdr_denvel(ndime,ndofn,npoin,thpre,unkno,uncdr)
!
! Evaluates rho u to calculate a stream function
!
  use def_kintyp
  implicit none
  integer(ip), intent(in)  :: ndime,ndofn,npoin
  real(rp)   , intent(in)  :: thpre,uncdr(ndofn,npoin)
  real(rp)   , intent(out) :: unkno(ndime,npoin)
  integer(ip)              :: ipoin,idofn

  if(thpre>1.0e-8) then
     do ipoin=1,npoin
        do idofn=1,ndime
           unkno(idofn,ipoin) = uncdr(idofn,ipoin) *  thpre / uncdr(ndofn,ipoin)
        end do
     end do
  else
     do ipoin=1,npoin
        do idofn=1,ndime
           unkno(idofn,ipoin) = uncdr(idofn,ipoin)
        end do
     end do
  end if

end subroutine cdr_denvel
