subroutine cdr_restar(itask)
!------------------------------------------------------------------------
!****f* Codire/cdr_restar
! NAME 
!    cdr_restar
! DESCRIPTION
!    This routine
!
!    itask=1 reads the initial values from the restart file
!
!    itask=2 writes the restart file.
!
! USES
! USED BY
!    nsi_turnon
!***
!------------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_codire
  use      mod_iofile
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: idofn,ipoin

  select case (itask)
 
  case (1)
     if(kfl_rstar>=1) then
        call iofile(zero,lun_rstar_cdr,fil_rstar_cdr,'RESTART','old','unformatted') 
        read(lun_rstar_cdr) &
             ((uncdr(idofn,ipoin,ncomp_cdr),idofn=1,ndofn_cdr),ipoin=1,npoin)
        call iofile(two,lun_rstar_cdr,fil_rstar_cdr,'RESTART') 
        ! Update uncdr in order to compute dtinv_nsi
        call cdr_updunk(six)
        !!!!!! Check restart for Low Mach!!!!!
        !if(kfl_modul(modul)==8) call cdr_updtpr(two)
     end if
  case (2)
     if(kfl_preli==1.and.(mod(ittim,nprit)==0.or.cutim>=timef.or.ittim>=mitim)) then
        call iofile(zero,lun_rstar_cdr,fil_rstar_cdr,'RESTART','unknown','unformatted')        
        write(lun_rstar_cdr) &
             ((uncdr(idofn,ipoin,1),idofn=1,ndofn_cdr),ipoin=1,npoin)                            
        call iofile(two,lun_rstar_cdr,fil_rstar_cdr,'RESTAR') 
     end if
  end select

end subroutine cdr_restar
