subroutine cdr_vinvte(ndofn,uncdr,vinvt)
!-----------------------------------------------------------------------
!****f* Codire/cdr_vinvte
! NAME 
!    cdr_vinvte
! DESCRIPTION
!    This subroutine integrates the inverse of the temperature on the
!    domain
! USES
! USED BY
!    cdr_initpr
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_cdrloc
  implicit none
  integer(ip), intent(in)  :: ndofn
  real(rp),    intent(in)  :: uncdr(ndofn,npoin)
  real(rp),    intent(out) :: vinvt
  real(rp)     :: tempg
  integer(ip)  :: ielem,igaus,inode,pelty,pnode,ipoin
!
! Allocate local variables
!
  call cdr_memelm(one)
!
! Initializations
!
  vinvt=0.0_rp
!
! Loop on elements
!
  elements: do ielem=1,nelem

     pelty=ltype(ielem)
     pnode=nnode(pelty)

     do inode=1,pnode
        ipoin=lnods(inode,ielem)
        elunk(1:ndofn,inode,1) = uncdr(1:ndofn,ipoin)
        elcod(1:ndime,inode)   = coord(1:ndime,ipoin)
     end do

     gauss_points: do igaus=1,ngaus(pelty)
        call elmder(&
             pnode,ndime,elmar(pelty)%deriv(1,1,igaus),    &
             elcod,cartd,detjm,xjacm,xjaci)
        dvolu=elmar(pelty)%weigp(igaus)*detjm

        tempg = 0.0_rp
        do inode=1,pnode
           tempg=tempg+elmar(pelty)%shape(inode,igaus)*elunk(ndofn,inode,1)
        end do

        vinvt = vinvt +  dvolu/tempg

     end do gauss_points

  end do elements
!
! Deallocate local variables
!
  call cdr_memelm(two)

end subroutine cdr_vinvte
