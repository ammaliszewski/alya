module def_codire
!-----------------------------------------------------------------------
!
! Heading for the CDR routines
!
!-----------------------------------------------------------------------
  use def_kintyp
!
! Logical units
!
  integer(ip), parameter :: &
     lun_pdata_cdr = 301, lun_outpu_cdr = 302, lun_conve_cdr = 303,&
     lun_solve_cdr = 304, lun_rstar_cdr = 305, lun_thpre_cdr = 308,&
     lun_linse_cdr = 309,&
     lun_sectx_cdr = 311, lun_secty_cdr = 312, lun_sectz_cdr = 313,&
     lun_trap1_cdr = 321, lun_trap2_cdr = 322, lun_trap3_cdr = 323,&
     lun_trap4_cdr = 324, lun_trap5_cdr = 325, lun_fluxe_cdr = 330,&
     lun_force_cdr = 331, lun_splot_cdr = 332
!
! File names
!
  character(150) :: fil_linse_cdr,fil_rstar_cdr
  character(150) :: fil_sectx_cdr,fil_secty_cdr,fil_sectz_cdr
  character(150) :: fil_splot_cdr,fil_fluxe_cdr,fil_force_cdr
  character(150) :: fil_trap1_cdr,fil_trap2_cdr,fil_trap3_cdr
  character(150) :: fil_trap4_cdr,fil_trap5_cdr
!
! General data
!
  integer(ip) :: &
     ndofn_cdr,&                  ! # of d.o.f. of the CDR unknown
     ndof2_cdr,&                  ! ndofn_cdr*ndofn_cdr
     ncomp_cdr                    ! Number of components of the CDR unknown
!--BEGIN REA GROUP
!
! Physical problem
!
  integer(ip) :: &
     kfl_timei_cdr,&              ! Existence of dU/dt
     kfl_fdiff_cdr,&              ! Form of diffusive term in Nvs, Bou and Lom
     kfl_syseq_cdr,&              ! Equation system
     kfl_exacs_cdr,&              ! Exact solution
     nunkn_cdr                    ! # of unknonws ndofn*npoin

  real(rp) :: &
     gnorm_cdr,&                  ! |g|
     grdir_cdr( 3),&              ! g versor
     grvec_cdr( 3),&              ! g vector
     vinvt_cdr( 3),&              ! Inverse of temeperature integral       (used for Low Mach)
     v_dom_cdr( 3),&              ! Domain size                            (used for Low Mach)
     s_vfl_cdr( 3),&              ! Boundary volume flow                   (used for Low Mach)
     s_hfl_cdr( 3),&              ! Boundary heat flow                     (used for Low Mach)
     sitfl_cdr( 3),&              ! Boundary inverse of temperature flow   (used for Low Mach)
     thpre_cdr( 3),&              ! Thermodynamic pressure                 (used for Low Mach)
     dtpdt_cdr,&                  ! Thermodynamic pressure time derivative (used for Low Mach)
     other_cdr(10)                ! Other parameters
!
! Physical properties
!
  integer(ip) :: &
     lawvi_cdr                    ! Viscosity law (nvs, bou, lom)

  real(rp) ::&
     phypa_cdr(20),&              ! Physical parameters (problem dependent)
     visco_cdr(10)                ! Viscosity parameters (nvs, bou, lom)

  real(rp), allocatable ::&
     masma_cdr(:,:),&             ! Coefficients of the time derivative
     dires_cdr(:,:,:,:),&         ! Residual diffusion  matrix
     cores_cdr(:,:,:),&           ! Residual convection matrix (not integrated by parts)
     flres_cdr(:,:,:),&           ! Residual convection matrix (    integrated by parts)
     sores_cdr(:,:),&             ! Residual reaction   matrix
     cotes_cdr(:,:,:),&           ! Test     convection matrix
     dites_cdr(:,:,:,:),&         ! Test     diffusion  matrix
     sotes_cdr(:,:),&             ! Test     reaction   matrix
     tauma_cdr(:,:),&             ! Tau matrix
     force_cdr(:),&               ! Force vector
     ditne_cdr(:,:)               ! Dirichlet-to-Neumann matrix
!
! Boundary conditions
!
  integer(ip) :: &
     kfl_conbc_cdr                ! Constant b.c.

  integer(ip), allocatable :: &
     kfl_fixno_cdr(:,:),      &   ! Nodal fixity
     kfl_fixbo_cdr(:,:),      &   ! Element boundary fixity
     kfl_funno_cdr(:),        &   ! Functions for node bc
     kfl_funty_cdr(:,:)           ! Function type and number of paremeters
  real(rp),    allocatable :: &
     bvess_cdr(:,:,:)             ! Essential bc (or initial) values
  type(r1p),   allocatable :: &
     funpa_cdr(:)                 ! Function parameters
  type(r2p),   allocatable :: &
     bvnat_cdr(:,:)               ! Natural bc values
!
! Output
!
  integer(ip) :: &
     kfl_outsp_cdr,&              ! Output of surface plot
     kfl_outfm_cdr,&              ! Output of forces and moments
     kfl_outfl_cdr,&              ! Output of fluxes
     nou_secsi_cdr,&              ! Sections step interval
     nou_unksi_cdr,&              ! Output step interval U
     nou_stlsi_cdr,&              ! Output step interval s.l.
     nou_famsi_cdr,&              ! Output step interval forces and moments.
     npp_unksi_cdr,&              ! Postp. step interval U
     npp_stlsi_cdr,&              ! Postp. step interval s.l.
     lptra_cdr(5),&               ! List of nodal points to be tracked
     nptra_cdr,&                  ! Number of points to be tracked
     eltra(5),&                   ! Elements that host tracked points
     nbody_cdr                    ! # of bodies

  real(rp) ::&
     out_timun_cdr(10),&          ! Output times for U
     out_timsl_cdr(10),&          ! Output times for s.l.
     pos_timun_cdr(10),&          ! Postp. times for U
     pos_timsl_cdr(10),&          ! Postp. times for s.l.
     cptra_cdr( 3, 5),&           ! Coordinates of points to be tracked
     csect_cdr( 3),&              ! Coordinates of the sections to be plotted
     adimf_cdr( 6),&              ! Adimensionalization factor for forces
     adimm_cdr( 3),&              ! Adimensionalization factor for moments
     origm_cdr( 3)                ! Moment origin (application) point

  integer(ip), allocatable :: &
     lbody_cdr(:)                 ! Code for body in forces/moment computation

  real(rp), allocatable :: &
     shatp(:,:),&                 ! Shape functions for tracking points
     unktp(:,:)                   ! Unkno at tracking points

!
! Numerical treatment
!
  integer(ip) :: &
     kfl_weigh_cdr,&              ! Weighting of dU/dt
     kfl_repro_cdr,&              ! Stabilization based on residual projection
     kfl_grast_cdr,&              ! Stabilization based on gradient projection
     kfl_goite_cdr,&              ! Keep iterating
     kfl_stead_cdr,&              ! Steady-state has been reached
     kfl_linea_cdr,&              ! Linear model
     kfl_thpeq_cdr,&              ! Thermodynamic pressure integration scheme (Low Mach)
     kfl_tmass_cdr,&              ! Total mass is constant (0) or not (1) (Low Mach)
     kfl_shcty_cdr,&              ! Shock capturing type
     kfl_tiacc_cdr,&              ! Temporal accuracy
     kfl_schem_cdr,&              ! Temporal integration scheme
     kfl_twost_cdr,&              ! Two step time scheme
     kfl_normc_cdr,&              ! Norm of convergence
     kfl_linme_cdr,&              ! Method of linearization
     kfl_linse_cdr,&              ! Line search
     kfl_algso_cdr,&              ! Type of algebraic solver
     nkryd_cdr,&                  ! # Krylov dimension
     npica_cdr,&                  ! # of Picard iterations
     miinn_cdr,&                  ! Max # of CDR iterations
     mitls_cdr,&                  ! Max # of line search iterations
     mipls_cdr,&                  ! Max # of line search picard iterations
     misol_cdr                    ! Max # of solver iterations

  real(rp) ::&
     welin_cdr(20),&              ! Linearization paramters
     parls_cdr,&                  ! Line search parameter
     tolls_cdr,&                  ! Line search tolerance
     staco_cdr(10),&              ! Stability constants
     dtinv_cdr,&                  ! 1/dt
     dtcri_cdr,&                  ! Critical time step
     theta_cdr(3),&               ! Time integration parameters
     shcva_cdr,&                  ! Shock capturing parameter
     sstol_cdr,&                  ! Steady state tolerance
     cotol_cdr,&                  ! Convergence tolerance
     resid_cdr,&                  ! Residual of the CDR problem
     weigh_cdr,&                  ! Weight of dU/dt in the residual
     solco_cdr                    ! Solver tolerance

  integer(ip), allocatable, target ::&
     lpcdr(:)                     ! Skyline system matrix pointers
!--END REA GROUP
  include 'dmumps_struc.h'
  type(dmumps_struc) :: mumps_cdr ! Mumps data structure

end module def_codire
