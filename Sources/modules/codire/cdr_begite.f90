subroutine cdr_begite
!-----------------------------------------------------------------------
!****f* Codire/cdr_begite
! NAME 
!    cdr_begite
! DESCRIPTION
!    This routine starts an internal iteration for the CDR equations.
! USES
!    cdr_inisol
!    cdr_updunk
! USED BY
!    cdr_doiter
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  implicit none
!
! Initializations
!
  if(itcou==1) then
     if(kfl_modul(modul)==8.and.kfl_thpeq_cdr==0) write(lun_thpre_cdr,110) itcou
     if(kfl_modul(modul)==8.and.kfl_thpeq_cdr==1) write(lun_thpre_cdr,120) itcou
     if(kfl_modul(modul)==8.and.kfl_thpeq_cdr==2) write(lun_thpre_cdr,130) itcou
     if(kfl_linse_cdr > zero) write(lun_linse_cdr, 50) itcou
     call cdr_tittim
  end if 
  call livinf(15_ip,' ',modul)
  kfl_goite_cdr = 1
  itinn(modul)     = 0
!
! Set up the solver parameters for the CDR equation
!
  call cdr_inisol  
!
! Obtain the initial guess for inner iterations.
!
  call cdr_updunk(two)
!
! Formats.
!
   50 format(/,10x,'Global iteration number:',i5,/,                          &
        10x,       '------------------------',//,                            &
        10x,       'Inner iteration',10x,'s',14x,                            &
                   'g(0)',13x,'g(1)',13x,'g(s)')

  110 format(/,10x,'Global iteration number:',i5,/,                          &
        10x,       '------------------------',//,                            &
        10x,       'Inner iteration',6x,'Boundary vol. flow',9x,             &
                   'Boundary mass flow',5x,'Thermodynamic pressure',5x,      &
                   'Th_p time derivative')

  120 format(/,10x,'Global iteration number:',i5,/,                          &
        10x,       '------------------------',//,                            &
        10x,       'Inner iteration',6x,'Integral of inv(T)',9x,             &
                   'Boundary mass flow',5x,'Thermodynamic pressure',5x,      &
                   'Th_p time derivative')

  130 format(/,10x,'Global iteration number:',i5,/,                          &
        10x,       '------------------------',//,                            &
        10x,       'Inner iteration',6x,'Integral of div(u)',9x,             &
                   'Boundary heat flow',5x,'Thermodynamic pressure',5x,      &
                   'Th_p time derivative')

end subroutine cdr_begite

