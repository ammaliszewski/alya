subroutine cdr_smflow(kflow,ndofn,uncdr,smflo)
!-----------------------------------------------------------------------
!****f* Codire/cdr_smflow
! NAME 
!    cdr_smflow
! DESCRIPTION
!    This subroutine computes flows through the domain boundary:
!    - kflow = 0  int_bou 1/T n.u
!    - kflow = 1  int_bou n.u
! USED BY
!    cdr_initpr
!    cdr_updtpr
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_cdrloc
  implicit none
  integer(ip), intent(in)  :: kflow,ndofn
  real(rp),    intent(in)  :: uncdr(ndofn,npoin)
  real(rp),    intent(out) :: smflo
  real(rp)    :: velog(3),venor,tempg
  integer(ip) :: ielem,pelty,pmate,pnode,igaus,inode,idime,ipoin
  integer(ip) :: iboun,pblty,pnodb,pgaub,inodb,igaub
!
! Allocate local variables
!
  call cdr_membou(one)
!
! Initializations
!
!  smflo=0.0_rp

  boundaries: do iboun=1,nboun

     pblty=ltypb(iboun)
     pnodb=nnode(pblty)
     ielem=lboel(pnodb+1,iboun)
     pelty=ltype(ielem)
     pnode=nnode(pelty)

     do inodb=1,pnodb                                      ! Gather operations
        ipoin=lnodb(inodb,iboun)
        bounk(:,inodb) = uncdr(1:ndofn,ipoin)
        bocod(:,inodb) = coord(1:ndime,ipoin)
     end do
     do inode=1,pnode
        ipoin=lnods(inode,ielem)
        elcod(:,inode) = coord(1:ndime,ipoin)
     end do

     gauss: do igaub=1,ngaus(pblty)

        ! Jacobian
        call bouder(&
             pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&
             bocod,baloc,eucta)
        dsurf=elmar(pblty)%weigp(igaub)*eucta 
        call chenor(pnode,baloc,bocod,elcod)      ! Check normal

        do idime=1,ndime                                   ! Tang. velocity u
           velog(idime)=dot_product(elmar(pblty)%shape(1:pnodb,igaub),bounk(idime,1:pnodb))
        end do
        venor=dot_product(baloc(1:ndime,ndime),velog(1:ndime))

        if(kflow==0) then
          tempg=dot_product(elmar(pblty)%shape(1:pnodb,igaub),bounk(ndofn,1:pnodb))
          smflo = smflo + dsurf*venor/tempg
        else
          smflo = smflo + dsurf*venor
        end if

     end do gauss

  end do boundaries
!
! Deallocate local variables
!
  call cdr_membou(two)

end subroutine cdr_smflow
