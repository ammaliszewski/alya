subroutine cdr_endite(itask)
!-----------------------------------------------------------------------
!****f* Codire/cdr_endite
! NAME 
!    cdr_endite
! DESCRIPTION
!    This routine checks convergence and updates the unknown of the CDR
!    equations at:
!    - itask=1 The end of an internal iteration
!    - itask=2 The end of the internal loop iteration
! USES
!    cdr_cvgunk
!    cdr_updunk
!    cdr_updtpr to update thermodynamic pressure (low Mach case only)
! USED BY
!    cdr_doiter during the inner loop (itask=1)
!    cdr_doiter at the end of the inner loop (itask=2)
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  implicit none
  integer(ip) :: itask

  select case(itask)

  case(one)

  !  Compute convergence residual of the internal iteration (that is,
  !  || U(n,i,j) - U(n,i,j-1)|| / ||U(n,i,j)||) and update unknowns:
  !  U(n,i,j-1) <-- U(n,i,j).

     if( kfl_linse_cdr > zero .and. itinn(modul)>npica_cdr) then
     !  Perform a line search
        call cdr_linsea
     else
        call cdr_cvgunk(  one)
        call cdr_updunk(three)
     !  Update the thermodynamic pressure in Low Mach number flows
        if(kfl_modul(modul)==8) call cdr_updtpr(two)
     end if

  case(two)

  !  Compute convergence residual of the external iteration (that is,
  !  || U(n,i,*) - U(n,i-1,*)|| / ||U(n,i,*)||) and update unknowns:
  !  U(n,i-1,*) <-- U(n,i,*).
     call livinf(16_ip,' ',itinn(modul))
     call cdr_cvgunk(two)
     call cdr_updunk(four)

  end select

end subroutine cdr_endite
