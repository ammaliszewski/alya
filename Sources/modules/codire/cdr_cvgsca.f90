subroutine cdr_cvgsca(ndofn,npoin,nunkn,zero_rp, &
                      unkno,uncdr,rnume,rdeno,ricdr,rmaxi)
!-----------------------------------------------------------------------
!****f* Codire/cdr_cvgsca
! NAME 
!    cdr_cvgsca
! DESCRIPTION
!    This routine checks convergence of the CDR equations when the system
!    is comes from the coupling of scalar fields.
! USES
! USED BY
!    cdr_cvgunk
!***
!-----------------------------------------------------------------------
use def_kintyp
implicit none
integer(ip), intent(in)   :: ndofn,npoin,nunkn
real(rp)   , intent(in)   :: zero_rp,unkno(nunkn),uncdr(ndofn,npoin)
real(rp)   , intent(out)  :: rnume(7),rdeno(7),ricdr(7),rmaxi(6)

integer(ip)               :: idofn,ipoin,ixcdr,i

  rnume=0.0_rp
  rdeno=0.0_rp
  ricdr=0.0_rp
  rmaxi=0.0_rp

  do ipoin=1,npoin
     ixcdr = (ipoin-1)*ndofn
     do idofn=1,ndofn
        ixcdr = ixcdr + 1
        rnume(1) = rnume(1) + (unkno(ixcdr)-uncdr(idofn,ipoin))* &
                              (unkno(ixcdr)-uncdr(idofn,ipoin))
        rdeno(1) = rdeno(1) +  unkno(ixcdr)*unkno(ixcdr)
        rnume(1+idofn) = rnume(1+idofn) + (unkno(ixcdr)-uncdr(idofn,ipoin))* &
                                          (unkno(ixcdr)-uncdr(idofn,ipoin))
        rdeno(1+idofn) = rdeno(1+idofn) +  unkno(ixcdr)*unkno(ixcdr)
        rmaxi(idofn) = max(unkno(ixcdr),rmaxi(idofn))
     end do
  end do
  do i=1,1+ndofn
     if(rdeno(i) > zero_rp) ricdr(i) = 100.0_rp*sqrt(rnume(i)/rdeno(i))
  end do

end subroutine cdr_cvgsca
