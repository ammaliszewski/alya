subroutine cdr_output(itask)
!-----------------------------------------------------------------------
!****f* Codire/cdr_output
! NAME 
!    cdr_output
! DESCRIPTION
!    The output is performed when:
!
!    itask = 0  timemarching is true. There is output or post-process
!               of results if required.
!    itask = 1  timemarching is false. Output and/or post-process of
!               results is forced if they have not been written
!               previously.
! USES
!    tem_cvgunk
!    tem_updunk
!    tem_output
! USED BY
!    cdr_endste
!    cdr_turnof
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  use mod_postpr
  use mod_iofile
  implicit none
  integer(ip)    :: itask,i,ntrac
!
! 1) Tracking of points.
!
  if(nptra_cdr > 0 .or. lptra_cdr(1)>0) then
     if(itask==0) call cdr_outtpo
  end if
!
! 2) Plotting of sections.
!
  if(itask==0) then
     if(mod(ittim,nou_secsi_cdr)==0) &
     call cdr_outsec
  else if(itask==1) then
     if((nou_secsi_cdr<1d9).and.(mod(ittim,nou_secsi_cdr)/=0)) &
     call cdr_outsec
  end if
!
! 3) Computation of forces and moments.
!
  if(kfl_outfm_cdr==1) then
     if(itask==0) then
        if(mod(ittim,nou_famsi_cdr)==0) call cdr_forces
     else if(itask==1) then
        if((nou_famsi_cdr<1d9).and.(mod(ittim,nou_famsi_cdr)/=0)) call cdr_forces
     end if
  end if
!
! 4) Output of surface plot.
!
  if(itask==0) then
     if(mod(ittim,nou_unksi_cdr)==0) then                     ! At a given time step.
        call wcdrau('UNKNO',1,cutim)
        if(kfl_outsp_cdr==1) then
           call iofile(zero,lun_splot_cdr,fil_splot_cdr,'CDR SURFACE PLOT')
           call suplot(ndofn_cdr,unkno,lun_splot_cdr)
           call iofile(one ,lun_splot_cdr,fil_splot_cdr,'CDR SURFACE PLOT')
        end if
     end if
     do i=1,10                                            ! At a given time.
        if(abs(out_timun_cdr(i)-cutim)<(0.5*dtime)) then
           call wcdrau('UNKNO',1,cutim)
           if(kfl_outsp_cdr==1) then
              call iofile(zero,lun_splot_cdr,fil_splot_cdr,'CDR SURFACE PLOT')
              call suplot(ndofn_cdr,unkno,lun_splot_cdr)
              call iofile(one ,lun_splot_cdr,fil_splot_cdr,'CDR SURFACE PLOT')
           end if
        end if
     end do
  else if(itask==1) then                            ! At the end of the analysis.
     if((nou_unksi_cdr < 1e9     ).and. &
        (ittim < nou_unksi_cdr   ).and. &
        (cutim < out_timun_cdr(1)) ) then
        call wcdrau('UNKNO',1,cutim)
        if(kfl_outsp_cdr==1) then
           call iofile(zero,lun_splot_cdr,fil_splot_cdr,'CDR SURFACE PLOT')
           call suplot(ndofn_cdr,unkno,lun_splot_cdr)
           call iofile(one ,lun_splot_cdr,fil_splot_cdr,'CDR SURFACE PLOT')
        end if
     end if
  end if
!
! 5) Post-process of unknowns.
!
  if(itask==0) then                                 ! At a given time step
     if(mod(ittim,npp_unksi_cdr)==0) then               ! unkno
        call wcdrau('UNKNO',2,cutim)
     end if
     if(mod(ittim,npp_stlsi_cdr)==0.and. &              ! Stream lines (for NVS BOU LOMA)
        ndime==2.and. &
        (kfl_modul(modul)==2.or.kfl_modul(modul)==7.or.kfl_modul(modul)==8)) then
        if(kfl_modul(modul)/=8) thpre_cdr=0.0_rp
           call cdr_denvel(ndime,ndofn_cdr,npoin,thpre_cdr(1),unkno,uncdr)
           call memgen(zero,npoin,zero)
           call strfun(unkno)
           !call postpr(gesca,'STREA',ittim,cutim)
           call memgen(two,npoin,zero)
        end if
     do i=1,10                                         ! At a given time
        if(abs(pos_timun_cdr(i)-cutim)<(0.5*dtime)) then
           call wcdrau('UNKNO',2,cutim)
        end if
        if(abs(pos_timsl_cdr(i)-cutim)<(0.5*dtime).and.&
           ndime==2.and. &
           (kfl_modul(modul)==2.or.kfl_modul(modul)==7.or.kfl_modul(modul)==8)) then
           if(kfl_modul(modul)/=8) thpre_cdr=0.0_rp
           call cdr_denvel(ndime,ndofn_cdr,npoin,thpre_cdr(1),unkno,uncdr)
           call memgen(zero,npoin,zero)
           call strfun(unkno)
           !call postpr(gesca,'STREA',ittim,cutim)
           call memgen(two,npoin,zero)
        end if
     end do
  else if(itask==1) then                            ! At the end of the analysis.
     if((npp_unksi_cdr<1e9     ).and. &
        (mod(ittim,npp_unksi_cdr)/=0) ) then
        call wcdrau('UNKNO',2,cutim)
     end if
     if(npp_stlsi_cdr<1e9.and. &
        mod(ittim,npp_stlsi_cdr)/=0.and. &
        ndime==2.and. &
        (kfl_modul(modul)==2.or.kfl_modul(modul)==7.or.kfl_modul(modul)==8)) then
        if(kfl_modul(modul)/=8) thpre_cdr=0.0_rp
        call cdr_denvel(ndime,ndofn_cdr,npoin,thpre_cdr(1),unkno,uncdr)
        call memgen(zero,npoin,zero)
        call strfun(unkno)
        !call postpr(gesca,'STREA',ittim,cutim)
        call memgen(two,npoin,zero)
     end if
  end if
      
end subroutine cdr_output

!
! Auxiliar routine.
!
subroutine wcdrau(wopos,itask,time)
  use def_master
  use def_domain
  use def_codire
  use mod_postpr
!  use mod_output
  implicit none
  integer(ip)  ::  itask
  character(5) ::  wopos
  integer(ip)  ::  idofn,ipoin,idime
  real(rp)     ::  time,phpre,diffu
!
! Post-process.
!
      if(itask==2.and.wopos=='UNKNO') then
        if(kfl_modul(modul)==1) then                                 ! General CDR
           !call postpr(uncdr(1:ndofn_cdr        ,1:npoin,1),'uncdr' ,ittim,cutim)
        else if(kfl_modul(modul)==2) then                            ! NS CDR
           !call postpr(uncdr(1:ndime  ,1:npoin,1),'VELOC_CDR' ,ittim,cutim)
           !call postpr(uncdr(ndofn_cdr,1:npoin,1),'PRESS_CDR' ,ittim,cutim)
        else if(kfl_modul(modul)==3) then                            ! Plate CDR
           !call postpr(uncdr(1:ndime  ,1:npoin,1),'ROTATIONS' ,ittim,cutim)
           !call postpr(uncdr(ndofn_cdr,1:npoin,1),'DEFLECTION',ittim,cutim)
        else if(kfl_modul(modul)==4) then                            ! Helmholtz CDR
           !call postpr(uncdr(1:2      ,1:npoin,1),'Wave components (Re, Im)',ittim,cutim)
        else if(kfl_modul(modul)==5) then                            ! Contaminant CDR
           !call postpr(uncdr(1:ndime  ,1:npoin,1),'Particle Velo',ittim,cutim)
           !call postpr(uncdr(ndofn_cdr,1:npoin,1),'CONCENTRATION',ittim,cutim)
        else if(kfl_modul(modul)==6) then
           !call postpr(uncdr(ndofn_cdr,1:npoin,1),'CONCENTRATION',ittim,cutim)
        else if(kfl_modul(modul)==7) then                            ! Boussinesq CDR
           !call postpr(uncdr(1:ndime  ,1:npoin,1),'VELOC_CDR',ittim,cutim)
           !call postpr(uncdr(ndime+1  ,1:npoin,1),'PRESS_CDR',ittim,cutim)
           !call postpr(uncdr(ndofn_cdr,1:npoin,1),'TEMPE_CDR',ittim,cutim)
        else if(kfl_modul(modul)==8) then                            ! Low Mach CDR
           !call postpr(             uncdr(1:ndime  ,1:npoin,1),'VELOC_CDR',ittim,cutim)
           !call postpr(             uncdr(ndime+1  ,1:npoin,1),'PRESS_CDR',ittim,cutim)
           !call postpr(             uncdr(ndofn_cdr,1:npoin,1),'TEMPE_CDR',ittim,cutim)
           !call postpr(thpre_cdr(1)/uncdr(ndofn_cdr,1:npoin,1),'DENSI_CDR',ittim,cutim)
        end if
      end if

      return

end subroutine wcdrau


!c
!c***  Auxiliar routine to compute diffusivity
!c
!      subroutine visaux(diffu,kvila,visco,grunk,vtemp,
!     .                  ndime,ndofn,npoin,ipoin)
!      implicit none
!      integer ndime,ndofn,npoin,ipoin,kvila
!      real*8 diffu,vtemp,visco(4)
!      real*8 grunk(ndime,ndofn,npoin)
!
!      call vislaw(diffu,1,visco,grunk(1,1,ipoin),vtemp,ndime)
!
!      return
!      end
