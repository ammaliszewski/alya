subroutine cdr_elmmul(ndime,nnode,mnode,ndofn,         &
                  tauma,ptest,resma,resfo,             &
                  wmatr,wrhsi,wprod)
!-----------------------------------------------------------------------
!
! This routine multiplies test functions by residuals
!
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none

  integer(ip), intent(in)    :: ndime,nnode,mnode,ndofn
  real(rp),    intent(in)    :: tauma(ndofn,ndofn)
  real(rp),    intent(in)    :: ptest(ndofn,ndofn,nnode)
  real(rp),    intent(in)    :: resma(ndofn,ndofn,nnode)
  real(rp),    intent(in)    :: resfo(ndofn)
  real(rp),    intent(out)   :: wmatr(ndofn,mnode,ndofn,mnode)
  real(rp),    intent(out)   :: wrhsi(ndofn,nnode)
  real(rp),    intent(inout) :: wprod(ndofn)

  integer(ip)              :: inode,jnode,idofn,jdofn,kdofn,ldofn,idime

  wprod=0.0_rp
  do ldofn=1,ndofn
     do kdofn=1,ndofn
        wprod(kdofn)=wprod(kdofn)            &
          +   tauma(kdofn,ldofn)*resfo(ldofn)
     end do
  end do

  do inode=1,nnode
     do kdofn=1,ndofn
        do idofn=1,ndofn
           wrhsi(idofn,inode) = wrhsi(idofn,inode)   &
             +   ptest(idofn,kdofn,inode)*wprod(kdofn)
        end do
     end do
  end do

  do jnode=1,nnode
     do jdofn=1,ndofn
        wprod=0.0_rp
        do ldofn=1,ndofn
           do kdofn=1,ndofn
              wprod(kdofn)=wprod(kdofn)                  &
                +   tauma(kdofn,ldofn)*resma(ldofn,jdofn,jnode)
           end do
        end do
        do inode=1,nnode
           do kdofn=1,ndofn
              do idofn=1,ndofn
                 wmatr(idofn,inode,jdofn,jnode) = wmatr(idofn,inode,jdofn,jnode) &
                   +   ptest(idofn,kdofn,inode)*wprod(kdofn)
              end do
           end do
        end do
     end do
  end do

end subroutine cdr_elmmul
