subroutine cdr_iniunk
!-----------------------------------------------------------------------
!****f* Codire/cdr_iniunk
! NAME 
!    cdr_iniunk
! DESCRIPTION
!    This routine sets up the initial condition for the unknown of the
!    CDR equations. If this is a restart, initial condition are loaded 
!    somewhere else.
! USES
!    cdr_iniofl
!    cdr_reabod
! USED BY
!    cdr_begste
!***
!-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_codire
  implicit none
  integer(ip) ::  ipoin,idofn

  if(kfl_rstar==0) then
     ! Load initial conditions for the temperature and correct fluxes
     do ipoin = 1,npoin
        do idofn = 1,ndofn_cdr
           if(kfl_fixno_cdr(idofn,ipoin)/=2.and.kfl_fixno_cdr(idofn,ipoin)/=3) then
              uncdr(idofn,ipoin,ncomp_cdr) = bvess_cdr(idofn,ipoin,1)
           end if
        end do
     end do
     ! Set initial unknown for points with prescribed heat fluxes.
     call cdr_iniofl
     ! Assign U(0,*,*) = U(-1,*,*) for two step schemes (BDF2)
     if(kfl_twost_cdr==1) then
        uncdr(:,:,3) = uncdr(:,:,4)
     end if
  end if

end subroutine cdr_iniunk

      
