subroutine cdr_memelm(itask)
!------------------------------------------------------------------------
!****f* Codire/cdr_memelm
! NAME 
!    cdr_memelm
! DESCRIPTION
!    This routine allocates memory for local element arrays
! USED BY
!    cdr_matrix
!    cdr_lstarg
!    cdr_forces
!***
!-----------------------------------------------------------------------
use def_parame
use def_master
use def_domain
use def_codire
use def_cdrloc
use mod_memchk
implicit none
integer(ip), intent(in) :: itask
integer(4)              :: istat

  if(itask==1) then ! allocate local memory

     ! Gather operations
     allocate(elunk(ndofn_cdr,mnode,ncomp_cdr-1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',elunk)
     allocate(elcod(ndime,mnode),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',elcod)

     ! Jacobians and derivatives
     allocate(cartd(ndime,mnode),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',cartd)
     allocate(xjaci(ndime,ndime),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',xjaci)
     allocate(xjacm(ndime,ndime),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',xjacm)

     if(mlapl>0) then
        allocate(hessi(ntens,mnode*mlapl),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',hessi)
        allocate(wmat1(ndime,ndime,mnode*mlapl),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',wmat1)
        allocate(wmat2(ndime,ndime,mnode*mlapl),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',wmat2)
        allocate(d2sdx(ndime,ndime,ndime*mlapl),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',d2sdx)
     end if

     ! Stabilization
     allocate(tragl(ndime,ndime),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',tragl)
     allocate(hleng(ndime),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',hleng)

     ! Values at Gauss points
     allocate(olunk(ndofn_cdr,ncomp_cdr-2),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',olunk)
     allocate(grunk(ndime,ndofn_cdr),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',grunk)
     allocate(vlapl(ntens*mlapl),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',vlapl)
     allocate(ptest(ndofn_cdr,ndofn_cdr,mnode),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',ptest)
     allocate(resma(ndofn_cdr,ndofn_cdr,mnode),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',resma)
     allocate(resfo(ndofn_cdr),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',resfo)

  else if(itask==2) then

     ! Gather operations
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',elunk)
     deallocate(elunk,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',elcod)
     deallocate(elcod,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)

     ! Jacobians and derivatives
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',cartd)
     deallocate(cartd,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',xjaci)
     deallocate(xjaci,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',xjacm)
     deallocate(xjacm,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)

     if(mlapl>0) then
        call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',hessi)
        deallocate(hessi,stat=istat)
        if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
        call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',wmat1)
        deallocate(wmat1,stat=istat)
        if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
        call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',wmat2)
        deallocate(wmat2,stat=istat)
        if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
        call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',d2sdx)
        deallocate(d2sdx,stat=istat)
        if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     end if

     ! Stabilization
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',tragl)
     deallocate(tragl,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',hleng)
     deallocate(hleng,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)

     ! Values at Gauss points
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',olunk)
     deallocate(olunk,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',grunk)
     deallocate(grunk,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',vlapl)
     deallocate(vlapl,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',ptest)
     deallocate(ptest,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',resma)
     deallocate(resma,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memelm',resfo)
     deallocate(resfo,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memelm',0_ip)

  end if

end subroutine cdr_memelm
