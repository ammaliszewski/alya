subroutine cdr_iniofl
!-----------------------------------------------------------------------
!
! This subroutine sets the initial unknown on the boundary for
! those points with prescribed heat fluxes. To avoid the creation of
! spurious transients, CDR unknown is set to the value of an interior
! conected point.      
!
!-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_codire
  implicit none
  integer ipoin,idofn,izdom,jpoin,jcode

  do ipoin = 1,npoin
     do idofn = 1,ndofn_cdr
        if(kfl_fixno_cdr(idofn,ipoin)>=2) then
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              jpoin = c_dom(izdom)
              jcode = kfl_fixno_cdr(idofn,jpoin)
              if(jcode==0) &
                 uncdr(idofn,ipoin,ncomp_cdr) = uncdr(idofn,jpoin,ncomp_cdr)
           end do
        end if
     end do
  end do

end subroutine cdr_iniofl
