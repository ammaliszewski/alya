subroutine cdr_cvgvec(ndofn,ndime,npoin,nunkn,zero_rp, &
                      unkno,uncdr,rnume,rdeno,ricdr,rmaxi)
!-----------------------------------------------------------------------
!****f* Codire/cdr_cvgvec
! NAME 
!    cdr_cvgvec
! DESCRIPTION
!    This routine checks convergence of the CDR equations when the first
!    ndime components correspond to a vector field (Navier Stokes, Plates,
!    Boussinesq and Low Mach).
! USES
! USED BY
!    cdr_cvgunk
!***
!-----------------------------------------------------------------------
use def_kintyp
implicit none
integer(ip), intent(in)   :: ndofn,ndime,npoin,nunkn
real(rp)   , intent(in)   :: zero_rp,unkno(nunkn),uncdr(ndofn,npoin)
real(rp)   , intent(out)  :: rnume(7),rdeno(7),ricdr(7),rmaxi(6)

integer(ip)               :: idofn,ipoin,ixcdr,i

  rnume=0.0_rp
  rdeno=0.0_rp
  ricdr=0.0_rp
  rmaxi=0.0_rp

  do ipoin=1,npoin
     ixcdr = (ipoin-1)*ndofn
     do idofn =1,ndime
        ixcdr = ixcdr + 1
        rnume(1) = rnume(1) + (unkno(ixcdr)-uncdr(idofn,ipoin))* &
                              (unkno(ixcdr)-uncdr(idofn,ipoin))
        rdeno(1) = rdeno(1) +  unkno(ixcdr)*unkno(ixcdr)
        rnume(2) = rnume(2) + (unkno(ixcdr)-uncdr(idofn,ipoin))* &
                              (unkno(ixcdr)-uncdr(idofn,ipoin))
        rdeno(2) = rdeno(2) +  unkno(ixcdr)*unkno(ixcdr)
        rmaxi(idofn) = max(unkno(ixcdr),rmaxi(idofn)) 
     end do
     do idofn=ndime+1,ndofn
        ixcdr = ixcdr + 1
        rnume(1) = rnume(1)  + (unkno(ixcdr)-uncdr(idofn,ipoin))* &
                              (unkno(ixcdr)-uncdr(idofn,ipoin))
        rdeno(1) = rdeno(1)  +  unkno(ixcdr)*unkno(ixcdr)
        rnume(2+idofn-ndime) = rnume(2+idofn-ndime)               &
                             + (unkno(ixcdr)-uncdr(idofn,ipoin))* &
                               (unkno(ixcdr)-uncdr(idofn,ipoin))
        rdeno(2+idofn-ndime) = rdeno(2+idofn-ndime) &
                             +  unkno(ixcdr)*unkno(ixcdr)
        rmaxi(idofn) = max(unkno(ixcdr),rmaxi(idofn))
     end do
  end do
  do i=1,2+ndofn-ndime
     if(rdeno(i) > zero_rp) ricdr(i) = 100.0_rp*sqrt(rnume(i)/rdeno(i))
  end do

end subroutine cdr_cvgvec
