subroutine cdr_reanut
!-----------------------------------------------------------------------
!****f* Codire/cdr_reanut
! NAME
!    cdr_reanut
! DESCRIPTION
!    This routine reads the numerical treatment for the CDR equations.
! USED BY
!    cdr_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_codire
  use      def_domain
  use      mod_memchk
  implicit none
  integer(ip) :: istab,ipart,istat
!
!  Initializations (defaults)
!
  kfl_weigh_cdr = 1               ! Weighting of dU/dt
  kfl_repro_cdr = 0               ! Stabilization based on residual projection
  kfl_grast_cdr = 0               ! Stabilization based on gradient projection
  kfl_shcty_cdr = 0               ! Shock capturing off
  kfl_tiacc_cdr = 1               ! First order time integ.
  kfl_schem_cdr = 1               ! Gear scheme
  kfl_twost_cdr = 0               ! Two step time scheme
  kfl_normc_cdr = 2               ! L2 norm for convergence
  kfl_linme_cdr = 1               ! Method of linearization is Picard
  kfl_linse_cdr = 0               ! Line search off
  kfl_algso_cdr = 8               ! Algebraic solver is GMRES
  miinn_cdr     = 1               ! One internal iteration
  other_cdr     = 0.0_rp          ! Other parameters
  welin_cdr     = 0.0_rp          ! Linearization parameters
  staco_cdr     = 0.0_rp
  staco_cdr(1)  = 4.0_rp          ! Viscous part of tau1
  staco_cdr(2)  = 2.0_rp          ! Convective part of tau1
  staco_cdr(3)  = 1.0_rp          ! Reactive part of tau1
  shcva_cdr     = 0.0_rp          ! SC parameter
  sstol_cdr     = 1.0e-5_rp       ! Steady-satate tolerance
  cotol_cdr     = 1.0_rp          ! Internal tolerance
!
! Reach the section
!
  rewind(lisda)
  call ecoute('cdr_reanut')
  do while(words(1)/='NUMER')
     call ecoute('cdr_reanut')
  end do
!
! Begin to read data
!
  do while(words(1)/='ENDNU')
     call ecoute('cdr_reanut')
        if(     words(1)=='STABI') then
           do istab = 1,10
              staco_cdr(istab) = param(istab)
           end do
        else if(words(1)=='TYPEO') then
          if(words(2)=='RESID') then
             kfl_repro_cdr = 1
          else if(words(2)=='GRADI') then
             kfl_grast_cdr = 1
          end if
        else if(words(1)=='SHOCK') then
          if(exists('ISOTR')) then
             kfl_shcty_cdr = 1
             shcva_cdr = getrea( &
              'VALUE',0.0_rp,'#Shock capturing parameter')
          else if(exists('ANISO')) then
             kfl_shcty_cdr = 2
             shcva_cdr = getrea( &
              'VALUE',0.0_rp,'#Shock capturing parameter')
          end if
        else if(words(1)=='TEMPO') then
          if(exists('GALER')) kfl_weigh_cdr = 0
        else if(words(1)=='TIMEA') then
          kfl_tiacc_cdr = int(param(1))
        else if(words(1).eq.'TIMEI') then
          if(exists('GEAR ')) then
             kfl_schem_cdr = 1
!             kfl_tiacc_cdr = getint('ORDER',1,'#Order of the time integration')
          else if(exists('TRAPE')) then
             kfl_schem_cdr = 2
!          else if(exists('ABCN ')) then
!             kfl_schem_cdr = 3
!          else if(exists('AB   ')) then
!             kfl_schem_cdr = 4
          else
             call runend('cdr_reanut: Time integration scheme not considered')
          end if
        else if(words(1)=='STEAD') then
          sstol_cdr = param(1)
        else if(words(1)=='NORMO') then
          if(exists('L1   ')) then
            kfl_normc_cdr = 1
          else if(exists('L-inf')) then
            kfl_normc_cdr = 0
          end if
        else if(words(1)=='MAXIM') then
          miinn_cdr = int(param(1))
        else if(words(1)=='CONVE') then
          cotol_cdr = param(1)
        else if(words(1)=='ALGEB') then
          if(exists('MUMPS')) kfl_algso_cdr = -1
          if(exists('DIREC')) kfl_algso_cdr =  0
          if(exists('CG   ')) kfl_algso_cdr =  1
          if(exists('CGNOR')) kfl_algso_cdr =  2
          if(exists('BiCG ')) kfl_algso_cdr =  3
          if(exists('BiCGW')) kfl_algso_cdr =  4
          if(exists('BiCGS')) kfl_algso_cdr =  5
          if(exists('TRANS')) kfl_algso_cdr =  6
          if(exists('FULLO')) kfl_algso_cdr =  7
          if(exists('GMRES')) kfl_algso_cdr =  8
          if(exists('FLEXI')) kfl_algso_cdr =  9
          if(exists('QUASI')) kfl_algso_cdr = 10
        else if(words(1)=='SOLVE') then
          misol_cdr = int(param(1))
        else if(words(1)=='TOLER') then
          solco_cdr = param(1)
        else if(words(1)=='KRYLO') then
          nkryd_cdr = int(param(1))
        else if(words(1)=='OTHER') then
          do ipart = 1,10
            other_cdr(ipart) = param(ipart)
          end do
        else if(words(1)=='LINEA') then
          if(exists('NEWTO')) then
            kfl_linme_cdr = 2
            npica_cdr = getint( &
              'PICAR',0,'#Number of Picard iterations')
          end if
          do ipart=1,nnwor
            if(words(ipart)=='PARAM') &
              welin_cdr(11:20)=param(ipart:ipart+10)
          end do
        else if(words(1)=='LINES') then
          if(exists('ARMIJ')) then
            kfl_linse_cdr = 1
            parls_cdr = getrea( &
              'PARAM',0.0_rp,'#Line search parameter')
            mitls_cdr = getint( &
              'ITERA',5,'#Number of Line search iterations')
            mipls_cdr = getint( &
              'PICAR',0,'#Number of Line search picard iterations')
            tolls_cdr = getrea( &
              'TOLER',0.0_rp,'#Line search tolerance')
          else if(exists('TESTC')) then
            kfl_linse_cdr = 2
          end if
        end if
  end do
  if(kfl_schem_cdr==1.and.kfl_tiacc_cdr==2) kfl_twost_cdr=1

end subroutine cdr_reanut
