subroutine cdr_turnof
  !-----------------------------------------------------------------------
  !****f* Codire/cdr_turnof
  ! NAME 
  !    cdr_turnof
  ! DESCRIPTION
  !    This routine closes the run for the CDR equations.
  ! USES
  !    cdr_outcpu
  !    cdr_output
  ! USED BY
  !    Codire
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_codire
  implicit none
  !
  ! Output results
  !
  call cdr_output(one)
  !
  ! Write tail for formatted files.
  !
  call outfor(6_ip,lun_solve_cdr,' ')
  call outfor(6_ip,lun_outpu_cdr,' ')
  !
  ! Close CDR files.
  !
  close(lun_pdata_cdr)
  close(lun_outpu_cdr)
  close(lun_conve_cdr)
  close(lun_solve_cdr)

end subroutine cdr_turnof
