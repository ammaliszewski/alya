subroutine cdr_updunk(itask)
!-----------------------------------------------------------------------
!****f* Codire/cdr_updunk
! NAME 
!    cdr_updunk
! DESCRIPTION
!    This routine performs several types of updates for the unknown of
!    the CDR equations.      
! USED BY
!    cdr_begste (itask=1)
!    cdr_begite (itask=2)
!    cdr_endite (itask=3, inner loop) 
!    cdr_endite (itask=4, outer loop) 
!    cdr_endste (itask=5)
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin
  
  select case (itask)
!
! Assign U(n,0,*) <-- U(n-1,*,*), initial guess for outer iterations
!
  case(1)
     uncdr(:,:,2) = uncdr(:,:,3)
!
! Assign U(n,i,0) <-- U(n,i-1,*), initial guess for inner iterations
!
  case(2)
     uncdr(:,:,1)     = uncdr(:,:,2)
     unkno(:) = reshape(uncdr(:,:,2),(/nunkn_cdr/))
!
! Assign U(n,i,j-1) <-- U(n,i,j), update of the unknonwn
!
  case(3)
     uncdr(:,:,1) = reshape(unkno,(/ndofn_cdr,npoin/))
!
! Assign U(n,i-1,*) <-- U(n,i,*)
!
  case(4)
     uncdr(:,:,2) = uncdr(:,:,1)
!
! Obtain U(n,*,*) for the Crank-Nicolson method.
! Assign U(n-2,*,*) <-- U(n-1,*,*) for two step methods (BDF2).
! Assign U(n-1,*,*) <-- U(n,*,*).
!
  case(5)
     if(kfl_twost_cdr==1) then
        uncdr(:,:,4) = uncdr(:,:,3)
     end if
     if(kfl_schem_cdr==2.and.kfl_tiacc_cdr==2) then
        uncdr(:,:,3) = 2.0_rp*uncdr(:,:,1)-uncdr(:,:,3)
     else
        uncdr(:,:,3) = uncdr(:,:,1)
     end if
!
! Assign U(n,0,*) <-- U(n-1,*,3), restart
!
  case(6)
     uncdr(:,:,1) = uncdr(:,:,3)

  end select


end subroutine cdr_updunk
