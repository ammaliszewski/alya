subroutine cdr_elmope(ielem,pnode,pelty,kprob,ncomp,ndofn,uncdr)
!------------------------------------------------------------------------
!****f* Codire/cdr_elmope
! NAME 
!    cdr_elmope
! DESCRIPTION
!    Compute elemental matrix and RHS for the CDR equations
!    Matrials are not implemented.
! USES
!    cdr_elmgat
!    elmlen
!    elmder
! USED BY
!    cdr_matrix
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_codire
  use def_cdrloc
  implicit none
  integer(ip) :: ielem,pelty,pnode
  integer(ip) :: kprob,ncomp,ndofn
  real(rp)    :: uncdr(ndofn,npoin,ncomp)
  integer(ip) :: igaus,idofn,jdofn,inode,jnode

     ! Initialize
     elmat=0.0_rp
     elrhs=0.0_rp

     ! Gather operations
     call cdr_elmgat(ndofn,pnode,mnode,ndime,npoin,lnods(1,ielem),    &
                     kfl_timei_cdr,kfl_twost_cdr,elunk,elcod,uncdr,coord)

     ! hleng and tragl at center of gravity
     call elmlen(ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&
          hnatu(pelty),hleng)

     gauss_points: do igaus=1,ngaus(pelty)

        wmatr=0.0_rp
        wrhsi=0.0_rp

        ! Cartesian derivatives and Jacobian
        call elmder(pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&
                    elcod,cartd,detjm,xjacm,xjaci)
        dvolu=elmar(pelty)%weigp(igaus)*detjm
        if(llapl(pelty)==1) &
             call elmhes(&
             elmar(pelty)%heslo(1,1,igaus),hessi,ndime,nnode,ntens,    &
             xjaci,d2sdx,elmar(pelty)%deriv(1,1,igaus),elcod)

        ! Definition of cdr matrices
        call cdr_defmat(kprob,kfl_syseq_cdr,kfl_fdiff_cdr,           &
                        lawvi_cdr,ncomp,ndofn,pnode,mnode,ndime,     &
                        phypa_cdr,visco_cdr,thpre_cdr,welin_cdr,     &
                        staco_cdr,hleng,elmar(pelty)%shape(1,igaus), &
                        cartd,elcod,elunk,masma_cdr,dires_cdr,       &
                        cores_cdr,flres_cdr,sores_cdr,dites_cdr,     &
                        cotes_cdr,sotes_cdr,tauma_cdr,force_cdr,grunk)

        ! Contribution from Galerkin terms
        call cdr_elmgal(ndime,pnode,mnode,ntens,ndofn,ncomp,   &
                        llapl(pelty),kfl_timei_cdr,theta_cdr,  &
                        dires_cdr,cores_cdr,flres_cdr,         &
                        sores_cdr,force_cdr,masma_cdr,         &
                        elunk,olunk,dtinv_cdr,                 &
                        elmar(pelty)%shape(1,igaus),           &
                        cartd,zero_rp,wmatr,wrhsi)

        ! Contribution from stabilization terms
        if(kfl_repro_cdr == 0) then	                                   ! ASGS
           call cdr_elmtes(ndime,pnode,ntens,ndofn,llapl(pelty),     & ! Perturbation of the Galerkin test functions
                           dites_cdr,cotes_cdr,sotes_cdr,            &
                           elmar(pelty)%shape(1,igaus),cartd,hessi,vlapl,   &
                           zero_rp,ptest)

           call cdr_elmres(ndime,pnode,ntens,ndofn,ncomp,            & ! Computes the residual
                           llapl(pelty),kfl_timei_cdr,kfl_weigh_cdr, &
                           theta_cdr,dires_cdr,cores_cdr,flres_cdr,  &
                           sores_cdr,force_cdr,masma_cdr,            &
                           olunk,dtinv_cdr,elmar(pelty)%shape(1,igaus),&
                           cartd,hessi,vlapl,zero_rp,resma,resfo)
           call cdr_elmmul(ndime,pnode,mnode,ndofn,tauma_cdr,        & ! Multiplication of test functions by residuals
                           ptest,resma,resfo,wmatr,wrhsi,wprod)
        else 
            call runend('cdr_elmope: Residual proyection not ready  ')
        end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!        To do: Contribution from the shock capturing
!        if (kfl_shock_tem >= 1) call cdrshc(&
!              velac,grate,hessi,cartd,shape,eltem,sourc_tem,&
!              wmatr,react_tem,temol,hleng,timhe,shock_tem,&
!	            rhocp,tcond,dtinv_tem,ndime,nnode,ntens,llapl,&
!              kfl_shock_tem,kfl_timei_tem,kfl_advec_tem)
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!        elrhs=elrhs+dvolu*wrhsi                ! Add Gauss point contribution
!        elmat=elmat+dvolu*wmatr
         do jnode=1,pnode
            do jdofn=1,ndofn
               elrhs(jdofn,jnode) = elrhs(jdofn,jnode) &
                 + dvolu*wrhsi(jdofn,jnode)
               do inode=1,pnode
                  do idofn=1,ndofn
                     elmat(idofn,inode,jdofn,jnode) = elmat(idofn,inode,jdofn,jnode) &
                       + dvolu*wmatr(idofn,inode,jdofn,jnode)
                  end do
               end do
            end do
         end do

     end do gauss_points

end subroutine cdr_elmope
