subroutine cdr_bouope(iboun,pblty,pnodb,ielem,pelty,pnode, &
                      kprob,ncomp,ndofn,uncdr)
!------------------------------------------------------------------------
!****f* Codire/cdr_bouope
! NAME 
!    cdr_bouope
! DESCRIPTION
!    Compute boundary matrix and RHS for the CDR equations
!    Matrials are not implemented.
! USES
! USED BY
!    cdr_matrix
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_codire
  use def_cdrloc
  implicit none
  integer(ip),intent(in) :: ielem,pelty,pnode,iboun,pblty,pnodb
  integer(ip),intent(in) :: kprob,ncomp,ndofn
  real(rp)   ,intent(in) :: uncdr(ndofn,npoin,ncomp)

  integer(ip) :: inode,jnode,ipoin,igaus,idime,jdime
  integer(ip) :: igaub,inodb,jnodb,idofn,jdofn
  real(rp)    :: kcoef,tempg,densg,coorg

!  real(rp)    :: shapp(mnode)
  real(rp)    :: cartb(ndime,mnode)
  real(rp)    :: tract(ndofn)


  ! Initialize
  elmat=0.0_rp
  elrhs=0.0_rp

  ! Gather operations
  do inodb=1,pnodb
     ipoin=lnodb(inodb,iboun)
     bounk(:,inodb) = uncdr(:,ipoin,1)
     bocod(:,inodb) = coord(:,ipoin)
  end do
  do inode=1,pnode
     ipoin=lnods(inode,ielem)
     elcod(:,inode)=coord(1:ndime,ipoin)
     elunk(:,inode,1)=uncdr(:,ipoin,1)
  end do
  
  gauss: do igaub=1,ngaus(pblty)

     wmatr=0.0_rp
     wrhsi=0.0_rp

     ! Jacobian
     call bouder(pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&
                 bocod,baloc,eucta)
     dsurf=elmar(pblty)%weigp(igaub)*eucta 
     call chenor(pnode,baloc,bocod,elcod)                          ! Check normal
                 
     do inode=1,pnode                                              ! Calculate carte-
        do idime=1,ndime                                           ! sian derivates
           cartb(idime,inode)=0.0_rp                               ! at boundary 
           do inodb=1,pnodb                                        ! gauss points
              do igaus=1,ngaus(pelty)
                 call elmder(&
                      pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&  ! Cartesian derivative
                      elcod,cartd,detjm,xjacm,xjaci)               ! and Jacobian
                 cartb(idime,inode) = cartb(idime,inode)&
                                    + elmar(pelty)%shaga(igaus,lboel(inodb,iboun))&
                                    * elmar(pblty)%shape(inodb,igaub)*cartd(idime,inode)
              end do
           end do
        end do
     end do
!     shapp=0.0_rp                                                    ! Shape at boundary
!     do inodb=1,pnodb                                                ! gauss points
!        shapp(lboel(inodb,iboun))=shape(inodb,igaub,pelty)
!     end do

     ! Evaluate matrices
!     call cdr_defmat(kprob,kfl_syseq_cdr,kfl_fdiff_cdr,           &
!                     lawvi_cdr,ncomp,ndofn,pnode,mnode,ndime,     &
!                     phypa_cdr,visco_cdr,thpre_cdr,welin_cdr,     &
!                     staco_cdr,hleng,shapp,                       &
!                     cartb,elcod,elunk,masma_cdr,dires_cdr,       &
!                     cores_cdr,flres_cdr,sores_cdr,dites_cdr,     &
!                     cotes_cdr,sotes_cdr,tauma_cdr,force_cdr,grunk)

     tract=0.0_rp
     do idofn=1,ndofn
        if(kfl_fixbo_cdr(idofn,iboun)==2) then
           ! Neumann conditions
           do inodb=1,pnodb
              tract(idofn) = tract(idofn) &
                           + elmar(pblty)%shape(inodb,igaub)*bvnat_cdr(idofn,iboun)%a(1,inodb)
           end do
        else if(kfl_fixbo_cdr(idofn,iboun)==3) then
           ! Robin conditions
           do inodb=1,pnodb
              tract(idofn) = tract(idofn)                                              &
                +  elmar(pblty)%shape(inodb,igaub) *                                   &
                (  bvnat_cdr(idofn,iboun)%a(3,inodb)                                   &
                 - bvnat_cdr(idofn,iboun)%a(1,inodb)*bvnat_cdr(idofn,iboun)%a(2,inodb) )
           end do
           do inodb=1,pnodb
              inode=lboel(inodb,iboun)
              do jnodb=1,pnodb
                 inode=lboel(jnodb,iboun)
                 wmatr(idofn,inode,idofn,jnode) = wmatr(idofn,inode,idofn,jnode) &
                   - elmar(pblty)%shape(inodb,igaub) * elmar(pblty)%shape(jnodb,igaub) *       &
                     bvnat_cdr(idofn,iboun)%a(1,jnodb)
              end do
           end do
        else if(kfl_fixbo_cdr(idofn,iboun)==4) then
           ! Atmospheric stress        1,ndime
           if(kprob==2) then
              coorg = 0.0_rp
              do inodb=1,pnodb
                 inode=lboel(jnodb,iboun)
                 coorg = coorg + elmar(pblty)%shape(inodb,igaub) * bvnat_cdr(ndofn-1,iboun)%a(1,inodb)
              end do
              tract(idofn) = tract(idofn) &
                +  densg*phypa_cdr(9)*coorg
           else if(kprob==7) then
              tempg = 0.0_rp
              coorg = 0.0_rp
              do inodb=1,pnodb
                 inode=lboel(jnodb,iboun)
                 tempg = tempg + elmar(pblty)%shape(inodb,igaub) * elunk(ndofn,inode,1)
                 coorg = coorg + elmar(pblty)%shape(inodb,igaub) * bvnat_cdr(idofn,iboun)%a(1,inodb)
              end do
              densg = phypa_cdr(1)
!              dilat = phypa(15)
!              teref = phypa(16)
!              gnorm    = phypa( 9)
!              g_dir(1) = phypa(10)
!              g_dir(2) = phypa(11)
!              g_dir(3) = phypa(12)
              tract(idofn) = tract(idofn) &
                +  densg*phypa_cdr(9)*phypa_cdr(15)*(tempg-phypa_cdr(16))*coorg
                !   rho   gnorm         dilat        tempg  teref
           else if(kprob==8) then
              tempg = 0.0_rp
              coorg = 0.0_rp
              do inodb=1,pnodb
                 inode=lboel(jnodb,iboun)
                 tempg = tempg + elmar(pblty)%shape(inodb,igaub) * elunk(ndofn,inode,1)
                 coorg = coorg + elmar(pblty)%shape(inodb,igaub) * bvnat_cdr(idofn,iboun)%a(1,inodb)
              end do
              densg=thpre_cdr(1)/tempg
              tract(idofn) = tract(idofn) &
                +  (densg-phypa_cdr(1))*phypa_cdr(9)*tempg*coorg
                !   rho    rho_ref       gnorm       tempg
           end if
        end if
     end do

     if(kprob==8.and.kfl_syseq_cdr==2) then         ! Special b.c. for low mach
        idofn=ndofn-1
        if(kfl_fixbo_cdr(idofn,iboun)<=1) then
           kcoef = phypa_cdr(17)/thpre_cdr(1)
!           do idime=1,ndime                           ! RHS
!              tract(idofn) = tract(idofn) &
!                           + kcoef * baloc(idime,ndime) * grunk(idime,ndofn)
!           end do
           do inodb=1,pnodb                            ! LHS
              inode=lboel(inodb,iboun)
              do jnode=1,pnode
                 do idime=1,ndime
                 wmatr(idofn,inode,ndofn,jnode) = wmatr(idofn,inode,ndofn,jnode) &
                   - elmar(pblty)%shape(inodb,igaub) * kcoef *      &
                     baloc(idime,ndime) * cartb(idime,jnode)
                 end do
              end do
           end do
        end if
     end if

     do inodb=1,pnodb
        inode=lboel(inodb,iboun)
        do idofn=1,ndofn
           wrhsi(idofn,inode) = wrhsi(idofn,inode) &
                              + elmar(pblty)%shape(inodb,igaub)*tract(idofn)
        end do
     end do

     elmat=elmat+wmatr*dsurf
     elrhs=elrhs+wrhsi*dsurf

  end do gauss

  
end subroutine cdr_bouope
