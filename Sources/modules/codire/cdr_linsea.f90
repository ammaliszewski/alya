subroutine cdr_linsea
!-----------------------------------------------------------------------
!****f* Codire/cdr_linsea
! NAME 
!    cdr_linsea
! DESCRIPTION
!    This routine performs a line search and backtracking.
! USES
! USED BY
!    cdr_endite
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  use mod_memchk
  implicit none
  integer(ip),save :: linme,kpite
  integer(4)       :: istat
  real(rp)         :: glin0,glin1,dglin
  real(rp)         :: glins(10),slins(10)
  data kpite/0/
!
! Memory allocation
!
  allocate(geten(ndofn_cdr,npoin,2),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'GETEN','cdr_linse',geten)
!
! Set linearization parameters to Picard in order to evaluate F
!
  if(kpite==0) linme=kfl_linme_cdr
  welin_cdr(1:10) = 0.0_rp
!
! Evaluate F at s=0
!
  call cdr_lstarg
!
! Save u_i and the search direction p
!
  geten(:,:,1) =  uncdr(:,:,1)
  geten(:,:,2) =  reshape(unkno,(/ndofn_cdr,npoin/)) - uncdr(:,:,1)
!
! Evaluate f and g at s=0
  glin0=0.5_rp*dot_product(rhsid,rhsid)
  dglin=-2.0_rp*glin0
!
! Evaluate F at s=1
  uncdr(:,:,1) = reshape(unkno,(/ndofn_cdr,npoin/))
  if(kfl_modul(modul)==8) call cdr_updtpr(one)
  call cdr_lstarg
  glin1=0.5_rp*dot_product(rhsid,rhsid)
!
! Line search and backtracking, if necesary
!
  if(kfl_linse_cdr == 1) then
     if(kpite==0) then          ! Armijo rule
        call cdr_lsarmj(kpite,glin0,glin1,dglin,glins,slins)
     else
        call cdr_lspics(kpite,glin0,glin1,dglin,glins,slins)
     end if
  else
     slins(3)=1.0_rp
     write(lun_linse_cdr,100) itinn(modul),1.0,glin0,glin1,glin1
     if(glin1 > 1.0e12_rp) itinn(modul) = miinn_cdr
  end if
!
! Updates
!
  uncdr(:,:,1) =   geten(:,:,1)
  unkno = reshape( geten(:,:,1) + slins(3) * geten(:,:,2) ,(/nunkn_cdr/))

  if(kpite == 0) then
     kfl_linme_cdr=linme          ! Set paramters again to Newton
     call cdr_cvgunk(  one)       ! Check convergence
     call cdr_updunk(three)       ! Update unknonws
  else
     kfl_linme_cdr=1
  end if

  if(kfl_modul(modul)==8) call cdr_updtpr(two)      !  Update p_th in Low Mach
!
! Memory deallocation
!
  call memchk(two,istat,mem_modul(1:2,modul),'GETEN','cdr_linse',geten)
  deallocate(geten,stat=istat)
  if(istat.ne.0)  call memerr(two,'GETEN','cdr_linse',0_ip)

100 format(10x,i15,4x,4(e13.6,4x),'Line search not performed')

end subroutine cdr_linsea
