subroutine cdr_reaphy
!-----------------------------------------------------------------------
!****f* Codire/cdr_reaphy
! NAME
!    cdr_reaphy
! DESCRIPTION
!    This routine reads the physical problem definition for the
!    CDR equations.
! USED BY
!    cdr_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_codire
  use      def_domain
  use      mod_memchk
  use      mod_iofile
  implicit none
  integer(ip) :: imate,npara,ielem
  integer(ip) :: i
  integer(4)  :: istat
  real(rp)    :: rdumm
!
! Read and write files
!
  lispa        = 0
  lisda        = lun_pdata_cdr   ! Reading file
  lisre        = lun_outpu_cdr   ! Writing file
!
! Initializations (defaults)
!
  kfl_timei_cdr = 0              ! Steady-state
  kfl_thpeq_cdr = 0              ! Thermodynamic pressure determined by mass balance
  phypa_cdr=0.0_rp
!
! Initializations
!
  select case(kfl_modul(modul))
  case(one)

  case(two)
     ndofn_cdr = ndime + 1       ! Navier Stokes
  case(three)
     ndofn_cdr = 3               ! Plates
  case(four)
     ndofn_cdr = 2               ! Helmholtz
  case(five)
     ndofn_cdr = ndime + 1       ! Contaminant
  case(six)
     ndofn_cdr = 1               ! Concentration
  case(seven)
     ndofn_cdr = ndime + 2       ! Boussinesq equations
  case(eight)
     ndofn_cdr = ndime + 2       ! Low Mach number equations
  case(nine)
     ndofn_cdr = ndime + 3       ! Compressible Navier Stokes
  end select

  ndof2_cdr    = ndofn_cdr*ndofn_cdr
  nunkn_cdr    = ndofn_cdr*npoin                           ! # of d.o.f.

!
! Reach the section
!
  rewind(lisda)
  call ecoute('cdr_reaphy')
  do while(words(1)/='PHYSI')
     call ecoute('cdr_reaphy')
  end do
!
! Allocate memory for the matrix coefficients (understood as
! physical properties), and also for the TAU matrix.
!
  allocate(masma_cdr(ndofn_cdr,ndofn_cdr),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'MASMA_CDR','cdr_reaphy',masma_cdr)
  masma_cdr=0.0_rp

  allocate(dires_cdr(ndofn_cdr,ndofn_cdr,ndime,ndime),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'DIRES_CDR','cdr_reaphy',dires_cdr)
  dires_cdr=0.0_rp

  allocate(cores_cdr(ndofn_cdr,ndofn_cdr,ndime),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'CORES_CDR','cdr_reaphy',cores_cdr)
  cores_cdr=0.0_rp

  allocate(flres_cdr(ndofn_cdr,ndofn_cdr,ndime),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'FLRES_CDR','cdr_reaphy',flres_cdr)
  flres_cdr=0.0_rp

  allocate(sores_cdr(ndofn_cdr,ndofn_cdr),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'SORES_CDR','cdr_reaphy',sores_cdr)
  sores_cdr=0.0_rp

  allocate(tauma_cdr(ndofn_cdr,ndofn_cdr),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'TAUMA_CDR','cdr_reaphy',tauma_cdr)
  tauma_cdr=0.0_rp

  allocate(force_cdr(ndofn_cdr),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'FORCE_CDR','cdr_reaphy',force_cdr)
  force_cdr=0.0_rp

  allocate(ditne_cdr(ndofn_cdr,ndofn_cdr),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'DITNE_CDR','cdr_reaphy',ditne_cdr)
  ditne_cdr=0.0_rp

  if(kfl_modul(modul)/=1) then
     allocate(cotes_cdr(ndofn_cdr,ndofn_cdr,ndime),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'COTES_CDR','cdr_reaphy',cotes_cdr)
     cotes_cdr=0.0_rp
     allocate(dites_cdr(ndofn_cdr,ndofn_cdr,ndime,ndime),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'DITES_CDR','cdr_reaphy',dites_cdr)
     dites_cdr=0.0_rp
     allocate(sotes_cdr(ndofn_cdr,ndofn_cdr),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'SOTES_CDR','cdr_reaphy',sotes_cdr)
     sotes_cdr=0.0_rp
  end if
!
! Read data for the general case.
!
  if(kfl_modul(modul)==1) then
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='TEMPO') then                         ! Temporal evolution
           if(exists('ON   ')) kfl_timei_cdr = 1
        else if(words(1)=='MASSM') then                    ! Mass matrix
           call matrea(ndofn_cdr,ndofn_cdr,1,masma_cdr)
        else if(words(1)=='RESDI') then                    ! Residual Diffusive matrices
           call matrea(ndofn_cdr,ndime,ndime,dires_cdr)
        else if(words(1)=='RESCO') then                    ! Residual Convective matrices
           call matrea(ndofn_cdr,ndime,1,cores_cdr)
        else if(words(1)=='RESFL') then                    ! Residual Flux matrices
           call matrea(ndofn_cdr,ndime,1,flres_cdr)
        else if(words(1)=='RESSO') then                    ! Residual Source matrices
           call matrea(ndofn_cdr,1,1,sores_cdr)
        else if(words(1)=='FORCE') then                    ! Force vector
           call vecrea(ndofn_cdr,force_cdr)
        else if(words(1)=='DTNMA') then                    ! Dirichlet-to-Neumann matrices
           call matrea(ndofn_cdr,1,1,ditne_cdr)
        else if(words(1)=='TESDI') then                    ! Test Diffusive matrices
           allocate(dites_cdr(ndofn_cdr,ndofn_cdr,ndime,ndime),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'DITES_CDR','cdr_reaphy',dites_cdr)
           call matrea(ndofn_cdr,ndime,ndime,dites_cdr)
        else if(words(1)=='TESCO') then                    ! Test Convective matrices
           allocate(cotes_cdr(ndofn_cdr,ndofn_cdr,ndime),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'COTES_CDR','cdr_reaphy',cotes_cdr)
           call matrea(ndofn_cdr,ndime,1,cotes_cdr)
        else if(words(1)=='TESSO') then                    ! Test Source matrices
           allocate(sotes_cdr(ndofn_cdr,ndofn_cdr),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'SOTES_CDR','cdr_reaphy',sotes_cdr)
           call matrea(ndofn_cdr,1,1,sotes_cdr)
        end if
     end do
     ! Allocate test coefficient arrays and set them equal to the residual ones if
     ! they have not been read
     if(.not. allocated(dites_cdr)) then
        allocate(dites_cdr(ndofn_cdr,ndofn_cdr,ndime,ndime),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'DITES_CDR','cdr_reaphy',dites_cdr)
        dites_cdr=dires_cdr
     end if
     if(.not. allocated(cotes_cdr)) then
        allocate(cotes_cdr(ndofn_cdr,ndofn_cdr,ndime),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'COTES_CDR','cdr_reaphy',cotes_cdr)
        cotes_cdr=cores_cdr
     end if
     if(.not. allocated(sotes_cdr)) then
        allocate(sotes_cdr(ndofn_cdr,ndofn_cdr),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'SOTES_CDR','cdr_reaphy',sotes_cdr)
        sotes_cdr=sores_cdr
     end if
!
! Read data for the Navier-Stokes equations.
!
  else if(kfl_modul(modul)==2) then
     kfl_fdiff_cdr = 0                                  ! Laplacian form of viscous term
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='TEMPO') then                      ! Temporal evolution
           if(exists('ON   ')) kfl_timei_cdr = 1
        else if(words(1)=='SECON') then 
           if(exists('DIVER')) kfl_fdiff_cdr=1
        else if(words(1)=='LAWVI') then                 ! Law for mu
           if(words(2)=='CONST') lawvi_cdr=0
           if(words(2)=='POWER') lawvi_cdr=1
           if(words(2)=='CARRE') lawvi_cdr=2
           if(words(2)=='PERZY') lawvi_cdr=3
        else if(words(1)=='VISCO') then                 ! Viscosity
           visco_cdr=param(1:10)
        else if(words(1)=='DENSI') then                 ! Density
           phypa_cdr(1) = param(1)
        else if(words(1)=='PERME') then                 ! Permeability
           phypa_cdr(2) = param(1)
        else if(words(1)=='ANGUL') then                 ! Angular velocity
           phypa_cdr(3) = getrea('NORM ',0.0_rp,'#Axes rotation norm')
           phypa_cdr(4) = getrea('OX   ',0.0_rp,'#x-component of w')
           phypa_cdr(5) = getrea('OY   ',0.0_rp,'#y-component of w')
           phypa_cdr(6) = getrea('OZ   ',0.0_rp,'#z-component of w')
           call vecuni(3,phypa_cdr(4),rdumm)
           phypa_cdr(4:6) = phypa_cdr(3)*phypa_cdr(4:6)
        else if(words(1)=='PENAL') then                 ! Penalty parameter
           phypa_cdr(7) = param(1)
        else if(words(1)=='SPEED') then                 ! Speed of sound
           phypa_cdr(8) = param(1)
        end if
        if(phypa_cdr(8).lt.zero_rp) phypa_cdr(8) = 1.0_rp
     end do
!
! Read data for the plate problem (always steady-state).
!
  else if(kfl_modul(modul)==3) then
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='THICK') then                      ! Plate thickness
           phypa_cdr(1) = param(1)
        else if(words(1)=='YOUNG') then                 ! Young modulus
           phypa_cdr(2) = param(1)
        else if(words(1)=='POISS') then                 ! Poisson coefficient
           phypa_cdr(3) = param(1)
        else if(words(1)=='SHEAR') then                 ! Shear correction
           phypa_cdr(4) = param(1)
        else if(words(1)=='NORMA') then                 ! Normalized load
           phypa_cdr(5) = param(1)
        end if
     end do
!
! Read data for the Helmholtz problem (always steady-state).
!
  else if(kfl_modul(modul)==4) then
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='WAVEN') then                      ! Wave number
           phypa_cdr(1) = param(1)
        else if(words(1)=='CONST') then                 ! Constant force
           phypa_cdr(2) = param(1)
        end if
     end do
!
! Read data for the transport of contaminants.
!
  else if(kfl_modul(modul)==5) then 
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='TEMPO') then                      ! Temporal evolution
           if(exists('ON   ')) kfl_timei_cdr = 1
        else if(words(1)=='DIFFU') then                 ! Diffusion coefficient
           phypa_cdr(1) = param(1)
        else if(words(1)=='CONTA') then                 ! Contaminant density
           phypa_cdr(2) = param(1)
        else if(words(1)=='FLUID') then                 ! Fluid density
           phypa_cdr(3) = param(1)
        else if(words(1)=='PARTI') then                 ! Particle size
           phypa_cdr(4) = param(1)  
        else if(words(1)=='GRAVI') then                 ! Gravity for CDR problem
           phypa_cdr(5) = getrea('NORM ',0.0_rp,'#Gravity norm')
           phypa_cdr(6) = getrea('GX   ',0.0_rp,'#x-component of g')
           phypa_cdr(7) = getrea('GY   ',0.0_rp,'#y-component of g')
           phypa_cdr(8) = getrea('GZ   ',0.0_rp,'#z-component of g')
           call vecuni(3,phypa_cdr(6),rdumm)
           phypa_cdr(6:8) = phypa_cdr(5)*phypa_cdr(6:8)
        end if
     end do
!     phypa_cdr(9) = visconst(1)                          ! Fluid viscosity
!     if(kfl_modul_nst==0) &
!        call wriwar('Contaminant transport is running without NST')
     if(phypa_cdr(1) < zero_rp) &
        call runend('cdr_reaphy: Negative diffusion coefficient')
     if(phypa_cdr(2) < zero_rp) &
        call runend('cdr_reaphy: Non-positive contaminant density')
     if(phypa_cdr(3) < zero_rp) &
        call runend('cdr_reaphy: Negative fluid density')
!     if(phypa_cdr(3)/=densinst(1))  &
!        call wriwar('Fluid density for NST and CDR do not coincide')
     if(phypa_cdr(4).lt.zero_rp) &
        call runend('cdr_reaphy: Non-positive particle size')
!     if(phypa_cdr(9).lt.zero_rp) &
!        call wriwar('cdr_reaphy: Non-positive fluid viscosity')
!
! Read data for the transport of concentration.
!
  else if(kfl_modul(modul)==6) then 
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='TEMPO') then                      ! Temporal evolution
           if(exists('ON   ')) kfl_timei_cdr = 1
        else if(words(1)=='DIFFU') then                 ! Diffusion coefficient
           phypa_cdr(1) = param(1)
        end if
     end do
!     if(kfl_modul_nst==0) &
!        call wriwar('Contaminant transport is running without NST')
     if(phypa_cdr(1) < 0_rp) &
        call runend('cdr_reaphy: Negative diffusion coefficient')
!
! Read data for the Boussinesq equations.
!
  else if(kfl_modul(modul)==7) then
     kfl_fdiff_cdr = 0                                  ! Laplacian form of viscous term
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='TEMPO') then                      ! Temporal evolution
           if(exists('ON   ')) kfl_timei_cdr = 1
        else if(words(1)=='SECON') then 
           if(exists('DIVER')) kfl_fdiff_cdr=1
        else if(words(1)=='LAWVI') then                 ! Law for mu
           if(words(2)=='CONST') lawvi_cdr=0
           if(words(2)=='POWER') lawvi_cdr=1
           if(words(2)=='CARRE') lawvi_cdr=2
           if(words(2)=='PERZY') lawvi_cdr=3
        else if(words(1)=='VISCO') then                 ! Viscosity
           visco_cdr=param(1:10)
        else if(words(1)=='DENSI') then                 ! Density
           phypa_cdr(1) = param(1)
        else if(words(1)=='PERME') then                 ! Permeability
           phypa_cdr(2) = param(1)
        else if(words(1)=='ANGUL') then                 ! Angular velocity
           phypa_cdr(3) = getrea('NORM ',0.0_rp,'#Axes rotation norm')
           phypa_cdr(4) = getrea('OX   ',0.0_rp,'#x-component of w')
           phypa_cdr(5) = getrea('OY   ',0.0_rp,'#y-component of w')
           phypa_cdr(6) = getrea('OZ   ',0.0_rp,'#z-component of w')
           call vecuni(3,phypa_cdr(4),rdumm)
           phypa_cdr(4:6) = phypa_cdr(3)*phypa_cdr(4:6)
        else if(words(1)=='PENAL') then                 ! Penalty parameter
           phypa_cdr(7) = param(1)
        else if(words(1)=='SPEED') then                 ! Speed of sound
           phypa_cdr(8) = param(1)
        else if(words(1)=='GRAVI') then
           phypa_cdr( 9) = getrea('NORM ',0.0_rp,'#Gravity norm')
           phypa_cdr(10) = getrea('GX   ',0.0_rp,'#x-component of g')
           phypa_cdr(11) = getrea('GY   ',0.0_rp,'#y-component of g')
           phypa_cdr(12) = getrea('GZ   ',0.0_rp,'#z-component of g')
           call vecuni(3,phypa_cdr(10),rdumm)
!           phypa_cdr(10:12)=phypa_cdr(9)*phypa_cdr(10:12)
        else if(words(1)=='THERM') then                 ! Thermal conductivity
           phypa_cdr(13) = param(1)
        else if(words(1)=='SPECI') then                 ! Specific heat
           phypa_cdr(14) = param(1)
        else if(words(1)=='DILAT') then
           phypa_cdr(15) = param(1)
        else if(words(1)=='REFER') then
           phypa_cdr(16) = param(1)
        end if
        if(phypa_cdr(8).lt.zero_rp) phypa_cdr(8) = 1.0_rp
     end do
!
! Read data for the Low Mach number equations.
!
  else if(kfl_modul(modul)==8) then
     kfl_syseq_cdr = 1
     kfl_fdiff_cdr = 1                                  ! Divergence form of viscous term
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='SYSTE') then                      ! System of equations
           kfl_syseq_cdr = int(param(1))
        else if(words(1)=='TEMPO') then                 ! Temporal evolution
           if(exists('ON   ')) kfl_timei_cdr = 1
        else if(words(1)=='LAWVI') then                 ! Law for mu
           if(words(2)=='CONST') lawvi_cdr=0
           if(words(2)=='POWER') lawvi_cdr=1
           if(words(2)=='CARRE') lawvi_cdr=2
           if(words(2)=='PERZY') lawvi_cdr=3
        else if(words(1)=='VISCO') then                 ! Viscosity
           visco_cdr=param(1:10)
        else if(words(1)=='DENSI') then                 ! Density
           phypa_cdr(1) = param(1)
        else if(words(1)=='PERME') then                 ! Permeability
           phypa_cdr(2) = param(1)
        else if(words(1)=='ANGUL') then                 ! Angular velocity
           phypa_cdr(3) = getrea('NORM ',0.0_rp,'#Axes rotation norm')
           phypa_cdr(4) = getrea('OX   ',0.0_rp,'#x-component of w')
           phypa_cdr(5) = getrea('OY   ',0.0_rp,'#y-component of w')
           phypa_cdr(6) = getrea('OZ   ',0.0_rp,'#z-component of w')
           call vecuni(3,phypa_cdr(4),rdumm)
           phypa_cdr(4:6) = phypa_cdr(3)*phypa_cdr(4:6)
        else if(words(1)=='PENAL') then                 ! Penalty parameter
           phypa_cdr(7) = param(1)
        else if(words(1)=='SPEED') then                 ! Speed of sound
           phypa_cdr(8) = param(1)
        else if(words(1)=='GRAVI') then
           phypa_cdr( 9) = getrea('NORM ',0.0_rp,'#Gravity norm')
           phypa_cdr(10) = getrea('GX   ',0.0_rp,'#x-component of g')
           phypa_cdr(11) = getrea('GY   ',0.0_rp,'#y-component of g')
           phypa_cdr(12) = getrea('GZ   ',0.0_rp,'#z-component of g')
           call vecuni(3,phypa_cdr(10),rdumm)
        else if(words(1)=='THERM') then                 ! Thermal conductivity
           phypa_cdr(13) = param(1)
        else if(words(1)=='SPECI') then                 ! Specific heat
           phypa_cdr(14) = param(1)
        else if(words(1)=='GAMMA') then                 ! Gamma (= c_p/c_v)
           phypa_cdr(15) = param(1)
        else if(words(1)=='INITI') then                 ! Initial thermodynamic pressure
           phypa_cdr(16) = param(1)
        else if(words(1)=='THPRE') then                 ! Thermodynamic pressure equation
           if(exists('MASS ')) kfl_thpeq_cdr = 1
           if(exists('ENERG')) kfl_thpeq_cdr = 2
        end if
     end do
     if( phypa_cdr( 8) < zero_rp ) phypa_cdr( 8) = 1.0_rp
     if( phypa_cdr(15) < zero_rp ) phypa_cdr(15) = 1.4_rp
     ! R / c_p = gamma-1/gamma
     phypa_cdr(17) = (phypa_cdr(15)-1.0_rp)/phypa_cdr(15)
     ! Ideal gas consntant R
     phypa_cdr(18) = phypa_cdr(14)*phypa_cdr(17)
!
! Read data for the Compressible flow equations
!
  else if(kfl_modul(modul)==8) then
     kfl_fdiff_cdr = 1                                  ! Divergence form of viscous term
     do while(words(1)/='ENDPH')
        call ecoute('cdr_reaphy')
        if(words(1)=='TEMPO') then                      ! Temporal evolution
           if(exists('ON   ')) kfl_timei_cdr = 1
        else if(words(1)=='LAWVI') then                 ! Law for mu
           if(words(2)=='CONST') lawvi_cdr=0
           if(words(2)=='POWER') lawvi_cdr=1
           if(words(2)=='CARRE') lawvi_cdr=2
           if(words(2)=='PERZY') lawvi_cdr=3
        else if(words(1)=='VISCO') then                 ! Viscosity
           visco_cdr=param(1:10)
        else if(words(1)=='DENSI') then                 ! Density
           phypa_cdr(1) = param(1)
        else if(words(1)=='PERME') then                 ! Permeability
           phypa_cdr(2) = param(1)
        else if(words(1)=='ANGUL') then                 ! Angular velocity
           phypa_cdr(3) = getrea('NORM ',0.0_rp,'#Axes rotation norm')
           phypa_cdr(4) = getrea('OX   ',0.0_rp,'#x-component of w')
           phypa_cdr(5) = getrea('OY   ',0.0_rp,'#y-component of w')
           phypa_cdr(6) = getrea('OZ   ',0.0_rp,'#z-component of w')
           call vecuni(3,phypa_cdr(4),rdumm)
           phypa_cdr(4:6) = phypa_cdr(3)*phypa_cdr(4:6)
        else if(words(1)=='PENAL') then                 ! Penalty parameter
           phypa_cdr(7) = param(1)
        else if(words(1)=='SPEED') then                 ! Speed of sound
           phypa_cdr(8) = param(1)
        else if(words(1)=='GRAVI') then
           phypa_cdr( 9) = getrea('NORM ',0.0_rp,'#Gravity norm')
           phypa_cdr(10) = getrea('GX   ',0.0_rp,'#x-component of g')
           phypa_cdr(11) = getrea('GY   ',0.0_rp,'#y-component of g')
           phypa_cdr(12) = getrea('GZ   ',0.0_rp,'#z-component of g')
           call vecuni(3,phypa_cdr(10),rdumm)
!           phypa_cdr(10:12)=phypa_cdr(9)*phypa_cdr(10:12)
        else if(words(1)=='THERM') then                 ! Thermal conductivity
           phypa_cdr(13) = param(1)
        else if(words(1)=='SPECI') then                 ! Specific heat
           phypa_cdr(14) = param(1)
        end if
        if(phypa_cdr(8).lt.zero_rp) phypa_cdr(8) = 1.0_rp
     end do

  end if
!
! Initialization
!
  if(kfl_timei_cdr==0) then
     dtinv_cdr=1.0_rp
  else
     kfl_timei=1
  end if

end
