subroutine cdr_initpr
!-----------------------------------------------------------------------
!****f* Codire/cdr_initpr
! NAME 
!    cdr_initpr
! DESCRIPTION
!    This subroutine checks the method for the determination of the 
!    thermodynamic pressure and performs initial calculations. The 
!    variables are:
!    - thpre_cdr:  p_th              (thermodynamic pressure)
!    - dtpdt_cdr:  d/dt p_th         (thermodynamic pressure time drivative)
!    - vinvt_cdr:  int_V 1/T         (R/p_th * total mass)
!    - sitfl_cdr:  int_S 1/T n.u     (1/T flow on the boundary)
!    - s_vfl_cdr:  int_S n.u         (volume flow on the boundary)
!    - s_hfl_cdr:  int_S k n.grad(u) (heat flow on the boundary)
! USES
! USED BY
!    cdr_begste
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  implicit none
  real(rp)     :: tmass
  integer(ip)  :: ipoin,kpoin,ibopo,idime,kdime
!
! Initial thermodynamic pressure
!
  thpre_cdr(1) = phypa_cdr(16)
  thpre_cdr(2) = thpre_cdr(1)
!
! Check if only Dirichlet conditions are prescribed for the velocity to
! determine if the problem is open or closed
!
  kpoin = 0
  do ipoin = 1,npoin
     ibopo = lpoty(ipoin)
     if(ibopo.gt.0) then
        kdime = 0
        do idime = 1,ndime
           if(kfl_fixno_cdr(idime,ipoin)==1) kdime = kdime + 1
        end do
        if(kdime==ndime) kpoin = kpoin + 1
     end if
  end do
!
! For open problems set pth=pth(0) and dpth/dt=0.
! For closed problems. For steady problems check that thpre is being determined
! from the global mass comservation equation.
!
  if(kpoin/=nbopo) then
     dtpdt_cdr = 0.0_rp
     kfl_thpeq_cdr = 0
  else if( kpoin==nbopo) then
     if(kfl_timei_cdr==0.and.kfl_thpeq_cdr==2) &
        call runend('INITPR: THPRE CANNOT BE INTEGRATED BY ENERGY CONSERVATION')
     if(kfl_thpeq_cdr==0) kfl_thpeq_cdr = 1
  end if
!
! Calculate integerals of initial data
!
  if(kfl_thpeq_cdr==0) then

     ! Evaluate the integral of the initial density
     call cdr_vinvte(ndofn_cdr,uncdr(1,1,3),vinvt_cdr(1))
     tmass=vinvt_cdr(1)*thpre_cdr(1)/phypa_cdr(18)
     write(lun_thpre_cdr,100) tmass

     ! Calculate volume flow on the boundary
     call cdr_smflow(1,ndofn_cdr,uncdr(1,1,3),s_vfl_cdr(1))

     ! Calculate mass flow on the boundary
     call cdr_smflow(0,ndofn_cdr,uncdr(1,1,3),sitfl_cdr(1))

  elseif(kfl_thpeq_cdr==1) then

     ! Evaluate the integral of the initial density
     call cdr_vinvte(ndofn_cdr,uncdr(1,1,3),vinvt_cdr(1))
     tmass=vinvt_cdr(1)*thpre_cdr(1)/phypa_cdr(18)
     write(lun_thpre_cdr,100) tmass

     ! Calculate inverse of temperature flow on the boundary
     call cdr_smflow(0,ndofn_cdr,uncdr(1,1,3),sitfl_cdr(1))
     if(abs(sitfl_cdr(1)) < zero_rp) then
        kfl_tmass_cdr=0
     else
        kfl_tmass_cdr=1
     end if

     ! Save initial values
     vinvt_cdr(2) = vinvt_cdr(1)
     sitfl_cdr(2) = sitfl_cdr(1)

  else if(kfl_thpeq_cdr==1) then

     ! Calculate volume flow on the boundary
     call cdr_smflow(1,ndofn_cdr,uncdr(1,1,3),s_vfl_cdr(1))

     ! Calculate heat flow on the boundary
     call cdr_shflow(uncdr(1,1,3),s_hfl_cdr(1),kfl_modul(modul))

     ! Save initial values
     s_vfl_cdr(2) = s_vfl_cdr(1)
     s_hfl_cdr(2) = s_hfl_cdr(1)

  end if


100   format(5x,'Total initial mass:',5x,e20.13,/,  &
             5x,'-------------------')
110   format(10x,i15,4x,e20.13,7x,e20.13)

end subroutine cdr_initpr

