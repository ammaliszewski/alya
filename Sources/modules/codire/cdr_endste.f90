subroutine cdr_endste
  !-----------------------------------------------------------------------
  !****f* Codire/cdr_endste
  ! NAME 
  !    cdr_endste
  ! DESCRIPTION
  !    This routine ends a time step of the CDR equation.
  ! USES
  !    tem_cvgunk
  !    tem_updunk
  !    tem_output
  ! USED BY
  !    Codire
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_codire
  implicit none
  !
  ! Compute convergence residual of the time evolution (that is,
  ! || u(n,*,*) - u(n-1,*,*)|| / ||u(n,*,*)||) and update unknowns
  ! u(n-1,*,*) <-- u(n,*,*) 
  if(kfl_stead_cdr==0.and.kfl_timei_cdr==1) then
     call cdr_cvgunk(three)
     call cdr_updunk( five)
  end if
  !
  ! Output results corresponding to the end of a time step
  !
  call cdr_output(zero)
  !
  ! Write restart file
  !
  call cdr_restar(two)
  !
  ! If not steady, go on
  !
  if(kfl_stead_cdr==0) then
     if(kfl_timei_cdr==1) kfl_gotim = 1
  end if

end subroutine cdr_endste
