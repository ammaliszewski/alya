subroutine cdr_reaous
!-----------------------------------------------------------------------
!****f* Codire/cdr_reaous
! NAME
!    cdr_reaous
! DESCRIPTION
!    This routine reads the output strategy for the CDR equations.
! USED BY
!    cdr_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_codire
  use      def_domain
  use      mod_memchk
  implicit none
  integer(ip)   :: ipara,pnodb,iboun,ipsta,ipoin,idime
  integer(ip)   :: knodb(mnodb)
  integer(4)    :: istat
  character(5)  :: word1
  character(35) :: word2
!
! Initializations.
!
  kfl_outsp_cdr = 0                ! No surface plot
  kfl_outfm_cdr = 0                ! No output of forces and moments
  kfl_outfl_cdr = 0                ! No output of fluxes
  kfl_exacs_cdr = 0                ! Exact solution: u = 0
  nou_unksi_cdr = 1e9              ! Output step interval u
  nou_stlsi_cdr = 1e9              ! Output step interval s.l.
  nou_famsi_cdr = 1e9              ! Output step interval forces and moments.
  out_timun_cdr = 0.0_rp           ! Output times for u 
  out_timsl_cdr = 0.0_rp           ! Output times for s.l.
  npp_unksi_cdr = 1e9              ! Postp. step interval u
  npp_stlsi_cdr = 1e9              ! Postp. step interval s.l.
  pos_timun_cdr = 0.0_rp           ! Postp. times for u
  pos_timsl_cdr = 0.0_rp           ! Postp. times for s.l.
  nou_secsi_cdr = 1e9              ! Plotting of sections
  lptra_cdr     = 0                ! No nodes to be tracked
  nptra_cdr     = 0                ! No points to be tracked
  csect_cdr(1)  = 1e9_rp
  csect_cdr(2)  = 1e9_rp
  csect_cdr(3)  = 1e9_rp
!
! Reach the section.
!
  rewind(lisda)
  call ecoute('cdr_reaous')
  do while(words(1)/='OUTPU')
     call ecoute('cdr_reaous')
  end do
!
! Begin to read data.
!
  do while(words(1)/='ENDOU')
     call ecoute('cdr_reaous')

      ! Points to be tracked.
        if(words(1)=='TRACK') then
           do while(words(1).ne.'ENDTR')
              call ecoute('cdr_reaous')
              if(kfl_timei_cdr==1) then
                 if(words(1)=='NODAL') then
                    do ipara = 1,5
                       lptra_cdr(ipara) = int(param(ipara))
                    end do
                 else if(words(1)=='POINT') then
                    ipara=1
                    nptra_cdr=int(param(ipara))
                    do ipoin = 1,nptra_cdr
                       do idime = 1,3
                          ipara=ipara+1
                          cptra_cdr(idime,ipoin) = int(param(ipara))
                       end do
                    end do
                 end if
              end if
           end do

      ! Sections.
        else if(words(1)=='PLOTS') then
           nou_secsi_cdr = getint('STEPS',int(1e9),'#Sections interval')
           csect_cdr(1)  = getrea('XSECT',1.0e9_rp,'#x-section')
           csect_cdr(2)  = getrea('YSECT',1.0e9_rp,'#y-section')
           csect_cdr(3)  = getrea('ZSECT',1.0e9_rp,'#z-section')

      ! Forces and Moments.
        else if(words(1)=='FORCE') then
           do ipara = 1,3
              adimf_cdr(ipara) = 1.0_rp
              adimm_cdr(ipara) = 1.0_rp
              origm_cdr(ipara) = 0.0_rp
           end do
           do while(words(1).ne.'ENDFO')
              call ecoute('cdr_reaous')
              if(words(1)=='STEPS') then
                 kfl_outfm_cdr = 1
                 nou_famsi_cdr = getint &
                    ('STEPS',int(1e9),'#Computation interval')
              else if(words(1)=='APPLI') then
                 kfl_outfm_cdr = 1
                 origm_cdr(1) = getrea &
                    ('XCOOR',0.0_rp,'#x-coordinate')
                 origm_cdr(2) = getrea &
                    ('YCOOR',0.0_rp,'#y-coordinate')
                 origm_cdr(3) = getrea &
                    ('ZCOOR',0.0_rp,'#z-coordinate')
              else if(words(1)=='OUTPU') then
                 kfl_outfm_cdr = 1
                 kfl_outfl_cdr = 1
              else if(words(1)=='FORCE') then
                 kfl_outfm_cdr = 1
                 if(ndofn_cdr.le.6)then
                    word1='1FACT'
                    word2='#1-factor '
                    do ipara=1,ndofn_cdr
                       write(word1(1:1),'(i1)') ipara
                       write(word2(2:2),'(i1)') ipara
                       adimf_cdr(ipara) = getrea(word1,1.0_rp,word2)
                    end do
                 end if
              else if(words(1)=='MOMEN') then
                 kfl_outfm_cdr = 1
                 adimm_cdr(1) = getrea &
                    ('XFACT',1.0_rp,'#x-coordinate')
                 adimm_cdr(2) = getrea &
                    ('YFACT',1.0_rp,'#x-coordinate')
                 adimm_cdr(3) = getrea &
                    ('ZFACT',1.0_rp,'#x-coordinate')
              else if(words(1)=='BODYD') then
                 kfl_outfm_cdr = 1
                 nbody_cdr = 0
                 if(nboun==0) call runend('cdr_reaous: BODY DEFINITION NEEDS BOUNDARIES')
                 allocate(lbody_cdr(nboun),stat=istat)
                 call memchk(zero,istat,mem_modul(1:2,modul),'LBODY_CDR','cdr_reaous',lbody_cdr)
                 call ecoute('cdr_reaous')
                 do while(words(1).ne.'ENDBO')
                    if(kfl_autbo==1) then
                       pnodb=int(param(2))
                       knodb(1:pnodb)=int(param(3:3+pnodb))
                       call finbou(pnodb,knodb,iboun)
                       if(iboun==0) then
                          write(lun_outpu,101)&
                             'EDGE IS NOT EXTERNAL: '//&
                             'BODY DEFINITION CANNOT BE IMPOSED'
                       end if
                       ipsta=3+pnodb
                    else
                       iboun=int(param(1))
                       pnodb=nnode(ltypb(iboun))
                       ipsta=2
                    end if
                    lbody_cdr(iboun) = int(param(ipsta))
                    nbody_cdr = max(nbody_cdr,lbody_cdr(iboun))
                    call ecoute('cdr_reaous')
                 end do
              end if
           end do
           if(nbody_cdr==0 .and. kfl_outfm_cdr/=0) &
              call runend('cdr_reaous: FORCES AND MOMENTS NEED BODY DEFINITION')

      ! Output.
        else if(words(1)=='OUTPU') then
           if(words(2)=='SURFA') then                 ! Surface plot
              kfl_outsp_cdr = 1
           end if

      ! Post-process.
        else if(words(1)=='POSTP') then
           if(words(2)=='UNKNO') then                      ! Post-process of unknowns
              npp_unksi_cdr = getint('STEPS',1,'#Unknown postp. step interval')
              if(exists('ATTIM')) pos_timun_cdr=param(4:13)
           else if(words(2)=='STREA') then                 ! Post-process of streamlines
              npp_stlsi_cdr = getint( &
              'STEPS',1,'#Streamlines postp. step interval')
              if(exists('ATTIM')) pos_timsl_cdr=param(4:13)
           end if
        end if
  end do
!
! Formats
!
100 format(5x,'ERROR:   ',a)
101 format(5x,'WARNING: ',a)

end subroutine cdr_reaous

