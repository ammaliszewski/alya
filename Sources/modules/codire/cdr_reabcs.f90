subroutine cdr_reabcs
!-----------------------------------------------------------------------
!****f* Codire/cdr_reabcs
! NAME
!    cdr_reabcs
! DESCRIPTION
!    This routine reads the boundary conditions for the CDR equations.
!
!    For conditions on boundaries, bvnat(iboun) is allocated ONLY if a
!    condition is imposed on it. Moreover, its length depends on the
!    type of condition:
!
!         Neumann ... fixbo(iboun)=2 -> bvnat(iboun)%(1,nnodb)
!         Robin ..... fixbo(iboun)=3 -> bvnat(iboun)%(3,nnodb)
!
! USED BY
!    cdr_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      def_codire
  use      mod_memchk
  implicit none
  integer(ip) :: ipoin,pnodb,iboun,inodb,idofn,icode,jcode,idirc
  integer(ip) :: knodb(mnodb),ipsta,ifunp,ifunc,nfunp
  integer(4)  :: istat
!
! Initialization
!
  kfl_conbc_cdr = 0                                    ! Non-Constant boundary conditions
!
! Allocate memory
!
  allocate(kfl_fixno_cdr(ndofn_cdr,npoin),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FIXNO_CDR','cdr_reabcs',kfl_fixno_cdr)
  allocate(kfl_fixbo_cdr(ndofn_cdr,nboun),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FIXBO_CDR','cdr_reabcs',kfl_fixbo_cdr)
  allocate(bvnat_cdr(ndofn_cdr,nboun),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'BVNAT_CDR',    'cdr_reabcs',bvnat_cdr)
!
! Reach the nodal-wise section.
!
  rewind(lisda)
  call ecoute('cdr_reabcs')
  do while(words(1)/='BOUND')
     call ecoute('cdr_reabcs')
  end do
!
! Constant/variable boundary conditions
!
  if(exists('CONST')) then
     kfl_conbc_cdr = 1 
     allocate(bvess_cdr(ndofn_cdr,npoin,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'BVESS_CDR',    'cdr_reabcs',bvess_cdr)
  else
     kfl_conbc_cdr = 0
     allocate(kfl_funno_cdr(npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FUNNO_CDR','cdr_reabcs',kfl_funno_cdr)
     allocate(kfl_funty_cdr(10,2), stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'KFL_FUNTY_CDR','cdr_reabcs',kfl_funty_cdr)
     allocate(funpa_cdr(10),       stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'FUNPA_CDR',    'cdr_reabcs',funpa_cdr)
     allocate(bvess_cdr(ndofn_cdr,npoin,2),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'BVESS_CDR',    'cdr_reabcs',bvess_cdr)
  end if
!
! Read data.
!
  call ecoute('cdr_reabcs')
  do while(words(1)/='ENDBO')
     ! On nodes
     if(words(1)=='ONNOD') then                            ! Conditions on nodes
        if(kfl_conbc_cdr==1) then
           call ecoute('cdr_reabcs')
           do while(words(1)/='ENDON')
              ipoin = int(param(1))                               ! Read condition
              icode = int(param(2))
              do idofn = 1,ndofn_cdr                              ! Decod iffix
                 jcode = icode/10**(ndofn_cdr-idofn)
                 kfl_fixno_cdr(idofn,ipoin) = jcode
                 icode = icode - kfl_fixno_cdr(idofn,ipoin)*10**(ndofn_cdr-idofn)
              end do
              bvess_cdr(1:ndofn_cdr,ipoin,1) = param(3:2+ndofn_cdr)     ! Read values
              call ecoute('cdr_reabcs')
           end do
        else
           call ecoute('cdr_reabcs')
           do while(words(1)/='ENDON')
              ipoin = int(param(1))                                     ! Read condition
              icode = int(param(2))
              do idofn = 1,ndofn_cdr                                    ! Decod iffix
                 jcode = icode/10**(ndofn_cdr-idofn)
                 kfl_fixno_cdr(idofn,ipoin) = jcode
                 icode = icode - kfl_fixno_cdr(idofn,ipoin)*10**(ndofn_cdr-idofn)
              end do
              bvess_cdr(1:ndofn_cdr,ipoin,1) = param(3:2+ndofn_cdr)     ! Read values
              kfl_funno_cdr(ipoin)     = int(param(3+ndofn_cdr))
              if( (kfl_fixno_cdr(    1,ipoin)==1.or.&
                   kfl_fixno_cdr(    2,ipoin)==1.or.&
                   kfl_fixno_cdr(ndime,ipoin)==1).and.kfl_funno_cdr(ipoin)/=0) then
                 bvess_cdr(1:ndofn_cdr,ipoin,2)=bvess_cdr(1:ndofn_cdr,ipoin,1)
              end if
              call ecoute('cdr_reabcs')
           end do
        end if
     else if(words(1)=='ONBOU') then
        call ecoute('cdr_reabcs')
        do while(words(1)/='ENDON')
           ! Automatic boundaries: look for boundary number
           if(kfl_autbo==1) then
              pnodb=int(param(2))
              knodb(1:pnodb)=int(param(3:3+pnodb))
              call finbou(pnodb,knodb,iboun)
              if(iboun==0) then
                 write(lun_outpu,101)&
                      'BOUNDARY '//intost(iboun)//' IS NOT EXTERNAL: '//&
                      'BOUNDARY CONDITON CANNOT BE IMPOSED'
              end if
              ipsta=3+pnodb
           else
              iboun=int(param(1))
              pnodb=nnode(ltypb(iboun))
              ipsta=2
           end if
           if(iboun/=0) then
              icode = int(param(ipsta))
              do idofn = 1,ndofn_cdr
                 jcode = icode/10**(ndofn_cdr-idofn)
                 kfl_fixbo_cdr(idofn,iboun) = jcode
                 icode = icode - kfl_fixbo_cdr(idofn,ipoin)*10**(ndofn_cdr-idofn)
              end do
              do idofn=1,ndofn_cdr
                 if(kfl_fixbo_cdr(idofn,iboun)==1) then
                    write(lun_outpu,101)&
                         'DIRICHLET BOUNDARY CONDITIONS ON BOUNDARIES'//&
                         'CANNOT BE IMPOSED (NOT IMPLEMENTED ON CDR)'
                 elseif(kfl_fixbo_cdr(idofn,iboun)==2) then
                    allocate(bvnat_cdr(idofn,iboun)%a(1,pnodb),stat=istat)
                    call memchk(zero,istat,memor_dom,'BVNAT_CDR','cdr_reabcs',bvnat_cdr(idofn,iboun)%a)
                    do inodb=1,pnodb
                       bvnat_cdr(idofn,iboun)%a(1,inodb)=param(inodb+2)
                    end do
                 else if(kfl_fixbo_cdr(idofn,iboun)==3) then
                    allocate(bvnat_cdr(idofn,iboun)%a(3,pnodb),stat=istat)
                    call memchk(zero,istat,memor_dom,'BVNAT_CDR','cdr_reabcs',bvnat_cdr(idofn,iboun)%a)
                    do inodb=1,pnodb
                       bvnat_cdr(idofn,iboun)%a(1,inodb)=param(1+(inodb-1)*3+2)
                       bvnat_cdr(idofn,iboun)%a(2,inodb)=param(1+(inodb-1)*3+3)
                       bvnat_cdr(idofn,iboun)%a(3,inodb)=param(1+(inodb-1)*3+4)
                    end do
                 end if
              end do
           end if
           call ecoute('cdr_reabcs')
        end do
     else if(words(1)=='FUNCT') then
        call ecoute('cdr_reabcs')
        do while(words(1)/='ENDFU')
           if(kfl_conbc_cdr==0) then
              ifunc=getint('FUNCT',1,'#FUNCTION NUMBER')
              if(ifunc<0.or.ifunc>10) then
                 call runend('cdr_reabcs: WRONG FUNCION NUMBER')
              else
                 if(words(2)=='PARAB') then
                    kfl_funty_cdr(ifunc,1)=1
                    kfl_funty_cdr(ifunc,2)=7
                 else if(words(2)=='PERIO') then
                    kfl_funty_cdr(ifunc,1)=2
                    kfl_funty_cdr(ifunc,2)=6
                 else if(words(2)=='DISCR') then
                    kfl_funty_cdr(ifunc,1)=3
                    kfl_funty_cdr(ifunc,2)=2*int(param(1))
                 elseif(words(2)=='SPECI') then
                    kfl_funty_cdr(ifunc,1)=4
                    kfl_funty_cdr(ifunc,2)=2
                 else
                    kfl_funty_cdr(ifunc,1)=0
                 end if 
                 if(kfl_funty_cdr(ifunc,1)>0) then
                    allocate(funpa_cdr(ifunc)%a(kfl_funty_cdr(ifunc,2)),stat=istat)
                    call memchk(zero,istat,memor_dom,'FUNPA_cdr','cdr_reabcs',funpa_cdr(ifunc)%a) 
                    if(kfl_funty_cdr(ifunc,1)==3) then
                       ifunp=0
                       nfunp=kfl_funty_cdr(ifunc,2)/2
                       do while(words(1)/='ENDDI')
                          ifunp=ifunp+1
                          if(ifunp>nfunp)call runend('DISREA: WRONG DIRICHLET FUNCTION FILE')
                          funpa_cdr(ifunc)%a((ifunp-1)*2+1)=param(1)
                          funpa_cdr(ifunc)%a((ifunp-1)*2+2)=param(2)
                          call ecoute('cdr_reabcs')
                       end do
                       ! Order the function field
                       call ordena(nfunp,funpa_cdr(ifunc)%a)
                    else if(kfl_funty_cdr(ifunc,1)<3) then
                       funpa_cdr(ifunc)%a(1:7)=param(3:9)
                    else if(kfl_funty_cdr(ifunc,1)==4) then
                       funpa_cdr(ifunc)%a(1:2)=param(3:4)
                    end if
                 end if
              end if
           end if
           call ecoute('cdr_reabcs')
        end do
     else 
        if(kfl_conbc_cdr==1) then
           ipoin = int(param(1))
           icode = int(param(2))
           do idofn = 1,ndofn_cdr
              jcode = icode/10**(ndofn_cdr-idofn)
              kfl_fixno_cdr(idofn,ipoin) = jcode
              icode = icode - kfl_fixno_cdr(idofn,ipoin)*10**(ndofn_cdr-idofn)
           end do
           bvess_cdr(1:ndofn_cdr,ipoin,1) = param(3:2+ndofn_cdr)
        else
           ipoin = int(param(1))
           icode = int(param(2))
           do idofn = 1,ndofn_cdr
              jcode = icode/10**(ndofn_cdr-idofn)
              kfl_fixno_cdr(idofn,ipoin) = jcode
              icode = icode - kfl_fixno_cdr(idofn,ipoin)*10**(ndofn_cdr-idofn)
           end do
           bvess_cdr(1:ndofn_cdr,ipoin,1) = param(3:2+ndofn_cdr)
           bvess_cdr(1:ndofn_cdr,ipoin,2) = bvess_cdr(1:ndofn_cdr,ipoin,1)
           kfl_funno_cdr(ipoin)           = int(param(3+ndofn_cdr))
        end if
     end if
     call ecoute('cdr_reabcs')
  end do
!
! Check if there are enough Dirichlet conditions.
!
  idirc = 0
  do ipoin = 1,npoin
     do idofn = 1,ndofn_cdr
        if(kfl_fixno_cdr(idofn,ipoin)==1) idirc = idirc + 1
     end do
  end do
  if(idirc<ndofn_cdr) call runend('cdr_reabcs: NOT ENOUGH DIRICHLET CONDITIONS')
!
!  call cdrbty
!
101 format(5x,'WARNING: ',a)
!
end subroutine cdr_reabcs



