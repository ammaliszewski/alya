subroutine cdr_inisol
!-----------------------------------------------------------------------
!
! This routine loads the solver data for the CDR equations. In
! general, it may change from time step to time step or even from
! iteration to iteration.
!
!-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_codire
  use def_solver
  implicit none

end subroutine cdr_inisol


