subroutine cdr_turnon
!-----------------------------------------------------------------------
!****f* Codire/cdr_turnon
! NAME
!    cdr_turnon
! DESCRIPTION
!    This routine performs the following tasks:
!    - Gets file names and open them.
!    - Read data for the CDR equations.
!    - Write some info
!    - Allocate memory
! USES
!    cdr_openfi
!    cdr_reaphy
!    cdr_reabcs
!    cdr_reanut
!    cdr_reaous
!    cdr_outinf
!    cdr_memall
! USED BY
!    codire
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_codire
  use      mod_iofile
  implicit none
!
! Open files
!
  call cdr_openfi
!
! Read the physical problem.
!
  call cdr_reaphy
!
! Read the boundary conditions.
!
  call cdr_reabcs
!
! Read the numerical treatment.
!
  call cdr_reanut
!
! Read the output strategy.
!
  call cdr_reaous
!
! Write info
!
  call cdr_outinf
!
! Warnings and errors
!
  call cdr_outerr
!
! Allocate memory
!
  call cdr_memall
!
! Read restart file
!
  call cdr_restar(one)
!
! Close files
!
  call iofile(two,lun_pdata_cdr,' ','CODIRE DATA')

end subroutine cdr_turnon
