subroutine cdr_assmat(ndofn,nnode,mevat,nequa,algso,lnods,lpcdr,elmat,amatr)
!-----------------------------------------------------------------------
!
! This routine performs the assembly for the CDR equation
!
!-----------------------------------------------------------------------
  use      def_kintyp
  use      def_solver
  implicit none
  integer(ip), intent(in)    :: ndofn,nnode,mevat,nequa
  integer(ip), intent(in)    :: algso
  integer(ip), intent(in)    :: lpcdr(nequa)
  integer(ip), intent(in)    :: lnods(nnode)
  real(rp),    intent(in)    :: elmat(mevat,mevat)
  real(rp),    intent(inout) :: amatr(nesky_sol)
!
! Direct solver
!
  if(algso==0) then
     call skyase(elmat,amatr,nnode,1,ndofn,mevat,nequa,lnods,lpcdr,3)
!
! Iterative solvers
!
  else
     call csrase(elmat,amatr,ndofn,nnode,mevat,lnods,2)
  end if

end subroutine cdr_assmat
