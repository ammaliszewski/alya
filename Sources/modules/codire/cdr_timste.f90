subroutine cdr_timste
!-----------------------------------------------------------------------
!****f* Codire/cdr_timste
! NAME 
!    cdr_timste
! DESCRIPTION
!    This routine computes the time step
! USES
!    cdr_iniunk
!    cdr_updtss
!    cdr_updbcs
!    cdr_updunk
!    cdr_radvuf
! USED BY
!    Codire
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master
  use def_codire
  implicit none
!
! For the first time step, set up the initial condition and 
! initialize kfl_stead_cdr.     
!
  if(ittim==0) then
     kfl_stead_cdr = 0
     call cdr_iniunk
     theta_cdr(1) =  1.0_rp
     theta_cdr(2) = -1.0_rp
     theta_cdr(3) =  0.0_rp
  else
     if(kfl_schem_cdr==1.and.kfl_tiacc_cdr==2) then
        theta_cdr(1) =  1.5_rp
        theta_cdr(2) = -2.0_rp
        theta_cdr(3) =  0.5_rp
     else
        theta_cdr(1) =  1.0_rp
        theta_cdr(2) = -1.0_rp
        theta_cdr(3) =  0.0_rp
     end if
  end if
 if(kfl_stead_cdr==1) return
!
! Time step size.
!
!  dtinv_cdr = dtin0
!  if(kfl_timei_cdr==0) dtinv_cdr = 0.0_rp
!  weigh_cdr = 0.0_rp
!  if(kfl_weigh_cdr==1) weigh_cdr = dtinv_cdr

end subroutine cdr_timste

      
