subroutine cdr_elmdir(ndofn,nnode,mnode,npoin,lnods, &
                      kfl_fixno_cdr,bvess_cdr,elmat,elrhs)
!------------------------------------------------------------------------
!
! This routine modifies the element stiffness matrix to impose boundary 
! conditions
!
!------------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip), intent(in)    :: ndofn,nnode,mnode,npoin
  integer(ip), intent(in)    :: kfl_fixno_cdr(ndofn,npoin),lnods(nnode)
  real(rp),    intent(in)    :: bvess_cdr(ndofn,npoin)
  real(rp),    intent(inout) :: elmat(ndofn,mnode,ndofn,mnode),elrhs(ndofn,mnode)

  real(rp)                   :: adiag
  integer(ip)                :: ipoin,inode,idofn,jnode,jdofn

     do inode=1,nnode
        ipoin=lnods(inode)
        do idofn=1,ndofn
           if(kfl_fixno_cdr(idofn,ipoin)==1) then
              adiag=elmat(idofn,inode,idofn,inode)
              do jnode=1,nnode
                 do jdofn=1,ndofn
                    elmat(idofn,inode,jdofn,jnode)=0.0_rp
                    elrhs(jdofn,jnode)=elrhs(jdofn,jnode)-elmat(jdofn,jnode,idofn,inode)*bvess_cdr(idofn,ipoin)
                    elmat(jdofn,jnode,idofn,inode)=0.0_rp
                 end do
              end do
              elmat(idofn,inode,idofn,inode)=adiag
              elrhs(idofn,inode)=adiag*bvess_cdr(idofn,ipoin)
           end if
        end do
     end do

end subroutine cdr_elmdir
