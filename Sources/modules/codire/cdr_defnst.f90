subroutine cdr_defnst(kfdif,lawvi,ncomp,ndofn,nnode,mnode,  &
                      ndime,phypa,visco,welin,staco,hleng,  &
                      shape,cartd,elcod,elunk,masma,dires,  &
                      cores,flres,sores,dites,cotes,sotes,  &
                      tauma,force,grunk)
!------------------------------------------------------------------------
!****f* Codire/cdr_defnst
! NAME 
!    cdr_defnst
! DESCRIPTION
!    This routine obtains the coefficients of the CDR equations for the
!    Navier-Stokes case, including the matrix of stabilization parameters.
! USES
! USED BY
!    cdr_defmat
!***
!-----------------------------------------------------------------------
  use def_parame
  implicit none
  integer(ip), intent(in)  :: kfdif,lawvi,ncomp,ndofn,nnode,mnode,ndime
  real(rp),    intent(in)  :: phypa(20),visco(10),welin(20),staco(10)
  real(rp),    intent(in)  :: hleng(2)
  real(rp),    intent(in)  :: shape(nnode)
  real(rp),    intent(in)  :: cartd(ndime,mnode)
  real(rp),    intent(in)  :: elcod(ndime,mnode)
  real(rp),    intent(in)  :: elunk(ndofn,mnode,ncomp)
  real(rp),    intent(out) :: masma(ndofn,ndofn)
  real(rp),    intent(out) :: dires(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(out) :: cores(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: flres(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: sores(ndofn,ndofn)
  real(rp),    intent(out) :: dites(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(out) :: cotes(ndofn,ndofn,ndime)
  real(rp),    intent(out) :: sotes(ndofn,ndofn)
  real(rp),    intent(out) :: tauma(ndofn,ndofn)
  real(rp),    intent(out) :: force(ndofn)
  real(rp)                 :: grunk(ndime,ndofn)
  integer(ip), save        :: ipass
  real(rp),    save        :: densi,viscp,diffu,perme,react,soun2
  real(rp)                 :: vx,vy,vz
  real(rp)                 :: advec,hdisi,tauc1,tauc2
  integer(ip)              :: idime,jdime,kdime,ldime,inode,idofn
  data ipass/0/
!
! Constant coefficients.
!
  if(ipass==0) then
     densi = phypa(1)
     perme = phypa(2)/densi
     react = phypa(3)          ! = sqrt(phypa(4)**2+phypa(5)**2+phypa(6)**2)
     soun2 = phypa(8)*phypa(8)

     if(lawvi==0) then
        call cdr_vislaw(zero_rp,viscp,visco,grunk,0.0_rp,lawvi,ndime)
        diffu = viscp/densi
        call cdr_defdif(kfdif,ndofn,ndime,diffu,dires)
     end if

     sores(1,1) =  perme
     sores(2,2) =  perme
     sores(1,2) = -phypa(6)
     sores(2,1) =  phypa(6)

     if(ndime.eq.2) then
        masma(1,1) = 1.0_rp
        masma(2,2) = 1.0_rp
        masma(3,3) = 1.0_rp/soun2
        cores(1,3,1) = 1.0_rp
        cores(3,1,1) = 1.0_rp
        cores(2,3,2) = 1.0_rp
        cores(3,2,2) = 1.0_rp
        flres(1,3,1) = 1.0_rp
        flres(3,1,1) = 0.0_rp
        flres(2,3,2) = 1.0_rp
        flres(3,2,2) = 0.0_rp
        sores(3,3) = phypa(7)
     else if(ndime.eq.3) then
        masma(1,1) = 1.0_rp
        masma(2,2) = 1.0_rp
        masma(3,3) = 1.0_rp
        masma(4,4) = 1.0_rp/soun2
        dires(3,3,1,1) = diffu
        dires(3,3,2,2) = diffu
        dires(1,1,3,3) = diffu
        dires(2,2,3,3) = diffu 
        dires(3,3,3,3) = diffu 
        cores(1,4,1) = 1.0_rp
        cores(4,1,1) = 1.0_rp
        cores(2,4,2) = 1.0_rp
        cores(4,2,2) = 1.0_rp
        cores(3,4,3) = 1.0_rp
        cores(4,3,3) = 1.0_rp
        flres(1,4,1) = 1.0_rp
        flres(4,1,1) = 0.0_rp
        flres(2,4,2) = 1.0_rp
        flres(4,2,2) = 0.0_rp
        flres(3,4,3) = 1.0_rp
        flres(4,3,3) = 0.0_rp
        sores(3,3) =  perme
        sores(4,4) =  phypa(7)
        sores(1,3) =  phypa(5)
        sores(3,1) = -phypa(5)
        sores(2,3) = -phypa(4)
        sores(3,2) =  phypa(4)
     end if

     dites = dires   ! Test functions
     cotes = cores
     sotes = sores

     ipass = 1
  end if
!
! Coefficients that depend on the iteration.
!
  vx=0.0_rp
  vy=0.0_rp
  do inode=1,nnode
     vx = vx + shape(inode)*elunk(1,inode,1)
     vy = vy + shape(inode)*elunk(2,inode,1)
  end do
  if(ndime==3) then
     vz=0.0_rp
     do inode=1,nnode
        vz = vz + shape(inode)*elunk(3,inode,1)
     end do
  end if
  if( lawvi>0 .or. welin(1)>zero_rp ) then
     grunk= 0.0_rp
     do inode=1,nnode
        do idofn=1,ndofn
           do idime=1,ndime
              grunk(idime,idofn) = grunk(idime,idofn) + cartd(idime,inode)*elunk(idofn,inode,1)
           end do
        end do
     end do
  end if
  if(lawvi>0) then
     call cdr_vislaw(zero_rp,viscp,visco,grunk,0.0_rp,lawvi,ndime)
     diffu = viscp/densi
     call cdr_defdif(kfdif,ndofn,ndime,diffu,dires)
     dites = dires            ! Test function
  end if

  advec = sqrt(vx*vx+vy*vy+vz*vz)
  hdisi = 1.0_rp/hleng(2)                   ! Min element length
  tauc1 = staco(1)*diffu*hdisi*hdisi &
    +     staco(2)*advec*hdisi       &
    +     staco(3)*react             &
    +     perme
  if(tauc1>zero_rp) tauc1 = 1.0_rp/tauc1
  tauc2 = staco(4)*diffu             &
    +     staco(5)*advec*hleng(2)    &
    +     staco(6)*react*hleng(2)*hleng(2)
  tauma(1,1) = tauc1
  tauma(2,2) = tauc1
  if(ndime==2) then
     tauma(3,3) =  tauc2
  else if(ndime==3) then
     tauma(3,3) = tauc1
     tauma(4,4) = tauc2
  end if

  cores(1,1,1) = vx
  cores(2,2,1) = vx
  cores(1,1,2) = vy
  cores(2,2,2) = vy
  if(ndime.eq.3) then
     cores(3,3,1) = vx
     cores(3,3,2) = vy
     cores(1,1,3) = vz
     cores(2,2,3) = vz
     cores(3,3,3) = vz
  end if
  cotes = cores ! Test function

  if(welin(1)>0.1_rp) then
     sores(1,1) =   perme + welin(1)*grunk(1,1)
     sores(2,2) =   perme + welin(1)*grunk(2,2)
     sores(1,2) =  -phypa(6) + welin(1)*grunk(2,1)
     sores(2,1) =   phypa(6) + welin(1)*grunk(1,2)
     force(1) = welin(1)*vx*grunk(1,1) + welin(1)*vy*grunk(2,1)
     force(2) = welin(1)*vx*grunk(1,2) + welin(1)*vy*grunk(2,2)
     if(ndime.eq.3) then
        sores(3,3) =  perme + welin(1)*grunk(3,3)
        sores(1,3) =  phypa(5) + welin(1)*grunk(3,1)
        sores(3,1) = -phypa(5) + welin(1)*grunk(1,3)
        sores(2,3) = -phypa(4) + welin(1)*grunk(3,2)
        sores(3,2) =  phypa(4) + welin(1)*grunk(2,3)
        force(1) = force(1)+ welin(1)*vz*grunk(3,1)
        force(2) = force(2)+ welin(1)*vz*grunk(3,2)
        force(3) = welin(1)*vx*grunk(1,3) + welin(1)*vy*grunk(2,3) &
                 + welin(1)*vz*grunk(3,3)
     end if
  else
     sores = 0.0_rp
     sores(1,1) =  perme
     sores(2,2) =  perme
     sores(1,2) = -phypa(6)
     sores(2,1) =  phypa(6)
     force(1) = 0.0_rp
     force(2) = 0.0_rp
     if(ndime.eq.2) then
        sores(3,3) = phypa(7)
     else if(ndime.eq.3) then
        sores(3,3) =  perme
        sores(4,4) =  phypa(7)
        sores(1,3) =  phypa(5)
        sores(3,1) = -phypa(5)
        sores(2,3) = -phypa(4)
        sores(3,2) =  phypa(4)
        force(3) = 0.0_rp
     end if
  end if
! sotes = sores

end

