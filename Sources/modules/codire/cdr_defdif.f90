subroutine cdr_defdif(kfdif,ndofn,ndime,diffu,dires)
!------------------------------------------------------------------------
!****f* Codire/cdr_defdif
! NAME 
!    cdr_defdif
! DESCRIPTION
!    This routine defines the diffusion matrix.
! USES
! USED BY
!    cdr_defnst
!    cdr_defbou
!    cdr_deflom
!***
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none
  integer(ip),intent(in)  :: kfdif,ndofn,ndime
  real(rp)   ,intent(in)  :: diffu
  real(rp)   ,intent(out) :: dires(ndofn,ndofn,ndime,ndime)
  real(rp)                :: delta(3,3)
  integer(ip)             :: idime,jdime,kdime,ldime
  data  delta/1.0_rp, 0.0_rp, 0.0_rp, 0.0_rp, 1.0_rp, 0.0_rp, 0.0_rp, 0.0_rp, 1.0_rp /

  if(kfdif==0) then
!
!    Laplacian form of viscous term:
!
     do idime=1,ndime
        do jdime=1,ndime
           do kdime=1,ndime
              do ldime=1,ndime
                 dires(kdime,ldime,idime,jdime) =  &  
                 ! mu divj diuj
                 diffu * delta(kdime,ldime) * delta(idime,jdime)
              end do
           end do
        end do
     end do

  else if(kfdif==1) then
!
!    Divergence form of viscous term:
!
     do idime=1,ndime
        do jdime=1,ndime
           do kdime=1,ndime
              do ldime=1,ndime
                 dires(kdime,ldime,idime,jdime) =  & 
                 !mu divj diuj + mu divj djui + 2mu/3 divi djuj
                 diffu * delta(kdime,ldime) * delta(idime,jdime) + &
                 diffu * delta(kdime,jdime) * delta(ldime,idime) + &
                 (2.0_rp/3.0_rp) * diffu * delta(kdime,idime) * delta(ldime,jdime)
              end do
           end do
        end do
     end do

  end if


end subroutine cdr_defdif
