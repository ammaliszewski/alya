subroutine cdr_lstarg
!-----------------------------------------------------------------------
!****f* Codire/cdr_lstarg
! NAME 
!    cdr_lstarg
! DESCRIPTION
!    This routine evaluates the target function F(U) and stores it in
!    rhsid. This routine is similar to cdr_matrix.
! USES
! USED BY
!    cdr_linse
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  use def_cdrloc
  implicit none
!  include 'cdr_elmvar.h'     ! Automatic arrays to be used inside elmope
  integer(ip) :: ielem,pelty,pnode
  integer(ip) :: iboun,pblty,pnodb
  integer(ip) :: inode,jnode,idofn,jdofn,ipoin,ixcdr
!
! Allocate local variables
!
  call cdr_memelm(one)
  call cdr_membou(one)
  call cdr_memass(one)
!
! Initializations
!
  rhsid=0.0_rp
!
! Loop over elements
!
  elements: do ielem=1,nelem
     
     ! Initialize
     pelty=ltype(ielem)
     pnode=nnode(pelty)

     ! Element matrix and right hand side
     call cdr_elmope(ielem,pnode,pelty,kfl_modul(modul),ncomp_cdr,ndofn_cdr,uncdr)

     do inode=1,pnode
        do idofn=1,ndofn_cdr
           elrhs(idofn,inode)=-elrhs(idofn,inode)
           do jnode=1,pnode
              do jdofn=1,ndofn_cdr
                 elrhs(idofn,inode) = elrhs(idofn,inode)         &
                   +  elmat(idofn,inode,jdofn,jnode) * elunk(jdofn,jnode,1)
              end do
           end do
        end do
     end do

     ! Assembly
     call cdr_assrhs(ndofn_cdr,pnode,mnode,lnods(1,ielem),elrhs,rhsid)

  end do elements
!
! Loop over boundary
!
  if(nboun>0) then

     boundaries: do iboun=1,nboun
     
        if( maxval(kfl_fixbo_cdr(:,iboun)) > 0 .or.  &
            (kfl_modul(modul)==8.and.kfl_syseq_cdr==2) ) then

        pblty=ltypb(iboun)
        pnodb=nnode(pblty)
        ielem=lboel(pnodb+1,iboun)
        pelty=ltype(ielem)
        pnode=nnode(pelty)

        call cdr_bouope(iboun,pblty,pnodb,ielem,pelty,pnode, &
                        kfl_modul(modul),ncomp_cdr,ndofn_cdr,uncdr)


        do inode=1,pnode
           do idofn=1,ndofn_cdr
              elrhs(idofn,inode)=-elrhs(idofn,inode)
              do jnode=1,pnode
                 do jdofn=1,ndofn_cdr
                    elrhs(idofn,inode) = elrhs(idofn,inode)         &
                      +  elmat(idofn,inode,jdofn,jnode) * elunk(jdofn,jnode,1)
                 end do
              end do
           end do
        end do

!        do inode=1,pnode
!           do idofn=1,ndofn_cdr
!              elrhs(idofn,inode)=-elrhs(idofn,inode)
!           end do
!        end do

        ! Assembly
        call cdr_assrhs(ndofn_cdr,pnode,mnode,lnods(1,ielem),elrhs,rhsid)

        end if

     end do boundaries

  end if
!
! Set F=0 on Dirichlet nodes
!
  do ipoin=1,npoin
     do idofn=1,ndofn_cdr
        if(kfl_fixno_cdr(idofn,ipoin)==1) then
           ixcdr=(ipoin-1)*ndofn_cdr+idofn
           rhsid(ixcdr)=0.0_rp
        end if
     end do
  end do
!
! Deallocate local variables
!
  call cdr_memelm(two)
  call cdr_membou(two)
  call cdr_memass(two)

end subroutine cdr_lstarg
