subroutine cdr_memall
!-----------------------------------------------------------------------
!****f* Codire/cdr_memall
! NAME
!    cdr_memall
! DESCRIPTION
!    This routine allocates memory for the arrays needed to solve the
!    CDR equations.
! USED BY
!    cdr_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      def_solver
  use      def_codire
  use      mod_memchk
  use      mod_iofile
  implicit none
  integer(ip) :: lodof !,knodb,iboun
  integer(4)  :: istat
!
! Allocate memory for CDR unknwons 
!
  ncomp_cdr = 3
  if(kfl_twost_cdr==1) ncomp_cdr = 4
  allocate(uncdr(ndofn_cdr,npoin,ncomp_cdr),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'UNCDR','memunk',uncdr)
!
! Dimensions required for the CDR equations (recall that the
! memory for the physical properties has been allocated in
! cdr_reaphy).
!
  lodof     = ndofn_cdr
  nusol     = max(nusol,1)
  mxdof     = max(mxdof,lodof)

end subroutine cdr_memall
