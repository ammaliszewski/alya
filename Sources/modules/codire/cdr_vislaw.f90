subroutine cdr_vislaw(zero_rp,visce,visco,grave,vtemp,kvila,ndime)
!------------------------------------------------------------------------
!****f* Codire/cdr_vislaw
! NAME 
!    cdr_vislaw
! DESCRIPTION
!    This routine computes the viscosity as a dependent variable
! USES
! USED BY
!    cdr_defnst
!    cdr_defbou
!    cdr_deflom
!***
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none
  integer(ip) :: kvila,ndime,i,j
  real(rp)    :: zero_rp,visce,vtemp,totem,seci4
  real(rp)    :: secib,zetap,sigma
  real(rp)    :: visco(10)
  real(rp)    :: grave(ndime,ndime)
!
! Constant viscosity.
!
  if(kvila==0) then
     visce=visco(1)
!
! Power law: mu = mu0 [4 I2]^((n-1)/2) exp (b/T), where:
! VISCO = [ mu0 , n , b , cut-off ].
!
  else if(kvila==1) then 
     seci4 = 0.0_rp
     do i = 1,ndime
        do j = 1,ndime
           seci4 = seci4+grave(i,j)*(grave(i,j)+grave(j,i))
        end do
     end do
     visce=visco(4)
     if(seci4 > zero_rp) then
        totem=0.0_rp
        if(visco(3) > zero_rp) totem=visco(3)/(273.0_rp+vtemp)
        visce=visco(1)*(seci4**(visco(2)*0.5_rp-0.5_rp))*exp(totem)
     end if
!
! Carreau model: mu = mu0 [1+ 4 l^2 I2]^((n-1)/2) exp (b/T), where:
! VISCO = [ mu0 , l , n , b , cut-off ].
!
  else if(kvila==2) then 
     seci4 = 0.0_rp
     do i = 1,ndime
        do j = 1,ndime
           seci4 = seci4+grave(i,j)*(grave(i,j)+grave(j,i))
        end do
     end do
     visce=visco(5)
     if(seci4.gt.zero_rp) then
        totem=0.0_rp
        if(visco(4) > zero_rp) totem=visco(4)/(273.0_rp+vtemp)
        visce=visco(1)*(1.0_rp+visco(2)*visco(2)* &
              seci4**(visco(3)*0.5_rp-0.5_rp))*exp(totem)
     end if
!
! Perzyna's model with a Sellars-Tegart yield stress variation:
! mu = {sigma + [g1*sqrt(4 I2/3)]^(1/m)}/sqrt(3.0 * 4 I2), with
! sigma = 1/p1 * log { (Z/p2)^(p3) + sqrt [(Z/p2)^(2 p3) + 1]} and
! Z = sqrt(4 I2 / 3) * exp(Q/RT), where
! VISCO = [ p1 , p2 , p3 , Q , R , g1 , m , cut-off ].
!
  else if(kvila.eq.3) then
     seci4 = 0.0_rp
     do i = 1,ndime
        do j = 1,ndime
           seci4 = seci4 +grave(i,j)*(grave(i,j)+grave(j,i))
        end do
     end do
     secib = sqrt(seci4/3.0_rp)
     if(secib.gt.zero_rp) then
        visce=visco(8)
     else
        totem=0.0_rp
        if(visco(4).gt.zero_rp) totem=visco(4)/(273.0_rp+vtemp)
        zetap = secib*exp(totem/visco(5))
        sigma = 1.0_rp/visco(1)*log((zetap/visco(2))**visco(3)+       &
                sqrt((zetap/visco(2))**(2.0_rp*visco(3))+1.0_rp))
        visce = (sigma + (visco(6)*secib)**(1.0_rp/visco(7)))         &
                /(3.0_rp*secib)
        if(visce.gt.visco(8)) visce=visco(8)
     end if
  end if

end subroutine cdr_vislaw


