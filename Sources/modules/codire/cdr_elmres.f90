subroutine cdr_elmres(ndime,nnode,ntens,ndofn,ncomp,        &
                      llapl,kfl_timei_cdr,kfl_weigh_cdr,    &
                      theta,dires_cdr,cores_cdr,flres_cdr,  &
                      sores_cdr,force_cdr,masma_cdr,        &
                      olunk,dtinv,shape,cartd,hessi,        &
                      vlapl,zero_rp,resma,resfo)
!------------------------------------------------------------------------
!****f* Codire/cdr_elmres
! NAME 
!    cdr_elmres
! DESCRIPTION
!    This routine computes the residuals of the CDR equation
!    within each element
! USED BY
!    cdr_elmope
!***
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none

  integer(ip), intent(in)  :: ndime,nnode,ntens,ndofn,ncomp,llapl
  integer(ip), intent(in)  :: kfl_timei_cdr,kfl_weigh_cdr
  real(rp),    intent(in)  :: theta(3)
  real(rp),    intent(in)  :: dires_cdr(ndofn,ndofn,ndime,ndime)
  real(rp),    intent(in)  :: cores_cdr(ndofn,ndofn,ndime)
  real(rp),    intent(in)  :: flres_cdr(ndofn,ndofn,ndime)
  real(rp),    intent(in)  :: sores_cdr(ndofn,ndofn)
  real(rp),    intent(in)  :: force_cdr(ndofn)
  real(rp),    intent(in)  :: masma_cdr(ndofn,ndofn)
  real(rp),    intent(in)  :: olunk(ndofn,ncomp-2)
  real(rp),    intent(in)  :: dtinv
  real(rp),    intent(in)  :: shape(nnode)
  real(rp),    intent(in)  :: cartd(ndime,nnode)
  real(rp),    intent(in)  :: hessi(ntens,nnode*llapl)
  real(rp),    intent(in)  :: zero_rp
  real(rp),    intent(out) :: resma(ndofn,ndofn,nnode)
  real(rp),    intent(out) :: resfo(ndofn)

  integer(ip)              :: inode,idofn,jdofn,idime,itens
  real(rp)                 :: vlapl(ntens*llapl),wpro1,wpro2
!
   resma=0.0_rp
   resfo=0.0_rp
!
! Residual due to the evolution in time.
  if((kfl_timei_cdr>=1).and.(kfl_weigh_cdr>=1)) then
!      do idofn=1,ndofn
!         do jdofn=1,ndofn
!            do inode=1,nnode
!               resma(idofn,jdofn,inode) = resma(idofn,jdofn,inode)                  &
!                 +   theta(1)*dtinv*masma_cdr(idofn,jdofn)*shape(inode)
!            end do
!         end do
!      end do
!
     wpro1=theta(1)*dtinv
     do inode=1,nnode
        wpro2=wpro1*shape(inode)
        do jdofn=1,ndofn
           do idofn=1,ndofn
               resma(idofn,jdofn,inode) = resma(idofn,jdofn,inode)                  &
                 +   wpro2*masma_cdr(idofn,jdofn)
           end do
        end do
     end do
     do idofn=1,ndofn
        wpro1=( theta(2)*olunk(idofn,1)+theta(3)*olunk(idofn,ncomp-2) )* dtinv
        do jdofn=1,ndofn
            resfo(idofn) = resfo(idofn) - wpro1*masma_cdr(idofn,jdofn)
        end do
     end do
  end if

! Contribution from the advection term
!  do inode=1,nnode
!     do idofn=1,ndofn
!        do jdofn=1,ndofn
!           do idime=1,ndime
!              resma(idofn,jdofn,inode) = resma(idofn,jdofn,inode)                   &
!                +   cartd(idime,inode)*cores_cdr(idofn,jdofn,idime)
!           end do
!        end do
!     end do
!  end do
  do inode=1,nnode
     do idime=1,ndime
        do jdofn=1,ndofn
           do idofn=1,ndofn
              resma(idofn,jdofn,inode) = resma(idofn,jdofn,inode)                   &
                +   cartd(idime,inode)*cores_cdr(idofn,jdofn,idime)
           end do
        end do
     end do
  end do

  if(llapl.ne.0) then
! Contribution from the diffusive term (only K_ij d^2 N / dx_i  dx_j)
! To do: add dK_ij /dx_i dN / dx_j)
     do inode=1,nnode
        do jdofn=1,ndofn
           do idofn=1,ndofn
              call matove(vlapl,dires_cdr(idofn,jdofn,:,:),ndime,ntens)
              do itens=1,ntens
                 resma(idofn,jdofn,inode) = resma(idofn,jdofn,inode)                &
                   +   hessi(itens,inode)*vlapl(itens)
              end do
           end do
        end do
     end do
  end if

! Contribution from the reaction term
  do inode=1,nnode
     do jdofn=1,ndofn
        do idofn=1,ndofn
           resma(idofn,jdofn,inode) = resma(idofn,jdofn,inode)                      &
             +   shape(inode)*sores_cdr(idofn,jdofn)
         end do
     end do
  end do

! Contribution from the force term
  do idofn=1,ndofn
     resfo(idofn) = resfo(idofn)                                                    &
       +   force_cdr(idofn)
  end do

end subroutine cdr_elmres
