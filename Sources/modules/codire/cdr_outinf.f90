subroutine cdr_outinf
  !-----------------------------------------------------------------------
  !****f* Codire/cdr_outinf
  ! NAME
  !    cdr_outinf
  ! DESCRIPTION
  !    This routine writes on the CDR files
  ! USED BY
  !    cdr_turnon
  !***
  !-----------------------------------------------------------------------
  use      def_master
  use      def_codire
  use      mod_iofile
  implicit none
  character(60) :: equat
  !
  ! Headings
  !
  if(kfl_rstar/=2) then
     write(lun_outpu_cdr,100) title
     write(lun_solve_cdr,105) title
     if(kfl_modul(modul)==8) &
          write(lun_thpre_cdr,110) title
     if(kfl_linse_cdr > zero)  then
        call iofile(zero,lun_linse_cdr,fil_linse_cdr,'cdr_memall')
        write(lun_linse_cdr,115) title
     end if
  else
     write(lun_outpu_cdr,200)
     write(lun_solve_cdr,200)
     if(kfl_modul(modul)==8) &
          write(lun_thpre_cdr,200)
     if(kfl_linse_cdr > zero)  then
        call iofile(zero,lun_linse_cdr,fil_linse_cdr,'cdr_memall','old','formatted','append')
        write(lun_linse_cdr,200)
     end if
  end if

  !
  ! Write information in Result file
  !
  equat=''
  if(kfl_timei_cdr==1) equat=trim(equat)//'M*dU/dt'
  equat=trim(equat)//'- d/dx_i ( K_ij dU/dx_j ) + d/dx_i (A_i U) + S U = F'
  write(lun_outpu_cdr,300) trim(equat)

  select case(kfl_modul(modul))
  case(one)
     write(lun_outpu_cdr,310) 'GENERAL CDR EQUATIONS'
  case(two)
     write(lun_outpu_cdr,310) 'NAVIER STOKES EQUATIONS'
  case(three)
     write(lun_outpu_cdr,310) 'RIESSNIER MIDLIN PLATES'
  case(four)
     write(lun_outpu_cdr,310) 'HELMHOLTZ EQUATION'
  case(five)
     write(lun_outpu_cdr,310) 'CONTAMINANT TRANSPORT'
  case(six)
     write(lun_outpu_cdr,310) 'CONCENTRATION TRANSPORT'
  case(seven)
     write(lun_outpu_cdr,310) 'BOUSSINESQ EQUATIONS'
  case(eight)
     write(lun_outpu_cdr,310) 'LOW MACH NUMBER EQUATIONS'
  case(nine)
     write(lun_outpu_cdr,310) 'COMPRESSIBLE NAVIER STOKES EQUATIONS'
  end select
  !
  ! Formats
  !
100 format(///,                                                             &
       & 5x,'********************* ',/,                                        &
       & 5x,'CDR RESULTS FOR MODEL: ',a10,/,                                   &
       & 5x,'********************* ',/)
105 format(///,                                                             &
       & 5x,'******************************** ',/,                             &
       & 5x,'CDR SOLVER INFORMATION FOR MODEL: ',a10,/,                        &
       & 5x,'******************************** ',/)
110 format(///,                                                             &
       & 5x,'******************************************************* ',/,      &
       & 5x,'EVOULUTION OF THE THERMODUYNAMIC PRESSURE FOR CDR MODEL: ',a10,/, &
       & 5x,'******************************************************* ',/)
115 format(///,                                                             &
       & 5x,'******************************************************* ',/,      &
       & 5x,'CDR LINE SEARCH INFORMATION: ',a10,/,                             &
       & 5x,'******************************************************* ',//,     &
       & 10x,'g(s) = 0.5  F(u + s d) . F(u + s d)')

200 format(//,&
       & 5x, '>>>  THIS IS A TIME RESTART RUN: CONTINUING... ',///)

300 format(/,&
       & 5x,'>>>  DIFFERENTIAL EQUATION:',///,&
       & 10x,a,//)

310 format(/,&
       & 5x,'     The particular problem is',///,&
       & 10x,a,//)

end subroutine cdr_outinf
