subroutine cdr_elmgat(&
               ndofn,pnode,mnode,ndime,npoin,lnods,  &
               kfl_timei_cdr,kfl_twost_cdr,elunk,elcod,uncdr,coord)
!-----------------------------------------------------------------------
!
! Gather operations
!
!-----------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip),  intent(in) :: ndofn,pnode,mnode,ndime,npoin
  integer(ip),  intent(in) :: lnods(pnode)
  integer(ip),  intent(in) :: kfl_timei_cdr,kfl_twost_cdr
  real(rp), intent(in)     :: uncdr(ndofn,npoin,*)
  real(rp), intent(in)     :: coord(ndime,npoin)
  real(rp), intent(out)    :: elunk(ndofn,mnode,*)
  real(rp), intent(out)    :: elcod(ndime,mnode)
  integer(ip)              :: inode,ipoin,idome

  do inode=1,pnode
     ipoin=lnods(inode)
     elunk(1:ndofn,inode,1) = uncdr(1:ndofn,ipoin,1)
     elcod(1:ndime,inode)   = coord(1:ndime,ipoin)
  end do
  if(kfl_timei_cdr==1) then
     do inode=1,pnode
        ipoin=lnods(inode)
        elunk(1:ndofn,inode,2) = uncdr(1:ndofn,ipoin,3)
     end do
     if(kfl_twost_cdr==1) then
        do inode=1,pnode
           ipoin=lnods(inode)
           elunk(1:ndofn,inode,3) = uncdr(1:ndofn,ipoin,4)
        end do
     end if
  end if

end subroutine cdr_elmgat
