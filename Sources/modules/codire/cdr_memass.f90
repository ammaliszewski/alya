subroutine cdr_memass(itask)
!------------------------------------------------------------------------
!****f* Codire/cdr_memass
! NAME 
!    cdr_memass
! DESCRIPTION
!    This routine allocates memory for local assembly arrays
! USED BY
!    cdr_matrix
!    cdr_lstarg
!    cdr_forces
!***
!-----------------------------------------------------------------------
use def_parame
use def_master
use def_domain
use def_codire
use def_cdrloc
use mod_memchk
implicit none
integer(ip), intent(in) :: itask
integer(4)              :: istat

  if(itask==1) then

     ! Assembly matrices
     allocate(wmatr(ndofn_cdr,mnode,ndofn_cdr,mnode),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',wmatr)
     allocate(wrhsi(ndofn_cdr,mnode),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',wrhsi)
     allocate(elmat(ndofn_cdr,mnode,ndofn_cdr,mnode),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',elmat)
     allocate(elrhs(ndofn_cdr,mnode),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',elrhs)
     allocate(wprod(ndofn_cdr),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',wprod)

  else if(itask==2) then

     ! Assembly matrices
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',wmatr)
     deallocate(wmatr,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memass',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',wrhsi)
     deallocate(wrhsi,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memass',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',elmat)
     deallocate(elmat,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memass',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',elrhs)
     deallocate(elrhs,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memass',0_ip)
     call memchk(two,istat,mem_modul(1:2,modul),'LOCAL','cdr_memass',wprod)
     deallocate(wprod,stat=istat)
     if(istat.ne.0)  call memerr(two,'LOCAL','cdr_memass',0_ip)

  end if

end subroutine cdr_memass
