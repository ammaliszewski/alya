subroutine Codire(order)
!-----------------------------------------------------------------------
!****f* Codire/codire
! NAME
!   Codire
! DESCRIPTION
!   This is the main routine of Codire, the module that deals with CDR 
!   systems of equations.The task done corresponds to the order given 
!   by the master.
! USES
!    cdr_turnon
!    cdr_begste
!    cdr_doiter
!    cdr_gencon
!    cdr_endste
!    cdr_turnof
! USED BY
!    Turnon
!    Begste
!    Doiter
!    Gencon
!    Endste
!    Turnof
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  implicit none
  integer(ip) :: order

  select case (order)

  case(ITASK_TURNON)
     if(kfl_modul(modul)/=0) call cdr_turnon
  case(ITASK_TIMSTE) 
     if(kfl_modul(modul)/=0) call cdr_timste
  case(ITASK_BEGSTE) 
     if(kfl_modul(modul)/=0) call cdr_begste
  case(ITASK_DOITER)
     if(kfl_modul(modul)/=0) call cdr_doiter
  case(ITASK_CONCOU)
     if(kfl_modul(modul)/=0) call cdr_concou
  case(ITASK_CONBLK)
     if(kfl_modul(modul)/=0) call cdr_conblk
  case(ITASK_NEWMSH)
     if(kfl_modul(modul)/=0) call cdr_newmsh
  case(ITASK_ENDSTE)
     if(kfl_modul(modul)/=0) call cdr_endste
  case(ITASK_TURNOF)
     if(kfl_modul(modul)/=0) call cdr_turnof

  end select

end subroutine Codire
