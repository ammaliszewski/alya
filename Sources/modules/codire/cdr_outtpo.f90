subroutine cdr_outtpo
!-----------------------------------------------------------------------
!
! This routine tracks points for the CDR problem.
!
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_codire
  use mod_iofile
  implicit none
  integer(ip), save     :: ipass,ntrac
  data                     ipass/0/
  real(rp), allocatable :: xshap(:),xderi(:),xjacm(:),xjaci(:)
  real(rp), allocatable :: deltx(:),delts(:),xbbox(:),coorf(:)
  integer(ip)           :: i,itrac,ilist,ielem,ifoun,idofn,ipoin,inode,pnode
  real(rp)              :: ltole

!
! Initializations.
!
  if(ipass==0) then
     ntrac = 0
     do	i=1,5
        if(lptra_cdr(i)/=0) ntrac = ntrac + 1
     end do

     if(nptra_cdr>=1) then

        allocate( shatp(mnode,nptra_cdr) )
        allocate( unktp(ndofn_cdr,nptra_cdr) )

        allocate( xshap(mnode))
        allocate( xderi(ndime*mnode) )
        allocate( xjacm(ndime*ndime) )
        allocate( xjaci(ndime*ndime) )
        allocate( deltx(ndime) )
        allocate( delts(ndime) )
        allocate( xbbox(2*ndime) )
        allocate( coorf(ndime) )
 
        ltole=1.0e-4_rp
        do ilist=1,nptra_cdr
           !call  Elsest(1,2,0,1,ielem,ifoun,npoin,nnode,nelem,  &
           !             nelty,nnode,ndime,ip,rp,0,0,0,0,0,1,    &
           !             namod(modul),coord,lnods,cptra_cdr(1,ilist),&
           !             xshap,xderi,xjacm,xjaci,deltx,delts,    &
           !             coorf,ltole,xbbox)
           eltra(ilist)   = ielem
           shatp(:,ilist) = xshap
        end do
        ilist=1
        !call  Elsest(2,2,0,1,ielem,ifoun,npoin,nnode,nelem,  &
        !             nelty,nnode,ndime,ip,rp,0,0,0,0,0,1,    &
        !             namod(modul),coord,lnods,cptra_cdr(1,ilist),&
        !             xshap,xderi,xjacm,xjaci,deltx,delts,    &
        !             coorf,ltole,xbbox)

        deallocate(xshap)
        deallocate(xderi)
        deallocate(xjacm)
        deallocate(xjaci)
        deallocate(deltx)
        deallocate(delts)
        deallocate(xbbox)
        deallocate(coorf)
        
     end if

     if(ndofn_cdr>=1) then
        call iofile(zero,lun_trap1_cdr,fil_trap1_cdr,'CDR TRACKING OF POINT 1')
        rewind(lun_trap1_cdr)
        write (lun_trap1_cdr,10) (lptra_cdr(itrac)         ,itrac=1,ntrac)
        write (lun_trap1_cdr,11) (coord(1,lptra_cdr(itrac)),itrac=1,ntrac)
        write (lun_trap1_cdr,12) (coord(2,lptra_cdr(itrac)),itrac=1,ntrac)
        if(ndime.eq.3) &
        write (lun_trap1_cdr,13) (coord(3,lptra_cdr(itrac)),itrac=1,ntrac)         
        write (lun_trap1_cdr,15)
        ipass = 1
     end if
     if(ndofn_cdr>=2) then
          call iofile(zero,lun_trap2_cdr,fil_trap2_cdr,'CDR TRACKING OF POINT 2')
          rewind(lun_trap2_cdr)
          write (lun_trap2_cdr,10) (lptra_cdr(itrac)         ,itrac=1,ntrac)
          write (lun_trap2_cdr,11) (coord(1,lptra_cdr(itrac)),itrac=1,ntrac)
          write (lun_trap2_cdr,12) (coord(2,lptra_cdr(itrac)),itrac=1,ntrac)
          if(ndime.eq.3) &
          write (lun_trap2_cdr,13) (coord(3,lptra_cdr(itrac)),itrac=1,ntrac)         
          write (lun_trap2_cdr,15)
     end if
     if(ndofn_cdr>=3) then
          call iofile(zero,lun_trap3_cdr,fil_trap3_cdr,'CDR TRACKING OF POINT 3')
          rewind(lun_trap3_cdr)
          write (lun_trap3_cdr,10) (lptra_cdr(itrac)         ,itrac=1,ntrac)
          write (lun_trap3_cdr,11) (coord(1,lptra_cdr(itrac)),itrac=1,ntrac)
          write (lun_trap3_cdr,12) (coord(2,lptra_cdr(itrac)),itrac=1,ntrac)
          if(ndime.eq.3) &
          write (lun_trap3_cdr,13) (coord(3,lptra_cdr(itrac)),itrac=1,ntrac)         
          write (lun_trap3_cdr,15)
     end if
     if(ndofn_cdr>=4) then
          call iofile(zero,lun_trap4_cdr,fil_trap4_cdr,'CDR TRACKING OF POINT 4')
          rewind(lun_trap4_cdr)
          write (lun_trap4_cdr,10) (lptra_cdr(itrac)         ,itrac=1,ntrac)
          write (lun_trap4_cdr,11) (coord(1,lptra_cdr(itrac)),itrac=1,ntrac)
          write (lun_trap4_cdr,12) (coord(2,lptra_cdr(itrac)),itrac=1,ntrac)
          if(ndime.eq.3) &
          write (lun_trap4_cdr,13) (coord(3,lptra_cdr(itrac)),itrac=1,ntrac)         
          write (lun_trap4_cdr,15)
     end if
     if(ndofn_cdr>=5) then
          call iofile(zero,lun_trap5_cdr,fil_trap5_cdr,'CDR TRACKING OF POINT 5')
          rewind(lun_trap5_cdr)
          write (lun_trap5_cdr,10) (lptra_cdr(itrac)         ,itrac=1,ntrac)
          write (lun_trap5_cdr,11) (coord(1,lptra_cdr(itrac)),itrac=1,ntrac)
          write (lun_trap5_cdr,12) (coord(2,lptra_cdr(itrac)),itrac=1,ntrac)
          if(ndime.eq.3) &
          write (lun_trap5_cdr,13) (coord(3,lptra_cdr(itrac)),itrac=1,ntrac)         
          write (lun_trap5_cdr,15)
     end if

  end if
!
! Writes temporal evolution.
!
  if(nptra_cdr>=1) then
     do ilist=1,nptra_cdr
        pnode=nnode(ltype(eltra(ilist)))
        unktp=0.0_rp
        do inode=1,pnode
           ipoin=lnods(inode,ielem)
           do idofn=1,ndofn_cdr
              unktp(idofn,ilist) = unktp(idofn,ilist)  &
                + shatp(inode,ilist)*uncdr(idofn,ipoin,1)
           end do
        end do
     end do
  end if

  if(ndofn_cdr>=1) then
     write(lun_trap1_cdr,'(6(1x,e14.7))') &
           cutim,(uncdr(1,lptra_cdr(itrac),1),itrac = 1,ntrac) &
                ,(unktp(1,itrac),itrac=1,nptra_cdr)
     call flush(lun_trap1_cdr)
  end if
  if(ndofn_cdr>=2) then
     write(lun_trap2_cdr,'(6(1x,e14.7))') &
           cutim,(uncdr(2,lptra_cdr(itrac),1),itrac = 1,ntrac) &
                ,(unktp(1,itrac),itrac=1,nptra_cdr)
     call flush(lun_trap2_cdr)
  end if
  if(ndofn_cdr>=3) then
     write(lun_trap3_cdr,'(6(1x,e14.7))') &
           cutim,(uncdr(3,lptra_cdr(itrac),1),itrac = 1,ntrac) &
                ,(unktp(1,itrac),itrac=1,nptra_cdr)
     call flush(lun_trap3_cdr)
  end if
  if(ndofn_cdr>=4) then
     write(lun_trap4_cdr,'(6(1x,e14.7))') &
           cutim,(uncdr(4,lptra_cdr(itrac),1),itrac = 1,ntrac) &
                ,(unktp(1,itrac),itrac=1,nptra_cdr)
     call flush(lun_trap4_cdr)
  end if
  if(ndofn_cdr>=5) then
     write(lun_trap5_cdr,'(6(1x,e14.7))') &
           cutim,(uncdr(5,lptra_cdr(itrac),1),itrac = 1,ntrac) &
                ,(unktp(1,itrac),itrac=1,nptra_cdr)
     call flush(lun_trap5_cdr)
  end if

   10 format(1x,'Nodal Point number:  ',5(1x,i12  ))
   11 format(1x,'X-Coordinate:        ',5(1x,e14.7))
   12 format(1x,'Y-Coordinate:        ',5(1x,e14.7))
   13 format(1x,'Z-Coordinate:        ',5(1x,e14.7))
   15 format(1x,/, &
             1x,'Time  Point1  Point2 ...',/, &
             1x,'------------------------')
  return

end subroutine cdr_outtpo

