subroutine wav_turnof
  !-----------------------------------------------------------------------
  !****f* Wavequ/wav_turnof
  ! NAME 
  !    wav_turnof
  ! DESCRIPTION
  !    This routine closes the run for the temperature equation
  ! USES
  !    wav_outcpu
  !    wav_output
  ! USED BY
  !    Wavequ
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_wavequ
  use      def_solver
  implicit none
 
end subroutine wav_turnof

