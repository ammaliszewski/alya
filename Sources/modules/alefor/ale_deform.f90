subroutine ale_deform()
  !-----------------------------------------------------------------------
  !****f* domain/ale_deform
  ! NAME
  !    domain
  ! DESCRIPTION
  !    This routines ale_deformes the mesh. Idea: Put a node at the middle
  !    of its neighbors. A Gauss Seidel in 1D would give:
  !
  !    X^{k+1}(i) = 1/2 [  X^k(i-1) + X^k(i+1) ]
  !    X^{k+1}(i) = X^k(i) + 1/2 [ X^k(i-1) -2 X^k(i) + X^k(i+1) ]
  !    X^{k+1}(i) = X^k(i) + 1/2 h^2 [ X^k(i-1) -2 X^k(i) + X^k(i+1) ]/h^2
  !    X^{k+1}(i) = X^k(i) + h^2 [ 0 - Lapl(X) ]
  !
  !    Let X = x0+d. x0 is initial solution. d is displacement.
  !    Solve div[ a * grad(X) ] = 0 with X=x0 on boundary
  ! 
  !    In 1D:
  !
  !    i-1   i   i+1
  !     o----o----o
  !        h    h
  !    +-
  !    |  a* grad(X).grad(v) dx =   a*h* [X(i)-X(i-1)]/h (1/h) 
  !   -+                          + a*h* [X(i+1)-X(i)]/h (-1/h) 
  !                             = -alpha*h* Lapla(X)
  !   => a = h
  !
  ! USED BY
  !    Turnon 
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_parame
  use def_elmtyp
  use def_domain
  use def_alefor
  use mod_ker_deform
  implicit none
  real(rp),  pointer :: coord_loc(:,:)

  coord_loc => coord_ale(:,:,1)

  call deform_deform(&  
       ndefo_ale,kfl_defor_ale,ID_ALEFOR,kfl_fixno_ale,bvess_ale,&
       coord_loc,amatr,unkno,rhsid,solve_sol)
  
end subroutine ale_deform
