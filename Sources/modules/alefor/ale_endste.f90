subroutine ale_endste()
  !-----------------------------------------------------------------------
  !****f* Alefor/ale_endste
  ! NAME 
  !    ale_endste
  ! DESCRIPTION
  !    This routine ends a time step of the ALE formulation equation.
  ! USES
  !    ale_cvgunk
  !    ale_output
  ! USED BY
  !    Alefor
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_alefor
  implicit none
  !
  ! Update RB unknowns
  !
  call ale_updunk(25_ip)
  !
  ! Write restart file
  !
  call ale_restar(2_ip)
  !
  ! Update unknowns d(n-1,*,*) >-- d(n,*,*)
  !
  call ale_updunk(5_ip)

end subroutine ale_endste
