subroutine ale_elmpno(&
     pnode,pelty,lnods,lelch,elcod,asele,elmat,elrhs,rhsid_tmp)
  !-----------------------------------------------------------------------
  !****f* Alefor/ale_elmpno
  ! NAME
  !    elmpno
  ! DESCRIPTION
  !    Assemble the Laplacian-like matrix
  ! USED BY
  !    Turnon 
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_elmtyp, only     :  QUA04,HEX08,PEN06,ELEXT
  use def_domain, only     :  ndime
  use def_master, only     :  kfl_fixno_ale
  implicit none
  integer(ip), intent(in)  :: pnode
  integer(ip), intent(in)  :: pelty
  integer(ip), intent(in)  :: lnods(pnode)
  integer(ip), intent(in)  :: lelch
  real(rp),    intent(in)  :: elcod(ndime,pnode)
  real(rp),    intent(in)  :: asele
  real(rp),    intent(out) :: elmat(pnode,pnode)
  real(rp),    intent(out) :: elrhs(pnode)
  real(rp),    intent(out) :: rhsid_tmp(ndime,*)
  integer(ip)              :: inode,jnode,ipoin,idime

  do inode = 1,pnode
     do jnode = 1,pnode
        elmat(inode,jnode) = -1.0_rp / real(pnode-1,rp)
     end do
     elmat(inode,inode) = 1.0_rp
     elrhs(inode)       = 0.0_rp
  end do

  if( asele > 5.0_rp ) then

     if(       ( ndime == 2 .and. pelty == QUA04 ) &
          .or. ( ndime == 3 .and. ( pelty == HEX08 .or. pelty == PEN06 ) ) ) then
        !
        ! Fix node
        !
        !do inode = 1,pnode
        !   do jnode = 1,pnode
        !      elmat(inode,jnode) = 0.0_rp
        !   end do
        !   elmat(inode,inode) = 1.0e5_rp
        !   elrhs(inode)       = 1.0e5_rp
        !end do
        !do inode = 1,pnode
        !   ipoin = lnods(inode)
        !   kfl_fixno_ale(1,ipoin) = 1
        !end do
        !
        ! Relax stiffness
        !
        do inode = 1,pnode
           elmat(inode,inode) = elmat(inode,inode) + asele
           elrhs(inode)       = elrhs(inode)       + asele
        end do
     end if

  end if
  !
  ! Extension elements
  !
  !if( lelch == ELEXT ) then
  !   call elmext(&
  !        4_ip,1_ip,pnode,dummr,dummr,dummr,dummr,elmat,&
  !        dummr,elrhs,dummr)
  !end if
  !
  ! Assemble RHS
  !
  do inode = 1,pnode
     ipoin = lnods(inode)
     do idime = 1,ndime
        rhsid_tmp(idime,ipoin) = rhsid_tmp(idime,ipoin) + elrhs(inode) * elcod(idime,inode)
     end do
  end do  

end subroutine ale_elmpno
