subroutine ale_smodef()
  !-----------------------------------------------------------------------
  !****f* domain/ale_smodef
  ! NAME
  !    domain
  ! DESCRIPTION
  !    This routines 
  !    KFL_FIXNO_ALE(:,:) = -1 ... FMALE free
  !                       =  0 ... ALE free
  !                       =  1 ... ALE fixed
  !                       =  3 ... FMALE fixed
  ! USED BY
  !    Turnon 
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_parame
  use def_elmtyp
  use def_domain
  use def_alefor
  use mod_memchk
  implicit none
  integer(ip)    :: idime,ipoin
  character(150) :: messa

  if( kfl_defor_ale /= 0 .or. kfl_smoot_ale /= 0 .or. kfl_smobo_ale /= 0 ) then

     !-------------------------------------------------------------------
     !
     ! Copy new coordinates
     !
     !-------------------------------------------------------------------

     if( INOTMASTER ) then
        do ipoin = 1,npoin
           do idime = 1,ndime
              coord_ale(idime,ipoin,1) = coord(idime,ipoin) 
           end do
        end do
     end if

     !-------------------------------------------------------------------
     !
     ! Boundary smoothing: Sharp edges have KFL_FIXNO_ALE(1,IPOIN) = 2
     !
     !-------------------------------------------------------------------

     if( kfl_smobo_ale /= 0 ) then
        messa = intost(nsmob_ale) 
        messa = 'BOUNDARY SMOOTHING USING '//trim(messa)//' STEP'
        call livinf(79_ip,trim(messa),modul)
        if( IMASTER ) then
           call ale_smobou()
        else
           call ale_smobou()
        end if
     end if

     !-------------------------------------------------------------------
     !                   
     ! Mesh deformation
     !  
     !-------------------------------------------------------------------

     if( kfl_defor_ale /= 0 ) then
        messa = intost(ndefo_ale) 
        messa = 'MESH DEFORMATION USING '//trim(messa)//' LOADING STEPS'
        call livinf(79_ip,trim(messa),modul)

        if( IMASTER ) then
           call ale_deform()
        else
           call ale_deform()
        end if
     end if

     !-------------------------------------------------------------------
     !                                                    
     ! Mesh smoothing
     !                                                                   
     !-------------------------------------------------------------------

     if( kfl_smoot_ale /= 0 ) then
        messa = intost(nsmoo_ale) 
        messa = 'MESH SMOOTHING USING '//trim(messa)//' STEP'
        call livinf(79_ip,trim(messa),modul)
        call ale_smooth()
     end if

     !-------------------------------------------------------------------
     ! 
     ! Update displacement and mesh velocity
     ! FMALE: 1. does not update coordinate
     !        2. Invert mesh velocity
     !
     !-------------------------------------------------------------------

     call ale_updunk(3_ip)

  end if

end subroutine ale_smodef
