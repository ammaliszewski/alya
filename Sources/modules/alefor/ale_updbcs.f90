subroutine ale_updbcs()
  !-----------------------------------------------------------------------
  !****f* Alefor/ale_updbcs
  ! NAME 
  !    ale_updbcs
  ! DESCRIPTION
  !    This routine sets up the initial condition for the mesh velocity
  ! USED BY
  !    ale_begste
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_alefor
  use def_kermod
  use mod_ker_space_time_function

  implicit none
  integer(ip) :: ipoin,ifunc
  real   (rp) :: vefun(3)


  if( kfl_conbc_ale == 0 .and. INOTMASTER ) then
     !
     ! Space/Time functions
     !
     if( number_space_time_function > 0 ) then
        do ipoin = 1,npoin
           if( kfl_funno_ale(ipoin) < 0 ) then 
              ifunc = -kfl_funno_ale(ipoin)            
              call ker_space_time_function(&
                 ifunc,coord(1,ipoin),coord(2,ipoin),coord(ndime,ipoin),cutim,vefun(1))
              call ker_space_time_function(&
                 ifunc,coord(1,ipoin),coord(2,ipoin),coord(ndime,ipoin),cutim-dtime,vefun(2))
              bvess_ale(1:ndime,ipoin) = bvess_ref(1:ndime,ipoin)*(vefun(1) - vefun(2))
           end if
        end do
     else
        do ipoin = 1,npoin
           if( kfl_fixno_ale(1,ipoin) /= 0 ) then
              if( kfl_funno_ale(ipoin) /= 0 ) then 
                 ifunc = kfl_funno_ale(ipoin)
                 call ale_funcre(&
                      funpa_ale(ifunc) % a,   &
                      kfl_funty_ale(ifunc,2), &
                      kfl_funty_ale(ifunc,1), &
                      cutim,                  &
                      coord(1,ipoin),         &
                      coord_ale(1,ipoin,2),   &
                      bvess_ale(1,ipoin)      )
              end if
           end if
        end do

     end if

  end if

end subroutine ale_updbcs
