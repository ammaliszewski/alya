subroutine ale_solrbo()
  !-----------------------------------------------------------------------
  !****f* Alefor/ale_solrbo
  ! NAME
  !    ale_solrbo
  ! DESCRIPTION
  !    This routines solves the rigid body motion ! similar to ibm_solite
  ! USED BY
  !    domain
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_alefor
  implicit none
  !
  ! Obtain the initial guess for inner iterations - This is here instead of nsi_begite 
  ! because nsi_begite is used for the basic ale - mesh deform
  !
  call ale_updunk(22_ip)
  !
  ! Add forces: gravity, magnetic: F^n+1 and T^n+1
  !     
  call ale_forces()
  !
  ! Obtain accel, veloc and displacements 
  !
  if ( kfl_crist_ale == 1 ) then
     call ale_sorbcr(dtime)
  else
     call ale_sorbhh(dtime)
  end if
  !
  call ale_fixirb
  !  if( ittim /= 0 ) call livinf(164_ip,' ',1_ip) 

end subroutine ale_solrbo
