subroutine ale_smooth()
  !-----------------------------------------------------------------------
  !****f* domain/ale_smooth
  ! NAME
  !    domain
  ! DESCRIPTION
  !    Laplacian ale_smoothing using Gauss-Seidel
  ! USED BY
  !    Turnon 
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_elmtyp
  use def_parame
  use def_elmtyp
  use def_alefor
  use def_domain
  use mod_memchk
  implicit none
  integer(ip)           :: iters,ipoin,inode,idime,ielem,pelty,pnode
  integer(ip)           :: izdom,jpoin,kelem
  integer(4)            :: istat
  real(rp)              :: ri(3),ro(3),xjacm(9),gpdet,w1,w,dummr(4)
  real(rp)              :: elcod(ndime,mnode)
  real(rp)              :: gpcar(ndime,mnode,mgaus)
  real(rp)              :: hleng(3),asele
  real(rp),    pointer  :: unkno_tmp(:,:)
  real(rp),    pointer  :: rhsid_tmp(:,:)
  real(rp),    pointer  :: unkno_new(:)
  real(rp),    pointer  :: rhsid_new(:)
  real(rp),    pointer  :: amatr_tmp(:)
  real(rp),    pointer  :: invdiag(:)
  real(rp)              :: elmat(mnode,mnode)
  real(rp)              :: elrhs(mnode)

  w  = resmo_ale
  w1 = (1.0_rp-w)

  if( INOTMASTER ) then

     !-------------------------------------------------------------------
     !
     ! Assemble matrix
     !
     !-------------------------------------------------------------------

     allocate( rhsid_tmp(ndime,npoin) , stat = istat )
     call memchk(zero,istat,memor_dom,'RHSID_TMP','ale_smooth',rhsid_tmp) 

     allocate( amatr_tmp(solve_sol(1)%nzmat) , stat = istat )
     call memchk(zero,istat,memor_dom,'AMATR_TMP','ale_smooth',amatr_tmp) 

     do ipoin = 1,npoin
        if( lpoty(ipoin) /= 0 ) then
           do idime = 1,ndime
              kfl_fixno_ale(idime,ipoin) = 1
           end do
        else
           do idime = 1,ndime
              kfl_fixno_ale(idime,ipoin) = 0
           end do
        end if
        do idime = 1,ndime
           rhsid_tmp(idime,ipoin) = 0.0_rp
        end do
     end do
     do izdom = 1,nzdom
        amatr_tmp(izdom) = 0.0_rp
     end do
     !
     ! Prescribe hole nodes
     !
     do ipoin = 1,npoin
        if( lnoch(ipoin) == NOHOL ) then
           do idime = 1,ndime
              kfl_fixno_ale(idime,ipoin) = 1
           end do
        end if
     end do
     !
     ! Assemble matrix
     !
     do kelem = 1,nelez(lzone(ID_ALEFOR))
        ielem = lelez(lzone(ID_ALEFOR)) % l(kelem)
     
        if( lelch(ielem) /= ELHOL ) then
           pelty = ltype(ielem)
           pnode = nnode(pelty)
           do inode = 1,pnode
              ipoin = lnods(inode,ielem)
              do idime = 1,ndime
                 elcod(idime,inode) = coord_ale(idime,ipoin,1)
              end do
           end do
           call elmlen(&
                ndime,pnode,elmar(pelty) % dercg,xjacm,elcod,&
                hnatu(pelty),hleng)
           asele = hleng(1) / hleng(ndime)
           call ale_elmpno(&
                pnode,pelty,lnods(1,ielem),lelch(ielem),elcod,asele,&
                elmat,elrhs,rhsid_tmp) 
           call assmat(&
                1_ip,pnode,pnode,npoin,solve_sol(1) % kfl_algso,&
                ielem,lnods(1,ielem),elmat,amatr_tmp) 
        end if
     end do

     !-------------------------------------------------------------------
     !
     ! Prescribe boundary conditions
     !
     !-------------------------------------------------------------------

     do ipoin = 1,npoin 
        if( kfl_fixno_ale(1,ipoin) == 1 ) then
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              jpoin = c_dom(izdom)
              if( ipoin == jpoin ) then
                 amatr_tmp(izdom) = 1.0_rp
              else
                 amatr_tmp(izdom) = 0.0_rp
              end if
           end do
        end if
     end do
  end if

  if( kfl_smoot_ale == 1 ) then

     !-------------------------------------------------------------------
     !
     ! Implicit: Solve non-linear system
     !
     !-------------------------------------------------------------------

     allocate( rhsid_new(npoin) , stat = istat )
     call memchk(zero,istat,memor_dom,'RHSID_NEW','ale_smooth',rhsid_new) 

     allocate( unkno_new(npoin) , stat = istat )
     call memchk(zero,istat,memor_dom,'UNKNO_NEW','ale_smooth',unkno_new) 
     
     do iters = 1,nsmoo_ale

        do idime = 1,ndime

           if( INOTMASTER ) then

              do ipoin = 1,npoin 

                 unkno_new(ipoin) = coord_ale(idime,ipoin,1)
                 if( kfl_fixno_ale(1,ipoin) == 1 ) then
                    rhsid_new(ipoin) = coord_ale(idime,ipoin,1)
                 else
                    rhsid_new(ipoin) = rhsid_tmp(idime,ipoin)
                 end if
              end do

              call solver(rhsid_new,unkno_new,amatr_tmp,dummr(4)) 
 
              do ipoin = 1,npoin
                 coord_ale(idime,ipoin,1) = w1 * coord_ale(idime,ipoin,1) + w * unkno_new(ipoin)
              end do
           else
              call solver(dummr(1),dummr(2),dummr(3),dummr(4)) 
           end if

        end do
     end do

     deallocate( unkno_new , stat = istat ) 
     if(istat/=0) call memerr(two,'UNKNO_NEW','ale_smooth',0_ip)
     call memchk(two,istat,memor_dom,'unkno_new','ale_smooth',unkno_new) 
     
     deallocate( rhsid_new , stat = istat ) 
     if(istat/=0) call memerr(two,'RHSID_NEW','ale_smooth',0_ip)
     call memchk(two,istat,memor_dom,'rhsid_new','ale_smooth',rhsid_new) 
     
  else

     !-------------------------------------------------------------------
     !
     ! Gauss-Seidel
     !
     !-------------------------------------------------------------------

     if( INOTMASTER ) then

        allocate( unkno_tmp(ndime,npoin) , stat = istat )
        call memchk(zero,istat,memor_dom,'UNKNO_TMP','ale_smooth',unkno_tmp) 
        allocate( invdiag(npoin) , stat = istat )
        call memchk(zero,istat,memor_dom,'INVDIAG','ale_smooth',invdiag) 

        do ipoin = 1,npoin
           izdom = r_dom(ipoin)
           jpoin = c_dom(izdom)
           do while( jpoin /= ipoin )
              izdom = izdom + 1
              jpoin = c_dom(izdom)
           end do
           invdiag(ipoin) = amatr_tmp(izdom)
        end do
        call pararr('SLX',NPOIN_TYPE,npoin,invdiag)
        call pararr('SLX',NPOIN_TYPE,ndime*npoin,rhsid_tmp)
        do ipoin = 1,npoin
           invdiag(ipoin) = 1.0_rp / invdiag(ipoin)
        end do
        !
        ! Scale matrix: Put zero on invdiagnal
        !
        do ipoin = 1,npoin
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              jpoin = c_dom(izdom)
              if( ipoin == jpoin ) then
                 amatr_tmp(izdom) = invdiag(ipoin) * amatr_tmp(izdom) 
              else
                 amatr_tmp(izdom) = invdiag(ipoin) * amatr_tmp(izdom) 
              end if
           end do
        end do
        !
        ! Loop over iterations
        !
        do iters = 1,nsmoo_ale
           !
           ! Update interior nodes
           !
           do ipoin = 1,npoi1
              if( kfl_fixno_ale(1,ipoin) == 0 ) then

                 w = resmo_ale
                 do idime = 1,ndime
                    ri(idime) = coord_ale(idime,ipoin,1) + invdiag(ipoin) * rhsid_tmp(idime,ipoin)
                    ro(idime) = coord_ale(idime,ipoin,1)
                 end do

                 do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                    jpoin = c_dom(izdom)

                    do idime = 1,ndime
                       ri(idime) = ri(idime) - amatr_tmp(izdom) * coord_ale(idime,jpoin,1) 
                    end do
                 end do

                 do idime = 1,ndime
                    coord_ale(idime,ipoin,1) = (1.0_rp-w) * ro(idime) + w * ri(idime)
                 end do

                 if( kfl_smoot_ale == 3 ) then
                    !
                    ! Check Jacobian
                    !
                    do idime = 1,ndime
                       unkno_tmp(idime,ipoin) = coord_ale(idime,ipoin,1)
                    end do
                    izdom = pelpo(ipoin)-1
                    do while( izdom < pelpo(ipoin+1)-1 )
                       izdom = izdom + 1
                       ielem = lelpo(izdom)
                       pelty = ltype(ielem)
                       pnode = nnode(pelty)
                       do inode = 1,pnode
                          jpoin = lnods(inode,ielem)
                          do idime = 1,ndime
                             elcod(idime,inode) = coord_ale(idime,jpoin,1)
                          end do
                       end do
                       call determ(&
                            ndime,pnode,elcod,elmar(pelty) % dercg,xjacm,gpcar,gpdet)
                       if( gpdet <= 0.0_rp ) then
                          w = w / 2.0_rp
                          do idime = 1,ndime
                             coord_ale(idime,ipoin,1) = (1.0_rp-w) * ro(idime) + w * ri(idime)
                          end do
                          izdom = pelpo(ipoin) - 1
                       end if
                    end do
                 end if
              end if
           end do
           !
           ! Update boundary nodes
           !
           do ipoin = npoi1+1,npoin
              if( kfl_fixno_ale(1,ipoin) == 0 ) then
                 do idime = 1,ndime
                    unkno_tmp(idime,ipoin) = 0.0_rp
                 end do
                 do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                    jpoin = c_dom(izdom)
                    do idime = 1,ndime
                       unkno_tmp(idime,ipoin) = unkno_tmp(idime,ipoin) - amatr_tmp(izdom) * coord_ale(idime,jpoin,1) 
                    end do
                 end do
              end if
           end do

           call pararr('SLX',NPOIN_TYPE,ndime*npoin,unkno_tmp)

           do ipoin = npoi1+1,npoin
              if( kfl_fixno_ale(1,ipoin) == 0 ) then
                 do idime = 1,ndime
                    unkno_tmp(idime,ipoin) = unkno_tmp(idime,ipoin) &
                         + coord_ale(idime,ipoin,1) + invdiag(ipoin) * rhsid_tmp(idime,ipoin)
                    coord_ale(idime,ipoin,1) = (1.0_rp-w) * coord_ale(idime,ipoin,1) + w * unkno_tmp(idime,ipoin)
                 end do
              end if
           end do

        end do

        deallocate( unkno_tmp , stat = istat ) 
        if(istat/=0) call memerr(two,'UNKNO_TMP','ale_smooth',0_ip)
        call memchk(two,istat,memor_dom,'unkno_tmp','ale_smooth',unkno_tmp) 

        deallocate( invdiag , stat = istat ) 
        if(istat/=0) call memerr(two,'INVDIAG','ale_smooth',0_ip)
        call memchk(two,istat,memor_dom,'invdiag','ale_smooth',invdiag) 

     end if

  end if
  !
  ! Deallocate memory
  !
  if( INOTMASTER ) then

     deallocate( amatr_tmp , stat = istat ) 
     if(istat/=0) call memerr(two,'AMATR_TMP','ale_smooth',0_ip)
     call memchk(two,istat,memor_dom,'amatr_tmp','ale_smooth',amatr_tmp)  
    
     deallocate( rhsid_tmp , stat = istat ) 
     if(istat/=0) call memerr(two,'RHSID_TMP','ale_smooth',0_ip)
     call memchk(two,istat,memor_dom,'rhsid_tmp','ale_smooth',rhsid_tmp)  
    
  end if

end subroutine ale_smooth
