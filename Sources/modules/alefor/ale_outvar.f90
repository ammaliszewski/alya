!-----------------------------------------------------------------------
!> @addtogroup Alefor
!> @{
!> @file    ale_outvar.f90
!> @author  Guillaume Houzeaux
!> @date    16/11/1966
!> @brief   Output a postprocess variable
!> @details Output a postprocess variable
!> @} 
!-----------------------------------------------------------------------
subroutine ale_outvar(ivari)
  use def_parame
  use def_master
  use def_domain
  use def_alefor
  implicit none
  integer(ip), intent(in) :: ivari
  integer(ip)             :: idime,ipoin,kpoin
  real(rp)                :: rutim
  !
  ! Define postprocess variable
  !
  rutim = cutim

  select case (ivari)  

  case(0_ip)
     !
     ! Nothing
     !
     return

  case(1_ip)
     !
     ! DISPM: Mesh displacement
     !
!print*,'A=',displ(2,5229,1),displ(2,3070,1)
!print*,'B=',coord(2,5229) - coord_ale(2,5229,2),coord(2,3070) - coord_ale(2,3070,2)
!print*,'C=',bvess_ale(2,5229)
     if( INOTMASTER ) then
        if( coupling('ALEFOR','SOLIDZ') >= 1 ) then
           call memgen(zero,ndime,npoin)
           do ipoin = 1,npoin
              do idime = 1,ndime
                 gevec(idime,ipoin) = displ(idime,ipoin,1) + coord(idime,ipoin) - coord_ale(idime,ipoin,2)
              end do
           end do
        else
!            gevec => dispm(:,:,1)
           !
           ! Alefor postprocess the total displacement with respect to the original configuration, like solidz
           !
           call memgen(zero,ndime,npoin)
           do ipoin = 1,npoin

              do idime = 1,ndime

                 gevec(idime,ipoin) = coord(idime,ipoin) - coord_ori(idime,ipoin)

              end do

           end do

        end if

     end if

  case(2_ip)
     !
     ! Mesh velocity
     !
     if( INOTMASTER ) gevec => velom(:,:) 

  case(3_ip)
     !
     ! New coordinates
     !
     if( INOTMASTER ) gevec => coord_ale(:,:,1) 

  case(4_ip)
     !
     ! Groups
     !
     if( INOTMASTER ) then
       do ipoin = 1,npoin
           rhsid(ipoin) = real(solve(1) % lgrou(ipoin),rp)
        end do
        gesca => rhsid
     end if

  case(5_ip)
     !
     ! New coordinates
     !
     if( INOTMASTER ) gevec => bvess_ale

  end select

  call outvar(&
       ivari,&
       ittim,rutim,postp(1) % wopos(1,ivari))

end subroutine ale_outvar
