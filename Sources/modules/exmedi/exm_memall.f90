subroutine exm_memall
  !-----------------------------------------------------------------------
  !****f* Exmedi/exm_memall
  ! NAME 
  !    exm_memall
  ! DESCRIPTION
  !    This routine allocates memory for the arrays needed to solve the
  !    equations of EXMEDI
  !    Allocation is done for:
  !      - Potentials (intra- and extracellular, and transmembrane)
  !      - Subcell and no-subcell models of ionic currents
  ! USES
  !    memchk
  !    mediso
  ! USED BY
  !    exm_turnon
  !***
  !-----------------------------------------------------------------------
  !
  ! DESCRIPTION OF VARIABLES' MEANING 
  !
  !-----------------------------------------------------------------------
  !
  ! Variable: elmag
  ! 
  !     Potentials (i.e. the unknowns) are stored in elmag(1-3,npoin,ncomp_exm):
  !        - internal action potential (v) ---> elmag(1,:,:)
  !        - external action potential (u) ---> elmag(2,:,:)
  !        - FHN recovery potential        ---> refhn_exm(:,:)
  !
  !-----------------------------------------------------------------------
  !
  ! Variable: vauxi_exm
  !
  !     vauxi_exm(nauxi_exm,npoin,ncomp_exm)
  !        It is used in exm_ceauxi.f90 subroutine for computation of activation/
  !        inactivation states corresponding to Hodgkin-Huxley formalism
  !
  !-----------------------------------------------------------------------
  !
  ! Variable: 

  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      def_solver
  use      def_exmedi
  use      mod_memchk

  implicit none
  integer(ip)    :: lodof,ncdof,nzrhs_exm
  integer(4)     :: istat,imate

  ! FOR SUBCELLULAR IONIC CURRENTS MODELS (TT, LR, BR, ...)

  ncdof= 1
  nzrhs_exm     = 1
  ngate_exm = 1    ! Default for fhn

  !
  ! Derived dimensions according to the general algorithm type
  !
  if(kfl_genal_exm==1 .or. kfl_genal_exm==2)  then
     lodof     = 1                              ! explicit or decoupled
  else if(kfl_genal_exm==3)  then
     lodof     = ndofn_exm                      ! monolithic
  end if

  nauxi_exm = 1
  nconc_exm = 1
  nicel_exm = 1
  ncdof     = 1    

  do imate= 1,nmate

     if (kfl_gemod_exm == 0) then
        if (kfl_spmod_exm(imate) == 11) then
           nauxi_exm = max(nauxi_exm,1)    ! Default value of number of variables used for activation/inactivation 
           nconc_exm = max(nconc_exm,1)    ! Default value of number of variables used for concentrations
           nicel_exm = max(nicel_exm,1)
        else if (kfl_spmod_exm(imate) == 0) then
           nauxi_exm = max(nauxi_exm,1)    ! Default value of number of variables used for activation/inactivation 
           nconc_exm = max(nconc_exm,1)    ! Default value of number of variables used for concentrations
           nicel_exm = max(nicel_exm,1)
        else if (kfl_spmod_exm(imate) == 12) then
           call runend("EXM_MEMALL: FENTON KARMA MODELS NOT UPDATED")
           nconc_exm = 2
           ngate_exm = 2
        end if
     else if (kfl_gemod_exm == 1) then
        if (kfl_spmod_exm(imate) == 1) then
           nauxi_exm = max(nauxi_exm,12)  
           nconc_exm = max(nconc_exm, 8)  
           ncdof = npoin
        else if (kfl_spmod_exm(imate) == 4) then
           nauxi_exm = max(nauxi_exm,12)  
           nconc_exm = max(nconc_exm,10)  
           nicel_exm = max(nconc_exm,18)  
           ncdof = npoin
        else if (kfl_spmod_exm(imate) == 5) then
           nauxi_exm = max(nauxi_exm,29)  
           nconc_exm = max(nconc_exm,11)  
           nicel_exm = max(nconc_exm,26)  
           ncdof = npoin
        else if (kfl_spmod_exm(imate) == 6) then
           nauxi_exm = max(nauxi_exm,14)  
           nconc_exm = max(nconc_exm,10)  
           nicel_exm = max(nconc_exm,18)  
           ncdof = npoin
        end if
     end if

  end do


  if(kfl_timei_exm==1) ncomp_exm = 2 + kfl_tiacc_exm

  ! kfl_gemod_exm = 0 --> No-Subcellular models for ionic currents: FHN, FENTON...
  ! kfl_gemod_exm = 1 --> Subcellular models for ionic currents: TT, LR, BR, ...

  if(kfl_paral/=0) then


     ! ALLOCATION of variables:
     !   - elmag
     !   - vconc
     !   - taulo
     !   - vauxi_exm
     !   - vicel_exm
     !   - rhsau_exm
     !   - nospm_exm
     !   - volts_exm

     !!     if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
     allocate(taulo(npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'TAULO','exm_memall',taulo)
     taulo= 0.0_rp
     allocate(fisoc(npoin,4),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'FISOC','exm_memall',fisoc)
     fisoc = -1.0_rp
     allocate(kwave_exm(npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'kwave_exm','exm_memall',kwave_exm)
     kwave_exm = 0_ip

     allocate(nomat_exm(npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'nomat_exm','exm_memall',nomat_exm)
     nomat_exm = -1_ip

     !!     else
     !!        allocate(taulo(1),stat=istat)
     !!        call memchk(zero,istat,mem_modul(1:2,modul),'TAULO','exm_memall',taulo)
     !!        taulo= 0.0_rp
     !!        allocate(fisoc(1),stat=istat)
     !!        call memchk(zero,istat,mem_modul(1:2,modul),'FISOC','exm_memall',fisoc)
     !!        fisoc = -1.0_rp
     !!        allocate(kwave_exm(1),stat=istat)
     !!        call memchk(zero,istat,mem_modul(1:2,modul),'kwave_exm','exm_memall',kwave_exm)
     !!        kwave_exm = 0_ip
     !!     end if

     allocate(elmag(2,npoin,ncomp_exm),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'ELMAG','exm_memall',elmag)

     allocate(volts_exm(2,npoin,ncomp_exm),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VOLTS_EXM','exm_memall',volts_exm)

     allocate(refhn_exm(npoin,ncomp_exm),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'REFHN_EXM','exm_memall',REFHN_exm)

     allocate(vauxi_exm(nauxi_exm,ncdof,ncomp_exm),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VAUXI_EXM','exm_memall',vauxi_exm)

     allocate(vconc(nconc_exm,ncdof,ncomp_exm),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VCONC','exm_memall',vconc)

     allocate(vicel_exm(nicel_exm,ncdof,ncomp_exm),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VICEL_EXM','exm_memall',vicel_exm)


     if (kfl_appli_exm > 0) then
        !
        ! Source fields
        !
        !    Applied current field flag:
        allocate(lapno_exm(npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LAPNO_EXM','exm_memall',lapno_exm)
        lapno_exm=0

        !    Applied current field:
        allocate(appfi_exm(npoin,ncomp_exm),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'APPFI_EXM','exm_memall',appfi_exm)
        appfi_exm=0.0_rp
     end if


     if(kfl_algso_exm==0 .and. kfl_genal_exm > 1) then
        call runend('NOT CODED')
        !allocate(lpexm(npoin),stat=istat)
        !call memchk(zero,istat,memdi,'LPEXM','exm_memall',lpexm)
        !call mediso(lodof,npoin,lun_solve_exm,lpexm)
     end if

     !
     ! Residual size
     !
     nzrhs_exm=npoin
     !
     ! Solver memory for NVARI variables
     !
     call soldef(4_ip)
     !
     ! Conductivity, etc.
     !
     if( kfl_comdi_exm == 2 ) then

        allocate(cecon_exm(ndime,ndime,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CECON_EXM','exm_memall',cecon_exm)
        allocate(grafi_exm(ndime,ndime,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'GRAFI_EXM','exm_memall',grafi_exm)

        allocate(kgrfi_exm(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'KGRFI_EXM','exm_memall',kgrfi_exm)

     else if( kfl_comdi_exm == 3 ) then

        allocate(kgrfi_exm(nelem),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'KGRFI_EXM','exm_memall',kgrfi_exm)

     end if

  else

     !
     ! Master: allocate minimum memory
     !

     allocate(elmag(ndofn_exm,1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'ELMAG','exm_memall',elmag)

     allocate(volts_exm(ndofn_exm,1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VOLTS_EXM','exm_memall',volts_exm)

     allocate(nomat_exm(1_ip),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'nomat_exm','exm_memall',nomat_exm)
     nomat_exm = -1_ip

     allocate(vauxi_exm(nauxi_exm,1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VAUXI_EXM','exm_memall',vauxi_exm)

     allocate(vconc(nconc_exm,1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VCONC','exm_memall',vconc)

     allocate(vicel_exm(nicel_exm,1,1),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'VICEL_EXM','exm_memall',vicel_exm)

     allocate(fisoc(1,4),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'FISOC','exm_memall',fisoc)
     fisoc = -1.0_rp

     if (kfl_appli_exm > 0) then
        allocate(lapno_exm(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'LAPNO_EXM','exm_memall',lapno_exm)
        lapno_exm=0

        ncomp_exm = 2
        if(kfl_timei_exm==1) ncomp_exm=3
        allocate(appfi_exm(1,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'APPFI_EXM','exm_reaphy',appfi_exm)
        appfi_exm=0.0_rp
     end if

     if(kfl_algso_exm==0) then
        call runend('NOT CODED')
        !allocate(lpexm(1),stat=istat)
        !call memchk(zero,istat,memdi,'LPEXM','exm_memall',lpexm)
        !call mediso(lodof,npoin,lun_solve_exm,lpexm)
     end if
     !
     ! Conductivity, etc.
     !
     if( 1 == 1 ) then
        allocate(cecon_exm(3,3,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'CECON_EXM','exm_memall',cecon_exm)
        allocate(kgrfi_exm(1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'KGRFI_EXM','exm_memall',kgrfi_exm)
        allocate(grafi_exm(1,1,1),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'GRAFI_EXM','exm_memall',grafi_exm)
     end if

  end if

  !
  ! Actualize maximum sizes of RHSID
  !


  nzrhs=max(nzrhs,nzrhs_exm)


end subroutine exm_memall
