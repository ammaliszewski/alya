subroutine exm_endite(itask)
!-----------------------------------------------------------------------
!****f* Exmedi/exm_endite
! NAME 
!    exm_endite
! DESCRIPTION
!    This routine checks convergence and updates unknowns at:
!    - itask=1 The end of an internal iteration
!    - itask=2 The end of the internal loop iteration
! USES
!    exm_cvgunk
! USED BY
!    exm_doiter
!***
!-----------------------------------------------------------------------
  use      def_master
  use      def_parame
  use      def_exmedi


  implicit none
  integer(ip) :: itask, ipoin

  select case(itask)

  case(one)
     !
     !  Compute convergence residual of the internal iteration (that is,
     !  || u(n,i,j) - u(n,i,j-1)|| / ||u(n,i,j)||) and update unknowns:
     !  u(n,i,j-1) <-- u(n,i,j)
     !
     call exm_cvgunk(  one)
     if (kfl_gemod_exm == 0) then  !no cell
        call exm_updunk(three)
     end if

  case(two)
     !
     !  Compute convergence residual of the external iteration (that is,
     !  || u(n,i,*) - u(n,i-1,*)|| / ||u(n,i,*)||) and update unknowns:
     !  u(n,i-1,*) <-- u(n,i,*)
     !
     call livinf(16_ip,' ',itinn(modul))
     call exm_cvgunk( two)

     if (kfl_gemod_exm == 0) then  !no cell
        call exm_updunk(four)
     end if
     
  end select

end subroutine exm_endite
