!-----------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_updtss.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Computes the proper time step size 
!> @details Computes the proper time step size 
!> @} 
!-----------------------------------------------------------------------
subroutine exm_updtss
  use      def_parame
  use      def_master
  use      def_domain
  use      def_exmedi
  implicit none 

  integer(ip) :: ielem,kelem,pmate,idime,jdime,inode
  integer(ip) :: pnode,ipoin,pelty,igaus,noion
  real(rp)    :: velno,dtcri,dtmin,dtaux,hlmin,cndmx,xfact,xpfhn,xmccc,xmalp,xcosa
  real(rp)    :: cndex(ndime,ndime,mgaus), cndin(ndime,ndime,mgaus), elion(mnode,2),eliap(mnode,2)
  real(rp)    :: hleng(ndime),tragl(ndime,ndime),elcod(ndime,mnode)

!
! Time step computed from the critical one. : 
!
! The time step is computed from the diffusive limit... this should be
! IMPROVED to take into account:
! 
! 1. reaction and non-linear terms
! 2. tensorial character of the conduction
! 3. coupling
!

  if(INOTMASTER) then
  
  cndex = 0.0_rp
  cndin = 0.0_rp

  if (kfl_tiext_exm == 1 ) then                             ! Externally fixed time step
     dtmin = dtext_exm
     dtcri = dtmin
     return
  else
     dtmin = 0.0_rp
     dtcri = 1000000000.0_rp
  end if

  xfact =  0.5_rp 

!!!! ojo: corregir este xfact para elementos cuadraticos...

  xfact = xfact * safet_exm


     elements: do kelem = 1,nelez(lzone(ID_EXMEDI))
        ielem = lelez(lzone(ID_EXMEDI)) % l(kelem)
        
        pelty = ltype(ielem)
        pnode = nnode(pelty)
        
        pmate = 1
        if( nmate > 1 ) then
           pmate = lmate_exm(ielem)
        end if
        
        if(kfl_gemod_exm == 0) then       
            xmccc = xmopa_exm( 5,pmate)
            xmalp = xmopa_exm( 3,pmate)
            xmsuv_exm = 1.0
        else 
            xmccc = pdccm_exm 
            xmalp = 0.0_rp
        end if
            
        do inode=1,pnode
           ipoin=lnods(inode,ielem)
           eliap(inode,1) = elmag(1,ipoin,1)
           xcosa = eliap(inode,1)
           elion(inode,1) = xmccc *  eliap(inode,1) * & 
                ( eliap(inode,1) - 1.0_rp ) * ( eliap(inode,1) - xmalp ) 
           xcosa = elion(inode,1)
           do idime= 1,ndime
              elcod(idime,inode)  = coord(idime,ipoin)
           end do
           if( kfl_coupl(ID_SOLIDZ,ID_EXMEDI) >= 1 .or. kfl_coupl(ID_EXMEDI,ID_SOLIDZ) >=1 ) then
           !if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
              if (kfl_gcoup_exm == 1) then
                 do idime=1,ndime
                    elcod(idime,inode)= elcod(idime,inode) + displ(idime,ipoin,1)
                 end do
              end if
           end if

        end do

        igaus = 1
!        xpfhn = dot_product(elion(1:pnode,1) , elmar(pelty)%shape(1:pnode,igaus)) 
        
        ! 1. calcular la hleng mas corta de cada elemento...      
        
        call elmlen(ndime,pnode,elmar(pelty)%dercg(1,1),tragl,elcod,hnatu(pelty),hleng)
        hlmin = hleng(1)
       
        ! 2. calcular la conductividad maxima de cada elemento... 
        
!        call exm_comcnd(-ielem,pmate,cndin,noion)
!        cndmx=0.0_rp
!        do jdime=1,ndime
!           do idime=1,ndime
!              if (cndin(idime,jdime,1) >= cndmx) cndmx = cndin(idime,jdime,1)
!              if (kfl_cemod_exm == 2) then
!                 if (cndex(idime,jdime,1) >= cndmx) cndmx = cndex(idime,jdime,1)
!              end if
!           end do
!        end do

        cndmx= gcond_exm(1,1,pmate)

        ! 3. sacar un dtcri
        
        if (cndmx > 1.0e-10) then
           if(kfl_gemod_exm == 0) then    
              dtaux =   xfact * hlmin * hlmin * pdccm_exm / cndmx  ! * xmsuv_exm
           else
              dtaux =   xfact * hlmin * hlmin  / cndmx  !conductivity is diffusion already
           end if
        else
           dtaux= 1.0e10
        end if
        !     if (xpfhn <= 1.0e-10) xpfhn = - xpfhn
        !     if (xpfhn >= 1.0e-10) then
        !        dtaux = cndmx / hlmin / hlmin / xfact + xpfhn / hlmin / hlmin / hlmin
        !        dtaux = safet_exm / dtaux
        !     else
        !        dtaux = cndmx / hlmin / hlmin / xfact 
        !        dtaux = safet_exm / dtaux
        !     end if
        
        if (dtaux <= dtcri) dtcri = dtaux

     end do elements
     
     ! compare dtcri with dtext_exm
     
     if (kfl_genal_exm >= 2) then
        call runend('EXM_UPDTSS: USE ONLY EXPLICIT SCHEMES')
     end if

     dtcri_exm = dtcri

     if (kfl_tiext_exm == 1) then 

!        if (dtime > dtcri) then
!           if (ittim == 1) cutim = cutim - dtime + dtcri     
!           dtime = dtcri
!        end if
     else
        dtmin = dtcri
     end if

  end if
  !
  ! Look for minimum over whole mesh
  !
  call pararr('MIN',0_ip,1_ip,dtmin)
  
  dtcri_exm = dtmin
  if(dtcri_exm/=0.0_rp) dtinv_exm = 1.0_rp/(dtcri_exm*safet_exm)
  if(kfl_timco==1) dtinv=max(dtinv,dtinv_exm)
  
end subroutine exm_updtss
