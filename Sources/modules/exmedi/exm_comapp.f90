!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_comapp.f90
!> @author  Various
!> @brief   Compute of stimulus current
!> @details Setup the appfi_exm (applied current field) to the EP problem\n
!! The parameters are being read at exm_reaphy.f90 \n
!> @} 
!!-----------------------------------------------------------------------
subroutine exm_comapp

  use      def_master
  use      def_domain
  use      def_exmedi
  implicit none

  integer(ip) :: ipoin,idofn,istim,iboun,nauxi,pblty,pnodb,inodb
  real(rp)    :: dicen,xicen,yicen,zicen,batim
  integer(ip) :: kstim(5700) , istim_compute
  real(rp)    :: sqrea(5700) 

  batim = cutim  
  !  ltime = cutim -dtime                        !!! <<<---  Iapp is that of the CURRENT time step

  if (kfl_gemod_exm == 1 ) then  !CELL MODELS
     !batim = cutim * 1000.0_rp       
     appfi_exm(1:npoin,1)= 0._rp
     appfi_exm(1:npoin,3)= 0._rp 

     do istim= 1, nstim_exm

        istim_compute = 0
        if (kfl_ptrig_exm == 1) then
           if (epres > aptim(istim)) then
              istim_compute = 1  
              kfl_ptrig_exm = -1
              aptim(istim)  = batim  ! start the chrono
           end if
        else if (kfl_ptrig_exm == -1) then
           if (batim <= (aplap_exm(istim)+ aptim(istim))) then
              istim_compute = 1           
           end if
        else if (kfl_ptrig_exm == 0) then
           if (batim >= aptim(istim) .and. batim <= (aplap_exm(istim)+ aptim(istim))) then
              istim_compute = 1           
           end if
        end if

        if (istim_compute == 1) then
           
           sqrea(istim)= aprea_exm(istim)*aprea_exm(istim)    

           do ipoin= 1,npoin

              if (.not.(exm_zonenode(ipoin))) cycle

              xicen = coord(1,ipoin)-apcen_exm(1,istim)
              yicen = coord(2,ipoin)-apcen_exm(2,istim)
              zicen = 0.0_rp
              if (ndime == 3) zicen = coord(ndime,ipoin)-apcen_exm(ndime,istim)

              dicen = xicen*xicen + yicen*yicen + zicen*zicen

              if (dicen <= sqrea(istim)) then
                 lapno_exm(ipoin) = kfl_appli_exm
                 appfi_exm(ipoin,1)= apval_exm(istim)
                 appfi_exm(ipoin,3)= appfi_exm(ipoin,1)
              end if
           end do
           !else if (batim < aptim(istim) .and. batim > (aplap_exm(istim)+ aptim(istim))) then
           !appfi_exm(1:npoin,3)= 0._rp           
           !appfi_exm(1:npoin,1)= 0._rp          
        end if
     end do

     if (nstim_exm < 0) then
        !
        !
        !
        do iboun= 1,nboun
           pblty = ltypb(iboun) 
           pnodb = nnode(pblty)
           
           if (lbset(iboun)== nstis_exm) then
              
              do inodb= 1,pnodb
                 ipoin= lnodb(inodb,iboun)
                 
                 if (.not.(exm_zonenode(ipoin))) cycle
                 
                 lapno_exm(ipoin) = kfl_appli_exm
                 appfi_exm(ipoin,1)= apval_exm(1)
                 appfi_exm(ipoin,3)= appfi_exm(ipoin,1)
                 
              end do
           end if
        end do

     end if

     return      

  else if(kfl_gemod_exm == 0 ) then   !FHN
     istim_compute = 0
     kstim=0 
     if (kfl_appli_exm == 200) then
        nauxi= nstim_exm        
        if (nstim_exm < 0) nauxi=1 

        do istim= 1, nauxi
           if (kfl_ptrig_exm == 1) then
              if (epres > aptim(istim)) then
                 istim_compute = 1  
                 kfl_ptrig_exm = -1
                 aptim(istim)  = batim  ! start the chrono
              end if
           else if (kfl_ptrig_exm == -1) then
              !              if (batim <= (aplap_exm(istim)+ aptim(istim))) then
              !                 istim_compute = 1           
              !              end if
              !  no lapse considered, just a flash 
              kfl_ptrig_exm = 0
           else if (kfl_ptrig_exm == 0) then
              if (aptim(istim) >= 0) then
                 if (batim >= aptim(istim) ) then
                    kstim(istim) = istim
                    aptim(istim) = -1.0_rp
                    istim_compute = istim_compute + 1                    ! count the starting stimuli
                 end if
              end if
           end if           
        end do

        if (istim_compute == 0) return

        if (nstim_exm < 0) then
           !
           !
           !
           do iboun= 1,nboun
              pblty = ltypb(iboun) 
              pnodb = nnode(pblty)

              if (lbset(iboun)== nstis_exm) then
                 
                 do inodb= 1,pnodb
                    ipoin= lnodb(inodb,iboun)
                    
                    if (.not.(exm_zonenode(ipoin))) cycle
                    do idofn=1,ndofn_exm  ! Estímul inicial i Normalització del potencial
                       elmag(idofn,ipoin,1)= &
                            (apval_exm(1) - poref_exm(1)) /  (poref_exm(2) -poref_exm(1))      
                       elmag(idofn,ipoin,2)= &
                            (apval_exm(1) - poref_exm(1)) /  (poref_exm(2) -poref_exm(1))      
                       elmag(idofn,ipoin,3)= &
                            (apval_exm(1) - poref_exm(1)) /  (poref_exm(2) -poref_exm(1))     
                    end do
                    
                 end do
              end if
           end do

           return

        end if

        ! Starting potential
        do istim= 1,nstim_exm
           sqrea(istim)= 0
           if (kstim(istim) > 0) then
              sqrea(istim)= aprea_exm(kstim(istim))*aprea_exm(kstim(istim))
           end if
        end do

        do ipoin= 1,npoin

           if (.not.(exm_zonenode(ipoin))) cycle

           do istim= 1,nstim_exm

              if (kstim(istim) > 0) then

                 xicen = coord(1,ipoin)-apcen_exm(1,kstim(istim))
                 yicen = coord(2,ipoin)-apcen_exm(2,kstim(istim))
                 zicen = 0.0_rp
                 if (ndime == 3) zicen = coord(ndime,ipoin)-apcen_exm(ndime,kstim(istim))

                 dicen = xicen*xicen + yicen*yicen + zicen*zicen

                 if (dicen <= sqrea(istim)) then
                    do idofn=1,ndofn_exm  ! Estímul inicial i Normalització del potencial
                       elmag(idofn,ipoin,1)= (apval_exm(kstim(istim)) - poref_exm(1)) /  (poref_exm(2) -poref_exm(1))      
                       elmag(idofn,ipoin,2)= (apval_exm(kstim(istim)) - poref_exm(1)) /  (poref_exm(2) -poref_exm(1))      
                       elmag(idofn,ipoin,3)= (apval_exm(kstim(istim)) - poref_exm(1)) /  (poref_exm(2) -poref_exm(1))     
                    end do
                 end if

              end if
           end do

        end do

     end if
  end if

end subroutine exm_comapp
