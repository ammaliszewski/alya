!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_conhet.f90
!> @author  Jazmin Aguado-Sierra
!> @brief   Ion Concentration calculation for TenTuscher-Panfilov 2006 heterogeneous model
!> @details Calculate every ION at every time step\n!!
!! The auxiliary variables are 9:
!!   - cai:  Ca_i    -->  vconc(1,ipoin,1)  -->  Concentration of total calcium in the Cytoplasm  (caitot = caifre + caibuf)\n
!!   - casr:  Ca_sr   -->  vconc(2,ipoin,1)  --> Concentration of total calcium in the Sarcoplasmic Reticulum SR  
!!                                                             (casrto = casrfr + casrbu)\n
!!   - nai:  Na_i    -->  vconc(3,ipoin,1)  -->  Intracellular Sodium Concentration \n
!!   - ki:   K_i     -->  vconc(4,ipoin,1)  -->  Intracellular Potassium Concentration \n
!!   - cai_SS:  Ca_i_SS   -->  vconc(5,ipoin,1)  -->  Concentration of SubSarcolemma calcium  \n
!!   - caibuf:  Ca_i_buff   -->  vconc(6,ipoin,1)  -->  Concentration of buffered calcium in the Cytoplasm  \n
!!   - casrbuf:  Ca_sr_buff -->  vconc(7,ipoin,1)  -->  Concentration of Buffered calcium in the Sarcoplasmic Reticulum SR  \n
!!   - cassbuf:  Ca_ss_buff  -->  vconc(8,ipoin,1)  --> Concentration of buffered calcium in the SubSarcolemma\n
!!   - Rprime:  proportion of closed Irel channels vconc(9,ipoin,1)\n
!!   -  O:       Gate of activation O (related with I_rel current) vconc(10,ipoin,1)\n
!! The order is important because it is related with the order of nconc_exm variables (exm_memall.f90)\n
!> @} 
!!-----------------------------------------------------------------------
subroutine exm_cttina

  use      def_parame
  use      def_master
  use      def_elmtyp
  use      def_domain
  use      def_exmedi

  implicit none


! DEFINITION OF VARIABLES

  integer(ip) :: ipoin
  integer(ip) :: ielem,inode,pnode,pmate,pelty
  real(rp)    :: caitot, casrto, naitot, kitot, caifre, caibuf, casrbu, cassto, rprime

  real(rp)    :: vaux1, vaux2, vaux3, k1, k2, k3, k4
  
  real(rp)    :: rhsx1, rhsx2, rhsx, val0


! DESCRIPTION OF VARIABLES

! ipoin: point number (it is related with a node)
! nmait: maximum number of iterations used for computation of activation variables 
!       (it is related with non-linearity)
! imait: auxiliary variable used as counter of iterations

! caitot, casrto, naitot, kitot, caifre, caibuf, casrfr, casrbu: concentrations

! vaux1, vaux2, vaux3: auxiliar variables 1, 2 and 3

! rhsx: variable used by each activation variable for computation of Right Hand Side (dy/dt=F(t,y)=RHS)
! rhsx1: auxiliary variable used for computation of rhsx
! rhsx2: auxiliary variable used for computation of rhsx
! val0: value of activation variable in the previous time step

! DESCRIPTION OF CONSTANTS (PARAMETERS)

! PARAMETERS of Ten Tusscher model for concentrations
! These constants (page H1574 of TT paper) are used for computation of ionic currents

  real(rp)   ::  volcyto, volsr, capacit, volss
  real(rp)   ::  bufcyto, kbufcyto, bufss, kbufss
  real(rp)   ::  bufsr, kbufsr
  real(rp)   ::  faradco, dtimeEP
  
  volcyto = 0.016404_rp      ! V_c [nL] --> cytoplasmic volume
  volsr = 0.001094_rp  * ttpar_exm(1,5)       ! V_SR [nL] --> sarcoplasmic reticulum (SR) volume
  capacit = 0.185_rp       ! C [nF] --> Capacitance used in Ca_i_tot, Na_i_toy and K_i_tot
  volss = 0.00005468_rp        ! V_SS

  bufcyto = 0.2_rp      ! Buf_c [mM] --> total cytoplasmic buffer concentration
  kbufcyto = 0.001_rp     ! K_Bufc [mM] --> Cai half-saturation constant for cytoplasmic buffer
  bufsr = 10.0_rp        ! Buf_sr [mM] --> total sarcoplasmic buffer concentration
  kbufsr = 0.3_rp       ! K_Bufsr [mM] --> Ca_SR half-saturation constant for sarcoplasmic buffer
  bufss = 0.4_rp        ! Buf_sr [mM] --> total sarcoplasmic buffer concentration
  kbufss = 0.00025_rp       ! K_Bufsr [mM] --> Ca_SR half-saturation constant for sarcoplasmic buffer
    
  faradco = 96485.3415_rp       ! F [C/mol] --> faraday constant
  dtimeEP = dtime *1000.0_rp
! COMPUTATION of concentrations
                
  if(INOTMASTER) then
    do ipoin= 1,npoin
       !ipoin= lnods(inode,ielem)     
       ituss_exm = int(celty_exm(1,ipoin))
    
     ! Concentration of total calcium in the Cytoplasm (Ca_i_tot)
     ! nconc_exm = 1
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
          
     !   Ca_i_bufc = 1/(1+( Buf_c*K_buf_c)/(pow((Ca_i+K_buf_c), 2)))
         vaux1 = vconc(1,ipoin,2) + kbufcyto
         vaux1 = 1.0_rp / (vaux1 * vaux1)
         vaux1 = vaux1 * kbufcyto * bufcyto    !Variable caibuf
         vaux1 = 1.0_rp / (1.0_rp + vaux1)
         vconc(6,ipoin,1) = vaux1
         vconc(6,ipoin,2) = vaux1
     ! d_caitot/d_t = - (I_CaL + I_bCa + I_pCa - 2 * I_NaCa) * C / (2 * V_c * F) + I_leak - I_up + I_rel
     ! dCa_i_dt = Ca_i_bufc*((( (i_leak - i_up)*V_sr)/V_c+i_xfer) - (1*((i_b_Ca+i_p_Ca) - 2*i_NaCa)*Cm)/( 2*1*V_c*F))
     
          vaux1 = - 2.0_rp * volcyto * faradco
          vaux1 = 1.0_rp / vaux1  
     
          vaux3 = (vicel_exm(12,ipoin,1) + vicel_exm(9,ipoin,1)) - (2.0_rp *  vicel_exm(7,ipoin,1) )
          vaux3 = vaux1 * vaux3 * capacit
          vaux2 = (vicel_exm(13,ipoin,1) - vicel_exm(14,ipoin,1)) * (volsr / volcyto) + vicel_exm(18,ipoin,1)
          rhsx =  vconc(6,ipoin,2) * (vaux2 + vaux3) !
     
          val0 = vconc(1,ipoin,3)
     
          k1 = rhsx 
          k2 = rhsx + 0.5_rp * dtimeEP * k1
          k3 = rhsx + 0.5_rp * dtimeEP * k2 
          k4 = rhsx + dtimeEP * k3 
          caitot = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
          
          !caitot = val0 + dtimeEP/xmccm_exm * rhsx
          !caitot =  val0 + (0.5_rp * dtimeEP * (k1 + k2))
          vconc(1,ipoin,1) = caitot       ! Value of Ca_i_tot concentration
     
     ! Concentration of total calcium in the Sarcoplasmic Reticulum (Ca_sr_tot)
     ! nconc_exm = 2
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
                         
         vaux1 = vconc(2,ipoin,2)  + kbufsr
         vaux1 = 1.0_rp / (vaux1 * vaux1)
         vaux1 = vaux1 * kbufsr * bufsr
         vaux1 = 1.0_rp / (1.0_rp + vaux1)
         vconc(7,ipoin,1) = vaux1
         vconc(7,ipoin,2) = vaux1
         
     ! d_casrto/d_t = Ca_sr_bufsr*(i_up - (i_rel+i_leak))
     
          vaux1 = vicel_exm(14,ipoin,1) - vicel_exm(13,ipoin,1) - vicel_exm(15,ipoin,1)
     
          rhsx = vaux1 * vconc(7,ipoin,2)  !capacit *
     
          val0 = vconc(2,ipoin,3)
          k1 = rhsx 
          k2 = rhsx + 0.5_rp * dtimeEP * k1
          k3 = rhsx + 0.5_rp * dtimeEP * k2 
          k4 = rhsx + dtimeEP * k3 
          casrto = val0 + ((dtimeEP / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
     
          !casrto = val0 + dtimeEP/xmccm_exm * rhsx
          vconc(2,ipoin,1) = casrto       ! Value of Ca_sr_tot concentration
     
     ! Concentration of total calcium in the diadic space (Ca_ss_tot)
     ! nconc_exm = 5
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
         vaux1 = vconc(5,ipoin,2)  + kbufss
         vaux1 = 1.0_rp / (vaux1 * vaux1)
         vaux1 = vaux1 * kbufss * bufss
         vaux1 = 1.0_rp / (1.0_rp + vaux1)
         vconc(8,ipoin,1) = vaux1                    !Variable cassbufss
         vconc(8,ipoin,2) = vaux1
         
     ! d_cass/d_t = Ca_ss_bufss*(((-1*i_CaL)/( 2*1*V_ss*F)+( i_rel*V_sr)/V_ss) - ( i_xfer*V_c)/V_ss)
     
          vaux1 = 1.0_rp / (2.0_rp * faradco * volss)
          vaux1 = vaux1 * vicel_exm(2,ipoin,1) * capacit
          vaux2 = vicel_exm(15,ipoin,1) * volsr / volss
          vaux3 = vicel_exm(18,ipoin,1) * volcyto / volss
     
          rhsx =  vconc(8,ipoin,2) * (- vaux1 + vaux2 - vaux3)
     
          val0 = vconc(5,ipoin,3)
          
          k1 = rhsx 
          k2 = rhsx + 0.5_rp * dtimeEP * k1
          k3 = rhsx + 0.5_rp * dtimeEP * k2 
          k4 = rhsx + dtimeEP * k3 
          cassto = val0 + ((dtimeEP / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
          !cassto = val0 + dtimeEP/xmccm_exm * rhsx
          vconc(5,ipoin,1) = cassto       ! Value of Ca_ss_tot concentration
     
     ! Intracellular sodium concentration (Na_i_tot)
     ! nconc_exm = 3
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
     
     ! d_naitot/d_t =  ((- 1*(i_Na+i_b_Na+ 3*i_NaK+ 3*i_NaCa))/( 1*V_c*F))*Cm
     
          vaux1 = volcyto * faradco
          vaux1 = 1.0_rp / vaux1 !* capacit
     
          vaux3 = (3.0_rp * vicel_exm(8,ipoin,1)) + (3.0_rp * vicel_exm(7,ipoin,1)) 
          vaux3 = vaux3 + vicel_exm(1,ipoin,1) + vicel_exm(11,ipoin,1)
          rhsx = - capacit * vaux1 * vaux3
     
          val0 = vconc(3,ipoin,3)
               
          k1 = rhsx 
          k2 = rhsx + 0.5_rp * dtimeEP * k1
          k3 = rhsx + 0.5_rp * dtimeEP * k2 
          k4 = rhsx + dtimeEP * k3 
          naitot = val0 + ((dtimeEP / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
          !naitot = val0 + dtimeEP/xmccm_exm * rhsx
          vconc(3,ipoin,1) = naitot       ! Value of Na_i_tot concentration
     
     
     ! Intracellular potassium concentration (K_i_tot)
     ! nconc_exm = 4
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
     
     ! d_kitot/d_t = ((- 1*((i_K1+i_to+i_Kr+i_Ks+i_p_K+i_Stim) -  2*i_NaK))/( 1*V_c*F))*Cm
     
          vaux1 = - volcyto * faradco
          vaux1 = capacit / vaux1 !* capacit  
     
          vaux3 = (vicel_exm(6,ipoin,1) + vicel_exm(3,ipoin,1) + vicel_exm(5,ipoin,1) &
          + vicel_exm(4,ipoin,1) + vicel_exm(10,ipoin,1) + vicel_exm(16,ipoin,1)) - (2.0_rp * vicel_exm(8,ipoin,1)) 
     
          rhsx = vaux1 * vaux3
     
          val0 = vconc(4,ipoin,3)
          
          k1 = rhsx 
          k2 = rhsx + 0.5_rp * dtimeEP * k1
          k3 = rhsx + 0.5_rp * dtimeEP * k2 
          k4 = rhsx + dtimeEP * k3 
          kitot = val0 + ((dtimeEP / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
          !kitot = val0 + dtimeEP/xmccm_exm * rhsx
          vconc(4,ipoin,1) = kitot          ! Value of K_i_tot concentration
     
          ! Variable of activation O (related with I_rel current)
     ! nconc = 10
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     !  kcasr = max_sr - (max_sr - min_sr)/(1+(pow((EC/Ca_SR), 2)))
           vaux1 = 1.0_rp + ((1.5_rp / vconc(2,ipoin,2))*(1.5_rp / vconc(2,ipoin,2)))  !conc2 = CaSRfree = CaSR
           vaux1 = 1.0_rp / vaux1
           vaux1 = 2.5_rp - (1.5_rp * vaux1)  !kCaSR
     !     O = ( k1*pow(Ca_ss, 2)*R_prime)/(k3+ k1*pow(Ca_ss, 2))      
           vaux3 = 0.15_rp / vaux1   !K1
           vaux2 = 0.045_rp * vaux1  !K2
           
           
           rhsx = 1.0_rp / (0.06_rp + (vaux3*vconc(5,ipoin,2)*vconc(5,ipoin,2)))      !O for calcium dynamics
           rhsx = vaux3 * vconc(5,ipoin,2) * vconc(5,ipoin,2) * vconc(9,ipoin,2) * rhsx !O
           vconc(10,ipoin,1) = rhsx 
           vconc(10,ipoin,2) = rhsx
           
     ! Variable of activation O (related with I_rel current)
     ! nconc= 9
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     !  dR_prime_dt = - k2*Ca_ss*R_prime+ k4*(1 - R_prime)
           
           vaux3 = - vaux2 * vconc(5,ipoin,2) * vconc(9,ipoin,2) 
           rhsx = vaux3 + (0.005_rp * (1.0_rp - vconc(9,ipoin,2)))
           
           val0 = vconc(9,ipoin,3)    ! Value of Rprime in previous dtimeEP step 
                
           k1 = rhsx 
           k2 = rhsx + 0.5_rp * dtimeEP * k1
           k3 = rhsx + 0.5_rp * dtimeEP * k2 
           k4 = rhsx + dtimeEP * k3 
           rprime = val0 + ((dtimeEP / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
          !rprime = val0 + dtimeEP/xmccm_exm * rhsx
          vconc(9,ipoin,1) = rprime
    end do
 end if
!write(993,*) vconc(:,1,:)  !JAS
!write(994,*) vicel_exm(:,1,:)
!write(995,*) cutim
!write(992,*) elmag(1,:,1)

end subroutine exm_cttina
