subroutine exm_begste
!-----------------------------------------------------------------------
!****f* Exmedi/exm_begste
! NAME 
!    exm_begste
! DESCRIPTION
!    This routine prepares for a new time step 
! USES
!    exm_iniunk
!    exm_updtss
!    exm_updbcs
!    exm_updunk
! USED BY
!    Exmedi
!***
!-----------------------------------------------------------------------

  use def_parame
  use def_master
  use def_domain

  use def_exmedi

  implicit none
  
  !
  ! Initial guess fo the unknowns: U(n,0,*) <-- U(n-1,*,*).
  !
  if (kfl_gemod_exm==0) then
     call exm_updunk(one)
  end if


end subroutine exm_begste
