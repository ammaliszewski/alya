subroutine exm_iniunk
  !-----------------------------------------------------------------------
  !****f* Exmedi/exm_inicia for different cellular model initialisations
  ! NAME 
  !    exm_inicia
  ! DESCRIPTION
  !    This routine sets up the initial conditions 
  ! USED BY
  !    exm_begste
  !***
  !-----------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      mod_iofile
  use      def_exmedi

  implicit none
  integer(ip) :: kmodel, imate,ielem,kelem,pmate,pelty,pnode,inode,ipoin
  character(30) :: mesau(2)


  if (INOTMASTER) then
     !
     ! Loop over elements to create a material-per-node vector: NOMAT_EXM

     elements: do kelem = 1,nelez(lzone(ID_EXMEDI))
        ielem = lelez(lzone(ID_EXMEDI)) % l(kelem)
        pelty = ltype(ielem)        

        if( pelty > 0 ) then
           pnode = nnode(pelty)           
           pmate = 1
           if( nmate > 1 ) then
              pmate = lmate_exm(ielem)
           end if
           do inode= 1,pnode
              ipoin= lnods(inode,ielem)
              ! convention: material n-1 supersedes material n
              if ((nomat_exm(ipoin) == -1) .or. (pmate < nomat_exm(ipoin)) ) then
                 nomat_exm(ipoin) = pmate                 
              end if
           end do
        end if
     end do elements

     call parari('MIN',NPOIN_TYPE,npoin,nomat_exm)

  end if


  if( kfl_rstar == 0 ) then
     !
     ! check if any TT-like model is present
     !
     kmodel= 0
     do imate= 1,nmate
        kmodel = max(kfl_spmod_exm(imate),0)
     end do

     if (INOTSLAVE) then
        if (kfl_gemod_exm == 1) then
           call livinf(42_ip,'  COMPUTING INITIAL PHYSIOLOGICAL CONDITIONS...',0)
           if (kmodel == 4 .or. kmodel==5 .or. kmodel==6 ) then
              call livinf(42_ip,'    ITERATING ONE CELL MODEL...',0)
              mesau(1)= intost(onecl_exm(1)) ! beats
              mesau(2)= intost(onecl_exm(2)) ! cyclelength
              call livinf(42_ip,'    BEATS=       '//trim(mesau(1)),0)
              call livinf(42_ip,'    CYCLELENGTH= '//trim(mesau(2)),0)
           end if
        end if
     end if

     if(INOTMASTER) then

        if (kfl_gemod_exm == 1) then

           ! in parallel, this is done by all slaves, so they do not interchange data about this 
           call exm_inicia
           if (kmodel == 1) then    ! Ten Tusscher model 
              !write(990,*) kfl_spmod_exm(imate)
           else if (kmodel == 4) then    !TT Heterogeneous                            
              call livinf(42_ip,'  SUB CELL MODEL: TT HETEROGENEOUS',0)
              call exm_onecel
              call exm_inihet
           else if (kmodel == 5) then    !O'hara-Rudy
              call livinf(42_ip,'  SUB CELL MODEL: OHARA - RUDY',0)
              call exm_oneohr
              call exm_iniohr
           else if (kmodel == 6) then    !TenTuscher - INaL                           
              call livinf(42_ip,'  SUB CELL MODEL: TT INaL',0)
              call exm_ontina
              call exm_intina
           end if
        end if
     end if

     if (INOTSLAVE) then
        if (kfl_gemod_exm == 1) then
           call livinf(42_ip,'  INITIAL CONDITIONS COMPUTED',0)
        end if
     end if

  end if

end subroutine exm_iniunk
