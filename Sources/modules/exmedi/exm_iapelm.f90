!-----------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_iapelm.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Compute elemental matrix and RHS. Old subroutine. Will desapear en future versions. replaced by iapelmnew. 
!> @details Compute elemental matrix and RHS. Old subroutine. Will desapear en future versions. replaced by iapelmnew. 
!> @} 
!-----------------------------------------------------------------------
subroutine exm_iapelm
  use      def_master
  use      def_domain
  use      def_elmtyp
  use      def_exmedi
  implicit none

  real(rp)    :: gpcar(ndime,mnode,mgaus)
  real(rp)    :: dvolu(mgaus),gpdet(mgaus),dvoti                     ! Values at Gauss points
  real(rp)    :: gpgdi(ndime,ndime,mgaus)
  real(rp)    :: hessi(ntens,mnode*mlapl)
  real(rp)    :: hleng(ndime),tragl(ndime,ndime)

  real(rp)    :: elmat(mnode,mnode),wmatr(mnode,mnode) ! Element matrices
  real(rp)    :: esili(mnode), wreco(mnode), wapcu(mnode), wnoli(mnode), enoli(mnode)
  real(rp)    :: elaux(mnode,2), elcur(mnode,2), eliap(mnode,2), elion(mnode,2) , elian(mnode,2) 
  real(rp)    :: eleap(mnode,2), elgat(ngate_exm,mnode,2), elcod(ndime,mnode), eldis(ndime,mnode),elfib(ndime,mnode)

  real(rp)    :: cndex(ndime,ndime,mgaus), cndin(ndime,ndime,mgaus), shodi(ndime),&
       nfibe0(ndime,mgaus),nfibe(ndime,mgaus),nfibe_length

  real(rp)    :: wmat1(ndime,ndime,mnode*mlapl)        ! Created here to avoid using an 
  real(rp)    :: d2sdx(ndime,ndime,ndime*mlapl)        ! that is called inside do igaus 

  integer(ip) :: ielem,kelem,inode,ipoin,jnode,jpoin         ! Indices and dimensions
  integer(ip) :: igaus,igacn,idime,jdime,pnode,iicel,noion
  integer(ip) :: pelty,pmate,igate,izrhs,ncomp

  integer(ip) :: iposi,jposi,imasl                     ! Assembly
  real(rp)    :: acont,adiag,xreac,rauxi,xauxi(2)

  real(rp)    :: sphac,conac,difac,temol
  real(rp)    :: xmuit,xmui1,rhodt
  real(rp)    :: xgate(ngate_exm),xpold,xpapp,xpion,xioni,xmion,xpio1,&
       xpio2,xpio3,xdtin,wdiff, argth, factk, phics, taude, gephi
  real(rp)    :: fact1,fact2,fact3
  real(rp)    :: greap(ndime),griap(ndime),grdif(ndime),grdin,tauro, tauso, tausi, taufi 
  real(rp)    :: &
       xmeps,xmalp,xmgam,unlst,unprv,xlian,&
       xfact,xexdi,ximdi,ximol,fnol1,fnol2,dtedt,xgrno,vepar,reloc,xdumy
  real(rp)              :: xjaci(ndime,ndime,mgaus),xjacm(ndime,ndime,mgaus),gpsha,xhea1,xhea2,voaux(10)


  !do izrhs=1,nzrhs_exm
  !rhsid(izrhs)     = 0.0_rp
  rhsid    = 0.0_rp
  !end do
  amatr     = 0.0_rp

  ecgvo_exm = 0.0_rp
  !  eliap = 0.0_rp
  !  elian = 0.0_rp
  !  elcur = 0.0_rp

  xexdi = 1.0_rp
  ximol = 0.0_rp
  fnol1 = 1.0_rp
  fnol2 = 0.0_rp
  xioni = 0.0_rp

  if (kfl_genal_exm == 2) then ! Implicit solve?
     xexdi =   0.0_rp
     ximol =   1.0_rp
     fnol1 =   0.5_rp
     fnol2 =   0.5_rp
     if (kfl_nolim_exm == 1) then        
        fnol1 =   1.0_rp
        fnol2 =   1.0_rp        
     end if
  end if

  ximdi = 1.0_rp - xexdi

  ncomp= ncomp_exm
  if (ittim < ncomp) ncomp= ittim
  !
  ! Loop over elements

  elements: do kelem = 1,nelez(lzone(ID_EXMEDI))
     ielem = lelez(lzone(ID_EXMEDI)) % l(kelem)

     ! Initialize
     pelty = ltype(ielem)

     voaux= 0.0_rp

     if( pelty > 0 ) then
        pnode = nnode(pelty)

        pmate = 1
        if( nmate > 1 ) then
           pmate = lmate_exm(ielem)
        end if

        ! model parameters
        xmeps = 0.0_rp
        xmalp = 0.0_rp
        xmgam = 0.0_rp
        taufi = 0.0_rp
        if (kfl_spmod_exm(pmate) == 11) then 
           xmeps = xmopa_exm( 1,pmate)           ! epsilon for the rcp 
           xmalp = xmopa_exm( 3,pmate)           ! alpha for the Iion
           xmgam = xmopa_exm( 4,pmate)           ! gamma for the rcp
           taufi = xmopa_exm( 5,pmate)           ! C for the Iion 

        else if (kfl_spmod_exm(pmate) == 12) then                 !fenton
           xmalp = xmopa_exm( 3,pmate)           ! phi c
           tauro = xmopa_exm( 9,pmate)               
           tausi = xmopa_exm( 10,pmate) 
           tauso = xmopa_exm( 11,pmate)
           phics = xmopa_exm( 12,pmate)
           gephi = xmopa_exm( 13,pmate) 
           factk = xmopa_exm( 16,pmate)           
        end if
        fact1 = - taufi *  xmalp
        fact2 =   taufi * (xmalp + 1.0_rp)
        fact3 = - taufi  

        cndin = 0.0_rp
        elmat = 0.0_rp
        esili = 0.0_rp
        enoli = 0.0_rp
        elion = 0.0_rp
        eliap = 0.0_rp
        eleap = 0.0_rp

        ! Gather operations
        do inode=1,pnode
           ipoin = lnods(inode,ielem)
           eliap(inode,1) = elmag(1,ipoin,1)    ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
           eliap(inode,2) = elmag(1,ipoin,4) !!changed by Jaz (3)
           if(kfl_gemod_exm > 0) then
              eliap(inode,2) = elmag(1,ipoin,3) !!added by Jaz 
           end if  
           if (kfl_cemod_exm == 2) then
              eleap(inode,1) = elmag(2,ipoin,1)    ! EXTRA (BI-DOMAIN) POTENTIAL
           else
              eleap(inode,1) = 0.0_rp
              eleap(inode,2) = 0.0_rp
           end if

           elcur(inode,1) = appfi_exm(ipoin,1)  ! APPLIED CURRENT
           elcur(inode,2) = appfi_exm(ipoin,3)
           elian(inode,1) = elmag(1,ipoin,2)    ! iap for the non-linear fhn term

           if (kfl_gemod_exm == 0) then   ! NO CELL 
              do igate=1,ngate_exm
                 !      elgat(igate,inode,1) = vgate_exm(igate,ipoin,1)    ! ACTIVATION GATES ! This line is commented because vgate does not exist anymore.

              end do

              !else if (kfl_gemod_exm == 1) then   ! CELL MODELS
              !     if (kfl_spmod_exm(pmate) == 1) then   ! Ten Tuscher
              !        do iicel=1,12
              !           elion(inode,1) =  elion(inode,1) + vicel_exm(iicel,ipoin,1)
              !        end do
              !     end if 
              !     if (kfl_spmod_exm(pmate) == 4) then   ! Ten Tuscher Heterogeneous
              !         do iicel=1,12
              !            elion(inode,1) =  elion(inode,1) + vicel_exm(iicel,ipoin,1)
              !         end do
              !         elion(inode,1) =  elion(inode,1) + vicel_exm(16,ipoin,1)
              !     end if
              !     if (kfl_spmod_exm(pmate) == 5) then   ! O'Hara-Rudy
              !         elion(inode,1) =  elion(inode,1) + vicel_exm(1,ipoin,1)               
              !         do iicel=3,14
              !            elion(inode,1) =  elion(inode,1) + vicel_exm(iicel,ipoin,1) 
              !         end do
              !         do iicel=24,25
              !            elion(inode,1) =  elion(inode,1) + vicel_exm(iicel,ipoin,1) 
              !         end do
              !     end if
              !     elion(inode,1)=elion(inode,1)/xmccm_exm
           end if
           do idime=1,ndime
              elcod(idime,inode)=coord(idime,ipoin)
           end do

            if( kfl_coupl(ID_SOLIDZ,ID_EXMEDI) >= 1 .or. kfl_coupl(ID_EXMEDI,ID_SOLIDZ) >=1 ) then
!           if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
              if (kfl_gcoup_exm == 1) then
                 do idime=1,ndime
                    elcod(idime,inode)= elcod(idime,inode) + displ(idime,ipoin,1)
                    eldis(idime,inode)= displ(idime,ipoin,1)
                    elfib(idime,inode)= fiber(idime,ipoin)
                 end do
              end if
           end if

        end do

        ! elmag(...,1) working step n 
        ! elmag(...,2) step n, iteration i
        ! elmag(...,3) stored step n 
        ! elmag(...,4) n - 1
        ! elmag(...,5) n - 2
        ! elmag(...,6) n - 3 ...
        if (kfl_gemod_exm == 0) then   ! NO CELL 
           do inode=1,pnode
              ipoin=lnods(inode,ielem)
              eliap(inode,2) = elmag(1,ipoin,4)
              !if (kfl_cemod_exm == 2) & eleap(inode,2) = elmag(2,ipoin,4)
              !   do igate=1,ngate_exm
              !      elgat(igate,inode,2) = refhn_exm(ipoin,4)
              !   end do        
              elcur(inode,2) = appfi_exm(ipoin,3)
           end do
        end if

        ! Cartesian derivatives and Jacobian
        gauss_points_cartesian: do igaus=1,ngaus(pelty)
           call elmder(pnode,ndime,elmar(pelty)%deriv(1,1,igaus),elcod,&
                gpcar(1,1,igaus),gpdet(igaus),xjacm(1,1,igaus),xjaci(1,1,igaus))
           dvolu(igaus)=elmar(pelty)%weigp(igaus)*gpdet(igaus)
        end do gauss_points_cartesian

        ! Compute extra and intracellular conductivity matrices        

         if( kfl_coupl(ID_SOLIDZ,ID_EXMEDI) >= 1 .or. kfl_coupl(ID_EXMEDI,ID_SOLIDZ) >=1 ) then
!        if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
           if (kfl_gcoup_exm == 1) then
              !
              ! GPGDI: Deformation gradients
              !
!!$              gpgdi  = 0.0_rp
!!$              nfibe0 = 0.0_rp
!!$              nfibe  = 0.0_rp
!!$              do igaus=1,ngaus(pelty)
!!$                 do inode= 1,pnode
!!$                    gpsha= elmar(pelty)%shape(inode,igaus)
!!$                    do idime=1,ndime
!!$                       nfibe0(idime,igaus)= nfibe0(idime,igaus) + gpsha(inode,igaus) * elfib(idime,inode)
!!$                       do jdime=1,ndime
!!$                          gpgdi(idime,jdime,igaus)=gpgdi(idime,jdime,igaus)&
!!$                               +eldis(idime,inode)*gpcar(jdime,inode,igaus)
!!$                       end do
!!$                    end do
!!$                 end do
!!$                 gpgdi(1,1,igaus)= gpgdi(1,1,igaus) + 1.0_rp
!!$                 gpgdi(2,2,igaus)= gpgdi(2,2,igaus) + 1.0_rp
!!$                 if (ndime==3) gpgdi(3,3,igaus)= gpgdi(3,3,igaus) + 1.0_rp
!!$                 do idime=1,ndime
!!$                    do jdime=1,ndime
!!$                       nfibe(idime,igaus) = nfibe(idime,igaus) + gpgdi(idime,jdime,igaus) * nfibe0(jdime,igaus)
!!$                    end do
!!$                 end do
!!$              end do

              if (ittim == 1) then
                 ! the first time gpfib is still zero

                 call exm_comcnd(ielem,pmate,cndin,noion)

              else
                                  
                 do igaus=1,ngaus(pelty)
                    nfibe_length=0.0_rp
                    do idime=1,ndime
                       nfibe(idime,igaus)= gpfib(idime,igaus,ielem)
                       nfibe_length= nfibe_length + nfibe(idime,igaus) * nfibe(idime,igaus)
                    end do
                    nfibe_length= sqrt(nfibe_length)
                    do idime=1,ndime
                       nfibe(idime,igaus)= nfibe(idime,igaus) / nfibe_length
                    end do

!!                    call exm_comcnd(ielem,pmate,cndin,noion)   ! just to debug, do not uncomment
                    call exm_coucnd(pmate,cndin(1,1,igaus),nfibe(1,igaus),noion)

                 end do

              end if

           else

              call exm_comcnd(ielem,pmate,cndin,noion)

           end if

        else
           call exm_comcnd(ielem,pmate,cndin,noion)
        end if

        igacn = 1
        gauss_points: do igaus=1,ngaus(pelty)

           if (kfl_prdef_exm == 2) igacn=igaus   ! for nodal-field conductivity models

           wmatr = 0.0_rp
           wreco = 0.0_rp
           wnoli = 0.0_rp        
           wapcu = 0.0_rp        

           greap = 0.0_rp        
           griap = 0.0_rp        

           !     xpion = 0.0_rp
           !     xprcp = 0.0_rp

           xpion = 0.0_rp
           xgate = 0.0_rp

           if(llapl(pelty)==1) & 
                call elmhes(elmar(pelty)%heslo(1,1,igaus),hessi,ndime,pnode,ntens, &
                xjaci,d2sdx,elmar(pelty)%deriv,elcod)

           ! Previous time step unknown         
           xpapp = 0.0_rp
           unlst = 0.0_rp
           unprv = 0.0_rp

           do inode=1,pnode
              gpsha= elmar(pelty)%shape(inode,igaus)
              ! Applied currents Iapp (at THIS timestep)
              xpapp = xpapp + elcur(inode,1) * gpsha           
              ! Last unknown iap (at time n)
              unlst = unlst + eliap(inode,1) * gpsha  !eliap is elmag(1)
              ! Previous unknown iap (at time n-1)
              unprv = unprv + eliap(inode,2) * gpsha !eliap is elmag(4) 
           end do

           ! volume integral of eliap
           voaux(1) = voaux(1) + unlst * dvolu(igaus)

           xpold = (xmccm_exm * ximol * unlst) / dtime !ximol  is zero for TThet

           xpion = 0.0_rp
           xmion = 0.0_rp
           xlian = 0.0_rp
           xdtin = xmccm_exm / dtime

           if (kfl_spmod_exm(pmate) .ne. 0) then
              if (kfl_gemod_exm==0) then  ! for FHN
                 do inode=1,pnode
                    gpsha= elmar(pelty)%shape(inode,igaus)
                    ! Recovery potential w
                    do igate=1,ngate_exm
                       xgate(igate) = xgate(igate) + elgat(igate,inode,1) * gpsha
                    end do
                    
                    ! Ionic current (FHN function, non-linear in the intracellular a.p.) Iion (at THIS time step)
                    xlian = xlian + elian(inode,1) * gpsha
                 end do
                 
                 ! Right hand side contribution of the FHN
                 ! (fnol1 is not used any more...)
                 call exm_ionicu(-1_ip,xgate,xlian,xpion,xdumy,xdumy,xdumy)
                 
                 ! Matrix contribution of both the FHN and the time derivative
                 xmion = xdtin + fnol2 * taufi * (xlian - 1.0_rp) * (xlian - xmalp)
                 
                 if (kfl_nolim_exm == 1) then
                    xpion=  &
                         (fact1 * 0.5_rp                 - fact3 * xlian * xlian * 0.5_rp) * xlian
                    xmion= xdtin - &
                         (fact1 * 0.5_rp + fact2 * xlian + fact3 * xlian * xlian * 1.5_rp) * fnol2
                 end if
              else if (kfl_gemod_exm==1) then !  Cell models
                 xioni = 0.0_rp
                 do inode=1,pnode
                    gpsha= elmar(pelty)%shape(inode,igaus)
                    xioni = xioni + eliap(inode,1) * gpsha
                    xpion = xioni  ! Jaz took the minus -
                 end do
              end if
           end if

           ! Extracellular and intracellular action potential gradients 
           do idime=1,ndime
              do inode=1,pnode
                 greap(idime)=greap(idime) + gpcar(idime,inode,igaus) * eleap(inode,1)
                 griap(idime)=griap(idime) + gpcar(idime,inode,igaus) * eliap(inode,1)
              end do
           end do

           ! Contribution from the shock capturing
           shodi = 0.0_rp
           if (kfl_shock_exm >= 1) then
              if (kfl_spmod_exm(pmate) ==  0) then
                 xreac = 0.0_rp
              else if (kfl_spmod_exm(pmate) == 11) then
                 !              xreac = taufi *  xlian * (xlian - 1.0_rp) * (xlian - xmalp) + xgate(1)
                 xreac = taufi *  unprv * (unprv - 1.0_rp) * (unprv - xmalp) + xgate(1)
              else if (kfl_spmod_exm(pmate) == 12) then   
                 xreac = - xpion
              end if
              call elmlen(ndime,pnode,elmar(pelty)%dercg(1,1),tragl,elcod,hnatu(pelty),hleng)

              if(kfl_shock_exm < 11) then
                 dtedt=(unlst-unprv)*xdtin
              end if

              xgrno = griap(1)*griap(1)+griap(2)*griap(2)
              if (ndime == 3) xgrno = xgrno + griap(3)*griap(3)
              xgrno = sqrt(xgrno)

              reloc= dtedt+xreac-xpapp
              if(reloc<0.0_rp) reloc=-reloc

              if((reloc>1.0d-8).and.(xgrno>1.0d-8)) then
                 vepar=reloc/xgrno              
                 do idime=1,ndime
                    shodi(idime)= 0.5d0*hleng(1)*shock_exm*vepar
                 end do
              end if
           end if


           ! Extracellular and intracellular action potential gradients contributions to the RHS
           do idime=1,ndime
              grdif(idime)=0.0_rp
              if (kfl_genal_exm == 1) then  !!EXPLICIT SOLVE
                 grdif(idime) = cndin(1,idime,igacn) * griap(1) + cndin(2,idime,igacn) * griap(2)
                 if (ndime == 3) grdif(idime) = grdif(idime) + cndin(3,idime,igacn) * griap(3)
              else
                 grdif(idime) = cndin(1,idime,igacn) * greap(1) + cndin(2,idime,igacn) * greap(2)
                 if (ndime == 3) grdif(idime) = grdif(idime) + cndin(3,idime,igacn) * greap(3)
              end if

              if (shodi(idime) < cndin(idime,idime,igacn)) then
                 shodi(idime)= 0.0_rp
              else
                 shodi(idime)=  shodi(idime) - cndin(idime,idime,igacn)
              end if
              grdif(idime) = grdif(idime) + xexdi * shodi(idime) * griap(idime)
              grdif(idime) = grdif(idime) 
           end do

           if (kfl_spmod_exm(pmate) == 0) then                        !no ionic model
              do inode=1,pnode
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 ! (M w ) terms
                 wreco(inode) = 0.0_rp
                 ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  * (xpold + xpapp)
                 ! (- M Iion) term
                 wnoli(inode) = 0.0_rp
              end do
           else if (kfl_spmod_exm(pmate) == 11) then                  !fitzhugh nagumo
              if (kfl_nolum_exm == 1) then
                 xpion= 0.0_rp            !lumped non-linear terms
              end if
              do inode=1,pnode
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 ! (M w ) terms
                 wreco(inode) = wreco(inode) +  gpsha  * (- xgate(1))
                 ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  * (xpold + xpapp)
                 ! (- M Iion) term, i.e. the FHN non-linear term
                 wnoli(inode) = wnoli(inode) +  gpsha  *  xpion 
              end do
           else if (kfl_spmod_exm(pmate) == 12) then                 !fenton           
              !if (kfl_nolum_exm == 1) xpion= 0.0_rp            !lumped non-linear terms
              do inode=1,pnode
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  * (xpold + xpapp)
                 ! (- M Iion) term, i.e. the Fenton ionic term
                 wnoli(inode) = wnoli(inode) +  gpsha  *  xpion
              end do
           else if (kfl_spmod_exm(pmate) == 1) then                 !Ten Tuscher           
              !if (kfl_nolum_exm == 1) xpion= 0.0_rp            !lumped non-linear terms
              do inode=1,pnode
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 !  (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  *  (xpold + xpapp)
                 ! (- M Iion) term, i.e. the Fenton ionic term
                 wnoli(inode) = wnoli(inode) +  gpsha  *  xpion 
              end do
           else if (kfl_spmod_exm(pmate) == 4 .or. kfl_spmod_exm(pmate) == 6) then                 !Ten Tuscher Heterogeneous          
              !if (kfl_nolum_exm == 1) xpion= 0.0_rp            !lumped non-linear terms
              do inode=1,pnode
                 wreco(inode) = 0.0_rp
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  * (xpold + xpapp )
                 ! (- M Iion) term, i.e. the Fenton ionic term
                 wnoli(inode) = wnoli(inode) +  gpsha  *  xpion 
              end do
           else if (kfl_spmod_exm(pmate) == 5) then                 !O'Hara-Rudy          
              !if (kfl_nolum_exm == 1) xpion= 0.0_rp            !lumped non-linear terms
              do inode=1,pnode
                gpsha= elmar(pelty)%shape(inode,igaus)
                ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                wapcu(inode) = wapcu(inode) +  gpsha  *  (xpold )!!+ xpapp
                ! (- M Iion) term, i.e. the Fenton ionic term
                wnoli(inode) = wnoli(inode) +  gpsha  *  xpion 
              end do
           end if
           !end if

           !
           ! Elementary matrix contribution from each gauss point (ONLY IN IMPLICIT CASES):
           !
           ! Matrix  =  M ( Cm / dt  +  C(v-1)(v-alpha) )  +  0.5 K / Chi 

           if (kfl_genal_exm == 2) then           
              do inode=1,pnode
                 do idime=1,ndime
                    do jdime=1,ndime
                       wmatr(inode,1:pnode)=wmatr(inode,1:pnode) &
                            + gpcar(jdime,inode,igaus) &
                            *    ( cndex(idime,jdime,igacn)+cndin(idime,jdime,igacn) ) * ximdi & 
                            * gpcar(idime,1:pnode,igaus)
                    end do
                    wmatr(inode,1:pnode)=wmatr(inode,1:pnode) &
                         + gpcar(idime,1:pnode,igaus) * gpcar(idime,inode,igaus) * shodi(idime) * ximdi
                 end do
                 wmatr(inode,1:pnode)=wmatr(inode,1:pnode) &
                      +  elmar(pelty)%shape(1:pnode,igaus) * xmion *  elmar(pelty)%shape(inode,igaus)
              end do
           end if

           !
           ! Elementary RHS contribution from each gauss point:
           !
           ! RHS  =  - K (v+u)   +   M w   +   M Iapp  -  M Iion


           if (noion == 1) then
              !              wnoli= 0.0_rp
              !              wreco= 0.0_rp
              !              grdif= 0.0_rp
              !              grdif= 0.0_rp
           end if

           do inode=1,pnode
              wdiff=0.0_rp

              ! (K u) term: Extracellular action potential weak laplacian 
              do idime=1,ndime
                 wdiff = wdiff + gpcar(idime,inode,igaus) * grdif(idime)
              end do
              if (kfl_gemod_exm == 0) then
                 esili(inode) = esili(inode) &
                      + dvolu(igaus) * (     - wdiff + wapcu(inode) )       ! -  K (v+u) + M Iapp
                 enoli(inode) = enoli(inode) &
                      + dvolu(igaus) * ( wnoli(inode)+ wreco(inode) )       ! -  M Iion  + M w

              else if (kfl_gemod_exm == 1) then  !cell Models
                 esili(inode) = esili(inode) &
                      + dvolu(igaus) * (     - wdiff + wapcu(inode) )       ! -  K (v+u) + M Iapp!!  

                 enoli(inode) = enoli(inode) &
                      + dvolu(igaus) * ( wnoli(inode) )       ! -  M Iion
              end if
              if (kfl_genal_exm == 2) then
                 do jnode=1,pnode
                    elmat(inode,jnode) = elmat(inode,jnode) &
                         + dvolu(igaus) * wmatr(inode,jnode)
                 end do
              end if

           end do


        end do gauss_points


        ! Prescribe Dirichlet boundary conditions
        if (kfl_exboc_exm == 1) then
           do inode=1,pnode
              ipoin=lnods(inode,ielem)
              if(kfl_fixno_exm(1,ipoin)==1) then
                 adiag=elmat(inode,inode)
                 do jnode=1,pnode
                    elmat(inode,jnode)=0.0_rp
                    esili(jnode)=esili(jnode)-elmat(jnode,inode)*bvess_exm(1,ipoin)
                    elmat(jnode,inode)=0.0_rp
                 end do
                 elmat(inode,inode)=adiag
                 esili(inode)=adiag*bvess_exm(1,ipoin)
                 enoli(inode)= 0.0_rp
              end if
           end do
        end if


        if( lelch(ielem) == ELEXT ) then
           inode = 1
           ipoin = lnods(inode,ielem)
           rhsid(ipoin)       = rhsid(ipoin) + ( esili(inode) + enoli(inode) ) 
        else
           ! Assembly
           do inode=1,pnode
              ipoin=lnods(inode,ielem)
              rhsid(ipoin)       = rhsid(ipoin) + ( esili(inode) + enoli(inode) )  ! Complete
           end do
        end if

        if (kfl_genal_exm == 2) then           

           call runend('EXM_IAPELM: BI-DOMAIN MODELS NOT ALLOWED')

           !        call exm_assmat(&
           !             elmat,amatr,pnode,ndime,ndofn_exm,nevat_exm,&
           !             nunkn_exm,lnods(1,ielem),lpexm,kfl_algso_exm)
        end if


     end if


     ecgvo_exm(1) = ecgvo_exm(1) + voaux(1)


  end do elements

end subroutine exm_iapelm
