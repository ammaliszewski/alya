subroutine exm_ontina
!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_onecel.f90
!> @date    03/10/2012
!> @author  Mariano Vazquez
!> @brief   One cell simulations to obtain initial conditions
!> @details Runs for changes of cycle length and drug administration
!> @}
!------------------------------------------------------------------------ 
  use      def_master
  use      def_elmtyp
  use      def_domain
  use      def_exmedi
  
  implicit none
  
    if (kfl_hfmod_exm == 0) then  !if it's Normal
       if((onecl_exm(2) == 857) .and. (kfl_drugs_exm == 0)) then   !Normal at 70 bpm
           vmini_exm = -85.48_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
           vauin_exm(1,1,1:3) = 0.00172_rp           !Variable m
           vauin_exm(2,1,1:3) = 0.7444_rp        !Variable h 
           vauin_exm(3,1,1:3) = 0.7045_rp        !Variable j
           vauin_exm(4,1,1:3) = 0.00003373_rp           !Variable d
           vauin_exm(5,1,1:3) = 0.7888_rp         !Variable f
           vauin_exm(12,1,1:3) = 0.9755_rp        !Variable f2
           vauin_exm(6,1,1:3) = 0.9953_rp         ! Variable fCa
           vauin_exm(7,1,1:3) = 0.0000000242_rp           !Variable r
           vauin_exm(8,1,1:3) = 0.999998_rp           !Variable s
           vauin_exm(9,1,1:3) = 0.0095_rp        !Variable xs 
           vauin_exm(10,1,1:3) = 0.00621_rp        !Variable xr1
           vauin_exm(11,1,1:3) = 0.4712_rp       !Variable xr2
           vauin_exm(13,1,1:3) = 0.0_rp       !Variable mL
           vauin_exm(14,1,1:3) = 1.0_rp       !Variable hL 
           vcoin_exm(1,1,1:3) = 0.000126_rp                 !Variable Cai
           vcoin_exm(2,1,1:3) = 3.64_rp                 !Variable CaSR
           vcoin_exm(3,1,1:3) = 8.608_rp                 !Variable Nai
           vcoin_exm(4,1,1:3) = 136.89_rp                 !Variable Ki
           vcoin_exm(5,1,1:3) = 0.00036_rp                 !Variable CaSS
           vcoin_exm(9,1,1:3) = 0.9073_rp       !Variable Rprime
           
           vauin_exm(1,2,1:3) = 0.0016_rp           !Variable m
           vauin_exm(2,2,1:3) = 0.7573_rp        !Variables h 
           vauin_exm(3,2,1:3) = 0.7225_rp        !Variable j
           vauin_exm(4,2,1:3) = 0.00003164_rp           !Variable d
           vauin_exm(5,2,1:3) = 0.8009_rp         !Variable f
           vauin_exm(12,2,1:3) = 0.9778_rp          ! Variable f2
           vauin_exm(6,2,1:3) = 0.99530_rp         ! Variable fCa
           vauin_exm(7,2,1:3) = 0.00000002235_rp           !Variable r
           vauin_exm(8,2,1:3) = 0.3212_rp           !Variable s
           vauin_exm(9,2,1:3) = 0.0087_rp        !Variable xs 
           vauin_exm(10,2,1:3) = 0.0045_rp        !Variable xr1
           vauin_exm(11,2,1:3) = 0.4760_rp       !Variable xr2
           vauin_exm(13,1,1:3) = 0.0_rp       !Variable mL
           vauin_exm(14,1,1:3) = 1.0_rp       !Variable hL 
           vcoin_exm(1,2,1:3) = 0.00013_rp                 !Variable Cai
           vcoin_exm(2,2,1:3) = 3.7150_rp                 !Variable CaSR
           vcoin_exm(3,2,1:3) = 10.355_rp                 !Variable Nai
           vcoin_exm(4,2,1:3) = 138.4_rp                 !Variable Ki
           vcoin_exm(5,2,1:3) = 0.00036_rp                 !Variable CaSS
           vcoin_exm(9,2,1:3) = 0.9068_rp        !Variable Rprime
           
           vauin_exm(1,3,1:3) = 0.0017_rp           !Variable m
           vauin_exm(2,3,1:3) = 0.749_rp        !Variables h 
           vauin_exm(3,3,1:3) = 0.6788_rp        !Variable j
           vauin_exm(4,3,1:3) = 0.00003288_rp           !Variable d
           vauin_exm(5,3,1:3) = 0.7026_rp         !Variable f
           vauin_exm(12,3,1:3) = 0.9526_rp          !Variable f2
           vauin_exm(6,3,1:3) = 0.99420_rp         ! Variable fCa
           vauin_exm(7,3,1:3) = 0.00000002347_rp           !Variable r
           vauin_exm(8,3,1:3) = 1.0_rp           !Variable s
           vauin_exm(9,3,1:3) = 0.0174_rp        !Variable xs 
           vauin_exm(10,3,1:3) = 0.0165_rp        !Variable xr1
           vauin_exm(11,3,1:3) = 0.473_rp       !Variable xr2
           vauin_exm(13,1,1:3) = 0.0_rp       !Variable mL
           vauin_exm(14,1,1:3) = 1.0_rp       !Variable hL 
           vcoin_exm(1,3,1:3) = 0.000153_rp                 !Variable Cai
           vcoin_exm(2,3,1:3) = 4.2720_rp                 !Variable CaSR
           vcoin_exm(3,3,1:3) = 10.132_rp                 !Variable Nai
           vcoin_exm(4,3,1:3) = 138.52_rp                 !Variable Ki
           vcoin_exm(5,3,1:3) = 0.00042_rp                 !Variable CaSS
           vcoin_exm(9,3,1:3) = 0.8978_rp        !Variable Rprime
           return
       else if ((onecl_exm(2) /= 857) .or. (kfl_drugs_exm == 1))  then      !Normal with different heart rate
               
           elmlo_exm(1) = -85.48_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
           elmlo_exm(2) = -85.48_rp
        !do nbeat = 1, beat
           ituss_exm = 3    !epicardial
           vaulo_exm(1,1) = 0.00172_rp           !Variable m
           vaulo_exm(2,1) = 0.7444_rp        !Variable h 
           vaulo_exm(3,1) = 0.7045_rp        !Variable j
           vaulo_exm(4,1) = 0.00003373_rp           !Variable d
           vaulo_exm(5,1) = 0.7888_rp         !Variable f
           vaulo_exm(12,1) = 0.9755_rp        !Variable f2
           vaulo_exm(6,1) = 0.9953_rp         ! Variable fCa
           vaulo_exm(7,1) = 0.0000000242_rp           !Variable r
           vaulo_exm(8,1) = 0.999998_rp           !Variable s
           vaulo_exm(9,1) = 0.0095_rp        !Variable xs 
           vaulo_exm(10,1) = 0.00621_rp        !Variable xr1
           vaulo_exm(11,1) = 0.4712_rp       !Variable xr2
           vaulo_exm(13,1) = 0.0_rp       !Variable mL
           vaulo_exm(14,1) = 1.0_rp       !Variable hL 
           vcolo_exm(1,1) = 0.000126_rp                 !Variable Cai
           vcolo_exm(2,1) = 3.64_rp                 !Variable CaSR
           vcolo_exm(3,1) = 8.608_rp                 !Variable Nai
           vcolo_exm(4,1) = 136.89_rp                 !Variable Ki
           vcolo_exm(5,1) = 0.00036_rp                 !Variable CaSS
           vcolo_exm(9,1) = 0.9073_rp       !Variable Rprime
           call exm_ottina

           ituss_exm = 1    !endocardial
           vaulo_exm(1,1) = 0.0016_rp           !Variable m
           vaulo_exm(2,1) = 0.7573_rp        !Variables h 
           vaulo_exm(3,1) = 0.7225_rp        !Variable j
           vaulo_exm(4,1) = 0.00003164_rp           !Variable d
           vaulo_exm(5,1) = 0.8009_rp         !Variable f
           vaulo_exm(12,1) = 0.9778_rp          ! Variable f2
           vaulo_exm(6,1) = 0.99530_rp         ! Variable fCa
           vaulo_exm(7,1) = 0.00000002235_rp           !Variable r
           vaulo_exm(8,1) = 0.3212_rp           !Variable s
           vaulo_exm(9,1) = 0.0087_rp        !Variable xs 
           vaulo_exm(10,1) = 0.0045_rp        !Variable xr1
           vaulo_exm(11,1) = 0.4760_rp       !Variable xr2
           vaulo_exm(13,1) = 0.0_rp       !Variable mL
           vaulo_exm(14,1) = 1.0_rp       !Variable hL 
           vcolo_exm(1,1) = 0.00013_rp                 !Variable Cai
           vcolo_exm(2,1) = 3.7150_rp                 !Variable CaSR
           vcolo_exm(3,1) = 10.355_rp                 !Variable Nai
           vcolo_exm(4,1) = 138.4_rp                 !Variable Ki
           vcolo_exm(5,1) = 0.00036_rp                 !Variable CaSS
           vcolo_exm(9,1) = 0.9068_rp        !Variable Rprime
           call exm_ottina

           ituss_exm = 2    ! MIDMYOCARDIAL                     
           vaulo_exm(1,1) = 0.0017_rp           !Variable m
           vaulo_exm(2,1) = 0.749_rp        !Variables h 
           vaulo_exm(3,1) = 0.6788_rp        !Variable j
           vaulo_exm(4,1) = 0.00003288_rp           !Variable d
           vaulo_exm(5,1) = 0.7026_rp         !Variable f
           vaulo_exm(12,1) = 0.9526_rp          !Variable f2
           vaulo_exm(6,1) = 0.99420_rp         ! Variable fCa
           vaulo_exm(7,1) = 0.00000002347_rp           !Variable r
           vaulo_exm(8,1) = 1.0_rp           !Variable s
           vaulo_exm(9,1) = 0.0174_rp        !Variable xs 
           vaulo_exm(10,1) = 0.0165_rp        !Variable xr1
           vaulo_exm(11,1) = 0.473_rp       !Variable xr2
           vaulo_exm(13,1) = 0.0_rp       !Variable mL
           vaulo_exm(14,1) = 1.0_rp       !Variable hL 
           vcolo_exm(1,1) = 0.000153_rp                 !Variable Cai
           vcolo_exm(2,1) = 4.2720_rp                 !Variable CaSR
           vcolo_exm(3,1) = 10.132_rp                 !Variable Nai
           vcolo_exm(4,1) = 138.52_rp                 !Variable Ki
           vcolo_exm(5,1) = 0.00042_rp                 !Variable CaSS
           vcolo_exm(9,1) = 0.8978_rp        !Variable Rprime
           call exm_ottina
               !end do
           return
       end if
           
    else if(kfl_hfmod_exm==1) then
       if((onecl_exm(2) == 857) .and. (kfl_drugs_exm == 0)) then   !Heart Failure at 70 bpm
 
           vmini_exm = -83.4949_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL   
           !EPICARDIAL HEART FAILURE
           !VOLTAGE -82.5277658119425
           vauin_exm(1,1,1:3) = 0.00303276091526701_rp
           vauin_exm(2,1,1:3) = 0.661659742738716_rp
           vauin_exm(3,1,1:3) = 0.634628244886488_rp
           vauin_exm(4,1,1:3) = 4.83560182302662e-05_rp
           vauin_exm(5,1,1:3) = 0.926032127446219_rp
           vauin_exm(12,1,1:3) = 0.999241154724418_rp
           vauin_exm(6,1,1:3) = 0.999869780216330_rp
           vauin_exm(7,1,1:3) = 3.79981651056992e-08_rp
           vauin_exm(8,1,1:3) = 0.999996272388211_rp
           vauin_exm(9,1,1:3) = 0.00455055453605926_rp
           vauin_exm(10,1,1:3) = 0.00168040788756380_rp
           vauin_exm(11,1,1:3) = 0.443217253596480_rp
           vauin_exm(13,1,1:3) = 0.0_rp       !Variable mL
           vauin_exm(14,1,1:3) = 1.0_rp       !Variable hL            
           vcoin_exm(1,1,1:3) = 8.63303349957043e-05_rp
           vcoin_exm(2,1,1:3) = 2.97266869543131_rp
           vcoin_exm(3,1,1:3) = 9.72525639401306_rp
           vcoin_exm(4,1,1:3) = 135.848336580048_rp
           vcoin_exm(5,1,1:3) = 0.000257031579830260_rp
           vcoin_exm(9,1,1:3) = 0.980074819847184_rp
           
           !!!  ENDOCARDIAL  HEART FAILURE
           !  VOLTAGE -84.2920820453902
           vauin_exm(1,2,1:3) = 0.00209247866395593_rp
           vauin_exm(2,2,1:3) = 0.717586845558180_rp
           vauin_exm(3,2,1:3) = 0.703610324031878_rp
           vauin_exm(4,2,1:3) = 3.82182239461387e-05_rp
           vauin_exm(5,2,1:3) = 0.927116903038717_rp
           vauin_exm(12,2,1:3) = 0.997941159192524_rp
           vauin_exm(6,2,1:3) = 0.999857972863601_rp
           vauin_exm(7,2,1:3) = 2.82975518708235e-08_rp
           vauin_exm(8,2,1:3) = 0.499007377734646_rp
           vauin_exm(9,2,1:3) = 0.00404264224963596_rp
           vauin_exm(10,2,1:3) = 0.000568838457062049_rp
           vauin_exm(11,2,1:3) = 0.461434647116189_rp
           vauin_exm(13,1,1:3) = 0.0_rp       !Variable mL
           vauin_exm(14,1,1:3) = 1.0_rp       !Variable hL 
           vcoin_exm(1,2,1:3) = 0.000132911621580043_rp
           vcoin_exm(2,2,1:3) = 3.97092446663378_rp
           vcoin_exm(3,2,1:3) = 10.1377699148182_rp
           vcoin_exm(4,2,1:3) = 138.592632537711_rp
           vcoin_exm(5,2,1:3) = 0.000283334727151014_rp
           vcoin_exm(9,2,1:3) = 0.979004485438106_rp
      
            !mid HEART FAILURE
           ! Voltage:  -83.6649251312024
           vauin_exm(1,3,1:3) = 0.00238853756757240_rp           !Variable m
           vauin_exm(2,3,1:3) = 0.698129858756220_rp        !Variables h 
           vauin_exm(3,3,1:3) = 0.670007606728020_rp        !Variable j
           vauin_exm(4,3,1:3) = 4.15544209009521e-05_rp           !Variable d
           vauin_exm(5,3,1:3) = 0.882815089054957_rp         !Variable f
           vauin_exm(12,3,1:3) = 0.995047915633200_rp          !Variable f2
           vauin_exm(6,3,1:3) = 0.999809858618127_rp         ! Variable fCa
           vauin_exm(7,3,1:3) = 3.14471296989901e-08_rp           !Variable r
           vauin_exm(8,3,1:3) = 0.999997027982031_rp           !Variable s
           vauin_exm(9,3,1:3) = 0.00539523790336693_rp        !Variable xs 
           vauin_exm(10,3,1:3) = 0.00221286069613379_rp        !Variable xr1
           vauin_exm(11,3,1:3) = 0.454935810543141_rp       !Variable xr2
           vauin_exm(13,1,1:3) = 0.0_rp       !Variable mL
           vauin_exm(14,1,1:3) = 1.0_rp       !Variable hL 
           vcoin_exm(1,3,1:3) = 0.000148758208418421_rp                 !Variable Cai
           vcoin_exm(2,3,1:3) = 4.50025313681586_rp                 !Variable CaSR
           vcoin_exm(3,3,1:3) = 9.73203055605199_rp                 !Variable Nai
           vcoin_exm(4,3,1:3) = 138.900841456135_rp                 !Variable Ki
           vcoin_exm(5,3,1:3) = 0.000316819832530177_rp                 !Variable CaSS
           vcoin_exm(9,3,1:3) = 0.976140456252427_rp        !Variable Rprime
           return       
                
       else if((onecl_exm(2) /= 857) .or. (kfl_drugs_exm == 1))  then
 
           elmlo_exm(1) = -83.4949_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
           elmlo_exm(2) = -83.4949_rp
           !do nbeat = 1, beat
           ituss_exm = 3    !epicardial
            !EPICARDIAL HEART FAILURE
            !VOLTAGE -82.5277658119425
           vaulo_exm(1,1) = 0.00303276091526701_rp
           vaulo_exm(2,1) = 0.661659742738716_rp
           vaulo_exm(3,1) = 0.634628244886488_rp
           vaulo_exm(4,1) = 4.83560182302662e-05_rp
           vaulo_exm(5,1) = 0.926032127446219_rp
           vaulo_exm(12,1) = 0.999241154724418_rp
           vaulo_exm(6,1) = 0.999869780216330_rp
           vaulo_exm(7,1) = 3.79981651056992e-08_rp
           vaulo_exm(8,1) = 0.999996272388211_rp
           vaulo_exm(9,1) = 0.00455055453605926_rp
           vaulo_exm(10,1) = 0.00168040788756380_rp
           vaulo_exm(11,1) = 0.443217253596480_rp
           vaulo_exm(13,1) = 0.0_rp       !Variable mL
           vaulo_exm(14,1) = 1.0_rp       !Variable hL 
           vcolo_exm(1,1) = 8.63303349957043e-05_rp
           vcolo_exm(2,1) = 2.97266869543131_rp
           vcolo_exm(3,1) = 9.72525639401306_rp
           vcolo_exm(4,1) = 135.848336580048_rp
           vcolo_exm(5,1) = 0.000257031579830260_rp
           vcolo_exm(9,1) = 0.980074819847184_rp
           call exm_ottina

      !do nbeat = 1, beat
           ituss_exm = 1    !endocardial
                    !!!  ENDOCARDIAL  HEART FAILURE
            !  VOLTAGE -84.2920820453902
           vaulo_exm(1,1) = 0.00209247866395593_rp
           vaulo_exm(2,1) = 0.717586845558180_rp
           vaulo_exm(3,1) = 0.703610324031878_rp
           vaulo_exm(4,1) = 3.82182239461387e-05_rp
           vaulo_exm(5,1) = 0.927116903038717_rp
           vaulo_exm(12,1) = 0.997941159192524_rp
           vaulo_exm(6,1) = 0.999857972863601_rp
           vaulo_exm(7,1) = 2.82975518708235e-08_rp
           vaulo_exm(8,1) = 0.499007377734646_rp
           vaulo_exm(9,1) = 0.00404264224963596_rp
           vaulo_exm(10,1) = 0.000568838457062049_rp
           vaulo_exm(11,1) = 0.461434647116189_rp
           vaulo_exm(13,1) = 0.0_rp       !Variable mL
           vaulo_exm(14,1) = 1.0_rp       !Variable hL 
           vcolo_exm(1,1) = 0.000132911621580043_rp
           vcolo_exm(2,1) = 3.97092446663378_rp
           vcolo_exm(3,1) = 10.1377699148182_rp
           vcolo_exm(4,1) = 138.592632537711_rp
           vcolo_exm(5,1) = 0.000283334727151014_rp
           vcolo_exm(9,1) = 0.979004485438106_rp
           call exm_ottina

        !do nbeat = 1, beat
           ituss_exm = 2    !midmyocardial                                    
                     !mid HEART FAILURE
            ! Voltage:  -83.6649251312024
           vaulo_exm(1,1) = 0.00238853756757240_rp           !Variable m
           vaulo_exm(2,1) = 0.698129858756220_rp        !Variables h 
           vaulo_exm(3,1) = 0.670007606728020_rp        !Variable j
           vaulo_exm(4,1) = 4.15544209009521e-05_rp           !Variable d
           vaulo_exm(5,1) = 0.882815089054957_rp         !Variable f
           vaulo_exm(12,1) = 0.995047915633200_rp          !Variable f2
           vaulo_exm(6,1) = 0.999809858618127_rp         ! Variable fCa
           vaulo_exm(7,1) = 3.14471296989901e-08_rp           !Variable r
           vaulo_exm(8,1) = 0.999997027982031_rp           !Variable s
           vaulo_exm(9,1) = 0.00539523790336693_rp        !Variable xs 
           vaulo_exm(10,1) = 0.00221286069613379_rp        !Variable xr1
           vaulo_exm(11,1) = 0.454935810543141_rp       !Variable xr2
           vaulo_exm(13,1) = 0.0_rp       !Variable mL
           vaulo_exm(14,1) = 1.0_rp       !Variable hL 
           vcolo_exm(1,1) = 0.000148758208418421_rp                 !Variable Cai
           vcolo_exm(2,1) = 4.50025313681586_rp                 !Variable CaSR
           vcolo_exm(3,1) = 9.73203055605199_rp                 !Variable Nai
           vcolo_exm(4,1) = 138.900841456135_rp                 !Variable Ki
           vcolo_exm(5,1) = 0.000316819832530177_rp                 !Variable CaSS
           vcolo_exm(9,1) = 0.976140456252427_rp        !Variable Rprime
           call exm_ottina
           return
       end if          
    end if
    !end if

end subroutine exm_ontina
