!-----------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_outvar.f90
!> @author  Mariano Vazquez
!> @brief   This routine bridges output with postpr
!> @date    16/11/1966
!> @details This routine bridges output with postpr
!> @} 
!-----------------------------------------------------------------------
subroutine exm_outvar(ivari)
  !-----------------------------------------------------------------------
  !****f* exmedi/exm_outvar
  ! NAME 
  !    exm_outvar
  ! DESCRIPTION
  !    This routine bridges output with postpr
  ! USES
  ! USED BY
  !    exm_output
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_postpr

  use      def_exmedi

  use      mod_memchk
  use      mod_postpr
  use      mod_iofile

  implicit none
  integer(ip), intent(in)  :: ivari
  integer(ip)              :: ibopo,icurr,iconc
  integer(ip)              :: ippva,ipopo,ipoin,idime
  character(35)            :: messa,chdum
  real(rp)                 :: xauxi,xveca(3),xvalp,xvalq,discr,xauxa,xauxb,xauxc,temp1,xvap1,xvap2,xvap3,&
       vap2r,vap2i,xvalv,xvalu,phi,temp2,vauxi
  complex(rp)              :: xval1,xval2,xvaps(3)


  if ( ivari== 0_ip ) then

     return

  else if (ivari == 1_ip ) then

     if( kfl_prdef_exm > 0 ) then
        gevec => fiber_exm
     else
        return
     end if

  else if (ivari ==2_ip .or. ivari==3) then 

     if( kfl_prdef_exm > 0 ) then
        
        if ( INOTMASTER ) then
           
           call exm_gravec(fiber,grafi_exm(1:ndime,1:ndime,1:npoin))

           ! calcular los valores propios

           if (ivari==2) then   ! eigenvalues
              do ipoin=1,npoin
                 
                 if (.not.(exm_zonenode(ipoin))) cycle
                 
                 xauxa = -(grafi_exm(1,1,ipoin) + grafi_exm(2,2,ipoin) + grafi_exm(3,3,ipoin))
                 xauxb = grafi_exm(1,1,ipoin)*grafi_exm(2,2,ipoin) + &
                      grafi_exm(1,1,ipoin)*grafi_exm(3,3,ipoin) + &
                      grafi_exm(2,2,ipoin)*grafi_exm(3,3,ipoin) - &
                      grafi_exm(1,2,ipoin)*grafi_exm(2,1,ipoin) - &
                      grafi_exm(1,3,ipoin)*grafi_exm(3,1,ipoin) - &
                      grafi_exm(2,3,ipoin)*grafi_exm(3,2,ipoin) 
                 xauxc = grafi_exm(1,1,ipoin)*grafi_exm(2,3,ipoin)*grafi_exm(3,2,ipoin) + &
                      grafi_exm(1,2,ipoin)*grafi_exm(2,1,ipoin)*grafi_exm(3,3,ipoin) + &
                      grafi_exm(1,3,ipoin)*grafi_exm(2,2,ipoin)*grafi_exm(3,1,ipoin) - &
                      grafi_exm(1,1,ipoin)*grafi_exm(2,2,ipoin)*grafi_exm(3,3,ipoin) - &
                      grafi_exm(1,2,ipoin)*grafi_exm(2,3,ipoin)*grafi_exm(3,1,ipoin) - &
                      grafi_exm(1,3,ipoin)*grafi_exm(2,1,ipoin)*grafi_exm(3,2,ipoin)
                 
                 xvalp = xauxb - xauxa**2/3.0_rp
                 xvalq = (2.0_rp*xauxa**3 - 9.0_rp*xauxa*xauxb + 27.0_rp*xauxc)/27.0_rp
                 discr = (xvalp/3.0_rp)**3 + (xvalq/2.0_rp)**2
                 
                 if(discr .lt. 0.0_rp)then        ! 3 real roots -- use the trigonometric formulation
                    phi = acos(-xvalq/2.0_rp/sqrt(abs(xvalp*xvalp*xvalp)/27.0_rp))
                    temp1= 2.0_rp*sqrt(abs(xvalp)/3.0_rp)
                    xvap1 =  temp1*cos(phi/3.0_rp)
                    xvap2 = -temp1*cos((phi+pi)/3.0_rp)
                    xvap3 = -temp1*cos((phi-pi)/3.0_rp)
                 else                             ! 1 real root & 2 conjugate complex roots or 3 real roots
                    temp1 = -xvalq/2.0_rp + sqrt(discr)
                    temp2 = -xvalq/2.0_rp - sqrt(discr)
                    xvalu = abs(temp1)**(1.0_rp/3.0_rp)
                    xvalv = abs(temp2)**(1.0_rp/3.0_rp)
                    if(temp1 .lt. 0.0_rp) xvalu=-xvalu
                    if(temp2 .lt. 0.0_rp) xvalv=-xvalv
                    xvap1  = xvalu + xvalv
                    vap2r = -(xvalu + xvalv)/2.0_rp
                    vap2i =  (xvalu-xvalv)*sqrt(3.0_rp)/2.0_rp
                 end if
                 
                 temp1 = xauxa/3.0_rp
                 xvap1 = xvap1 - temp1
                 xvap2 = xvap2 - temp1
                 xvap3 = xvap3 - temp1
                 vap2r = vap2r - temp1
                 
                 if(discr .lt. 0.0_rp)then
                    xvaps(1) = cmplx( xvap1, 0.0_rp)
                    xvaps(2) = cmplx( xvap2, 0.0_rp)
                    xvaps(3) = cmplx( xvap3, 0.0_rp)
                 else if(discr .eq. 0.0_rp)then
                    xvaps(1) = cmplx( xvap1, 0.0_rp)
                    xvaps(2) = cmplx( vap2r, 0.0_rp)
                    xvaps(3) = cmplx( vap2r, 0.0_rp)
                 else
                    xvaps(1) = cmplx( xvap1, 0.0_rp)
                    xvaps(2) = cmplx( vap2r, vap2i)
                    xvaps(3) = cmplx( vap2r, -vap2i)
                 end if
                 
                 ! los guardo en grafi
                 
                 if (ndime .eq. 2)then
                    vauxi = sqrt(vmass(ipoin))
                 else
                    vauxi = vmass(ipoin)**(1.0_rp/3.0_rp)
                 end if
                 
                 grafi_exm(1,1,ipoin) = Real(xvaps(1))*vauxi
                 grafi_exm(1,2,ipoin) = Real(xvaps(2))*vauxi
                 grafi_exm(1,3,ipoin) = Real(xvaps(3))*vauxi
              end do
           else if(ivari == 3) then
              do ipoin= 1,npoin
                 
                 if (.not.(exm_zonenode(ipoin))) cycle
           
                 xvaps(1) = grafi_exm(1,1,ipoin) * grafi_exm(1,1,ipoin) &
                      + grafi_exm(2,1,ipoin) * grafi_exm(2,1,ipoin)
                 xvaps(2) = grafi_exm(1,2,ipoin) * grafi_exm(1,2,ipoin) &
                      + grafi_exm(2,2,ipoin) * grafi_exm(2,2,ipoin)
                 if (ndime==3) then
                    xvaps(1) = xvaps(1) &
                      + grafi_exm(3,1,ipoin) * grafi_exm(3,1,ipoin) 
                    xvaps(2) = xvaps(2) &
                      + grafi_exm(3,2,ipoin) * grafi_exm(3,2,ipoin) 
                    xvaps(3) = grafi_exm(1,3,ipoin) * grafi_exm(1,3,ipoin) &
                         + grafi_exm(2,3,ipoin) * grafi_exm(2,3,ipoin) &
                         + grafi_exm(3,3,ipoin) * grafi_exm(3,3,ipoin) 
                 end if
                 grafi_exm(1,1,ipoin) = sqrt(xvaps(1))
                 grafi_exm(1,2,ipoin) = sqrt(xvaps(2))
                 grafi_exm(1,3,ipoin) = sqrt(xvaps(3))
              end do
           end if
           
           do ipoin=1, npoin           
              if (.not.(exm_zonenode(ipoin))) cycle

              grafi_exm(1:ndime,1:ndime,ipoin) = grafi_exm(1:ndime,1:ndime,ipoin)*vmass(ipoin)**(0.3333_rp)
           end do
           
           ! los postprocesas
           gevec => grafi_exm(1,1:ndime,1:npoin)

        end if
     else
        return
     end if
     
  else if  ( ivari == 4_ip ) then
     !
     ! Concentrations
     !
          
     if ( INOTMASTER ) then

        call memgen(zero, nconc_exm, npoin)
        do ipoin = 1,npoin     
           if (.not.(exm_zonenode(ipoin))) cycle           
           do iconc= 1,nconc_exm
              gevec(iconc,ipoin) = vconc(iconc,ipoin,1)
           end do
        end do
     else
         !
        ! This is necessary for outvar to get the right dimension
        !
        call memgen(0_ip,nconc_exm,1_ip)
       
     end if

  else if ( ivari == 20_ip ) then 
     
     if ( INOTMASTER ) then
        do ipoin= 1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           unkno(ipoin) = elmag(1,ipoin,1) * ( poref_exm(2) - poref_exm(1) ) + poref_exm(1)  ! output V in correct scale
        end do
        gesca => unkno
     end if

  else if  ( ivari == 21_ip ) then 

     if ( INOTMASTER ) then
        do ipoin = 1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           unkno(ipoin) = elmag(2,ipoin,1)
        end do
        gesca => unkno
     end if

  else if  (ivari == 22_ip ) then

     if ( INOTMASTER ) then
        do ipoin = 1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           unkno(ipoin) = refhn_exm(ipoin,1)
        end do
        gesca => unkno
     end if

  else if  ( ivari == 23_ip ) then

     if ( INOTMASTER ) then
        do ipoin = 1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           unkno(ipoin) = fiber(1,ipoin)
        end do
        gesca => unkno
     end if

  else if  ( ivari == 24_ip ) then

     if ( INOTMASTER ) then
        do ipoin = 1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           unkno(ipoin) = fiber(2,ipoin)
        end do
        gesca => unkno
     end if
 
  else if  ( ivari == 25_ip ) then

     if ( INOTMASTER ) then
        do ipoin = 1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           unkno(ipoin) = fiber(ndime,ipoin)
        end do
        gesca => unkno
     end if

  else if ( ivari == 26_ip ) then

     if ( INOTMASTER ) then
        gesca => fisoc(:,1)
     end if

  else if (ivari == 27_ip ) then
     
     if ( INOTMASTER ) then
        gevec => cecon_exm(1,:,:)
     end if

  else if (ivari == 28_ip ) then
     
     if ( INOTMASTER ) then
        gevec => cecon_exm(2,:,:)
     end if

  else if (ivari == 29_ip ) then
     
     if ( INOTMASTER ) then
        gevec => cecon_exm(3,:,:)
     end if
!+MRV
  else if (ivari == 30_ip ) then

     if ( INOTMASTER ) then
        gevec => fibe2_exm     
     end if


  else if (ivari == 31_ip ) then

     !
     ! Currents
     !
          
     if ( INOTMASTER ) then

        call memgen(zero, nicel_exm, npoin)
        do ipoin = 1,npoin     
           if (.not.(exm_zonenode(ipoin))) cycle           
           do icurr= 1,nicel_exm
              gevec(icurr,ipoin) = vicel_exm(icurr,ipoin,1)
           end do
        end do

     else
         !
        ! This is necessary for outvar to get the right dimension
        !
        call memgen(0_ip,nicel_exm,1_ip)
       
     end if


  end if
  !
  ! Postprocess
  !
  
  call outvar(&
       ivari,&
       ittim,cutim,postp(1) % wopos(1,ivari))

end subroutine exm_outvar
