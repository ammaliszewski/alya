subroutine exm_turnof
!-----------------------------------------------------------------------
!****f* Exmedi/exm_turnof
! NAME 
!    exm_turnof
! DESCRIPTION
!    This routine closes the run for the current module
! USES
!    exm_outcpu
!    exm_output
! USED BY
!    Exmedi
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_exmedi
  implicit none

end subroutine exm_turnof

