!---------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_auxhet.f90
!> @author  Jazmin Aguado-Sierra
!> @brief   Gating variables of TenTuscher-Panfilov 2006 heterogeneous model
!> @details Calculate every gating variable at every time step\n
!! The ODEs of this routine respond to Hodgkin-Huxley formalism:\n
!!
!!     du / dt = (u_inf - u) / tau     \n
!!
!! u represents activation/inactivation variable \n
!! u_inf and tau have to been specified in order to solve the equation (u_inf and tau are voltage-dependent)\n
!!
!! The auxiliary variables are 12:\n
!!   - m:     vauxi_exm(1,ipoin,1)   -->  activation gate (for I_Na current)\n
!!   - h:     vauxi_exm(2,ipoin,1)   -->  fast inactivation gate (for I_Na current)\n
!!   - j:     vauxi_exm(3,ipoin,1)   -->  slow inactivation gate (for I_Na current)\n
!!   - d:     vauxi_exm(4,ipoin,1)   -->  voltage-dependent activation gate (for I_CaL current)            \n
!!   - f:     vauxi_exm(5,ipoin,1)   -->  voltage-dependent inactivation gate (for I_CaL current)\n
!!   - fcass:   vauxi_exm(6,ipoin,1) -->  intracellular calcium-dependent inactivation gate (for I_CaL current)     \n      
!!   - r:     vauxi_exm(7,ipoin,1)   -->  
!!            voltage-dependent activation gate (for I_to current)\n
!!   - s:     vauxi_exm(8,ipoin,1)   -->  
!!            voltage-dependent inactivation gate (for I_to current)\n
!!   - xs:    vauxi_exm(9,ipoin,1)   -->  activation gate (for I_Ks current)\n
!!   - xr1:   vauxi_exm(10,ipoin,1)  -->  activation gate (for I_Kr current)\n
!!   - xr2:   vauxi_exm(11,ipoin,1)  -->  inactivation gate (for I_Kr current)\n
!!   - f2:    vauxi_exm(12,ipoin,1)
!!            calcium-dependent inactivation gate (for I_rel current)\n
!> @} 
!------------------------------------------------------------------------
subroutine exm_auxhet

  use      def_parame
  use      def_elmtyp
  use      def_master
  use      def_domain
  use      def_exmedi

  implicit none

  integer(ip) :: ipoin,kfl_odets_exm
  real(rp)    :: thold, thnew, dtimeEP
  real(rp)    :: vinf, alphx, alph1, alph2, betax, beta1, beta2, gammax, taux, tau1, tau2, tau3, xitaux
  real(rp)    :: vaux0, vaux1, vaux2, vaux3, vaux4, vaux5
  
! La variable ituss_exm indica a que parte del musculo cardiaco pertenece la celula
  !integer(ip) :: ituss_exm=ituss_exm  !(3=epicardial, 1=endocardial, 2=M cells)

! DESCRIPTION OF VARIABLES

! ipoin: point number (it is related with a node)

! thold: relaxation coefficient of old RHS used for computation of non-linear iteration
! thnew: relaxation coefficient of new RHS used for computation of non-linear iteration (thold+thnew=1)

! vinf: 'infinite' value of activation variable (depends on voltage) of Hodgkin-Huxley formalism (u_inf)
! alphx: alpha rate coefficient used by activation variables (Hodgkin-Huxley formalism)
! alph1: auxiliary rate coefficient used for computation of alphx
! alph2: auxiliary rate coefficient used for computation of alphx 
! betax: beta rate coefficient used by activation variables (Hodgkin-Huxley formalism)
! beta1: auxiliary rate coefficient used for computation of betax
! beta2: auxiliary rate coefficient used for computation of betax
! gammax: auxiliary variable used for computation of taux
! taux: time 'constant' (depends on voltage value) of Hodgkin-Huxley formalism
! tau1: auxiliary variable used for computation of taux
! tau2: auxiliary variable used for computation of taux
! xitaux: inverse of time 'constant' (depends on voltage value) of Hodgkin-Huxley formalism (tau)

! vaux0, vaux1, vaux2, vaux3, vaux4, vaux5: auxiliar variables 0, 1, 2, 3, 4 and 5
  thnew = 0.5_rp            
  thold = 1.0_rp - thnew       
  kfl_odets_exm = 1
  dtimeEP=dtime * 1000.0_rp
                
  if(INOTMASTER) then
    do ipoin= 1,npoin  
       ituss_exm = int(celty_exm(1,ipoin))
         ! Variable of activation m (related with I_Na current)
         ! nauxi_exm = 1
         ! "A model for ventricular tissue", Ten Tusscher, Page H1585 (col 2)

         vinf = 1.0_rp + exp((-56.86_rp - volts_exm(1,ipoin,1)) / 9.03_rp)
         vinf = vinf * vinf
         vinf = 1.0_rp / vinf              ! Value of m_infinite
    
         alphx = 1.0_rp + exp((-60.00_rp - volts_exm(1,ipoin,1)) / 5.0_rp)
         alphx = 1.0_rp / alphx            ! Value of alpha_m
    !   beta_m = 0.1/(1+(exp(((V+35)/5))))+0.1/(1+(exp(((V - 50)/200))))
         beta1 = 1.0_rp + exp(( 35.00_rp + volts_exm(1,ipoin,1)) / 5.0_rp)
         beta1 = 0.1_rp / beta1
         beta2 = 1.0_rp + exp((-50.00_rp + volts_exm(1,ipoin,1)) / 200.0_rp)
         beta2 = 0.1_rp / beta2
         betax = beta1 + beta2             ! Value of beta_m
    
         taux  = betax * alphx             ! Value of tau_m
         xitaux = 1.0_rp / taux            ! Value of 1/tau_m 
         
         vaux0 = vauxi_exm(1,ipoin,3)      ! Value of m in previous time step 
    
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(1,ipoin,1) = vaux3      ! Value of variable m
    
    
    ! Variable of activation h (related with I_Na current)
    ! nauxi_exm = 2
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
    
         vinf = 1.0_rp + exp((71.55_rp + volts_exm(1,ipoin,1)) / 7.43_rp)
         vinf = vinf * vinf
         vinf = 1.0_rp / vinf              ! Value of h_infinite
    
         if (volts_exm(1,ipoin,1) >= -40.0) then
    
           alphx = 0                       ! Value of alpha_h
    
           betax = 0.13_rp * ( 1.0_rp + exp((-10.66_rp - volts_exm(1,ipoin,1)) / 11.1_rp) )
           betax = 0.77_rp / betax         ! Value of beta_h
    
          else
    
           alphx = 0.057_rp * exp((-80.0_rp - volts_exm(1,ipoin,1)) / 6.8_rp)        ! Value of alpha_h
    !   ( 2.7*(exp(( 0.079*V)))+ 310000*(exp((0.3485*V)))) 
           beta1 = 2.7_rp * exp(0.079_rp * volts_exm(1,ipoin,1))
           beta2 = 310000.0_rp * exp(0.3485_rp * volts_exm(1,ipoin,1))
           betax = beta1 + beta2           ! Value of beta_h
    
         end if
    
         xitaux  = betax + alphx          ! Value of 1/tau_h
    
         vaux0 = vauxi_exm(2,ipoin,3)     ! Value of h in previous time step 
    
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then     ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
         vauxi_exm(2,ipoin,1) = vaux3      ! Value of variable h
    
    ! Variable of activation j (related with I_Na current)
    ! nauxi_exm = 3
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
    
         vinf = 1.0_rp + exp((71.55_rp + volts_exm(1,ipoin,1)) / 7.43_rp)
         vinf = vinf * vinf
         vinf = 1.0_rp / vinf              ! Value of j_infinite
    
         if (volts_exm(1,ipoin,1) >= -40.0) then
    
           alphx = 0                       ! Value of alpha_j
    
           beta1 = 1.0_rp + exp(-0.1_rp * (32.0_rp + volts_exm(1,ipoin,1)))
           beta1 = 1.0_rp / beta1
           beta2 = 0.6_rp * exp(0.057_rp * volts_exm(1,ipoin,1))
           betax = beta1 * beta2           ! Value of beta_j
    
          else
    !    (( ( (-25428*exp( 0.2444*V)) -  (6.948e-6*exp(-0.04391*V)) )*(V+37.78) ) / (1+exp( 0.311*(V+79.23)) ))
           alph1 = 1.0_rp + exp(0.311_rp * (79.23_rp + volts_exm(1,ipoin,1)))
           alph1 = 1.0_rp / alph1
           alph2 = - 25428.0_rp * exp(0.2444_rp * volts_exm(1,ipoin,1))
           alph2 = alph2 - (0.000006948_rp * exp(-0.04391_rp * volts_exm(1,ipoin,1)))
           alph2 = alph2 * (volts_exm(1,ipoin,1) + 37.78_rp)
           alphx = alph1 * alph2           ! Value of alpha_j
    
           beta1 = 1.0_rp + exp(-0.1378_rp * (40.14_rp + volts_exm(1,ipoin,1)))
           beta1 = 1.0_rp / beta1
           beta2 = 0.02424_rp * exp(-0.01052_rp * volts_exm(1,ipoin,1))
           betax = beta1 * beta2           ! Value of beta_j
    
         end if 
    
         xitaux  = betax + alphx           ! Value of 1/tau_j
      
         vaux0 = vauxi_exm(3,ipoin,3)      ! Value of j in previous time step 
    
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
    
         vauxi_exm(3,ipoin,1) = vaux3      ! Value of variable j
    
        
    ! Variable of activation d (related with I_CaL current)
    ! nauxi_exm = 4
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
    
         vinf = 1.0_rp + exp((-8.0_rp - volts_exm(1,ipoin,1)) / 7.5_rp)
         vinf = 1.0_rp / vinf              ! Value of d_infinite
    
         alphx = 1.0_rp + exp((-35.0_rp - volts_exm(1,ipoin,1)) / 13.0_rp)
         alphx = (1.4_rp / alphx) + 0.25_rp  ! Value of alpha_d
    
         betax = 1.0_rp + exp((5.0_rp + volts_exm(1,ipoin,1)) / 5.0_rp)
         betax = 1.4_rp / betax            ! Value of beta_d
    
         gammax = 1.0_rp + exp((50.0_rp - volts_exm(1,ipoin,1)) / 20.0_rp)
         gammax = 1.0_rp / gammax          ! Value of gamma_d
    
         taux = (alphx * betax) + gammax   ! Value of tau_d
         xitaux = 1.0_rp / taux            ! Value of 1/tau_d
         
         vaux0 = vauxi_exm(4,ipoin,3)      ! Value of d in previous time step 
    
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(4,ipoin,1) = vaux3      ! Value of variable d
    
    ! Variable of activation f (related with I_CaL current)
    ! nauxi_exm = 5
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
    
         vinf = 1.0_rp + exp((20.0_rp + volts_exm(1,ipoin,1)) / 7.0_rp)
         vinf = 1.0_rp / vinf              ! Value of f_infinite
    ! tau_f = 1102.5*(exp(( - (pow((V+27),2))/225)))+200/(1+(exp(((13 - V)/10))))+180/(1+(exp(((V+30)/10))))+20
         tau1 = 1102.5_rp * exp(-((27.0_rp + volts_exm(1,ipoin,1)) * (27.0_rp + volts_exm(1,ipoin,1))) / 225.0_rp)
         tau2 = 1.0_rp + exp((13.0_rp - volts_exm(1,ipoin,1)) / 10.0_rp)
         tau2 = 200.0_rp / tau2
         tau3 = 1.0_rp + exp((30.0_rp + volts_exm(1,ipoin,1)) / 10.0_rp)
         tau3 = 180.0_rp / tau3
         taux = tau1 + tau2 + tau3 + 20.0_rp      ! Value of tau_f
         xitaux = 1.0_rp / taux            ! Value of 1/tau_f
         
         vaux0 = vauxi_exm(5,ipoin,3)      ! Value of f in previous time step 
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(5,ipoin,1) = vaux3      ! Value of variable f
    
    ! Variable of activation f2 (related with I_CaL current)
    ! nauxi_exm = 12
    ! "A model for ventricular tissue", Ten Tusscher, 2006 paper
    ! modified by JAS
    
         vinf = 1.0_rp + exp((35.0_rp + volts_exm(1,ipoin,1)) / 7.0_rp)
         vinf = (0.67_rp / vinf ) + 0.33_rp            ! Value of f2_infinite
    
         tau1 = 600.0_rp * exp((-(25.0_rp + volts_exm(1,ipoin,1)) * (25.0_rp + volts_exm(1,ipoin,1))) / 170.0_rp)
         tau2 = 1.0_rp + exp((25.0_rp - volts_exm(1,ipoin,1)) / 10.0_rp)
         tau2 = 31.0_rp / tau2
         tau3 = 1.0_rp + exp((30.0_rp + volts_exm(1,ipoin,1)) / 10.0_rp)
         tau3 = 16.0_rp / tau3
         taux = tau1 + tau2 + tau3      ! Value of tau_f2
         xitaux = 1.0_rp / taux         ! Value of 1/tau_f2
         
         vaux0 = vauxi_exm(12,ipoin,3)  ! Value of f2 in previous time step 
    
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(12,ipoin,1) = vaux3      ! Value of variable f2
    
    ! Variable of activation fCass (related to I_CaL current)
    ! nauxi_exm = 6
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
    
         vinf = 1.0_rp + ((vconc(5,ipoin,2) / 0.05_rp)*(vconc(5,ipoin,2) / 0.05_rp))
         vinf = (0.6_rp / vinf ) + 0.4_rp            ! Value of fCass_infinite
    
         tau1 = 1.0_rp + ((vconc(5,ipoin,2) / 0.05_rp)*(vconc(5,ipoin,2) / 0.05_rp))
         tau1 = (80.0_rp / tau1) + 2.0_rp           ! Value of tau_fCass    
         xitaux = 1.0_rp / tau1            ! Value of 1/tau_f
         
         vaux0 = vauxi_exm(6,ipoin,3)      ! Value of f in previous time step 
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(6,ipoin,1) = vaux3      ! Value of variable fCass
    
    ! Variable of activation r (related with I_to current)
    ! nauxi_exm = 7
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
    
         vinf = 1.0_rp + exp((20.0_rp - volts_exm(1,ipoin,1)) / 6.0_rp)
         vinf = 1.0_rp / vinf              ! Value of r_infinite
     
         tau1 = 9.5_rp * exp(-((40.0_rp + volts_exm(1,ipoin,1)) * (40.0_rp + volts_exm(1,ipoin,1))) / 1800.0_rp)
         taux = tau1 + 0.8_rp              ! Value of tau_r
         xitaux = 1.0_rp / taux            ! Value of 1/tau_r
      
         vaux0 = vauxi_exm(7,ipoin,3)      ! Value of r in previous time step 
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(7,ipoin,1) = vaux3      ! Value of variable r
    
    ! Variable of activation s (related with I_to current)
    ! nauxi_exm = 8
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
    
    !           ituss_exm==  !(3=epicardial, 1=endocardial, 2=M cells)
        if(ituss_exm == 3)  then
    
           vinf = 1.0_rp + exp((20.0_rp + volts_exm(1,ipoin,1)) / 5.0_rp)
           vinf = 1.0_rp / vinf              ! Value of s_infinite
           
           tau1 = 85.0_rp * exp(-((45.0_rp + volts_exm(1,ipoin,1)) * (45.0_rp + volts_exm(1,ipoin,1))) / 320.0_rp)
           tau2 = 1.0_rp + exp((-20.0_rp + volts_exm(1,ipoin,1)) / 5.0_rp)
           tau2 = 5.0_rp / tau2
           taux = (tau1 + tau2 + 3.0_rp)*1.6       ! Value of tau_s
           xitaux = 1.0_rp / taux            ! Value of 1/tau_s
    
         else if (ituss_exm == 1) then
    
           vinf = 1.0_rp + exp((28.0_rp + volts_exm(1,ipoin,1)) / 5.0_rp)
           vinf = 1.0_rp / vinf              ! Value of s_infinite
           
           tau1 = 1000.0_rp * exp(-((67.0_rp + volts_exm(1,ipoin,1)) * (67.0_rp + volts_exm(1,ipoin,1))) / 1000.0_rp)
           taux = (tau1 + 8.0_rp)*1.6_rp     ! Value of tau_s
           xitaux = 1.0_rp / taux            ! Value of 1/tau_s 
    
        else if (ituss_exm == 2) then
    
           vinf = 1.0_rp + exp((20.0_rp + volts_exm(1,ipoin,1)) / 5.0_rp)
           vinf = 1.0_rp / vinf              ! Value of s_infinite
           
           tau1 = 85.0_rp * exp(-((45.0_rp + volts_exm(1,ipoin,1)) * (45.0_rp + volts_exm(1,ipoin,1))) / 320.0_rp)
           tau2 = 1.0_rp + exp((-20.0_rp + volts_exm(1,ipoin,1)) / 5.0_rp)
           tau2 = 5.0_rp / tau2
           taux = (tau1 + tau2 + 3.0_rp)*1.6_rp       ! Value of tau_s
           xitaux = 1.0_rp / taux            ! Value of 1/tau_s 
    
        end if
    
         vaux0 = vauxi_exm(8,ipoin,3)      ! Value of s in previous time step 
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(8,ipoin,1) = vaux3      ! Value of variable s
    
    ! Variable of activation xs (related with I_Ks current)
    ! nauxi_exm = 9
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
    
         vinf = 1.0_rp + exp((-5.0_rp - volts_exm(1,ipoin,1)) / 14.0_rp)
         vinf = 1.0_rp / vinf              ! Value of xs_infinite
    
         alphx = 1.0_rp + exp((5.0_rp - volts_exm(1,ipoin,1)) / 6.0_rp)
         alphx = 1400.0_rp / sqrt(alphx)   ! Value of alpha_xs
    
         betax = 1.0_rp + exp((-35.0_rp + volts_exm(1,ipoin,1)) / 15.0_rp)
         betax = 1.0_rp / betax            ! Value of beta_xs
    
         taux = (alphx * betax) + 80.0_rp             ! Value of tau_xs
         xitaux = 1.0_rp / taux            ! Value of 1/tau_xs
     
         vaux0 = vauxi_exm(9,ipoin,3)      ! Value of xs in previous time step 
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(9,ipoin,1) = vaux3      ! Value of variable xs
    
    ! Variable of activation xr1 (related with I_Kr1 current)
    ! nauxi_exm = 10
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
    
         vinf = 1.0_rp + exp((-26.0_rp - volts_exm(1,ipoin,1)) / 7.0_rp)
         vinf = 1.0_rp / vinf              ! Value of xr1_infinite
    
         alphx = 1.0_rp + exp((-45.0_rp - volts_exm(1,ipoin,1)) / 10.0_rp)
         alphx = 450.0_rp / alphx          ! Value of alpha_xr1
    
         betax = 1.0_rp + exp((30.0_rp + volts_exm(1,ipoin,1)) / 11.5_rp)
         betax = 6.0_rp / betax            ! Value of beta_xr1
    
         taux = alphx * betax              ! Value of tau_xr1
         xitaux = 1.0_rp / taux            ! Value of 1/tau_xr1
         
         vaux0 = vauxi_exm(10,ipoin,3)     ! Value of xr1 in previous time step 
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(10,ipoin,1) = vaux3     ! Value of variable xr1
    
    ! Variable of activation xr2 (related with I_Kr2 current)
    ! nauxi_exm = 11
    ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
    
         vinf = 1.0_rp + exp((88.0_rp + volts_exm(1,ipoin,1)) / 24.0_rp)
         vinf = 1.0_rp / vinf              ! Value of xr2_infinite
    
         alphx = 1.0_rp + exp((-60.0_rp - volts_exm(1,ipoin,1)) / 20.0_rp)
         alphx = 3.0_rp / alphx            ! Value of alpha_xr2
    
         betax = 1.0_rp + exp((-60.0_rp + volts_exm(1,ipoin,1)) / 20.0_rp)
         betax = 1.12_rp / betax           ! Value of beta_xr2
    
         taux = alphx * betax              ! Value of tau_xr2
         xitaux = 1.0_rp / taux            ! Value of 1/tau_xr2
         
         vaux0 = vauxi_exm(11,ipoin,3)     ! Value of xr2 in previous time step 
    
         ! Scheme for numerical integration
         if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
    
            vaux1 = (vinf - vaux0) * xitaux
            vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux
            vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux
            vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux
            vaux5 = vaux0 + (dtimeEP/ 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4) 
            vaux3 = vaux5
    
         else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
    
            vaux1 = dtimeEP * xitaux
            vaux2 = 1.0_rp + thnew * vaux1
            vaux2 = 1.0_rp / vaux2
            vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
            vaux3 = vaux3 * vaux2       
    
         else if (kfl_odets_exm == 3) then      ! Rush - Larsen
    
            vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
    
         end if
    
         vauxi_exm(11,ipoin,1) = vaux3     ! Value of variable xr2
    end do    
  end if
end subroutine exm_auxhet
