subroutine exm_inicia
  !-----------------------------------------------------------------------
  !****f* Exmedi/exm_inicia
  ! NAME 
  !    exm_inicia
  ! DESCRIPTION
  !    This routine sets up the initial condition for the module
  ! USED BY
  !    exm_begste
  !***
  !-----------------------------------------------------------------------
  use      def_master
  use      def_elmtyp
  use      def_domain

  use      def_exmedi

  implicit none
  integer(ip) :: ielem,ipoin,icomp
  integer(ip) :: inode,pnode,pmate,pelty,kmodel_ipoin
  real(rp)   ::  bufcyto, kbufcyto, bufsr, kbufsr
  real(rp)   ::  vaux1, vaux2, vaux3
  real(rp)    :: enapot, ecapot, ekpot, ekspot 
  real(rp)    :: groupcon, xgroupcon
  real(rp)   ::  celltem, rgascon, faracon, xtrkcon, xtrnacon, xtrcacon
  real(rp)   ::  k1gmax, ksnaperm
  real(rp)   ::  nacamax, gamanaca, kmcanaca, kmnanaca, satunaca, alfanaca
  real(rp)   ::  nakmax, kmknak, kmnanak
  real(rp)   ::  pcagmax, kpcapca
  real(rp)   ::  pkgmax
  real(rp)   ::  bnagmax
  real(rp)   ::  bcagmax
  real(rp)   ::  upvmax, upkcon
  real(rp)   ::  relacon, relbcon, relccon
  real(rp)   ::  leakvmax


  if(INOTMASTER) then


     if(kfl_timei_exm==1) then 
        !
        ! Load initial conditions for the potentials
        !
        do icomp= 1,ncomp_exm
           do ipoin=1,npoin

              if (.not.(exm_zonenode(ipoin))) cycle

              kmodel_ipoin= 0
              pmate = nomat_exm(ipoin)
              kmodel_ipoin= kfl_spmod_exm(pmate)

              elmag(1,ipoin,icomp) = 0.0_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
              elmag(2,ipoin,icomp) = 0.0_rp     ! EXTRA (BI-DOMAIN) POTENTIAL
              refhn_exm(ipoin,icomp)   = 0.0_rp ! Recuperation potential (only) for FHN.

              if (kmodel_ipoin == 1) then          ! Ten Tusscher !!!!!!!
                 elmag(1,ipoin,icomp) = -86.2_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
                 elmag(2,ipoin,icomp) = -86.2_rp     ! EXTRA (BI-DOMAIN) POTENTIAL
              end if
              if (kmodel_ipoin == 4) then          ! Ten Tusscher !heterogeneous !!!!!!
                 elmag(1,ipoin,icomp) = -0.0002875322500607_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
                 elmag(2,ipoin,icomp) = -0.0002875322500607_rp     ! EXTRA (BI-DOMAIN) POTENTIAL
                 volts_exm(1,ipoin,icomp) = -86.2_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
                 volts_exm(2,ipoin,icomp) = -86.2_rp     ! EXTRA (BI-DOMAIN) POTENTIAL
              end if
              if (kmodel_ipoin == 5) then          ! O'Hara - Rudy !!!!!!
                 elmag(1,ipoin,icomp) = -87.0_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
                 elmag(2,ipoin,icomp) = -87.0_rp     ! EXTRA (BI-DOMAIN) POTENTIAL
              end if
              if (kmodel_ipoin == 6) then          ! Ten Tusscher + INaL !!!!!!
                 elmag(1,ipoin,icomp) = -86.2_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
                 elmag(2,ipoin,icomp) = -86.2_rp     ! EXTRA (BI-DOMAIN) POTENTIAL
              end if

              !
              ! check if the pmate element has an initial voltage set, which supersedes the default values

              if (kfl_voini_exm(pmate) == 1) then
                 elmag(1,ipoin,icomp) = voini_exm(pmate)    ! initial voltage
                 elmag(2,ipoin,icomp) = voini_exm(pmate)    ! initial voltage
              end if

              if (kmodel_ipoin == 12) then
                 call runend('EXM_INICIA: FENTON-KARMA MODEL NOT PROGRAMMED')
              end if
           end do
        end do


        return

        !
        !  ESTO QUE SIGUE HAY QUE QUITARLO!!
        !


        if (kfl_gemod_exm == 1) then

           do ielem=1,nelem
              pelty = ltype(ielem)

              if ( pelty > 0 .and. lelch(ielem) /= ELCNT ) then
                 pnode = nnode(pelty)
                 pmate = 1                       
                 if( nmate > 1 ) then
                    pmate = lmate_exm(ielem)
                 end if

                 do inode= 1,pnode
                    ipoin= lnods(inode,ielem)
                    !ituss = pmate

                    rgascon = cupar_exm(1,pmate)%value(1)      ! R [mJ/(K*mol)] --> ideal gas constant
                    celltem = cupar_exm(1,pmate)%value(2)      ! T [K] --> cell system temperature 
                    faracon = cupar_exm(1,pmate)%value(3)      ! F [C/mol] --> faraday constant
                    xtrkcon = cupar_exm(1,pmate)%value(4)      ! K_o [mM] --> extracellular concentration of potassium (K+)
                    xtrnacon = cupar_exm(1,pmate)%value(5)     ! Na_o [mM] --> extracellular concentration of sodium (Na+)
                    xtrcacon = cupar_exm(1,pmate)%value(6)     ! Ca_o [mM] --> extracellular concentration of calcium (Ca++)

                    ksnaperm = cupar_exm(5,pmate)%value(4)     ! p_KNa [adim] --> relative I_Ks current permeability to Na+

                    k1gmax = cupar_exm(7,pmate)%value(1)       ! G_K1 [nS/pF] --> maximal conductance of I_K1 current

                    nacamax = cupar_exm(8,pmate)%value(1)      ! k_NaCa [pA/pF] --> maximal I_NaCa current
                    gamanaca = cupar_exm(8,pmate)%value(2)     ! gama [adim] --> voltage dependent parameter of I_NaCa current
                    kmcanaca = cupar_exm(8,pmate)%value(3)     ! K_mCa [mM] --> Cai half-saturation constant for I_NaCa current
                    kmnanaca = cupar_exm(8,pmate)%value(4)     ! K_mNai [mM] --> Nai half-saturation constant for I_NaCa current
                    satunaca =  cupar_exm(8,pmate)%value(5)    ! k_sat [adim] --> saturation factor I_NaCa current
                    alfanaca = cupar_exm(8,pmate)%value(6)     ! alfa [adim] --> factor enhancing outward nature of I_NaCa current

                    nakmax = cupar_exm(9,pmate)%value(1)       ! P_NaK [pA/pF] --> maximal I_NaK current
                    kmknak = cupar_exm(9,pmate)%value(2)       ! K_mK [mM] --> K_o half-saturation constant of I_NaK current
                    kmnanak = cupar_exm(9,pmate)%value(3)      ! K_mNa [mM] --> Nai half-saturation constant of I_NaK current

                    pcagmax = cupar_exm(10,pmate)%value(1)     ! G_pCa [pA/pF] --> maximal conductance of I_pCa current
                    kpcapca = cupar_exm(10,pmate)%value(2)     ! K_pCa [mM] --> Cai half-saturation constant of I_pCa current

                    pkgmax = cupar_exm(11,pmate)%value(1)      ! G_pK [nS/pF] --> maximal conductance of I_pK current

                    bnagmax = cupar_exm(12,pmate)%value(1)     ! G_bNa [nS/pF] --> maximal conductance of I_bNa current

                    bcagmax = cupar_exm(13,pmate)%value(1)     ! G_bCa [nS/pF] --> maximal conductance of I_bCa current

                    upvmax = cupar_exm(14,pmate)%value(1)      ! V_maxup [mM/ms] --> maximal I_up current
                    upkcon = cupar_exm(14,pmate)%value(2)      ! K_up [mM] --> half-saturation constant of I_up current

                    relacon = cupar_exm(15,pmate)%value(1)     ! a_rel [mM/ms] --> maximal Ca_SR dependent I_rel current
                    relbcon = cupar_exm(15,pmate)%value(2)     ! b_rel [mM] --> Ca_SR half-saturation constant of I_rel current
                    relccon = cupar_exm(15,pmate)%value(3)     ! c_rel [mM/ms] --> maximal Ca_SR independent I_rel current

                    leakvmax = cupar_exm(16,pmate)%value(1)    ! V_leak [1/ms] --> maximal I_leak current

                    groupcon = rgascon * celltem / faracon          ! Group of constants: R * T / F  [mV] 
                    xgroupcon = 1.0_rp / groupcon

                    bufcyto = cupar_exm(17,pmate)%value(1)     ! Buf_c [mM] --> total cytoplasmic buffer concentration
                    kbufcyto = cupar_exm(17,pmate)%value(2)    ! K_Bufc [mM] --> Cai half-saturation constant for cytoplasmic buffer

                    bufsr = cupar_exm(17,pmate)%value(3)       ! Buf_sr [mM] --> total sarcoplasmic buffer concentration
                    kbufsr = cupar_exm(17,pmate)%value(4)      ! K_Bufsr [mM] --> Ca_SR half-saturation constant for sarcoplasmic buffer

                    vauxi_exm(1,ipoin,1:3) = 0.0_rp           !Variable m
                    vauxi_exm(2,ipoin,1:3) = 0.75_rp        !Variables h and j
                    vauxi_exm(4,ipoin,1:3) = 0.0_rp           !Variable d
                    vauxi_exm(5,ipoin,1:3) = 1.0_rp         !Variable f y fCa
                    vauxi_exm(7,ipoin,1:3) = 0.0_rp           !Variable r
                    vauxi_exm(8,ipoin,1:3) = 1.0_rp           !Variable s
                    vauxi_exm(9,ipoin,1:3) = 0.0_rp        !Variable xs y xr1
                    vauxi_exm(11,ipoin,1:3) = 1.0_rp       !Variable xr2 y g    

                    ! Load initial conditions for cellular ionic concentrations
                    !

                    vconc(3,ipoin,1:3) = xmopa_exm(10,pmate)        !Variable naitot
                    vconc(4,ipoin,1:3) = xmopa_exm(11,pmate)        !Variable Kitot
                    vconc(5,ipoin,1:3) = xmopa_exm(8,pmate)         !Variable caifre
                    vconc(7,ipoin,1:3) = xmopa_exm(9,pmate)         !Variable casrfr

                    vaux1 = vconc(5,ipoin,1) + kbufcyto
                    vaux1 = 1.0_rp / vaux1
                    vaux1 = vaux1 * vconc(5,ipoin,1) * bufcyto    !Variable caibuf
                    vconc(6,ipoin,1:3) = vaux1

                    vaux1 = vconc(7,ipoin,1)  + kbufsr
                    vaux1 = 1.0_rp / vaux1
                    vaux1 = vaux1 * vconc(7,ipoin,1) * bufsr
                    vconc(8,ipoin,1:3) = vaux1                    !Variable casrbu

                    vconc(1,ipoin,1) = vconc(5,ipoin,1) + vconc(6,ipoin,1)      !Variable caitot
                    vconc(1,ipoin,3) = vconc(5,ipoin,3) + vconc(6,ipoin,3) 
                    vconc(2,ipoin,1) = vconc(7,ipoin,1) + vconc(8,ipoin,1)      !Variable casrto
                    vconc(2,ipoin,3) = vconc(7,ipoin,3) + vconc(8,ipoin,3)

                    vicel_exm(1,ipoin,1:3) = 0.0_rp         !Variable xina
                    vicel_exm(2,ipoin,1:3) = 0.0_rp         !Variable xical
                    vicel_exm(3,ipoin,1:3) = 0.0_rp         !Variable xito
                    vicel_exm(4,ipoin,1:3) = 0.0_rp         !Variable xiks
                    vicel_exm(5,ipoin,1:3) = 0.0_rp         !Variable xikr
                    vicel_exm(15,ipoin,1:3) = 0.0_rp        !Variable xirel
                    vicel_exm(16,ipoin,1:3) = 0.0_rp        !Variable xistim
                    vicel_exm(17,ipoin,1:3) = 0.0_rp        !Variable xiax

                    !
                    ! Load initial conditions for ionic currents
                    !

                    enapot = groupcon * log (xtrnacon / vconc(3,ipoin,1))    ! Reversal potential of Na [mV]
                    ecapot = groupcon / 2.0_rp * log (xtrcacon / vconc(5,ipoin,1))    ! Reversal potential of Ca [mV]
                    ekpot = groupcon * log (xtrkcon / vconc(4,ipoin,1))    ! Reversal potential of K [mV]
                    vaux1 = xtrkcon + ksnaperm * xtrnacon
                    vaux2 = vconc(4,ipoin,1) + ksnaperm * vconc(3,ipoin,1)
                    ekspot = groupcon * log ( vaux1 / vaux2 )    ! Reversal potential of Ks [mV]

                    ! Variable x1k1
                    vaux1 = 3.0_rp * exp(0.0002_rp * (100.0_rp + elmag(1,ipoin,1) - ekpot))
                    vaux1 = vaux1 + exp(0.1_rp * (-10.0_rp + elmag(1,ipoin,1) - ekpot))
                    vaux2 = 1.0_rp + exp(-0.5_rp * (elmag(1,ipoin,1) - ekpot))
                    vaux2 = 1.0_rp / vaux2
                    vaux1 = vaux1 * vaux2                  ! Value of beta_K1
                    vaux2 = 1.0_rp + exp(0.06_rp * (elmag(1,ipoin,1) - ekpot - 200.0_rp))
                    vaux2 = 0.1_rp / vaux2                 ! Value of alpha_K1
                    vaux2 = vaux2 / (vaux1 + vaux2)        ! Value of K1_inf
                    vaux1 = sqrt(xtrkcon / 5.4_rp) * vaux2 * (elmag(1,ipoin,1) - ekpot)
                    vicel_exm(6,ipoin,1:3) = k1gmax * vaux1         !Variable xik1

                    ! Variable xinaca
                    vaux1 = exp((gamanaca-1.0_rp) * elmag(1,ipoin,1) * xgroupcon)
                    vaux2 = kmnanaca * kmnanaca * kmnanaca + xtrnacon * xtrnacon * xtrnacon
                    vaux2 = vaux2 * (kmcanaca + xtrcacon)
                    vaux2 = vaux2 * (1.0_rp + satunaca * vaux1)     ! current denominator 
                    vaux2 = 1.0_rp / vaux2                          ! inverse of current denominator
                    vaux3 = -vaux1 * xtrnacon * xtrnacon * xtrnacon * vconc(5,ipoin,1) * alfanaca         
                    vaux1 = exp(gamanaca * elmag(1,ipoin,1) * xgroupcon)
                    vaux3 = vaux3 + vaux1 * vconc(3,ipoin,1) * vconc(3,ipoin,1) * vconc(3,ipoin,1) * xtrcacon   ! current numerator
                    vicel_exm(7,ipoin,1:3) = nacamax * vaux3 * vaux2         !Variable xinaca

                    ! Variable xinak
                    vaux1 = (xtrkcon + kmknak) * (vconc(3,ipoin,1) + kmnanak)
                    vaux2 = 1.0_rp + 0.1245_rp * exp(-0.1_rp * elmag(1,ipoin,1) * xgroupcon)
                    vaux2 = vaux2 + 0.0353_rp * exp(-elmag(1,ipoin,1) * xgroupcon)
                    vaux1 = vaux1 * vaux2             ! current denominator 
                    vaux1 = 1.0_rp / vaux1            ! inverse of current denominator 
                    vaux2 =  xtrkcon * vconc(3,ipoin,1)     ! current numerator
                    vicel_exm(8,ipoin,1:3) = nakmax * vaux1 * vaux2       ! Variable xinak

                    ! Variable xipca
                    vaux1 = kpcapca + vconc(5,ipoin,1)
                    vaux1 = 1.0_rp / vaux1             ! inverse of current denominator
                    vicel_exm(9,ipoin,1:3) = pcagmax * vaux1 * vconc(5,ipoin,1)       ! Variable xipca

                    ! Variable xipk
                    vaux1 = 1.0_rp + exp((25.0_rp - elmag(1,ipoin,1)) / 5.98_rp)
                    vaux1 = 1.0_rp / vaux1             ! inverse of current denominator
                    vicel_exm(10,ipoin,1:3) = pkgmax * vaux1 * (elmag(1,ipoin,1) - ekpot)       ! Variable xipk

                    !Variable xibna
                    vicel_exm(11,ipoin,1:3) = bnagmax * (elmag(1,ipoin,1) - enapot)       !Variable xibna

                    !Variable xibca
                    vicel_exm(12,ipoin,1:3) = bcagmax * (elmag(1,ipoin,1) - ecapot)        !Variable xibca

                    !Variable xileak
                    vicel_exm(13,ipoin,1:3) = leakvmax * (vconc(7,ipoin,1) - vconc(5,ipoin,1))      !Variable xileak

                    !Variable xiup
                    vaux1 = vconc(5,ipoin,1) * vconc(5,ipoin,1)
                    vaux1 = 1.0_rp / vaux1
                    vaux2 = 1.0_rp + upkcon * upkcon * vaux1
                    vaux2 = 1.0_rp / vaux2            ! inverse of current denominator
                    vicel_exm(14,ipoin,1:3) = upvmax * vaux2       !Variable xiup

                 end do

              end if
           end do

           !           end if
        end if


     end if

  end if


end subroutine exm_inicia
