subroutine Exmedi(order)
!-----------------------------------------------------------------------
!****f* Estinc/Exmedi
! NAME 
!    Exmedi
! DESCRIPTION
!    This is the main subroutine for the Excitable Media module.
!    A Bidomain Model is used for cardiac electrical propagation.
!    Two different kinds of models are implemented for ionic currents:
!      - No subcell models (approximate models): FitzHugh-Nagumo (FHN)
!      - Subcell models: TenTusscher (TT), Luo-Rudy (LR), ...
!
! USES
!    exm_turnon
!    exm_begste
!    exm_doiter
!    exm_gencon
!    exm_endste
!    exm_turnof
! USED BY
!    Turnon
!    Begste
!    Doiter
!    Gencon
!    Endste
!    Turnof
!***
!
!    The task done corresponds to the order given by the master.
!-----------------------------------------------------------------------

  use      def_parame
  use      def_domain
  use      def_master
  use      def_solver
  use      def_exmedi

  implicit none
  integer(ip) :: order
  
  select case (order)
     
  case(ITASK_TURNON)
     call exm_turnon
  case(ITASK_INIUNK)
     call exm_iniunk
  case(ITASK_OUTPUT)
     call exm_output
  case(ITASK_TIMSTE) 
     call exm_timste
  case(ITASK_BEGSTE) 
     call exm_begste
  case(ITASK_DOITER)
     call exm_doiter
  case(ITASK_CONCOU)
     call exm_concou
  case(ITASK_CONBLK)
     return
!!     call exm_conblk   --> to be done when needed
  case(ITASK_ENDSTE)
     call exm_endste
  case(ITASK_TURNOF)
     call exm_turnof
     
  end select
  
end subroutine Exmedi
      
