subroutine exm_ionicu(ipoin,xgate,xlian,xpion,xpfhn,xpfen,xpten)

!-----------------------------------------------------------------------
!
! This routine computes the ionic current contribution
!
!       OLD SUBROUTINE. Shouldn't be used anymore. Was replaced by
!                       exm_ionicunew. This file will disapear in future
!                        versions.
!
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solver

  use      def_exmedi

  implicit none
  integer(ip) :: itask,iicel,imate,ipoin,kmodel_ipoin
  real(rp)    :: &
       xnoli,xdeno,xnumi,xreln,xrelo,xpion,xlian,xpio1,xpio2,xpio3,argth,&
       factk,tausi,tauro,tauso,phics,gephi
  real(rp)    :: &
       xmeps,xmalp,xmgam,taufi,xfact,xhea1,xhea2,taude,xgate(ngate_exm),vicel(nicel_exm), &
       xauxi(2),xpfhn,xpfen,xpten

  xpfhn = 1.0_rp
  xpfen = 0.0_rp
  xpten = 0.0_rp


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!!IMPORTANT NOTE: THE FOLLOWING PIECE OF CODE **IS** WRONG BECAUSE THE ONLY CALL TO
!!!!                IONICU WITH IPOINT<0 IS IN IAPELM AND IT IS CALLED WITH "-1"
!!!!                SO THERE IS NO SWEEEPING IN NODES.
!!!!
!!!!                AND, NOW THAT I AM NOTICING, XPTEN IS SENT TO AN XDUMMY VARIABLE
!!!!                IN IAPELM.
!!!!
  if ( ipoin < 0 .and.  kfl_spmod_exm(nomat_exm(-ipoin))==1) then  !Ten Tusscher
       !xpten = apval_exm(1)
        xpten = 0.0_rp
        do iicel = 1,12  
            xpten = xpten + vicel_exm(iicel,ipoin,1) 
        end do
     
        xpten = - xpten 

        return
  end if
!!!!
!!!!END OF IMPORTANT NOTE
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  if (ipoin > 0) kmodel_ipoin= kfl_spmod_exm(nomat_exm(ipoin))

    if (kmodel_ipoin == 0) then 
        xmeps = 0.0_rp
        xmalp = 0.0_rp
        xmgam = 0.0_rp
        taufi = 0.0_rp

    else if (kmodel_ipoin == 11) then 
        xmeps = xmopa_exm( 1,1)           ! epsilon for the rcp 
        xmalp = xmopa_exm( 3,1)           ! alpha for the Iion
        xmgam = xmopa_exm( 4,1)           ! gamma for the rcp
        taufi = xmopa_exm( 5,1)           ! C for the Iion 
       
  else if (kmodel_ipoin == 12) then                 !fenton
     xmalp = xmopa_exm( 3,1)           ! phi c
     tauro = xmopa_exm( 9,1)               
     tausi = xmopa_exm( 10,1) 
     tauso = xmopa_exm( 11,1)
     phics = xmopa_exm( 12,1)
     gephi = xmopa_exm( 13,1) 
     factk = xmopa_exm( 16,1)

  end if

  if (kmodel_ipoin == 0) then                       !no model
     xpion = 0.0_rp
  else if (kmodel_ipoin == 11) then                 !fitzhugh nagumo
     xpion =  - taufi * xlian * (xlian - 1.0_rp) * (xlian - xmalp)
     xpfhn = 1.0_rp + taufi * (xlian - 1.0_rp) * (xlian - xmalp) * dtime / xmccm_exm
  else if (kmodel_ipoin == 12) then                 !fenton
     xhea1 =   heavis(xlian,xmalp)
     xhea2 =   heavis(xmalp,xlian)
     taude =    xmccm_exm / gephi

     if (ipoin > 0) then
        !xgate(1) = vgate_exm(1,ipoin,1)    ! Line commented because vgate does not exist anymore
        !xgate(2) = vgate_exm(2,ipoin,1)    ! the same for this guy.
     end if

     xpio1 =   xgate(1) / taude * xhea1 * (1.0_rp - xlian) * (xlian - xmalp)
     xpio2 = - xlian / tauso * xhea2 - xhea1 / tauro
     argth =   factk * (xlian - phics)
     if (argth > 4) then
        xpio3 = xgate(2) / tausi                  
     else if (argth < - 4) then
        xpio3 = 0.0_rp                 
     else
        xauxi(1) =  exp( argth)
        xauxi(2) =  exp(-argth)
        xpio3    =  xgate(2) *  &                      
             (1.0_rp + (xauxi(1) - xauxi(2)) / (xauxi(1) + xauxi(2))) &
             / 2.0_rp / tausi 
     end if
     xpion = xpio1 + xpio2 + xpio3
     
     xpfen = xpion

  end if
  
end subroutine exm_ionicu
