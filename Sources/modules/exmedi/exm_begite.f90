subroutine exm_begite
!-----------------------------------------------------------------------
!****f* Exmedi/exm_begite
! NAME 
!    exm_begite
! DESCRIPTION
!    This routine starts the internal iteration 
! USES
!    exm_inisol
!    exm_updunk
! USED BY
!    exm_doiter
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain

  use      def_exmedi

  implicit none
  integer (ip) :: imate,kmodel
!
! Initializations
!
  kfl_goite_exm = 1
  itinn(modul)  = 0
  if(miinn_exm==0) kfl_goite_exm=0
!  if(itcou==1) call exm_tistep()
  call livinf(15_ip,' ',modul)
  !
  ! Obtain the initial guess for inner iterations: y(2) <- y(1)
  !

  !
  ! check if any TT-like model is present
  !
  kmodel= 0
  do imate= 1,nmate
     kmodel = max(kfl_spmod_exm(imate),0)
  end do
  
  if (kfl_gemod_exm == 0) then
     call exm_updunk(two)    
!    Compute currents when sub-cell models are used
  else if (kfl_gemod_exm == 1) then
     if (kmodel == 1) then  !TenTuscher 2004
        call exm_ceicur  !!! compute cellular ionic currents  
     else if (kmodel == 4) then  !TenTuscher 2006 Heterogeneo
        call exm_ceihet  !!! compute CURRENTS  TT Het
        call exm_updunk(nine)  !!update the currents for the next step        
     else if (kmodel == 5) then  !OHara-Rudy 2011
        call exm_ceiohr  !!! compute CURRENTS O'Hara-Rudy
     else if (kmodel == 6) then  !TenTuscher 2006 Heterogeneo
        call exm_ittina  !!! compute CURRENTS TT Het
        call exm_updunk(nine)  !!update the currents for the next step   
     end if

  end if
  
end subroutine exm_begite
