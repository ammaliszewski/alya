!-----------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_comcnd.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Compute the CONDUCTIVITY (FOR FHN) OR DIFFUSION (FOR CELL MODEL) tensor
!> @details Compute the CONDUCTIVITY (FOR FHN) OR DIFFUSION (FOR CELL MODEL) tensor
!> @} 
!-----------------------------------------------------------------------
subroutine exm_comcnd(ielem,imate,cndin,noion)
  use      def_master
  use      def_domain
  use      def_exmedi
  implicit none

  real(rp)    :: &
       cndin(ndime,ndime,mgaus),xshap,xceco(mgaus),&
       auxic(3,3),cndin_norm
  integer(ip) :: ielem,jelem,idime,jdime,kdime,pdime,inode,igaus,ipoin,icond,&
       pelty,pnode,pgaus,imate, itest,noion

  itest= 0
  if (kfl_comdi_exm == 0 .or. ielem < 0) itest = 1

  if (itest == 1) then
     
     cndin= 0.0_rp
     cndin(1,1,1)= gcond_exm(1,1,imate)
     cndin(2,2,1)= gcond_exm(1,2,imate)
     cndin(ndime,ndime,1)= gcond_exm(1,2,imate)
     

  else 

     ! with fibers

     jelem= ielem
     !!     if (ielem < 0) jelem= -ielem

     pelty = ltype(jelem)
     pnode = nnode(pelty)
     pgaus = ngaus(pelty)

     !!     if (ielem < 0) pgaus=1

     ! Gather operations     
     cndin_norm=0.0_rp
     do igaus=1,pgaus
        cndin(1,1,igaus) = 0.0_rp
        cndin(2,2,igaus) = 0.0_rp
        cndin(1,2,igaus) = 0.0_rp
        cndin(2,1,igaus) = 0.0_rp
        do inode = 1,pnode
           ipoin = lnods(inode,jelem)
           xshap = elmar(pelty)%shape(inode,igaus)
           !!           if (ielem < 0) xshap= 1.0_rp / real(pnode)
           cndin(1,1,igaus) = cndin(1,1,igaus) + xshap * cecon_exm(1,1,ipoin)
           cndin(2,1,igaus) = cndin(2,1,igaus) + xshap * cecon_exm(2,1,ipoin)
           cndin(1,2,igaus) = cndin(1,2,igaus) + xshap * cecon_exm(1,2,ipoin)
           cndin(2,2,igaus) = cndin(2,2,igaus) + xshap * cecon_exm(2,2,ipoin)              
        end do

        cndin_norm= cndin_norm + &
             + cndin(1,1,igaus) * cndin(1,1,igaus) &
             + cndin(2,2,igaus) * cndin(2,2,igaus) &
             + cndin(1,2,igaus) * cndin(1,2,igaus) &
             + cndin(2,1,igaus) * cndin(2,1,igaus)
        
     end do

     if (ndime == 3) then
        cndin_norm = 0.0_rp
        do igaus=1,pgaus
           cndin(3,1,igaus) = 0.0_rp
           cndin(3,2,igaus) = 0.0_rp
           cndin(1,3,igaus) = 0.0_rp
           cndin(2,3,igaus) = 0.0_rp
           cndin(3,3,igaus) = 0.0_rp
           do inode = 1,pnode
              ipoin = lnods(inode,jelem)
              xshap = elmar(pelty)%shape(inode,igaus)
              !!              if (ielem < 0) xshap= 1.0_rp / real(pnode)
              cndin(3,1,igaus) = cndin(3,1,igaus) + xshap * cecon_exm(3,1,ipoin)
              cndin(3,2,igaus) = cndin(3,2,igaus) + xshap * cecon_exm(3,2,ipoin)              
              cndin(1,3,igaus) = cndin(1,3,igaus) + xshap * cecon_exm(1,3,ipoin)
              cndin(2,3,igaus) = cndin(2,3,igaus) + xshap * cecon_exm(2,3,ipoin)
              cndin(3,3,igaus) = cndin(3,3,igaus) + xshap * cecon_exm(3,3,ipoin)              
           end do

           cndin_norm= cndin_norm + &
                + cndin(3,1,igaus) * cndin(3,1,igaus) &
                + cndin(3,2,igaus) * cndin(3,2,igaus) &
                + cndin(1,3,igaus) * cndin(1,3,igaus) &
                + cndin(2,3,igaus) * cndin(2,3,igaus) &
                + cndin(3,3,igaus) * cndin(3,3,igaus) 

        end do
        
     end if

     noion= 0
     if (cndin_norm < 1.0e-10) then
        noion= 1
        cndin(1,1,1)= 0.01*gcond_exm(1,1,1)
        cndin(2,2,1)= 0.01*gcond_exm(1,1,1)
        cndin(ndime,ndime,1)= 0.01*gcond_exm(1,1,1)
     end if

     if (kfl_cemod_exm == 2) then
        call runend('EXM_COMCND: NO BIDOMAIN MODEL WITH FIBERS PROGRAMMED')
     end if

  end if


end subroutine exm_comcnd
