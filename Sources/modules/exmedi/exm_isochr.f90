subroutine exm_isochr(icomp)
  !-----------------------------------------------------------------------
  !****f* exmedi/exm_isochr
  ! NAME 
  !    exm_isochr
  ! DESCRIPTION
  ! USES
  ! USED BY
  !    exm_isochr
  !***
  !-----------------------------------------------------------------------

  use      def_parame
  use      def_master
  use      def_domain
  use      def_postpr
  use      def_exmedi

  implicit none
  integer(ip), intent(in)  :: icomp
  integer(ip)              :: ipoin
  real(rp)                 :: xauxi

  if( INOTMASTER) then
     
     do ipoin= 1,npoin
        
        if (.not.(exm_zonenode(ipoin))) cycle
          xauxi = elmag(1,ipoin,icomp) * ( poref_exm(2) - poref_exm(1) ) + poref_exm(1)        
          if (thiso_exm(1)>0.0_rp) then             
             if (xauxi > thiso_exm(2)) then
                if (kwave_exm(ipoin) == 0) then
                   fisoc(ipoin,1) = cutim

                   ! wave propagation velocity in the fiber direction

                   !  x=X+s        (1)*---*(2)
                   !
                   ! V=(x2 - x1)/(isoch2-isoch1)
                   !
                   ! V= ((s2-X2)-(s1-X1))/(isoch2-isoch1)

                   !fisoc(ipoin,2)=((displ(1,ipoin2,1)- coord(1,ipoin2))-(displ(1,ipoin1,1)-coord(1,ipoin1)))&
                   !                /(fisoc(ipoin2,1)-fisoc(ipoin1,1))
                   !fisoc(ipoin,3)=((displ(2,ipoin2,1)- coord(2,ipoin2))-(displ(2,ipoin1,1)-coord(2,ipoin1)))&
                   !                /(fisoc(ipoin2,1)-fisoc(ipoin1,1))
                   !fisoc(ipoin,4)=((displ(3,ipoin2,1)- coord(3,ipoin2))-(displ(3,ipoin1,1)-coord(3,ipoin1)))&
                   !                /(fisoc(ipoin2,1)-fisoc(ipoin1,1))
                   kwave_exm(ipoin) = 1
                end if
                !               if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
                !                  if (kwave_exm(ipoin)==0) then
                !                     fisoc(ipoin,1)=0.0_rp                 
                !                     kwave_exm(ipoin)=1
                !                  else if (kwave_exm(ipoin)==1) then
                !                     kwave_exm(ipoin)=0
                !                  end if
                !!               end if
                !             else if (xauxi < thiso_exm(2)) then
                !               fisoc(ipoin,1) = -1.0_rp
                !             end if                         
             end if
          end if

       end do

    end if

end subroutine exm_isochr
