subroutine exm_doiter
!-----------------------------------------------------------------------
!****f* Exmedi/exm_doiter
! NAME 
!    exm_doiter
! DESCRIPTION
!   CORREGIR ESTO
!    This routine solves an iteration of the linearized incompressible NS
!    equations.
!    Fractional step is to be implemented
! USES
!    exm_comapp
!    exm_iapupd
!    exm_eapupd
!    exm_rcpupd
! USED BY
!    Exmedi
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_domain
  use      def_master
  use      def_solver

  use      def_exmedi
  
  implicit none
  real(rp)    :: cpu_refe1,cpu_refe2
  
  call cputim(cpu_refe1)
  
!      
! Solution of the staggered scheme:     
!      
!   iap = transmembrane action potential      
!   eap = extracellular action potential
!   rcp = recovery potential   
!      
!
  !    1.0  Update applied currents appfi_exm (source terms) 
  !    using its definition function  and lapno_exm 
  !    1.1  Compute currents when sub-cell models are used
  call exm_begite
  do while(kfl_goite_exm==1)
     !  2. compute iap^n+1 (iap^n  , eap^n, rcp^n)  -->> iteratively, 
     !     to cope with the non-linear FHN ionic current         
     !  3. compute eap^n+1 (iap^n+1, eap^n,      )  -->> by solving 
     !     a poisson equation when bidomain models are active
     !  3.1  Compute concentrations when sub-cell models are used     
     !  4. compute rcp^n+1 (iap^n+1,      , rcp^n)  -->> by an explicit update  
     
     call exm_solite
     call exm_endite(one)  ! elmag(*,*,1) <-- unkno

     call exm_odeite       ! refhn(*,*,1) <-- unkno
     
  end do

     call exm_endite(two)     ! elmag(*,*,2) <-- elmag(*,*,1) , refhn(*,*,2) <-- refhn(*,*,1)

  call cputim(cpu_refe2)
  cpu_exmed(1) = cpu_exmed(1) + cpu_refe2 - cpu_refe1 
  cpu_exmed(2) = cpu_exmed(2) + cpu_solve

end subroutine exm_doiter
