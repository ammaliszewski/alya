!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_ceiohr.f90
!> @author  Jazmin Aguado-Sierra
!> @brief   Currents calculation for Ohara-Rudy 20011 human model
!> @details Calculate every CURRENT at every time step\n
!! The variables are 26:\n
!! vicel_exm(1,ipoin,1)=!ina \n
!! vicel_exm(2,ipoin,1)=!inal \n
!! vicel_exm(3,ipoin,1)=!ito \n
!! vicel_exm(4,ipoin,1)=!ical \n
!! vicel_exm(5,ipoin,1)=!ikr \n
!! vicel_exm(6,ipoin,1)=!iks \n
!! vicel_exm(7,ipoin,1)=!ik1 \n
!! vicel_exm(8,ipoin,1)=!inaca_i \n
!! vicel_exm(9,ipoin,1)=!inaca_ss \n
!! vicel_exm(10,ipoin,1)=!inak \n
!! vicel_exm(11,ipoin,1)=!ikb \n
!! vicel_exm(12,ipoin,1)=!inab \n
!! vicel_exm(13,ipoin,1)=!icab \n
!! vicel_exm(14,ipoin,1)=!ipca \n
!! vicel_exm(15,ipoin,1)=!jdiff \n
!! vicel_exm(16,ipoin,1)=!jdiffna \n
!! vicel_exm(17,ipoin,1)=!jdiffk \n
!! vicel_exm(18,ipoin,1)=!jup \n
!! vicel_exm(19,ipoin,1)=!jleak \n
!! vicel_exm(20,ipoin,1)=!jtr \n
!! vicel_exm(21,ipoin,1)=!jrel \n
!! vicel_exm(22,ipoin,1)=!camka \n
!! vicel_exm(23,ipoin,1)=!istim \n
!! vicel_exm(24,ipoin,1)=!icana \n
!! vicel_exm(25,ipoin,1)=!icak \n
!! vicel_exm(26,ipoin,1)=!camkb \n
!> @} 
!!-----------------------------------------------------------------------
  subroutine exm_ceiohr

  use      def_parame
  use      def_master
  use      def_domain
  use      def_exmedi
  use      def_elmtyp

  implicit none

  ! definition of variables

  integer(ip) :: ipoin, iicel
  real(rp)    :: vaux1, vaux2, vaux3, dtimeEP
  real(rp)    :: vffrt, vfrt, ena, ek, eks, xistim
  real(rp)    ::  a1, a2, a3, a4, b1, b2, b3, b4, k1p, k2p, k3p, k4p, k1m, k2m, k3m, k4m
  real(rp)    :: h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, x1, x2, x3, x4
  real(rp)    ::  k1, k2, k3, k4, k5, k6, k7, k8, e1, e2, e3, e4, k3pp, k4pp, knai, knao0
  real(rp)    :: gna, gnal, gto, gks, gk1, gncx, gkb, gkr, gpca, phical, phicana, phicak, nao
  real(rp)    :: ahf, ahs, hp, hh, kmcamk, finalp, ii, aff, afs, afcaf, afcas, fcap, kmn, k2n, km2n
  real(rp)    :: zca, pca, pcap, pcak, pcanap, pcakp, ksca, kna1, kna2, kna3, kasymm
  real(rp)    :: wna, wca, wnaca, kcaon, kcaoff, qna, qca, hca, hna, kmcaact, allo, zna, jncxna, jncxca
  real(rp)    :: knai0, knao, delta, kki, kko, mgadp, mgatp, kmgatp, ep, khp, knap, kxkur, pp, zk
  real(rp)    :: jnakna, jnakk, pnak, xkb, pnab, ipp, aif, ais
  real(rp)    :: fp, ko, axrf, axrs, xr, rkr, rk1, fjupp, pcab, pcana, pkna
  real(rp)    :: anca, cao, dnca, ff, farad, fca, ficalp, finap,  fjrelp
  real(rp)    :: rgas, temp, jupnp, jupp, fitop, hbig, camko, kmcam

  !extracellular ionic concentrations
  nao = 140.0_rp
  cao = 1.8_rp
  ko = 5.4_rp

  !physical constants
  rgas = 8314.0_rp
  temp = 310.0_rp
  farad = 96485.0_rp 
  camko = 0.05_rp
  kmcam = 0.0015_rp
  !%camk constants
  kmcamk = 0.15_rp
  dtimeEP=dtime * 1000.0_rp
  
  if(INOTMASTER) then
    do ipoin= 1,npoin  
       ituss_exm = int(celty_exm(1,ipoin))
        !%reversal potentials
        vaux1 = rgas*temp/farad
        ena = vaux1 * log(nao/vconc(1,ipoin,1))
        ek = vaux1 * log(ko/vconc(3,ipoin,1))
        pkna = 0.01833_rp
        eks = vaux1 *log((ko + pkna*nao)/(vconc(3,ipoin,1) + pkna * vconc(1,ipoin,1)))
   
        !%convenient shorthand calculations
        vffrt = elmag(1,ipoin,1)*farad*farad / (rgas*temp)
        vfrt = elmag(1,ipoin,1)*farad / (rgas*temp)
   
   !!!! assign Istim
        xistim = appfi_exm(ipoin,1)
        vicel_exm(23,ipoin,1) = xistim
   
   !!! Update CaMKa
        vaux2 = 1.0_rp / (1.0_rp + kmcam/vconc(6,ipoin,1))
        vicel_exm(26,ipoin,1) = vaux2 * camko* (1.0_rp - vconc(11,ipoin,1)) 
        vicel_exm(22,ipoin,1) = vicel_exm(26,ipoin,1) + vconc(11,ipoin,1)  ! camka
   
   !!!!!!   calculate ina
        ahf = 0.99_rp
        ahs = 1.0_rp - ahf
        vaux1 = ahf * vauxi_exm(2,ipoin,1)
        hh = vaux1 + (ahs * vauxi_exm(3,ipoin,1))
   !!!!  hp
        hp = vaux1 + (ahs * vauxi_exm(5,ipoin,1))
   
        gna = 75.0_rp
        finap = 1.0_rp / (1.0_rp + (kmcamk/vicel_exm(22,ipoin,1)))
   
        vaux1 = gna*(elmag(1,ipoin,1)-ena) 
        vaux1 = vaux1 * vauxi_exm(1,ipoin,1) * vauxi_exm(1,ipoin,1) * vauxi_exm(1,ipoin,1)
        vaux2 = (1.0_rp-finap) * hh * vauxi_exm(4,ipoin,1)
        vaux2 = vaux2 + (finap * hp * vauxi_exm(6,ipoin,1)) 
        vicel_exm(1,ipoin,1) = vaux1 * vaux2  !!!!  ina
   
   !!!  inal  
        gnal = 0.0075_rp
        if(ituss_exm == 3) then !!epi
           gnal = gnal * 0.6_rp
        end if
   
        finalp = 1.0_rp + (kmcamk/vicel_exm(22,ipoin,1))
        finalp = 1.0_rp / finalp
   
        vaux1 = gnal*(elmag(1,ipoin,1)-ena)*vauxi_exm(7,ipoin,1)
        vaux2 = (1.0_rp-finalp)*vauxi_exm(8,ipoin,1) + (finalp*vauxi_exm(9,ipoin,1))
        vicel_exm(2,ipoin,1)= vaux1 * vaux2    
   
   !!!!! calculate ito
   !!!! calculate aif, ais and ii
        vaux1 = exp((elmag(1,ipoin,1)-213.6_rp)/151.2_rp)
        aif = 1.0_rp / (1.0_rp + vaux1)
        ais = 1.0_rp - aif
        ii = aif*vauxi_exm(11,ipoin,1) + (ais*vauxi_exm(12,ipoin,1))
   !!!!!!  gto
        ipp = aif*vauxi_exm(14,ipoin,1) + (ais*vauxi_exm(15,ipoin,1))
   
        gto = 0.02_rp
        if(ituss_exm == 3) then
           gto = gto*4.0_rp
        end if
        if(ituss_exm == 2) then
           gto = gto*4.0_rp
        end if
        vaux1 = kmcamk/vicel_exm(22,ipoin,1)
        fitop = 1.0_rp/(1.0_rp + vaux1)
   
        vaux2 = (fitop*vauxi_exm(13,ipoin,1) * ipp)
        vaux1 = ((1.0_rp-fitop) * vauxi_exm(10,ipoin,1) * ii ) + vaux2
        vaux1 = gto * (elmag(1,ipoin,1)-ek) * vaux1
        vicel_exm(3,ipoin,1) = vaux1        !!! ito
   
   !!! calculate ical
   !!!!!! calculate ff
        aff = 0.6_rp
        afs = 1.0_rp - aff    
        ff = (aff * vauxi_exm(17,ipoin,1)) + (afs * vauxi_exm(18,ipoin,1))   !value of gate ff
   !!! calculate fca
        vaux1 = 1.0_rp + exp((elmag(1,ipoin,1)-10.0_rp)/10.0_rp)
        afcaf = 0.3_rp + (0.6_rp/vaux1)
        afcas = 1.0_rp - afcaf    
        fca = (afcaf*vauxi_exm(19,ipoin,1)) + (afcas*vauxi_exm(20,ipoin,1))
   !!!!! calculate fp
        fp = aff*vauxi_exm(23,ipoin,1) + (afs*vauxi_exm(18,ipoin,1))
   !!!!  calculate fcap
        fcap = afcaf*vauxi_exm(24,ipoin,1) + (afcas*vauxi_exm(20,ipoin,1))    
   
   !!!!! calculate icana and icak
        kmn = 0.002_rp
        k2n = 1000.0_rp
        km2n = vauxi_exm(21,ipoin,1) * 1.0_rp
        vaux1 = kmn / vconc(6,ipoin,1)
        vaux1 = (1.0_rp+vaux1) *(1.0_rp+vaux1) * (1.0_rp+vaux1) * (1.0_rp+vaux1)
        vaux2 = k2n / (km2n+vaux1)
        anca = 1.0_rp / vaux2
        dnca = (anca*k2n) - (vauxi_exm(22,ipoin,1)*km2n)
        vaux2 = exp(2.0_rp*vfrt)
        vaux1 = exp(1.0_rp*vfrt)
        vaux3 = 1.0_rp / (vaux2-1.0_rp)
        phical = 4.0_rp * vffrt*((vconc(6,ipoin,1)*vaux2) - (0.341_rp*cao)) * vaux3
        vaux3 = 1.0_rp / (vaux1 -1.0_rp)
        phicana = 1.0_rp * vffrt*((0.75_rp*vconc(2,ipoin,1)*vaux1) - (0.75_rp*nao)) * vaux3
        vaux3 = 1.0_rp / (vaux1 -1.0_rp)
        phicak = 1.0_rp * vffrt*((0.75_rp*vconc(4,ipoin,1)*vaux1) - (0.75*ko)) * vaux3
        zca = 2.0_rp
        pca = 0.0001_rp !endo
        if(ituss_exm == 3) then
           pca = pca*1.2_rp  !epi
        end if
        if(ituss_exm == 2) then
           pca = pca*2.5_rp  !mid
        end if
        pcap = 1.1_rp * pca
        pcana = 0.00125_rp * pca
        pcak = 0.0003574_rp * pca
        pcanap = 0.00125_rp * pcap
        pcakp = 0.0003574_rp * pcap
        ficalp = 1.0_rp / (1.0_rp + (kmcamk/vicel_exm(22,ipoin,1)))
   
   !!!! calculate ical
        vaux1 = (fp * (1.0_rp-vauxi_exm(22,ipoin,1)) + (vauxi_exm(21,ipoin,1)*fcap*vauxi_exm(22,ipoin,1)))
        vaux1 = ficalp * pcap * phical * vauxi_exm(16,ipoin,1) * vaux1
        vaux2 = ff * (1.0_rp-vauxi_exm(22,ipoin,1)) + (vauxi_exm(21,ipoin,1) * fca * vauxi_exm(22,ipoin,1))
        vaux3 = (1.0_rp-ficalp) * pca * phical * vauxi_exm(16,ipoin,1)
        vicel_exm(4,ipoin,1)= (vaux3 * vaux2) + vaux1  !!! ical
   
   !!!! calculate icana
        vaux1 = (fp*(1.0_rp-vauxi_exm(22,ipoin,1)) + (vauxi_exm(21,ipoin,1)*fcap*vauxi_exm(22,ipoin,1)))
        vaux1 = vaux1 * ficalp*pcanap*phicana*vauxi_exm(16,ipoin,1)
        vaux2 = (ff*(1.0_rp-vauxi_exm(22,ipoin,1))) + (vauxi_exm(21,ipoin,1)*fca*vauxi_exm(22,ipoin,1))
        vaux2 = vaux2 * (1.0_rp-ficalp)*pcana*phicana*vauxi_exm(16,ipoin,1)
        vicel_exm(24,ipoin,1)= vaux2 + vaux1  !! icana
   
   !!!!! calculate icak
        vaux1 = fp * (1.0_rp-vauxi_exm(22,ipoin,1)) + (vauxi_exm(21,ipoin,1) * fcap * vauxi_exm(22,ipoin,1))
        vaux1 = vaux1 * ficalp * pcakp * phicak * vauxi_exm(16,ipoin,1)
        vaux2 = ff * (1.0_rp-vauxi_exm(22,ipoin,1)) + (vauxi_exm(21,ipoin,1) * fca * vauxi_exm(22,ipoin,1))
        vaux2 = vaux2 * (1.0_rp-ficalp) * pcak * phicak * vauxi_exm(16,ipoin,1)
        vicel_exm(25,ipoin,1) = vaux2 + vaux1   !!! icak
   
   !!!!  calculate ikr
        vaux1 = exp((elmag(1,ipoin,1)+54.81_rp)/38.21_rp)
        axrf = 1.0_rp / (1.0_rp + vaux1)
        axrs = 1.0_rp - axrf    
        xr = axrf*vauxi_exm(25,ipoin,1) + (axrs*vauxi_exm(26,ipoin,1))
        vaux1 = exp((elmag(1,ipoin,1)+55.0_rp)/75.0_rp)
        vaux1 = 1.0_rp/(1.0_rp + vaux1)
        vaux2 = exp((elmag(1,ipoin,1)-10.0_rp)/30.0_rp)
        rkr = vaux1 * (1.0_rp / (1.0_rp + vaux2))
        gkr = 0.046_rp
        if(ituss_exm == 3) then
           gkr = gkr * 1.3_rp
        end if
        if(ituss_exm == 2) then
           gkr = gkr * 0.8_rp
        end if
   
        vaux1 = sqrt(ko/5.4_rp) * (elmag(1,ipoin,1)-ek)
        vicel_exm(5,ipoin,1) = gkr * vaux1 * xr * rkr   !!! ikr
   
   !!!! calculate iks
        ksca = (0.000038_rp/vconc(5,ipoin,1)) ** (7./5.)
        ksca = 1.0_rp + (0.6_rp / (1.0_rp + ksca))
        gks = 0.0034_rp
        if(ituss_exm == 3) then
           gks = gks*1.4_rp
        end if
        vaux1 = gks * ksca * vauxi_exm(27,ipoin,1) * vauxi_exm(28,ipoin,1)
        vicel_exm(6,ipoin,1) =  vaux1 * (elmag(1,ipoin,1)-eks)
   
   !!!! calculate ik1
        rk1 = (1.0_rp+exp((elmag(1,ipoin,1) + 105.8_rp - 2.6_rp * ko) / 9.493_rp))
        rk1 = 1.0_rp / rk1
        gk1=0.1908_rp
        if(ituss_exm == 3) then
           gk1 = gk1 * 1.2_rp
        end if
        if(ituss_exm == 2) then
           gk1 = gk1 * 1.3_rp
        end if
        vaux1 = gk1 * sqrt(ko) * rk1 * vauxi_exm(29,ipoin,1) 
        vicel_exm(7,ipoin,1) = vaux1 * (elmag(1,ipoin,1)-ek) !!! ik1
   
        !%calculate inaca_i
        kna1 = 15.0_rp
        kna2 = 5.0_rp
        kna3 = 88.12_rp
        kasymm = 12.5_rp
        wna = 60000.0_rp
        wca = 60000.0_rp
        wnaca = 5000.0_rp
        kcaon = 1500000.0_rp
        kcaoff = 5000.0_rp
        qna = 0.5224_rp
        qca = 0.1670_rp
        hca = exp(qca * vfrt)
        hna = exp(qna * vfrt)
        h1 = 1.0_rp + ((vconc(1,ipoin,1)/kna3) * (1.0_rp +hna))
        h2 = (vconc(1,ipoin,1)*hna) / (kna3*h1)
        h3 = 1.0_rp/h1
        h4 = 1.0_rp + ((vconc(1,ipoin,1) / kna1) * (1.0_rp + (vconc(1,ipoin,1)/kna2)))
        h5 = vconc(1,ipoin,1)*vconc(1,ipoin,1) / (h4*kna1*kna2)
        h6 = 1.0_rp/h4
        h7 = 1.0_rp + ((nao/kna3)*(1.0_rp + (1.0_rp/hna)))
        h8 = nao / (kna3*hna*h7)
        h9 = 1.0_rp / h7
        h10 = kasymm + 1.0_rp + ((nao/kna1) * (1.0_rp + (nao/kna2)))
        h11 = nao*nao / (h10*kna1*kna2)
        h12 = 1.0_rp / h10
        k1 = h12*cao*kcaon
        k2 = kcaoff
        k3p = h9*wca
        k3pp = h8*wnaca
        k3 = k3p + k3pp
        k4p = h3*wca / hca
        k4pp = h2*wnaca
        k4 =k4p+k4pp
        k5 = kcaoff
        k6 = h6*vconc(5,ipoin,1)*kcaon
        k7 = h5*h2*wna
        k8 = h8*h11*wna
        x1 = (k2*k4*(k7+k6)) + (k5*k7*(k2+k3))
        x2 = (k1*k7*(k4+k5)) + (k4*k6*(k1+k8))
        x3 = (k1*k3*(k7+k6)) + (k8*k6*(k2+k3))
        x4 = (k2*k8*(k4+k5)) + (k3*k5*(k1+k8))
        e1 = x1 / (x1+x2+x3+x4)
        e2 = x2 / (x1+x2+x3+x4)
        e3 = x3 / (x1+x2+x3+x4)
        e4 = x4 / (x1+x2+x3+x4)
        kmcaact = 0.000150_rp
        allo = (kmcaact/vconc(5,ipoin,1)) * (kmcaact/vconc(5,ipoin,1))
        allo = 1.0_rp / (1.0_rp + allo)
        zna = 1.0_rp
        jncxna = (3.0_rp*(e4*k7-e1*k8)) + (e3*k4pp) - (e2*k3pp)
        jncxca = (e2*k2) - (e1*k1)
        gncx = 0.0008_rp
        if(ituss_exm == 3) then 
           gncx = gncx*1.1_rp
        end if
        if(ituss_exm == 2) then
           gncx = gncx*1.4_rp
        end if
        vaux1 = (zna*jncxna) + (zca*jncxca)
        vicel_exm(8,ipoin,1) = 0.8_rp * gncx * allo * vaux1  !!! inaca_i
   
        !%calculate inaca_ss
        h1 = 1.0 + ((vconc(2,ipoin,1)/kna3) * (1.0_rp + hna))
        h2 = (vconc(2,ipoin,1)*hna) / (kna3*h1)
        h3 = 1.0_rp/h1
        h4 = 1.0_rp + ((vconc(2,ipoin,1)/kna1) * (1.0_rp + (vconc(2,ipoin,1)/kna2)))
        h5 = vconc(2,ipoin,1)*vconc(2,ipoin,1) / (h4*kna1*kna2)
        h6 = 1.0_rp / h4
        h7 = 1.0_rp + ((nao/kna3) *(1.0_rp + (1.0_rp/hna)))
        h8 = nao / (kna3*hna*h7)
        h9 = 1.0_rp / h7
        h10 = kasymm + 1.0_rp + ((nao/kna1) * (1.0_rp + (nao/kna2)))
        h11 = nao*nao / (h10*kna1*kna2)
        h12 = 1.0_rp / h10
        k1 = h12*cao*kcaon
        k2 = kcaoff
        k3p = h9*wca
        k3pp = h8*wnaca
        k3 = k3p + k3pp
        k4p = h3*wca / hca
        k4pp = h2*wnaca
        k4 = k4p + k4pp
        k5 = kcaoff
        k6 = h6*vconc(6,ipoin,1)*kcaon
        k7 = h5*h2*wna
        k8 = h8*h11*wna
        x1 = (k2*k4*(k7+k6)) + (k5*k7*(k2+k3))
        x2 = (k1*k7*(k4+k5)) + (k4*k6*(k1+k8))
        x3 = (k1*k3*(k7+k6)) + (k8*k6*(k2+k3))
        x4 = (k2*k8*(k4+k5)) + (k3*k5*(k1+k8))
        e1 = x1 / (x1+x2+x3+x4)
        e2 = x2 / (x1+x2+x3+x4)
        e3 = x3 / (x1+x2+x3+x4)
        e4 = x4 / (x1+x2+x3+x4)
        kmcaact = 0.000150_rp
        allo = (kmcaact/vconc(6,ipoin,1)) * (kmcaact/vconc(6,ipoin,1))
        allo = 1.0_rp / (1.0_rp + allo)
        jncxna = (3.0_rp*(e4*k7-e1*k8)) + (e3*k4pp) - (e2*k3pp)
        jncxca = (e2*k2) - (e1*k1)
        vicel_exm(9,ipoin,1) = 0.2_rp * gncx * allo * ((zna*jncxna) + (zca*jncxca))   !!! inaca_ss
   
        !%calculate inak
        k1p = 949.5_rp
        k1m = 182.4_rp
        k2p = 687.2_rp
        k2m = 39.4_rp
        k3p = 1899.0_rp
        k3m = 79300.0_rp
        k4p = 639.0_rp
        k4m = 40.0_rp
        knai0 = 9.073_rp
        knao0 = 27.78_rp
        delta = -0.1550_rp
        knai = knai0 * exp(delta*vfrt/3.0_rp)
        knao = knao0 * exp((1.0_rp-delta)*vfrt/3.0_rp)
        kki = 0.5_rp
        kko = 0.3582_rp
        mgadp = 0.05_rp
        mgatp = 9.8_rp
        kmgatp = 0.0000001698_rp 
        hbig = 0.00000010_rp
        ep = 4.2_rp
        khp = 0.0000001698_rp 
        knap = 224.0_rp
        kxkur = 292.0_rp
        pp = ep / (1.0_rp + (hbig/khp) + (vconc(1,ipoin,1)/knap) + (vconc(3,ipoin,1)/kxkur))
        vaux1 = (vconc(1,ipoin,1)/knai) * (vconc(1,ipoin,1)/knai) * (vconc(1,ipoin,1)/knai)
        vaux2 = (1.0_rp + (vconc(1,ipoin,1)/knai)) * (1.0_rp + (vconc(1,ipoin,1)/knai)) * (1.0_rp + (vconc(1,ipoin,1)/knai))
        vaux3 = (1.0_rp + (vconc(3,ipoin,1)/kki)) * (1.0_rp + (vconc(3,ipoin,1)/kki))
        a1 = (k1p*vaux1) / (vaux2 + vaux3 - 1.0_rp)
        b1 = k1m * mgadp
        a2 = k2p
        vaux1 = (nao/knao) * (nao/knao) * (nao/knao)
        vaux2 = (1.0_rp + (nao/knao)) * (1.0_rp + (nao/knao)) * (1.0_rp + (nao/knao))
        vaux3 = (1.0_rp + (ko/kko)) * (1.0_rp + (ko/kko))
        b2 = (k2m*vaux1)/(vaux2 + vaux3 -1.0_rp)
        vaux1 = (ko/kko) * (ko/kko)
        vaux2 = (1.0_rp+(nao/knao)) * (1.0_rp+(nao/knao)) * (1.0_rp+(nao/knao))
        vaux3 = (1.0_rp + (ko/kko)) * (1.0_rp + (ko/kko)) 
        a3 = (k3p * vaux1) / (vaux2 + vaux3 - 1.0_rp)
        b3 = (k3m*pp*hbig) / (1.0_rp + (mgatp/kmgatp))
        a4 = (k4p*mgatp/kmgatp) / (1.0_rp + (mgatp/kmgatp))
        vaux1 = (vconc(3,ipoin,1)/kki) * (vconc(3,ipoin,1)/kki)
        vaux2 = (1.0_rp + (vconc(1,ipoin,1)/knai)) * (1.0_rp + (vconc(1,ipoin,1)/knai)) * (1.0_rp + (vconc(1,ipoin,1)/knai))
        vaux3 = (1.0_rp + (vconc(3,ipoin,1)/kki)) * (1.0_rp + (vconc(3,ipoin,1)/kki))
        b4 = (k4m*vaux1)/(vaux2 + vaux3 -1.0_rp)
        x1 = (a4*a1*a2) + (b2*b4*b3) + (a2*b4*b3) + (b3*a1*a2)
        x2 = (b2*b1*b4) + (a1*a2*a3) + (a3*b1*b4) + (a2*a3*b4)
        x3 = (a2*a3*a4) + (b3*b2*b1) + (b2*b1*a4) + (a3*a4*b1)
        x4 = (b4*b3*b2) + (a3*a4*a1) + (b2*a4*a1) + (b3*b2*a1)
        e1 = x1/(x1+x2+x3+x4)
        e2 = x2/(x1+x2+x3+x4)
        e3 = x3/(x1+x2+x3+x4)
        e4 = x4/(x1+x2+x3+x4)
        zk = 1.0_rp
        jnakna = 3.0_rp*((e1*a3)-(e2*b3))
        jnakk = 2.0_rp*((e4*b1)-(e3*a1))
        pnak = 30.0_rp
        if(ituss_exm == 3) then
           pnak = pnak*0.9_rp
        end if
        if(ituss_exm == 2) then
           pnak = pnak*0.7_rp
        end if
        vicel_exm(10,ipoin,1) = pnak*((zna*jnakna) + (zk*jnakk))  !!!! inak
   
        !%calculate ikb
        xkb = 1.0_rp / (1.0_rp + exp(-(elmag(1,ipoin,1)-14.48_rp)/18.34_rp))
        gkb = 0.003_rp
        if(ituss_exm == 3)  then
           gkb = gkb*0.6_rp
        end if
        vicel_exm(11,ipoin,1) = gkb*xkb*(elmag(1,ipoin,1)-ek)  !!!! ikb
   
        !%calculate inab
        pnab = 0.000000000375_rp
        vaux1 = pnab*vffrt*(vconc(1,ipoin,1)*exp(vfrt) - nao)
        vicel_exm(12,ipoin,1) =  vaux1 / (exp(vfrt)-1.0_rp)
   
        !%calculate icab
        pcab = 0.000000025_rp
        vaux1 = (vconc(5,ipoin,1)*exp(2.0_rp*vfrt) - (0.341_rp*cao))/(exp(2.0_rp*vfrt)-1.0_rp)
        vicel_exm(13,ipoin,1) = pcab * 4.0_rp*  vffrt * vaux1   !!!!icab
   
        !%calculate ipca
        gpca = 0.0005_rp
        vicel_exm(14,ipoin,1) = gpca * vconc(5,ipoin,1) / (0.0005_rp+vconc(5,ipoin,1))  !!! ipca
   
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        !%calculate diffusion fluxes
        vicel_exm(16,ipoin,1)=(vconc(2,ipoin,1)-vconc(1,ipoin,1))/2.0_rp    !!! jdiffna
        vicel_exm(17,ipoin,1)=(vconc(4,ipoin,1)-vconc(3,ipoin,1))/2.0_rp    !!! jdiffk
        vicel_exm(15,ipoin,1)=(vconc(6,ipoin,1)-vconc(5,ipoin,1))/0.2_rp    !!! jdiff
   
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   !!!!!  calculate jrel
        !%update camk
   
        fjrelp= 1.0_rp / (1.0_rp + (kmcamk/vicel_exm(22,ipoin,1)))
        vicel_exm(21,ipoin,1)= (1.0_rp - fjrelp) *vconc(9,ipoin,1) + (fjrelp * vconc(10,ipoin,1)) !!! jrel
   
   
   
        !%calculate serca pump, ca uptake flux
        jupnp = 0.004375_rp * vconc(5,ipoin,1) / (vconc(5,ipoin,1) + 0.00092_rp)
        vaux1 = 1.0_rp / (vconc(5,ipoin,1) + 0.00092_rp - 0.00017_rp)
        jupp = 2.75_rp * 0.004375_rp * vconc(5,ipoin,1) * vaux1
        if(ituss_exm == 3) then
           jupnp = jupnp * 1.3_rp
           jupp = jupp * 1.3_rp
        end if
        fjupp = (1.0_rp / (1.0_rp + (kmcamk/vicel_exm(22,ipoin,1))))
        vicel_exm(19,ipoin,1) = 0.0039375_rp*vconc(7,ipoin,1)/15.0_rp   !!!! jleak
        vicel_exm(18,ipoin,1) = (1.0_rp-fjupp) * jupnp + (fjupp * jupp) - vicel_exm(19,ipoin,1)  !!! jup
   
        !%calculate tranlocation flux
        vicel_exm(20,ipoin,1) = (vconc(7,ipoin,1)-vconc(8,ipoin,1)) / 100.0_rp  !!!! jtr
        
        !!  VOLTAGE INTEGRATION  t2 is the used one, t is the new
       !! dv=-(INa+INaL+Ito+ICaL+ICaNa+ICaK+IKr+IKs+IK1+INaCa_i+INaCa_ss+INaK+INab+IKb+IpCa+ICab+Istim)
         elmag(1,ipoin,2) = 0.0_rp
         do iicel=1,14
            elmag(1,ipoin,2)= elmag(1,ipoin,2) + vicel_exm(iicel,ipoin,1)
         end do
         do iicel=23,25
            elmag(1,ipoin,2)= elmag(1,ipoin,2) + vicel_exm(iicel,ipoin,1)
         end do
         elmag(1,ipoin,2) =  -elmag(1,ipoin,2)
    !!  Runge Kutta
         k1 = elmag(1,ipoin,2) 
         k2 = elmag(1,ipoin,2) + 0.5_rp * dtimeEP * k1
         k3 = elmag(1,ipoin,2) + 0.5_rp * dtimeEP * k2 
         k4 = elmag(1,ipoin,2) + dtimeEP * k3 
         elmag(1,ipoin,2) = elmag(1,ipoin,1) + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
    end do
  end if
  !  write(992,*) elmag(:,1,:)
  !write(993,*) vicel_exm(:,1,:)
  !  write(994,*) vconc(:,1,:)

  !write(996,*) cutim
end subroutine exm_ceiohr
