!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_inihet.f90
!> @author  Jazmin Aguado-Sierra
!> @brief   Initial condition setup for TenTuscher-Panfilov 2006 heterogeneous model
!> @details Obtains initial conditions of Normal or Heart Failure cell at  70 bpm or 857 ms \n
!!    Otherwise, it obtains the initial conditions from single cell runs in ONECEL.F90 AND OCEIHE.F90 \n
!> @} 
!!-----------------------------------------------------------------------
subroutine exm_intina

  use      def_master
  use      def_elmtyp
  use      def_domain

  use      def_exmedi

  implicit none
  integer(ip) :: ipoin,icomp
  integer(ip) :: ielem,inode,pnode,pmate,pelty
  real(rp)    ::  vaux1, vaux2, vaux3, rhsx
            
    if(kfl_timei_exm==1) then 
        !
        ! Load initial conditions for the potentials
        !
        do icomp= 1,ncomp_exm
           do ipoin=1,npoin
             elmag(1,ipoin,icomp) = vmini_exm(1,3)     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
             elmag(2,ipoin,icomp) = vmini_exm(1,3)     ! EXTRA (BI-DOMAIN) POTENTIAL
           end do
        end do
        
     do ipoin=1,npoin
        vicel_exm(:,ipoin,1:3) = 0.0_rp
        vconc(6:8,ipoin,1:3) = 0.0_rp
        vconc(10,ipoin,1:3) = 0.0_rp
     end do

     do ielem=1,nelem
        pelty = ltype(ielem)
 
        if ( pelty > 0 .and. lelch(ielem) /= ELCNT ) then
           pnode = nnode(pelty)
           pmate = 1                       
           if( nmate > 1 ) then
              pmate = lmate_exm(ielem)
           end if
 
           do inode= 1,pnode
              ipoin= lnods(inode,ielem)     
              ituss_exm = int(celty_exm(1,ipoin))
              !write(*,*) ituss_exm
              if(ituss_exm == 3) then   !epicardial
                vauxi_exm(1,ipoin,1:3) = vauin_exm(1,1,3)          !Variable m
                vauxi_exm(2,ipoin,1:3) = vauin_exm(2,1,3)        !Variable h 
                vauxi_exm(3,ipoin,1:3) = vauin_exm(3,1,3)        !Variable j
                vauxi_exm(4,ipoin,1:3) = vauin_exm(4,1,3)           !Variable d
                vauxi_exm(5,ipoin,1:3) = vauin_exm(5,1,3)         !Variable f
                vauxi_exm(12,ipoin,1:3) = vauin_exm(12,1,3)        !Variable f2
                vauxi_exm(6,ipoin,1:3) = vauin_exm(6,1,3)        ! Variable fCa
                vauxi_exm(7,ipoin,1:3) = vauin_exm(7,1,3)           !Variable r
                vauxi_exm(8,ipoin,1:3) = vauin_exm(8,1,3)           !Variable s
                vauxi_exm(9,ipoin,1:3) = vauin_exm(9,1,3)        !Variable xs 
                vauxi_exm(10,ipoin,1:3) = vauin_exm(10,1,3)        !Variable xr1
                vauxi_exm(11,ipoin,1:3) = vauin_exm(11,1,3)       !Variable xr2
                vauxi_exm(13,ipoin,1:3) = vauin_exm(13,1,3)       !Variable mL
                vauxi_exm(14,ipoin,1:3) = vauin_exm(14,1,3)       !Variable hL 
                vconc(1,ipoin,1:3) = vcoin_exm(1,1,3)                 !Variable Cai
                vconc(2,ipoin,1:3) = vcoin_exm(2,1,3)                 !Variable CaSR
                vconc(3,ipoin,1:3) = vcoin_exm(3,1,3)                 !Variable Nai
                vconc(4,ipoin,1:3) = vcoin_exm(4,1,3)                !Variable Ki
                vconc(5,ipoin,1:3) = vcoin_exm(5,1,3)                 !Variable CaSS
                vconc(9,ipoin,1:3) = vcoin_exm(9,1,3)      !Variable Rprime
              else if(ituss_exm == 1) then  !endocardial
                vauxi_exm(1,ipoin,1:3) = vauin_exm(1,2,3)           !Variable m
                vauxi_exm(2,ipoin,1:3) = vauin_exm(2,2,3)       !Variables h 
                vauxi_exm(3,ipoin,1:3) = vauin_exm(3,2,3)       !Variable j
                vauxi_exm(4,ipoin,1:3) = vauin_exm(4,2,3)           !Variable d
                vauxi_exm(5,ipoin,1:3) = vauin_exm(5,2,3)         !Variable f
                vauxi_exm(12,ipoin,1:3) = vauin_exm(12,2,3)          ! Variable f2
                vauxi_exm(6,ipoin,1:3) = vauin_exm(6,2,3)         ! Variable fCa
                vauxi_exm(7,ipoin,1:3) = vauin_exm(7,2,3)           !Variable r
                vauxi_exm(8,ipoin,1:3) = vauin_exm(8,2,3)           !Variable s
                vauxi_exm(9,ipoin,1:3) = vauin_exm(9,2,3)        !Variable xs 
                vauxi_exm(10,ipoin,1:3) = vauin_exm(10,2,3)        !Variable xr1
                vauxi_exm(11,ipoin,1:3) = vauin_exm(11,2,3)      !Variable xr2
                vauxi_exm(13,ipoin,1:3) = vauin_exm(13,1,3)       !Variable mL
                vauxi_exm(14,ipoin,1:3) = vauin_exm(14,1,3)       !Variable hL 
                vconc(1,ipoin,1:3) = vcoin_exm(1,2,3)                !Variable Cai
                vconc(2,ipoin,1:3) = vcoin_exm(2,2,3)                !Variable CaSR
                vconc(3,ipoin,1:3) = vcoin_exm(3,2,3)                 !Variable Nai
                vconc(4,ipoin,1:3) = vcoin_exm(4,2,3)                !Variable Ki
                vconc(5,ipoin,1:3) = vcoin_exm(5,2,3)                 !Variable CaSS
                vconc(9,ipoin,1:3) = vcoin_exm(9,2,3)       !Variable Rprime
              else if(ituss_exm == 2) then  !mid
                vauxi_exm(1,ipoin,1:3) = vauin_exm(1,3,3)          !Variable m
                vauxi_exm(2,ipoin,1:3) = vauin_exm(2,3,3)       !Variables h 
                vauxi_exm(3,ipoin,1:3) = vauin_exm(3,3,3)        !Variable j
                vauxi_exm(4,ipoin,1:3) = vauin_exm(4,3,3)           !Variable d
                vauxi_exm(5,ipoin,1:3) = vauin_exm(5,3,3)        !Variable f
                vauxi_exm(12,ipoin,1:3) = vauin_exm(12,3,3)          !Variable f2
                vauxi_exm(6,ipoin,1:3) = vauin_exm(6,3,3)        ! Variable fCa
                vauxi_exm(7,ipoin,1:3) = vauin_exm(7,3,3)          !Variable r
                vauxi_exm(8,ipoin,1:3) = vauin_exm(8,3,3)           !Variable s
                vauxi_exm(9,ipoin,1:3) = vauin_exm(9,3,3)       !Variable xs 
                vauxi_exm(10,ipoin,1:3) = vauin_exm(10,3,3)        !Variable xr1
                vauxi_exm(11,ipoin,1:3) = vauin_exm(11,3,3)      !Variable xr2
                vauxi_exm(13,ipoin,1:3) = vauin_exm(13,1,3)       !Variable mL
                vauxi_exm(14,ipoin,1:3) = vauin_exm(14,1,3)       !Variable hL 
                vconc(1,ipoin,1:3) = vcoin_exm(1,3,3)               !Variable Cai
                vconc(2,ipoin,1:3) = vcoin_exm(2,3,3)                !Variable CaSR
                vconc(3,ipoin,1:3) = vcoin_exm(3,3,3)                 !Variable Nai
                vconc(4,ipoin,1:3) = vcoin_exm(4,3,3)                 !Variable Ki
                vconc(5,ipoin,1:3) = vcoin_exm(5,3,3)                 !Variable CaSS
                vconc(9,ipoin,1:3) = vcoin_exm(9,3,3)        !Variable Rprime
            end if
            ! Variable of activation O (related with I_rel current)
            ! nconc = 10
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
            !  kcasr = max_sr - (max_sr - min_sr)/(1+(pow((EC/Ca_SR), 2)))
            vaux1 = 1.0_rp + ((1.5_rp / vconc(2,ipoin,2))*(1.5_rp / vconc(2,ipoin,2)))  !conc2 = CaSRfree = CaSR
            vaux1 = 1.0_rp / vaux1
            vaux1 = 2.5_rp - (1.5_rp * vaux1)  !kCaSR
            !     O = ( k1*pow(Ca_ss, 2)*R_prime)/(k3+ k1*pow(Ca_ss, 2))      
            vaux3 = 0.15_rp / vaux1   !K1
            vaux2 = 0.045_rp * vaux1  !K2
            rhsx = 1.0_rp / (0.06_rp + (vaux3*vconc(5,ipoin,2)*vconc(5,ipoin,2)))      !O for calcium dynamics
            rhsx = vaux3 * vconc(5,ipoin,2) * vconc(5,ipoin,2) * vconc(9,ipoin,2) * rhsx !O
            vconc(10,ipoin,1:3) = rhsx 
           end do
        end if
    end do
  end if
!  end if

!write(991,*) vconc(1,:,1)  !JAS
end subroutine exm_intina
