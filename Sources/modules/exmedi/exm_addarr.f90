!-----------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_addarr.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   This routine computes some additional arrays like conductivty
!> @details This routine computes some additional arrays like conductivty
!> @} 
!-----------------------------------------------------------------------
subroutine exm_addarr()
  !-----------------------------------------------------------------------
  !****f* Exmedi/exm_addarr
  ! NAME 
  !    exm_addarr
  ! DESCRIPTION
  !   subroutine that computes the conductivity tensor for 2D and 3D 
  ! USES
  !    exm_...
  ! USED BY
  !    exm_doiter
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_exmedi
  implicit none
  integer(ip) :: istat,npara,ipoin,kpoin,ielem,iipar,iauxi,imate,pelty,pnode,inode
  integer(ip) :: idime,icomo,igrou,ngrou,ivalu,kelem,icmod,nrema,irema,nrefi,irefi,igaus
  real(rp)    :: xauxi,xrefi(12),xbalo(3,3),xnorm(3),xlong,xtra1,xtra2,elcod(ndime,mnode),elfib(ndime,mnode)
  real(rp)    :: cartd(ndime,mnode)
  real(rp)    :: dvolu,detjm
  real(rp)    :: xjaci(ndime,ndime),xjacm(ndime,ndime),xshap




  if( INOTMASTER ) then
     !
     ! Fibers
     !
     if( modfi_exm < 0 ) then
        fiber_exm => xfiel(-modfi_exm) % a
     end if
     !
     ! Cell types
     !
     if( kfl_heter_exm == 1 ) then
        celty_exm => xfiel(-modce_exm) % a
     end if

     if( kfl_comdi_exm == 2) then

        nrema = ndime
        nrefi = npoin

        cecon_exm= 0.0_rp

        elements: do kelem = 1,nelez(lzone(ID_EXMEDI))
           ielem = lelez(lzone(ID_EXMEDI)) % l(kelem)
           
           ! Initialize
           pelty = ltype(ielem)
           
           if( pelty > 0 ) then
              pnode = nnode(pelty)
              
              imate = 1
              if( nmate > 1 ) then
                 imate = lmate_exm(ielem)
              end if

              do inode=1,pnode
                 ipoin= lnods(inode,ielem)
                 do idime=1,ndime
                    elcod(idime,inode)=coord(idime,ipoin)
                    elfib(idime,inode)=fiber_exm(idime,ipoin)
                 end do                 
              end do

              gauss_points: do igaus=1,ngaus(pelty)

                 ! Cartesian derivatives and Jacobian
                 call elmder(pnode,ndime,elmar(pelty)%deriv(1,1,igaus),elcod,cartd,detjm,xjacm,xjaci)
                 dvolu=elmar(pelty)%weigp(igaus)*detjm

                 xrefi= 0.0_rp
                 do inode=1,pnode
                    xshap= elmar(pelty)%shape(inode,igaus)
                    do idime= 1,ndime
                       xrefi(idime)= xrefi(idime)+elfib(idime,inode)*xshap
                    end do
                 end do
             
                 xrefi(ndime+1)       = gcond_exm(1,1,imate)  ! longitudinal intra
                 xrefi(ndime+2)       = gcond_exm(1,2,imate)  ! cross-wise (1) intra
                 xrefi(ndime+ndime)   = gcond_exm(1,3,imate)  ! cross-wise (2) intra
                 
                 xauxi = xrefi(1) * xrefi(1) + xrefi(2) * xrefi(2)                 
                 if (ndime==3) xauxi= xauxi+xrefi(3)*xrefi(3)
                 xauxi= sqrt(xauxi)
                 
                 xbalo= 0.0_rp
                 if (xauxi > 1.0e-8) then              
                    xbalo(1,1) = xrefi(1)/xauxi
                    xbalo(2,1) = xrefi(2)/xauxi
                    if (ndime==3) xbalo(ndime,1) = xrefi(ndime)/xauxi
                    
                    if( ndime == 2 ) then
                       
                       xbalo(1,2)=   - xbalo(2,1)
                       xbalo(2,2)=     xbalo(1,1)
                       
                       do inode=1,pnode
                          ipoin= lnods(inode,ielem)
                          xshap= elmar(pelty)%shape(inode,igaus)
                          
                          cecon_exm(1,1,ipoin) = cecon_exm(1,1,ipoin) + &
                               xshap * (xrefi(ndime+1)*xbalo(1,1)*xbalo(1,1) &
                               + xrefi(ndime+2)*xbalo(2,1)*xbalo(2,1) ) * dvolu / vmass(ipoin)
                          
                          cecon_exm(2,2,ipoin) = cecon_exm(2,2,ipoin) + &
                               xshap * (xrefi(ndime+1)*xbalo(1,2)*xbalo(1,2) &
                               + xrefi(ndime+2)*xbalo(2,2)*xbalo(2,2) ) * dvolu / vmass(ipoin)
                          
                          cecon_exm(1,2,ipoin) = cecon_exm(1,2,ipoin) + &
                               xshap * (xrefi(ndime+1)*xbalo(1,2)*xbalo(1,1) &
                               + xrefi(ndime+2)*xbalo(2,1)*xbalo(2,2) ) * dvolu / vmass(ipoin)
                          
                          
                          cecon_exm(2,1,ipoin) = cecon_exm(1,2,ipoin)
                          
                       end do
                       
                    else if (ndime==3) then
                       
                       do inode= 1,pnode
                          
                          ipoin= lnods(inode,ielem)
                          xshap= elmar(pelty)%shape(inode,igaus)

                          xnorm(1)=0.0_rp
                          xnorm(2)=1.0_rp
                          xnorm(3)=0.0_rp
                          
                          xbalo(1,2) = xnorm(2)*xbalo(3,1)-xnorm(3)*xbalo(2,1)
                          xbalo(2,2) = xnorm(3)*xbalo(1,1)-xnorm(1)*xbalo(3,1)
                          xbalo(3,2) = xnorm(1)*xbalo(2,1)-xnorm(2)*xbalo(1,1)
                          
                          xauxi=xbalo(1,2)*xbalo(1,2)+xbalo(2,2)*xbalo(2,2)+xbalo(3,2)*xbalo(3,2)
                          xauxi=sqrt(xauxi)
                          
                          if (xauxi < 0.000001_rp) then
                             xnorm(1) = 1.0_rp
                             xnorm(2) = 0.0_rp
                             xnorm(3) = 0.0_rp
                             
                             xbalo(1,2) = xnorm(2)*xbalo(3,1)-xnorm(3)*xbalo(2,1)
                             xbalo(2,2) = xnorm(3)*xbalo(1,1)-xnorm(1)*xbalo(3,1)
                             xbalo(3,2) = xnorm(1)*xbalo(2,1)-xnorm(2)*xbalo(1,1)
                             
                             xauxi = xbalo(1,2)*xbalo(1,2)+xbalo(2,2)*xbalo(2,2)+xbalo(3,2)*xbalo(3,2)
                             xauxi = sqrt(xauxi)                                  
                          end if
                          
                          xbalo(1,2) = xbalo(1,2)/xauxi
                          xbalo(2,2) = xbalo(2,2)/xauxi
                          xbalo(3,2) = xbalo(3,2)/xauxi
                          
                          ! vector axial
                          
                          xbalo(1,3) = xbalo(2,1)*xbalo(3,2)-xbalo(3,1)*xbalo(2,2)
                          xbalo(2,3) = xbalo(3,1)*xbalo(1,2)-xbalo(1,1)*xbalo(3,2)
                          xbalo(3,3) = xbalo(1,1)*xbalo(2,2)-xbalo(2,1)*xbalo(1,2)
                          
                          xlong= xrefi(ndime+1)
                          xtra1= xrefi(ndime+2)
                          xtra2= xrefi(ndime+2)
                          
                          cecon_exm(1,1,ipoin) = cecon_exm(1,1,ipoin) + &
                               xshap*(xlong*xbalo(1,1)*xbalo(1,1) + &
                               xtra1*xbalo(1,2)*xbalo(1,2) + &
                               xtra2*xbalo(1,3)*xbalo(1,3)) * dvolu / vmass(ipoin)                
                          cecon_exm(2,2,ipoin) = cecon_exm(2,2,ipoin) +&
                               xshap*(xlong*xbalo(2,1)*xbalo(2,1) + &
                               xtra1*xbalo(2,2)*xbalo(2,2) + &
                               xtra2*xbalo(2,3)*xbalo(2,3)) * dvolu / vmass(ipoin) 
                          cecon_exm(3,3,ipoin) = cecon_exm(3,3,ipoin) + &
                               xshap*(xlong*xbalo(3,1)*xbalo(3,1) + &
                               xtra1*xbalo(3,2)*xbalo(3,2) + &
                               xtra2*xbalo(3,3)*xbalo(3,3)) * dvolu / vmass(ipoin)              
                          cecon_exm(1,2,ipoin) = cecon_exm(1,2,ipoin) + &
                               xshap*(xlong*xbalo(1,1)*xbalo(2,1) + &
                               xtra1*xbalo(1,2)*xbalo(2,2) + &
                               xtra2*xbalo(1,3)*xbalo(2,3)) * dvolu / vmass(ipoin)              
                          cecon_exm(1,3,ipoin) = cecon_exm(1,3,ipoin) + &
                               xshap*(xlong*xbalo(1,1)*xbalo(3,1) + &
                               xtra1*xbalo(1,2)*xbalo(3,2) + &
                               xtra2*xbalo(1,3)*xbalo(3,3)) * dvolu / vmass(ipoin)              
                          cecon_exm(2,3,ipoin) = cecon_exm(2,3,ipoin) + &
                               xshap*(xlong*xbalo(2,1)*xbalo(3,1) + &
                               xtra1*xbalo(2,2)*xbalo(3,2) + &
                               xtra2*xbalo(2,3)*xbalo(3,3)) * dvolu / vmass(ipoin)              

                          cecon_exm(3,2,ipoin) = cecon_exm(2,3,ipoin)
                          cecon_exm(3,1,ipoin) = cecon_exm(1,3,ipoin)
                          cecon_exm(2,1,ipoin) = cecon_exm(1,2,ipoin)
                          
                          
                       end do
                       
                    end if
                    
                 end if
              end do gauss_points
              
           end if
        end do elements
     end if

     call rhsmod(ndime*ndime,cecon_exm)



  end if
  
  

end subroutine exm_addarr
