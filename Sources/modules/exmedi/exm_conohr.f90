!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_conohr.f90
!> @author  Jazmin Aguado-Sierra
!> @brief   Ion Concentration calculation for Ohara-Rudy human heterogeneous model 2011
!> @details Calculate every ION at every time step\n
!! The auxiliary variables are 9: \n
!! nai=vconc(1,ipoin,1:3) \n
!! nass=vconc(2,ipoin,1:3) \n
!! ki=vconc(3,ipoin,1:3) \n
!! kss=vconc(4,ipoin,1:3) \n
!! cai=vconc(5,ipoin,1:3) \n
!! cass=vconc(6,ipoin,1:3) \n
!! cansr=vconc(7,ipoin,1:3) \n
!! cajsr=vconc(8,ipoin,1:3) \n
!! jrelnp=vconc(9,ipoin,1:3) \n
!! jrelp=vconc(10,ipoin,1:3) \n
!! camkt=vconc(11,ipoin,1:3) \n
!> @} 
!!-----------------------------------------------------------------------
subroutine exm_conohr

  use      def_parame
  use      def_master
  use      def_elmtyp
  use      def_domain
  use      def_exmedi

  implicit none

  integer(ip) :: ipoin
  integer(ip) :: ielem,inode,pnode,pmate
  real(rp)    :: cai, cass, cansr, cajsr, nass, nai, ki, kss, camkt
  real(rp)    :: vaux1, vaux2, vaux3, k1, k2, k3, k4, nao, cao, ko, rgas, temp, farad  
  real(rp)    :: rhsx1, rhsx2, rhsx, val0, bslmax, bsrmax,cmdnmax, csqnmax
  real(rp)    :: leng, rad, vcell, ageo, acap, vmyo, vnsr, vjsr, vss, kmcamk, kmcmdn
  real(rp)    :: acamk, bcamk, camko, kmcam, trpnmax, kmcsqn, kmtrpn, kmbsl, kmbsr
  real(rp)    :: bt, a_rel, btp, a_relp, vinf, xitaux, vaux0, dtimeEP, vaux4, vaux5
  

!extracellular ionic concentrations
nao = 140.0_rp
cao = 1.8_rp
ko = 5.4_rp

!physical constants
rgas = 8314.0_rp
temp = 310.0_rp
farad = 96485.0_rp

!cell geometry
leng = 0.01_rp
rad = 0.0011_rp
vcell = 1000_rp*3.14_rp*rad*rad*leng  !  transform from microLiters to cubic cm
ageo = 2_rp*3.14_rp*rad*rad + 2_rp*3.14_rp*rad*leng
acap = 2_rp*ageo
vmyo = 0.68_rp*vcell
vnsr = 0.0552_rp*vcell
vjsr = 0.0048_rp*vcell
vss = 0.02_rp*vcell
!%camk constants
kmcamk = 0.15_rp

acamk = 0.05_rp
bcamk = 0.00068_rp
camko = 0.05_rp
kmcam = 0.0015_rp
dtimeEP=dtime *1000.0_rp

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! computation of concentrations
  if(INOTMASTER) then
    do ipoin= 1,npoin  
       ituss_exm = int(celty_exm(1,ipoin))
        
        !%update camk
        rhsx1 = acamk * vicel_exm(26,ipoin,1) * (vicel_exm(26,ipoin,1) + vconc(11,ipoin,2))
        rhsx =  rhsx1 - (bcamk*vconc(11,ipoin,2))
        val0  =  vconc(11,ipoin,3)
         
        !camkt = val0 + dtimeEP/xmccm_exm * rhsx
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         camkt = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
        
        vconc(11,ipoin,1) = camkt      ! value of camkt concentration
        
        !%calcium buffer constants
        cmdnmax = 0.05_rp
        if(ituss_exm == 3) cmdnmax = cmdnmax*1.3_rp
    
        kmcmdn = 0.00238_rp
        trpnmax = 0.07_rp
        kmtrpn = 0.0005_rp
        bsrmax = 0.047_rp
        kmbsr = 0.00087_rp
        bslmax = 1.124_rp
        kmbsl = 0.0087_rp
        csqnmax = 10.0_rp
        kmcsqn = 0.8_rp
        
        !%update intracellular concentrations, using buffers for cai, cass, cajsr
        !! calculate na current
        vaux1 =  vicel_exm(1,ipoin,1) + vicel_exm(2,ipoin,1) + vicel_exm(12,ipoin,1)
        vaux2 = 3.0_rp*vicel_exm(8,ipoin,1) + 3.0_rp*vicel_exm(10,ipoin,1)
        vaux3 = -(vaux1+vaux2) * acap/(farad*vmyo)
        rhsx = vaux3 + (vicel_exm(16,ipoin,1)*vss/vmyo)
        val0 = vconc(1,ipoin,3)
         
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         nai = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
        !nai = val0 + dtimeEP/xmccm_exm * rhsx
        vconc(1,ipoin,1) = nai
        
        !!! calculate na current in subspace ss
        vaux1 = (vicel_exm(24,ipoin,1) + 3.0_rp * vicel_exm(9,ipoin,1)) * acap / (farad*vss)
        rhsx =  -vaux1 - vicel_exm(16,ipoin,1)
        val0 = vconc(2,ipoin,3)
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         nass = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
        !nass = val0 + dtimeEP/xmccm_exm * rhsx
        vconc(2,ipoin,1) = nass             !!! nass
     
         !!! calculate k current
        vaux1 = vicel_exm(3,ipoin,1) + vicel_exm(5,ipoin,1) + vicel_exm(6,ipoin,1)
        vaux2 = vicel_exm(7,ipoin,1) + vicel_exm(11,ipoin,1) + vicel_exm(23,ipoin,1) - (2.0_rp*vicel_exm(10,ipoin,1))
        vaux3 = (vicel_exm(17,ipoin,1)*vss/vmyo)
        rhsx = (-(vaux1+ vaux2) * acap/(farad*vmyo)) + vaux3
        val0 = vconc(3,ipoin,3)
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         ki = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))                
        !ki = val0 + dtimeEP/xmccm_exm * rhsx
        vconc(3,ipoin,1) = ki             !!! ki  
        
        !!!!  calculate k current in the subspace ss
        
        rhsx = -(vicel_exm(25,ipoin,1)*acap/(farad*vss)) - vicel_exm(17,ipoin,1)
        val0 = vconc(4,ipoin,3)
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         kss = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))                 
        !kss = val0 + dtimeEP/xmccm_exm * rhsx
        vconc(4,ipoin,1) = kss             !!! kss
        
        !!!  calculate ca current
        vaux1 = (kmcmdn+vconc(5,ipoin,2)) * (kmcmdn+vconc(5,ipoin,2))
        vaux2 = (kmtrpn+vconc(5,ipoin,2)) * (kmtrpn+vconc(5,ipoin,2))
        vaux3 = 1.0_rp/(1.0_rp + (cmdnmax*kmcmdn/vaux1) + (trpnmax*kmtrpn/vaux2))
        rhsx1 = vicel_exm(14,ipoin,1)+vicel_exm(13,ipoin,1) - (2.0_rp*vicel_exm(8,ipoin,1))
        rhsx2 = -(vicel_exm(18,ipoin,1)*vnsr/vmyo) + (vicel_exm(15,ipoin,1)*vss/vmyo)
        rhsx = vaux3 *(-(rhsx1 *acap/(2.0_rp*farad*vmyo)) + rhsx2 )
        val0 = vconc(5,ipoin,3)
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         cai = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))                 
        !cai = val0 + dtimeEP/xmccm_exm * rhsx
        vconc(5,ipoin,1) = cai             !!! cai
    
        !!!! calculate ca current in the subspace ss
        vaux1 = (kmbsr+vconc(6,ipoin,2)) * (kmbsr+vconc(6,ipoin,2))
        vaux2 = (kmbsl+vconc(6,ipoin,2)) * (kmbsl+vconc(6,ipoin,2))
        vaux3 = 1.0_rp / (1.0_rp + (bsrmax*kmbsr/vaux1) + (bslmax*kmbsl/vaux2))
        rhsx1 = vicel_exm(4,ipoin,1) - (2.0_rp*vicel_exm(9,ipoin,1))
        rhsx2 = vicel_exm(21,ipoin,1)*vjsr/vss
        rhsx = vaux3 *(-rhsx1 *acap/(2.0_rp*farad*vss) + rhsx2 - vicel_exm(15,ipoin,1))
        val0 = vconc(6,ipoin,3)
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         cass = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))                 
        !cass = val0 + dtimeEP/xmccm_exm * rhsx
        vconc(6,ipoin,1) = cass             !!! cass
        
        !!!! calculate ca current in the sarcoplasmic reticulum nsr
        rhsx1 = vicel_exm(20,ipoin,1)*vjsr / vnsr
        rhsx = vicel_exm(18,ipoin,1) - rhsx1
        val0 = vconc(7,ipoin,3)
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         cansr = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))                 
        !cansr = val0 + dtimeEP/xmccm_exm * rhsx
        vconc(7,ipoin,1) = cansr             !!! cansr
        
        !!!! calculate ca current in the junctional sarcoplasmic reticulum jsr
        vaux1 = (kmcsqn+vconc(8,ipoin,2)) * (kmcsqn+vconc(8,ipoin,2))
        vaux3 = csqnmax * kmcsqn / vaux1
        vaux2 = 1.0_rp / (1.0_rp + vaux3) 
        rhsx = vaux2 *(vicel_exm(20,ipoin,1)-vicel_exm(21,ipoin,1))
        val0 = vconc(8,ipoin,3)
         k1 = rhsx 
         k2 = rhsx + 0.5_rp * dtimeEP * k1
         k3 = rhsx + 0.5_rp * dtimeEP * k2 
         k4 = rhsx + dtimeEP * k3 
         cajsr = val0 + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))                 
        !cajsr = val0 + dtimeEP/xmccm_exm * rhsx
        vconc(8,ipoin,1) = cajsr             !!! cajsr
        
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        !%calculate ryanodione receptor calcium induced calcium release from the jsr
        bt = 4.75_rp
        a_rel = 0.5_rp * bt
        vaux1 = (1.5_rp/vconc(8,ipoin,2)) * (1.5_rp/vconc(8,ipoin,2)) 
        vaux1 = vaux1 * vaux1 * vaux1 * vaux1
        vinf = a_rel * (-vicel_exm(4,ipoin,1)) / (1.0_rp + vaux1)
        if(ituss_exm == 2) then
            vinf = vinf * 1.7_rp
        end if
        xitaux = bt / (1.0_rp + (0.0123_rp/vconc(8,ipoin,2)))
        
        if(xitaux < 0.001_rp) then
           xitaux = 0.001_rp
        end if
        xitaux = 1.0_rp / xitaux
        
        vaux0 = vconc(9,ipoin,3)  
        
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vconc(9,ipoin,1) = vaux3      ! value of variable jrelnp   
        
        !!!!!%%%%%%%%%%%%%%%%%%%%% 
        
        btp = 1.25_rp * bt
        a_relp = 0.5_rp * btp
        vaux1 = (1.5_rp/vconc(8,ipoin,2)) * (1.5_rp/vconc(8,ipoin,2))
        vaux1 = vaux1 * vaux1 * vaux1 * vaux1
        vinf = a_relp * (-vicel_exm(4,ipoin,1)) / (1.0_rp + vaux1)
        if(ituss_exm == 2) then
            vinf = vinf * 1.7_rp
        end if
        xitaux = btp / (1.0_rp+ (0.0123_rp/vconc(8,ipoin,2)))
        
        if(xitaux < 0.001_rp) then
           xitaux = 0.001_rp 
        end if
        vaux0 = vconc(10,ipoin,3)
        xitaux = 1.0_rp / xitaux
    
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
    
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vconc(10,ipoin,1) = vaux3      ! value of jrelp
        
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end do
  end if
      
end subroutine exm_conohr
