subroutine exm_restar(itask)
  !------------------------------------------------------------------------
  !****f* Temper/exm_restar
  ! NAME 
  !    exm_restar
  ! DESCRIPTION
  !    This routine:
  !    ITASK = 1 ... Reads the initial values from the restart file
  !            2 ... Writes restart file
  ! USES
  ! USED BY
  !    exm_turnon
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_exmedi
  use mod_postpr
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: icomp,kfl_gores,igate
  character(5)            :: wopoe(2)
  !
  ! Check if restart file should be read or written
  !
  call respre(itask,kfl_gores)
  if( kfl_gores == 0 ) return

  if( itask == READ_RESTART_FILE ) then
     icomp = ncomp_exm
  else
     icomp = 1
  end if

  !----------------------------------------------------------------------
  !
  ! Variables
  !
  !----------------------------------------------------------------------

  wopoe(1) = 'ELMA1'
  wopoe(2) = 'SCALA'
  if( INOTMASTER ) gesca => elmag(1,:,icomp)
  call postpr(gesca,wopoe,ittim,cutim)

  wopoe(1) = 'ELMA2'
  wopoe(2) = 'SCALA'
  if( INOTMASTER ) gesca => elmag(2,:,icomp)
  call postpr(gesca,wopoe,ittim,cutim)

  wopoe(1) = 'REFHN'
  wopoe(2) = 'SCALA'
  if( INOTMASTER ) gesca => refhn_exm(:,icomp)
  call postpr(gesca,wopoe,ittim,cutim)     

  !----------------------------------------------------------------------
  !
  ! Finish
  !
  !----------------------------------------------------------------------

  call respre(3_ip,kfl_gores)

end subroutine exm_restar
 
