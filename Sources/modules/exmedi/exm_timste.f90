subroutine exm_timste
  !-----------------------------------------------------------------------
  !****f* Exmedi/exm_timste
  ! NAME 
  !    exm_timste
  ! DESCRIPTION
  !    This routine computes the time step
  ! USES
  !    exm_iniunk
  !    exm_updtss
  !    exm_updbcs
  !    exm_updunk
  ! USED BY
  !    Exmedi
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master

  use def_exmedi

  implicit none


  !    Update applied currents appfi_exm (source terms) 
  !    using its definition function  and lapno_exm,  done in exm_comapp  

  if (INOTMASTER) call exm_comapp

  !
  ! Time step size.
  !
  call exm_updtss     

end subroutine exm_timste
