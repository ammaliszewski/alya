subroutine exm_solsys

!-----------------------------------------------------------------------
!
! This routine is the bridge to the different solvers programmed
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain

  use      def_exmedi

  implicit none
!
! Update inner iteration counter and write headings in the solver
! file.
!
  itera_exm = itera_exm + 1

end subroutine exm_solsys
