subroutine exm_solite
  !-----------------------------------------------------------------------
  !****f* Exmedi/exm_solite
  ! NAME 
  !    exm_solite
  ! DESCRIPTION
  !    This routine is the bridge to the different iteration solution schemes
  ! USES
  !    exm_...
  ! USED BY
  !    exm_doiter
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain

  use      def_exmedi

  implicit none
  integer(ip) :: imate,kmodel
  !
  ! Update inner iteration counter and write headings in the solver file.
  !
  itinn(modul) = itinn(modul) + 1

  if (kfl_paral/=0) then

     !  2. compute iap^n+1 (iap^n  , eap^n, rcp^n)  -->> iteratively, 
     !     to cope with the non-linear FHN ionic current 

     call exm_iapupd                                ! <---- unknown elmag(1,..): INTRA

     !  3. compute eap^n+1 (iap^n+1, eap^n,      )  -->> by solving 
     !     a poisson equation when bidomain models are active

     if (kfl_cemod_exm == 2) call exm_eapupd        ! <---- unknown elmag(2,..): EXTRA


     if ( kfl_gemod_exm == 1) then

        !
        ! check if any TT-like model is present
        !
        kmodel= 0
        do imate= 1,nmate
           kmodel = max(kfl_spmod_exm(imate),0)
        end do

        call exm_updunk(10)
        if (kmodel == 1) then  !Tentu
           !  3.1  Compute concentrations when sub-cell models are used
           call exm_ceconc
           call exm_ceauxi  
        else if (kmodel == 4) then !TT Het
           !  3.1  Compute concentrations when sub-cell models are used
           call exm_conhet  !
           call exm_auxhet            
           !call exm_stthtb
        else if (kmodel == 5) then !O'hara-Rudy
           call exm_conohr
           call exm_auxohr

        else if (kmodel == 6) then !TenTuscher + INaL
           call exm_cttina
           call exm_attina
        end if
        call exm_updunk(8)
     else if ( kfl_gemod_exm == 0) then

        !  4. compute rcp^n+1 (iap^n+1,      , rcp^n)  -->> by an explicit update
        ! Recuperation of the action potential is not done here anymore.
        ! A new subroutine that solves the ode was written, exm_rcpupd is obsolete
        ! call exm_rcpupd                             
     end if

  endif

end subroutine exm_solite
