subroutine exm_reanut
!-----------------------------------------------------------------------
!****f* Exmedi/exm_reanut
! NAME 
!    exm_reanut
! DESCRIPTION
!    This routine reads the numerical treatment 
! USES
!    listen
! USED BY
!    exm_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      mod_memchk

  use      def_exmedi

  implicit none
  integer(ip) :: istab,ipart,rpalo

  if(kfl_paral<=0) then

!
!  Initializations (defaults)
!
     kfl_repro_exm = 0                                ! Res. projection not used 
     kfl_shock_exm = 0                                ! Shock capturing off
     kfl_weigh_exm = 1                                ! dT/dt is in the residual
     kfl_tiacc_exm = 2                                ! 2nd order time integration
     kfl_normc_exm = 2                                ! L2 norm for convergence
     kfl_algso_exm = 8                                ! Algebraic solver is GMRES
     kfl_nolim_exm = 0                                ! No non-linear correction method used
     kfl_nolum_exm = 0                                ! Non-linear terms are not lumped
     cpu_exmed = 0.0_rp                               ! Total/Solver module CPU
     
     miinn_exm     = 1                                ! One internal iteration
     mnoli_exm     = 1                                ! One non-linear iteration (to compute iap)
     staco_exm(1)  = 4.0_rp                           ! Diffusive term
     staco_exm(2)  = 2.0_rp                           ! Convective term
     staco_exm(3)  = 1.0_rp                           ! Reactive term
     shock_exm     = 0.0_rp                           ! SC parameter
     sstol_exm     = 1.0d-5 !F90                      ! Steady-satate tolerance
     safet_exm     = 1.0                              ! Safety factor
     tnoli_exm     = 0.00001_rp                       ! Tolerance for the non-linear intracellular problem
     cotol_exm     = 1.0_rp                           ! Internal tolerance
     resid_exm     = 0.0_rp
     
     dtext_exm     = 0.0_rp                           ! Externally fixed time step
     
     !
     ! Reach the section
     !
     rewind(lisda)
     call ecoute('exm_reanut')
     do while(words(1)/='NUMER')
        call ecoute('exm_reanut')
     end do
     !
     ! Begin to read data
     !
     do while(words(1)/='ENDNU')
        call ecoute('exm_reanut')
        if(     words(1)=='STABI') then
           do istab = 1,3
              staco_exm(istab) = param(istab)
           end do
        else if(words(1).eq.'GENER') then
           if(exists('EXPLI')) kfl_genal_exm = 1
           if(exists('DECOU')) kfl_genal_exm = 2
           if(exists('MONOL')) kfl_genal_exm = 3
        else if(words(1).eq.'NONLI') then
           if(exists('LUMPE')) kfl_nolum_exm = 1
        else if(words(1)=='TYPEO') then
           if(words(2)=='RESID') kfl_repro_exm = 1
        else if(words(1)=='SHOCK') then
           if(exists('ISOTR').or.exists('ON   ')) then
              kfl_shock_exm = 1
              !!           shock_exm     = getrea('VALUE',0.0_rp,'#Shock capturing parameter')   ! deprecated
              shock_exm     = getrea('FACTO',0.0_rp,'#Shock capturing parameter')
          else if(exists('ANISO')) then
              kfl_shock_exm = 2
              !!           shock_exm     = getrea('VALUE',0.0_rp,'#Shock capturing parameter')   ! deprecated
              shock_exm     = getrea('FACTO',0.0_rp,'#Shock capturing parameter')
           end if
           if (exists('STATI')) then
              kfl_shock_exm = 11  ! stationary residual, no transient terms
           end if
        else if(words(1)=='TEMPO') then
           if(exists('GALER')) kfl_weigh_exm = 0
!        else if(words(1)=='TIMEA') then
!           kfl_tiacc_exm = int(param(1))
!           if(kfl_timei_exm==1) kfl_tiacc_exm = 2
        else if(words(1)=='SAFET') then
           safet_exm = param(1)
        else if(words(1)=='STEAD') then
           sstol_exm = param(1)
        else if(words(1)=='NORMO') then
           if(exists('L1   ')) then
              kfl_normc_exm = 1
           else if(exists('L-inf')) then
              kfl_normc_exm = 0
           end if
        else if(words(1)=='MAXIM') then
           miinn_exm = int(param(1))
        else if(words(1)=='CONVE') then
           cotol_exm = param(1)
        else if(words(1)=='NLMET') then
           if (exists('ADRIA')) kfl_nolim_exm =  1
        else if(words(1)=='NLITE') then
           mnoli_exm = int(param(1))
        else if(words(1)=='NLTOL') then
           tnoli_exm = param(1)
        else if(words(1)=='ALGEB') then
           if(exists('DIREC')) kfl_algso_exm =  0
           if(exists('CG   ')) kfl_algso_exm =  1
           if(exists('CGNOR')) kfl_algso_exm =  2
           if(exists('BiCG ')) kfl_algso_exm =  3
           if(exists('BiCGW')) kfl_algso_exm =  4
           if(exists('BiCGS')) kfl_algso_exm =  5
           if(exists('TRANS')) kfl_algso_exm =  6
           if(exists('FULLO')) kfl_algso_exm =  7
           if(exists('GMRES')) kfl_algso_exm =  8
           if(exists('FLEXI')) kfl_algso_exm =  9
           if(exists('QUASI')) kfl_algso_exm = 10
        else if(words(1)=='SOLVE') then
           msoit_exm = int(param(1))
        else if(words(1)=='TOLER') then
           solco_exm = param(1)
        else if(words(1)=='KRYLO') then
           nkryd_exm = int(param(1))
        end if
     end do


     if (dtime >= 0._rp ) dtext_exm    = dtime
     if(kfl_genal_exm==1 .or. kfl_genal_exm==2)  then
        nunkn_exm = npoin
        if (kfl_genal_exm==1) kfl_algso_exm =  -2
     else if(kfl_genal_exm==3)  then
        nunkn_exm = ndofn_exm *  npoin
     end if

     !if (kfl_spmod_exm(imate) == 0) kfl_nolum_exm = 0

  end if
  
end subroutine exm_reanut
