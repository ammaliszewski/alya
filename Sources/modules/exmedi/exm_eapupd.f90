subroutine exm_eapupd

!-----------------------------------------------------------------------
!
! This routine updates the extracellular action potential by solving
! a Poisson equation
!
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solver

  use      def_exmedi

  implicit none
  integer(ip) :: inoli
  real(rp)    :: xnoli,xdeno,xnumi
  real(rp)    :: xmeps,xmchi,xmalp,xmgam,xmccc,xmccm
  real(rp)    :: cpu_refe1,cpu_refe2


!
! Initializations
!
  amatr     = 0.0_rp
  rhsid     = 0.0_rp
  cpu_solve = 0.0_rp
  call cputim(cpu_refe1)
  
  itera_exm     = 0
  
  !
  ! Set up the solver parameters
  !
  
  call exm_eapsol
  
  !
  ! RHS and matrix construction
  
  call exm_eapelm
  
  ! Assign    unkno <-- U(e,i,1)
  unkno(  1:npoin  ) = elmag(2,1:npoin,1)
  
  ! Solve the system     
  call exm_solsys
  
  ! Update  U(e,i,2) <-- unkno     
  elmag(2,1:npoin,2) = unkno(  1:npoin  ) 
  

  call cputim(cpu_refe2)
  cpu_exmed(1) = cpu_exmed(1) + cpu_refe2 - cpu_refe1 
  cpu_exmed(2) = cpu_exmed(2) + cpu_solve
  

end subroutine exm_eapupd
