subroutine exm_ionicunew(ipoin,xgate,xlian,xpion,xpfhn,xpfen,xpten)

!-----------------------------------------------------------------------
!
! This routine computes the ionic current contribution
!
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solver

  use      def_exmedi

  implicit none
  integer(ip) :: ipoin,kmodel
  real(rp)    :: xmeps, xchic ,xmalp,xmgam,taufi,xhea1,xgate(ngate_exm),xpfhn,xpfen,xpten,xpion,xlian
  real(rp)    :: tauro, tausi, tauso, phics, gephi, factk, xhea2, taude, xpio1, xpio2, xpio3, xauxi(2), argth   ! Variables used in FK
  real(rp)    :: xpovl

  xpfhn = 1.0_rp
  xpfen = 0.0_rp
  xpten = 0.0_rp



  kmodel= kfl_spmod_exm(nomat_exm(ipoin))


  if (kmodel == 0) then                       !no model
     xmeps = 0.0_rp
     xmalp = 0.0_rp
     xmgam = 0.0_rp
     taufi = 0.0_rp
     xpion = 0.0_rp

  else if (kmodel == 11) then          !fitzhugh nagumo
     
     xmeps = xmopa_exm( 1,nomat_exm(ipoin))           ! epsilon for the rcp
     xchic = xmopa_exm( 2,nomat_exm(ipoin))
     xmalp = xmopa_exm( 3,nomat_exm(ipoin))           ! alpha for the Iion
     xmgam = xmopa_exm( 4,nomat_exm(ipoin))           ! gamma for the rcp
     taufi = xmopa_exm( 5,nomat_exm(ipoin))           ! C for the Iion


     xpion =        - taufi * xlian * (xlian - 1.0_rp) * (xlian - xmalp)

     xpfhn = 1.0_rp + taufi * (xlian - 1.0_rp) * (xlian - xmalp) * dtime / xmccm_exm


  else if (kmodel == 12) then          !fenton-karma
     call runend('EXM_IONICUnew: FENTON-KARMA MODEL PROGRAMMED BUT NOT UPDATED')
     xmalp = xmopa_exm( 3,1)           ! phi c
     tauro = xmopa_exm( 9,1)               
     tausi = xmopa_exm( 10,1) 
     tauso = xmopa_exm( 11,1)
     phics = xmopa_exm( 12,1)
     gephi = xmopa_exm( 13,1) 
     factk = xmopa_exm( 16,1)

     xhea1 =   heavis(xlian,xmalp)
     xhea2 =   heavis(xmalp,xlian)
     taude =    xmccm_exm / gephi

     !xgate(1) = recfk_exm(ipoin,1)   ! This variable does not exist
     !xgate(2) = recfk_exm(2,ipoin,1) ! and must be defined

     xpio1 =   xgate(1) / taude * xhea1 * (1.0_rp - xlian) * (xlian - xmalp)
     xpio2 = - xlian / tauso * xhea2 - xhea1 / tauro

     argth =   factk * (xlian - phics)

     if (argth > 4) then
        xpio3 = xgate(2) / tausi                  
     else if (argth < - 4) then
        xpio3 = 0.0_rp                 
     else
        xauxi(1) =  exp( argth)
        xauxi(2) =  exp(-argth)
        xpio3    =  xgate(2) *  &                      
             (1.0_rp + (xauxi(1) - xauxi(2)) / (xauxi(1) + xauxi(2))) &
             / 2.0_rp / tausi 
     end if

     xpion = xpio1 + xpio2 + xpio3
     
     xpfen = xpion


  else
     call runend('EXM_IONICUnew: IONIC CURRENT FOR SELECTED MODEL NOT PROGRAMMED')

  end if
  
end subroutine exm_ionicunew
