!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_ceihet.f90
!> @author  Jazmin Aguado-Sierra
!> @brief   Currents calculation for TenTuscher-Panfilov 2006 heterogeneous model
!> @details Calculate every CURRENT at every time step\n
!! The variables are 18:\n
!!   - xina:   I_Na    -->  vicel_exm(1,ipoin,1)  -->  fast current of sodium \n
!!   - xical:  I_CaL   -->  vicel_exm(2,ipoin,1)  -->  current of L-Type calcium \n
!!   - xito:   I_to    -->  vicel_exm(3,ipoin,1)  -->  transient outward current \n
!!   - xiks:   I_Ks    -->  vicel_exm(4,ipoin,1)  -->  slow delayed rectifier current \n
!!   - xikr:   I_Kr    -->  vicel_exm(5,ipoin,1)  -->  rapid delayed rectifier current \n
!!   - xik1:   I_K1    -->  vicel_exm(6,ipoin,1)  -->  inward rectifier K current \n
!!   - xinaca: I_NaCa  -->  vicel_exm(7,ipoin,1)  -->  sodium/calcium exchanger current \n
!!   - xinak:  I_NaK   -->  vicel_exm(8,ipoin,1)  -->  sodium/potassium pump current \n
!!   - xipca:  I_pCa   -->  vicel_exm(9,ipoin,1)  -->  calcium plateau current \n
!!   - xipk:   I_pK    -->  vicel_exm(10,ipoin,1) -->  potassium plateau current \n
!!   - xibna:  I_bNa   -->  vicel_exm(11,ipoin,1) -->  sodium background current \n
!!   - xibca:  I_bCa   -->  vicel_exm(12,ipoin,1) -->  calcium background current \n
!!   - xileak: I_leak  -->  vicel_exm(13,ipoin,1) -->  leak from SR to the cytoplasm \n
!!   - xiup:   I_up    -->  vicel_exm(14,ipoin,1) -->  pump taking up Ca in the SR \n
!!   - xirel:  I_rel   -->  vicel_exm(15,ipoin,1) -->  Ca-induced Ca release (CICR) \n
!!   - xistim: I_stim  -->  vicel_exm(16,ipoin,1) -->  external stimulus current \n
!!   - xiax:   I_ax    -->  vicel_exm(17,ipoin,1) -->  UNUSED IN TTHET\n
!!   - xixfer: I_xfer  -->  vicel_exm(18,ipoin,1) -->  diffusive Ca current between diadic subspace and bulk cytoplasm \n
!! The order is important because it is related with the order of nicel_exm variables (exm_memall.f90):\n
!> @} 
!!-----------------------------------------------------------------------

subroutine exm_ittina

  use      def_parame
  use      def_master
  use      def_elmtyp
  use      def_domain
  use      def_exmedi


! DEFINITION OF VARIABLES
  implicit none
  integer(ip) :: ipoin, iicel
  integer(ip) :: ielem,inode,pnode,pmate,pelty
  real(rp)    :: xina, xical, xito, xiks, xikr, xik1, xinaca, xinak, xipca, xipk
  real(rp)    :: xibna, xibca, xileak, xiup, xirel, xistim, xiax
  real(rp)    :: enapot, ecapot, ekpot, ekspot 
  real(rp)    :: groupcon, xgroupcon, rhsx
  real(rp)    :: vaux1, vaux2, vaux3
  real(rp)   ::  nagmax, dosis1, dosis2, dosis3, dosis4, i50cal, i50kr, i50na, i50nal
  real(rp)   ::  calgmax, gcaldrug, gkrdrug, gnadrug, gnaldrug, gnalmax
  real(rp)   ::  celltem, rgascon, faracon, xtrkcon, xtrnacon, xtrcacon
  real(rp)   ::  togmaxmce, togmaxepi, togmaxend
  real(rp)   ::  ksgmaxend, ksgmaxepi, ksgmaxmce, ksnaperm
  real(rp)   ::  krgmax, k1, k2, k3, k4, dtimeEP
  real(rp)   ::  k1gmax
  real(rp)   ::  nacamax, gamanaca, kmcanaca, kmnanaca, satunaca, alfanaca
  real(rp)   ::  nakmax, kmknak, kmnanak
  real(rp)   ::  pcagmax, kpcapca
  real(rp)   ::  pkgmax
  real(rp)   ::  bnagmax
  real(rp)   ::  bcagmax
  real(rp)   ::  upvmax, upkcon
  real(rp)   ::  leakvmax, aux1
  real(rp)   ::  bufcyto, bufsr, bufss, kbufcyto, kbufsr, kbufss
  
    !!!!  DRUG DEFINITION TO IKR, INA OR ICAL
    dosis1 = ttpar_exm(2,1)
    i50cal = ttpar_exm(2,2)
    dosis2 = ttpar_exm(2,3)
    i50kr = ttpar_exm(2,4)
    dosis3 = ttpar_exm(2,5)
    i50na = ttpar_exm(2,6)
    dosis4 = ttpar_exm(2,7)
    i50nal = ttpar_exm(2,8)
    gcaldrug = 1.0_rp / (1.0_rp + (dosis1/i50cal))
    gkrdrug = 1.0_rp / (1.0_rp + (dosis2/i50kr))
    gnadrug = 1.0_rp / (1.0_rp + (dosis3/i50na))
    gnaldrug = 1.0_rp / (1.0_rp + (dosis4/i50nal))
! CONSTANTS DEFINITION  
    rgascon = 8314.472_rp       ! R [mJ/(K*mol)] --> ideal gas constant
    celltem = 310.0_rp      ! T [K] --> cell system temperature 
    faracon = 96485.3415_rp       ! F [C/mol] --> faraday constant
    xtrkcon = 5.4_rp       ! K_o [mM] --> extracellular concentration of potassium (K+)
    xtrnacon = 140.0_rp !    ! Na_o [mM] --> extracellular concentration of sodium (Na+)
    xtrcacon = 2.0_rp      ! Ca_o [mM] --> extracellular concentration of calcium (Ca++)
    
    nagmax = 14.838_rp * gnadrug       ! G_Na [nS/pF] --> maximal conductance of I_Na current
    gnalmax = 0.0065_rp * gnaldrug
    calgmax = 0.0000398_rp * gcaldrug      ! G_CaL [L/(mF*s)]--> maximal conductance of I_CaL current
    
    togmaxmce = 0.294_rp * ttpar_exm(1,1)    ! G_to_M [nS/pF] --> maximal conductance of I_to current for M Cell
    togmaxepi = 0.294_rp * ttpar_exm(1,1)    ! G_to_epi [nS/pF] --> max conductance of I_to current for epicardium
    togmaxend = 0.073_rp * ttpar_exm(1,1)   ! G_to_end [nS/pF] --> max conductance of I_to current for endocardium
    
    ksgmaxmce = 0.098_rp * ttpar_exm(1,2)    ! G_Ks_M [nS/pF] --> max conductance of I_Ks current for M Cell
    ksgmaxepi = 0.392_rp * ttpar_exm(1,2)     ! G_Ks_epi [nS/pF] --> max conductance of I_Ks current for epicardium
    ksgmaxend = 0.392_rp * ttpar_exm(1,2)     ! G_Ks_end [nS/pF] --> max conductance of I_Ks current for endocardium
    ksnaperm = 0.03_rp      ! p_KNa [adim] --> relative I_Ks current permeability to Na+
    
    krgmax = 0.153_rp * gkrdrug        ! G_Kr [nS/pF] --> max conductance of I_Kr current
    
    k1gmax = 5.405_rp * ttpar_exm(1,3)        ! G_K1 [nS/pF] --> max conductance of I_K1 current
    
    nacamax = 1000.0_rp     ! k_NaCa [pA/pF] --> max I_NaCa current
    gamanaca = 0.35_rp      ! gama [adim] --> voltage dependent parameter of I_NaCa current
    kmcanaca = 1.38_rp      ! K_mCa [mM] --> Cai half-saturation constant for I_NaCa current
    kmnanaca = 87.5_rp     ! K_mNai [mM] --> Nai half-saturation constant for I_NaCa current
    satunaca = 0.1_rp     ! k_sat [adim] --> saturation factor I_NaCa current
    alfanaca = 2.5_rp     ! alfa [adim] --> factor enhancing outward nature of I_NaCa current
    
    nakmax = 2.724_rp       ! P_NaK [pA/pF] --> max I_NaK current
    kmknak = 1.0_rp        ! K_mK [mM] --> K_o half-saturation constant of I_NaK current
    kmnanak = 40.0_rp      ! K_mNa [mM] --> Nai half-saturation constant of I_NaK current
    
    pcagmax = 0.1238_rp     ! G_pCa [pA/pF] --> max conductance of I_pCa current
    kpcapca = 0.0005_rp      ! K_pCa [mM] --> Cai half-saturation constant of I_pCa current
    
    pkgmax = 0.0146_rp      ! G_pK [nS/pF] --> max conductance of I_pK current
    
    bnagmax = 0.00029_rp      ! G_bNa [nS/pF] --> max conductance of I_bNa current
    
    bcagmax = 0.000592_rp      ! G_bCa [nS/pF] --> max conductance of I_bCa current
    
    upvmax = 0.006375_rp      ! V_maxup [mM/ms] --> max I_up current
    upkcon = 0.00025_rp      ! K_up [mM] --> half-saturation constant of I_up current
    
    aux1 = ttpar_exm(1,4)
    leakvmax = 0.00036_rp * aux1     ! V_leak [1/ms] --> max I_leak current
    
    bufcyto = 0.2_rp     ! Buf_c [mM] --> total cytoplasmic buffer concentration
    kbufcyto = 0.001_rp    ! K_Bufc [mM] --> Cai half-saturation constant for cytoplasmic buffer
    bufsr = 10.0_rp        ! Buf_sr [mM] --> total sarcoplasmic buffer concentration
    kbufsr = 0.3_rp       ! K_Bufsr [mM] --> Ca_SR half-saturation constant for sarcoplasmic buffer
    bufss = 0.4_rp        ! Buf_sr [mM] --> total sarcoplasmic buffer concentration
    kbufss = 0.00025_rp       ! K_Bufsr [mM] --> Ca_SR half-saturation constant for sarcoplasmic buffer
    
    groupcon = rgascon * celltem / faracon          ! Group of constants: R * T / F  [mV] 
    xgroupcon = 1.0_rp / groupcon
                
  if(INOTMASTER) then
  
    dtimeEP=dtime * 1000.0_rp

    do ipoin= 1,npoin
       !ipoin= lnods(inode,ielem)     
       ituss_exm = int(celty_exm(1,ipoin))
     ! Fast current of sodium I_Na [pA/pF]
     ! nicel_exm = 1
     ! "A model for ventricular tissue", Ten Tusscher, Page H1585 (col 2)
          enapot = groupcon * log (xtrnacon / vconc(3,ipoin,1))    ! Reversal potential of Na [mV]
          vaux1 = vauxi_exm(1,ipoin,1) * vauxi_exm(1,ipoin,1) * vauxi_exm(1,ipoin,1)
          vaux1 = nagmax * vaux1 * vauxi_exm(2,ipoin,1) * vauxi_exm(3,ipoin,1)
          vaux2 = elmag(1,ipoin,1) - enapot
          xina = vaux1 * vaux2  ! Variable xina
     
          vicel_exm(1,ipoin,3) = xina       ! Value of I_Na current [pA/pF]
     
     
     ! Current of L-Type calcium I_CaL   [pA/pF]
     ! nicel_exm = 2
     ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
     !i_CaL = ((( g_CaL*d*f*f2*fCass*4*(V - 15)*pow(F, 2))/(R*T))*(0.25*Ca_ss*(exp((( 2*(V - 15)*F)/(R*T)))) - Ca_o)) /
     !        ((exp((( 2*(V - 15)*F)/(R*T)))) - 1)
     
          vaux1 = calgmax * vauxi_exm(4,ipoin,1) * vauxi_exm (5,ipoin,1) * vauxi_exm(12,ipoin,1)
          vaux1 = vaux1 * vauxi_exm(6,ipoin,1) * 4.0_rp * faracon * faracon * (elmag(1,ipoin,1) - 15.0_rp)
          vaux1 = vaux1 / (rgascon * celltem)
          vaux2 = exp((2.0_rp * (elmag(1,ipoin,1) - 15.0_rp) * faracon) / (rgascon * celltem))
          vaux3 = (0.25_rp * vconc(5,ipoin,1) * vaux2) - xtrcacon
          vaux3 = vaux1 * vaux3 
          xical = vaux3 / (vaux2 - 1.0_rp) ! Variable xical
     
          vicel_exm(2,ipoin,3) = xical       ! Value of I_CaL current [pA/pF]
     
     ! Transient outward current I_to [pA/pF]
     ! nicel_exm = 3
     ! AND Slow delayed rectifier current I_Ks [pA/pF]
     ! nicel_exm = 4
     !    i_to = g_to*r*s*(V - E_K)
     !    i_Ks = g_Ks*(pow(Xs,2))*(V - E_Ks)
          ekpot = groupcon * log(xtrkcon / vconc(4,ipoin,1))    ! Reversal potential of K [mV]
          
          vaux1 = xtrkcon + (ksnaperm * xtrnacon)
          vaux2 = vconc(4,ipoin,1) + (ksnaperm * vconc(3,ipoin,1))               
          ekspot = groupcon * log( vaux1 / vaux2 )    ! Reversal potential of Ks [mV]
          ! Variable xito
          vaux1 =  vauxi_exm(7,ipoin,1) * vauxi_exm(8,ipoin,1)!
          vaux1 = vaux1 * (elmag(1,ipoin,1) - ekpot)
         ! Variable xiks                    
          vaux2 = vauxi_exm(9,ipoin,1) * vauxi_exm(9,ipoin,1)
          vaux2 = vaux2 * (elmag(1,ipoin,1) - ekspot)
         
          if(ituss_exm == 3) then   !epicardial    
             xito = vaux1 * togmaxepi
             xiks = vaux2 * ksgmaxepi
          else if(ituss_exm == 1) then !endocardial
             xito = vaux1 * togmaxend
             xiks = vaux2 * ksgmaxend
          else if(ituss_exm == 2) then  !mid
             xito = vaux1 * togmaxmce
             xiks = vaux2 * ksgmaxmce
          end if
          vicel_exm(3,ipoin,3) = xito  ! Variable xIto
          vicel_exm(4,ipoin,3) = xiks  ! Variable xIks
       
     ! Rapid delayed rectifier current I_Kr [pA/pF]
     ! nicel_exm = 5
     ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
     
          vaux1 = krgmax * sqrt(xtrkcon / 5.4_rp) * vauxi_exm(10,ipoin,1) * vauxi_exm(11,ipoin,1)
          xikr = vaux1 * (elmag(1,ipoin,1) - ekpot)
          vicel_exm(5,ipoin,3) = xikr  ! Value of I_Kr current [pA/pF]
     
     ! Inward rectifier potassium current I_K1 [pA/pF]
     ! nicel_exm = 6
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     !  beta_K1 = ( 3*exp( 0.0002*(V - E_K+100))+(exp(( 0.1*((V - E_K) - 10))))) /
     !   (1+(exp((-0.5*(V - E_K)))))
          vaux1 = 3.0_rp * exp(0.0002_rp * (100.0_rp + elmag(1,ipoin,1) - ekpot))
          vaux1 = vaux1 + exp(0.1_rp * (-10.0_rp + elmag(1,ipoin,1) - ekpot))
          vaux2 = 1.0_rp + exp(-0.5_rp * (elmag(1,ipoin,1) - ekpot))
          vaux2 = 1.0_rp / vaux2
          vaux1 = vaux1 * vaux2                  ! Value of beta_K1
          vaux2 = 1.0_rp + exp(0.06_rp * (elmag(1,ipoin,1) - ekpot - 200.0_rp))
          vaux2 = 0.1_rp / vaux2                 ! Value of alpha_K1
          vaux3 = vaux2 / (vaux1 + vaux2)        ! Value of K1_inf
     !   i_K1 = g_K1*xK1_inf* pow((K_o/5.4), (1/2))*(V - E_K)     
          vaux1 = sqrt(xtrkcon / 5.4_rp) * vaux3 * (elmag(1,ipoin,1) - ekpot)
          xik1 = k1gmax * vaux1
          vicel_exm(6,ipoin,3) = xik1       ! Value of I_K1 current [pA/pF]
     
     ! Sodium/calcium exchanger current I_NaCa [pA/pF]
     ! nicel_exm = 7
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1) 
     !     xinaca = ( K_NaCa.*( (exp((( gamma_NaCa.*V.*F)./( R.*T)))).*(Nai .^ 3.0).*Cao -  (exp((( (gamma_NaCa - 1.0).*V.*F)./( R.*T)))).
     !   *(Nao .^ 3.0).*Cai.*alpha_NaCa))./( ((Km_Nai .^ 3.0)+(Nao .^ 3.0)).*(Km_Ca+Cao).*(1.0+ K_sat.*(exp((( (gamma_NaCa - 1.0).*V.*F)./( R.*T))))));
     
          vaux1 = exp((gamanaca-1.0_rp) * elmag(1,ipoin,1) * xgroupcon)
          vaux2 = (kmnanaca * kmnanaca * kmnanaca) + (xtrnacon * xtrnacon * xtrnacon)
          vaux2 = vaux2 * (kmcanaca + xtrcacon)
          vaux2 = vaux2 * (1.0_rp + (satunaca * vaux1))     ! current denominator 
          vaux2 = 1.0_rp / vaux2                          ! inverse of current denominator
     
          vaux3 = -vaux1 * xtrnacon * xtrnacon * xtrnacon * vconc(1,ipoin,1) * alfanaca         
          vaux1 = exp(gamanaca * elmag(1,ipoin,1) * xgroupcon)
          vaux3 = vaux3 + (vaux1 * vconc(3,ipoin,1) * vconc(3,ipoin,1) * vconc(3,ipoin,1) * xtrcacon)          ! current numerator
     
          xinaca = nacamax * vaux3 * vaux2 
     
          vicel_exm(7,ipoin,3) = xinaca       ! Value of I_NaCa current [pA/pF]
     
     ! Sodium/potassium pump current I_NaK [pA/pF]
     ! nicel_exm = 8
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     !i_NaK = (((( P_NaK*K_o)/(K_o+K_mk))*Na_i)/(Na_i+K_mNa))/
     !        (1+ 0.1245*(exp(((-0.1*V*F)/( R*T))))+ 0.0353*(exp(((-V*F)/( R*T))))) 
          vaux1 = ((nakmax * xtrkcon) / (xtrkcon + kmknak)) * vconc(3,ipoin,1) 
          vaux1 = vaux1 / (vconc(3,ipoin,1) + kmnanak)             ! current denominator 
          vaux2 = 1.0_rp + 0.1245_rp * exp(-0.1_rp * elmag(1,ipoin,1) * xgroupcon)
          vaux2 = vaux2 + 0.0353_rp * exp(-elmag(1,ipoin,1) * xgroupcon)
          xinak =  vaux1 / vaux2 
     
          vicel_exm(8,ipoin,3) = xinak       ! Value of I_NaK current [pA/pF]
     
     ! Calcium plateau current I_pCa [pA/pF]
     ! nicel_exm = 9
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
      
          vaux1 = kpcapca + vconc(1,ipoin,1)
          vaux1 = 1.0_rp / vaux1             ! inverse of current denominator
          xipca = pcagmax * vaux1 * vconc(1,ipoin,1)
     
          vicel_exm(9,ipoin,3) = xipca       ! Value of I_pCa current [pA/pF]
     
     ! Potassium plateau current I_pK [pA/pF]
     ! nicel_exm = 10
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     !    i_p_K = (g_pK*(V - E_K))/(1+(exp(((25 - V)/5.98))))
          vaux1 = 1.0_rp + exp((25.0_rp - elmag(1,ipoin,1)) / 5.98_rp)
          vaux1 = 1.0_rp / vaux1             ! inverse of current denominator
          xipk = pkgmax * vaux1 * (elmag(1,ipoin,1) - ekpot)
     
          vicel_exm(10,ipoin,3) = xipk       ! Value of I_pK current [pA/pF]
     
      ! Sodium background current I_bNa [pA/pF]
     ! nicel_exm = 11
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
      
          xibna = bnagmax * (elmag(1,ipoin,1) - enapot)
     
          vicel_exm(11,ipoin,3) = xibna      ! Value of I_bNa current [pA/pF]
     
     ! Calcium background current I_bCa [pA/pF]
     ! nicel_exm = 12
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     
          ecapot = 0.50_rp * groupcon  * log (xtrcacon / vconc(1,ipoin,1))    ! Reversal potential of Ca [mV]
          xibca = bcagmax * (elmag(1,ipoin,1) - ecapot)
     
          vicel_exm(12,ipoin,3) = xibca      ! Value of I_bCa current [pA/pF]
     
     ! Leak current from sarcoplasmic reticulum (SR) to the cytoplasm I_leak [mM/ms]
     ! nicel_exm = 13
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     
          xileak = leakvmax * (vconc(2,ipoin,1) - vconc(1,ipoin,1))
     
          vicel_exm(13,ipoin,3) = xileak      ! Value of I_leak current [mM/ms]
     
     ! Pump current taking up calcium in the sarcoplasmic reticulum (SR) I_up [mM/ms]
     ! nicel_exm = 14
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     ! i_up = Vmax_up/(1+pow(K_up, 2)/pow(Ca_i, 2)) 
          vaux1 = vconc(1,ipoin,1) * vconc(1,ipoin,1)
          vaux1 = 1.0_rp / vaux1
          vaux2 = 1.0_rp + (upkcon * upkcon * vaux1)
          vaux2 = 1.0_rp / vaux2            ! inverse of current denominator
          xiup = upvmax * vaux2
     
          vicel_exm(14,ipoin,3) = xiup      ! Value of I_up current [mM/ms]
     
     ! Calcium-induced calcium release (CICR) current I_rel [mM/ms]
     ! nicel_exm = 15
     ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
     
          xirel = 0.102_rp * vconc(10,ipoin,1) * (vconc(2,ipoin,1) - vconc(5,ipoin,1))  
     
          vicel_exm(15,ipoin,3) = xirel      ! Value of I_rel current [mM/ms]
     
     ! External stimulus current I_stim [pA/pF]
     ! nicel_exm = 16
     ! "A model for ventricular tissue", Ten Tusscher, Page H1581
     
          xistim = appfi_exm(ipoin,1)
          vicel_exm(16,ipoin,3) = xistim      ! Value of I_stim current [pA/pF]
     
     
     ! Late INaL+ current, introduced from "Simulation of Brugada Syndrome" Physiol Meas 2006.
          
          vaux1 = vauxi_exm(13,ipoin,1) * vauxi_exm(13,ipoin,1) * vauxi_exm(13,ipoin,1) * vauxi_exm(14,ipoin,1)
          vaux2 = gnalmax * (elmag(1,ipoin,1) - enapot)
          vicel_exm(17,ipoin,3) = vaux1 * vaux2      ! Value of I_NAL current [pA/pF]
           
     !Variable xixfer
          vicel_exm(18,ipoin,3) = 0.0038_rp * (vconc(5,ipoin,1) - vconc(1,ipoin,1))
          
          !!  VOLTAGE INTEGRATION  t2 is the used one, t is the new
          elmag(1,ipoin,2) = 0.0_rp

          do iicel=1,12
             elmag(1,ipoin,2)= elmag(1,ipoin,2) + vicel_exm(iicel,ipoin,3)
          end do
          elmag(1,ipoin,2) =  -(elmag(1,ipoin,2) + vicel_exm(16,ipoin,3) + vicel_exm(17,ipoin,3))
     !!  Runge Kutta
          k1 = elmag(1,ipoin,2) 
          k2 = elmag(1,ipoin,2) + 0.5_rp * dtimeEP * k1
          k3 = elmag(1,ipoin,2) + 0.5_rp * dtimeEP * k2 
          k4 = elmag(1,ipoin,2) + dtimeEP * k3 
          elmag(1,ipoin,2) = elmag(1,ipoin,1) + ((dtimeEP/6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
                           
     end do
 end if

end subroutine exm_ittina
