!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_auxohr.f90
!> @author  Jazmin Aguado-Sierra
!> @brief   Gating variables of Ohara-Rudy 2011 human heterogeneous model
!> @details  Calculate every gating variable at every time step\n
!! The gating variables are 29: (second number is the correspondence in MATLAB) \n
!!  m=vauxi_exm(1,ipoin,1:3)10) \n
!!  hf=vauxi_exm(2,ipoin,1:3)11) \n
!!  hs=vauxi_exm(3,ipoin,1:3)12) \n
!!  j=vauxi_exm(4,ipoin,1:3)13) \n
!!  hsp=vauxi_exm(5,ipoin,1:3)14) \n
!!  jp=vauxi_exm(6,ipoin,1:3)15) \n
!!  ml=vauxi_exm(7,ipoin,1:3)16) \n
!!  hl=vauxi_exm(8,ipoin,1:3)17) \n
!!  hlp=vauxi_exm(9,ipoin,1:3)18) \n
!!  a=vauxi_exm(10,ipoin,1:3)19) \n
!!  if=vauxi_exm(11,ipoin,1:3)20) \n
!!  is=vauxi_exm(12,ipoin,1:3)21) \n
!!  ap=vauxi_exm(13,ipoin,1:3)22) \n
!!  ifp=vauxi_exm(14,ipoin,1:3)23) \n
!!  isp=vauxi_exm(15,ipoin,1:3)24) \n
!!  d=vauxi_exm(16,ipoin,1:3)25) \n
!!  ff=vauxi_exm(17,ipoin,1:3)26) \n
!!  fs=vauxi_exm(18,ipoin,1:3)27) \n
!!  fcaf=vauxi_exm(19,ipoin,1:3)28) \n
!!  fcas=vauxi_exm(20,ipoin,1:3)29) \n
!!  jca=vauxi_exm(21,ipoin,1:3)30) \n
!!  nca=vauxi_exm(22,ipoin,1:3)31) \n
!!  ffp=vauxi_exm(23,ipoin,1:3)32) \n
!!  fcafp=vauxi_exm(24,ipoin,1:3)33) \n
!!  xrf=vauxi_exm(25,ipoin,1:3)34) \n
!!  xrs=vauxi_exm(26,ipoin,1:3)35) \n
!!  xs1=vauxi_exm(27,ipoin,1:3)36) \n
!!  xs2=vauxi_exm(28,ipoin,1:3)37) \n
!!  xk1=vauxi_exm(29,ipoin,1:3)38) \n
!> @} 
!-----------------------------------------------------------------------
subroutine exm_auxohr

  use      def_parame
  use      def_master
  use      def_elmtyp
  use      def_domain
  use      def_exmedi

  implicit none

  integer(ip) :: ipoin,kfl_odets_exm
  integer(ip) :: ielem,inode,pnode,pmate
  real(rp)    :: vinf, xitaux, vaux0, vaux1, vaux2, vaux3, vaux4, vaux5 
  real(rp)    ::  vffrt, vfrt, ena, ek, eks
  real(rp)    :: kmcamk, dtimeEP
  real(rp)    :: jss, ths, tm
  real(rp)    :: delepi, ta, devel, recov, fss
  real(rp)    :: tff, tfcaf, ko
  real(rp)    :: pkna, rgas, farad, iss, nao, temp, tif, tis, tj

  nao = 140.0_rp
  ko = 5.4_rp
  !physical constants
  rgas = 8314.0_rp
  temp = 310.0_rp
  farad = 96485.0_rp 
  pkna = 0.01833_rp
  !%camk constant
  kmcamk=0.15_rp

  kfl_odets_exm = 1
  dtimeEP=dtime *1000.0_rp

  if(INOTMASTER) then
    do ipoin= 1,npoin  
       ituss_exm = int(celty_exm(1,ipoin))
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        !%reversal potentials
        ena=(rgas*temp/farad)*log(nao/vconc(1,ipoin,2))
        ek=(rgas*temp/farad)*log(ko/vconc(3,ipoin,2))
        eks=(rgas*temp/farad)*log((ko+pkna*nao)/(vconc(3,ipoin,2)+pkna*vconc(1,ipoin,2)))
   
        !%convenient shorthand calculations
        vffrt=elmag(1,ipoin,1)*farad*farad/(rgas*temp)
        vfrt=elmag(1,ipoin,1)*farad/(rgas*temp)
  
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
        !calculate ina
        vinf = exp(-(elmag(1,ipoin,1) + 39.57_rp) / 9.871_rp)
        vinf = 1.0_rp / (1.0_rp + vinf)
        vaux1 = 8.552_rp * exp(-(elmag(1,ipoin,1) + 77.42_rp) / 5.955_rp)
        xitaux = vaux1 + (6.765_rp * exp((elmag(1,ipoin,1) + 11.64_rp) / 34.77_rp))
        tm = 1.0_rp/ xitaux
        xitaux = 1.0_rp / tm
   
        vaux0 = vauxi_exm(1,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(1,ipoin,1) = vaux3      ! value of variable m
   
   !!!!!!! hf
        vinf = exp((elmag(1,ipoin,1) + 82.90_rp) / 6.086_rp)
        vinf = 1.0_rp / (1.0_rp + vinf)
        vaux1 = exp(-(elmag(1,ipoin,1) + 1.196_rp) / 6.285_rp)
        vaux2 = exp((elmag(1,ipoin,1) + 0.5096_rp) / 20.27_rp)
        xitaux = (0.00001432_rp * vaux1) + (6.149_rp * vaux2)
        xitaux = 1.0_rp / xitaux
        !ahf=0.99_rp
        !ahs=1.0_rp-ahf
   
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(2,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(2,ipoin,1) = vaux3      ! value of variable hf
   
        jss = vinf    
   !!!! hs
        vaux1 = exp(-(elmag(1,ipoin,1) + 17.95_rp) / 28.05_rp)
        vaux2 = exp((elmag(1,ipoin,1) + 5.730_rp) / 56.66_rp)
        xitaux = (0.009794_rp * vaux1) + (0.3343_rp * vaux2) 
        ths = 1.0_rp / xitaux
        xitaux = 1.0_rp / ths
   
        vaux0 = vauxi_exm(3,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(3,ipoin,1) = vaux3      ! value of variable hf    
   
        !h=ahf*vauxi_exm(2,ipoin,1)+ahs*vauxi_exm(3,ipoin,1)
   
   !!!!!  j
        vaux1 = exp(-(elmag(1,ipoin,1) + 100.6_rp) / 8.281_rp)
        vaux2 = exp((elmag(1,ipoin,1) + 0.9941_rp) / 38.45_rp)
        xitaux = (0.02136_rp * vaux1) + (0.3052_rp * vaux2)
        xitaux = 2.038_rp + (1.0_rp / xitaux)
        xitaux = 1.0_rp / xitaux
        tj = xitaux
        vaux0 = vauxi_exm(4,ipoin,3)  
        vaux1 = (jss - vaux0) * xitaux;
        vaux2 = (jss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (jss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (jss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = jss - (jss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(4,ipoin,1) = vaux3      ! value of variable j       
   
   !!!!  hsp
        vinf = 1.0_rp / (1.0_rp + exp((elmag(1,ipoin,1) + 89.1_rp) / 6.086_rp))
        xitaux = 3.0_rp * ths
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(5,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(5,ipoin,1) = vaux3      ! value of variable hsp         
   
   !!!!!  hp
        !hp=ahf*vauxi_exm(2,ipoin,1)+ahs*vauxi_exm(5,ipoin,1)
   
   !!! jp  
        xitaux = 1.46_rp * tj
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(6,ipoin,3)  
        vaux1 = (jss - vaux0) * xitaux;
        vaux2 = (jss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (jss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (jss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = jss - (jss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(6,ipoin,1) = vaux3      ! value of variable jp             
   
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
        !%calculate inal
   
   !!!!!!  ml
        vinf = 1.0_rp + exp(-(elmag(1,ipoin,1) + 42.85_rp) / 5.264_rp)
        vinf = 1.0_rp / vinf
   
        xitaux = 1.0_rp / tm
   
        vaux0 = vauxi_exm(7,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(7,ipoin,1) = vaux3      ! value of variable ml   
   
   !!!!! hl
        vinf = 1.0_rp + exp((elmag(1,ipoin,1) + 87.61_rp) / 7.488_rp)
        vinf = 1.0_rp / vinf
        xitaux = 1.0_rp / 200.0_rp
   
        vaux0 = vauxi_exm(8,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(8,ipoin,1) = vaux3      ! value of variable hl      
   
   !!!!  hlp
        vinf = 1.0_rp + exp((elmag(1,ipoin,1) + 93.81_rp) / 7.488_rp)
        vinf = 1.0_rp / vinf
        xitaux = 1.0_rp / (3.0_rp * 200.0_rp)
   
        vaux0 = vauxi_exm(9,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(9,ipoin,1) = vaux3      ! value of variable hlp          
   
   !!!!!!!!!!!!!!!!!!!!  calculate ito
        !%calculate ito
   !!!!!!  calculate variable a
        vinf = 1.0_rp + exp(-(elmag(1,ipoin,1) - 14.34_rp) / 14.82_rp)
        vinf = 1.0_rp / vinf
        vaux1 = 1.2089_rp * (1.0_rp + exp(-(elmag(1,ipoin,1) - 18.4099_rp) / 29.3814_rp))
        vaux2 = 3.5_rp / (1.0_rp + exp((elmag(1,ipoin,1) + 100.0_rp) / 29.3814_rp))
        xitaux =  1.0_rp / (vaux1 + vaux2)
        ta = 1.0515_rp / xitaux
   
        xitaux = 1.0_rp / ta
   
        vaux0 = vauxi_exm(10,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(10,ipoin,1) = vaux3      ! value of variable a
   
   !!!!! calculate ifast  
        vinf = 1.0_rp + exp((elmag(1,ipoin,1) + 43.94_rp) / 5.711_rp)
        iss = 1.0_rp / vinf
        if(ituss_exm == 3) then  !epicardium
           delepi = (1.0_rp + exp((elmag(1,ipoin,1) + 70.0_rp) / 5.0_rp))
           delepi = (0.95_rp / delepi)
           delepi = 1.0_rp - delepi
        else
           delepi = 1.0_rp
        end if
   
        vaux1 = 0.08004_rp * exp((elmag(1,ipoin,1) + 50.0_rp) / 16.59_rp)
        vaux2 = 0.3933_rp * exp(-(elmag(1,ipoin,1) + 100.0_rp) / 100.0_rp)
        vaux3 = vaux1 + vaux2
        xitaux = 4.562_rp + (1.0_rp/ vaux3 )
        xitaux = delepi * xitaux
        tif = xitaux
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(11,ipoin,3)  
        vaux1 = (iss - vaux0) * xitaux;
        vaux2 = (iss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (iss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (iss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = iss - (iss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(11,ipoin,1) = vaux3      ! value of variable ifast
   
   !!!!!!  calculate islow
        vaux1 = 0.001416_rp * exp(-(elmag(1,ipoin,1) + 96.52_rp) / 59.05_rp)
        vaux2 = 0.00000001780_rp * exp((elmag(1,ipoin,1) + 114.1_rp) / 8.079_rp)
        vaux3 = vaux1 + vaux2
        xitaux = 23.62_rp + (1.0_rp / vaux3)
        xitaux = delepi * xitaux
        tis = xitaux
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(12,ipoin,3)  
        vaux1 = (iss - vaux0) * xitaux;
        vaux2 = (iss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (iss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (iss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = iss - (iss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(12,ipoin,1) = vaux3      ! value of variable islow    
   
   !!!!! calculate aif, ais and i
        !aif = 1.0_rp/(1.0_rp+exp((elmag(1,ipoin,1)-213.6_rp)/151.2_rp))
        !ais = 1.0_rp - aif
        !i = aif*vauxi_exm(11,ipoin,1) + ais*vauxi_exm(12,ipoin,1)
   
   !!!!  calculate ap
        vinf = 1.0_rp + exp(-(elmag(1,ipoin,1)-24.34_rp)/14.82_rp)
        vinf = 1.0_rp / vinf
   
        xitaux = 1.0_rp / ta
   
        vaux0 = vauxi_exm(13,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(13,ipoin,1) = vaux3      ! value of variable ap   
   
   !!!!!!!!!! calculate ifp
        vaux1 = exp((elmag(1,ipoin,1)-167.4_rp)/15.89_rp)
        vaux2 = exp(-(elmag(1,ipoin,1)-12.23_rp)/0.2154_rp)
        devel =  vaux1 + vaux2 
        devel = 1.354_rp + (0.0001 / devel)
   
        vaux1 = exp((elmag(1,ipoin,1)+70.0_rp)/20.0_rp)
        vaux1 = 1.0_rp + vaux1
        recov = 1.0_rp - (0.5_rp / vaux1)
        xitaux = devel * recov * tif
   
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(14,ipoin,3)  
        vaux1 = (iss - vaux0) * xitaux;
        vaux2 = (iss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (iss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (iss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = iss - (iss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(14,ipoin,1) = vaux3      ! value of variable ifp
   
   !!!!! calculate isp
        xitaux = devel * recov * tis    
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(15,ipoin,3)  
        vaux1 = (iss - vaux0) * xitaux;
        vaux2 = (iss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (iss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (iss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        !vaux3 = iss - (iss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(15,ipoin,1) = vaux3      ! value of variable isp      
   
        !%calculate ical, icana, icak
   !!!!  calculate gate d
        vaux1 = exp(-(elmag(1,ipoin,1) + 3.940_rp) / 4.230_rp)
        vinf = 1.0_rp / (1.0_rp + vaux1)
        vaux1 =  exp(0.09_rp*(elmag(1,ipoin,1) + 14.0_rp))
        vaux2 = exp(-0.05_rp*(elmag(1,ipoin,1) + 6.0_rp))
        xitaux = vaux1 + vaux2
        xitaux = 0.6_rp + (1.0_rp/xitaux)
   
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(16,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(16,ipoin,1) = vaux3      ! value of variable d
   
   !!!! calculate gate ff
        vaux1 = exp((elmag(1,ipoin,1) + 19.58_rp) / 3.696_rp)
        vinf = 1.0_rp/(1.0_rp + vaux1)
        fss = vinf
        vaux1 = 0.0045_rp * exp((elmag(1,ipoin,1) + 20.0_rp) / 10.0_rp)
        vaux2 = 0.0045_rp * exp(-(elmag(1,ipoin,1) + 20.0_rp) / 10.0_rp) 
        xitaux = 7.0_rp + (1.0_rp / (vaux1 + vaux2))
        tff = xitaux
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(17,ipoin,3)  
        vaux1 = (fss - vaux0) * xitaux;
        vaux2 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (fss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = fss - (fss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(17,ipoin,1) = vaux3      ! value of variable ff
   
   !!!!!!  calculate gate fs
        vaux1 =  0.000035_rp * exp((elmag(1,ipoin,1)+5.0_rp)/6.0_rp)
        vaux2 = 0.000035_rp * exp(-(elmag(1,ipoin,1)+5.0_rp)/4.0_rp) 
        xitaux = 1000.0_rp + (1.0_rp / (vaux1 + vaux2))
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(18,ipoin,3)  
        vaux1 = (fss - vaux0) * xitaux;
        vaux2 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (fss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = fss - (fss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(18,ipoin,1) = vaux3      ! value of variable fs
   
   !!!!!!! calculate f
        !aff=0.6_rp
        !afs=1.0_rp-aff    
        !f = (aff * vauxi_exm(17,ipoin,1)) + (afs * vauxi_exm(18,ipoin,1))   !value of gate f
   
   !!!!! calculate fcass
        !!fcass=fss
        vaux1 = (0.04_rp*exp((elmag(1,ipoin,1)-4.0_rp)/7.0_rp))
        vaux2 = (0.04_rp*exp(-(elmag(1,ipoin,1)-4.0_rp)/7.0_rp))  
        xitaux = 7.0_rp + (1.0_rp / (vaux1 + vaux2))
        tfcaf = xitaux
        xitaux = 1.0_rp / tfcaf
   
        vaux0 = vauxi_exm(19,ipoin,3)  
        vaux1 = (fss - vaux0) * xitaux;
        vaux2 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (fss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = fss - (fss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(19,ipoin,1) = vaux3      ! value of variable fcaf   
   
   !!!! calculate gate fcas
        vaux1 = (0.00012_rp * exp(elmag(1,ipoin,1)/7.0_rp))
        vaux2 = (0.00012_rp * exp(-elmag(1,ipoin,1)/3.0_rp))  
        xitaux = 100.0_rp + (1.0_rp / (vaux1 + vaux2))
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(20,ipoin,3)  
        vaux1 = (fss - vaux0) * xitaux;
        vaux2 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (fss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = fss - (fss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(20,ipoin,1) = vaux3      ! value of variable fcas       
   
   !!! calculate gate jca
        xitaux = 1.0_rp / 75.0_rp
   
        vaux0 = vauxi_exm(21,ipoin,3)  
        vaux1 = (fss - vaux0) * xitaux;
        vaux2 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (fss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = fss - (fss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(21,ipoin,1) = vaux3      ! value of variable jca  
   
   !!! calculate gate ffp 
        xitaux = 2.5_rp * tff
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(23,ipoin,3)  
        vaux1 = (fss - vaux0) * xitaux;
        vaux2 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (fss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = fss - (fss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(23,ipoin,1) = vaux3      ! value of variable ffp   
   
   !!!!!! calculate fp
        !fp = aff*vauxi_exm(23,ipoin,1) + afs*vauxi_exm(18,ipoin,1)
   !!!! calculate gate fcafp
        xitaux = 2.5_rp * tfcaf
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(24,ipoin,3)  
        vaux1 = (fss - vaux0) * xitaux;
        vaux2 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (fss - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (fss - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = fss - (fss - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(24,ipoin,1) = vaux3      ! value of variable ffp   
   
   !!!!!  calculate fcap
   
        !%calculate ikr
        vinf = exp(-(elmag(1,ipoin,1) + 8.337_rp) / 6.789_rp)
        vinf = 1.0_rp / (1.0_rp + vinf)
        vaux1 = 0.00004123_rp * exp(-(elmag(1,ipoin,1) - 47.78_rp) / 20.38_rp)
        vaux2 = 0.3652_rp * exp((elmag(1,ipoin,1) - 31.66_rp) / 3.869_rp) 
        xitaux = 12.98_rp + (1.0_rp / (vaux1 + vaux2))
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(25,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(25,ipoin,1) = vaux3      ! value of variable xrf 
   
        vaux1 = 0.06629_rp * exp((elmag(1,ipoin,1) - 34.70_rp) / 7.355_rp)
        vaux2 = 0.00001128_rp * exp(-(elmag(1,ipoin,1) - 29.74_rp) / 25.94_rp)
        xitaux = 1.865_rp + (1.0_rp / (vaux1 + vaux2))
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(26,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(26,ipoin,1) = vaux3      ! value of variable xrs
   
   !!! %calculate iks
        vinf =  exp(-(elmag(1,ipoin,1) + 11.60_rp)/8.932_rp)
        vinf = 1.0_rp / (1.0_rp + vinf)
        vaux1 = 0.0002326_rp * exp((elmag(1,ipoin,1) + 48.28_rp)/17.80_rp)
        vaux2 = 0.001292_rp * exp(-(elmag(1,ipoin,1) + 210.0_rp)/230.0_rp)
        xitaux = 817.3_rp + (1.0_rp/(vaux1 + vaux2))
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(27,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(27,ipoin,1) = vaux3      ! value of variable xs1
   
        !vinf = xs2ss = xs1ss
        vaux1 = 0.01_rp * exp((elmag(1,ipoin,1) - 50.0_rp) / 20.0_rp)
        vaux2 =  0.0193_rp * exp(-(elmag(1,ipoin,1) + 66.54_rp) / 31.0_rp)
        xitaux = 1.0_rp / (vaux1 + vaux2) 
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(28,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(28,ipoin,1) = vaux3      ! value of variable xs2
   
        !ksca = (0.000038_rp/vconc(5,ipoin,1)) ** 1.4_rp
        !ksca = 1.0_rp + (0.6_rp / (1.0_rp + ksca))
        !gks=0.0034_rp
        !if(ituss_exm == 3) !epicardium
        !    gks=gks*1.4_rp
        !end if
        !vicel_exm(6,ipoin,1) = gks * ksca * vauxi_exm(27,ipoin,1) * vauxi_exm(28,ipoin,1) * (elmag(1,ipoin,1)-eks)
   
   !!!!  calculate gate xk1
        vaux1 = 1.0_rp / (1.5692_rp * ko + 3.8115_rp)
        vaux2 = elmag(1,ipoin,1) + 2.5538_rp*ko + 144.59_rp
        vinf = exp(-vaux2 * vaux1 )
        vinf = 1.0_rp / (1.0_rp + vinf)
        vaux1 = exp(-(elmag(1,ipoin,1) + 127.2_rp) / 20.36_rp)
        vaux2 = exp((elmag(1,ipoin,1) + 236.8_rp) / 69.33_rp)
        xitaux = 122.2_rp / (vaux1 + vaux2)
        !dxk1=(xk1ss-vauxi_exm(29,ipoin,1))/txk1
        xitaux = 1.0_rp / xitaux
   
        vaux0 = vauxi_exm(29,ipoin,3)  
        vaux1 = (vinf - vaux0) * xitaux;
        vaux2 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux1)) * xitaux;
        vaux3 = (vinf - (vaux0 + 0.5_rp * dtimeEP * vaux2)) * xitaux;
        vaux4 = (vinf - (vaux0 + dtimeEP * vaux3)) * xitaux;
        vaux5 = vaux0 + (dtimeEP / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4); 
        vaux3 = vaux5
        !vaux3 = vinf - (vinf - vaux0) * exp(-dtimeEP/xmccm_exm * xitaux)    
        vauxi_exm(29,ipoin,1) = vaux3      ! value of variable xk1    

    end do    
  end if


  !write(995,*) vauxi_exm(:,1,:)

end subroutine exm_auxohr
