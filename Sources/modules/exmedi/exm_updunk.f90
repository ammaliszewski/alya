subroutine exm_updunk(itask)
!-----------------------------------------------------------------------
!****f* Exmedi/exm_updunk
! NAME 
!    exm_updunk
! DESCRIPTION
!    This routine performs several types of updates 
! USED BY
!    exm_begste (itask=1)
!    exm_begite (itask=2)
!    exm_endite (itask=3, inner loop) 
!    exm_endite (itask=4, outer loop) 
!    exm_endste (itask=5)
!***
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!
! This routine performs several types of updates for the unknowns
!
! NO ESTA BIEN ESTA SUBRUTINA.. REVISAR ANTES DE USAR!!!!!!!!!
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_exmedi

  implicit none
  integer(ip), intent(in) :: itask !> case to update
  integer(ip)             :: ipoin,ncomp,idofn,igate,itime
  
  
  
  if(kfl_paral/=0) then

     select case (itask)
        !
        ! Assign U(n,0,*) <-- U(n-1,*,*), initial guess for outer iterations
        !
     case(1)
        ncomp=min(3,ncomp_exm) 
        do ipoin=1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           do idofn=1,ndofn_exm
              elmag(idofn,ipoin,2) = elmag(idofn,ipoin,ncomp)
           end do

           refhn_exm(ipoin,2)= refhn_exm(ipoin,ncomp)

        end do
        !
        ! Assign U(n,i,0) <-- U(n,i-1,*), initial guess for inner iterations
        !
     case(2)
        do ipoin=1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           do idofn=1,ndofn_exm
              elmag(idofn,ipoin,1)     = elmag(idofn,ipoin,2)
           end do
            
           refhn_exm(ipoin,1)=refhn_exm(ipoin,2)

        end do
     case(3)
        !
        ! Assign u(n,i,j-1) <-- u(n,i,j)
        !        
        do ipoin=1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           elmag(1,ipoin,1) = unkno(ipoin)

        end do

     case(4)
        !
        ! Assign U(n,i-1,*) <-- U(n,i,*)
        !        
        do ipoin=1,npoin
           if (.not.(exm_zonenode(ipoin))) cycle           
           elmag(1,ipoin,2) = elmag(1,ipoin,1)
           refhn_exm(ipoin,2)=refhn_exm(ipoin,1)
        end do


     case(5)
       if (kfl_gemod_exm==0) then
         if(kfl_tiacc_exm==2) then
            do itime=2+kfl_tiacc_exm,4,-1
               do ipoin=1,npoin
                 
                  if (.not.(exm_zonenode(ipoin))) cycle
           
                  do idofn=1,ndofn_exm
                     elmag(idofn,ipoin,itime) = elmag(idofn,ipoin,itime-1)
                  end do

                  refhn_exm(ipoin,itime)=refhn_exm(ipoin,itime-1)

               end do
            end do
         end if
         do ipoin=1,npoin
           
            if (.not.(exm_zonenode(ipoin))) cycle
           
            do idofn=1,ndofn_exm
               elmag(idofn,ipoin,3) = elmag(idofn,ipoin,1)
            end do

            refhn_exm(ipoin,3)=refhn_exm(ipoin,1)

         end do
       else       
          do ipoin=1,npoin
           
            if (.not.(exm_zonenode(ipoin))) cycle
           
            do idofn=1,ndofn_exm
               elmag(idofn,ipoin,3) = elmag(idofn,ipoin,1)
            end do
         end do
       end if
     case(6)
        do ipoin=1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           elmag(1,ipoin,3) = elmag(1,ipoin,1)
           elmag(1,ipoin,1) = elmag(1,ipoin,2)
        end do
        !        
        ! Update intracell action potential 
        ! U(1,*,*) <-- U(1,*,*)
        !        
     case(7)
        do ipoin=1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           do idofn=1,ndofn_exm
              elmag(idofn,ipoin,1) = elmag(idofn,ipoin,2)
           end do
    
           refhn_exm(ipoin,1)=refhn_exm(ipoin,2)

        end do
        
     case(8)
        do ipoin=1,npoin
           
           if (.not.(exm_zonenode(ipoin))) cycle
           
           do idofn=1,nauxi_exm
              vauxi_exm(idofn,ipoin,2) = vauxi_exm(idofn,ipoin,1)
              vauxi_exm(idofn,ipoin,3) = vauxi_exm(idofn,ipoin,2)
           end do
           do idofn=1,nconc_exm
               vconc(idofn,ipoin,2) = vconc(idofn,ipoin,1)
               vconc(idofn,ipoin,3) = vconc(idofn,ipoin,2)
           end do

        end do
        
     case(9)
        do ipoin=1,npoin
           if (.not.(exm_zonenode(ipoin))) cycle
           do idofn=1,nicel_exm   
              vicel_exm(idofn,ipoin,1) = vicel_exm(idofn,ipoin,3) 
           end do
           do idofn=1,ndofn_exm   
              elmag(idofn,ipoin,2) = volts_exm(idofn,ipoin,2) * 0.000003335640952_rp
           end do
        end do
     
     case(10)
        do ipoin=1,npoin
           if (.not.(exm_zonenode(ipoin))) cycle
           do idofn=1,ndofn_exm
              volts_exm(idofn,ipoin,2) = elmag(idofn,ipoin,1) * 299792.458000137_rp
              volts_exm(idofn,ipoin,1) = elmag(idofn,ipoin,2) * 299792.458000137_rp              
              elmag(idofn,ipoin,3) = elmag(idofn,ipoin,1)
              elmag(idofn,ipoin,1) = elmag(idofn,ipoin,2)
           end do        
        end do
        
     end select
  
  end if

end subroutine exm_updunk

