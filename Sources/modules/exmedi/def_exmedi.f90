!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    def_exmedi.f90
!> @date    12/03/2013
!> @author  Mariano Vazquez
!> @brief   Def file
!> @details Def file
!> @}
!------------------------------------------------------------------------
module def_exmedi

!-----------------------------------------------------------------------
!    
! Heading for the module subroutines
!
!-----------------------------------------------------------------------
  use def_kintyp
!
! General
!------------------------------------------------------------------------
! Parameters
!------------------------------------------------------------------------
  integer(ip), parameter ::&
      lun_maxmi_exm = 510
  real(rp), parameter :: &
       zeexm = epsilon(0.0_rp)
  integer(ip), parameter ::&                     ! # postprocess variables
       nvarp_exm=60, nvars_exm=20, nvecp_exm=20, nspep_exm=35, nscap_exm=60
  integer(ip), parameter ::&                     ! # max sets
       msets_exm=10

!------------------------------------------------------------------------
! Physical problem: read, modified or defined in exm_reaphy FIRST CALL
!------------------------------------------------------------------------
!--BEGIN REA GROUP
  integer(ip) ::&
       kfl_timei_exm,&            ! Existence of dT/dt
       kfl_gemod_exm,&            ! General model type of ionic 
       !     current  --> 1=subcell (TT, LR, BR, ...) or 0=no-subcell (FHN,...)
       kfl_spmod_exm(20),&        ! Specific model type of subcellular ionic 
       !     current model (1=TT, 2=LR, 3=BR, ...)       
       kfl_voini_exm(20),&        ! Initial voltage flag (per material)
       kfl_fento_exm,&            ! Specific sub model of fenton's model
       kfl_appli_exm,&            ! Existence of applied currents
       kfl_comdi_exm,&            ! Conductivity model distribution (homogeneous, nodal, elemental)
       kfl_stead_exm,&            ! Steady-state has been reached 
       kfl_tiext_exm,&              ! Externally fixed time step
       kfl_cemod_exm,&              ! Cell model (propagation model): monodomain o bidomain
       kfl_ptrig_exm,&              ! Electrical stimuli starting either with time or triggered by pressure
       kfl_hfmod_exm,&              ! Runs the model on Heart Failure mode
       kfl_stree_exm,&              ! Construct a streeter fiber model
       onecl_exm(2),&                 ! Number of beats to steady state and Cycle length
       kfl_drugs_exm,&                ! includes changes due to drug administration
       kfl_heter_exm                ! heterogeneous cell model
!  integer(ip),  pointer ::&

       
  integer(ip) ::         &
       ndofn_exm,      &         ! # of d.o.f. of the problem
       ndof2_exm,      &         ! ndofn_*ndofn_
       nmate_exm,      &         ! # of materials
       nevat_exm,      &         ! Element matrix dim.=(ndime+1)*nnode
       nstim_exm,      &         ! Number of initial stimuli
       nstis_exm,      &         ! If needed, set number of initial stimuli
       modfi_exm,      &         ! Fiber model
       modce_exm,      &         ! Cell type
       ncomp_exm,      &         ! Number of components of the fields
       setno_exm,      &         ! Number of sets to generate streeter fiber field 
       strbo_exm(3)              ! Boundaries for the streeter model


  real(rp) ::&
       dtinv_exm    ,&              ! 1/dt
       xmccm_exm    ,&              ! Chi membrane capacitance
       xmsuv_exm    ,&              ! SURFACE TO VOLUME RATIO FOR DIFUSSION 
       pdccm_exm    ,&              ! Chi membrane capacitance for the PDE
       pdsuv_exm    ,&              ! SURFACE TO VOLUME RATIO FOR DIFUSSION for the PDE
       voini_exm(20),&              ! Initial voltage (per material)
       ceglo_exm(2,2),&             ! global cellular conductances
       cleng_exm    ,&              ! Reference characteristic length       
       apval_exm(  5700),&            ! Applied current intensity  
       aplap_exm(  5700),&            ! Applied current time lapse 
       apcen_exm(3,5700),&            ! Applied current center 
       aprea_exm(  5700),&            ! Reach 
       scond_exm,&                  ! Surface conductivity coefficient
       react_exm,&                  ! Reaction term
       gcond_exm(2,3,20),&             ! Global conductivities
       gfibe_exm(2,3),&             ! Global fiber orientation
       xmopa_exm(20,20),&              ! Physical model parameters
       ttpar_exm(2,8),&           ! parameters to identify normal vs heart failure 1-cell simulations
       poref_exm(2),&               ! Potin and potfi, reference potentials
       xthri_exm,&                  ! Lower threshold for fiber gradient
       xthrs_exm,&                    ! Upper threshold for fiber gradient
       vauin_exm(30,3,3),&               ! initial values of  vauxi for 3D simulation 
       vcoin_exm(12,3,3),&                     ! initial vaules of vconc for 3D simulation
       vmini_exm(2,3), &                     ! new voltage
       ituss_exm ,&                    ! Cell type :  3-Epicardium, 1-endocardium, 2-midmyocardium
       vaulo_exm(30,3),&             ! Auxiliary variables for single cell solution
       vcolo_exm(12,3),&                 ! Concentrations for single cell solution
       viclo_exm(27,3),&              ! Currents for single cell solution
       fiaxe_exm(3),&                ! Ventricular axis
       elmlo_exm(3)                  ! New local voltage
       
  integer(ip) ::&
       ngrou_exm,&                  ! # of cell currents groups
       nauxi_exm,&                  ! # of auxiliary variables
       nconc_exm,&                  ! # of concentration unknowns
       ngate_exm,&                  ! # of activation gate fields
       nicel_exm                  ! # of cell ionic currents

!------------------------------------------------------------------------
! Numerical problem: read, modified or defined in exm_reanut
!------------------------------------------------------------------------
  integer(ip) ::&
       kfl_genal_exm,&              ! General algorithm type 
                                    !    (explicit, decoupled implicit, monolithic implicit, ...)
       kfl_goite_exm,&              ! Keep iterating
       kfl_shock_exm,&              ! Shock capturing type 
       kfl_weigh_exm,&              ! Weighting of dT/dt
       kfl_tiacc_exm,&              ! Temporal accuracy
       kfl_normc_exm,&              ! Norm of convergence
       kfl_algso_exm,&              ! Type of algebraic solver
       kfl_repro_exm,&              ! Stabilization based on residual projection
       kfl_nolim_exm,&              ! Non-linear correction method
       kfl_nolum_exm,&              ! Non-linear terms lumped or not
       kfl_gcoup_exm,&              ! Geometric coupling with SOLIDZ
       miinn_exm,&                  ! Max # of iterations
       mnoli_exm,&                  ! Max # of iterations for the non-linear intracellular problem
       msoit_exm,&                  ! Max # of solver iterations
       nkryd_exm,&                  ! Krylov dimension
       memor_exm(2),&               ! Memory counter
       itera_exm,&                  ! Internal iteration counter
       nunkn_exm

  real(rp) ::&
       dtcri_exm    ,&              ! Critical time step
       shock_exm    ,&              ! Shock capturing parameter
       sstol_exm    ,&              ! Steady state tolerance
       cotol_exm    ,&              ! Convergence tolerance
       dtext_exm    ,&              ! Externally fixed time step
       safet_exm    ,&              ! Safety factor for time step
       solco_exm    ,&              ! Solver tolerance
       weigh_exm,    &              ! Weight of dU/dt in the residual
       tnoli_exm    ,&              ! Tolerance for the non-linear intracellular problem
       resid_exm(10),&              ! Residual for outer iterations
       err01_exm(2),&               ! L1 error T
       err02_exm(2),&               ! L2 error T
       err0i_exm(2),&               ! Linf error T
       err11_exm(2),&               ! L1 error grad(T)
       err12_exm(2),&               ! L2 error grad(T)
       err1i_exm(2),&               ! Linf error grad(T)
       staco_exm(3),&               ! Stability constants
       cpu_exmed(2)                 ! CPU for the EXM problem

!------------------------------------------------------------------------
! Physical problem: read, modified or defined in exm_reaphy SECOND CALL
!------------------------------------------------------------------------

!
! Physical properties used in the model
!
  integer(ip)               ::   kfl_prdef_exm
  integer(ip),  pointer ::&
       nomat_exm(:),      &              ! Per-node material definition
       kgrfi_exm(:)                      ! Large gradient fibers label vector
       
  real(rp),     pointer ::&
       cecon_exm(:,:,:)  ,&             ! Ex/Intracellular Conductivity (point-wise)
       grafi_exm(:,:,:)  ,&             ! Fiber orientation gradient (a tensor)
       fiber_exm(:,:)    ,&             ! Fiber 
       celty_exm(:,:)    ,&             ! Cell types 
!+MRV       
       fibe2_exm(:,:)    ,&
!-MRV
       volts_exm(:,:,:)                 ! ODE voltge in mV not STATVOLTS
!------------------------------------------------------------------------
! Boundary conditions: read, modified or defined  in exm_reabcs
!------------------------------------------------------------------------
!
! Boundary conditions
! 
  integer(ip)           ::   kfl_exboc_exm    ! Boundary conditions explicitly given
  type(bc_nodes), pointer              :: &     
       tncod_exm(:)                          ! Node code type
  type(bc_bound), pointer              :: &     
       tbcod_exm(:)                          ! Boundary code type

!------------------------------------------------------------------------
! Output and Postprocess: read, modified or defined  in exm_reaous
!------------------------------------------------------------------------

  real(rp)                 :: &
       thiso_exm(2)                 ! Isochrones trigger and threshold       
!--END REA GROUP
!------------------------------------------------------------------------
! Derived variables (slave-local in MPI)
!------------------------------------------------------------------------
  logical                  :: &
       iloner,weparal
!
! Physical properties used in the model
!
  real(rp),     pointer ::&
       appfi_exm(:,:)               ! Applied current field 
  
  real(rp)                ::&
       vamxm_exm(2,10),&             ! Max-min values for the variables
       ecgvo_exm(10)               ! Volume integrals used to compute ECG values
       
  integer(ip),  pointer ::&
       lmate_exm(:),&               ! Materials (element-wise)
       kfl_fixno_exm(:,:),&         ! Nodal fixity 
       kfl_fixbo_exm(:)             ! Element boundary fixity
  real(rp),     pointer ::&
       bvess_exm(:,:)               ! Essential bc (or initial) values 
  real(rp),     pointer ::&
       bvnat_exm(:,:,:)             ! Natural bc values
  !
  ! Cell model types parameters and variables
  !
  real(rp),     pointer ::&
       vauxi_exm(:,:,:),&           ! Auxiliary variables
       refhn_exm(:,:)  ,&           ! Recuperation potential (only) for FHN       
       vicel_exm(:,:,:)             ! Cell ionic currents
       

  integer(ip),    pointer ::&
       lapno_exm(:),&                 ! Model materials 2 / applied currents (node-wise)
       kwave_exm(:)                   ! Detect a new depolarization wave
       
!
! Module types
! 
  type modcon_sol
     !
     ! This type defines each of the model constants, for each current
     !
     integer(ip)         :: iacti         ! activation state (0 , 1)
     integer(ip)         :: ituss        ! tissue (1 ENDO, 2 EPI , 3 M Cell)
     real(rp)            :: value(10)     ! parameter value (until 10 parameters)
     character(len=8)    :: iname         ! current name
     character(len=12)   :: vanam(10)     ! parameter name
     
  end type modcon_sol

  type(modcon_sol)       :: cupar_exm(20,20)


contains
!!!!!
  
  function exm_zonenode(ipoin)
    use def_kintyp
    use def_domain
    use def_master
    implicit none

    logical :: exm_zonenode
    integer(ip) :: izone,ipoin

    exm_zonenode = .true.
    izone= lzone(ID_EXMEDI)

    if (nzone > 1) then
 !!      if (lpoiz(izone,ipoin) == 0) exm_zonenode = .false.
    end if

  end function exm_zonenode


  function heavis(a,b)
    use def_kintyp
    implicit none
    real(rp) :: a,b,heavis
    
    heavis = 1.0_rp
    if (a<b) heavis = 0.0_rp
    
    
  end function heavis
 

end module def_exmedi
