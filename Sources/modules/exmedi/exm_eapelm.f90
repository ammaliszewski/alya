!-----------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_eapelm.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Compute elemental matrix and RHS for the bidomain model
!> @details Compute elemental matrix and RHS for the bidomain model
!> @} 
!-----------------------------------------------------------------------
subroutine exm_eapelm
  use      def_master
  use      def_domain
  use      def_exmedi
  implicit none

  real(rp)    :: cartd(ndime,mnode)
  real(rp)    :: hessi(ntens,mnode*mlapl)
  
  real(rp)    :: elmat(mnode,mnode),wmatr(mnode,mnode) ! Element matrices
  real(rp)    :: elrhs(mnode), wrhsi(mnode), wauxi(mnode)
  real(rp)    :: elaux(mnode,2), eliap(mnode,2), elion(mnode,2) 
  real(rp)    :: eleap(mnode,2), elgat(ngate_exm,mnode,2), elcod(ndime,mnode)

  real(rp)    :: cndex(ndime,ndime,mgaus), cndin(ndime,ndime,mgaus) 
  
  real(rp)    :: wmat1(ndime,ndime,mnode*mlapl)        ! Created here to avoid using an 
  real(rp)    :: d2sdx(ndime,ndime,ndime*mlapl)        ! that is called inside do igaus 

  integer(ip) :: ielem,kelem,inode,ipoin,jnode,jpoin         ! Indices and dimensions
  integer(ip) :: igaus,igacn,idime,jdime,pnode,noion
  integer(ip) :: pelty,NCOMP_INTRA_LAST

  integer(ip) :: iposi,jposi,imasl                     ! Assembly
  real(rp)    :: acont,adiag 

  real(rp)    :: dvolu,detjm,dvoti                     ! Values at Gauss points
  real(rp)    :: sphac,conac,difac,temol
  real(rp)    :: xmuit,xmui1,rhodt
  real(rp)    :: xprcp,xpapp,xpfhn,wliap
  real(rp)    :: griap(ndime),grdif(ndime)
  real(rp)    :: xjaci(ndime,ndime),xjacm(ndime,ndime) 

!
! Loop over elements


  call runend('EXM_EAPELM: BIDOMAIN MODELS TO BE REPROGRAMMED')

  NCOMP_INTRA_LAST= 2   ! last computed value of intracell

  elements: do kelem = 1,nelez(lzone(ID_EXMEDI))
     ielem = lelez(lzone(ID_EXMEDI)) % l(kelem)
     
     ! Initialize
     pelty = ltype(ielem)
     pnode = nnode(pelty)
     elmat = 0.0_rp
     elrhs = 0.0_rp

     ! Gather operations
     do inode=1,pnode
        ipoin=lnods(inode,ielem)
        eliap(inode,1)=elmag(1,ipoin,NCOMP_INTRA_LAST)
        eleap(inode,1)=elmag(2,ipoin,1)
        do idime=1,ndime
           elcod(idime,inode)=coord(idime,ipoin)
        end do
     end do

     if(kfl_timei_exm==1) then
        do inode=1,pnode
           ipoin=lnods(inode,ielem)
           eliap(inode,2) = elmag(1,ipoin,3)
           eleap(inode,2) = elmag(2,ipoin,3)
        end do
     end if

     ! Compute extra and intracellular conductivity matrices

!!!     call exm_comcnd(ielem,cndex,cndin,noion)

     igacn = 1
     gauss_points: do igaus=1,ngaus(pelty)

        if (kfl_prdef_exm == 2) igacn=igaus   ! for nodal-field conductivity models       

        wmatr = 0.0_rp
        wrhsi = 0.0_rp
        griap = 0.0_rp        
        grdif = 0.0_rp
        wauxi = 0.0_rp        
        
        ! Cartesian derivatives and Jacobian
        call elmder(pnode,ndime,elmar(pelty)%deriv(1,1,igaus),elcod,cartd,detjm,xjacm,xjaci)
        dvolu= elmar(pelty)%weigp(igaus)*detjm
        
        if(llapl(pelty)==1) & 
             call elmhes(elmar(pelty)%heslo(1,1,igaus),hessi,ndime,pnode,ntens, &
             xjaci,d2sdx,elmar(pelty)%deriv,elcod)
        
        ! Intracellular action potential gradient (to the RHS)
        do idime=1,ndime
           do inode=1,pnode
              griap(idime) = griap(idime) + cartd(idime,inode) * eliap(inode,1)
           end do
        end do

        do idime=1,ndime
           do jdime=1,ndime
              grdif(idime) = grdif(idime) + cndin(jdime,idime,igacn) * griap(jdime)
           end do
        end do

        ! Total diffusion (to the matrix)
        do inode=1,pnode
           do idime=1,ndime
              do jdime=1,ndime
                 wmatr(inode,1:pnode)=wmatr(inode,1:pnode) &
                      + cartd(jdime,inode) &
                      *    (cndex(idime,jdime,igacn)+cndin(idime,jdime,igacn)) & 
                      * cartd(idime,1:pnode)
              end do
           end do
        end do

        !
        ! Elementary RHS and Matrix construction


        do inode=1,pnode
           wliap=0.0_rp

           ! (K u) term: Intracellular action potential weak laplacian 
           do idime=1,ndime
              wliap = wliap + cartd(idime,inode) * grdif(idime)
           end do

           elrhs(inode)         = elrhs(inode)         - dvolu * wliap
           elmat(inode,1:pnode) = elmat(inode,1:pnode) + dvolu * wmatr(inode,1:pnode)

        end do
        
     end do gauss_points
     
     
     ! Prescribe Dirichlet boundary conditions
     if (kfl_exboc_exm == 1) then
        do inode=1,pnode
           ipoin=lnods(inode,ielem)
           if(kfl_fixno_exm(2,ipoin)==1) then
              adiag=elmat(inode,inode)
              do jnode=1,pnode
                 elmat(inode,jnode)=0.0_rp
                 elrhs(jnode)=elrhs(jnode)-elmat(jnode,inode)*bvess_exm(2,ipoin)
                 elmat(jnode,inode)=0.0_rp
              end do
              elmat(inode,inode)=adiag
              elrhs(inode)=adiag*bvess_exm(2,ipoin)
           end if
        end do
     end if
  
  
     ! Assembly

     do inode=1,pnode
        ipoin=lnods(inode,ielem)
        rhsid(ipoin)       = rhsid(ipoin)       + elrhs(inode)
     end do
     
     !call exm_assmat(&
     !     elmat,amatr,pnode,ndime,ndofn_exm,nevat_exm,&
     !     nunkn_exm,lnods(1,ielem),lpexm,kfl_algso_exm)
     
  end do elements


end subroutine exm_eapelm
