subroutine exm_reaphy
  !-----------------------------------------------------------------------
  !****f* Exmedi/exm_reaphy
  ! NAME 
  !    exm_reaphy
  ! DESCRIPTION
  !    This routine reads the physical problem definition.
  ! S
  !    listen
  !    memchk
  !    runend
  ! D BY
  !    exm_turnon
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      mod_memchk
  use      def_exmedi

  implicit none
  integer(ip) :: imate,iipar,iauxi,nauxi
  integer(ip) :: igrou,ivalu,istim,iall_materials
  real(rp)    :: xrefi(12),xbalo(3,3),xnorm(3)

  xrefi= 0.0_rp
  xbalo= 0.0_rp
  do iauxi=1,3
     xnorm(iauxi)= 0.0_rp
  end do
  thiso_exm = 100000000.0_rp
  setno_exm=0_ip

  if( INOTSLAVE ) then

     !
     ! Reach the section
     !
     rewind(lisda)
     call ecoute('exm_reaphy')
     do while(words(1)/='PHYSI')
        call ecoute('exm_reaphy')
     end do

     !
     ! Initializations (defaults)
     !
     kfl_stead_exm = 0                    ! No steady-state yet
     kfl_appli_exm = 0                    ! Applied currents and starting pots. are off
     kfl_gcoup_exm = 0                    ! No geometric coupling with SOLIDZ
     kfl_genal_exm = 1                    ! General algorithm type (EXPLICIT=1, decoupled 
     !    implicit, monolithic implicit, ...)
     kfl_gemod_exm = 0                    ! Default: FHN
     !    (TT,LR,BR, ...) or 0=no-subcell or approximate models (FHN, FENTON...)

     kfl_cemod_exm = 1                    ! Cell model (propagation model): monodomain 1 o bidomain 2
     kfl_ptrig_exm = 0                    ! Stimuli start by time and not by pressure trigger     
     
     kfl_comdi_exm = 0                    ! Conductivity model distribution

     kfl_prdef_exm = 0                    ! Properties global (0), node-wise(1) or element-wise(2) 
     kfl_tiext_exm = 0                    ! Variable time step (from critical)

     kfl_stree_exm = 0                    ! No streeter fiber field created
     kfl_voini_exm = 0                    ! No initial potential imposed

     modfi_exm     = 0                    ! Fiber model
     nstim_exm     = 1                    ! One starting stimulus
     nconc_exm = 8                        ! TT MODEL number of ionic concentration
     nauxi_exm = 12                       ! TT MODEL 
     nicel_exm = 17                       ! TT MODEL 
     kfl_heter_exm = 0                     ! homogeneous cell model
     apval_exm     = 0.0_rp                               ! Applied current intensity
     aplap_exm     = 0.0_rp                               ! Applied current time lapse
     apcen_exm     = 0.0_rp                               ! Applied current center
     aprea_exm     = 0.0_rp                               ! Applied current reach

     ngrou_exm     = 0
     nstis_exm     = 0

     xthri_exm     = 1.0e20
     xthrs_exm     = 1.0e20
     xmsuv_exm     = 1.0_rp
     xmccm_exm     = 1.0_rp
     pdccm_exm     = 1.0_rp
     pdsuv_exm     = 1.0_rp
     iall_materials= 0                                    ! Each material reads its own set of properties
     ! specific model type for all materials
     !     subcellular model (1=TT, 2=LR, 3=BR, ...) 
     !
     ! FHN IS NO MORE THE DEFAULT!!!!


    fiaxe_exm = 1.0_rp
    strbo_exm(1)=1_ip
    strbo_exm(2)=2_ip
    strbo_exm(3)=3_ip

     kfl_spmod_exm = -1


     ! FOR SUBCELLULAR IONIC CURRENTS MODELS (TT, LR, BR, ...)
     nauxi_exm = 1    ! Default value of number of variables used for activation/inactivation 
     !modce_exm = 1
     !
     ! Begin to read data
     !
     do while(words(1)/='ENDPH')
        call ecoute('exm_reaphy')
        if(words(1)=='PROBL') then
           !
           ! Problem definition data
           !
           call ecoute('exm_reaphy')
           do while(words(1)/='ENDPR')
              if(words(1)=='TEMPO') then                    ! Temporal evolution
                 if(exists('ON   ')) kfl_timei_exm = 1 
                 if(exists('ACTIV')) kfl_timei_exm = 1 
              else if(words(1)=='ALLMA') then               ! When many materials are present, all of them share properties
                 iall_materials = 1
              else if(words(1)=='TIMES') then               ! Fixed or variable time step
                 if(exists('FIXED')) kfl_tiext_exm = 1
              else if(words(1)=='APPLI') then               ! Applied currents           

                 call runend("EXM_COMAPP: ALWAYS STARTING POTENTIAL OPTION")

                 call ecoute('exm_reaphy')
              else if(words(1)=='GEOCO') then               ! Geometric coupling: 
                 if (words(2)=='LAGRA') kfl_gcoup_exm = 1   !    is SOLIDZ moving my mesh?
              else if(words(1)=='START') then               ! Starting potential
                 kfl_appli_exm = 200
                 if(exists('TABLE')) then
                    call ecoute('exm_reaphy')
                    if (words(1) == 'NSTIM') then       ! Reach from the center
                       nstim_exm = int(param(1))
                       if (words(2) == 'BOUND') nstim_exm = -1
                    else 
                       call runend('EXM_REAPHY: GIVE FIRST THE TOTAL NUMBER OF STARTING STIMULI')
                    end if

                    if(size(apval_exm) .lt. nstim_exm) then
                       call runend('EXM_REAPHY: apval_exm: MAXIMUM NUMBER OF STARTING STIMULI EXCEEDED ')
                    end if   
                     if(size(apval_exm) .lt. nstim_exm/ndime) then
                       call runend('EXM_REAPHY: apval_exm: MAXIMUM NUMBER OF STARTING STIMULI EXCEEDED ')
                    end if
                    if(size(aprea_exm) .lt. nstim_exm) then
                       call runend('EXM_REAPHY: aprea_exm: MAXIMUM NUMBER OF STARTING STIMULI EXCEEDED ')
                    end if
                    if(size(aptim) .lt. nstim_exm) then
                       call runend('EXM_REAPHY: aptim: MAXIMUM NUMBER OF STARTING STIMULI EXCEEDED ')
                    end if

                    nauxi= nstim_exm
                    if (nstim_exm < 0) nauxi= 1                    
                    do istim=1,nauxi
                       ! SIEMPRE DARLAS EN ESTE ORDEN: VALUE, CENTER (DE TAMANYO NDIME), REACH, TSTAR, LAPSE
                       call ecoute('exm_reaphy')                       
                       apval_exm(istim) = param(1)
                       apcen_exm(1:ndime,istim) = param(2:ndime+1)
                       aprea_exm(istim) = param(ndime+2)
                       aptim(istim) = param(ndime+3)
                       aplap_exm(istim) = param(ndime+4)
                    end do
                 else       
                    call ecoute('exm_reaphy')
                    if (words(1) == 'NSTIM') then       ! Reach from the center
                       nstim_exm = getint('NSTIM',-1_ip,'#Number of initial stimuli')
                       if (words(2) == 'BOUND') then
                          nstim_exm = -1
                          nstis_exm = getint('BOUND', 2_ip,'#Set number for the initial stimuli')
                       end if
                    else 
                       call runend('EXM_REAPHY: GIVE FIRST THE TOTAL NUMBER OF STARTING STIMULI')
                    end if
                    nauxi= nstim_exm
                    if (nstim_exm < 0) nauxi = 1
                    do while(words(1)/='ENDST')
                       if (words(1) == 'VALUE') then            ! Value
                          apval_exm(1:nauxi) = param(1:nauxi)
                       else if (words(1) == 'CENTE') then       ! Center of application
                          do iauxi=1,nauxi
                             iipar=(iauxi-1)*ndime + 1
                             apcen_exm(1:ndime,iauxi) = param(iipar:ndime+iipar-1)
                          end do
                       else if (words(1) == 'LAPSE') then       ! Time lapse of application
                          aplap_exm(1:nauxi) = param(1:nauxi)
                       else if (words(1) == 'TSTAR') then       ! Start time
                          if (words(2) == 'PTRIG') then
                             kfl_ptrig_exm = 1
                             aptim(1:nauxi) = param(2:nauxi+1)
                          else 
                             aptim(1:nauxi) = param(1:nauxi)
                          end if
                       else if (words(1) == 'REACH') then       ! Reach from the center
                          aprea_exm(1:nauxi) = param(1:nauxi)
                       else if (words(1) == 'NSTIM') then       ! Reach from the center
                          if (nauxi==0) nauxi = param(1)
                       end if
                       call ecoute('exm_reaphy')
                       
                    end do
                 end if
                 
              end if
              
              call ecoute('exm_reaphy')
           end do
           
        else if(words(1)=='PROPE') then
           !
           ! Properties
           ! needed for the default FHN:
           poref_exm(1)  = -86.2_rp    ! potin  ,1:nmate
           poref_exm(2)  =  22.0_rp    ! potfi  ,1:nmate

           ! Default: Ca+ concentration is not coming from a cell model, but from time spent after a wave 
           ! has passed, like FHN or Fenton.

           kfl_ephys         = 0           
           kfl_voini_exm     = 0           

           ! RUTH REVISAR ESTO!!!!!
!!           poref_exm(1)  = -85.0_rp    ! potin  ,1:nmate
!!           poref_exm(2)  =  -0.5_rp    ! potfi  ,1:nmate

           imate= 1_ip
           call ecoute('exm_reaphy')
           do while(words(1)/='ENDPR')
              if( words(1) == 'MATER' ) then
                 imate = getint('MATER',1_ip,'#Current material')
                 if( imate > nmate_exm ) then
                    call runend('EXM_REAPHY: THIS MODULE HAS MORE MATERIALS THAN THOSE DEFINED IN DOM.DAT')
                 end if
                 ! ADOC[2]> DENSITY: real                                           $ Density (rho)                   
              else if(words(1)=='INICE') then                    ! CURRENTS PARAMETERS AND CONSTANTS
                 if (exists('HETER')) kfl_heter_exm = 1
                 igrou = 0
                 do while(words(1)/='ENDIN')
                    if(words(1)=='BEATS') then
                       onecl_exm( 1)=int(param(1))             ! Number of Beats to simulate until steady state                       
                    else if(words(1)=='CYCLE') then
                       onecl_exm( 2)=int(param(1))              ! Heart beat cycle length
                    else if(words(1)=='MYOCY') then               ! Normal Cell or Heart Failure simulation
                       if(words(2)=='NORMA') then
                          kfl_hfmod_exm = 0
                          ttpar_exm(1,:) = 1.0_rp
                       else if(words(2)=='HEART') then
                          kfl_hfmod_exm = 1
                       end if
                          !call ecoute('exm_reaphy')
                    else if(words(1)=='CONDU' .and. kfl_hfmod_exm == 1) then
                       call ecoute('exm_reaphy')
                       igrou = igrou+1
                       ivalu = 0
                       if(words(1)=='INAME') then  
                           call ecoute('exm_reaphy')
                           do while(words(1)/='ENDCO') 
                               ivalu=ivalu+1
                               ttpar_exm(1,ivalu) = param(1)
                           call ecoute('exm_reaphy')
                           end do
                       end if
                       ngrou_exm = igrou
                       !end if
                    else if(words(1)=='ONDRU') then               ! Simulation with or without a drug
                       if(exists('YES  ') ) then
                          kfl_drugs_exm = 1 
                       else
                          kfl_drugs_exm = 0 
                       end if
                    else if(words(1)=='DOSIS') then
                       call ecoute('exm_reaphy')
                       igrou = igrou+1
                       ivalu = 0
                       if(words(1)=='INAME') then  
                          call ecoute('exm_reaphy')
                          do while(words(1)/='ENDDO') 
                             ivalu=ivalu+1
                             ttpar_exm(2,ivalu) = param(1)
                             call ecoute('exm_reaphy')
                          end do
                       end if
                       ngrou_exm = igrou
                    end if
                    call ecoute('exm_reaphy')   
                 end do
                 
              else if(words(1)=='INCON') then
                 gcond_exm(1,1,imate) = param(1)
                 gcond_exm(1,2,imate) = param(2)
                 gcond_exm(1,3,imate) = param(3)
              else if(words(1)=='SUBCE') then

                 if (words(2)=='FITZH') then

                    if (exists('INITI')) then
                       kfl_voini_exm(imate) = 1
                       voini_exm(imate) = getrea('INITI',0.0_rp,'#Initial voltage of the material')
                    end if
                    
                    kfl_spmod_exm(imate) = 11
                    xmopa_exm( 2,imate)=1.0_rp !Default constant added for retrocompatibility

                 else if (words(2) =='FENTO') then                   

                    if (exists('INITI')) then
                       kfl_voini_exm(imate) = 1
                       voini_exm(imate) = getrea('INITI',0.0_rp,'#Initial voltage of the material')
                    end if

                    kfl_spmod_exm(imate) = 12 
                    kfl_fento_exm = 1
                    if (words(3) == 'BEELE') kfl_fento_exm = 1
                    if (words(3) == 'MODBE') kfl_fento_exm = 2
                    if (words(3) == 'LUORU') kfl_fento_exm = 3
                    if (words(3) == 'GIROU') kfl_fento_exm = 4
                    !
                    ! Define some model parameters and dimensions for the fenton model
                    !
                    
                    poref_exm(  1) = -85.0_rp   ! potin (V0)
                    poref_exm(  2) = 15.0_rp    ! potfi 
                    
                    
                    if (kfl_fento_exm == 1) then
                       
                       xmopa_exm( 3,imate)  = 0.13_rp    ! phi c
                       xmopa_exm( 4,imate)  = 3.33_rp    ! tau v +
                       xmopa_exm( 5,imate)  = 0.04_rp    ! phi v
                       
                       xmopa_exm( 7,imate)  = 1250.0_rp  ! tau v1 -
                       xmopa_exm( 8,imate)  = 19.6_rp    ! tau v2 -
                       xmopa_exm( 9,imate)  = 33.0_rp    ! tauro             
                       xmopa_exm( 10,imate) = 30.0_rp    ! tausi
                       xmopa_exm( 11,imate) = 12.5_rp    ! tauso
                       xmopa_exm( 12,imate) = 0.85_rp    ! phics
                       xmopa_exm( 13,imate) = 4.0_rp     ! gephi
                       xmopa_exm( 16,imate) = 10.0_rp    ! consk
                       xmopa_exm( 17,imate) = 870.0_rp   ! tau w +
                       xmopa_exm( 18,imate) = 41.0_rp    ! tau w -
                       
                    else if (kfl_fento_exm == 2) then
                       
                       xmopa_exm( 3,imate)  =  0.13_rp    ! phi c
                       xmopa_exm( 4,imate)  =  3.33_rp    ! tau v +
                       xmopa_exm( 5,imate)  =  0.055_rp   ! phi v
                       
                       xmopa_exm( 7,imate)  =  1000.0_rp  ! tau v1 -
                       xmopa_exm( 8,imate)  =  19.2_rp    ! tau v2 -
                       xmopa_exm( 9,imate)  =  50.0_rp    ! tauro          
                       xmopa_exm( 10,imate) =  45.0_rp    ! tausi
                       xmopa_exm( 11,imate) =  8.3_rp     ! tauso
                       xmopa_exm( 12,imate) =  0.85_rp    ! phics
                       xmopa_exm( 13,imate) =  4.0_rp     ! gephi
                       xmopa_exm( 16,imate) =  10.0_rp    ! consk
                       xmopa_exm( 17,imate) =  667.0_rp   ! tau w +
                       xmopa_exm( 18,imate) =  11.0_rp    ! tau w -
                       
                    else if (kfl_fento_exm == 3) then
                       
                       xmopa_exm( 3,imate)  =  0.13_rp    ! phi c
                       xmopa_exm( 4,imate)  =  10.0_rp    ! tau v +
                       xmopa_exm( 5,imate)  =  0.0_rp     ! phi v 
                       
                       xmopa_exm( 7,imate)  =  18.2_rp    ! tau v1 - 
                       xmopa_exm( 8,imate)  =  18.2_rp    ! tau v2 -
                       xmopa_exm( 9,imate)  =  130.0_rp   ! tauro                
                       xmopa_exm( 10,imate) =  127.0_rp   ! tausi
                       xmopa_exm( 11,imate) =  12.5_rp    ! tauso 
                       xmopa_exm( 12,imate) =  0.85_rp    ! phics
                       xmopa_exm( 13,imate) =  5.8_rp     ! gephi
                       xmopa_exm( 16,imate) =  10.0_rp    ! consk
                       xmopa_exm( 17,imate) =  1020.0_rp  ! tau w +
                       xmopa_exm( 18,imate) =  80.0_rp    ! tau w -
                       
                       
                    else if (kfl_fento_exm == 4) then
                       
                       
                       xmopa_exm( 3,imate)  =  0.13_rp    ! phi c
                       xmopa_exm( 4,imate)  =  10.0_rp    ! tau v +
                       xmopa_exm( 5,imate)  =  0.025_rp   ! phi v
                       
                       xmopa_exm( 7,imate)  =  333.0_rp   ! tau v1 -
                       xmopa_exm( 8,imate)  =  40.0_rp    ! tau v2 -
                       xmopa_exm( 9,imate)  =  25.0_rp    ! tauro                
                       xmopa_exm( 10,imate) =  22.0_rp    ! tausi
                       xmopa_exm( 11,imate) =  12.5_rp    ! tauso 
                       xmopa_exm( 12,imate) =  0.85_rp    ! phics
                       xmopa_exm( 13,imate) =  8.7_rp     ! gephi
                       xmopa_exm( 16,imate) =  10.0_rp    ! consk
                       xmopa_exm( 17,imate) =  1000.0_rp  ! tau w +
                       xmopa_exm( 18,imate) =  65.0_rp    ! tau w -
                       
                       
                    end if

                 else if (words(2) =='TENTU') then 
                    poref_exm(1)  =  0.0_rp    ! potin
                    poref_exm(2)  =  1.0_rp    ! potfi
                    kfl_spmod_exm(imate) = 1 
                    kfl_gemod_exm = 1
                    kfl_ephys(imate) = 1
                    if (exists('INITI')) then
                       kfl_voini_exm(imate) = 1
                       voini_exm(imate) = getrea('INITI',0.0_rp,'#Initial voltage of the material')
                    end if

                 else if (words(2) =='TTHET') then 
                    poref_exm(1)  =  0.0_rp    ! potin
                    poref_exm(2)  =  1.0_rp    ! potfi
                    kfl_spmod_exm(imate) = 4 
                    kfl_gemod_exm = 1
                    kfl_ephys(imate) = 1                      
                    nconc_exm = 10                        
                    nauxi_exm = 12                       
                    nicel_exm = 18                                        
                    if (exists('INITI')) then
                       kfl_voini_exm(imate) = 1
                       voini_exm(imate) = getrea('INITI',0.0_rp,'#Initial voltage of the material')
                    end if
                 else if (words(2) =='OHARA') then 
                    poref_exm(1)  =  0.0_rp    ! potin
                    poref_exm(2)  =  1.0_rp    ! potfi
                    kfl_spmod_exm(imate) = 5 
                    kfl_gemod_exm = 1
                    kfl_ephys(imate) = 1                      
                    nconc_exm = 11                        
                    nauxi_exm = 29                       
                    nicel_exm = 26          
                    if (exists('INITI')) then
                       kfl_voini_exm(imate) = 1
                       voini_exm(imate) = getrea('INITI',0.0_rp,'#Initial voltage of the material')
                    end if
                 else if (words(2) =='TTINA') then 
                    poref_exm(1)  =  0.0_rp    ! potin
                    poref_exm(2)  =  1.0_rp    ! potfi
                    kfl_spmod_exm(imate) = 6 
                    kfl_gemod_exm = 1
                    kfl_ephys(imate) = 1                      
                    nconc_exm = 10                        
                    nauxi_exm = 12                       
                    nicel_exm = 18  
                    if (exists('INITI')) then
                       kfl_voini_exm(imate) = 1
                       voini_exm(imate) = getrea('INITI',0.0_rp,'#Initial voltage of the material')
                    end if
                    
                 else if (words(2) =='NOMOD') then                   ! no ionic current model
                    kfl_spmod_exm(imate) = 0
                    kfl_ephys(imate)     = 0                    
                    if (exists('INITI')) then
                       kfl_voini_exm(imate) = 1
                       voini_exm(imate) = getrea('INITI',0.0_rp,'#Initial voltage of the material')
                    end if
                 end if
                 if (imate == 20) call runend ('EXM_REAPHY: KFL_EPHYS IS DEFINED AS A 20-ROW VECTOR')
              end if

              if(words(1)=='EPSIL') then               ! Model constant epsilon
                 xmopa_exm( 1,imate)=param(1)
              else if(words(1)=='CHIME') then               ! Model constant chi
                 xmopa_exm( 2,imate)=param(1)
              else if(words(1)=='ALPHA') then               ! Model constant alpha
                 xmopa_exm( 3,imate)=param(1)
              else if(words(1)=='GAMMA') then               ! Model constant gamma
                 xmopa_exm( 4,imate)=param(1)
              else if(words(1)=='CMEMB') then               ! Model constant c
                 xmopa_exm( 5,imate)=param(1)
              else if(words(1)=='CMCON') then               ! Model constant Cm
                 xmccm_exm=param(1)
                 pdccm_exm=param(1)
!!                 xmopa_exm( 6,imate)=param(1)             ! antes era asi...
              else if(words(1)=='SUVOL') then             ! SURFACE TO VOLUME RATIO NECESSARY FOR CELL MODELS
                 xmsuv_exm=param(1)
              else if(words(1)=='ISOCH') then
                 if(words(2)=='HIGHE') then
                    thiso_exm(1)=  1.0_rp         ! isochrones are taken when value becomes higher than threshold
                    thiso_exm(2)= param(2)        ! isochrones threshold
                 else if(words(2)=='LOWER') then
                    thiso_exm(1)= -1.0_rp         ! isochrones are taken when value becomes lower than threshold
                    thiso_exm(2)= param(2)        ! isochrones threshold
                 end if                 
              else if(words(1)=='IONIZ') then               ! Ionization current function (FHN)
                 continue
              end if

              call ecoute('exm_reaphy')

           end do

        else if (words(1)=='FIBER') then              
           modfi_exm = -getint('FIELD',1_ip,'#Field Number fo fibers')
           ! conductivity based on fibers
           kfl_comdi_exm = 2
        else if (words(1)=='CREAT') then
           call ecoute('exm_reaphy')
           do while(words(1).ne.'ENDCR')
             if(words(1)=='FUNCT') then
               if (words(2) == 'CUBIC') then
                 kfl_stree_exm=3
               elseif (words(2) == 'LINEA') then
                 kfl_stree_exm=1
               else
                 kfl_stree_exm=1 ! Default option is linear
               endif 
             elseif (words(1) == 'VAXIS') then
               fiaxe_exm(1:3)= param(1:3)
      
             elseif (words(1) == 'EPICA') then
               strbo_exm(1)=param(1)
               setno_exm=setno_exm+1
             elseif(words(1) == 'LEFTE') then
               strbo_exm(2)=param(1)
               setno_exm=setno_exm+1
             elseif(words(1) == 'RIGHT') then
               strbo_exm(3)=param(1)
               setno_exm=setno_exm+1
             endif
             call ecoute('exm_reaphy')
           enddo 
        else if (words(1)=='CELLT') then              
           modce_exm = -getint('FIELD',2_ip,'#heterogeneous cell type')
        end if
     end do

     !
     ! Unknowns element-wise dimensions
     !
     ndofn_exm = 1
     if (kfl_cemod_exm == 2) ndofn_exm = 2
     ndof2_exm = ndofn_exm*ndofn_exm

     !  FOR NO SUBCELLULAR IONIC CURRENTS MODELS (FHN, ...)
     nevat_exm = mnode    ! <--- explicit (default value)
     if (kfl_genal_exm == 2) nevat_exm = mnode                   ! <--- decoupled implicit
     if (kfl_genal_exm == 3) nevat_exm = ndofn_exm*mnode         ! <--- monolithic implicit

     !
     ! Initialization
     !
     dtinv_exm=0.0_rp

     if (iall_materials == 1) then

        do imate= 1,nmate
           gcond_exm(1,1,imate) = gcond_exm(1,1,1) 
           gcond_exm(1,2,imate) = gcond_exm(1,2,1) 
           gcond_exm(1,3,imate) = gcond_exm(1,3,1) 
           xmopa_exm( 3,imate)  = xmopa_exm(  3,1)  
           xmopa_exm( 4,imate)  = xmopa_exm(  4,1)  
           xmopa_exm( 5,imate)  = xmopa_exm(  5,1)  
                                                  
           xmopa_exm( 7,imate)  = xmopa_exm(  7,1)  
           xmopa_exm( 8,imate)  = xmopa_exm(  8,1)  
           xmopa_exm( 9,imate)  = xmopa_exm(  9,1)             
           xmopa_exm( 10,imate) = xmopa_exm( 10,1) 
           xmopa_exm( 11,imate) = xmopa_exm( 11,1) 
           xmopa_exm( 12,imate) = xmopa_exm( 12,1) 
           xmopa_exm( 13,imate) = xmopa_exm( 13,1) 
           xmopa_exm( 16,imate) = xmopa_exm( 16,1) 
           xmopa_exm( 17,imate) = xmopa_exm( 17,1) 
           xmopa_exm( 18,imate) = xmopa_exm( 18,1) 
           kfl_ephys(imate)     = kfl_ephys(    1)     
        end do

     end if


     iauxi= -1
     do imate= 1,nmate
        if (kfl_spmod_exm(imate) .ge. 0) iauxi= 1
     end do
     if (iauxi == -1) then

        call runend('EXM_REAPHY: FITZHUGH MUST BE EXPLICTILY DEFINED AS SUBCELL=FITZHUGH')
        
     end if


     if (kfl_gemod_exm == 1) then
        ! TEN TUSSCHER model  
        ! kfl_spmod_exm(imate) = 1 --> TenTusscher model 
        !       (Paper 'A model for human ventricular tissue', 2003)
        ! kfl_spmod_exm(imate) = 2 --> LuoRudyII  model (Paper ..................... , 1994)
        ! kfl_spmod_exm(imate) = 3 --> BeelerReuter model (Paper ................., 1977)
        pdsuv_exm = xmsuv_exm * 10.0_rp   !unit  conversion to cm
        pdccm_exm = xmccm_exm * 898755.1787365_rp !from microFarads/cm2; capacitance in cgs is esu
        !gcond_exm(1,:,imate) = gcond_exm(1,:,imate) * 8991997122.56092078050535_rp
        !(1.0_rp / (4.0_rp * 3.1415_rp * 0.00000000000885_rp)) !conversion from SI to CGS
        
        do imate= 1,nmate
            gcond_exm(1,1,imate) = (gcond_exm(1,1,imate) * 8991997122.56092078050535_rp)/(pdsuv_exm*pdccm_exm)
            gcond_exm(1,2,imate) = (gcond_exm(1,2,imate) * 8991997122.56092078050535_rp)/(pdsuv_exm*pdccm_exm) 
            gcond_exm(1,3,imate) = (gcond_exm(1,3,imate) * 8991997122.56092078050535_rp)/(pdsuv_exm*pdccm_exm)
        end do
        
        if (kfl_spmod_exm(imate) == 1) then
           nauxi_exm = 12     
           nconc_exm = 8
           nicel_exm = 17

        else if (kfl_spmod_exm(imate) == 4) then
           poref_exm(1)  =  0.0_rp    ! potin
           poref_exm(2)  =  1.0_rp    ! potfi
           nconc_exm = 10                        ! TT MODEL number of ionic concentration
           nauxi_exm = 12                       ! TT MODEL 
           nicel_exm = 18                       ! TT MODEL

        else if (kfl_spmod_exm(imate) == 5) then
           poref_exm(1)  =  0.0_rp    ! potin
           poref_exm(2)  =  1.0_rp    ! potfi
           nconc_exm = 11                        ! TT MODEL number of ionic concentration
           nauxi_exm = 29                       ! TT MODEL 
           nicel_exm = 26                       ! TT MODEL
           
        else if (kfl_spmod_exm(imate) == 6) then
           poref_exm(1)  =  0.0_rp    ! potin
           poref_exm(2)  =  1.0_rp    ! potfi
           nconc_exm = 10                        ! TT MODEL number of ionic concentration
           nauxi_exm = 14                       ! TT MODEL 
           nicel_exm = 18                       ! TT MODEL
        end if
     end if
     
  end if      !! kfl_paral


end subroutine exm_reaphy
