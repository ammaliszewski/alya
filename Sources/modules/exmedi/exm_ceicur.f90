subroutine exm_ceicur
!-----------------------------------------------------------------------
!****f* Exmedi/exm_ceicur
! NAME 
!    exm_ceicur
! DESCRIPTION
!   This routine computes cell ionic currents of Ten Tusscher model (TT). 
!
!   [pA/pF] --> I_Na, I_CaL, I_to, I_Ks, I_Kr, I_K1, I_NaCa, I_NaK
!   [pA/pF] --> I_pCa, I_pK, I_bNa, I_bCa, I_stim, I_ax
!   [mM/ms] --> I_leak, I_up, I_rel
!
! USES
!   def_parame
!   def_master
!   def_domain
!   def_exmedi
! USED BY
!    exm_doiter
!***
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!
! The variables are 17:
!   - xina:   I_Na    -->  vicel_exm(1,ipoin,1)  -->  fast current of sodium (Page H1575 TT paper)
!   - xical:  I_CaL   -->  vicel_exm(2,ipoin,1)  -->  current of L-Type calcium (Page H1576 TT paper)
!   - xito:   I_to    -->  vicel_exm(3,ipoin,1)  -->  transient outward current (Page H1577 TT paper)
!   - xiks:   I_Ks    -->  vicel_exm(4,ipoin,1)  -->  slow delayed rectifier current (Page H1578 TT paper)
!   - xikr:   I_Kr    -->  vicel_exm(5,ipoin,1)  -->  rapid delayed rectifier current (Page H1579 TT paper)
!   - xik1:   I_K1    -->  vicel_exm(6,ipoin,1)  -->  inward rectifier K current (Page H1580 TT paper)
!   - xinaca: I_NaCa  -->  vicel_exm(7,ipoin,1)  -->  sodium/calcium exchanger current (Page H1580 TT paper)
!   - xinak:  I_NaK   -->  vicel_exm(8,ipoin,1)  -->  sodium/potassium pump current (Page H1580 TT paper)
!   - xipca:  I_pCa   -->  vicel_exm(9,ipoin,1)  -->  calcium plateau current (Page H1580 TT paper)
!   - xipk:   I_pK    -->  vicel_exm(10,ipoin,1) -->  potassium plateau current (Page H1580 TT paper)
!   - xibna:  I_bNa   -->  vicel_exm(11,ipoin,1) -->  sodium background current (Page H1580 TT paper)
!   - xibca:  I_bCa   -->  vicel_exm(12,ipoin,1) -->  calcium background current (Page H1581 TT paper)
!   - xileak: I_leak  -->  vicel_exm(13,ipoin,1) -->  leak from SR to the cytoplasm (Page H1581 TT paper)
!   - xiup:   I_up    -->  vicel_exm(14,ipoin,1) -->  pump taking up Ca in the SR (Page H1581 TT paper)
!   - xirel:  I_rel   -->  vicel_exm(15,ipoin,1) -->  Ca-induced Ca release (CICR) (Page H1581 TT paper)
!   - xistim: I_stim  -->  vicel_exm(16,ipoin,1) -->  external stimulus current (Page H1581 TT paper)
!   - xiax:   I_ax    -->  vicel_exm(17,ipoin,1) -->  axial current flow (Page H1581 TT paper)
!
! The order is important because it is related with the order of nicel_exm variables (exm_memall.f90):
!
!-----------------------------------------------------------------------

  use      def_parame
  use      def_master
  use      def_elmtyp
  use      def_domain
  use      def_exmedi

  implicit none


! ESTO ESTA AGREGADO SOLAMENTE PARA QUE COMPILE
! ESTO ESTA AGREGADO SOLAMENTE PARA QUE COMPILE
! ESTO ESTA AGREGADO SOLAMENTE PARA QUE COMPILE
! La variable tipocelula indica a que parte del musculo cardiaco pertenece la celula
  real(rp) :: tipocelula=0  !(0=epicardial, 1=endocardial, 2=M cells)




! DEFINITION OF VARIABLES

  integer(ip) :: ipoin
  integer(ip) :: nmait, imait
  integer(ip) :: ielem,inode,pnode,pmate,ituss,pelty
  real(rp)    :: xina, xical, xito, xiks, xikr, xik1, xinaca, xinak, xipca, xipk
  real(rp)    :: xibna, xibca, xileak, xiup, xirel, xistim, xiax

  real(rp)    :: enapot, ecapot, ekpot, ekspot 

  real(rp)    :: groupcon, xgroupcon

  real(rp)    :: vaux1, vaux2, vaux3




! DESCRIPTION OF VARIABLES

! ipoin: point number (it is related with a node)
! nmait: maximum number of iterations used for computation of activation variables 
!        (it is related with non-linearity)
! imait: auxiliary variable used as counter of iterations

! xina, xical, xito, xiks, xikr, xik1, xinaca, xinak, xipca, xipk:  ionic currents 
! xibna, xibca, xileak, xiup, xirel, xistim, xiax: ionic currents

! enapot: reversal (Nerst) potential for sodium ion
! ecapot: reversal (Nerst) potential for calcium ion
! ekpot: reversal (Nerst) potential for potassium ion
! ekspot: reversl potential () for potassium-sodium ions

! groupcon: R * T / F (group of constants)   [mV]
! xgroupcon: 1 / groupcon   [1/mV]

! vaux1, vaux2, vaux3: auxiliar variables 1, 2 and 3




! DESCRIPTION OF CONSTANTS (PARAMETERS)

! PARAMETERS of Ten Tusscher model for cell ionic currents (they appear in currents same order)
! These constants (page H1574 of TT paper) are used for computation of ionic currents

  real(rp)   ::  celltem, rgascon, faracon, xtrkcon, xtrnacon, xtrcacon
  real(rp)   ::  nagmax
  real(rp)   ::  calgmax
  real(rp)   ::  togmaxmce, togmaxepi, togmaxend
  real(rp)   ::  ksgmaxend, ksgmaxepi, ksgmaxmce, ksnaperm
  real(rp)   ::  krgmax
  real(rp)   ::  k1gmax
  real(rp)   ::  nacamax, gamanaca, kmcanaca, kmnanaca, satunaca, alfanaca
  real(rp)   ::  nakmax, kmknak, kmnanak
  real(rp)   ::  pcagmax, kpcapca
  real(rp)   ::  pkgmax
  real(rp)   ::  bnagmax
  real(rp)   ::  bcagmax
  real(rp)   ::  upvmax, upkcon
  real(rp)   ::  relacon, relbcon, relccon
  real(rp)   ::  leakvmax, dtimeEP




! COMPUTATION of cell ionic currents
! Currents are computed in the order specified above


  groupcon = rgascon * celltem / faracon          ! Group of constants: R * T / F  [mV] 
  xgroupcon = 1.0_rp / groupcon
  dtimeEP=dtime*1000.0_rp

     do ielem=1,nelem
        pelty = ltype(ielem)
 
        if ( pelty > 0 .and. lelch(ielem) /= ELCNT ) then
           pnode = nnode(pelty)
           pmate = 1                       
           if( nmate > 1 ) then
              pmate = lmate_exm(ielem)
           end if
 
           do inode= 1,pnode
              ipoin= lnods(inode,ielem)     
              ituss = pmate
              
                rgascon = cupar_exm(1,pmate)%value(1)      ! R [mJ/(K*mol)] --> ideal gas constant
                celltem = cupar_exm(1,pmate)%value(2)      ! T [K] --> cell system temperature 
                faracon = cupar_exm(1,pmate)%value(3)      ! F [C/mol] --> faraday constant
                xtrkcon = cupar_exm(1,pmate)%value(4)      ! K_o [mM] --> extracellular concentration of potassium (K+)
                xtrnacon = cupar_exm(1,pmate)%value(5)     ! Na_o [mM] --> extracellular concentration of sodium (Na+)
                xtrcacon = cupar_exm(1,pmate)%value(6)     ! Ca_o [mM] --> extracellular concentration of calcium (Ca++)
                
                nagmax = cupar_exm(2,pmate)%value(1)       ! G_Na [nS/pF] --> maximal conductance of I_Na current
                
                calgmax = cupar_exm(3,pmate)%value(1)      ! G_CaL [L/(mF*s)]--> maximal conductance of I_CaL current
              
                togmaxmce = cupar_exm(4,pmate)%value(1)    ! G_to_M [nS/pF] --> maximal conductance of I_to current for M Cell
                togmaxepi = cupar_exm(4,pmate)%value(2)    ! G_to_epi [nS/pF] --> max conductance of I_to current for epicardium
                togmaxend = cupar_exm(4,pmate)%value(3)    ! G_to_end [nS/pF] --> max conductance of I_to current for endocardium
                
                ksgmaxmce = cupar_exm(5,pmate)%value(1)    ! G_Ks_M [nS/pF] --> max conductance of I_Ks current for M Cell
                ksgmaxepi = cupar_exm(5,pmate)%value(2)    ! G_Ks_epi [nS/pF] --> max conductance of I_Ks current for epicardium
                ksgmaxend = cupar_exm(5,pmate)%value(3)    ! G_Ks_end [nS/pF] --> max conductance of I_Ks current for endocardium
                ksnaperm = cupar_exm(5,pmate)%value(4)     ! p_KNa [adim] --> relative I_Ks current permeability to Na+
                
                krgmax = cupar_exm(6,pmate)%value(1)       ! G_Kr [nS/pF] --> max conductance of I_Kr current
                
                k1gmax = cupar_exm(7,pmate)%value(1)       ! G_K1 [nS/pF] --> max conductance of I_K1 current
                
                nacamax = cupar_exm(8,pmate)%value(1)      ! k_NaCa [pA/pF] --> max I_NaCa current
                gamanaca = cupar_exm(8,pmate)%value(2)     ! gama [adim] --> voltage dependent parameter of I_NaCa current
                kmcanaca = cupar_exm(8,pmate)%value(3)     ! K_mCa [mM] --> Cai half-saturation constant for I_NaCa current
                kmnanaca = cupar_exm(8,pmate)%value(4)     ! K_mNai [mM] --> Nai half-saturation constant for I_NaCa current
                satunaca =  cupar_exm(8,pmate)%value(5)    ! k_sat [adim] --> saturation factor I_NaCa current
                alfanaca = cupar_exm(8,pmate)%value(6)     ! alfa [adim] --> factor enhancing outward nature of I_NaCa current
                
                nakmax = cupar_exm(9,pmate)%value(1)       ! P_NaK [pA/pF] --> max I_NaK current
                kmknak = cupar_exm(9,pmate)%value(2)       ! K_mK [mM] --> K_o half-saturation constant of I_NaK current
                kmnanak = cupar_exm(9,pmate)%value(3)      ! K_mNa [mM] --> Nai half-saturation constant of I_NaK current
                
                pcagmax = cupar_exm(10,pmate)%value(1)     ! G_pCa [pA/pF] --> max conductance of I_pCa current
                kpcapca = cupar_exm(10,pmate)%value(2)     ! K_pCa [mM] --> Cai half-saturation constant of I_pCa current
                
                pkgmax = cupar_exm(11,pmate)%value(1)      ! G_pK [nS/pF] --> max conductance of I_pK current
                
                bnagmax = cupar_exm(12,pmate)%value(1)     ! G_bNa [nS/pF] --> max conductance of I_bNa current
                
                bcagmax = cupar_exm(13,pmate)%value(1)     ! G_bCa [nS/pF] --> max conductance of I_bCa current
                
                upvmax = cupar_exm(14,pmate)%value(1)      ! V_maxup [mM/ms] --> max I_up current
                upkcon = cupar_exm(14,pmate)%value(2)      ! K_up [mM] --> half-saturation constant of I_up current
                
                relacon = cupar_exm(15,pmate)%value(1)     ! a_rel [mM/ms] --> max Ca_SR dependent I_rel current
                relbcon = cupar_exm(15,pmate)%value(2)     ! b_rel [mM] --> Ca_SR half-saturation constant of I_rel current
                relccon = cupar_exm(15,pmate)%value(3)     ! c_rel [mM/ms] --> max Ca_SR independent I_rel current
                
                leakvmax = cupar_exm(16,pmate)%value(1)    ! V_leak [1/ms] --> max I_leak current

            ! Fast current of sodium I_Na [pA/pF]
            ! nicel_exm = 1
            ! "A model for ventricular tissue", Ten Tusscher, Page H1585 (col 2)
                 
                 enapot = groupcon * log (xtrnacon / vconc(3,ipoin,1))    ! Reversal potential of Na [mV]
            
                 vaux1 = vauxi_exm(1,ipoin,1) * vauxi_exm(1,ipoin,1) &
                      * vauxi_exm(1,ipoin,1) * vauxi_exm(2,ipoin,1) * vauxi_exm(3,ipoin,1) 
                 xina =  nagmax * vaux1 * (elmag(1,ipoin,1) - enapot)
            
                 vicel_exm(1,ipoin,1) = xina       ! Value of I_Na current [pA/pF]
            
            
            ! Current of L-Type calcium I_CaL   [pA/pF]
            ! nicel_exm = 2
            ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
            
                 vaux1 = exp(2.0_rp * elmag(1,ipoin,1) * xgroupcon)
                 vaux2 = vaux1 - 1.0_rp
                 vaux2 = 1.0_rp / vaux2
                 vaux3 = vconc(5,ipoin,1) * vaux1 - 0.341_rp * xtrcacon
                 vaux3 = calgmax * 4.0_rp * xgroupcon * elmag(1,ipoin,1) * faracon * vaux3 * vaux2
            
                 xical = vaux3 * vauxi_exm(4,ipoin,1) * vauxi_exm(5,ipoin,1) * vauxi_exm(6,ipoin,1)
            
                 vicel_exm(2,ipoin,1) = xical       ! Value of I_CaL current [pA/pF]
            
            
            
            ! Transient outward current I_to [pA/pF]
            ! nicel_exm = 3
            ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
                 
                 ekpot = groupcon * log (xtrkcon / vconc(4,ipoin,1))    ! Reversal potential of K [mV]
            
                 vaux1 = vauxi_exm(7,ipoin,1) * vauxi_exm(8,ipoin,1)
                 vaux2 = vaux1 * (elmag(1,ipoin,1) - ekpot)
              
            
              
                 if (tipocelula == 0)  xito = togmaxepi * vaux2
                 if (tipocelula == 1)  xito = togmaxend * vaux2
                 if (tipocelula == 2)  xito = togmaxmce * vaux2
             
                 vicel_exm(3,ipoin,1) = xito       ! Value of I_to current [pA/pF]
            
              
            
            ! Slow delayed rectifier current I_Ks [pA/pF]
            ! nicel_exm = 4
            ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
                 
                 vaux1 = xtrkcon + ksnaperm * xtrnacon
                 vaux2 = vconc(4,ipoin,1) + ksnaperm * vconc(3,ipoin,1)               
                 ekspot = groupcon * log ( vaux1 / vaux2 )    ! Reversal potential of Ks [mV]
            
                 vaux1 = vauxi_exm(9,ipoin,1) * vauxi_exm(9,ipoin,1)
                 vaux2 = vaux1 * (elmag(1,ipoin,1) - ekspot)
              
                 if (tipocelula == 0)  xiks = ksgmaxepi * vaux2
                 if (tipocelula == 1)  xiks = ksgmaxend * vaux2
                 if (tipocelula == 2)  xiks = ksgmaxmce * vaux2
            
                 vicel_exm(4,ipoin,1) = xiks       ! Value of I_Ks current [pA/pF]
            
            
            
            ! Rapid delayed rectifier current I_Kr [pA/pF]
            ! nicel_exm = 5
            ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
                 
                 vaux1 = vauxi_exm(10,ipoin,1) * vauxi_exm(11,ipoin,1)
                 vaux2 = sqrt(xtrkcon / 5.4_rp) * vaux1 * (elmag(1,ipoin,1) - ekpot)
              
                 xikr = krgmax * vaux2
            
                 vicel_exm(5,ipoin,1) = xikr       ! Value of I_Kr current [pA/pF]
            
            
            
            ! Inward rectifier potassium current I_K1 [pA/pF]
            ! nicel_exm = 6
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
            
                 vaux1 = 3.0_rp * exp(0.0002_rp * (100.0_rp + elmag(1,ipoin,1) - ekpot))
                 vaux1 = vaux1 + exp(0.1_rp * (-10.0_rp + elmag(1,ipoin,1) - ekpot))
                 vaux2 = 1.0_rp + exp(-0.5_rp * (elmag(1,ipoin,1) - ekpot))
                 vaux2 = 1.0_rp / vaux2
                 vaux1 = vaux1 * vaux2                  ! Value of beta_K1
            
                 vaux2 = 1.0_rp + exp(0.06_rp * (elmag(1,ipoin,1) - ekpot - 200.0_rp))
                 vaux2 = 0.1_rp / vaux2                 ! Value of alpha_K1
            
                 vaux3 = vaux2 / (vaux1 + vaux2)        ! Value of K1_inf
            
            
                 vaux1 = sqrt(xtrkcon / 5.4_rp) * vaux3 * (elmag(1,ipoin,1) - ekpot)
                 xik1 = k1gmax * vaux1
            
                 vicel_exm(6,ipoin,1) = xik1       ! Value of I_K1 current [pA/pF]
            
            
            
            ! Sodium/calcium exchanger current I_NaCa [pA/pF]
            ! nicel_exm = 7
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
             
                 vaux1 = exp((gamanaca-1.0_rp) * elmag(1,ipoin,1) * xgroupcon)
                 
                 vaux2 = kmnanaca * kmnanaca * kmnanaca + xtrnacon * xtrnacon * xtrnacon
                 vaux2 = vaux2 * (kmcanaca + xtrcacon)
                 vaux2 = vaux2 * (1.0_rp + satunaca * vaux1)     ! current denominator 
                 vaux2 = 1.0_rp / vaux2                          ! inverse of current denominator
            
                 vaux3 = -vaux1 * xtrnacon * xtrnacon * xtrnacon * vconc(5,ipoin,1) * alfanaca         
                 vaux1 = exp(gamanaca * elmag(1,ipoin,1) * xgroupcon)
                 vaux3 = vaux3 + vaux1 * vconc(3,ipoin,1) * vconc(3,ipoin,1) &
                      * vconc(3,ipoin,1) * xtrcacon          ! current numerator
            
                 xinaca = nacamax * vaux3 * vaux2 
            
                 vicel_exm(7,ipoin,1) = xinaca       ! Value of I_NaCa current [pA/pF]
            
            
            
            ! Sodium/potassium pump current I_NaK [pA/pF]
            ! nicel_exm = 8
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
             
                 vaux1 = (xtrkcon + kmknak) * (vconc(3,ipoin,1) + kmnanak)
                 vaux2 = 1.0_rp + 0.1245_rp * exp(-0.1_rp * elmag(1,ipoin,1) * xgroupcon)
                 vaux2 = vaux2 + 0.0353_rp * exp(-elmag(1,ipoin,1) * xgroupcon)
                 vaux1 = vaux1 * vaux2             ! current denominator 
                 vaux1 = 1.0_rp / vaux1            ! inverse of current denominator 
            
                 vaux2 =  xtrkcon * vconc(3,ipoin,1)     ! current numerator
                 
                 xinak = nakmax * vaux1 * vaux2 
            
                 vicel_exm(8,ipoin,1) = xinak       ! Value of I_NaK current [pA/pF]
            
            
            
            ! Calcium plateau current I_pCa [pA/pF]
            ! nicel_exm = 9
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
             
                 vaux1 = kpcapca + vconc(5,ipoin,1)
                 vaux1 = 1.0_rp / vaux1             ! inverse of current denominator
                 
                 xipca = pcagmax * vaux1 * vconc(5,ipoin,1)
            
                 vicel_exm(9,ipoin,1) = xipca       ! Value of I_pCa current [pA/pF]
            
            
            
            ! Potassium plateau current I_pK [pA/pF]
            ! nicel_exm = 10
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
             
                 vaux1 = 1.0_rp + exp((25.0_rp - elmag(1,ipoin,1)) / 5.98_rp)
                 vaux1 = 1.0_rp / vaux1             ! inverse of current denominator
            
                 xipk = pkgmax * vaux1 * (elmag(1,ipoin,1) - ekpot)
            
                 vicel_exm(10,ipoin,1) = xipk       ! Value of I_pK current [pA/pF]
            
             
            
            ! Sodium background current I_bNa [pA/pF]
            ! nicel_exm = 11
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
             
                 xibna = bnagmax * (elmag(1,ipoin,1) - enapot)
            
                 vicel_exm(11,ipoin,1) = xibna      ! Value of I_bNa current [pA/pF]
            
            
            
            ! Calcium background current I_bCa [pA/pF]
            ! nicel_exm = 12
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
            
                 ecapot = groupcon / 2.0_rp * log (xtrcacon / vconc(5,ipoin,1))    ! Reversal potential of Ca [mV]
            
                 xibca = bcagmax * (elmag(1,ipoin,1) - ecapot)
            
                 vicel_exm(12,ipoin,1) = xibca      ! Value of I_bCa current [pA/pF]
            
            
            
            ! Leak current from sarcoplasmic reticulum (SR) to the cytoplasm I_leak [mM/ms]
            ! nicel_exm = 13
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
            
                 xileak = leakvmax * (vconc(7,ipoin,1) - vconc(5,ipoin,1))
            
                 vicel_exm(13,ipoin,1) = xileak      ! Value of I_leak current [mM/ms]
            
            
            
            ! Pump current taking up calcium in the sarcoplasmic reticulum (SR) I_up [mM/ms]
            ! nicel_exm = 14
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
             
                 vaux1 = vconc(5,ipoin,1) * vconc(5,ipoin,1)
                 vaux1 = 1.0_rp / vaux1
                 vaux2 = 1.0_rp + upkcon * upkcon * vaux1
                 vaux2 = 1.0_rp / vaux2            ! inverse of current denominator
            
                 xiup = upvmax * vaux2
            
                 vicel_exm(14,ipoin,1) = xiup      ! Value of I_up current [mM/ms]
            
            
            
            ! Calcium-induced calcium release (CICR) current I_rel [mM/ms]
            ! nicel_exm = 15
            ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
             
                 vaux1 = vconc(7,ipoin,1) * vconc(7,ipoin,1)
                 vaux2 = relbcon * relbcon + vaux1
                 vaux2 = 1.0_rp / vaux2
                 vaux3 = relacon * vaux1 * vaux2 + relccon
            
                 xirel = vaux3 * vauxi_exm(4,ipoin,1) * vauxi_exm(12,ipoin,1) 
            
                 vicel_exm(15,ipoin,1) = xirel      ! Value of I_rel current [mM/ms]
            
            
            
            ! External stimulus current I_stim [pA/pF]
            ! nicel_exm = 16
            ! "A model for ventricular tissue", Ten Tusscher, Page H1581
            
                 xistim = appfi_exm(ipoin,1)
                 vicel_exm(16,ipoin,1) = xistim      ! Value of I_stim current [pA/pF]
            
            
            ! IMPORTANTE: ESTO (I_ax) EN REALIDAD NO SE DEBERIA DEFINIR ACA
            !             HABRIA QUE INTRODUCIRLO DESDE AFUERA
            !             SE USA PARA EL CALCULO DE LAS CONCENTRACIONES
            
            ! Axial current flow I_ax [pA/pF]
            ! nicel_exm = 17
            ! "A model for ventricular tissue", Ten Tusscher, Page H1581
            
                 xiax = 0.0_rp
            
                 vicel_exm(17,ipoin,1) = xiax      ! Value of I_ax current [pA/pF]
             end do
        end if
  end do

end subroutine exm_ceicur
