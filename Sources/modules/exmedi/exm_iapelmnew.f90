!-----------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_iapelm.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Compute elemental matrix and RHS  
!> @details Compute elemental matrix and RHS
!> @} 
!-----------------------------------------------------------------------
subroutine exm_iapelmnew
  use      def_master
  use      def_domain
  use      def_elmtyp
  use      def_exmedi
  implicit none

  real(rp)    :: gpcar(ndime,mnode,mgaus)
  real(rp)    :: dvolu(mgaus),gpdet(mgaus)                     ! Values at Gauss points
  real(rp)    :: hessi(ntens,mnode*mlapl)

  real(rp)    :: elmat(mnode,mnode),wmatr(mnode,mnode) ! Element matrices
  real(rp)    :: esili(mnode), wreco(mnode), wapcu(mnode), wnoli(mnode), enoli(mnode)
  real(rp)    :: elcur(mnode,2), eliap(mnode,2), elion(mnode,2) , elian(mnode,2) 
  real(rp)    :: elgat(ngate_exm,mnode,2), elcod(ndime,mnode), eldis(ndime,mnode),elfib(ndime,mnode)

  real(rp)    :: cndin(ndime,ndime,mgaus), nfibe(ndime,mgaus),nfibe_length

  real(rp)    :: d2sdx(ndime,ndime,ndime*mlapl)        ! that is called inside do igaus 

  integer(ip) :: ielem,kelem,inode,ipoin,jnode         ! Indices and dimensions
  integer(ip) :: igaus,igacn,idime,pnode,noion
  integer(ip) :: pelty,pmate,igate,ncomp

  real(rp)    :: adiag

  real(rp)    :: xgate(ngate_exm),xpapp,xioni,xmion,xdtin,wdiff
  real(rp)    :: fact1,fact2,fact3
  real(rp)    :: tauro, tausi, tauso, phics, gephi, factk, xpold,ximol !used by FK !,grdin
  real(rp)    :: griap(ndime),grdif(ndime), taufi
  real(rp)    :: xmeps,xmalp,xmgam,unlst,unprv,xlian,xexdi,ximdi,fnol1,fnol2
  real(rp)    :: xjaci(ndime,ndime,mgaus),xjacm(ndime,ndime,mgaus),gpsha,voaux(10)



  if (kfl_cemod_exm == 2) then
    call runend('EXM_IAPELM: EXMEDI NOT PREPARED FOR BIDOMAIN MODEL')
  endif

  if (kfl_shock_exm .ne. 0) then
    call runend('EXM_IAPELM: EXMEDI NOT PREPARED WITH SHOCK CAPTURING')
  endif

  if(kfl_genal_exm .ne. 1 ) then
    call runend('EXM_IAPELM: EXMEDI NOT PREPARED FOR IMPLICIT SOLVING')
  endif

  if(kfl_nolum_exm .ne. 1) then
    call runend('EXM_IAPELM: EXMEDI NOT PREPARED FOR NON_LUMPED SOLVING')
  endif

  do pmate=1,nmate
    if(kfl_spmod_exm(pmate)==12) call runend('EXM_IAPELM: FENTON KARMA CODED BUT NOT UPDATED')
  enddo 

  rhsid    = 0.0_rp

  amatr     = 0.0_rp

  ecgvo_exm = 0.0_rp

  xexdi = 1.0_rp
  ximol = 0.0_rp
  fnol1 = 1.0_rp
  fnol2 = 0.0_rp
  xioni = 0.0_rp

  ximdi = 1.0_rp - xexdi

  ncomp= ncomp_exm
  !write(992,*) volts_exm(1,:,1)
  !write(993,*) elmag(1,:,2)

  if (ittim < ncomp) ncomp= ittim
  !
  ! Loop over elements

  elements: do kelem = 1,nelez(lzone(ID_EXMEDI))
     ielem = lelez(lzone(ID_EXMEDI)) % l(kelem)

     ! Initialize
     pelty = ltype(ielem)

     voaux= 0.0_rp

     if( pelty > 0 ) then
        pnode = nnode(pelty)

        pmate = 1
        if( nmate > 1 ) then
           pmate = lmate_exm(ielem)
        end if

        ! model parameters
        xmeps = 0.0_rp
        xmalp = 0.0_rp
        xmgam = 0.0_rp
        taufi = 0.0_rp

        if (kfl_spmod_exm(pmate) == 11) then     ! FHN 
           xmeps = xmopa_exm( 1,pmate)           ! epsilon for the rcp 
           xmalp = xmopa_exm( 3,pmate)           ! alpha for the Iion
           xmgam = xmopa_exm( 4,pmate)           ! gamma for the rcp
           taufi = xmopa_exm( 5,pmate)           ! C for the Iion

        else if (kfl_spmod_exm(pmate) == 12) then !fenton
           xmalp = xmopa_exm( 3,pmate)            ! phi c
           tauro = xmopa_exm( 9,pmate)               
           tausi = xmopa_exm( 10,pmate) 
           tauso = xmopa_exm( 11,pmate)
           phics = xmopa_exm( 12,pmate)
           gephi = xmopa_exm( 13,pmate) 
           factk = xmopa_exm( 16,pmate)           
        end if

        fact1 = - taufi *  xmalp
        fact2 =   taufi * (xmalp + 1.0_rp)
        fact3 = - taufi  

        cndin = 0.0_rp
        elmat = 0.0_rp
        esili = 0.0_rp
        enoli = 0.0_rp
        elion = 0.0_rp
        eliap = 0.0_rp

        ! Gather operations
        do inode=1,pnode
           ipoin = lnods(inode,ielem)
           eliap(inode,1) = elmag(1,ipoin,1)    ! SINGLE (MONODOMAIN) POTENTIAL
           eliap(inode,2) = elmag(1,ipoin,4)    !!changed by Jaz (3)
           elcur(inode,1) = appfi_exm(ipoin,1) !/xmccm_exm  ! APPLIED CURRENT
           elcur(inode,2) = appfi_exm(ipoin,3) !/xmccm_exm
           elian(inode,1) = elmag(1,ipoin,2)    ! iap for the non-linear fhn term
           
           if(kfl_spmod_exm(pmate).ne.0 .and. kfl_spmod_exm(pmate).ne.11 ) then ! Using celular models.
              eliap(inode,2) = elmag(1,ipoin,3) !!added by Jaz 
              elcur(inode,1) = 0.0_rp !/xmccm_exm  ! APPLIED CURRENT
              elcur(inode,2) = 0.0_rp !/xmccm_exm
           end if  

           if(kfl_spmod_exm(pmate)==0 .or. kfl_spmod_exm(pmate)==11) then !NO_MODEL or FHN
                
              do igate=1,ngate_exm
                 elgat(igate,inode,1) = refhn_exm(ipoin,1)    ! Recovery potential
              end do
           end if

           do idime=1,ndime
              elcod(idime,inode)=coord(idime,ipoin)
           end do
           
           if( kfl_coupl(ID_SOLIDZ,ID_EXMEDI) >= 1 .or. kfl_coupl(ID_EXMEDI,ID_SOLIDZ) >=1 ) then
           !if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
              if (kfl_gcoup_exm == 1) then
                 do idime=1,ndime
                    elcod(idime,inode)= elcod(idime,inode) + displ(idime,ipoin,1)
                    eldis(idime,inode)= displ(idime,ipoin,1)
                    elfib(idime,inode)= fiber(idime,ipoin)
                 end do
              end if
           end if

        end do

        ! elmag(...,1) working step n 
        ! elmag(...,2) step n, iteration i
        ! elmag(...,3) stored step n 
        ! elmag(...,4) n - 1
        ! elmag(...,5) n - 2
        ! elmag(...,6) n - 3 ...

        if (kfl_spmod_exm(pmate)==0 .or. kfl_spmod_exm(pmate)==11) then   ! NO_MODEL or FHN 
           do inode=1,pnode
              ipoin=lnods(inode,ielem)
              eliap(inode,2) = elmag(1,ipoin,4)
              elcur(inode,2) = appfi_exm(ipoin,3)
           end do
        end if

        ! Cartesian derivatives and Jacobian
        gauss_points_cartesian: do igaus=1,ngaus(pelty)
           call elmder(pnode,ndime,elmar(pelty)%deriv(1,1,igaus),elcod,&
                gpcar(1,1,igaus),gpdet(igaus),xjacm(1,1,igaus),xjaci(1,1,igaus))
           dvolu(igaus)=elmar(pelty)%weigp(igaus)*gpdet(igaus)
        end do gauss_points_cartesian

        ! Compute extra and intracellular conductivity matrices        
         if( kfl_coupl(ID_SOLIDZ,ID_EXMEDI) >= 1 .or. kfl_coupl(ID_EXMEDI,ID_SOLIDZ) >=1 ) then
        !if(( coupling('SOLIDZ','EXMEDI') >= 1 ) .or. ( coupling('EXMEDI','SOLIDZ') >= 1 )) then
           if (kfl_gcoup_exm == 1) then
              if (ittim == 1) then ! the first time gpfib is still zero
                 call exm_comcnd(ielem,pmate,cndin,noion)
              else
                                  
                 do igaus=1,ngaus(pelty)
                    nfibe_length=0.0_rp

                    do idime=1,ndime
                       nfibe(idime,igaus)= gpfib(idime,igaus,ielem)
                       nfibe_length= nfibe_length + nfibe(idime,igaus) * nfibe(idime,igaus)
                    end do

                    nfibe_length= sqrt(nfibe_length)

                    do idime=1,ndime
                       nfibe(idime,igaus)= nfibe(idime,igaus) / nfibe_length
                    end do

                    call exm_coucnd(pmate,cndin(1,1,igaus),nfibe(1,igaus),noion)

                 end do

              end if

           else

              call exm_comcnd(ielem,pmate,cndin,noion)

           end if

        else
           call exm_comcnd(ielem,pmate,cndin,noion)
        end if

        igacn = 1
        gauss_points: do igaus=1,ngaus(pelty)

           if (kfl_prdef_exm == 2) igacn=igaus   ! for nodal-field conductivity models

           wmatr = 0.0_rp
           wreco = 0.0_rp
           wnoli = 0.0_rp        
           wapcu = 0.0_rp        

           griap = 0.0_rp        

           xgate = 0.0_rp

           if(llapl(pelty)==1) & 
                call elmhes(elmar(pelty)%heslo(1,1,igaus),hessi,ndime,pnode,ntens, &
                xjaci,d2sdx,elmar(pelty)%deriv,elcod)

           ! Previous time step unknown         
           xpapp = 0.0_rp
           unlst = 0.0_rp
           unprv = 0.0_rp

           do inode=1,pnode
              gpsha= elmar(pelty)%shape(inode,igaus)
              ! Applied currents Iapp (at THIS timestep)
              !xpapp = xpapp + elcur(inode,1) * gpsha           
              ! Last unknown iap (at time n)
              unlst = unlst + eliap(inode,1) * gpsha  !eliap is elmag(1)
              ! Previous unknown iap (at time n-1)
              unprv = unprv + eliap(inode,2) * gpsha !eliap is elmag(4) 
           end do

           ! volume integral of eliap
           voaux(1) = voaux(1) + unlst * dvolu(igaus)

           xpold = (pdccm_exm * ximol* unlst) / dtime !* ximol  is zero for TThet

           xmion = 0.0_rp
           xlian = 0.0_rp
           xdtin = pdccm_exm / dtime


           if (kfl_spmod_exm(pmate)==0) then  ! for NO_MODEL
                !Nothing happens.
           elseif (kfl_spmod_exm(pmate)==11) then  ! for FHN
                do inode=1,pnode
                    gpsha= elmar(pelty)%shape(inode,igaus)
                    ! Recovery potential w
                    do igate=1,ngate_exm
                       xgate(igate) = xgate(igate) + elgat(igate,inode,1) * gpsha
                    end do
                    
                    ! Ionic current (FHN function, non-linear in the intracellular a.p.) Iion (at THIS time step)
                   xlian = xlian + elian(inode,1) * gpsha
                end do
                
                ! Matrix contribution of both the FHN and the time derivative
                xmion = xdtin + fnol2 * taufi * (xlian - 1.0_rp) * (xlian - xmalp)
                
                if (kfl_nolim_exm == 1) then
                   xmion= xdtin - &
                        (fact1 * 0.5_rp + fact2 * xlian + fact3 * xlian * xlian * 1.5_rp) * fnol2
                end if

           !else if (kfl_spmod_exm(pmate).ne.0 .and. kfl_spmod_exm(pmate).ne.11) then !  Cell models
           !     xioni = 0.0_rp
           !     do inode=1,pnode
           !        gpsha= elmar(pelty)%shape(inode,igaus)
           !        xioni = xioni + eliap(inode,1) * gpsha
           !     end do
           end if

           ! Intracellular action potential gradients 
           do idime=1,ndime
              do inode=1,pnode
                 griap(idime)=griap(idime) + gpcar(idime,inode,igaus) * eliap(inode,1)
              end do
           end do


           ! Intracellular action potential gradients contributions to the RHS
           do idime=1,ndime
              grdif(idime)=0.0_rp
              grdif(idime) = cndin(1,idime,igacn) * griap(1) + cndin(2,idime,igacn) * griap(2)
              if (ndime == 3) grdif(idime) = grdif(idime) + cndin(3,idime,igacn) * griap(3)
           end do

           if (kfl_spmod_exm(pmate) == 0) then                        !no ionic model
              do inode=1,pnode
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 ! (M w ) terms
                 wreco(inode) = 0.0_rp
                 ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  * (xpold + xpapp)  ! 
                 ! (- M Iion) term
                 wnoli(inode) = 0.0_rp
              end do

           else if (kfl_spmod_exm(pmate) == 11) then                  !fitzhugh nagumo
              do inode=1,pnode
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 ! (M w ) terms
                 wreco(inode) = wreco(inode) +  gpsha  * (- xgate(1))
            
                 !
                 !CODE IN TEST
                 !
                 !wreco(inode)=0.0_rp
                 !
                 ! END CODE IN TEST
                 !
    
                 ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  * (xpold +xpapp)  ! 
              end do

           else if (kfl_spmod_exm(pmate) == 12) then                 !fenton           
              do inode=1,pnode
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  * (xpold +xpapp)  ! 
              end do

           else if (kfl_spmod_exm(pmate) == 1) then                 !Ten Tuscher           
              do inode=1,pnode
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 !  (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  *  (xpold +xpapp)  ! 
                 ! (- M Iion) term, i.e. the Fenton ionic term
                 wnoli(inode) = wnoli(inode) +  gpsha  *  xioni
              end do

           else if (kfl_spmod_exm(pmate) == 4 .or. kfl_spmod_exm(pmate) == 6) then                 !Ten Tuscher Heterogeneous          
              do inode=1,pnode
                 wreco(inode) = 0.0_rp
                 gpsha= elmar(pelty)%shape(inode,igaus)
                 ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                 wapcu(inode) = wapcu(inode) +  gpsha  * (xpapp + xpold)  ! 
                 ! (- M Iion) term, i.e. the Fenton ionic term
                 wnoli(inode) = wnoli(inode) +  gpsha  *  xioni

              end do

           else if (kfl_spmod_exm(pmate) == 5) then                 !O'Hara-Rudy          
              do inode=1,pnode
                gpsha= elmar(pelty)%shape(inode,igaus)
                ! (M Iapp) terms, or (Cm M v / dt + M Iapp) for implicit algorithms
                wapcu(inode) = wapcu(inode) +  gpsha  *  (xpapp+xpold )!! 
                ! (- M Iion) term, i.e. the Fenton ionic term
                wnoli(inode) = wnoli(inode) +  gpsha  *  xioni
              end do
           end if

           !
           ! Elementary RHS contribution from each gauss point:
           !
           ! RHS  =  - K (v+u)   +   M w   +   M Iapp  -  M Iion

           do inode=1,pnode
              wdiff=0.0_rp

              ! (K u) term: Extracellular action potential weak laplacian 
              do idime=1,ndime
                 wdiff = wdiff + gpcar(idime,inode,igaus) * grdif(idime)
              end do

              if (kfl_spmod_exm(pmate) == 0 .or. kfl_spmod_exm(pmate) == 11) then !NO_MOD and FHN


                 esili(inode) = esili(inode) &
                      + dvolu(igaus) * (     - wdiff + wapcu(inode) )       ! -  K (v+u) + M Iapp
                 enoli(inode) = enoli(inode) &
                      + dvolu(igaus) * ( wnoli(inode)+ wreco(inode) )       ! -  M Iion  + M w
                 !
                 !CODE IN TEST
                 !
                 !enoli(inode)=0.0_rp
                 !
                 ! END CODE IN TEST
                 !
              !

              else if (kfl_spmod_exm(pmate) .ne. 0 .and. kfl_spmod_exm(pmate) .ne. 11) then  !cell Models
                 esili(inode) = esili(inode) &
                      + dvolu(igaus) * (     - wdiff + wapcu(inode) )       ! -  K (v+u) + M Iapp!!  

                 enoli(inode) = enoli(inode) &
                      + dvolu(igaus) * ( wnoli(inode) )       ! -  M Iion
              end if


           end do

        end do gauss_points


        ! Prescribe Dirichlet boundary conditions
        if (kfl_exboc_exm == 1) then
           do inode=1,pnode
              ipoin=lnods(inode,ielem)
              if(kfl_fixno_exm(1,ipoin)==1) then
                 adiag=elmat(inode,inode)
                 do jnode=1,pnode
                    elmat(inode,jnode)=0.0_rp
                    esili(jnode)=esili(jnode)-elmat(jnode,inode)*bvess_exm(1,ipoin)
                    elmat(jnode,inode)=0.0_rp
                 end do
                 elmat(inode,inode)=adiag
                 esili(inode)=adiag*bvess_exm(1,ipoin)
                 enoli(inode)= 0.0_rp
              end if
           end do
        end if


        if( lelch(ielem) == ELEXT ) then
           inode = 1
           ipoin = lnods(inode,ielem)
           rhsid(ipoin)       = rhsid(ipoin) + ( esili(inode) + enoli(inode) ) 
        else
           ! Assembly
           do inode=1,pnode
              ipoin=lnods(inode,ielem)
              rhsid(ipoin)       = rhsid(ipoin) + ( esili(inode) + enoli(inode) )  ! Complete
           end do
        end if

     end if


     ecgvo_exm(1) = ecgvo_exm(1) + voaux(1)


  end do elements

end subroutine exm_iapelmnew
