!------------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_odeite.f90
!> @date    08/04/2015
!> @author  Alfonso Santiago The Great
!> @brief   Computes the EDO for the recuperation of the fhn potential
!> @details Computes the EDO for the recuperation of the fhn potential
!> @}
!
! Equation solved in the subroutine:
!
!                   dW/dt=  C_2*\epsilon*(\phi+\gamma*W)
! Where:
!       C_2         => chi constant. Multiplies \epsilon in the FHN model.
!                      Generally it is equal to one
!       \epsilon    => epsilon constant. Same effect as chi (C_2). Repeated
!                      to be consistent with the model
!       \gamma      => Gamma constant. multiplies the variable to be solved.
!
!------------------------------------------------------------------------
subroutine exm_odeite
  use def_exmedi
  use def_master
  use def_parame
  use def_domain

  implicit none

  integer (ip)  :: ipoin, & ! index for the nodes
                   igate    ! Gate index
                   
  real   (rp)   :: xmeps, & ! epsilon constant
                   xchic, & ! chi constant
                   xmgam, & ! gamma constant
                   epsch, & ! epsilon times chi
                   xfact, & ! factor used in roselen formulation
                   xfacv    ! factor used in roselen formulation
                   
if(INOTMASTER) then
  !!!!
  ! Calculation of the new recovery pòtential
  !!!!
  nodeloop: do ipoin=1,npoin

            if (kfl_spmod_exm(nomat_exm(ipoin) ) == 0) then
            !
            ! No ionic current model (NOMOD)
            !
                cycle nodeloop 

            elseif (kfl_spmod_exm(nomat_exm(ipoin) )  == 11) then
            !
            !FHN ionic curren model
            !
                if (.not.(exm_zonenode(ipoin))) cycle nodeloop

                xmeps = xmopa_exm( 1,nomat_exm(ipoin))
                xchic = xmopa_exm( 2,nomat_exm(ipoin))
                xmgam = xmopa_exm( 4,nomat_exm(ipoin))

                epsch = xmeps * xchic

                xfact = 1.0_rp + 0.5_rp * epsch * xmgam * dtime / xmccm_exm

                !!!!
                ! Original FHN formulation
                !!!! 
                !refhn_exm(ipoin,1) =  refhn_exm(ipoin,2)&
                !                       + dtime * epsch&
                !                       * (elmag(1,ipoin,2) - xmgam * refhn_exm(ipoin,2) ) 
                !!!!!
                ! END_ Original FHN formulation
                !!!!

                !!!!
                ! Adrian Roselen formulation
                !!!!
                xfacv = elmag(1,ipoin,1) - 0.5_rp * xmgam * refhn_exm(ipoin,2)         
                unkno(ipoin) = ( refhn_exm(ipoin,2)&
                                       +  dtime / xmccm_exm * epsch * xfacv ) / xfact
                !!!!
                ! END_ Adrian Roselen formulation
                !!!!

            elseif (kfl_spmod_exm(nomat_exm(ipoin) )  == 12) then
            !
            !FK ionic curren model
            !
                call runend('EXM_ODEITE: Fenton-Karma model not programmed.')

            endif

  enddo nodeloop
  !!!!
  ! END_ Calculation of the new recovery potential
  !!!!

  !!!!
  ! Compute the iteration update
  !!!!
  ! Description: put the new one in the old one. Analog to exm_updunk(four)
  !
  ! Assign U(n,i-1,*) <-- U(n,i,*)
  !        
  do ipoin=1,npoin
        if (.not.(exm_zonenode(ipoin))) cycle           
        refhn_exm(ipoin,1) = unkno(ipoin)
  end do
  !!!!
  ! END_ Compute the iteration update
  !!!!

endif

  
end subroutine exm_odeite
