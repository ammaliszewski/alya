subroutine exm_iapsol
!-----------------------------------------------------------------------
!   
! This routine loads the solver data for the intracellular / transmembrane 
! action potential.
! In general, it may change from time step to time step or even
! from iteration to iteration.
!
!-----------------------------------------------------------------------
  use      def_domain
  use      def_exmedi
  use      def_solver
  implicit none
  
end subroutine exm_iapsol
