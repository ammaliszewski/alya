subroutine exm_iapupd

!-----------------------------------------------------------------------
!
! This routine updates the intracellular / transmembrane
! action potential by performing
! non-linear iterations
!
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solver

  use      def_exmedi

  implicit none
  integer(ip) :: inoli,ipoin,iicel,imate
  real(rp)    :: xnoli,xdeno,xnumi,xreln,xrelo,xauxi,xlian,xpfhn,xpfen,xpten,xpion
  real(rp)    :: xdumy,dtimeEP
  real(rp)    :: cpu_refe1,cpu_refe2

  !
  ! Initializations
  !
 
  amatr     = 0.0_rp
  rhsid     = 0.0_rp
  cpu_solve = 0.0_rp
  call cputim(cpu_refe1)

  inoli = 1
  xnoli = tnoli_exm + 2._rp
  
 ! call exm_updunk(10)
  ! Update the intracellular action potential 

  if (kfl_genal_exm == 1) then             

     ! kfl_genal_exm ==  1 :  
     ! 
     ! -->> Explicit algorithm 
     ! Explicit advance of the intracellular / transmembrane potential
     ! 
     
     ! xreln = 1: no relaxation
    ! xreln= 1.0_rp
    ! xrelo= 0.0_rp
    xreln = 0.5_rp
    xrelo = 0.5_rp

    !elmag(1,1:npoin,2) = elmag(1,1:npoin,1)
    

    !! ESTO ESTA SUPER FATAL!!!! HABRA QUE CAMBIARLO, NO PUEDE SER
    if(kfl_gemod_exm==0) then
        dtimeEP = dtime
    else
        dtimeEP = dtime * 1000.0_rp  !! change from seconds to milliseconds
    end if

     !
     ! Non-linear iterations
 
!     do while ((inoli <= mnoli_exm) .and. (tnoli_exm <= xnoli))

        !
        ! RHS construction
!        call exm_iapelm           
        call exm_iapelmnew 
        
        !
        !    residual interchange in parall (rhsid)
        call exm_parall(three)
        
        !

        ! Time advance
                   
        if (kfl_nolum_exm == 0) then         ! non-linear terms are "spacialized"

           do ipoin=1,npoin              
              unkno(ipoin) = elmag(1,ipoin,1) +  dtimeEP / pdccm_exm * rhsid(ipoin) / vmass(ipoin) 
           end do

        else if (kfl_nolum_exm == 1) then    ! lumped non-linear terms REPASSAR!!!!!!!!!
           if (kfl_gemod_exm == 0) then

              do ipoin=1,npoin              
                 xlian= elmag(1,ipoin,1)                        
                 !call exm_ionicu(ipoin,xdumy,xlian,xdumy,xpfhn,xpfen,xpten)    
                 call exm_ionicunew(ipoin,xdumy,xlian,xpion,xpfhn,xpfen,xpten)


!!!!
! Original FHN formulation
!!!!
                ! xlian =  elmag(1,ipoin,1) &
                !          + (dtimeEP / pdccm_exm )&
                !          * ( rhsid(ipoin) / vmass(ipoin) + xpion)!  - refhn_exm(ipoin,1) )
                ! unkno(ipoin) = xlian
!!!!
! END_ Original FHN formulation
!!!!

!!!!
! Roselen Formulation, capable of an implicit scheme
!!!!

                 xlian =  elmag(1,ipoin,1) +  dtimeEP / xmccm_exm * ( xpfen + xpten + rhsid(ipoin) / vmass(ipoin) ) 
                 unkno(ipoin) = xlian / xpfhn
!!!!
! END_ Roselen Formulation, capable of an implicit scheme
!!!!

              end do

           else ! CELL MODEL
              do ipoin=1,npoin                       
                  xlian =  elmag(1,ipoin,1) +  ( rhsid(ipoin) / vmass(ipoin) )* ( dtimeEP/pdccm_exm)  !  *
                  unkno(ipoin) = xlian
              end do
           end if
        end if
       
 
        do ipoin=1,npoin
!! ojo_yo
           elmag(1,ipoin,2) = xrelo * elmag(1,ipoin,2) + xreln * unkno(ipoin)
           unkno(ipoin) = xrelo * elmag(1,ipoin,1) + xreln * unkno(ipoin)
!! end_ojo_yo
        end do

        xnumi=0.0_rp
        xdeno=0.0_rp
        do ipoin=1,npoin
           xauxi= rhsid(ipoin) - unkno(ipoin)
           xnumi= xnumi + xauxi*xauxi
           xauxi= unkno(ipoin)
           xdeno= xdeno + xauxi*xauxi
        end do

!        xnoli= xnumi 
!        if (xdeno > 0.0_rp) xnoli=xnoli/xdeno
!        inoli = inoli + 1
!        write(6,*) inoli, xdeno, xnumi
        
!     end do
       
  else if (kfl_genal_exm == 2) then
     
     ! kfl_genal_exm ==  2 :  
     ! 
     ! -->> Decoupled implicit algorithm
     ! Implicit advance of the intracellular / transmembrane potential
     ! 
     
     elmag(1,1:npoin,2) = elmag(1,1:npoin,1)

!     xreln= 0.5_rp
!     xrelo= 0.5_rp

     xreln= 1.0_rp
     xrelo= 0.0_rp

     do while ((inoli <= mnoli_exm) .and. (tnoli_exm <= xnoli))
 

        !
        ! Set up the solver parameters
        !
        
        call exm_iapsol        
        
        !
        ! RHS and matrix construction
        
!        call exm_iapelm
        call exm_iapelmnew
        !
        !    residual interchange in parall
        call exm_parall(three)
           
        
        !
        ! Solve the system
        
        unkno(1:npoin) = elmag(1,1:npoin,1)
   
        call exm_solsys
        
        xnumi=0.0_rp
        xdeno=0.0_rp
        do ipoin=1,npoin
           xauxi= elmag(1,ipoin,2) - unkno(ipoin)
           xnumi= xnumi + xauxi*xauxi
           xauxi= unkno(ipoin)
           xdeno= xdeno + xauxi*xauxi
        end do
        
        xnoli=xnumi/xdeno
        
        
        elmag(1,1:npoin,2) = xrelo * elmag(1,1:npoin,2) + xreln * unkno(1:npoin)
        
        !        xreln= xreln + (xreln-xrelo)/real(mnoli_exm+1)
        !        xrelo= xrelo - (xreln-xrelo)/real(mnoli_exm+1)
        
        inoli = inoli + 1
        
     end do

  end if

  call cputim(cpu_refe2)
  cpu_exmed(1) = cpu_exmed(1) + cpu_refe2 - cpu_refe1 
  cpu_exmed(2) = cpu_exmed(2) + cpu_solve


end subroutine exm_iapupd
