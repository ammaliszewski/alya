!-----------------------------------------------------------------------
!> @addtogroup Exmedi
!> @{
!> @file    exm_ceconc.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Compute ODEs corresponding to concentration variables of Ten Tusscher model (TT). 
!> @details Compute ODEs corresponding to concentration variables of Ten Tusscher model (TT). 
!> @} 
!-----------------------------------------------------------------------
subroutine exm_ceconc
!-----------------------------------------------------------------------
!
! The auxiliary variables are 8:
!   - caitot:  Ca_i_tot    -->  vconc(1,ipoin,1)  -->  
! Concentration of total calcium in the Cytoplasm  (Page H1581 TT paper)
!                                                             (caitot = caifre + caibuf)
!   - casrto:  Ca_sr_tot   -->  vconc(2,ipoin,1)  -->  
!              Concentration of total calcium in the Sarcoplasmic Reticulum SR  (Page H1581 TT paper)
!                                                             (casrto = casrfr + casrbu)
!   - naitot:  Na_i_tot    -->  vconc(3,ipoin,1)  -->  
!              Intracellular Sodium Concentration (Page H1581 TT paper)
!   - kitot:   K_i_tot     -->  vconc(4,ipoin,1)  -->  
!              Intracellular Potassium Concentration (Page H1581 TT paper)
!   - caifre:  Ca_i_free   -->  vconc(5,ipoin,1)  -->  
!              Concentration of free calcium in the Cytoplasm  (Page H1581 TT paper)
!   - caibuf:  Ca_i_buff   -->  vconc(6,ipoin,1)  -->  
!              Concentration of buffered calcium in the Cytoplasm  (Page H1581 TT paper)
!   - casrfr:  Ca_sr_free  -->  vconc(7,ipoin,1)  -->  
!              Concentration of free calcium in the Sarcoplasmic Reticulum SR  (Page H1581 TT paper)
!   - casrbu:  Ca_sr_buff  -->  vconc(8,ipoin,1)  -->  
!              Concentration of buffered calcium in the Sarcoplasmic Reticulum SR  (Page H1581 TT paper)
!
! The order is important because it is related with the order of nconc_exm variables (exm_memall.f90)
!
!-----------------------------------------------------------------------

  use      def_parame
  use      def_master
  use      def_elmtyp
  use      def_domain
  use      def_exmedi

  implicit none


! DEFINITION OF VARIABLES

  integer(ip) :: ipoin
  integer(ip) :: ielem,inode,pnode,pmate,ituss,pelty
  real(rp)    :: caitot, casrto, naitot, kitot, caifre, caibuf, casrfr, casrbu

  real(rp)    :: vaux1, vaux2, vaux3

  real(rp)    :: rhsx1, rhsx2, rhsx, val0


! DESCRIPTION OF VARIABLES

! ipoin: point number (it is related with a node)
! nmait: maximum number of iterations used for computation of activation variables 
!       (it is related with non-linearity)
! imait: auxiliary variable used as counter of iterations

! caitot, casrto, naitot, kitot, caifre, caibuf, casrfr, casrbu: concentrations

! vaux1, vaux2, vaux3: auxiliar variables 1, 2 and 3

! rhsx: variable used by each activation variable for computation of Right Hand Side (dy/dt=F(t,y)=RHS)
! rhsx1: auxiliary variable used for computation of rhsx
! rhsx2: auxiliary variable used for computation of rhsx
! val0: value of activation variable in the previous time step






! DESCRIPTION OF CONSTANTS (PARAMETERS)

! PARAMETERS of Ten Tusscher model for concentrations
! These constants (page H1574 of TT paper) are used for computation of ionic currents

  real(rp)   ::  volcyto, volsr, capacit
  real(rp)   ::  bufcyto, kbufcyto
  real(rp)   ::  bufsr, kbufsr
  real(rp)   ::  faradco, dtimeEP

dtimeEP=dtime  !*1000.0_rp

! COMPUTATION of concentrations

     do ielem=1,nelem
        pelty = ltype(ielem)
 
        if ( pelty > 0 .and. lelch(ielem) /= ELCNT ) then
           pnode = nnode(pelty)
           pmate = 1                       
           if( nmate > 1 ) then
              pmate = lmate_exm(ielem)
           end if
 
           do inode= 1,pnode
              ipoin= lnods(inode,ielem)     
              ituss = pmate

            
              volcyto = cupar_exm(1,pmate)%value(7)      ! V_c [nL] --> cytoplasmic volume
              volsr = cupar_exm(1,pmate)%value(8)        ! V_SR [nL] --> sarcoplasmic reticulum (SR) volume
              capacit = cupar_exm(1,pmate)%value(9)      ! C [nF] --> Capacitance used in Ca_i_tot, Na_i_toy and K_i_tot
              
              bufcyto = cupar_exm(17,pmate)%value(1)     ! Buf_c [mM] --> total cytoplasmic buffer concentration
              kbufcyto = cupar_exm(17,pmate)%value(2)    ! K_Bufc [mM] --> Cai half-saturation constant for cytoplasmic buffer
            
              bufsr = cupar_exm(17,pmate)%value(3)       ! Buf_sr [mM] --> total sarcoplasmic buffer concentration
              kbufsr = cupar_exm(17,pmate)%value(4)      ! K_Bufsr [mM] --> Ca_SR half-saturation const. of sarcoplasmic buffer
            
              faradco = cupar_exm(1,pmate)%value(3)      ! F [C/mol] --> faraday constant

        ! Concentration of total calcium in the Cytoplasm (Ca_i_tot)
        ! nconc_exm = 1
        ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
        
        ! d_caitot/d_t = - (I_CaL + I_bCa + I_pCa - 2 * I_NaCa) * C / (2 * V_c * F) + I_leak - I_up + I_rel
        
             vaux1 = - 2.0_rp * volcyto * faradco
             vaux1 = 1.0_rp / vaux1 * capacit
        
             vaux3 = vicel_exm(2,ipoin,1) + vicel_exm(12,ipoin,1) &
                  + vicel_exm(9,ipoin,1) - 2.0_rp *  vicel_exm(7,ipoin,1) 
             vaux3 = vaux1 * vaux3
        
             rhsx = vaux3 + vicel_exm(13,ipoin,1) - vicel_exm(14,ipoin,1) + vicel_exm(15,ipoin,1)
        
             val0 = vconc(1,ipoin,3)
             caitot = val0 + dtimeEP/xmccm_exm * rhsx
        
             vconc(1,ipoin,1) = caitot       ! Value of Ca_i_tot concentration
        
        
        
        ! Concentration of total calcium in the Sarcoplasmic Reticulum (Ca_sr_tot)
        ! nconc_exm = 2
        ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
        
        ! d_casrto/d_t = (V_c / V_sr) * (-I_leak + I_up - I_rel)
        
             vaux1 = volcyto / volsr
        
             rhsx = vaux1 * (-vicel_exm(13,ipoin,1) + vicel_exm(14,ipoin,1) - vicel_exm(15,ipoin,1))
        
             val0 = vconc(2,ipoin,3)
             casrto = val0 + dtimeEP/xmccm_exm * rhsx
        
             vconc(2,ipoin,1) = casrto       ! Value of Ca_sr_tot concentration
        
        
        
        ! Intracellular sodium concentration (Na_i_tot)
        ! nconc_exm = 3
        ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
        
        ! d_naitot/d_t = - (I_Na + I_bNa + 3 * I_NaK + 3 * I_NaCa) * C / (V_c * F)
        
             vaux1 = - volcyto * faradco
             vaux1 = 1.0_rp / vaux1 * capacit
        
             vaux3 = vicel_exm(1,ipoin,1) + vicel_exm(11,ipoin,1) &
                  + 3.0_rp * vicel_exm(8,ipoin,1) + 3.0_rp * vicel_exm(7,ipoin,1) 
             rhsx = vaux1 * vaux3
        
             val0 = vconc(3,ipoin,3)
             naitot = val0 + dtimeEP/xmccm_exm * rhsx
        
             vconc(3,ipoin,1) = naitot       ! Value of Na_i_tot concentration
        
        
        
        ! Intracellular potassium concentration (K_i_tot)
        ! nconc_exm = 4
        ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
        
        ! d_kitot/d_t = - (I_K1 + I_to + I_Kr + I_Ks - 2 * I_NaK + I_pK + I_stim - I_ax) * C / (V_c * F)
        
             vaux1 = - volcyto * faradco
             vaux1 = 1.0_rp / vaux1 * capacit
        
             vaux3 = vicel_exm(6,ipoin,1) + vicel_exm(3,ipoin,1) &
                  +  vicel_exm(5,ipoin,1) + vicel_exm(4,ipoin,1) - 2.0_rp * vicel_exm(8,ipoin,1) 
             vaux3 = vaux3 + vicel_exm(10,ipoin,1) + vicel_exm(16,ipoin,1)
        
             rhsx = vaux1 * vaux3
        
             val0 = vconc(4,ipoin,3)
        
             kitot = val0 + dtimeEP/xmccm_exm * rhsx
             vconc(4,ipoin,1) = kitot          ! Value of K_i_tot concentration
        
        ! Concentration of free calcium in the cytoplasm  (Ca_i_free)
        ! nconc_exm = 5
        ! "A model for ventricular tissue", Ten Tusscher, Page H1581
        
             vaux1 = bufcyto + kbufcyto - caitot
             vaux2 = vaux1 * vaux1
             vaux3 = sqrt(vaux2 + 4.0_rp * caitot * kbufcyto)
        
             caifre = 0.5_rp * (-vaux1 + vaux3)
        
             vconc(5,ipoin,1) = caifre          ! Value of Ca_i_free concentration
        
        
        
        ! Concentration of buffered calcium in the cytoplasm  (Ca_i_buff)
        ! nconc_exm = 6
        ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
        
             vaux1 = caifre + kbufcyto
             vaux1 = 1.0_rp / vaux1
             vaux2 = caifre * bufcyto
        
             caibuf = vaux1 * vaux2
         
             vconc(6,ipoin,1) = caibuf         ! Value of Ca_i_buf concentration
        
        
        
        ! Concentration of free calcium in the sarcoplasmic reticulum SR (Ca_sr_free)
        ! nconc_exm = 7
        ! "A model for ventricular tissue", Ten Tusscher, Page H1581
        
             vaux1 = bufsr + kbufsr - casrto
             vaux2 = vaux1 * vaux1
             vaux3 = sqrt(vaux2 + 4.0_rp * casrto * kbufsr)
        
             casrfr = 0.5_rp * (-vaux1 + vaux3)
        
             vconc(7,ipoin,1) = casrfr         ! Value of Ca_sr_free concentration
        
        
        
        ! Concentration of buffered calcium in the sarcoplasmic reticulum  (Ca_sr_buff)
        ! nconc_exm = 8
        ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 2)
        
             vaux1 = casrfr + kbufsr
             vaux1 = 1.0_rp / vaux1
             vaux2 = casrfr * bufsr
        
             casrbu = vaux1 * vaux2
         
             vconc(8,ipoin,1) = casrbu         ! Value of Ca_sr_buf concentration
        end do
    end if
  end do

end subroutine exm_ceconc
