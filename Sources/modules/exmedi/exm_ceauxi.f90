subroutine exm_ceauxi
!-----------------------------------------------------------------------
!***** Exmedi/exm_ceauxi
! NAME 
!    exm_ceauxi
! DESCRIPTION
!   This routine computes ODEs corresponding to activation/inactivation
!   variables of Ten Tusscher model (TT). 
!   These variables are used as auxiliary variables for the computation 
!   of cell ionic currents.
!   All the variables are DIMENSIONLESS.
! USES
!   def_parame
!   def_master
!   def_domain
!   def_exmedi
! USED BY
!    exm_doiter
!***
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!
! The ODEs of this routine respond to Hodgkin-Huxley formalism:
!
!     du / dt = (u_inf - u) / tau     
!
! u represents activation/inactivation variable 
! u_inf and tau have to been specified in order to solve the equation (u_inf and tau are voltage-dependent)
!
! The auxiliary variables are 12:
!   - m:     vauxi_exm(1,ipoin,1)   -->  activation gate (for I_Na current, page H1575 TT paper)
!   - h:     vauxi_exm(2,ipoin,1)   -->  fast inactivation gate (for I_Na current, page H1575 TT paper)
!   - j:     vauxi_exm(3,ipoin,1)   -->  slow inactivation gate (for I_Na current, page H1575 TT paper)
!   - d:     vauxi_exm(4,ipoin,1)   -->  
!            voltage-dependent activation gate (for I_CaL current, page H1576 TT paper)
!   - f:     vauxi_exm(5,ipoin,1)   -->  
!            voltage-dependent inactivation gate (for I_CaL current, page H1576 TT paper)
!   - fca:   vauxi_exm(6,ipoin,1)   -->  
!            intracellular calcium-dependent inactivation gate (for I_CaL current, page H1576 TT paper)
!   - r:     vauxi_exm(7,ipoin,1)   -->  
!            voltage-dependent activation gate (for I_to current, page H1578 TT paper)
!   - s:     vauxi_exm(8,ipoin,1)   -->  
!            voltage-dependent inactivation gate (for I_to current, page H1578 TT paper)
!   - xs:    vauxi_exm(9,ipoin,1)   -->  activation gate (for I_Ks current, page H1578 TT paper)
!   - xr1:   vauxi_exm(10,ipoin,1)  -->  activation gate (for I_Kr current, page H1579 TT paper)
!   - xr2:   vauxi_exm(11,ipoin,1)  -->  inactivation gate (for I_Kr current, page H1579 TT paper)
!   - g:     vauxi_exm(12,ipoin,1)  -->  
!            calcium-dependent inactivation gate (for I_rel current, page H1581 TT paper)
!
! The order is important because it is related with the order of nauxi_exm variables (exm_memall.f90)
!
! PARA HACER:
! PARA HACER:
! PARA HACER:
!
!   definir tipocelula (en la malla) necesario para calcular s
!
!-----------------------------------------------------------------------

  use      def_parame
  use      def_master
  use      def_elmtyp
  use      def_domain
  use      def_exmedi

  implicit none

  integer(ip) :: ipoin,kfl_odets_exm
  integer(ip) :: ielem,inode,pnode,pmate,ituss,pelty
  real(rp)    :: thold, thnew
  real(rp)    :: vinf, alphx, alph1, alph2, betax, beta1, beta2, gammax, taux, tau1, tau2, xitaux
  real(rp)    :: vaux0, vaux1, vaux2, vaux3, vaux4, vaux5  



! ESTO ESTA AGREGADO SOLamente PARA QUE COMPILE
! ESTO ESTA AGREGADO SOLamente PARA QUE COMPILE
! ESTO ESTA AGREGADO SOLamente PARA QUE COMPILE
! La variable tipocelula indica a que parte del musculo cardiaco pertenece la celula
  real(rp) :: tipocelula=0  !(0=epicardial, 1=endocardial, 2=M cells)


!write(*,*) ituss

! DESCRIPTION OF VARIABLES

! ipoin: point number (it is related with a node)

! thold: relaxation coefficient of old RHS used for computation of non-linear iteration
! thnew: relaxation coefficient of new RHS used for computation of non-linear iteration (thold+thnew=1)

! vinf: 'infinite' value of activation variable (depends on voltage) of Hodgkin-Huxley formalism (u_inf)
! alphx: alpha rate coefficient used by activation variables (Hodgkin-Huxley formalism)
! alph1: auxiliary rate coefficient used for computation of alphx
! alph2: auxiliary rate coefficient used for computation of alphx 
! betax: beta rate coefficient used by activation variables (Hodgkin-Huxley formalism)
! beta1: auxiliary rate coefficient used for computation of betax
! beta2: auxiliary rate coefficient used for computation of betax
! gammax: auxiliary variable used for computation of taux
! taux: time 'constant' (depends on voltage value) of Hodgkin-Huxley formalism
! tau1: auxiliary variable used for computation of taux
! tau2: auxiliary variable used for computation of taux
! xitaux: inverse of time 'constant' (depends on voltage value) of Hodgkin-Huxley formalism (tau)

! vaux0, vaux1, vaux2, vaux3, vaux4, vaux5: auxiliar variables 0, 1, 2, 3, 4 and 5

  kfl_odets_exm = 3

  thnew = 0.5_rp            
  thold = 1.0_rp - thnew       
 
     do ielem=1,nelem
        pelty = ltype(ielem)
 
        if ( pelty > 0 .and. lelch(ielem) /= ELCNT ) then
           pnode = nnode(pelty)
           pmate = 1                       
           if( nmate > 1 ) then
              pmate = lmate_exm(ielem)
           end if
 
           do inode= 1,pnode
              ipoin= lnods(inode,ielem)     
              ituss = pmate
  
! Variable of activation m (related with I_Na current)
! nauxi_exm = 1
! "A model for ventricular tissue", Ten Tusscher, Page H1585 (col 2)

                vinf = 1.0_rp + exp((-56.86_rp - elmag(1,ipoin,1)) / 9.03_rp)
                vinf = vinf * vinf
                vinf = 1.0_rp / vinf     ! Value of m_infinite
           
                alphx = 1.0_rp + exp((-60.00_rp - elmag(1,ipoin,1)) / 5.0_rp)
                alphx = 1.0_rp / alphx            ! Value of alpha_m
           
                beta1 = 1.0_rp + exp(( 35.00_rp + elmag(1,ipoin,1)) / 5.0_rp)
                beta1 = 0.1_rp / beta1
                beta2 = 1.0_rp + exp((-50.00_rp + elmag(1,ipoin,1)) / 200.0_rp)
                beta2 = 0.1_rp / beta2
                betax = beta1 + beta2             ! Value of beta_m
           
                taux  = betax * alphx             ! Value of tau_m
                xitaux = 1.0_rp / taux            ! Value of 1/tau_m 
           
                vaux0 = vauxi_exm(1,ipoin,3)      ! Value of m in previous time step 
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
                vauxi_exm(1,ipoin,1) = vaux3      ! Value of variable m
           
           
           
           ! Variable of activation h (related with I_Na current)
           ! nauxi_exm = 2
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
           
                vinf = 1.0_rp + exp((71.55_rp + elmag(1,ipoin,1)) / 7.43_rp)
                vinf = vinf * vinf
                vinf = 1.0_rp / vinf              ! Value of h_infinite
           
                if (elmag(1,ipoin,1) >= -40) then
           
                  alphx = 0                       ! Value of alpha_h
           
                  betax = 0.13_rp * ( 1.0_rp + exp((-10.66_rp - elmag(1,ipoin,1)) / 11.1_rp) )
                  betax = 0.77_rp / betax         ! Value of beta_h
           
                 else
           
                  alphx = 0.057_rp * exp((-80.00_rp - elmag(1,ipoin,1)) / 6.8_rp)        ! Value of alpha_h
           
                  beta1 = 2.7_rp * exp(0.079_rp * elmag(1,ipoin,1))
                  beta2 = 310000.0_rp * exp(0.3485_rp * elmag(1,ipoin,1))
                  betax = beta1 + beta2           ! Value of beta_h
           
                end if
           
                xitaux  = betax + alphx           ! Value of 1/tau_h
           
                vaux0 = vauxi_exm(2,ipoin,3)      ! Value of h in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(2,ipoin,1) = vaux3      ! Value of variable h
           
           
           
           
           ! Variable of activation j (related with I_Na current)
           ! nauxi_exm = 3
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
           
                vinf = 1.0_rp + exp((71.55_rp + elmag(1,ipoin,1)) / 7.43_rp)
                vinf = vinf * vinf
                vinf = 1.0_rp / vinf              ! Value of j_infinite
           
                if (elmag(1,ipoin,1) >= -40) then
           
                  alphx = 0                       ! Value of alpha_j
           
                  beta1 = 1.0_rp + exp(-0.1_rp * (32.0_rp + elmag(1,ipoin,1)))
                  beta1 = 1.0_rp / beta1
                  beta2 = 0.6_rp * exp(0.057_rp * elmag(1,ipoin,1))
                  betax = beta1 * beta2           ! Value of beta_j
           
                 else
           
                  alph1 = 1.0_rp + exp(0.311_rp * (79.23_rp + elmag(1,ipoin,1)))
                  alph1 = 1.0_rp / alph1
                  alph2 = - 25428.0_rp * exp(0.2444_rp * elmag(1,ipoin,1))
                  alph2 = alph2 - 0.000006948_rp * exp(-0.04391_rp * elmag(1,ipoin,1))
                  alph2 = alph2 * (elmag(1,ipoin,1) + 37.78_rp)
                  alphx = alph1 * alph2           ! Value of alpha_j
           
                  beta1 = 1.0_rp + exp(-0.1378_rp * (40.14_rp + elmag(1,ipoin,1)))
                  beta1 = 1.0_rp / beta1
                  beta2 = 0.02424_rp * exp(-0.01052_rp * elmag(1,ipoin,1))
                  betax = beta1 * beta2           ! Value of beta_j
           
                end if 
           
                xitaux  = betax + alphx           ! Value of 1/tau_j
             
                vaux0 = vauxi_exm(3,ipoin,3)      ! Value of j in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(3,ipoin,1) = vaux3      ! Value of variable j
           
           
           
           
           ! Variable of activation d (related with I_CaL current)
           ! nauxi_exm = 4
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
           
                vinf = 1.0_rp + exp((-5.0_rp - elmag(1,ipoin,1)) / 7.5_rp)
                vinf = 1.0_rp / vinf              ! Value of d_infinite
           
                alphx = 1.0_rp + exp((-35.0_rp - elmag(1,ipoin,1)) / 13.0_rp)
                alphx = 1.4_rp / alphx + 0.25_rp  ! Value of alpha_d
           
                betax = 1.0_rp + exp((5.0_rp + elmag(1,ipoin,1)) / 5.0_rp)
                betax = 1.4_rp / betax            ! Value of beta_d
           
                gammax = 1.0_rp + exp((50.0_rp - elmag(1,ipoin,1)) / 20.0_rp)
                gammax = 1.0_rp / gammax          ! Value of gamma_d
           
                taux = alphx * betax + gammax     ! Value of tau_d
                xitaux = 1.0_rp / taux            ! Value of 1/tau_d
                
                vaux0 = vauxi_exm(4,ipoin,3)      ! Value of d in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(4,ipoin,1) = vaux3      ! Value of variable d
           
           
           
           
           ! Variable of activation f (related with I_CaL current)
           ! nauxi_exm = 5
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 1)
           
                vinf = 1.0_rp + exp((20.0_rp + elmag(1,ipoin,1)) / 7.0_rp)
                vinf = 1.0_rp / vinf              ! Value of f_infinite
           
                tau1 = 1125.0_rp * exp(-(27.0_rp + elmag(1,ipoin,1)) * (27.0_rp + elmag(1,ipoin,1)) / 240.0_rp)
                tau2 = 1.0_rp + exp((25.0_rp - elmag(1,ipoin,1)) / 10.0_rp)
                tau2 = 165.0_rp / tau2
                taux = tau1 + tau2 + 80.0_rp      ! Value of tau_f
                xitaux = 1.0_rp / taux            ! Value of 1/tau_f
                
                vaux0 = vauxi_exm(5,ipoin,3)      ! Value of f in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(5,ipoin,1) = vaux3      ! Value of variable f
           
           
           
           
           ! Variable of activation fca (related with I_CaL current)
           ! nauxi_exm = 6
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
           
                alphx = (vconc(5,ipoin,1) / 0.000325_rp) * (vconc(5,ipoin,1) / 0.000325_rp)
                alphx = alphx * alphx
                alphx = alphx * alphx
                alphx = 1.0_rp + alphx 
                alphx = 1.0_rp / alphx            ! Value of alpha_fca
           
                betax = 1.0_rp + exp((-0.0005_rp + vconc(5,ipoin,1)) / 0.0001_rp)
                betax = 0.1_rp / betax            ! Value of beta_fca
           
                gammax = 1.0_rp + exp((-0.00075_rp + vconc(5,ipoin,1)) / 0.0008_rp)
                gammax = 0.2_rp / gammax            ! Value of gamma_fca
           
                vinf = (alphx + betax + gammax + 0.23_rp) / 1.46_rp         ! Value of fca_infinite
           
                taux = 2.0_rp                     ! Value of tau_fca [ms]
                xitaux = 0.5_rp                   ! Value of 1/tau_fca
                
           
                vaux0 = vauxi_exm(6,ipoin,3)      ! Value of fca in previous time step 
           
           
                if ( vinf > vaux0 .and. elmag(1,ipoin,1) > -60) then
           
                   vaux3 = vaux0
           
                else
           
                   ! Scheme for numerical integration
                   if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
                      
                      vaux1 = (vinf - vaux0) * xitaux;
                      vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                      vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                      vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                      vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                      vaux3 = vaux5
                      
                   else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
                      
                      vaux1 = dtime/xmccm_exm * xitaux
                      vaux2 = 1.0_rp + thnew * vaux1
                      vaux2 = 1.0_rp / vaux2
                      vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                      vaux3 = vaux3 * vaux2       
                      
                   else if (kfl_odets_exm == 3) then      ! Rush - Larsen
                      
                      vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
                      
                   end if
                   
                end if
           
           
               vauxi_exm(6,ipoin,1) = vaux3      ! Value of variable fca
           
           
           
           
           ! Variable of activation r (related with I_to current)
           ! nauxi_exm = 7
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
           
                vinf = 1.0_rp + exp((20.0_rp - elmag(1,ipoin,1)) / 6.0_rp)
                vinf = 1.0_rp / vinf              ! Value of r_infinite
            
                tau1 = 9.5_rp * exp(-(40.0_rp + elmag(1,ipoin,1)) * (40.0_rp + elmag(1,ipoin,1)) / 1800.0_rp)
                taux = tau1 + 0.8_rp              ! Value of tau_r
                xitaux = 1.0_rp / taux            ! Value of 1/tau_r
             
                vaux0 = vauxi_exm(7,ipoin,3)      ! Value of r in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(7,ipoin,1) = vaux3      ! Value of variable r
           
           
           
           
           ! Variable of activation s (related with I_to current)
           ! nauxi_exm = 8
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
           
           !           tipocelula==  !(0=epicardial, 1=endocardial, 2=M cells)
           
            
               if (tipocelula == 0)  then
           
                  vinf = 1.0_rp + exp((20.0_rp + elmag(1,ipoin,1)) / 5.0_rp)
                  vinf = 1.0_rp / vinf              ! Value of s_infinite
            
                  tau1 = 85.0_rp * exp(-(45.0_rp + elmag(1,ipoin,1)) * (45.0_rp + elmag(1,ipoin,1)) / 320.0_rp)
                  tau2 = 1.0_rp + exp((-20.0_rp + elmag(1,ipoin,1)) / 5.0_rp)
                  tau2 = 5.0_rp / tau2
                  taux = tau1 + tau2 + 3.0_rp       ! Value of tau_s
                  xitaux = 1.0_rp / taux            ! Value of 1/tau_s
           
               else if (tipocelula == 1) then
           
                  vinf = 1.0_rp + exp((28.0_rp + elmag(1,ipoin,1)) / 5.0_rp)
                  vinf = 1.0_rp / vinf              ! Value of s_infinite
            
                  tau1 = 1000.0_rp * exp(-(67.0_rp + elmag(1,ipoin,1)) * (67.0_rp + elmag(1,ipoin,1)) / 1000.0_rp)
                  tau2 = 0.0_rp
                  taux = tau1 + tau2 + 8.0_rp       ! Value of tau_s
                  xitaux = 1.0_rp / taux            ! Value of 1/tau_s
            
               else if (tipocelula == 2) then
           
                  vinf = 1.0_rp + exp((20.0_rp + elmag(1,ipoin,1)) / 5.0_rp)
                  vinf = 1.0_rp / vinf              ! Value of s_infinite
            
                  tau1 = 85.0_rp * exp(-(45.0_rp + elmag(1,ipoin,1)) * (45.0_rp + elmag(1,ipoin,1)) / 320.0_rp)
                  tau2 = 1.0_rp + exp((-20.0_rp + elmag(1,ipoin,1)) / 5.0_rp)
                  tau2 = 5.0_rp / tau2
                  taux = tau1 + tau2 + 3.0_rp       ! Value of tau_s
                  xitaux = 1.0_rp / taux            ! Value of 1/tau_s
           
               end if
           
                vaux0 = vauxi_exm(8,ipoin,3)      ! Value of s in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(8,ipoin,1) = vaux3      ! Value of variable s
           
           
           
           
           ! Variable of activation xs (related with I_Ks current)
           ! nauxi_exm = 9
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
           
                vinf = 1.0_rp + exp((-5.0_rp - elmag(1,ipoin,1)) / 14.0_rp)
                vinf = 1.0_rp / vinf              ! Value of xs_infinite
           
                alphx = 1.0_rp + exp((-10.0_rp - elmag(1,ipoin,1)) / 6.0_rp)
                alphx = 1100.0_rp / sqrt(alphx)   ! Value of alpha_xs
           
                betax = 1.0_rp + exp((-60.0_rp + elmag(1,ipoin,1)) / 20.0_rp)
                betax = 1.0_rp / betax            ! Value of beta_xs
           
                taux = alphx * betax              ! Value of tau_xs
                xitaux = 1.0_rp / taux            ! Value of 1/tau_xs
            
           
                vaux0 = vauxi_exm(9,ipoin,3)      ! Value of xs in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(9,ipoin,1) = vaux3      ! Value of variable xs
           
           
           
           
           ! Variable of activation xr1 (related with I_Kr current)
           ! nauxi_exm = 10
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
           
                vinf = 1.0_rp + exp((-26.0_rp - elmag(1,ipoin,1)) / 7.0_rp)
                vinf = 1.0_rp / vinf              ! Value of xr1_infinite
           
                alphx = 1.0_rp + exp((-45.0_rp - elmag(1,ipoin,1)) / 10.0_rp)
                alphx = 450.0_rp / alphx          ! Value of alpha_xr1
           
                betax = 1.0_rp + exp((30.0_rp + elmag(1,ipoin,1)) / 11.5_rp)
                betax = 6.0_rp / betax            ! Value of beta_xr1
           
                taux = alphx * betax              ! Value of tau_xr1
                xitaux = 1.0_rp / taux            ! Value of 1/tau_xr1
                
                vaux0 = vauxi_exm(10,ipoin,3)     ! Value of xr1 in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(10,ipoin,1) = vaux3     ! Value of variable xr1
           
           
           
           
           ! Variable of activation xr2 (related with I_Kr current)
           ! nauxi_exm = 11
           ! "A model for ventricular tissue", Ten Tusscher, Page H1586 (col 2)
           
                vinf = 1.0_rp + exp((88.0_rp + elmag(1,ipoin,1)) / 24.0_rp)
                vinf = 1.0_rp / vinf              ! Value of xr2_infinite
           
                alphx = 1.0_rp + exp((-60.0_rp - elmag(1,ipoin,1)) / 20.0_rp)
                alphx = 3.0_rp / alphx            ! Value of alpha_xr2
           
                betax = 1.0_rp + exp((-60.0_rp + elmag(1,ipoin,1)) / 20.0_rp)
                betax = 1.12_rp / betax           ! Value of beta_xr2
           
                taux = alphx * betax              ! Value of tau_xr2
                xitaux = 1.0_rp / taux            ! Value of 1/tau_xr2
                
                vaux0 = vauxi_exm(11,ipoin,3)     ! Value of xr2 in previous time step 
           
           
                ! Scheme for numerical integration
                if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
           
                   vaux1 = (vinf - vaux0) * xitaux;
                   vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                   vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                   vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                   vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                   vaux3 = vaux5
           
                else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
           
                   vaux1 = dtime/xmccm_exm * xitaux
                   vaux2 = 1.0_rp + thnew * vaux1
                   vaux2 = 1.0_rp / vaux2
                   vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                   vaux3 = vaux3 * vaux2       
           
                else if (kfl_odets_exm == 3) then      ! Rush - Larsen
           
                   vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
           
                end if
           
           
                vauxi_exm(11,ipoin,1) = vaux3     ! Value of variable xr2
           
           
           
           
           ! Variable of activation g (related with I_rel current)
           ! nauxi_exm = 12
           ! "A model for ventricular tissue", Ten Tusscher, Page H1587 (col 1)
           
                if ( vconc(5,ipoin,1) <= 0.00035_rp ) then
           
                  vinf = (vconc(5,ipoin,1) / 0.00035_rp) * (vconc(5,ipoin,1) / 0.00035_rp)
                  vinf = vinf * vinf * vinf
                  vinf = 1.0_rp + vinf
                  vinf = 1.0_rp / vinf            ! Value of g_infinite
           
                 else
           
                  vinf = (vconc(5,ipoin,1) / 0.00035_rp) * (vconc(5,ipoin,1) / 0.00035_rp)
                  vinf = vinf * vinf 
                  vinf = vinf * vinf 
                  vinf = vinf * vinf 
                  vinf = 1.0_rp + vinf
                  vinf = 1.0_rp / vinf            ! Value of g_infinite
           
                end if
           
                taux = 2.0_rp                     ! Value of tau_g [ms]
                xitaux = 0.5_rp                   ! Value of 1/tau_g
           
           
                vaux0 = vauxi_exm(12,ipoin,3)      ! Value of g in previous time step 
           
                if ( vinf > vaux0 .and. elmag(1,ipoin,1) > -60) then
           
                   vaux3 = vaux0
           
                else
           
                   ! Scheme for numerical integration
                   if (kfl_odets_exm == 1) then      ! Runge - Kutta 4
                      
                      vaux1 = (vinf - vaux0) * xitaux;
                      vaux2 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux1)) * xitaux;
                      vaux3 = (vinf - (vaux0 + 0.5_rp * dtime/xmccm_exm * vaux2)) * xitaux;
                      vaux4 = (vinf - (vaux0 + dtime/xmccm_exm * vaux3)) * xitaux;
                      vaux5 = vaux0 + dtime/xmccm_exm * 1.0_rp / 6.0_rp * (vaux1 + 2 * vaux2 + 2 * vaux3 + vaux4); 
                      vaux3 = vaux5
                      
                   else if (kfl_odets_exm == 2) then      ! Crank - Nicholson
                      
                      vaux1 = dtime/xmccm_exm * xitaux
                      vaux2 = 1.0_rp + thnew * vaux1
                      vaux2 = 1.0_rp / vaux2
                      vaux3 = (1.0_rp - thold * vaux1) * vaux0 + vaux1 * vinf
                      vaux3 = vaux3 * vaux2       
                      
                   else if (kfl_odets_exm == 3) then      ! Rush - Larsen
                      
                      vaux3 = vinf - (vinf - vaux0) * exp(-dtime/xmccm_exm * xitaux)    
                      
                   end if
           
                end if
           
                
                vauxi_exm(12,ipoin,1) = vaux3      ! Value of variable g

            end do
        end if

  end do

end subroutine exm_ceauxi
