subroutine exm_cvgunk(itask)
!-----------------------------------------------------------------------
!****f* Exmedi/exm_cvgunk
! NAME 
!    exm_cvgunk
! DESCRIPTION
!    This routine performs several convergence checks for the 
!    incompressible NS equations
! USES
!    exm_endite (itask=1,2) (TO BE UNPROGRAMMED)
!    exm_endste (itask=3)
! USED BY
!    Exmedi
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_exmedi
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip), save       :: ipass=0
  integer(ip)             :: ipoin,ixexm,idime,isizi
  real(rp)                :: va,vo,numer,denom,pnume,pdeno,prexm,riexm(4),cpnew,cpdif
  real(rp),save           :: cpold
!
! Initializations
!
  numer = 0.0_rp
  denom = 0.0_rp

  select case(itask)


  case(1)
     
     !
     ! Check convergence of the inner iterations:
     ! || u(n,i,j) - u(n,i,j-1)|| / ||u(n,i,j)||
     !
     
     riexm=0.0_rp

     call residu(kfl_normc_exm,one,ndofn_exm,unkno,elmag    ,one,  one,one,1.0_rp,riexm(1))
     if (kfl_cemod_exm == 2) call residu(kfl_normc_exm,one,ndofn_exm,unkno,elmag    ,one,one+1,one,1.0_rp,riexm(2))
!!     call residu(kfl_normc_exm,one,ndofn_exm,unkno,refhn_exm,one,  one,one,1.0_rp,riexm(3))
     
     riexm(4) = riexm(1) + riexm(2)

     if(riexm(4)<cotol_exm.or.itinn(modul)>=miinn_exm) kfl_goite_exm = 0

!!!     call minmax(one,npoin,tempe,vamxm_nsa(1,3),vamxm_nsa(2,3))

     !
     ! Sum up slaves contribution for ecgvo_exm
     !
     call pararr('SUM',0_ip,2_ip, ecgvo_exm)

     if(INOTSLAVE) then

        if(ipass==0.and.kfl_rstar/=2) then
           ipass=1
           write(momod(modul)%lun_conve,200)
!!           write(lun_maxmi_exm,300)
        end if
        if (ittim == 1) cpold = 0.0_rp
        call cputim(cpnew)
        cpdif = cpnew - cpold
        cpold = cpnew
!
!       Write convergence files
!
        write(momod(modul)%lun_conve,1000) ittim,itcou,itinn(modul),cutim,&
             riexm(4),riexm(1),riexm(2),riexm(3),1.0_rp/dtinv_exm,cpdif,ecgvo_exm(1)
        call flush(momod(modul)%lun_conve)
!!        write(lun_maxmi_nsa,1000) ittim,itcou,itinn(modul),&
!!             vamxm_nsa(1,1),vamxm_nsa(2,1),vamxm_nsa(1,2),vamxm_nsa(2,2),&
!!             vamxm_nsa(1,3),vamxm_nsa(2,3),vamxm_nsa(1,4),vamxm_nsa(2,4)
!!        call flush(lun_maxmi_nsa)

     end if


!
! Check convergence of the outer iterations in the norm selected by the user:
! || u(n,i,*) - u(n,i-1,*)|| / ||u(n,i,*)||
!
  case(2)


     call residu(kfl_normc_exm,one,ndofn_exm,unkno,elmag    ,one,  one,one,1.0_rp,riexm(1))
     if (kfl_cemod_exm == 2) call residu(kfl_normc_exm,one,ndofn_exm,unkno,elmag    ,one,one+1,one,1.0_rp,riexm(2))
!!     call residu(kfl_normc_exm,one,ndofn_exm,unkno,refhn_exm,one,  one,one,1.0_rp,riexm(3))

!
! Check residual of the time evolution, always in the L2 norm:
! || u(n,*,*) - u(n-1,*,*)|| / ||u(n,*,*)||
!
  case(3)

!!! ver estoooooooooooooooooo

!     call vecres(two,npoin,elmag(1,1:npoin,1),elmag(1,1:npoin,3),riexm,zeexm)
!     if(riexm.le.sstol_exm) then
!        kfl_stead_exm = 1
!        write(lun_outpu_exm,102) istep
!     end if

  end select
!
! Formats
!
200 format('# ',' --|  Exmedi Convergence File '       ,/,&
         & '# ',' --|  Columns displayed:' ,/,&
         & '# ',' --|  1. Time Step   2. Global Iteration   3. Inner Iteration   4. Current time'  ,/,&
         & '# ',' --|  5. Total Res.  6. Intra. Res.        7. Extra. Res.       8. Recov. Res.' ,/,&
         & '# ',' --|  9. Critical Time Step  10. CPU time        11. ECG.' ,/,&
         & '# ',' --|  ') 

1000 format(4x,i9,2x,i9,2x,i9,15(2x,e12.6))
        
end subroutine exm_cvgunk


