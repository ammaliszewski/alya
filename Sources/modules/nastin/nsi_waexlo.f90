!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_waexlo.f90
!> @author  Herbert Owen
!> @brief   Preliminary operations to apply the wall law at the 'Exchange Location' 
!> @details - See Bodart and Larsson - Wall modeled large eddy simulation in complex ... 2011
!!          - Obtain the exchange location for each wall law boundary gauss point
!!          - Intialize interpolation - PAR_INIT_INTERPOLATE_POINTS_VALUES
!> @} 
!-----------------------------------------------------------------------

subroutine nsi_waexlo()
  use def_domain

  use def_kintyp, only     :  ip,rp
  use def_master, only     :  current_code,current_zone,ID_NASTIN,lzone,kfl_paral,INOTMASTER
  use def_kermod, only     :  kfl_delta
  use def_nastin, only     :  delta_nsi,zensi,kfl_fixbo_nsi,&
                              dexlo_nsi,velel_nsi,lexlo_nsi,wallcoupling,kfl_waexl_nsi
  use mod_couplings, only :  COU_INIT_INTERPOLATE_POINTS_VALUES
  use mod_parall,        only :  par_code_zone_subd_to_color,PAR_MY_CODE_RANK


  implicit none

  integer(ip)              :: ielem,inode,ipoin,idime,kboun,kount,ii,ierr
  integer(ip)              :: pnode,pgaus,iboun,igaub,inodb
  integer(ip)              :: pelty,pblty,pnodb,pgaub,pmate
  real(rp)                 :: bocod(ndime,mnodb),elcod(ndime,mnode),gbcod(ndime)
  real(rp)                 :: baloc(ndime,ndime),eucta
  real(rp),pointer         :: xcoor(:,:)
  integer(ip)              :: icolo,jcolo

  nullify(velel_nsi)
  nullify(xcoor)
  nullify(lexlo_nsi)

  if ( kfl_waexl_nsi == 0_ip ) then 

     allocate( velel_nsi(ndime,1_ip) )
     allocate( lexlo_nsi(mgaub,1_ip) )

  else
     kount = 0_ip

     if( INOTMASTER ) then
        !
        ! Loop over boundaries - preliminary just to obtain kount
        !
        boun0: do kboun = 1,nbouz(lzone(ID_NASTIN))
           iboun = lbouz(lzone(ID_NASTIN)) % l(kboun)

           if(  kfl_fixbo_nsi(iboun) ==  3 .or. &      ! Wall law
                & kfl_fixbo_nsi(iboun) == 13 .or. &    ! Wall law + open pressure
                & kfl_fixbo_nsi(iboun) == 18)  then    ! u.n in weak form
              !
              ! Element properties and dimensions
              !
              pblty = ltypb(iboun) 
              pnodb = nnode(pblty)
              ielem = lboel((pnodb+1),iboun)
              pgaub = ngaus(pblty) 
              pmate = 1

              if( nmate > 1 ) then
                 pmate = lmate(ielem)
              end if

              if ( ( pmate /= -1 ) .and. ( ( delta_nsi > zensi ) .or. ( kfl_delta == 1 ) )  ) kount = kount + pgaub

           end if

        end do boun0
        allocate( velel_nsi(ndime,kount) )
        allocate( xcoor(ndime,kount) )
        allocate( lexlo_nsi(mgaub,nboun) )

     end if


     kount = 0_ip
     !
     ! Loop over boundaries
     !
     if( INOTMASTER ) then
        boundaries: do kboun = 1,nbouz(lzone(ID_NASTIN))
           iboun = lbouz(lzone(ID_NASTIN)) % l(kboun)
           if(  kfl_fixbo_nsi(iboun) ==  3 .or. &      ! Wall law
                & kfl_fixbo_nsi(iboun) == 13 .or. &    ! Wall law + open pressure
                & kfl_fixbo_nsi(iboun) == 18)  then    ! u.n in weak form
              !
              ! Element properties and dimensions
              !
              pblty = ltypb(iboun) 
              pnodb = nnode(pblty)
              ielem = lboel((pnodb+1),iboun)
              pelty = ltype(ielem)
              pnode = nnode(pelty)
              pgaub = ngaus(pblty) 
              pgaus = ngaus(pelty)
              pmate = 1

              if( nmate > 1 ) then
                 pmate = lmate(ielem)
              end if

              if ( ( pmate /= -1 ) .and. ( ( delta_nsi > zensi ) .or. ( kfl_delta == 1 ) )  )  then 
                 !
                 ! Gather operations: ELCOD, BOCOD
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    do idime = 1,ndime
                       elcod(idime,inode) = coord(idime,ipoin)             
                    end do
                 end do

                 do inodb = 1,pnodb     ! obtain bocod for bouder
                    ipoin = lnodb(inodb,iboun)
                    do idime = 1,ndime
                       bocod(idime,inodb) = coord(idime,ipoin)
                    end do
                 end do

                 gauss_points: do igaub = 1,pgaub
                    !
                    ! Obtain normal (baloc(:,ndime) to the surface (following nsi_bouset)
                    !                 
                    call bouder(&
                         pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&    ! Cartesian derivative
                         bocod,baloc,eucta)                                   ! and Jacobian
                    call chenor(pnode,baloc,bocod,elcod)                      ! Check normal

                    kount = kount + 1_ip
                    lexlo_nsi(igaub,iboun) = kount

                    gbcod=0.0_rp
                    do inodb=1,pnodb
                       do idime=1,ndime
                          gbcod(idime) = gbcod(idime)        &
                               + elmar(pblty)%shape(inodb,igaub) * bocod(idime,inodb)
                       end do
                    end do
                    do idime=1,ndime
                       xcoor(idime,kount) = gbcod(idime) - dexlo_nsi * baloc(idime,ndime)  ! for the moment I will set dexlo_nsi a const
                    end do         !  value. Later we can set some constant times de 1st elemente normal size or even more elaborate opts

                 end do gauss_points

              end if

           end if

        end do boundaries
     end if

     !
     ! Intialize interpolation
     !
     icolo = par_code_zone_subd_to_color(current_code,current_zone,0_ip)     ! icolo and jcolo I do not understan well 
     jcolo = par_code_zone_subd_to_color(current_code,current_zone,0_ip)     ! I just put them following nsi_velobl
     wallcoupling % itype = 1

     call COU_INIT_INTERPOLATE_POINTS_VALUES(xcoor,icolo,jcolo,wallcoupling)

     ierr = 0
     do ii = 1,kount
        if( wallcoupling % geome % status(ii) == 0 ) then
           print*,ii,PAR_MY_CODE_RANK,' is lost'
           ierr = ierr + 1
        end if
     end do
     if ( ierr/=0 ) call runend

  end if

end subroutine nsi_waexlo
