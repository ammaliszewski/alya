!-----------------------------------------------------------------------
!> @addtogroup NastinInput
!> @{
!> @file    nsi_reaous.f90
!> @author  Guillaume Houzeaux
!> @brief   Read postprocess data
!> @details Read postprocess data
!!          - Array postprocess
!!          - Witness points
!!          - Element, boundary and node sets
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_reaous
  use def_parame
  use def_inpout
  use def_master
  use def_nastin
  use def_domain
  implicit none
  integer(ip) :: ii,dummi

  if( INOTSLAVE ) then
     !
     ! Initializations
     !
     kfl_exacs_nsi = 0            ! Exact solution
     kfl_exfix_nsi = 1            ! Dirichlet condition imposed 
     kfl_inert_nsi = 0            ! Non-inertial velocity  
     kfl_psmat_nsi = 0            ! Postscript file of the matrix
     cloth_nsi     = -500.0_rp    ! CLO
     metab_nsi     = -500.0_rp    ! MET
     wetme_nsi     = -500.0_rp    ! WME
     ambie_nsi     = -500.0_rp    ! TA
     radia_nsi     = -500.0_rp    ! TR
     relat_nsi     = -500.0_rp    ! RH
     avtim_nsi     = 0.0_rp       ! Start averaging time
      !--><group>
      !-->            <groupName>OUTPUT_&amp;_POST_PROCESS</groupName>
      !-->            <groupType>oupos</groupType>
      !-->            <groupTypeDefinition>
      !-->        <![CDATA[    POSTPROCESS XXXXX, STEPS=int1 [, FILTER=int2]                                                   $ Variables computed on all nodes or filtered
      !-->                     ELEMENT_SET
      !-->                       XXXXX                                                                                         $ Variables computed in volume integrals
      !-->                     END_ELEMENT_SET
      !-->                     BOUNDARY_SET
      !-->                       XXXXX                                                                                         $ Variables computed in boundary integrals
      !-->                     END_BOUNDARY_SET
      !-->                     NODE_SET
      !-->                       XXXXX                                                                                         $ Variables computed on nodes
      !-->                     END_NODE_SET
      !-->                     WITNESS_POINTS
      !-->                       XXXXX                                                                                         $ Variables computed witness points
      !-->                     END_WITNESS_POINTS
      !-->                     OUTPUT ERROR, SOLUTION = int1                                                                   $ Manufactured solution
      !-->                  ]]>
      !-->      </groupTypeDefinition>
      !-->            <postProcess>
      !-->                <inputLineHelp><![CDATA[POSTPROCESS:
      !-->                  Postprocess variables on nodes at each int1 time steps. the name of the file
      !-->                  where the variables is stored at time step nnn is: <tt> sample-XXXXX-00000nnn.post.alyabin</tt>.
      !-->                  A fliter can be applied to the variable. Filters are defined in kermod.]]></inputLineHelp>
      !-->                <var>VELOCITY_MODULE</var>
      !-->                <var>VORTICITY</var>
      !-->                <var>KINETIC_ENERGY</var>
      !-->            </postProcess>
      !-->            <elementSet>
      !-->                <inputLineHelp><![CDATA[ELEMENT_SET:
      !-->                  Variables computed as volume integrals on element sets. The results are stored
      !-->                  in file <tt>sample-element.nsi.set</tt>. Let V be the size of the bounday set. 
      !-->                  The following variables can be postprocessed:
      !-->                  <ul>
      !-->                    <li>VELOCITY_MODULE: int_V u^2 </li>
      !-->                    <li>VORTICITY:       int_V (dv/dx-du/dy)^2</li>
      !-->                    <li>KINETIC_ENERGY:  int_V 1/2*rho*u^2 </li>
      !-->                  </ul>]]></inputLineHelp>
      !-->                <var>VELOCITY_MODULE</var>
      !-->                <var>VORTICITY</var>
      !-->                <var>KINETIC_ENERGY</var>
      !-->            </elementSet>
      !-->            <boundarySet>
      !-->                <inputLineHelp><![CDATA[BOUNDARY_SET:
      !-->                 Variables computed as boundary integrals on boundary sets. The results are stored
      !-->                 in file <tt>sample-boundary.nsi.set</tt>. Let S be the size of the boundary set. 
      !-->                 The following variables can be postprocessed:
      !-->                 <ul>
      !-->                  <li>MEAN_PRESSURE: int_S P/|S| </li>
      !-->                  <li>MASS:          int_S rho*u.n</li>
      !-->                  <li>FORCE:         int_S 2 mu eps(u).n, int_S (-pI).n</li>
      !-->                  <li>TORQUE:        int_S (r-rc) x ( 2 mu eps(u).n ), int_S  (r-rc) x (-pI.n) </li>
      !-->                  <li>MEAN_YPLUS:    int_S y+ / S</li>
      !-->                  <li>MEAN_VELOCITY: int_S u.n / S</li>
      !-->                  <li>WEAT_FORCE:    int_Sw 2 mu eps(u).n, int_Sw (-pI).n </li>
      !-->                  <li>WEAT_SURFACE:  int_Sw, Sw=wet surface</li>
      !-->                </ul>]]></inputLineHelp>
      !-->                <var>MEAN_PRESSURE</var>
      !-->                <var>MASS</var>
      !-->                <var>FORCE</var>
      !-->                <var>TORQUE</var>
      !-->                <var>MEAN_YPLUS</var>
      !-->                <var>MEAN_VELOCITY</var>
      !-->                <var>WEAT_FORCE</var>
      !-->                <var>WEAT_SURFACE</var>
      !-->            </boundarySet>
      !-->            <nodeSet>
      !-->                <inputLineHelp><![CDATA[NODE_SET:
      !-->                 Variables computed as boundary integrals on boundary sets. The results are stored
      !-->                 in file <tt>sample-node.nsi.set</tt>. 
      !-->                 The following variables can be postprocessed:
      !-->                 <ul>
      !-->                   <li>VELOX: X-velocity</li>
      !-->                   <li>VELOY: Y-velocity</li>
      !-->                   <li>VELOZ: Z-velocity</li>
      !-->                   <li>PRESS: Pressure</li>
      !-->                   <li>YPLUS: y+</li>
      !-->                 </ul>
      !-->                 WITNESS_POINTS
      !-->                    XXXXX                                                       $ Variables computed witness points
      !-->                 END_WITNESS_POINTS
      !-->                 WITNESS_POINTS:
      !-->                 <ul>
      !-->                   <li>VELOX: X-velocity</li>
      !-->                   <li>VELOY: Y-velocity</li>
      !-->                   <li>VELOZ: Z-velocity</li>
      !-->                   <li>PRESS: Pressure</li>
      !-->                 </ul>]]></inputLineHelp>
      !-->                <var>VELOX</var>
      !-->                <var>VELOY</var>
      !-->                <var>VELOZ</var>
      !-->                <var>PRESS</var>
      !-->                <var>YPLUS</var>
      !-->            </nodeSet>
      !-->            <witnessPoints>
      !-->              <inputLineHelp><![CDATA[<ul>
      !-->                <li>VELOX: X-velocity</li>
      !-->                <li>VELOY: Y-velocity</li>
      !-->                <li>VELOZ: Z-velocity</li>
      !-->                <li>PRESS: Pressure</li>
      !-->                </ul>]]></inputLineHelp>
      !-->                <var>VELOX</var>
      !-->                <var>VELOY</var>
      !-->                <var>VELOZ</var>
      !-->                <var>PRESS</var>
      !-->            </witnessPoints>
      !-->            <outputError>
      !-->               <inputLineHelp><![CDATA[OUTPUT ERROR:
      !-->                Manufactured solutions used to carry out code typing checking and mesh convergence.
      !-->                The L2, L1 and Linf norm of the velocity, pressure, velocity gradients and pressure 
      !-->                gradients can be found in file <tt>sample.nsi.log</tt>. Dirhclet boundary
      !-->                conditions are automatically imposed on the velocity on all the boundary. Pressure
      !-->                should be imposed on one node to the right value as the flow is confined.
      !-->                The following manufactured solutions are available:
      !-->                <ul>
      !-->                  <li> SOLUTION = 5:  r=sqrt(x^2+y^2); u = 2*y*(r-0.5), v = -2*x*(r-0.5),  p = r-1 </li>
      !-->                  <li> SOLUTION = 13: u=2x +  y + 3z, v=- x - 3y - 4z, w=6x + 7y - z, p=-5x + 6y + 2z </li>
      !-->                  <li> SOLUTION = 20: u=-cos(pi*x)*sin(pi*y), v=sin(pi*x)*cos(pi*y), p=sin(pi*x)*sin(pi*x) + sin(pi*y)*sin(pi*y)</li>
      !-->                </ul>]]></inputLineHelp>
      !-->            </outputError>
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Output and postprocess
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> OUTPUT_&_POST_PROCESS
     !
     ! ADOC[1]> POSTPROCESS XXXXX, STEPS=int1 [, FILTER=int2]                                                   $ Variables computed on all nodes or filtered
     ! ADOC[d]> POSTPROCESS:
     ! ADOC[d]> Postprocess variables on nodes at each int1 time steps. the name of the file
     ! ADOC[d]> where the variables is stored at time step nnn is: <tt> sample-XXXXX-00000nnn.post.alyabin</tt>.
     ! ADOC[d]> A fliter can be applied to the variable. Filters are defined in kermod.
     !
     ! ADOC[1]> ELEMENT_SET
     ! ADOC[1]>   XXXXX                                                                                         $ Variables computed in volume integrals
     ! ADOC[1]> END_ELEMENT_SET
     ! ADOC[d]> ELEMENT_SET:
     ! ADOC[d]> Variables computed as volume integrals on element sets. The results are stored
     ! ADOC[d]> in file <tt>sample-element.nsi.set</tt>. Let V be the size of the bounday set. 
     ! ADOC[d]> The following variables can be postprocessed:
     ! ADOC[d]> <ul>
     ! ADOC[d]> <li>VELOCITY_MODULE: int_V u^2 </li>
     ! ADOC[d]> <li>VORTICITY:       int_V (dv/dx-du/dy)^2</li>
     ! ADOC[d]> <li>KINETIC_ENERGY:  int_V 1/2*rho*u^2 </li>
     ! ADOC[d]> </ul>
     !
     ! ADOC[1]> BOUNDARY_SET
     ! ADOC[1]>   XXXXX                                                                                         $ Variables computed in boundary integrals
     ! ADOC[1]> END_BOUNDARY_SET
     ! ADOC[d]> BOUNDARY_SET:
     ! ADOC[d]> Variables computed as boundary integrals on boundary sets. The results are stored
     ! ADOC[d]> in file <tt>sample-boundary.nsi.set</tt>. Let S be the size of the boundary set. 
     ! ADOC[d]> The following variables can be postprocessed:
     ! ADOC[d]> <ul>
     ! ADOC[d]> <li>MEAN_PRESSURE: int_S P/|S| </li>
     ! ADOC[d]> <li>MASS:          int_S rho*u.n</li>
     ! ADOC[d]> <li>FORCE:         int_S 2 mu eps(u).n, int_S (-pI).n</li>
     ! ADOC[d]> <li>TORQUE:        int_S (r-rc) x ( 2 mu eps(u).n ), int_S  (r-rc) x (-pI.n) </li>
     ! ADOC[d]> <li>MEAN_YPLUS:    int_S y+ / S</li>
     ! ADOC[d]> <li>MEAN_VELOCITY: int_S u.n / S</li>
     ! ADOC[d]> <li>WEAT_FORCE:    int_Sw 2 mu eps(u).n, int_Sw (-pI).n </li>
     ! ADOC[d]> <li>WEAT_SURFACE:  int_Sw, Sw=wet surface</li>
     ! ADOC[d]> </ul>
     !
     ! ADOC[1]> NODE_SET
     ! ADOC[1]>   XXXXX                                                                                         $ Variables computed on nodes
     ! ADOC[1]> END_NODE_SET
     ! ADOC[d]> NODE_SET:
     ! ADOC[d]> Variables computed as boundary integrals on boundary sets. The results are stored
     ! ADOC[d]> in file <tt>sample-node.nsi.set</tt>. 
     ! ADOC[d]> The following variables can be postprocessed:
     ! ADOC[d]> <ul>
     ! ADOC[d]> <li>VELOX: X-velocity</li>
     ! ADOC[d]> <li>VELOY: Y-velocity</li>
     ! ADOC[d]> <li>VELOZ: Z-velocity</li>
     ! ADOC[d]> <li>PRESS: Pressure</li>
     ! ADOC[d]> <li>YPLUS: y+</li>
     ! ADOC[d]> </ul>
     ! ADOC[d]>  
     ! ADOC[1]> WITNESS_POINTS
     ! ADOC[1]>   XXXXX                                                                                         $ Variables computed witness points
     ! ADOC[1]> END_WITNESS_POINTS
     ! ADOC[d]> WITNESS_POINTS:
     ! ADOC[d]> <ul>
     ! ADOC[d]> <li>VELOX: X-velocity</li>
     ! ADOC[d]> <li>VELOY: Y-velocity</li>
     ! ADOC[d]> <li>VELOZ: Z-velocity</li>
     ! ADOC[d]> <li>PRESS: Pressure</li>
     ! ADOC[d]> </ul>
     ! ADOC[d]> 
     !
     call ecoute('nsi_reaous')
     do while(words(1)/='OUTPU')
        call ecoute('nsi_reaous')
     end do
     !
     ! Begin to read data
     !
     do while(words(1)/='ENDOU')
        call ecoute('nsi_reaous')

        call posdef(2_ip,dummi)

        if(words(1)=='INERT') then
           !
           ! Post process in inertial frame of reference
           !
           kfl_inert_nsi=1

        else if(words(1)=='OUTPU') then

           if(words(2)=='ERROR') then
              ! ADOC[1]> OUTPUT ERROR, SOLUTION = int1, DIRICHLET: ON | OFF                                     $ Manufactured solution
              ! ADOC[d]> OUTPUT ERROR:
              ! ADOC[d]> Manufactured solutions used to carry out code typing checking and mesh convergence.
              ! ADOC[d]> The L2, L1 and Linf norm of the velocity, pressure, velocity gradients and pressure 
              ! ADOC[d]> gradients can be found in file <tt>sample.nsi.log</tt>. Dirhclet boundary
              ! ADOC[d]> conditions are automatically imposed on the velocity on all the boundary. Pressure
              ! ADOC[d]> should be imposed on one node to the right value as the flow is confined.
              ! ADOC[d]> The following manufactured solutions are available:
              ! ADOC[d]> <ul>
              ! ADOC[d]> <li> SOLUTION = 5:  r=sqrt(x^2+y^2); u = 2*y*(r-0.5), v = -2*x*(r-0.5),  p = r-1 </li>
              ! ADOC[d]> <li> SOLUTION = 13: u=2x +  y + 3z, v=- x - 3y - 4z, w=6x + 7y - z, p=-5x + 6y + 2z </li>
              ! ADOC[d]> <li> SOLUTION = 20: u=-cos(pi*x)*sin(pi*y), v=sin(pi*x)*cos(pi*y), p=sin(pi*x)*sin(pi*x) + sin(pi*y)*sin(pi*y)</li>
              ! ADOC[d]> </ul>
              ! ADOC[d]> If DIRICHLET option is ON, Alya imposes automatically exact Dirichlet conditions on the boundary.
              !
              kfl_exacs_nsi = getint('SOLUT',1_ip,'#Exact solution')
              expar_nsi     = param(4:13)
              if( exists('DIRIC') ) then
                 if( trim(getcha('DIRIC','ON   ','#Automatic Dirichlet for exact solution')) == 'OFF  ') then
                    kfl_exfix_nsi = 0
                 end if
              else if( exists('ONLYO') ) then
                 kfl_exfix_nsi = 2
              end if

           else if(words(2)=='MATRI') then
              !
              ! Matrix profile
              !
              kfl_psmat_nsi=getint('ITERA',1_ip,'#Iteration to postprocess matrix') 
 
           end if

        else if(words(1)=='PARAM') then
           !
           ! Comfort factor for postprocess
           !
           cloth_nsi = getrea('CLO  ',-500.0_rp,'#CLO') 
           metab_nsi = getrea('MET  ',-500.0_rp,'#MET')  
           wetme_nsi = getrea('WME  ',-500.0_rp,'#WME')  
           ambie_nsi = getrea('TA   ',-500.0_rp,'#TA')  
           radia_nsi = getrea('TR   ',-500.0_rp,'#TR')  
           relat_nsi = getrea('RH   ',-500.0_rp,'#RH')  

        else if(words(1)=='AVERA') then
           !
           ! Averaging starting time
           !
           avtim_nsi = getrea('AVERA',0.0_rp,'#Averaging starting time')  
      
        end if
     end do
     !
     ! Check if velocity residual is required VEOLD_NSI
     !
     if(postp(1) % npp_stepi(4)/=0.or.maxval(postp(1) % pos_times(:,4))>zensi) then
        kfl_resid_nsi=1
     else
        kfl_resid_nsi=0
     end if
     !
     ! Force, Torque and wet force 
     !
     if( postp(1) % npp_setsb(3) == 1 ) then
        postp(1) % npp_setsb(3:8) = 1
     end if
     if( postp(1) % npp_setsb(9) == 1 ) then       
        postp(1) % npp_setsb(9:14) = 1 
        do ii = 10,14
           postp(1) % pabse(1:3,ii) = postp(1) % pabse(1:3,9)   ! Copy Torque center 
        end do
     end if
     if( postp(1) % npp_setsb(17) == 1 ) then
        postp(1) % npp_setsb(17:23) = 1
     end if
     if( postp(1) % npp_setsb(24) == 1 ) then
        postp(1) % npp_setsb(24:29) = 1
     end if
     !
     ! Reattachment: force the force to be computed
     !
     if( postp(1) % npp_setsb(27) == 1 ) then
        postp(1) % npp_setsb(27:30) = 1
        postp(1) % npp_setsb( 3)    = 1 
     end if
     !-->        </group>
     !
     ! ADOC[0]> END_OUTPUT_&_POST_PROCESS
     ! 
  end if

end subroutine nsi_reaous
    
 
