!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_updtss.f90
!> @author  Guillaume Houzeaux
!> @brief   Critical time steo
!> @details Computes the time step size for the incompressible NS
!>          equation.
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_updtss()
  use def_kintyp,         only : ip,rp
  use def_master,         only : current_zone,INOTMASTER 
  use def_master,         only : kfl_timco,dtinv,veloc
  use def_master,         only : fleve,press,tempe,densi
  use def_kermod,         only : gasco
  use def_domain,         only : lnods,coord,nelem,lelez
  use def_domain,         only : nelez,ltype,nnode,elmar
  use def_domain,         only : mnode,lmate,nmate,ndime
  use def_domain,         only : lorde,hnatu
  use def_nastin,         only : kfl_timei_nsi,kfl_colev_nsi
  use def_nastin,         only : kfl_advec_nsi,kfl_regim_nsi
  use def_nastin,         only : kfl_ellen_nsi
  use def_nastin,         only : dtcri_nsi
  use def_nastin,         only : dtsgs_nsi,kfl_dttyp_nsi
  use def_nastin,         only : kfl_sgsti_nsi,dtinv_nsi
  use def_nastin,         only : safet_nsi,tamin_nsi
  use mod_communications, only : PAR_MIN
  implicit none 
  integer(ip)      :: ielem,idime,inode,ipoin,icolo
  integer(ip)      :: pnode,pelty,pmate,kelem
  real(rp)         :: dtcri
  real(rp)         :: dtmin
  real(rp)         :: cartd(ndime,mnode)
  real(rp)         :: chave(ndime,2)
  real(rp)         :: elcod(ndime,mnode)
  real(rp)         :: elvel(ndime,mnode)
  real(rp)         :: chale(2)
  real(rp)         :: hleng(3)
  real(rp)         :: tragl(9)
  integer(ip)      :: porde

#ifdef EVENT
  call mpitrace_user_function(1)
#endif

  if( kfl_timei_nsi /= 0 ) then

     if( kfl_dttyp_nsi == 1 ) then

        !---------------------------------------------------------------
        !
        ! Minimum of element time steps
        !
        !---------------------------------------------------------------

        call nsi_updunk(17_ip) ! Compute density

        if( INOTMASTER ) then

           dtmin = huge(1.0_rp)
           !
           ! OpenMP declarations
           !
           !$OMP PARALLEL  DO SCHEDULE (STATIC)                                      & 
           !$OMP DEFAULT   (NONE)                                                    &
           !$OMP PRIVATE   (ielem,kelem,pelty,pnode,porde,pmate,inode,ipoin,idime,   &
           !$OMP            elcod,elvel,tragl,hleng,chave,chale,cartd,dtcri)         &
           !$OMP SHARED    (ltype,nnode,lorde,kfl_colev_nsi,tempe,densi,gasco,       &
           !$OMP            lmate,lnods,coord,veloc,elmar,hnatu,kfl_advec_nsi,       &
           !$OMP            kfl_ellen_nsi,fleve,kfl_regim_nsi,press,                 &
           !$OMP            nmate,ndime,nelez,lelez,current_zone)                    & 
           !$OMP REDUCTION (MIN:dtmin)                        
           ! 
           ! Loop over elements
           !      
           elements: do kelem = 1,nelez(current_zone)
              ielem = lelez(current_zone) % l(kelem)
              pelty = ltype(ielem) 

              if( pelty > 0 ) then
                 pnode = nnode(pelty)
                 porde = lorde(pelty)
                 if( nmate > 1 ) pmate = lmate(ielem)
                 !
                 ! Default: ELVEL and ELCOD
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    elcod(1:ndime,inode) = coord(1:ndime,ipoin)
                    elvel(1:ndime,inode) = veloc(1:ndime,ipoin,1)
                 end do
                 !
                 ! HLENG and TRAGL at center of gravity
                 !
                 call elmlen(&
                      ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&
                      hnatu(pelty),hleng)
                 !
                 ! Compute the characteristic length: CHALE
                 ! 
                 call elmchl(&
                      tragl,hleng,elcod,elvel,chave,chale,pnode,&
                      porde,hnatu(pelty),kfl_advec_nsi,kfl_ellen_nsi)
                 !
                 ! Time step
                 !
                 call nsi_elmtss(&
                      pelty,pmate,pnode,lnods(1,ielem),ielem,elcod,elvel,&
                      cartd,chale,hleng,dtcri)
                 dtmin = min( dtmin , dtcri )
              end if
           end do elements
           !$OMP END PARALLEL DO

        end if
        !
        ! Parall: Look for minimum over all subdomains (dtmin)
        !
        call PAR_MIN(dtmin,'IN MY CODE')

     else if( kfl_dttyp_nsi == 2 ) then

        !---------------------------------------------------------------
        !
        ! Adaptive time step
        !
        !---------------------------------------------------------------

        call nsi_updtse(dtmin)

     else if( kfl_dttyp_nsi == 3 ) then

        !---------------------------------------------------------------
        !
        ! Use minimum TAU
        !
        !---------------------------------------------------------------

        dtmin = tamin_nsi

     end if
     !
     ! Assign 1/dt
     !
     dtcri_nsi = dtmin
     if( dtcri_nsi /= 0.0_rp ) dtinv_nsi = 1.0_rp/(dtcri_nsi*safet_nsi)
     dtsgs_nsi = dtinv_nsi
     if( kfl_sgsti_nsi == 0 ) dtsgs_nsi = 0.0_rp
     if( kfl_timco     == 1 ) dtinv = max(dtinv,dtinv_nsi)

  end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine nsi_updtss
