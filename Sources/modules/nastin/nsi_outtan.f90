subroutine nsi_outtan(itask)
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_outtan
  ! NAME 
  !    nsi_outtan
  ! DESCRIPTION
  !    This routine computes the tangential component of the traction
  ! USES
  ! USED BY
  !    nsi_output
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use mod_memory
  use def_kermod
  use mod_gradie
  use mod_ker_proper, only : ker_proper
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,ibopo,itens,idime,dummi
  real(rp)                :: n1,n2,n3,s11,s12,s22,s13,s23,s33,stnn,us2
  real(rp)                :: sn1,sn2,sn3,stx,sty,stz,rho,nu,u,ustar,dummr, yplus
  real(rp),    pointer    :: visco_tmp(:)

  if( delta_nsi > 0.0_rp .or. kfl_delta == 1 ) then 
     !
     ! Wall law: no velocity gradient needed
     !
  else
     ! 
     ! Velocity gradients GRADV_NSI
     ! 
     call memory_alloca(mem_modul(1:2,modul),'GRADV_NSI','nsi_outtan',gradv_nsi,ntens,npoin)
     call gradie(veloc(1:ndime,1:npoin,1),gradv_nsi)
     nullify(  visco_tmp )
     allocate( visco_tmp(npoin) )
     call ker_proper('VISCO','NPOIN',dummi,dummi,visco_tmp)
     do ipoin = 1,npoin  
        gradv_nsi(1:ntens,ipoin) = gradv_nsi(1:ntens,ipoin) * visco_tmp(ipoin) 
     end do
     deallocate( visco_tmp )
  end if
  

  if( itask == 0 ) then

     !-------------------------------------------------------------------
     !
     ! Tangential traction on boundary nodes only
     !
     !-------------------------------------------------------------------

     if( delta_nsi > 0.0_rp .or. kfl_delta == 1 ) then 
        !
        ! Wall law
        !
        do ipoin=1,npoin
           ibopo=lpoty(ipoin)
           if(ibopo>=1) then
              rho = prope_nsi(1,ipoin)
              nu  = prope_nsi(2,ipoin)/rho
              u   = 0.0_rp
              do idime=1,ndime
                 u = u + veloc(idime,ipoin,1)*veloc(idime,ipoin,1)
              end do
              u = sqrt(u)
              if( kfl_rough >  0 ) rough_dom = rough(ipoin) 
              if( kfl_delta == 1 ) then
                 call frivel(ywalp(ibopo),rough_dom,u,nu,ustar)
                 if(u/=0.0_rp) us2 = rho*ustar*ustar/u
              else                
                 call frivel(delta_nsi,rough_dom,u,nu,ustar)
                 yplus = delta_nsi * u / nu
                 if( yplus < 5.0_rp  .and. kfl_ustar == 0 ) then
                    us2 = rho * nu/ delta_nsi 
                 else if( kfl_ustar == 1 ) then 
                    ustar = 0.41_rp  / log((delta_nsi+rough_dom) / rough_dom)
                    us2 = rho * u * ustar*ustar           
                 else if( kfl_ustar == 2 ) then ! ABL 2
!                    velfr = 0.41_rp / log((delta_nsi+rough_dom) / rough_dom)
!                    velf2 = kinen**0.5 *cmu_st**0.25 ! frveloc in terms of kinetic turbulent energy 

!                    fact1 = gbden * velfr * velf2
                 else
                    us2 = rho * ustar *ustar / u          
                 end if
              end if
              if(u/=0.0_rp) then                 
                 do idime=1,ndime
                    gevec(idime,ibopo) = -us2*veloc(idime,ipoin,1)
                 end do
              end if
           end if
        end do
     else
        !
        ! Up to the wall: (sig.n) t = sig.n - (n.sig.n) n 
        !
        do ipoin=1,npoin
           ibopo=lpoty(ipoin)
           if(ibopo>=1) then
              n1  = exnor(1,1,ibopo)
              n2  = exnor(2,1,ibopo)
              s11 = gradv_nsi(1,ipoin)
              s22 = gradv_nsi(2,ipoin)
              s12 = gradv_nsi(3,ipoin)
              sn1 = s11*n1+s12*n2
              sn2 = s12*n1+s22*n2
              if(ndime==3) then
                 n3   = exnor(3,1,ibopo) 
                 s33  = gradv_nsi(4,ipoin)
                 s13  = gradv_nsi(5,ipoin)
                 s23  = gradv_nsi(6,ipoin)
                 sn1  = sn1+s13*n3
                 sn2  = sn2+s23*n3
                 sn3  = s13*n1+s23*n2+s33*n3
                 stnn = sn1*n1+sn2*n2+sn3*n3
                 stx  = sn1-stnn*n1
                 sty  = sn2-stnn*n2
                 stz  = sn3-stnn*n3
                 gevec(1,ibopo)=stx         
                 gevec(2,ibopo)=sty       
                 gevec(3,ibopo)=stz      
              else
                 stnn = sn1*n1+sn2*n2
                 stx  = sn1-stnn*n1
                 sty  = sn2-stnn*n2
                 gevec(1,ibopo)=stx         
                 gevec(2,ibopo)=sty       
              end if
           end if
        end do

     end if

  else

     !-------------------------------------------------------------------
     !
     ! Tangential traction on all nodes. Set to 0 on interior nodes
     !
     !-------------------------------------------------------------------

     if( delta_nsi > 0.0_rp .or. kfl_delta == 1 ) then
        !
        ! Wall law
        !
        do ipoin = 1,npoin
           ibopo = lpoty(ipoin)
           if( ibopo >= 1 ) then
              rho = prope_nsi(1,ipoin)
              nu  = prope_nsi(2,ipoin)/rho
              u   = 0.0_rp
              do idime=1,ndime
                 u = u + veloc(idime,ipoin,1)*veloc(idime,ipoin,1)
              end do
              u = sqrt(u)
              if( kfl_rough >  0 ) rough_dom = rough(ipoin) 
              if( kfl_delta == 1 ) then
                 call frivel(ywalp(ibopo),rough_dom,u,nu,ustar)
                 if(u/=0.0_rp) us2 = rho*ustar*ustar/u
              else
                 call frivel(delta_nsi,rough_dom,u,nu,ustar)
                 yplus = delta_nsi * u / nu
                 if( yplus < 5.0_rp  .and. kfl_ustar == 0 ) then
                    us2 = rho * nu/ delta_nsi
                 else if( kfl_ustar == 1 ) then
                    ustar = 0.41_rp  / log((delta_nsi+rough_dom) / rough_dom)
                    us2 = rho * u * ustar*ustar
                 else if( kfl_ustar == 2 ) then ! ABL 2
!                    velfr = 0.41_rp / log((delta_nsi+rough_dom) / rough_dom)
!                    velf2 = kinen**0.5 *cmu_st**0.25 ! frveloc in terms of kinetic turbulent energy 

!                    fact1 = gbden * velfr * velf2
                 else
                    us2 = rho * ustar *ustar / u
                 end if
              end if
              if(u/=0.0_rp) then
                 do idime=1,ndime
                    gevec(idime,ipoin) = -us2*veloc(idime,ipoin,1)
                 end do
              end if
           else
              do idime = 1,ndime
                 gevec(idime,ipoin) = 0.0_rp
              end do
           end if
        end do

     else
        !
        ! Up to the wall
        !
        do ipoin = 1,npoin
           ibopo = lpoty(ipoin)
           if( ibopo >= 1 ) then
              n1  = exnor(1,1,ibopo)
              n2  = exnor(2,1,ibopo)
              s11 = gradv_nsi(1,ipoin)
              s22 = gradv_nsi(2,ipoin)
              s12 = gradv_nsi(3,ipoin)
              sn1 = s11*n1+s12*n2
              sn2 = s12*n1+s22*n2
              if( ndime == 3 ) then
                 n3   = exnor(3,1,ibopo) 
                 s33  = gradv_nsi(4,ipoin)
                 s13  = gradv_nsi(5,ipoin)
                 s23  = gradv_nsi(6,ipoin)
                 sn1  = sn1+s13*n3
                 sn2  = sn2+s23*n3
                 sn3  = s13*n1+s23*n2+s33*n3
                 stnn = sn1*n1+sn2*n2+sn3*n3
                 stx  = sn1-stnn*n1
                 sty  = sn2-stnn*n2
                 stz  = sn3-stnn*n3
                 gevec(1,ipoin)=stx         
                 gevec(2,ipoin)=sty       
                 gevec(3,ipoin)=stz      
              else
                 stnn = sn1*n1+sn2*n2
                 stx  = sn1-stnn*n1
                 sty  = sn2-stnn*n2
                 gevec(1,ipoin)=stx         
                 gevec(2,ipoin)=sty       
              end if
           else
              do idime = 1,ndime
                 gevec(idime,ipoin) = 0.0_rp
              end do
           end if
        end do
     end if
  end if
  !
  ! Deallocate memory
  !
  if( delta_nsi > 0.0_rp .or. kfl_delta == 1 ) then        
  else
     call memory_deallo(mem_modul(1:2,modul),'GRADV_NSI','nsi_outtan',gradv_nsi)
  end if

end subroutine nsi_outtan

!------------------------------------------------------------------------ 
!
! - Velocity strain rates gravb(ntens,npoin)
!   gradv_nsi(1,ipoin)=mu*(du/dx+du/dx)     
!   gradv_nsi(2,ipoin)=mu*(dv/dy+dv/dy)     
!   gradv_nsi(3,ipoin)=mu*(du/dy+dv/dx)     
!   gradv_nsi(4,ipoin)=mu*(dw/dz+dw/dz)     
!   gradv_nsi(5,ipoin)=mu*(du/dz+dw/dz)     
!   gradv_nsi(6,ipoin)=mu*(dv/dz+dw/dy)
! 
! In the case of the law of the wall, on walls:
! 
! (sig.n).t = rho*(U*)^2
! 
! Otherwise:      
!  
! Let sij=1/2(ui,j+uj,i), the traction is given by      
!         +-                                               -+
!         | -p*n1 + 2*mu*s11*n1 + 2*mu*s12*n2 + 2*mu*s13*n3 |
! sig.n = | -p*n2 + 2*mu*s12*n1 + 2*mu*s22*n2 + 2*mu*s23*n3 |
!         | -p*n3 + 2*mu*s13*n1 + 2*mu*s23*n2 + 2*mu*s33*n3 |
!         +-                                               -+
!       = (sn1,sn2,sn3)
! 
! The tangential component of the traction is computed as:
! (sig.n).t = sig.n-(n.sig.n)n
!           = (sn1,sn2,sn3)-(sn1*n1+sn2*n2+sn3*n3)(n1,n2,n3)
!             +-                                       -+
!             | sn1 - sn1*n1*n1 - sn2*n2*n1 - sn3*n3*n1 |
!           = | sn2 - sn1*n1*n2 - sn2*n2*n2 - sn3*n3*n2 |
!             | sn3 - sn1*n1*n3 - sn2*n2*n3 - sn3*n3*n3 |
!             +-                                       -+
! 
! NB: the pressure does not intervene in the tangential stress
!     and grave(j,i)=mu*(ui,j+uj,i) so that sni=grave(j,i)*nj      
!***
!-----------------------------------------------------------------------
