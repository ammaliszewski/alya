subroutine nsi_outres

!------------------------------------------------------------------------
!****f* Nastin/nsi_outres
! NAME 
!    nsi_outres
! DESCRIPTION
!    This routine output the velocity residual
! USES
! USED BY
!    nsi_output
!------------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      def_nastin
  implicit none
  integer(ip) :: ipoin,idime
  real(rp)    :: rnorm
!
! Compute residual
!
  rnorm=0.0_rp
  do ipoin=1,npoin
     gesca(ipoin)=0.0_rp
     do idime=1,ndime
        rnorm=rnorm&
             +veloc(idime,ipoin,1)**2
        gesca(ipoin)=gesca(ipoin)&
             +(veloc(idime,ipoin,1)-veold_nsi(idime,ipoin))**2
     end do
  end do
  do ipoin=1,npoin
     gesca(ipoin)=100.0_rp*sqrt(gesca(ipoin)/rnorm)
  end do

end subroutine nsi_outres
