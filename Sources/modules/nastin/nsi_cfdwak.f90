 !-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_cfdwak.f90
!> @author  Guillaume Houzeaux
!> @brief   Read physical data
!> @details Read physical data
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_cfdwak(itask)
  use def_kintyp
  use def_elmtyp , only : ELFEM
  use def_inpout
  use def_master
  use def_nastin
  use def_domain
  use mod_memory
  use def_kermod
  use mod_elmgeo, only  : elmgeo_natural_coordinates 
  use mod_elsest, only  : elsest_host_element
  use mod_ker_proper 
  implicit none
  integer(ip), intent(in)    :: itask
  integer(ip)                :: ielem,pelty,pnode,pgaus,inode
  integer(ip)                :: idime,igaus,pmate,kmate,ipoin
  integer(ip)                :: kmate_wake,imate,dummi,ifoun
  integer(ip)                :: ptopo,jelse
  real(rp)                   :: elcod(ndime,mnode)
  real(rp)                   :: xjacm(9),gpdet,gpvol
  real(rp)                   :: gpcod(3),lmini,lmaxi,dista
  real(rp)                   :: A_disk,d_ref,n_disk(3),V_disk,d_disk
  real(rp)                   :: shapf(64),deriv(64*3),coloc(3), dummr
  integer(ip), pointer, save :: lelem_material_nsi(:) => null()
  real(rp),    pointer       :: xvolu(:),xxcog(:,:),xxref(:,:)
  real(rp),    pointer       :: xvelo(:,:), xdensi(:)
  !
  ! Initialization
  !
  if( kfl_force_nsi /= 1 ) return
  kmate = max(nmate,1_ip)

  select case ( itask )

  case ( 1_ip )

     !----------------------------------------------------------------
     !
     ! Initialize CFD Wake (for material force term)
     !
     !---------------------------------------------------------------
     !
     ! Allocate memory
     !
     nullify(xvolu)
     nullify(xxcog)
     nullify(xxref)
     call memory_alloca(mem_modul(1:2,modul),'XVOLU','nsi_cfdwak',xvolu,kmate)
     call memory_alloca(mem_modul(1:2,modul),'XXVOG','nsi_cfdwak',xxcog,3_ip,kmate)
     call memory_alloca(mem_modul(1:2,modul),'XXREF','nsi_cfdwak',xxref,3_ip,kmate)
     !
     ! KMATE_WAKE = Number of wake materials
     !
     kmate_wake = 0
     do pmate = 1,kmate
        if( lforc_material_nsi(pmate) == 1 ) kmate_wake = kmate_wake + 1
     end do
     !
     ! Compute volume
     !
     if( INOTMASTER ) then

        do ielem = 1,nelem
           pelty = ltype(ielem)  
           pmate = 1
           if( pelty > 0 ) then
              pnode = nnode(pelty) 
              pgaus = ngaus(pelty)
              if( nmate > 1 ) pmate = lmate(ielem)
              if( lforc_material_nsi(pmate) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    do idime = 1,ndime
                       elcod(idime,inode) = coord(idime,ipoin)
                    end do
                 end do
                 do igaus = 1,pgaus
                    gpcod(1) = 0.0_rp
                    gpcod(2) = 0.0_rp
                    gpcod(3) = 0.0_rp
                    do inode = 1,pnode
                       do idime = 1,ndime
                          gpcod(idime) = gpcod(idime) + elmar(pelty) % shape(inode,igaus) * elcod(idime,inode)
                       end do
                    end do
                    call jacdet(&
                         ndime,pnode,elcod,elmar(pelty) % deriv(1,1,igaus),&
                         xjacm,gpdet)
                    gpvol = elmar(pelty) % weigp(igaus) * gpdet
                    xvolu(pmate) = xvolu(pmate) + gpvol
                    do idime = 1,ndime
                       xxcog(idime,pmate) = xxcog(idime,pmate) + gpcod(idime) * gpvol
                    end do
                 end do
              end if
           end if
        end do

     end if
     !
     ! Parall
     !
     call pararr('SUM',0_ip,kmate,xvolu)
     call pararr('SUM',0_ip,kmate*3_ip,xxcog)
     do pmate = 1,kmate
        if( lforc_material_nsi(pmate) == 1 ) then
           xxcog(1,pmate) = xxcog(1,pmate) / xvolu(pmate)
           xxcog(2,pmate) = xxcog(2,pmate) / xvolu(pmate)
           xxcog(3,pmate) = xxcog(3,pmate) / xvolu(pmate)
        end if
     end do
     !
     ! Output information
     !
     call outfor(59_ip,momod(modul) % lun_outpu,' ')
     !
     ! Save results in force parameters
     !
     do pmate = 1,kmate
        if( lforc_material_nsi(pmate) == 1 ) then
           xforc_material_nsi( 6,pmate) = xxcog(1,pmate)
           xforc_material_nsi( 7,pmate) = xxcog(2,pmate)
           xforc_material_nsi( 8,pmate) = xxcog(3,pmate)
           xforc_material_nsi(15,pmate) = xvolu(pmate)
           d_ref                        = xforc_material_nsi( 2,pmate)
           n_disk(1)                    = xforc_material_nsi( 3,pmate) 
           n_disk(2)                    = xforc_material_nsi( 4,pmate) 
           n_disk(3)                    = xforc_material_nsi( 5,pmate) 
           V_disk                       = xforc_material_nsi(15,pmate)
           d_disk                       = xforc_material_nsi( 1,pmate)
           
           xforc_material_nsi(12,pmate) = xforc_material_nsi(6,pmate) + d_ref  * n_disk(1)
           xforc_material_nsi(13,pmate) = xforc_material_nsi(7,pmate) + d_ref  * n_disk(2)
           xforc_material_nsi(14,pmate) = xforc_material_nsi(8,pmate) + d_ref  * n_disk(3)
           xforc_material_nsi(16,pmate) = 0.7_rp  ! initial or default Ct
           ioutp( 1) = pmate
           routp( 1) = V_disk/d_disk
           routp( 2) = d_ref     
           routp( 3) = n_disk(1)     
           routp( 4) = n_disk(2)     
           routp( 5) = n_disk(3)     
           routp( 6) = xforc_material_nsi( 6,pmate)
           routp( 7) = xforc_material_nsi( 7,pmate)
           routp( 8) = xforc_material_nsi( 8,pmate)
           routp( 9) = xforc_material_nsi(12,pmate)
           routp(10) = xforc_material_nsi(13,pmate)
           routp(11) = xforc_material_nsi(14,pmate)
           routp(12) = V_disk
           routp(13) = d_disk

           call outfor(60_ip,momod(modul) % lun_outpu,' ')

        end if
     end do
     !
     ! Deallocate memory
     !
     if( IMASTER ) call nsi_memphy(-5_ip)
     call memory_deallo(mem_modul(1:2,modul),'XVOLU','nsi_cfdwak',xvolu)
     call memory_deallo(mem_modul(1:2,modul),'XXVOG','nsi_cfdwak',xxcog)
     call memory_deallo(mem_modul(1:2,modul),'XXREF','nsi_cfdwak',xxref)
     !
     ! Look for host element of x_ref points
     !
     if( INOTMASTER ) then
        call memory_alloca(mem_modul(1:2,modul),'LELEM_MATERIAL_NSI','nsi_cfdwak',lelem_material_nsi,kmate)
        imate = 0
        do pmate = 1,kmate
           if( lforc_material_nsi(pmate) == 1 ) then
            
              jelse = ielse(14)
              ielse(14) = ELFEM

              call elsest_host_element(&
                ielse,relse,1_ip,meshe(ndivi),xforc_material_nsi(12:,pmate),ielem,&
                shapf,deriv,coloc,dista)

              !call elsest(&
              !     2_ip,1_ip,ielse,mnode,ndime,npoin,nelem,nnode(1:),&
              !     lnods,ltype,ltopo,coord,xforc_material_nsi(12,pmate),relse,&
              !     ielem,shapf,deriv,coloc,lelch)

              if( ielem > 0 ) then
                 imate = imate + 1
                 lelem_material_nsi(pmate) = ielem
              end if
              ielse(14)=jelse
           end if
          
        end do
     end if
     call parari('SUM',0_ip,1_ip,imate)
     if( imate /= kmate_wake .and. INOTMASTER ) then
        npari =  kmate
        pari1 => lelem_material_nsi
        call Parall(707_ip) 
        if( INOTMASTER .and. pard1 == 1 ) then 
           do pmate = 1,kmate
              lelem_material_nsi(pmate) = max(lelem_material_nsi(pmate),0_ip)
           end do
        end if
     end if

  case ( 2_ip )

     !----------------------------------------------------------------
     !
     ! Compute velocity at reference position and calculate Ct
     !
     !----------------------------------------------------------------

     nullify(xvelo)
     call memory_alloca(mem_modul(1:2,modul),'XVELO','nsi_cfdwak',xvelo,3_ip,kmate)
     nullify(xdensi)
     call memory_alloca(mem_modul(1:2,modul),'XDENSI','nsi_cfdwak',xdensi,kmate)

     if( INOTMASTER ) then
        lmini = -relse(1)
        lmaxi =  relse(1) + 1.0_rp

        do pmate = 1,kmate
           if( lforc_material_nsi(pmate) == 1 ) then
              ielem = lelem_material_nsi(pmate) 
              if( ielem > 0 ) then !only one process has ielem >0
                 pelty = abs(ltype(ielem))
                 pnode = nnode(pelty)
                 ptopo = ltopo(pelty)
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    do idime = 1,ndime
                       elcod(idime,inode) = coord(idime,ipoin)
                    end do
                 end do
                 call elmgeo_natural_coordinates(      &
                      ndime,pelty,pnode,elcod,shapf,   &
                      deriv,xforc_material_nsi(12:,pmate),coloc,ifoun)

                 !call elsest_chkelm(&
                 !     ndime,ptopo,pnode,elcod,shapf,deriv,&
                 !     xforc_material_nsi(12,pmate),coloc,ifoun,lmini,lmaxi)

                 if( ifoun == 0 ) then
                    call runend('NSI_CFDWAK: WE HAVE A PROBLEM')
                 else
                    do inode = 1,pnode                 
                       ipoin = lnods(inode,ielem)
                       do idime = 1,ndime
                          xvelo(idime,pmate) = xvelo(idime,pmate) + shapf(inode) * veloc(idime,ipoin,1)
                       end do                    
                    end do
                    ! loads density
                    call ker_proper('DENSI','COG  ',dummi,ielem,xdensi(pmate:pmate),dummi, dummi,shapf,deriv)
                 end if
              end if
           end if
        end do
     end if

     call pararr('SUM',0_ip,kmate*3_ip,xvelo)
     call pararr('SUM',0_ip,kmate,xdensi) 
     if( INOTMASTER ) then
        do pmate = 1,kmate
           if( lforc_material_nsi(pmate) == 1 ) then
              xforc_material_nsi( 9,pmate) = xvelo(1,pmate)
              xforc_material_nsi(10,pmate) = xvelo(2,pmate)
              xforc_material_nsi(11,pmate) = xvelo(3,pmate)
              ! calculates Ct and power
              call nsi_ctcalc(xforc_material_nsi(1, pmate), pmate)
              !
              ! only first process writes Ct, Cp and  power in nsi.log 
              !
              if (kfl_paral==1.or.kfl_paral==-1)  then
                 ioutp(1) = ittim
                 ioutp(2) = pmate
                 routp(1) = xforc_material_nsi(16, pmate) !Ct
                 routp(2) = xforc_material_nsi(17, pmate) !Cp
                 routp(3) = xforc_material_nsi(18, pmate) !Infinite velocity
                 ! power =0.5*rho*cp*U^3*A
                 routp(4) = 0.5_rp*xdensi(pmate)*xforc_material_nsi(15, pmate)/&
                      xforc_material_nsi(1, pmate)*xforc_material_nsi(17, pmate)*&
                      xforc_material_nsi(18, pmate)*xforc_material_nsi(18, pmate)*&
                      xforc_material_nsi(18, pmate)
                 ! I want to write in name.nsi.log, but only master process can write to, 
                 ! this file, but it does not have the data
                 ! by the moment I will write data to screen
                 call outfor(-65_ip,-6,' ')
              end if

           end if

        end do
     end if     
      
      
     call memory_deallo(mem_modul(1:2,modul),'XVELO','nsi_cfdwak',xvelo)
   
  end select

end subroutine nsi_cfdwak
