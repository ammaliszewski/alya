subroutine nsi_outcpu()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_outcpu
  ! NAME 
  !    nsi_outcpu
  ! DESCRIPTION
  !    This routine writes a summary of spent computing time. The
  !    quantities computed by the code are:
  !      
  !    CPUTI_NSI(1) .... Element assembly 
  !    CPUTI_NSI(2) .... Boundary assembly
  !    CPUTI_NSI(3) .... SGS solver
  !    CPUTI_NSI(4) .... Correction
  !
  ! USES
  !    outfor
  ! USED BY
  !    nsi_turnof
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_nastin
  use mod_communications, only : PAR_MAX
  implicit none
  real(rp) :: xfact,cpuot

  call PAR_MAX(10_ip,cputi_nsi,'IN MY CODE')
  
  if( INOTSLAVE ) then
     !
     ! Initializations
     !
     routp(1) = cpu_modul(30,modul)
     call outfor(29_ip,momod(modul)%lun_outpu,' ')

     if( cpu_modul(30,modul) > 0.0_rp ) then
        xfact = 100.0_rp / routp(1)
     else
        xfact = 1.0_rp
     end if
     cpuot = 0.0_rp

     if( NSI_MONOLITHIC ) then
        !
        ! MONOLITHIC
        !
        coutp(1)  = 'SOLVER   MOM.+CON.'
        routp(1)  = solve(1) % cputi(1)
        routp(2)  = xfact * routp(1)
        cpuot     = cpuot + routp(1)
        call outfor(30_ip,momod(modul)%lun_outpu,' ')

     else if( NSI_SCHUR_COMPLEMENT ) then
        !
        ! SCHUR COMPLEMENT
        !
        coutp(1)  = 'SOLVER   MOMENTUM='
        routp(1)  = solve(1) % cputi(1)
        routp(2)  = xfact * routp(1)
        cpuot     = cpuot + routp(1)
        call outfor(30_ip,momod(modul)%lun_outpu,' ')
        coutp(1)  = 'SOLVER   CONTINUITY='
        routp(1)  = solve(2) % cputi(1)
        routp(2)  = xfact * routp(1)
        cpuot     = cpuot + routp(1)
        call outfor(30_ip,momod(modul)%lun_outpu,' ')       
     end if

     if( kfl_corre_nsi /= 0 ) then
        coutp(1)  = 'VELOCITY CORRECTION='
        routp(1)  = cputi_nsi(4)
        routp(2)  = xfact * routp(1)
        cpuot     = cpuot + routp(1)
        call outfor(30_ip,momod(modul)%lun_outpu,' ')
     end if
     if( kfl_sgsti_nsi /= 0 .or. kfl_sgsco_nsi /= 0 .or. kfl_stabi_nsi /= 0 ) then
        coutp(1)  = 'SGS + PROJECTION=' 
        routp(1)  = cputi_nsi(3)
        routp(2)  = xfact * routp(1)
        cpuot     = cpuot + routp(1)
        call outfor(30_ip,momod(modul)%lun_outpu,' ')
     end if

     cpuot = cpu_modul(30,modul) - cpuot
     !
     ! Others
     !
     coutp(1)  = 'OTHERS'
     routp(1)  = cpuot
     routp(2)  = xfact*routp(1)
     call outfor(30_ip,momod(modul)%lun_outpu,' ')

  end if

end subroutine nsi_outcpu
