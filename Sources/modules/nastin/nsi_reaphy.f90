!-----------------------------------------------------------------------
!> @addtogroup NastinInput
!> @{
!> @file    nsi_reaphy.f90
!> @author  Guillaume Houzeaux
!> @brief   Read physical data
!> @details Read physical data
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_reaphy()
  !-----------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_nastin
  use def_domain
  use def_kermod, only : gasco, kfl_prope
  implicit none
  integer(ip) :: imate, icoun
  real(rp)    :: dummr
  
  if( INOTSLAVE ) then
     !
     ! Initializations (defaults)
     !
     kfl_timei_nsi = 0                                    ! Time integration off
     kfl_advec_nsi = 0                                    ! Convection is off
     kfl_confi_nsi = 0                                    ! Flow is not confined -- This should not be initialized here it is initialized in nsi_reabcs
     kfl_visco_nsi = 1                                    ! Viscous term on
     kfl_colev_nsi = 0                                    ! No coupling with LEVELS
     kfl_regim_nsi = 0                                    ! Incompressible flow
     kfl_dynco_nsi = 0                                    ! Dynamical coupling
     kfl_prthe_nsi = 0                                    ! Thermodynamic pressure calculation
     kfl_surte_nsi = 0                                    ! Do not include surface tension
     kfl_force_nsi = 0                                    ! Force type           
     kfl_mfrco_nsi = 0                                    ! Mass flow rate control activation flag (=0 OFF, =1 ON)
     kfl_bnods_nsi = 0                                    ! boundary nodes defined
     kfl_hydro_gravity_nsi = 0                            ! No hydrostatic gravity

     fcons_nsi     = 0.0_rp                               ! Non conservative form default
     fvins_nsi     = 1.0_rp                               ! Divergence form default
     dtinv_nsi     = 1.0_rp                               ! 1/dt=1.0
     nbnod_nsi     = 0                                    ! Number of nodes on boundary
     nbval_nsi     = 0                                    ! number of boundary values for time-space boundary from file
     nbtim_nsi     = 0                                    ! number of time instances for time-space boundary from file

     grnor_nsi     = 0.0_rp                               ! Gravity norm
     gravi_nsi     = 0.0_rp                               ! Gravity vector
     gravb_nsi     = 0.0_rp                               ! Gravity vector for Boussinesq coupling
     fvnoa_nsi     = 0.0_rp                               ! Frame angular velocity norm     
     fvdia_nsi     = 0.0_rp                               ! Frame angular velocity direction 
     fvela_nsi     = 0.0_rp                               ! Frame angular velocity vector    
     fvpaa_nsi     = 0.0_rp                               ! Frame angular velocity parameters  
     kfl_fvfua_nsi = 0                                    ! Frame angular velocity function  
     fanoa_nsi     = 0.0_rp                               ! Frame angular acceleration norm   
     fadia_nsi     = 0.0_rp                               ! Frame angular acceleration direction 
     facca_nsi     = 0.0_rp                               ! Frame angular acceleration vector       
     fvnol_nsi     = 0.0_rp                               ! Frame linear velocity norm 
     fvdil_nsi     = 0.0_rp                               ! Frame linear velocity direction     
     fvell_nsi     = 0.0_rp                               ! Frame linear velocity vector        
     fvpal_nsi     = 0.0_rp                               ! Frame linear velocity parameters  
     kfl_fvful_nsi = 0                                    ! Frame linear velocity function  
     fanol_nsi     = 0.0_rp                               ! Frame linear acceleration norm      
     fadil_nsi     = 0.0_rp                               ! Frame linear acceleration direction 
     faccl_nsi     = 0.0_rp                               ! Frame linear acceleration vector 
     frotc_nsi(1:3)= 1.0e20_rp                            ! Rotation center
     centr_nsi     = 1.0_rp                               ! Centrifugal force

     kfl_cotem_nsi = 0                                    ! No coupling with temperature
     boube_nsi     = 0.0_rp                               ! Beta
     bougr_nsi     = 0.0_rp                               ! g
     boutr_nsi     = 0.0_rp                               ! Tr
     lowtr_nsi    =  0.0_rp                               ! Ref tempe for hydrodynamic pressure in Low Mach regime

     kfl_cotur_nsi = 0                                    ! No coupling with Turbulence 
     kfl_grtur_nsi = 0                                    ! Do not add grad(K)
     turbu_nsi     = 0.0_rp                               ! Turbulence parameter
     heihy_nsi     = 0.0_rp                               ! Height for hydrostatic pressure
     if (kfl_prope ==0 ) gasco     = 287.0_rp             ! Gas constant R [J/K Kg], only if kernel does not carry it
     sphea_nsi     = 1006.0_rp                            ! Specific heat Cp [J/K Kg]
     prthe_nsi     = 101325.0_rp                          ! Thermodynamic pressure
     imate         = 1

     surte_nsi = 0.0_rp                                   ! Surface tension coeficient (sigma)
     tmass_nsi = 0.0_rp                                   ! initial mean density for low Mach problem
     mfrub_nsi = 0.0_rp                                   ! Target bulk velocity when mass flow control activated
     ubpre_nsi = 0.0_rp                                   ! Bulk velocity from previous time-steo
     mfrse_nsi = 1                                        ! Set from which the mass flow rate is calculated (by default = 1)
     mfccf_nsi = 1.0_rp                                   ! Coefficient for the mass flow control formula (by default = 1)
     !
     ! Reach the section
     !
     rewind(lisda)
     call ecoute('nsi_reaphy')
     do while(words(1)/='PHYSI')
        call ecoute('nsi_reaphy')
     end do
     !--><group>
     !-->    <groupName>PHYSICAL_PROBLEM</groupName>
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Physical properties definition
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> PHYSICAL_PROBLEM
     !
     do while(words(1)/='ENDPH')

        call ecoute('nsi_reaphy')

        if(words(1) == 'PROBL' ) then

           call ecoute('nsi_reaphy')

           do while(words(1)/='ENDPR')
              !--><subGroup>
              !-->     <subGroupName>PROBLEM_DEFINITION</subGroupName>           
              !
              ! ADOC[1]> PROBLEM_DEFINITION
              ! 
              if(words(1)=='TEMPO') then          
                 !--><inputLine>
                 !-->      <inputLineName>TEMPORAL_DERIVATIVES</inputLineName>
                 !-->      <inputLineHelp>
			           !-->	   <![CDATA[TEMPORAL_DERIVATIVES:			     
                 !-->         Decide if time derivative should be calculated in the Navier-Stokes equations.
                 !-->         This term should be put to ON as for most of the problems, the convergence
                 !-->          is enhanced when passing through a stationary regime to reach a steady state.
                 !-->        ]]>
			           !-->		</inputLineHelp>
                 !--></inputLine> 
                 !
                 ! ADOC[2]> TEMPORAL_DERIVATIVES: ON | OFF                                                              $ Existence of temporal derivatives
                 ! ADOC[d]> TEMPORAL_DERIVATIVES:
                 ! ADOC[d]> Decide if time derivative should be calculated in the Navier-Stokes equations.
                 ! ADOC[d]> This term should be put to ON as for most of the problems, the convergence
                 ! ADOC[d]> is enhanced when passing through a stationary regime to reach a steady state.
                 !
                 if(exists('ON   ')) kfl_timei_nsi = 1

              else if( words(1) == 'HYDRO' ) then 
                 !
                 ! Hydrostatic gravity term in the RHS of NS equations
                 !
                 if(exists('ON   ')) kfl_hydro_gravity_nsi = 1

              else if(words(1)=='CONVE') then 
                 !--><inputLine>
                 !-->     <inputLineName>CONVECTIVE_TERM</inputLineName>
                 !-->     <inputLineHelp><![CDATA[Decide if the convective term should be calculated.]]></inputLineHelp>
                 !--> </inputLine>                         
                 !
                 ! ADOC[2]> CONVECTIVE_TERM:      ON | OFF                                                              $ Existence of convective term    
                 ! ADOC[d]> CONVECTIVE_TERM:
                 ! ADOC[d]> Decide if the convective term should be calculated.
                 !
                 if( words(2) /= 'OFF  ') kfl_advec_nsi = 1
                 if( words(2) == 'NONCO' ) then
                    fcons_nsi = 0.0_rp
                 else if( words(2) == 'CONSE' ) then
                    fcons_nsi = 1.0_rp                 
                 else if(words(2) == 'SKEWS' ) then                 
                    fcons_nsi = 0.5_rp
                 end if
                 if( words(3) == 'VELOC' ) then
                    if( words(4) == 'NAVIE' ) then
                       kfl_advec_nsi = 1
                    else
                       kfl_advec_nsi = getint('VELOC',0_ip,'#Velocity function')
                    end if
                 end if

              else if(words(1)=='VISCO') then 
                 !--> <inputLine>
				         !-->      <inputLineName>VISCOUS_TERM</inputLineName>
				         !-->      <inputLineHelp><![CDATA[Form of the viscous term. Different forms are available dependending on the problem treated.
				         !-->                         <ul>    
				         !-->                         <li> LAPLACIAN: this form is the simplest one and it requires less operations than the others. It is however an approximation
				         !-->                              for low Mach regime or fluid with variable viscosity.</li>
				         !-->                         <li> DIVERGENCE: this form is the correct one for flows with variable viscosity </li>
				         !-->                         <li> COMPLETE: this form should be used for low Mach regime as it includes the non-zero velocity divergence term. </li></ul>
				         !-->                      ]]>
				         !-->      </inputLineHelp>
				         !-->      <inputElement>
				         !-->          <inputElementType>combo</inputElementType>
				         !-->          <item>
				         !-->              <itemName>OFF</itemName>
				         !-->          </item>
				         !-->          <item>
				         !-->              <itemName>LAPLACIAN</itemName>
				         !-->          </item>
				         !-->          <item>
				         !-->              <itemName>DIVERGENCE</itemName>
				         !-->          </item>
                 !-->          <item>
                 !-->              <itemName>COMPLETE</itemName>
                 !-->          </item>
                 !-->      </inputElement>
                 !-->  </inputLine>
                 !
                 ! ADOC[2]> VISCOUS_TERM:         OFF | LAPLACIAN | DIVERGENCE | COMPLETE                               $ Viscous term 
                 ! ADOC[d]> VISCOUS_TERM: 
                 ! ADOC[d]> Form of the viscous term. Different forms are available dependending on the problem treated.
                 ! ADOC[d]> <ul>
                 ! ADOC[d]> <li> LAPLACIAN: this form is the simplest one and it requires less operations than the others. It is however an approximation
                 ! ADOC[d]>      for low Mach regime or fluid with variable viscosity. </li>
                 ! ADOC[d]> <li> DIVERGENCE: this form is the correct one for flows with variable viscosity. </li>
                 ! ADOC[d]> <li> COMPLETE: this form should be used for low Mach regime as it includes the non-zero velocity divergence term. </li>
                 ! ADOC[d]> </ul> 
                 !
                 if(words(2)=='OFF  ') kfl_visco_nsi = 0
                 if(words(2)=='ON   ') fvins_nsi     = 0.0_rp                        ! Divergence form default
                 if(words(2)=='DIVER') fvins_nsi     = 1.0_rp                        ! Idem
                 if(words(2)=='LAPLA') fvins_nsi     = 0.0_rp                        ! Laplacian form
                 if(words(2)=='COMPL' .or. words(2)=='FULL ') fvins_nsi = 2.0_rp     ! Complete form

              else if( words(1) == 'REGIM' ) then
                 !--><inputLine>
                 !-->    <inputLineName>REGIME</inputLineName>
                 !-->    <inputLineHelp>Regime of the flow. If the flow is low mach, additional information is required. If system is open</inputLineHelp>
                 !-->    <inputElement>
                 !-->        <inputElementType>combo</inputElementType>
                 !-->        <item>
                 !-->            <itemName>INCOMPRESSIBLE</itemName>                            
                 !-->        </item>
                 !-->        <item>
                 !-->            <itemName>LOW_MACH</itemName>
                 !-->            <itemDependence>0</itemDependence>
                 !-->        </item>
                 !-->    </inputElement>
                 !-->    <inputElement>
                 !-->        <inputElementGroup>0</inputElementGroup>
                 !-->        <inputElementType>edit</inputElementType>
                 !-->        <inputElementValueType>REAL</inputElementValueType>                 
                 !-->        <inputLineEditName>Temperature</inputLineEditName>
                 !-->        <inputLineEditValue>5</inputLineEditValue>
                 !-->    </inputElement>
                 !-->    <inputElement>
                 !-->        <inputElementGroup>0</inputElementGroup>
                 !-->        <inputElementType>edit</inputElementType>
                 !-->        <inputElementValueType>REAL</inputElementValueType>                 
                 !-->        <inputLineEditName>Pressure</inputLineEditName>
                 !-->        <inputLineEditValue>5</inputLineEditValue>
                 !-->    </inputElement>
                 !--></inputLine>
                 !
                 ! ADOC[2]> REGIME:               INCOMPRESSIBLE | LOW_MACH [TEMPERATURE= real or PRESSURE= real]       $ Flow regime. If low mach, reference temp or press are needed
                 ! ADOC[d]> REGIME:
                 ! ADOC[d]> Regime of the flow. If the flow is low mach, additional information is required.
                 ! ADOC[d]> If system is open: 
                 !
                 if(words(2)=='INCOM') then                 ! Incompressible
                    kfl_regim_nsi=0
                 else if(words(2)=='COMPR') then            ! Compressible
                    kfl_regim_nsi=1
                    if(exists('PRESS')) kfl_regim_nsi=1
                    if(exists('DENSI')) kfl_regim_nsi=2
                 else if(words(2)=='LOWMA') then            ! Low-Mach
                    kfl_regim_nsi=3
                    if(exists('TEMPE')) then
                       lowtr_nsi = getrea('TEMPE',0.0_rp,'#Reference temperature for hydrodynamic pressure')
                    endif                   
                 end if

              else if(words(1)=='DYNAM') then               ! Dynamical coupling
                 if(words(2)=='ON   ') kfl_dynco_nsi=1

              else if(words(1)=='GRAVI') then
                 !--><inputLine>
                 !-->   <inputLineName>GRAVITY</inputLineName>
                 !-->   <inputLineHelp> If this option is present, the gravity acceleration is added to the Navier-Stokes equations. Nastin does not use Kermod's gravity, as some modules could require gravity (Partis) but Nastin could run without.</inputLineHelp>
                 !-->  <inputElement>
                 !-->      <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->      <inputLineEditName>Norm</inputLineEditName>
                 !-->       <inputLineEditValue>5</inputLineEditValue>
                 !-->   </inputElement>
                 !-->   <inputElement>
                 !-->       <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->       <inputLineEditName>GX</inputLineEditName>
                 !-->       <inputLineEditValue>5</inputLineEditValue>
                 !-->   </inputElement>
                 !-->   <inputElement>
                 !-->       <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->       <inputLineEditName>GY</inputLineEditName>
                 !-->       <inputLineEditValue>5</inputLineEditValue>
                 !-->   </inputElement>
                 !-->   <inputElement>
                 !-->       <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->       <inputLineEditName>GZ</inputLineEditName>
                 !-->       <inputLineEditValue>5</inputLineEditValue>
                 !-->   </inputElement>
                 !--> </inputLine>
                 !
                 ! ADOC[2]> GRAVITY:              NORM= real, GX= real, GY= real, GZ= real                              $ Gravity accelaration
                 ! ADOC[d]> GRAVITY:
                 ! ADOC[d]> If this option is present, the gravity acceleration is added to the 
                 ! ADOC[d]> Navier-Stokes equations. Nastin does not use Kermod's gravity, as some
                 ! ADOC[d]> modules could require gravity (Partis) but Nastin could run without.
                 !
                 grnor_nsi    = getrea('NORM ',0.0_rp,'#Gravity norm')
                 gravi_nsi(1) = getrea('GX   ',0.0_rp,'#x-component of g')
                 gravi_nsi(2) = getrea('GY   ',0.0_rp,'#y-component of g')
                 gravi_nsi(3) = getrea('GZ   ',0.0_rp,'#z-component of g')
                 call vecuni(3_ip,gravi_nsi,dummr)

              else if( words(1) == 'CENTR' ) then
                 !--><inputLine>
                 !-->    <inputLineName>CENTRIFUGAL_FORCE</inputLineName>
                 !-->    <inputLineHelp>GRAVITY: If this option is present, the gravity acceleration is added to the</inputLineHelp>
                 !--></inputLine>
                 !
                 ! ADOC[2]> CENTRIFUGAL_FORCE:    ON | OFF                                                              $ Centrifugal force
                 ! ADOC[d]> GRAVITY:
                 ! ADOC[d]> If this option is present, the gravity acceleration is added to the 

                 if( words(2) == 'ON   ' ) then
                    centr_nsi = 1.0_rp
                 else if( words(2) == 'OFF  ' ) then
                    centr_nsi = 0.0_rp
                 end if

              else if(words(1)=='AXESR') then
                 !--><inputLine>
                 !-->    <inputLineName>Axes_rotation</inputLineName>
                 !-->    <inputLineHelp> Axes rotation vector definition.</inputLineHelp>
                 !-->    <inputElement>
                 !-->        <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->        <inputLineEditName>Norm</inputLineEditName>
                 !-->        <inputLineEditValue>5</inputLineEditValue>
                 !-->    </inputElement>
                 !-->    <inputElement>
                 !-->        <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>
                 !-->        <inputLineEditName>OX</inputLineEditName>
                 !-->        <inputLineEditValue>5</inputLineEditValue>
                 !-->    </inputElement>
                 !-->    <inputElement>
                 !-->        <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->        <inputLineEditName>OY</inputLineEditName>
                 !-->        <inputLineEditValue>5</inputLineEditValue>
                 !-->    </inputElement>
                 !-->    <inputElement>
                 !-->        <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->        <inputLineEditName>OZ</inputLineEditName>
                 !-->        <inputLineEditValue>5</inputLineEditValue>
                 !-->    </inputElement>
                 !--></inputLine>
                 !
                 ! ADOC[2]> AXES_ROTATION:        NORM= real, OX= real, OY= real, OZ= real                              $ Axes rotation vector definition
                 ! ADOC[d]> AXES_ROTATION: 
                 ! ADOC[d]> Axes rotation vector definition. 
                 !
                 fvnoa_nsi    = getrea('NORM ',0.0_rp,'#Axes rotation norm')
                 fvdia_nsi(1) = getrea('OX   ',0.0_rp,'#x-component of w')
                 fvdia_nsi(2) = getrea('OY   ',0.0_rp,'#y-component of w')
                 fvdia_nsi(3) = getrea('OZ   ',0.0_rp,'#z-component of w')
                 call vecuni(3_ip,fvdia_nsi,dummr)

              else if(words(1)=='AXESV') then
                 !--><inputLine>
                 !-->      <inputLineName>AXES_VELOCITY</inputLineName>
                 !-->      <inputLineHelp>Axes velocity vector definition.</inputLineHelp>
                 !-->      <inputElement>
                 !-->          <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>
                 !-->          <inputLineEditName>Norm</inputLineEditName>
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditName>VX</inputLineEditName>
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditName>VY</inputLineEditName>
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditName>VZ</inputLineEditName>
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !-->  </inputLine>
                 !
                 ! ADOC[2]> AXES_VELOCITY:        NORM= real, VX= real, VY= real, VZ= real                              $ Axes velocity vector definition
                 ! ADOC[d]> AXES_VELOCITY:
                 ! ADOC[d]> Axes velocity vector definition.
                 !
                 fvnol_nsi    = getrea('NORM ',0.0_rp,'#Axes velocity norm')
                 fvdil_nsi(1) = getrea('VX   ',0.0_rp,'#x-component of v')
                 fvdil_nsi(2) = getrea('VY   ',0.0_rp,'#y-component of v')
                 fvdil_nsi(3) = getrea('VZ   ',0.0_rp,'#z-component of v')
                 call vecuni(3_ip,fvdil_nsi,dummr)

              else if(words(1)=='ROTAT') then
                 !--><inputLine>
                 !-->    <inputLineName>ROTATION_FUNCTION</inputLineName>
                 !-->    <inputLineHelp><![CDATA[ROTATION_FUNCTION:
                 !-->     Rotation time function definition. It multiplies the AXES_ROTATION vector.
                 !-->     Parameters (up to 6 numbers after the type of function)
                 !-->     CONSTANT (no parameters, it implies a value of 1.0)
                 !-->     PARABOLIC (a t^2 + b t + c)    start_time end_time a b c 
                 !-->     SINUS (a cos( w t + phi) + c)  start_time end_time a w phi c]]></inputLineHelp>
                 !-->    <inputElement>
                 !-->        <inputElementType>combo</inputElementType>
                 !-->        <item>
                 !-->            <itemName>CONSTANT</itemName>
                 !-->        </item>
                 !-->        <item>
                 !-->            <itemName>PARABOLIC</itemName>
                 !-->        </item>
                 !-->        <item>
                 !-->            <itemName>SINUS</itemName>
                 !-->        </item>
                 !-->        <item>
                 !-->            <itemName>EXTERIOR</itemName>
                 !-->        </item>
                 !-->    </inputElement>
                 !--></inputLine>                 
                 !
                 ! ADOC[2]> ROTATION_FUNCTION:    CONSTANT | PARABOLIC | SINUS | EXTERIOR                               $ Rotation function definition
                 ! ADOC[d]> ROTATION_FUNCTION:
                 ! ADOC[d]> Rotation time function definition. It multiplies the AXES_ROTATION vector.
                 ! ADOC[d]> Parameters (up to 6 numbers after the type of function)
                 ! ADOC[d]> CONSTANT (no parameters, it implies a value of 1.0)
                 ! ADOC[d]> PARABOLIC (a t^2 + b t + c)    start_time end_time a b c 
                 ! ADOC[d]> SINUS (a cos( w t + phi) + c)  start_time end_time a w phi c
                 !
                 if(exists('CONST')) kfl_fvfua_nsi = 0
                 if(exists('PARAB')) kfl_fvfua_nsi = 1 
                 if(exists('SINUS')) kfl_fvfua_nsi = 2 
                 if(exists('EXTER')) kfl_fvfua_nsi = 3 
                 fvpaa_nsi = param(1:6)

              else if(words(1)=='VELOC') then
                 !--><inputLine>
                 !-->      <inputLineName>Velocity_function</inputLineName>
                 !-->      <inputLineHelp>Velocity time function definition. It multiplies the AXES_VELOCITY vector.</inputLineHelp>
                 !-->      <inputElement>
                 !-->          <inputElementType>combo</inputElementType>
                 !-->          <item>
                 !-->              <itemName>CONSTANT</itemName>
                 !-->          </item>
                 !-->          <item>
                 !-->              <itemName>PARABOLIC</itemName>
                 !-->          </item>
                 !-->          <item>
                 !-->              <itemName>SINUS</itemName>
                 !-->          </item>
                 !-->          <item>
                 !-->              <itemName>EXTERIOR</itemName>
                 !-->          </item>
                 !-->      </inputElement>
                 !-->  </inputLine>           
                 !
                 ! ADOC[2]> VELOCITY_FUNCTION:    CONSTANT | PARABOLIC | SINUS | EXTERIOR                               $ Velocity function definition
                 ! ADOC[d]> VELOCITY_FUNCTION:
                 ! ADOC[d]> Velocity time function definition. It multiplies the AXES_VELOCITY vector.
                 !
                 if(exists('CONST')) kfl_fvful_nsi = 0
                 if(exists('PARAB')) kfl_fvful_nsi = 1
                 if(exists('SINUS')) kfl_fvful_nsi = 2
                 if(exists('EXTER')) kfl_fvful_nsi = 3
                 fvpal_nsi = param(1:6)

              else if(words(1)=='CENTE') then
                 !--><inputLine>
                 !-->      <inputLineName>Center_rotation</inputLineName>
                 !-->      <inputLineHelp>Center of rotation definition. Used to compute the centrifugal force.</inputLineHelp>
                 !-->      <inputElement>
                 !-->          <inputElementType>edit2</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementType>edit2</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementType>edit2</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !-->  </inputLine>
                 !
                 ! ADOC[2]> CENTER_ROTATION:      real1, real2, real3                                                   $ Center of rotation of coordinates
                 ! ADOC[d]> CENTER_ROTATION:
                 ! ADOC[d]> Center of rotation definition. Used to compute the centrifugal force.
                 !
                 frotc_nsi(1:3) = param(1:3)

              else if((words(1)=='TEMPE'.and.words(2)=='BOUSS').or.&
                   &  (words(1)=='BOUSS'.and.exists('ON   '))) then
                 !--><inputLine>
                 !-->  <inputLineName>Temper_coupling</inputLineName>
                 !-->  <inputLineHelp><![CDATA[TEMPER_COUPLING:
                 !--> Temperature coupling definition. Only Boussinesq coupling is available here.
                 !--> Low Mach system should be chosen as a REGIME (see REGIME option).
                 !--> <ul>
                 !--> <li> real1 = beta: thermal expansion coefficient [1/K]. </li> 
                 !--> <li> real2 = Tref: reference temperature [K]. </li> 
                 !--> <li> real3 = gb: gravity module [m/s2]. It can be useful for example if g_i /= 0 but |g| = 0. </li> 
                 !--> </ul>
                 !--> The force term added to the ith momentum equations is: f_i = rho * gb * g_i * beta * ( T - Tref ).]]> 
                 !--> </inputLineHelp>
                 !-->  <inputElement>
                 !-->      <inputElementType>combo</inputElementType>
                 !-->      <item>
                 !-->          <itemName>OFF</itemName>
                 !-->      </item>
                 !-->      <item>
                 !-->          <itemName>BOUSSINESQ</itemName>
                 !-->          <itemDependence>0</itemDependence>
                 !-->      </item>
                 !-->  </inputElement>
                 !-->  <inputElement>
                 !-->      <inputElementGroup>0</inputElementGroup>
                 !-->      <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->      <inputLineEditName>BETA</inputLineEditName>
                 !-->      <inputLineEditValue>5</inputLineEditValue>
                 !-->  </inputElement>
                 !-->  <inputElement>
                 !-->      <inputElementGroup>0</inputElementGroup>
                 !-->      <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->      <inputLineEditName>TR</inputLineEditName>
                 !-->      <inputLineEditValue>5</inputLineEditValue>
                 !-->  </inputElement>
                 !-->  <inputElement>
                 !-->      <inputElementGroup>0</inputElementGroup>
                 !-->      <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->      <inputLineEditName>G</inputLineEditName>
                 !-->      <inputLineEditValue>5</inputLineEditValue>
                 !-->  </inputElement>
                 !--></inputLine>
                 !
                 ! ADOC[2]> TEMPER_COUPLING:      OFF | BOUSSINESQ [BETA=real1, TR=real2, G=real3]                      $ Coupling with temper module
                 ! ADOC[d]> TEMPER_COUPLING:
                 ! ADOC[d]> Temperature coupling definition. Only Boussinesq coupling is available here.
                 ! ADOC[d]> Low Mach system should be chosen as a REGIME (see REGIME option).
                 ! ADOC[d]> <ul>
                 ! ADOC[d]> <li> real1 = beta: thermal expansion coefficient [1/K]. </li> 
                 ! ADOC[d]> <li> real2 = Tref: reference temperature [K]. </li> 
                 ! ADOC[d]> <li> real3 = gb: gravity module [m/s2]. It can be useful for example if g_i /= 0 but |g| = 0. </li> 
                 ! ADOC[d]> </ul>
                 ! ADOC[d]> The force term added to the ith momentum equations is: f_i = rho * gb * g_i * beta * ( T - Tref ).
                 !
                 kfl_cotem_nsi=1
                 boube_nsi = getrea('BETA ',0.0_rp,'#Beta coefficient')                 
                 boutr_nsi = getrea('TR   ',0.0_rp,'#Reference temperature')
                 bougr_nsi = getrea('G    ',0.0_rp,'#Gravity acceleration')
                 gravb_nsi(1) = getrea('GX   ',0.0_rp,'#x-component of G')
                 gravb_nsi(2) = getrea('GY   ',0.0_rp,'#y-component of G')
                 gravb_nsi(3) = getrea('GZ   ',0.0_rp,'#z-component of G')
                 call vecuni(3_ip,gravb_nsi,dummr) 
                 if (dummr<epsilon(1.0_rp)) then
                    gravb_nsi(1) =gravi_nsi(1)
                    gravb_nsi(2) =gravi_nsi(2)
                    gravb_nsi(3) =gravi_nsi(3)
                 end if
                 if (abs(boube_nsi).lt.epsilon(1.0)) boube_nsi= 1.0/boutr_nsi
              else if(words(1)=='TURBU') then
                 !--><inputLine>
                 !-->      <inputLineName>TURBULENCE_MODEL</inputLineName>
                 !-->      <inputLineHelp>Turbulence Model definition. For LES_MODEL, a real number is required. For smagorinsky, From Turbul options, Nastin uses the turbulent viscosity computed by Turbul.</inputLineHelp>
                 !-->      <inputElement>
                 !-->          <inputElementType>combo</inputElementType>
                 !-->          <item>
                 !-->              <itemName>LES_MODEL</itemName>
                 !-->              <itemDependence>0</itemDependence>
                 !-->          </item>
                 !-->          <item>
                 !-->              <itemName>FROM_TURBUL</itemName>                            
                 !-->          </item>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementGroup>0</inputElementGroup>
                 !-->          <inputElementType>combo</inputElementType>
                 !-->          <item>
                 !-->              <itemName>SMAGORINSKY</itemName>
                 !-->          </item>
                 !-->          <item>
                 !-->              <itemName>WALE</itemName>
                 !-->          </item>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementGroup>0</inputElementGroup>
                 !-->          <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditName>Parameter</inputLineEditName>
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !--></inputLine>              
                 !
                 ! ADOC[2]> TURBULENCE_MODEL:     LES_MODEL [SMAGORINSKY | WALE , PARAMETER=real] | FROM_TURBUL         $ Turbulence model
                 ! ADOC[d]> TURBULENCE_MODEL:
                 ! ADOC[d]> Turbulence Model definition.
                 ! ADOC[d]> For LES_MODEL, a real number is required. For smagorinsky, 
                 ! ADOC[d]> From Turbul options, Nastin uses the turbulent viscosity computed by Turbul.
                 !
                 if(words(2)=='LESMO') then
                    if(exists('SMAGO')) then
                       kfl_cotur_nsi=-1
                       turbu_nsi=getrea('PARAM',0.01_rp,'#Coefficient c**2')   ! The typical value is 0.1**2 = 0.01
                    else if(exists('WALE ')) then
                       kfl_cotur_nsi=-2
                       turbu_nsi=getrea('PARAM',0.25_rp,'#Coefficient c**2')   ! The default for WALE is 0.5**2 = 0.25
                    else if(exists('SIGMA')) then
                       kfl_cotur_nsi=-3
                       turbu_nsi=getrea('PARAM',2.25_rp,'#Coefficient c**2')   ! The default for SIGMA is 1.5**2 = 2.25
                    end if
                 else if(words(2)=='RANSA') then
                    if(exists('MIXIN')) then
                       kfl_cotur_nsi=-10
                       turbu_nsi=getrea('PARAM',0.0_rp,'#Coefficient C L^2')
                    else if(exists('CONST')) then

                       kfl_cotur_nsi=-11
                       turbu_nsi=getrea('PARAM',0.0_rp,'#Constant turbulent viscosity')
                    else if(exists('XUCHE')) then
                       kfl_cotur_nsi=-12
                    end if
                 else if(words(2)=='RANSD'.or.words(2)=='FROMT') then   ! We are using turmu
                    if(exists('TURPR')) kfl_grtur_nsi=1
                    kfl_cotur_nsi=1
                 end if

              else if(words(1)=='LEVEL') then
                 !--><inputLine>
                 !-->   <inputLineName>Levels_coupling</inputLineName>
                 !-->   <inputLineHelp>Coupling with Levels module. If this option is ON, then the air properties should also be provided.</inputLineHelp>
                 !--></inputLine>
                 !
                 ! ADOC[2]> LEVELS_COUPLING:      ON | OFF                                                              $ Coupling with Levels module
                 ! ADOC[d]> LEVELS_COUPLING:
                 ! ADOC[d]> Coupling with Levels module. If this option is ON, then the air properties should
                 ! ADOC[d]> also be provided.
                 !
                 if(words(2)=='ON   ') then
                    kfl_colev_nsi=1
                    if(exists('THICK')) then
                       call runend('NSI_REAPHY: now thickness is read by level and stored in a thick that belongs to master')  
                    end if
                    if(exists('STAGG')) kfl_colev_nsi=3
                    !if(exists('NOHYD')) kfl_nohyd_nsi=1
                    !if(exists('ANALY')) then
                    !   kfl_nohyd_nsi=2
                    !   if( exists('HEIGH') ) &
                    !        heihy_nsi = getrea('HEIGH',0.0_rp,'#Height for hydrostatic pressure')
                    !end if
                 end if

              else if(words(1)=='SUFAC') then
                 !--><inputLine>
                 !-->      <inputLineName>SURFACE_TENSION</inputLineName>
                 !-->      <inputLineHelp>If Levels is used, apply surface tension.</inputLineHelp>
                 !-->      <inputElement>
                 !-->          <inputElementType>combo</inputElementType>
                 !-->          <item>
                 !-->              <itemName>On</itemName>
                 !-->              <itemDependence>0</itemDependence>
                 !-->          </item>
                 !-->          <item>
                 !-->              <itemName>Off</itemName>
                 !-->          </item>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementGroup>0</inputElementGroup>
                 !-->          <inputElementType>edit</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditName>Coefficient</inputLineEditName>
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !--></inputLine>
                 !
                 ! ADOC[2]> SURFACE_TENSION:      ON, COEFFICIENT=real | OFF                                            $ If Levels is used, apply surface tension
                 ! ADOC[d]> SURFACE_TENSION:
                 ! ADOC[d]> If Levels is used, apply surface tension.
                 !
                 if(words(2)=='ON   ') then
                    kfl_surte_nsi = 1
                    if( exists('COEFI') ) &
                         surte_nsi = getrea('COEFI',0.0_rp,'#Surface tension coeficient')
                 end if

              else if(words(1)=='FORCE') then  
                 !--><inputLine>
                 !-->     <inputLineName>Force_term</inputLineName>
                 !-->     <inputLineHelp><![CDATA[FORCE_TERM: 
                 !-->        int1 is the material number. char1 is the force model (only WAKE model is available).
                 !-->        The parameters are: Aref,dref,nx,ny,nz]]></inputLineHelp>
                 !-->     <inputElement>
                 !-->         <inputElementType>combo</inputElementType>
                 !-->         <item>
                 !-->             <itemName>MATERIAL</itemName>
                 !-->             <itemDependence>0</itemDependence>
                 !-->         </item>
                 !-->         <item>
                 !-->             <itemName>Off</itemName>
                 !-->         </item>
                 !-->     </inputElement>
                 !-->     <inputElement>
                 !-->         <inputElementType>list</inputElementType>
                 !-->         <inputElementGroup>0</inputElementGroup>
                 !-->         <item>
                 !-->             <itemName>MATERIALS</itemName>
                 !-->             <itemValueType>INT</itemValueType>
                 !-->         </item>
                 !-->         <item>
                 !-->             <itemName>MODEL</itemName>
                 !-->             <itemValueType>CUSTOM:WAKE</itemValueType>                 
                 !-->         </item>
                 !-->         <item>
                 !-->             <itemName>PARAMETERS</itemName>
                 !-->             <itemValueType>REALSEQ</itemValueType>
                 !-->         </item>
                 !-->     </inputElement>
                 !--> </inputLine> 
                 !
                 ! ADOC[2]> FORCE_TERM:           OFF | MATERIAL                                                        $ Force term
                 ! ADOC[3]> MATERIALS=int1, MODEL=WAKE, PARAMETERS=real1,real2,...                                  $ material force model and parameters 
                 ! ADOC[3]> ...
                 ! ADOC[2]> END_FORCE_TERM
                 ! ADOC[d]> FORCE_TERM: 
                 ! ADOC[d]> int1 is the material number. char1 is the force model (only WAKE model is available).
                 ! ADOC[d]> The parameters are: Aref,dref,nx,ny,nz
                 !
                 if( words(2) == 'MATER' ) then
                    kfl_force_nsi = 1
                    call nsi_memphy(5_ip)
                    call ecoute('nsi_reaphy')
                    do while( words(1) /= 'ENDFO' )
                       if( words(1) == 'MATER' ) then
                          imate = getint('MATER',1_ip,'#Material force number')
                          if( imate < 1 .or. imate > nmate ) call runend('NSI_REAPHY: WRONG MATERIAL NUMBER')
                          if( exists('WAKE ') ) then
                             lforc_material_nsi(imate) = 1
                          else if (exists('CONST')) then
                             lforc_material_nsi(imate) = 2
                          else
                             call runend('NSI_REAPHY: WRONG MATERIAL FORCE MODEL')
                          end if
                          xforc_material_nsi(1:mforc_material_nsi,imate) = param(4:3+mforc_material_nsi)
                       else if (words(1) == 'TABLE') then  ! loads ct cp table
                          if( imate < 1 .or. imate > nmate ) call runend('NSI_REAPHY: WRONG MATERIAL NUMBER TO LOAD TABLE')
                          icoun = 0                          
                          do while( words(1) /= 'ENDTA' )
                             call ecoute('nsi_reaphy')
                             icoun = icoun +1
                             velta_nsi(icoun, imate) = param(1) !velocity
                             thrta_nsi(icoun, imate) = param(2) !thrust coef.
                             powta_nsi(icoun, imate) = param(3) !power  coef.
                          end do                       
                          ntabl_nsi(imate)= icoun -1
                          ! velocity values in table is assumed to be in increasingorder
                          do icoun =1, ntabl_nsi(imate)-1
                             if (velta_nsi(icoun, imate).gt.velta_nsi(icoun+1, imate)) &
                                  call runend('NSI_REAPHY:WRONG CT TABLE,VELOCITY IS NOT IN INCREA')
                          end do

                       else
                          call runend('NSI_REAPHY: WRONG FORCE FIELD') 
                       end if
                       call ecoute('nsi_reaphy')
                    end do
                 else if( words(2) == 'OFF  ' ) then
                    kfl_force_nsi = 0
                 end if                 
                 !
                 ! ADOC[d]> END_FORCE_TERM: 
                 !
              else if(words(1)=='MASSF') then

                 !--><inputLine>
                 !-->     <inputLineName>MASS_FLOW_CONTROL</inputLineName>
                 !-->     <inputLineHelp><![CDATA[
                 !-->     Mass flow control formula using a PD controller.
                 !-->     The formula modifies the force term based on a target bulk velocity over a set. 
                 !-->     The three input parameters are:
                 !-->     <ul>
                 !-->     <li>  real1 is the target bulk velocity, positive values for inlet </li>
                 !-->     <li>  int1 is the set on which we want the target velocity </li>
                 !-->     <li>  real2 is the value of the coefficient b of the formula (optimal value is 0.5) </li>
                 !-->     </ul>
                 !-->     The formula is: Fnew=Fold+(b/dt)*(Utarget-2Ucurrent+Uprevious)]]>
                 !-->     </inputLineHelp>
                 !--> </inputLine> 
                 !
                 ! ADOC[2]> MASS_FLOW_CONTROL: real1, int1, real2
                 ! ADOC[d]> MASS_FLOW_CONTROL: 
                 ! ADOC[d]> real1 is the target bulk velocity, int1 is the set on which we want to impose the target velocity
                 ! ADOC[d]> real2 is the value of the coefficient b of the formula (optimal value is 0.5)
                 !
                 kfl_mfrco_nsi = 1
                 mfrub_nsi = param(1)
                 mfrse_nsi = param(2)
                 mfccf_nsi = param(3)

              else if( words(1) == 'BOUND' ) then
                 !
                 ! ADOC[1]> BOUNDARY_NODES = int                                  $ Number of nodes on the boundary
                 ! ADOC[d]> <b>BOUNDARY_NODES</b>:
                 ! ADOC[d]> This number if the number of boundary nodes. It is used for space and time dependent boundary
                 ! ADOC[d]> conditions defined by an input file.
                 !
                 kfl_bnods_nsi = 1
                 nbnod_nsi = getint('BOUND',0_ip,'*NUMBER OF BOUNDARY NODES')
                 call nsi_memphy(6)
                 call ecoute('nsi_reaphy')
                 icoun = 0
                 do while(words(1)/='ENDBO')
                   icoun = icoun + 1
                   bntab_nsi(icoun,1) = param(3)
                   bntab_nsi(icoun,2) = param(4)
                   bntab_nsi(icoun,3) = param(5)
                   call ecoute('nsi_reaphy')
                 end do
                 if (icoun /= nbnod_nsi) call runend('NSI_REAPHY: WRONG BOUNDARY FILE')

              else if( words(1) == 'VALUE' ) then
                 !
                 ! ADOC[1]> VALUES_ON_BOUNDARY = int1, int2                $ Number of nodes on the boundary
                 ! ADOC[d]> <b>VALUES_ON_BOUNDARY</b>:
                 ! ADOC[d]> This are the values for the space and time dependent boundary
                 ! ADOC[d]> conditions defined by an input file.
                 !
                 if (kfl_bnods_nsi /= 1) call runend('NSI_REAPHY: FOR THE VALUES_ON_BOUNDARY OPTION &
                                                      THE BOUNDARY_NODES ARE REQUIRED AS WELL')
                 nbval_nsi = param(1)
                 nbtim_nsi = param(2)
                 call nsi_memphy(7)
                 call ecoute('nsi_reaphy')
                 icoun = 0
                 do while(words(1)/='ENDVA')
                   icoun = icoun + 1
                   bnval_nsi(icoun,1) = param(3)
                   bnval_nsi(icoun,2) = param(4)
                   bnval_nsi(icoun,3) = param(5)
                   call ecoute('nsi_reaphy')
                 end do
                 if (icoun /= nbval_nsi*nbtim_nsi) call runend('NSI_REAPHY: WRONG BOUNDARY FILE')
                 
              end if

              call ecoute('nsi_reaphy')
              !--> </subGroup>
              !
              ! ADOC[1]> END_PROBLEM_DEFINITION
              ! ADOC[d]> END_PROBLEM_DEFINITION
              ! ADOC[d]>
              !
           end do

        else if(words(1)=='PROPE') then
           !--><subGroup>
           !-->     <subGroupName>PROPERTIES</subGroupName>
           !-->     <subGroupHelp>PROPERTIES:
           !-->           Properties. Here, some properties could be required if Nastin is coupled with another
           !-->           module. The fluid properties (density, viscosity) should be defined in Kermod.</subGroupHelp>
           !
           ! ADOC[1]> PROPERTIES:
           ! ADOC[d]> PROPERTIES:
           ! ADOC[d]> Properties. Here, some properties could be required if Nastin is coupled with another
           ! ADOC[d]> module. The fluid properties (density, viscosity) should be defined in Kermod.
           !
           call ecoute('nsi_reaphy')
           imate=1
           do while(words(1)/='ENDPR')
              if( words(1) == 'MATER' ) then
                 imate = getint('MATER',1_ip,'#Current material')
                 if( imate > nmate ) then
                    print*,'nmate=',nmate
                    call runend('NSI_REAPHY: WRONG MATERIAL')
                 end if
              else if(words(1)=='GASCO') then               ! Gas constant (R)
                 gasco = getrea('GASCO',287.0_rp,'#Gas constant')
              else if(words(1)=='SPECI') then               ! Specific heat (Cp)
                 sphea_nsi = getrea('SPECI',1006.1_rp,'#Gas constantSpecific heat')

              else if(words(1)=='THERM') then               ! Thermodynamic pressure
                 !--><inputLine>
                 !-->      <inputLineName>THERMODYNAMIC_PRESSURE</inputLineName>
                 !-->      <inputLineHelp>Thermodynamic pressure "real" for Low Mach regime. - Closed system: choose MASS_CONSERVATION. Thermodynamics pressure is computed from mass conservation equation. "real" is therefore the initial pressure. - Open system without.</inputLineHelp>
                 !-->      <inputElement>
                 !-->          <inputElementType>edit2</inputElementType>
                 !-->          <inputElementValueType>REAL</inputElementValueType>                 
                 !-->          <inputLineEditValue>5</inputLineEditValue>
                 !-->      </inputElement>
                 !-->      <inputElement>
                 !-->          <inputElementType>combo</inputElementType>
                 !-->          <item>
                 !-->              <itemName>MASS_CONSERVATION</itemName>
                 !-->          </item>
                 !-->          <item>
                 !-->              <itemName>TIME_DEPENDENT</itemName>
                 !-->          </item>
                 !-->      </inputElement>
                 !-->  </inputLine>
                 !
                 ! ADOC[2]> THERMODYNAMIC_PRESSURE: real, MASS_CONSERVATION | TIME_DEPENDENT
                 ! ADOC[d]> THERMODYNAMIC_PRESSURE:
                 ! ADOC[d]> Thermodynamic pressure "real" for Low Mach regime.
                 ! ADOC[d]> - Closed system: choose MASS_CONSERVATION. Thermodynamics pressure is computed from mass conservation equation.
                 ! ADOC[d]>   "real" is therefore the initial pressure.
                 ! ADOC[d]> - Open system without.  
                 !
                 prthe_nsi=param(1) !getrea('THERM',101325.0_rp,'#Thermodynamic pressure')
                 if(exists('MASSC')) then
                    kfl_prthe_nsi=1 ! Computed from mass conservation, closed system
                    tmass_nsi = getrea('MASSC',0.0_rp,'#initial density')
                 else if(exists('TIMED')) then
                    kfl_prthe_nsi=2 ! Computed from evolution equation, closed system
                    tmass_nsi = getrea('TIMED',0.0_rp,'#initial density')
                 else
                    kfl_prthe_nsi=0
                 end if
              end if
              call ecoute('nsi_reaphy')
           end do
        end if
           !--></subGroup>
           !
           ! ADOC[1]> END_PROPERTIES
           !
     end do
     !--></group>
     !
     ! ADOC[0]> END_PHYSICAL_PROBLEM
     !

  end if
end subroutine nsi_reaphy
