subroutine nsi_solsgs(itask)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_solsgs
  ! NAME 
  !    nsi_solsgs
  ! DESCRIPTION
  !
  !    This routine solves the SGS equation and the projection
  !
  !    Global    Elem. Gauss  | Full Oss       Split
  !    -----------------------+-----------------------------------------
  !    VEPRO_NSI ELVEP GPVEP  | -tau1'*R(u)    tau1'*rho*(a.grad)u
  !    PRPRO_NSI ELPRP GPPRP  | tau2'*div(u)   tau2'*div(u)  
  !    GRPRO_NSI ELGRP GPGRP  |    -           tau1'*( grad(p) - rho*f )
  !
  !    Using open rule, the projection of a linear function is NOT the
  !    exact linear function near the boundary
  !
  ! USES
  ! USED BY
  !    nsi_endite
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_domain
  use def_master
  use def_nastin
  use mod_memory
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: icomp,ipoin,idime,imeth
  real(rp)                :: time1,time2,dummr

  if( kfl_sgsti_nsi /= 0 .or. kfl_sgsco_nsi /= 0 .or. kfl_stabi_nsi /= 0 ) then

     select case ( itask )

     case ( 1_ip )
        !
        ! Initialization
        !
        if( kfl_sgsco_nsi == 1 ) itsta_nsi = 0
        resgs_nsi(1) = 0.0_rp
        resgs_nsi(2) = 0.0_rp
        rmsgs_nsi    = 0.0_rp
        if( kfl_sgsco_nsi == 1 .or. kfl_sgsti_nsi == 1 ) then
           itsta_nsi = 0
           resis_nsi = 0.0_rp
           resis_nsi = 0.0_rp
        end if

     case ( 2_ip )
        !
        ! Residual projections
        !
        call cputim(time1)

        if( INOTMASTER .and. kfl_stabi_nsi /= 0 ) then

           call memory_alloca(mem_modul(1:2,modul),'VEPR2_NSI','nsi_solsgs',vepr2_nsi,ndime,npoin)
           call memory_alloca(mem_modul(1:2,modul),'PRPR2_NSI','nsi_solsgs',prpr2_nsi,npoin)
           if( kfl_stabi_nsi == 2 ) then
              call memory_alloca(mem_modul(1:2,modul),'GRPR2_NSI','nsi_solsgs',grpr2_nsi,ndime,npoin)
           end if

        end if
        !
        ! Update SGS and projections
        !
        imeth = 1
        if( INOTMASTER .and. kfl_sgscp_nsi == 0 ) then
           if( imeth == 2 ) call opeclo(1_ip)
           !call nsi_elmope(4_ip)   
           call nsi_elmope_omp(4_ip)   
           if( imeth == 2 ) call opeclo(2_ip)
        end if
        !
        ! Residual projections
        !
        if( INOTMASTER .and. kfl_stabi_nsi /= 0 ) then

           call rhsmod(ndime,vepr2_nsi)
           call rhsmod(1_ip, prpr2_nsi)

           if( imeth == 1 ) then

              do ipoin = 1,npoin
                 dummr                    = 1.0_rp / vmass(ipoin)
                 vepro_nsi(1:ndime,ipoin) = vepr2_nsi(1:ndime,ipoin) * dummr 
                 prpro_nsi(ipoin)         = prpr2_nsi(ipoin) * dummr 
              end do
              if( kfl_stabi_nsi == 2 ) then
                 call rhsmod(ndime,grpr2_nsi)
                 do ipoin = 1,npoin
                    dummr                    = 1.0_rp / vmass(ipoin)
                    grpro_nsi(1:ndime,ipoin) = grpr2_nsi(1:ndime,ipoin) * dummr 
                 end do
              end if

           else

              do ipoin = 1,npoin
                 dummr                    = 1.0_rp / vmasc(ipoin)
                 vepro_nsi(1:ndime,ipoin) = vepr2_nsi(1:ndime,ipoin) * dummr 
                 prpro_nsi(ipoin)         = prpr2_nsi(ipoin) * dummr 
              end do
              if( kfl_stabi_nsi == 2 ) then
                 call rhsmod(ndime,grpro_nsi)
                 do ipoin = 1,npoin
                    dummr                    = 1.0_rp / vmasc(ipoin)
                    grpro_nsi(1:ndime,ipoin) = grpr2_nsi(1:ndime,ipoin) * dummr 
                 end do
              end if

           end if

           call memory_deallo(mem_modul(1:2,modul),'VEPR2_NSI','nsi_solsgs',vepr2_nsi)
           call memory_deallo(mem_modul(1:2,modul),'PRPR2_NSI','nsi_solsgs',prpr2_nsi)
           if( kfl_stabi_nsi == 2 ) then
              call memory_deallo(mem_modul(1:2,modul),'GRPR2_NSI','nsi_solsgs',grpr2_nsi)
           end if

        end if
        !
        ! Output SGS convergence
        !
        call nsi_cvgsgs()

        call cputim(time2)
        cputi_nsi(3) = cputi_nsi(3) + time2 - time1

     end select

  end if

end subroutine nsi_solsgs

subroutine caca()
  use def_master
  use def_domain
  use def_nastin
  implicit none
  integer(ip) :: inode,igaus,pelty,ielem,pgaus,pnode,plapl
  integer(ip) :: ipoin,idime,jdime
  real(rp)    :: xproj(3),gradu(3,3),gpvel(3),agrau(3),gpcod(3)
  real(rp)    :: gpcar(ndime,mnode,mgaus)
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: gpvol(mgaus)
  real(rp)    :: gphes(ntens*mnode,mgaus)

  if( IMASTER ) return

  do ielem = 1,nelem
     pnode = lnnod(ielem)
     pelty = ltype(ielem)
     pgaus = ngaus(pelty)
     plapl = 0
     do inode = 1,pnode
        ipoin = lnods(inode,ielem)
        do idime = 1,ndime
           elcod(idime,inode) = coord(idime,ipoin)
        end do
     end do
     call elmcar(&
          pnode,pgaus,plapl,elmar(pelty) % weigp,elmar(pelty) % shape,&
          elmar(pelty) % deriv,elmar(pelty) % heslo,elcod,gpvol,gpcar,&
          gphes,ielem)
     do igaus = 1,pgaus
        xproj = 0.0_rp
        gradu = 0.0_rp
        gpvel = 0.0_rp
        agrau = 0.0_rp
        gpcod = 0.0_rp
        do inode = 1,pnode
           ipoin = lnods(inode,ielem)
           do idime = 1,ndime
              xproj(idime) = xproj(idime) + elmar(pelty) % shape(inode,igaus) * vepro_nsi(idime,ipoin)
              gpvel(idime) = gpvel(idime) + elmar(pelty) % shape(inode,igaus) * veloc(idime,ipoin,1)
              gpcod(idime) = gpcod(idime) + elmar(pelty) % shape(inode,igaus) * coord(idime,ipoin)
           end do
           do idime = 1,ndime
              do jdime = 1,ndime
                 gradu(jdime,idime) = gradu(jdime,idime) + gpcar(jdime,inode,igaus) * veloc(idime,ipoin,1)
              end do
           end do
        end do
        do idime = 1,ndime
           do jdime = 1,ndime
              agrau(idime) = agrau(idime) + gpvel(jdime) * gradu(jdime,idime)
           end do
        end do
        if(ielem==1) then
           print*,xproj(1),agrau(1),4.0_rp*gpcod(1)
           print*,xproj(2),agrau(2),9.0_rp*gpcod(2)
           stop
        end if
        do idime = 1,ndime
         !  print*,agrau(idime)-xproj(idime)
        end do
        
     end do

  end do
stop
end subroutine caca
