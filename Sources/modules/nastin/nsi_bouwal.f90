subroutine nsi_bouwal(&
     itask,pevat,pnodb,ndofn,iboun,lboel,gbsha,bovel,bovfi,tract,gbvis,&
     gbden,baloc,velfr,wmatr,rough,kinen,veexl)

  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_bouwal
  ! NAME 
  !    nsi_bouwal
  ! DESCRIPTION
  !    This routine computes the surface traction for the NS equations at
  !    a given integration point of a boundary IBOUN received by argument
  !    due to the use of a turbulent wall law. The algorithm is:
  !    - Compute the tangential velocity u at the integration point x.
  !      In fact, u is not necessarily tangential at x. For example:
  !      -> u    -> u      
  !      o---x---o---x---o
  !                      |\ u
  !                      | 
  !    - Given the distance to the wall y, compute U*
  !    - Compute y+=yU*/nu
  !      if(y+>5) then
  !        t=-rho*U*^2*(u_tan-u_fix_tan)/|u_tan-u_fix_tan|
  !      else
  !        u+=y+ => U*^2=u*nu/y so that
  !        t=-mu*u/y
  !      end if
  ! USES
  !    vecnor
  !    frivel
  ! USED BY
  !    nsi_bouope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_master, only     :  kfl_custo,veloc
  use def_kermod, only     :  kfl_delta,usref,kfl_ustar, cmu_st
  use def_domain, only     :  ndime,ywalb
  use def_nastin, only     :  zensi,delta_nsi,grnor_nsi,kfl_waexl_nsi,dexlo_nsi
  implicit none
  integer(ip), intent(in)  :: itask,pnodb,pevat,ndofn,iboun
  integer(ip), intent(in)  :: lboel(pnodb)
  real(rp),    intent(in)  :: gbvis,gbden
  real(rp),    intent(in)  :: bovel(ndime,pnodb),gbsha(pnodb)
  real(rp),    intent(in)  :: bovfi(ndime,pnodb)
  real(rp),    intent(in)  :: baloc(ndime,ndime),rough, kinen
  real(rp),    intent(in)  :: veexl(ndime)   ! velocity at the exchange location - only needed for  kfl_waexl_nsi /= 0
  real(rp),    intent(out) :: velfr
  real(rp),    intent(out) :: tract(ndime),wmatr(pevat,pevat)
  integer(ip)              :: ldime,idime,inodb,idofn
  integer(ip)              :: jevab,ievab,jdime,jnodb,imeth 
  real(rp)                 :: vewal(3),fact1,fact2,velfi(3)
  real(rp)                 :: vikin,yplus                      ! nu, U*, y+, y
  real(rp)                 :: tveno                            ! |u_tan-u_fix_tan|
  real(rp)                 :: tvelo(3)                         !  u_tan
  real(rp)                 :: tvefi(3)                         !  u_fix_tan  - it comes from bvess_nsi put in global system
  real(rp)                 :: tvedi(3)                         !  u_tan - u_fix_tan
  real(rp)                 :: velsh(ndime,pnodb*ndime)
  real(rp)                 :: delta_aux
  !
  ! Base solution
  !
  real(rp)                ::  x(ndime)
  real(rp)                ::  kb,gradk(ndime),laplk
  real(rp)                ::  eb,grade(ndime),laple
  real(rp)                ::  mut,gradmut(ndime)
  real(rp)                ::  ub(ndime),gradu(ndime,ndime)
  real(rp)                ::  grad2u(ndime,ndime), velf2, umbra
  !
  !
  !
  real(rp)                ::  h1,h2,hs,u1,u2,u3
  
  if ( kfl_waexl_nsi == 0_ip ) then  ! normal behaviour
     if( kfl_delta == 1 ) then
        delta_aux = ywalb(iboun)                                  ! variable wall distance
     else
        delta_aux = delta_nsi                                     ! fixed wall distance
     end if
  else      ! using exchange location
     delta_aux = dexlo_nsi
  end if
  
  imeth = 2
  if ( kfl_waexl_nsi == 1_ip ) imeth = 1     ! when exchange locationis used it must be done explicitly

  umbra = 1.0e-6_rp

  if( ( delta_nsi > zensi ) .or. ( kfl_delta == 1 ) ) then

     if ( kfl_waexl_nsi == 0_ip ) then  ! normal behaviour

        do idime = 1,ndime                                        ! Velocity
           vewal(idime) = 0.0_rp
           velfi(idime) = 0.0_rp                                  ! velocity of the wall, due to moving domain
        end do
        do inodb = 1,pnodb
           do idime = 1,ndime            
              vewal(idime) = vewal(idime) &
                   + gbsha(inodb) * bovel(idime,inodb)
              velfi(idime) = velfi(idime) &
                   + gbsha(inodb) * bovfi(idime,inodb)
           end do
        end do

     else  ! using velocity from exchange location
        do idime = 1,ndime
           vewal(idime) = veexl(idime)                            ! Velocity - this comes as an input to this subroutine
           velfi(idime) = 0.0_rp                                  ! velocity of the wall, due to moving domain
        end do
        do inodb = 1,pnodb
           do idime = 1,ndime            
              velfi(idime) = velfi(idime) &
                   + gbsha(inodb) * bovfi(idime,inodb)
           end do
        end do

     end if

     if( imeth == 1 ) then

        !-----------------------------------------------------------------
        !
        ! Explicit treatment: Compute u_tan and |u_tan|
        !
        !-----------------------------------------------------------------
        !
        ! Tangent velocity (TVELO), tangent component of the prescribed velocity (TVEFI) and TVENO = |TVELO-TVEFI| 
        !
        do idime = 1,ndime     
           tvelo(idime) = vewal(idime)
           tvefi(idime) = velfi(idime)
           do ldime = 1,ndime
              tvelo(idime) = tvelo(idime)   &
                   - baloc(idime,ndime) &
                   * baloc(ldime,ndime) * vewal(ldime)
              tvefi(idime) = tvefi(idime)   &
                   - baloc(idime,ndime) &
                   * baloc(ldime,ndime) * velfi(ldime)
           end do
           tvedi(idime) = tvelo(idime) - tvefi(idime)
        end do
        !
        ! Compute U*: VELFR
        !
        call vecnor(tvedi,ndime,tveno,2_ip)                    ! |u_tan-u_fix_tan|
        vikin = gbvis/gbden                                    ! nu
        call frivel(delta_aux,rough,tveno,vikin,velfr)         ! U*
        !
        ! Compute prescribed traction
        !
        if( itask == 1 ) then 
           yplus = delta_aux*velfr/vikin
           if( yplus < 5.0_rp .and. kfl_ustar == 0 ) then
              !
              ! t = - mu/y * u
              !
              fact1 = gbden * vikin / delta_aux       
         
           else if( kfl_ustar == 1 ) then
              velfr = 0.41_rp / log( (delta_aux+rough) / rough )
              fact1 = gbden * tveno * velfr * velfr

           else if( kfl_ustar == 2 ) then
              velfr = 0.41_rp / log( (delta_aux+rough) / rough )
              fact1 = gbden * velfr * ( kinen**0.5 *cmu_st**0.25)           
           else
              !
              ! t = - rho*U*^2/|u_tan-u_fix_tan| * (u_tan-u_fix_tan)
              !
              fact1 = gbden * velfr * velfr / tveno            
           end if
           do idime = 1,ndime
              tract(idime) = tract(idime) - fact1 * tvedi(idime) 
           end do

           if( kfl_custo == -2 ) then
              !
              ! t = - rho*U*^2/|u| *u 
              !
              call vecnor(ub,ndime,tveno,2_ip)
              fact1 = gbden * usref * usref / tveno            
              do idime = 1,ndime
                 tract(idime) = tract(idime) + fact1 * ub(idime) 
              end do
           end if

        end if

     else

        !-----------------------------------------------------------------
        !
        ! Implicit treatment
        !
        !-----------------------------------------------------------------

        velsh = 0.0_rp
        do idime = 1,ndime                                     ! Tangent veloc.
           tvelo(idime) = vewal(idime)
           tvefi(idime) = velfi(idime)
           do inodb = 1,pnodb
              idofn = (inodb-1)*ndime+idime
              velsh(idime,idofn) = velsh(idime,idofn) &
                   + gbsha(inodb)
           end do
           do ldime = 1,ndime
              tvelo(idime) = tvelo(idime) &
                       - baloc(idime,ndime)   &
                       * baloc(ldime,ndime) * vewal(ldime)
              tvefi(idime) = tvefi(idime)   &
                       - baloc(idime,ndime) &
                       * baloc(ldime,ndime) * velfi(ldime)
              do inodb = 1,pnodb
                 idofn = (inodb-1)*ndime+ldime
                 velsh(idime,idofn) = velsh(idime,idofn) &
                      - baloc(idime,ndime)               &
                      * baloc(ldime,ndime)*gbsha(inodb)
              end do
           end do
           tvedi(idime) = tvelo(idime) - tvefi(idime)
        end do
        call vecnor(tvedi,ndime,tveno,2_ip)                    ! |u_tan-u_fix_tan| 
        !
        ! Compute U*
        !
        vikin = gbvis/gbden                                    ! nu
        call frivel(delta_aux,rough,tveno,vikin,velfr)         ! U*

        if( itask == 1 ) then
           !
           ! Compute prescribed traction
           !
           yplus = delta_aux * velfr / vikin
           if( yplus < 5.0_rp  .and. kfl_ustar == 0 ) then
              fact1 = gbden * vikin / delta_aux 
           else if( kfl_ustar == 1 ) then 
              velfr = 0.41_rp  / log((delta_aux+rough) / rough)
              fact1 = gbden * tveno * velfr * velfr           
           else if( kfl_ustar == 2 ) then ! ABL 2
              velfr = 0.41_rp / log((delta_aux+rough) / rough)
              velf2 = kinen**0.5 *cmu_st**0.25 ! frveloc in terms of kinetic turbulent energy 
!              if (velf2.gt.1.0e-1) then
                 fact1 = gbden * velfr * velf2
!              else
!                 fact1 = gbden * tveno * velfr * velfr
!              end if
           else
              fact1 = gbden * velfr * velfr / tveno          
           end if

           do idime = 1,ndime
              tract(idime) = tract(idime) + fact1 * tvefi(idime) 
           end do
           
           do inodb = 1,pnodb
              fact2 = fact1 * gbsha(inodb)
              ievab = (lboel(inodb)-1)*ndofn
              do idime = 1,ndime
                 ievab = ievab+1
                 do jnodb = 1,pnodb
                    jevab=(lboel(jnodb)-1)*ndofn
                    do jdime = 1,ndime
                       jevab = jevab+1
                       wmatr(ievab,jevab) = wmatr(ievab,jevab) &
                            + fact2 * velsh(idime,(jnodb-1)*ndime+jdime)
                    end do
                 end do
              end do
           end do

        end if

     end if

  end if
  !
  ! Second order correction: OJO QUITAR
  !
  h1 = 0.213767_rp
  h2 = 0.255452_rp
  hs = h2*(h1+h2)
 ! u1 = veloc(1,81,1)
 ! u2 = veloc(1,80,1)
 ! u3 = veloc(1,78,1)
 ! tract(1) = tract(1) + gbvis/hs* ( h2*u1 - (h1+h2)*u2 + h1*u3 ) * baloc(ndime,ndime)

end subroutine nsi_bouwal
