!-----------------------------------------------------------------------
!> @addtogroup NastinInput
!> @{
!> @file    nsi_reabcs.f90
!> @author  Guillaume Houzeaux
!> @brief   Read boundary conditions 
!> @details Read boundary conditions, initial conditions and parameters
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_reabcs()
  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use def_nastin 
  use mod_memchk
  use mod_opebcs
  use mod_ker_space_time_function
  implicit none
  integer(ip)  :: dummi
  character(5) :: wfname
  !
  ! Allocate memory
  !
  if( kfl_icodn > 0 ) then
     call opnbcs(1_ip,2_ip,dummi,dummi,tncod_nsi)             ! Memory for structure
     call opnbcs(2_ip,1_ip,ndime, 0_ip,tncod_nsi)             ! Memory for velocity
     call opnbcs(2_ip,2_ip, 1_ip, 1_ip,tncod_nsi)             ! Memory for pressure
  end if
  if( kfl_icodb > 0 ) then
     call opbbcs(0_ip,1_ip,1_ip,tbcod_nsi)      
  end if
  if( kfl_geome > 0 ) then
     call opnbcs(0_ip,1_ip,ndime, 0_ip,tgcod_nsi)
  end if
 
  if( INOTSLAVE ) then
     !
     ! Initialization global variables
     !
     kfl_confi_nsi = -1                                   ! Flow is not confined
     kfl_local_nsi = 0                                    ! No local system of reference
     kfl_conbc_nsi = 1                                    ! Constant boundary conditions
     kfl_syntu_nsi = 0                                    ! Synthetic eddy method (SEM) (OFF: == 0; ON:==1)
     kfl_initi_nsi = 0                                    ! Initial condition
     kfl_inico_nsi = 0                                    ! Coarse grid system not solved
     kfl_inipr_nsi = 0                                    ! Initial pressure
     kfl_nopen_nsi = 0                                    ! No penetration condition in strong form
     kfl_cadan_nsi = 0                                    ! No coupling with ADAN for the pressure condition
     neddy_nsi     = 0                                    ! Number of eddies in the inlet box for SEM
     itebc_nsi     = 1                                    ! Initial step boundary condition
     nodpr_nsi     = 0                                    ! Node where to impose pressure
     exfpr_nsi     = 0                                    ! Extension of o fixpr one layer of elements
     kfl_imppr_nsi = 0                                    ! Imposses pressure in nodes w/ fixpr>0
     delta_nsi     = 0.0_rp                               ! Distance to the wall
     relbc_nsi     = 1.0_rp                               ! Boundary condition relaxation
     valpr_nsi     = 0.0_rp                               ! Pressure value
     velin_nsi     = 0.0_rp                               ! Initial velocity
     poise_nsi     = 0.0_rp                               ! Parameters for Poiseuille distribution
     !
     ! Coupling with ALEFOR: bc must be non-consatnt
     !
     if( kfl_coupl(ID_NASTIN,ID_ALEFOR) /= 0 ) then
        kfl_conbc_nsi = 0
     end if
     !
     ! Reach the nodal-wise section
     !
     call ecoute('nsi_reabcs')
     do while(words(1)/='BOUND')
        call ecoute('nsi_reabcs')
     end do
     !--><group>
     !-->       <groupName>BOUNDARY_CONDITIONS</groupName>
     !-->       <groupType>bouco</groupType>
     !-->       <groupTypeDefinition><![CDATA[
     !-->         PARAMETERS
     !-->           INITIAL_CONDITION: OFF | COARSE | SPACE_TIME_FUNCTON=char1 | CONSTANT, VALUES=real1,real2,real3 [COARSE]  $ Type of initial condition
     !-->           FIX_PRESSURE:      OFF | AUTOMATIC | ON[, ON_NODE= int1][, VALUE= real1]       $ Fix pressure for confined flow
     !-->           VARIATION:         CONSTANT | NON_CONSTANT                                     $ Boundary conditions constant/variable in time
     !-->         END_PARAMETERS
     !-->         CODES, NODES, PRESSURE
     !-->         ...
     !-->         int1 1                                                                       $  int1=code
     !-->         ...
     !-->        END_CODES
     !-->        CODES, NODES
     !-->          ...
     !-->          int1 int2 real1 real2 real3 AXES:LOCAL | GLOBAL, SPACE_TIME_FUNCTION:char1, TIME_FUNCTION:char2, VALUE_FUNCTION=int3 $  int1=code, int2=velocity code,  char1=space/time function, char2=time function
     !-->          -int1 int2 ...                                                                                                        $ -int1=code, int2=value function, int3=time function, int4=basis 
     !-->          ...
     !-->       END_CODES
     !-->       CODES, BOUNDARIES
     !-->         ...
     !-->         int1 int2 real1                                                              $  int1=code, int2=type of condition, real1=value
     !-->         ...
     !-->       END_CODES]]></groupTypeDefinition>
     !-->       <groupHelp><![CDATA[BOUNDARY_CONDITIONS:
     !-->        In this section, the boundary conditions are defined.
     !-->        Previously, in the <tt>sample.dom.dat</tt> file, codes have been applied to
     !-->        boundaries and/or nodes, or extrapolated from boundaries to nodes.
     !-->        Nastin must translate these conditions in terms of its own degrees of freedom: velocity 
     !-->        and pressure.]]></groupHelp>
     !-->       <parameters><inputLineHelp><![CDATA[PARAMETERS:
     !-->         In this field, some parameters are defined for the Navier-Stokes equations
     !-->         concerning the initial and boundary conditions.]]></inputLineHelp></parameters>
     !-->       <initialCondition><inputLineHelp><![CDATA[<b>INITIAL_CONDITION</b>: 
     !-->         Special initial conditions of the Navier-Stokes equations. The initial condition 
     !-->         defined here is automatically overwritten by Dirichlet conditions.
     !-->         <ul>
     !-->          <li>
     !-->           OFF: initial conditions are prescribed in the field "CODES, NODES" with velocity fixity 0.
     !-->          </li>
     !-->          <li>
     !-->            SPACE_TIME_FUNCTION: Prescribe the char function on all nodes.
     !-->          </li>
     !-->          <li>
     !-->            CONSTANT, VALUES = real1, real2, real3: initial velocity is (real1,real2,real3)
     !-->            on nodes where no Dirichlet boundary condition has been prescribed. This option
     !-->            overwrites the initial condition prescribed in the field "CODES, NODES" with velocity fixity 0.
     !-->            If option COARSE is present, the procedure is the following: 
     !-->            an initial velocity (real1,real2,real3) is considered; 
     !-->            then the monolithic stationary Navier-Stokes system is solved on the coarse
     !-->            mesh, using the initial velocity as advection field; finally the solution is prolongated on the original mesh.
     !-->            The coarse problem is solved agglomerating the fine mesh matrix and RHS on the 
     !-->            groups using defined in the <tt>sample.dom.dat</tt> file.
     !-->          </li>
     !-->          <li>
     !-->            COARSE: the procedure is the following:
     !-->            the monolithic stationary Navier-Stokes system is solved on the coarse
     !-->            mesh; finally the solution is prolongated on the original mesh.
     !-->            The coarse problem is solved agglomerating the fine mesh matrix and RHS on the 
     !-->            groups using defined in the <tt>sample.dom.dat</tt> file.
     !-->            This option uses the initial condition prescribed in the file CODES, NODES with code 0.
     !-->          </li>
     !-->        </ul>]]></inputLineHelp></initialCondition>
     !-->       <fixPressure><inputLineHelp><![CDATA[FIX_PRESSURE:
     !-->         By default the pressure is not fixed anywhere. However, if the flow is confined
     !-->         that is if the velocity is prescribed with a Dirichlet boundary condition on 
     !-->         all the boundary nodes, the pressure should be prescribed on one node int1 to a value real1.
     !-->         This is necessary because the pressure is defined up to a constant,
     !-->         The node on which the pressure is prescribed should be located on the boundary.
     !-->         Example:  FIX_PRESSURE: ON, ON_NODE= 1251, VALUE= 0.0. The pressure is prescribed
     !-->         to zero on node 1251. Note that if the mesh multiplication is used, the node number
     !-->         refers to the original mesh. Option AUTOMATIC enables to let Alya choose the node
     !-->         where to prescribe the pressure to a zero value.]]></inputLineHelp></fixPressure>
     !-->       <variation><inputLineHelp><![CDATA[VARIATION:
     !-->             If a time function is used, NON_CONSTANT option should be selected.
     !-->              Alya does not identify non-constant boundary conditions automatically.]]></inputLineHelp></variation>
     !-->       <codesNodesPressure><inputLineHelp><![CDATA[<b>CODES, NODES, PRESSURE</b>:
     !-->                  Interpret the code on nodes to impose the pressure in the Schur complement matrix.
     !-->                  Nastin imposes automatically the pressure in the Schur complement preconditioner on outflows. However, one
     !-->                  can add some nodes to this aoutmatic list by prescribing the node code int2. ]]></inputLineHelp></codesNodesPressure>
     !-->       <codesNodes><inputLineHelp><![CDATA[<b>CODES, NODES</b>:
     !-->                   Interpret the code on nodes to impose velocity degrees of freedom.
     !-->                   <ul>
     !-->                     <li> 
     !-->                        int1 is the code node, int=1,2... or 99.
     !-->                        If the node has multiple codes (e.g. if the code was extrapolated from boundary codes), the
     !-->                        Syntax is int1 = 1 & 2 & 4. It means that nodes with the three codes 1,2 and 4 are considered.
     !-->                     </li> 
     !-->                     <li> 
     !-->                       int2 has the form f1f2f3: 11, 10, 01 in 2D and 101, 111, etc in 3D. 0 means free or initial condition, 1 means prescribed.
     !-->                       Values f1, f2 and f3 are fixity codes of the velocity degrees of freedom.
     !-->                       f1 refers to the first velocity components, f2 to the second and f3 to the third.
     !-->                     </li> 
     !-->                     <li> 
     !-->                       real1, real2 and real3 are the corresponding values for each degree of freedom (first to third velocity component).
     !-->                     </li> 
     !-->                     <li> 
     !-->                       char1 is the time function to applied to the nodes with code int1. If not time function is given, the conditions will be constant in time.
     !-->                       The function should be defined in sample.ker.dat file.
     !-->                     </li> 
     !-->                     <li> 
     !-->                       char2 is the space/time function f(x,t) to applied to the nodes with code int1. The value of the nodal condition will be
     !-->                       f(x,t)*(real1,real2,real3) is the function is a scalar or (f1(x,t)*real1,f2(x,t)*real2,f3(x,t)*real3) if the funciton
     !-->                       is vectorial.
     !-->                     </li> 
     !-->                     <li> 
     !-->                       AXES is the basis in which the velocity is prescibed. GLOBAL means that the three components of the velocity
     !-->                       are the three Cartesian components. LOCAL mean the local basis computed by Alya. The first component is the
     !-->                       normal component and the other two the tangential ones.
     !-->                     </li> 
     !-->                     <li> 
     !-->                       If the option VALUE_FUNCTION is present, then the nodal condition will be taken from the corresponding field defined
     !-->                       in the BOUNDARY_CONDITIONS field of the file sample.dom.dat   
     !-->                     </li> 
     !-->                    </ul>
     !-->                    Examples:
     !-->                    <ul>
     !-->                      <li> 
     !-->                         3 100 0.0 0.0 0.0 0 AXES:LOCAL => Slip condition: zero normal velocity is prescribed and tangent velocity is free on nodes with code 3.
     !-->                         To impose a wall law, a condition on boundaries of type 3 should be prescribed.
     !-->                      </li> 
     !-->                      <li> 
     !-->                        5 111 VALUE=2 => Prescribe value function number 2 as a Dirichlet boundary condition on all nodes.
     !-->                      </li> 
     !-->                      <li> 
     !-->                        4 & 6 111 0.0 0.0 0.0 => No-slip conditions on nodes with codes 4 and 6.
     !-->                      </li> 
     !-->                      <li> 
     !-->                        7 000 1.0 2.0 3.0 => Initial velocity is set to (1,2,3) on nodes with code 7.
     !-->                      </li> 
     !--> </ul>]]></inputLineHelp></codesNodes>
     !--> <codesBoundaries><inputLineHelp><![CDATA[CODES, BOUNDARIES:
     !-->                  Impose a natural boundary condition of type int2 on boundaries with code int1.
     !-->                  The different boundary conditions available are:
     !-->                  <ul>
     !-->                   <li> 
     !-->                      int2 = 3: law of the wall. It requires the distance to the wall as given in Kermod. real1 not needed.
     !-->                   </li> 
     !-->                   <li> 
     !-->                     int2 = 2: impose the traction to value real1. If the flow is uniform or the Reynolds number very high, this is 
     !-->                     equivalent, this is equivalent to imposing the pressure.
     !-->                   </li> 
     !-->                  </ul>
     !-->                  Examples:
     !-->                  <ul>
     !-->                   <li> 
     !-->                     5 3: Impose the law of the wall to boundaries with code 5.
     !-->                   </li> 
     !-->                   <li> 
     !-->                     6 2 5.0: Impose the pressure to 5.0 on boundaries with code 6.
     !-->                   </li> 
     !-->                   </ul>]]></inputLineHelp></codesBoundaries>
     !--></group>
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ boundary conditions
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> BOUNDARY_CONDITIONS
     ! ADOC[d]> BOUNDARY_CONDITIONS:
     ! ADOC[d]> In this section, the boundary conditions are defined.
     ! ADOC[d]> Previously, in the <tt>sample.dom.dat</tt> file, codes have been applied to
     ! ADOC[d]> boundaries and/or nodes, or extrapolated from boundaries to nodes.
     ! ADOC[d]> Nastin must translate these conditions in terms of its own degrees of freedom: velocity 
     ! ADOC[d]> and pressure.
     !
     call ecoute('nsi_reabcs')
     do while(words(1)/='ENDBO')

        if (words(1) == 'PARAM' ) then

           !-------------------------------------------------------------
           !
           ! Parameters
           !
           !-------------------------------------------------------------

           call ecoute('nsi_reabcs')
           do while(words(1)/='ENDPA') 
              !
              ! ADOC[1]> PARAMETERS
              ! ADOC[d]> PARAMETERS:
              ! ADOC[d]> In this field, some parameters are defined for the Navier-Stokes equations
              ! ADOC[d]> concerning the initial and boundary conditions.
              !
              !if( words(1) == 'HYDRO' ) then
              !   !
              !   ! Initial hydrostatic pressure: KFL_HYDRO_NSI, HYDRO_NSI
              !   !
              !   if( words(2) == 'ON   ') kfl_hydro_nsi = -1
              !   if(exists('Z    ')) then
              !      kfl_hydro_nsi = 1
              !      hydro_nsi     = getrea('Z    ',0.0_rp,'#Hydrostatic z-plane')
              !   else if(exists('Y    ')) then
              !      kfl_hydro_nsi = 1
              !      hydro_nsi     = getrea('Y    ',0.0_rp,'#Hydrostatic y-plane')
              !   end if

              if( words(1) == 'PRESS' ) then
                 !
                 ! Initial hydrostatic pressure: KFL_HYDRO_NSI, HYDRO_NSI
                 !
                 if( words(2) == 'LAPLA' ) then 
                    kfl_inipr_nsi = 1
                 else if( words(2) == 'HYDRO' ) then
                    kfl_inipr_nsi = 2
                 end if

              else if(words(1) == 'NOPEN' ) then
                 !
                 ! No penetration condition: weak or strong form
                 !
                 if( words(2) == 'WEAK ' .or. words(2) == 'NATUR' .or. words(2) == 'WEAKF' ) then
                    kfl_nopen_nsi = 1
                 else if( words(2) == 'STRON' .or. words(2) == 'DIRIC' ) then
                    kfl_nopen_nsi = 0
                 end if

              else if( words(1) == 'INITI' ) then                 
                 !
                 ! Initial solution KFL_INITI: Constant, Stokes, etc.
                 !
                 ! ADOC[2]> INITIAL_CONDITION: OFF | COARSE | SPACE_TIME_FUNCTON=char1 | CONSTANT, VALUES=real1,real2,real3 [COARSE]  $ Type of initial condition
                 ! ADOC[d]> <b>INITIAL_CONDITION</b>: 
                 ! ADOC[d]> Special initial conditions of the Navier-Stokes equations. The initial condition 
                 ! ADOC[d]> defined here is automatically overwritten by Dirichlet conditions.
                 ! ADOC[d]> <ul>
                 ! ADOC[d]> <li>
                 ! ADOC[d]>       OFF: initial conditions are prescribed in the field "CODES, NODES" with velocity fixity 0.
                 ! ADOC[d]> </li>
                 ! ADOC[d]> <li>
                 ! ADOC[d]>       SPACE_TIME_FUNCTION: Prescribe the char function on all nodes.
                 ! ADOC[d]> </li>
                 ! ADOC[d]> <li>
                 ! ADOC[d]>       CONSTANT, VALUES = real1, real2, real3: initial velocity is (real1,real2,real3)
                 ! ADOC[d]>       on nodes where no Dirichlet boundary condition has been prescribed. This option
                 ! ADOC[d]>       overwrites the initial condition prescribed in the field "CODES, NODES" with velocity fixity 0.
                 ! ADOC[d]>       If option COARSE is present, the procedure is the following: 
                 ! ADOC[d]>       an initial velocity (real1,real2,real3) is considered; 
                 ! ADOC[d]>       then the monolithic stationary Navier-Stokes system is solved on the coarse
                 ! ADOC[d]>       mesh, using the initial velocity as advection field; finally the solution is prolongated on the original mesh.
                 ! ADOC[d]>       The coarse problem is solved agglomerating the fine mesh matrix and RHS on the 
                 ! ADOC[d]>       groups using defined in the <tt>sample.dom.dat</tt> file.
                 ! ADOC[d]> </li>
                 ! ADOC[d]> <li>
                 ! ADOC[d]>       COARSE: the procedure is the following:
                 ! ADOC[d]>       the monolithic stationary Navier-Stokes system is solved on the coarse
                 ! ADOC[d]>       mesh; finally the solution is prolongated on the original mesh.
                 ! ADOC[d]>       The coarse problem is solved agglomerating the fine mesh matrix and RHS on the 
                 ! ADOC[d]>       groups using defined in the <tt>sample.dom.dat</tt> file.
                 ! ADOC[d]>       This option uses the initial condition prescribed in the file CODES, NODES with code 0.
                 ! ADOC[d]> </li>
                 ! ADOC[d]> </ul>
                 !
                 if( exists('NONIN') .or. exists('CONST') ) then                           ! 1: Constant
                    if (kfl_initi_nsi /= 6 .and. kfl_initi_nsi /= 7) kfl_initi_nsi = 1
                    velin_nsi(1:3) = param(3:5)    
                 else if( exists('INERT') ) then                                           ! 2: Constant in inertial f.o.r     
                    velin_nsi(1:3) = param(3:5)
                    kfl_initi_nsi = 2
                 else if( exists('STOKE') ) then                                           ! 3: Stokes
                    kfl_initi_nsi = 3
                 else if( exists('POTEN') ) then                                           ! 4: Potential flow
                    kfl_initi_nsi = 4
                 else if( exists('LAPLA') ) then                                           ! 5: Laplacian
                    kfl_initi_nsi = 5
                    solve(3)%kfl_solve = 1                                                 ! Output flag
                 else if( exists('POISE') ) then                                           ! 6: Poiseuille distribution 
                    if (exists('BOUND')) then
                       kfl_initi_nsi = 7
                       poise_nsi(1:6) = param(4:9)                                         ! Parameters: Flow axis, radius, max. velocity
                    else
                       kfl_initi_nsi = 6
                       poise_nsi(1:6) = param(3:8)               
                    endif
                 else if( exists('SPACE') ) then                                           ! >100: Space time function
                    wfname = getcha('SPACE','NULL ','#Space/time Function name')
                    kfl_initi_nsi = 100 + space_time_function_number(wfname)
                 else if( exists('VALUE') ) then                                           ! <0: value function
                    kfl_initi_nsi = &
                         -getint('VALUE',1_ip,'#Initial condition is from value function')
                 end if
                 if( exists('COARS') ) kfl_inico_nsi = 1                                   ! Coarse grid system

              else if( words(1) == 'FIXPR' ) then
                 !
                 ! Pressure fixity
                 !
                 ! ADOC[2]> FIX_PRESSURE:      OFF | AUTOMATIC | ON[, ON_NODE= int1][, VALUE= real1]       $ Fix pressure for confined flow
                 ! ADOC[d]> FIX_PRESSURE:
                 ! ADOC[d]> By default the pressure is not fixed anywhere. However, if the flow is confined
                 ! ADOC[d]> that is if the velocity is prescribed with a Dirichlet boundary condition on 
                 ! ADOC[d]> all the boundary nodes, the pressure should be prescribed on one node int1 to a value real1.
                 ! ADOC[d]> This is necessary because the pressure is defined up to a constant,
                 ! ADOC[d]> The node on which the pressure is prescribed should be located on the boundary.
                 ! ADOC[d]> Example:  FIX_PRESSURE: ON, ON_NODE= 1251, VALUE= 0.0. The pressure is prescribed
                 ! ADOC[d]> to zero on node 1251. Note that if the mesh multiplication is used, the node number
                 ! ADOC[d]> refers to the original mesh. Option AUTOMATIC enables to let Alya choose the node
                 ! ADOC[d]> where to prescribe the pressure to a zero value.
                 !
                 if( words(2) == 'AUTOM' ) then
                    kfl_confi_nsi = 0
                 else if( words(2) == 'NO   '.or. words(2) == 'OFF  ' ) then
                    kfl_confi_nsi = -1
                 else if( words(2) == 'YES  '.or. words(2) == 'ON   ' ) then
                    kfl_confi_nsi =  1
                    if( exists('ONNOD') ) then
                       nodpr_nsi = getint('ONNOD',1_ip,'#Node where to impose pressure')
                    else if( exists('NODE ') ) then
                       nodpr_nsi = getint('NODE ',1_ip,'#Node where to impose pressure')
                    end if
                    valpr_nsi = getrea('VALUE',0.0_rp,'#Pressure value')
                 end if

              else if( words(1) == 'VARIA' ) then
                 !
                 ! ADOC[2]> VARIATION:         CONSTANT | NON_CONSTANT                                     $ Boundary conditions constant/variable in time
                 ! ADOC[d]> VARIATION:
                 ! ADOC[d]> If a time function is used, NON_CONSTANT option should be selected.
                 ! ADOC[d]> Alya does not identify non-constant boundary conditions automatically.
                 !
                 ! if( words(2) == 'NONCO' .and. kfl_conbc_nsi /= 0 ) then   
                 if( words(2) == 'NONCO' ) then
                    kfl_conbc_nsi = 0 
                 else
                    kfl_conbc_nsi = 1
                 end if

              else if( words(1) == 'SYNTH' ) then
                 !
                 ! ADOC[2]> SYNTHETIC:             $ Syhntetic eddy method (SEM) 
                 ! ADOC[d]> SYNTHETIC:
                 ! ADOC[d]> Syhntetic eddy method (SEM) based on the work by Jarrin et al. (2006).
                 ! ADOC[d]>    A Synthetic-Eddy Method for Generating Inflow Conditions 
                 ! ADOC[d]>    for LES, Jarrin, N., Benhamadouche, S., Laurence, D. and Prosser, R 
                 ! ADOC[d]>    International Journal of Heat and Fluid Flow, Vol. 27, pp. 585-593, (2006). 
                 !
                 if( exists('NUMBE') ) &
                       neddy_nsi = getint('NUMBE',0_ip,'#Node where to impose pressure')
                 kfl_syntu_nsi = 1

              end if

              call ecoute('nsi_reabcs')
              !
              ! ADOC[1]> END_PARAMETERS
              !
           end do

        else if( words(1) == 'COUAD') then           
           !
           ! Pressure prescriptions will respond to coupling with ADAN
           !           
           kfl_cadan_nsi=1
           kfl_aiobo_nsi= int(param(1)) !Boundary connected with ADAN

        else if( words(1) == 'CODES' .and. exists('NODES') ) then

           !-------------------------------------------------------------
           !
           ! User-defined codes on nodes
           !
           !-------------------------------------------------------------

           if( exists('PRESS') ) then
              !
              ! ADOC[1]> CODES, NODES, PRESSURE
              ! ADOC[2]>   ...
              ! ADOC[2]>   int1 1                                                                       $  int1=code
              ! ADOC[2]>   ...
              ! ADOC[1]> END_CODES
              ! ADOC[d]> <b>CODES, NODES, PRESSURE</b>:
              ! ADOC[d]> Interpret the code on nodes to impose the pressure in the Schur complement matrix.
              ! ADOC[d]> Nastin imposes automatically the pressure in the Schur complement preconditioner on outflows. However, one
              ! ADOC[d]> can add some nodes to this aoutmatic list by prescribing the node code int2. 
              ! 
              tncod => tncod_nsi(2:)
              !tncod => momod(ID_NASTIN) % tncod(2:)
              call reacod(1_ip)

           else if( exists('GEOME') ) then
              !
              ! Velocity: geometrical node code
              !              
              tgcod => tgcod_nsi(1:)
              !tgcod => momod(ID_NASTIN) % tgcod(1:)
              call reacod(4_ip)

           else
              !
              ! ADOC[1]> CODES, NODES
              ! ADOC[2]>   ...
              ! ADOC[2]>   int1 int2 real1 real2 real3 AXES:LOCAL | GLOBAL, SPACE_TIME_FUNCTION:char1, TIME_FUNCTION:char2, VALUE_FUNCTION=int3 $  int1=code, int2=velocity code,  char1=space/time function, char2=time function
              ! ADOC[2]>  -int1 int2 ...                                                                                                        $ -int1=code, int2=value function, int3=time function, int4=basis 
              ! ADOC[2]>   ...
              ! ADOC[1]> END_CODES
              ! ADOC[d]> <b>CODES, NODES</b>:
              ! ADOC[d]> Interpret the code on nodes to impose velocity degrees of freedom.
              ! ADOC[d]> <ul>
              ! ADOC[d]> <li> 
              ! ADOC[d]>      int1 is the code node, int=1,2... or 99.
              ! ADOC[d]>      If the node has multiple codes (e.g. if the code was extrapolated from boundary codes), the
              ! ADOC[d]>      Syntax is int1 = 1 & 2 & 4. It means that nodes with the three codes 1,2 and 4 are considered.
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>      int2 has the form f1f2f3: 11, 10, 01 in 2D and 101, 111, etc in 3D. 0 means free or initial condition, 1 means prescribed.
              ! ADOC[d]>      Values f1, f2 and f3 are fixity codes of the velocity degrees of freedom.
              ! ADOC[d]>      f1 refers to the first velocity components, f2 to the second and f3 to the third.
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>      real1, real2 and real3 are the corresponding values for each degree of freedom (first to third velocity component).
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>      char1 is the time function to applied to the nodes with code int1. If not time function is given, the conditions will be constant in time.
              ! ADOC[d]>      The function should be defined in sample.ker.dat file.
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>      char2 is the space/time function f(x,t) to applied to the nodes with code int1. The value of the nodal condition will be
              ! ADOC[d]>      f(x,t)*(real1,real2,real3) is the function is a scalar or (f1(x,t)*real1,f2(x,t)*real2,f3(x,t)*real3) if the funciton
              ! ADOC[d]>      is vectorial.
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>      AXES is the basis in which the velocity is prescibed. GLOBAL means that the three components of the velocity
              ! ADOC[d]>      are the three Cartesian components. LOCAL mean the local basis computed by Alya. The first component is the
              ! ADOC[d]>      normal component and the other two the tangential ones.
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>      If the option VALUE_FUNCTION is present, then the nodal condition will be taken from the corresponding field defined
              ! ADOC[d]>      in the BOUNDARY_CONDITIONS field of the file sample.dom.dat            
              ! ADOC[d]> </li> 
              ! ADOC[d]> </ul>
              ! ADOC[d]> Examples:
              ! ADOC[d]> <ul>
              ! ADOC[d]> <li> 
              ! ADOC[d]>  3 100 0.0 0.0 0.0 0 AXES:LOCAL => Slip condition: zero normal velocity is prescribed and tangent velocity is free on nodes with code 3.
              ! ADOC[d]>  To impose a wall law, a condition on boundaries of type 3 should be prescribed.
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>  5 111 VALUE=2 => Prescribe value function number 2 as a Dirichlet boundary condition on all nodes.
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>  4 & 6 111 0.0 0.0 0.0 => No-slip conditions on nodes with codes 4 and 6.
              ! ADOC[d]> </li> 
              ! ADOC[d]> <li> 
              ! ADOC[d]>  7 000 1.0 2.0 3.0 => Initial velocity is set to (1,2,3) on nodes with code 7.
              ! ADOC[d]> </li> 
              ! ADOC[d]> </ul>
              !
              !tncod => momod(ID_NASTIN) % tncod(1:)
              tncod => tncod_nsi(1:)
              call reacod(1_ip)

           end if

        else if( words(1) == 'CODES' .and. exists('BOUND') ) then

           !-------------------------------------------------------------
           !
           ! User-defined codes on boundaries
           !          
           !-------------------------------------------------------------
           !
           ! ADOC[1]> CODES, BOUNDARIES
           ! ADOC[2]>   ...
           ! ADOC[2]>   int1 int2 real1                                                              $  int1=code, int2=type of condition, real1=value
           ! ADOC[2]>   ...
           ! ADOC[1]> END_CODES
           ! ADOC[d]> CODES, BOUNDARIES:
           ! ADOC[d]> Impose a natural boundary condition of type int2 on boundaries with code int1.
           ! ADOC[d]> The different boundary conditions available are:
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> 
           ! ADOC[d]>      int2 = 3: law of the wall. It requires the distance to the wall as given in Kermod. real1 not needed.
           ! ADOC[d]> </li> 
           ! ADOC[d]> <li> 
           ! ADOC[d]>      int2 = 2: impose the traction to value real1. If the flow is uniform or the Reynolds number very high, this is 
           ! ADOC[d]>      equivalent, this is equivalent to imposing the pressure.
           ! ADOC[d]> </li> 
           ! ADOC[d]> </ul>
           ! ADOC[d]> Examples:
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> 
           ! ADOC[d]>  5 3: Impose the law of the wall to boundaries with code 5.
           ! ADOC[d]> </li> 
           ! ADOC[d]> <li> 
           ! ADOC[d]>  6 2 5.0: Impose the pressure to 5.0 on boundaries with code 6.
           ! ADOC[d]> </li> 
           ! ADOC[d]> </ul>
           !
           tbcod => tbcod_nsi(1:)
           !tbcod => momod(ID_NASTIN) % tncod(1:)
           call reacod(2_ip)

        else if( words(1) == 'EXTEN' ) then
           exfpr_nsi = 1

        else if( words(1) == 'IMPOS' ) then
           kfl_imppr_nsi = 1
        end if

        call ecoute('nsi_reabcs')

     end do
     !
     ! ADOC[0]> END_BOUNDARY_CONDITIONS
     !

  end if
  !
  ! Formats
  !
101 format(5x,'WARNING: ',a,$)
103 format(1x,a,$)
104 format(1x,a)

end subroutine nsi_reabcs

subroutine nsi_extnor(ibopo,exwor)
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_extnor
  ! NAME 
  !    nsi_extnor
  ! DESCRIPTION
  !    Computes the normal at a boundary node
  ! USES
  ! USED BY
  !    nsi_reabcs
  !***
  !------------------------------------------------------------------------  
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  ndime,skcos,exnor
  use def_nastin, only     :  kfl_local_nsi,skcos_nsi,kfl_fixrs_nsi
  implicit none
  integer(ip), intent(in)  :: ibopo
  real(rp),    intent(out) :: exwor(ndime,ndime)
  integer(ip)              :: iroty,idime,jdime

  if( kfl_local_nsi == 1 ) then
     iroty = kfl_fixrs_nsi(ibopo)
     if(iroty==-1) then                                    ! Tangent system
        do idime = 1,ndime
           do jdime = 1,ndime
              exwor(jdime,idime) = exnor(jdime,idime,ibopo)
           end do
        end do
     else if(iroty>=1) then                                ! Given system
        do idime = 1,ndime
           do jdime = 1,ndime
              exwor(jdime,idime) = skcos(jdime,idime,iroty)
           end do
        end do
     else if(iroty==-2) then                               ! Given system
        do idime = 1,ndime
           do jdime = 1,ndime
              exwor(jdime,idime) = skcos_nsi(jdime,idime,ibopo)
           end do
        end do
     else if(iroty==-3) then                               ! Geometrical normals
        do idime = 1,ndime
           do jdime = 1,ndime
              exwor(jdime,idime) = skcos(jdime,idime,ibopo)
           end do
        end do
     end if
  else
     do idime = 1,ndime
        do jdime = 1,ndime
           exwor(jdime,idime) = exnor(jdime,idime,ibopo)
        end do
     end do
  end if

end subroutine nsi_extnor
