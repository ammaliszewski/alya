!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_elmexf.f90
!> @author  Guillaume Houzeaux
!> @brief   Add a force term to the momentum equations
!> @details 
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_elmexf(                     &
     ndime,pgaus,lforc_material_nsi,gpden, &
     xforc_material_nsi,gprhs, gpvel)

  use def_kintyp, only     :  ip,rp

  implicit none
  integer(ip), intent(in)  :: ndime
  integer(ip), intent(in)  :: pgaus
  integer(ip), intent(in)  :: lforc_material_nsi
  real(rp),    intent(in)  :: gpden(pgaus)
  real(rp),    intent(in)  :: xforc_material_nsi(*), gpvel(ndime, pgaus)
  real(rp),    intent(out) :: gprhs(ndime,pgaus)
  integer(ip)              :: idime,igaus
  real(rp)                 :: u_ref(3),A_disk,n_disk(3),Ct,rho,uref2,V_disk
  real(rp)                 :: F_total,F_unit_vol, d_disk

  if( lforc_material_nsi == 1 ) then
     !
     ! Wake model
     !
     n_disk(1) = xforc_material_nsi( 3)
     n_disk(2) = xforc_material_nsi( 4)
     n_disk(3) = xforc_material_nsi( 5)
     V_disk    = xforc_material_nsi(15)
     d_disk    = xforc_material_nsi( 1)

     ! traction coefficient
     Ct        =  xforc_material_nsi(16)
     !  modification of Ct to use local velocity
     !  assuming theoretical relationship Ct=4a*(1-a)
     Ct         = 4.0_rp*Ct/(2.0_rp+2.0_rp*sqrt(1.0_rp-Ct)-Ct)

     do igaus = 1,pgaus
       
        uref2 = gpvel(1, igaus) * n_disk(1) &
             + gpvel(2, igaus) * n_disk(2) & 
             + gpvel(3, igaus) * n_disk(3) 
        uref2 = uref2*uref2
        
        ! End modification

        rho        = gpden(igaus)
!      UREF2=8*8
!       F_total    = 0.5_rp * rho * Ct * uref2*A_disk
!       F_unit_vol = F_total/ V_disk
        F_unit_vol = 0.5_rp * rho * Ct * uref2/d_disk
        do idime = 1,ndime
           gprhs(idime,igaus) = gprhs(idime,igaus) + n_disk(idime) * F_unit_vol
        end do
     end do
!     print*, 'force_tot, for_vol, veloc', F_total, F_unit_vol,  sqrt(uref2)
  else if ( lforc_material_nsi == 2) then
     do igaus = 1,pgaus
        do idime=1, ndime
           gprhs(idime, igaus) =  gprhs(idime, igaus) +  xforc_material_nsi(idime)
        end do
     end do
  end if

end subroutine nsi_elmexf

