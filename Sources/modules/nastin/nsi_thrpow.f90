!-------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_ctcalc.f90
!> @author  Matias
!> @brief   Interpolates thrust and power coeffs for actuator disk in terms of veinf.
!> @details 
!> @} 
!-------------------------------------
subroutine nsi_thrpow(veinf, thrco, powco, pmate)


  use def_kintyp, only     : ip,rp
  use def_nastin, only     : velta_nsi, & ! velocity table
       thrta_nsi, &    ! thrust table
       powta_nsi, &    ! power table
       ntabl_nsi       ! # data table

  implicit none
  real(rp), intent(in)   ::  veinf
  integer(ip), intent(in)::  pmate
  real(rp), intent(out)  ::  powco, thrco
  integer(ip)            ::  iz, jz, kz
  real(rp)               ::  facto

  !
  !  for interpolation  velocity values are supposed to be given in increasing order
  !
  if (veinf.lt.velta_nsi(1, pmate)) then  ! cuts production     
     thrco = 0.0_rp  !  thrta(1)
     powco = 0.0_rp  !  powta(1)
  else if (veinf.gt.velta_nsi(ntabl_nsi(pmate), pmate)) then !cuts production
     thrco = 0.0_rp  !  thrta(ntabl)
     powco = 0.0_rp  !  powta(ntabl)
  else !linear interpolation
     iz = 1
     jz = ntabl_nsi(pmate)
     kz = ntabl_nsi(pmate)/2            
     do while ((jz-iz).gt.1)                 
        if (veinf.lt.velta_nsi(kz, pmate)) then
           jz = kz                  
        else
           iz = kz
        end if
        kz = (iz+jz)/2
     end do

     facto = (veinf-velta_nsi(iz, pmate))/(velta_nsi(jz, pmate)-velta_nsi(iz, pmate))          
     thrco = thrta_nsi(iz, pmate) + facto*(thrta_nsi(jz, pmate)-thrta_nsi(iz, pmate))
     powco = powta_nsi(iz, pmate) + facto*(powta_nsi(jz, pmate)-powta_nsi(iz, pmate))
  end if


end subroutine nsi_thrpow
