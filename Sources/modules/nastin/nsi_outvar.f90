!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_outvar.f90
!> @author  Guillaume Houzeaux
!> @date    20/02/2013
!> @brief   Output a postprocess variable
!> @details Output a postprocess variable for nastin.
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_outvar(jvari)
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_nastin
  use mod_memchk
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  use mod_lodi_nsi 
  use mod_ker_proper


  implicit none
  integer(ip), intent(in) :: jvari   !< 1 for veloc , 2 press , etc...
  integer(ip)             :: ibopo,ivari
  integer(ip)             :: idime,ipoin,iline,icont,jpoin
  integer(ip)             :: izdom,jzdom,jdime
  integer(4)              :: istat
  real(rp)                :: vmodu,xsoun,sofac,xfact,h,rho,mu,u,auxi
  real(rp)                :: tveno,delta_aux,vikin,gesma(3)
  real(rp),    pointer    :: geve2(:,:)
  real(rp)                :: rot_matrix(ndime,ndime)

integer(ip)             :: dummi

  nullify(geve2)
  !
  ! Define postprocess variable
  !
  ibopo = 0
  ivari = abs(jvari)

  select case (ivari)  

  case(0_ip)
     !
     ! Do not postprocess anything
     !
     return

  case(1_ip)
     !
     ! VELOC: Velocity
     !
     if( INOTMASTER ) then
        if( kfl_inert_nsi == 0 ) then
           gevec => veloc(:,:,1) 
        else
           !
           ! u <= u + w x r
           !
           call memgen(zero,ndime,npoin)
           call rotmat(ndime,cutim*fvnoa_nsi,fvdia_nsi,rot_matrix)
           do ipoin = 1,npoin
              if( ndime == 2 ) then
                 gesma(1) = veloc(1,ipoin,1)&
                      - fvela_nsi(3) * coord(2,ipoin)            ! u-wz*y
                 gesma(2) = veloc(2,ipoin,1)&  
                      + fvela_nsi(3) * coord(1,ipoin)            ! v+wz*x
              else
                 gesma(1) = veloc(1,ipoin,1)&
                      - fvela_nsi(3) * coord(2,ipoin)&           ! u-wz*y
                      + fvela_nsi(2) * coord(3,ipoin)            !  +wy*z
                 gesma(2) = veloc(2,ipoin,1)&
                      + fvela_nsi(3) * coord(1,ipoin)&           ! v+wz*x
                      - fvela_nsi(1) * coord(3,ipoin)            !  -wx*z
                 gesma(3) = veloc(3,ipoin,1)&
                      + fvela_nsi(1) * coord(2,ipoin)&           ! w+wx*y
                      - fvela_nsi(2) * coord(1,ipoin)            !  -wy*x
              end if
              do idime=1,ndime
                 gevec(idime,ipoin) = 0.0_rp              
                 do jdime=1,ndime
                    gevec(idime,ipoin) = gevec(idime,ipoin) + rot_matrix(idime,jdime)*gesma(jdime)
                 end do
              end do
           end do
        end if
     end if

  case(2_ip)
     !
     ! PRESS: Pressure
     !
     if( kfl_regim_nsi == 2 ) then
        if( INOTMASTER ) then
           call memgen(zero,npoin,zero)
           do ipoin = 1,npoin
              rhsid(ipoin) = densi(ipoin,1)*gasco*tempe(ipoin,1)
           end do
           gesca => rhsid
        end if
     else 
        gesca => press(:,1) 
     end if

  case(3_ip)
     !
     ! STREA: Streamlines 
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call strfun(veloc)
     end if

  case(4_ip)
     !
     ! RESID: Velocity residual
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call nsi_outres()
     end if

  case(5_ip)
     !
     ! VESGS: Velocity subgrid scale
     !
     if( INOTMASTER ) ger3p => vesgs(:) 

  case(6_ip)
     !
     ! BOUND: Boundary conditions
     !
     if( INOTMASTER ) then 
        call memgen(zero,ndime,npoin)
        do ipoin=1,npoin
           do idime=1,ndime
              gevec(idime,ipoin)=unkno((ipoin-1)*ndime+idime)
           end do
        end do
     end if

  case(7_ip)
     !
     ! DENSI: Density
     !
     if( INOTMASTER ) then
        if ( kfl_prope /= 0_ip ) call runend ('DENSITY must be output by kernel not nsi')
        call memgen(zero,npoin,zero)
        if (kfl_regim_nsi == 3) then !Low Mach regime computes its own density from temperature
           do ipoin = 1,npoin
              gesca(ipoin) = prthe(1)/(gasco*tempe(ipoin,1))
           end do
        else
           do ipoin = 1,npoin
              gesca(ipoin) = prope_nsi(1,ipoin)
           end do
        endif
     end if

  case(8_ip)
     !
     ! PMV
     !
     if( INOTMASTER ) then
        call nsi_outpmv(1_ip,1_ip,npoin,rhsid)
        gesca => rhsid
     end if

  case(9_ip)
     !
     ! PPD
     !
     if( INOTMASTER ) then
        call nsi_outpmv(2_ip,1_ip,npoin,rhsid)
        gesca => rhsid
     end if

  case(10_ip)
     !
     ! MACHN: Mach number
     !
     if( INOTMASTER ) then
        sofac=sqrt(gamth_nsi*gasco)
        do ipoin=1,npoin
           xsoun = sofac*sqrt(abs(tempe(ipoin,1)))
           if (kfl_coupl(ID_NASTIN,ID_CHEMIC) > 0 ) xsoun = xsoun / sqrt(abs(wmean(ipoin,1)))
           vmodu = 0.0_rp
           do idime=1,ndime
              vmodu=vmodu+veloc(idime,ipoin,1)*veloc(idime,ipoin,1)
           end do
           vmodu = sqrt(vmodu)
           rhsid(ipoin) = vmodu/xsoun
        end do
        gesca => rhsid
     end if

  case(11_ip)
     !
     ! TANGE: Tangential stress
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        call nsi_outtan(1_ip)
     end if
     !ibopo=1

  case(12_ip)
     !
     ! VORTI
     !
     if(ndime==2) then
        if( INOTMASTER ) then
           call memgen(zero,ndime+1_ip,npoin)
           vorti => gevec
           call vortic(1_ip)
           do ipoin=1,npoin
              rhsid(ipoin)=sqrt(&
                   &  vorti(1,ipoin)*vorti(1,ipoin)&
                   & +vorti(2,ipoin)*vorti(2,ipoin))
           end do
           gesca => rhsid           
        end if
     else
        if( INOTMASTER ) then
           call memgen(zero,ndime,npoin)
           allocate(geve2(ndime+1,npoin),stat=istat)
           call memchk(zero,istat,mem_modul(1:2,modul),'GEVE2','nsi_outvar',geve2)
           nullify(geve2)
           vorti => geve2
           call vortic(1_ip)
           do ipoin = 1,npoin
              do idime = 1,ndime
                 gevec(idime,ipoin) = geve2(idime,ipoin)
              end do
           end do
           deallocate(geve2,stat=istat)
        end if
     end if
     nullify(vorti)

  case(13_ip)
     !
     ! MODVO
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime+1_ip,npoin)
        vorti => gevec
        call vortic(1_ip)
        do ipoin=1,npoin
           rhsid(ipoin)=0.0_rp
           do idime=1,ndime
              rhsid(ipoin)=rhsid(ipoin)+vorti(idime,ipoin)*vorti(idime,ipoin)
           end do
           rhsid(ipoin)=sqrt(rhsid(ipoin))
        end do
        gesca => rhsid
     end if
     nullify(vorti)
     
  case(14_ip)
     !
     ! LAMB2
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime+1_ip,npoin)
        call memgen(zero,npoin,0_ip)
        vorti => gevec
        call vortic(1_ip)
        do ipoin = 1,npoin
           gesca(ipoin) = gevec(ndime+1_ip,ipoin)
        end do
     end if
     nullify(vorti)

  case(15_ip)
     !
     ! GROUP: GROUPS FOR DEFLATED CG
     !
     if( INOTMASTER ) then
        do ipoin=1,npoin
           rhsid(ipoin)=real(solve(2)%lgrou(ipoin))
        end do
        gesca => rhsid
     end if

  case(16_ip)
     !
     ! LINEL: Linelets of preconditioner CG
     !
     if( INOTMASTER ) then
        icont=0
        do ipoin=1,npoin
           rhsid(ipoin)=0.0_rp
        end do
        do iline=1,solve(2)%nline
           icont=icont+1
           do ipoin=solve(2)%lline(iline),solve(2)%lline(iline+1)-1
              jpoin=solve(2)%lrenup(ipoin)
              rhsid(jpoin)=real(icont)
           end do
        end do
        gesca => rhsid
     end if

  case(17_ip)
     !
     ! VISCO: Viscosity
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        do ipoin = 1,npoin
           gesca(ipoin) = prope_nsi(2,ipoin)
        end do
     end if

  case(18_ip)
     ! 
     ! FIXPR: KFL_FIXPR_NSI
     !
     if( NSI_SCHUR_COMPLEMENT ) then
        if( INOTMASTER ) then
           do ipoin=1,npoin
              if(lpoty(ipoin)/=0) then
                 if( kfl_fixpr_nsi(1,ipoin) > 0 ) then
                    rhsid(ipoin)=1.0_rp
                 else
                    rhsid(ipoin)=0.0_rp
                 end if
              else
                 rhsid(ipoin)=0.0_rp                 
              end if
           end do
           gesca => rhsid
        end if
     end if

  case(19_ip)
     !
     ! FIXNO: KFL_FIXNO_NSI
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        do ipoin=1,npoin
           do idime=1,ndime
              gevec(idime,ipoin)=real(kfl_fixno_nsi(idime,ipoin))
           end do
        end do
     end if

  case(20_ip)
     !
     ! TAU
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,0_ip)
        call runend('NSI_OUTVAR: NOT CODED')
     end if

  case(21_ip)
     !
     ! AVVEL_NSI: averaged velocity
     !
     if(cutim>avtim_nsi) then
        auxi = cutim - avtim_nsi
        if( INOTMASTER ) then
           call memgen(zero,ndime,npoin)
           do ipoin=1,npoin
              do idime=1,ndime
                 gevec(idime,ipoin) = avvel_nsi(idime,ipoin) / auxi
                 avvel_nsi(idime,ipoin) = 0.0_rp
              end do
           end do
        end if
     else
        return
     end if

  case(22_ip)
     !
     ! SCHUR: Schur complement residual
     !
     if( INOTMASTER ) gesca => resch_nsi

  case(23_ip)
     !
     ! VEPRO: Velocity projection
     !
     gevec => vepro_nsi 

  case(24_ip)
     !
     ! PRPRO: Pressure projection
     !
     gesca => prpro_nsi 

  case(25_ip)
     ! 
     ! YPLUS: Dimensionless distance to the wall y+
     !  
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call nsi_wyplus()
     end if
     ibopo = 0

  case(26_ip)
     !
     ! LIMIT: Limiter 
     !
     if( kfl_stabi_nsi == 2 ) then
        if( INOTMASTER ) then
           do ipoin = 1,npoin
              rhsid(ipoin) = 0.0_rp
           end do
           call nsi_elmope_omp(5_ip)
           call rhsmod(1_ip,rhsid)
           do ipoin = 1,npoin
              rhsid(ipoin) = rhsid(ipoin) / vmass(ipoin)
           end do
           gesca => rhsid
        end if
     end if

  case(27_ip)
     !
     ! GRPRO: Pressure gradient projection
     !
     gevec => grpro_nsi 

  case(28_ip)
     !
     ! AVPRE_NSI: averaged pressure
     !
     if(cutim>avtim_nsi) then
        auxi = cutim - avtim_nsi
        if( INOTMASTER ) then
           call memgen(zero,npoin,zero)
           do ipoin=1,npoin
              gesca(ipoin) = avpre_nsi(ipoin) / auxi
              avpre_nsi(ipoin) = 0.0_rp
           end do
        end if
     else
        return
     end if

  case(29_ip)
     !
     ! Velocity error w/r exact solution
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call nsi_exaerr(3_ip)
     end if

  case(30_ip)
     !
     ! PECLET: Pe = rho*u*h/(2*mu)
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        xfact = 1.0_rp / real(ndime,rp)
        do ipoin = 1,npoin
           h     = vmass(ipoin) ** xfact
           rho   = prope_nsi(1,ipoin)
           mu    = prope_nsi(2,ipoin)
           !if( kfl_cotur > 0 ) mu = mu + turmu(ipoin)
           u     = 0.0_rp
           do idime = 1,ndime
              u = u + veloc(idime,ipoin,1)*veloc(idime,ipoin,1)
           end do
           u = sqrt(u)
           gesca(ipoin) = 0.5_rp * rho * u * h / mu
        end do
     end if

  case(31_ip)
     !
     ! Hydrostatic pressure
     !
     if( INOTMASTER ) then        
        gesca => bpess_nsi(1,:)
     end if

  case(32_ip)
     !
     ! du/dt
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        call nsi_outdt2(1_ip,ndime,veloc)
     end if

  case(33_ip)
     !
     ! d^2u/dt^2
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        call nsi_outdt2(2_ip,ndime,veloc)
     end if

  case(34_ip)
     !
     ! Dt criterion 1
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call nsi_outdt2(3_ip,ndime,veloc)
     end if

  case(35_ip)
     !
     ! Dt criterion 2
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call nsi_outdt2(4_ip,ndime,veloc)
     end if

  case(36_ip)
     !
     ! Pressure error w/r exact solution
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)
        call nsi_exaerr(4_ip)
     end if

  case(37_ip)
     !
     ! LINEL: Linelets of preconditioner CG
     !
     if( INOTMASTER ) then
        icont=0
        do ipoin=1,npoin
           rhsid(ipoin)=0.0_rp
        end do
        do iline=1,solve(1)%nline
           icont=icont+1
           do ipoin=solve(1)%lline(iline),solve(1)%lline(iline+1)-1
              jpoin=solve(1)%lrenup(ipoin)
              rhsid(jpoin)=real(icont)
           end do
        end do
        gesca => rhsid
     end if

  case(38_ip)
     !
     ! DEFOR: deformaiton of the mesh
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        do ipoin = 1,npoin
           do idime = 1,ndime
              gevec(idime,ipoin) = 0.0_rp
           end do
        end do
        do ipoin = 1,npoin
           do idime = 1,ndime
              gevec(idime,ipoin) = dispm(idime,ipoin,1) 
           end do
        end do
     end if

  case(39_ip)
     !
     ! NODPR
     !
     if( INOTMASTER ) then
        do ipoin = 1,npoin
           rhsid(ipoin) = 0.0_rp
        end do
        if( nodpr_nsi > 0 ) rhsid( nodpr_nsi ) = 1.0_rp
        gesca => rhsid
     end if

  case(45_ip)
     !
     ! MODVE: module of velocity vector
     !

     if( INOTMASTER ) then
        do ipoin=1,npoin
           rhsid(ipoin)=0.0_rp
           do idime=1,ndime
              rhsid(ipoin)=rhsid(ipoin)+veloc(idime,ipoin,1)*veloc(idime,ipoin,1)
           end do
           rhsid(ipoin)=sqrt(rhsid(ipoin))
        end do
        gesca => rhsid
     end if

  case(46_ip)
     !
     ! AVTAN_NSI: averaged tangential force
     !
     if(cutim>avtim_nsi) then
        auxi = cutim - avtim_nsi
        if( INOTMASTER ) then
           call memgen(zero,ndime,npoin)
           do ipoin=1,npoin
              do idime=1,ndime
                 gevec(idime,ipoin) = avtan_nsi(idime,ipoin) / auxi
                 avtan_nsi(idime,ipoin) = 0.0_rp
              end do
           end do
        end if
     else
        return
     end if


  case(47_ip)
     !
     ! LINVE: Linelets of preconditioner for velocity 
     !
     if( INOTMASTER ) then
        icont=0
        do ipoin=1,npoin
           rhsid(ipoin)=0.0_rp
        end do
        do iline=1,solve(1)%nline
           icont=icont+1
           do ipoin=solve(1)%lline(iline),solve(1)%lline(iline+1)-1
              jpoin=solve(1)%lrenup(ipoin)
              rhsid(jpoin)=real(icont)
           end do
        end do
        gesca => rhsid
     end if

  case(48_ip)
     !
     ! VELOM
     !
     if( INOTMASTER ) gevec => velom(:,:)

  case(49_ip)
     !
     ! VELOM
     !
     if( INOTMASTER ) gevec => forcf

  case(50_ip)
     !
     ! INTFO: internal force
     !
     if( kfl_intfo_nsi /= 2 ) return
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        do ipoin = 1,npoin
           if( lpoty(ipoin) > 0 ) then
              do idime = 1,ndime
                 gevec(idime,ipoin) = intfo_nsi(ipoin) % bu(idime)
                 jzdom = 0
                 do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                    jpoin = c_dom(izdom)
                    jzdom = jzdom + 1
                    do jdime = 1,ndime
                       gevec(idime,ipoin) = gevec(idime,ipoin) - intfo_nsi(ipoin) % Auu(jdime,idime,jzdom) * veloc(jdime,jpoin,1) 
                    end do
                    gevec(idime,ipoin) = gevec(idime,ipoin) - intfo_nsi(ipoin) % Aup(idime,jzdom) * press(jpoin,1) 
                 end do
              end do
           end if
        end do
        call rhsmod(ndime,gevec)
     end if

  case(51_ip)
     !
     ! USTAR
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,0_ip)
        if( kfl_rough > 0 ) then
           rough_dom = rough(ipoin)
        end if
        call runend('NSI_OUTVAR: RECODE WITH PROPERTIES')
        do ipoin = 1,npoin
           if( lpoty(ipoin) > 0 ) then
              if( kfl_delta==1) then
                 delta_aux = ywalp(lpoty(ipoin))
              else
                 delta_aux = delta_nsi  
              end if
              !vikin = visco_nsi(1,1)/densi_nsi(1,1)
              tveno = 0.0_rp
              do idime = 1,ndime
                 tveno = tveno + veloc(idime,ipoin,1) * veloc(idime,ipoin,1) 
              end do
              tveno = sqrt(tveno)
              call frivel(delta_aux,rough_dom,tveno,vikin,gesca(ipoin))
           end if
        end do
     end if

  case(52_ip)
     !
     ! TRACT: Traction
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        call nsi_outtra(1_ip)
     end if

  case(53_ip)
     !
     ! KFL_FIXRS_NSI
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,0_ip)
        do ipoin = 1,npoin
           if( lpoty(ipoin) > 0 ) then
              gesca(ipoin) = kfl_fixrs_nsi(lpoty(ipoin))
           else
              gesca(ipoin) = 0.0_rp 
           end if
        end do
     end if

  case(54_ip)
     !
     ! FVELO: Velocity for filter
     !
     if( INOTMASTER ) then
        if( kfl_inert_nsi == 0 ) then
           gevec => veloc(:,:,1) 
        else
           call memgen(zero,ndime,npoin)
           if( ndime == 2 ) then
              do ipoin = 1,npoin
                 gevec(1,ipoin) = veloc(1,ipoin,1)&
                      - fvela_nsi(3) * coord(2,ipoin)            ! u-wz*y
                 gevec(2,ipoin) = veloc(2,ipoin,1)&  
                      + fvela_nsi(3) * coord(1,ipoin)            ! v+wz*x
              end do
           else
              do ipoin = 1,npoin
                 gevec(1,ipoin) = veloc(1,ipoin,1)&
                      - fvela_nsi(3) * coord(2,ipoin)&           ! u-wz*y
                      + fvela_nsi(2) * coord(3,ipoin)            !  +wy*z
                 gevec(2,ipoin) = veloc(2,ipoin,1)&
                      + fvela_nsi(3) * coord(1,ipoin)&           ! v+wz*x
                      - fvela_nsi(1) * coord(3,ipoin)            !  -wx*z
                 gevec(3,ipoin) = veloc(3,ipoin,1)&
                      + fvela_nsi(1) * coord(2,ipoin)&           ! w+wx*y
                      - fvela_nsi(2) * coord(1,ipoin)            !  -wy*x
              end do
           end if
        end if
     end if

  case(55_ip)
     !
     ! FPRES: Pressure for filter 
     !
     if( kfl_regim_nsi == 2 ) then
        if( INOTMASTER ) then
           call memgen(zero,npoin,zero)
           do ipoin = 1,npoin
              rhsid(ipoin) = densi(ipoin,1)*gasco*tempe(ipoin,1)
           end do
           gesca => rhsid
        end if

     else 
        gesca => press(:,1) 
     end if


  case(56_ip)
     !
     ! FTANG: Tangential stress for filter
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        call nsi_outtan(1_ip)
     end if

  case(57_ip)
     !
     ! AVVE2_NSI: averaged velocity**2
     !
     if(cutim>avtim_nsi) then
        auxi = cutim - avtim_nsi
        if( INOTMASTER ) then
           call memgen(zero,ndime,npoin)
           do ipoin=1,npoin
              do idime=1,ndime
                 gevec(idime,ipoin) = avve2_nsi(idime,ipoin) / auxi
                 avve2_nsi(idime,ipoin) = 0.0_rp
              end do
           end do
        end if
     else
        return
     end if

  case(58_ip)
     !
     ! AVVXY_NSI: averaged vx * vy
     !
     if(cutim>avtim_nsi) then
        auxi = cutim - avtim_nsi
        if( INOTMASTER ) then
           call memgen(zero,ndime,npoin)
           do ipoin=1,npoin
              do idime=1,ndime
                 gevec(idime,ipoin) = avvxy_nsi(idime,ipoin) / auxi
                 avvxy_nsi(idime,ipoin) = 0.0_rp
              end do
           end do
        end if
     else
        return
     end if

  case(59_ip)
     !
     ! AVPR2_NSI: averaged pressure**2
     !
     if(cutim>avtim_nsi) then
        auxi = cutim - avtim_nsi
        if( INOTMASTER ) then
           call memgen(zero,npoin,zero)
           do ipoin=1,npoin
              gesca(ipoin) = avpr2_nsi(ipoin) / auxi 
              avpr2_nsi(ipoin) = 0.0_rp
           end do
        end if
     else
        return
     end if

  case (60_ip)
     !
     ! Mesh rotation
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)           
        if( kfl_inert_nsi == 0 ) then
           gevec = 0.0_rp
        else
           !
           ! Compute rotation matrix
           !
           call rotmat(ndime,cutim*fvnoa_nsi,fvdia_nsi,rot_matrix)
           do ipoin = 1,npoin
              do idime=1,ndime
                 gevec(idime,ipoin)=0.0_rp              
                 do jdime=1,ndime
                    gevec(idime,ipoin) = gevec(idime,ipoin) + rot_matrix(idime,jdime)*coord(jdime,ipoin) ! Rotated coordinate
                 enddo
                 gevec(idime,ipoin) =  gevec(idime,ipoin) - coord(idime,ipoin) ! But we must save the displacement
              end do
           enddo
        end if
     end if

  case (61_ip)
     !
     ! NODE_FORCE
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)   
        do ipoin = 1,npoin
           gesca(ipoin) = real(intfo_nsi(ipoin) % kfl_exist,rp)              
        end do
        call pararr('SMA',NPOIN_TYPE,npoin,gesca)
     end if

  case(62_ip)
     !
     ! Momentum residual
     !
     if( INOTMASTER ) gevec => remom_nsi

  case(63_ip)
     ! 
     ! FIXPP_NSI
     !
     if( INOTMASTER ) then
        call memgen(zero,npoin,zero)   
        do ipoin = 1,npoin
           gesca(ipoin) = real(kfl_fixpp_nsi(1,ipoin),rp)
        end do
     end if

  case(64_ip)
     ! 
     ! Neumann condition at algebraic level
     !
     if( INOTMASTER ) then
        !call memgen(zero,ndime,npoin)
        !do ipoin = 1,npoin
        !   gevec(1:ndime,ipoin) = solve_sol(1) % block_array(1) % bvnat(1:ndime,ipoin) 
        !end do
        !call PAR_INTERFACE_NODE_EXCHANGE(gevec,'SUM','IN MY ZONE')
        call memgen(zero,npoin,0_ip)
        print*,associated(solve_sol(1) % block_array(2) % bvnat),size(solve_sol(1) % block_array(2) % bvnat),npoin
        do ipoin = 1,npoin
           gesca(ipoin) = solve_sol(1) % block_array(2) % bvnat(1,ipoin) 
        end do
        call PAR_INTERFACE_NODE_EXCHANGE(gesca,'SUM','IN MY ZONE')
     end if

  case(65_ip) 
     ! 
     ! Reaction at algebraic level
     !
     if( INOTMASTER ) then
        call memgen(zero,ndime,npoin)
        do ipoin = 1,npoin
           gevec(1:ndime,ipoin) = solve_sol(1) % reaction(1:ndime,ipoin) 
        end do
     end if

  case(66_ip)
     if( INOTMASTER ) then
        !call memgen(zero,npoin,zero)
        !call ker_proper('DENSI','NPOIN',dummi,dummi,gesca)
        !gesca(1:npoin) = prthe(1)/(gasco*tempe(1:npoin,1))
        !CHARAs%densi => gesca 
        CHARAs%gamme = gamth_nsi  
        call lodi_nsi_allocate( CHARAs )
        call lodi_nsi_get_characteristics( CHARAs )
        !
        call memgen(zero,ndime,npoin)
        do idime = 1,ndime
          do ipoin = 1,npoin 
            gevec(idime,ipoin) = CHARAs%chrc(1_ip,ipoin,idime) ! chrc_nsi(ndofn_nsi,npoin,ndime) 
          enddo
        enddo
        !
        call lodi_nsi_deallocate( CHARAs )
     end if

  case(67_ip)
    call get_characteristic( 2_ip ) 

  case(68_ip)
    call get_characteristic( 3_ip )

  case(69_ip)
    call get_characteristic( 4_ip )

  case(70_ip)
    call get_characteristic( 5_ip )

  end select

  !
  ! Postprocess
  !
  call outvar(&
       jvari,&
       ittim,cutim,postp(1) % wopos(1,ivari))


contains

  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  subroutine get_characteristic( idofn )  
  implicit none
  integer(ip), intent(in) :: idofn 
  !
  if( INOTMASTER ) then
        !call memgen(zero,npoin,zero)
        !call ker_proper('DENSI','NPOIN',dummi,dummi,gesca)
        !gesca(1:npoin) = prthe(1)/(gasco*tempe(1:npoin,1))
        !CHARAs%densi => gesca 
        CHARAs%gamme = gamth_nsi
        call lodi_nsi_allocate( CHARAs )
        call lodi_nsi_get_characteristics( CHARAs )
        !
        call memgen(zero,ndime,npoin)
        do idime = 1,ndime
          do ipoin = 1,npoin
            gevec(idime,ipoin) = CHARAs%chrc(idofn,ipoin,idime) ! chrc_nsi(ndofn_nsi,npoin,ndime) 
          enddo
        enddo
        !
        call lodi_nsi_deallocate( CHARAs )
  end if
  !
  end subroutine
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!


end subroutine nsi_outvar
