subroutine nsi_outwit()
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_outwit
  ! NAME 
  !    nsi_outwit
  ! DESCRIPTION
  !    Output results on witness points
  ! USES
  ! USED BY
  !    nsi_output
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_nastin
  implicit none
  integer(ip) :: iwitn,ielem,inode,pnode,pelty,ipoin,ivawi,dummi

  if( nwitn > 0 .and. maxval(postp(1) % npp_witne) > 0 ) then

     if( INOTMASTER ) then 

        do iwitn = 1, nwitn
           ielem = lewit(iwitn)
           if( ielem > 0 ) then

              pelty = ltype(ielem)
              pnode = nnode(pelty)
              do ivawi = 1,postp(1) % nvawi
                 witne(ivawi,iwitn) = 0.0_rp
              end do

              if( postp(1) % npp_witne(1) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(1,iwitn) = witne(1,iwitn) + shwit(inode,iwitn) * veloc(1,ipoin,1)
                 end do
              end if

              if( postp(1) % npp_witne(2) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(2,iwitn) = witne(2,iwitn) + shwit(inode,iwitn) * veloc(2,ipoin,1)
                 end do
              end if

              if( postp(1) % npp_witne(3) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(3,iwitn) = witne(3,iwitn) + shwit(inode,iwitn) * veloc(ndime,ipoin,1)
                 end do
              end if

              if( postp(1) % npp_witne(4) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(4,iwitn) = witne(4,iwitn) + shwit(inode,iwitn) * press(ipoin,1)
                 end do
              end if

              if( postp(1) % npp_witne(5) == 1 ) then  
                 !
                 ! S11
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(5,iwitn) = witne(5,iwitn) + dewit(1,inode,iwitn) * veloc(1,ipoin,1)
                 end do
              end if

              if( postp(1) % npp_witne(6) == 1 ) then  
                 !
                 ! S22
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(6,iwitn) = witne(6,iwitn) + dewit(2,inode,iwitn) * veloc(2,ipoin,1)
                 end do
              end if

              if( postp(1) % npp_witne(7) == 1 ) then  
                 !
                 ! S12
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(7,iwitn) = witne(7,iwitn) &
                         + 0.5_rp*(   dewit(2,inode,iwitn) * veloc(1,ipoin,1) &
                         &          + dewit(1,inode,iwitn) * veloc(2,ipoin,1) )
                 end do
              end if

              if( postp(1) % npp_witne(8) == 1 ) then  
                 !
                 ! S33
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(8,iwitn) = witne(8,iwitn) + dewit(ndime,inode,iwitn) * veloc(ndime,ipoin,1)
                 end do
              end if

              if( postp(1) % npp_witne(9) == 1 ) then  
                 !
                 ! S13
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(9,iwitn) = witne(9,iwitn) &
                         + 0.5_rp*(   dewit(ndime,inode,iwitn) * veloc(1,ipoin,1) &
                         &          + dewit(1,inode,iwitn)     * veloc(ndime,ipoin,1) )
                 end do
              end if

              if( postp(1) % npp_witne(10) == 1 ) then  
                 !
                 ! S23
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(10,iwitn) = witne(10,iwitn) &
                         + 0.5_rp*(   dewit(ndime,inode,iwitn) * veloc(2,ipoin,1) &
                         &          + dewit(2,inode,iwitn)     * veloc(ndime,ipoin,1) )
                 end do
              end if

              if( postp(1) % npp_witne(11) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(11,iwitn) = witne(11,iwitn) + shwit(inode,iwitn) * coord(1,ipoin)
                 end do
              end if

              if( postp(1) % npp_witne(12) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(12,iwitn) = witne(12,iwitn) + shwit(inode,iwitn) * coord(2,ipoin)
                 end do
              end if

              if( postp(1) % npp_witne(13) == 1 ) then
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    witne(13,iwitn) = witne(13,iwitn) + shwit(inode,iwitn) * coord(ndime,ipoin)
                 end do
              end if

           end if
        end do

     end if
     !
     ! Parall
     !
     call posdef(24_ip,dummi)

  end if

end subroutine nsi_outwit

