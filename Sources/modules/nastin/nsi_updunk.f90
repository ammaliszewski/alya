subroutine nsi_updunk(itask)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_updunk
  ! NAME 
  !    nsi_updunk
  ! DESCRIPTION
  !    This routine performs several types of updates for the velocity and
  !    pressure.
  ! USED BY
  !    nsi_begste (itask=1)
  !    nsi_begite (itask=2)
  !    nsi_endite (itask=3, inner loop) 
  !    nsi_endite (itask=4, outer loop) 
  !    nsi_endste (itask=5)
  !    nsi_endste (itask=6)               Updates Pressure
  !***
  !-----------------------------------------------------------------------
  use def_kintyp,           only : ip,rp
  use def_master,           only : INOTMASTER,veloc,press,densi
  use def_master,           only : tempe,unkno,vesgs,solve,rbbou
  use def_master,           only : coupling,ID_NASTIN,lzone
  use def_domain,           only : npoin,nelem,nelez,lelez,ndime
  use def_domain,           only : ngaus,ltype,nrbod
  use def_nastin,           only : kfl_regim_nsi,NSI_MONOLITHIC
  use def_nastin,           only : relax_nsi,kfl_tiaor_nsi
  use def_nastin,           only : kfl_tiacc_nsi,kfl_tisch_nsi
  use def_nastin,           only : kfl_sgsac_nsi,kfl_resid_nsi
  use def_nastin,           only : veold_nsi,bpess_nsi
  use def_nastin,           only : ivari_nsi,kfl_dttyp_nsi
  use def_nastin,           only : kfl_relax_nsi
  use def_nastin,           only : nprev_nsi,relap_nsi
  use def_nastin,           only : kfl_sgsti_nsi,kfl_relap_nsi
  use def_nastin,           only : kfl_updpr_nsi,ndbgs_nsi
  use def_kermod,           only : gasco
  use mod_array_operations, only : array_operations_axpby
  use mod_array_operations, only : array_operations_copy
  use mod_array_operations, only : array_operations_axoz
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,itotv,idime,itime,ielem,igaus
  integer(ip)             :: pelty,pgaus,ivar1,ivar2,kelem,iimbo
  real(rp)                :: rela1_nsi
  real(rp),    pointer    :: vpfor(:,:)
  real(rp),    pointer    :: vptor(:,:)
  character(20)           :: useomp

  if( 1 == 1 ) then
     useomp = 'DO NOT USE OPENMP'
  else
     useomp = 'USE OPENMP'
  end if

  if( INOTMASTER ) then

     select case (itask)

     case(1_ip) 
        !
        ! Assign u(n,0,*) <-- u(n-1,*,*), initial guess for outer iterations
        !
        !call array_operations_copy(ndime,npoin,veloc(:,:,nprev_nsi),veloc(:,:,2),useomp)
        do ipoin = 1,npoin
           do idime = 1,ndime
              veloc(idime,ipoin,2) = veloc(idime,ipoin,nprev_nsi)
           end do
        end do
        if( kfl_regim_nsi == 2 ) then
           !call array_operations_copy(npoin,densi(:,nprev_nsi),densi(:,2),useomp)
           do ipoin = 1,npoin
              densi(ipoin,2) = densi(ipoin,nprev_nsi)
           end do
        else
           !call array_operations_copy(npoin,press(:,nprev_nsi),press(:,2),useomp)
           do ipoin = 1,npoin
              press(ipoin,2) = press(ipoin,nprev_nsi)
           end do
        end if
        if( kfl_sgsti_nsi == 1 ) then
           !
           ! Time tracking of the subscales
           !
           !do ielem = 1,nelem
           !   if( lelch(ielem) /= ELHOL ) then
           !   pelty = ltype(ielem)
           !   pgaus = ngaus(pelty)
           !   do igaus = 1,pgaus
           !      do idime = 1,ndime
           !         vesgs(ielem)%a(idime,igaus,1) = vesgs(ielem)%a(idime,igaus,2)
           !      end do
           !   end do
           !   end if
           !end do
        end if

     case(2_ip)
        !
        ! Assign u(n,i,0) <-- u(n,i-1,*), initial guess for inner iterations
        !
        !call array_operations_copy(ndime,npoin,veloc(:,:,2),veloc(:,:,1),useomp)
        do ipoin = 1,npoin
           do idime = 1,ndime
              veloc(idime,ipoin,1) = veloc(idime,ipoin,2)
           end do
        end do
        if( kfl_regim_nsi == 2 ) then
           !call array_operations_copy(npoin,densi(:,2),densi(:,1),useomp)
           do ipoin = 1,npoin
              densi(ipoin,1) = densi(ipoin,2)
           end do
        else
           !call array_operations_copy(npoin,press(:,2),press(:,1),useomp)
           do ipoin = 1,npoin
              press(ipoin,1) = press(ipoin,2)
           end do
        end if

        if( NSI_MONOLITHIC ) then
           !
           ! Monolithic: UNKNO=[ u1 v1 p1 u2 v2 p2 ... ]
           !
           if( kfl_regim_nsi == 2 ) then
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = (ipoin-1) * solve(1) % ndofn + idime
                    unkno(itotv) = veloc(idime,ipoin,2)
                 end do
                 unkno(itotv+1) = densi(ipoin,2)
              end do
           else
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = (ipoin-1) * solve(1) % ndofn + idime
                    unkno(itotv) = veloc(idime,ipoin,2)
                 end do
                 unkno(itotv+1) = press(ipoin,2)
              end do
           end if

        else
           !
           ! Schur complement: UNKNO=[ u1 v1 u2 v2 ... p1 p2 ... ]
           !
           !call array_operations_copy(ndime,npoin,veloc(:,:,2),unkno,useomp)

           if( kfl_regim_nsi == 2 ) then
              !call array_operations_copy(npoin,densi(:,2),unkno(ndbgs_nsi+1:),useomp)
              ndbgs_nsi = npoin*ndime
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = (ipoin-1) * ndime + idime
                    unkno(itotv)=veloc(idime,ipoin,2)
                 end do
                 unkno(ndbgs_nsi+ipoin)=densi(ipoin,2)
              end do
           else
              !call array_operations_copy(npoin,press(:,2),unkno(ndbgs_nsi+1:),useomp)
              ndbgs_nsi = npoin*ndime
              itotv = 0
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = (ipoin-1) * ndime + idime
                    unkno(itotv) = veloc(idime,ipoin,2)
                 end do
                 unkno(ndbgs_nsi+ipoin) = press(ipoin,2)
              end do
           end if

        end if

     case(3_ip)
        !
        ! Assign u(n,i,j-1) <-- u(n,i,j), update of the velocity and pressure
        !
        if( .not. NSI_MONOLITHIC ) then
           !
           ! Schur complement: UNKNO=[ u1 v1 u2 v2 ... p1 p2 ... ]
           !
           if(kfl_regim_nsi==2) then
              !call array_operations_copy(ndime,npoin,unkno,veloc(:,:,1),useomp)
              !call array_operations_copy(npoin,unkno(ndbgs_nsi+1:),densi(:,1),useomp)
              ndbgs_nsi=npoin*ndime
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = (ipoin-1)*ndime + idime 
                    veloc(idime,ipoin,1)=unkno(itotv)
                 end do
                 densi(ipoin,1)=unkno(ndbgs_nsi+ipoin)
              end do
           else
              !call array_operations_copy(ndime,npoin,unkno,veloc(:,:,1),useomp)
              !call array_operations_copy(npoin,unkno(ndbgs_nsi+1:),press(:,1),useomp)
              ndbgs_nsi=npoin*ndime
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv= (ipoin-1)*ndime + idime 
                    veloc(idime,ipoin,1) = unkno(itotv)
                 end do
                 press(ipoin,1)=unkno(ndbgs_nsi+ipoin)
              end do
           end if

        else
           !
           ! Monolithic: UNKNO=[ u1 v1 p1 u2 v2 p2 ... ]
           !
           if( kfl_regim_nsi == 2 ) then
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = (ipoin-1) * solve(1) % ndofn + idime
                    veloc(idime,ipoin,1) = unkno(itotv)
                 end do
                 densi(ipoin,1) = unkno(itotv+1)
              end do
           else
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = (ipoin-1) * solve(1) % ndofn + idime
                    veloc(idime,ipoin,1) = unkno(itotv)
                 end do
                 press(ipoin,1) = unkno(itotv+1)
              end do
           end if

        end if

     case(4_ip)
        !
        ! Assign u(n,i-1,*) <-- u(n,i,*)
        !        
        ! If velocity residual is required, save previous interation in veold_nsi
        !
        if( kfl_resid_nsi == 1 ) then
           !call array_operations_copy(ndime,npoin,veloc(:,:,2),veold_nsi,useomp)
           do ipoin = 1,npoin
              do idime = 1,ndime
                 veold_nsi(idime,ipoin) = veloc(idime,ipoin,2)
              end do
           end do
        end if

        !call array_operations_copy(ndime,npoin,veloc(:,:,1),veloc(:,:,2),useomp)

        if( kfl_regim_nsi == 2 ) then
           !call array_operations_copy(npoin,densi(:,1),densi(:,2),useomp)
           !call array_operations_copy(veloc(:,:,2),veloc(:,:,1))
           !call array_operations_copy(densi(:,2),densi(:,1))
           do ipoin = 1,npoin
              do idime = 1,ndime
                 veloc(idime,ipoin,2) = veloc(idime,ipoin,1)
              end do
              densi(ipoin,2) = densi(ipoin,1)
           end do
        else
           !call array_operations_copy(npoin,press(:,1),press(:,2),useomp)
           do ipoin = 1,npoin
              do idime = 1,ndime
                 veloc(idime,ipoin,2) = veloc(idime,ipoin,1)
              end do
              press(ipoin,2) = press(ipoin,1)
           end do
        end if

     case(5_ip)
        !
        ! u(n-1,*,*) <-- u(n,*,*)
        !        
        if( kfl_tisch_nsi == 1 .and. kfl_tiacc_nsi == 2 ) then
           !
           ! Crank-Nicolson method 
           ! 
           !call array_operations_axpby(ndime,npoin,-1.0_rp,2.0_rp,veloc(:,:,3),veloc(:,:,1),useomp) 
           do ipoin = 1,npoin
              do idime = 1,ndime
                 veloc(idime,ipoin,1) = 2.0_rp*veloc(idime,ipoin,1)-veloc(idime,ipoin,3)
              end do
           end do
           if( kfl_updpr_nsi== 1 ) then
              if( kfl_regim_nsi == 2 ) then
                 !call array_operations_axpby(npoin,-1.0_rp,2.0_rp,densi(:,3),densi(:,1),useomp)
                 do ipoin = 1,npoin
                    densi(ipoin,1) = 2.0_rp*densi(ipoin,1)-densi(ipoin,3)
                 end do
              else
                 !call array_operations_axpby(npoin,-1.0_rp,2.0_rp,press(:,3),press(:,1),useomp)
                 do ipoin = 1,npoin
                    press(ipoin,1) = 2.0_rp*press(ipoin,1)-press(ipoin,3)
                 end do
              end if
           end if

        else if( kfl_tisch_nsi == 2 ) then
           !
           ! BDF scheme
           !
           do itime = 2+kfl_tiaor_nsi,4,-1
              !call array_operations_copy(ndime,npoin,veloc(:,:,itime-1),veloc(:,:,itime),useomp)
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    veloc(idime,ipoin,itime) = veloc(idime,ipoin,itime-1)
                 end do
              end do
           end do
        end if

        ivar1 = 32
        call posdef(25_ip,ivar1) 
        ivar2 = 33
        call posdef(25_ip,ivar2) 
        if( kfl_dttyp_nsi == 2 .or. ivar1 /= 0 .or. ivar2 /= 0 ) then
           !call array_operations_copy(ndime,npoin,veloc(:,:,4),veloc(:,:,5),useomp)
           !call array_operations_copy(ndime,npoin,veloc(:,:,3),veloc(:,:,4),useomp)
           !call array_operations_copy(ndime,npoin,veloc(:,:,1),veloc(:,:,3),useomp)
           do ipoin = 1,npoin
              veloc(1:ndime,ipoin,5) = veloc(1:ndime,ipoin,4)
              veloc(1:ndime,ipoin,4) = veloc(1:ndime,ipoin,3)
              veloc(1:ndime,ipoin,3) = veloc(1:ndime,ipoin,1)
           end do
        else
           !call array_operations_copy(ndime,npoin,veloc(:,:,1),veloc(:,:,3),useomp)
           do ipoin = 1,npoin
              veloc(1:ndime,ipoin,3) = veloc(1:ndime,ipoin,1)
           end do
        end if
        if( kfl_regim_nsi == 2 ) then
           !call array_operations_copy(npoin,densi(:,1),densi(:,3),useomp)
           do ipoin = 1,npoin
              densi(ipoin,3) = densi(ipoin,1)
           end do
        else
           !call array_operations_copy(npoin,press(:,1),press(:,3),useomp)
           do ipoin = 1,npoin
              press(ipoin,3) = press(ipoin,1)
           end do
        end if

        if( kfl_sgsti_nsi == 1 ) then
           !
           ! Time tracking of the subscales
           !
           if( kfl_sgsac_nsi == 2 .and. kfl_tiacc_nsi == 2 ) then 
              !$OMP PARALLEL DO SCHEDULE (STATIC)                              & 
              !$OMP DEFAULT  ( NONE )                                          &
              !$OMP PRIVATE  ( kelem, ielem, pelty, pgaus )                    &            
              !$OMP SHARED   ( nelez, lelez, vesgs, ndime, ltype, ngaus, lzone )
              do kelem = 1,nelez(lzone(ID_NASTIN))
                 ielem = lelez(lzone(ID_NASTIN)) % l(kelem)
                 pelty = ltype(ielem)
                 pgaus = ngaus(pelty)
                 vesgs(ielem) % a(1:ndime,1:pgaus,1) =             &
                      & 2.0_rp*vesgs(ielem) % a(1:ndime,1:pgaus,1) &
                      &       -vesgs(ielem) % a(1:ndime,1:pgaus,2)
              end do
              !$OMP END PARALLEL DO
           end if
           !$OMP PARALLEL DO SCHEDULE (STATIC)                              & 
           !$OMP DEFAULT  ( NONE )                                          &
           !$OMP PRIVATE  ( kelem, ielem, pelty, pgaus )                    &            
           !$OMP SHARED   ( nelez, lelez, vesgs, ndime, ltype, ngaus, lzone )
           do kelem = 1,nelez(lzone(ID_NASTIN))
              ielem = lelez(lzone(ID_NASTIN)) % l(kelem)
              pelty = ltype(ielem)
              pgaus = ngaus(pelty)
              vesgs(ielem) % a(1:ndime,1:pgaus,2) = vesgs(ielem) % a(1:ndime,1:pgaus,1)
           end do
           !$OMP END PARALLEL DO
        end if
        if ( ( coupling('ALEFOR','NASTIN') >= 1 ) .and. ( nrbod /= 0 ) ) then   ! rigid body
           do iimbo = 1,nrbod
              vpfor         => rbbou(iimbo) % vpfor
              vptor         => rbbou(iimbo) % vptor
              vpfor(1:3,4)  =  vpfor(1:3,3)
              vpfor(1:3,3)  =  vpfor(1:3,1)
              vptor(1:3,4)  =  vptor(1:3,3)
              vptor(1:3,3)  =  vptor(1:3,1)
           end do
        end if

     case(11_ip)
        !
        ! Assign u(n,i,*)  <-- u(n-1,*,*), initial guess after reading restart
        !        u'(n,i,*) <-- u'(n-1,*,*)
        !
        !call array_operations_copy(ndime,npoin,veloc(:,:,nprev_nsi),veloc(:,:,1),useomp)
        !call array_operations_copy(ndime,npoin,veloc(:,:,nprev_nsi),veloc(:,:,2),useomp)
        do ipoin = 1,npoin
           do idime = 1,ndime           
              veloc(idime,ipoin,1) = veloc(idime,ipoin,nprev_nsi) 
              veloc(idime,ipoin,2) = veloc(idime,ipoin,nprev_nsi) 
           end do
        end do
        if( kfl_regim_nsi == 2 ) then
           !call array_operations_copy(npoin,densi(:,nprev_nsi),densi(:,1),useomp)
           !call array_operations_copy(npoin,densi(:,nprev_nsi),densi(:,2),useomp)
           do ipoin = 1,npoin
              densi(ipoin,1) = densi(ipoin,nprev_nsi) 
              densi(ipoin,2) = densi(ipoin,nprev_nsi) 
           end do
        else
           !call array_operations_copy(npoin,press(:,nprev_nsi),press(:,1),useomp)
           !call array_operations_copy(npoin,press(:,nprev_nsi),press(:,2),useomp)
           do ipoin = 1,npoin
              press(ipoin,1) = press(ipoin,nprev_nsi) 
              press(ipoin,2) = press(ipoin,nprev_nsi) 
           end do
        end if

        if( kfl_sgsti_nsi == 1 ) then
           !$OMP PARALLEL DO SCHEDULE (STATIC)                              & 
           !$OMP DEFAULT  ( NONE )                                          &
           !$OMP PRIVATE  ( kelem, ielem, pelty, pgaus )                    &            
           !$OMP SHARED   ( nelez, lelez, vesgs, ndime, ltype, ngaus, lzone )
           do kelem = 1,nelez(lzone(ID_NASTIN))
              ielem = lelez(lzone(ID_NASTIN)) % l(kelem)
              pelty = ltype(ielem)
              pgaus = ngaus(pelty)
              vesgs(ielem) % a(1:ndime,1:pgaus,1) = vesgs(ielem) % a(1:ndime,1:pgaus,2)
           end do
           !$OMP END PARALLEL DO
        end if

     case(14_ip)  
        !
        ! Assign xx(n,i-1,*) <-- xx(n,i,*) for rigid body variables
        !
        if ( ( coupling('ALEFOR','NASTIN') >= 1 ) .and. ( nrbod /= 0 ) ) then   ! rigid body
           do iimbo = 1,nrbod
              vpfor         => rbbou(iimbo) % vpfor
              vptor         => rbbou(iimbo) % vptor
              vpfor(1:3,2)  =  vpfor(1:3,1)
              vptor(1:3,2)  =  vptor(1:3,1)
           end do
        end if

     case(15_ip)  
        !
        ! Relax UNKNO
        !
        if( .not. NSI_MONOLITHIC ) then
           !
           ! Block Gauss-Seidel: UNKNO=[ u1 v1 u2 v2 ... p1 p2 ... ]
           !
           if( ivari_nsi == 1 .and. kfl_relax_nsi /= 0 ) then
              rela1_nsi = 1.0_rp - relax_nsi
              !call array_operations_axpby(ndime,npoin,rela1_nsi,relax_nsi,veloc(:,:,1),unkno,useomp) 
              do ipoin = 1,npoin
                 itotv=(ipoin-1)*ndime
                 do idime = 1,ndime
                    itotv=itotv+1
                    unkno(itotv) = relax_nsi*unkno(itotv)&
                         &           + rela1_nsi*veloc(idime,ipoin,1)
                 end do
              end do
           end if
           if( ivari_nsi == 2 .and. kfl_relap_nsi /= 0 ) then
              rela1_nsi = 1.0_rp-relap_nsi
              if( kfl_regim_nsi == 2 ) then
                 !call array_operations_axpby(npoin,rela1_nsi,relap_nsi,densi(:,1),unkno(ndbgs_nsi+1:),useomp)
                 do ipoin = 1,npoin
                    itotv = ndbgs_nsi+ipoin
                    unkno(itotv) =    relap_nsi * unkno(itotv)&
                         &          + rela1_nsi * densi(ipoin,1)
                 end do
              else
                 !call array_operations_axpby(npoin,rela1_nsi,relap_nsi,press(:,1),unkno(ndbgs_nsi+1:),useomp)
                 do ipoin = 1,npoin
                    itotv = ndbgs_nsi+ipoin
                    unkno(itotv) =    relap_nsi*unkno(itotv)&
                         &          + rela1_nsi*press(ipoin,1)
                 end do
              end if
           end if

        else
           !
           ! Monolithic: UNKNO=[ u1 v1 p1 u2 v2 p2 ... ]
           !
           if( kfl_relax_nsi /= 0 ) then
              rela1_nsi = 1.0_rp - relax_nsi
              do ipoin = 1,npoin
                 itotv = (ipoin-1) * solve(1) % ndofn
                 do idime = 1,ndime
                    itotv=itotv+1
                    unkno(itotv) = relax_nsi*unkno(itotv)&
                         &       + rela1_nsi*veloc(idime,ipoin,1)
                 end do
              end do
           end if
           if( kfl_relap_nsi /= 0 ) then
              rela1_nsi = 1.0_rp - relap_nsi
              if( kfl_regim_nsi == 2 ) then
                 do ipoin = 1,npoin
                    itotv=ipoin * solve(1)%ndofn
                    unkno(itotv) = relax_nsi*unkno(itotv)&
                         &       + rela1_nsi*densi(ipoin,1)
                 end do
              else
                 do ipoin = 1,npoin
                    itotv = ipoin * solve(1)%ndofn
                    unkno(itotv) = relax_nsi*unkno(itotv)&
                         &       + rela1_nsi*press(ipoin,1)
                 end do
              end if
           end if

        end if

     case(17_ip)
        !
        ! Update density DENSI
        !
        if( kfl_regim_nsi == 1 ) then
           !
           ! Compressilbe flow: rho=P/(RT)
           !
           !call array_operations_axoz(npoin,1.0_rp/gasco,'/',press(:,1),tempe(:,1),densi(:,1),useomp)
!!$--OMP PARALLEL DO SCHEDULE (STATIC)  
           do ipoin = 1,npoin
              densi(ipoin,1) = press(ipoin,1) / ( gasco * tempe(ipoin,1) )
           end do
           !$--OMP END PARALLEL DO 
        end if

     case(18_ip)
        !
        ! Update density PRESS
        !
        if( kfl_regim_nsi == 2 ) then
           !
           ! Compressilbe flow: P=rho*R*T
           !
           !call array_operations_axoz(npoin,gasco,'*',densi(:,1),tempe(:,1),press(:,1),useomp)
           !$--OMP PARALLEL DO SCHEDULE (STATIC)  
           do ipoin = 1,npoin
              press(ipoin,1) = densi(ipoin,1) * gasco * tempe(ipoin,1)
           end do
!!$--OMP END PARALLEL DO 
        end if

     case(20_ip)
        !
        ! Hydrostatic pressure  
        !
        !call array_operations_copy(npoin,press(:,nprev_nsi),unkno,useomp)
        do ipoin = 1,npoin
           unkno(ipoin) = press(ipoin,nprev_nsi)
        end do

     case(21_ip)

        call runend('NSI_UPDUNK: OBSOLETE')
        !if( kfl_nohyd_nsi /= 0 ) then
        !   !
        !   ! Save initial hydrostatic pressure to compute total pressure when needed (output)
        !   !
        !   !call array_operations_copy(npoin,unkno,bpess_nsi(:,1),useomp)
        !   do ipoin = 1,npoin
        !      bpess_nsi(1,ipoin) = unkno(ipoin)
        !   end do
        !else
        !   !
        !   ! Initial pressure is the hydrostatic one (used to start with good initial guess)
        !   !
        !   !call array_operations_copy(npoin,unkno,press(:,nprev_nsi),useomp)
        !   do ipoin = 1,npoin
        !      press(ipoin,nprev_nsi) = unkno(ipoin)
        !   end do
        !end if

     case(22_ip)
        !
        ! UNKNO <= VELOC(:,:,NPREV_NSI)
        !
        if( NSI_MONOLITHIC ) then
           !
           ! Monolithic: UNKNO=[ u1 v1 p1 u2 v2 p2 ... ]
           !
           if( kfl_regim_nsi == 2 ) then
              do ipoin = 1,npoin
                 itotv = (ipoin-1) * solve(1) % ndofn
                 do idime = 1,ndime
                    itotv = itotv+1
                    unkno(itotv) = veloc(idime,ipoin,nprev_nsi)
                 end do
                 unkno(itotv+1) = densi(ipoin,nprev_nsi)
              end do
           else
              do ipoin = 1,npoin
                 itotv = (ipoin-1) * solve(1) % ndofn
                 do idime = 1,ndime
                    itotv = itotv+1
                    unkno(itotv) = veloc(idime,ipoin,nprev_nsi)
                 end do
                 unkno(itotv+1) = press(ipoin,nprev_nsi)
              end do
           end if

        else
           !
           ! Schur complement: UNKNO=[ u1 v1 u2 v2 ... p1 p2 ... ]
           !
           !call array_operations_copy(ndime,npoin,veloc(:,:,nprev_nsi),unkno,useomp)

           if( kfl_regim_nsi == 2 ) then
              !call array_operations_copy(npoin,densi(:,nprev_nsi),unkno(ndbgs_nsi+1:),useomp)
              ndbgs_nsi = npoin*ndime
              itotv     = 0
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = itotv+1
                    unkno(itotv) = veloc(idime,ipoin,nprev_nsi)
                 end do
                 unkno(ndbgs_nsi+ipoin) = densi(ipoin,nprev_nsi)
              end do
           else
              !call array_operations_copy(npoin,press(:,nprev_nsi),unkno(ndbgs_nsi+1:),useomp)
              ndbgs_nsi = npoin*ndime
              itotv     = 0
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = itotv + 1
                    unkno(itotv) = veloc(idime,ipoin,nprev_nsi)
                 end do
                 unkno(ndbgs_nsi+ipoin) = press(ipoin,nprev_nsi)
              end do
           end if

        end if

     case(23_ip)
        !
        ! VELOC(:,:,NPREV_NSI) <= UNKNO
        !
        if( .not. NSI_MONOLITHIC ) then
           !
           ! Schur complement: UNKNO=[ u1 v1 u2 v2 ... p1 p2 ... ]
           !
           !call array_operations_copy(ndime,npoin,unkno,veloc(:,:,nprev_nsi),useomp)

           if( kfl_regim_nsi == 2 ) then
              !call array_operations_copy(npoin,unkno(ndbgs_nsi+1:),densi(:,nprev_nsi),useomp)
              ndbgs_nsi = npoin*ndime
              itotv = 0
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = itotv+1
                    veloc(idime,ipoin,nprev_nsi) = unkno(itotv)
                 end do
                 densi(ipoin,nprev_nsi) = unkno(ndbgs_nsi+ipoin)
              end do
           else
              !call array_operations_copy(npoin,unkno(ndbgs_nsi+1:),press(:,nprev_nsi),useomp)
              ndbgs_nsi = npoin*ndime
              itotv = 0
              do ipoin = 1,npoin
                 do idime = 1,ndime
                    itotv = itotv+1
                    veloc(idime,ipoin,nprev_nsi) = unkno(itotv)
                 end do
                 press(ipoin,nprev_nsi) = unkno(ndbgs_nsi+ipoin)
              end do
           end if

        else
           !
           ! Monolithic: UNKNO=[ u1 v1 p1 u2 v2 p2 ... ]
           !
           if( kfl_regim_nsi == 2 ) then
              do ipoin = 1,npoin
                 itotv = (ipoin-1) * solve(1) % ndofn
                 do idime = 1,ndime
                    itotv = itotv + 1
                    veloc(idime,ipoin,nprev_nsi) = unkno(itotv)
                 end do
                 densi(ipoin,nprev_nsi) = unkno(itotv+1)
              end do
           else
              do ipoin = 1,npoin
                 itotv = (ipoin-1) * solve(1) % ndofn
                 do idime = 1,ndime
                    itotv = itotv + 1
                    veloc(idime,ipoin,nprev_nsi) = unkno(itotv)
                 end do
                 press(ipoin,nprev_nsi) = unkno(itotv+1)
              end do
           end if

        end if

     end select

  else  ! the RB variables must also be updated by the master

     select case (itask)

     case(5_ip)

        if ( ( coupling('ALEFOR','NASTIN') >= 1 ) .and. ( nrbod /= 0 ) ) then   ! rigid body
           do iimbo = 1,nrbod
              vpfor         => rbbou(iimbo) % vpfor
              vptor         => rbbou(iimbo) % vptor
              vpfor(1:3,4)  =  vpfor(1:3,3)
              vpfor(1:3,3)  =  vpfor(1:3,1)
              vptor(1:3,4)  =  vptor(1:3,3)
              vptor(1:3,3)  =  vptor(1:3,1)
           end do
        end if

        !     case(11_ip)
        !        !
        !        ! Assign xx(n,i,*)  <-- xx(n-1,*,*), initial guess after reading restart
        !        !        xx'(n,i,*) <-- xx'(n-1,*,*)
        !


     case(14_ip)  
        !
        ! Assign xx(n,i-1,*) <-- xx(n,i,*) for rigid body variables
        !
        if ( ( coupling('ALEFOR','NASTIN') >= 1 ) .and. ( nrbod /= 0 ) ) then   ! rigid body
           do iimbo = 1,nrbod
              vpfor         => rbbou(iimbo) % vpfor
              vptor         => rbbou(iimbo) % vptor
              vpfor(1:3,2)  =  vpfor(1:3,1)
              vptor(1:3,2)  =  vptor(1:3,1)
           end do
        end if

     end select

  end if

end subroutine nsi_updunk

