!------------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_iniunk.f90
!> @author  Guillaume Houzeaux
!> @brief   Initial value of velocity and pressurfe
!> @details Set up the initial condition for velocity and pressure.
!>          If this is a restart, initial condition are read from files.
!>          The different options for the velocity are:
!>          \verbatim
!>          Velocity
!>          --------
!>          KFL_INITI_NSI = 0 ... Values from CODES fields
!>                        = 1 ... Constant value
!>                        = 2 ... Constant value in inertial frame of reference
!>                        = 3 ... Stokes flow (Solve a PDE)
!>                        = 4 ... Potential flow (Solve a PDE)
!>                        = 5 ... Laplacian (Solve a PDE)
!>                        = 6 ... Bulk Poiseuille distribution
!>                        = 7 ... Boundary Poiseuille distribution
!>
!>          Pressure
!>          --------
!>          KFL_INIPR_NSI = 0 ... Hydrostatic pressure if coupling with LEVELS
!>                        = 1 ... Solve Laplace equation (Solve a PDE)
!>
!>          Other
!>          -----
!>          KFL_INICO_NSI = 1 ... After imposing initial condition,
!>                                solve for one Richardson iteration
!>                                preconditioned by a coarse problem
!>          \endverbatim
!>          Values are stored in position:
!>          \verbatim
!>          VELOC(1:NDIME,1:NPOIN,NPREV_NSI) 
!>          PRESS(1:NPOIN,NPREV_NSI) 
!>          \endverbatim
!> @} 
!------------------------------------------------------------------------
subroutine nsi_iniunk()
  use def_parame 
  use def_master
  use def_elmtyp
  use def_kermod
  use def_domain
  use def_nastin 
  use mod_chktyp,           only : check_type
  use mod_communications,   only : PAR_MAX
  use mod_array_operations, only : array_operations_copy
  use mod_nsi_hydrostatic,  only : nsi_hydrostatic_pressure
  use mod_ker_space_time_function
  implicit none
  integer(4)  :: istat
  integer(ip) :: ipoin,ipoin1,idime,inodb,iboun,ibopo,kfl_weaun_nsi,izone
  integer(ip) :: kfl_value,kpoin,ifunc, nptot,i
  real(rp)    :: Rdist,dummr(3),rot_matrix(ndime,ndime)
  real(rp)    :: vefun(3)
  real(rp)    :: vx, vy
  real(rp),  allocatable   :: velx(:),vely(:)

  !-------------------------------------------------------------------
  !
  ! For time & space BC from file: get boundary nodes
  !
  !-------------------------------------------------------------------
  if (kfl_bnods_nsi == 1) then
    if( INOTMASTER ) then
      iboun_nsi = 0
      ibopo = 0
      do ipoin = 1,npoin
        do iboun = 1,nbnod_nsi
          if ( abs(coord(1,ipoin) - bntab_nsi(iboun,1)) < 1e-6 .and. &
               abs(coord(2,ipoin) - bntab_nsi(iboun,2)) < 1e-6 .and. &
               abs(coord(3,ipoin) - bntab_nsi(iboun,3)) < 1e-6 ) then
             iboun_nsi(ipoin) = iboun
             ibopo = ibopo + 1
          end if
        end do
      end do
      if (ibopo /= 0) write(*,*) 'total number of boundary nodes: ', nbnod_nsi, '. computed by processor ', kfl_paral,': ', ibopo
    end if
  end if
   
  izone = lzone(ID_NASTIN)

  if( kfl_rstar == 0 ) then  
     !
     ! Info and latex files
     !
     call nsi_outinf(1_ip)
     call nsi_outlat(1_ip)
     !
     ! Cheak if there are weak no-penetration conditions
     !
     kfl_weaun_nsi = 0
     if( INOTMASTER ) then
        loop_iboun: do iboun = 1,nboun
           if( kfl_fixbo_nsi(iboun) == 18 ) then
              kfl_weaun_nsi = 1
              exit loop_iboun
           end if
        end do loop_iboun
     end if
     call PAR_MAX(kfl_weaun_nsi,'IN MY CODE')
     if( kfl_weaun_nsi /= 0 ) kfl_local_nsi = max(1_ip,kfl_local_nsi)

     !-------------------------------------------------------------------
     !
     ! Non-PDE-based initial solution in global system
     !
     !-------------------------------------------------------------------

     if( INOTMASTER ) then

        if( kfl_exacs_nsi /= 0 ) then
           !
           ! Exact solution
           !
           call nsi_exaerr(5_ip)

        else if( kfl_initi_nsi < 0 ) then
           !
           ! Take initial condition from a value function field
           ! 
           kfl_value = -kfl_initi_nsi
           call check_type(bvcod,kfl_value,ndime,npoin) ! Check if value function exist 
           call array_operations_copy(ndime,npoin,bvcod(kfl_value) % a,veloc(:,:,nprev_nsi))                         
           !do ipoin = 1,npoin
           !    do idime = 1,ndime
           !       veloc(idime,ipoin,nprev_nsi) = bvcod(kfl_value) % a(idime,ipoin)
           !   end do
           !end do

        else if( kfl_initi_nsi == 0 ) then
           !
           ! Take initial conditions as prescribed in codes fields
           !
           call array_operations_copy(ndime,npoin,bvess_nsi(:,:,1),veloc(:,:,nprev_nsi))                         
           !do ipoin = 1,npoin
           !   do idime = 1,ndime
           !      veloc(idime,ipoin,nprev_nsi) = bvess_nsi(idime,ipoin,1)
           !   end do
           !end do

        else if( kfl_initi_nsi == 1 ) then
           !
           ! Constant value prescribed
           !
           do ipoin = 1,npoin
              veloc(1:ndime,ipoin,nprev_nsi) = velin_nsi(1:ndime)
           end do
          
        else if( kfl_initi_nsi == 2 ) then
           !
           ! Constant value prescribed in inertial frame of reference
           !
           call nsi_updfor()
           dummr = velin_nsi
           call rotmat(ndime,-cutim*fvnoa_nsi,fvdia_nsi,rot_matrix)
           call mbvab0(velin_nsi,rot_matrix,dummr,ndime,ndime)
           if( ndime == 2 ) then
              do ipoin = 1,npoin
                 veloc(1,ipoin,nprev_nsi) = velin_nsi(1) + fvela_nsi(3) * coord(2,ipoin)
                 veloc(2,ipoin,nprev_nsi) = velin_nsi(2) - fvela_nsi(3) * coord(1,ipoin)
              end do
           else
              do ipoin = 1,npoin
                 veloc(1,ipoin,nprev_nsi) = velin_nsi(1) + fvela_nsi(3) * coord(2,ipoin) &
                      &                                  - fvela_nsi(2) * coord(3,ipoin) 
                 veloc(2,ipoin,nprev_nsi) = velin_nsi(2) - fvela_nsi(3) * coord(1,ipoin) &
                      &                                  + fvela_nsi(1) * coord(3,ipoin)
                 veloc(3,ipoin,nprev_nsi) = velin_nsi(3) - fvela_nsi(1) * coord(2,ipoin) &
                      &                                  + fvela_nsi(2) * coord(1,ipoin)       
              end do
           end if
           velin_nsi = dummr  
           
        else if(kfl_initi_nsi==6) then
           !
           ! Bulk Poiseuille distribution
           !
           do ipoin = 1,npoin
              Rdist = (coord(1,ipoin)-poise_nsi(4))**2 + (coord(2,ipoin)-poise_nsi(5))**2 
              if (ndime == 3) Rdist = Rdist + (coord(3,ipoin)-poise_nsi(6))**2
              do idime = 1,ndime
                 if (int(poise_nsi(1))==idime) then  ! This dim is the axis of flow
                    Rdist = Rdist - (coord(idime,ipoin)-poise_nsi(3+idime))**2  ! Only use distance to the axis of flow
                    Rdist = min( Rdist, poise_nsi(2)**2 ) ! Zero out veloxity outside the radius of influence
                    veloc(idime,ipoin,nprev_nsi) = poise_nsi(3)*(1.0_rp - Rdist/poise_nsi(2)**2 )
                 else
                    veloc(idime,ipoin,nprev_nsi) = 0.0_rp
                 endif
                 if(kfl_fixno_nsi(idime,ipoin) == 16 ) then ! We modify the inlet boundary condition to the same distribution
                    bvess_nsi(idime,ipoin,1) = veloc(idime,ipoin,nprev_nsi)
                    kfl_fixno_nsi(idime,ipoin) = 1 ! And we make it a Dirichlet condition from now on
                 end if
              end do              
           enddo
           
        else if(kfl_initi_nsi==7) then
           !
           ! Boundary Poiseuille distribution, Bulk is constant, hopefully loaded in velin_nsi
           !
           do ipoin = 1,npoin
              do idime = 1,ndime
                 if(kfl_fixno_nsi(idime,ipoin) == 16 ) then ! We modify the inlet boundary condition to the Poiseuille distribution
                    Rdist = (coord(1,ipoin)-poise_nsi(4))**2 + (coord(2,ipoin)-poise_nsi(5))**2 
                    if (ndime == 3) Rdist = Rdist + (coord(3,ipoin)-poise_nsi(6))**2
                    if (int(poise_nsi(1))==idime) then  ! This dim is the axis of flow
                       Rdist = Rdist - (coord(idime,ipoin)-poise_nsi(3+idime))**2  ! Only use distance to the axis of flow
                       Rdist = min( Rdist, poise_nsi(2)**2 ) ! Zero out veloxity outside the radius of influence
                       veloc(idime,ipoin,nprev_nsi) = poise_nsi(3)*(1.0_rp - Rdist/poise_nsi(2)**2 )
                    else
                       veloc(idime,ipoin,nprev_nsi) = 0.0_rp
                    endif
                    bvess_nsi(idime,ipoin,1) = veloc(idime,ipoin,nprev_nsi)
                    kfl_fixno_nsi(idime,ipoin) = 1 ! And we make it a Dirichlet condition from now on                
                 else
                    veloc(idime,ipoin,nprev_nsi) = velin_nsi(idime)
                 end if
              end do
           end do

        else if(kfl_initi_nsi==8) then

 !          nptot = 1031829 
		open(90,file='vel.dat')
                read (90,*), nptot

	   allocate(velx(nptot),stat=istat)
	   allocate(vely(nptot),stat=istat)
           if(kfl_paral==1) print*,' kfl_paral ', kfl_paral,' npoin_total ', nptot

		do i=1,nptot
		   read (90,*) vx, vy
		   velx(i)=vx
		   vely(i)=vy
		enddo
		close(90)

		do ipoin=1,npoin
		   ipoin1 = lninv_loc(ipoin)
		   veloc(1,ipoin,nprev_nsi)=velx(ipoin1)  
		   veloc(2,ipoin,nprev_nsi)=vely(ipoin1)  
		enddo

		deallocate(velx,vely)

        else if(kfl_initi_nsi>100) then
           !
           ! Space time function
           !
           ifunc = kfl_initi_nsi - 100
           do ipoin = 1,npoin
              call ker_space_time_function(&
                   ifunc,coord(1,ipoin),coord(2,ipoin),coord(ndime,ipoin),cutim,vefun(1:ndime))
              do idime = 1,ndime
                 veloc(idime,ipoin,nprev_nsi) = vefun(idime) 
              end do
           end do
        end if
        !
        ! Functions and specific boundary conditions
        !
        call nsi_updbcs(-1_ip)
        !
        ! Bemol
        !
        if( bemol_nsi > 0.0_rp ) then
           if( kfl_local_nsi == 0 ) kfl_local_nsi = -1
           do iboun = 1,nboun
              if(kfl_fixbo_nsi(iboun) == 13 ) then
                 do inodb = 1,nnode(ltypb(iboun))
                    ipoin = lnodb(inodb,iboun)
                    ibopo = lpoty(ipoin)
                    if( ibopo > 0 ) then
                       if( kfl_fixrs_nsi(ibopo) == 0 ) then 
                          kfl_fixrs_nsi(ibopo)   = -4
                          kfl_fixno_nsi(1,ipoin) =  1
                       end if
                    end if
                 end do
              end if
           end do
        end if
        !
        ! Weak imposition of no-penetration:
        ! If there is a boundary condition KFL_FIXBO_NSI(IBOUN)=18:
        ! - Set KFL_WEAUN_NSI = 1:
        ! - Create a temporal local basis with KFL_FIXRS_NSI(IBOPO)=-1 in  
        !   order to project the initial condition such that u.n = 0        
        !
        if( kfl_weaun_nsi /= 0 ) then
           call memgen(1_ip,npoin,0_ip)
           do iboun = 1,nboun
              if( kfl_fixbo_nsi(iboun) == 18 ) then
                 do inodb = 1,nnode(abs(ltypb(iboun)))
                    ipoin = lnodb(inodb,iboun)
                    gisca(ipoin) = 1
                 end do
              end if
           end do
           call parari('SLX',NPOIN_TYPE,npoin,gisca)
           do ipoin = 1,npoin
              gisca(ipoin) = min(1_ip,gisca(ipoin))
              ibopo = lpoty(ipoin)
              if( ibopo > 0 ) then
                 if( kfl_fixrs_nsi(ibopo) == 0 .and. gisca(ipoin) == 1 ) then
                    kfl_fixrs_nsi(ibopo) = -1
                    if( kfl_fixno_nsi(1,ipoin) == 0 ) then
                       kfl_fixno_nsi(1,ipoin) = 10
                       bvess_nsi(1,ipoin,1)   = 0.0_rp
                    end if
                    gisca(ipoin) = 2
                 end if
              end if
           end do
        end if
        !
        ! Impose boundary/initial condition values
        !
        call nsi_rotunk(3_ip,veloc(1,1,nprev_nsi)) ! Global to local
        do ipoin = 1,npoin
           do idime = 1,ndime
              if(       kfl_fixno_nsi(idime,ipoin) ==  1 &
                   .or. kfl_fixno_nsi(idime,ipoin) ==  8 &
                   .or. kfl_fixno_nsi(idime,ipoin) ==  9 &
                   .or. kfl_fixno_nsi(idime,ipoin) ==  5 &
                   .or. kfl_fixno_nsi(idime,ipoin) ==  6 &
                   .or. kfl_fixno_nsi(idime,ipoin) == 10 ) then
                 veloc(idime,ipoin,nprev_nsi) = bvess_nsi(idime,ipoin,1)
              end if
           end do
        end do
        call nsi_rotunk(4_ip,veloc(1,1,nprev_nsi)) ! Local to global
        !
        ! Pressure initial condition
        !
        if( kfl_exacs_nsi == 0 ) then
           if( kfl_regim_nsi == 0 .or. kfl_regim_nsi == 3 ) then
              do ipoin = 1,npoin
                 press(ipoin,nprev_nsi) = valpr_nsi
              end do
           else if( kfl_regim_nsi == 1  ) then
              do ipoin = 1,npoin
                 press(ipoin,nprev_nsi) = prthe(1)
              end do
           else if( kfl_regim_nsi == 2 ) then
              call runend('NOT CODED')
              !do ipoin = 1,npoin
              !   densi(ipoin,nprev_nsi) = 
              !end do
           end if
        end if
        !
        ! Weak no-penetration: Recover KFL_FIXRS_NSI and BVESS_NSI
        !
        if( kfl_weaun_nsi /= 0 ) then          
           do ipoin = 1,npoin
              ibopo = lpoty(ipoin)
              if( gisca(ipoin) == 2 ) then
                 kfl_fixrs_nsi(ibopo) = 0
                 if( kfl_fixno_nsi(1,ipoin) == 10 ) kfl_fixno_nsi(1,ipoin) = 0
              end if
           end do
           call memgen(3_ip,npoin,0_ip)
        end if
        !
        ! Bemol
        !
        if( bemol_nsi > 0.0_rp ) then
           if( kfl_local_nsi == -1 ) kfl_local_nsi = 0
           do iboun = 1,nboun
              if(kfl_fixbo_nsi(iboun) == 13 ) then
                 do inodb = 1,nnode(ltypb(iboun))
                    ipoin = lnodb(inodb,iboun)
                    ibopo = lpoty(ipoin)
                    if( ibopo > 0 ) then
                       if( kfl_fixrs_nsi(ibopo) == -4 ) then 
                          kfl_fixrs_nsi(ibopo)   = 0
                          kfl_fixno_nsi(1,ipoin) = 0
                       end if
                    end if
                 end do
              end if
           end do
        end if

     end if

     !-------------------------------------------------------------------
     !
     ! Initial solution: solve coarse grid
     !
     !-------------------------------------------------------------------

     if( kfl_inico_nsi == 1 ) then
        call nsi_inicoa() 
     end if

     !-------------------------------------------------------------------
     !
     ! Initial pressure: Laplacian pressure 
     !
     !-------------------------------------------------------------------

     if( kfl_inipr_nsi == 1 ) then
        call nsi_inipre()
     end if

     !-------------------------------------------------------------------
     !
     ! Compute hydrostatic pressure if required
     !
     !-------------------------------------------------------------------

     call nsi_hydrostatic_pressure(ITASK_INIUNK)

     !-------------------------------------------------------------------
     !
     ! First guess based on PDE: Stokes, potential, Laplacian
     !
     !------------------------------------------------------------------- 

     if( kfl_initi_nsi == 3 ) then
        call nsi_stokes()                 ! Initial solution is Stokes flow
     else if( kfl_initi_nsi == 4 ) then
        call nsi_potent()                 ! Initial solution is potential flow
     else if( kfl_initi_nsi == 5 ) then
        call nsi_chkbcs()                 ! Initial solution is from Laplacian
     end if

  else
     !
     ! Read restart file
     !
     call nsi_restar(1_ip)
    
     avtim_nsi = cutim ! Accumulated time for time-averaging variables     
  end if 

  !-------------------------------------------------------------------
  !
  ! Initial computations
  !
  !-------------------------------------------------------------------
  
  call nsi_updunk(11_ip)                  ! VELOC(:,:,1) <= VELOC(:,:,nprev_nsi)
  call nsi_inivar( 2_ip)

  !-------------------------------------------------------------------
  !
  ! If pressure matrix comes from a Schur complement
  ! Must be here because amatr must be allocated
  !
  !-------------------------------------------------------------------

  if( NSI_SCHUR_COMPLEMENT ) then
     solve(2) % A1       => amatr(poapp_nsi:)
     solve(2) % A2       => amatr(poapu_nsi:)
     solve(2) % A3       => amatr(poauu_nsi:)
     solve(2) % A4       => amatr(poaup_nsi:)
     solve(2) % ndofn_A3 =  ndime
     nullify(solve(2) % invA3)
  end if

  !-------------------------------------------------------------------
  !
  ! Wall law with exchange location
  !
  !-------------------------------------------------------------------
  call nsi_waexlo()

  !-------------------------------------------------------------------
  !
  ! Warnings and errors
  !
  !-------------------------------------------------------------------

  call nsi_outerr(2_ip)

  !-------------------------------------------------------------------
  !
  ! Interpolation from coarse to fine mesh
  !
  !-------------------------------------------------------------------

  if(kfl_meshi_nsi /= 0_ip) call nsi_coarfine(1_ip)

 call nsi_coupli(ITASK_INIUNK)

end subroutine nsi_iniunk
