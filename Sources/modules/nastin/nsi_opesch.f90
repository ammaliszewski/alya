
subroutine nsi_aupvec(itask,Aup,pp,uu)
  !
  ! uu = Aup pp
  !
  use def_kintyp
  use def_domain
  use def_master
  use mod_solver, only : solver_smvp
  implicit none
  integer(ip), intent(in)  :: itask
  real(rp),    intent(in)  :: Aup(ndime,nzdom),pp(npoin)
  real(rp),    intent(out) :: uu(ndime,npoin)
  integer(ip)              :: izdom,ipoin,jpoin

  if ( INOTMASTER ) then

     if( itask == 1 ) then
        do ipoin = 1,npoin
           uu(1:ndime,ipoin) = 0.0_rp 
        end do
     end if 

     if( itask == 2 ) then
        
        !call solver_matrix_vector_product(npoin,ndime,1_ip,Aup,c_dom,r_dom,pp,uu) 

        !$OMP PARALLEL DO SCHEDULE (DYNAMIC)               &
        !$OMP DEFAULT  ( NONE )                            &
        !$OMP PRIVATE  ( ipoin, izdom, jpoin )             &           
        !$OMP SHARED   ( r_dom, c_dom, uu, Aup, pp, ndime, &
        !$OMP            npoin ) 
        do ipoin = 1,npoin
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1       
              jpoin = c_dom(izdom)     
              uu(1:ndime,ipoin) = uu(1:ndime,ipoin) - Aup(1:ndime,izdom) * pp(jpoin) 
           end do
        end do
        !$OMP END PARALLEL DO
     else
        !$OMP PARALLEL DO SCHEDULE (DYNAMIC)               &
        !$OMP DEFAULT  ( NONE )                            &
        !$OMP PRIVATE  ( ipoin, izdom, jpoin )             &           
        !$OMP SHARED   ( r_dom, c_dom, uu, Aup, pp, ndime, &
        !$OMP            npoin ) 
        do ipoin = 1,npoin
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1       
              jpoin = c_dom(izdom)
              uu(1:ndime,ipoin) = uu(1:ndime,ipoin) + Aup(1:ndime,izdom) * pp(jpoin) 
           end do
        end do
        !$OMP END PARALLEL DO
     end if

  end if

end subroutine nsi_aupvec

subroutine nsi_apuvec(itask,Apu,uu,pp)
  !
  ! ITASK = 0 ... pp = pp + Apu uu
  !         1 ... pp =      Apu uu
  !         2 ... pp = pp - Apu uu
  !         
  !
  use def_kintyp
  use def_domain
  use def_master
  implicit none
  integer(ip), intent(in)  :: itask
  real(rp),    intent(in)  :: Apu(ndime,nzdom),uu(ndime,npoin)
  real(rp),    intent(out) :: pp(npoin)
  integer(ip)              :: idime,izdom,ipoin,jpoin

  if ( INOTMASTER ) then

     if( itask == 1 ) then
        do ipoin = 1,npoin
           pp(ipoin) = 0.0_rp     
        end do
     end if

     if( itask == 2 ) then
        !$OMP PARALLEL DO SCHEDULE (DYNAMIC)               &
        !$OMP DEFAULT  ( NONE )                            &
        !$OMP PRIVATE  ( ipoin, izdom, jpoin, idime )      &           
        !$OMP SHARED   ( r_dom, c_dom, uu, Apu, pp, ndime, &
        !$OMP            npoin ) 
        do ipoin = 1,npoin
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1       
              jpoin     = c_dom(izdom)    
              do idime = 1,ndime
                 pp(ipoin) = pp(ipoin) - Apu(idime,izdom) * uu(idime,jpoin) 
              end do
           end do
        end do
        !$OMP END PARALLEL DO 
     else
        !$OMP PARALLEL DO SCHEDULE (DYNAMIC)               &
        !$OMP DEFAULT  ( NONE )                            &
        !$OMP PRIVATE  ( ipoin, izdom, jpoin, idime )      &           
        !$OMP SHARED   ( r_dom, c_dom, uu, Apu, pp, ndime, &
        !$OMP            npoin ) 
        do ipoin = 1,npoin
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1       
              jpoin = c_dom(izdom)  
              do idime = 1,ndime   
                 pp(ipoin) = pp(ipoin) + Apu(idime,izdom) * uu(idime,jpoin)      
              end do
           end do
        end do
        !$OMP END PARALLEL DO 
     end if

  end if

end subroutine nsi_apuvec

subroutine nsi_appvec(itask,App,pp,qq)
  !
  ! qq = App pp
  !
  use def_kintyp
  use def_domain
  use def_nastin
  use def_master
  implicit none
  integer(ip), intent(in)          :: itask
  real(rp),    intent(in)          :: App(*)
  real(rp),    intent(in)          :: pp(npoin)
  real(rp),    intent(out), target :: qq(npoin)
  integer(ip)                      :: kk,ipoin,jpoin,izsym,izdom
  real(rp)                         :: raux,raux2

  if ( INOTMASTER ) then

     if( itask == 1 ) then
        do ipoin = 1,npoin
           qq(ipoin) = 0.0_rp     
        end do
     end if

     if( itask == 2 ) then
        if( solve(2) % kfl_symme == 1 ) then
           do ipoin = 1, npoin
              raux  = 0.0_rp
              raux2 = pp(ipoin)
              do izsym= r_sym(ipoin), r_sym(ipoin+1)-2
                 jpoin     = c_sym(izsym)
                 raux      = raux + App(izsym) * pp(jpoin)
                 qq(jpoin) = qq(jpoin) - App(izsym) * raux2
              end do
              kk        = r_sym(ipoin+1)-1
              qq(ipoin) = qq(ipoin) - raux - App(kk) * pp(c_sym(kk))
           end do
        else
           !$OMP PARALLEL DO SCHEDULE (DYNAMIC)              &
           !$OMP DEFAULT  ( NONE )                           &
           !$OMP PRIVATE  ( ipoin, izdom, jpoin )            &           
           !$OMP SHARED   ( r_dom, c_dom, App, pp, qq,       &
           !$OMP            npoin  ) 
           do ipoin = 1,npoin
              do izdom = r_dom(ipoin),r_dom(ipoin+1)-1    
                 qq(ipoin) = qq(ipoin) - App(izdom) * pp(c_dom(izdom))
              end do
           end do
           !$OMP END PARALLEL DO
        end if
     else
        if( solve(2) % kfl_symme == 1 ) then
           do ipoin = 1,npoin
              raux  = 0.0_rp
              raux2 = pp(ipoin)
              do izsym= r_sym(ipoin), r_sym(ipoin+1)-2
                 jpoin     = c_sym(izsym)
                 raux      = raux + App(izsym) * pp(jpoin)
                 qq(jpoin) = qq(jpoin) + App(izsym) * raux2
              end do
              kk        = r_sym(ipoin+1)-1
              qq(ipoin) = qq(ipoin) + raux + App(kk) * pp(c_sym(kk))
           end do
        else
           !$OMP PARALLEL DO SCHEDULE (DYNAMIC)              &
           !$OMP DEFAULT  ( NONE )                           &
           !$OMP PRIVATE  ( ipoin, izdom, jpoin )            &           
           !$OMP SHARED   ( r_dom, c_dom, App, pp, qq ,      &
           !$OMP            npoin ) 
           do ipoin = 1,npoin
              do izdom = r_dom(ipoin),r_dom(ipoin+1)-1    
                 qq(ipoin) = qq(ipoin) + App(izdom) * pp(c_dom(izdom))
              end do
           end do
           !$OMP END PARALLEL DO
        end if
     end if

  end if

end subroutine nsi_appvec

subroutine nsi_auuvec(itask,Auu,vv,uu)
  !
  ! uu = Auu vv
  !
  use def_kintyp
  use def_domain
  use def_master
  implicit none
  integer(ip), intent(in)  :: itask
  real(rp),    intent(in)  :: Auu(ndime,ndime,nzdom)
  real(rp),    intent(in)  :: vv(ndime,npoin)
  real(rp),    intent(out) :: uu(ndime,npoin)
  integer(ip)              :: idime,izdom,ipoin,jpoin,jdime

  if ( INOTMASTER ) then

     if( itask == 1 ) then
        do ipoin = 1,npoin
           uu(1:ndime,ipoin) = 0.0_rp     
        end do
     end if

     if( itask == 2 ) then
        !$OMP PARALLEL DO SCHEDULE (DYNAMIC)                 &
        !$OMP DEFAULT  ( NONE )                              &
        !$OMP PRIVATE  ( ipoin, izdom, jpoin, idime, jdime ) &           
        !$OMP SHARED   ( r_dom, c_dom, Auu, uu, vv, ndime  , &
        !$OMP            npoin )  
        do ipoin = 1,npoin
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1       
              jpoin = c_dom(izdom)
              do idime = 1,ndime
                 do jdime = 1,ndime
                    uu(idime,ipoin) = uu(idime,ipoin) - Auu(jdime,idime,izdom) * vv(jdime,jpoin)      
                 end do
              end do
           end do
        end do
        !$OMP END PARALLEL DO
     else
        !$OMP PARALLEL DO SCHEDULE (DYNAMIC)                 &
        !$OMP DEFAULT  ( NONE )                              &
        !$OMP PRIVATE  ( ipoin, izdom, jpoin, idime, jdime ) &           
        !$OMP SHARED   ( r_dom, c_dom, Auu, uu, vv, ndime ,  &
        !$OMP            npoin ) 
        do ipoin = 1,npoin
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1       
              jpoin = c_dom(izdom)
              do idime = 1,ndime
                 do jdime = 1,ndime
                    uu(idime,ipoin) = uu(idime,ipoin) + Auu(jdime,idime,izdom) * vv(jdime,jpoin)      
                 end do
              end do
           end do
        end do
        !$OMP END PARALLEL DO
     end if

  end if

end subroutine nsi_auuvec

subroutine nsi_solini(itask)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_solini
  ! NAME 
  !    nsi_solini
  ! DESCRIPTION
  !    This routine loads the solver data for the incomp. NS equations.
  !    In general, it may change from time step to time step or even
  !    from iteration to iteration.
  ! USED BY
  !    nsi_begite
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use def_solver
  implicit none
  integer(ip), intent(in) :: itask

  if( itask == 1 ) then
     !
     ! Momentum
     !
     ivari_nsi            =  1
     solve(1) % ndofn     =  ndime
     solve(1) % ndof2     =  solve(1) % ndofn**2
     solve(1) % nzmat     =  solve(1) % ndof2*nzdom
     solve(1) % nunkn     =  solve(1) % ndofn*npoin
     solve(1) % nzrhs     =  solve(1) % ndofn*npoin
     solve(1) % bvess     => bvess_nsi(:,:,1)
     solve_sol            => solve(1:)

  else if( itask == 2 ) then
     !
     ! Continuity
     !
     ivari_nsi          =  2
     solve_sol          => solve(2:)

  else if( itask == 3 ) then
     !
     ! Momentum + continuity (used for example to initialize matrix)
     !
     ivari_nsi        =  1
     solve(1) % ndofn =  ndime + 1
     solve(1) % ndof2 =  solve(1) % ndofn**2
     solve(1) % nzmat =  nmauu_nsi + nmaup_nsi + nmapu_nsi + nmapp_nsi
     solve(1) % nunkn =  solve(1) % ndofn * npoin
     solve(1) % nzrhs =  solve(1) % ndofn * npoin 
     solve_sol        => solve(1:)

     if( IMASTER ) then
        Auu_nsi => nul1r
        Aup_nsi => nul1r
        Apu_nsi => nul1r
        App_nsi => nul1r
     else
        Auu_nsi => amatr(poauu_nsi:poauu_nsi+nmauu_nsi-1)
        Aup_nsi => amatr(poaup_nsi:poaup_nsi+nmaup_nsi-1)
        Apu_nsi => amatr(poapu_nsi:poapu_nsi+nmapu_nsi-1)
        App_nsi => amatr(poapp_nsi:poapp_nsi+nmapp_nsi-1)
     end if

  else if( itask == 4 ) then
     !
     ! Momentum variation (second solve in Du)
     !
     ivari_nsi            =  1
     solve(1) % ndofn     =  ndime
     solve(1) % ndof2     =  solve(1) % ndofn**2
     solve(1) % nzmat     =  solve(1) % ndof2*nzdom
     solve(1) % nunkn     =  solve(1) % ndofn*npoin
     solve(1) % nzrhs     =  solve(1) % ndofn*npoin
     solve(1) % bvess     => null()
     solve_sol            => solve(1:)

  end if

end subroutine nsi_solini

subroutine nsi_inivec(ncomp,xx)
  !
  ! uu = Aup pp
  !
  use def_kintyp
  use def_domain
  use def_master
  implicit none
  integer(ip), intent(in)  :: ncomp
  real(rp),    intent(out) :: xx(ncomp)
  integer(ip)              :: icomp

  if( INOTMASTER ) then
     do icomp = 1,ncomp
        xx(icomp) = 0.0_rp
     end do
  end if

end subroutine nsi_inivec

subroutine nsi_rotsch(itask,uu)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_rotunk
  ! NAME 
  !    nsi_rotunk
  ! DESCRIPTION
  !    This routine rotates the nodal velocities using the appropiate 
  !    rotation matrix:
  !    ITASK=1 ... From global to local
  !    ITASK=2 ... From local to global
  !    Modifications need to be done only if there exist image nodes or
  !    boundary conditions in skew systems.
  ! USES
  !    mbvab0
  ! USED BY
  !    nsi_solite
  !***
  !-----------------------------------------------------------------------
  use def_domain
  use def_master
  use def_nastin
  implicit none
  integer(ip), intent(in)    :: itask
  real(rp),    intent(inout) :: uu(*)
  integer(ip)                :: ipoin,ibopo,iroty,itotv
  real(rp)                   :: worma(3),worve(3)

  if( kfl_local_nsi /= 0 .and. INOTMASTER ) then 

     if( itask == 1 ) then
        !
        ! Global to local
        !
        do ipoin = 1,npoin
           ibopo = lpoty(ipoin)
           if( ibopo > 0 ) then
              iroty = kfl_fixrs_nsi(ibopo)

              if( iroty == -1 ) then
                 !
                 ! Boundary conditions in the tangent skew system
                 !           
                 itotv=(ipoin-1)*ndime
                 worve(1:ndime)=uu(itotv+1:itotv+ndime)
                 call mbvatb(worma,exnor(1,1,ibopo),worve,ndime,ndime)
                 uu(itotv+1:itotv+ndime)=worma(1:ndime)

              else if( iroty == -2 ) then
                 !
                 ! Boundary conditions in the NSI tangent skew system
                 !           
                 itotv=(ipoin-1)*ndime
                 worve(1:ndime)=uu(itotv+1:itotv+ndime)
                 call mbvatb(worma,skcos_nsi(1,1,ibopo),worve,ndime,ndime)
                 uu(itotv+1:itotv+ndime)=worma(1:ndime)

              else if( iroty == -3 ) then
                 !
                 ! Boundary conditions in geometrical system
                 !           
                 itotv=(ipoin-1)*ndime
                 worve(1:ndime)=uu(itotv+1:itotv+ndime)
                 call mbvatb(worma,skcos(1,1,ibopo),worve,ndime,ndime)
                 uu(itotv+1:itotv+ndime)=worma(1:ndime)

              else if( iroty >= 1 ) then
                 !
                 ! Boundary conditions in a given skew system
                 ! 
                 itotv=(ipoin-1)*ndime
                 worve(1:ndime)=uu(itotv+1:itotv+ndime)
                 call mbvatb(worma,skcos(1,1,iroty),worve,ndime,ndime)
                 uu(itotv+1:itotv+ndime)=worma(1:ndime)

              end if
           end if
        end do

     else if( itask == 2 ) then
        !
        ! Local to global
        !
        do ipoin = 1,npoin
           ibopo = lpoty(ipoin)
           if( ibopo > 0 ) then
              iroty = kfl_fixrs_nsi(ibopo)
              if( iroty == -1 ) then
                 !
                 ! Boundary conditions in the tangent skew system
                 !           
                 itotv=(ipoin-1)*ndime
                 worve(1:ndime)=uu(itotv+1:itotv+ndime)
                 call mbvab0(worma,exnor(1,1,ibopo),worve,ndime,ndime)
                 uu(itotv+1:itotv+ndime)=worma(1:ndime)

              else if( iroty == -2 ) then
                 !
                 ! Boundary conditions in the NSI skew system
                 !           
                 itotv=(ipoin-1)*ndime
                 worve(1:ndime)=uu(itotv+1:itotv+ndime)
                 call mbvab0(worma,skcos_nsi(1,1,ibopo),worve,ndime,ndime)
                 uu(itotv+1:itotv+ndime)=worma(1:ndime)

              else if( iroty == -3 ) then
                 !
                 ! Boundary conditions in geometrical system
                 !           
                 itotv=(ipoin-1)*ndime
                 worve(1:ndime)=uu(itotv+1:itotv+ndime)
                 call mbvab0(worma,skcos(1,1,ibopo),worve,ndime,ndime)
                 uu(itotv+1:itotv+ndime)=worma(1:ndime)

              else if( iroty >= 1 ) then
                 !
                 ! Boundary conditions in a given skew system
                 ! 
                 itotv=(ipoin-1)*ndime
                 worve(1:ndime)=uu(itotv+1:itotv+ndime)
                 call mbvab0(worma,skcos(1,1,iroty),worve,ndime,ndime)
                 uu(itotv+1:itotv+ndime)=worma(1:ndime)
              end if
           end if
        end do

     end if

  end if

end subroutine nsi_rotsch

subroutine nsi_allvec(itask,Auu,Aup,App,Apu,vv,uu)
  !
  ! uu = Auu vv
  !
  use def_kintyp
  use def_domain
  use def_master
  implicit none
  integer(ip), intent(in)  :: itask
  real(rp),    intent(in)  :: Auu(ndime,ndime,nzdom)
  real(rp),    intent(in)  :: Aup(ndime,nzdom)
  real(rp),    intent(in)  :: App(nzdom)
  real(rp),    intent(in)  :: Apu(ndime,nzdom)
  real(rp),    intent(in)  :: vv(ndime+1,npoin)
  real(rp),    intent(out) :: uu(ndime+1,npoin)
  integer(ip)              :: idime,izdom,ipoin,jpoin,jdime,ndofn

  if ( INOTMASTER ) then

     ndofn = ndime + 1

     do ipoin = 1,npoin
        uu(1:ndime,ipoin) = 0.0_rp     
     end do

     !$OMP PARALLEL DO SCHEDULE (DYNAMIC)                 &
     !$OMP DEFAULT  ( NONE )                              &
     !$OMP PRIVATE  ( ipoin, izdom, jpoin, idime, jdime ) &           
     !$OMP SHARED   ( r_dom, c_dom, Auu, Aup, Apu, App,   &
     !$OMP            uu, vv, ndime, ndofn, npoin ) 
     do ipoin = 1,npoin
        do izdom = r_dom(ipoin),r_dom(ipoin+1)-1       
           jpoin = c_dom(izdom)
           do idime = 1,ndime
              do jdime = 1,ndime
                 uu(idime,ipoin) = uu(idime,ipoin) + Auu(jdime,idime,izdom) * vv(jdime,jpoin)      
              end do
              uu(idime,ipoin) = uu(idime,ipoin) + Aup(idime,izdom) * vv(ndofn,jpoin)      
              uu(ndofn,ipoin) = uu(ndofn,ipoin) + Apu(idime,izdom) * vv(idime,jpoin)      
           end do
           uu(ndofn,ipoin) = uu(ndofn,ipoin) + App(izdom) * vv(ndofn,jpoin)      
        end do
     end do
     !$OMP END PARALLEL DO
     
     if( itask == 1 ) call pararr('SLX',NPOIN_TYPE,npoin*ndofn,uu)

  end if

end subroutine nsi_allvec



