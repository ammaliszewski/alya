subroutine nsi_solite()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_solite
  ! NAME 
  !    nsi_solite
  ! DESCRIPTION
  !    This routine solves an iteration for the incompressible
  !    Navier-Stokes equations, using:
  !    - A Monolithic scheme
  !    - A block Gauss-Seidel scheme
  ! USES
  !    nsi_ifconf
  !    nsi_solmon
  !    nsi_solbgs
  !    nsi_rotunk
  ! USED BY
  !    nsi_doiter
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use mod_nsi_solsch, only : nsi_solsch  
  use mod_solver,     only : solver_postprocess
  implicit none
  integer(ip) :: kfl_linea_old
#ifdef outmateocoe
  integer(ip)        :: ipass_aux
#endif

  !----------------------------------------------------------------------
  !
  ! Initialization
  !
  !----------------------------------------------------------------------
  !
  ! Update inner iteration counter and write headings in the solver file.
  !
  itinn(modul) = itinn(modul) + 1
  ittot_nsi    = ittot_nsi + 1
  !
  ! Linearization
  !
  kfl_linea_old = kfl_linea_nsi
  if( itinn(modul) <= npica_nsi ) kfl_linea_nsi = 1
  !
  ! Update boundary conditions
  !
  call nsi_updbcs(3_ip)
  !
  ! Check if flow is confined
  !
  call nsi_ifconf(2_ip)
  !
  ! Update density
  !
  call nsi_updunk(17_ip)
  !
  ! Initialize SGS residual
  !
  call nsi_solsgs(1_ip)

  !----------------------------------------------------------------------
  !
  ! Solve equations 
  !
  !----------------------------------------------------------------------

  if( NSI_MONOLITHIC ) then
     call nsi_inisol(1_ip)
     call nsi_matrix()
#ifdef outmateocoe
     call nsi_matndof(rhsid,unkno,amatr,r_sol,c_sol,ndime+1,npoin,ipass_aux)
#endif
#ifdef solve_w_agmg
     call nsi_agmgsol(rhsid,unkno,amatr,r_sol,c_sol,ndime+1,npoin)
#else
     call solver(rhsid,unkno,amatr,pmatr) 
#endif       
  else
     call nsi_solsch()
  end if
  !
  ! Solver postprocess
  !
  call solver_postprocess(momod(modul) % solve,amatr,rhsid,unkno)
  !
  ! Linearization
  !
  kfl_linea_nsi = kfl_linea_old

end subroutine nsi_solite
