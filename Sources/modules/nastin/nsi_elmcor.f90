!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_elmcor.f90
!> @author  Guillaume Houzeaux
!> @brief   Velocity correction
!> @details Correct velocity in roder to obtain a continuity 
!>          conservation scheme
!>          KFL_PREDI_NSI = 2 ... u = u* - (dt/rho)*[ grad(p^i+1) - grad(p^i) ]
!>                        = 3 ... u = u* - tau'1   *[ grad(p^i+1) - grad(p^i) ]
!>                                with tau' = 1/( rho/dt + 1/tau )
!> @} 
!-----------------------------------------------------------------------

subroutine nsi_elmcor() 
  use def_kintyp,     only : ip,rp
  use def_master,     only : unkno,INOTMASTER,amatr
  use def_master,     only : kfl_timco
  use def_elmtyp,     only : ELHOL
  use def_kermod,     only : kfl_kemod_ker
  use def_domain,     only : mnode,mnoga,ndime
  use def_domain,     only : lnods,ltype,elmar,lelch
  use def_domain,     only : hnatu,ltypb,lnodb
  use def_domain,     only : ngaus,nnode,lorde,nboun
  use def_domain,     only : nmate,npoin,lmate,ntens
  use mod_parall,     only : par_omp_ia_colors,par_omp_ja_colors
  use mod_parall,     only : par_omp_num_colors
  use def_nastin,     only : kfl_fixno_nsi,kfl_fixbo_nsi,bvess_nsi
  use def_nastin,     only : saflo_nsi,safet_nsi,kfl_timei_nsi
  use def_nastin,     only : kfl_stead_nsi,kfl_predi_nsi
  use def_nastin,     only : kfl_grvir_nsi,kfl_ellsh_nsi
  use def_nastin,     only : kfl_cotur_nsi,kfl_corre_nsi
  use def_nastin,     only : kfl_advec_nsi,dtinv_nsi
  use def_nastin,     only : dtcri_nsi,cputi_nsi,ndbgs_nsi
  use mod_nsi_solsch, only : xx
  use mod_ker_proper, only : ker_proper
  implicit none
  integer(ip)       :: ielem,dummi,inodb,iboun           ! Indices and dimensions
  integer(ip)       :: pnode,pgaus,pelty,pmate,porde
  integer(ip)       :: idime,idofn,ipoin,kelem,icolo
  real(rp)          :: time1,time2,dummr, dtcri
  real(rp)          :: elcod(ndime,mnode)                ! x
  real(rp)          :: elvel(ndime,mnode)                ! u
  real(rp)          :: elpre(mnode)                      ! p
  real(rp)          :: elmut(mnode)                      ! mut
  real(rp)          :: eltem(mnode)                      ! T
  real(rp)          :: gpsha(mnode,mnoga)                ! N
  real(rp)          :: gpcar(ndime,mnode,mnoga)          ! dN/dxi
  real(rp)          :: gphes(ntens,mnode,mnoga)          ! d^2N/dxidxj
  real(rp)          :: gpvol(mnoga)                      ! w*|J|, |J|
  real(rp)          :: gpgpr(ndime,mnoga)                ! grad(p)
  real(rp)          :: gppor(mnoga)                      ! sig
  real(rp)          :: gpgvi(ndime,mnoga)                ! grad(mu+mut)
  real(rp)          :: gpgve(9,mnoga)                    ! grad(u)
  real(rp)          :: gpmut(mnoga)                      ! mut
  real(rp)          :: grvis(ndime,mnoga)                ! grad(mut)
  real(rp)          :: gpden(mnoga)                      ! rho
  real(rp)          :: gpvis(mnoga)                      ! mu
  real(rp)          :: chale(3)                          ! Characteristic lengths
  real(rp)          :: hleng(3)                          ! Element lengths (max to min)
  real(rp)          :: chave(3)                          ! Characteristic velocity
  real(rp)          :: dtinv_loc                         ! Time step
  real(rp)          :: tragl(9)
  real(rp), pointer :: my_gpsha(:,:),gpder(:,:,:),gpwei(:)

  if( kfl_corre_nsi == 0 ) return 
  call cputim(time1)

  if ( INOTMASTER ) then

     !-------------------------------------------------------------------
     !
     ! Assemble RHS: \int_V tau1'*[ grad(p^i+1) - grad(p^i) ].v dx
     ! 
     !-------------------------------------------------------------------

     do idofn = 1,ndbgs_nsi
        xx(idofn) = 0.0_rp                               ! Initialization
     end do
     dtinv_loc = dtinv_nsi

     colors: do icolo = 1,par_omp_num_colors   
        ! 
        ! Loop over elements
        !     
        !$OMP  PARALLEL DO SCHEDULE (STATIC)                                        & 
        !$OMP  DEFAULT      ( NONE )                                                &
        !$OMP  FIRSTPRIVATE ( dtinv_loc )                                           &
        !$OMP  PRIVATE      ( kelem, pelty, pnode, porde, gpsha, my_gpsha,          &
        !$OMP                 gpder, gpwei, pgaus, pmate, elcod, elpre, elvel,      &
        !$OMP                 elmut, eltem, gpvol, gpcar, ielem, dummr,             &
        !$OMP                 tragl, hleng, chave, chale, dtcri, gpden, gpvis,      &
        !$OMP                 gppor, gpgvi, grvis, gpgve, gpmut, dummi,             &
        !$OMP                 gpgpr, gphes )                                        &
        !$OMP  SHARED       ( icolo, par_omp_ia_colors, par_omp_ja_colors, ltype,   &
        !$OMP                 kfl_corre_nsi, elmar, ngaus, lmate,                   &
        !$OMP                 lelch, lnods, kfl_predi_nsi, kfl_timco, ndime,        &
        !$OMP                 hnatu, kfl_advec_nsi, kfl_ellsh_nsi, kfl_cotur_nsi,   &
        !$OMP                 kfl_kemod_ker, kfl_grvir_nsi, nmate, safet_nsi,       &
        !$OMP                 dtcri_nsi, kfl_timei_nsi, saflo_nsi,                  &
        !$OMP                 kfl_stead_nsi, nnode, lorde, xx )                                               
        !elements: do kelem = 1,nelez(current_zone)
        !ielem = lelez(current_zone) % l(kelem)

        elements: do kelem = par_omp_ia_colors(icolo),par_omp_ia_colors(icolo+1)-1
           ielem = par_omp_ja_colors(kelem) 
           pelty = ltype(ielem)                             ! Element properties and dimensions
           if( pelty > 0 ) then
              pnode = nnode(pelty)
              porde = lorde(pelty)

              if( kfl_corre_nsi == 1 ) then                 ! Close rule for RHS
                 my_gpsha => elmar(pelty) % shapc
                 gpder    => elmar(pelty) % deric
                 gpwei    => elmar(pelty) % weigc 
                 pgaus    =  nnode(pelty) 
              else                                          ! Open rule for RHS
                 my_gpsha => elmar(pelty) % shape
                 gpder    => elmar(pelty) % deriv
                 gpwei    => elmar(pelty) % weigp
                 pgaus    =  ngaus(pelty)
              end if

              if( nmate > 1 ) then
                 pmate = lmate(ielem)
              else
                 pmate = 1
              end if

              if( pmate /= -1 .and. lelch(ielem) /= ELHOL ) then
                 !
                 ! Gather
                 !
                 call nsi_elmgap(&              
                      pnode,pmate,lnods(1,ielem),elcod,elpre,&
                      elvel,elmut,eltem)
                 !
                 ! GPVOL, GPCAR: Jabobian, Cartesian derivatives
                 ! 
                 call elmca2(&
                      pnode,pgaus,0_ip,gpwei,my_gpsha,&
                      gpder,elmar(pelty)%heslo,elcod,gpvol,gpsha,&
                      gpcar,gphes,ielem)
                 !
                 ! CHALE: characteristic length
                 !
                 if( kfl_predi_nsi == 3 .or. kfl_timco == 2 ) then   
                    call elmlen(&
                         ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&
                         hnatu(pelty),hleng)
                    call elmchl(&
                         tragl,hleng,elcod,elvel,chave,chale,pnode,&
                         porde,hnatu(pelty),kfl_advec_nsi,kfl_ellsh_nsi)
                 end if
                 !
                 ! Local time step: DTINV_LOC
                 ! 
                 if( kfl_timco == 2 ) then 
                    call nsi_elmtss(&
                         pelty,pmate,pnode,lnods(1,ielem),ielem,elcod,elvel,&
                         gpcar,chale,hleng,dtcri)
                    dtinv_loc = min(1.0_rp / (dtcri*safet_nsi), 1.0_rp/(dtcri_nsi*saflo_nsi))
                    if( kfl_stead_nsi == 1 ) dtinv_loc = 0.0_rp
                    if( kfl_timei_nsi == 0 ) dtinv_loc = 0.0_rp
                 end if

                 call ker_proper('DENSI','PGAUS',dummi,ielem,gpden,pnode,pgaus,gpsha,gpcar)
                 call ker_proper('VISCO','PGAUS',dummi,ielem,gpvis,pnode,pgaus,gpsha,gpcar)
                 call ker_proper('POROS','PGAUS',dummi,ielem,gppor,pnode,pgaus,gpsha,gpcar)
                 call ker_proper('GRVIS','PGAUS',dummi,ielem,gpgvi,pnode,pgaus,gpsha,gpcar)
                 
                 call nsi_turbul(&
                      1_ip,0_ip,pnode,pgaus,1_ip,pgaus,kfl_cotur_nsi,&
                      gpsha,gpcar,hleng,elvel,elmut,gpden,gpvis,gpmut,&
                      gpgvi,grvis,gpgve,ielem,kfl_kemod_ker)
                 
                 if( kfl_grvir_nsi == 0 ) gpgvi = 0.0_rp ! zero viscosity gradient

                 call nsi_elmrhc(&                    
                      ielem,pgaus,pnode,lnods(1,ielem),gpvol,gpsha,&
                      gpcar,elvel,chale,gpden,gpvis,gppor,gpgpr,&
                      dtinv_loc,xx)

              end if

           end if

        end do elements
        !$OMP  END PARALLEL DO 

     end do colors

  end if

  !-------------------------------------------------------------------
  !
  ! Do not modify weak form no-penetration
  ! 
  !-------------------------------------------------------------------

  if( INOTMASTER ) then
     do iboun = 1,nboun
        if( kfl_fixbo_nsi(iboun) == 18 ) then
           do inodb = 1,nnode(abs(ltypb(iboun)))
              ipoin = lnodb(inodb,iboun)
              idofn = (ipoin-1)*ndime
              do idime = 1,ndime
                 idofn = idofn + 1
                 !xx(idofn) = 0.0_rp
              end do
           end do
        end if
     end do
  end if

  !-------------------------------------------------------------------
  !
  ! Impose boundary conditions and solve system using diagonal solver
  ! 
  !-------------------------------------------------------------------

  call nsi_inisol(5_ip)
  call solver(xx,unkno,amatr,dummr) 

  if( INOTMASTER ) then
     call nsi_rotsch(1_ip,unkno)                         ! Transform global to local  
     idofn = 0
     do ipoin = 1,npoin                                  ! Prescribe bc 
        do idime = 1,ndime
           idofn = idofn + 1
           if( kfl_fixno_nsi(idime,ipoin) > 0 ) &
                unkno(idofn) = bvess_nsi(idime,ipoin,1)
        end do
     end do
     call nsi_rotsch(2_ip,unkno)                         ! Transform local to global  
  end if

  call cputim(time2)
  cputi_nsi(4) = cputi_nsi(4) + time2 - time1

end subroutine nsi_elmcor
