!-----------------------------------------------------------------------
!> @addtogroup NastinTurnon
!> @{
!> @file    nsi_memall.f90
!> @author  Guillaume Houzeaux
!> @brief   Allocate memory 
!> @details Allocate memory 
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_memall()
  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use def_solver
  use def_nastin
  use mod_memchk
  use mod_memory
  implicit none
  integer(ip) :: ncsgs,ielem,pgaus,pelty,ipoin

  !----------------------------------------------------------------------
  !
  ! Solver
  !
  !----------------------------------------------------------------------
  !
  ! Memory
  !
  solve_sol => solve(1:5)
  call soldef(4_ip)
  !
  ! Boundary conditions
  !
  solve(1) % bvess     => bvess_nsi(:,:,1)   ! Momentum
  solve(1) % kfl_fixno => kfl_fixno_nsi

  if( solve(1) % kfl_iffix /= 0 ) then       ! Velocity correction
     solve(4) % kfl_iffix =  2
     solve(4) % kfl_fixno => kfl_fixno_nsi
     solve(4) % bvess     => null()
  end if

  solve(2) % kfl_fixno => kfl_fixpr_nsi      ! Pressure schur complement
  solve(2) % bvess     => null()
  !
  ! Navier-Stokes is block-wise:
  ! First block:  momentum equations
  ! Second block: continuity equation
  !
  solve(1) % block_array(1) % bvess     => bvess_nsi(:,:,1)
  solve(1) % block_array(1) % kfl_fixno => kfl_fixno_nsi
  solve(1) % block_array(1) % bvnat     => solve(1) % bvnat

  solve(1) % block_array(2) % bvess     => bpess_nsi
  solve(1) % block_array(2) % kfl_fixno => kfl_fixpp_nsi
  solve(1) % block_array(2) % bvnat     => solve(2) % bvnat
  !
  ! Tell the solver that although we have defined
  ! 2 solvers (momentum and continuity), the 
  ! matrix used for these two solvers has a block
  ! structure, owned by the first solver!
  !
  if( NSI_MONOLITHIC ) then
     solve(1) % num_blocks          = 1
     solve(1) % block_dimensions(1) = ndime+1  
  else
     solve(1) % num_blocks          = 2    
     solve(1) % block_dimensions(1) = ndime         
     solve(1) % block_dimensions(2) = 1         
     solve(1) % block_num           = 1   
     solve(2) % block_num           = 2
  end if
  !
  ! Schur complement system: modify matrix and RHIS size
  ! Velocity and pressure are consecutively in UNKNO even
  ! if split scheme is used
  !
  if( INOTMASTER ) then

     if( NSI_SCHUR_COMPLEMENT ) then
        nmauu_nsi =  solve(1) % nzmat
        nmaup_nsi =  solve(1) % nzmat/ndime
        nmapu_nsi =  solve(1) % nzmat/ndime
        nmapp_nsi =  solve(2) % nzmat
        poauu_nsi =  1
        poaup_nsi =  poauu_nsi + nmauu_nsi
        poapu_nsi =  poaup_nsi + nmaup_nsi
        poapp_nsi =  poapu_nsi + nmapu_nsi
        nzmat     =  max(nzmat,nmauu_nsi+nmaup_nsi+nmapu_nsi+nmapp_nsi)
        nzrhs     =  max(nzrhs,(ndime+1_ip)*npoin)
     end if

  else

     nmauu_nsi =  1
     nmaup_nsi =  1
     nmapu_nsi =  1
     nmapp_nsi =  1
     poauu_nsi =  1
     poaup_nsi =  1
     poapu_nsi =  1
     poapp_nsi =  1
     
  end if

  !----------------------------------------------------------------------
  !
  ! Arrays
  !
  !----------------------------------------------------------------------

  if( INOTMASTER ) then
     !
     ! VELOC, PRESS: Allocate memory for velocity and pressure
     ! VELOC(:,:,1) = u^{n,i}
     ! VELOC(:,:,2) = u^{n,i-1}
     ! VELOC(:,:,3) = u^{n-1}
     ! VELOC(:,:,4) = u^{n-2}
     ! VELOC(:,:,5) = u^{n-3}, etc.
     !
     call memory_alloca(mem_modul(1:2,modul),'VELOC','nsi_memall',veloc,ndime,npoin,ncomp_nsi)
     call memory_alloca(mem_modul(1:2,modul),'PRESS','nsi_memall',press,npoin,ncomp_nsi)
     !
     ! DENSI: Compressible regime
     !
     if( kfl_regim_nsi == 1 ) then
        call memory_alloca(mem_modul(1:2,modul),'DENSI','nsi_memall',densi,npoin,1_ip)
     else if( kfl_regim_nsi == 2 ) then
        call memory_alloca(mem_modul(1:2,modul),'DENSI','nsi_memall',densi,npoin,ncomp_nsi)
     end if
     !
     ! UNK2N_NSI, Nastin second variable: PRESS or DENSI
     !
     if( kfl_regim_nsi == 2 ) then
        unk2n_nsi => densi   
     else
        unk2n_nsi => press
     end if
     !
     ! VEOLD_NSI: Allocate memory for old (last iteration) velocity 
     !
     if(kfl_resid_nsi==1) then
        call memory_alloca(mem_modul(1:2,modul),'VEOLD','nsi_memall',veold_nsi,ndime,npoin)
     end if
     !
     ! VEPRO_NSI, PRPRO_NSI, GRPRO_NSI: Projections for orthogonal SGS
     !
     if( kfl_stabi_nsi /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'VEPRO_NSI','nsi_memall',vepro_nsi,ndime,npoin)
        call memory_alloca(mem_modul(1:2,modul),'PRPRO_NSI','nsi_memall',prpro_nsi,npoin)
        if( kfl_stabi_nsi == 2 ) then
           call memory_alloca(mem_modul(1:2,modul),'GRPRO_NSI','nsi_memall',grpro_nsi,ndime,npoin)
        end if
     end if
     !
     ! VESGS: Subgrid scale velocity
     !
     if( kfl_sgsco_nsi == 1 .or. kfl_sgsti_nsi == 1 ) then
        call memory_alloca(mem_modul(1:2,modul),'VESGS','nsi_memall',vesgs,nelem)
        ncsgs=min(2_ip,2_ip*kfl_sgsti_nsi+kfl_sgsco_nsi)
        do ielem=1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           call memory_alloca(mem_modul(1:2,modul),'VESGS(IELEM)','nsi_memall',vesgs(ielem)%a,ndime,pgaus,ncsgs)
        end do
        allocate( itsta_nsi(misgs_nsi) )
        allocate( resis_nsi(2_ip,misgs_nsi) )
        !call memory_alloca(mem_modul(1:2,modul),'ITSTA_NSI','nsi_memall',itsta_nsi,misgs_nsi)
        !call memory_alloca(mem_modul(1:2,modul),'RESIS_NSI','nsi_memall',resis_nsi,2_ip,misgs_nsi)
     else
        allocate( itsta_nsi(1_ip) )
        allocate( resis_nsi(2_ip,1_ip) )
        !call memory_alloca(mem_modul(1:2,modul),'ITSTA_NSI','nsi_memall',itsta_nsi,1_ip)
        !call memory_alloca(mem_modul(1:2,modul),'RESIS_NSI','nsi_memall',resis_nsi,2_ip,1_ip)        
     end if
     !
     ! DUNKN_NSI, DUNKP_NSI: Delta velocity and pressure  for Aitken relaxation strategy
     !
     if(kfl_relax_nsi==2) then
        call memory_alloca(mem_modul(1:2,modul),'DUNKN_NSI','nsi_memall',dunkn_nsi,ndime*npoin)
     end if
     if(kfl_relap_nsi==2) then
        call memory_alloca(mem_modul(1:2,modul),'DUNKN_NSI','nsi_memall',dunkn_nsi,npoin)
     end if
     !
     ! AVVEL_NSI: average velocity (postprocess)
     !
     if(postp(1) % npp_stepi(21)/=0.or.maxval(postp(1) % pos_times(1:nvart,21))>zensi) then
        call memory_alloca(mem_modul(1:2,modul),'AVVEL_NSI','nsi_memall',avvel_nsi,ndime,npoin)
     end if
     !
     ! AVPRE_NSI: average pressure (postprocess)
     !
     if(postp(1) % npp_stepi(28)/=0.or.maxval(postp(1) % pos_times(1:nvart,28))>zensi) then
        call memory_alloca(mem_modul(1:2,modul),'AVPRE_NSI','nsi_memall',avpre_nsi,npoin)
     end if
     !
     ! AVVE2_NSI: average velocity**2 (postprocess)
     !
     if(postp(1) % npp_stepi(57)/=0.or.maxval(postp(1) % pos_times(1:nvart,57))>zensi) then
        call memory_alloca(mem_modul(1:2,modul),'AVVE2_NSI','nsi_memall',avve2_nsi,ndime,npoin)
     end if
     !
     ! AVVXY_NSI: average vx*vy (postprocess)
     !
     if(postp(1) % npp_stepi(58)/=0.or.maxval(postp(1) % pos_times(1:nvart,58))>zensi) then
        call memory_alloca(mem_modul(1:2,modul),'AVVXY_NSI','nsi_memall',avvxy_nsi,ndime,npoin)
     end if
     !
     ! AVPRE_NSI: average pressure (postprocess)
     !
     if(postp(1) % npp_stepi(59)/=0.or.maxval(postp(1) % pos_times(1:nvart,59))>zensi) then
        call memory_alloca(mem_modul(1:2,modul),'AVPR2_NSI','nsi_memall',avpr2_nsi,npoin)
     end if
     !
     ! AVTAN_NSI: average TANGE (postprocess)
     !
     if(postp(1) % npp_stepi(46)/=0.or.maxval(postp(1) % pos_times(1:nvart,46))>zensi) then
        call memory_alloca(mem_modul(1:2,modul),'AVTAN_NSI','nsi_memall',avtan_nsi,ndime,npoin)
     end if
     !
     ! RESCH_NSI: Schur complement residual
     !
     if(postp(1) % npp_stepi(22)/=0.or.maxval(postp(1) % pos_times(1:nvart,22))>zensi) then
        call memory_alloca(mem_modul(1:2,modul),'RESCH_NSI','nsi_memall',resch_nsi,npoin)
     end if
     !
     ! REMOM_NSI: Schur complement residual
     !
     if(postp(1) % npp_stepi(62)/=0.or.maxval(postp(1) % pos_times(1:nvart,62))>zensi) then
        call memory_alloca(mem_modul(1:2,modul),'REMOM_NSI','nsi_memall',remom_nsi,ndime,npoin)
     end if
     !
     ! Hydrostatic level set: used to compute rho_{hyd} to add to NS' RHS
     !
     if( kfl_hydro_gravity_nsi /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'HYDRO_DENSITY_NSI','nsi_memall',hydro_density_nsi,nelem) 
        do ielem=1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           call memory_alloca(mem_modul(1:2,modul),'HYDRO_DENSITY_NSI(IELEM)','nsi_memall',hydro_density_nsi(ielem)%a,pgaus)
        end do
     end if
     !
     ! Surface tension
     !
     if( kfl_surte_nsi /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'NORLE_NSI','nsi_memall',norle_nsi,ndime,npoin)
        call memory_alloca(mem_modul(1:2,modul),'CURLE_NSI','nsi_memall',curle_nsi,npoin)
     end if
     !
     ! Laplacian matrix
     !
     if(kfl_predi_nsi/=0) then
        call memory_alloca(mem_modul(1:2,modul),'LAPLA_NSI','nsi_memall',lapla_nsi,solve(2) % nzmat)
     end if
     !
     ! Save linear matrix
     !
     if(kfl_savco_nsi==1) then
        call memory_alloca(mem_modul(1:2,modul),'AMATR_NSI','nsi_memall',amatr_nsi,solve(1) % nzmat)
     end if
     !
     ! Coupling with SOLIDZ
     !
     if( coupling('SOLIDZ','NASTIN') >= 1 .or. coupling('NASTIN','IMMBOU') >= 1 ) then
        call memory_alloca(mem_modul(1:2,modul),'FORCF','nsi_memall',forcf,ndime,npoin)
     end if
     !
     ! Matrix copy for internal force 
     !
     if( kfl_intfo_nsi > 0 ) then
        allocate(intfo_nsi(npoin)) 
        do ipoin = 1,npoin
           intfo_nsi(ipoin) % kfl_exist = 0
        end do
     end if

     if (kfl_bnods_nsi == 1) then 
       allocate(iboun_nsi(npoin))
     end if

  else
     !
     ! Master: allocate minimum memory
     !
     call memory_alloca(mem_modul(1:2,modul),'VELOC','nsi_memall',veloc,1_ip,1_ip,ncomp_nsi)
     call memory_alloca(mem_modul(1:2,modul),'PRESS','nsi_memall',press,1_ip,ncomp_nsi)
     if( kfl_regim_nsi == 2 ) then
        unk2n_nsi => densi   
     else
        unk2n_nsi => press
     end if     
     if( kfl_stabi_nsi /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'VEPRO_NSI','nsi_memall',vepro_nsi,1_ip,1_ip)
        call memory_alloca(mem_modul(1:2,modul),'PRPRO_NSI','nsi_memall',prpro_nsi,1_ip)
        if( kfl_stabi_nsi == 2 ) then
           call memory_alloca(mem_modul(1:2,modul),'GRPRO_NSI','nsi_memall',grpro_nsi,1_ip,1_ip)
        end if
     end if
     if(kfl_relax_nsi==2) then
        call memory_alloca(mem_modul(1:2,modul),'DUNKN_NSI','nsi_memall',dunkn_nsi,1_ip)
     end if
     if(kfl_relap_nsi==2) then
        call memory_alloca(mem_modul(1:2,modul),'DUNKP_NSI','nsi_memall',dunkp_nsi,1_ip)     
     end if
     if(kfl_regim_nsi==1.or.kfl_regim_nsi==2) then
        call memory_alloca(mem_modul(1:2,modul),'DENSI','nsi_memall',densi,1_ip,1_ip)    
     end if
     if(kfl_predi_nsi/=0) then
        call memory_alloca(mem_modul(1:2,modul),'LAPLA_NSI','nsi_memall',lapla_nsi,1_ip)  
     end if
     if( kfl_hydro_gravity_nsi /= 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'HYDRO_DENSITY_NSI','nsi_memall',hydro_density_nsi,1_ip)
     end if
     !
     ! VESGS: Subgrid scale velocity (needed for output of residuals)
     !
     if( kfl_sgsco_nsi == 1 .or. kfl_sgsti_nsi == 1 ) then
        call memory_alloca(mem_modul(1:2,modul),'VESGS','nsi_memall',vesgs,1_ip)  
        if( kfl_sgsco_nsi==1.or. kfl_sgsti_nsi == 1 ) then
           allocate( itsta_nsi(misgs_nsi) )
           allocate( resis_nsi(2_ip,misgs_nsi) )
           !call memory_alloca(mem_modul(1:2,modul),'ITSTA_NSI','nsi_memall',itsta_nsi,misgs_nsi) 
           !call memory_alloca(mem_modul(1:2,modul),'RESIS_NSI','nsi_memall',resis_nsi,2_ip,misgs_nsi) 
        end if
     else
        allocate( itsta_nsi(1_ip) )
        allocate( resis_nsi(2_ip,1_ip) )
        !call memory_alloca(mem_modul(1:2,modul),'ITSTA_NSI','nsi_memall',itsta_nsi,1_ip) 
        !call memory_alloca(mem_modul(1:2,modul),'RESIS_NSI','nsi_memall',resis_nsi,2_ip,1_ip)         
     end if

  end if
  
end subroutine nsi_memall
