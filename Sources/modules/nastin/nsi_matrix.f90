subroutine nsi_matrix()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_matrix
  ! NAME 
  !    nsi_matrix
  ! DESCRIPTION
  !    This routine computes elemental matrix and RHS.
  !    ITASK=1 ... Momentum+continuity equations
  !    ITASK=2 ... Momentum equation only
  ! USES
  !    nsi_elmope
  !    nsi_bouope
  ! USED BY
  !    nsi_solmon
  !    nsi_solbgs
  !***
  !-----------------------------------------------------------------------
  use def_solver
  use def_master
  use def_nastin
  use def_domain
  use mod_elmgeo
  use mod_interpolation, only : COU_GET_INTERPOLATE_POINTS_VALUES
  use mod_solver,        only : solver_preprocess
  use mod_solver,        only : solver_initialize_matrix_and_rhs
  implicit none
  integer(ip) :: icomp
  real(rp)    :: time1,time2,dummr

  call cputim(time1)
  ! 
  ! Initializations
  !
  tamin_nsi =  huge(1.0_rp)
  tamax_nsi = -huge(1.0_rp)

  if( INOTMASTER ) then
     !
     ! Matrix, preconditioner and RHS initializations
     !
     if( NSI_MONOLITHIC ) then
        call solver_initialize_matrix_and_rhs(momod(modul) % solve,amatr,rhsid)
     else
        call solver_initialize_matrix_and_rhs(momod(modul) % solve,amatr,rhsid,lapla_nsi)        
     end if
     if( solve(ivari_nsi) % kfl_preco >= 3 ) then
        do icomp = 1,solve(ivari_nsi) % nzpre
           pmatr(icomp) = 0.0_rp
        end do
     end if
     !
     ! Element assembly
     !    
     !call nsi_elmope(1_ip) 
     call nsi_elmope_omp(1_ip) 

  end if

  if ( kfl_waexl_nsi == 1_ip ) then  ! Wall with exchange location  - this was inside bouope - moved outside so that master enters
     !
     ! Interpolate
     !
     call COU_GET_INTERPOLATE_POINTS_VALUES(veloc,velel_nsi,wallcoupling)
  end if
  !
  ! Boundary assembly
  !
  if( INOTMASTER ) then
     call nsi_bouope()
  end if

  call cputim(time2) 
  cpu_modul(CPU_ASSEMBLY,modul) = cpu_modul(CPU_ASSEMBLY,modul) + time2 - time1
  !
  ! Internal force
  !
  if( .not. NSI_MONOLITHIC ) &
       call nsi_intfor(& 
       amatr(poauu_nsi),amatr(poaup_nsi),rhsid)
  !
  ! Coupling with Immbou: compute force
  !
  call nsi_coupli(ITASK_MATRIX)
  !
  ! Solver pre-process
  !
  call solver_preprocess(momod(modul) % solve,amatr,rhsid,unkno,lapla_nsi)
  !
  ! Boundary conditions on matrix
  !
  if( kfl_matdi_nsi == 1 .and. INOTMASTER ) then
     call nsi_matdir(&
          amatr(poauu_nsi),amatr(poaup_nsi),amatr(poapu_nsi),amatr(poapp_nsi),&
          lapla_nsi,rhsid,rhsid(ndbgs_nsi+1),unkno,unkno(ndbgs_nsi+1))
  end if
  !
  ! Penalize pressure equation
  !
  if( INOTMASTER .and. kfl_prepe_nsi == 1 ) &
       call nsi_penpre(&
       amatr(poapp_nsi),lapla_nsi,rhsid(ndbgs_nsi+1))
  
     !call pipiprout(&
     !     2_ip,amatr(poauu_nsi),amatr(poaup_nsi),amatr(poapu_nsi),amatr(poapp_nsi),&
     !     lapla_nsi,rhsid,rhsid(ndbgs_nsi+1))

end subroutine nsi_matrix

subroutine pipiprout(itask,Auu,Aup,Apu,App,Q,bu,bp)
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use def_kermod
  use def_elmtyp
  use mod_cutele
  use mod_memory
  implicit none
  integer(ip), intent(in)    :: itask
  real(rp),    intent(out)   :: Auu(ndime,ndime,nzdom) 
  real(rp),    intent(out)   :: Aup(ndime,nzdom)
  real(rp),    intent(out)   :: Apu(ndime,nzdom)
  real(rp),    intent(out)   :: App(nzdom)
  real(rp),    intent(out)   :: Q(nzdom)
  real(rp),    intent(out)   :: bu(ndime,npoin)
  real(rp),    intent(out)   :: bp(npoin)
  integer(ip)                :: ielem,inode,ipoin,iplus,iminu,pnode
  integer(ip)                :: jpoin,izdom,knode,idime,jdime,jnode
  real(rp)                   :: x1,x2,fi,fj,t,norma(3)
  real(rp)                   :: xx(3,10),pcoor(3),dummr

  return

  if( IMASTER ) return

  if( itask == 1 ) then


     do ielem = 1,nelem
        lelch(ielem) = ELFEM
     end do

     nullify(prout_nsi)
     call memory_alloca(mem_modul(1:2,modul),'PROUT_NSI','nsi_memall',prout_nsi,npoin)

     do ielem = 1,nelem
        iplus = 0
        iminu = 0
        do inode = 1,lnnod(ielem)
           ipoin = lnods(inode,ielem)
           if( fleve(ipoin,1) > 0.0_rp ) then
              iplus = 1
           else
              iminu = 1
           end if
        end do
        if( iplus + iminu == 2 ) then
           lelch(ielem) = ELCUT
           do inode = 1,lnnod(ielem)
              ipoin = lnods(inode,ielem)
              if( fleve(ipoin,1) <= 0.0_rp ) then
                 prout_nsi(ipoin) = 1
              end if
           end do
        end if
     end do
     do ielem = 1,nelem
        do inode = 1,lnnod(ielem)
           ipoin = lnods(inode,ielem)
           if( prout_nsi(ipoin) == 1 .and. lelch(ielem) /= ELCUT ) lelch(ielem) = 32
        end do
     end do
     !
     ! Cut elements
     !
     do ielem = 1,nelem
        if( lelch(ielem) == ELCUT ) then      
           knode = 0
           pnode = lnnod(ielem)
           do inode = 1,pnode
              if( inode == pnode ) then
                 jnode = 1
              else
                 jnode = inode + 1
              end if
              ipoin = lnods(inode,ielem)
              jpoin = lnods(jnode,ielem)
              fi    = fleve(ipoin,1)
              fj    = fleve(jpoin,1)
              t     = -fi / ( fj - fi + zeror)
              if( t >= 0.0_rp .and. t <= 1.0_rp ) then
                 knode = knode + 1
                 do idime = 1,ndime
                    xx(idime,knode) = coord(idime,ipoin) + t * ( coord(idime,jpoin) - coord(idime,ipoin) )
                 end do
              end if
           end do
           if( knode /= 2 ) then
              call runend('PROBLEM CUTTING ELEMENT')
           else
              norma(1) = - ( xx(2,2) - xx(2,1) ) 
              norma(2) =   ( xx(1,2) - xx(1,1) ) 
              call vecuni(ndime,norma,dummr)
              pcoor = 0.0_rp
              do inode = 1,knode
                 pcoor(1:ndime) = pcoor(1:ndime) + xx(1:ndime,knode)
              end do
              pcoor(1:ndime) = pcoor(1:ndime) / real(knode,rp)
              call onecut(-1_ip,ielem,norma,pcoor,'DO_NOT_MOVE_NODES')
           end if
        end if
     end do

  else if( itask == 2 ) then

     do ielem = 1,nelem
        lelch(ielem) = ELFEM
     end do

     do ipoin = 1,npoin
        if( fleve(ipoin,1) < 0.0_rp .and. prout_nsi(ipoin) /= 1 ) then
           do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
              jpoin = c_dom(izdom)
              if( ipoin /= jpoin ) then
                 Aup(1:ndime,izdom) = 0.0_rp
                 Apu(1:ndime,izdom) = 0.0_rp
                 App(izdom)         = 0.0_rp
                 Q(izdom)           = 0.0_rp
              else
                 Apu(1:ndime,izdom) = 0.0_rp
                 Aup(1:ndime,izdom) = 0.0_rp
                 bp(ipoin)          = 0.0_rp
              end if
           end do
        end if
     end do
           
     !do ipoin = 1,npoin
     !   if( fleve(ipoin,1) < 0.0_rp ) then
     !      do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
     !         jpoin = c_dom(izdom) 
     !         if( fleve(jpoin,1) < 0.0_rp .and. prout_nsi(ipoin) /= 1 ) then
     !            Auu(1:ndime,1:ndime,izdom) = 0.0_rp
     !            !Aup(1:ndime,izdom) = 0.0_rp
     !            do idime = 1,ndime
     !               do jdime = 1,ndime
     !                  !              bu(idime,ipoin) = bu(idime,ipoin) - Auu(jdime,idime,izdom) * veloc(idime,ipoin,1)
     !                  !               Auu(jdime,idime,izdom) = 0.0_rp
     !               end do
     !            end do
     !         end if
     !      end do
     !   end if
     !end do

     call memory_deallo(mem_modul(1:2,modul),'PROUT_NSI','nsi_memall',prout_nsi)

  end if

end subroutine pipiprout

