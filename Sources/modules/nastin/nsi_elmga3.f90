subroutine nsi_elmga3(&
     pnode,lnods,elcod,elpre,elvel,elfle,elvep,elprp,&
     elgrp,elmut,eltem,elmsh,elnor,elcur,elwmean)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_elmga3
  ! NAME 
  !    nsi_elmga3
  ! DESCRIPTION
  !    Compute some variables at the Gauss points
  !    ELVEL, ELCOD, ELPRE
  ! OUTPUT
  ! USES
  ! USED BY
  !    nsi_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  veloc,press,tempe,fleve,&
       &                        turmu,velom,kfl_coupl,wmean,&
       &                        ID_NASTIN,ID_ALEFOR,ID_TEMPER,&
       &                        ID_LEVELS,ID_CHEMIC
  use def_domain, only       :  ndime,coord
  use def_nastin, only       :  kfl_timei_nsi,kfl_regim_nsi,&
       &                        curle_nsi,norle_nsi,&
       &                        kfl_stabi_nsi,vepro_nsi,prpro_nsi,&
       &                        grpro_nsi,kfl_cotur_nsi,kfl_cotem_nsi,&
       &                        nbdfp_nsi,kfl_surte_nsi
  implicit none
  integer(ip), intent(in)    :: pnode
  integer(ip), intent(in)    :: lnods(pnode)
  real(rp),    intent(out)   :: elcod(ndime,pnode),elpre(pnode)
  real(rp),    intent(out)   :: elvel(ndime,pnode,*)
  real(rp),    intent(out)   :: elfle(pnode)
  real(rp),    intent(out)   :: elvep(ndime,pnode)
  real(rp),    intent(out)   :: elprp(pnode)
  real(rp),    intent(out)   :: elgrp(ndime,pnode)
  real(rp),    intent(out)   :: elmut(pnode)
  real(rp),    intent(out)   :: eltem(pnode,nbdfp_nsi)
  real(rp),    intent(out)   :: elwmean(pnode,nbdfp_nsi)
  real(rp),    intent(out)   :: elmsh(ndime,pnode)
  real(rp),    intent(out)   :: elnor(ndime,pnode)
  real(rp),    intent(out)   :: elcur(pnode)
  integer(ip)                :: inode,idime,ipoin,itime

  if( kfl_timei_nsi == 0 ) then
     !
     ! Stationary
     !     
     do inode = 1,pnode
        ipoin = lnods(inode)
        do idime = 1,ndime
           elvel(idime,inode,1) = veloc(idime,ipoin,1)
           elvel(idime,inode,2) = 0.0_rp
           elcod(idime,inode)   = coord(idime,ipoin)
        end do
        elpre(inode) = press(ipoin,1)
     end do
  else
     !
     ! Transient
     !     
     do inode = 1,pnode
        ipoin = lnods(inode)
        do idime = 1,ndime
           elvel(idime,inode,1) = veloc(idime,ipoin,1)
           elvel(idime,inode,2) = veloc(idime,ipoin,3)
           do itime=3,nbdfp_nsi
              elvel(idime,inode,itime) = veloc(idime,ipoin,itime+1)
           end do
           elcod(idime,inode)   = coord(idime,ipoin)
        end do
        elpre(inode) = press(ipoin,1)
     end do
  end if
  !
  ! Surface tension
  !
  if( kfl_surte_nsi /= 0 ) then
     do inode = 1,pnode
        ipoin = lnods(inode)
        elfle(inode) = fleve(ipoin,1)
        elcur(inode) = curle_nsi(ipoin)
        do idime =1,ndime
           elnor(idime,inode) = norle_nsi(idime,ipoin)
        end do
     end do
  end if
  !
  ! Projections
  !
  if( kfl_stabi_nsi /= 0 ) then
     do inode = 1,pnode
        ipoin = lnods(inode)
        do idime = 1,ndime
           elvep(idime,inode) = vepro_nsi(idime,ipoin) 
        end do
        elprp(inode) = prpro_nsi(ipoin) 
     end do
     if( kfl_stabi_nsi == 2 ) then
        do inode = 1,pnode
           ipoin = lnods(inode)
           do idime = 1,ndime
              elgrp(idime,inode) = grpro_nsi(idime,ipoin) 
           end do
        end do
     end if
  end if
  !
  ! Coupling with Turbul
  !
  if( kfl_cotur_nsi == 1 ) then
     do inode = 1,pnode
        ipoin = lnods(inode)
        elmut(inode) = turmu(ipoin)
     end do
  end if
  !
  ! Coupling with Temper
  !
  if( kfl_coupl(ID_NASTIN,ID_TEMPER) /= 0 .or. kfl_cotem_nsi == 1 .or. kfl_regim_nsi==3 ) then 
     do inode = 1,pnode
        ipoin = lnods(inode)
        if( kfl_coupl(ID_NASTIN,ID_CHEMIC) /= 0) then
          elwmean(inode,1) = wmean(ipoin,1)
        else
          elwmean(inode,1) = 1.0_rp
        endif
        eltem(inode,1) = tempe(ipoin,1)
     end do
     if (kfl_regim_nsi == 3) then ! Low Mach needs time information
        do itime=2,nbdfp_nsi
           do inode = 1,pnode
              ipoin = lnods(inode)
              if( kfl_coupl(ID_NASTIN,ID_CHEMIC) /= 0) then
                elwmean(inode,itime) = wmean(ipoin,itime+1)
              else
                elwmean(inode,itime) = 1.0_rp
              endif
              eltem(inode,itime) = tempe(ipoin,itime+1)
           end do
        end do
     endif
  end if
  !
  ! Mesh velocity
  !     
  if( kfl_coupl(ID_NASTIN,ID_ALEFOR) /= 0 ) then ! TESTEO 
     do inode = 1,pnode
        ipoin = lnods(inode)
        do idime = 1,ndime
           elmsh(idime,inode) = velom(idime,ipoin)
        end do
     end do
  end if

end subroutine nsi_elmga3
