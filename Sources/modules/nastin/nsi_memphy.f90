subroutine nsi_memphy(itask)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_memphy
  ! NAME 
  !    nsi_memphy
  ! DESCRIPTION
  !    This routine allocates memory for physical arrays
  ! USES
  !    ecoute
  !    memchk
  !    runend
  ! USED BY
  !    nsi_reaphy
  !***
  !-----------------------------------------------------------------------
  use def_parame 
  use def_inpout
  use def_master
  use def_nastin
  use def_domain
  use mod_memchk
  use mod_memory
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: icoef,imate
  integer(4)              :: istat

  select case(itask)

  case(1_ip)
     !
     ! List of element materials
     ! 

  case(2_ip)
     !
     ! Properties: Allocate memory
     !

  case(3_ip)
     !
     ! Properties: deallocate memory
     ! DO NOT DEALLOCATE BECAUSE THEY ARE USED BY NSI_OUTINF
     !

  case( 4_ip)
     !
     ! Properties: Allocate memory
     !
     if( INOTMASTER ) then
        allocate(prope_nsi(2,npoin),stat=istat)
        call memchk(zero,istat,mem_modul(1:2,modul),'PROPE_NSI','nsi_memphy',prope_nsi)
     end if

  case(-4_ip)
     !
     ! Properties: Deallocate memory
     !
     if( INOTMASTER ) then
        call memchk(two,istat,mem_modul(1:2,modul),'PROPE_NSI','nsi_memphy',prope_nsi)
        deallocate(prope_nsi,stat=istat)
        if(istat/=0) call memerr(two,'PROPE_NSI','nsi_rescon',0_ip)
     end if

  case( 5_ip)
     !
     ! Material force
     !
     call memory_alloca(mem_modul(1:2,modul),'LFORC_MATERIAL_NSI','nsi_memphy',lforc_material_nsi,nmate)
     call memory_alloca(mem_modul(1:2,modul),'XFORC_MATERIAL_NSI','nsi_memphy',xforc_material_nsi,mforc_material_nsi,nmate)
     call memory_alloca(mem_modul(1:2,modul),'VELTA_NSI','nsi_memphy',velta_nsi,mtabl_nsi, nmate)
     call memory_alloca(mem_modul(1:2,modul),'THRTA_NSI','Nsi_memphy',thrta_nsi,mtabl_nsi, nmate)
     call memory_alloca(mem_modul(1:2,modul),'POWTA_NSI','nsi_memphy',powta_nsi,mtabl_nsi, nmate)
     call memory_alloca(mem_modul(1:2,modul),'NTABL_NSI','nsi_memphy',ntabl_nsi, nmate)

  case(-5_ip)
     !
     ! Material force : deallocates structures if master
     !
     call memory_deallo(mem_modul(1:2,modul),'LFORC_MATERIAL_NSI','nsi_memphy',lforc_material_nsi)
     call memory_deallo(mem_modul(1:2,modul),'XFORC_MATERIAL_NSI','nsi_memphy',xforc_material_nsi)
     call memory_deallo(mem_modul(1:2,modul),'VELTA_NSI','nsi_memphy',velta_nsi)
     call memory_deallo(mem_modul(1:2,modul),'THRTA_NSI','nsi_memphy',thrta_nsi)
     call memory_deallo(mem_modul(1:2,modul),'POWTA_NSI','nsi_memphy',powta_nsi)
     call memory_deallo(mem_modul(1:2,modul),'NTABL_NSI','nsi_memphy',ntabl_nsi)

  case( 6_ip)
     !
     ! List for boundary nodes
     !
     allocate(bntab_nsi(nbnod_nsi,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'BNTAB_NSI','nsi_memphy',bntab_nsi)

  case( 7_ip)
     !
     ! List for boundary nodes
     !
     allocate(bnval_nsi(nbval_nsi*nbtim_nsi,3),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'BNVAL_NSI','nsi_memphy',bnval_nsi)
     allocate(iboun_nsi(npoin),stat=istat)
     call memchk(zero,istat,mem_modul(1:2,modul),'IBOUN_NSI','nsi_memphy',iboun_nsi)

  end select

end subroutine nsi_memphy
