subroutine nsi_averag()
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_averag
  ! NAME 
  !    nsi_averag
  ! DESCRIPTION
  !    Average velocity and pressure
  ! USES
  ! USED BY
  !    nsi_endste 
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  implicit none
  integer(ip) :: idime,ipoin
  real(rp)    :: auxii

  if( cutim > avtim_nsi ) then
     

     if( INOTMASTER ) then

        !AVVEL
        if( postp(1) % npp_stepi(21) /= 0 .or. maxval(postp(1) % pos_times(1:nvart,21)) > zensi ) then
           do ipoin=1,npoin
              do idime=1,ndime
                 avvel_nsi(idime,ipoin)=avvel_nsi(idime,ipoin)&
                      +veloc(idime,ipoin,1)*dtime
              end do
           end do
        end if

        ! AVPRE
        if( postp(1) % npp_stepi(28) /= 0 .or. maxval(postp(1) % pos_times(1:nvart,28)) > zensi ) then
           do ipoin = 1,npoin
              avpre_nsi(ipoin)=avpre_nsi(ipoin)&
                   +press(ipoin,1)*dtime
           end do
        end if

        ! AVTAN
        if( postp(1) % npp_stepi(46) /= 0 .or. maxval(postp(1) % pos_times(1:nvart,46)) > zensi ) then
           call memgen(zero,ndime,npoin)
           call nsi_memphy(4_ip)
           call nsi_denvis()
           call nsi_outtan(1_ip)
           call nsi_memphy(-4_ip)
           do ipoin=1,npoin
              do idime=1,ndime
                 avtan_nsi(idime,ipoin) = avtan_nsi(idime,ipoin)&
                      + gevec(idime,ipoin)*dtime
              end do
           end do
           call memgen(2_ip,ndime,npoin)
        end if

        ! V**2
        if( postp(1) % npp_stepi(57) /= 0 .or. maxval(postp(1) % pos_times(1:nvart,57)) > zensi ) then
           do ipoin=1,npoin
              do idime=1,ndime
                 auxii = veloc(idime,ipoin,1)                 
                 avve2_nsi(idime,ipoin)=avve2_nsi(idime,ipoin)&
                      +auxii*auxii*dtime
              end do
           end do
        end if

        !Vx*Vy
        if( postp(1) % npp_stepi(58) /= 0 .or. maxval(postp(1) % pos_times(1:nvart,58)) > zensi ) then
           do ipoin=1,npoin
              avvxy_nsi(1,ipoin)=avvxy_nsi(1,ipoin)&
                   +veloc(1,ipoin,1)*&
                   veloc(2,ipoin,1)*dtime
              if (ndime==3) then
                 avvxy_nsi(2,ipoin)=avvxy_nsi(2,ipoin)&
                      +veloc(2,ipoin,1)*&
                      veloc(3,ipoin,1)*dtime
                 avvxy_nsi(3,ipoin)=avvxy_nsi(3,ipoin)&
                      +veloc(3,ipoin,1)*&
                      veloc(1,ipoin,1)*dtime
              end if
           end do
        end if

        !P**2
        if( postp(1) % npp_stepi(59) /= 0 .or. maxval(postp(1) % pos_times(1:nvart,59)) > zensi ) then
           do ipoin = 1,npoin
              avpr2_nsi(ipoin)=avpr2_nsi(ipoin)&
                   +press(ipoin,1)*press(ipoin,1)*dtime
           end do
        end if

     end if

  end if

end subroutine nsi_averag
