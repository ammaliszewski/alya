subroutine nsi_elmmat(                                &
     pnode,pgaus,pevat,gpden,gpvis,gppor,gpgvi,gpsp1, &
     gptt1,gpsp2,gptt2,gpvol,gpsha,gpcar,gplap,gphes, &
     gpadv,gpvep,gpprp,gpgrp,gprhs,gprhc,rmomu,rcont, &
     p1vec,p2vec,p2sca,wgrgr,wgrvi,elauu,elaup,elapp, &
     elapu,elrbu,elrbp,rmom2,p1ve2,gpst1,gpsgs,gpgve, &
     gprh2,gppre,gprhs_sgs,elvel,ellum,dtinv_loc)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_elmmat
  ! NAME 
  !    nsi_elmmat
  ! DESCRIPTION
  !    Compute element matrix and RHS
  ! USES
  ! USED BY
  !    nsi_elmope
  !***
  !----------------------------------------------------------------------- 
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,mnode,ntens,lnods
  use def_nastin, only       :  kfl_cotur_nsi,kfl_stabi_nsi
  use def_nastin, only       :  kfl_sgsti_nsi,dtsgs_nsi
  use def_nastin, only       :  fvins_nsi,fcons_nsi,bemol_nsi,kfl_regim_nsi
  use def_nastin, only       :  fvela_nsi,kfl_rmom2_nsi,kfl_press_nsi
  use def_nastin, only       :  kfl_p1ve2_nsi,kfl_linea_nsi
  use def_master, only       :  kfl_lumped
  use def_kermod, only       :  kfl_duatss,fact_duatss
  implicit none
  integer(ip), intent(in)    :: pnode,pgaus,pevat
  real(rp),    intent(in)    :: gpden(pgaus),gpvis(pgaus)
  real(rp),    intent(in)    :: gppor(pgaus),gpgvi(ndime,pgaus)
  real(rp),    intent(inout) :: gpsp1(pgaus),gptt1(pgaus)
  real(rp),    intent(inout) :: gpsp2(pgaus),gptt2(pgaus)
  real(rp),    intent(in)    :: gpvol(pgaus),gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)    :: gpadv(ndime,pgaus)
  real(rp),    intent(inout) :: gpvep(ndime,pgaus)
  real(rp),    intent(in)    :: gpprp(pgaus)      
  real(rp),    intent(in)    :: gpgrp(ndime,pgaus)
  real(rp),    intent(in)    :: gplap(pnode,pgaus)
  real(rp),    intent(in)    :: gphes(ntens,mnode,pgaus)
  real(rp),    intent(in)    :: gprhs(ndime,pgaus)
  real(rp),    intent(inout) :: gprhs_sgs(ndime,pgaus)
  real(rp),    intent(in)    :: gprhc(pgaus)
  real(rp),    intent(in)    :: gprh2(pgaus)
  real(rp),    intent(in)    :: rmomu(pnode,pgaus)
  real(rp),    intent(in)    :: rcont(ndime,pnode,pgaus)
  real(rp),    intent(out)   :: p1vec(pnode,pgaus)
  real(rp),    intent(out)   :: p2vec(ndime,pnode,pgaus)
  real(rp),    intent(out)   :: p2sca(pnode,pgaus)
  real(rp),    intent(out)   :: wgrgr(pnode,pnode,pgaus)
  real(rp),    intent(out)   :: wgrvi(pnode,pgaus)
  real(rp),    intent(out)   :: elauu(pnode*ndime,pnode*ndime)
  real(rp),    intent(out)   :: elaup(pnode*ndime,pnode)
  real(rp),    intent(out)   :: elapp(pnode,pnode)
  real(rp),    intent(out)   :: elapu(pnode,pnode*ndime)
  real(rp),    intent(out)   :: elrbu(ndime,pnode)
  real(rp),    intent(out)   :: elrbp(pnode)
  real(rp),    intent(in)    :: rmom2(ndime,ndime,pnode,pgaus)
  real(rp),    intent(out)   :: p1ve2(ndime,ndime,pnode,pgaus)
  real(rp),    intent(in)    :: gpst1(pgaus)
  real(rp),    intent(in)    :: gpsgs(ndime,pgaus,*)
  real(rp),    intent(in)    :: gpgve(ndime,ndime,pgaus)
  real(rp),    intent(in)    :: gppre(pgaus)
  real(rp),    intent(in)    :: elvel(ndime, pnode, *)
  real(rp),    intent(out)   :: ellum(pnode)
  real(rp),    intent(in)    :: dtinv_loc
  integer(ip)                :: inode,jnode,idofn,jdofn,idofv
  integer(ip)                :: jdof2,jdof3,idof1,idof3,idof2
  integer(ip)                :: igaus,idime,jdof1,jdofv,kdime
  integer(ip)                :: jdime
  real(rp)                   :: fact2,fact3,fact4,fact5,fact6
  real(rp)                   :: factx,facty,factz,fact8,fact0,fact9
  real(rp)                   :: facx1,facy1,facz1,fact1,xvisc
  real(rp)                   :: xvis2,fact7, penal, gprhh(ndime,pgaus), taupr(pgaus), gpveo(ndime)

  !----------------------------------------------------------------------
  !
  ! Initialization
  !
  !----------------------------------------------------------------------

  do inode = 1,pnode 
     elrbp(inode) = 0.0_rp
     do idime = 1,ndime
        elrbu(idime,inode) = 0.0_rp
     end do
     do jnode = 1,pnode
        elapp(jnode,inode) = 0.0_rp
     end do
  end do
  do idofn = 1,pevat
     do jdofn = 1,pevat
        elauu(jdofn,idofn) = 0.0_rp
     end do
     do jnode = 1,pnode
        elaup(idofn,jnode) = 0.0_rp
        elapu(jnode,idofn) = 0.0_rp
     end do
  end do
  xvis2 = 0.0_rp
  if( fvins_nsi > 0.9_rp ) then
     xvisc = 1.0_rp
     if( fvins_nsi > 1.9_rp ) xvis2 = 2.0_rp/3.0_rp
  else
     xvisc = 0.0_rp
  end if

  !----------------------------------------------------------------------
  !
  ! Test functions
  !
  !----------------------------------------------------------------------
  !
  ! P1VEC =  v * [ (tau1'/tau1) - tau1' * sig ] + tau1' * rho * (uc.grad)v
  ! P2SCA =  ( tau2^{-1} * tau2' ) * q   
  ! P2VEC =  tau1' * grad(q)  ( and * rho if in Low-Mach, but not yet)
  ! WGRVI =  grad(Ni) . grad(mu) + mu * Delta(Ni)
  ! WGRGR =  grad(Ni) . grad(Nj)
  !
  if( kfl_regim_nsi == 3 ) then
     do igaus =1, pgaus
        taupr(igaus) = 1.0_rp
     end do
  else
     do igaus =1, pgaus
        taupr(igaus) = 1.0_rp/gpst1(igaus)
     end do
  end if

  if( ndime == 2 ) then

     do igaus = 1,pgaus
        fact1 = gpsp1(igaus)*gpden(igaus)                * gpvol(igaus)
        fact2 = (gptt1(igaus)-gpsp1(igaus)*gppor(igaus)) * gpvol(igaus)
        !!FER !!$        if (kfl_regim_nsi == 3) then 
!!$           fact3 = gpsp1(igaus) * gpden(igaus)           * gpvol(igaus)
!!$        else
        fact3 = gpsp1(igaus)                          * gpvol(igaus)
        !!FER        endif
        fact4 = gptt2(igaus)                             * gpvol(igaus)
        do inode = 1,pnode
           p1vec(inode,igaus)   = fact2 * gpsha(inode,igaus) + fact1 *    & 
                &                 ( gpadv(1,igaus) * gpcar(1,inode,igaus) &
                &                 + gpadv(2,igaus) * gpcar(2,inode,igaus) )
           p2sca(inode,igaus)   = fact4 * gpsha(inode,igaus)
           p2vec(1,inode,igaus) = fact3 * gpcar(1,inode,igaus)
           p2vec(2,inode,igaus) = fact3 * gpcar(2,inode,igaus)
           wgrvi(inode,igaus)   = gpgvi(1,igaus) * gpcar(1,inode,igaus) &
                &               + gpgvi(2,igaus) * gpcar(2,inode,igaus) &
                &               + gpvis(igaus)   * gplap(inode,igaus)
           do jnode = 1,pnode
              wgrgr(inode,jnode,igaus) = &
                   &   gpcar(1,inode,igaus)*gpcar(1,jnode,igaus) &
                   &  +gpcar(2,inode,igaus)*gpcar(2,jnode,igaus)             
           end do
        end do
     end do

  else

     do igaus = 1,pgaus
        fact1 = gpsp1(igaus)*gpden(igaus)                * gpvol(igaus)
        fact2 = (gptt1(igaus)-gpsp1(igaus)*gppor(igaus)) * gpvol(igaus)
        !!FER !!$        if (kfl_regim_nsi == 3) then 
!!$           fact3 = gpsp1(igaus) * gpden(igaus)           * gpvol(igaus)
!!$        else
        fact3 = gpsp1(igaus)                          * gpvol(igaus)
        !!FER        endif
        fact4 = gptt2(igaus)                             * gpvol(igaus)
        do inode = 1,pnode
           p1vec(inode,igaus)   = fact2 * gpsha(inode,igaus) + fact1 *    &
                &                 ( gpadv(1,igaus) * gpcar(1,inode,igaus) &
                &                 + gpadv(2,igaus) * gpcar(2,inode,igaus) &
                &                 + gpadv(3,igaus) * gpcar(3,inode,igaus) )
           p2sca(inode,igaus)   = fact4 * gpsha(inode,igaus)
           p2vec(1,inode,igaus) = fact3 * gpcar(1,inode,igaus)
           p2vec(2,inode,igaus) = fact3 * gpcar(2,inode,igaus)
           p2vec(3,inode,igaus) = fact3 * gpcar(3,inode,igaus)
           wgrvi(inode,igaus)   = gpgvi(1,igaus) * gpcar(1,inode,igaus) &
                &               + gpgvi(2,igaus) * gpcar(2,inode,igaus) &
                &               + gpgvi(3,igaus) * gpcar(3,inode,igaus) &
                &               + gpvis(igaus)   * gplap(inode,igaus)
           do jnode = 1,pnode
              wgrgr(inode,jnode,igaus) = &
                   &    gpcar(1,inode,igaus) * gpcar(1,jnode,igaus) &
                   &  + gpcar(2,inode,igaus) * gpcar(2,jnode,igaus) &
                   &  + gpcar(3,inode,igaus) * gpcar(3,jnode,igaus)             
           end do
        end do
     end do
    
  end if
  !
  ! P1VE2: Off-diagonal test function
  !
  if( kfl_p1ve2_nsi /= 0 ) then
     call veczer(pgaus*pnode*ndime*ndime,p1ve2,0.0_rp)
     !
     ! tau1'*2*rho*(w x v)
     ! x-equation: v = (v,0,0) => w x v = (    0 , wz*v , -wy*v)     
     ! y-equation: v = (0,v,0) => w x v = (-wz*v ,    0 ,  wx*v)     
     ! z-equation: v = (0,0,v) => w x v = ( wy*v ,-wx*v ,     0)     
     ! 
     if( ndime == 2 ) then
        do igaus = 1,pgaus           
           fact3 = 2.0_rp * gpden(igaus) * fvela_nsi(3)
           factz = gpsp1(igaus) * fact3 * gpvol(igaus)
           do inode = 1,pnode 
              fact1                  =   factz * gpsha(inode,igaus)
              p1ve2(1,2,inode,igaus) =   fact1
              p1ve2(2,1,inode,igaus) = - fact1
           end do
        end do
     else
        do igaus = 1,pgaus   
           fact8 = 2.0_rp * gpden(igaus) * gpsp1(igaus) * gpvol(igaus)
           factx = fact8  * fvela_nsi(1)
           facty = fact8  * fvela_nsi(2)
           factz = fact8  * fvela_nsi(3)
           do inode = 1,pnode
              p1ve2(1,2,inode,igaus) =  factz * gpsha(inode,igaus)  ! x-equation
              p1ve2(1,3,inode,igaus) = -facty * gpsha(inode,igaus)  ! x-equation
              p1ve2(2,1,inode,igaus) = -factz * gpsha(inode,igaus)  ! y-equation
              p1ve2(2,3,inode,igaus) =  factx * gpsha(inode,igaus)  ! y-equation
              p1ve2(3,1,inode,igaus) =  facty * gpsha(inode,igaus)  ! z-equation
              p1ve2(3,2,inode,igaus) = -factx * gpsha(inode,igaus)  ! z-equation    
           end do
        end do
     end if
  end if

  !----------------------------------------------------------------------
  !
  ! Auu
  !
  !----------------------------------------------------------------------
  !
  !  Laplacian  form: A=0, B=0, eps(u) = 1/2 grad(u)
  !  Divergence form: A=1, B=0, eps(u) = 1/2 ( grad(u) + grad(u)^t )
  !  Complete   form: A=1, B=1, eps(u) = 1/2 ( grad(u) + grad(u)^t ) - 1/3 div(u) I
  !
  !    ( div(u) , tau2' div(v) )             (1)   <=   tau2' * duj/dxj * dvi/dxi
  !  + ( rmomu(u) , p1vec(v) )               (2)       
  !  + ( mu dui/dxj , dvi/dxj )              (3)   <=   ( 2 mu eps(u) , grad(v) ) = mu * ( dui/dxj + duj/dxi - 2/3 div(u) delta_ij ) dvi/dxj
  !  + A * ( mu duj/dxi , dvi/dxj )          (4)        divergence
  !  - 2/3 * B * ( mu (div u) , dvi/dxi )    (5)        complete
  !  + ( mu d^2ui/dxj^2 , vi )               (6)   <= - ( div[-2 mu eps(u)] , v ) = - d/dxj ( -2*mu* ( dui/dxj + duj/dxi - 2/3*mu div(u) delta_ij ) * vi
  !  + ( dmu/dxj dui/dxj , vi )              (7)        laplacian
  !  + A * ( mu  d^2uj/dxidxj , vi )         (8)        divergence                           
  !  + A * ( dmu/dxj duj/dxi , vi )          (9)        divergence
  !  - 2/3 * B * ( dmu/dxi (div u) , vi )   (10)        complete
  !  - 2/3 * B * ( mu d(div u)/dxi , vi )   (11)        complete         
  !  
  !  1. The terms ( div(u) , tau2' div(v) ) and - 2/3 * B * ( mu div u, dvi/dxj ) are assembled together as
  !     ( ( tau2' - 2/3 B mu ) div u, dvi/dxi ). Therefore, term (5) is assembled always although it is zero
  !  2. Terms (3), (6) and (7) are assembled just below as they are the same for all momentum equations 
  !  3. Terms (4), (8), (9), (10) and (11) are assmbled later on
  !
  !  (3)        x:     mu * ( dux/dx * dv/dx  +  dux/dy * dv/dy  +  dux/dz * dv/dz ) 
  !             y:     mu * ( duy/dx * dv/dx  +  duy/dy * dv/dy  +  duy/dz * dv/dz ) 
  !             z:     mu * ( duz/dx * dv/dx  +  duz/dy * dv/dy  +  duz/dz * dv/dz )   
  !
  !  (4)        x: A * mu * ( dux/dx * dv/dx  +  duy/dx * dv/dy  +  duz/dx * dv/dz )
  !             y: A * mu * ( dux/dy * dv/dx  +  duy/dy * dv/dy  +  duz/dy * dv/dz )
  !             z: A * mu * ( dux/dz * dv/dx  +  duy/dz * dv/dy  +  duz/dz * dv/dz )
  !
  !  (6)        x:  mu * ( d^2ux/dx^2  +  d^2ux/dy^2  +  d^2ux/dz^2 ) v 
  !             y:  mu * ( d^2uy/dx^2  +  d^2uy/dy^2  +  d^2uy/dz^2 ) v 
  !             z:  mu * ( d^2uz/dx^2  +  d^2uz/dy^2  +  d^2uz/dz^2 ) v 
  !
  !  (7)        x: ( dmu/dx * dux/dx  +  dmu/dy * dux/dy  +  dmu/dz * dux/dz ) v 
  !             y: ( dmu/dx * duy/dx  +  dmu/dy * duy/dy  +  dmu/dz * duy/dz ) v 
  !             y: ( dmu/dx * duz/dx  +  dmu/dy * duz/dy  +  dmu/dz * duz/dz ) v 
  !
  !  (8)        x: A * mu * ( d^2ux/dxdx  +  d^2uy/dxdy  +  d^2uz/dxdz ) v
  !             y: A * mu * ( d^2ux/dxdy  +  d^2uy/dydy  +  d^2uz/dydz ) v
  !             z: A * mu * ( d^2ux/dxdz  +  d^2uy/dzdy  +  d^2uz/dzdz ) v
  !
  !  (9)        x: A * ( dmu/dx * dux/dx  +  dmu/dy * duy/dx  +  dmu/dz * duz/dx ) v
  !             y: A * ( dmu/dx * dux/dy  +  dmu/dy * duy/dy  +  dmu/dz * duz/dy ) v
  !             z: A * ( dmu/dx * dux/dz  +  dmu/dy * duy/dz  +  dmu/dz * duz/dz ) v
  !
  !  (10)       x: -2/3 B * dmu/dx ( dux/dx  +  duy/dy  +  duz/dz ) * v
  !             y: -2/3 B * dmu/dy ( dux/dx  +  duy/dy  +  duz/dz ) * v
  !             z: -2/3 B * dmu/dz ( dux/dx  +  duy/dy  +  duz/dz ) * v
  !
  !  (11)       x: -2/3 B * mu * ( d^2ux/dxdx + d^2uy/dydx + d^2uz/dzdx ) v
  !             y: -2/3 B * mu * ( d^2ux/dxdy + d^2uy/dydy + d^2uz/dzdy ) v
  !             z: -2/3 B * mu * ( d^2ux/dxdz + d^2uy/dydz + d^2uz/dzdz ) v
  !
  !  (1) + (5)  x: ( tau2' - 2/3 B mu ) ( dux/dx  +  duy/dy  +  duz/dz ) * dv/dx
  !             y: ( tau2' - 2/3 B mu ) ( dux/dx  +  duy/dy  +  duz/dz ) * dv/dy
  !             z: ( tau2' - 2/3 B mu ) ( dux/dx  +  duy/dy  +  duz/dz ) * dv/dz
  !
  if( ndime == 2 ) then

     do igaus = 1,pgaus

        fact0 = ( gpsp2(igaus) - xvis2 * gpvis(igaus) ) * gpvol(igaus)                        ! (1) + (5)
        fact6 = gpvis(igaus) * gpvol(igaus)

        do inode = 1,pnode
           idof1 = 2*inode-1
           idof2 = idof1+1
           fact1 = fact0 * gpcar(1,inode,igaus) 
           fact2 = fact0 * gpcar(2,inode,igaus)
           fact5 = gpsha(inode,igaus) * gpvol(igaus)

           do jnode = 1,pnode              
              jdof1              =   2*jnode-1
              jdof2              =   jdof1+1
              fact4              =   p1vec(inode,igaus) * rmomu(jnode,igaus) &                ! (2):       ( rmomu(u) , p1vec(v) ) 
                   &               + fact5 * wgrvi(jnode,igaus)              &                ! (3) + (6): ( mu d^2ui/dxj^2 + dmu/dxj dui/dxj, vi )
                   &               + fact6 * wgrgr(inode,jnode,igaus)                         ! (7):       ( mu dui/dxj , dvi/dxj ) 

              elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact1 * rcont(1,jnode,igaus) + fact4  ! Auu_xx
              elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact2 * rcont(1,jnode,igaus)          ! Auu_yx
              elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact1 * rcont(2,jnode,igaus)          ! Auu_xy
              elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact2 * rcont(2,jnode,igaus) + fact4  ! Auu_yy

           end do

        end do
     end do

  else

     do igaus = 1,pgaus

        fact0 = ( gpsp2(igaus) - xvis2 * gpvis(igaus) ) * gpvol(igaus)                        ! (1) + (5)
        fact6 = gpvis(igaus) * gpvol(igaus)

        do inode = 1,pnode
           idof1 = 3*inode-2
           idof2 = idof1+1
           idof3 = idof2+1

           fact1 = fact0*gpcar(1,inode,igaus) 
           fact2 = fact0*gpcar(2,inode,igaus)
           fact3 = fact0*gpcar(3,inode,igaus)
           fact5 = gpsha(inode,igaus)*gpvol(igaus)

           do jnode = 1,pnode              
              jdof1              = 3*jnode-2
              jdof2              = jdof1+1
              jdof3              = jdof2+1

              fact4              =   p1vec(inode,igaus)*rmomu(jnode,igaus) &                  ! (2):       ( rmomu(u) , p1vec(v) ) 
                   &               + fact5*wgrvi(jnode,igaus)&                                ! (3) + (6): ( mu*d^2ui/dxk^2 + grad(mu).grad(u), v )
                   &               + fact6*wgrgr(inode,jnode,igaus)                           ! (7):       ( mu*dui/dxk , dv/dxk ) 

              elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact1 * rcont(1,jnode,igaus) + fact4  ! Auu_xx
              elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact2 * rcont(1,jnode,igaus)          ! Auu_yx
              elauu(idof3,jdof1) = elauu(idof3,jdof1) + fact3 * rcont(1,jnode,igaus)          ! Auu_zx
              elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact1 * rcont(2,jnode,igaus)          ! Auu_xy
              elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact2 * rcont(2,jnode,igaus) + fact4  ! Auu_yy
              elauu(idof3,jdof2) = elauu(idof3,jdof2) + fact3 * rcont(2,jnode,igaus)          ! Auu_zy
              elauu(idof1,jdof3) = elauu(idof1,jdof3) + fact1 * rcont(3,jnode,igaus)          ! Auu_xz
              elauu(idof2,jdof3) = elauu(idof2,jdof3) + fact2 * rcont(3,jnode,igaus)          ! Auu_yz
              elauu(idof3,jdof3) = elauu(idof3,jdof3) + fact3 * rcont(3,jnode,igaus) + fact4  ! Auu_zz
           end do

        end do
     end do

  end if

  if( fvins_nsi > 0.9_rp ) then

     if( ndime == 2 ) then

        do igaus = 1,pgaus

           fact4 = gpgvi(1,igaus) * gpvol(igaus)
           fact5 = gpgvi(2,igaus) * gpvol(igaus)
           fact7 = gpvis(igaus)   * gpvol(igaus)
!           factx = xvis2 * fact4
!           facty = xvis2 * fact5

           do inode = 1,pnode
              idof1 = 2*inode-1
              idof2 = idof1+1
              fact0 = fact7 * gpsha(inode,igaus) *(1.0_rp-xvis2)
              fact1 = fact4 * gpsha(inode,igaus)
              fact2 = fact5 * gpsha(inode,igaus)
              facx1 = fact1 * xvis2
              facy1 = fact2 * xvis2 
!              facx1 = factx * gpsha(inode,igaus)
!              facy1 = facty * gpsha(inode,igaus)

             
              do jnode = 1,pnode              
                 jdof1              = 2*jnode-1
                 jdof2              = jdof1+1
                 !
                 !                                         (4)  ( mu duj/dxi , dvi/dxj )
                 elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact7 * gpcar(1,inode,igaus) * gpcar(1,jnode,igaus)         ! Auu_xx
                 elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact7 * gpcar(2,inode,igaus) * gpcar(1,jnode,igaus)         ! Auu_xy
                 elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact7 * gpcar(1,inode,igaus) * gpcar(2,jnode,igaus)         ! Auu_yx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact7 * gpcar(2,inode,igaus) * gpcar(2,jnode,igaus)         ! Auu_yy
                 !
                 !                                         (10) - 2/3 * B * ( dmu/dxi (div u) , vi )
                 elauu(idof1,jdof1) = elauu(idof1,jdof1) - facx1 * gpcar(1,jnode,igaus)                                ! Auu_xx
                 elauu(idof1,jdof2) = elauu(idof1,jdof2) - facx1 * gpcar(2,jnode,igaus)                                ! Auu_xy
                 elauu(idof2,jdof1) = elauu(idof2,jdof1) - facy1 * gpcar(1,jnode,igaus)                                ! Auu_yx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) - facy1 * gpcar(2,jnode,igaus)                                ! Auu_yy
                 !
                 !                                         (9)  ( dmu/dxj duj/dxi , vi ) + (8)  ( mu d^2uj/dxidxj , vi )
                 !                                               + (11)   - 2/3 * B * ( mu d(div u)/dxi , vi )
                 elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact1 * gpcar(1,jnode,igaus) + fact0 * gphes(1,jnode,igaus) ! Auu_xx
                 elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact2 * gpcar(1,jnode,igaus) + fact0 * gphes(3,jnode,igaus) ! Auu_xy
                 elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact1 * gpcar(2,jnode,igaus) + fact0 * gphes(3,jnode,igaus) ! Auu_yx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact2 * gpcar(2,jnode,igaus) + fact0 * gphes(2,jnode,igaus) ! Auu_yy
                 !
                 !                                        
       
              end do

           end do
        end do

     else

        do igaus = 1,pgaus

           fact4 = gpgvi(1,igaus) * gpvol(igaus)
           fact5 = gpgvi(2,igaus) * gpvol(igaus)
           fact6 = gpgvi(3,igaus) * gpvol(igaus)
           fact7 = gpvis(igaus)   * gpvol(igaus)
 !          factx = xvis2 * fact4
 !          facty = xvis2 * fact5
 !          factz = xvis2 * fact6

           do inode = 1,pnode
              idof1 = 3*inode-2
              idof2 = idof1+1
              idof3 = idof2+1
              fact0 = fact7 * gpsha(inode,igaus)*(1.0_rp-xvis2)
              fact1 = fact4 * gpsha(inode,igaus)
              fact2 = fact5 * gpsha(inode,igaus)
              fact3 = fact6 * gpsha(inode,igaus)
              facx1 = fact1 * xvis2
              facy1 = fact2 * xvis2
              facz1 = fact3 * xvis2

!              facx1 = factx * gpsha(inode,igaus)
!              facy1 = facty * gpsha(inode,igaus)
!              facz1 = factz * gpsha(inode,igaus)
             
              do jnode = 1,pnode              
                 jdof1              = 3*jnode-2
                 jdof2              = jdof1+1
                 jdof3              = jdof2+1
                 !
                 !                                         (4)  ( mu duj/dxi , dvi/dxj )
                 elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact7 * gpcar(1,inode,igaus) * gpcar(1,jnode,igaus)         ! Auu_xx
                 elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact7 * gpcar(2,inode,igaus) * gpcar(1,jnode,igaus)         ! Auu_xy
                 elauu(idof1,jdof3) = elauu(idof1,jdof3) + fact7 * gpcar(3,inode,igaus) * gpcar(1,jnode,igaus)         ! Auu_xz

                 elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact7 * gpcar(1,inode,igaus) * gpcar(2,jnode,igaus)         ! Auu_yx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact7 * gpcar(2,inode,igaus) * gpcar(2,jnode,igaus)         ! Auu_yy
                 elauu(idof2,jdof3) = elauu(idof2,jdof3) + fact7 * gpcar(3,inode,igaus) * gpcar(2,jnode,igaus)         ! Auu_yz

                 elauu(idof3,jdof1) = elauu(idof3,jdof1) + fact7 * gpcar(1,inode,igaus) * gpcar(3,jnode,igaus)         ! Auu_zx
                 elauu(idof3,jdof2) = elauu(idof3,jdof2) + fact7 * gpcar(2,inode,igaus) * gpcar(3,jnode,igaus)         ! Auu_zy
                 elauu(idof3,jdof3) = elauu(idof3,jdof3) + fact7 * gpcar(3,inode,igaus) * gpcar(3,jnode,igaus)         ! Auu_zz
                 !
                 !                                         (10) - 2/3 * B * ( dmu/dxi (div u) , vi )
                 elauu(idof1,jdof1) = elauu(idof1,jdof1) - facx1 * gpcar(1,jnode,igaus)                                ! Auu_xx
                 elauu(idof1,jdof2) = elauu(idof1,jdof2) - facx1 * gpcar(2,jnode,igaus)                                ! Auu_xy
                 elauu(idof1,jdof3) = elauu(idof1,jdof3) - facx1 * gpcar(3,jnode,igaus)                                ! Auu_xz

                 elauu(idof2,jdof1) = elauu(idof2,jdof1) - facy1 * gpcar(1,jnode,igaus)                                ! Auu_yx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) - facy1 * gpcar(2,jnode,igaus)                                ! Auu_yy
                 elauu(idof2,jdof3) = elauu(idof2,jdof3) - facy1 * gpcar(3,jnode,igaus)                                ! Auu_yz

                 elauu(idof3,jdof1) = elauu(idof3,jdof1) - facz1 * gpcar(1,jnode,igaus)                                ! Auu_zx
                 elauu(idof3,jdof2) = elauu(idof3,jdof2) - facz1 * gpcar(2,jnode,igaus)                                ! Auu_zy
                 elauu(idof3,jdof3) = elauu(idof3,jdof3) - facz1 * gpcar(3,jnode,igaus)                                ! Auu_zz
                 !
                 !                                         (9)  ( dmu/dxj duj/dxi , vi ) + (8)  ( mu d^2uj/dxidxj , vi )
                 !                                                + (11)   - 2/3 * B * ( mu d(div u)/dxi , vi )
                 elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact1 * gpcar(1,jnode,igaus) + fact0 * gphes(1,jnode,igaus) ! Auu_xx
                 elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact2 * gpcar(1,jnode,igaus) + fact0 * gphes(4,jnode,igaus) ! Auu_xy
                 elauu(idof1,jdof3) = elauu(idof1,jdof3) + fact3 * gpcar(1,jnode,igaus) + fact0 * gphes(5,jnode,igaus) ! Auu_xz

                 elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact1 * gpcar(2,jnode,igaus) + fact0 * gphes(4,jnode,igaus) ! Auu_yx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact2 * gpcar(2,jnode,igaus) + fact0 * gphes(2,jnode,igaus) ! Auu_yy
                 elauu(idof2,jdof3) = elauu(idof2,jdof3) + fact3 * gpcar(2,jnode,igaus) + fact0 * gphes(6,jnode,igaus) ! Auu_yz

                 elauu(idof3,jdof1) = elauu(idof3,jdof1) + fact1 * gpcar(3,jnode,igaus) + fact0 * gphes(5,jnode,igaus) ! Auu_zx
                 elauu(idof3,jdof2) = elauu(idof3,jdof2) + fact2 * gpcar(3,jnode,igaus) + fact0 * gphes(6,jnode,igaus) ! Auu_zy
                 elauu(idof3,jdof3) = elauu(idof3,jdof3) + fact3 * gpcar(3,jnode,igaus) + fact0 * gphes(3,jnode,igaus) ! Auu_zz

              end do

           end do
        end do
            
     end if

  end if

  if( fcons_nsi .gt. 0.1_rp ) then   
     
     if( ndime == 2 ) then

        do igaus = 1,pgaus

           fact0 = fcons_nsi * gpden(igaus) * gpvol(igaus)

           do inode = 1,pnode
              idof1 = 2*inode-1
              idof2 = idof1+1

             
              do jnode = 1,pnode              
                 jdof1              = 2*jnode-1
                 jdof2              = jdof1+1

                 fact1 = (gpadv(1,igaus) * gpcar(1,inode,igaus) + gpadv(2,igaus) * gpcar(2,inode,igaus)) * gpsha(jnode,igaus) * fact0
                 fact2 = (gpadv(1,igaus) * gpcar(1,jnode,igaus) + gpadv(2,igaus) * gpcar(2,jnode,igaus)) * gpsha(inode,igaus) * fact0

                 elauu(idof1,jdof1) = elauu(idof1,jdof1) - fact1 - fact2                      ! Auu_xx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) - fact1 - fact2                      ! Auu_yy

              end do

           end do

        end do

     else

        do igaus = 1,pgaus

           fact0 = fcons_nsi * gpden(igaus) * gpvol(igaus)

           do inode = 1,pnode
              idof1 = 3*inode-2
              idof2 = idof1+1
              idof3 = idof2+1
             
              do jnode = 1,pnode              
                 jdof1              = 3*jnode-2
                 jdof2              = jdof1+1
                 jdof3              = jdof2+1

                 fact1 = (gpadv(1,igaus) * gpcar(1,inode,igaus) + gpadv(2,igaus) * gpcar(2,inode,igaus) + gpadv(3,igaus) * gpcar(3,inode,igaus)) * gpsha(jnode,igaus) * fact0
                 fact2 = (gpadv(1,igaus) * gpcar(1,jnode,igaus) + gpadv(2,igaus) * gpcar(2,jnode,igaus) + gpadv(3,igaus) * gpcar(3,jnode,igaus)) * gpsha(inode,igaus) * fact0

                 elauu(idof1,jdof1) = elauu(idof1,jdof1) - fact1 - fact2                      ! Auu_xx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) - fact1 - fact2                      ! Auu_yy
                 elauu(idof3,jdof3) = elauu(idof3,jdof3) - fact1 - fact2                      ! Auu_zz
              end do

           end do

        end do
            
     end if

  end if
     

  !----------------------------------------------------------------------
  !
  ! Auu: Off-diagonal terms
  !
  !----------------------------------------------------------------------

  if( kfl_rmom2_nsi /= 0 ) then
     !
     ! p1vec * rmom2
     !
     do igaus = 1,pgaus
        do inode = 1,pnode
           do jnode = 1,pnode              
              do idime = 1,ndime
                 idof1 = (inode-1)*ndime+idime
                 do jdime = 1,ndime
                    jdof1 = (jnode-1)*ndime+jdime
                    elauu(idof1,jdof1) = elauu(idof1,jdof1) &
                         + p1vec(inode,igaus)*rmom2(idime,jdime,jnode,igaus)
                 end do
              end do
           end do
        end do
     end do

     if( kfl_p1ve2_nsi /= 0 ) then
        !
        ! p1ve2 * rmomu
        ! p1ve2 * rmom2
        !
        do igaus=1,pgaus
           do inode=1,pnode
              idofv=(inode-1)*ndime
              do idime=1,ndime
                 idofv=idofv+1                 
                 do jnode=1,pnode
                    jdofv=(jnode-1)*ndime
                    do jdime=1,ndime
                       jdofv=jdofv+1
                       elauu(idofv,jdofv)=elauu(idofv,jdofv)    &
                            +p1ve2(idime,jdime,inode,igaus)     &
                            *rmomu(jnode,igaus)
                       do kdime = 1,ndime
                          elauu(idofv,jdofv)=elauu(idofv,jdofv) &
                               +p1ve2(idime,kdime,inode,igaus)  &
                               *rmom2(kdime,jdime,jnode,igaus)
                       end do
                    end do
                 end do
              end do
           end do
        end do
     end if
  end if
  !
  ! Bemol:
  ! - b * ( v , rho * (a.grad) u ) - b * ( u , rho * (a.grad) v ) - b ( rho*div(a) u , v )
  !
  if( abs(bemol_nsi).gt.1.0e-9_rp ) then

     if( ndime == 2 ) then

        do igaus = 1,pgaus

           fact0 = bemol_nsi * gpden(igaus) * gpvol(igaus)
           fact5 = fact0 * ( gpgve(1,1,igaus) + gpgve(2,2,igaus) )

           do inode = 1,pnode
              idof1 = 2 * inode - 1
              idof2 = idof1 + 1

              do jnode = 1,pnode              
                 jdof1 = 2 * jnode - 1
                 jdof2 = jdof1 + 1
                 !
                 ! fact2 = - b * ( v , rho * (a.grad) u )
                 ! fact3 = - b * ( u , rho * (a.grad) v ) 
                 ! fact4 = - b * ( rho * div(a) u , v ) 
                 !
                 fact2 = ( gpadv(1,igaus) * gpcar(1,inode,igaus) + gpadv(2,igaus) * gpcar(2,inode,igaus) ) * gpsha(jnode,igaus)
                 fact3 = ( gpadv(1,igaus) * gpcar(1,jnode,igaus) + gpadv(2,igaus) * gpcar(2,jnode,igaus) ) * gpsha(inode,igaus)
                 fact4 = fact5 * gpsha(inode,igaus) * gpsha(jnode,igaus) 
                 fact2 = fact0 * fact2
                 fact3 = fact0 * fact3

                 elauu(idof1,jdof1) = elauu(idof1,jdof1) - fact2 - fact3 - fact4
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) - fact2 - fact3 - fact4

              end do

           end do

        end do

     else

        do igaus = 1,pgaus

           fact0 = bemol_nsi * gpden(igaus) * gpvol(igaus)
           fact5 = fact0 * ( gpgve(1,1,igaus) + gpgve(2,2,igaus) + gpgve(3,3,igaus))

           do inode = 1,pnode
              idof1 = 3* inode - 2
              idof2 = idof1 + 1
              idof3 = idof2 + 1

              do jnode = 1,pnode
                 jdof1 = 3 * jnode - 2
                 jdof2 = jdof1 + 1
                 jdof3 = jdof2 + 1
                 !
                 ! fact2 = - b * ( v , rho * (a.grad) u )
                 ! fact3 = - b * ( u , rho * (a.grad) v ) 
                 ! fact4 = - b * ( rho * div(a) u , v ) 
                 !
                 fact2 = ( gpadv(1,igaus) * gpcar(1,inode,igaus) + gpadv(2,igaus) * gpcar(2,inode,igaus) + gpadv(3,igaus) * gpcar(3,inode,igaus)) * gpsha(jnode,igaus)
                 fact3 = ( gpadv(1,igaus) * gpcar(1,jnode,igaus) + gpadv(2,igaus) * gpcar(2,jnode,igaus) + gpadv(3,igaus) * gpcar(3,jnode,igaus)) * gpsha(inode,igaus)
                 fact4 = fact5 * gpsha(inode,igaus) * gpsha(jnode,igaus)
                 fact2 = fact0 * fact2
                 fact3 = fact0 * fact3

                 elauu(idof1,jdof1) = elauu(idof1,jdof1) - fact2 - fact3 - fact4
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) - fact2 - fact3 - fact4
                 elauu(idof3,jdof3) = elauu(idof3,jdof3) - fact2 - fact3 - fact4

              end do

           end do

        end do

     end if

  end if
  !
  ! Newton-Raphson
  !
  if( kfl_linea_nsi == 2 ) then
     if( ndime == 2 ) then
        do igaus = 1,pgaus
           do inode = 1,pnode
              idof1 = 2 * inode - 1
              idof2 = idof1 + 1
              fact0 = gpvol(igaus) * gpden(igaus) * gpsha(inode,igaus)
              do jnode = 1,pnode
                 jdof1 = 2 * jnode - 1
                 jdof2 = jdof1 + 1
                 fact1 = fact0 * gpsha(jnode,igaus)
                 ! Auu_1i
                 elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact1 * gpgve(1,1,igaus) ! rho * ux * dux^i-1/dx
                 elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact1 * gpgve(2,1,igaus) ! rho * uy * dux^i-1/dy
                 ! Auu_2i
                 elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact1 * gpgve(1,2,igaus) ! rho * ux * duy^i-1/dx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact1 * gpgve(2,2,igaus) ! rho * uy * duy^i-1/dy
              end do
           end do
        end do
     else
        do igaus = 1,pgaus
           do inode = 1,pnode
              idof1 = 3 * inode - 2
              idof2 = idof1 + 1
              idof3 = idof2 + 1
              fact0 = gpvol(igaus) * gpden(igaus) * gpsha(inode,igaus)
              do jnode = 1,pnode
                 jdof1 = 3 * jnode - 2
                 jdof2 = jdof1 + 1
                 jdof3 = jdof2 + 1                 
                 fact1 = fact0 * gpsha(jnode,igaus)
                 ! Auu_1i
                 elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact1 * gpgve(1,1,igaus) ! rho * ux * dux^i-1/dx
                 elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact1 * gpgve(2,1,igaus) ! rho * uy * dux^i-1/dy
                 elauu(idof1,jdof3) = elauu(idof1,jdof3) + fact1 * gpgve(3,1,igaus) ! rho * uz * dux^i-1/dz
                 ! Auu_2i
                 elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact1 * gpgve(1,2,igaus) ! rho * ux * duy^i-1/dx
                 elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact1 * gpgve(2,2,igaus) ! rho * uy * duy^i-1/dy
                 elauu(idof2,jdof3) = elauu(idof2,jdof3) + fact1 * gpgve(3,2,igaus) ! rho * uz * duy^i-1/dz
                 ! Auu_3i
                 elauu(idof3,jdof1) = elauu(idof3,jdof1) + fact1 * gpgve(1,3,igaus) ! rho * ux * duz^i-1/dx
                 elauu(idof3,jdof2) = elauu(idof3,jdof2) + fact1 * gpgve(2,3,igaus) ! rho * uy * duz^i-1/dy
                 elauu(idof3,jdof3) = elauu(idof3,jdof3) + fact1 * gpgve(3,3,igaus) ! rho * uz * duz^i-1/dz

              end do
           end do
        end do
     end if
  end if
  
  if (kfl_lumped==1) then ! lumped evolution matrix (only backward euler)
     do igaus =1, pgaus
        gpveo(1:3) = 0.0_rp
        do inode =1, pnode
           gpveo(1:3) = gpveo(1:3) + elvel(1:3, inode, 2)* gpsha(inode, igaus)
        end do
        do inode =1, pnode
           idof1 = 3 * inode - 2
           idof2 = idof1 + 1
           idof3 = idof2 + 1
           fact0 = gpvol(igaus) * gpden(igaus) * gpsha(inode,igaus)*dtinv_loc
           elauu(idof1,idof1) =  elauu(idof1,idof1) + fact0
           elauu(idof2,idof2) =  elauu(idof2,idof2) + fact0
           elauu(idof3,idof3) =  elauu(idof3,idof3) + fact0
           elrbu(1:3,inode)   =  elrbu(1:3,inode)   - fact0*gpveo(1:3)
           elrbu(1:3,inode)   =  elrbu(1:3,inode)   + fact0*elvel(1:3, inode, 2)
           do jnode =1, pnode 
              jdof1 = 3 * jnode - 2
              jdof2 = jdof1 + 1
              jdof3 = jdof2 + 1  
              elauu(idof1,jdof1) =  elauu(idof1,jdof1) - fact0*gpsha(jnode, igaus) 
              elauu(idof2,jdof2) =  elauu(idof2,jdof2) - fact0*gpsha(jnode, igaus) 
              elauu(idof3,jdof3) =  elauu(idof3,jdof3) - fact0*gpsha(jnode, igaus) 
           end do
        end do
     end do

  end if
  !
  ! Dual time step preconditioner
  !
  if( kfl_duatss == 1 ) then
     ellum = 0.0_rp 
     do igaus =1, pgaus
        do inode =1, pnode
           fact0 = gpvol(igaus) * gpden(igaus) * gpsha(inode,igaus) * dtinv_loc * real(fact_duatss-1,rp)
           ellum(inode) = ellum(inode) + fact0 
        end do
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! Aup
  !
  !----------------------------------------------------------------------
  !
  ! Pressure: - ( p , div(v)  ) + ( grad(p) , p1vec(v)-v ) ( if kfl_press_nsi = 1 )
  ! Pressure: - ( grad(p) , v ) + ( grad(p) , p1vec(v)-v ) ( if kfl_press_nsi = 0 )
  !  
  if( ndime == 2 ) then
     if( kfl_press_nsi == 1 ) then
        do igaus = 1,pgaus
           do jnode = 1,pnode
              fact2 = gpvol(igaus)*gpsha(jnode,igaus)
              do inode = 1,pnode
                 fact1              = -gpvol(igaus)*gpsha(inode,igaus)+p1vec(inode,igaus)
                 idofv              = (inode-1)*ndime+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) - fact2*gpcar(1,inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1*gpcar(1,jnode,igaus)
                 idofv              = idofv+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) - fact2*gpcar(2,inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1*gpcar(2,jnode,igaus)
              end do
           end do
        end do
     else
        do igaus = 1,pgaus
           do jnode = 1,pnode
              fact2 = gpvol(igaus)*gpsha(jnode,igaus)
              do inode = 1,pnode
                 fact1              = -gpvol(igaus)*gpsha(inode,igaus)+p1vec(inode,igaus)
                 idofv              = (inode-1)*ndime+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) + gpcar(1,jnode,igaus)*gpvol(igaus)*gpsha(inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1*gpcar(1,jnode,igaus)
                 idofv              = idofv+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) + gpcar(2,jnode,igaus)*gpvol(igaus)*gpsha(inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1*gpcar(2,jnode,igaus)
              end do
           end do
        end do
     end if
     if (kfl_p1ve2_nsi /=0 ) then
        do igaus = 1,pgaus
           do jnode = 1,pnode
              do inode = 1,pnode
                 idof1 = (inode-1)*ndime +1 
                 idof2 = idof1 +1

                 elaup(idof1,jnode) = elaup(idof1,jnode)+ &
                      gpcar(1,jnode,igaus)*p1ve2(1,1,inode,igaus) + &
                      gpcar(2,jnode,igaus)*p1ve2(1,2,inode,igaus)              
                 elaup(idof2,jnode) = elaup(idof2,jnode)+ &
                      gpcar(1,jnode,igaus)*p1ve2(2,1,inode,igaus) + &
                      gpcar(2,jnode,igaus)*p1ve2(2,2,inode,igaus)             
             
              end do
           end do
        end do
     end if
  else
     if( kfl_press_nsi == 1 ) then
        do igaus = 1,pgaus
           do jnode = 1,pnode
              fact2 = gpvol(igaus)*gpsha(jnode,igaus)
              do inode = 1,pnode
                 fact1              = -gpvol(igaus)*gpsha(inode,igaus)+p1vec(inode,igaus)
                 idofv              = (inode-1)*ndime+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) - fact2 * gpcar(1,inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1 * gpcar(1,jnode,igaus)
                 idofv              = idofv+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) - fact2 * gpcar(2,inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1 * gpcar(2,jnode,igaus)
                 idofv              = idofv+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) - fact2 * gpcar(3,inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1 * gpcar(3,jnode,igaus)
              end do
           end do
        end do
     else
        do igaus = 1,pgaus
           do jnode = 1,pnode
              fact2 = gpvol(igaus)*gpsha(jnode,igaus)
              do inode = 1,pnode
                 fact1              = -gpvol(igaus)*gpsha(inode,igaus)+p1vec(inode,igaus)
                 idofv              = (inode-1)*ndime+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) + gpcar(1,jnode,igaus)*gpvol(igaus)*gpsha(inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1 * gpcar(1,jnode,igaus)
                 idofv              = idofv+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) + gpcar(2,jnode,igaus)*gpvol(igaus)*gpsha(inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1 * gpcar(2,jnode,igaus)
                 idofv              = idofv+1
                 elaup(idofv,jnode) = elaup(idofv,jnode) + gpcar(3,jnode,igaus)*gpvol(igaus)*gpsha(inode,igaus)
                 elaup(idofv,jnode) = elaup(idofv,jnode) + fact1 * gpcar(3,jnode,igaus)
              end do
           end do
        end do
     end if
     if (kfl_p1ve2_nsi /=0 ) then
        do igaus = 1,pgaus
           do jnode = 1,pnode
              do inode = 1,pnode
                 idof1 = (inode-1)*ndime +1 
                 idof2 = idof1 +1
                 idof3 = idof1 +2
                 elaup(idof1,jnode) = elaup(idof1,jnode)+ &
                      gpcar(1,jnode,igaus)*p1ve2(1,1,inode,igaus) + &
                      gpcar(2,jnode,igaus)*p1ve2(1,2,inode,igaus) + &
                      gpcar(3,jnode,igaus)*p1ve2(1,3,inode,igaus) 
                 elaup(idof2,jnode) = elaup(idof2,jnode)+ &
                      gpcar(1,jnode,igaus)*p1ve2(2,1,inode,igaus) + &
                      gpcar(2,jnode,igaus)*p1ve2(2,2,inode,igaus) + &
                      gpcar(3,jnode,igaus)*p1ve2(2,3,inode,igaus)
                 elaup(idof3,jnode) = elaup(idof3,jnode)+ &
                      gpcar(1,jnode,igaus)*p1ve2(3,1,inode,igaus) + &
                      gpcar(2,jnode,igaus)*p1ve2(3,2,inode,igaus) + &
                      gpcar(3,jnode,igaus)*p1ve2(3,3,inode,igaus)
              end do
           end do
        end do
     end if
  end if

  !----------------------------------------------------------------------
  !
  ! Apu
  !
  !----------------------------------------------------------------------
  !
  ! ( div(u) , (tau2^{-1}*tau2')*q )   --- or ( div (rho u), q) if Low-Mach 
  ! + ( rho*(uc.grad)u + 2*rho*(w x u) + sig*u -div[2*mu*eps(u)], tau1' grad(q) )  (only laplacian form of div[2 mu eps(u)] )
  !
#ifdef matiaslma 
  ! integration by parts
  if (kfl_regim_nsi==3) then ! Penalization of pressure, never imposse pressure
     do igaus =1, pgaus
        penal = 1.0e-4_rp*gpden(igaus)/gpvis(igaus)
        do inode =1, pnode
           fact0 = penal*p2sca(inode,igaus)
           do jnode =inode +1, pnode
              fact1 = fact0*gpsha(jnode,igaus)
              elapp(inode, jnode) = elapp(inode, jnode) + fact1
              elapp(jnode, inode) = elapp(jnode, inode) + fact1
           end do
           elapp (inode, inode) = elapp (inode, inode) + fact0*gpsha(inode, igaus)
           elrbp (inode) = elrbp (inode) + fact0*gppre(igaus)
        end do
     end do
  end if
  if (ndime==2) then
     do igaus =1, pgaus
        do inode =1,pnode
           p2vec(1,inode,igaus)= p2vec(1,inode,igaus)*gpden(igaus)
           p2vec(2,inode,igaus)= p2vec(2,inode,igaus)*gpden(igaus)
           p2sca(inode, igaus) = p2sca(inode, igaus)*gpden(igaus)
        end do
        fact0 = gpden(igaus)* gpvol(igaus)
        do inode =1, pnode           
           idof1 = 2*inode-1
           idof2 = idof1+1
           fact1 = gpsha(inode,igaus)* fact0
           do jnode =1, pnode
              elapu(jnode, idof1) = elapu(jnode, idof1) -fact1             * gpcar(1,jnode,igaus)  &
                   &                                  + rmomu(inode,igaus) * p2vec(1,jnode,igaus)
              elapu(jnode, idof2) = elapu(jnode, idof2) -fact1             * gpcar(2,jnode,igaus)  &
                   &                                  + rmomu(inode,igaus) * p2vec(2,jnode,igaus)
           end do
        end do
     end do
  else !ndime =3
     do igaus =1, pgaus
        do inode =1,pnode
           p2vec(1,inode,igaus)= p2vec(1,inode,igaus)*gpden(igaus)
           p2vec(2,inode,igaus)= p2vec(2,inode,igaus)*gpden(igaus)
           p2vec(3,inode,igaus)= p2vec(3,inode,igaus)*gpden(igaus)
           p2sca(inode, igaus) = p2sca(inode, igaus) *gpden(igaus)
        end do
        fact0 = gpden(igaus)* gpvol(igaus)
        do inode =1, pnode
           idof1 = 3*inode - 2 
           idof2 = idof1+1
           idof3 = idof2+1
           fact1 = gpsha(inode,igaus)* fact0
           do jnode =1, pnode 
              elapu(jnode, idof1) = elapu(jnode, idof1) -   fact1          * gpcar(1,jnode,igaus) &
                   &                                  + rmomu(inode,igaus) * p2vec(1,jnode,igaus)
              elapu(jnode, idof2) = elapu(jnode, idof2) -  fact1            *gpcar(2,jnode,igaus) &
                   &                                  + rmomu(inode,igaus) * p2vec(2,jnode,igaus)
              elapu(jnode, idof3) = elapu(jnode, idof3) -  fact1           * gpcar(3,jnode,igaus) &
                   &                                  + rmomu(inode,igaus) * p2vec(3,jnode,igaus)
           end do
        end do
     end do
  end if

#else

  if( ndime == 2 ) then
     do igaus = 1,pgaus
        do inode = 1,pnode
           idof1 = 2*inode-1
           idof2 = idof1+1
           do jnode = 1,pnode
              elapu(jnode,idof1) = elapu(jnode,idof1) + rcont(1,inode,igaus) * p2sca(jnode,igaus)&
                   &                                  + rmomu(inode,igaus)   * p2vec(1,jnode,igaus)
              elapu(jnode,idof2) = elapu(jnode,idof2) + rcont(2,inode,igaus) * p2sca(jnode,igaus)&
                   &                                  + rmomu(inode,igaus)   * p2vec(2,jnode,igaus)
           end do
        end do
     end do
  else
     do igaus = 1,pgaus
        do inode = 1,pnode
           idof1 = 3*inode-2
           idof2 = idof1+1
           idof3 = idof2+1
           do jnode = 1,pnode
              elapu(jnode,idof1) = elapu(jnode,idof1) + rcont(1,inode,igaus) * p2sca(jnode,igaus)&
                   &                                  + rmomu(inode,igaus)   * p2vec(1,jnode,igaus)
              elapu(jnode,idof2) = elapu(jnode,idof2) + rcont(2,inode,igaus) * p2sca(jnode,igaus)&
                   &                                  + rmomu(inode,igaus)   * p2vec(2,jnode,igaus)
              elapu(jnode,idof3) = elapu(jnode,idof3) + rcont(3,inode,igaus) * p2sca(jnode,igaus)&
                   &                                  + rmomu(inode,igaus)   * p2vec(3,jnode,igaus)
           end do
        end do
     end do
  end if
#endif
  if( kfl_rmom2_nsi /= 0 ) then
     do igaus = 1,pgaus
        do inode = 1,pnode
           do jnode = 1,pnode
              jdofv = (jnode-1)*ndime
              do jdime = 1,ndime
                 jdofv = jdofv + 1
                 do kdime = 1,ndime
                    elapu(inode,jdofv) = elapu(inode,jdofv) + rmom2(kdime,jdime,jnode,igaus) * p2vec(kdime,inode,igaus)
                 end do
              end do
           end do
        end do
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! App
  !
  !----------------------------------------------------------------------
  !
  ! Pressure: ( grad(p) , tau1' grad(q) )
  ! 
  if( ndime == 2 ) then
     do igaus=1,pgaus
        do inode=1,pnode
           do jnode=inode+1,pnode
              fact1 =  p2vec(1,jnode,igaus) * gpcar(1,inode,igaus)&
                   & + p2vec(2,jnode,igaus) * gpcar(2,inode,igaus)
              elapp(jnode,inode) = elapp(jnode,inode) + fact1
              elapp(inode,jnode) = elapp(inode,jnode) + fact1
           end do
           fact1 =  p2vec(1,inode,igaus) * gpcar(1,inode,igaus)&
                & + p2vec(2,inode,igaus) * gpcar(2,inode,igaus)
           elapp(inode,inode) = elapp(inode,inode) + fact1
        end do
     end do
  else
     do igaus=1,pgaus
        do inode=1,pnode
           do jnode=inode+1,pnode
              fact1 =  p2vec(1,jnode,igaus) * gpcar(1,inode,igaus)&
                   & + p2vec(2,jnode,igaus) * gpcar(2,inode,igaus)&
                   & + p2vec(3,jnode,igaus) * gpcar(3,inode,igaus)
              elapp(jnode,inode) = elapp(jnode,inode) + fact1
              elapp(inode,jnode) = elapp(inode,jnode) + fact1
           end do
           fact1 =  p2vec(1,inode,igaus) * gpcar(1,inode,igaus)&
                & + p2vec(2,inode,igaus) * gpcar(2,inode,igaus)&
                & + p2vec(3,inode,igaus) * gpcar(3,inode,igaus)
           elapp(inode,inode) = elapp(inode,inode) + fact1
        end do
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! bu and bp
  !
  !----------------------------------------------------------------------
  !
  ! bu = ( f , p1vec(v) ) 
  ! bp = ( f , tau1' grad(q) )
  !
  if( ndime == 2 ) then
     do igaus = 1,pgaus
        gprhs_sgs(1,igaus) = gprhs_sgs(1,igaus) + taupr(igaus) * gpvep(1,igaus)
        gprhs_sgs(2,igaus) = gprhs_sgs(2,igaus) + taupr(igaus) * gpvep(2,igaus)
        gprhh(1,igaus)     = gprhs(1,igaus)     + gprhs_sgs(1,igaus)
        gprhh(2,igaus)     = gprhs(2,igaus)     + gprhs_sgs(2,igaus)
        do inode = 1,pnode
           elrbu(1,inode) = elrbu(1,inode) + p1vec(inode,igaus)   * gprhh(1,igaus)
           elrbu(2,inode) = elrbu(2,inode) + p1vec(inode,igaus)   * gprhh(2,igaus)
           elrbp(inode)   = elrbp(inode)   + p2vec(1,inode,igaus) * gprhh(1,igaus) &
                &                          + p2vec(2,inode,igaus) * gprhh(2,igaus) 
        end do
     end do
  else
     do igaus = 1,pgaus
        gprhs_sgs(1,igaus) = gprhs_sgs(1,igaus) + taupr(igaus) * gpvep(1,igaus)
        gprhs_sgs(2,igaus) = gprhs_sgs(2,igaus) + taupr(igaus) * gpvep(2,igaus)
        gprhs_sgs(3,igaus) = gprhs_sgs(3,igaus) + taupr(igaus) * gpvep(3,igaus)
        gprhh(1, igaus)    = gprhs(1,igaus)     + gprhs_sgs(1,igaus)
        gprhh(2, igaus)    = gprhs(2,igaus)     + gprhs_sgs(2,igaus)
        gprhh(3, igaus)    = gprhs(3,igaus)     + gprhs_sgs(3,igaus)
        do inode = 1,pnode
           elrbu(1,inode) = elrbu(1,inode) + p1vec(inode,igaus)   * gprhh(1,igaus)
           elrbu(2,inode) = elrbu(2,inode) + p1vec(inode,igaus)   * gprhh(2,igaus)
           elrbu(3,inode) = elrbu(3,inode) + p1vec(inode,igaus)   * gprhh(3,igaus)
           elrbp(inode)   = elrbp(inode)   + p2vec(1,inode,igaus) * gprhh(1,igaus) &
                &                          + p2vec(2,inode,igaus) * gprhh(2,igaus) &
                &                          + p2vec(3,inode,igaus) * gprhh(3,igaus)
        end do
     end do
  end if
  if( kfl_p1ve2_nsi /= 0 ) then
     do igaus = 1,pgaus
        do inode = 1,pnode
           do idime = 1,ndime
              do kdime = 1,ndime
                 elrbu(idime,inode) = elrbu(idime,inode) &
                      + p1ve2(idime,kdime,inode,igaus) * gprhh(kdime,igaus)
              end do
           end do
        end do
     end do
  end if
  !
  ! low Mach regime: we add to the right hand side the residue of the continuity eq
  !
  if( ndime == 2 ) then
     do igaus = 1,pgaus
        fact1 =  gpsp2(igaus) * gprh2(igaus) * gpvol(igaus)
        do inode = 1,pnode
           elrbu(1,inode) = elrbu(1,inode) + fact1 * gpcar(1,inode,igaus)        ! ( rhs, tau2' * div(v) )
           elrbu(2,inode) = elrbu(2,inode) + fact1 * gpcar(2,inode,igaus)
           elrbp(inode)   = elrbp(inode)   + gprhc(igaus) * p2sca(inode,igaus)   ! ( rhs , q)
        end do
     enddo
  else
     do igaus = 1,pgaus
        fact1 = gpsp2(igaus) * gprh2(igaus) * gpvol(igaus) 
        do inode = 1,pnode
           elrbu(1,inode) = elrbu(1,inode) + fact1 * gpcar(1,inode,igaus)
           elrbu(2,inode) = elrbu(2,inode) + fact1 * gpcar(2,inode,igaus)
           elrbu(3,inode) = elrbu(3,inode) + fact1 * gpcar(3,inode,igaus)
           elrbp(inode)   = elrbp(inode)   + gprhc(igaus) * p2sca(inode,igaus)
        end do
     enddo
  end if
  !
  ! Newton-Raphson
  !
  if( kfl_linea_nsi == 2 ) then
     if( ndime == 2 ) then
        do igaus = 1,pgaus
           fact0 =  gpden(igaus) * gpvol(igaus) 
           do inode = 1,pnode
              fact1 = fact0 * gpsha(inode,igaus)
              elrbu(1,inode) = elrbu(1,inode) + fact1 * ( gpadv(1,igaus) * gpgve(1,1,igaus) + gpadv(2,igaus) * gpgve(2,1,igaus) )
              elrbu(2,inode) = elrbu(2,inode) + fact1 * ( gpadv(1,igaus) * gpgve(1,2,igaus) + gpadv(2,igaus) * gpgve(2,2,igaus) )
           end do
        end do
     else
        do igaus = 1,pgaus
           fact0 =  gpden(igaus) * gpvol(igaus) 
           do inode = 1,pnode
              fact1 = fact0 * gpsha(inode,igaus)
              elrbu(1,inode) = elrbu(1,inode) & 
                   + fact1 * ( gpadv(1,igaus) * gpgve(1,1,igaus) + gpadv(2,igaus) * gpgve(2,1,igaus) &
                   + gpadv(3,igaus) * gpgve(3,1,igaus) )
              elrbu(2,inode) = elrbu(2,inode) &
                   + fact1 * ( gpadv(1,igaus) * gpgve(1,2,igaus) + gpadv(2,igaus) * gpgve(2,2,igaus) &
                   + gpadv(3,igaus) * gpgve(3,2,igaus) )
              elrbu(3,inode) = elrbu(3,inode) &
                   + fact1 * ( gpadv(1,igaus) * gpgve(1,3,igaus) + gpadv(2,igaus) * gpgve(2,3,igaus) &
                   + gpadv(3,igaus) * gpgve(3,3,igaus) )
           end do
        end do
     end if
  end if
  !
  ! Projection: ( L*(v) , P )
  !
  if( kfl_stabi_nsi == 1 ) then

     if( ndime == 2 ) then

        do igaus = 1, pgaus
           do inode = 1,pnode
              elrbu(1,inode) = elrbu(1,inode) - gprhs_sgs(1, igaus)*gpsha(inode, igaus)*gpvol(igaus)
              elrbu(2,inode) = elrbu(2,inode) - gprhs_sgs(2, igaus)*gpsha(inode, igaus)*gpvol(igaus)
           end do
        end do
!!$        do igaus = 1,pgaus 
!!$           fact1 = gpden(igaus) * dtsgs_nsi
!!$           fact2 = gpvol(igaus) / gpst1(igaus)
!!$           factx = fact1 * gpsgs(1,igaus,2) + gpvep(1,igaus) / gpst1(igaus) ! rho/dt * u'^n + tau^-1 P
!!$           facty = fact1 * gpsgs(2,igaus,2) + gpvep(2,igaus) / gpst1(igaus) 
!!$           facx1 = fact2 * gpvep(1,igaus)                                   ! tau^-1 P
!!$           facy1 = fact2 * gpvep(2,igaus)
!!$           do inode = 1,pnode
!!$              elrbu(1,inode) = elrbu(1,inode) + p1vec(inode,igaus)   * factx - gpsha(inode,igaus) * facx1
!!$              elrbu(2,inode) = elrbu(2,inode) + p1vec(inode,igaus)   * facty - gpsha(inode,igaus) * facy1
!!$              elrbp(inode)   = elrbp(inode)   + p2vec(1,inode,igaus) * factx + p2vec(2,inode,igaus) * facty 
!!$           end do
!!$        end do

     else
        do igaus = 1, pgaus
           do inode = 1,pnode
              elrbu(1,inode) = elrbu(1,inode) - gprhs_sgs(1, igaus)*gpsha(inode, igaus)*gpvol(igaus)
              elrbu(2,inode) = elrbu(2,inode) - gprhs_sgs(2, igaus)*gpsha(inode, igaus)*gpvol(igaus)
              elrbu(3,inode) = elrbu(3,inode) - gprhs_sgs(3, igaus)*gpsha(inode, igaus)*gpvol(igaus)
           end do
        end do
!!$        do igaus = 1,pgaus 
!!$           fact1 = gpden(igaus) * dtsgs_nsi
!!$           fact2 = gpvol(igaus) / gpst1(igaus)
!!$           factx = fact1 * gpsgs(1,igaus,2) + gpvep(1,igaus) / gpst1(igaus) ! rho/dt * u'^n + tau^-1 P
!!$           facty = fact1 * gpsgs(2,igaus,2) + gpvep(2,igaus) / gpst1(igaus) 
!!$           factz = fact1 * gpsgs(3,igaus,2) + gpvep(3,igaus) / gpst1(igaus) 
!!$           facx1 = fact2 * gpvep(1,igaus)                                   ! tau^-1 P
!!$           facy1 = fact2 * gpvep(2,igaus)
!!$           facz1 = fact2 * gpvep(3,igaus)
!!$           do inode = 1,pnode
!!$              elrbu(1,inode) = elrbu(1,inode) + p1vec(inode,igaus)   * factx - gpsha(inode,igaus) * facx1
!!$              elrbu(2,inode) = elrbu(2,inode) + p1vec(inode,igaus)   * facty - gpsha(inode,igaus) * facy1
!!$              elrbu(3,inode) = elrbu(3,inode) + p1vec(inode,igaus)   * factz - gpsha(inode,igaus) * facz1
!!$              elrbp(inode)   = elrbp(inode)   + p2vec(1,inode,igaus) * factx &
!!$                   &                          + p2vec(2,inode,igaus) * facty &
!!$                   &                          + p2vec(3,inode,igaus) * factz 
!!$           end do
!!$        end do

     end if
  end if

end subroutine nsi_elmmat
