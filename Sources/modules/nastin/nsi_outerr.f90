subroutine nsi_outerr(itask)
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_outerr
  ! NAME 
  !    nsi_outerr
  ! DESCRIPTION
  !    This routine checks if there are errros and warnings
  ! USES
  ! USED BY
  !    nsi_turnon
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_kermod
  use def_domain
  use def_nastin
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ierro=0,iwarn=0,iboun,ipoin,idime,ibopo,ipara
  integer(ip)             :: iroty,dumm1,dumm2,ivara,ivar1,ivar2
  character(20)           :: messa
  character(200)          :: wmess

  select case ( itask )

  case( 1_ip ) 
     !
     ! Set postprocess
     !
     if( neset == 0 .and. maxval(postp(1) % npp_setse) > 0 ) then
        iwarn = iwarn + 1
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'ELEMENT SETS HAVE NOT BEEN DEFINED: CANNOT OUTPUT ELEMENT SET RESULTS')
     end if
     if( nbset == 0 .and. maxval(postp(1) % npp_setsb) > 0 ) then
        iwarn = iwarn + 1
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'BOUNDARY SETS HAVE NOT BEEN DEFINED: CANNOT OUTPUT BOUNDARY SET RESULTS')
     end if
     if( nnset == 0 .and. maxval(postp(1) % npp_setsn) > 0 ) then
        iwarn = iwarn + 1
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'NODE SETS HAVE NOT BEEN DEFINED: CANNOT OUTPUT NODE SET RESULTS')
     end if
     !
     ! Check the transient evolution
     !
     if( kfl_timei /= 0 ) then
        if( kfl_timei_nsi == 0 ) then
           iwarn = iwarn + 1
           call outfor(2_ip,momod(modul)%lun_outpu,&
                'STEADY NAVIER STOKES EQUATIONS IN A TRANSIENT CALCULATION')
        end if
     end if
     !
     ! Coupling with levels
     !
     if( kfl_colev_nsi /= 0 .and. kfl_modul(ID_LEVELS) == 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'LEVELS MODULE SHOULD BE PUT TO ON')     
     end if
     !
     ! Dirichlet Matrix needed when there are periodic nodes
     !
     if( kfl_matdi_nsi /= 1 .and. nperi /= 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'DIRICLET MATRIX NEEDED WHEN THERE ARE PERIODIC NODES')     
     end if
     !
     ! Boussinesq without temperature
     !
     if( kfl_cotem_nsi==1 .and. kfl_modul(ID_TEMPER) == 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'BOUSSINESQ COUPLING IS IMPOSSIBLE IF TEMPER MODULE IS NOT SOLVED')
     end if
     !
     ! Boussinesq without gravity vector
     !
     if( kfl_cotem_nsi==1 ) then
        if( &
             abs(gravb_nsi(    1))<zensi .and. &
             abs(gravb_nsi(    2))<zensi .and. &
             abs(gravb_nsi(ndime))<zensi ) then
           ierro = ierro + 1
           call outfor(1_ip,momod(modul)%lun_outpu,&
                'BOUSSINESQ COUPLING REQUIRES TO DEFINE THE GRAVITY VECTOR')
        end if
     end if
     !
     ! Low Mach without temperature
     !
     if( kfl_regim_nsi==3 .and. kfl_modul(ID_TEMPER) == 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'LOW MACH REGIME IS IMPOSSIBLE IF TEMPER MODULE IS NOT SOLVED')
     end if
     !
     ! Low Mach with only one Gauss point
     !
     if( kfl_regim_nsi==3 .and. mgaus == 1 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'LOW MACH REGIME WITH 1 GP: NOT CODED')
     end if
     !
     ! Low Mach only developed with KERMOD
     !
     if( kfl_regim_nsi==3 .and. kfl_prope == 0 ) then
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'WARNING: LOW MACH REGIME ONLY TESTED WITH PROPERTIES FROM KERNEL (KERMOD)')
     end if
     !
     ! Turbulence without solving TURBUL
     !
     if( kfl_cotur_nsi==1 .and. kfl_modul(4) == 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'TURBULENCE COUPLING IS IMPOSSIBLE IF TURBUL MODULE IS NOT SOLVED')
     end if
     !
     ! Turbulence without convective term
     !
     if( kfl_cotur_nsi /= 0 .and. kfl_advec_nsi == 0 ) then
        iwarn = iwarn + 1
        kfl_advec_nsi=1
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'TURBULENCE MODELING REQUIRES A CONVECTIVE TERM: IT WAS TURNED ON AUTOMATICALLY')
     end if
     !
     ! Turbulence without viscous term
     !
     if( kfl_cotur_nsi /= 0 .and. kfl_visco_nsi == 0 ) then
        iwarn = iwarn + 1
        kfl_visco_nsi=1
        fvins_nsi=1.0_rp
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'TURBULENCE MODELING REQUIRES A VISCOUS TERM: IT WAS TURNED ON AUTOMATICALLY')
     end if
     !
     ! Turbulence with Laplacian viscous term
     !
     if( kfl_cotur_nsi /= 0 .and. fvins_nsi == 0.0_rp ) then
        iwarn = iwarn + 1
        kfl_visco_nsi=1
        !fvins_nsi=1.0_rp
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'TURBULENCE MODELING REQUIRES THE VISCOUS TERM IN DIVERGENCE FORM. SHOULD BE CHANGED IN NSI-DAT FILE')
     end if
     if( kfl_cotur_nsi == 0 .and. kfl_grtur_nsi /= 0 ) then
        iwarn = iwarn + 1
        kfl_grtur_nsi=0
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'CANNOT INCLUDE -2/3*rho*K TERM IF TURBULENCE MODELING IS OFF')
     end if
     !
     ! Wall law conditions with zero wall distance
     !
     if( delta_nsi<=zensi .and. INOTMASTER  .and. kfl_delta /= 1 ) then
        boundaries: do iboun=1,nboun
           if( kfl_fixbo_nsi(iboun)==3 ) then
              iwarn = iwarn + 1
              call outfor(2_ip,momod(modul)%lun_outpu,&
                   'WALL DISTANCE IS ZERO: WALL LAW CONDITION IS REPLACED BY A SLIP CONDITION')
              exit boundaries
           end if
        end do boundaries
     end if
     !
     ! Time integration scheme and accuracy
     !
     if( kfl_timei_nsi==1 .and. kfl_tisch_nsi==1 .and. kfl_tiacc_nsi>2 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'WRONG TIME INTEGRATION ORDER USING TRAPEZOIDAL RULE. MUST BE 1 OR 2.')
     end if
     if( kfl_timei_nsi==1 .and. kfl_tisch_nsi==2 ) then   ! BDF
        if ( kfl_timco > 1 ) then
           ierro = ierro + 1
           call outfor(1_ip,momod(modul)%lun_outpu,&
                'BDF MAKES NO SENSE WITH LOCAL TIME STEPS')
        end if
        if ( kfl_timco /= 0 .and. kfl_tiacc_nsi > 2 ) then
           ierro = ierro + 1
           call outfor(1_ip,momod(modul)%lun_outpu,&
                'BDF OF ORDER HIGHER THAN 2 ONLY READY FOR PRESCRIBED TIME STEP')
        end if
     end if
     !
     ! Time tracking and integration scheme
     !
     if( kfl_timei_nsi==1 .and. kfl_sgsti_nsi /= 0 .and. kfl_tisch_nsi==2 .and. kfl_sgsac_nsi/=1) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'CANNOT TRACK THE SUBGRID SCALES WITH A BDF SCHEME AND SECOND ORDER ACCURACY')
     end if
     !
     ! Time tracking of the subscales
     !
     if( kfl_timei_nsi == 0 .and. kfl_sgsti_nsi /= 0 ) then
        iwarn = iwarn + 1
        kfl_sgsti_nsi=0
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'CANNOT TRACK THE SUBGRID SCALES IN TIME FOR STATIONARY PROBLEM')
     end if
     !
     ! Convection tracking of the subscales
     !
     if( kfl_advec_nsi == 0 .and. kfl_sgsco_nsi /= 0 ) then
        iwarn = iwarn + 1
        kfl_sgsco_nsi=0
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'CANNOT TRACK THE SUBGRID SCALES IN CONVECTION FOR STOKES PROBLEM')
     end if

     if( kfl_sgsli_nsi == 2 .and.  kfl_stabi_nsi /= 0  ) then
        !ierro = ierro + 1
        !call outfor(1_ip,momod(modul)%lun_outpu,&
        !     'NEWTON RAPHSON LINEARIZATION OF SUBGRID SCALES ONLY READY WITH ASGS')
     end if
     if( kfl_sgsli_nsi == 2 .and.  kfl_taust_nsi /= 1  ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,&
             'NEWTON RAPHSON LINEARIZATION OF SUBGRID SCALES ONLY READY WITH CODINA TAU')
     end if
     !
     ! Non-inertial boundary conditions
     !
     if( INOTMASTER ) then
        if( kfl_local_nsi==1 ) then
           nodes1: do ipoin=1,npoin
              ibopo=lpoty(ipoin)
              if( ibopo /= 0 ) then
                 if( kfl_fixno_nsi(1,ipoin)==9 .and. kfl_fixrs_nsi(ibopo) /= 0 ) then
                    ierro = ierro + 1
                    call outfor(1_ip,momod(modul)%lun_outpu,&
                         'CANNOT PRESCRIBE NON-INERTIAL DIRICHLET BOUNDARY CONDITION IN LOCAL AXES.'&
                         //'CHECK NODE '//intost(ipoin))
                    exit nodes1          
                 end if
              end if
           end do nodes1
        end if
        nodes2: do ipoin=1,npoin
           dumm1=0
           dumm2=0
           do idime=1,ndime        
              if( kfl_fixno_nsi(idime,ipoin)==9 ) then
                 dumm1=1
              else
                 dumm2=1
              end if
           end do
           if( dumm1 /= 0 .and. dumm2 /= 0 ) then
              ierro = ierro + 1
              call outfor(1_ip,momod(modul)%lun_outpu,&
                   'WHEN PRESCRIBING A NON-INERTIAL DIRICHLET BOUNDARY, ALL VELOCITY D.O.F'&
                   //char(13)//' MUST HAVE THIS CONDITION. CHECK NODE '//intost(ipoin))
              exit nodes2
           else if( dumm1==1 .and. kfl_conbc_nsi == 0 ) then
              if( kfl_funno_nsi(ipoin) /= 0 ) then
                 ierro = ierro + 1
                 call outfor(1_ip,momod(modul)%lun_outpu,&
                      'CANNOT APPLY A TIME FUNCTION TO A NON-INERTIAL DIRICHLET BOUNDARY,'&
                      //' CHECK NODE '//intost(ipoin))
                 exit nodes2
              end if
           else if( dumm1==1 .and. kfl_fvfua_nsi /= 0 .and. kfl_conbc_nsi==1 ) then
              ierro = ierro + 1
              call outfor(1_ip,momod(modul)%lun_outpu,&
                   'NON-INERTIAL BOUNDARY CONDITIONS WITH A TIME DEPENDENT ROTATION CANNOT BE CONSTANT')
              exit nodes2
           end if
        end do nodes2
     end if
     !
     ! Solver
     !
     if(      (solve(1)%nkryd == 0 .and. solve(1)%kfl_algso==8)&
          .or.(solve(1)%nkryd == 0 .and. solve(1)%kfl_algso==8) ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,'KRYLOV DIOMENSION MUST BE  > 0 WHEN USING GMRES SOLVER')
     end if
     !
     ! Laplacian preconditioning
     !
     if( (kfl_predi_nsi==1.or.kfl_predi_nsi==2) .and. kfl_timei_nsi == 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,'LAPLACIAN PRECONDITIONING ONLY FOR TRANSIENT PROBLEM')     
     end if
     !
     ! Fast assembly
     !
     if( kfl_assem_nsi==1 ) then
        if( corio_nsi > zeror ) then
           ierro = ierro + 1
           call outfor(1_ip,momod(modul)%lun_outpu,'FAST ASSEMBLY INCOMPATIBLE WITH ROTATION')
        end if
        if( fvins_nsi==2.0_rp ) then
           iwarn = iwarn + 1
           call outfor(1_ip,momod(modul)%lun_outpu,&
                'FAST ASSEMBLY INCOMPATIBLE WITH COMPLETE FORM OF VISCOUS TERM.'//&
                ' SWITCHED TO DIVERGENCE FORM AUTOMATICALLY')                  
        end if
     end if
     !
     !  Local bc
     !
     if(  INOTMASTER  ) then
        do ipoin = 1,npoin
           ibopo = lpoty(ipoin)
           if( ibopo > 0 ) then
              iroty = kfl_fixrs_nsi(ibopo)
              if( iroty < -3 .or. iroty > nskew ) then
                 ierro = ierro + 1
                 wmess = 'LOCAL BC''S IMPOSED ON NODE'
                 messa = intost(ipoin)
                 wmess = trim(wmess)//' '//trim(messa)//' FOR WRONG SKEW SYSEM'
                 messa = intost(iroty)
                 wmess = trim(wmess)//' '//trim(messa)
                 call outfor(1_ip,momod(modul)%lun_outpu,trim(wmess))
              end if
           end if
        end do
     end if
     !
     ! Stabilization parameters
     !
     do ipara = 1,size(staco_nsi)
        if( staco_nsi(ipara) /= 0.0_rp .and. staco_nsi(ipara) /= 1.0_rp  ) then
           iwarn = iwarn+1
           wmess = 'CHECK NAVIER-STOKES STABILIZATION PARAMETERS'       
           call outfor(2_ip,momod(modul)%lun_outpu,trim(wmess))
        end if
     end do
     !
     ! Comfort postprocess
     !
     ivar1 = 8
     call posdef(25_ip,ivar1)
     ivar2 = 9
     call posdef(25_ip,ivar2)
     if( ivar1 /= 0 .or. ivar2 /= 0 ) then
        if(&
             cloth_nsi == -500.0_rp .or. & 
             metab_nsi == -500.0_rp .or. & 
             wetme_nsi == -500.0_rp .or. & 
             ambie_nsi == -500.0_rp .or. & 
             radia_nsi == -500.0_rp .or. & 
             relat_nsi == -500.0_rp ) then
           ierro = ierro+1
           wmess = 'COMFORT CONSTANT WERE NOT DEFINED: CANNOT POSTPROCESS PMV NOR PPD'
           call outfor(1_ip,momod(modul)%lun_outpu,trim(wmess))
        end if
     end if
     !
     ! Schur complement solver
     !
     if( NSI_SCHUR_COMPLEMENT .and. kfl_sosch_nsi == 1  ) then
        iwarn = iwarn + 1
        kfl_sosch_nsi = 2
        call outfor(2_ip,momod(modul)%lun_outpu,'ORTHOMIN(1) IS EITHER MOMENTUM OR CONTINUITY PRESERVING')     
     end if
     !
     ! Boundary conditions
     !
     if( kfl_matdi_nsi == 1 .and. NSI_MONOLITHIC ) then
        iwarn = iwarn + 1
        kfl_matdi_nsi = 0
        call outfor(2_ip,momod(modul)%lun_outpu,'DIRICHLET ONLY ON ELEMENT WHEN DING MONOLITHIC')     
     end if

     !if (nodpr_nsi > npoin .and. INOTMASTER ) then
     !   ierro = ierro + 1
     !   call outfor(2_ip,momod(modul)%lun_outpu,'PRESSURE FIXED ON A NON-EXISTING NODE')
     !endif
     !
     ! Fast assembly+monolithic
     !
     !if(kfl_assem_nsi==1.and.NSI_MONOLITHIC ) then
     !   ierro = ierro + 1
     !   wmess='FAST ASSEMBLY NOT COMPATIBLE WITH MONOLITHIC APPROACH'
     !   call outfor(1_ip,momod(modul)%lun_outpu,trim(wmess))     
     !end if
     if( kfl_normc_nsi == 4 .and. kfl_refer_nsi == 0 ) then
        ierro = ierro + 1
        call outfor(1_ip,momod(modul)%lun_outpu,'REFERENCE RESIDUAL NORM ONLY POSSIBLE IF REFERENCE SOLUTION IS OUTPUT')
     end if
     !
     ! Internal force
     !
     if( kfl_intfo_nsi == 1 .and. kfl_matdi_nsi == 0 ) then
        ierro = ierro + 1
        call outfor(2_ip,momod(modul)%lun_outpu,&
             'REAIDUAL INTERNAL FORCE ONLY IF DIRICHLET BC ARE IMPOSED DIRECTLY IN MATRICES')
     end if
     !
     ! Decoupled SGS and OSS-like stabilizations
     !     
     if( kfl_sgscp_nsi == 1 .and. kfl_stabi_nsi /= 0 ) then
        ierro = ierro + 1
        call outfor(2_ip,momod(modul)%lun_outpu,'OSS-LIKE STABILIZATION INCOMPATBILE WITH COUPLED SGS')        
     end if
     !
     ! Properties
     !
     if( kfl_prope == 0 ) then
        ierro = ierro + 1
        call outfor(2_ip,momod(modul)%lun_outpu,'PROPERTIES SHOULD BE DECALRED IN KERMOD')        
     end if
     !
     ! Hydrostatic pressure
     !
     if( kfl_inipr_nsi == 2 .and. kfl_hydro_nsi == 0 ) then
        ierro = ierro + 1
        call outfor(2_ip,momod(modul)%lun_outpu,'HYDROSTATIC PRESSURE MUST BE COMPUTED TO IMPOSE IT AS INITIAL SOLUTION')        
     end if

     !----------------------------------------------------------------------
     !
     ! Postprocess
     !
     !----------------------------------------------------------------------
     !
     ! DCG groups
     !
     ivara = 15
     call posdef(25_ip,ivara)
     if( ivara /= 0 .and. solve(2)%kfl_algso /= 2 ) then
        call posdef(26_ip,ivara)
        iwarn = iwarn+1
        call outfor(2_ip,momod(modul)%lun_outpu,'CANNOT POSTPROCESS GROUPS IF DEFLATED CG SOLVER WAS NOT CHOSEN')
     end if
     ivara = 16
     call posdef(25_ip,ivara)
     if( ivara /= 0 .and. solve(2)%kfl_preco /= 4 ) then
        call posdef(26_ip,ivara)
        iwarn = iwarn+1
        call outfor(2_ip,momod(modul)%lun_outpu,'CANNOT POSTPROCESS LINELET IF LINELET PRECONDITIONER WAS NOT CHOSEN')
     end if
     !
     ! Streamlines
     !
     ivara = 3
     call posdef(25_ip,ivara)
     if( ivara /= 0 .and.ndime /= 2 ) then
        call posdef(26_ip,ivara)
        iwarn = iwarn+1
        call outfor(2_ip,momod(modul)%lun_outpu,'CANNOT POSTPROCESS STREAMLINES IN 3D')
     end if
     !
     ! Projection
     !
     ivara = 23
     call posdef(25_ip,ivara)
     if( ivara /= 0 .and. kfl_stabi_nsi == 0 ) then
        call posdef(26_ip,ivara)
        iwarn = iwarn + 1
        call outfor(2_ip,momod(modul)%lun_outpu,'CANNOT POSTPROCESS PROJECTION')
     end if
     ivara = 24
     call posdef(25_ip,ivara)
     if( ivara /= 0 .and. kfl_stabi_nsi == 0 ) then
        call posdef(26_ip,ivara)
        iwarn = iwarn + 1
        call outfor(2_ip,momod(modul)%lun_outpu,'CANNOT POSTPROCESS PROJECTION')
     end if
     !
     ! Hydrostatic pressure
     !
     !ivara = 31
     !call posdef(25_ip,ivara)
     !if( ivara /= 0 .and. kfl_outpr_nsi == 0 .and. kfl_nohyd_nsi == 0 ) then
     !   call posdef(26_ip,ivara)
     !   iwarn = iwarn + 1
     !   call outfor(2_ip,momod(modul)%lun_outpu,'CANNOT POSTPROCESS HYDROSTATIC PRESSURE')
     !end if
     !
     ! Internal force
     !
     ivara = 50
     call posdef(25_ip,ivara)
     if( ivara /= 0 .and. kfl_intfo_nsi == 0 ) then
        call posdef(26_ip,ivara)
        iwarn = iwarn + 1
        call outfor(2_ip,momod(modul)%lun_outpu,'CANNOT POSTPROCESS INTERNAL FORCE')
     end if

     !iwarn = 0
     !ierro = 0

  end select

  !----------------------------------------------------------------------
  !
  ! ERROR MESSAGE
  !
  !----------------------------------------------------------------------

  call errors(3_ip,ierro,iwarn,'MENSAJE VACIO???? ')

end subroutine nsi_outerr
