subroutine nsi_outinf(itask)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_outinf
  ! NAME 
  !    nsi_outinf
  ! DESCRIPTION
  !    This routine writes on the incompressible Navier-Stokes files
  ! USED BY
  !    nsi_turnon
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_solver
  use def_nastin
  use def_kermod, only       :  gasco
  implicit none
  integer(ip), intent(in) :: itask
  character(100)          :: equat
  character(9)            :: lvisc
  integer(ip)             :: ierhs

  if( INOTSLAVE ) then

     select case(itask)

     case(1)
        !
        ! Write information in Result file
        !
        if(kfl_rstar/=2) then   
           equat=''
           if(kfl_timei_nsi==1) equat=trim(equat)//'rho*du/dt'
           if(kfl_advec_nsi>=1) then
              !
              ! Convection term
              !
              if(int(fcons_nsi)==0) then
                 equat=trim(equat)//' + rho*(u.grad)u'
              else if(abs(fcons_nsi-0.5)<=zensi) then
                 equat=trim(equat)//' + rho*[(u.grad)u+1/2*u(div u)]'
              else 
                 equat=trim(equat)//' + rho*[(u.grad)u+u(div u)]'
              end if
           end if
           if(kfl_visco_nsi/=0) then
              !
              ! Diffusion term
              !
              lvisc='mu'
              if(kfl_cotur_nsi/=0) lvisc='(mu+mu_t)'
              if(int(fvins_nsi)==0) then
                 equat=trim(equat)//' - div['//trim(lvisc)//'grad(u)] '
              else if(int(fvins_nsi)==1) then
                 equat=trim(equat)//' - div[2*'//trim(lvisc)//'*eps(u)] '
              else
                 equat=trim(equat)//' - div[2*'//trim(lvisc)//'*(eps(u)-1/3*div(u) I)] '
              end if
           end if
           if(fvnoa_nsi>zensi) then
              !
              ! Rotation term
              !
              equat=trim(equat)//' + 2*rho*w x u '
           end if

           equat=trim(equat)//' + grad(p)'
           equat=trim(equat)//' = '
           ierhs=0
           if(grnor_nsi>zensi)  then
              ierhs=1
              equat=trim(equat)//' + rho*g'
           end if
           if(fvnoa_nsi>zensi) then
              equat=trim(equat)//' - rho*w x w x (r-r0) -rho*dw/dt x (r-r0) '
           end if
           if(fvnol_nsi>zensi) then
              equat=trim(equat)//' - rho*a' 
           end if
           if(kfl_cotem_nsi/=0) then
              ierhs=1
              equat=trim(equat)//' - rho*g*beta*(T-Tref)'
           end if
           if(ierhs==0) equat=trim(equat)//'0 '

           call outfor(25_ip,momod(modul) % lun_outpu,'DIFFERENTIAL EQUATION')
           write(momod(modul) % lun_outpu,110) trim(equat)
           if(kfl_regim_nsi==1) then
              write(momod(modul) % lun_outpu,113) '1/(RT)*dp/dt + 1/(RT)*u.grad(p) + rho*div(u) = rho/T*[ dT/dt + u.grad(T) ]'
           else if(kfl_regim_nsi==3) then
              write(momod(modul) % lun_outpu,113) 'rho*div(u) = rho/T*[ dT/dt + u.grad(T) ] - rho/p0*dp0/dt'
              if(kfl_prthe_nsi==1) then
                 write(momod(modul) % lun_outpu,113) 'p0(t) = p0(0)*[int_V 1/T(0) dV]/[int_V 1/T(t) dV]'
              else
                 write(momod(modul) % lun_outpu,113) &
                      'V/gam*dp0/dt + p0*[int u.n ds] = (gam-1)/gam*[ (int_S q.n ds) + (int_V Q dV) ]'
              end if
           else
              write(momod(modul) % lun_outpu,113) 'rho*div(u)=0'
           end if
           !write(momod(modul) % lun_outpu,112)
           !write(momod(modul) % lun_outpu,111) 'rho [ M / L^3 ]     = ',densi_nsi(1,1)
           !write(momod(modul) % lun_outpu,111) 'mu  [ M / L T ]     = ',visco_nsi(1,1)
           !if(kfl_regim_nsi>=1) then
           !   write(momod(modul) % lun_outpu,111) 'R   [ L^2 / T^2.K ] = ',gasco
           !   write(momod(modul) % lun_outpu,111) 'Cp  [ L^2 / T^2 K ] = ',sphea_nsi
           !   write(momod(modul) % lun_outpu,111) 'gam=Cp/(Cp-R)       = ',gamth_nsi
           !end if
           write(momod(modul) % lun_outpu,*)
           call flush(momod(modul) % lun_outpu)
        end if

     case(2)

     end select
  end if
  !
  ! Formats
  !
110 format(/,10x,a)
112 format(//,&
       & 10x,'Physical properties: ',/,&
       & 10x,'------------------------------------------------------------ ')
111 format(&
       & 10x,a,10(1x,e12.6))
113 format(&
       & 10x,a)
104 format(&
       '#',/,&
       '# CONTINUITY EQ:',/,&
       '# --------------')
105 format(&
       '#',/,&
       '# MOMENTUM EQS:',/,&
       '# -------------')

end subroutine nsi_outinf
      
