subroutine nsi_output()
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_output
  ! NAME 
  !    nsi_output
  ! DESCRIPTION
  !    End of a NASTIN time step 
  !    ITASK=0 ... When timemarching is true. There is output or
  !                post-process of results if required.
  !    ITASK=1 ... When timemarching is false. Output and/or post-process
  !                of results is forced if they have not been written 
  !                previously
  ! USES
  !    nsi_outvar
  !    postpr
  !    nsi_outbcs
  !    nsi_outset
  !    nsi_exaerr
  ! USED BY
  !    nsi_endste (itask=1)
  !    nsi_turnof (itask=2)
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use mod_postpr

  implicit none
  integer(ip) :: ivari,ivarp,ii,devis(10)
  integer(ip) :: kfl_devis

#ifdef EVENT
  call mpitrace_user_function(1)
#endif
  !
  ! Check if density and/or viscosity are needed
  !  
  kfl_devis = 0
  devis     = 0
  devis(1)  = 7  ! rho
  devis(2)  = 11 ! Tw
  devis(3)  = 17 ! mu
  devis(4)  = 25 ! y+
  devis(5)  = 30 ! Pe
  ii        = 1

  do while( ii < 10 .and. devis(ii) /= 0 )
     ivari = devis(ii)
     call posdef(11_ip,ivari)
     if( ivari /= 0 ) kfl_devis = 1
     ii = ii + 1
  end do

  if( kfl_devis == 1 ) then
     call nsi_memphy(4_ip)
     call nsi_denvis()
  end if
  !
  ! Postprocess variables
  !
  do ivarp = 1,nvarp
     ivari = ivarp
     call posdef(11_ip,ivari)
     call nsi_outvar(ivari)
  end do

  if( postp(1) % npp_stepi(21) /= 0 ) then
     if( mod(ittim, postp(1) % npp_stepi(21) ) == 0 ) then     ! AVVEL frequency
        avtim_nsi = cutim  ! Update reference time for time-averaging
     endif
  endif

  if( kfl_devis == 1 ) then
     call nsi_memphy(-4_ip)
  end if

  if( ittyp == ITASK_INITIA .or. ittyp == ITASK_ENDTIM ) then
     !
     ! Calculations on sets
     !
     call nsi_outset()
     !
     ! Postprocess on witness points
     !
     call nsi_outwit()
     !
     ! End of the run
     !
     call nsi_exaerr(2_ip)

  else if( ittyp == ITASK_ENDRUN ) then

  end if

#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine nsi_output
