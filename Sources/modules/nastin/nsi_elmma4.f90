subroutine nsi_elmma4(&
     pnode,pgaus,pevat,gpden,gpvis,gppor,gpsp1,gpsp2,&
     gpvol,gpsha,gpcar,gpadv,gpvep,gpprp,gpgrp,gprhs,&
     gpvel,gpsgs,wgrgr,agrau,elvel,elauu,elaup,elapp,&
     elapu,elrbu,elrbp,dtinv,dtsgs)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_elmma4
  ! NAME 
  !    nsi_elmma4
  ! DESCRIPTION
  !    Compute element matrix and RHS
  ! USES
  ! USED BY
  !    nsi_elmop3
  !***
  !----------------------------------------------------------------------- 
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,mnode,lnods
  use def_nastin, only       :  kfl_cotur_nsi,kfl_stabi_nsi,&
       &                        kfl_sgsti_nsi,kfl_limit_nsi,&
       &                        fvins_nsi,nbdfp_nsi,pabdf_nsi
  implicit none
  integer(ip), intent(in)    :: pnode,pgaus,pevat
  real(rp),    intent(in)    :: gpden(pgaus),gpvis(pgaus)
  real(rp),    intent(in)    :: gppor(pgaus)
  real(rp),    intent(in)    :: gpsp1(pgaus)
  real(rp),    intent(in)    :: gpsp2(pgaus)
  real(rp),    intent(in)    :: gpvol(pgaus),gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)    :: gpadv(ndime,pgaus)
  real(rp),    intent(inout) :: gpvep(ndime,pgaus)
  real(rp),    intent(inout) :: gpprp(pgaus)      
  real(rp),    intent(inout) :: gpgrp(ndime,pgaus)
  real(rp),    intent(inout) :: gprhs(ndime,pgaus)
  real(rp),    intent(in)    :: gpvel(ndime,pgaus,*)
  real(rp),    intent(in)    :: gpsgs(ndime,pgaus,*)
  real(rp),    intent(out)   :: wgrgr(pnode,pnode,pgaus)
  real(rp),    intent(out)   :: agrau(pnode,pgaus)
  real(rp),    intent(in)    :: elvel(ndime,pnode)
  real(rp),    intent(out)   :: elauu(pnode*ndime,pnode*ndime)
  real(rp),    intent(out)   :: elaup(pnode*ndime,pnode)
  real(rp),    intent(out)   :: elapp(pnode,pnode)
  real(rp),    intent(out)   :: elapu(pnode,pnode*ndime)
  real(rp),    intent(out)   :: elrbu(ndime,pnode)
  real(rp),    intent(out)   :: elrbp(pnode)
  real(rp),    intent(in)    :: dtinv
  real(rp),    intent(in)    :: dtsgs
  integer(ip)                :: inode,jnode,idofn,jdofn,idofv,jdof2,jdof3
  integer(ip)                :: idof1,idof3,idof2,igaus,idime,jdof1,jdofv,itime
  real(rp)                   :: fact0,fact1,fact2,fact3,fact4,fact5,fact6
  real(rp)                   :: fact7,fact8,c1,c2,c3,c4,alpha,beta

  !----------------------------------------------------------------------
  !
  ! Initialization
  !
  !----------------------------------------------------------------------

  do inode = 1,pnode 
     elrbp(inode) = 0.0_rp
     do idime = 1,ndime
        elrbu(idime,inode) = 0.0_rp
     end do
     do jnode = 1,pnode
        elapp(jnode,inode) = 0.0_rp
     end do
  end do
  do idofn = 1,pevat
     do jdofn = 1,pevat
        elauu(jdofn,idofn) = 0.0_rp
     end do
     do jnode = 1,pnode
        elaup(idofn,jnode) = 0.0_rp
        elapu(jnode,idofn) = 0.0_rp
     end do
  end do

  !----------------------------------------------------------------------
  !
  ! Test functions
  !
  !----------------------------------------------------------------------

  !
  ! AGRAU = rho * (a.grad) Ni
  ! WGRGR = grad(Ni) . grad(Nj)
  !
  if( ndime == 2 ) then

     do igaus = 1,pgaus
        do inode = 1,pnode
           agrau(inode,igaus) =  gpden(igaus) * (                    &
                &                gpadv(1,igaus)*gpcar(1,inode,igaus) &
                &              + gpadv(2,igaus)*gpcar(2,inode,igaus) )
           do jnode = 1,pnode
              wgrgr(inode,jnode,igaus) = &
                   &             gpcar(1,inode,igaus)*gpcar(1,jnode,igaus) &
                   &           + gpcar(2,inode,igaus)*gpcar(2,jnode,igaus) 
           end do
        end do
     end do

  else

     do igaus = 1,pgaus
        do inode = 1,pnode
           agrau(inode,igaus) =  gpden(igaus) * (                    &
                &                gpadv(1,igaus)*gpcar(1,inode,igaus) &
                &              + gpadv(2,igaus)*gpcar(2,inode,igaus) &
                &              + gpadv(3,igaus)*gpcar(3,inode,igaus) )
           do jnode = 1,pnode
              wgrgr(inode,jnode,igaus) = &
                   &             gpcar(1,inode,igaus)*gpcar(1,jnode,igaus) &
                   &           + gpcar(2,inode,igaus)*gpcar(2,jnode,igaus) & 
                   &           + gpcar(3,inode,igaus)*gpcar(3,jnode,igaus) 
           end do
        end do
     end do

  end if

  !----------------------------------------------------------------------
  !
  ! Auu
  !
  !----------------------------------------------------------------------

  !
  ! Galerkin + ( tau2 * div(u) , div(v) ) + ( tau1 * rho*a.grad(u), rho*a.grad(v) )
  !
  if( ndime == 2 ) then

     do igaus = 1,pgaus

        fact0 = gpsp2(igaus) * gpvol(igaus)
        fact6 = gpvis(igaus) * gpvol(igaus)
        fact7 = gpsp1(igaus) * gpvol(igaus) 
        fact8 = gpden(igaus) * dtinv + gppor(igaus)

        do inode = 1,pnode

           idof1 = 2*inode-1
           idof2 = idof1+1

           fact1 = fact0 * gpcar(1,inode,igaus)      ! div(u) * tau2' * dv/dx
           fact2 = fact0 * gpcar(2,inode,igaus)      ! div(u) * tau2' * dv/dy
           fact4 = gpsha(inode,igaus) * gpvol(igaus)
           
           do jnode = 1,pnode    

              jdof1 = 2*jnode-1
              jdof2 = jdof1+1

              fact5 = fact4 * ( agrau(jnode,igaus) + fact8 * gpsha(jnode,igaus) ) &          ! ( rho/dt N_j + s Nj + rho*(a.grad)Nj ) Ni
                   +  fact6 * wgrgr(inode,jnode,igaus) &                                     ! mu * grad(Ni) . grad(Nj)
                   +  fact7 * agrau(jnode,igaus) * agrau(inode,igaus)                        ! tau1 * rho*(a.grad)Nj * rho*(a.grad)Ni

              elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact1 * gpcar(1,jnode,igaus) + fact5
              elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact2 * gpcar(1,jnode,igaus)
              elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact1 * gpcar(2,jnode,igaus) 
              elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact2 * gpcar(2,jnode,igaus) + fact5

           end do

        end do
     end do

  else

     do igaus = 1,pgaus

        fact0 = gpsp2(igaus) * gpvol(igaus)
        fact6 = gpvis(igaus) * gpvol(igaus)
        fact7 = gpsp1(igaus) * gpvol(igaus)
        fact8 = gpden(igaus) * dtinv + gppor(igaus)

        do inode = 1,pnode

           idof1 = 3 * inode - 2
           idof2 = idof1     + 1
           idof3 = idof2     + 1

           fact1 = fact0 * gpcar(1,inode,igaus)      ! div(u) * tau2' * dv/dx
           fact2 = fact0 * gpcar(2,inode,igaus)      ! div(u) * tau2' * dv/dy
           fact3 = fact0 * gpcar(3,inode,igaus)      ! div(u) * tau2' * dv/dz
           fact4 = gpsha(inode,igaus) * gpvol(igaus)

           do jnode = 1,pnode    

              jdof1 = 3 * jnode - 2
              jdof2 = jdof1     + 1
              jdof3 = jdof2     + 1

              fact5 = fact4 * ( agrau(jnode,igaus) + fact8 * gpsha(jnode,igaus) ) &          ! ( rho/dt N_j + s Nj + rho*(a.grad)Nj ) Ni
                   +  fact6 * wgrgr(inode,jnode,igaus) &                                     ! mu * grad(Ni) . grad(Nj)
                   +  fact7 * agrau(jnode,igaus) * agrau(inode,igaus)                        ! t1 * rho*(a.grad)Nj * rho*(a.grad)Ni

              elauu(idof1,jdof1) = elauu(idof1,jdof1) + fact1 * gpcar(1,jnode,igaus) + fact5
              elauu(idof2,jdof1) = elauu(idof2,jdof1) + fact2 * gpcar(1,jnode,igaus)
              elauu(idof3,jdof1) = elauu(idof3,jdof1) + fact3 * gpcar(1,jnode,igaus)

              elauu(idof1,jdof2) = elauu(idof1,jdof2) + fact1 * gpcar(2,jnode,igaus) 
              elauu(idof2,jdof2) = elauu(idof2,jdof2) + fact2 * gpcar(2,jnode,igaus) + fact5
              elauu(idof3,jdof2) = elauu(idof3,jdof2) + fact3 * gpcar(2,jnode,igaus) 

              elauu(idof1,jdof3) = elauu(idof1,jdof3) + fact1 * gpcar(3,jnode,igaus) 
              elauu(idof2,jdof3) = elauu(idof2,jdof3) + fact2 * gpcar(3,jnode,igaus)
              elauu(idof3,jdof3) = elauu(idof3,jdof3) + fact3 * gpcar(3,jnode,igaus) + fact5

           end do

        end do
     end do

  end if

  if( fvins_nsi > 0.9_rp ) then
     !
     ! ( mu*duj/dxi , dv/dxj ) (only div form)
     !
     if( ndime == 2 ) then
        do igaus = 1,pgaus
           do inode = 1,pnode
              idofv = (inode-1)*ndime
              do idime = 1,ndime
                 idofv = idofv+1
                 do jnode = 1,pnode
                    jdofv = (jnode-1)*ndime
                    fact1 = gpvis(igaus) * gpvol(igaus) * gpcar(idime,jnode,igaus)     
                    jdofv = jdofv + 1
                    elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(1,inode,igaus)
                    jdofv = jdofv + 1
                    elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(2,inode,igaus)
                 end do
                 if( fvins_nsi == 2.0_rp ) then
                    fact1 = -2.0_rp/3.0_rp * gpvis(igaus) * gpvol(igaus) * gpcar(idime,inode,igaus)
                    do jnode = 1,pnode
                       jdofv = (jnode-1)*ndime   
                       jdofv = jdofv+1
                       elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(1,jnode,igaus)
                       jdofv = jdofv+1
                       elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(2,jnode,igaus)
                    end do
                 end if
              end do
           end do
        end do
     else
        do igaus = 1,pgaus
           do inode = 1,pnode
              idofv = (inode-1)*ndime
              do idime = 1,ndime
                 idofv = idofv + 1
                 do jnode = 1,pnode
                    jdofv = (jnode-1)*ndime
                    fact1 = gpvis(igaus) * gpvol(igaus) * gpcar(idime,jnode,igaus)     
                    jdofv = jdofv + 1
                    elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(1,inode,igaus)
                    jdofv = jdofv + 1
                    elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(2,inode,igaus)
                    jdofv = jdofv + 1
                    elauu(idofv,jdofv)=elauu(idofv,jdofv)+fact1*gpcar(3,inode,igaus)
                 end do
                 if( fvins_nsi == 2.0_rp ) then
                    fact1 = -2.0_rp/3.0_rp*gpvis(igaus) * gpvol(igaus) * gpcar(idime,inode,igaus)
                    do jnode = 1,pnode
                       jdofv = (jnode-1)*ndime   
                       jdofv = jdofv + 1
                       elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(1,jnode,igaus)
                       jdofv = jdofv + 1
                       elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(2,jnode,igaus)
                       jdofv = jdofv + 1
                       elauu(idofv,jdofv) = elauu(idofv,jdofv) + fact1 * gpcar(3,jnode,igaus)
                    end do
                 end if
              end do
           end do
        end do
     end if
  end if

  !----------------------------------------------------------------------
  !
  ! Apu and Aup
  !
  !----------------------------------------------------------------------
  !
  ! ( div(u) , q ) and - ( p , div(v) ) 
  !
  if( ndime == 2 ) then
     do igaus = 1,pgaus
        do inode = 1,pnode
           idof1 = 2 * inode - 1
           idof2 = idof1 + 1
           do jnode = 1,pnode
              fact0              = gpvol(igaus) * gpsha(jnode,igaus) 
              fact1              = fact0 * gpcar(1,inode,igaus)
              fact2              = fact0 * gpcar(2,inode,igaus)
              elapu(jnode,idof1) = elapu(jnode,idof1) + fact1
              elapu(jnode,idof2) = elapu(jnode,idof2) + fact2
              elaup(idof1,jnode) = elaup(idof1,jnode) - fact1
              elaup(idof2,jnode) = elaup(idof2,jnode) - fact2
           end do
        end do
     end do
  else
     do igaus = 1,pgaus
        do inode = 1,pnode
           idof1 = 3 * inode - 2
           idof2 = idof1 + 1
           idof3 = idof2 + 1
           do jnode = 1,pnode
              fact0              = gpvol(igaus) * gpsha(jnode,igaus) 
              fact1              = fact0 * gpcar(1,inode,igaus)
              fact2              = fact0 * gpcar(2,inode,igaus)
              fact3              = fact0 * gpcar(3,inode,igaus)
              elapu(jnode,idof1) = elapu(jnode,idof1) + fact1
              elapu(jnode,idof2) = elapu(jnode,idof2) + fact2
              elapu(jnode,idof3) = elapu(jnode,idof3) + fact3
              elaup(idof1,jnode) = elaup(idof1,jnode) - fact1
              elaup(idof2,jnode) = elaup(idof2,jnode) - fact2
              elaup(idof3,jnode) = elaup(idof3,jnode) - fact3
           end do
        end do
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! App
  !
  !----------------------------------------------------------------------
  !
  ! Pressure: ( tau1' * grad(p) , grad(q) )
  ! 
  do igaus = 1,pgaus
     do inode = 1,pnode
        do jnode = inode+1,pnode
           fact1 = gpsp1(igaus) * wgrgr(jnode,inode,igaus) * gpvol(igaus)
           elapp(jnode,inode) = elapp(jnode,inode) + fact1
           elapp(inode,jnode) = elapp(inode,jnode) + fact1
        end do
        fact1 = gpsp1(igaus) * wgrgr(inode,inode,igaus) * gpvol(igaus)
        elapp(inode,inode) = elapp(inode,inode) + fact1
     end do
  end do

  !----------------------------------------------------------------------
  !
  ! bu and bp
  !
  ! P1  = P [ tau1' * rho * a.grad(u) ]
  ! P1' = P1 + tau1' * rho * u'n / dt
  !
  ! P2  = P [ tau1' * ( grad(p) - f ) ]
  ! P2' = P2 + tau1' * rho * u'n / dt + tau1' * f 
  !
  !----------------------------------------------------------------------
  !
  ! Limiter
  !
  if( kfl_limit_nsi == -1 ) then

     do igaus = 1,pgaus
        do idime = 1,ndime
           gpvep(idime,igaus) = 0.0_rp
        end do
     end do

  else if( kfl_limit_nsi > 0 ) then

     do igaus = 1,pgaus
        c1 = 0.0_rp
        c2 = 0.0_rp
        c3 = 0.0_rp
        do idime = 1,ndime
           c4 = 0.0_rp
           do inode = 1,pnode
              c4 = c4 + agrau(inode,igaus) * elvel(idime,inode)
           end do
           c4 = gpsp1(igaus) * c4
           c1 = c1 + ( gpvep(idime,igaus) - c4 )**2
           c3 = c3 + gpvep(idime,igaus) * gpvep(idime,igaus)
           c2 = c2 + c4 * c4
        end do
        c3 = sqrt( c2 ) + sqrt( c3 )
        c1 = sqrt( c1 )
        if( c3 /= 0.0_rp ) then
           beta  = c1 / c3
        else
           beta  = 0.0_rp
        end if
        if( kfl_limit_nsi == 1 ) then
           alpha = min(1.0_rp,2.0_rp*(1.0_rp-beta))
        else if( kfl_limit_nsi == 2 ) then
           alpha = 0.5_rp*(tanh(20.0_rp*(beta-0.8_rp))+1.0_rp)
        end if
        do idime = 1,ndime
           gpvep(idime,igaus) = alpha * gpvep(idime,igaus)
        end do
     end do

  end if
  !
  ! P2 <= P2 + tau1' * f
  !
  if( ndime == 2 ) then
     do igaus = 1,pgaus
        gpgrp(1,igaus) = gpgrp(1,igaus) + gpsp1(igaus) * gprhs(1,igaus) 
        gpgrp(2,igaus) = gpgrp(2,igaus) + gpsp1(igaus) * gprhs(2,igaus) 
     end do
  else
     do igaus = 1,pgaus
        gpgrp(1,igaus) = gpgrp(1,igaus) + gpsp1(igaus) * gprhs(1,igaus)
        gpgrp(2,igaus) = gpgrp(2,igaus) + gpsp1(igaus) * gprhs(2,igaus)
        gpgrp(3,igaus) = gpgrp(3,igaus) + gpsp1(igaus) * gprhs(3,igaus)
     end do
  end if
  !
  ! P1 <= P1 + tau1' * rho * u'n / dt
  ! P2 <= P2 + tau1' * rho * u'n / dt
  !
  if( kfl_sgsti_nsi == 1 ) then
     if( ndime == 2 ) then
        do igaus = 1,pgaus
           fact1 = gpden(igaus) * dtsgs * gpsp1(igaus)
           gpvep(1,igaus) = gpvep(1,igaus) + fact1 * gpsgs(1,igaus,2)
           gpvep(2,igaus) = gpvep(2,igaus) + fact1 * gpsgs(2,igaus,2)
           gpgrp(1,igaus) = gpgrp(1,igaus) + fact1 * gpsgs(1,igaus,2)
           gpgrp(2,igaus) = gpgrp(2,igaus) + fact1 * gpsgs(2,igaus,2)
        end do
     else
        do igaus = 1,pgaus 
           fact1 = gpden(igaus) * dtsgs * gpsp1(igaus)
           gpvep(1,igaus) = gpvep(1,igaus) + fact1 * gpsgs(1,igaus,2)
           gpvep(2,igaus) = gpvep(2,igaus) + fact1 * gpsgs(2,igaus,2)
           gpvep(3,igaus) = gpvep(3,igaus) + fact1 * gpsgs(3,igaus,2)
           gpgrp(1,igaus) = gpgrp(1,igaus) + fact1 * gpsgs(1,igaus,2)
           gpgrp(2,igaus) = gpgrp(2,igaus) + fact1 * gpsgs(2,igaus,2)
           gpgrp(3,igaus) = gpgrp(3,igaus) + fact1 * gpsgs(3,igaus,2)
        end do        
     end if
  end if
  !
  ! bu = ( f + rho*u^n/dt , v ) + ( rho * a.grad(v) , tau1' * rho u'^n/dt + P1 ) 
  !    = ( f + rho*u^n/dt , v ) + ( rho * a.grad(v) , P1' ) 
  !
  ! bp = ( f + rho*u'^n/dt , tau1' grad(q) ) + ( P2 , grad(q) )
  !    = ( P2' , grad(q) ) 
  !
  if( ndime == 2 ) then
     do igaus = 1,pgaus
        fact4 = gpden(igaus) * dtinv
        do itime = 2,nbdfp_nsi
           gprhs(1,igaus) = gprhs(1,igaus) - pabdf_nsi(itime) * fact4 * gpvel(1,igaus,itime)  
           gprhs(2,igaus) = gprhs(2,igaus) - pabdf_nsi(itime) * fact4 * gpvel(2,igaus,itime)
        end do
        do inode = 1,pnode
           fact1          = gpvol(igaus) * gpsha(inode,igaus)                                ! ( f + rho*u^n/dt , v )
           fact3          = gpvol(igaus) * agrau(inode,igaus)                                ! ( rho * a.grad(v) , P1' ) 
           elrbu(1,inode) = elrbu(1,inode) + fact1 * gprhs(1,igaus) + fact3 * gpvep(1,igaus) 
           elrbu(2,inode) = elrbu(2,inode) + fact1 * gprhs(2,igaus) + fact3 * gpvep(2,igaus) 
           elrbp(inode)   = elrbp(inode)   + gpvol(igaus) * ( &                              ! ( P2' , grad(q) ) 
                &    gpcar(1,inode,igaus) * gpgrp(1,igaus)  &
                &  + gpcar(2,inode,igaus) * gpgrp(2,igaus)  )
        end do
     end do
  else
     do igaus = 1,pgaus
        fact4 = gpden(igaus) * dtinv
        do itime = 2,nbdfp_nsi
           gprhs(1,igaus) = gprhs(1,igaus) - pabdf_nsi(itime) * fact4 * gpvel(1,igaus,itime)  
           gprhs(2,igaus) = gprhs(2,igaus) - pabdf_nsi(itime) * fact4 * gpvel(2,igaus,itime)
           gprhs(3,igaus) = gprhs(3,igaus) - pabdf_nsi(itime) * fact4 * gpvel(3,igaus,itime)
        end do
        do inode = 1,pnode
           fact1          = gpvol(igaus) * gpsha(inode,igaus)
           fact3          = gpvol(igaus) * agrau(inode,igaus)
           elrbu(1,inode) = elrbu(1,inode) + fact1 * gprhs(1,igaus) + fact3 * gpvep(1,igaus) 
           elrbu(2,inode) = elrbu(2,inode) + fact1 * gprhs(2,igaus) + fact3 * gpvep(2,igaus) 
           elrbu(3,inode) = elrbu(3,inode) + fact1 * gprhs(3,igaus) + fact3 * gpvep(3,igaus) 
           elrbp(inode)   = elrbp(inode)   + gpvol(igaus) * ( &
                &    gpcar(1,inode,igaus) * gpgrp(1,igaus) &
                &  + gpcar(2,inode,igaus) * gpgrp(2,igaus) &
                &  + gpcar(3,inode,igaus) * gpgrp(3,igaus) )
        end do
     end do
  end if

end subroutine nsi_elmma4

