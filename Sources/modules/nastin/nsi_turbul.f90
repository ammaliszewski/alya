!!!#define ransopenint
!! ojo tambien hay que corregir la palablra usa en la linea 31 - solucion a deficiencia del configure
!! for SST k-omega you also need to touch tur_elmco2  see FOR GEORGIOS
subroutine nsi_turbul(&
     itask,jtask,pnode,pgaus,igaui,igauf,kfl_cotur,   &
     gpsha,gpcar,hleng,elvel,elmut,gpden,gpvis,gpmut, &
     gpgvi,grvis,gpgve, ielem, kfl_kemod)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_turbul
  ! NAME 
  !    nsi_turbul
  ! DESCRIPTION
  !    Compute viscosity and its gradient due to turbulence
  !    GBMUT ....... mut
  !    JTASK = 1 ... Compute gradient GRVIS = grad(mut)
  !            0 ... Do not compute gradient GRVIS = grad(mut)
  !    ITASK = 1 ... GPVIS <= GPVIS + GPMUT
  !            0 ... Do not change GPVIS
  !    GPGVI = Viscosity gradient grad(mu+mut)
  ! USES
  !
  ! USED BY
  !    nsi_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp  
  use def_domain, only      :  mnode,ndime, lnods, walld
  use def_master, only      :  zeror, untur
  use def_nastin, only      :  turbu_nsi
  use def_kermod, only      :  cmu_st
#ifdef ransopenint
  usa def_turbul, only      :  TUR_K_EPS_STD,TUR_SST_K_OMEGA
#endif
  implicit none 
  integer(ip),  intent(in)  :: itask,jtask,pnode,pgaus,igaui,igauf
  integer(ip),  intent(in)  :: kfl_cotur, kfl_kemod, ielem
  real(rp),     intent(in)  :: gpsha(pnode,pgaus)
  real(rp),     intent(in)  :: gpcar(ndime,mnode,pgaus)
  real(rp),     intent(in)  :: hleng(ndime)
  real(rp),     intent(in)  :: elvel(ndime,pnode)
  real(rp),     intent(in)  :: elmut(pnode)
  real(rp),     intent(in)  :: gpden(pgaus)
  real(rp),     intent(out) :: gpvis(pgaus)
  real(rp),     intent(out) :: gpmut(pgaus)
  real(rp),     intent(out) :: gpgvi(ndime,pgaus)
  real(rp),     intent(out) :: grvis(ndime,pgaus)
  real(rp),     intent(out) :: gpgve(ndime,ndime,pgaus)
  integer(ip)               :: igaus,inode,idime,jdime,kdime, ipoin
  real(rp)                  :: xmile,seci4,seci5,sd_ij,denom, uaste,eltur(2, mnode)
  real(rp)                  :: G__ij(3,3),Ginv(3),angle(3), eigen(3), sigma(3),g2_ij(3,3),g2_kk
  real(rp)                  :: A0, As, W, phi, cmu, k, eps, divve, simgr(3,3), kinen 
  real(rp)                  :: epsil, gpwal, omega, gpvor, a1, F2, pi,  Cr, f0, f02

  if( kfl_cotur /= 0 ) then
     !-------------------------------------------------------------------
     !
     ! GPMUT <- ELMUT
     ! GRVIS <- grad(ELMUT)
     ! GPGVE <- grad(ELVEL)
     !
     !-------------------------------------------------------------------

     if( kfl_cotur == 1 ) then
        !
        ! GPMUT and GRVIS
        !
        if( jtask == 0 ) then
           do igaus = igaui,igauf
              gpmut(igaus) = 0.0_rp
              do idime = 1,ndime
                 grvis(idime,igaus) = 0.0_rp
              end do
              do inode = 1,pnode
                 gpmut(igaus) = gpmut(igaus) + gpsha(inode,igaus) * elmut(inode)
              end do
           end do
        else
           do igaus = igaui,igauf
              gpmut(igaus) = 0.0_rp
              do idime = 1,ndime
                 grvis(idime,igaus) = 0.0_rp
              end do
              do inode = 1,pnode
                 gpmut(igaus) = gpmut(igaus) + gpsha(inode,igaus) * elmut(inode)
                 do idime = 1,ndime
                    grvis(idime,igaus) = grvis(idime,igaus) &
                         + gpcar(idime,inode,igaus) * elmut(inode)
                 end do
              end do
           end do
        end if
#ifdef ransopenint
        if (TUR_K_EPS_STD) then   ! open intergration points for K Epsilon model 
           do igaus = igaui,igauf          
              kinen = 0.0_rp
              epsil = 0.0_rp
              do inode = 1,pnode
                 ipoin = lnods(inode,ielem)
                 kinen = kinen + gpsha(inode, igaus)*untur(1,ipoin,1)
                 epsil = epsil + gpsha(inode, igaus)*untur(2,ipoin,1)             
              end do
              gpmut(igaus) = max (cmu_st*gpden(igaus)*kinen*kinen/epsil, gpvis(igaus))
           end do
        else if (TUR_SST_K_OMEGA) then ! K OMEGA SST for GEORGIOS (open integration) 
           do igaus = igaui,igauf          
              kinen = 0.0_rp
              omega = 0.0_rp
              gpwal = 0.0_rp
              do idime = 1,ndime
                 do jdime = 1,ndime
                    gpgve(jdime,idime,igaus) = 0.0_rp
                 end do
              end do
              do inode = 1,pnode
                 ipoin = lnods(inode,ielem)
                 kinen = kinen + gpsha(inode, igaus)*untur(1,ipoin,1)
                 omega = omega + gpsha(inode, igaus)*untur(2,ipoin,1)             
                 gpwal = gpwal + gpsha(inode, igaus)*walld(ipoin)             
                 do idime = 1,ndime
                    do jdime = 1,ndime
                       gpgve(jdime,idime,igaus) = gpgve(jdime,idime,igaus) &
                            + gpcar(jdime,inode,igaus) * elvel(idime,inode)
                    end do
                 end do
              end do
              gpvor = 0.0_rp   ! 2 S_ij : S_ij             
              do idime = 1,ndime
                 do jdime = 1,ndime
                    gpvor = gpvor + gpgve(idime,jdime,igaus) &    ! 2 S_ij : S_ij
                      *(gpgve(idime,jdime,igaus)        &
                      +gpgve(jdime,idime,igaus))
                 end do
              end do
              gpvor = sqrt(gpvor)
              a1 =0.31_rp
              gpmut(igaus) = 0.0_rp
              if (gpwal.gt.1.0e-8) then
                 F2 = max( 2.0_rp *sqrt(max(0.0_rp,kinen)) /(0.09_rp*omega*gpwal), 500.0_rp * gpvis(igaus) / (gpwal*gpwal*omega)) 
                 F2 = tanh( F2 * F2 )
                 as = max( a1 * omega, gpvor * F2 )             
                 gpmut(igaus) = gpden(igaus)*a1*kinen/as
              end if
           end do
        end if
#endif
        !
        ! k eps REALIZABLE or kefp  model
        !        
        if ((kfl_kemod==2.or.kfl_kemod==3).and.itask==1) then
           A0=4.04_rp
           do inode = 1,pnode
              ipoin = lnods(inode,ielem)
              eltur(1, inode) = untur(1, ipoin, 1)
              eltur(2, inode) = untur(2, ipoin, 1)
           end do
           do igaus = igaui,igauf
              do idime = 1,ndime
                 do jdime = 1,ndime
                    gpgve(jdime,idime,igaus) = 0.0_rp
                 end do
              end do
              do inode = 1,pnode
                 do idime = 1,ndime
                    do jdime = 1,ndime
                       gpgve(jdime,idime,igaus) = gpgve(jdime,idime,igaus) &
                            + gpcar(jdime,inode,igaus) * elvel(idime,inode)
                    end do
                 end do
              end do
              divve =0.0_rp
              do idime = 1,ndime
                 divve = divve + gpgve(idime, idime, igaus)
                 do jdime = 1,ndime
                    simgr(idime,jdime) = 0.5_rp*(gpgve(idime, jdime,igaus) &
                         +  gpgve(jdime, idime,igaus))
                 end do
              end do
              seci4 = 0.0_rp
              W=0.0_rp
              uaste =0.0_rp
              do idime = 1,ndime
                 simgr(idime, idime ) = simgr(idime, idime) - divve/3.0_rp
              end do
              do idime = 1,ndime
                 do jdime = 1,ndime
                    seci4 = seci4 + simgr(idime, jdime) &        ! S_ij : S_ij
                         *simgr(idime, jdime)
                    uaste = uaste +gpgve(idime,jdime,igaus) &    ! D_ij : D_ij
                         *(gpgve(idime,jdime,igaus))   
                    
                    do kdime =1, ndime
                       W = W +  simgr(idime,jdime)* &
                            simgr(jdime,kdime)* &
                            simgr(kdime,idime)
                    end do                    
                 end do
              end do
              uaste = sqrt(uaste -divve*divve /3.0_rp)
              if (abs(W).gt.0.0_rp) W = W/sqrt(seci4*seci4*seci4)
              phi = 1.0_rp/3.0_rp*acos(sqrt(6.0_rp)*W)
              As= sqrt(6.0_rp)*cos(phi)
              
              ! interpolarion of k and epsilon
              k=0.0_rp
              eps=0.0_rp
              do inode = 1,pnode
                 k =k +gpsha(inode,igaus) * eltur(1, inode)
                 eps =eps +gpsha(inode,igaus) * eltur(2, inode)
              end do

              ! calculates turb viscosity
              if (kfl_kemod==2) then ! REALIZABLE MODEL
                 cmu =1.0_rp/(A0+As*k/eps*uaste)
              else if (kfl_kemod==3) then ! KEFP
                 Cr= 4.5_rp
                 f0 = Cr/(Cr-1.0_rp)
                 f02 = 4.0_rp*f0*(f0-1.0_rp)
                 cmu = cmu_st*2.0_rp*f0 /(1.0_rp+ sqrt(1.0_rp + f02*cmu_st *(uaste*k/eps)*(uaste*k/eps)))
              end if

              gpmut(igaus)= gpden(igaus)*cmu*k*k/eps
              ! vis gradient set to zero
              do idime = 1,ndime
                 grvis(idime,igaus) = 0.0_rp
              end do
           end do
         
        end if

     else if( ( kfl_cotur == -1 ) .or. ( kfl_cotur == -2 ) .or. ( kfl_cotur == -3 ) ) then
        ! 
        ! GPGVE: Smago or WALE
        !
        do igaus = igaui,igauf
           do idime = 1,ndime
              do jdime = 1,ndime
                 gpgve(jdime,idime,igaus) = 0.0_rp
              end do
           end do
           do inode = 1,pnode
              do idime = 1,ndime
                 do jdime = 1,ndime
                    gpgve(jdime,idime,igaus) = gpgve(jdime,idime,igaus) &
                         + gpcar(jdime,inode,igaus) * elvel(idime,inode)
                 end do
              end do
           end do
        end do
     end if

     !-------------------------------------------------------------------
     !
     ! GPMUT: Turbulent viscosity 
     ! 
     !-------------------------------------------------------------------

     if( kfl_cotur == -1 ) then     
        !
        ! Smagorinsky
        ! -----------
        !
        ! h calculated as vol**(1/ndime) - Vol is approximated by  hleng(1) * hleng(2) * hleng(3) 
        ! - Otherwise jacobi (as in mescek) could have been used but the implementation would have 
        !   been more complicated. 
        !
        if (ndime == 2) then
           xmile = sqrt ( hleng(1) * hleng(2) )
        else
           xmile = ( hleng(1) * hleng(2) * hleng(3) )** 0.3333333_rp
        end if
        !     xmile = hleng(1)    ! OLD: hmax
        do igaus = igaui,igauf
           seci4 = 0.0_rp
           do idime = 1,ndime
              do jdime = 1,ndime
                 seci4 = seci4+gpgve(idime,jdime,igaus) &    ! 2 S_ij : S_ij
                      *(gpgve(idime,jdime,igaus)        &
                      +gpgve(jdime,idime,igaus))
              end do
           end do
           gpmut(igaus) = gpden(igaus)*turbu_nsi(1)*xmile*xmile*sqrt(seci4)
           do idime = 1,ndime
              grvis(idime,igaus) = 0.0_rp      
           end do
        end do

     else if( kfl_cotur == -2 ) then  
        !
        ! WALE  - Wall-adapting local eddy-viscosity - Nicoud-Ducros 99
        ! -------------------------------------------------------------
        !
        ! h calculated as vol**(1/ndime) - Vol is approximated by  hleng(1) * hleng(2) * hleng(3) 
        ! - Otherwise jacobi (as in mescek) could have been used but the implementation would have 
        !   been more complicated. 
        !
        if (ndime == 2) then
           call runend('nsi_turbul: WALE only ready in 3d - 2d LES does not make sense')
        else
           xmile = ( hleng(1) * hleng(2) * hleng(3) )** 0.3333333_rp
        end if
        do igaus = igaui,igauf
           !
           ! Obtain g2_ij
           !
           g2_ij = 0.0_rp
           do idime = 1,3_ip
              do jdime = 1,3_ip
                 do kdime = 1,3_ip
                    g2_ij(idime,jdime) = g2_ij(idime,jdime)  + gpgve(idime,kdime,igaus) * gpgve(kdime,jdime,igaus)
                 end do
              end do
           end do
           g2_kk = g2_ij(1,1) +  g2_ij(2,2) +  g2_ij(3,3)

           seci5 = 0.0_rp
           seci4 = 0.0_rp
           do idime = 1,3_ip
              do jdime = 1,3_ip

                 seci4 = seci4+ 0.5_rp * gpgve(idime,jdime,igaus) &      ! S_ij : S_ij
                      *(gpgve(idime,jdime,igaus)        &
                      +gpgve(jdime,idime,igaus))

                 sd_ij = 0.5_rp * ( g2_ij(idime,jdime)  + g2_ij(jdime,idime) )  

                 if(idime == jdime) sd_ij = sd_ij  - 0.3333333333_rp * g2_kk    ! -1/3 delta_ij  * gkk**2

                 seci5 = seci5 + sd_ij * sd_ij

              end do
           end do

           denom = max ( zeror , (seci4**2.5_rp) + (seci5**1.25_rp) )   ! avoid divide by zero
           gpmut(igaus) = gpden(igaus) * turbu_nsi(1)*xmile*xmile* (seci5**1.5_rp)/ denom

           do idime = 1,3_ip
              grvis(idime,igaus) = 0.0_rp      
           end do
        end do

     else if( kfl_cotur == -3 ) then     ! Sigma model  - Baya Toda, Nicoud 2010
        !
        ! h calculated as vol**(1/ndime) - Vol is approximated by  hleng(1) * hleng(2) * hleng(3) 
        ! - Otherwise jacobi (as in mescek) could have been used but the implementation would have 
        !   been more complicated. 
        !
        xmile = ( hleng(1) * hleng(2) * hleng(3) )** 0.3333333_rp

        do igaus = igaui,igauf


           divve =0.0_rp
           do idime = 1,ndime
              divve = divve + gpgve(idime, idime, igaus)                
           end do
           seci4 = 0.0_rp
           W=0.0_rp
           uaste =0.0_rp

           G__ij = 0.0_rp ! G = g^T*g = g_ki * g_kj  - note that gpgve(kdime,idime, )  corresponds to g_ik = du_i/dx_k from  Nicoud
           do idime = 1,3_ip
              do jdime = 1,3_ip
                 do kdime = 1,3_ip
                    G__ij(idime,jdime) = G__ij(idime,jdime) +gpgve(idime,kdime,igaus) &
                         *gpgve(jdime,kdime,igaus)
                 end do
              end do
           end do

           ! Calculating the invariants of G
           ! I1 = tr(G)
           ! I2 = 1/2 * (tr(G)^2 - tr(G^2))
           ! I3 = det(G)

           Ginv(1) = G__ij(1,1) + G__ij(2,2) + G__ij(3,3)
           Ginv(2) =  G__ij(1,1)*G__ij(2,2) + G__ij(2,2)*G__ij(3,3) + G__ij(3,3)*G__ij(1,1) &
                    - G__ij(1,2)*G__ij(1,2) - G__ij(2,3)*G__ij(2,3) - G__ij(1,3)*G__ij(1,3)
           Ginv(3) =   G__ij(1,1) * (G__ij(2,2)*G__ij(3,3) - G__ij(2,3)*G__ij(2,3)) &
                     - G__ij(1,2) * (G__ij(1,2)*G__ij(3,3) - G__ij(2,3)*G__ij(1,3)) &
                     + G__ij(1,3) * (G__ij(1,2)*G__ij(2,3) - G__ij(2,2)*G__ij(1,3))

           ! Calculating angles from the invariants
           ! a1 = (I1^2)/9  - I2/3
           ! a2 = (I1^3)/27 - I1*I2/6 + I3/2
           ! a3 = 1/3 * arccos(a2/(a3^(3/2)))

           angle(1) = Ginv(1)*Ginv(1)/9.0_rp - Ginv(2)/3.0_rp
           angle(2) = Ginv(1)*Ginv(1)*Ginv(1)/27.0_rp - Ginv(1)*Ginv(2)/6.0_rp + Ginv(3)/2.0_rp
           angle(3) = 0.3333333333_rp*(acos(angle(2)/(angle(1)**1.5_rp)))

           pi = 4.0_rp*atan(1.0_rp)

           ! Calculating the eigenvalues and singular values
           ! lamda1= I1/3 + 2*sqrt(a1)*cos(a3)
           ! lamda2= I1/3 - 2*sqrt(a1)*cos(pi/3 + a3)
           ! lamda3= I1/3 - 2*sqrt(a1)*cos(pi/3 - a3)


           eigen(1) = (Ginv(1)/3.0_rp + 2.0_rp*sqrt(angle(1))*cos(angle(3)))
           eigen(2) = (Ginv(1)/3.0_rp - 2.0_rp*sqrt(angle(1))*cos(pi/3.0_rp + angle(3)))
           eigen(3) = (Ginv(1)/3.0_rp - 2.0_rp*sqrt(angle(1))*cos(pi/3.0_rp - angle(3)))

           eigen(3) = max(eigen(3), 0.0_rp)    ! The eigenvalues of a semi-definite positive matrix G are always non-negative

           sigma(1) = (eigen(1))**0.5_rp
           sigma(2) = (eigen(2))**0.5_rp
           sigma(3) = (eigen(3))**0.5_rp

           gpmut = gpden(igaus) * turbu_nsi(1) * xmile * xmile
           if ( sigma(1)*sigma(1) > 10.0*zeror ) then                         ! Avoid divide by zero
              gpmut = gpmut * sigma(3) * ( sigma(1) - sigma(2) ) * ( sigma(2) - sigma(3) ) / ( sigma(1) * sigma(1) )
           else
              gpmut = 0.0_rp
           end if
           grvis = 0.0_rp   

        end do

     else if( kfl_cotur == -12 ) then   ! Xu-Chen: to do

        call runend('XU-CHEN: NOT PROGRAMMED')

     end if

     !-------------------------------------------------------------------
     !
     ! GPVIS <= GPVIS + GPMUT
     ! GPGVI <= GPGVI + GRVIS
     ! 
     !-------------------------------------------------------------------

     if ( ( itask == 1 ) .or. ( itask == 12 ) ) then
        do igaus = igaui,igauf
           gpvis(igaus) = gpvis(igaus) + gpmut(igaus)  ! Total viscosity <= mu+mut
           do idime = 1,ndime
              gpgvi(idime,igaus) = gpgvi(idime,igaus) + grvis(idime,igaus)
           end do
        end do
     end if

  end if
end subroutine nsi_turbul
