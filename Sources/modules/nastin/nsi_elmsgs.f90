subroutine nsi_elmsgs(&
     pgaus,pnode,chale,hleng,gpadv,gpvis,gpden,gpcar,gpst1,&
     gpst2,gpsp1,gpsp2,gptt1,gptt2,rmom1,gppor,dtsgs,&
     tamin_loc,tamax_loc)
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_elmsgs
  ! NAME 
  !    nsi_elmsgs
  ! DESCRIPTION
  !    1. Compute stability parameters
  !       - tau1 [ L/(rho*U) ]
  !       - tau2 [ rho*L*U ]
  !    3. Update momentum residual
  ! USES
  ! USED BY
  !    nsi_elmope
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,mnode,mgaus 
  use def_nastin, only       :  staco_nsi,kfl_sgsti_nsi,&
       &                        kfl_taust_nsi,kfl_stabi_nsi,&
       &                        kfl_ellen_nsi,corio_nsi
  implicit none
  integer(ip), intent(in)    :: pgaus,pnode
  real(rp),    intent(in)    :: chale(2),hleng(ndime)
  real(rp),    intent(in)    :: gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)    :: gpadv(ndime,pgaus)
  real(rp),    intent(in)    :: gpvis(pgaus)
  real(rp),    intent(in)    :: gpden(pgaus)
  real(rp),    intent(in)    :: gppor(pgaus)
  real(rp),    intent(out)   :: gpst1(pgaus),gpst2(pgaus)
  real(rp),    intent(out)   :: gpsp1(pgaus),gpsp2(pgaus)
  real(rp),    intent(out)   :: gptt1(pgaus),gptt2(pgaus)
  real(rp),    intent(out)   :: rmom1(pnode,pgaus)
  real(rp),    intent(in)    :: dtsgs
  real(rp),    intent(inout) :: tamin_loc
  real(rp),    intent(inout) :: tamax_loc
  integer(ip)                :: idime,igaus,inode
  real(rp)                   :: adv,dif,rea,h2,gpvno

  h2 = chale(2) * chale(2)

  !----------------------------------------------------------------------
  !
  ! TAU1 and TAU2
  !
  !----------------------------------------------------------------------

  do igaus = 1,pgaus
     !
     ! tau1 = 1 / ( 4*mu/h^2 + 2*rho*uc/h + rho*|w| + sig )
     ! tau2 = 1 / [ (eps + 1/(h^2*4*mu/h^2 + 2*rho*uc/h + rho*|w|)) ]
     !
     call vecnor(gpadv(1,igaus),ndime,gpvno,2_ip)
     adv   = gpden(igaus)*gpvno                                 ! Convective term: rho*|u+u'|
     dif   = gpvis(igaus)                                       ! Viscous term:    mu
     rea   = gpden(igaus)*corio_nsi + abs(gppor(igaus))           ! Coriolis: w + Porosity: sig
     call tauadr(&
          kfl_taust_nsi,staco_nsi,adv,dif,rea,&
          chale(1),chale(2),gpst1(igaus))
     if( gpst1(igaus) /= 0.0_rp ) then
         if( kfl_ellen_nsi == 6 ) then   ! MIXLE
             gpst2(igaus) = staco_nsi(4)*(4.0_rp*dif + 2.0_rp*adv*hleng(1))    ! Use maximum elem length only for tau2
         else
             gpst2(igaus) = staco_nsi(4)*h2/gpst1(igaus)
         end if
     else
        gpst2(igaus) = 0.0_rp
     end if 
     !
     ! (tau1',tau2') and (tau1'/tau1,tau2'/tau2)
     !
     if( kfl_sgsti_nsi == 1 ) then
        gpsp1(igaus) = 1.0_rp / ( gpden(igaus) * dtsgs + 1.0_rp / gpst1(igaus) )
        gpsp2(igaus) = gpst2(igaus)
        gptt1(igaus) = gpsp1(igaus) / gpst1(igaus)
        gptt2(igaus) = 1.0_rp
     else
        gpsp1(igaus) = gpst1(igaus)
        gpsp2(igaus) = gpst2(igaus)
        gptt1(igaus) = 1.0_rp
        gptt2(igaus) = 1.0_rp
     end if
     if (kfl_stabi_nsi==1) gptt1(igaus)=1.0_rp

  end do

  !----------------------------------------------------------------------
  !
  ! RMOMU: Add advection to residual; split OSS needs only advection
  !
  !----------------------------------------------------------------------

  do igaus = 1,pgaus
     do inode = 1,pnode
        do idime = 1,ndime
           rmom1(inode,igaus) = rmom1(inode,igaus)& 
                + gpden(igaus) * gpadv(idime,igaus)&
                * gpcar(idime,inode,igaus)
        end do
     end do
  end do
  
  !----------------------------------------------------------------------
  !
  ! TAMIN_NSI, TAMAX_NSI: Minimum and maximum tau
  !
  !----------------------------------------------------------------------

  do igaus = 1,pgaus
     tamax_loc = max(tamax_loc,gpsp1(igaus))   
     tamin_loc = min(tamin_loc,gpsp1(igaus))   
  end do

end subroutine nsi_elmsgs
