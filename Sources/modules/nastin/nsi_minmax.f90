subroutine nsi_minmax()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_minmax
  ! NAME 
  !    nsi_minmax
  ! DESCRIPTION
  !    This routine computes some min and max
  ! USES
  ! USED BY
  !    nsi_cvgunk
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use mod_communications, only : PAR_MIN
  use mod_communications, only : PAR_MAX
  implicit none
  integer(ip), parameter  :: nmima=3
  integer(ip)             :: ipoin,idime,ndofn,idofn
  integer(ip)             :: ifac1,ifac2
  real(rp),    target     :: rmini(nmima),rmaxi(nmima)
  real(rp)                :: vnorm,rpres

  vemin_nsi =  huge(1.0_rp)
  vemax_nsi = -huge(1.0_rp)
  prmin_nsi =  huge(1.0_rp)
  prmax_nsi = -huge(1.0_rp)
  ndofn     =  solve(1) % ndofn
  if( NSI_MONOLITHIC ) then
     ifac1 = ndofn
     ifac2 = 0
  else
     ifac1 = 1
     ifac2 = npoin * ndofn
  end if
  !
  ! VEMIN and VEMAX: Min and max pressure
  ! PRMIN and PRMAX: Min and max pressure
  !
  do ipoin = 1,npoin
     idofn = ( ipoin-1 ) * ndofn
     vnorm = 0.0_rp
     do idime = 1,ndime
        idofn = idofn + 1
        vnorm = vnorm + unkno(idofn) * unkno(idofn)
     end do
     idofn = ipoin * ifac1 + ifac2  
     vemax_nsi = max(vemax_nsi,vnorm)
     vemin_nsi = min(vemin_nsi,vnorm)
     prmax_nsi = max(prmax_nsi,unkno(idofn))
     prmin_nsi = min(prmin_nsi,unkno(idofn))
  end do
  vemin_nsi = sqrt(max(0.0_rp,vemin_nsi))
  vemax_nsi = sqrt(max(0.0_rp,vemax_nsi))
  !
  ! Call to parall service
  !
  if( IPARALL ) then

     rmini(1)  =  tamin_nsi
     rmini(2)  =  vemin_nsi
     rmini(3)  =  prmin_nsi
     call PAR_MIN(nmima,rmini,'IN MY CODE')
     tamin_nsi =  rmini(1)
     vemin_nsi =  rmini(2)
     prmin_nsi =  rmini(3)

     rmaxi(1)  =  tamax_nsi
     rmaxi(2)  =  vemax_nsi
     rmaxi(3)  =  prmax_nsi
     call PAR_MAX(nmima,rmaxi,'IN MY CODE')
     tamax_nsi =  rmaxi(1)
     vemax_nsi =  rmaxi(2)
     prmax_nsi =  rmaxi(3)

  end if

end subroutine nsi_minmax

