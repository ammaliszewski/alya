!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    mod_nsi_subgrid_scale.f90
!> @author  Guillaume Houzeaux
!> @date    23/07/2015
!> @brief   Subrgid scale
!> @details Subrgid scale operation
!> @} 
!-----------------------------------------------------------------------
module mod_nsi_subgrid_scale

  use def_kintyp, only : ip,rp,r3p
  use def_nastin, only : kfl_sgsco_nsi
  use def_nastin, only : kfl_sgsti_nsi
  implicit none  
  private

  public :: nsi_subgrid_scale_gather
  public :: nsi_subgrid_scale_residual_and_update

contains 

  !-----------------------------------------------------------------------
  !
  !> @brief   Gather subgrid scale
  !> @details Gather the subgrid scale at the element level
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine nsi_subgrid_scale_gather(ndime,pgaus,ielem,vesgs,gpsgs)
    integer(ip), intent(in)            :: ndime
    integer(ip), intent(in)            :: pgaus
    integer(ip), intent(in)            :: ielem
    type(r3p),   intent(in),   pointer :: vesgs(:)
    real(rp),    intent(out)           :: gpsgs(ndime,pgaus,*)
    
    if( kfl_sgsco_nsi == 1 .or. kfl_sgsti_nsi == 1 ) then

       gpsgs(:,1:pgaus,1) = vesgs(ielem) % a(:,1:pgaus,1)

       if( kfl_sgsti_nsi == 1 ) then
          gpsgs(:,1:pgaus,2) = vesgs(ielem) % a(:,1:pgaus,2)
       end if

    end if

  end subroutine nsi_subgrid_scale_gather

  !-----------------------------------------------------------------------
  !
  !> @brief   Gather subgrid scale
  !> @details Compute subgrid scale residual and update its value
  !> @author  Guillaume Houzeaux
  !
  !-----------------------------------------------------------------------

  subroutine nsi_subgrid_scale_residual_and_update(ndime,pgaus,ielem,gpsgs,vesgs,resgs)
    integer(ip), intent(in)            :: ndime
    integer(ip), intent(in)            :: pgaus
    integer(ip), intent(in)            :: ielem
    real(rp),    intent(in)            :: gpsgs(ndime,pgaus,*)
    type(r3p),   intent(out),  pointer :: vesgs(:)
    real(rp),    intent(out)           :: resgs(2)
    integer(ip)                        :: idime,igaus

    if( kfl_sgsco_nsi == 1 .or. kfl_sgsti_nsi == 1 ) then

       do igaus = 1,pgaus
          do idime = 1,ndime 
             resgs(1) = resgs(1) + ( gpsgs(idime,igaus,1) - vesgs(ielem) % a(idime,igaus,1) ) ** 2
             resgs(2) = resgs(2) +   gpsgs(idime,igaus,1) * gpsgs(idime,igaus,1)
             vesgs(ielem) % a(idime,igaus,1) = gpsgs(idime,igaus,1)
          end do
       end do

    end if

  end subroutine nsi_subgrid_scale_residual_and_update

end module mod_nsi_subgrid_scale
