subroutine nsi_tistep
!-----------------------------------------------------------------------
!****f* Nastin/nsi_tistep
! NAME 
!    nsi_tistep
! DESCRIPTION
!    This routine sets the time step. 
! USES
! USED BY
!    nsi_begite
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_nastin
  implicit none

  if(kfl_timco/=2) then

     dtinv_nsi = dtinv
     if( kfl_timei_nsi == 0) dtinv_nsi = 0.0_rp 
     if( kfl_stead_nsi == 1) dtinv_nsi = 0.0_rp
     if( kfl_sgsti_nsi == 0) dtsgs_nsi = 0.0_rp

     if(kfl_tisch_nsi==1) then
        !
        ! Trapezoidal rule: Euler iterations
        !
        pabdf_nsi(1) = 1.0_rp
        pabdf_nsi(2) = -1.0_rp
        if(ittim<=neule_nsi) then
           kfl_tiacc_nsi=1
        else
           kfl_tiacc_nsi=kfl_tiaor_nsi
        end if
        dtsgs_nsi = dtinv_nsi
        if( kfl_tiacc_nsi == 2 ) then
           pabdf_nsi(1) =  2.0_rp  ! from vers 772 theta is no longer included in dtinv_nsi , instead pabdf_nsi is exteded to CN also
           pabdf_nsi(2) = -2.0_rp
           if( kfl_sgsac_nsi == 2 ) dtsgs_nsi = 2.0_rp*dtsgs_nsi ! vers 772 sgs CN need to be updated for programing consistency
        end if
        nbdfp_nsi = 2

     else if(kfl_tisch_nsi==2) then
        !
        ! BDF scheme: increase integration order at each time step
        !
        dtsgs_nsi = dtinv_nsi
        if(ittim<=neule_nsi) then
           kfl_tiacc_nsi=1
        else
           kfl_tiacc_nsi=min(kfl_tiaor_nsi,ittim)
        end if
        call parbdf(kfl_tiacc_nsi,pabdf_nsi)
        nbdfp_nsi = kfl_tiacc_nsi + 1

     end if
     !
     ! If dt is prescribed, compute corresponding safety factor
     !
     if(kfl_timco==0) then
        if(dtcri_nsi/=0.0_rp.and.dtinv_nsi/=0.0_rp) then
           safet_nsi=1.0_rp/(dtcri_nsi*dtinv_nsi)
        else
           safet_nsi=1.0_rp
        end if
     end if
 
  else

     if(kfl_tisch_nsi==1) then
        !
        ! Trapezoidal rule: Euler iterations
        !
        nbdfp_nsi = 2       ! This line is needed the 4 below I am not totally sure
        pabdf_nsi(1) = 1.0_rp
        pabdf_nsi(2) = -1.0_rp
        dtsgs_nsi = dtinv_nsi
        if( kfl_tiacc_nsi == 2 ) call runend('LOCAL TIME STEP WITH CN NEEDS THINKING')


     else if(kfl_tisch_nsi==2) then
        !
        ! BDF scheme: increase integration order at each time step
        !
        call runend('LOCAL TIME STEP NO READY WITH BDF2')

     end if

  end if

  routp(1) = dtcri_nsi
  ioutp(1) = kfl_timei_nsi
  ioutp(2) = kfl_stead_nsi

  ! minimum time step when using local time step
  routp(2) = max(saflo_nsi, safet_nsi)*dtcri_nsi  

  !
  ! Look for maximum  (time step) over subdomains
  !
  if(kfl_timco==2.and.kfl_paral>=0)  &                      
       call pararr('MAX',0_ip,1_ip,dtmax_nsi)
  routp(3) = dtmax_nsi
  call outfor(8_ip,lun_outpu,' ')

end subroutine nsi_tistep
