!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    mod_nsi_solsch.f90
!> @author  Guillaume Houzeaux
!> @date    16/11/1966
!> @brief   This routine solves iterations of the Schur complement solver Richardson - Orthomin(1)
!> @details This routine solves iterations of the Schur complement solver Richardson - Orthomin(1)
!> @} 
!-----------------------------------------------------------------------
module mod_nsi_solsch

  use def_parame
  use def_master
  use def_domain
  use def_solver
  use def_nastin
  use mod_postpr
  use def_kermod, only : kfl_duatss
  use mod_maths,  only : maths_equalize_arrays
  use mod_memory, only : memory_alloca_min
  use mod_memory, only : memory_alloca
  implicit none
  integer(ip), private                   :: idofn,ipoin,icomp,idime,nu,np,nzP,izone
  integer(ip), private                   :: kfl_incre_nsi
  integer(ip), private,    save          :: ipass=0
  integer(4),  private                   :: istat
  real(rp),    private                   :: numer,denom,dummr
  real(rp),    private                   :: ak=1.0_rp,raux1,raux2,raux3,rauxc,rauxm
  real(rp),    private,    save          :: cschu=0.0_rp
  real(rp),                save, pointer :: xx(:),zz(:),vv(:),rr(:)
  real(rp),                      pointer :: bp(:),bu(:),uu(:),pp(:),Q(:)
  real(rp),                      pointer :: zzold(:)
  character(5)                           :: wopos(3)
#ifdef outmateocoe
  integer(ip)        :: ipass_aux
#endif
   
contains 

  subroutine nsi_solsch()

    if ( (kfl_expco_nsi/=1) .or. (itinn(modul)==1) ) then  ! for the case kfl_expco_nsi==1 do this part only the first iteration, else do it always

       !-------------------------------------------------------------------
       !                 +-       -+ +- -+   +-  -+
       !                 | Auu Aup | | u |   | bu |
       ! Assemble system |         | |   | = |    |
       !                 | Apu App | | p |   | bp |
       !                 +-       -+ +- -+   +-  -+
       !-------------------------------------------------------------------

       call nsi_inisch()                                            ! Allocate memory
       call nsi_solini(3_ip)                                        ! Initialize solver
       call nsi_rotsch(1_ip,uu)                                     ! Gobal to local
       call nsi_matrix()                                            ! Assemble equation
       call livinf(56_ip,' ',modul)

       !-------------------------------------------------------------------
       !                                            ----------------
       ! Schur complement preconditioning Q = App - Apu Auu^{-1} Aup
       ! if it was not done previously
       !
       !-------------------------------------------------------------------

       call nsi_solini(2_ip) 
       call nsi_schpre() 

    end if

    !-------------------------------------------------------------------
    !
    ! Iterative Schur complement solver
    !
    !-------------------------------------------------------------------

    call livinf(160_ip,' ',1_ip)  

    if( kfl_sosch_nsi == 2 ) then                 ! Orthomin(1): momentum preserving
       call nsi_solmom()  
       call nsi_solort()  
    else if( kfl_sosch_nsi == 3 ) then            ! Orthomin(1): continuity preserving
       call nsi_solmom() 
       call nsi_solort() 
       call nsi_solric()  
    else if( kfl_sosch_nsi == 4 ) then            ! Richardson
       call nsi_solmom()  
       call nsi_solric() 
    else if( kfl_sosch_nsi == 5 ) then            ! Richardson: momentum preserving
       call nsi_solmom() 
       call nsi_solric() 
       call nsi_solmom() 
    else if( kfl_sosch_nsi == 6 ) then            ! Richardson: continuity preserving
       call nsi_solmom() 
       call nsi_solric()  
    end if

    call nsi_rotsch(2_ip,uu)                      ! Lobal to global 
    !
    ! Velocity correction
    ! 
    if( kfl_sosch_nsi == 3 .or. kfl_sosch_nsi == 6 ) then
       call nsi_solcor()
    end if

    call livinf(164_ip,' ',1_ip) 

  end subroutine nsi_solsch

  subroutine nsi_solmom()

    !-------------------------------------------------------------------
    !
    ! Momentum equation 
    !
    ! INPUT  p^k
    ! OUTPUT u^k, r^k
    !
    !-------------------------------------------------------------------
    implicit none
    !
    ! Solve the momentum equation: Auu u^{k+1} = bu - Aup p^k
    !
    nmome_nsi =  nmome_nsi + 1
    call nsi_solini(1_ip)                                              ! Initialize solver

    if( INOTMASTER ) then

       call nsi_aupvec(1_ip,amatr(poaup_nsi),unkno(ndbgs_nsi+1),vv)    ! vv = Aup p^k
       do idofn = 1,nu                                                 ! vv = bu - Aup p^k
          vv(idofn) = bu(idofn) - vv(idofn)
       end do

    end if

    if( kfl_duatss == 1 ) then
       call nsi_sol_dts_prec(vv,uu,amatr(poauu_nsi),pmatr)
    else
       !
       ! For the moment I will only leave the second velocity solved with AGMG
       ! For the second velocity the initial guess es 0 and therefore ratio and toler
       ! are actually the same thing. We put 2 different values but actually the one
       ! that counts is the smaller (typically ratio). AGMG only has stopping by toler.
       ! Therefore Alya's solver and AGMG ae only comparable if we do not use adaptive
       ! (which would significantly alter our typical startegy) or if the initial guess is 0.
       !
!#ifdef solve_w_agmg
!       call nsi_agmgsol(vv,uu,amatr(poauu_nsi),r_sol,c_sol,ndime,npoin)
!#else
       call solver(vv,uu,amatr(poauu_nsi),pmatr)                          ! Solve system
!#endif   
    end if

    call livinf(165_ip,'M',0_ip) 

    if( INOTMASTER ) then

       call nsi_apuvec(1_ip,amatr(poapu_nsi),uu,rr)                    ! rr =  Apu u^{k+1}
       do ipoin = 1,np                                                 ! rr <= bp - rr 
          rr(ipoin) = rhsid(ndbgs_nsi+ipoin) - rr(ipoin)
       end do
       call nsi_appvec(2_ip,amatr(poapp_nsi),unkno(ndbgs_nsi+1),rr)    ! rr <= rr - App p^k

    end if

  end subroutine nsi_solmom

  subroutine nsi_solric()

    !-----------------------------------------------------------------------
    !
    !> @author  Guillaume Houzeaux
    !> @brief   Schur complement system with a preconditioned Richardson
    !>          iteration
    !> @details Solve and actualize pressure
    !>          INPUT  r^k
    !>          OUTPUT p^{k+1}
    !
    !-----------------------------------------------------------------------
    !
    ! Solve: Q z = r^k   with   Q = App + P
    !
    nschu_nsi = nschu_nsi + 1
    call nsi_solini(2_ip)                                     ! Initialize solver
    call nsi_inivec(np,zz)                                    ! Dp=0
    call solver(rr,zz,Q,pmatr)                                ! Solve system
    call livinf(165_ip,'S',0_ip) 
    !
    ! Actualize p^{k+1} = p^k + z
    !
    do ipoin = 1,np
       unkno(ndbgs_nsi+ipoin) = unkno(ndbgs_nsi+ipoin) + zz(ipoin)
    end do

  end subroutine nsi_solric

  subroutine nsi_solcor()    

    !-------------------------------------------------------------------
    !
    ! Correct velocity
    !
    ! INPUT  u^{k}, p^{k+1}, p^k
    ! OUTPUT u^{k+1}
    !
    !-------------------------------------------------------------------

    !                                  ----------
    ! Correction M u^{k+1} = M u^{k} - Auu^-1 Aup [p^{k+1})-p^k]
    !
    call nsi_elmcor()
    if( kfl_timin == 1 ) call Parall(20_ip)

    call livinf(165_ip,'C',0_ip) 

  end subroutine nsi_solcor

  subroutine nsi_solort()

    !-------------------------------------------------------------------
    !
    ! Schur complement system with a preconditioned Orthomin(1) iteration
    !
    ! INPUT  u^{k+1}
    ! OUTPUT u^{k+2}, p^{k+1}, r^{k+1}
    !
    !-------------------------------------------------------------------

    !integer(ip), save :: ipass = 0

    kfl_incre_nsi = 1
    !
    ! Solve: Q z = r^k   with   Q = App + P
    !
    nmome_nsi = nmome_nsi + 1
    nschu_nsi = nschu_nsi + 1
    call nsi_solini(2_ip)                                     ! Initialize solver

    if( kfl_incre_nsi == 0 ) then
       call bcsrax(0_ip,npoin,1_ip,lapla_nsi,c_dom,r_dom,unkno(ndbgs_nsi+1),zz) 
       do ipoin = 1,npoin
          vv(ipoin) = rr(ipoin)
          rr(ipoin) = rr(ipoin) + zz(ipoin)
          zz(ipoin) = unkno(ndbgs_nsi+ipoin)
       end do
    else
       call nsi_inivec(npoin,zz)                              ! Dp=0       
    end if

#ifdef outmateocoe
    call nsi_matndof(rr,zz,lapla_nsi,r_dom,c_dom,1_ip,npoin,ipass_aux)
#endif
  
    if( mitri_nsi == 1 ) then
#ifdef solve_w_agmg
       call nsi_agmgsol(rr,zz,lapla_nsi,r_dom,c_dom,1_ip,npoin)
#else
       call solver(rr,zz,lapla_nsi,pmatr)                        ! Solve system
#endif
    else
       call nsi_richar()
    end if

#ifdef outmateocoe
    call nsi_out_res_eocoe(zz,npoin,ipass_aux)
#endif

    call livinf(165_ip,'S',0_ip) 

    if( kfl_incre_nsi == 0 ) then
       do ipoin = 1,npoin
          zz(ipoin) = zz(ipoin) - unkno(ndbgs_nsi+ipoin)
          rr(ipoin) = vv(ipoin) 
       end do
    end if
    !
    ! Solve: Auu v = Aup z 
    !
    call nsi_solini(4_ip)   
    call nsi_aupvec(1_ip,amatr(poaup_nsi),zz,xx)              ! xx = Aup z
    call nsi_inivec(nu,vv)

#ifdef outmateocoe
    !calculate residual
!    allocate(hhh(nu))
!    do ipoin = 1,nu
!       hhh(ipoin) = xx(ipoin)
!    end do
!    call nsi_auuvec(2_ip,amatr(poauu_nsi),vv,hhh)         ! hhh = hhh - Auu vv
    ! norm hhh
!    norm_hhh=sqrt(dot_product(hhh,hhh))
!    print*,'norm_hhh=',norm_hhh
!    deallocate(hhh)
    !
    ! transform matrix to agmg format and output
    !
    call nsi_matndof(xx,vv,amatr(poauu_nsi),r_sol,c_sol,ndime,npoin,ipass_aux)
#endif
    
    if( kfl_duatss == 1 ) then
       call nsi_sol_dts_prec(xx,vv,amatr(poauu_nsi),pmatr)
    else
#ifdef solve_w_agmg
       call nsi_agmgsol(xx,vv,amatr(poauu_nsi),r_sol,c_sol,ndime,npoin)
#else
       call solver(xx,vv,amatr(poauu_nsi),pmatr)                         ! Solve system
#endif       
    end if

#ifdef outmateocoe
    call nsi_out_res_eocoe(vv,ndime*npoin,ipass_aux)
#endif

    call livinf(165_ip,'M',0_ip) 
    !
    ! Compute x = App z - Apu v
    !
    call nsi_appvec(1_ip,amatr(poapp_nsi),zz,xx)              ! xx = App z
    call nsi_apuvec(2_ip,amatr(poapu_nsi),vv,xx)              ! rr = Apu v  
    call rhsmod(1_ip,xx) 
    !
    ! Compute ak = <rr,xx>/<xx,xx>
    !    
    call prodts(1_ip,np,xx,rr,denom,numer)
    if( denom == 0.0_rp ) then
       ak = 1.0_rp
    else
       ak = numer / denom
    end if
    !
    ! Actualize p^{k+1} = p^k     + ak*z
    !           u^{k+2} = u^{k+1} - ak*v
    !
    do ipoin = 1,np
       unkno(ndbgs_nsi+ipoin) = unkno(ndbgs_nsi+ipoin) + ak * zz(ipoin)
    end do
    do idofn = 1,nu
       uu(idofn) = uu(idofn) - ak * vv(idofn)
    end do
    !
    ! Actualize r^{k+1} = r^k - ak*xx
    !
    call nsi_apuvec(1_ip,amatr(poapu_nsi),uu,rr)                ! rr = Apu u^{k+1}
    do ipoin = 1,np                                             ! rr = bp - rr 
       rr(ipoin) = rhsid(ndbgs_nsi+ipoin) - rr(ipoin)
    end do

    call nsi_appvec(2_ip,amatr(poapp_nsi),unkno(ndbgs_nsi+1),rr) ! rr = rr - App p^k
    !do ipoin=1,np
    !   rr(ipoin)=rr(ipoin)-ak*xx(ipoin) ! Cannot do this because therefore rr is global
    !end do

  end subroutine nsi_solort

  subroutine nsi_inisch()

    !-------------------------------------------------------------------
    !
    ! Initialize pointers
    !
    !-------------------------------------------------------------------

    if( IMASTER ) then                                         ! Pointers
       nu  =  0
       np  =  0 
       bu  => nul1r
       bp  => nul1r
       uu  => nul1r
       pp  => nul1r
       Q   => nul1r
       nzp =  0
    else
       nu  =  ndime*npoin
       np  =  npoin     
       bu  => rhsid
       bp  => rhsid(ndbgs_nsi+1:)
       uu  => unkno
       pp  => unkno(ndbgs_nsi+1:)
       Q   => lapla_nsi
       nzp =  size(Q)
    end if

    if( ipass == 0 ) then
       ipass = 1
       nullify(xx,zz,vv,rr)
       if( IMASTER ) then 
          call memory_alloca_min(xx)
          call memory_alloca_min(zz)
          call memory_alloca_min(vv)
          call memory_alloca_min(rr)
       else 
          call memory_alloca(mem_modul(1:2,modul),'XX','mod_nsi_solsch',xx,nu)
          call memory_alloca(mem_modul(1:2,modul),'ZZ','mod_nsi_solsch',zz,nu)
          call memory_alloca(mem_modul(1:2,modul),'VV','mod_nsi_solsch',vv,nu)
          call memory_alloca(mem_modul(1:2,modul),'RR','mod_nsi_solsch',rr,np)
       end if
       deltp_nsi => zz       
    end if

  end subroutine nsi_inisch

  !----------------------------------------------------------------------
  !>
  !> @author  Guillaume Houzeaux
  !> @date    13/10/2014
  !> @brief   Schur complement and momentum residuals 
  !> @details Compute momentum and continuity residuals for 
  !>          convergence and postprocess purposes:
  !>          Continuity: rp = bp - Apu.u - App.p
  !>          Momentum:   ru = bu - Auu.u - Aup.p
  !>          
  !>          OUTPUT resin_nsi, resou_nsi or resss_nsi
  !>
  !----------------------------------------------------------------------

  subroutine nsi_momentum_continuity_residuals(itask)
    integer(ip), intent(in) :: itask
    integer(ip)             :: ii
    real(rp)                :: resim,resis

    if( itask == 1 ) then                                               ! Inner residual
       ii = 1
    else if( itask == 2 ) then                                          ! Outer residual
       ii = 2
       if( momod(modul) % miinn == 1 .and. micou(iblok) <= 1 ) then
          resou_nsi(1) = resin_nsi(1)
          resou_nsi(2) = resin_nsi(2)
          return
       end if
    else                                                                ! Steady state residual
       ii = nprev_nsi
       if( momod(modul) % miinn == 1 .and. micou(iblok) <= 1 ) then
          resss_nsi(1) = resin_nsi(1)
          resss_nsi(2) = resin_nsi(2)
          return
       end if
    end if
    !
    ! Gobal to local
    !
    call nsi_rotsch(1_ip,veloc(1,1,ii)) 
    !
    ! Momentum residual: || [bu - Aup p^k] - Auu u^k || // || bu - Aup p^k ||
    !
    call nsi_aupvec(1_ip,amatr(poaup_nsi),press(1,ii),vv)           ! vv = Aup p^k
    do ipoin = 1,nu                                                 ! vv = bu - vv 
       xx(ipoin) = bu(ipoin) - vv(ipoin)
       vv(ipoin) = xx(ipoin)
    end do
    call nsi_auuvec(2_ip,amatr(poauu_nsi),veloc(1,1,ii),xx)         ! vv = vv - Auu u^k

    call nsi_solini(1_ip)
    call rhsmod(ndime,vv)
    call rhsmod(ndime,xx)
    call norm2x(ndime,vv,rauxm)
    call norm2x(ndime,xx,raux2)

    if( rauxm == 0.0_rp ) rauxm = 1.0_rp 
    resim = raux2 / (rauxm+zeror)                                   ! Residual norm for u

    if( postp(1) % npp_stepi(62) > 0 .and. INOTMASTER ) then
       call maths_equalize_arrays(xx,remom_nsi)
    end if
    !
    ! Continuity residual: || [bp - Apu u^k] - App p^k || // || bp - Apu u^k ||
    !
    call nsi_apuvec(1_ip,amatr(poapu_nsi),veloc(1,1,ii),rr)         ! rr = Apu u^k
    do ipoin = 1,np                                                 ! rr = bp - rr 
       zz(ipoin) = bp(ipoin) - rr(ipoin)
       rr(ipoin) = zz(ipoin) 
    end do
    call nsi_appvec(2_ip,amatr(poapp_nsi),press(1,ii),zz)           ! rr = rr - App p^k

    call nsi_solini(2_ip)
    call rhsmod(1_ip,rr)
    call rhsmod(1_ip,zz)
    call norm2x(1_ip,rr,rauxc)
    call norm2x(1_ip,zz,raux2)

    if( rauxc == 0.0_rp ) rauxc = 1.0_rp  
    resis = raux2 / (rauxc+zeror)                                   ! Residual norm for p

    if( postp(1) % npp_stepi(22) > 0  .and. INOTMASTER ) then
       call maths_equalize_arrays(zz,resch_nsi)
    end if
    !
    ! Local to global
    !
    call nsi_rotsch(2_ip,veloc(1,1,ii)) 

    if(itask==1) then
       resin_nsi(1)=resim
       resin_nsi(2)=resis
    else if(itask==2) then
       resou_nsi(1)=resim
       resou_nsi(2)=resis 
    else
       resss_nsi(1)=resim
       resss_nsi(2)=resis        
    end if

  end subroutine nsi_momentum_continuity_residuals

  subroutine nsi_richar()
    !
    ! Solve Q z = r using a Richardson method, where Q=D is the discrete Laplacian
    ! 
    ! z^{i+1} = z^i + C^-1 ( r - D z^i )
    !
    ! 1. Compute: r2 = r - Q z^i 
    ! 2. Solve:   C y = r2 => y = D ^-1 r2
    ! 3  Update:  z^{i+1} = z^i + y
    !
    ! Typically, C is the continuous Laplacian (symmetric) and D the discrete one 
    !
    use mod_matrix
    implicit none
    integer(ip)          :: iiter
    real(rp),    pointer :: yy(:)
    real(rp),    pointer :: zz_new(:)
    real(rp),    pointer :: r2(:)
    real(rp)             :: resid(2),alpha
    real(rp),    pointer :: invAuu(:)

    if( INOTMASTER ) then
       !
       ! Allocate memory
       !
       allocate( yy(npoin) )
       allocate( zz_new(npoin) )
       allocate( r2(npoin) )
       allocate( invAuu(ndime*npoin) ) 
       !
       ! Diagonal of Auu
       !
       call matrix_invdia(&
            npoin,ndime,solve(2) % kfl_symme,r_dom,c_dom,&
            amatr(poauu_nsi:),invAuu,memit)

       do ipoin = 1,npoin
          zz_new(ipoin) = zz(ipoin)
          yy(ipoin)     = 0.0_rp
          r2(ipoin)     = 0.0_rp
       end do

    else

       allocate( yy(1) )
       allocate( r2(1) )

    end if

    do iiter = 1,mitri_nsi
       !
       ! [ App - Apu diag(Auu)^-1 Aup ] z^i
       !
       call nsi_shuvec(&
            0_ip,amatr(poauu_nsi),amatr(poapp_nsi),amatr(poapu_nsi),invAuu,amatr(poaup_nsi),&
            zz_new,r2)
       if( INOTMASTER ) then                 
          do ipoin = 1,npoin
             r2(ipoin) = rr(ipoin) - r2(ipoin) 
             yy(ipoin) = 0.0_rp
          end do
       end if
       !
       ! Solve C y = r2
       !
       call solver(r2,yy,lapla_nsi,pmatr)

       alpha = 1.0_rp
       !call nsi_shuvec(&
       !     0_ip,amatr(poapp_nsi),amatr(poapu_nsi),invAuu,amatr(poaup_nsi),&
       !     yy,zz)
       !call prodxy(1_ip,npoin,zz,r2,alpha) 
       !call norm2x(1_ip,zz,denom)
       !alpha = alpha / (denom*denom)
       if( INOTMASTER ) then
          resid(1) = 0.0_rp
          resid(2) = 0.0_rp
          do ipoin = 1,npoin
             resid(1)      = resid(1)      + yy(ipoin) * yy(ipoin)
             resid(2)      = resid(2)      + zz_new(ipoin) * zz_new(ipoin)
             zz_new(ipoin) = zz_new(ipoin) + alpha * yy(ipoin)
          end do
       end if

       call pararr('SUM',0_ip,2_ip,resid)
       resid(1) = sqrt(resid(1)/(resid(2)+zeror))

    end do
    !
    ! Continuity residual MUST be global
    !
    call pararr('SLX',NPOIN_TYPE,npoin,rr)
    !
    ! Update solution and deallocate memory
    !
    if( INOTMASTER ) then
       do ipoin = 1,npoin
          zz(ipoin) = zz_new(ipoin)
       end do
       deallocate( invAuu )  
       deallocate( zz_new )
    end if
    deallocate( yy )
    deallocate( r2 )

  end subroutine nsi_richar 

end module mod_nsi_solsch

