subroutine nsi_begite()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_begite
  ! NAME 
  !    nsi_begite
  ! DESCRIPTION
  !    This routine starts an internal iteration for the incompressible NS
  !    equations. 
  ! USES
  !    nsi_tittim
  !    sni_updbcs
  !    nsi_inisol
  !    nsi_updunk
  ! USED BY
  !    nsi_doiter
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use mod_nsi_hydrostatic,  only : nsi_hydrostatic_pressure
  implicit none
  !
  ! Initializations
  !
  kfl_goite_nsi = 1
  itinn(modul)  = 0
  if( momod(modul) % miinn == 0 ) kfl_goite_nsi = 0
  if( itcou == 1 ) call nsi_tistep()
  if( NSI_MONOLITHIC ) call livinf(15_ip,' ',modul)
  ! 
  ! Update boundary conditions
  !
  call nsi_updbcs(two)
  !
  ! CFD wake
  !
  call nsi_cfdwak(2_ip)
  !
  ! Coupling
  !
  call nsi_coupli(ITASK_BEGITE)
  !
  ! Set up the solver parameters for the NS equations
  !
  call nsi_inisol(one)
  !
  ! If hydrostatic pressure should be updated
  !
  call nsi_hydrostatic_pressure(ITASK_BEGITE)
  !
  ! Obtain the initial guess for inner iterations, with the prescriptions
  ! in local coordinates.
  !
  call nsi_updunk(two)
  !
  ! Low-Mach
  ! initializes thermodynamic pressure, now made in temper
  !
  !  if (itcou == 1.and.ittim==1 )  call nsi_updthe(-1_ip) 

end subroutine nsi_begite



