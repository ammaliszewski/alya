!------------------------------------------------------------------------
!> @addtogroup NastinMatrixAssembly
!> @{
!> @file    nsi_elmope.f90
!> @author  Guillaume Houzeaux
!> @brief   Navier-Stokes system element assembly and other element 
!>          calculations
!> @details Elemental operations, according to ITASK:
!>
!>          \verbatim
!>
!>          1 ........ Element calculations and assembly of global system:
!>                     A <= A^(e): matrix ........................... AMATR
!>                     b <= b^(e): RHS .............................. RHSID
!>                     Q <= Q^(e): Schur complement precond ......... LAPLA_NSI
!>
!>                     Resulting system is A u* = b. For the pressure Schur 
!>                     complement-based solver, the following blocks are 
!>                     assembled:
!>
!>                     +-        +  +-  +     +-   +
!>                     | Auu Aup |  | u |     | bu |
!>                     |         |  |   |  =  |    |
!>                     | Apu App |  | p |     | bp |
!>                     +-       -+  +- -+     +-  -+
!>
!>          4 ........ Compute SGS .................................. VESGS_NSI
!>          5 ........ Assemble limiter to be L2 projected .......... RHSID
!>          6 ........ Assemble pressure Schur complement precond ... LAPLA_NSI
!>          10->19 ... Assemble properties to be L2 projected ....... PROPE_NSI
!>
!>          \endverbatim
!>
!> @} 
!------------------------------------------------------------------------
subroutine nsi_elmope_omp(itask)
use def_elmtyp, only : ELCUT
  use def_kintyp,            only : ip,rp
  use def_master,            only : amatr,rhsid,cutim,kfl_timco
  use def_master,            only : tesgs,current_zone,lumma,vesgs
  use def_kermod,            only : kfl_kemod_ker,kfl_prope
  use def_kermod,            only : kfl_duatss
  use def_domain,            only : nelez,lelez,ltype,nnode
  use def_domain,            only : ngaus,llapl,lorde,ltopo
  use def_domain,            only : ngaus,ndime,lnods,nmate
  use def_domain,            only : lelch,elmar,mnode,mgaus
  use def_domain,            only : lmate,hnatu,ntens
  use def_elmtyp,            only : ELEXT
  use def_nastin,            only : ncomp_nsi,dtmax_nsi
  use def_nastin,            only : kfl_stabi_nsi
  use def_nastin,            only : kfl_sgste_nsi,kfl_advec_nsi
  use def_nastin,            only : tamin_nsi,tamax_nsi
  use def_nastin,            only : dtinv_nsi,safet_nsi  
  use def_nastin,            only : dtcri_nsi,saflo_nsi
  use def_nastin,            only : dtsgs_nsi,kfl_stead_nsi
  use def_nastin,            only : kfl_timei_nsi,NSI_MONOLITHIC
  use def_nastin,            only : NSI_SCHUR_COMPLEMENT
  use def_nastin,            only : kfl_cotur_nsi,kfl_grvir_nsi
  use def_nastin,            only : prope_nsi,gradv_nsi
  use def_nastin,            only : lapla_nsi,kfl_sgscp_nsi
  use def_nastin,            only : kfl_force_nsi,kfl_shock_nsi
  use def_nastin,            only : kfl_matdi_nsi,poauu_nsi
  use def_nastin,            only : poaup_nsi,poapp_nsi
  use def_nastin,            only : poapu_nsi,ndbgs_nsi
  use def_nastin,            only : kfl_predi_nsi,kfl_ellen_nsi
  use def_nastin,            only : kfl_ellsh_nsi,lforc_material_nsi
  use def_nastin,            only : xforc_material_nsi
  use def_nastin,            only : resis_nsi,itsta_nsi
  use def_nastin,            only : rmsgs_nsi,resgs_nsi
  use def_nastin,            only : kfl_enric_nsi
  use mod_nsi_subgrid_scale, only : nsi_subgrid_scale_gather
  use mod_nsi_subgrid_scale, only : nsi_subgrid_scale_residual_and_update
  use mod_ker_proper,        only : ker_proper
  use mod_parall,            only : par_omp_num_colors
  use mod_parall,            only : par_omp_ia_colors
  use mod_parall,            only : par_omp_ja_colors
  use mod_parall,            only : par_omp_nelem_chunk
  implicit none

  integer(ip), intent(in) :: itask                     !< What to do
  ! 
  ! Element matrices and vectors (stiffness and preconditioner)
  ! 
  real(rp)    :: elauu(mnode*ndime,mnode*ndime)        ! Auu
  real(rp)    :: elaup(mnode*ndime,mnode)              ! Aup
  real(rp)    :: elapp(mnode,mnode)                    ! App
  real(rp)    :: elapu(mnode,mnode*ndime)              ! Apu
  real(rp)    :: elrbu(ndime,mnode)                    ! bu
  real(rp)    :: elrbp(mnode)                          ! bp
  real(rp)    :: elrhs(6*mnode)                        ! Generic RHS
  real(rp)    :: elmap(mnode,mnode)                    ! Q
  !
  ! Enrichement matrices
  !
  real(rp)    :: elaupe(mnode*ndime,1)                 ! Aupe
  real(rp)    :: elappe(mnode,1)                       ! Appe
  real(rp)    :: elapeu(1,mnode*ndime)                 ! Apeu
  real(rp)    :: elapep(1,mnode)                       ! Apep
  real(rp)    :: elapepe(1,1)                          ! Apepe  
  !
  ! Gather 
  !
  real(rp)    :: elvel(ndime,mnode,ncomp_nsi)          ! u
  real(rp)    :: elpre(mnode,ncomp_nsi-1)              ! p
  real(rp)    :: elfle(mnode)                          ! phi
  real(rp)    :: elcod(ndime,mnode)                    ! x
  real(rp)    :: elvep(ndime,mnode)                    ! Pi(momentum)
  real(rp)    :: elprp(mnode)                          ! Pi(div(u))
  real(rp)    :: elgrp(ndime,mnode)                    ! Pi(grad(p))
  real(rp)    :: elmut(mnode)                          ! mut
  real(rp)    :: eltem(mnode,ncomp_nsi)                ! T
  real(rp)    :: elwmean(mnode,ncomp_nsi)              ! W mean
  real(rp)    :: elmsh(ndime,mnode)                    ! u mesh
  real(rp)    :: elnor(ndime,mnode)                    ! Normal to the Level Set interface 
  real(rp)    :: elcur(mnode)                          ! Level Set interface curvature
  real(rp)    :: ellum(mnode)                          ! Lumped mass matrix
  !
  ! Indices and dimensions
  !
  integer(ip) :: ielem,inode
  integer(ip) :: pnode,pgaus,pevat,kelem,dummi
  integer(ip) :: pelty,plapl,porde,pmate,ptopo
  integer(ip) :: ipoin,ncolo,icolo
  !
  ! Gauss point values
  !
  real(rp)    :: gpsha(mnode,mgaus)                    ! N
  real(rp)    :: gpcar(ndime,mnode,mgaus)              ! dN/dxi
  real(rp)    :: gphes(ntens,mnode,mgaus)              ! d2N/dxidxj
  real(rp)    :: gplap(mnode,mgaus)                    ! Lapl(N)
  real(rp)    :: gpvol(mgaus)                          ! w*|J|, |J|
  real(rp)    :: gpvis(mgaus)                          ! Viscosity 
  real(rp)    :: gpgvi(ndime,mgaus)                    ! Viscosity gradients
  real(rp)    :: gpmut(mgaus)                          ! mut
  real(rp)    :: grvis(ndime,mgaus)                    ! grad(mut)
  real(rp)    :: gppor(mgaus)                          ! Porosity
  real(rp)    :: gpden(mgaus)                          ! Density
  real(rp)    :: gpfle(mgaus)                          ! Level set function
  real(rp)    :: gpst1(mgaus)                          ! tau1
  real(rp)    :: gpst2(mgaus)                          ! tau2
  real(rp)    :: gpsp1(mgaus)                          ! tau1'
  real(rp)    :: gpsp2(mgaus)                          ! tau2'
  real(rp)    :: gptt1(mgaus)                          ! tau1'/tau1
  real(rp)    :: gptt2(mgaus)                          ! tau2'/tau2
  real(rp)    :: gpadv(ndime,mgaus)                    ! u+u'
  real(rp)    :: gprhs(ndime,mgaus)                    ! RHS
  real(rp)    :: gprhs_sgs(ndime,mgaus)                ! RHS due to subscales
  real(rp)    :: gprhc(mgaus)                          ! RHS for the continuity equation (Low Mach)
  real(rp)    :: gprh2(mgaus)                          ! RHS for the residual of continuity equation (Low Mach)
  real(rp)    :: gpsgs(ndime,mgaus,2)                  ! u'
  real(rp)    :: gpsgt(mgaus)                          ! T'
  real(rp)    :: gppre(mgaus,ncomp_nsi-1)              ! p
  real(rp)    :: gpvel(ndime,mgaus,ncomp_nsi-1)        ! u
  real(rp)    :: gpgve(ndime,ndime,mgaus)              ! grad(u)
  real(rp)    :: gpgpr(ndime,mgaus,2)                  ! grad(p)
  real(rp)    :: gpgde(ndime,mgaus)                    ! grad(den)
  real(rp)    :: gptem(mgaus,ncomp_nsi)                ! T
  real(rp)    :: gpsgi(ndime,mgaus)                    ! SGS (work array)
  real(rp)    :: gpvep(ndime,mgaus)                    ! -tau1'*R(u) or tau1*rho*(a.grad)u
  real(rp)    :: gpprp(mgaus)                          ! tau2*div(u)
  real(rp)    :: gpgrp(ndime,mgaus)                    ! tau1'*( grad(p) - rho*f )
  real(rp)    :: gpfli(mgaus)                          ! phy_hyd
  real(rp)    :: gphyd(mgaus)                          ! rho_hyd
  real(rp)    :: gpmsh(ndime,mgaus)                    ! u_mesh
  real(rp)    :: gpnor(ndime,mgaus)                    ! LS normal
  real(rp)    :: gpcur(mgaus)                          ! LS curvature
  !
  ! Enrichement
  !
  real(rp)    :: gpsha_bub(1,mgaus)                    ! Ne
  real(rp)    :: gpcar_bub(ndime,1,mgaus)              ! dNe/dxi
  !
  ! Element characteristics (to compute tau1 and tau2)
  !
  real(rp)    :: tragl(ndime,ndime)
  real(rp)    :: chave(ndime,2)
  real(rp)    :: chale(2)
  real(rp)    :: hleng(ndime)
  real(rp)    :: dummr(2),dtcri
  real(rp)    :: dtinv_loc,dtsgs_loc
  !
  ! Perturbation and residuals
  !
  real(rp)    :: rmomu(mnode,mgaus)                    ! Residual velocity in momentum
  real(rp)    :: rmom2(ndime,ndime,mnode,mgaus)        ! Residual velocity in momentum
  real(rp)    :: rcont(ndime,mnode,mgaus)              ! Residual velocity in continuity
  real(rp)    :: wgrgr(mnode,mnode,mgaus)              ! grad(Ni).grad(Nj)
  real(rp)    :: wgrvi(mnode,mgaus)                    ! grad(mu).grad(Ni)
  real(rp)    :: agrau(mnode,mgaus)                    ! a.grad(Ni)
  real(rp)    :: p1vec(ndime,ndime,mnode,mgaus)        ! Test funct. velocity in momentum
  real(rp)    :: p1ve2(ndime,ndime,mnode,mgaus)        ! Test funct. velocity in momentum
  real(rp)    :: p2vec(ndime,mnode,mgaus)              ! Test funct. velocity in continuity
  real(rp)    :: p2sca(mnode,mgaus)                    ! Test function pressure in continuity

  integer(4)   :: idime,igaus  !
  ! Event and counters
  !
#ifdef EVENT
  call mpitrace_user_function(1)
#endif
  !
  ! Initialization
  !
  dtmax_nsi = -1.0_rp

#ifndef NO_COLORING
  colors: do icolo = 1,par_omp_num_colors   
#endif
     !
     ! OpenMP declarations 
     ! 
     !$OMP  PARALLEL DO                                                            &
     !$OMP  SCHEDULE     ( DYNAMIC , par_omp_nelem_chunk )                         & 
     !$OMP  DEFAULT      ( NONE )                                                  &
     !$OMP  PRIVATE      ( elauu, elaup, elapp, elapu, elrbu, elrhs, elrbp, elmap, &
     !$OMP                 elvel, elpre, elfle, elcod, elvep, elprp, elgrp,        &
     !$OMP                 elmut, eltem, ielem, kelem, igaus, pnode, pgaus, pevat, &
     !$OMP                 pelty, plapl, porde, pmate, ptopo, gpcar, gphes, gplap, &
     !$OMP                 gpvol, gpmut, grvis, gpfle, gpst1, gpst2, gpsp1, gpsp2, &
     !$OMP                 gptt1, gptt2, gpadv, gpsgt, gppre, gpvel, gpgve, gpgpr, &
     !$OMP                 gptem, gpsgi, gpvep, gpprp, gpgrp, gpfli, gphyd, tragl, &
     !$OMP                 chave, chale, hleng, dummr, rmomu, rcont, wgrgr, wgrvi, &
     !$OMP                 p1vec, p2vec, p2sca, elmsh, elnor, elcur, dtcri, gpsha, &
     !$OMP                 idime, gprhc, gpnor, agrau, dummi, gpcur, ellum, inode, &
     !$OMP                 ipoin, gprhs_sgs, elwmean, gpgde , gpden, gpvis, gppor, &
     !$OMP                 gprhs, gprh2, gpgvi, gpsgs, gpmsh, rmom2, p1ve2,        &
     !$OMP                 dtinv_loc, dtsgs_loc )                                  &
     !$OMP  SHARED       ( kfl_ellen_nsi, elmar, hnatu, kfl_advec_nsi, lorde,      &
     !$OMP                 itask, llapl, ndime, lmate, current_zone, lumma,        &
     !$OMP                 ltype, nnode,  ngaus, kfl_sgste_nsi, tesgs, lnods,      &
     !$OMP                 nmate, nelez, ltopo, lelch,                             &
     !$OMP                 kfl_cotur_nsi, poauu_nsi, poaup_nsi, poapu_nsi,         &
     !$OMP                 poapp_nsi, ndbgs_nsi, kfl_ellsh_nsi, kfl_predi_nsi,     &
     !$OMP                 safet_nsi, saflo_nsi, kfl_stead_nsi, kfl_timei_nsi,     &
     !$OMP                 kfl_kemod_ker, kfl_grvir_nsi, mgaus, cutim, lelez,      &
     !$OMP                 lforc_material_nsi,xforc_material_nsi, kfl_matdi_nsi,   &
     !$OMP                 kfl_stabi_nsi, kfl_timco, dtcri_nsi, kfl_prope,         &
     !$OMP                 kfl_force_nsi, kfl_sgscp_nsi, kfl_shock_nsi,            &
     !$OMP                 par_omp_ja_colors, par_omp_ia_colors, icolo,            &
     !$OMP                 par_omp_num_colors, NSI_MONOLITHIC, kfl_duatss,         &
     !$OMP                 NSI_SCHUR_COMPLEMENT,dtinv_nsi,dtsgs_nsi,               &
     !$OMP                 vesgs, prope_nsi, gradv_nsi, rhsid, amatr, lapla_nsi )  &
     !$OMP REDUCTION       ( +:resis_nsi,itsta_nsi,resgs_nsi )                     &
     !$OMP REDUCTION       ( MIN:tamin_nsi )                                       &  
     !$OMP REDUCTION       ( MAX:rmsgs_nsi,tamax_nsi,dtmax_nsi )                   
     ! 
     ! Loop over elements
     !     
#ifndef NO_COLORING
     elements: do kelem = par_omp_ia_colors(icolo),par_omp_ia_colors(icolo+1)-1
        ielem = par_omp_ja_colors(kelem) 
#else
     elements: do kelem = 1,nelez(current_zone)
        ielem = lelez(current_zone) % l(kelem)        
#endif
        pelty = ltype(ielem)
        !
        ! Element properties and dimensions
        !
        if( pelty > 0 ) then
           pnode = nnode(pelty)
           pgaus = ngaus(pelty)
           plapl = llapl(pelty) 
           porde = lorde(pelty)
           ptopo = ltopo(pelty)
           pevat = ndime * pnode
           if( kfl_stabi_nsi == 2 ) plapl = 0
           !
           ! Check if element is a solid
           !
           if( nmate > 1 ) then
              pmate = lmate(ielem)
           else
              pmate = 1
           end if
           !
           ! Initializations
           !
           gpden     =  0.0_rp
           gpvis     =  0.0_rp
           gppor     =  0.0_rp
           gprhc     =  0.0_rp
           gprh2     =  0.0_rp
           gpgvi     =  0.0_rp
           gpsgs     =  0.0_rp
           gpmsh     =  0.0_rp
           rmom2     =  0.0_rp
           p1ve2     =  0.0_rp  
           dtinv_loc = dtinv_nsi
           dtsgs_loc = dtsgs_nsi
           !
           ! Initializations of the subgrid scales
           !
           call nsi_subgrid_scale_gather(ndime,pgaus,ielem,vesgs,gpsgs)
           if( kfl_sgste_nsi == 1 ) then  
              gpsgt(1:pgaus) = tesgs(ielem) % a(1,1:pgaus,1)
           else
              gpsgt(1:pgaus) = 0.0_rp
           end if
           !
           ! Gather 
           !
           call nsi_elmga3(&
                pnode,lnods(1,ielem),elcod,elpre,elvel,elfle,&
                elvep,elprp,elgrp,elmut,eltem,elmsh,elnor,&
                elcur,elwmean)
           !
           ! HLENG and TRAGL at center of gravity
           !
           call elmlen(&
                ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&
                hnatu(pelty),hleng)
           !
           ! Compute the characteristic length: CHALE
           ! 
           call elmchl(&
                tragl,hleng,elcod,elvel,chave,chale,pnode,&
                porde,hnatu(pelty),kfl_advec_nsi,kfl_ellen_nsi)
           !
           ! Local time step: DTINV_LOC
           !
           ! Maximum time step between that given by the global safety factor saflo_nsi, 
           ! and local safet_nsi
           ! 
           if( kfl_timco == 2 ) then 
              call nsi_elmtss(&
                   pelty,pmate,pnode,lnods(1,ielem),ielem,elcod,elvel,&
                   gpcar,chale,hleng,dtcri)
              dtinv_loc = min(1.0_rp / (dtcri*safet_nsi), 1.0_rp/(dtcri_nsi*saflo_nsi))
              dtsgs_loc = min(1.0_rp / (dtcri*safet_nsi), 1.0_rp/(dtcri_nsi*saflo_nsi))
              if( kfl_stead_nsi == 1 ) dtinv_loc = 0.0_rp
              if( kfl_timei_nsi == 0 ) dtinv_loc = 0.0_rp              
              dtmax_nsi = max(dtmax_nsi,1.0_rp/dtinv_loc)  ! Stores maximum time step
           end if
           !
           ! Cartesian derivatives, Hessian matrix and volume: GPCAR, GPHES, PGVOL
           !
           call elmca2(&
                pnode,pgaus,plapl,elmar(pelty)%weigp,elmar(pelty)%shape,&
                elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,gpvol,gpsha,&
                gpcar,gphes,ielem)
           !
           ! Enrichement
           !
           !if( kfl_enric_nsi == 1 ) then
           !   !call elmca2(&
           !   !     1_ip,pgaus,plapl,elmar(pelty) % weigp,shape_e,&
           !   !     deriv_e,dummr,elcod,gpvol_e,gpsha_e,&
           !   !     gpcar_e,dummr,ielem)              
           !end if
           !
           ! Properties
           !
           call ker_proper('DENSI','PGAUS',dummi,ielem,gpden,pnode,pgaus,gpsha,gpcar)
           call ker_proper('GRDEN','PGAUS',dummi,ielem,gpgde,pnode,pgaus,gpsha,gpcar)
           call ker_proper('VISCO','PGAUS',dummi,ielem,gpvis,pnode,pgaus,gpsha,gpcar)
           !  BIFLU ignores laminar viscosity gradient - BIFL2 includes it
           call ker_proper('GRVIS','PGAUS',dummi,ielem,gpgvi,pnode,pgaus,gpsha,gpcar)
           call ker_proper('POROS','PGAUS',dummi,ielem,gppor,pnode,pgaus,gpsha,gpcar)
           call nsi_turbul(&
                itask,1_ip,pnode,pgaus,1_ip,pgaus,kfl_cotur_nsi,&
                gpsha,gpcar,hleng,elvel,elmut,gpden,gpvis,gpmut,&
                gpgvi,grvis,gpgve,ielem,kfl_kemod_ker)
           
           if( kfl_grvir_nsi == 0 ) gpgvi = 0.0_rp ! zero viscosity gradient

           if( itask >= 10 .and. itask < 20 ) then
              !
              ! Assemble properties
              !
              call asspro(&
                   itask,pnode,2_ip,pgaus,lnods(1,ielem),lelch(ielem),gpden,gpvis,&
                   gpvol,gpsha,elrhs,prope_nsi)

           else if( itask == 6 ) then

              !-------------------------------------------------------------
              !
              ! Assemble pressure equation only
              !
              !-------------------------------------------------------------

              call nsi_elmpri(&
                   pnode,pgaus,lnods(1,ielem),gpcar,gpvol,gpden,&
                   gpvis,gppor,gpsha,chale,elmap,elrhs)
              !call assrhs(&
              !     solve(2) % ndofn,pnode,lnods(1,ielem),elrhs,rhsid)
              !call assmat(&
              !     solve(2) % ndofn,pnode,pnode,npoin,solve(2) % kfl_algso,&
              !     ielem,lnods(1,ielem),elmap,lapla_nsi)   

              call nsi_assmat(&
                   -1_ip,pnode,pnode,lnods(1,ielem),elmap,dummr,dummr,&
                   dummr,lapla_nsi)
 
           else
              !
              ! Residual and RHS
              !
              call nsi_elmres(                                             &
                   pnode,pgaus,plapl,gpsha,gpcar,gphes,gpgvi,gpden,gpvis,  &
                   gppor,gptem,gpsgs,elvel,elpre,elvep,elprp,elgrp,        &
                   eltem,elmsh,elcod,elnor,elcur,elwmean,hleng,chale,gpvel,&
                   gpgpr,rmomu,rmom2,rcont,gprhs,gprhc,gplap,gpadv,gpvep,  &
                   gpprp,gpgrp,gphyd,gpmsh,gpgve,gpnor,gpcur,gpfle,        &
                   ielem,gprh2,gppre,gprhs_sgs,dtinv_loc,gpgde) 
              !
              ! Compute and assemble subgrid scale: VESGS
              !
              if( kfl_sgscp_nsi == 1 ) then
                 call nsi_updsgs(                                            &
                      pgaus,pnode,ndime,ielem,chale,elvel,gpadv,gpvis,gpden, &
                      rmomu,rmom2,gprhs,gpgpr,gpvel,gpcar,gpsp1,gpsgs,gpsgi, &
                      gpgve,gpvep,gpgrp,gpst1,gprhs_sgs,dtsgs_loc,resis_nsi, &
                      itsta_nsi,rmsgs_nsi,resgs_nsi,gppor)       
                 call nsi_subgrid_scale_residual_and_update(                 &
                      ndime,pgaus,ielem,gpsgs,vesgs,resgs_nsi)
              end if
              !
              ! Exact solution: GPRHS
              !
              call nsi_elmexa(                                            &
                   pgaus,pnode,gpsha,elcod,gpden,gpvis,gppor,gpgvi,cutim, &
                   dummr,gprhs,gprhc,gprh2)
              !
              ! External force: GPRHS
              !
              if( kfl_force_nsi == 1 ) then
                 call nsi_elmexf(                                  &
                      ndime,pgaus,lforc_material_nsi(pmate),gpden, &
                      xforc_material_nsi(1,pmate),gprhs,gpvel)
              end if

              if( itask == 4 ) then

                 !-------------------------------------------------------------
                 !
                 ! Compute and assemble subgrid scale: VESGS, RESGS_NSI
                 !
                 !-------------------------------------------------------------

                 call nsi_updsgs(                                            &
                      pgaus,pnode,ndime,ielem,chale,elvel,gpadv,gpvis,gpden, &
                      rmomu,rmom2,gprhs,gpgpr,gpvel,gpcar,gpsp1,gpsgs,gpsgi, &
                      gpgve,gpvep,gpgrp,gpst1,gprhs_sgs,dtsgs_loc,resis_nsi, &
                      itsta_nsi,rmsgs_nsi,resgs_nsi,gppor)
                 call nsi_subgrid_scale_residual_and_update(                 &
                      ndime,pgaus,ielem,gpsgs,vesgs,resgs_nsi)
                 call nsi_elmsgs(                                            &
                      pgaus,pnode,chale,hleng,gpadv,gpvis,gpden,gpcar,gpst1, &
                      gpst2,gpsp1,gpsp2,gptt1,gptt2,rmomu,gppor,dtsgs_loc,   &
                      tamin_nsi,tamax_nsi)
                 call nsi_elmort(                                            &
                      ielem,pgaus,pnode,ndime,elvel,elpre,rmomu,rmom2,gprhs, &
                      gpgpr,gpsha,gpvol,gpden,gpadv,gpcar,gpsp1,gpsp2,gpst1  )

              else if( itask == 5 ) then

                 !-------------------------------------------------------------
                 !
                 ! Limiter
                 !
                 !-------------------------------------------------------------

                 call nsi_elmsgs(                                               &
                      pgaus,pnode,chale,hleng,gpadv,gpvis,gpden,gpcar,gpst1,    &
                      gpst2,gpsp1,gpsp2,gptt1,gptt2,rmomu,gppor,dtsgs_loc,      &
                      tamin_nsi,tamax_nsi)
                 call nsi_asslim(                                               &
                      pnode,pgaus,lnods(1,ielem),gpden,gpsp1,gpadv,gpvep,gpvol, &
                      elvel,gpsha,gpcar,wgrvi,elrhs,rhsid)

              else if( itask /= 4 ) then

                 !-------------------------------------------------------------
                 !
                 ! Assemble equations
                 !
                 !-------------------------------------------------------------
                 !
                 ! Stabilization parameters
                 !
                 call nsi_elmsgs(                                              &
                      pgaus,pnode,chale,hleng,gpadv,gpvis,gpden,gpcar,gpst1,   &
                      gpst2,gpsp1,gpsp2,gptt1,gptt2,rmomu,gppor,dtsgs_loc,     &
                      tamin_nsi,tamax_nsi)
                 !
                 ! Assembly
                 !
                 if( kfl_stabi_nsi == 2 ) then
                    call nsi_elmma4(                                            &
                         pnode,pgaus,pevat,gpden,gpvis,gppor,gpsp1,gpsp2,gpvol, &
                         gpsha,gpcar,gpadv,gpvep,gpprp,gpgrp,gprhs,gpvel,gpsgs, &
                         wgrgr,agrau,elvel,elauu,elaup,elapp,elapu,elrbu,elrbp, &
                         dtinv_loc,dtsgs_loc)
                 else
                    call nsi_elmmat(                                      &
                         pnode,pgaus,pevat,gpden,gpvis,gppor,gpgvi,gpsp1, &
                         gptt1,gpsp2,gptt2,gpvol,gpsha,gpcar,gplap,gphes, &
                         gpadv,gpvep,gpprp,gpgrp,gprhs,gprhc,rmomu,rcont, &
                         p1vec,p2vec,p2sca,wgrgr,wgrvi,elauu,elaup,elapp, &
                         elapu,elrbu,elrbp,rmom2,p1ve2,gpst1,gpsgs,gpgve, &
                         gprh2,gppre,gprhs_sgs,elvel,ellum,dtinv_loc)
                 end if
                 !
                 ! Shock capturing
                 !
                 if( kfl_shock_nsi /= 0 ) then 
                    call nsi_elmshc(                                      &
                         pnode,pgaus,ptopo,pevat,ndime,gpden,gpvel,gprhs, &
                         gpsp1,gpsgs,gpvol,elvel,gpcar,chale,rmomu,rmom2, &
                         elauu)
                 end if
                 !
                 ! Extension elements
                 !
                 if( lelch(ielem) == ELEXT ) then
                    call nsi_elmext(&
                         1_ip,pnode,elauu,elaup,elapu,elapp,elmap,elrbu,elrbp)
                 end if
                 !
                 ! Prescribe Dirichlet boundary conditions
                 !
                 if( kfl_matdi_nsi == 0 ) &
                      call nsi_elmdi3(&
                      pnode,pevat,lnods(1,ielem),&
                      elauu,elaup,elapp,elapu,elrbu,elrbp)
                 !
                 ! Assembly: AMATR and RHSID
                 !
                 if( NSI_MONOLITHIC ) then
                    call nsi_assemble_monolithic(&            
                         pnode,pevat,lnods(1,ielem),elauu,elaup,elapp,elapu,&
                         elrbu,elrbp,amatr,rhsid)
                 else
                    call nsi_assemble_schur(&            
                         1_ip,pnode,pevat,lnods(1,ielem),elauu,elaup,elapp,elapu,&
                         elrbu,elrbp,amatr(poauu_nsi),amatr(poaup_nsi),amatr(poapp_nsi),&
                         amatr(poapu_nsi),rhsid,rhsid(ndbgs_nsi+1))
                 end if
                 !
                 ! Dual time step preconditioner
                 !
                 if( kfl_duatss == 1 ) then 
                    do inode = 1,pnode
                       ipoin = lnods(inode,ielem)
#ifdef NO_COLORING
                       !$OMP ATOMIC
#endif
                       lumma(ipoin) = lumma(ipoin) + ellum(inode)
                    end do
                 end if
                 !
                 ! Schur complement preconditioner: LAPLA_NSI
                 !
                 if(  NSI_SCHUR_COMPLEMENT .and.&
                      ( kfl_predi_nsi == 2 .or. kfl_predi_nsi == 3 .or. kfl_predi_nsi == 4 ) ) then

                    call elmchl(&
                         tragl,hleng,elcod,elvel,chave,chale,pnode,&
                         porde,hnatu(pelty),kfl_advec_nsi,kfl_ellsh_nsi)
                    call nsi_elmsch(&
                         pnode,pgaus,lnods(1,ielem),gpcar,gpvol,gpden,&
                         gpvis,gppor,gpsha,elvel,chale,gpsp1,&
                         elmap,dtinv_loc)
                    !
                    ! Extension elements 
                    !
                    if( lelch(ielem) == ELEXT ) then
                       call nsi_elmext(&
                            2_ip,pnode,elauu,elaup,elapu,elapp,elmap,elrbu,elrbp)
                    end if
                    call nsi_assemble_schur(&            
                         2_ip,pnode,pevat,lnods(1,ielem),dummr,dummr,elmap,dummr,&
                         dummr,dummr,dummr,dummr,lapla_nsi,&
                         dummr,dummr,dummr)
                 end if

              end if
           end if

        end if
     end do elements
     !$OMP END PARALLEL DO

#ifndef NO_COLORING
  end do colors
#endif
  !
  ! Event and counters
  !
#ifdef EVENT
  call mpitrace_user_function(0)
#endif

end subroutine nsi_elmope_omp
