subroutine nsi_updbcs(itask)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_updbcs
  ! NAME 
  !    nsi_updbcs
  ! DESCRIPTION
  !    This routine updates the velocity boundary conditions:
  !    1. Before a time step begins
  !    2. Before a global iteration begins
  !    3. Before an inner iteration begins
  ! USED BY
  !    nsi_begste
  !***
  !-----------------------------------------------------------------------
  use def_elmtyp
  use def_master
  use def_parame, only       :  pi
  use def_kermod
  use def_domain
  use def_nastin
  use mod_ker_space_time_function
  use mod_couplings, only : COU_PUT_VALUE_ON_TARGET
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,idime,iboun,dummi,itotv,izdom,jpoin
  integer(ip)             :: inodb,inode,ifunc,ibopo,jtask,jdime,kpoin
  integer(ip)             :: iperi,izone
  integer(ip)             :: kk
  real(rp)                :: venew,veold,cuti2,vefun(3)
  real(rp)                :: rot_matrix(ndime,ndime),udotn
  real(rp)                :: bvess_new(3),xnorm,relax,bvnew,bvold
  real(rp)                :: xx
  real(rp), external      :: funcre

  jtask = abs(itask) 
  select case( jtask )

  case( 0_ip )

     !-------------------------------------------------------------------
     !
     ! At the beginning of the run
     !
     !-------------------------------------------------------------------

     izone = lzone(ID_NASTIN)

     if( kfl_confi_nsi == 0 ) then 
        !
        ! Prescribe pressure: Automatic
        !    
        if( INOTMASTER ) then 
           inode = huge(ip)
           do kpoin = 1,npoiz(izone)
              ipoin = lpoiz(izone) % l(kpoin)
              if( lpoty(ipoin) /= 0 ) then
                 if( lninv_loc(ipoin) < inode ) inode = lninv_loc(ipoin)
              end if
           end do
        end if
        call parari('MIN',0_ip,1_ip,inode)
        if( INOTMASTER ) then 
           nodpr_nsi = 0 
           do ipoin = 1,npoin
              if( lninv_loc(ipoin) == inode ) then
                 nodpr_nsi = ipoin
              end if
           end do
        end if

     else if( kfl_confi_nsi > 0 ) then 
        !
        ! Prescribe pressure: On node
        ! If MM is used, the numbering refers to the MM(0) level, that is the original one
        !     
        if( INOTMASTER ) then 
           inode     = nodpr_nsi
           nodpr_nsi = 0
           kpoin     = 0
           do while( kpoin < npoiz(izone) )
              kpoin = kpoin + 1
              ipoin = lpoiz(izone) % l(kpoin)
              if( lnlev(ipoin) /= 0 ) then
                 if( meshe(0) % lninv_loc(lnlev(ipoin)) == inode ) then
                    nodpr_nsi = ipoin
                    kpoin = npoiz(izone)
                 end if
              end if
           end do
        end if

     else if( kfl_confi_nsi == -1 ) then 
        !
        ! Automatic bc for pressure: Prescribe when velocity is free
        !     
        if( INOTMASTER  ) then
           do kpoin = 1,npoiz(izone)
              ipoin = lpoiz(izone) % l(kpoin)
              ibopo = lpoty(ipoin)
              if( ibopo > 0 ) then
                 if( kfl_fixpr_nsi(1,ipoin) == 0 ) then
                    dummi = 0
                    do idime = 1,ndime
                       if( kfl_fixno_nsi(idime,ipoin) == 0 ) dummi = dummi + 1
                    end do
                    if( dummi == ndime ) kfl_fixpr_nsi(1,ipoin)=1  
                    !! You should also check above that the node is not periodic
                 end if
              end if
           end do
        end if
     end if
     !
     ! Prescribed node cannot be a periodic slave node
     !
     if( kfl_confi_nsi >= 0 .and. INOTMASTER ) then
        if( kfl_matdi_nsi /= 0 ) then
           do iperi = 1,nperi
              if( lperi(2,iperi) == nodpr_nsi ) then
                 nodpr_nsi = lperi(1,iperi)
              end if
           end do
        else
           do iperi = 1,nperi        
              if( lperi(2,iperi) == nodpr_nsi .or. lperi(1,iperi) == nodpr_nsi ) then
                 call runend('NSI_UPDBCS: DO NOT PRESCRIBE PRESSURE ON A MASTER OR SLAVE NODE')
              end if
           end do
        end if
     end if
     !
     ! User modifications to boundary conditions
     !
     if( INOTMASTER  ) then
        call nsi_modbcs()
     end if
     !
     ! Modify KFL_FIXPR_NSI if flow is confined
     !
     if( INOTMASTER ) then
        if( kfl_confi_nsi >= 0 .and. nodpr_nsi > 0 ) then
           ibopo = lpoty(nodpr_nsi)
           !           if( ibopo == 0 ) call runend('NSI_UPDBCS: PRESSURE IS PRESCRIBED ON INTERIOR NODE')
           kfl_fixpr_nsi(1,nodpr_nsi) = 1
        end if
     end if
     !
     ! Pressure Dirichlet
     !
     if( nodpr_nsi > 0 .and. INOTMASTER ) then
        bpess_nsi(1,nodpr_nsi)     = valpr_nsi
        kfl_fixpp_nsi(1,nodpr_nsi) = 1
     end if
     ! 
     ! Bemol: modify if using Parall
     !
     if( bemol_nsi > 0.0_rp .and. INOTMASTER ) then
        do iboun = 1,nboun
           if( kfl_fixbo_nsi(iboun) == 13 ) then
              do inodb = 1,nnode(ltypb(iboun))
                 ipoin = lnodb(inodb,iboun)
                 kfl_fixpr_nsi(1,ipoin) = 1
              end do
           end if
        end do
     end if
     !
     ! No-penetration in weak form
     !
     if( INOTMASTER ) then
        call memgen(1_ip,npoin,0_ip)
        do iboun = 1,nboun
           if( kfl_fixbo_nsi(iboun) == 18 ) then
              do inodb = 1,nnode(abs(ltypb(iboun)))
                 ipoin = lnodb(inodb,iboun)
                 gisca(ipoin) = 1
              end do
           end if
        end do
        call parari('SLX',NPOIN_TYPE,npoin,gisca)
        do ipoin = 1,npoin
           if( gisca(ipoin) /= 0 ) then
              kfl_fixpr_nsi(1,ipoin) = 0
           end if
        end do
        call memgen(3_ip,npoin,0_ip)
     end if

     if( INOTMASTER.and.exfpr_nsi == 1 ) then
        write (*,*) 'CONDITION EXFPR, EXTENDING FIXPR NODES '
        call memgen(1_ip,npoin,0_ip)
        do ipoin = 1,npoin
           if( kfl_fixpr_nsi(1,ipoin) == 1 ) then
              do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
                 jpoin = c_dom(izdom)
                 if( kfl_fixpr_nsi(1,jpoin) == 0 ) gisca(jpoin) = 1
              end do
           end if
        end do
        call parari('SLX',NPOIN_TYPE,npoin,gisca)
        do ipoin = 1,npoin
           if( gisca(ipoin) /= 0 .and. lpoty(ipoin)/=0) then
              kfl_fixpr_nsi(1,ipoin) = 1
           end if
        end do
        call memgen(3_ip,npoin,0_ip)
     end if

  case( 1_ip )

     !-------------------------------------------------------------------
     !  
     ! Before a time step
     !     
     !-------------------------------------------------------------------



     !if( ittim == 3 ) then
     !   allocate( wboun(nboun) ) 
     !   do iboun = 1,nboun
     !      if( kfl_fixbo_nsi(iboun) == 1 ) then
     !         wboun(iboun) == .true.
     !      else
     !         wboun(iboun) == .false.
     !      end if             
     !   end do
     !   deallocate( wboun )
     !end if
     if( INOTMASTER .and. kfl_conbc_nsi == 0 ) then
        !
        ! Space/Time functions
        !
        if( number_space_time_function > 0 ) then
           do ipoin = 1,npoin
              if( kfl_funno_nsi(ipoin) < 0 ) then 
                 ifunc = -kfl_funno_nsi(ipoin)            
                 call ker_space_time_function(&
                      ifunc,coord(1,ipoin),coord(2,ipoin),coord(ndime,ipoin),cutim,vefun(1:ndime))
                 do idime = 1,ndime
                    vefun(idime) = vefun(idime) * bvess_nsi(idime,ipoin,2)
                 end do
                 if( kfl_timei_nsi /= 0 .and. kfl_tiacc_nsi == 2 .and. kfl_tisch_nsi == 1 ) then
                    do idime = 1,ndime
                       veold = veloc(idime,ipoin,ncomp_nsi)
                       bvess_nsi(idime,ipoin,1) = 0.50_rp*(vefun(idime)+veold)
                    end do
                 else
                    do idime = 1,ndime
                       bvess_nsi(idime,ipoin,1) = vefun(idime)
                    end do
                 end if
              end if
           end do
        end if
        !
        ! Transient fields
        !
        if( kexist_tran_fiel > 0 ) then
           do ipoin = 1,npoin
              if( kfl_funno_nsi(ipoin) > interval_funno ) then 
                 ifunc = kfl_funno_nsi(ipoin) - interval_funno
                 kk = k_tran_fiel(ifunc)
                 xx = x_tran_fiel(ifunc)
                 do idime = 1,ndime
                    vefun(idime) = xfiel(ifunc) % a(idime+(kk-1)*ndime,ipoin) * xx + &
                         xfiel(ifunc) % a(idime+(kk)*ndime,ipoin) * (1.0_rp-xx)
                 end do
                 !
                 ! These lines are identical to the ones from space time function perhaps it would be nicer to create a small subroutine  
                 !
                 if( kfl_timei_nsi /= 0 .and. kfl_tiacc_nsi == 2 .and. kfl_tisch_nsi == 1 ) then
                    do idime = 1,ndime
                       veold = veloc(idime,ipoin,ncomp_nsi)
                       bvess_nsi(idime,ipoin,1) = 0.50_rp*(vefun(idime)+veold)
                    end do
                 else
                    do idime = 1,ndime
                       bvess_nsi(idime,ipoin,1) = vefun(idime)
                    end do
                 end if
              end if
           end do
        end if
        !
        ! Nodes with fixity 7 (open or close uses) bvess_nsi which has already been updated if it comes from a transient field
        !
        if( kfl_exist_fixi7_nsi==1 .and. itask==1 ) then    

           call memgen(1_ip,npoin,0_ip)   !allocate gisca
           call open_close(1,kfl_fixno_nsi,bvess_nsi,ndime)

           do ipoin = 1,npoin
              if( abs(kfl_fixno_nsi(1,ipoin)) == 7 ) then
                 if ( kfl_fixrs_nsi(lpoty(ipoin)) /= 0 ) then
                    print*,'ipoinwith skew',lninv_loc(ipoin)
                    call runend('nsi_updbcs:ipoinwith skew')
                 end if
                 !
                 ! Open of close point - the decision has already been taken in open_close and is stored in gisca
                 !
                 if( gisca(ipoin) > 0 ) then
                    kfl_fixno_nsi(1:ndime-1,ipoin) = 7
                    veloc(1:ndime-1,ipoin,2) = bvess_nsi(1:ndime-1,ipoin,1)  ! correct initial guess for outer iterations  - x & y components
                    if( abs(kfl_fixno_nsi(ndime,ipoin)) == 7 ) then          ! z component
                       kfl_fixno_nsi(ndime,ipoin) = 7 !if z component is fixed(771) it will not be modified
                       veloc(ndime,ipoin,2) = bvess_nsi(ndime,ipoin,1)  ! correct initial guess for outer iterations - this will be used by tem & tur_updbcs
                    end if
                    kfl_fixpr_nsi(1,ipoin) = 0    ! for fixpr I don not care to use a 7 as it is what happens with velocity that decides

                    veloc(1:ndime,ipoin,2) = bvess_nsi(1:ndime,ipoin,1)    ! not sure if it is ok for CN
                 else
                    kfl_fixno_nsi(1:ndime-1,ipoin) = -7    
                    if( abs(kfl_fixno_nsi(ndime,ipoin)) == 7 ) kfl_fixno_nsi(ndime,ipoin) = -7
                    kfl_fixpr_nsi(1,ipoin) = 1
                 end if
              end if
           end do
           call memgen(3_ip,npoin,0_ip)   !deallocate gisca

        end if
        !
        ! BVESS: Dirichlet time dependent condition, time functions
        !
        do ipoin = 1,npoin
           ifunc = kfl_funno_nsi(ipoin)

           if( kfl_fixno_nsi(1,ipoin) == 9 ) then
              !
              ! U(t) = U_inf - w x r
              !
              bvess_nsi(:,ipoin,1)=bvess_nsi(:,ipoin,2)
              if(ndime==2) then
                 bvess_nsi(1,ipoin,1) = bvess_nsi(1,ipoin,1) &
                      + fvela_nsi(3) * coord(2,ipoin)
                 bvess_nsi(2,ipoin,1) = bvess_nsi(2,ipoin,1) &
                      - fvela_nsi(3) * coord(1,ipoin)
              else
                 bvess_nsi(1,ipoin,1) = bvess_nsi(1,ipoin,1) &
                      + fvela_nsi(3) * coord(2,ipoin) &
                      - fvela_nsi(2) * coord(3,ipoin)
                 bvess_nsi(2,ipoin,1) = bvess_nsi(2,ipoin,1) &
                      - fvela_nsi(3) * coord(1,ipoin) &
                      + fvela_nsi(1) * coord(3,ipoin)
                 bvess_nsi(3,ipoin,1) = bvess_nsi(3,ipoin,1) &
                      - fvela_nsi(1) * coord(2,ipoin) &
                      + fvela_nsi(2) * coord(1,ipoin)        
              end if
              
           else if( abs(kfl_fixno_nsi(1,ipoin)) == 8 ) then
              !
              !             R(theta)
              ! No inertial ========> Lab
              !
              !             R(-theta)
              !         Lab ========> No inertial
              !
              ! u(t) = R(-theta).U_inf - w x r
              ! U(t) = R(theta).( u + w x r ) (used in INERTIAL postprocess, to be used with postprocess of Mesh rotation)
              !
              call rotmat(ndime,-cutim*fvnoa_nsi,fvdia_nsi,rot_matrix)
              do idime = 1,ndime
                 bvess_new(idime) = 0.0_rp
                 do jdime = 1,ndime
                    bvess_new(idime) = bvess_new(idime) + rot_matrix(idime,jdime)*bvess_nsi(jdime,ipoin,2)
                 end do
              end do
              if( ndime == 2 ) then
                 bvess_new(1) = bvess_new(1) &
                      + fvela_nsi(3) * coord(2,ipoin)
                 bvess_new(2) = bvess_new(2) &
                      - fvela_nsi(3) * coord(1,ipoin)
              else
                 bvess_new(1) = bvess_new(1) &
                      + fvela_nsi(3) * coord(2,ipoin) &
                      - fvela_nsi(2) * coord(3,ipoin)
                 bvess_new(2) = bvess_new(2) &
                      - fvela_nsi(3) * coord(1,ipoin) &
                      + fvela_nsi(1) * coord(3,ipoin)
                 bvess_new(3) = bvess_new(3) &
                      - fvela_nsi(1) * coord(2,ipoin) &
                      + fvela_nsi(2) * coord(1,ipoin)        
              end if

              ibopo = lpoty(ipoin)
              if( ibopo > 0 ) then
                 udotn = 0.0_rp
                 xnorm = 0.0_rp
                 do idime = 1,ndime
                    xnorm = xnorm + bvess_new(idime) * bvess_new(idime)
                 end do
                 if( xnorm > zeror ) xnorm = 1.0_rp / sqrt(xnorm)
                 do idime = 1,ndime
                    udotn = udotn + exnor(idime,1,ibopo) * bvess_new(idime) 
                 end do
                 udotn = udotn * xnorm
                 !
                 udotn = -1.0_rp                  !  All Dirichlet !!!!
                 !
                 if( udotn <= 0.1_rp ) then       !  Inflow
                    if( kfl_fixno_nsi(1,ipoin) == -8 ) then
                       relax = 0.1_rp
                       do idime = 1,ndime
                          kfl_fixno_nsi(idime,ipoin) =  8
                          kfl_fixpr_nsi(1,ipoin)     =  0
                          bvess_nsi(idime,ipoin,1)   =  relax * bvess_new(idime) + (1.0_rp-relax) * bvess_nsi(idime,ipoin,1)
                       end do
                    else
                       do idime = 1,ndime
                          kfl_fixno_nsi(idime,ipoin) =  8
                          kfl_fixpr_nsi(1,ipoin)     =  0
                          bvess_nsi(idime,ipoin,1)   =  bvess_new(idime)
                       end do
                    end if
                 else                             !  Outflow
                    do idime = 1,ndime
                       kfl_fixno_nsi(idime,ipoin) = -8
                       kfl_fixpr_nsi(1,ipoin)     =  1
                    end do
                 end if
              end if

           else if( ifunc > 0 .and. ifunc <= interval_funno ) then
              !
              ! Function: U(t)=f(t)*U(0)
              !
              do idime = 1,ndime
                 if( kfl_fixno_nsi(idime,ipoin) == 1 ) then     
                    venew = bvess_nsi(idime,ipoin,2) * funcre( &
                         time_function(ifunc) % parameters,    &
                         time_function(ifunc) % npara,         &
                         time_function(ifunc) % kfl_type,      &
                         cutim)
                    if( kfl_timei_nsi /= 0 .and. kfl_tiacc_nsi == 2 .and. kfl_tisch_nsi == 1 ) then
                       veold = veloc(idime,ipoin,ncomp_nsi)
                       bvess_nsi(idime,ipoin,1) = 0.50_rp*(venew+veold)
                    else
                       bvess_nsi(idime,ipoin,1) = venew                 
                    end if
                 end if
              end do
           else if( kfl_fixno_nsi(1,ipoin) == 5 ) then
              !
              ! SEM: synthetic eddy method
              !
              !    bvess_nsi(1,ipoin,1) = 3.7_rp
              !    do idime = 2,ndime
              !       bvess_nsi(idime,ipoin,1) = 0.0_rp                 
              !    end do             

           else if( kfl_fixno_nsi(1,ipoin) == 6 ) then
              !
              ! SPACE-TIME boundary condition read from file
              !
              if (ittim > 0 ) then
                do idime = 1,ndime
                   kfl_fixno_nsi(idime,ipoin) = 6
                   bvess_nsi(idime,ipoin,1) = bnval_nsi((ittim-1)*nbval_nsi+iboun_nsi(ipoin),idime) 
                end do
              else
                do idime = 1,ndime
                   kfl_fixno_nsi(idime,ipoin) = 6
                   bvess_nsi(idime,ipoin,1) = bnval_nsi(iboun_nsi(ipoin),idime)
                end do
              end if
           end if
        end do
        !
        ! BVNAT: Neumann space/time or time functions
        !
        if( itask < 0 ) then
           cuti2 = cutim + dtime
        else
           cuti2 = cutim
        end if
        do iboun = 1,nboun
           if( kfl_fixbo_nsi(iboun) /= 0 ) then
              ifunc = kfl_funbo_nsi(iboun)
              if( ifunc > 0 ) then
                 venew = bvnat_nsi(1,iboun,2) *funcre(   &
                      time_function(ifunc) % parameters, & 
                      time_function(ifunc) % npara,      &
                      time_function(ifunc) % kfl_type,   &
                      cuti2)
                 if( kfl_timei_nsi /= 0 .and. kfl_tiacc_nsi == 2 .and. kfl_tisch_nsi == 1 ) then
                    bvnat_nsi(1,iboun,1) = 0.50_rp * ( venew + bvnat_nsi(1,iboun,1) )
                 else
                    bvnat_nsi(1,iboun,1) = venew                 
                 end if
              else if( ifunc < 0 ) then
                 ifunc = -kfl_funbo_nsi(iboun)            
                 call ker_space_time_function(&
                      ifunc,coord(1,ipoin),coord(2,ipoin),coord(ndime,ipoin),cutim,bvnew)
                 bvnew = bvnew * bvnat_nsi(1,iboun,2)
                 if( kfl_timei_nsi /= 0 .and. kfl_tiacc_nsi == 2 .and. kfl_tisch_nsi == 1 ) then
                    bvold = bvnat_nsi(1,iboun,1)
                    bvnat_nsi(1,iboun,1) = 0.50_rp*(bvnew+bvold)
                 else
                    bvnat_nsi(1,iboun,1) = bvnew
                 end if
              else if (kfl_cadan_nsi == 1) then

                 if (kfl_fixbo_nsi(iboun) == 2) bvnat_nsi(1,iboun,1) = press_cadan_nsi

              end if
           end if
        end do

     end if
     !
     ! Impose boundary conditions on VELOC
     !
     if( INOTMASTER ) then
        do ipoin = 1,npoin
           do idime = 1,ndime
              if( kfl_fixno_nsi(idime,ipoin) > 0 ) then
                 veloc(idime,ipoin,1) = bvess_nsi(idime,ipoin,1)
              end if
           end do
        end do
     end if

  case( 2_ip )

     !-------------------------------------------------------------------
     !
     ! Before a global iteration
     !  
     !-------------------------------------------------------------------

  case( 3_ip )

     !-------------------------------------------------------------------
     !
     ! Before an inner iteration
     ! 
     !-------------------------------------------------------------------

     if( INOTMASTER ) then
     end if

  case(4_ip)

     !-------------------------------------------------------------------
     !
     ! Enforce boundary conditions
     !
     !-------------------------------------------------------------------

     if( NSI_MONOLITHIC ) then
        call nsi_rotunk(1_ip,unkno) ! Gobal to local
        do ipoin = 1,npoin
           itotv = (ipoin-1)*(ndime+1)
           do idime = 1,ndime
              itotv = itotv + 1
              if(  kfl_fixno_nsi(idime,ipoin) == 1 .or. &
                   kfl_fixno_nsi(idime,ipoin) == 8 .or. &
                   kfl_fixno_nsi(idime,ipoin) == 9 .or. &
                   kfl_fixno_nsi(idime,ipoin) == 6   ) then
                 unkno(itotv) = bvess_nsi(idime,ipoin,1)
              end if
           end do
        end do
        call nsi_rotunk(2_ip,unkno) ! Local to global
     else
        call nsi_rotsch(1_ip,unkno) ! Global to local
        itotv = 0
        do ipoin = 1,npoin
           do idime = 1,ndime
              itotv = itotv + 1
              if(  kfl_fixno_nsi(idime,ipoin) == 1 .or. &
                   kfl_fixno_nsi(idime,ipoin) == 8 .or. &
                   kfl_fixno_nsi(idime,ipoin) == 9 .or. &
                   kfl_fixno_nsi(idime,ipoin) == 6   ) then
                 unkno(itotv) = bvess_nsi(idime,ipoin,1)
              end if
           end do
        end do
        call nsi_rotsch(2_ip,unkno) ! Local to global
     end if

  end select

end subroutine nsi_updbcs



