subroutine nsi_restar(itask)
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_restar
  ! NAME 
  !    nsi_restar
  ! DESCRIPTION
  !    This routine:
  !    ITASK = 1 ... Reads the initial values from the restart file
  !            2 ... Writes restart file
  ! USES
  ! USED BY
  !    nsi_turnon
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use mod_postpr
  use mod_memchk
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: icomp,kfl_gores,idime,igaus
  integer(ip)             :: ielem,pgaus,pelty
  character(5)            :: wopos(3)
  !
  ! Check if restrt file should be read or written
  !
  call respre(itask,kfl_gores)
  if( kfl_gores == 0 ) return

  if( itask == READ_RESTART_FILE ) then
     icomp = nprev_nsi
  else
     icomp = 1
  end if

  !----------------------------------------------------------------------
  !
  ! Velocity and pressure 
  !
  !----------------------------------------------------------------------
  gevec => veloc(:,:,icomp)
  call postpr(gevec,postp(1)%wopos(1:3,1),ittim,cutim)
  gesca => press(:,icomp)
  call postpr(gesca,postp(1)%wopos(1:3,2),ittim,cutim)

  if (kfl_tisch_nsi==2) then    ! BDF
     if (ncomp_nsi > 3) then    ! BDF2 and bigger
        gevec => veloc(:,:,4)
        call postpr(gevec,postp(1)%wopos(1:3,41),ittim,cutim)

        gesca => press(:,4)
        call postpr(gesca,postp(1)%wopos(1:3,42),ittim,cutim)
     end if

     if (ncomp_nsi > 4) then  ! BDF3 and bigger ! beware for BDF4 and bigger more components should be written
        gevec => veloc(:,:,5)
        call postpr(gevec,postp(1)%wopos(1:3,43),ittim,cutim)

        gesca => press(:,5)
        call postpr(gesca,postp(1)%wopos(1:3,44),ittim,cutim)
     end if
  end if

  !----------------------------------------------------------------------
  !
  ! Hydrostatic values
  !
  !----------------------------------------------------------------------

  if( kfl_hydro_nsi /= 0 ) then
     call postpr(bpess_nsi,postp(1) % wopos(1:3,31),ittim,cutim)
  end if
  if( kfl_hydro_gravity_nsi /= 0 ) then
     call runend('NSI_RESTAR: NOT CODED')
     ! should postprocess NSI_HYDRO_DENSITY or not
     !wopos(1) = 'FLEVE'
     !wopos(2) = 'SCALA'
     !wopos(3) = 'NPOIN'
     !call postpr(,wopos,ittim,cutim)    
  end if

  !----------------------------------------------------------------------
  !
  ! Projections for OSS stabilization
  !
  !----------------------------------------------------------------------

  if( kfl_stabi_nsi /= 0 ) then
     !
     ! Velocity
     !
     call postpr(vepro_nsi,postp(1)%wopos(1:3,23),ittim,cutim)
     !
     ! Velocity divergence
     !
     call postpr(prpro_nsi,postp(1)%wopos(1:3,24),ittim,cutim)

  end if

  if( kfl_stabi_nsi == 2 ) then
     !
     ! Pressure gradients
     !
     call postpr(grpro_nsi,postp(1)%wopos(1:3,27),ittim,cutim)

  end if

  !----------------------------------------------------------------------
  !
  ! SGS for tracking
  !
  !----------------------------------------------------------------------

  if( kfl_sgsco_nsi /= 0 .or. kfl_sgsti_nsi /= 0 ) then
     !
     ! Velocity SGS
     ! 
     call postpr(vesgs,postp(1)%wopos(1:3,5),ittim,cutim,1_ip)

     if(itask == READ_RESTART_FILE .and. INOTMASTER ) then
        do ielem = 1,nelem
           pelty = abs(ltype(ielem))
           pgaus = ngaus(pelty)
           do igaus = 1,pgaus
              do idime = 1,ndime
                 vesgs(ielem)%a(idime,igaus,2) = vesgs(ielem)%a(idime,igaus,1)
              end do
           end do
        end do
     end if

  end if
  !
  ! Finish
  !
  call respre(3_ip,kfl_gores)

end subroutine nsi_restar
 
