!-----------------------------------------------------------------------
!> @addtogroup NastinMatrixAssembly
!> @{
!> @file    nsi_bouope.f90
!> @author  Guillaume Houzeaux
!> @brief   Matrix assembly: boundary contribution
!> @details Boundary operations
!>          - Element matrix calculation
!>          - Scatter in global matrix
!>          Used variables:
!>          LNNOB(1:NBOUN) ........... Number of boundary nodes for each IBOUN
!>          KFL_FIXBO(1:NBOUN) ....... Condition type (e.g.: 3 is wall law)
!>          LTYPB(1:NBOUN) ........... Boundary type
!>          LNODB(1:MNODB,1:NBOUN) ... Boundary connectvity
!>          LBOEL(MNODB,1:NBOUN) ..... Local boundary to local element numbering
!>
!>                          45        77
!>                           o---------o  Example: PNODE = 2
!>                           |         |  LNODS(1:PNODE,IBOUN) = 77,45,6,12
!>                           |  IELEM  |  LNODB(1:PNODB,IBOUN) = 6,12
!>                           |         |  
!>                           o--IBOUN--o----------O
!>                           6 \\\\\\\ 12 \\\\\\\\\
!>
!>                           1,2,3,4 = Local numbering 
!>
!>                           2         1
!>                           o---------o  Example: PNODB = 2
!>                           |         |  LBOEL(1:PNODB,IBOUN) = 3,4
!>                           |  IELEM  |  LBOEL(PNODB+1,IBOUN) = IELEM
!>                           |         |  LNODB(1:PNODB,IBOUN) = 
!>                           o--IBOUN--o----------O
!>                           3 \\\\\\\ 4 \\\\\\\\\
!>           
!>
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_bouope()
  use def_parame
  use def_elmtyp
  use def_master
  use def_kermod
  use def_domain
  use def_nastin
  use mod_ker_proper
  implicit none

  real(rp)    :: elmat(nevat_nsi,nevat_nsi)                ! Element matrices
  real(rp)    :: elmap(mnode,mnode)
  real(rp)    :: wmatr(nevat_nsi,nevat_nsi)
  real(rp)    :: elrhs(nevat_nsi)
  real(rp)    :: wrhsi(nevat_nsi)

  real(rp)    :: baloc(ndime,ndime)                       ! Gather  
  real(rp)    :: bovel(ndime,mnodb)     
  real(rp)    :: bovfi(ndime,mnodb)     
  real(rp)    :: bocod(ndime,mnodb)
  real(rp)    :: bopre(mnodb)
  real(rp)    :: bomut(mnodb)
  real(rp)    :: botem(mnodb)
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: elvel(ndime,mnode)

  real(rp)    :: gbcar(ndime,mnode,mgaus)
  real(rp)    :: gpcar(ndime,mnode,mgaus)
  real(rp)    :: xjaci(9),xjacm(9),shapp(mnode)
  real(rp)    :: gbsur(mgaub),eucta,detjm                 ! Values at Gauss points
  real(rp)    :: gbden(mgaub)
  real(rp)    :: gbvis(mgaub)
  real(rp)    :: gbpor(mgaub)
  real(rp)    :: gbmut(mgaub)
  real(rp)    :: gbgvi(ndime,mgaub)
  real(rp)    :: grvis(ndime,mgaub)
  real(rp)    :: gbtem(mgaub)
  real(rp)    :: gbgve(ndime,ndime,mgaub)
  real(rp)    :: gbpre(mgaub)
  real(rp)    :: gbfle(mgaub)
  real(rp)    :: gpvol(mgaus)
  real(rp)    :: tract(3),chale(3),chave(3),dummr
  real(rp)    :: gpvis,ustar,tragl(9),hleng(3), kinen
  integer(ip) :: ielem,ipoin,igaus,inode,idime             ! Indices and dimensions
  integer(ip) :: pnode,pgaus,iboun,igaub,inodb
  integer(ip) :: pelty,pmate,pblty,pnodb,pgaub
  integer(ip) :: pevat,ievat,jevat,porde,kboun
  integer(ip) :: dummi
  !
  ! If Dynamic pressure cond. exist, compute average dynamic pressure
  !
  if( ittot_nsi >= itebc_nsi ) call nsi_bouave()
  !  if ( kfl_waexl_nsi == 1_ip ) then  ! Wall with exchange location
  !     !
  !     ! Interpolate
  !     !
  !     call PAR_GET_INTERPOLATE_POINTS_VALUES(veloc,velel_nsi,wallcoupling)
  !  end if
  !
  ! Loop over boundaries
  !
  !
  ! OpenMP declarations
  ! 
  !$OMP  PARALLEL DO SCHEDULE ( STATIC )                                      & 
  !$OMP  DEFAULT  ( NONE )                                                    &
  !$OMP  PRIVATE  ( kboun, iboun, pblty, pnodb, ielem, pelty, pnode, pgaub,   &
  !$OMP             pgaus, porde, pevat, pmate, ievat, jevat, elmat, elrhs,   &
  !$OMP             inode, ipoin, elcod, elvel, inodb, bovel, bovfi, bopre,   &
  !$OMP             bomut, botem, tragl, hleng, gbcar, gbden, gbvis,          &
  !$OMP             gbgvi, gbmut, grvis, gbgve, gbpor, wmatr, wrhsi, tract,   &
  !$OMP             bocod, baloc, eucta, kinen, ustar, dummr, shapp, xjacm,   &
  !$OMP             xjaci, detjm, gpcar, gbpre, chave, chale, gpvol, gbfle,   &
  !$OMP             elmap, dummi, gbtem, gbsur, gpvis )                       &
  !$OMP  SHARED  ( kfl_fixbo_nsi, nbouz, lzone, bemol_nsi, kfl_regim_nsi,     &
  !$OMP            ltypb, nnode, lboel, ltype, ngaus, lorde, solve,           &
  !$OMP            lmate, nevat_nsi, veloc, coord, lnodb,                     &
  !$OMP            velom, kfl_coupl, hnatu, kfl_prope, kfl_cotur_nsi,         &
  !$OMP            kfl_kemod_ker, ndime, ndimb, kfl_rough, rough_dom,         &
  !$OMP            rough, untur, kfl_ustar, kfl_waexl_nsi, velel_nsi,         &
  !$OMP            lexlo_nsi, bvnat_nsi, bpess_nsi, gravi_nsi, grnor_nsi,     &                  
  !$OMP            prthe, lowtr_nsi, gasco, cutim, lapla_nsi, lbouz, nmate,   &   
  !$OMP            ivari_nsi, lnods, elmar, kfl_addpr_nsi, kfl_ellsh_nsi,     &     
  !$OMP            kfl_advec_nsi, kfl_matdi_nsi, nsi_schur_complement,        &
  !$OMP            poauu_nsi, poaup_nsi, poapu_nsi, poapp_nsi, kfl_predi_nsi, &
  !$OMP            fcons_nsi,                                                 &
  !$OMP            amatr, pmatr, rhsid )                 
  boundaries: do kboun = 1,nbouz(lzone(ID_NASTIN))
     iboun = lbouz(lzone(ID_NASTIN)) % l(kboun)

     if(  bemol_nsi > 0.0_rp           .or. &    ! Convective term integrated by parts
          & kfl_fixbo_nsi(iboun) ==  2 .or. &    ! Pressure (in bvnat_nsi)
          & kfl_fixbo_nsi(iboun) ==  3 .or. &    ! Wall law
          & kfl_fixbo_nsi(iboun) == 13 .or. &    ! Wall law + open pressure
          & kfl_fixbo_nsi(iboun) ==  5 .or. &    ! Dynamic pressure
          & kfl_fixbo_nsi(iboun) ==  6 .or. &    ! Open flow
          & kfl_fixbo_nsi(iboun) == 10 .or. &    ! Dynamic pressure + open flow 
          & kfl_fixbo_nsi(iboun) == 11 .or. &    ! Dynamic pressure + open flow 
          & kfl_fixbo_nsi(iboun) == 12 .or. &    ! Nodal pressure (in bpess_nsi)
          & kfl_fixbo_nsi(iboun) == 15 .or. &    ! Outflow with pressure dependent on density (Low Mach regime)
          & kfl_fixbo_nsi(iboun) == 17 .or. &    ! Outflow with pressure dependent on density (Low Mach regime)
          & kfl_fixbo_nsi(iboun) == 18 .or. &    ! u.n in weak form
          & kfl_fixbo_nsi(iboun) == 19 .or. &    ! Imposse traction 
          & kfl_regim_nsi        ==  3 ) then               
        !
        ! Element properties and dimensions
        !
        pblty = ltypb(iboun) 
        pnodb = nnode(pblty)
        ielem = lboel((pnodb+1),iboun)
        pelty = ltype(ielem)

        if( pelty > 0 ) then

           pnode = nnode(pelty)
           pgaub = ngaus(pblty) 
           pgaus = ngaus(pelty)
           porde = lorde(pelty)
           pevat = solve(ivari_nsi) % ndofn * pnode
           pmate = 1

           if( nmate > 1 ) then
              pmate = lmate(ielem)
           end if

           if( pmate /= -1 ) then
              !
              ! Initialize
              !
              do ievat = 1,nevat_nsi
                 do jevat = 1,nevat_nsi
                    elmat(jevat,ievat) = 0.0_rp
                 end do
                 elrhs(ievat) = 0.0_rp
              end do
              !
              ! Gather operations: ELVEL, ELCOD, BOVEL
              !
              do inode = 1,pnode
                 ipoin = lnods(inode,ielem)
                 elvel(1:ndime,inode) = veloc(1:ndime,ipoin,1)
                 elcod(1:ndime,inode) = coord(1:ndime,ipoin)    
              end do

              do inodb = 1,pnodb
                 ipoin = lnodb(inodb,iboun)
                 bovel(1:ndime,inodb) = veloc(1:ndime,ipoin,1)
                 if( kfl_coupl(ID_NASTIN,ID_ALEFOR) /= 0 ) then 
                    bovfi(1:ndime,inodb) = velom(1:ndime,ipoin)    
                    ! before I was using bvess_nsi(idime,ipoin,1) 
                    ! but that introduced problems for cases without mesh movement where an inflow and
                    ! a wall law met because bvess_nsi/=0 there. The solution we decided
                    ! with guillaume is to use velom (and only in the cases with ale coupling).
                    ! Velom is already in the global system - no rotation needed.
                 else
                    bovfi(1:ndime,inodb) = 0.0_rp
                 end if
              end do
              call nsi_elmgap(&  
                   pnodb,pmate,lnodb(1,iboun),bocod,bopre,&
                   bovel,bomut,botem)
              !
              ! Element length HLENG
              !
              call elmlen(&
                   ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&
                   hnatu(pelty),hleng)
              !
              ! Properties: GPPOR, GPVIS and GPDEN
              ! OJO: if Smagorinsky used, GBCAR has not been computed
              !
              gbcar = 0.0_rp             
              call ker_proper('DENSI','PGAUB',dummi,iboun,gbden)
              call ker_proper('VISCO','PGAUB',dummi,iboun,gbvis)
              gbgvi =0.0_rp
              call nsi_turbul(&
                   -1_ip,0_ip,pnodb,pgaub,1_ip,pgaub,kfl_cotur_nsi,&
                   elmar(pblty)%shape,gbcar,hleng,bovel,bomut,gbden,gbvis,gbmut,&
                   gbgvi,grvis,gbgve,ielem,kfl_kemod_ker)
              
              gauss_points: do igaub = 1,pgaub

                 do ievat = 1,nevat_nsi
                    do jevat = 1,nevat_nsi
                       wmatr(jevat,ievat) = 0.0_rp
                    end do
                 end do
                 wrhsi(1:nevat_nsi) = 0.0_rp
                 tract(1:3)         = 0.0_rp

                 call bouder(&
                      pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&    ! Cartesian derivative
                      bocod,baloc,eucta)                                   ! and Jacobian
                 gbsur(igaub) = elmar(pblty)%weigp(igaub)*eucta 
                 call chenor(pnode,baloc,bocod,elcod)                      ! Check normal

#ifdef matiaslma
                 !boundary term coming from by parts integration in mass equation 
                 call nsi_boumas(pevat,pnodb,solve(ivari_nsi)%ndofn, &
                      lboel(1,iboun),elmar(pblty)%shape(1,igaub), &
                      gbden(igaub),baloc,wmatr)          

#endif


                 if( kfl_fixbo_nsi(iboun) == 2 .or. &
                      kfl_fixbo_nsi(iboun) == 6 ) then
                    !
                    ! Pressure: sig.n = - p n
                    !
                    tract(1:ndime) = -bvnat_nsi(1,iboun,1) * baloc(1:ndime,ndime)

                 else if( kfl_fixbo_nsi(iboun) == 3 .or. kfl_fixbo_nsi(iboun) == 13 .or. kfl_fixbo_nsi(iboun) == 18 ) then
                    !
                    ! Wall law: sig.n = - rho* (U*^2) * (u_tan-u_fix_tan)/|u_tan-u_fix_tan|
                    !
                    if( kfl_rough > 0 ) then
                       rough_dom = 0.0_rp
                       do inodb = 1,pnodb
                          ipoin = lnodb(inodb,iboun)
                          rough_dom = rough_dom + rough(ipoin) * elmar(pblty)%shape(inodb,igaub)
                       end do
                    end if
                    kinen = 0.0_rp
                    if( kfl_ustar == 2 ) then 
                       do inodb = 1,pnodb
                          ipoin = lnodb(inodb,iboun)
                          kinen = kinen + untur(1,ipoin,1) * elmar(pblty)%shape(inodb,igaub)
                       end do
                    end if
                    if ( kfl_waexl_nsi == 1_ip ) then !if exchange location for wall law
                       call nsi_bouwal(&                        
                            1_ip,pevat,pnodb,solve(ivari_nsi)%ndofn,iboun,&
                            lboel(1,iboun),elmar(pblty)%shape(1,igaub),bovel,bovfi,tract,&
                            gbvis(igaub),gbden(igaub),baloc,ustar,wmatr,rough_dom,kinen,velel_nsi(:,lexlo_nsi(igaub,iboun)) )
                    else
                       call nsi_bouwal(&                        
                            1_ip,pevat,pnodb,solve(ivari_nsi)%ndofn,iboun,&
                            lboel(1,iboun),elmar(pblty)%shape(1,igaub),bovel,bovfi,tract,&
                            gbvis(igaub),gbden(igaub),baloc,ustar,wmatr,rough_dom,kinen,dummr)
                    end if

                    if( kfl_fixbo_nsi(iboun) == 13 ) then
                       !
                       ! Weak imposition of u.n: assemble ( p n , v )_S on LHS
                       !
                       tract = 0.0_rp
                       wmatr = 0.0_rp
                       wrhsi = 0.0_rp

                       do igaus = 1,pgaus
                          call elmder(&
                               pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&      ! Cartesian derivative
                               elcod,gpcar(1,1,igaus),detjm,xjacm,xjaci)        ! and Jacobian
                       end do
                       call cartbo(&
                            2_ip,lboel(1,iboun),elmar(pblty)%shape(1,igaub),&
                            elmar(pelty)%shaga,gpcar,elmar(pelty)%shape,&
                            shapp,gbcar,pnodb,pnode,pgaus)                    
                       call nsi_zobi(&
                            pevat,pnodb,pnode,lboel(1,iboun),elmar(pblty)%shape(1,igaub),&
                            shapp,gbcar,gbvis(igaub),baloc(1,ndime),wmatr)
                    end if

                 else if( kfl_fixbo_nsi(iboun) ==  5 .or.&
                      &   kfl_fixbo_nsi(iboun) == 10 ) then
                    !
                    ! Dynamic pressure: sig.n = - (-1/2*rho*u^2) n
                    !
                    tract(1:ndime) = -bvnat_nsi(1,iboun,1) * baloc(1:ndime,ndime)    

                 end if

                 if(     kfl_fixbo_nsi(iboun) ==  6 .or.&
                      &  kfl_fixbo_nsi(iboun) == 10 ) then
                    !
                    ! Open boundary: assemble -2*mu*Sym(grad(u).n and possibly pI.n (check nsi_bouopb)
                    !
                    do igaus = 1,pgaus
                       call elmder(&
                            pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&      ! Cartesian derivative
                            elcod,gpcar(1,1,igaus),detjm,xjacm,xjaci)        ! and Jacobian
                    end do
                    call cartbo(&
                         2_ip,lboel(1,iboun),elmar(pblty) % shape(1,igaub),&
                         elmar(pelty) % shaga,gpcar,elmar(pelty) % shape,  &
                         shapp,gbcar,pnodb,pnode,pgaus)
                    gpvis = gbvis(igaub)
                    if( kfl_cotur_nsi == 1 ) then
                       gpvis = gpvis + gbmut(igaub)
                    end if
                    call nsi_bouopb(&
                         lboel(1,iboun),elmar(pblty) % shape(1,igaub),gbcar,&
                         baloc(1,ndime),wmatr,pnode,pnodb,solve(ivari_nsi) % ndofn,&
                         pevat,gpvis,shapp)

                 else if( kfl_fixbo_nsi(iboun) == 11 .or. ( kfl_fixbo_nsi(iboun) == 3 .and. kfl_addpr_nsi == 1) ) then
                    !
                    ! Open boundary: assemble pI.n or R*T*rho*I.n - or wall law and correction 'do nothing'
                    !
                    do igaus=1,pgaus
                       call elmder(&
                            pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&      ! Cartesian derivative
                            elcod,gpcar(1,1,igaus),detjm,xjacm,xjaci)        ! and Jacobian
                    end do
                    call cartbo(&
                         2_ip,lboel(1,iboun),elmar(pblty)%shape(1,igaub),&
                         elmar(pelty)%shaga,gpcar,elmar(pelty)%shape,&
                         shapp,gbcar,pnodb,pnode,pgaus)
                    call nsi_bouopp(&
                         lboel(1,iboun),lnodb(1,iboun),elmar(pblty)%shape(1,igaub),&
                         baloc(1,ndime),wmatr,pnode,pnodb,solve(ivari_nsi)%ndofn,&
                         pevat,shapp)

                 else if( kfl_fixbo_nsi(iboun) == 12 ) then
                    !
                    ! Pressure: sig.n = - p n, with p is nodal
                    !
                    gbpre(igaub) = 0.0_rp
                    do inodb = 1,pnodb 
                       ipoin = lnodb(inodb,iboun)
                       gbpre(igaub) = gbpre(igaub) + bpess_nsi(1,ipoin) * elmar(pblty)%shape(inodb,igaub)
                    end do
                    tract(1:ndime) = - gbpre(igaub) * baloc(1:ndime,ndime)

                 else if( kfl_fixbo_nsi(iboun) == 15 ) then
                    !
                    ! Pressure depends on density
                    ! sig.n = - rho g.r n
                    !
                    dummr=0.0_rp
                    do idime=1,ndime
                       dummr = dummr + bocod(idime,igaub)* gravi_nsi(idime)
                    enddo
                    tract(1:ndime) =  - (gbden(igaub)-prthe(1)/(lowtr_nsi * gasco) )  &
                         * abs(dummr) * grnor_nsi *  baloc(1:ndime,ndime) ! p = (rho- rho_0) g.r !-(lowpr_nsi/(lowtr_nsi * gasco))

                 else if( kfl_fixbo_nsi(iboun) == 17 ) then
                    call runend('NASTIN: Boundary code 17 not coded')
                    !
                    ! Open boundary: assemble 2*mu*eps(grad(u).n-div.u/3)-p I.n
                    !
                    do igaus=1,pgaus
                       call elmder(&
                            pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&      ! Cartesian derivative
                            elcod,gpcar(1,1,igaus),detjm,xjacm,xjaci)        ! and Jacobian
                    end do
                    call cartbo(&
                         2_ip,lboel(1,iboun),elmar(pblty)%shape(1,igaub),&
                         elmar(pelty)%shaga,gpcar,elmar(pelty)%shape,&
                         shapp,gbcar,pnodb,pnode,pgaus)
                    gpvis = gbvis(igaub)
                    if( kfl_cotur_nsi == 1 ) then
                       gpvis = gpvis + gbmut(igaub)
                    end if
                    call nsi_bouopb(&
                         lboel(1,iboun),elmar(pblty)%shape(1,igaub),gbcar,&
                         baloc(1,ndime),wmatr,pnode,pnodb,solve(ivari_nsi)%ndofn,&
                         pevat,gpvis,shapp)
                    call nsi_bouopp(&
                         lboel(1,iboun),elmar(pblty)%shape(1,igaub),gbcar,&
                         baloc(1,ndime),wmatr,pnode,pnodb,solve(ivari_nsi)%ndofn,&
                         pevat,shapp)

                 else if( kfl_fixbo_nsi(iboun) == 18 ) then

                    call nsi_boupen(&                        
                         pevat,pnodb,solve(ivari_nsi)%ndofn,lboel(1,iboun),&
                         elmar(pblty)%shape(1,igaub),bovel,bovfi,&
                         gbvis(igaub),gbden(igaub),hleng,baloc,wmatr)

                 else if( bemol_nsi > 0.0_rp .and. kfl_fixbo_nsi(iboun) /= 13 ) then
                    !
                    ! Integrate  ( bemol * rho * (u.n) u . v )_S on LHS
                    !
                    call nsi_boubem(&
                         pevat,pnodb,lboel(1,iboun),elmar(pblty)%shape(1,igaub),bovel,&
                         baloc(1,ndime),gbden(igaub),wmatr)

                 else if( fcons_nsi > 0.2_rp .and. kfl_fixbo_nsi(iboun) /= 13 ) then
                    !
                    ! Boundary term  ( fcons * rho * (u.n) u . v )_S on LHS for the
                    ! conservative and skew-symmetric forms of the convective term
                    !
                    call nsi_bouske(&
                         pevat,pnodb,lboel(1,iboun),elmar(pblty)%shape(1,igaub),bovel,&
                         baloc(1,ndime),gbden(igaub),wmatr)

                 else if( kfl_fixbo_nsi(iboun) == 19 ) then  
                    !
                    ! Impose traction to the boundary
                    !
                    tract(1)   = bvnat_nsi(1,iboun,1)
                    tract(2:3) = 0.0_rp

                 end if
                 !
                 ! Exact solution: GPRHS
                 !
                 call nsi_elmexa(                                                    &
                      -1_ip,pnodb,elmar(pblty)%shape(1,igaub),bocod,gbden(igaub),    &
                      gbvis(igaub),gbpor(igaub),gbgvi(1,igaub),cutim,baloc(1,ndime), &
                      tract,dummr,dummr)

                 call nsi_bouass(&
                      pevat,solve(ivari_nsi)%ndofn,pnodb,lboel(1,iboun),&
                      elmar(pblty)%shape(1,igaub),&
                      gbsur(igaub),tract,wmatr,wrhsi,elmat,elrhs)

              end do gauss_points
              !
              ! Schur complement preconditioner: - ( tau*grad(p).n, q )_S
              !
              if( .false. .and. ( kfl_predi_nsi == 2 .or. kfl_predi_nsi == 3 ) ) then
                 call elmchl(&
                      tragl,hleng,elcod,elvel,chave,chale,pnode,&
                      porde,hnatu(pelty),kfl_advec_nsi,kfl_ellsh_nsi)
                 if( kfl_cotur_nsi == 1 ) then
                    do igaub = 1,pgaub
                       gbvis(igaub) = gbvis(igaub) + gbmut(igaub)
                    end do
                 end if
                 call elmcar(&
                      pnode,pgaus,0_ip,elmar(pelty)%weigp,elmar(pelty)%shape,&
                      elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,gpvol,gpcar,&
                      dummr,ielem)
                 call nsi_bousch(&
                      pnode,pnodb,pgaub,pgaus,lnods(1,ielem),lboel(1,iboun),&
                      gpcar,gbcar,elmar(pelty)%shaga,baloc(1,ndime),gbsur,&
                      gbden,gbvis,gbpor,elmar(pblty)%shape,bovel,chale,elmap)
                 call nsi_assmat(&
                      -1_ip,pnode,pnode,lnods(1,ielem),elmap,dummr,dummr,&
                      dummr,lapla_nsi)
              end if
              !
              ! Prescribe Dirichlet boundary conditions
              !
              if( kfl_matdi_nsi == 0 ) & 
                   call nsi_elmdir(&
                   1_ip,1_ip,pnode,pevat,solve(ivari_nsi)%ndofn,lnods(1,ielem),&
                   elmat,elrhs)
              !
              ! Assembly 
              !
              !$OMP CRITICAL (nsi_bouope_lock1)
              call nsi_assrhs(&
                   1_ip,solve(ivari_nsi)%ndofn,pnode,pevat,ndime,&
                   solve(ivari_nsi)%kfl_algso,lnods(1,ielem),elvel,elrhs,&
                   elmat,rhsid)

              if( NSI_SCHUR_COMPLEMENT ) then
                 call nsi_assmat(&
                      1_ip,pnode,pevat,lnods(1,ielem),elmat,amatr(poauu_nsi),&
                      amatr(poaup_nsi),amatr(poapu_nsi),amatr(poapp_nsi))
              else
                 call assmat(&
                      solve(ivari_nsi)%ndofn,pnode,pevat,solve(ivari_nsi)%nunkn,&
                      solve(ivari_nsi)%kfl_algso,ielem,lnods(1,ielem),elmat,amatr)
              end if
              !
              ! Preconditioner: PMATR
              !
              if(solve(ivari_nsi)%kfl_preco==4) then
                 call nsi_assdia(&
                      solve(ivari_nsi)%ndofn,pnode,pevat,&
                      lnods(1,ielem),elmat,pmatr)             
              end if
              !$OMP END CRITICAL (nsi_bouope_lock1)

           end if

        end if

     end if

  end do boundaries

end subroutine nsi_bouope
   
subroutine nsi_boubem(&
     pevat,pnodb,lboel,gbsha,bovel,baloc,gbden,wmatr)

  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_boubem
  ! NAME 
  !    nsi_boubem
  ! DESCRIPTION
  !    Integrate int_\Gamma bemol * rho * (u.n) u . v ds 
  !    on the left-hand side
  ! USES
  ! USED BY
  !    nsi_matrix
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  ndime
  use def_nastin, only     :  bemol_nsi
  implicit none
  integer(ip), intent(in)  :: pevat,pnodb
  integer(ip), intent(in)  :: lboel(pnodb)
  real(rp),    intent(in)  :: gbsha(pnodb)
  real(rp),    intent(in)  :: bovel(ndime,pnodb)
  real(rp),    intent(in)  :: baloc(ndime)
  real(rp),    intent(in)  :: gbden
  real(rp),    intent(out) :: wmatr(pevat,pevat)
  integer(ip)              :: inodb,jnodb,ievat,jevat,idime,ndofn
  real(rp)                 :: gbvel(3),udotn

  ndofn    = ndime + 1
  gbvel(1) = 0.0_rp
  gbvel(2) = 0.0_rp
  gbvel(3) = 0.0_rp
  do inodb = 1,pnodb
    do idime = 1,ndime
       gbvel(idime) = gbvel(idime) + bovel(idime,inodb) * gbsha(inodb)
    end do    
  end do

  udotn = 0.0_rp
  do idime = 1,ndime
     udotn = udotn + baloc(idime) * gbvel(idime)
  end do
  udotn = bemol_nsi * gbden * udotn

  do inodb = 1,pnodb
     do jnodb = 1,pnodb
        do idime = 1,ndime
           ievat = (lboel(inodb)-1) * ndofn + idime
           jevat = (lboel(jnodb)-1) * ndofn + idime
           wmatr(jevat,ievat) = wmatr(jevat,ievat) + udotn * gbsha(inodb) * gbsha(jnodb) 
        end do
     end do
  end do

end subroutine nsi_boubem


subroutine nsi_bouske(&
     pevat,pnodb,lboel,gbsha,bovel,baloc,gbden,wmatr)

  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_bouske
  ! NAME 
  !    nsi_bouske
  ! DESCRIPTION
  !    Integrate int_\Gamma fcons * rho * (u.n) u . v ds 
  !    on the left-hand side for the conservative and skew-symmetric
  !    forms of the convective term 
  ! USES
  ! USED BY
  !    nsi_matrix
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  ndime
  use def_nastin, only     :  fcons_nsi
  implicit none
  integer(ip), intent(in)  :: pevat,pnodb
  integer(ip), intent(in)  :: lboel(pnodb)
  real(rp),    intent(in)  :: gbsha(pnodb)
  real(rp),    intent(in)  :: bovel(ndime,pnodb)
  real(rp),    intent(in)  :: baloc(ndime)
  real(rp),    intent(in)  :: gbden
  real(rp),    intent(out) :: wmatr(pevat,pevat)
  integer(ip)              :: inodb,jnodb,ievat,jevat,idime,ndofn
  real(rp)                 :: gbvel(3),udotn

  ndofn    = ndime + 1
  gbvel(1) = 0.0_rp
  gbvel(2) = 0.0_rp
  gbvel(3) = 0.0_rp
  do inodb = 1,pnodb
    do idime = 1,ndime
       gbvel(idime) = gbvel(idime) + bovel(idime,inodb) * gbsha(inodb)
    end do    
  end do

  udotn = 0.0_rp
  do idime = 1,ndime
     udotn = udotn + baloc(idime) * gbvel(idime)
  end do
  udotn = fcons_nsi * gbden * udotn

  do inodb = 1,pnodb
     do jnodb = 1,pnodb
        do idime = 1,ndime
           ievat = (lboel(inodb)-1) * ndofn + idime
           jevat = (lboel(jnodb)-1) * ndofn + idime
           wmatr(jevat,ievat) = wmatr(jevat,ievat) + udotn * gbsha(inodb) * gbsha(jnodb) 
        end do
     end do
  end do

end subroutine nsi_bouske

subroutine nsi_zobi(&
     pevat,pnodb,pnode,lboel,gbsha,shapp,gbcar,gbvis,baloc,wmatr)

  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_boubem
  ! NAME 
  !    nsi_boubem
  ! DESCRIPTION
  !    Integrate int_\Gamma ( p n - 2*mu*div(u) n ).v ds on LHS
  ! USES
  ! USED BY
  !    nsi_matrix
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime
  use def_nastin, only       :  fvins_nsi
  implicit none
  integer(ip), intent(in)    :: pevat,pnodb,pnode
  integer(ip), intent(in)    :: lboel(pnodb)
  real(rp),    intent(in)    :: gbsha(pnodb)
  real(rp),    intent(in)    :: shapp(pnode)
  real(rp),    intent(in)    :: gbcar(ndime,pnode)
  real(rp),    intent(in)    :: gbvis
  real(rp),    intent(in)    :: baloc(ndime)
  real(rp),    intent(inout) :: wmatr(pevat,pevat)
  integer(ip)                :: inodb,jnodb,ievat,jevat,idime,ndofn
  integer(ip)                :: jdime,jnode
  real(rp)                   :: xfact

  ndofn = ndime + 1

  if( 1 == 2 ) then

     do inodb = 1,pnodb
        do jnode = 1,pnode
           do idime = 1,ndime
              ievat = ( lboel(inodb) - 1 ) * ndofn + idime
              jevat =   jnode * ndofn
              wmatr(ievat,jevat) = wmatr(ievat,jevat) + gbsha(inodb) * shapp(jnode) * baloc(ndime)
           end do
        end do
     end do

  else
     !
     ! (pn,v)
     !
     do inodb = 1,pnodb
        ievat = ( lboel(inodb) - 1 ) * ndofn
        do idime = 1,ndime
           ievat =  ievat + 1
           do jnodb = 1,pnodb
              jevat = lboel(jnodb) * ndofn
              wmatr(ievat,jevat) = wmatr(ievat,jevat) + gbsha(inodb) * gbsha(jnodb) * baloc(ndime)
           end do
        end do
     end do

     return

     !
     ! Laplacian form:    -mu*( div(u) , v )
     ! Divergence form: -2 mu*( div(u) , v )
     !
     if( fvins_nsi > 0.9_rp ) then
        xfact = 2.0_rp * gbvis
     else
        xfact = 1.0_rp * gbvis
     end if
     do inodb = 1,pnodb
        do idime = 1,ndime
           ievat = ( lboel(inodb) - 1 ) * ndofn + idime
           do jnode = 1,pnode
              do jdime = 1,ndime
                 jevat = ( jnode - 1 ) * ndofn + jdime
                 wmatr(ievat,jevat) = wmatr(ievat,jevat) - xfact * gbcar(jdime,jnode) * baloc(ndime) * gbsha(inodb) 
              end do
           end do
        end do
     end do
  end if
! do inodb = 1,pnodb
!        do idime = 1,ndime
!           ievat = ( lboel(inodb) - 1 ) * ndofn + idime
!           do jnode = 1,pnode
!              do jdime = 1,ndime
!                 jevat = ( jnode - 1 ) * ndofn + jdime
!                 wmatr(ievat,jevat) = wmatr(ievat,jevat) - gbvis * gbcar(idime,jnode) * baloc(jdime,ndime) * gbsha(inodb) 
!              end do
!           end do
!        end do
!     end do

end subroutine nsi_zobi


subroutine nsi_pipi(npopo,nbvar,kfl_symme,ia,ja,an,wa1)
  !-----------------------------------------------------------------------
  !
  ! Compute the diagonal
  !
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  INOTMASTER,NPOIN_TYPE
  implicit none
  integer(ip), intent(in)    :: npopo,nbvar,kfl_symme
  integer(ip), intent(in)    :: ia(*),ja(*)
  real(rp),    intent(in)    :: an(nbvar,nbvar,*)
  real(rp),    intent(inout) :: wa1(*)
  integer(ip)                :: ii,jj,kk,ll

  if( INOTMASTER ) then
     ii = 24753
     jj = ia(ii)
     ll = -1
     do while (jj< ia(ii+1) .and. ll ==-1)
        if(ja(jj)==ii) then
           ll = jj
        end if
        jj = jj+1
     end do
     if(ll/=-1) then
        jj = (ii-1) * nbvar
        do kk= 1, nbvar
           print*,'a=',ii,kk,an(kk,kk,ll)
        end do
     end if
     ii = 25975
     jj = ia(ii)
     ll = -1
     do while (jj< ia(ii+1) .and. ll ==-1)
        if(ja(jj)==ii) then
           ll = jj
        end if
        jj = jj+1
     end do
     if(ll/=-1) then
        jj = (ii-1) * nbvar
        do kk= 1, nbvar
           print*,'a=',ii,kk,an(kk,kk,ll)
        end do
     end if

  end if

end subroutine nsi_pipi

