!------------------------------------------------------------------------
!> @addtogroup Nastin 
!> @{
!> @file    nsi_turnon.f90
!> @author  Guillaume Houzeaux
!> @brief   Turn on Nastin module
!> @details Read data and allocate memory
!> @} 
!------------------------------------------------------------------------
subroutine nsi_turnon()

  use def_kintyp
  use def_master
  use def_domain
  use def_nastin
  implicit none
  !
  ! Initial variables
  !
  call nsi_inivar(0_ip)
  !
  ! Read the physical problem
  !
  call nsi_reaphy()
  !
  ! Read the numerical treatment
  !
  call nsi_reanut()
  !
  ! Read the output strategy
  !
  call nsi_reaous()
  !
  ! Read the boundary conditions
  !
  call nsi_reabcs()
  !
  ! Service: Parall
  !
  call nsi_parall(1_ip)
  !
  ! Modify boundary conditions
  !
  call nsi_inibcs()
  call nsi_updbcs(0_ip)
  !
  ! Initial variables
  !
  call nsi_inivar(1_ip)
  !
  ! Allocate memory
  !
  call nsi_memall()
  !
  ! Warnings and errors
  !
  call nsi_outerr(1_ip)
  !
  ! Open additional files
  !
  call nsi_openfi(2_ip)

end subroutine nsi_turnon

