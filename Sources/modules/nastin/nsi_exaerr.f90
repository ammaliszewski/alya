!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_exaerr.f90
!> @author  Guillaume Houzeaux
!> @brief   Manufactured solution handling
!> @details This routine computes the FEM errors (referred to an analytical
!>          solution defined in exacso.f90. The errors are normalized by the 
!>          appropriate norm of the exact solution, except when this norm 
!>          is zero. 
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_exaerr(itask)
  use def_parame
  use def_domain 
  use def_master
  use def_kermod
  use def_nastin
  use def_elmtyp
  use mod_ker_proper
  implicit none
  integer(ip), intent(in) :: itask
  real(rp)                :: gpcar(ndime,mnode) 
  real(rp)                :: xjaci(ndime,ndime) 
  real(rp)                :: xjacm(ndime,ndime) 
  real(rp)                :: elvel(ndime,mnode)                          ! Gather 
  real(rp)                :: elpre(mnode)
  real(rp)                :: elcod(ndime,mnode)
  real(rp)                :: gpvol(mgaus),detjm                          ! Values at Gauss points
  real(rp)                :: gpvis(mgaus),gpden(mgaus),gppor(mgaus)
  real(rp)                :: gpgvi(ndime,mgaus)
  real(rp)                :: gpvel(ndime),grave(ndime,ndime)
  real(rp)                :: prgau,gradp(ndime),gpcod(ndime)
  real(rp)                :: exvel(ndime),exveg(ndime,ndime)
  real(rp)                :: expre,exprg(ndime),sgsno,velno
  real(rp)                :: difeu,abvel,diffp,abpre,dummr(3)
  real(rp)                :: erp01(2),erp02(2),erp0i(2)                  ! Pressure errors
  real(rp)                :: erp11(2),erp12(2),erp1i(2)                  ! grad(Pressure) errors
  real(rp)                :: eru01(2),eru02(2),eru0i(2)                  ! Velocity errors
  real(rp)                :: eru11(2),eru12(2),eru1i(2)                  ! grad(Velocity) errors
  integer(ip)             :: ielem,inode,ipoin,igaus,idime,jdime         ! Indices and dimensions
  integer(ip)             :: pnode,pgaus,pelty,dummi,ibopo
  integer(ip)             :: inodb,iboun
  real(rp)                :: gpdum(max(mnode,mgaus))

  if( kfl_exacs_nsi /= 0 ) then

     if( itask == 1 .and. INOTMASTER ) then

        !----------------------------------------------------------------------
        !
        ! Force dirichlet boundary condition and impose exact value on velocity
        !
        !----------------------------------------------------------------------

        if( kfl_exfix_nsi == 1 ) then 
           do ipoin = 1,npoin
              ibopo = lpoty(ipoin)
              if( ibopo > 0 ) then
                 call nsi_exacso(&
                      1_ip,coord(1,ipoin),gpdum,gpdum,&
                      gpdum,gpdum,exvel,exveg,expre,exprg,&
                      dummr,dummr,dummr,dummr)
                 do idime = 1,ndime
                    kfl_fixno_nsi(idime,ipoin) = 1
                    bvess_nsi(idime,ipoin,1) = exvel(idime)
                 end do
              end if
           end do
           call memgen(1_ip,npoin,0_ip)
           do iboun = 1,nboun
              if( kfl_fixbo_nsi(iboun) == 2 ) then
                 do inodb = 1,nnode(abs(ltypb(iboun)))
                    ipoin = lnodb(inodb,iboun)
                    gisca(ipoin) = 1
                 end do
              end if
           end do
           call parari('SLX',NPOIN_TYPE,npoin,gisca)
           do iboun = 1,nboun
              if( kfl_fixbo_nsi(iboun) /= 2 ) then
                 do inodb = 1,nnode(abs(ltypb(iboun)))
                    ipoin = lnodb(inodb,iboun)
                    gisca(ipoin) = 0
                 end do
              end if
           end do
           call parari('MIN',NPOIN_TYPE,npoin,gisca)
           do ipoin = 1,npoin
              if( gisca(ipoin) /= 0 ) then
                 do idime = 1,ndime
                    kfl_fixno_nsi(idime,ipoin) = 0
                 end do
              end if
           end do
           call memgen(3_ip,npoin,0_ip)

        else if( kfl_exfix_nsi == 2 ) then 
           do ipoin = 1,npoin
              ibopo = lpoty(ipoin)
              if( ibopo > 0 ) then
                 call nsi_exacso(&
                      1_ip,coord(1,ipoin),gpdum,gpdum,&
                      gpdum,gpdum,exvel,exveg,expre,exprg,&
                      dummr,dummr,dummr,dummr)
                 do idime = 1,ndime
                    if( kfl_fixno_nsi(idime,ipoin) == 1 ) &
                         bvess_nsi(idime,ipoin,1) = exvel(idime)
                 end do
              end if
           end do

        end if

     else if( itask == 5 .and. INOTMASTER ) then

        !----------------------------------------------------------------------
        !
        ! Impose exact initial condition
        !
        !----------------------------------------------------------------------

        do ipoin = 1,npoin
           call nsi_exacso(&
                1_ip,coord(1,ipoin),gpdum,gpdum,&
                gpdum,gpdum,exvel,exveg,expre,exprg,&
                dummr,dummr,dummr,dummr)
           press(ipoin,nprev_nsi) = expre
           do idime = 1,ndime
              veloc(idime,ipoin,nprev_nsi) = exvel(idime)
           end do
        end do

     else if( itask == 3 .or. itask == 4 .and. INOTMASTER) then

        !----------------------------------------------------------------------
        !
        ! Compute velocity error for postprocess
        !
        !----------------------------------------------------------------------

        do ipoin = 1,npoin        
           call nsi_exacso(&
                1_ip,coord(1,ipoin),gpdum,gpdum,&
                gpdum,gpdum,exvel,exveg,expre,exprg,&
                dummr,dummr,dummr,dummr)
           if( itask == 4 ) gesca(ipoin) = abs(expre - press(ipoin,1))
           if( itask == 3 ) then
              gesca(ipoin) = 0.0_rp
              do idime = 1,ndime
                 gesca(ipoin) = gesca(ipoin) + ( exvel(idime) - veloc(idime,ipoin,1) )**2
              end do
           end if
        end do

     else

        !----------------------------------------------------------------------
        !
        ! Compute error norms of velocity and pressure
        !
        !----------------------------------------------------------------------

        erp01 = 0.0_rp
        erp02 = 0.0_rp
        erp0i = 0.0_rp
        erp11 = 0.0_rp
        erp12 = 0.0_rp
        erp1i = 0.0_rp
        eru01 = 0.0_rp
        eru02 = 0.0_rp
        eru0i = 0.0_rp
        eru11 = 0.0_rp
        eru12 = 0.0_rp
        eru1i = 0.0_rp
        gpgvi = 0.0_rp
        gpdum = 0.0_rp
        !
        ! Loop over elements to calculate global error
        !
        if( INOTMASTER) then
           elements: do ielem = 1,nelem
              !
              ! Element properties and dimensions
              !
              pelty = ltype(ielem)
              if( pelty > 0 .and. lelch(ielem) /= ELEXT ) then
                 pnode = nnode(pelty)
                 pgaus = ngaus(pelty)
                 !
                 ! Gather operations
                 !
                 do inode = 1,pnode
                    ipoin = lnods(inode,ielem)
                    elvel(1:ndime,inode) = veloc(1:ndime,ipoin,1)
                    elcod(1:ndime,inode) = coord(1:ndime,ipoin)
                    elpre(inode)         = press(ipoin,1)
                 end do
                 !
                 ! Properties
                 !
                 call ker_proper('DENSI','PGAUS',dummi,ielem,gpden,pnode,pgaus,elmar(pelty)%shape)
                 call ker_proper('VISCO','PGAUS',dummi,ielem,gpvis,pnode,pgaus,elmar(pelty)%shape)
                 call ker_proper('POROS','PGAUS',dummi,ielem,gppor,pnode,pgaus,elmar(pelty)%shape)

                 do igaus=1,pgaus 
                    call elmder(pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&   ! Cartesian derivative
                         elcod,gpcar,detjm,xjacm,xjaci)                       ! and Jacobian
                    gpvol(igaus)=elmar(pelty)%weigp(igaus)*detjm
                    !
                    ! Velocity, pressure, velocity gradient and pressure gradient
                    !
                    prgau = 0.0_rp
                    gpvel = 0.0_rp
                    gradp = 0.0_rp
                    grave = 0.0_rp
                    gpcod = 0.0_rp
                    do inode = 1,pnode
                       do idime = 1,ndime
                          gpvel(idime) = gpvel(idime) + elmar(pelty)%shape(inode,igaus)*elvel(idime,inode)
                          gpcod(idime) = gpcod(idime) + elmar(pelty)%shape(inode,igaus)*elcod(idime,inode)
                          gradp(idime) = gradp(idime) + gpcar(idime,inode)*elpre(inode)
                          do jdime = 1,ndime
                             grave(jdime,idime) = grave(jdime,idime) + gpcar(jdime,inode)*elvel(idime,inode)
                          end do
                       end do
                       prgau = prgau + elmar(pelty)%shape(inode,igaus)*elpre(inode)
                    end do
                    !
                    ! Exact solution
                    !
                    call nsi_exacso(&
                         1_ip,gpcod,gpden(igaus),gpvis(igaus),gppor(igaus),&
                         gpgvi(1,igaus),exvel,exveg,expre,exprg,&
                         dummr,dummr,dummr,dummr)
                    !
                    ! Errors
                    ! 
                    diffp    = abs(prgau-expre)
                    abpre    = abs(expre)
                    erp01(1) = erp01(1) + diffp*gpvol(igaus)
                    erp02(1) = erp02(1) + diffp*diffp*gpvol(igaus)
                    erp0i(1) = max(erp0i(1),diffp)
                    erp01(2) = erp01(2) + expre*gpvol(igaus)
                    erp02(2) = erp02(2) + expre*expre*gpvol(igaus)
                    erp0i(2) = max(erp0i(2),expre)
                    do idime=1,ndime
                       difeu    = abs(gpvel(idime)-exvel(idime))
                       abvel    = abs(exvel(idime))
                       eru01(1) = eru01(1) + difeu*gpvol(igaus)
                       eru02(1) = eru02(1) + difeu*difeu*gpvol(igaus)
                       eru0i(1) = max(eru0i(1),difeu)
                       eru01(2) = eru01(2) + abvel*gpvol(igaus)
                       eru02(2) = eru02(2) + abvel*abvel*gpvol(igaus)
                       eru0i(2) = max(eru0i(2),abvel)
                       diffp    = abs(gradp(idime)-exprg(idime))
                       abpre    = abs(exprg(idime)) 
                       erp11(1) = erp11(1) + diffp*gpvol(igaus)
                       erp12(1) = erp12(1) + diffp*diffp*gpvol(igaus)
                       erp1i(1) = max(erp1i(1),diffp)
                       erp11(2) = erp11(2) + abpre*gpvol(igaus)
                       erp12(2) = erp12(2) + abpre*abpre*gpvol(igaus)
                       erp1i(2) = max(erp1i(2),abpre)
                       do jdime=1,ndime
                          difeu    = abs(grave(idime,jdime)-exveg(idime,jdime))
                          abvel    = abs(exveg(idime,jdime))
                          eru11(1) = eru11(1) + difeu*gpvol(igaus)
                          eru12(1) = eru12(1) + difeu*difeu*gpvol(igaus)
                          eru1i(1) = max(eru1i(1),difeu)
                          eru11(2) = eru11(2) + abvel*gpvol(igaus)
                          eru12(2) = eru12(2) + abvel*abvel*gpvol(igaus)
                          eru1i(2) = max(eru1i(2),abvel)
                       end do
                    end do

                 end do
              end if
           end do elements

        end if
        call pararr('MAX',0_ip,2_ip,eru0i)
        call pararr('MAX',0_ip,2_ip,erp0i)
        call pararr('MAX',0_ip,2_ip,eru1i)
        call pararr('MAX',0_ip,2_ip,erp1i)

        call pararr('SUM',0_ip,2_ip,erp02)
        call pararr('SUM',0_ip,2_ip,erp12)
        call pararr('SUM',0_ip,2_ip,eru02)
        call pararr('SUM',0_ip,2_ip,eru12)

        call pararr('SUM',0_ip,2_ip,erp01)
        call pararr('SUM',0_ip,2_ip,erp11)
        call pararr('SUM',0_ip,2_ip,eru01)
        call pararr('SUM',0_ip,2_ip,eru11)

        erp02(1) = sqrt(erp02(1))
        erp12(1) = sqrt(erp12(1))
        eru02(1) = sqrt(eru02(1))
        eru12(1) = sqrt(eru12(1))

        if(erp01(2)>0.0_rp) erp01(1) = erp01(1)/erp01(2) 
        if(erp02(2)>0.0_rp) erp02(1) = erp02(1)/sqrt(erp02(2))
        if(erp0i(2)>0.0_rp) erp0i(1) = erp0i(1)/erp0i(2)
        if(eru01(2)>0.0_rp) eru01(1) = eru01(1)/eru01(2) 
        if(eru02(2)>0.0_rp) eru02(1) = eru02(1)/sqrt(eru02(2))
        if(eru0i(2)>0.0_rp) eru0i(1) = eru0i(1)/eru0i(2) 
        if(erp11(2)>0.0_rp) erp11(1) = erp11(1)/erp11(2) 
        if(erp12(2)>0.0_rp) erp12(1) = erp12(1)/sqrt(erp12(2))
        if(erp1i(2)>0.0_rp) erp1i(1) = erp1i(1)/erp1i(2)
        if(eru11(2)>0.0_rp) eru11(1) = eru11(1)/eru11(2) 
        if(eru12(2)>0.0_rp) eru12(1) = eru12(1)/sqrt(eru12(2))
        if(eru1i(2)>0.0_rp) eru1i(1) = eru1i(1)/eru1i(2) 

        if(INOTSLAVE) &
             write(momod(modul)%lun_outpu,100) &
             &       ittim,itcou,                                            &
             &       eru01(1),erp01(1),eru02(1),erp02(1),eru0i(1),erp0i(1),  &
             &       eru11(1),erp11(1),eru12(1),erp12(1),eru1i(1),erp1i(1)
        !
        ! Subgrid scale norm
        !
        if(kfl_sgsco_nsi/=0) then
           sgsno=0.0_rp
           dummi=0
           if(INOTMASTER)then
              do ielem=1,nelem
                 pelty=ltype(ielem)
                 if(pelty>0)then
                    pnode=nnode(pelty)
                    pgaus=ngaus(pelty)
                    do igaus=1,pgaus
                       dummi=dummi+1
                       do idime=1,ndime
                          sgsno=sgsno+vesgs(ielem)%a(idime,igaus,1)**2
                       end do
                    end do
                 end if
              end do
           end if
           call pararr('SUM',0_ip,1_ip,sgsno)
           call parari('SUM',0_ip,1_ip,dummi)
           sgsno = sqrt(sgsno) / real(dummi,rp)

           call norm2x(ndime,veloc,velno)
           dummi = npoin
           if(IMASTER)dummi=0
           call parari('SUM',0_ip,1_ip,dummi)
           velno = velno/real(dummi,rp)
           write(momod(modul)%lun_outpu,101) ittim,itcou,sgsno/velno 
           ! velno=0.0_rp
           ! do ipoin=1,npoin
           !    do idime=1,ndime
           !       velno=velno+veloc(idime,ipoin,1)**2
           !    end do
           ! end do
           ! velno=sqrt(velno)/real(npoin)
           ! sgsno=sqrt(sgsno)/real(dummi)
           ! write(momod(modul)%lun_outpu,101) ittim,itcou,sgsno/velno 
        end if

     end if
  end if
100 format(///,10X,'FINITE ELEMENT ERRORS',                              &
       &              /,10X,'=====================',//,                           &
       &  '          TIME STEP NO.',I5,',  ITERATION NO. ',I5,/,10X,40('-'),/,    &
       &  '          NORM       VELOCITY             PRESSURE ',/,10X,40('-'),/,  &
       &  '          W(0,1) ',E12.5,9X,E12.5 ,/, &
       &  '          W(0,2) ',E12.5,9X,E12.5 ,/, &
       &  '          W(0,i) ',E12.5,9X,E12.5 ,/, &
       &  '          W(1,1) ',E12.5,9X,E12.5 ,/, &
       &  '          W(1,2) ',E12.5,9X,E12.5 ,/, &
       &  '          W(1,i) ',E12.5,9X,E12.5 ,/,10X,40('-'))
101 format(///,10X,'SUBGRID SCALE NORM',&
       &     /,10X,'==================',//,                           &
       &  '          TIME STEP NO.',I5,',  ITERATION NO. ',I5,/,10X,40('-'),/,    &
       &  '          NORM       VELOCITY ',/,10X,40('-'),/,  &
       &  '          W(0,2) ',E12.5)

end subroutine nsi_exaerr
