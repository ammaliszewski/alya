!------------------------------------------------------------------------
!> @addtogroup NastinMatrixAssembly
!> @{
!> @file    nsi_elmope.f90
!> @author  Guillaume Houzeaux
!> @brief   Coupling with Partis module
!> @details Remove momentum from the Navier-Stokes equations
!>          \verbatim
!>          x_p,rho_p: particle position, density
!>          f_drag,a_drag: particle drag force and acceleration
!>          Navier-Stokes = \int_W f_drag.v.delta(x-x_p) dw,
!>                        = - f_drag.v(x_p)*|J(x_p)| 
!>                        = - rho_p * a_drag * v(x_p) * |J(x_p)|
!>          \endverbatim
!> @} 
!------------------------------------------------------------------------

subroutine nsi_partis()
  use def_master
  use def_domain
  use def_elmtyp
  use def_nastin
  use mod_elmgeo, only  :  elmgeo_natural_coordinates 
  implicit none
  integer(ip) :: inode,ilagr,itype,ielem,idime
  integer(ip) :: pnode,ptopo,ifoun,pelty,ipoin
  integer(ip) :: elfix(ndime,mnode)
  real(rp)    :: elcod(ndime,mnode)
  real(rp)    :: accel(ndime)        
  real(rp)    :: shapf(mnode)       
  real(rp)    :: deriv(ndime,mnode)  
  real(rp)    :: elrbu(ndime,mnode)
  real(rp)    :: gpvol,xjacm(9),gpdet
  real(rp)    :: coloc(3)
  real(rp)    :: lmini,lmaxi

  lmini = -0.01_rp
  lmaxi =  1.01_rp
  do ilagr = 1,mlagr
     if( lagrtyp(ilagr) % kfl_exist == -1 ) then
        ielem = lagrtyp(ilagr) % ielem  
        itype = lagrtyp(ilagr) % itype
        if( parttyp(itype) % kfl_modla == 2 ) then
           pelty = ltype(ielem)
           pnode = nnode(pelty)
           ptopo = ltopo(pelty)
           do inode = 1,pnode
              ipoin = lnods(inode,ielem)
              do idime = 1,ndime
                 elcod(idime,inode) = coord(idime,ipoin) 
                 elfix(idime,inode) = kfl_fixno_nsi(idime,ipoin)
              end do
           end do

           call elmgeo_natural_coordinates(      &
                ndime,pelty,pnode,elcod,shapf,   &
                deriv,lagrtyp(ilagr) % coord,coloc,ifoun)
           !call elsest_chkelm(&
           !     ndime,ptopo,pnode,elcod,shapf,deriv,&
           !     lagrtyp(ilagr) % coord,coloc,ifoun,lmini,lmaxi)  
           call jacdet(ndime,pnode,elcod,deriv,xjacm,gpvol)
           do inode = 1,pnode
              do idime = 1,ndime
                 if( elfix(idime,ipoin) /= 1 ) then
                    elrbu(idime,inode) = -parttyp(itype) % denpa * lagrtyp(ilagr) % acced(idime) * shapf(inode) * gpvol
                 else if( kfl_matdi_nsi == 0 ) then
                    elrbu(idime,inode) = 0.0_rp
                 end if
              end do
           end do
           call assrhs(ndime,pnode,lnods(1,ielem),elrbu,rhsid)              

        end if
     end if
  end do

end subroutine nsi_partis
