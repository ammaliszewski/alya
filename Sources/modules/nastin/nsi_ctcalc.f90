
!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_ctcalc.f90
!> @author  Matias
!> @brief   Calculates thrust coeff for actuator disk in terms of hub velocity
!> @details 
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_ctcalc(                     &
    xforc_material_nsi, pmate)

  use def_kintyp, only     :  ip,rp

  implicit none

  real(rp), intent(inout)  :: xforc_material_nsi(*)
  integer(ip), intent(in)  :: pmate
  integer(ip)              :: iters
  real(rp)                 :: thrco, toler, oldve, powco
  real(rp)                 :: veinf, uhub1, u_ref(3), n_disk(3), afact

  iters = 0


  ! convergence tolerance between infinite velocity and Ct
  toler = 0.01_rp
  !  disk normal 
  n_disk(1) = xforc_material_nsi( 3)
  n_disk(2) = xforc_material_nsi( 4)
  n_disk(3) = xforc_material_nsi( 5)
  u_ref(1)  = xforc_material_nsi( 9) * n_disk(1)
  u_ref(2)  = xforc_material_nsi(10) * n_disk(2)
  u_ref(3)  = xforc_material_nsi(11) * n_disk(3)

  !  normal hub velocity u · n
  uhub1     = abs(u_ref(1)+u_ref(2)+u_ref(3))
  
  !
  !  axial induction factor from last Ct 
  !  using theoretical relation

  afact = (1.0_rp-sqrt(1.0_rp-xforc_material_nsi(16)))/2.0_rp
  !  theoretical infinite velocity uinf · n normal to disk
  veinf = uhub1/(1.0_rp-afact)
  oldve = 1.0e8

  !  iterate until obtain Ct 
  do while (abs(veinf-oldve).gt.toler*veinf.and.iters.lt.10)     
     iters = iters +1
     oldve = veinf
     call nsi_thrpow(veinf, thrco, powco, pmate)
!     call nsi_thrust(veinf, thrco, powco)
     veinf = 2.0_rp*uhub1/(1.0_rp+sqrt(1.0_rp-thrco))
  end do
  ! loadS last Ct, Cp, veinf
  call nsi_thrpow(veinf, thrco, powco, pmate)
!  call nsi_thrust(veinf, thrco, powco)
  xforc_material_nsi(16) = thrco
  xforc_material_nsi(17) = powco
  xforc_material_nsi(18) = veinf

20  format(a,i2,f7.3,f7.3, f6.2 ,f13.5)  
end subroutine nsi_ctcalc


  !-------------------------------------
  !> @addtogroup Nastin
  !> @{
  !> @file    nsi_ctcalc.f90
  !> @author  Matias
  !> @brief   Interpolates thrust and power coeffs for actuator disk in terms of veinf.
  !> @details 
  !> @} 
  !-------------------------------------
subroutine nsi_thrust(veinf, thrco, powco)  

  use def_kintyp, only     :  ip,rp

  implicit none
  real(rp), intent(in)   ::  veinf
  real(rp), intent(out)  ::  powco, thrco
  integer(ip)            ::  ntabl, iz, jz, kz
  real(rp)               ::  velin(17), thrta(17), powta(17), facto

  !
  !    loads table 
  !
  ntabl = 17 ! number of values
  
  !
  !    velocity values are supposed to be given in increasing order
  !
  
  velin(1)=5.0_rp
  velin(2)=6.0_rp
  velin(3)=7.0_rp
  velin(4)=8.0_rp
  velin(5)=9.0_rp
  velin(6)=10.0_rp
  velin(7)=11.0_rp
  velin(8)=12.0_rp
  velin(9)=12.4_rp
  velin(10)=13.0_rp
  velin(11)=14.0_rp
  velin(12)=15.0_rp
  velin(13)=16.0_rp
  velin(14)=17.0_rp
  velin(15)=18.0_rp
  velin(16)=19.0_rp
  velin(17)=20.0_rp

  thrta(1) = 0.876_rp
  thrta(2) = 0.810_rp
  thrta(3) = 0.750_rp
  thrta(4) = 0.750_rp
  thrta(5) = 0.750_rp
  thrta(6) = 0.750_rp
  thrta(7) = 0.700_rp
  thrta(8) = 0.660_rp
  thrta(9) = 0.644_rp
  thrta(10) = 0.500_rp
  thrta(11) = 0.370_rp
  thrta(12) = 0.300_rp
  thrta(13) = 0.240_rp
  thrta(14) = 0.200_rp
  thrta(15) = 0.170_rp  
  thrta(16) = 0.140_rp
  thrta(17) = 0.120_rp

  powta(1)  =0.360_rp
  powta(2)  =0.390_rp
  powta(3)  =0.390_rp
  powta(4)  =0.390_rp
  powta(5)  =0.390_rp
  powta(6)  =0.390_rp
  powta(7)  =0.380_rp
  powta(8)  =0.370_rp
  powta(9)  =0.366_rp
  powta(10) =0.323_rp
  powta(11) =0.257_rp
  powta(12) =0.210_rp
  powta(13) =0.172_rp
  powta(14) =0.144_rp
  powta(15) =0.121_rp
  powta(16) =0.102_rp
  powta(17) =0.087_rp

  if (veinf.lt.velin(1)) then  
     thrco = thrta(1)
     powco = powta(1)
  else if (veinf.gt.velin(ntabl)) then
     thrco =thrta(ntabl)
     powco =powta(ntabl)
  else !linear interpolation
     iz = 1
     jz = ntabl
     kz = ntabl/2            
     do while ((jz-iz).gt.1)                 
        if (veinf.lt.velin(kz)) then
           jz = kz                  
        else
           iz = kz
        end if
        kz = (iz+jz)/2
     end do

     facto = (veinf-velin(iz))/(velin(jz)-velin(iz))     
     thrco = thrta(iz) + facto*(thrta(jz)-thrta(iz))
     powco  =  powta(iz) + facto*(powta(jz)-powta(iz))
  end if


end subroutine nsi_thrust
