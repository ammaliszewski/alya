
!-----------------------------------------------------------------------
!> @addtogroup NastinInput
!> @{
!> @file    nsi_reanut.f90
!> @author  Guillaume Houzeaux
!> @brief   Read numerical data
!> @details Read numerical data
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_reanut()
  use def_parame
  use def_inpout
  use def_master
  use def_nastin
  use def_domain
  use def_solver
  use mod_memchk
  implicit none
  integer(ip) :: imome,icont
  integer(ip) :: kfl_nota2,kfl_ta2ma  ! using these two auxiliary flags I amke it independent of the order in which it is read 

  if( INOTSLAVE ) then
     !
     !  Initializations (defaults)
     !
     staco_nsi(1)            = 1.0_rp                               ! Viscous part of tau1
     staco_nsi(2)            = 1.0_rp                               ! Convective part of tau1
     staco_nsi(3)            = 1.0_rp                               ! Rotation part of tau1
     staco_nsi(4)            = 1.0_rp                               ! Coefficient of tau2  
     tosgs_nsi               = 1.0e-2_rp                            ! Subgrid scale tolerance
     kfl_ellen_nsi           = 0                                    ! Minimum element length
     kfl_relax_nsi           = 0                                    ! Velocity: no relaxation
     kfl_relap_nsi           = 0                                    ! Pressure: no relaxation
     kfl_dttyp_nsi           = 1                                    ! Module time step strategy
     kfl_sgsco_nsi           = 0                                    ! Stabilization strategy
     kfl_sgsti_nsi           = 0                                    ! Stabilization strategy
     kfl_sgsac_nsi           = -1                                   ! SGS time accuracy
     kfl_sgsli_nsi           = 1                                    ! SGS convection linearization PICARD
     kfl_sgscp_nsi           = 0                                    ! Coupling of SGS with grid scale
     kfl_algor_nsi           = 5                                    ! Schur
     kfl_predi_nsi           = 3                                    ! Predictor-corrector like term
     kfl_taush_nsi           = 1                                    ! Schur complement. Tau strategy 
     kfl_ellsh_nsi           = 0                                    ! Schur complement.           =0,1 for min/max element length 
     kfl_updpr_nsi           = 0                                    ! Update pressure for Crank-Nicolson
     kfl_intpr_nsi           = 1                                    !           =1 pressure term integrated by parts
     kfl_assem_nsi           = 3                                    ! Assembly type (0,1,2)
     kfl_taust_nsi           = 1                                    ! Tau strategy
     kfl_stabi_nsi           = 0                                    ! Orthogonal SGS
     kfl_limit_nsi           = 0                                    ! No limiter
     kfl_trres_nsi           = 1                                    ! Transient residual
     kfl_prtre_nsi           = 0                                    ! Pressure treatment (explicit/implicit)
     kfl_lumpe_nsi           = 0                                    ! Not Lumped time term
     kfl_matdi_nsi           = 0                                    ! Impose bc at element level
     kfl_intfo_nsi           = 0                                    ! Internal force residual
     kfl_press_nsi           = 1                                    ! Integrate pressure term in momentum equations
     relax_nsi               = 1.0_rp                               ! Velocity relaxation 
     relap_nsi               = 1.0_rp                               ! Pressure relaxation 
     relsg_nsi               = 1.0_rp                               ! Subgrid scale relaxation
     kfl_shock_nsi           = 0                                    ! Shock capturing off
     shock_nsi               = 0.0_rp                               ! SC parameter
     kfl_tiacc_nsi           = 1                                    ! First order time integ.
     neule_nsi               = 1                                    ! Number of Euler time steps
     sstol_nsi               = 1.0e-5_rp                            ! Steady-satate tolerance
     safet_nsi               = 1.0e10_rp                            ! Safety factor
     bemol_nsi               = 0.0_rp                               ! Integration of convective term by parts
     kfl_normc_nsi           = 2                                    ! L2 norm for convergence
     kfl_refer_nsi           = 0                                    ! Error with reference solution 
     momod(modul) % miinn    = 1                                    ! One internal iteration
     kfl_stain_nsi           = 0                                    ! Time step to start inner iterations
     misgs_nsi               = 1                                    ! Subgrid scale iterations
     cotol_nsi               = 0.1_rp                               ! Internal tolerance
     kfl_penal_nsi           = 0                                    ! No penalization
     kfl_prepe_nsi           = 0                                    ! No pressure penalization
     penal_nsi               = 0.0_rp                               ! Penalization factor
     prepe_nsi               = 0.0_rp                               ! Pressure penalization factor
     kfl_linea_nsi           = 1                                    ! Linearization (RHS   =0, Picard   =1, Newton   =2)
     npica_nsi               = 1                                    ! Number of Picard iteration (Newton's lin.) 
     kfl_meshi_nsi           = 0                                    ! Mesh interpolator activation (OFF   =0,ON   =1)       
     kfl_tisch_nsi           = 1                                    ! Time integration scheme
     kfl_savco_nsi           = 0                                    ! Save constant matrix
     kfl_corre_nsi           = 2                                    ! Do not correct momentum  
     kfl_sosch_nsi           = 2                                    ! Momentum preserving Orthomin(1)
     kfl_modfi_nsi           = 0                                    ! Do not modify kfl_fixno_nsi
     kfl_expco_nsi           = 0                                    ! Treat the convective term explicitly, that is, assemble the matrix only in the first ortomin iteration
     kfl_addpr_nsi           = 0                                    ! Add contribution due to pressure in matrix side (do nothing BC') on wall law boundaries
     solve_sol               => solve(1:)                           ! Solver type
     xfree_nsi               = 10000000.0_rp                        ! X coordinate of the plane where to free
     kfl_grvir_nsi           = 1                                    ! Viscous gradient in residual
     kfl_hydro_nsi           = 0                                    ! How to compute hydristatic pressure
     kfl_update_hydro_nsi    = ITASK_INIUNK                         ! When do we update the hydrostatic pressure
     kfl_hydro_interface_nsi = 0                                    ! Constant interface height
     kfl_waexl_nsi           = 0                                    ! Do not use exchange location for wall law
     mitri_nsi               = 1                                    ! Number of Richardson iterations 
     safex_nsi               = 1.0_rp                               ! Time function parameter for safety factor
     dexlo_nsi               = 0.0001_rp                            ! Distance for wall law exchange location
     safma_nsi               = 1.0e9_rp                             ! Maximum safety factor
     safeo_nsi               = safet_nsi                            ! Initial safety factor
     saflo_nsi               = safet_nsi                            ! Global safety factor for local time step
     kfl_nota2               = 0                                    ! Internal use
     kfl_ta2ma               = 0                                    ! Internal use
     !
     ! Reach the section
     !
     call ecoute('nsi_reanut')
     do while(words(1)/='NUMER')
        call ecoute('nsi_reanut')
     end do
     !--><group>
     !-->       <groupName>Numerical_treatment</groupName>
     !
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> $ Numerical treatment definition
     ! ADOC[0]> $-----------------------------------------------------------------------
     ! ADOC[0]> NUMERICAL_TREATMENT
     !
     ivari_nsi = 1
     imome     = 0
     icont     = 0
     !-->          <subGroup>
     do while(words(1)/='ENDNU')
        call ecoute('nsi_reanut')

        if( words(1) == 'TAUST' ) then
           !--><inputLine>
           !-->            <inputLineName>TAU_STRATEGY</inputLineName>
           !-->            <inputLineHelp><![CDATA[<b>TAU_STRATEGY</b>: Equation for stabilization parameter tau1.
           !-->                                     <ul>
           !-->                                     <li> CODINA: tau1 = 1 / [ 4 mu/h^2 + 2 |u|/h + |w| ] </li>
           !-->                                     <li> SHAKIB: tau1 = 1 / [ 9*(4 mu/h^2)^2 + (2|u|/h)^2 + |w|^2 ]^1/2 </li></ul>]]></inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>CODINA</itemName></item><item><itemName>SHAKIB</itemName></item><item><itemName>EXACT</itemName></item>
           !-->            </inputElement>
           !--></inputLine>
           !
           ! ADOC[1]> TAU_STRATEGY:               CODINA | SHAKIB | EXACT                                       $ Equation for stabilization parameter tau
           ! ADOC[d]> <b>TAU_STRATEGY</b>: Equation for stabilization parameter tau1.
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> CODINA: tau1 = 1 / [ 4 mu/h^2 + 2 |u|/h + |w| ] </li>
           ! ADOC[d]> <li> SHAKIB: tau1 = 1 / [ 9*(4 mu/h^2)^2 + (2|u|/h)^2 + |w|^2 ]^1/2 </li>
           !
           call reatau(kfl_taust_nsi)

        else if( words(1) == 'STABI' ) then
           !--><inputLine>
           !-->            <inputLineName>STABILIZATION</inputLineName>
           !-->            <inputLineHelp><![CDATA[<b>STABILIZATION</b>: Determines the stabilization technique of the Navier-Stokes equations.
           !-->                    Both ASGS and FULL_OSS adds to the momentum and continuity equations a stabilization term.
           !-->                    The difference if the equation of the subgrid scale.
           !-->                    <ul>
           !-->                    <li> ASGS:     Algebraic subgrid scale </li>
           !-->                    <li> FULL_OSS: Orthogonal subgrid scale </li>
           !-->                    <li> SPLIT:    Split orthogonal subgrid scale.</li>
           !-->                    </ul>
           !-->                    The ASGS is generally more robust but less accurate. For the SPLIT_OSS, an additional option is required to set the 
           !-->                    limiter. 
           !-->                    <ul>
           !-->                    <li> NO_LIMITER:  No limiter is used </li>
           !-->                    <li> FIRST_ORDER: SUPG method (only first order) </li>
           !-->                    <li> SOTO:        Soto's limiter </li>
           !-->                     </ul>]]></inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>ASGS</itemName></item><item><itemName>FULL_OSS</itemName></item><item><itemDependence>0</itemDependence><itemName>SPLIT_OSS</itemName></item>
           !-->            </inputElement>
           !-->            <inputElement>
           !-->                <inputElementGroup>0</inputElementGroup>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>EMPTY</itemName></item><item><itemName>FIRST_ORDER</itemName></item><item><itemName>SOTO</itemName></item><item><itemName>NO_LIMITER</itemName></item>
           !-->            </inputElement>
           !-->        </inputLine>
           !
           ! ADOC[1]> STABILIZATION:              ASGS | FULL_OSS | SPLIT_OSS[,FIRST_ORDER | SOTO | NO_LIMITER] $ Stabilization strategy
           ! ADOC[d]> <b>STABILIZATION</b>: Determines the stabilization technique of the Navier-Stokes equations.
           ! ADOC[d]> Both ASGS and FULL_OSS adds to the momentum and continuity equations a stabilization term.
           ! ADOC[d]> The difference if the equation of the subgrid scale.
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> ASGS:     Algebraic subgrid scale </li>
           ! ADOC[d]> <li> FULL_OSS: Orthogonal subgrid scale </li>
           ! ADOC[d]> <li> SPLIT:    Split orthogonal subgrid scale.</li>
           ! ADOC[d]> </ul>
           ! ADOC[d]> The ASGS is generally more robust but less accurate. For the SPLIT_OSS, an additional option is required to set the 
           ! ADOC[d]> limiter. 
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> NO_LIMITER:  No limiter is used </li>
           ! ADOC[d]> <li> FIRST_ORDER: SUPG method (only first order) </li>
           ! ADOC[d]> <li> SOTO:        Soto's limiter </li>
           ! ADOC[d]> </ul>
           !
           if( words(2) == 'ASGS' ) then
              kfl_stabi_nsi = 0                                          ! ASGS
              if (words(3) == 'NOGRV' ) kfl_grvir_nsi = 0                ! No viscous gradient in residual
           else if( words(2) == 'FULLO' ) then
              kfl_stabi_nsi = 1                                          ! Full OSS
              if (words(3) == 'NOGRV' ) kfl_grvir_nsi = 0                ! No viscous gradient in residual
           else if( words(2) == 'SPLIT' .or. words(2) == 'OSS  ' ) then 
              kfl_stabi_nsi = 2                                          ! Split OSS

              if( exists('NOLIM') ) kfl_limit_nsi =  0                   ! No limiter
              if( exists('SOTO ') ) kfl_limit_nsi =  1                   ! Soto limiter
              if( exists('DIFFU') ) kfl_limit_nsi =  2                   ! Very diffusive limiter
              if( exists('FIRST') ) kfl_limit_nsi = -1                   ! First order
           else if (words(2) == 'CONST' ) then                           ! stability constants
              staco_nsi(1) = param(2)                                    ! c1/4 diff
              staco_nsi(2) = param(3)                                    ! c2/2 conv
              staco_nsi(3) = param(4)                                    ! c3   react
              staco_nsi(4) = param(5)                                    ! c4   pressu
              if(exists('TA2MA'))   kfl_ta2ma = 1                        ! applied at the end of the subroutine
              if(exists('NOTA2'))   kfl_nota2 = 1                        ! to avoid different behaviours depending on the order of the lines
              if ( nnpar /= 4 ) call runend ('NSI_REANUT : if stability constants are given all 4 must be given') 

           end if

        else if( words(1) == 'TRACK' ) then
        !--><inputLine>
        !-->            <inputLineName>TRACKING_SUBGRID_SCALE</inputLineName>
        !-->           <inputLineHelp><![CDATA[ TRACKING_SUBGRID_SCALE: Decide if the subgrid scale is tracked in convection and/or in time.
        !-->                        CONVECTION for convection tracking. In the case, the subgrid scale is added to the convection velocity
        !-->                        which appers both in the Galerkin term and in the equation for the subgrid scale. The scheme is therefore non-linear
        !-->                        and requires a linearization procedure (PICARD/NEWTON) and convergence control parameters (ITERATIONS, TOLERANCE, RELAXATION)
        !-->                        It subgrid scale is tracked in time (TIME option), then the order of the time discretization should be given: 1 or 2.
        !-->             ]]></inputLineHelp>
        !-->            <inputElement>
        !-->                <inputElementType>combo</inputElementType>
        !-->                <item>
        !-->                    <itemName>EMPTY</itemName>
        !-->                </item>
        !-->                <item>
        !-->                    <itemName>TIME</itemName>
        !-->                    <itemDependence>0</itemDependence>
        !-->                </item>
        !-->            </inputElement>
        !-->            <inputElement>
        !-->                <inputElementGroup>0</inputElementGroup>
        !-->                <inputElementType>combo</inputElementType>
        !-->                <inputElementName>ORDER</inputElementName>
        !-->                <item>
        !-->                    <itemName>1</itemName>
        !-->                </item>
        !-->                <item>
        !-->                    <itemName>2</itemName>
        !-->                </item>
        !-->            </inputElement>
        !-->            <inputElement>
        !-->                <inputElementType>combo</inputElementType>
        !-->                <item>
        !-->                    <itemName>COUPLED</itemName>
        !-->                </item>
        !-->                <item>
        !-->                    <itemName>DECOUPLED</itemName>
        !-->                </item>
        !-->            </inputElement>
        !-->            <inputElement>
        !-->                <inputElementType>combo</inputElementType>
        !-->                <item>
        !-->                    <itemName>EMPTY</itemName>
        !-->                </item>
        !-->                <item>
        !-->                    <itemName>CONVECTION</itemName>
        !-->                    <itemDependence>1</itemDependence>
        !-->                </item>
        !-->            </inputElement>
        !-->            <inputElement>
        !-->                <inputElementGroup>1</inputElementGroup>
        !-->                <inputElementType>combo</inputElementType>
        !-->                <item>
        !-->                    <itemName>PICARD</itemName>
        !-->                </item>
        !-->                <item>
        !-->                    <itemName>NEWTON</itemName>
        !-->                </item>
        !-->            </inputElement>
        !-->            <inputElement>
        !-->                <inputElementGroup>1</inputElementGroup>
        !-->                <inputElementType>edit</inputElementType>
        !-->                <inputElementValueType>INT</inputElementValueType>
        !-->                <inputLineEditName>ITERA</inputLineEditName>        
        !-->                <inputLineEditValue>5</inputLineEditValue>
        !-->            </inputElement>
        !-->            <inputElement>
        !-->                <inputElementGroup>1</inputElementGroup>
        !-->                <inputElementType>edit</inputElementType>
        !-->                <inputElementValueType>REAL</inputElementValueType>
        !-->                <inputLineEditName>TOLER</inputLineEditName>
        !-->                <inputLineEditValue>5</inputLineEditValue>
        !-->            </inputElement>
        !-->            <inputElement>
        !-->                <inputElementGroup>1</inputElementGroup>
        !-->                <inputElementType>edit</inputElementType>
        !-->                <inputElementValueType>REAL</inputElementValueType>        
        !-->                <inputLineEditName>RELAX</inputLineEditName>
        !-->                <inputLineEditValue>5</inputLineEditValue>
        !-->            </inputElement>
        !-->        </inputLine>
           !
           ! ADOC[1]> <b>TRACKING_SUBGRID_SCALE:</b>     TIME[, ORDER= 1 | 2 ], COUPLED | DECOUPLED \
           ! ADOC[6]> CONVECTION[, PICARD | NEWTON, ITERA= int1 , TOLER= real1 , RELAX= real2 ]  $ Subgrid scale tracking
           ! ADOC[d]> TRACKING_SUBGRID_SCALE: Decide if the subgrid scale is tracked in convection and/or in time.
           ! ADOC[d]> CONVECTION for convection tracking. In the case, the subgrid scale is added to the convection velocity
           ! ADOC[d]> which appers both in the Galerkin term and in the equation for the subgrid scale. The scheme is therefore non-linear
           ! ADOC[d]> and requires a linearization procedure (PICARD/NEWTON) and convergence control parameters (ITERATIONS, TOLERANCE, RELAXATION)
           ! ADOC[d]> It subgrid scale is tracked in time (TIME option), then the order of the time discretization should be given: 1 or 2.
           ! ADOC[d]> COUPLED: SGS are computed at the same time as grid scale.
           !
           if(exists('CONVE') ) then
              kfl_sgsco_nsi = 1 
              misgs_nsi     = getint('ITERA',1_ip,     '#Subgrid scale iterations')
              tosgs_nsi     = getrea('TOLER',1.0e-2_rp,'#Subgrid scale Tolerance')
              relsg_nsi     = getrea('RELAX',1.0_rp,   '#Subgrid scale Relaxation') 
           end if
           if(exists('TIME ')) kfl_sgsti_nsi = 1 
           kfl_sgsac_nsi = getint('ORDER',1_ip,'#Time integration order')
           if(exists('FIRST')) kfl_sgsac_nsi = 1 
           if(exists('SECON')) kfl_sgsac_nsi = 2
           if(exists('PICAR')) kfl_sgsli_nsi = 1
           if(exists('NEWTO')) kfl_sgsli_nsi = 2
           if(exists('COUPL')) kfl_sgscp_nsi = 1
           if(exists('DECOU')) kfl_sgscp_nsi = 0

        else if( words(1) == 'TIMES' ) then
           !
           ! Time step strategy: in development
           !
           if( words(2) == 'LOCAL' ) then
              kfl_dttyp_nsi = 1
           else if( words(2) == 'TAU  ' ) then
              kfl_dttyp_nsi = 3
           else if( words(2) == 'ADAPT' ) then
              kfl_dttyp_nsi = 2
              strec_nsi = getrea('STRET',2.0_rp,  '#Adaptive dt: Stretching factor')
              dampi_nsi = getrea('DAMPI',2.0_rp,  '#Adaptive dt: damping')
              epsht_nsi = getrea('EPSR ',0.025_rp,'#Adaptive dt: eps_R')
              epstr_nsi = getrea('EPSA ',0.025_rp,'#Adaptive dt: eps_A')
           end if

        else if( words(1) == 'TIMET' ) then
           !
           ! Lumped time term: in developement
           !
           if( words(2) == 'LUMPE') kfl_lumpe_nsi = 1

        else if( words(1) == 'DIRIC' ) then
           !--><inputLine>
           !-->            <inputLineName>DIRICHLET_CONDITION</inputLineName>
           !-->            <inputLineHelp><![CDATA[<b>DIRICHLET_CONDITION</b>:
           !-->             Dirichlet boundary conditions can be applied at the element level, or directly
           !-->             on the matrix. The second option should be selected when using PERIODIC nodes.]]></inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>MATRIX</itemName></item><item><itemName>ELEMENT</itemName></item>
           !-->            </inputElement>
           !-->        </inputLine>
           !
           ! ADOC[1]> DIRICHLET_CONDITION:  MATRIX | ELEMENT                                                    $ WHere Dirichlet bc are applied
           ! ADOC[d]> <b>DIRICHLET_CONDITION</b>: 
           ! ADOC[d]> Dirichlet boundary conditions can be applied at the element level, or directly
           ! ADOC[d]> on the matrix. The second option should be selected when using PERIODIC nodes.
           !
           if( words(2) == 'MATRI' ) then
              kfl_matdi_nsi = 1
           else if( words(2) == 'ELEME' ) then
              kfl_matdi_nsi = 0
           end if

        else if( words(1) == 'INTER' ) then
           !--><inputLine>
           !-->            <inputLineName>INTERNAL_FORCES</inputLineName>
           !-->            <inputLineHelp><![CDATA[<b>INTERNAL_FORCES</b>:
           !-->                   The force of the fluid on the boundaries can be computed in two ways. On the one hand, 
           !-->                   by integrating the traction over the boundaries. On the other hand, by computing the nodal residual
           !-->                   of the momentum equations where a Dirichlet condition is prescibed: f^i = buu^i - (Auu.u)^i - (Aup.p)^i]]></inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>RESIDUAL</itemName></item><item><itemName>INTEGRAL</itemName></item>
           !-->            </inputElement>
           !--></inputLine>
           !
           ! ADOC[1]> INTERNAL_FORCES:      RESIDUAL | INTEGRAL                                                 $ How to compute forces on boundaries
           ! ADOC[d]> <b>INTERNAL_FORCES</b>:
           ! ADOC[d]> The force of the fluid on the boundaries can be computed in two ways. On the one hand, 
           ! ADOC[d]> by integrating the traction over the boundaries. On the other hand, by computing the nodal residual
           ! ADOC[d]> of the momentum equations where a Dirichlet condition is prescibed: f^i = buu^i - (Auu.u)^i - (Aup.p)^i
           !
           if( words(2) == 'RESID' ) then
              kfl_intfo_nsi = 1
           else if( words(2) == 'INTEG' ) then
              kfl_intfo_nsi = 0
           end if

           ! ************************************************
!            kfl_intfo_nsi = 1_ip ! Added for FSI coupling test
           ! ************************************************

        else if( words(1) == 'ELEME' ) then
           !--><inputLine>
           !-->            <inputLineName>ELEMENT_LENGTH</inputLineName>
           !-->            <inputLineHelp><![CDATA[ELEMENT_LENGTH:
           !-->                        Element length used to compute the stabilization parameters. MINIMUM leads to a less
           !-->                        diffusive algorithm. MAXIMUM length is more diffusive, and more non-linear as the stabilization
           !-->                        is a non-linear term.]]></inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>MINIMUM</itemName></item><item><itemName>MAXIMUM</itemName></item>
           !-->            </inputElement>
           !--> </inputLine>
           !
           ! ADOC[1]> ELEMENT_LENGTH:       MINIMUM | MAXIMUM                                                   $ Element length h for stablization parameter
           ! ADOC[d]> ELEMENT_LENGTH:
           ! ADOC[d]> Element length used to compute the stabilization parameters. MINIMUM leads to a less
           ! ADOC[d]> diffusive algorithm. MAXIMUM length is more diffusive, and more non-linear as the stabilization
           ! ADOC[d]> is a non-linear term.
           !
           call realen(kfl_ellen_nsi)

        else if( words(1) == 'ASSEM' ) then
           !
           ! Obsolete option
           !
           if( words(2) == 'FAST ' ) then
              kfl_assem_nsi= 1
           else if( words(2) == 'FULL ' ) then
              kfl_assem_nsi= 0
           else if( words(2) == 'OLD  ' ) then
              kfl_assem_nsi=-1
           else if( words(2) == 'CELL ' ) then
              kfl_assem_nsi= 3
           end if

        else if( words(1) == 'PENAL' ) then
           !
           ! Obsolete option
           !
           if(exists('ON   ')) kfl_penal_nsi = 1
           if(exists('CLASS')) kfl_penal_nsi = 1
           if(exists('ITERA')) kfl_penal_nsi = 2
           if(exists('ARTIF')) kfl_penal_nsi = 3
           if(kfl_penal_nsi>=1 ) then
              penal_nsi=getrea('VALUE',0.0_rp,'#Penalty parameter')
              if(exists('AUTOM')) kfl_penal_nsi = 4
           end if

        else if( words(1) == 'PREPE' ) then
           !--><inputLine>
           !-->            <inputLineName>PRE_PENALIZATION</inputLineName>
           !-->            <inputLineHelp><![CDATA[PRE_PENALIZATION:
           !-->                Penalize pressure in time. To be used when converging to a steady state when
           !-->                the pressure presents an odd-even decoupling in time. This option helps converging
           !-->                by adding a damping effect.]]></inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>edit</inputElementType>
           !-->                <inputElementValueType>REAL</inputElementValueType>           
           !-->                <inputLineEditName>VALUE</inputLineEditName>
           !-->                <inputLineEditValue>5</inputLineEditValue>
           !-->            </inputElement>
           !--></inputLine>
           !
           ! ADOC[1]> PRE_PENALIZATION:     VALUE=real                                                          $ Pressure penalization
           ! ADOC[d]> PRE_PENALIZATION:
           ! ADOC[d]> Penalize pressure in time. To be used when converging to a steady state when
           ! ADOC[d]> the pressure presents an odd-even decoupling in time. This option helps converging
           ! ADOC[d]> by adding a damping effect.
           !
           kfl_prepe_nsi = 1
           prepe_nsi = 1.0_rp
           if(kfl_prepe_nsi>=1 ) then
              prepe_nsi=getrea('VALUE',1.0_rp,'#Pressure Penalty parameter')
           end if

        else if( words(1) == 'SHOCK' ) then
           !--><inputLine>
           !-->            <inputLineName>SHOCK_CAPTURING</inputLineName>
           !-->            <inputLineHelp><![CDATA[SHOCK_CAPTURING:
           !-->               Shock capturing for the momentum equations. It enables to obtain
           !-->               a "monotonic" scheme. The algorithm can be ISOTROPIC (very diffusive)
           !-->               or ANISOTROPIC (by adding diffusion only in the crosswind direction).
           !-->               The added term can be weighted by the VALUE=real. 0 means nothing added
           !-->               and 1 means all the term is added.]]></inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>ISOTROPIC</itemName></item><item><itemName>ANISOTROPIC</itemName></item>
           !-->            </inputElement>
           !-->            <inputElement>
           !-->                <inputElementType>edit</inputElementType>
           !-->                <inputElementValueType>REAL</inputElementValueType>           
           !-->                <inputLineEditName>Value</inputLineEditName>
           !-->                <inputLineEditValue>5</inputLineEditValue>
           !-->            </inputElement>
           !-->   </inputLine>
           !
           ! ADOC[1]> SHOCK_CAPTURING:      ISOTROPIC | ANISOTROPIC, VALUE=real                                 $ Shock capturing for the momentum equations
           ! ADOC[d]> SHOCK_CAPTURING:
           ! ADOC[d]> Shock capturing for the momentum equations. It enables to obtain
           ! ADOC[d]> a "monotonic" scheme. The algorithm can be ISOTROPIC (very diffusive)
           ! ADOC[d]> or ANISOTROPIC (by adding diffusion only in the crosswind direction).
           ! ADOC[d]> The added term can be weighted by the VALUE=real. 0 means nothing added
           ! ADOC[d]> and 1 means all the term is added.
           !
           if(exists('ISOTR').or.exists('ON   ') ) then
              kfl_shock_nsi = 1
              shock_nsi     = getrea('VALUE',0.0_rp,'#Shock capturing parameter')
           else if(exists('ANISO') ) then
              kfl_shock_nsi = 2
              shock_nsi     = getrea('VALUE',0.0_rp,'#Shock capturing parameter')
           end if

        else if( words(1) == 'UPDAT' ) then
           !--><inputLine>
           !-->   <inputLineName>UPDATE_PRESSURE</inputLineName>
           !-->   <inputLineHelp><![CDATA[UPDATE_PRESSURE:
           !-->    If this option is activated, the pressure is extrapolated at the end of
           !-->    a time step: p^{n+1} = 2 p^{n+1} - p^n]]></inputLineHelp>
           !--></inputLine>
           !
           ! ADOC[1]> UPDATE_PRESSURE:      ON                                                                  $ Pressure extrapolation at end of time step
           ! ADOC[d]> UPDATE_PRESSURE:
           ! ADOC[d]> If this option is activated, the pressure is extrapolated at the end of
           ! ADOC[d]> a time step: p^{n+1} = 2 p^{n+1} - p^n
           !
           if( words(2) == 'ON   ') kfl_updpr_nsi=1

        else if( words(1) == 'EULER' ) then
           !
           ! Retro-compatibility
           !
           neule_nsi=getint('EULER',0_ip,'#EULER TIME STEPS')

        else if( words(1) == 'TIMEI' ) then
           !--><inputLine>
           !-->            <inputLineName>TIME_INTEGRATION</inputLineName>
           !-->            <inputLineHelp><![CDATA[TIME_INTEGRATION:
           !-->                 Time integration scheme and options. Two schemes are available: trapezoidal rule
           !-->                 of order 1 or 2, and backward finite difference BDF of order from 1 to 6. For order 2, BDF
           !-->                 is generally more stable than the trapezoidal rule. In addition, for the sake of time stability,
           !-->                 one can carry out some few EULER=int2 iterations before triggering a higher order method.
           !-->            ]]></inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>TRAPEZOIDAL</itemName></item><item><itemName>BDF</itemName></item>
           !-->            </inputElement>
           !-->            <inputElement>
           !-->                <inputElementType>edit</inputElementType>
           !-->                <inputElementValueType>INT</inputElementValueType>           
           !-->                <inputLineEditName>ORDER</inputLineEditName>
           !-->                <inputLineEditValue>5</inputLineEditValue>
           !-->            </inputElement>
           !-->            <inputElement>
           !-->                <inputElementType>edit</inputElementType>
           !-->                <inputElementValueType>INT</inputElementValueType>           
           !-->                <inputLineEditName>EULER</inputLineEditName>
           !-->                <inputLineEditValue>5</inputLineEditValue>
           !-->            </inputElement>
           !--></inputLine>
           !
           ! ADOC[1]> TIME_INTEGRATION:     TRAPEZOIDAL | BDF , ORDER=int1 , EULER=int2
           ! ADOC[d]> TIME_INTEGRATION:
           ! ADOC[d]> Time integration scheme and options. Two schemes are available: trapezoidal rule
           ! ADOC[d]> of order 1 or 2, and backward finite difference BDF of order from 1 to 6. For order 2, BDF
           ! ADOC[d]> is generally more stable than the trapezoidal rule. In addition, for the sake of time stability,
           ! ADOC[d]> one can carry out some few EULER=int2 iterations before triggering a higher order method.
           !
           if(exists('TRAPE')) kfl_tisch_nsi=1
           if(exists('BDF  ')) kfl_tisch_nsi=2
           kfl_tiacc_nsi = getint('ORDER',1_ip,'#Time integration order')
           neule_nsi     = getint('EULER',0_ip,'#EULER TIME STEPS')

        else if( words(1) == 'WALLE' ) then
           !
           ! ADOC[1]> WALL_EXCHANGE_LOCATION:       DISTANCE=
           !
           kfl_waexl_nsi = 1_ip
           if( words(2) == 'DISTA' ) then
              dexlo_nsi = param(2)
           end if

        else if( words(1) == 'SAFET' ) then
           !--><inputLine>
           !-->            <inputLineName>SAFETY_FACTOR</inputLineName>
           !-->            <inputLineHelp>When using an explicit scheme, the CFL condition determines a maximum time step to obtain a stable scheme. When using</inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>edit2</inputElementType>
           !-->                <inputElementValueType>REAL</inputElementValueType>           
           !-->                <inputLineEditValue>5</inputLineEditValue>
           !-->            </inputElement>
           !--> </inputLine>
           !
           ! ADOC[1]> SAFETY_FACTOR=        real
           ! ADOC[d]> SAFETY_FACTOR=
           ! ADOC[d]> When using an explicit scheme, the CFL condition determines a maximum time step 
           ! ADOC[d]> to obtain a stable scheme. When using 
           !
           safet_nsi = param(1)
           safeo_nsi = safet_nsi ! initial safety factor

           safex_nsi  = getrea('EXPON',1.0_rp,'#Safety Factor Exponential time function')
           
           safma_nsi  = getrea('MAXIM',1.0e9_rp,'#Maximum safety factor function')
           
           saflo_nsi  = getrea('MINGL',safet_nsi,'#Minimum global safety factor when using local time step. Fixes minimum time step over elements')          
        else if( words(1) == 'BEMOL' ) then        
           !--><inputLine>
           !-->            <inputLineName>BEMOL</inputLineName>
           !-->            <inputLineHelp>When BEMOL=0, the convection term is not integrated by parts. When BEMOL=1, it is fully integrated by parts. Intermediate values are also possible. BEMOL=1 can have some robustness advantages in some cases.</inputLineHelp>
           !-->            <inputElement>
           !-->                <inputElementType>edit2</inputElementType>
           !-->                <inputElementValueType>REAL</inputElementValueType>           
           !-->                <inputLineEditValue>5</inputLineEditValue>
           !-->            </inputElement>
           !--></inputLine>
           !
           ! ADOC[1]> BEMOL=                real                                                                $ Put 1 to integrate convection by parts
           ! ADOC[d]> BEMOL:
           ! ADOC[d]> When BEMOL=0, the convection term is not integrated by parts. When BEMOL=1, it is fully
           ! ADOC[d]> integrated by parts. Intermediate values are also possible. BEMOL=1 can have some
           ! ADOC[d]> robustness advantages in some cases.
           !
           bemol_nsi = param(1)

        else if( words(1) == 'STEAD' ) then
           !-->                <inputLine>
           !-->                    <inputLineName>STEADY_STATE_TOLERANCE</inputLineName>
           !-->                    <inputLineHelp>Tolerance to decide when the solution of the module should stop because the steady state have been reached.</inputLineHelp>
           !-->                    <inputElement>
           !-->                        <inputElementType>edit2</inputElementType>
           !-->                <inputElementValueType>REAL</inputElementValueType>           
           !-->                        <inputLineEditValue>5</inputLineEditValue>
           !-->                    </inputElement>
           !-->                </inputLine>
           !
           ! ADOC[1]> STEADY_STATE_TOLERANCE=real                                                               $ Tolerance for steady state
           ! ADOC[d]> STEADY_STATE_TOLERANCE:
           ! ADOC[d]> Tolerance to decide when the solution of the module should stop because the steady
           ! ADOC[d]> state have been reached.
           !
           sstol_nsi = param(1)

        else if( words(1) == 'MODIF' ) then
           !
           ! Users defined boundary conditions
           !
           if( words(2) == 'FREE ' ) then
              kfl_modfi_nsi = 1
              xfree_nsi = getrea('XFREE',0.0_rp,'#X Coordinate of the plane where to free')
           end if
           if( words(2) == 'PRNAC') kfl_modfi_nsi=2
           if( words(2) == 'PRNA3') kfl_modfi_nsi=3
           if( words(2) == 'PRNA4') kfl_modfi_nsi=4
           if( words(2) == 'PRNA5') kfl_modfi_nsi=5
           if( words(2) == 'PRNA6') kfl_modfi_nsi=6

        else if( words(1) == 'NORMO' ) then
           !
           ! Almost obsolete
           !
           if(exists('BACKW').or.exists('LAGGE').or.exists('ALGEB') ) then
              kfl_normc_nsi = 3
           else if(exists('L2   ') ) then
              kfl_normc_nsi = 2
           else if(exists('L1   ') ) then
              kfl_normc_nsi = 1
           else if(exists('L-inf') ) then
              kfl_normc_nsi = 0
           else if(exists('REFER') ) then
              kfl_normc_nsi = 4
              if( exists('FIELD') ) then
                 kfl_refer_nsi=getint('FIELD',1_ip,'#Field of the reference solution') 
              else
                 call runend('NSI_REAOUS: REFERENCE SOLUTION NOT CODED')
              end if

           end if

        else if( words(1) == 'MAXIM' ) then
           !-->                <inputLine>
           !-->                    <inputLineName>MAXIMUM_ITERATIONS</inputLineName>
           !-->                    <inputLineHelp>MAXIMUM_ITERATIONS:
           !-->                        Maximum number of inner iterations (non-linearity+Orthomin(1)). START_AT=int2 enables on to use only one
           !-->                        iterations until time step int2 is reached. This can be used to go fast when 
           !-->                        flow is establishing. The inner iterations are also controlled by the option CONVERGENCE_TOLERANCE.</inputLineHelp>
           !-->                    <inputElement>
           !-->                        <inputElementType>edit2</inputElementType>
           !-->                <inputElementValueType>INT</inputElementValueType>           
           !-->                        <inputLineEditValue>5</inputLineEditValue>
           !-->                    </inputElement>
           !-->                    <inputElement>
           !-->                        <inputElementType>edit</inputElementType>
           !-->                <inputElementValueType>INT</inputElementValueType>           
           !-->                        <inputLineEditName>START_AT</inputLineEditName>
           !-->                        <inputLineEditValue>5</inputLineEditValue>
           !-->                    </inputElement>
           !-->                </inputLine>
           !
           ! ADOC[1]> MAXIMUM_ITERATIONS=   int1 [START_AT=int2]                                                $ Max number of inner iterations
           ! ADOC[d]> MAXIMUM_ITERATIONS:
           ! ADOC[d]> Maximum number of inner iterations (non-linearity+Orthomin(1)). START_AT=int2 enables on to use only one
           ! ADOC[d]> iterations until time step int2 is reached. This can be used to go fast when 
           ! ADOC[d]> flow is establishing. The inner iterations are also controlled by the option CONVERGENCE_TOLERANCE.
           !
           momod(modul) % miinn = int(param(1))
           if( exists('START') ) kfl_stain_nsi = getint('START',0_ip,'#Time step to start')

        else if( words(1) == 'CONVE' ) then
           !-->                <inputLine>
           !-->                    <inputLineName>CONVERGENCE_TOLERANCE</inputLineName>
           !-->                    <inputLineHelp>CONVERGENCE_TOLERANCE:
           !-->                       Convergence tolerance to control the inner iterations (non-linearity+Orthomin(1)). The
           !-->                       residual is based on the maximum lagged algebraic residual of the momentum and continuity 
           !-->                       equations. The inner iterations are also controlled by the option MAXIMUM_ITERATIONS.
           !-->                    </inputLineHelp>
           !-->                    <inputElement>
           !-->                        <inputElementType>edit2</inputElementType>
           !-->                <inputElementValueType>REAL</inputElementValueType>           
           !-->                        <inputLineEditValue>5</inputLineEditValue>
           !-->                    </inputElement>
           !-->                </inputLine>
           !
           ! ADOC[1]> CONVERGENCE_TOLERANCE=real                                                                $ Tolerance for inner iterations
           ! ADOC[d]> CONVERGENCE_TOLERANCE:
           ! ADOC[d]> Convergence tolerance to control the inner iterations (non-linearity+Orthomin(1)). The
           ! ADOC[d]> residual is based on the maximum lagged algebraic residual of the momentum and continuity 
           ! ADOC[d]> equations. The inner iterations are also controlled by the option MAXIMUM_ITERATIONS.
           !
           cotol_nsi = param(1)

        else if( words(1) == 'LINEA' ) then
           !-->                <inputLine>
           !-->                    <inputLineName>LINEARIZATION</inputLineName>
           !-->                    <inputLineHelp>LINEARIZATION:
           !-->                        Linearization strategy for the convective term: PICARD or NEWTON. When NEWTON is
           !-->                        selected, an additional option PICARD=int enables to carry out some int Picard 
           !-->                        iterations at each inner iterations in order to have a more robust convergence. Tipically,
           !-->                        one carries out 1 or 2 Picard iterations. </inputLineHelp>
           !-->                    <inputElement>
           !-->                        <inputElementType>combo</inputElementType>
           !-->                        <item><itemName>PICARD</itemName></item><item><itemDependence>0</itemDependence><itemName>NEWTON</itemName></item>
           !-->                    </inputElement>
           !-->                    <inputElement>
           !-->                        <inputElementGroup>0</inputElementGroup>
           !-->                        <inputElementType>edit</inputElementType>
           !-->                <inputElementValueType>INT</inputElementValueType>           
           !-->                        <inputLineEditName>PICARD</inputLineEditName>
           !-->                        <inputLineEditValue>5</inputLineEditValue>
           !-->                    </inputElement>
           !-->                </inputLine>
           !
           ! ADOC[1]> LINEARIZATION:        PICARD | NEWTON[PICARD=int]                                         $ Convective term linearization
           ! ADOC[d]> LINEARIZATION:
           ! ADOC[d]> Linearization strategy for the convective term: PICARD or NEWTON. When NEWTON is
           ! ADOC[d]> selected, an additional option PICARD=int enables to carry out some int Picard 
           ! ADOC[d]> iterations at each inner iterations in order to have a more robust convergence. Tipically,
           ! ADOC[d]> one carries out 1 or 2 Picard iterations. 
           !
           if( words(2) == 'PICAR' ) then
              kfl_linea_nsi = 1
           else if( words(2) == 'NEWTO' ) then
              kfl_linea_nsi = 2
              npica_nsi     = getint('PICAR',0_ip,'#Number of Picard iterations')
           end if

        else if( words(1) == 'SAVEC' ) then 
           !
           ! Obsolete option
           !
           if( words(2) == 'YES  ') kfl_savco_nsi=1

        else if( words(1) == 'MOMEN' ) then
           !
           ! Current problem is momentum equations (used to define solver)
           !
           ivari_nsi = 1
           imome     = 1

        else if( words(1) == 'ENDMO' ) then
           !
           ! Current problem is no longer momentum equations (used to define solver)
           !
           imome     = 0

        else if( words(1) == 'CONTI' ) then
           !
           ! Current problem is continuity equation (used to define solver)
           !
           ivari_nsi = 2
           icont     = 1

        else if( words(1) == 'ENDCO' ) then
           !
           ! Current problem is no longer continuity equation (used to define solver)
           !
           icont     = 0

        else if( words(1) == 'HYDRO' ) then
           !
           ! Current problem is hydrostatic state equation (used to define solver)
           !
           do while( words(1) /= 'ENDHY' )
 
              if( words(1) == 'METHO' ) then

                 if( words(2) == 'ANALY' ) then
                    kfl_hydro_nsi = NSI_ANALYTICAL_HYDROSTATIC_PRESSURE
                 else if( words(2) == 'PDE  ' ) then
                    kfl_hydro_nsi = NSI_PDE_HYDROSTATIC_PRESSURE
                 else
                    call runend('NON EXISTING SOLUTION METHOD FOR HYDROSTATIC PRESSURE')
                 end if

              else if( words(1) == 'ALGEB' ) then
                 ivari_nsi =  5
                 solve_sol => solve(ivari_nsi:)
                 call reasol(1_ip)

              else if( words(1) == 'UPDAT' ) then

                 if( words(2) == 'ON   ') then
                    kfl_update_hydro_nsi = ITASK_BEGITE
                 else if( words(2) == 'OFF  ') then
                    kfl_update_hydro_nsi = ITASK_INIUNK
                 end if

              else if( words(1) == 'INTER' ) then

                 if( words(2) == 'CONST') then
                    kfl_hydro_interface_nsi = 0
                    heihy_nsi = getrea('VALUE',0.0_rp,'#Height of free surface')
                 else
                    kfl_hydro_interface_nsi = 1
                 end if

              end if
              call ecoute('NSI_REANUT')
           end do
              
        else if( words(1) == 'ENDHY' ) then
           !
           ! Current problem is no longer hydrostatic state equation (used to define solver)
           !
           ivari_nsi = 1
      
        else if( words(1) == 'INTEG' ) then
           !
           ! Obsolete option
           !
           if( words(2) == 'OFF  ' ) then
              kfl_intpr_nsi=0
           else
              kfl_intpr_nsi=1
           end if
!-->            </subGroup>
        else if( words(1) == 'ALGEB' ) then
           !-->            <subGroup>
           !-->                <subGroupName>MOMENTUM</subGroupName>
           !-->                <subGroupCheckeable>true</subGroupCheckeable>
           !-->                <subGroupType>ALGEBRAIC_SOLVER</subGroupType>
           !-->            </subGroup>
           !-->            <subGroup>
           !-->                <subGroupName>CONTINUITY</subGroupName>
           !-->                <subGroupCheckeable>true</subGroupCheckeable>
           !-->                <subGroupType>ALGEBRAIC_SOLVER</subGroupType>
           !-->            </subGroup>
           !-->            <subGroup>
           !-->                <subGroupName>HYDROSTATIC_STATE</subGroupName>
           !-->                <subGroupCheckeable>true</subGroupCheckeable>
           !-->                <subGroupType>ALGEBRAIC_SOLVER</subGroupType>
           !-->            </subGroup>
           !
           ! ADOC[1]> XXX_EQUATION                                                                              $ Define solver for equation XXX
           ! ADOC[2]>   ALGEBRAIC_SOLVER
           ! ADOC[2]>     INCLUDE ./sample-solver.dat
           ! ADOC[2]>   END_ALGEBRAIC_SOLVER
           ! ADOC[1]> END_XXX_EQUATION
           ! ADOC[d]> XXX_EQUATION:
           ! ADOC[d]> Define the algebraic solver options for equation XXX.
           ! ADOC[d]> XXX: MOMENTUM | CONTINUITY | HYDROSTATIC_STATE
           !  
           solve_sol => solve(ivari_nsi:)
           call reasol(1_ip)

        else if( words(1) == 'PRECO' ) then  
           !
           ! See previous option
           !
           solve_sol => solve(ivari_nsi:)
           call reasol(2_ip)

        else if( words(1) == 'PRESS' ) then  
           !
           ! Pressure integration by parts
           !
           if( option('PRESS') ) then
              kfl_press_nsi = 1
           else
              kfl_press_nsi = 0
           end if

        else if( words(1) == 'RELAX' ) then
           !
           ! Obsolete option: only available for monolithic scheme
           !
           relax_nsi            = param(1)
           relap_nsi            = param(1)

           if(exists('CONST') ) then
              if(exists('VELOC') ) then
                 kfl_relax_nsi=1
              end if
              if(exists('PRESS') ) then
                 kfl_relap_nsi=1
              end if
              if(exists('BOTH ') ) then
                 kfl_relax_nsi=1
                 kfl_relap_nsi=1
              end if

           else if(exists('AITKE') ) then
              if(exists('VELOC') ) then
                 kfl_relax_nsi=2
              end if
              if(exists('PRESS') ) then
                 kfl_relap_nsi=2
              end if
              if(exists('BOTH ') ) then
                 kfl_relax_nsi=2
                 kfl_relap_nsi=2
              end if
           end if

        else if( words(1) == 'NOTA2' ) then    
           !
           ! This option is being tested: do not stabilize continuity
           !
           kfl_nota2 = 1

        else if( words(1) == 'TA2MA' ) then    
           !
           ! This option is being tested: do not stabilize continuity
           !
           kfl_ta2ma = 1

        else if( words(1) == 'EXPCO' ) then    
           !
           ! This option is being tested: Treat the convective term explicitly, 
           ! that is, assemble the matrix only in the first ortomin iteration
           !
           kfl_expco_nsi = 1

        else if( words(1) == 'ADDPR' ) then
           !
           ! This option is being tested
           !
           kfl_addpr_nsi = 1

        else if( words(1) == 'MESHI' ) then
           !--><inputLine>
           !-->            <inputLineName>MESH_INTERPOLATION</inputLineName>
           !-->            <inputLineHelp><![CDATA[MESH_INTERPOLATION:
           !-->                        This tool allows you to interpolate a solution from an original grid or a particular setup
           !-->                        to a different mesh or setup. It requires the correct set of the coupling files and allows
           !-->                        to use different number of processors. 
           !-->            <inputElement>
           !-->                <inputElementType>combo</inputElementType>
           !-->                <item><itemName>MINIMUM</itemName></item><item><itemName>MAXIMUM</itemName></item>
           !-->            </inputElement>
           !--> </inputLine>
           !
           ! ADOC[1]> MESH_INTERPOLATION:       OFF | ON 
           ! ADOC[d]> MESH_INTERPOLATION:
           ! ADOC[d]> OFF: no interpolation, ON: interpolation activation. 
           ! 
           if(exists('ON   ') ) kfl_meshi_nsi = 1 

        else if( words(1) == 'ALGOR' ) then
            !-->            <subGroup>
            !-->                <subGroupName>ALGORITHM</subGroupName>
            !-->                <!--este valor se imprime cuando se crea el fichero de alya así: ALGORITHM: SCHUR_COMPLEMENT-->
            !-->                <subGroupValue>SCHUR_COMPLEMENT</subGroupValue>
            !-->                <inputLine>
            !-->                    <inputLineName>Solver</inputLineName>
            !-->                    <inputLineHelp><![CDATA[ALGORITHM: SCHUR_COMPLEMENT
            !-->                      Only the Schur complement based solver is available. This field defines
            !-->                      all the options of the solver. Four solvers are available:
            !-->                      <ul>
            !-->                         <li> ORTHOMIN,   MOMENTUM_PRESERVING;</li>
            !-->                         <li> ORTHOMIN,   CONTINUITY_PRESERVING;</li>
            !-->                         <li> RICHARDSON, MOMENTUM_PRESERVING;</li>
            !-->                         <li> RICHARDSON, CONTINUITY_PRESERVING.</li>
            !-->                      </ul>]]></inputLineHelp>
            !-->                    <inputElement>
            !-->                        <inputElementType>combo</inputElementType>
            !-->                        <item>
            !-->                            <itemName>ORTHOMIN</itemName>
            !-->                        </item>
            !-->                        <item>
            !-->                            <itemName>RICHARDSON</itemName>
            !-->                        </item>
            !-->                    </inputElement>
            !-->                    <inputElement>
            !-->                        <inputElementType>combo</inputElementType>
            !-->                        <item>
            !-->                            <itemName>MOMENTUM_PRESERVING</itemName>
            !-->                        </item>
            !-->                        <item>
            !-->                            <itemName>CONTINUITY_PRESERVING</itemName>
            !-->                        </item>
            !-->                    </inputElement>
            !-->                </inputLine>
            !-->                <inputLine>
            !-->                    <inputLineName>Preconditioner</inputLineName>
            !-->                    <inputLineHelp><![CDATA[Additional information is required. The definition of the pressure Schur complement
            !-->                       preconditioner, and in particular, the approximation of the Uzawa operator. Two options
            !-->                       are available: DT or TAU.]]></inputLineHelp>
            !-->                    <inputElement>
            !-->                        <inputElementType>combo</inputElementType>
            !-->                        <item>
            !-->                            <itemName>DT</itemName>
            !-->                        </item>
            !-->                        <item>
            !-->                            <itemName>TAU</itemName>
            !-->                        </item>
            !-->                    </inputElement>
            !-->                </inputLine>
            !-->                <inputLine>
            !-->                    <inputLineName>TAU_STRATEGY</inputLineName>
            !-->                    <inputLineHelp> If TAU is selected one then have to choose if TAU is approximated
            !-->                       using the CODINA, SHAKIB or EXACT solution based tau.</inputLineHelp>
            !-->                    <inputElement>
            !-->                        <inputElementType>combo</inputElementType>
            !-->                        <item>
            !-->                            <itemName>CODINA</itemName>
            !-->                        </item>
            !-->                        <item>
            !-->                            <itemName>SHAKIB</itemName>
            !-->                        </item>
            !-->                        <item>
            !-->                            <itemName>EXACT</itemName>
            !-->                        </item>
            !-->                    </inputElement>
            !-->                </inputLine>
            !-->                <inputLine>
            !-->                    <inputLineName>ELEMENT_LENGTH</inputLineName>
            !-->                    <inputLineHelp>These expressions require also
            !-->                    the definition of the element length: MINIMUM or MAXIMUM.</inputLineHelp>
            !-->                    <inputElement>
            !-->                        <inputElementType>combo</inputElementType>
            !-->                        <item>
            !-->                            <itemName>MINIMUM</itemName>
            !-->                        </item>
            !-->                        <item>
            !-->                            <itemName>MAXIMUM</itemName>
            !-->                        </item>
            !-->                    </inputElement>
            !-->                </inputLine>
            !-->                <inputLine>
            !-->                    <inputLineName>CORRECTION</inputLineName>
            !-->                    <inputLineHelp><![CDATA[For CONTINUITY_PRESERVING version
            !-->                         one has to finally define the rule to define the correction matrix.
            !-->                         Usually, the following set of options exhibits good convergence:
            !-->                    <ul>
            !-->                       <li> ORTHOMIN, MOMENTUM_PRESERVING; TAU; CODINA; MINIMUM</li>
            !-->                       <li> ORTHOMIN, CONTINUITY_PRESERVING; DT; CODINA; MINIMUM</li>
            !-->                    </ul>
            !-->                    The first set requires one less solve of the continuity equation, but the second
            !-->                    one is usually more robust.]]></inputLineHelp>
            !-->                    <inputElement>
            !-->                        <inputElementType>combo</inputElementType>
            !-->                        <item>
            !-->                            <itemName>OFF</itemName>
            !-->                        </item>
            !-->                        <item>
            !-->                            <itemName>CLOSE</itemName>
            !-->                        </item>
            !-->                        <item>
            !-->                            <itemName>OPEN</itemName>
            !-->                        </item>
            !-->                    </inputElement>
            !-->                </inputLine>
            !-->            </subGroup>
           !
           ! ADOC[1]> ALGORITHM: SCHUR_COMPLEMENT
           ! ADOC[2]>   SOLVER:         ORTHOMIN | RICHARDSON , MOMENTUM_PRESERVING | CONTINUITY_PRESERVING     $ Schur complement solver
           ! ADOC[2]>   PRECONDITIONER: DT | TAU                                                                $ Dt or tau for preconditioner
           ! ADOC[2]>   TAU_STRATEGY:   CODINA | SHAKIB | EXACT                                                 $ tau approximation
           ! ADOC[2]>   ELEMENT_LENGTH: MINIMUM | MAXIMUM                                                       $ h in tau
           ! ADOC[2]>   CORRECTION:     OFF | CLOSE | OPEN                                                      $ Rule for mass correction in continuity preserving
           ! ADOC[1]> END_ALGORITHM
           ! ADOC[d]> ALGORITHM: SCHUR_COMPLEMENT
           ! ADOC[d]> Only the Schur complement based solver is available. This field defines
           ! ADOC[d]> all the options of the solver. Four solvers are available:
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> ORTHOMIN,   MOMENTUM_PRESERVING;</li>
           ! ADOC[d]> <li> ORTHOMIN,   CONTINUITY_PRESERVING;</li>
           ! ADOC[d]> <li> RICHARDSON, MOMENTUM_PRESERVING;</li>
           ! ADOC[d]> <li> RICHARDSON, CONTINUITY_PRESERVING.</li>
           ! ADOC[d]> </ul>
           ! ADOC[d]> Additional information is required. The definition of the pressure Schur complement
           ! ADOC[d]> preconditioner, and in particular, the approximation of the Uzawa operator. Two options
           ! ADOC[d]> are available: DT or TAU. If TAU is selected one then have to choose if TAU is approximated
           ! ADOC[d]> using the CODINA, SHAKIB or EXACT solution based tau. These expressions require also
           ! ADOC[d]> the definition of the element length: MINIMUM or MAXIMUM. For CONTINUITY_PRESERVING version
           ! ADOC[d]> one has to finally define the rule to define the correction matrix.
           ! ADOC[d]> Usually, the following set of options exhibits good convergence:
           ! ADOC[d]> <ul>
           ! ADOC[d]> <li> ORTHOMIN, MOMENTUM_PRESERVING; TAU; CODINA; MINIMUM</li>
           ! ADOC[d]> <li> ORTHOMIN, CONTINUITY_PRESERVING; DT; CODINA; MINIMUM</li>
           ! ADOC[d]> </ul>
           ! ADOC[d]> The first set requires one less solve of the continuity equation, but the second
           ! ADOC[d]> one is usually more robust.
           !
           if(exists('MONOL')) kfl_algor_nsi = 1
           if(exists('SCHUR') ) then
              kfl_algor_nsi = 5
              kfl_taush_nsi = kfl_taust_nsi  ! Tau strategy for preconditioner
              kfl_predi_nsi = 3              ! Preconditioner=tau
              kfl_ellsh_nsi = 0              ! Minimum length
              kfl_corre_nsi = 2              ! Open correction
              kfl_sosch_nsi = 2              ! Momentum preserving Orthomin
              mitri_nsi     = 1              ! Number of Richardson iterations
              call ecoute('nsi_reanut')
              do while(words(1)/='ENDAL')
                 if( words(1) == 'SOLVE' ) then
                    if( words(2) == 'ORTHO' ) then
                       kfl_sosch_nsi = 1                       
                    else if( words(2) == 'RICHA' ) then
                       kfl_sosch_nsi = 4                       
                    else
                       call runend('WRONG SCHUR COMPLEMENT SOLVER')
                    end if
                    if(exists('MOMEN') ) then
                       kfl_sosch_nsi=kfl_sosch_nsi+1
                    else if(exists('CONTI') ) then
                       kfl_sosch_nsi=kfl_sosch_nsi+2
                    end if
                 else if( words(1) == 'RICHA' ) then
                    mitri_nsi = getint('RICHA',1_ip,'#Number of Richardson iterations')
                 else if( words(1) == 'PRECO' ) then
                    if(exists('DT   ')) kfl_predi_nsi = 2                    
                    if(exists('TAU  ')) kfl_predi_nsi = 3
                    if(exists('MASS ')) kfl_predi_nsi = 4     
                    if(exists('LUMPE')) kfl_predi_nsi = 5     
                 else if( words(1) == 'TAUST' ) then
                    call reatau(kfl_taush_nsi) 
                 else if( words(1) == 'ELEME' ) then
                    call realen(kfl_ellsh_nsi)
                 else if( words(1) == 'CORRE' ) then
                    if(exists('OFF  '))                     kfl_corre_nsi = 0
                    if(exists('CLOSE'))                     kfl_corre_nsi = 1                    
                    if(exists('OPENR').or.exists('OPEN ') ) kfl_corre_nsi = 2
                 end if
                 call ecoute('nsi_reanut')
              end do

           end if

        end if
     end do
     !-->        </group>     
     !
     ! ADOC[0]> END_NUMERICAL_TREATMENT
     !
     if ( kfl_nota2 /= 0 .and. kfl_ta2ma /=0 ) call runend ('NSI_REANUT: NOTA2 and TA2MA must not be used at the same time')
     if ( kfl_nota2 == 1 ) staco_nsi(4) = 0.0_rp
     if ( kfl_ta2ma == 1 ) staco_nsi(4) = 0.25_rp / staco_nsi(1)
     
  end if

end subroutine nsi_reanut
 
