subroutine nsi_immbou()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_immbou
  ! NAME
  !    nsi_immbou
  ! DESCRIPTION
  !    Compute force on particles
  ! USES
  !    bouder
  !    chenor
  ! USED BY
  !    nsi_outset
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_nastin
  use mod_gradie
  use mod_ker_proper
  implicit none
  real(rp)             :: elvel(ndime,mnode)
  real(rp)             :: elpre(mnode)
  real(rp)             :: elcod(ndime,mnode)
  real(rp)             :: elmut(mnode)
  real(rp)             :: eltem(mnode)
  real(rp)             :: elgra(ntens,mnode)

  real(rp)             :: bocod(ndime,mnodb)
  real(rp)             :: bovel(ndime,mnodb)
  real(rp)             :: bovfi(ndime,mnodb)     
  real(rp)             :: bopre(mnodb)
  real(rp)             :: baloc(ndime,ndime)

  real(rp)             :: gbmut
  real(rp)             :: gbgve(9)
  real(rp)             :: gbrvi(3)
  real(rp)             :: gbgvi
  real(rp)             :: gbden(mgaus)
  real(rp)             :: gbvis(mgaus)
  real(rp)             :: gbpor(mgaus)
  real(rp)             :: gbpre(mgaus)
  real(rp)             :: gbvel(ndime,mgaus)
  real(rp)             :: gbvdt(ndime,mgaus)   ! tangent component of velocity - prescribed velocity.
  real(rp)             :: gbcoo(3)
  real(rp)             :: grave(3,3)
  real(rp)             :: cartb(ndime,mnode)
  real(rp)             :: gpgvi(ndime,mgaus)
  real(rp)             :: gpcar(ndime,mnode,mgaus)

  integer(ip)          :: ielem,inode,ipoin,idime,iimbo,imeth,kgaus
  integer(ip)          :: pnode,iboun,igaub,inodb,dummi,itens,jelem
  integer(ip)          :: pelty,pblty,pnodb,pgaub,pmate,igaus,pgaus
  integer(ip)          :: jdime,kboun,izdom,jzdom,jpoin,kauxi
  real(rp)             :: xjaci(9),xjacm(9),hleng(3),velno,tauwa,ustar
  real(rp)             :: gbsur,eucta,gbdet,dummr
  real(rp)             :: tragl(9),F1v(3),F1p(3),T1p(3),T1v(3),x(3)  
  real(rp)             :: gbstr(ntens),bouno(3),gbpos(ndime)
  real(rp)             :: nx,ny,nz
  real(rp)             :: deriv(3,64),coloc(3),shaib(mnode)
  real(rp)             :: veaux(3),fauxi(3),fauxn
  real(rp),    pointer :: Fp(:),Fv(:),Tp(:),Tv(:)


  if( nimbo == 0 ) return

  call livinf(70,' ',0_ip)
  !
  ! Initialization
  !
  imeth = 1
  do iimbo = 1,nimbo
     Fv => imbou(iimbo) % vforce
     Fp => imbou(iimbo) % pforce
     Tv => imbou(iimbo) % vtorqu
     Tp => imbou(iimbo) % ptorqu
     do idime = 1,3
        Fp(idime)  = 0.0_rp
        Fv(idime)  = 0.0_rp
        Tp(idime)  = 0.0_rp
        Tv(idime)  = 0.0_rp

        F1v(idime) = 0.0_rp
        F1p(idime) = 0.0_rp

        T1p(idime) = 0.0_rp
        T1v(idime) = 0.0_rp

        x(idime)   = 0.0_rp
     end do
  end do
  if (ittim <= 1 ) return
  !
  ! Allocate memory: If gradients are smoothed
  !
  if( INOTMASTER ) then
     if( imeth == 1 .and. kfl_intfo_nsi == 0 ) then
        call memgen(0_ip,ntens,npoin)
        call graten(veloc,gevec)
     else if( kfl_intfo_nsi >= 1 ) then
        call memgen(1_ip,npoin,0_ip)
     end if
  end if
  !
  ! Loop over boundaries
  !
  if( INOTMASTER ) then

     kgaus = 0
     do iimbo = 1,nimbo

        Fv     => imbou(iimbo) % vforce
        Fp     => imbou(iimbo) % pforce
        Tv     => imbou(iimbo) % vtorqu
        Tp     => imbou(iimbo) % ptorqu

        !-------------------------------------------------------------
        !
        ! Embedded bodies
        !
        !-------------------------------------------------------------

        if( imbou(iimbo) % kfl_typeb == 0 ) then

           if( kfl_intfo_nsi >= 1 ) then
              !
              ! Nodal force
              ! 
              do ipoin = 1,npoin
                 if( lntib(ipoin) == -iimbo ) then
                    do idime = 1,ndime
                       Fv(idime) = Fv(idime) + intfo_nsi(ipoin) % bu(idime)
                       jzdom = 0
                       do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                          jpoin = c_dom(izdom)
                          jzdom = jzdom + 1
                          do jdime = 1,ndime
                             Fv(idime) = Fv(idime) - intfo_nsi(ipoin) % Auu(jdime,idime,jzdom) * veloc(jdime,jpoin,1) 
                          end do
                          Fp(idime) = Fp(idime) - intfo_nsi(ipoin) % Aup(idime,jzdom) * press(jpoin,1)                           
                       end do
                    end do
                    if( kfl_hydro_nsi /= 0 ) then
                       do idime = 1,ndime
                          jzdom = 0
                          do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                             jpoin = c_dom(izdom)
                             jzdom = jzdom + 1
                             Fp(idime) = Fp(idime) - intfo_nsi(ipoin) % Aup(idime,jzdom) * bpess_nsi(1,jpoin)         
                          end do
                       end do
                    end if
                    gisca(ipoin) = 0
                 end if
              end do

           else
              !
              ! Element properties and dimensions
              !
              boundaries: do iboun = 1,imbou(iimbo) % nboib

                 pblty    = imbou(iimbo) % ltyib(iboun)
                 pnodb    = nnode(pblty)
                 pgaub    = ngaib(pblty)
                 !
                 ! BOCOD: Gather operations
                 !
                 do inodb = 1,pnodb
                    ipoin = imbou(iimbo) % lnoib(inodb,iboun)
                    do idime = 1,ndime
                       bocod(idime,inodb) = imbou(iimbo) % cooib(idime,ipoin)
                    end do
                 end do
                 !
                 ! BOUNO: Exterior normal
                 !        Invert normal for body-fitted bodies
                 !
                 call exteib(iimbo,pnodb,iboun,bouno,0_ip)

                 do igaub = 1,pgaub
                    !
                    ! Jacobian
                    !
                    call bouder(&
                         pnodb,ndime,ndimb,elmar(pblty) % derib(1,1,igaub),& 
                         bocod,baloc,eucta) 
                    gbsur = elmar(pblty) % weiib(igaub)*eucta 
                    kgaus = kgaus + 1
                    !
                    ! Gauss point coordinates
                    !
                    do idime = 1,ndime
                       gbpos(idime) = 0.0_rp
                       do inodb = 1,pnodb
                          gbpos(idime) = gbpos(idime) &
                               + elmar(pblty) % shaib(inodb,igaub) * bocod(idime,inodb)
                       end do
                    end do
                    !
                    ! Embedded type: Look for element, shape function and derivative (IELEM, SHAIB, DERIV) 
                    !       
                    jelem = lgaib(kgaus)                   
                    if( jelem > 0 ) then
                       ielem = jelem
                       call runend('IBM NOT CODED')
                       !call elsest(&
                       !     6_ip,1_ip,ielse,mnode,ndime,npoin,nelem,nnode(1:),&
                       !     lnods,ltype,ltopo,coord,gbpos ,relse,&
                       !     jelem,shaib,deriv,coloc,dummi)                       
                       pelty = ltype(ielem)
                       if( pelty <= 0 ) call runend('FOUND A HOLE!')
                       pnode = nnode(pelty)
                       !
                       ! Compute stress and pressure
                       !
                       pmate = 1
                       if( nmate > 1 ) then
                          if( lmate(ielem) > nmatf ) pmate = -1
                       end if
                       call nsi_elmgap(&  
                            pnode,pmate,lnods(1,ielem),elcod,elpre,&
                            elvel,elmut,eltem)
                       do inode = 1,pnode
                          ipoin = lnods(inode,ielem)
                          elpre(inode) = press(ipoin,1)
                       end do
                       !
                       ! Add base pressure to account for buoyancy
                       !
                       if( kfl_hydro_nsi /= 0 ) then
                          do inode = 1,pnode
                             ipoin = lnods(inode,iboun)
                             elpre(inode) = elpre(inode) + bpess_nsi(1,ipoin)
                          end do
                       end if

                       if( imeth == 1 ) then
                          !
                          ! Projected gradients
                          !
                          do inode = 1,pnode
                             ipoin = lnods(inode,ielem)
                             do itens = 1,ntens
                                elgra(itens,inode) = gevec(itens,ipoin)
                             end do
                          end do

                       else if( imeth == 0 ) then
                          !
                          ! Embedded type: Cartesian derivatives
                          !
                          call elmder(&
                               pnode,ndime,deriv,elcod,gpcar,gbdet,xjacm,xjaci)
                       end if
                       !
                       ! GBVIS: Properties
                       !
                       call elmlen(&
                            ndime,pnode,elmar(pelty) % dercg,tragl,elcod,&
                            hnatu(pelty),hleng)
                       if( kfl_prope /= 0 ) then
                          call ker_proper('DENSI','IGAUS',1_ip,ielem,gbden,pnode,pgaus,shaib,gpcar)
                          call ker_proper('VISCO','IGAUS',1_ip,ielem,gbvis,pnode,pgaus,shaib,gpcar)
                          call nsi_turbul(&
                               -1_ip,0_ip,pnode,1_ip,1_ip,1_ip,kfl_cotur_nsi,&
                               shaib,gpcar,hleng,elvel,elmut,gbden,gbvis,gbmut,&
                               gbgvi,gbrvi,gbgve, ielem, kfl_kemod_ker)
                       else
                          call runend('DIEODIEOW')
                       end if
                       !
                       ! GBPRE, GBGRV: Pressure and velocity derivatives at Gauss points
                       !
                       gbstr    = 0.0_rp
                       gbpre(1) = 0.0_rp     
                       if( imeth == 0 ) then
                          if( ndime == 2 ) then
                             do inode = 1,pnode
                                gbpre(1) = gbpre(1) + shaib(inode) * elpre(inode)
                                gbstr(1) = gbstr(1) + 2.0_rp * gpcar(1,inode,1) * elvel(1,inode)
                                gbstr(2) = gbstr(2) + 2.0_rp * gpcar(2,inode,1) * elvel(2,inode)
                                gbstr(3) = gbstr(3) + gpcar(1,inode,1) * elvel(2,inode) &
                                     &              + gpcar(2,inode,1) * elvel(1,inode)
                             end do
                          else
                             do inode = 1,pnode
                                gbpre(1) = gbpre(1) + shaib(inode) * elpre(inode)
                                gbstr(1) = gbstr(1) + 2.0_rp * gpcar(1,inode,1) * elvel(1,inode)
                                gbstr(2) = gbstr(2) + 2.0_rp * gpcar(2,inode,1) * elvel(2,inode)
                                gbstr(3) = gbstr(3) + gpcar(1,inode,1) * elvel(2,inode) &
                                     &              + gpcar(2,inode,1) * elvel(1,inode)
                                gbstr(4) = gbstr(4) + 2.0_rp * gpcar(3,inode,1) * elvel(3,inode)
                                gbstr(5) = gbstr(5) + gpcar(1,inode,1) * elvel(3,inode) &
                                     &              + gpcar(3,inode,1) * elvel(1,inode)
                                gbstr(6) = gbstr(6) + gpcar(2,inode,1) * elvel(3,inode) &
                                     &              + gpcar(3,inode,1) * elvel(2,inode)
                             end do
                          end if
                       else   
                          if( ndime == 2 ) then
                             do inode = 1,pnode
                                gbpre(1) = gbpre(1) + shaib(inode) * elpre(inode)
                                gbstr(1) = gbstr(1) + shaib(inode) * elgra(1,inode)
                                gbstr(2) = gbstr(2) + shaib(inode) * elgra(2,inode)
                                gbstr(3) = gbstr(3) + shaib(inode) * elgra(3,inode)
                             end do
                          else
                             do inode = 1,pnode
                                gbpre(1) = gbpre(1) + shaib(inode) * elpre(inode)
                                gbstr(1) = gbstr(1) + shaib(inode) * elgra(1,inode)
                                gbstr(2) = gbstr(2) + shaib(inode) * elgra(2,inode)
                                gbstr(3) = gbstr(3) + shaib(inode) * elgra(3,inode)
                                gbstr(4) = gbstr(4) + shaib(inode) * elgra(4,inode)
                                gbstr(5) = gbstr(5) + shaib(inode) * elgra(5,inode)
                                gbstr(6) = gbstr(6) + shaib(inode) * elgra(6,inode)
                             end do
                          end if
                       end if
                       !
                       ! F1: Compute force (fluid on solid as normal points outward)
                       !
                       nx = gbvis(1) * bouno(1)
                       ny = gbvis(1) * bouno(2)
                       if( ndime == 2 ) then                   
                          F1p(1) = - gbpre(1) * bouno(1)    ! - p.nx 
                          F1v(1) =   gbstr(1) * nx        & ! + mu*(du/dx+du/dx).nx 
                               &   + gbstr(3) * ny          ! + mu*(du/dy+dv/dx).ny
                          F1p(2) = - gbpre(1) * bouno(2)    ! - p.ny
                          F1v(2) =   gbstr(3) * nx        & ! + mu*(du/dy+dv/dx).nx 
                               &   + gbstr(2) * ny          ! + mu*(dv/dy+dv/dx).ny
                       else
                          nz = gbvis(1) * bouno(3)
                          F1p(1) = - gbpre(1) * bouno(1) 
                          F1v(1) =   gbstr(1) * nx        &
                               &   + gbstr(3) * ny        &
                               &   + gbstr(5) * nz
                          F1p(2) = - gbpre(1) * bouno(2) 
                          F1v(2) =   gbstr(3) * nx        &
                               &   + gbstr(2) * ny        &
                               &   + gbstr(6) * nz
                          F1p(3) = - gbpre(1) * bouno(3) 
                          F1v(3) =   gbstr(5) * nx        &
                               &   + gbstr(6) * ny        &
                               &   + gbstr(4) * nz
                       end if
                       !
                       ! T1: Compute torque= F1 x X
                       !
                       x(3) = 0.0_rp   ! so that it has the correct value in the 2d case
                       do idime = 1,ndime
                          x(idime) = gbpos(idime) - imbou(iimbo) % posil(idime,1) 
                       end do
                       call vecpro(x,F1p,T1p,3_ip)                 ! T1 = (X-Xg) x F1  (pressure) 
                       call vecpro(x,F1v,T1v,3_ip)                 ! T1 = (X-Xg) x F1  (viscous) 
                       !
                       ! F, T: Actualize force and torque 
                       !
                       do idime = 1,3
                          Fv(idime) = Fv(idime) + F1v(idime) * gbsur
                          Tv(idime) = Tv(idime) + T1v(idime) * gbsur
                          Fp(idime) = Fp(idime) + F1p(idime) * gbsur
                          Tp(idime) = Tp(idime) + T1p(idime) * gbsur
                       end do

                    end if

                 end do

              end do boundaries

           end if

        else

           !-------------------------------------------------------------
           !
           ! Body fitted bodies
           !
           !-------------------------------------------------------------

           if( kfl_intfo_nsi >= 1 ) then
              !
              ! Force is computed using nodal force Fi: F = Sum_i Fi
              !
              do kboun = 1,imbou(iimbo) % nboib
                 iboun = imbou(iimbo) % lbinv(kboun)
                 pblty = ltypb(iboun) 
                 pnodb = nnode(pblty)
                 do inodb = 1,pnodb
                    ipoin = lnodb(inodb,iboun)
                    gisca(ipoin) = 1
                 end do
              end do
              do ipoin = 1,npoin
                 if( gisca(ipoin) == 1 ) then
                    do idime = 1,ndime
                       Fv(idime) = Fv(idime) + intfo_nsi(ipoin) % bu(idime)
                       jzdom = 0
                       do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                          jpoin = c_dom(izdom)
                          jzdom = jzdom + 1
                          do jdime = 1,ndime
                             Fv(idime) = Fv(idime) - intfo_nsi(ipoin) % Auu(jdime,idime,jzdom) * veloc(jdime,jpoin,1) 
                          end do
                          Fp(idime) = Fp(idime) - intfo_nsi(ipoin) % Aup(idime,jzdom) * press(jpoin,1)                           
                       end do
                    end do
                    if( kfl_hydro_nsi /= 0 ) then
                       do idime = 1,ndime
                          jzdom = 0
                          do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                             jpoin = c_dom(izdom)
                             jzdom = jzdom + 1
                             Fp(idime) = Fp(idime) - intfo_nsi(ipoin) % Aup(idime,jzdom) * bpess_nsi(1,jpoin)         
                          end do
                       end do
                    end if
                    gisca(ipoin) = 0
                 end if
              end do
              !
              ! For wall law we need to add the force due to the wall law as is done in the case (INTEG) because as the  
              ! tangential direction is free the residual formulation calculates no force in this direction. 
              ! We reapeat what is done in the INTEG case but only for wall law
              !
              kauxi = 0_ip
              boundaries3: do kboun = 1,imbou(iimbo) % nboib
                 iboun = imbou(iimbo) % lbinv(kboun)
                 if( kfl_fixbo_nsi(iboun) == 3 ) kauxi = kauxi + 1_ip
              end do boundaries3
              if (kauxi>0) then
                 !
                 ! Only wall law force is computed using traction: F = int_S sig.n ds
                 !             
                 boundaries4: do kboun = 1,imbou(iimbo) % nboib

                    iboun = imbou(iimbo) % lbinv(kboun)
                    pblty = ltypb(iboun) 
                    pnodb = nnode(pblty)
                    pgaub = ngaus(pblty)
                    ielem = lboel(pnodb+1,iboun)
                    pelty = ltype(ielem)
                    pmate = 1

                    do inodb = 1,pnodb
                       ipoin = lnodb(inodb,iboun)
                       bopre(inodb) = press(ipoin,1)
                       do idime = 1,ndime
                          bocod(idime,inodb) = coord(idime,ipoin)
                          bovel(idime,inodb) = veloc(idime,ipoin,1)
                       end do
                       if( kfl_coupl(ID_NASTIN,ID_ALEFOR) /= 0 ) then 
                          do idime = 1,ndime
                             bovfi(idime,inodb) = velom(idime,ipoin)    ! see comments in nsi_bouope
                          end do
                       else
                          do idime = 1,ndime
                             bovfi(idime,inodb) = 0.0_rp
                          end do
                       end if
                    end do

                    if( pelty > 0 ) then

                       pnode = nnode(pelty)
                       pgaus = ngaus(pelty)
                       do inode = 1,pnode
                          ipoin = lnods(inode,ielem)
                          do idime = 1,ndime
                             elvel(idime,inode) = veloc(idime,ipoin,1)
                             elcod(idime,inode) = coord(idime,ipoin)
                          end do
                       end do
                       !
                       ! Element length HLENG
                       !
                       call elmlen(&
                            ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&
                            hnatu(pelty),hleng)
                       !
                       ! Values at Gauss points: GBPRE, GBVEL, GBTEM
                       !
                       do igaub = 1,pgaub
                          gbpre(igaub) = 0.0_rp
                          do idime = 1,ndime
                             gbvel(idime,igaub) = 0.0_rp
                             gbvdt(idime,igaub) = 0.0_rp
                          end do
                          ! do inodb = 1,pnodb
                          !    ipoin = lnodb(inodb,iboun)
                          !    gbpre(igaub) = gbpre(igaub) + elmar(pblty)%shape(inodb,igaub) * press(ipoin,1)
                          !    do idime = 1,ndime
                          !       gbvel(idime,igaub) = gbvel(idime,igaub) + elmar(pblty)%shape(inodb,igaub) * veloc(idime,ipoin,1)
                          !    end do
                          ! end do
                          ! OJO no veo porqeu guillaume lo ha hecho con press y veloc cuando ya ha calculado bopre y bovel
                          ! yo prefiero hacerlo con bopre bovel  y para la differencia de veloc con bovfi(porque este ultimo est en eje global, lo he rotado
                          ! mus arriba
                          do inodb = 1,pnodb
                             gbpre(igaub) = gbpre(igaub) + elmar(pblty)%shape(inodb,igaub) * bopre(inodb)
                             do idime = 1,ndime
                                gbvel(idime,igaub) = gbvel(idime,igaub) + elmar(pblty)%shape(inodb,igaub) * bovel(idime,inodb)
                                gbvdt(idime,igaub) = gbvdt(idime,igaub) + elmar(pblty)%shape(inodb,igaub) * ( bovel(idime,inodb)  &
                                     &    - bovfi(idime,inodb) )  ! for the moment it includes normal component (substracted later)
                             end do
                          end do
                       end do
                       if( kfl_hydro_nsi /= 0 ) then
                          do igaub = 1,pgaub
                             do inodb = 1,pnodb
                                ipoin = lnodb(inodb,iboun)
                                gbpre(igaub) = gbpre(igaub) &
                                     + elmar(pblty)%shape(inodb,igaub) * bpess_nsi(1,ipoin)
                             end do
                          end do
                       end if
                       !
                       ! Properties: mu and rho 
                       !
                       call ker_proper('DENSI','PGAUB',dummi,iboun,gbden,pnode,pgaub,elmar(pblty)%shape)
                       call ker_proper('VISCO','PGAUB',dummi,iboun,gbvis,pnode,pgaub,elmar(pblty)%shape)   
                       !
                       ! Cartesian derivatives
                       !
                       do igaus = 1,pgaus
                          call elmder(&
                               pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&          ! Cartesian derivative
                               elcod,gpcar(1,1,igaus),dummr,xjacm,xjaci)            ! and Jacobian
                       end do
                       !
                       ! Loop over Gauss points
                       !
                       gauss_points: do igaub = 1,pgaub

                          call bouder(&
                               pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&    ! Cartesian derivative
                               bocod,baloc,eucta)                                   ! and Jacobian
                          gbsur = elmar(pblty)%weigp(igaub)*eucta 
                          call chenor(pnode,baloc,bocod,elcod)                      ! Check normal

                          do idime = 1,3  
                             F1v(idime) = 0.0_rp              
                             T1v(idime) = 0.0_rp
                          end do
                          !
                          ! Viscous force: Fv
                          !
                          if( kfl_fixbo_nsi(iboun) == 3 ) then
                             !
                             ! Law of the wall: F = - rho * (u*)^2 * (u_tan-u_fix_tan)/|u_tan-u_fix_tan|
                             !
                             if( kfl_rough > 0 ) then
                                rough_dom = 0.0_rp
                                do inodb = 1,pnodb
                                   ipoin = lnodb(inodb,iboun)
                                   rough_dom = rough_dom + rough(ipoin) * elmar(pblty)%shape(inodb,igaub)
                                end do
                             end if
                             call nsi_bouwal(&                        
                                  2_ip,1_ip,pnodb,dummi,iboun,lboel(1,iboun),elmar(pblty)%shape(1,igaub),&
                                  bovel,bovfi,dummr,gbvis(igaub),gbden(igaub),baloc,ustar,dummr,rough_dom,dummr,dummr)

                             do idime = 1,ndime              ! Substract normal componenet from gbvdt
                                veaux(idime) = gbvdt(idime,igaub)
                             end do
                             do idime = 1,ndime     
                                do jdime = 1,ndime
                                   gbvdt(idime,igaub) = gbvdt(idime,igaub)   &
                                        - baloc(idime,ndime) &
                                        * baloc(jdime,ndime) * veaux(jdime)
                                end do
                             end do

                             call vecnor(gbvdt(1,igaub),ndime,velno,2_ip)
                             if( velno == 0.0_rp ) velno = 1.0_rp
                             tauwa = gbden(igaub) * ustar * ustar
                             F1v(3) = 0.0_rp   ! so that in the 2d case it has the correct value
                             do idime = 1,ndime
                                F1v(idime) = - tauwa * gbvdt(idime,igaub) / velno
                             end do

                          end if
                          !
                          ! Torque: Tv
                          !
                          do idime = 1,3
                             gbcoo(idime) = 0.0_rp
                          end do
                          do inodb = 1,pnodb
                             ipoin = lnodb(inodb,iboun)
                             do idime = 1,ndime
                                gbcoo(idime) = gbcoo(idime) &
                                     + elmar(pblty) % shape(inodb,igaub) &
                                     * coord(idime,ipoin)
                             end do
                          end do
                          do idime = 1,ndime
                             gbcoo(idime) = gbcoo(idime) - imbou(iimbo) % posil(idime,1)
                          end do
                          call vecpro(gbcoo,F1v,T1v,3_ip)       ! Viscous torque:  (r-rc) x Fv

                          do idime = 1,3
                             Fv(idime) =  Fv(idime) - gbsur * F1v(idime)
                             Tv(idime) =  Tv(idime) - gbsur * T1v(idime)
                          end do

                       end do gauss_points

                    end if

                 end do boundaries4
              end if
              ! if( ittim <= 1 ) then
              !    Fv = 0.0_rp
              !    Fp = 0.0_rp
              ! end if
              !print*,'force a=',Fv(2),Fp(2)
              !   Fv = 0.0_rp
              !   Fp = 0.0_rp

           else
              !
              ! Forces is computed using traction: F = int_S sig.n ds
              !             
              boundaries2: do kboun = 1,imbou(iimbo) % nboib

                 iboun = imbou(iimbo) % lbinv(kboun)
                 pblty = ltypb(iboun) 
                 pnodb = nnode(pblty)
                 pgaub = ngaus(pblty)
                 ielem = lboel(pnodb+1,iboun)
                 pelty = ltype(ielem)
                 pmate = 1

                 do inodb = 1,pnodb
                    ipoin = lnodb(inodb,iboun)
                    bopre(inodb) = press(ipoin,1)
                    do idime = 1,ndime
                       bocod(idime,inodb) = coord(idime,ipoin)
                       bovel(idime,inodb) = veloc(idime,ipoin,1)
                    end do
                    if( kfl_coupl(ID_NASTIN,ID_ALEFOR) /= 0 ) then 
                       do idime = 1,ndime
                          bovfi(idime,inodb) = velom(idime,ipoin)    ! see comments in nsi_bouope
                       end do
                    else
                       do idime = 1,ndime
                          bovfi(idime,inodb) = 0.0_rp
                       end do
                    end if
                 end do

                 if( pelty > 0 ) then

                    pnode = nnode(pelty)
                    pgaus = ngaus(pelty)
                    do inode = 1,pnode
                       ipoin = lnods(inode,ielem)
                       do idime = 1,ndime
                          elvel(idime,inode) = veloc(idime,ipoin,1)
                          elcod(idime,inode) = coord(idime,ipoin)
                       end do
                    end do
                    !
                    ! Element length HLENG
                    !
                    call elmlen(&
                         ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&
                         hnatu(pelty),hleng)
                    !
                    ! Values at Gauss points: GBPRE, GBVEL, GBTEM
                    !
                    do igaub = 1,pgaub
                       gbpre(igaub) = 0.0_rp
                       do idime = 1,ndime
                          gbvel(idime,igaub) = 0.0_rp
                          gbvdt(idime,igaub) = 0.0_rp
                       end do
                       !                       do inodb = 1,pnodb
                       !                          ipoin = lnodb(inodb,iboun)
                       !                          gbpre(igaub) = gbpre(igaub) + elmar(pblty)%shape(inodb,igaub) * press(ipoin,1)
                       !                          do idime = 1,ndime
                       !                             gbvel(idime,igaub) = gbvel(idime,igaub) + elmar(pblty)%shape(inodb,igaub) * veloc(idime,ipoin,1)
                       !                          end do
                       !                       end do
                       ! OJO no veo porqeu guillaume lo ha hecho con press y veloc cuando ya ha calculado bopre y bovel
                       ! yo prefiero hacerlo con bopre, bovel  y para la differencia de veloc con bovfi(porque este último esta 
                       ! en eje global, lo he rotado más arriba)
                       ! podría haber puesto al diff en gbvel directamente pero no porque se usa en nsi_elmpro
                       do inodb = 1,pnodb
                          gbpre(igaub) = gbpre(igaub) + elmar(pblty)%shape(inodb,igaub) * bopre(inodb)
                          do idime = 1,ndime
                             gbvel(idime,igaub) = gbvel(idime,igaub) + elmar(pblty)%shape(inodb,igaub) * bovel(idime,inodb)
                             gbvdt(idime,igaub) = gbvdt(idime,igaub) + elmar(pblty)%shape(inodb,igaub) * ( bovel(idime,inodb)  &
                                  &  - bovfi(idime,inodb) )  ! for the moment it includes normal component (substracted later)
                          end do
                       end do
                    end do
                    if( kfl_hydro_nsi /= 0 ) then
                       do igaub = 1,pgaub
                          do inodb = 1,pnodb
                             ipoin = lnodb(inodb,iboun)
                             gbpre(igaub) = gbpre(igaub) &
                                  + elmar(pblty)%shape(inodb,igaub) * bpess_nsi(1,ipoin)
                          end do
                       end do
                    end if
                    !
                    ! Properties: mu and rho 
                    !
                    call ker_proper('DENSI','PGAUB',dummi,iboun,gbden,pnode,pgaub,elmar(pblty)%shape)
                    call ker_proper('VISCO','PGAUB',dummi,iboun,gbvis,pnode,pgaub,elmar(pblty)%shape)      
                    !
                    ! Cartesian derivatives
                    !
                    do igaus = 1,pgaus
                       call elmder(&
                            pnode,ndime,elmar(pelty)%deriv(1,1,igaus),&          ! Cartesian derivative
                            elcod,gpcar(1,1,igaus),dummr,xjacm,xjaci)            ! and Jacobian
                    end do
                    !
                    ! Loop over Gauss points
                    !
                    gauss_points1: do igaub = 1,pgaub

                       call bouder(&
                            pnodb,ndime,ndimb,elmar(pblty)%deriv(1,1,igaub),&    ! Cartesian derivative
                            bocod,baloc,eucta)                                   ! and Jacobian
                       gbsur = elmar(pblty)%weigp(igaub)*eucta 
                       call chenor(pnode,baloc,bocod,elcod)                      ! Check normal
                       !
                       ! Velocity gradients grad(u): GRAVE
                       !
                       call cartbo(&
                            1_ip,lboel(1,iboun),elmar(pblty)%shape(1,igaub),&
                            elmar(pelty)%shaga,gpcar,elmar(pelty)%shape,&
                            dummr,cartb,pnodb,pnode,pgaus)
                       do idime = 1,ndime
                          do jdime = 1,ndime
                             grave(idime,jdime) = 0.0_rp
                             do inode = 1,pnode
                                grave(idime,jdime) = grave(idime,jdime) &
                                     + cartb(idime,inode) * elvel(jdime,inode)
                             end do
                          end do
                       end do

                       do idime = 1,3  
                          F1v(idime) = 0.0_rp              
                          F1p(idime) = 0.0_rp
                          T1v(idime) = 0.0_rp
                          T1p(idime) = 0.0_rp                       
                       end do
                       !
                       ! Pressure force: Fp = -p.n
                       !
                       do idime = 1,ndime  
                          F1p(idime) = -gbpre(igaub)*baloc(idime,ndime)
                       end do
                       !
                       ! Viscous force: Fv
                       !
                       if( kfl_fixbo_nsi(iboun) == 3 ) then
                          !
                          ! Law of the wall: F = - rho * (u*)^2 * (u_tan-u_fix_tan)/|u_tan-u_fix_tan|
                          !
                          if( kfl_rough > 0 ) then
                             rough_dom = 0.0_rp
                             do inodb = 1,pnodb
                                ipoin = lnodb(inodb,iboun)
                                rough_dom = rough_dom + rough(ipoin) * elmar(pblty)%shape(inodb,igaub)
                             end do
                          end if
                          call nsi_bouwal(&                        
                               2_ip,1_ip,pnodb,dummi,iboun,lboel(1,iboun),elmar(pblty)%shape(1,igaub),&
                               bovel,bovfi,dummr,gbvis(igaub),gbden(igaub),baloc,ustar,dummr,rough_dom,dummr,dummr)

                          do idime = 1,ndime              ! Substract normal componenet from gbvdt
                             veaux(idime) = gbvdt(idime,igaub)
                          end do
                          do idime = 1,ndime     
                             do jdime = 1,ndime
                                gbvdt(idime,igaub) = gbvdt(idime,igaub)   &
                                     - baloc(idime,ndime) &
                                     * baloc(jdime,ndime) * veaux(jdime)
                             end do
                          end do

                          call vecnor(gbvdt(1,igaub),ndime,velno,2_ip)
                          if( velno == 0.0_rp ) velno = 1.0_rp
                          tauwa = gbden(igaub) * ustar * ustar
                          do idime = 1,ndime
                             F1v(idime) = - tauwa * gbvdt(idime,igaub) / velno
                          end do
                          !
                          ! Add the normal component of the traction vector  F
                          ! (F.n)n = (n.sig.n)n = {n.(mu * [ grad(u) + grad(u)^t ] . n )}n
                          ! because the wall law only deals with tangential component
                          !
                          do idime = 1,ndime   ! obtain F
                             fauxi(idime) = 0.0_rp
                             do jdime = 1,ndime
                                fauxi(idime) = fauxi(idime)&
                                     + gbvis(igaub) * ( grave(jdime,idime) + grave(idime,jdime) )&
                                     * baloc(jdime,ndime)
                             end do
                          end do
                          fauxn = 0.0_rp ! F.n
                          do idime = 1,ndime
                             fauxn = fauxi(idime)* baloc(idime,ndime)
                          end do
                          do idime = 1,ndime    ! add (F.n)n
                             F1v(idime) = F1v(idime)&
                                  + fauxn * baloc(idime,ndime)
                          end do

                       else
                          !
                          ! No-slip: F = sig.n = mu * [ grad(u) + grad(u)^t ] . n ! actually all non-wall law cases (Slip etc)
                          !
                          do idime = 1,ndime
                             do jdime = 1,ndime
                                F1v(idime) = F1v(idime)&
                                     + gbvis(igaub) * ( grave(jdime,idime) + grave(idime,jdime) )&
                                     * baloc(jdime,ndime)
                             end do
                          end do

                       end if
                       !
                       ! Torque: Tv and Tp
                       !
                       do idime = 1,3
                          gbcoo(idime) = 0.0_rp
                       end do
                       do inodb = 1,pnodb
                          ipoin = lnodb(inodb,iboun)
                          do idime = 1,ndime
                             gbcoo(idime) = gbcoo(idime) &
                                  + elmar(pblty) % shape(inodb,igaub) &
                                  * coord(idime,ipoin)
                          end do
                       end do
                       do idime = 1,ndime
                          gbcoo(idime) = gbcoo(idime) - imbou(iimbo) % posil(idime,1)
                       end do
                       call vecpro(gbcoo,F1v,T1v,3_ip)       ! Viscous torque:  (r-rc) x Fv
                       call vecpro(gbcoo,F1p,T1p,3_ip)       ! Pressure torque: (r-rc) x Fp

                       do idime = 1,3
                          Fv(idime) =  Fv(idime) - gbsur * F1v(idime)
                          Fp(idime) =  Fp(idime) - gbsur * F1p(idime)
                          Tv(idime) =  Tv(idime) - gbsur * T1v(idime)
                          Tp(idime) =  Tp(idime) - gbsur * T1p(idime)
                       end do

                    end do gauss_points1

                 end if

              end do boundaries2
           end if
           !   print*,'force b=',Fv(2),Fp(2)

        end if
     end do

  end if


  !write(99,*) Fv(2),Fp(2)
  !do iimbo = 1,nimbo
  !   Fv => imbou(iimbo) % vforce
  !   Fp => imbou(iimbo) % pforce
  !   Tv => imbou(iimbo) % vtorqu
  !   Tp => imbou(iimbo) % ptorqu
  !   do idime = 1,3
  !!      Fp(idime)  = 0.0_rp
  !!      Fv(idime)  = 0.0_rp
  !!      Tp(idime)  = 0.0_rp
  !!      Tv(idime)  = 0.0_rp
  !   end do
  !end do
  !
  ! Add buoyancy: OJO, poner en el bucle elemental mas arriba
  !
  !do iimbo = 1,nimbo
  !   Fp  => imbou(iimbo) % pforce
  !   do idime = 1,ndime
  !      Fp(idime) = Fp(idime) - grnor * gravi(idime) * imbou(iimbo) % volum * densi_nsi(1,1)
  !   end do
  !end do
  !
  ! Deallocate
  !
  if( INOTMASTER ) then
     if( imeth == 1 .and. kfl_intfo_nsi == 0 ) then
        call memgen(2_ip,ntens,npoin)
     else if( kfl_intfo_nsi >= 1 ) then
        call memgen(3_ip,npoin,0_ip)
     end if
  end if
  !
  ! Reduce sum in Parallel
  !
  call ibmdef(6_ip) 

end subroutine nsi_immbou
