subroutine Nasedg(order)
  !-----------------------------------------------------------------------
  !****f* nasedg/Nasedg
  ! NAME 
  !    Nasedg
  ! DESCRIPTION
  !    This routine deals with the incompressible NS equations.
  !    Nasedg is monitorized for Paraver. 
  ! USES
  !    nsi_turnon
  !    nsi_timste
  !    nsi_begste
  !    nsi_doiter
  !    nsi_concon
  !    nsi_conblk
  !    nsi_newmsh
  !    nsi_endste
  !    nsi_turnof
  ! USED BY
  !    Reapro
  !    Turnon
  !    Timste
  !    Begste
  !    Doiter
  !    Concon
  !    Conblk
  !    Newmsh
  !    Endste
  !    Turnof
  !***
  !-----------------------------------------------------------------------
  use      def_master
  use      def_nasedg
  implicit none
  integer(ip), intent(in) :: order

  select case (order)

  case(ITASK_REAPRO)
     !call nsi_reapro()
  case(ITASK_TURNON)
     !if(kfl_modul(modul)/=0) call nsg_turnon()
  case(ITASK_TIMSTE) 
     !if(kfl_modul(modul)/=0) call nsg_timste()
  case(ITASK_BEGSTE) 
     !if(kfl_modul(modul)/=0) call nsg_begste()
  case(ITASK_DOITER)
     !if(kfl_modul(modul)/=0) call nsg_doiter()
  case(ITASK_CONCOU)
     !if(kfl_modul(modul)/=0) call nsg_concou()
  case(ITASK_CONBLK)
     !if(kfl_modul(modul)/=0) call nsg_conblk()
  case(ITASK_NEWMSH)
     !if(kfl_modul(modul)/=0) call nsg_newmsh()
  case(ITASK_ENDSTE)
     !if(kfl_modul(modul)/=0) call nsg_endste()
  case(ITASK_TURNOF)
     !if(kfl_modul(modul)/=0) call nsg_turnof()

  end select

end subroutine Nasedg
