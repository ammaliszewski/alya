module def_nasedg

  ! NAME
  !   def_domain
  ! DESCRIPTION
  !   This module is the header of the meshing part
  !***
  !-----------------------------------------------------------------------
  use def_kintyp

  !------------------------------------------------------------------------
  ! Edge based mesh solver
  !------------------------------------------------------------------------
  integer(ip), pointer              ::     ptoed2(:)=>null()
  integer(ip), pointer              ::     ptoed3(:)=>null()
  integer(ip), pointer              ::     ledglm(:,:)=>null()
  integer(ip), pointer              ::     ledgfa(:,:)=>null()
  integer(ip), pointer              ::     ledgb(:,:)=>null()
  integer(ip), pointer              ::     lboup(:,:)=>null()
  integer(ip), pointer              ::     lbous(:,:)=>null()
  integer(8)                        ::     memor_edg(2)
  !------------------------------------------------------------------------
  ! Edge based  solver
  !------------------------------------------------------------------------
  real(rp), pointer                 ::     edsha(:,:)=>null() 
  real(rp), pointer                 ::     edshab(:,:)=>null() 
  real(rp), pointer                 ::     edshap(:,:)=>null() 
  real(rp), pointer                 ::     flu(:,:)=>null() 
  real(rp), pointer                 ::     var(:,:)=>null(),varn(:,:)=>null() 
  real(rp), pointer                 ::     varb(:,:)=>null()
  real(rp), pointer                 ::     varnn(:,:)=>null() 
  real(rp), pointer                 ::     varpn(:,:)=>null() 
  real(rp), pointer                 ::     dvar(:,:)=>null() 
  real(rp), pointer                 ::     rbous(:,:)=>null() 
  real(rp), pointer                 ::     extr1(:,:)=>null(),extr2(:,:)=>null() 
  real(rp), pointer                 ::     grad(:,:,:)=>null() 
  real(rp), pointer                 ::     rmass(:)=>null() 
  real(rp), pointer                 ::     rdir(:,:)=>null() 
  real(rp), pointer                 ::     sens(:)=>null() 
  real(rp), pointer                 ::     viscp(:,:)=>null() 
  real(rp), pointer                 ::     hydro1(:,:)=>null() 
  real(rp), pointer                 ::     hydro2(:,:)=>null() 
  real(rp), pointer                 ::     rdtloc(:)=>null() 
  !------------------------------------------------------------------------
  ! Edge based renumerotation
  !------------------------------------------------------------------------
  integer(ip), pointer              ::     edpar(:)=>null()


end module def_nasedg



