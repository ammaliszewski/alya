subroutine rkcomp(nrkord,nedge,ledge,edsha,var,nvar,dvar,npoin,flu,gam,grad,dtau,ndim,rmass,varpn,extr1,extr2,coor,ledgb,nedgb,edshab,edshap,nboup,mboup,lboup,rdir,nflu,cvi,sens,lbous,rbous,nsurf,grav,igrav,ipre,cp,niter,ierr,viscp,ivisc,temp0,Pri,cv,scons,mu0,varnn,idual,dt,varn,ifirst,isolv,cpi,R,hydro1,hydro2,varb,rdtloc)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nedge,nrkord,ndim,npoin,nvar,nflu,nsurf,ivisc,ifirst,isolv
  integer(ip), intent(in)    :: nedgb,nboup,mboup,igrav,ipre,niter,idual
  integer(ip), intent(in)    :: ledge(2,nedge),ledgb(2,nedgb),lboup(6,mboup)
  integer(ip), intent(in)    :: lbous(2,nsurf)
  integer(ip), intent(inout) :: ierr
  real(rp),intent(in)        :: gam,dtau,dt,rdir(ndim,nboup),cvi,rbous(5,nsurf),rdtloc(npoin)
  real(rp),intent(in)        :: grav,cp,temp0,Pri,cv,scons,mu0,cpi,R,varb(nvar,nboup)
  real(rp), intent(inout)    :: edsha(5,nedge),var(nvar,npoin),dvar(nflu,npoin),varnn(nvar,npoin) 
  real(rp), intent(inout)    :: edshab(3,nedgb),edshap(3,npoin),sens(nedge) 
  real(rp), intent(inout)    :: flu(nflu,nedge),grad(ndim,6,npoin),rmass(npoin) 
  real(rp), intent(inout)    :: coor(ndim,npoin),viscp(2,npoin)
  real(rp), intent(inout)    :: varn(nvar,npoin),extr1(nvar,nedge),extr2(nvar,nedge) 
  real(rp), intent(inout)    :: varpn(nvar,npoin),hydro1(2,nedge),hydro2(2,nedge) 
  integer(ip)                :: istep,i,ipm1,ipm2,ipm3,ipm4,ipm5,j
  integer(ip)                :: iedge,ip1,ip2,ipoin,ipo,ipoi
  real(rp)                   :: alpha,c10,dt2,dti,dvar1,dvar2,dvar3,dvar4,dvar5,c00,rl
  real(rp)                   :: gravi,gravie,rhot,temp00,pres00,rho,z,temp,rhoh,v,pres,presex,freq,freq2
  c10=1.0d+00
  c00=0.0d+00


  !
  !  isolv = 1   --> JST  flux
  !  isolv = 2   --> HLLC flux
  !  isolv = 3   --> ROE flux

  !
  !  ifirst=1  --> first order 
  !


  !goto 1000
  
  
1000 continue   


  do istep=1,nrkord
     !
     !     Clean up flu
     !
     flu=c00
     !
     !    Compute Runge-Kutta coefficient
     !
     alpha=c10/float(nrkord+1-istep) 

     if(isolv>1)then 

        !
        !     Riemann solver
        !

        if(ifirst==1)then
           !
           !     First order, copy the point value to extr1 && extr2
           !  
           call cpedpt(nedge,ledge,var,nvar,npoin,extr1,extr2)

        else

           !
           !     Second order, get the gradients of the unknowns
           !
           call gtgrad(nedge,ledge,nvar,ndim,var,grad,edsha,npoin,ledgb,nedgb,edshab,edshap,nboup,lboup,rmass,mboup)
           !
           !     Extrapolate and limit
           !
           call valimi(nedge,ledge,nvar,ndim,npoin,var,coor,extr1,extr2,grad)

        endif
        !
        !     Get the pressure of the extrapolated states 
        !
        write(*,*)'Computing pressure'
        ierr=0_ip 
        call gtpres(gam,extr1,nvar,nedge,ierr)
        if(ierr==1)return
        call gtpres(gam,extr2,nvar,nedge,ierr)
        if(ierr==1)return

     else
        !
        !     JST Flux
        !
        !
        !     Get the pressure  
        !
        write(*,*)'Computing pressure'
        ierr=0_ip
        call gtpres(gam,var,nvar,npoin,ierr)
        if(ierr==1)return
        !
        !     Get the gradient of the conservative variables
        !
        call gtgrad(nedge,ledge,nvar,ndim,var,grad,edsha,npoin,ledgb,nedgb,edshab,edshap,nboup,lboup,rmass,mboup)

     endif
     !
     !     Compute the Galerkin fluxes
     !
     !call galflu(nedge,ledge,nvar,ndim,edsha,extr1,extr2,flu,npoin,nflu)
     if(isolv==1)then
        !
        !     If JST chosen, compute pressure gradient
        !
        write(*,*)'Computing JST fluxes'
        write(*,*)'nedge',nedge
        call gtgradp(nedge,ledge,nvar,ndim,var,grad,edsha,npoin,ledgb,nedgb,edshab,edshap,nboup,lboup,rmass,mboup)

        !
        !     Do we want low Mach
        !
        if(ipre==0)then
           !
           !     No, compute JST fluxes
           !
           call jstflu(nedge,ledge,npoin,nvar,nflu,edsha,gam,var,sens,coor,ndim,flu,grad)
        else
           !
           !     First get the viscous gradients
           !
           call gtgradpre(nvar,ndim,var,grad,npoin,cvi)
           !
           !     Then get JST fluxes
           !  
           call jstflupre(nedge,ledge,npoin,nvar,nflu,edsha,gam,var,sens,coor,ndim,flu,grad,cvi,cp,ivisc,viscp)

        endif

     else if(isolv==2)then
        !
        !     Do we want low Mach
        !
        if(ipre==0)then
           !
           !     Compute the HLLC fluxes
           !
           write(*,*)'Computing HLLC fluxes'
           call hllcflu(nedge,ledge,npoin,nvar,nflu,edsha,gam,extr1,extr2,flu,coor,ndim,hydro1,hydro2,rmass,grav)
        
        else
        
           !
           !     Compute the low Mach HLLC fluxes 
           !
           write(*,*)'Computing low Mach HLLC fluxes'
           call hllcflupre(nedge,ledge,npoin,nvar,nflu,edsha,gam,extr1,extr2,flu,coor,ndim,ivisc,viscp,rmass,hydro1,hydro2,grav)
        
        endif

     else if(isolv==3)then 
        !
        !     Compute the Roe fluxes
        !
        write(*,*)'Computing Roe fluxes'
        call roeflu(nedge,ledge,npoin,edsha,flu,gam,extr1,extr2,nvar,nflu,coor,ndim)
     endif
     !
     !     Boundary edges contribution
     ! 
     call galflub(nedgb,ledgb,nvar,ndim,edshab,var,flu,npoin,nflu)
     !
     !     Boundary points contribution
     ! 
     call galflup(nboup,nvar,ndim,edshap,var,flu,npoin,lboup,nflu,mboup)
     !
     !     Viscous fluxes 
     !
     !call visflu(ledge,nedge,edsha,npoin,nvar,var,cvi,flu,nflu)
     if(ivisc==1)then
        call visflu(ledge,nedge,edsha,npoin,nvar,var,cvi,flu,nflu,viscp,ndim,grad,temp0,scons,cv,Pri,edshab,edshap,nboup,lboup,nedgb,ledgb,mu0,mboup)
     endif 
     !
     !     Do we have gravity
     ! 
     

     !
     !     Brunt-Vaisala frequency
     !
     freq=0.01d+00
     freq2=freq*freq
     temp00=280.0d+00
     pres00=100000.0d+00
     !temp00=300.0d+00
     !pres00=100000.0d+00


     if(igrav==1)then


!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(rmass,npoin,flu,var,grav,temp00,pres00,cp,R,cpi,coor) &
!$omp& private(i,gravi,z,temp,pres,rhoh,rho,v,presex)

        do i=1,npoin
           z=coor(3,i)
           !
           !     Straka test   
           !
           !temp=temp00-grav*z*cpi
           !pres=pres00*(temp/temp00)**(cp/R)
           !rhoh=pres/(R*temp)
           !
           !     Schar mountain
           !
           !Potential temperature
           temp=temp00*exp(freq2*z/grav)
           !Exner pressure
           presex=1+((grav*grav)/(cp*temp00*freq2))*(exp(-freq2*z/grav)-c10)
           !Density
           rhoh=(pres00/(R*temp))*presex**(cv/R)

           rho=var(1,i)
           v=var(4,i)/rho 

           flu(4,i)=flu(4,i)-(rho-rhoh)*grav/rmass(i)   
           flu(5,i)=flu(5,i)-v*rho*grav/rmass(i)
        enddo

     endif
     !
     !     For low Mach multiply the residual
     !
     if(ipre==1)then 
           !
           !     Do we want dual time step
           !
           if(idual==0)then
        
              call preresid(flu,nflu,npoin,var,gam,cvi,nvar,cp,viscp,ivisc,rmass)

           else

              call preresdual(flu,nflu,nvar,npoin,var,varn,varnn,dtau,niter,dt,cvi,rmass,ivisc,gam,viscp,cp)

           endif

     endif
     !
     !     Get delta u
     !
     dt2=dtau*alpha

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(rmass,dvar,npoin,flu,rdtloc,alpha) &
!$omp& private(i,dti)

     do i=1,npoin

        !dti=dt2*rmass(i)
        dti=rdtloc(i)*alpha*rmass(i)   
        !DBG 1D
        !dti=dt2   
        dvar(1,i)=dti*flu(1,i)   
        dvar(2,i)=dti*flu(2,i)   
        dvar(3,i)=dti*flu(3,i)   
        dvar(4,i)=dti*flu(4,i)   
        dvar(5,i)=dti*flu(5,i)   
      enddo
     !
     !  Apply boundary conditions
     !
     write(*,*)'Applying bc'
     call applbc(dvar,lboup,nboup,varpn,rdir,ndim,nvar,npoin,nflu,lbous,rbous,nsurf,var,gam,mboup,varb)

     !
     !    DBG  1D 
     !
     !dvar(2,1)=c00
     !dvar(2,npoin)=c00

     !
     !     Monitor the values
     !

     dvar1=dvar(1,1)
     ipm1=1_ip
     dvar2=dvar(2,1)
     ipm2=1_ip
     dvar3=dvar(3,1)
     ipm3=1_ip
     dvar4=dvar(4,1)
     ipm4=1_ip
     dvar5=dvar(5,1)
     ipm5=1_ip

     do i=2,npoin
        if(dvar(1,i)>dvar1)then
           dvar1=dvar(1,i)
           ipm1=i
        endif
        if(dvar(2,i)>dvar2)then
           dvar2=dvar(2,i)
           ipm2=i
        endif
        if(dvar(3,i)>dvar3)then
           dvar3=dvar(3,i)
           ipm3=i
        endif
        if(dvar(4,i)>dvar4)then 
           dvar4=dvar(4,i)
           ipm4=i
        endif
        if(dvar(5,i)>dvar5)then
           dvar5=dvar(5,i)
           ipm5=i
        endif
     enddo

     write(*,*)dvar1,dvar2,dvar3,dvar4,dvar5
     write(*,*)ipm1,ipm2,ipm3,ipm4,ipm5
     !
     !     Get new values
     !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(var,dvar,varpn,npoin) &
!$omp& private(i)

     do i=1,npoin
        var(1,i)=dvar(1,i)+varpn(1,i)
        var(2,i)=dvar(2,i)+varpn(2,i)
        var(3,i)=dvar(3,i)+varpn(3,i)
        var(4,i)=dvar(4,i)+varpn(4,i)
        var(5,i)=dvar(5,i)+varpn(5,i)
        !
        !     DBG 
        !
        !if(var(2,i)<-0.01d+00)then
        !  write(*,*)'negative velocity point:',i
        !stop
        !endif
     enddo




     !
     !     DBG
     !
     !do i=1,npoin
     !   if(var(1,i)>1.001d+00)then
     !      write(*,*)'overshoot in point:',i,'rho=',var(1,i)
     !   endif
     !enddo


     !
     !     Compute pressure for final state
     !
     ierr=0_ip 
     call gtpres(gam,var,nvar,npoin,ierr)
     if(ierr==1)return

  enddo


end subroutine rkcomp

subroutine gtpres(gam,var,nvar,npoin,ierr)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)     :: npoin,nvar
  integer(ip),intent(inout)  :: ierr
  real(rp),intent(in)        :: gam
  real(rp),intent(inout)     :: var(nvar,npoin) 
  real(rp)                   :: c10,gam1,rho,rhou,rhov,rhow,rhoe,pres,rhoi,c05,c00   
  integer(ip)                :: ipoin

  c10=1.0d+00
  c00=0.0d+00
  c05=0.5d+00
  gam1=gam-c10 


  !
  !     For the moment, perfect gas
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(var,npoin,c00,c10,c05,gam1,ierr) &
!$omp& private(ipoin,rho,rhoe,rhoi,pres,rhou,rhov,rhow)
!
  do ipoin=1,npoin

     rho=var(1,ipoin)
     rhou=var(2,ipoin)
     rhov=var(3,ipoin)
     rhow=var(4,ipoin)
     rhoe=var(5,ipoin)

     !
     !     Test everything is ok
     !
     if(rho<c00)then
        write(*,*)'negative density point:',ipoin
        ierr=1
     endif

     if(rhoe<c00)then
        write(*,*)'negative energy point:',ipoin
        ierr=1
     endif


     rhoi=c10/rho

     pres=gam1*(rhoe-c05*rhoi*(rhou*rhou+rhov*rhov+rhow*rhow))
     var(6,ipoin)=pres
     var(7,ipoin)=rhoi

     if(pres<c00)then
        write(*,*)'negative pressure point:',ipoin
        write(*,*)rhoe,rhoi,rhou,rhov,rhow
        write(*,*)c05*rhoi*(rhou*rhou+rhov*rhov+rhow*rhow)
        write(*,*)gam1*(rhoe-c05*rhoi*(rhou*rhou+rhov*rhov+rhow*rhow))
        write(*,*)pres
        ierr=1
     endif

  enddo


end subroutine gtpres






subroutine gtgrad(nedge,ledge,nvar,ndim,var,grad,edsha,npoin,ledgb,nedgb,edshab,edshap,nboup,lboup,rmass,mboup)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nedge,nvar,ndim,npoin,nedgb,nboup,mboup
  integer(ip),intent(in)  :: ledge(2,nedge),ledgb(2,nedgb),lboup(6,mboup)
  real(rp),intent(in)     :: var(nvar,npoin),edsha(5,nedge),edshab(3,nedgb),edshap(3,nboup),rmass(npoin)
  real(rp),intent(inout)  :: grad(ndim,6,npoin)
  real(rp)                :: rsum1,rsum2,rsum3,rsum4,rsum5,rtemp
  real(rp)                :: redg11,redg12,redg13,redg21,redg22,redg23
  real(rp)                :: redg31,redg32,redg33,redg41,redg42,redg43
  real(rp)                :: redg51,redg52,redg53,c00,coef1,coef2,coef3
  integer(ip)             :: ipoin,iedge,ip1,ip2,ipo

  c00=0.0d+00
  !
  !     Put grad to zero
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(grad,npoin,c00) &
!$omp& private(ipoin)
!
  do ipoin=1,npoin
     grad(1,1,ipoin)=c00
     grad(2,1,ipoin)=c00
     grad(3,1,ipoin)=c00
     grad(1,2,ipoin)=c00
     grad(2,2,ipoin)=c00
     grad(3,2,ipoin)=c00
     grad(1,3,ipoin)=c00
     grad(2,3,ipoin)=c00
     grad(3,3,ipoin)=c00
     grad(1,4,ipoin)=c00
     grad(2,4,ipoin)=c00
     grad(3,4,ipoin)=c00
     grad(1,5,ipoin)=c00
     grad(2,5,ipoin)=c00
     grad(3,5,ipoin)=c00
  enddo
  !
  !     Compute the projection of the gradient of the unknowns
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,var,grad) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rsum1,rsum2,rsum3,rsum4,rsum5,&  
!$omp&                       redg11,redg12,redg13,redg21,redg22,redg23,redg31,redg32,redg33,& 
!$omp&                       redg41,redg42,redg43,redg51,redg52,redg53 )
!
  do iedge=1,nedge

     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)

     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)

     rsum1=var(1,ip1)+var(1,ip2)
     rsum2=var(2,ip1)+var(2,ip2)
     rsum3=var(3,ip1)+var(3,ip2)
     rsum4=var(4,ip1)+var(4,ip2)
     rsum5=var(5,ip1)+var(5,ip2)


     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1

     redg21=coef1*rsum2
     redg22=coef2*rsum2
     redg23=coef3*rsum2
     redg31=coef1*rsum3
     redg32=coef2*rsum3
     redg33=coef3*rsum3
     redg41=coef1*rsum4
     redg42=coef2*rsum4
     redg43=coef3*rsum4

     redg51=coef1*rsum5
     redg52=coef2*rsum5
     redg53=coef3*rsum5

     !
     !     Obtain the final gradients
     !

     grad(1,1,ip1)=grad(1,1,ip1)-redg11
     grad(2,1,ip1)=grad(2,1,ip1)-redg12
     grad(3,1,ip1)=grad(3,1,ip1)-redg13

     grad(1,2,ip1)=grad(1,2,ip1)-redg21
     grad(2,2,ip1)=grad(2,2,ip1)-redg22
     grad(3,2,ip1)=grad(3,2,ip1)-redg23
     grad(1,3,ip1)=grad(1,3,ip1)-redg31
     grad(2,3,ip1)=grad(2,3,ip1)-redg32
     grad(3,3,ip1)=grad(3,3,ip1)-redg33
     grad(1,4,ip1)=grad(1,4,ip1)-redg41
     grad(2,4,ip1)=grad(2,4,ip1)-redg42
     grad(3,4,ip1)=grad(3,4,ip1)-redg43

     grad(1,5,ip1)=grad(1,5,ip1)-redg51
     grad(2,5,ip1)=grad(2,5,ip1)-redg52
     grad(3,5,ip1)=grad(3,5,ip1)-redg53


     grad(1,1,ip2)=grad(1,1,ip2)+redg11
     grad(2,1,ip2)=grad(2,1,ip2)+redg12
     grad(3,1,ip2)=grad(3,1,ip2)+redg13

     grad(1,2,ip2)=grad(1,2,ip2)+redg21
     grad(2,2,ip2)=grad(2,2,ip2)+redg22
     grad(3,2,ip2)=grad(3,2,ip2)+redg23
     grad(1,3,ip2)=grad(1,3,ip2)+redg31
     grad(2,3,ip2)=grad(2,3,ip2)+redg32
     grad(3,3,ip2)=grad(3,3,ip2)+redg33
     grad(1,4,ip2)=grad(1,4,ip2)+redg41
     grad(2,4,ip2)=grad(2,4,ip2)+redg42
     grad(3,4,ip2)=grad(3,4,ip2)+redg43

     grad(1,5,ip2)=grad(1,5,ip2)+redg51
     grad(2,5,ip2)=grad(2,5,ip2)+redg52
     grad(3,5,ip2)=grad(3,5,ip2)+redg53

  enddo

  !
  !     Boundary edge contribution 
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedgb,ledgb,edshab,var,grad) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rsum1,rsum2,rsum3,rsum4,rsum5,&  
!$omp&                       redg11,redg12,redg13,redg21,redg22,redg23,redg31,redg32,redg33,& 
!$omp&                       redg41,redg42,redg43,redg51,redg52,redg53 )

  do iedge=1,nedgb

     ip1=ledgb(1,iedge)
     ip2=ledgb(2,iedge)

     coef1=edshab(1,iedge)
     coef2=edshab(2,iedge)
     coef3=edshab(3,iedge)

     rsum1=var(1,ip1)+var(1,ip2)
     rsum2=var(2,ip1)+var(2,ip2)
     rsum3=var(3,ip1)+var(3,ip2)
     rsum4=var(4,ip1)+var(4,ip2)
     rsum5=var(5,ip1)+var(5,ip2)


     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1

     redg21=coef1*rsum2
     redg22=coef2*rsum2
     redg23=coef3*rsum2
     redg31=coef1*rsum3
     redg32=coef2*rsum3
     redg33=coef3*rsum3
     redg41=coef1*rsum4
     redg42=coef2*rsum4
     redg43=coef3*rsum4

     redg51=coef1*rsum5
     redg52=coef2*rsum5
     redg53=coef3*rsum5

     !
     !     Obtain the final gradients
     !

     grad(1,1,ip1)=grad(1,1,ip1)+redg11
     grad(2,1,ip1)=grad(2,1,ip1)+redg12
     grad(3,1,ip1)=grad(3,1,ip1)+redg13

     grad(1,2,ip1)=grad(1,2,ip1)+redg21
     grad(2,2,ip1)=grad(2,2,ip1)+redg22
     grad(3,2,ip1)=grad(3,2,ip1)+redg23
     grad(1,3,ip1)=grad(1,3,ip1)+redg31
     grad(2,3,ip1)=grad(2,3,ip1)+redg32
     grad(3,3,ip1)=grad(3,3,ip1)+redg33
     grad(1,4,ip1)=grad(1,4,ip1)+redg41
     grad(2,4,ip1)=grad(2,4,ip1)+redg42
     grad(3,4,ip1)=grad(3,4,ip1)+redg43

     grad(1,5,ip1)=grad(1,5,ip1)+redg51
     grad(2,5,ip1)=grad(2,5,ip1)+redg52
     grad(3,5,ip1)=grad(3,5,ip1)+redg53


     grad(1,1,ip2)=grad(1,1,ip2)+redg11
     grad(2,1,ip2)=grad(2,1,ip2)+redg12
     grad(3,1,ip2)=grad(3,1,ip2)+redg13

     grad(1,2,ip2)=grad(1,2,ip2)+redg21
     grad(2,2,ip2)=grad(2,2,ip2)+redg22
     grad(3,2,ip2)=grad(3,2,ip2)+redg23
     grad(1,3,ip2)=grad(1,3,ip2)+redg31
     grad(2,3,ip2)=grad(2,3,ip2)+redg32
     grad(3,3,ip2)=grad(3,3,ip2)+redg33
     grad(1,4,ip2)=grad(1,4,ip2)+redg41
     grad(2,4,ip2)=grad(2,4,ip2)+redg42
     grad(3,4,ip2)=grad(3,4,ip2)+redg43

     grad(1,5,ip2)=grad(1,5,ip2)+redg51
     grad(2,5,ip2)=grad(2,5,ip2)+redg52
     grad(3,5,ip2)=grad(3,5,ip2)+redg53

  enddo

  !
  !     Boundary point contribution
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nboup,lboup,edshap,var,grad) &
!$omp& private(ipo,ipoin,coef1,coef2,coef3,rsum1,rsum2,rsum3,rsum4,rsum5,&  
!$omp&                       redg11,redg12,redg13,redg21,redg22,redg23,redg31,redg32,redg33,& 
!$omp&                       redg41,redg42,redg43,redg51,redg52,redg53 )
 
  do ipo=1,nboup

     ipoin=lboup(1,ipo)

     coef1=edshap(1,ipo)
     coef2=edshap(2,ipo)
     coef3=edshap(3,ipo)


     rsum1=var(1,ipoin)
     rsum2=var(2,ipoin)
     rsum3=var(3,ipoin)
     rsum4=var(4,ipoin)
     rsum5=var(5,ipoin)


     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1

     redg21=coef1*rsum2
     redg22=coef2*rsum2
     redg23=coef3*rsum2
     redg31=coef1*rsum3
     redg32=coef2*rsum3
     redg33=coef3*rsum3
     redg41=coef1*rsum4
     redg42=coef2*rsum4
     redg43=coef3*rsum4

     redg51=coef1*rsum5
     redg52=coef2*rsum5
     redg53=coef3*rsum5

     !
     !     Obtain the final gradients
     !

     grad(1,1,ipoin)=grad(1,1,ipoin)-redg11
     grad(2,1,ipoin)=grad(2,1,ipoin)-redg12
     grad(3,1,ipoin)=grad(3,1,ipoin)-redg13

     grad(1,2,ipoin)=grad(1,2,ipoin)-redg21
     grad(2,2,ipoin)=grad(2,2,ipoin)-redg22
     grad(3,2,ipoin)=grad(3,2,ipoin)-redg23
     grad(1,3,ipoin)=grad(1,3,ipoin)-redg31
     grad(2,3,ipoin)=grad(2,3,ipoin)-redg32
     grad(3,3,ipoin)=grad(3,3,ipoin)-redg33
     grad(1,4,ipoin)=grad(1,4,ipoin)-redg41
     grad(2,4,ipoin)=grad(2,4,ipoin)-redg42
     grad(3,4,ipoin)=grad(3,4,ipoin)-redg43

     grad(1,5,ipoin)=grad(1,5,ipoin)-redg51
     grad(2,5,ipoin)=grad(2,5,ipoin)-redg52
     grad(3,5,ipoin)=grad(3,5,ipoin)-redg53

  enddo

  !
  !     Multiply by the inverse of the lumped mass matrix
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,rmass,grad) &
!$omp& private(ipoin,rtemp)

  do ipoin=1,npoin
     rtemp=rmass(ipoin)
     grad(1,1,ipoin)=rtemp*grad(1,1,ipoin)
     grad(2,1,ipoin)=rtemp*grad(2,1,ipoin)
     grad(3,1,ipoin)=rtemp*grad(3,1,ipoin)
     grad(1,2,ipoin)=rtemp*grad(1,2,ipoin)
     grad(2,2,ipoin)=rtemp*grad(2,2,ipoin)
     grad(3,2,ipoin)=rtemp*grad(3,2,ipoin)
     grad(1,3,ipoin)=rtemp*grad(1,3,ipoin)
     grad(2,3,ipoin)=rtemp*grad(2,3,ipoin)
     grad(3,3,ipoin)=rtemp*grad(3,3,ipoin)
     grad(1,4,ipoin)=rtemp*grad(1,4,ipoin)
     grad(2,4,ipoin)=rtemp*grad(2,4,ipoin)
     grad(3,4,ipoin)=rtemp*grad(3,4,ipoin)
     grad(1,5,ipoin)=rtemp*grad(1,5,ipoin)
     grad(2,5,ipoin)=rtemp*grad(2,5,ipoin)
     grad(3,5,ipoin)=rtemp*grad(3,5,ipoin)
  enddo




end subroutine gtgrad

subroutine gtgradp(nedge,ledge,nvar,ndim,var,grad,edsha,npoin,ledgb,nedgb,edshab,edshap,nboup,lboup,rmass,mboup)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nedge,nvar,ndim,npoin,nedgb,nboup,mboup
  integer(ip),intent(in)  :: ledge(2,nedge),ledgb(2,nedgb),lboup(6,mboup)
  real(rp),intent(in)     :: var(nvar,npoin),edsha(5,nedge),edshab(3,nedgb),edshap(3,nboup),rmass(npoin)
  real(rp),intent(inout)  :: grad(ndim,6,npoin)
  real(rp)                :: rsum1,rsum2,rsum3,rsum4,rsum5,rtemp
  real(rp)                :: redg11,redg12,redg13,redg21,redg22,redg23
  real(rp)                :: redg31,redg32,redg33,redg41,redg42,redg43
  real(rp)                :: redg51,redg52,redg53,c00,coef1,coef2,coef3
  integer(ip)             :: ipoin,iedge,ip1,ip2,ipo

  c00=0.0d+00
  !
  !     Put grad to zero
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,grad,c00) &
!$omp& private(ipoin)
  do ipoin=1,npoin
     grad(1,6,ipoin)=c00
     grad(2,6,ipoin)=c00
     grad(3,6,ipoin)=c00
  enddo
  !
  !     Compute the projection of the gradient of the unknowns
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,var,grad) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rsum1,& 
!$omp&                       redg11,redg12,redg13 )
  do iedge=1,nedge

     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)

     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)

     rsum1=var(6,ip1)+var(6,ip2)

     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1

     !
     !     Obtain the final gradients
     !

     grad(1,6,ip1)=grad(1,6,ip1)-redg11
     grad(2,6,ip1)=grad(2,6,ip1)-redg12
     grad(3,6,ip1)=grad(3,6,ip1)-redg13

     grad(1,6,ip2)=grad(1,6,ip2)+redg11
     grad(2,6,ip2)=grad(2,6,ip2)+redg12
     grad(3,6,ip2)=grad(3,6,ip2)+redg13

  enddo

  !
  !     Boundary edge contribution 
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedgb,ledgb,edshab,var,grad) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rsum1,& 
!$omp&                       redg11,redg12,redg13 )

  do iedge=1,nedgb

     ip1=ledgb(1,iedge)
     ip2=ledgb(2,iedge)

     coef1=edshab(1,iedge)
     coef2=edshab(2,iedge)
     coef3=edshab(3,iedge)

     rsum1=var(6,ip1)+var(6,ip2)

     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1


     !
     !     Obtain the final gradients
     !

     grad(1,6,ip1)=grad(1,6,ip1)+redg11
     grad(2,6,ip1)=grad(2,6,ip1)+redg12
     grad(3,6,ip1)=grad(3,6,ip1)+redg13

     grad(1,6,ip2)=grad(1,6,ip2)+redg11
     grad(2,6,ip2)=grad(2,6,ip2)+redg12
     grad(3,6,ip2)=grad(3,6,ip2)+redg13

  enddo

  !
  !     Boundary point contribution
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nboup,lboup,edshap,var,grad) &
!$omp& private(ipo,ipoin,coef1,coef2,coef3,rsum1,& 
!$omp&                       redg11,redg12,redg13 )
  do ipo=1,nboup

     ipoin=lboup(1,ipo)

     coef1=edshap(1,ipo)
     coef2=edshap(2,ipo)
     coef3=edshap(3,ipo)


     rsum1=var(6,ipoin)

     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1

     !
     !     Obtain the final gradients
     !

     grad(1,6,ipoin)=grad(1,6,ipoin)-redg11
     grad(2,6,ipoin)=grad(2,6,ipoin)-redg12
     grad(3,6,ipoin)=grad(3,6,ipoin)-redg13

  enddo

  !
  !     Multiply by the inverse of the lumped mass matrix
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,rmass,grad) &
!$omp& private(ipoin,rtemp)
  do ipoin=1,npoin
     rtemp=rmass(ipoin)
     grad(1,6,ipoin)=rtemp*grad(1,6,ipoin)
     grad(2,6,ipoin)=rtemp*grad(2,6,ipoin)
     grad(3,6,ipoin)=rtemp*grad(3,6,ipoin)
  enddo


end subroutine gtgradp

subroutine valimi(nedge,ledge,nvar,ndim,npoin,var,coor,extr1,extr2,grad)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nedge,nvar,ndim,npoin
  integer(ip),intent(in)  :: ledge(2,nedge)
  real(rp),intent(in)     :: var(nvar,npoin),coor(ndim,npoin),grad(3,6,npoin)
  real(rp),intent(inout)  :: extr1(nvar,nedge),extr2(nvar,nedge)
  real(rp)                :: c20,k,c00,dirx,diry,dirz,c10,c14 
  real(rp)                :: rho1,rhou1,rhov1,rhow1,rhoe1
  real(rp)                :: rho2,rhou2,rhov2,rhow2,rhoe2
  real(rp)                :: drho,drhou,drhov,drhow,drhoe
  real(rp)                :: bgra1,bgra2,bgra3,bgra4,bgra5 
  real(rp)                :: fgra1,fgra2,fgra3,fgra4,fgra5 
  real(rp)                :: s11,s12,s13,s14,s15,s21,s22,s23,s24,s25 
  real(rp)                :: epsil
  integer(ip)             :: iedge,ip1,ip2

  !
  !     This sub extrapolates the unknown in the MUSCL approach with 
  !     Van Albada limiters for conservative variables
  !

  !
  !     Define extrapolation order
  !
  k=0.33333d+00
  c20=2.0d+00
  c00=0.0d+00
  c10=1.0d+00
  c14=1.0d+00/4.0d+00
  epsil=1.0d-18

  do iedge=1,nedge

     !
     !     Points of the edge
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Edge direction
     !
     dirx=coor(1,ip2)-coor(1,ip1)
     diry=coor(2,ip2)-coor(2,ip1)
     dirz=coor(3,ip2)-coor(3,ip1)
     !
     !     Get relevant quantities 
     !
     rho1=var(1,ip1)
     rhou1=var(2,ip1)
     rhov1=var(3,ip1)
     rhow1=var(4,ip1)
     rhoe1=var(5,ip1)


     rho2=var(1,ip2)
     rhou2=var(2,ip2)
     rhov2=var(3,ip2)
     rhow2=var(4,ip2)
     rhoe2=var(5,ip2)

     !
     !     Delta edge
     !
     drho=rho2-rho1
     drhou=rhou2-rhou1
     drhov=rhov2-rhov1
     drhow=rhow2-rhow1
     drhoe=rhoe2-rhoe1


     !
     !     Backward gradient at ip1
     !

     bgra1=c20*(grad(1,1,ip1)*dirx+grad(2,1,ip1)*diry+grad(3,1,ip1)*dirz)-drho
     bgra2=c20*(grad(1,2,ip1)*dirx+grad(2,2,ip1)*diry+grad(3,2,ip1)*dirz)-drhou
     bgra3=c20*(grad(1,3,ip1)*dirx+grad(2,3,ip1)*diry+grad(3,3,ip1)*dirz)-drhov
     bgra4=c20*(grad(1,4,ip1)*dirx+grad(2,4,ip1)*diry+grad(3,4,ip1)*dirz)-drhow
     bgra5=c20*(grad(1,5,ip1)*dirx+grad(2,5,ip1)*diry+grad(3,5,ip1)*dirz)-drhoe


     !
     !     Forward gradient at ip2
     !

     fgra1=c20*(grad(1,1,ip2)*dirx+grad(2,1,ip2)*diry+grad(3,1,ip2)*dirz)-drho
     fgra2=c20*(grad(1,2,ip2)*dirx+grad(2,2,ip2)*diry+grad(3,2,ip2)*dirz)-drhou
     fgra3=c20*(grad(1,3,ip2)*dirx+grad(2,3,ip2)*diry+grad(3,3,ip2)*dirz)-drhov
     fgra4=c20*(grad(1,4,ip2)*dirx+grad(2,4,ip2)*diry+grad(3,4,ip2)*dirz)-drhow
     fgra5=c20*(grad(1,5,ip2)*dirx+grad(2,5,ip2)*diry+grad(3,5,ip2)*dirz)-drhoe

     !
     !     Get the Van Albada slope limiter 
     !
     s11=max(c00,(c20*bgra1*drho)/(bgra1*bgra1+drho*drho+epsil))
     s12=max(c00,(c20*bgra2*drhou)/(bgra2*bgra2+drhou*drhou+epsil))
     s13=max(c00,(c20*bgra3*drhov)/(bgra3*bgra3+drhov*drhov+epsil))
     s14=max(c00,(c20*bgra4*drhow)/(bgra4*bgra4+drhow*drhow+epsil))
     s15=max(c00,(c20*bgra5*drhoe)/(bgra5*bgra5+drhoe*drhoe+epsil))

     s21=max(c00,(c20*fgra1*drho)/(fgra1*fgra1+drho*drho+epsil))
     s22=max(c00,(c20*fgra2*drhou)/(fgra2*fgra2+drhou*drhou+epsil))
     s23=max(c00,(c20*fgra3*drhov)/(fgra3*fgra3+drhov*drhov+epsil))
     s24=max(c00,(c20*fgra4*drhow)/(fgra4*fgra4+drhow*drhow+epsil))
     s25=max(c00,(c20*fgra5*drhoe)/(fgra5*fgra5+drhoe*drhoe+epsil))

     !
     !     Finally compute the limited extrapolated values
     !

     extr1(1,iedge)=rho1+c14*s11*((c10-k*s11)*bgra1+(c10+k*s11)*drho)
     extr1(2,iedge)=rhou1+c14*s12*((c10-k*s12)*bgra2+(c10+k*s12)*drhou)
     extr1(3,iedge)=rhov1+c14*s13*((c10-k*s13)*bgra3+(c10+k*s13)*drhov)
     extr1(4,iedge)=rhow1+c14*s14*((c10-k*s14)*bgra4+(c10+k*s14)*drhow)
     extr1(5,iedge)=rhoe1+c14*s15*((c10-k*s15)*bgra5+(c10+k*s15)*drhoe)

     extr2(1,iedge)=rho2-c14*s21*((c10-k*s21)*fgra1+(c10+k*s21)*drho)
     extr2(2,iedge)=rhou2-c14*s22*((c10-k*s22)*fgra2+(c10+k*s22)*drhou)
     extr2(3,iedge)=rhov2-c14*s23*((c10-k*s23)*fgra3+(c10+k*s23)*drhov)
     extr2(4,iedge)=rhow2-c14*s24*((c10-k*s24)*fgra4+(c10+k*s24)*drhow)
     extr2(5,iedge)=rhoe2-c14*s25*((c10-k*s25)*fgra5+(c10+k*s25)*drhoe)



  enddo

end subroutine valimi

subroutine valimi2(nedge,ledge,nvar,ndim,npoin,var,coor,extr1,extr2,grad,gam)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nedge,nvar,ndim,npoin
  integer(ip),intent(in)  :: ledge(2,nedge)
  real(rp),intent(in)     :: var(nvar,npoin),coor(ndim,npoin),grad(3,6,npoin),gam
  real(rp),intent(inout)  :: extr1(nvar,nedge),extr2(nvar,nedge)
  real(rp)                :: c20,k,c00,dirx,diry,dirz,c10,c14,c05 
  real(rp)                :: rho1,rhou1,rhov1,rhow1,rhoe1
  real(rp)                :: u1,v1,w1,pres1,rhoi1
  real(rp)                :: rho2,rhou2,rhov2,rhow2,rhoe2
  real(rp)                :: u2,v2,w2,pres2,rhoi2
  real(rp)                :: drho,du,dv,dw,dpres,gam1,gam1i
  real(rp)                :: bgra1,bgra2,bgra3,bgra4,bgra5 
  real(rp)                :: fgra1,fgra2,fgra3,fgra4,fgra5 
  real(rp)                :: s11,s12,s13,s14,s15,s21,s22,s23,s24,s25 
  real(rp)                :: rex1,uex1,vex1,wex1,pex1 
  real(rp)                :: rex2,uex2,vex2,wex2,pex2 
  real(rp)                :: epsil
  integer(ip)             :: iedge,ip1,ip2

  !
  !     This sub extrapolates the unknown in the MUSCL approach with 
  !     Van Albada limiters for primitive variables 
  !     We assume that the given gradients are the gradients of the primitive variables
  
  !
  !     Define extrapolation order
  !
  k=0.33333d+00
  c20=2.0d+00
  c00=0.0d+00
  c05=0.5d+00
  c10=1.0d+00
  c14=1.0d+00/4.0d+00
  epsil=1.0d-18
  gam1=gam-c10
  gam1i=c10/gam1

  do iedge=1,nedge

     !
     !     Points of the edge
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Edge direction
     !
     dirx=coor(1,ip2)-coor(1,ip1)
     diry=coor(2,ip2)-coor(2,ip1)
     dirz=coor(3,ip2)-coor(3,ip1)
     !
     !     Get relevant quantities 
     !
     rho1=var(1,ip1)
     rhou1=var(2,ip1)
     rhov1=var(3,ip1)
     rhow1=var(4,ip1)
     rhoe1=var(5,ip1)
     rhoi1=c10/rho1
     u1=rhoi1*rhou1
     v1=rhoi1*rhov1
     w1=rhoi1*rhow1
     pres1=gam1*(rhoe1-c05*rho1*(u1*u1+v1*v1+w1*w1))

     rho2=var(1,ip2)
     rhou2=var(2,ip2)
     rhov2=var(3,ip2)
     rhow2=var(4,ip2)
     rhoe2=var(5,ip2)
     rhoi2=c10/rho2
     u2=rhoi2*rhou2
     v2=rhoi2*rhov2
     w2=rhoi2*rhow2
     pres2=gam1*(rhoe2-c05*rho2*(u2*u2+v2*v2+w2*w2))

     !
     !     Delta edge
     !
     drho=rho2-rho1
     du=u2-u1
     dv=v2-v1
     dw=w2-w1
     dpres=pres2-pres1

     !
     !     Backward gradient at ip1
     !

     bgra1=c20*(grad(1,1,ip1)*dirx+grad(2,1,ip1)*diry+grad(3,1,ip1)*dirz)-drho
     bgra2=c20*(grad(1,2,ip1)*dirx+grad(2,2,ip1)*diry+grad(3,2,ip1)*dirz)-du
     bgra3=c20*(grad(1,3,ip1)*dirx+grad(2,3,ip1)*diry+grad(3,3,ip1)*dirz)-dv
     bgra4=c20*(grad(1,4,ip1)*dirx+grad(2,4,ip1)*diry+grad(3,4,ip1)*dirz)-dw
     bgra5=c20*(grad(1,5,ip1)*dirx+grad(2,5,ip1)*diry+grad(3,5,ip1)*dirz)-dpres

     !
     !     Forward gradient at ip2
     !

     fgra1=c20*(grad(1,1,ip2)*dirx+grad(2,1,ip2)*diry+grad(3,1,ip2)*dirz)-drho
     fgra2=c20*(grad(1,2,ip2)*dirx+grad(2,2,ip2)*diry+grad(3,2,ip2)*dirz)-du
     fgra3=c20*(grad(1,3,ip2)*dirx+grad(2,3,ip2)*diry+grad(3,3,ip2)*dirz)-dv
     fgra4=c20*(grad(1,4,ip2)*dirx+grad(2,4,ip2)*diry+grad(3,4,ip2)*dirz)-dw
     fgra5=c20*(grad(1,5,ip2)*dirx+grad(2,5,ip2)*diry+grad(3,5,ip2)*dirz)-dpres

     !
     !     Get the Van Albada slope limiter 
     !
     s11=max(c00,(c20*bgra1*drho)/(bgra1*bgra1+drho*drho+epsil))
     s12=max(c00,(c20*bgra2*du)/(bgra2*bgra2+du*du+epsil))
     s13=max(c00,(c20*bgra3*dv)/(bgra3*bgra3+dv*dv+epsil))
     s14=max(c00,(c20*bgra4*dw)/(bgra4*bgra4+dw*dw+epsil))
     s15=max(c00,(c20*bgra5*dpres)/(bgra5*bgra5+dpres*dpres+epsil))

     s21=max(c00,(c20*fgra1*drho)/(fgra1*fgra1+drho*drho+epsil))
     s22=max(c00,(c20*fgra2*du)/(fgra2*fgra2+du*du+epsil))
     s23=max(c00,(c20*fgra3*dv)/(fgra3*fgra3+dv*dv+epsil))
     s24=max(c00,(c20*fgra4*dw)/(fgra4*fgra4+dw*dw+epsil))
     s25=max(c00,(c20*fgra5*dpres)/(fgra5*fgra5+dpres*dpres+epsil))

     !
     !     Finally compute the limited extrapolated values
     !

     rex1=rho1+c14*s11*((c10-k*s11)*bgra1+(c10+k*s11)*drho)
     uex1=u1+c14*s12*((c10-k*s12)*bgra2+(c10+k*s12)*du)
     vex1=v1+c14*s13*((c10-k*s13)*bgra3+(c10+k*s13)*dv)
     wex1=w1+c14*s14*((c10-k*s14)*bgra4+(c10+k*s14)*dw)
     pex1=pres1+c14*s15*((c10-k*s15)*bgra5+(c10+k*s15)*dpres)

     rex2=rho2-c14*s21*((c10-k*s21)*fgra1+(c10+k*s21)*drho)
     uex2=u2-c14*s22*((c10-k*s22)*fgra2+(c10+k*s22)*du)
     vex2=v2-c14*s23*((c10-k*s23)*fgra3+(c10+k*s23)*dv)
     wex2=w2-c14*s24*((c10-k*s24)*fgra4+(c10+k*s24)*dw)
     pex2=pres2-c14*s25*((c10-k*s25)*fgra5+(c10+k*s25)*dpres)
 
     !
     !     And switch back to conserved variables
     !
     extr1(1,iedge)=rex1
     extr1(2,iedge)=rex1*uex1
     extr1(3,iedge)=rex1*vex1
     extr1(4,iedge)=rex1*wex1
     extr1(5,iedge)=pex1*gam1i+c05*rex1*(uex1*uex1+vex1*vex1+wex1*wex1)
     extr1(6,iedge)=pex1
     extr1(7,iedge)=c10/rex1

     extr2(1,iedge)=rex2
     extr2(2,iedge)=rex2*uex2
     extr2(3,iedge)=rex2*vex2
     extr2(4,iedge)=rex2*wex2
     extr2(5,iedge)=pex2*gam1i+c05*rex2*(uex2*uex2+vex2*vex2+wex2*wex2)
     extr2(6,iedge)=pex2
     extr2(7,iedge)=c10/rex2

  enddo

end subroutine valimi2


subroutine galflu(nedge,ledge,nvar,ndim,edsha,extr1,extr2,flu,npoin,nflu)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nedge,nvar,ndim,npoin,nflu
  integer(ip),intent(in)  :: ledge(2,nedge)
  real(rp),intent(in)     :: edsha(5,nedge),extr1(nvar,npoin),extr2(nvar,npoin)
  real(rp),intent(inout)  :: flu(nflu,npoin)
  real(rp)                :: c14,coef1,coef2,coef3,press,radv,redg1,redg2,redg3,redg4,redg5 
  integer(ip)             :: iedge,ip1,ip2

  c14=0.25d+00


  do iedge=1,nedge
     !
     !     Points of the edge 
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Geometric coefficients
     !
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)
     !
     !     Compute advection and pressure
     !
     press=extr1(6,iedge)+extr2(6,iedge)
     radv=c14*(coef1*(extr1(2,iedge)+extr2(2,iedge))+coef2*(extr1(3,iedge)+extr2(3,iedge))+coef3*(extr1(4,iedge)+extr2(4,iedge)))*(extr1(7,iedge)+extr2(7,iedge))

     !
     !     Euler Galerkin Fluxes
     !
     redg1= radv*(extr1(1,iedge)+extr2(1,iedge)) 
     redg2= radv*(extr1(2,iedge)+extr2(2,iedge))+coef1*press
     redg3= radv*(extr1(3,iedge)+extr2(3,iedge))+coef2*press 
     redg4= radv*(extr1(4,iedge)+extr2(4,iedge))+coef3*press 
     redg5= radv*(extr1(5,iedge)+extr2(5,iedge)+press) 
     !
     !     Add to the flux points
     !
     flu(1,ip1)=flu(1,ip1)+redg1
     flu(2,ip1)=flu(2,ip1)+redg2
     flu(3,ip1)=flu(3,ip1)+redg3
     flu(4,ip1)=flu(4,ip1)+redg4
     flu(5,ip1)=flu(5,ip1)+redg5

     flu(1,ip2)=flu(1,ip2)-redg1
     flu(2,ip2)=flu(2,ip2)-redg2
     flu(3,ip2)=flu(3,ip2)-redg3
     flu(4,ip2)=flu(4,ip2)-redg4
     flu(5,ip2)=flu(5,ip2)-redg5

  enddo


end subroutine galflu



subroutine galflub(nedgb,ledgb,nvar,ndim,edshab,var,flu,npoin,nflu)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nedgb,nvar,ndim,npoin,nflu
  integer(ip),intent(in)  :: ledgb(2,nedgb)
  real(rp),intent(in)     :: edshab(3,nedgb),var(nvar,npoin)
  real(rp),intent(inout)  :: flu(nflu,npoin)
  real(rp)                :: c14,coef1,coef2,coef3,press,radv,redg1,redg2,redg3,redg4,redg5 
  integer(ip)             :: iedge,ip1,ip2

  c14=0.25d+00

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedgb,ledgb,edshab,var,flu,c14) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,press,radv,& 
!$omp&                       redg1,redg2,redg3,redg4,redg5 )
 
  do iedge=1,nedgb
     !
     !     Points of the edge 
     !
     ip1=ledgb(1,iedge)
     ip2=ledgb(2,iedge)
     !
     !     Geometric coefficients
     !
     coef1=edshab(1,iedge)
     coef2=edshab(2,iedge)
     coef3=edshab(3,iedge)
     !
     !     Compute advection and pressure
     !
     press=var(6,ip1)+var(6,ip2)
     radv=c14*(coef1*(var(2,ip1)+var(2,ip1))+coef2*(var(3,ip1)+var(3,ip2))+coef3*(var(4,ip1)+var(4,ip2)))*(var(7,ip1)+var(7,ip2))

     !
     !     Euler Galerkin Fluxes
     !
     redg1= radv*(var(1,ip1)+var(1,ip2)) 
     redg2= radv*(var(2,ip1)+var(2,ip2))+coef1*press
     redg3= radv*(var(3,ip1)+var(3,ip2))+coef2*press 
     redg4= radv*(var(4,ip1)+var(4,ip2))+coef3*press 
     redg5= radv*(var(5,ip1)+var(5,ip2)+press) 
     !
     !     Add to the flux points
     !
     flu(1,ip1)=flu(1,ip1)-redg1
     flu(2,ip1)=flu(2,ip1)-redg2
     flu(3,ip1)=flu(3,ip1)-redg3
     flu(4,ip1)=flu(4,ip1)-redg4
     flu(5,ip1)=flu(5,ip1)-redg5

     flu(1,ip2)=flu(1,ip2)-redg1
     flu(2,ip2)=flu(2,ip2)-redg2
     flu(3,ip2)=flu(3,ip2)-redg3
     flu(4,ip2)=flu(4,ip2)-redg4
     flu(5,ip2)=flu(5,ip2)-redg5

  enddo


end subroutine galflub


subroutine galflup(nboup,nvar,ndim,edshap,var,flu,npoin,lboup,nflu,mboup)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nboup,nvar,ndim,npoin,mboup,nflu
  integer(ip),intent(in)  :: lboup(6,mboup)
  real(rp),intent(in)     :: edshap(3,nboup),var(nvar,npoin)
  real(rp),intent(inout)  :: flu(nflu,npoin)
  real(rp)                :: coef1,coef2,coef3,press,radv,redg1,redg2,redg3,redg4,redg5 
  integer(ip)             :: ipoin,ipo


!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nboup,lboup,edshap,var,flu) &
!$omp& private(ipo,ipoin,coef1,coef2,coef3,press,radv,& 
!$omp&                       redg1,redg2,redg3,redg4,redg5 )

  do ipo=1,nboup
     ipoin=lboup(1,ipo)
     !
     !     Geometric coefficients
     !
     coef1=edshap(1,ipo)
     coef2=edshap(2,ipo)
     coef3=edshap(3,ipo)
     !
     !     Compute advection and pressure
     !
     press=var(6,ipoin)
     radv=(coef1*var(2,ipoin)+coef2*var(3,ipoin)+coef3*var(4,ipoin))*var(7,ipoin)
     !
     !     Euler Galerkin Fluxes
     !
     redg1= radv*var(1,ipoin) 
     redg2= radv*var(2,ipoin)+coef1*press
     redg3= radv*var(3,ipoin)+coef2*press 
     redg4= radv*var(4,ipoin)+coef3*press 
     redg5= radv*(var(5,ipoin)+press) 
     !
     !     Add to the flux points
     !
     flu(1,ipoin)=flu(1,ipoin)+redg1
     flu(2,ipoin)=flu(2,ipoin)+redg2
     flu(3,ipoin)=flu(3,ipoin)+redg3
     flu(4,ipoin)=flu(4,ipoin)+redg4
     flu(5,ipoin)=flu(5,ipoin)+redg5


  enddo


end subroutine galflup


subroutine roeflu(nedge,ledge,npoin,edsha,flu,gam,extr1,extr2,nvar,nflu,coor,ndim)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nedge,npoin,nvar,nflu,ndim
  integer(ip), intent(in)    :: ledge(2,nedge)
  real(rp), intent(in)       :: edsha(5,nedge),gam,extr1(nvar,nedge),extr2(nvar,nedge)
  real(rp),intent(inout)     :: flu(nflu,npoin) 
  real(rp),intent(in)     :: coor(ndim,npoin)
  real(rp)     :: srhor,srhol,det,deti,rhot,ut,vt,wt,Ht
  real(rp)     :: rnl,rnli,coef1,coef2,coef3,rho1,rhou1,rhov1,rhow1,rhoe1,rho2
  real(rp)     :: rhou2,rhov2,rhow2,rhoe2,pres1,pres2,rhoi1,rhoi2
  real(rp)     :: dirx,diry,dirz,rhor,vr,ur,wr,Hr,rhol,ul,vl,wl,Hl 
  real(rp)     :: at,c10,c05,gam1,c00,c20,cm2 
  real(rp)     :: redg1,redg2,redg3,redg4,redg5,vedg 
  real(rp)     :: lambda1,lambda2,lambda3,lambda4,lambda5 
  real(rp)     :: vt2,upw1,upw2,upw3,upw4,upw5,radv
  real(rp)     :: K1(5),K2(5),K3(5),K4(5),K5(5),du,dv,dw
  real(rp)     :: a1,a2,a3,a4,a5,at2,pres,drho,drhou,drhov,drhow,drhoe,dpres
  real(rp)     :: c1,c2,c12 
  real(rp)     :: l1,l2,l3,lx,ly,lz,ll,radv1,radv2
  real(rp)     :: eval,snew,ak,cflm,sig,revec(5),u1,u2,umm,cmm,sml,smr,smt
  real(rp)     :: f1,f2,f3,f4,f5,df1,df2,df3,df4,df5,dc1,dc2,dc3,dc4,dc5,dq
  real(rp)     :: sw1,sw2,sw3,lambda11,lambda12,lambda13,lambda21,lambda22,lambda23 
  real(rp)     :: delu,rhodu,m,ct,delp
  integer(ip)  :: iedge,ip1,ip2 

  c00=0.0d+00
  c10=1.0d+00
  c12=1.0d+00/2.0d+00
  c20=2.0d+00
  cm2=-2.0d+00
  c05=0.5d+00
  gam1=gam-c10

  do iedge=1,nedge

     !
     !   Edge point
     !  
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !    Edge coef
     !
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)
     !1D DBG
     !coef1=edsha(2,iedge)
     !coef2=edsha(3,iedge)
     !coef3=edsha(4,iedge)
     rnl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3) 
     rnli=c10/rnl

     dirx=coef1*rnli
     diry=coef2*rnli
     dirz=coef3*rnli


     lx=coor(1,ip2)-coor(1,ip1)
     ly=coor(2,ip2)-coor(2,ip1)
     lz=coor(3,ip2)-coor(3,ip1)
     ll=sqrt(lx*lx+ly*ly+lz*lz)

     !
     !     Get relevant quantities 
     !
     rho1=extr1(1,iedge)
     rhou1=extr1(2,iedge)
     rhov1=extr1(3,iedge)
     rhow1=extr1(4,iedge)
     rhoe1=extr1(5,iedge)
     pres1=extr1(6,iedge)
     rhoi1=extr1(7,iedge)
     c1=sqrt(gam*pres1*rhoi1)

     rho2=extr2(1,iedge)
     rhou2=extr2(2,iedge)
     rhov2=extr2(3,iedge)
     rhow2=extr2(4,iedge)
     rhoe2=extr2(5,iedge)
     pres2=extr2(6,iedge)
     rhoi2=extr2(7,iedge)
     c2=sqrt(gam*pres2*rhoi2)

     !
     !     Compute right and left velocities and enthalpies
     !
     rhol=rho1
     ul=rhou1*rhoi1
     vl=rhov1*rhoi1
     wl=rhow1*rhoi1
     Hl=(rhoe1+pres1)*rhoi1
     u1=ul*dirx+vl*diry+wl*dirz

     rhor=rho2
     ur=rhou2*rhoi2
     vr=rhov2*rhoi2
     wr=rhow2*rhoi2
     Hr=(rhoe2+pres2)*rhoi2
     u2=ur*dirx+vr*diry+wr*dirz
     !
     !     Compute Roe average
     !
     srhor=sqrt(rhor)
     srhol=sqrt(rhol)
     det=srhor+srhol
     deti=c10/det

     rhot=srhol*srhor
     ut=(srhol*ul+srhor*ur)*deti
     vt=(srhol*vl+srhor*vr)*deti
     wt=(srhol*wl+srhor*wr)*deti
     Ht=(srhol*Hl+srhor*Hr)*deti
     vt2=c05*(ut*ut+vt*vt+wt*wt)
     at=sqrt(gam1*(Ht-vt2))

     radv1=u1*rnl
     radv2=u2*rnl
     pres=(pres1+pres2)*rnl
     !
     !     Euler Galerkin Fluxes
     !
     f1= radv1*rho1   +radv2*rho2               
     f2= radv1*rhou1  +radv2*rhou2    +dirx*pres 
     f3= radv1*rhov1  +radv2*rhov2    +diry*pres 
     f4= radv1*rhow1  +radv2*rhow2    +dirz*pres  
     f5= radv1*(rhoe1+pres1)+radv2*(rhoe2+pres2)      
     !
     !     Change the direction
     !
     !dirx=-dirx
     !diry=-diry
     !dirz=-dirz
     vedg=ut*dirx+vt*diry+wt*dirz 
     !u1=-u1
     !u2=-u2
     !
     !     Increments
     !
     drho=rhor-rhol
     drhou=rhou2-rhou1
     drhov=rhov2-rhov1
     drhow=rhow2-rhow1
     drhoe=rhoe2-rhoe1
     dpres=pres2-pres1
     !
     !     Eigenvalues
     !
     lambda1=abs(vedg-at) 
     lambda2=abs(vedg)
     lambda3=abs(vedg+at)
     !
     !     Entropy fix for transonic expansion and scale
     !   
     lambda11=u1-c1
     lambda12=u1
     lambda13=u1+c1
     lambda21=u2-c2
     lambda22=u2
     lambda23=u2+c2

     sw1=(c05-sign(c05,lambda11))*(c05+sign(c05,lambda21))
     sw2=(c05-sign(c05,lambda12))*(c05+sign(c05,lambda22))
     sw3=(c05-sign(c05,lambda13))*(c05+sign(c05,lambda23))

     l1=rnl*(lambda1*(c10-sw1)+c05*(lambda21-lambda11)*sw1) 
     l2=rnl*(lambda2*(c10-sw2)+c05*(lambda22-lambda12)*sw2)
     l3=rnl*(lambda3*(c10-sw3)+c05*(lambda23-lambda13)*sw3)
     !
     !     Upwind dissipation
     !
     !     See Weiss Smith AIAAJ
     !  

     m=(-l3+l1)*c12/at
     ct=(l3+l1)*c12
     rhodu=(drho*vedg-(dirx*drhou+diry*drhov+dirz*drhow))
     delu=m*rhodu+(ct-l2)*dpres/(at*at)
     delp=m*dpres+(ct-l2)*rhodu 

     df1=l2*drho   +delu 
     df2=l2*drhou  +delu*ut  -delp*dirx
     df3=l2*drhov  +delu*vt  -delp*diry
     df4=l2*drhow  +delu*wt  -delp*dirz
     df5=l2*drhoe  +delu*Ht  -delp*vedg     
    
     !
     !     Final form
     !
     redg1=f1+df1
     redg2=f2+df2
     redg3=f3+df3
     redg4=f4+df4
     redg5=f5+df5
     !
     !     Send to points
     !

     flu(1,ip1)=flu(1,ip1)+redg1
     flu(2,ip1)=flu(2,ip1)+redg2
     flu(3,ip1)=flu(3,ip1)+redg3
     flu(4,ip1)=flu(4,ip1)+redg4
     flu(5,ip1)=flu(5,ip1)+redg5

     flu(1,ip2)=flu(1,ip2)-redg1
     flu(2,ip2)=flu(2,ip2)-redg2
     flu(3,ip2)=flu(3,ip2)-redg3
     flu(4,ip2)=flu(4,ip2)-redg4
     flu(5,ip2)=flu(5,ip2)-redg5


  enddo


end subroutine roeflu


subroutine starvals( sig, rho, u, rhoe, c, vedg, at, Ht, umm, cmm,gam)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  real(rp),intent(in)        :: sig,rho,u,rhoe,c,vedg,at,Ht,gam 
  real(rp),intent(inout)     :: umm,cmm
  real(rp)                   :: gam1,c10,c05,dmk,pm

  c10=1.0d+00
  c05=0.5d+00
  gam1=gam-c10


  dmk = rho+ sig*c
  umm = (rho*u  + sig*c*(vedg - sig*at))/dmk
  pm  = gam1*(rhoe + sig*c*(Ht - sig*u*c) - c05*dmk*umm*umm)
  cmm = sqrt(gam*pm/dmk)


end subroutine starvals





subroutine applbc(dvar,lboup,nboup,varn,rdir,ndim,nvar,npoin,nflu,lbous,rbous,nsurf,var,gam,mboup,varb)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: npoin,nvar,ndim,nflu,nsurf
  integer(ip), intent(in)    :: nboup,mboup
  integer(ip), intent(in)    :: lboup(6,mboup),lbous(2,nsurf)
  real(rp),intent(inout)     :: dvar(nflu,npoin),varn(nvar,npoin) 
  real(rp),intent(in)        :: rdir(ndim,nboup),rbous(5,nsurf),gam,var(nvar,npoin),varb(nvar,nboup)
  integer(ip)                :: ipoin,ipo
  real(rp)                   :: drho,drhou,drhov,drhow,drhoe,udrho,vdrho,wdrho
  real(rp)                   :: dirx,diry,dirz,cscal,c00

  c00=0.0d+00

  do ipo=1,nboup
     ipoin=lboup(1,ipo)

     !
     !     Do we have characteristic boundary conditions
     !
     if(lboup(3,ipo)==0)then
        !
        !     Do we have something to do?
        !
        if(lboup(2,ipo)==0)cycle
        !
        !     Use geometry to determine bc
        !
        if(lboup(2,ipo)==1)then
           !
           !     Everything imposed
           !

           dvar(1,ipoin)=c00
           dvar(2,ipoin)=c00
           dvar(3,ipoin)=c00
           dvar(4,ipoin)=c00
           dvar(5,ipoin)=c00

        else if(lboup(2,ipo)==2)then
           !
           !     Velocity imposed
           !

           drho=dvar(1,ipoin)/varn(1,ipoin)
           dvar(2,ipoin)=drho*varn(2,ipoin)
           dvar(3,ipoin)=drho*varn(3,ipoin)
           dvar(4,ipoin)=drho*varn(4,ipoin)

        else if(lboup(2,ipo)==3)then 
           !
           !     Tangent free
           !
           dirx=rdir(1,ipo)
           diry=rdir(2,ipo)
           dirz=rdir(3,ipo)
           !
           !     Conserved variable increments
           !
           drho=dvar(1,ipoin)/varn(1,ipoin)
           drhou=dvar(2,ipoin)
           drhov=dvar(3,ipoin)
           drhow=dvar(4,ipoin)
           drhoe=dvar(5,ipoin)
           udrho=varn(2,ipoin)*drho
           vdrho=varn(3,ipoin)*drho
           wdrho=varn(4,ipoin)*drho

           cscal=(drhou-udrho)*dirx+(drhov-vdrho)*diry+(drhow-wdrho)*dirz

           dvar(2,ipoin)=udrho+cscal*dirx
           dvar(3,ipoin)=vdrho+cscal*diry
           dvar(4,ipoin)=wdrho+cscal*dirz


        else if(lboup(2,ipo)==4)then
           !
           !     Normal imposed
           !
           dirx=rdir(1,ipo)
           diry=rdir(2,ipo)
           dirz=rdir(3,ipo)
           !
           !     Conserved variable increments
           !
           drho=dvar(1,ipoin)/var(1,ipoin)
           drhou=dvar(2,ipoin)
           drhov=dvar(3,ipoin)
           drhow=dvar(4,ipoin)
           drhoe=dvar(5,ipoin)
           udrho=varn(2,ipoin)*drho
           vdrho=varn(3,ipoin)*drho
           wdrho=varn(4,ipoin)*drho

           cscal=(drhou-udrho)*dirx+(drhov-vdrho)*diry+(drhow-wdrho)*dirz

           dvar(2,ipoin)=drhou-cscal*dirx
           dvar(3,ipoin)=drhov-cscal*diry
           dvar(4,ipoin)=drhow-cscal*dirz

        else 
           write(*,*)'Error in bc -> stop'
           stop

        endif

     else

        call invar(var,varn,dvar,nvar,npoin,gam,nflu,ipo,rdir,nboup,mboup,lboup,ndim,lbous,rbous,nsurf,varb)

     endif

  enddo

end subroutine applbc


subroutine cpedpt(nedge,ledge,var,nvar,npoin,extr1,extr2)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)   :: nedge,nvar,npoin
  integer(ip),intent(in)   :: ledge(2,nedge)
  real(rp),intent(in)      :: var(nvar,npoin)
  real(rp), intent(inout)  :: extr1(nvar,nedge),extr2(nvar,nedge) 
  integer(ip)              :: ip1,ip2,iedge

  do iedge=1,nedge

     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)

     extr1(1,iedge)=var(1,ip1)
     extr1(2,iedge)=var(2,ip1)
     extr1(3,iedge)=var(3,ip1)
     extr1(4,iedge)=var(4,ip1)
     extr1(5,iedge)=var(5,ip1)

     extr2(1,iedge)=var(1,ip2)
     extr2(2,iedge)=var(2,ip2)
     extr2(3,iedge)=var(3,ip2)
     extr2(4,iedge)=var(4,ip2)
     extr2(5,iedge)=var(5,ip2)

  enddo


end subroutine cpedpt


subroutine gtgradvisc(nvar,ndim,var,grad,npoin,cvi,viscp)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nvar,ndim,npoin
  real(rp),intent(in)     :: var(nvar,npoin),cvi,viscp(2,npoin)
  real(rp),intent(inout)  :: grad(ndim,6,npoin)
  integer(ip)             :: ipoin
  real(rp)             :: rhoi,u,v,w,e,drhox,drhoy,drhoz   
  real(rp)             :: dux,duy,duz,dvx,dvy,dvz,dwx,dwy,dwz,dex,dey,dez
  real(rp)             :: dtx,dty,dtz,dv,c23,c20,s(3,3),w1,w2,w3,mu,cond
  c23=2.0d+00/3.0d+00
  c20=2.0d+00


  !
  !     This subroutine computes the gradient of the viscous variables
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,var,grad,cvi,c23,c20,viscp) &
!$omp  private(ipoin,rhoi,u,v,w,e,drhox,drhoy,drhoz,&  
!$omp&                       dux,duy,duz,dvx,dvy,dvz,dwx,dwy,dwz,dex,dey,dez,& 
!$omp&                       dtx,dty,dtz,dv,s,w1,w2,w3,mu,cond)
  !
  do ipoin=1,npoin
     !
     !     Get velocities and energy
     !
     rhoi=var(7,ipoin)
     mu=viscp(1,ipoin)
     cond=viscp(2,ipoin)
     u=var(2,ipoin)
     v=var(3,ipoin)
     w=var(4,ipoin)
     e=var(5,ipoin)
     !  
     !     -----and form the gradient of the non-conserved quantities
     !          with the gradient of the conserved quantities

     drhox = grad(1,1,ipoin)
     drhoy = grad(2,1,ipoin)
     drhoz = grad(3,1,ipoin)

     dux  = rhoi*( grad(1,2,ipoin) - u*drhox )
     duy  = rhoi*( grad(2,2,ipoin) - u*drhoy )
     duz  = rhoi*( grad(3,2,ipoin) - u*drhoz )
     dvx  = rhoi*( grad(1,3,ipoin) - v*drhox )
     dvy  = rhoi*( grad(2,3,ipoin) - v*drhoy )
     dvz  = rhoi*( grad(3,3,ipoin) - v*drhoz )
     dwx  = rhoi*( grad(1,4,ipoin) - w*drhox )
     dwy  = rhoi*( grad(2,4,ipoin) - w*drhoy )
     dwz  = rhoi*( grad(3,4,ipoin) - w*drhoz )
     dex  = rhoi*( grad(1,5,ipoin) - e*drhox )
     dey  = rhoi*( grad(2,5,ipoin) - e*drhoy )
     dez  = rhoi*( grad(3,5,ipoin) - e*drhoz )
     dTx  = cvi*(dex - (u*dux + v*dvx + w*dwx))
     dTy  = cvi*(dey - (u*duy + v*dvy + w*dwy))
     dTz  = cvi*(dez - (u*duz + v*dvz + w*dwz))
     !
     !     Compressibility
     !
     dv=dux+dvy+dwz
     !
     !     Stress tensor
     !
     s(1,1)=c20*dux     -c23*dv
     s(2,1)=    duy+dvx
     s(3,1)=    duz+dwx

     s(1,2)=    dvx+duy
     s(2,2)=c20*dvy     -c23*dv
     s(3,2)=    dvz+dwy

     s(1,3)=    dwx+duz
     s(2,3)=    dwy+dvz
     s(3,3)=c20*dwz     -c23*dv
     !
     !     Stress work
     !
     w1=u*s(1,1)+v*s(1,2)+w*s(1,3)
     w2=u*s(2,1)+v*s(2,2)+w*s(2,3)
     w3=u*s(3,1)+v*s(3,2)+w*s(3,3)
     !
     !     -----then form the viscous stresses  (s is symmetric..............)
     !
     grad(1,2,ipoin) = mu*s(1,1)
     grad(2,2,ipoin) = mu*s(2,1)
     grad(3,2,ipoin) = mu*s(3,1)
     grad(1,3,ipoin) = mu*s(1,2)
     grad(2,3,ipoin) = mu*s(2,2)
     grad(3,3,ipoin) = mu*s(3,2)
     grad(1,4,ipoin) = mu*s(1,3)
     grad(2,4,ipoin) = mu*s(2,3)
     grad(3,4,ipoin) = mu*s(3,3)
     grad(1,5,ipoin) = w1+cond*dtx
     grad(2,5,ipoin) = w2+cond*dty
     grad(3,5,ipoin) = w3+cond*dtz

  enddo

end subroutine gtgradvisc


subroutine visflu(ledge,nedge,edsha,npoin,nvar,var,cvi,flu,nflu,viscp,ndim,grad,temp0,scons,cv,Pri,edshab,edshap,nboup,lboup,nedgb,ledgb,mu0,mboup)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)      :: nedge,npoin,nvar,nflu,ndim,nboup,mboup,nedgb
  integer(ip),intent(in)      :: ledge(2,nedge),lboup(6,mboup),ledgb(2,nedgb)
  real(rp),intent(in)         :: edsha(5,nedge),cvi,grad(ndim,6,npoin),temp0,scons,cv,Pri,mu0 
  real(rp),intent(in)         :: edshab(3,nedgb),edshap(3,nboup) 
  real(rp),intent(inout)      :: var(nvar,npoin),flu(nflu,npoin),viscp(2,npoin) 
  integer(ip)                 :: ip1,ip2,iedge,ipoin
  real(rp)                    :: lapl,visc,cond,u1,v1,w1,e1,u2,v2,w2,e2,v12,v22  
  real(rp)                    :: temp1,temp2,redg2,redg3,redg4,redg5,c05  
  real(rp)                    :: coef1,coef2,coef3  


  c05=0.5d+00
  !
  !     Switch from conservative to primitive variables
  ! 
  call conpri(npoin,var,nvar)
  !
  !     Compute viscosity
  !
  call suther(cvi,var,nvar,npoin,temp0,scons,viscp,cv,Pri,mu0)
  !
  !     Compute viscous gradients
  ! 
  call gtgradvisc(nvar,ndim,var,grad,npoin,cvi,viscp)
  !
  !     Compute viscous fluxes
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,grad,flu) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,&  
!$omp&                       redg2,redg3,redg4,redg5 )
!
  do iedge=1,nedge

     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)

     redg2=coef1*(grad(1,2,ip1)+grad(1,2,ip2))+coef2*(grad(2,2,ip1)+grad(2,2,ip2))+coef3*(grad(3,2,ip1)+grad(3,2,ip2)) 
     redg3=coef1*(grad(1,3,ip1)+grad(1,3,ip2))+coef2*(grad(2,3,ip1)+grad(2,3,ip2))+coef3*(grad(3,3,ip1)+grad(3,3,ip2)) 
     redg4=coef1*(grad(1,4,ip1)+grad(1,4,ip2))+coef2*(grad(2,4,ip1)+grad(2,4,ip2))+coef3*(grad(3,4,ip1)+grad(3,4,ip2)) 
     redg5=coef1*(grad(1,5,ip1)+grad(1,5,ip2))+coef2*(grad(2,5,ip1)+grad(2,5,ip2))+coef3*(grad(3,5,ip1)+grad(3,5,ip2)) 
     !
     !     Send to points
     !
     flu(2,ip1)=flu(2,ip1)-redg2
     flu(3,ip1)=flu(3,ip1)-redg3
     flu(4,ip1)=flu(4,ip1)-redg4
     flu(5,ip1)=flu(5,ip1)-redg5

     flu(2,ip2)=flu(2,ip2)+redg2
     flu(3,ip2)=flu(3,ip2)+redg3
     flu(4,ip2)=flu(4,ip2)+redg4
     flu(5,ip2)=flu(5,ip2)+redg5

  enddo
  !
  !     Boundary edges
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedgb,ledgb,edshab,grad,flu) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,&  
!$omp&                       redg2,redg3,redg4,redg5 )  
    
    do iedge=1,nedgb

     ip1=ledgb(1,iedge)
     ip2=ledgb(2,iedge)
     coef1=edshab(1,iedge)
     coef2=edshab(2,iedge)
     coef3=edshab(3,iedge)

     redg2=coef1*(grad(1,2,ip1)+grad(1,2,ip2))+coef2*(grad(2,2,ip1)+grad(2,2,ip2))+coef3*(grad(3,2,ip1)+grad(3,2,ip2)) 
     redg3=coef1*(grad(1,3,ip1)+grad(1,3,ip2))+coef2*(grad(2,3,ip1)+grad(2,3,ip2))+coef3*(grad(3,3,ip1)+grad(3,3,ip2)) 
     redg4=coef1*(grad(1,4,ip1)+grad(1,4,ip2))+coef2*(grad(2,4,ip1)+grad(2,4,ip2))+coef3*(grad(3,4,ip1)+grad(3,4,ip2)) 
     redg5=coef1*(grad(1,5,ip1)+grad(1,5,ip2))+coef2*(grad(2,5,ip1)+grad(2,5,ip2))+coef3*(grad(3,5,ip1)+grad(3,5,ip2)) 
     !
     !     Send to points
     !
     flu(2,ip1)=flu(2,ip1)+redg2
     flu(3,ip1)=flu(3,ip1)+redg3
     flu(4,ip1)=flu(4,ip1)+redg4
     flu(5,ip1)=flu(5,ip1)+redg5

     flu(2,ip2)=flu(2,ip2)+redg2
     flu(3,ip2)=flu(3,ip2)+redg3
     flu(4,ip2)=flu(4,ip2)+redg4
     flu(5,ip2)=flu(5,ip2)+redg5

  enddo
  !
  !     Boundary points
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nboup,lboup,edshap,grad,flu) &
!$omp& private(ipoin,ip1,coef1,coef2,coef3,&  
!$omp&                       redg2,redg3,redg4,redg5 )  

  do ipoin=1,nboup

     ip1=lboup(1,ipoin)
     coef1=edshap(1,ipoin)
     coef2=edshap(2,ipoin)
     coef3=edshap(3,ipoin)

     redg2=coef1*grad(1,2,ip1)+coef2*grad(2,2,ip1)+coef3*grad(3,2,ip1) 
     redg3=coef1*grad(1,3,ip1)+coef2*grad(2,3,ip1)+coef3*grad(3,3,ip1)
     redg4=coef1*grad(1,4,ip1)+coef2*grad(2,4,ip1)+coef3*grad(3,4,ip1)
     redg5=coef1*grad(1,5,ip1)+coef2*grad(2,5,ip1)+coef3*grad(3,5,ip1) 
     !
     !     Send to points
     !
     flu(2,ip1)=flu(2,ip1)-redg2
     flu(3,ip1)=flu(3,ip1)-redg3
     flu(4,ip1)=flu(4,ip1)-redg4
     flu(5,ip1)=flu(5,ip1)-redg5

  enddo
!
  !     Switch from primitive to conservative  variables
  ! 
  call pricon(npoin,var,nvar)

end subroutine visflu

subroutine visflulap(ledge,nedge,edsha,npoin,nvar,var,cvi,flu,nflu,viscp)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)      :: nedge,npoin,nvar,nflu
  integer(ip),intent(in)      :: ledge(2,nedge)
  real(rp),intent(in)         :: edsha(5,nedge),cvi,viscp(2,npoin) 
  real(rp),intent(inout)      :: var(nvar,npoin),flu(nflu,npoin) 
  integer(ip)                 :: ip1,ip2,iedge
  real(rp)                    :: lapl,visc,cond,u1,v1,w1,e1,u2,v2,w2,e2,v12,v22  
  real(rp)                    :: temp1,temp2,redg2,redg3,redg4,redg5,c05  


  c05=0.5d+00
  !
  !     Switch from conservative to primitive variables
  ! 
  call conpri(npoin,var,nvar)
  !
  !     Compute viscous stresses
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,flu,viscp,c05,var,cvi) &
!$omp& private(iedge,ip1,ip2,lapl,visc,cond,u1,v1,w1,&  
!$omp&         e1,v12,temp1,u2,v2,w2,e2,v22,temp2,&
!$omp&         redg2,redg3,redg4,redg5 )  


  do iedge=1,nedge

     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     lapl=edsha(5,iedge)*c05
     visc=lapl*(viscp(1,ip1)+viscp(1,ip2))
     cond=lapl*(viscp(2,ip1)+viscp(2,ip2)) 
     !
     !     Get the relevant quantities
     !
     u1=var(2,ip1)
     v1=var(3,ip1)
     w1=var(4,ip1)
     e1=var(5,ip1)
     v12=c05*u1*u1+v1*v1+w1*w1
     temp1=cvi*(e1-v12)

     u2=var(2,ip2)
     v2=var(3,ip2)
     w2=var(4,ip2)
     e2=var(5,ip2)
     v22=c05*u2*u2+v2*v2+w2*w2
     temp2=cvi*(e2-v22)


     redg2=visc*(u1-u2)
     redg3=visc*(v1-v2)
     redg4=visc*(w1-w2)
     redg5=visc*(v12-v22)+cond*(temp1-temp2)

     !
     !     Send to points
     !


     flu(2,ip1)=flu(2,ip1)+redg2
     flu(3,ip1)=flu(3,ip1)+redg3
     flu(4,ip1)=flu(4,ip1)+redg4
     flu(5,ip1)=flu(5,ip1)+redg5

     flu(2,ip2)=flu(2,ip2)-redg2
     flu(3,ip2)=flu(3,ip2)-redg3
     flu(4,ip2)=flu(4,ip2)-redg4
     flu(5,ip2)=flu(5,ip2)-redg5



  enddo
  !
  !     Switch from primitive to conservative  variables
  ! 
  call pricon(npoin,var,nvar)



end subroutine visflulap

subroutine suther(cvi,var,nvar,npoin,temp0,scons,viscp,cv,Pri,mu0)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)    :: nvar,npoin 
  real(rp),intent(in)       :: cvi,var(nvar,npoin),temp0,scons,cv,Pri,mu0
  real(rp),intent(inout)    :: viscp(2,npoin)
  integer(ip)               :: ipoin
  real(rp)                  :: c05,u2,u,v,w,e,temp,temp0i,c10,c32,rt,mu

  c05=0.5d+00
  c10=1.0d+00
  c32=3.0d+00/2.0d+00
  temp0i=c10/temp0

!
!     This sub computes the viscosity and the conductivity based on Sutherland's law:
!     mu=mu0*(T/T0)^(3/2)*(T0+S)/(T+S)
!




!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,var,c10,cvi,c05,c32,temp0i,temp0,scons,cv,Pri,viscp,mu0) &
!$omp& private(u,v,w,e,temp,mu,rt,u2)


  do ipoin=1,npoin
     u=var(2,ipoin)
     v=var(3,ipoin)
     w=var(4,ipoin)
     e=var(5,ipoin)
     u2=c05*(u*u+v*v+w*w)
     temp=cvi*(e-u2)
     rt=(temp*temp0i)**c32
     mu=mu0*rt*(temp0+scons)/(temp+scons)
     viscp(1,ipoin)=mu
     viscp(2,ipoin)=mu*cv*Pri 
  enddo


end subroutine suther




subroutine conpri(npoin,var,nvar)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)      :: npoin,nvar
  real(rp),intent(inout)      :: var(nvar,npoin)
  integer(ip)                 :: ipoin
  real(rp)                    :: rho,rhou,rhov,rhow,rhoe,rhoi,u,v,w,e,c10

  c10=1.0d+00

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,var,c10) &
!$omp& private(rho,rhou,rhov,rhow,rhoe,rhoi,u,v,w,e )

  do ipoin=1,npoin
     rho=var(1,ipoin)
     rhou=var(2,ipoin)
     rhov=var(3,ipoin)
     rhow=var(4,ipoin)
     rhoe=var(5,ipoin)

     rhoi=c10/rho
     u=rhou*rhoi 
     v=rhov*rhoi 
     w=rhow*rhoi 
     e=rhoe*rhoi

     var(2,ipoin)=u
     var(3,ipoin)=v
     var(4,ipoin)=w
     var(5,ipoin)=e

  enddo

end subroutine conpri

subroutine pricon(npoin,var,nvar)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)      :: npoin,nvar
  real(rp),intent(inout)      :: var(nvar,npoin)
  integer(ip)                 :: ipoin
  real(rp)                    :: rho,rhou,rhov,rhow,rhoe,rhoi,u,v,w,e

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,var) &
!$omp& private(rho,rhou,rhov,rhow,rhoe,u,v,w,e )
 
  do ipoin=1,npoin
     rho=var(1,ipoin)
     u=var(2,ipoin)
     v=var(3,ipoin)
     w=var(4,ipoin)
     e=var(5,ipoin)

     rhou=u*rho 
     rhov=v*rho 
     rhow=w*rho 
     rhoe=e*rho 

     var(2,ipoin)=rhou
     var(3,ipoin)=rhov
     var(4,ipoin)=rhow
     var(5,ipoin)=rhoe

  enddo

end subroutine pricon

subroutine hllcflu(nedge,ledge,npoin,nvar,nflu,edsha,gam,extr1,extr2,flu,coor,ndim,hydro1,hydro2,rmass,grav)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nedge,npoin,nvar,nflu,ndim
  real(rp), intent(in)       :: ledge(2,nedge),edsha(5,nedge),gam,extr1(nvar,nedge),extr2(nvar,nedge)
  real(rp), intent(in)       :: coor(ndim,npoin),rmass(npoin),hydro1(2,nedge),hydro2(2,nedge),grav
  real(rp),intent(inout)     :: flu(nflu,npoin) 
  real(rp)     :: srhor,srhol,det,deti,rhot,ut,vt,wt,Ht
  real(rp)     :: rnl,rnli,coef1,coef2,coef3,rho1,rhou1,rhov1,rhow1,rhoe1,rho2
  real(rp)     :: rhou2,rhov2,rhow2,rhoe2,pres1,pres2,rhoi1,rhoi2
  real(rp)     :: dirx,diry,dirz,rhor,vr,ur,wr,Hr,er,rhol,ul,vl,wl,Hl,el 
  real(rp)     :: at,c10,c00,c05,c20,c14,gam1,cm2
  real(rp)     :: redg1,redg2,redg3,redg4,redg5,vedg,pres12,pres22 
  real(rp)     :: u1,u2,vt2,upw1,upw2,upw3,upw4,upw5,radv1,radv2
  real(rp)     :: stat1(5),stat2(5),S1,S2,Sstar
  real(rp)     :: f11,f12,f13,f14,f15,f21,f22,f23,f24,f25 
  real(rp)     :: c1,c2,cons 
  real(rp)     :: g1,g2,g3,g4,g5,g6,g7,g8
  real(rp)     :: pmin,pmax,qmax,quser,um,pq,pt1,pt2,ge1,ge2,cup,ppv,pm 
  real(rp)     :: ux,lx,ly,lz,ll,dz,presh1,presh2,rhoh1,rhoh2,dpresh1,dpresh2,drhoh1,drhoh2
  real(rp)     :: rho,pres,temp,z,temp0,pres0,epsil
  integer(ip)  :: iedge,ip1,ip2,ipstrang,iopt


  c00=0.0d+00
  c05=0.5d+00
  c10=1.0d+00
  c14=1.0d+00/4.0d+00
  c20=2.0d+00
  cm2=-2.0d+00
  gam1=gam-c10
  g1=gam1/(c20*gam)
  g2=(gam+c10)/(c20*gam)
  g3=c20*gam/gam1
  g4=c20/gam1
  g5=c20/(gam+c10)
  g6=gam1/(gam+c10)
  g7=gam1/c20
  g8=gam1
  quser=c20
  iopt=1_ip
  ipstrang=46176_ip
  temp0=300.0d+00
  pres0=100000.0d+00
  epsil=1.0d-06

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,extr1,extr2,iopt,flu,c10,c14,c05,gam,c00,cm2, &
!$omp&       gam1,quser,g1,g2,g3,g4,g5,g6,g7,g8,ipstrang,hydro1,hydro2)&
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rnl,rnli,& 
!$omp&        dirx,diry,dirz,rho1,rhou1,rhov1,rhow1,rhoe1,rho2,rhou2,rhov2,rhow2,rhoe2,&
!$omp&        rhoi1,u1,c1,rhoi2,u2,c2,pres1,pres2,redg1,redg2,redg3,redg4,redg5,&
!$omp&        vedg,ut,vt,wt,at,Ht,rhol,ul,vl,wl,el,Hl,rhor,ur,vr,wr,er,Hr,&
!$omp&        srhol,srhor,det,deti,vt2,S1,S2,Sstar, &
!$omp&        cup,ppv,pmin,pmax,qmax,um,pm,ge1,ge2,radv1,radv2,pt1,pt2,pq,cons,& 
!$omp&        f11,f12,f13,f14,f15,f21,f22,f23,f24,f25,stat1,stat2,rhoh1,rhoh2,presh1,presh2,dpresh1,dpresh2)


  do iedge=1,nedge

     !
     !     Edge points
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)

     !
     !    Edge coef *2 in the direction oposite to the edge direction
     !     
     ! 
     coef1=cm2*edsha(2,iedge)
     coef2=cm2*edsha(3,iedge)
     coef3=cm2*edsha(4,iedge)
     ! DBG 1D
     !coef1=edsha(2,iedge)
     !coef2=edsha(3,iedge)
     !coef3=edsha(4,iedge)

     rnl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3) 
     rnli=c10/rnl

     dirx=coef1*rnli
     diry=coef2*rnli
     dirz=coef3*rnli
     !
     !     Get relevant quantities 
     !
     rho1=extr1(1,iedge)
     rhou1=extr1(2,iedge)
     rhov1=extr1(3,iedge)
     rhow1=extr1(4,iedge)
     rhoe1=extr1(5,iedge)
     pres1=extr1(6,iedge)
     rhoi1=extr1(7,iedge)
     u1=(dirx*rhou1+diry*rhov1+dirz*rhow1)*rhoi1
     c1=sqrt(gam*pres1*rhoi1)

     !if(abs(rho1*rhoi1-1)>1.0d-03)then
     !   write(*,*)'error rhoi1'
     !   stop
     !endif


     rho2=extr2(1,iedge)
     rhou2=extr2(2,iedge)
     rhov2=extr2(3,iedge)
     rhow2=extr2(4,iedge)
     rhoe2=extr2(5,iedge)
     pres2=extr2(6,iedge)
     rhoi2=extr2(7,iedge)
     u2=(dirx*rhou2+diry*rhov2+dirz*rhow2)*rhoi2
     c2=sqrt(gam*pres2*rhoi2)

     !if(abs(rho1*rhoi1-1)>1.0d-03)then
     !   write(*,*)'error rhoi1'
     !   stop
     !endif
     !
     !     Take into account gravity 
     !
     rhoh1=hydro1(1,iedge)
     presh1=hydro1(2,iedge)
     !drhoh1=rho1-rhoh1
     dpresh1=pres1-presh1
     !dpresh1=pres1
     rhoh2=hydro2(1,iedge)
     presh2=hydro2(2,iedge)
     !drhoh2=rho2-rhoh2
     dpresh2=pres2-presh2
     !dpresh2=pres2
     !
     !     Compute left and right velocities and enthalpies
     !
     rhol=rho1
     ul=rhou1*rhoi1
     vl=rhov1*rhoi1
     wl=rhow1*rhoi1
     el=rhoe1*rhoi1
     Hl=(rhoe1+pres1)*rhoi1

     rhor=rho2
     ur=rhou2*rhoi2
     vr=rhov2*rhoi2
     wr=rhow2*rhoi2
     er=rhoe2*rhoi2
     Hr=(rhoe2+pres2)*rhoi2


     if(iopt==1)then
        !
        !     Compute Roe average
        !
        srhol=sqrt(rhol)
        srhor=sqrt(rhor)
        det=srhol+srhor
        deti=c10/det

        !rhot=srhol*srhor
        ut=(srhol*ul+srhor*ur)*deti
        vt=(srhol*vl+srhor*vr)*deti
        wt=(srhol*wl+srhor*wr)*deti
        Ht=(srhol*Hl+srhor*Hr)*deti
        vt2=ut*ut+vt*vt+wt*wt
        at=sqrt(gam1*(Ht-c05*vt2))
        !
        !     Velocity in the edge direction
        ! 
        vedg=dirx*ut+diry*vt+dirz*wt
        !
        !     Wave speed estimate
        !
        S1=min(u1-c1,vedg-at)    
        S2=max(u2+c2,vedg+at)
        Sstar=(rho2*u2*(S2-u2)-rho1*u1*(S1-u1)+dpresh1-dpresh2)/(rho2*(S2-u2)-rho1*(S1-u1))

     else


        cup=c14*(rho1+rho2)*(c1+c2)
        ppv=c05*(pres1+pres2)+c05*(u1-u2)*cup
        ppv=max(c00,ppv)
        pmin=min(pres1,pres2)
        pmax=max(pres1,pres2)
        qmax=pmax/pmin

        if(qmax<=quser .and. (pmin<=ppv .and. ppv<=pmax))then

           !
           !     PRVS Riemann solver
           !
           pm=ppv
           um=c05*(u1+u2)+c05*(pres1-pres2)/cup  

        else if(ppv<pmin)then

           !
           !    Two rarefaction Riemann solver
           !
           pq=(pres1/pres2)**g1
           um=(pq*u1/c1+u2/c2+g4*(pq-c10))/(pq/c1+c10/c2)
           pt1=c10+g7*(u1-um)/c1
           pt2=c10+g7*(um-u2)/c2
           pm=c05*(pres1*pt1**g3+pres2*pt2**g3)

        else

           !
           !   Two Shock Riemann solver with PVRS estimate
           !
           ge1=sqrt((g5/rho1)/(g6*pres1+ppv))
           ge2=sqrt((g5/rho2)/(g6*pres2+ppv))
           pm=(ge1*pres1+ge2*pres2-(u2-u1))/(ge1+ge2) 
           um=c05*(u1+u2)+c05*(ge2*(pm-pres2)-ge1*(pm-pres1))

        endif
        !
        !     Find speed
        !

        if(pm<=pres1)then
           S1=u1-c1
        else
           S1=u1-c1*sqrt(c10+g2*(pm/pres1-c10))
        endif

        if(pm<=pres2)then
           S2=u2+c2
        else
           S2=u2+c2*sqrt(c10+g2*(pm/pres2-c10))
        endif

        Sstar=um

     endif
     !
     !     Compute states
     !
     cons=rho1*(S1-u1)/(S1-Sstar)
     stat1(1)=cons
     stat1(2)=cons*(ul+(Sstar-u1)*dirx)
     stat1(3)=cons*(vl+(Sstar-u1)*diry)
     stat1(4)=cons*(wl+(Sstar-u1)*dirz)
     stat1(5)=cons*(el+(Sstar-u1)*(Sstar+(pres1*rhoi1/(S1-u1))))

     cons=rho2*(S2-u2)/(S2-Sstar)
     stat2(1)=cons
     stat2(2)=cons*(ur+(Sstar-u2)*dirx)
     stat2(3)=cons*(vr+(Sstar-u2)*diry)
     stat2(4)=cons*(wr+(Sstar-u2)*dirz)
     stat2(5)=cons*(er+(Sstar-u2)*(Sstar+(pres2*rhoi2/(S2-u2))))

     radv1=u1
     radv2=u2
     !
     !     Euler Galerkin Fluxes
     !
     f11= radv1*rho1               
     f12= radv1*rhou1+dirx*dpresh1 
     f13= radv1*rhov1+diry*dpresh1
     f14= radv1*rhow1+dirz*dpresh1 !+ drhoh1*grav*rnli/rmass(ip1) 
     f15= radv1*(rhoe1+pres1)!+rho1*grav*wl*rnli/rmass(ip1)

     f21= radv2*rho2               
     f22= radv2*rhou2+dirx*dpresh2 
     f23= radv2*rhov2+diry*dpresh2 
     f24= radv2*rhow2+dirz*dpresh2 !+ drhoh2*grav*rnli/rmass(ip2) 
     f25= radv2*(rhoe2+pres2)!+rho2*grav*wr*rnli/rmass(ip2)

     !
     !     Get the HLLC fluxes
     !
     if(S1>c00)then

        redg1=f11    
        redg2=f12    
        redg3=f13    
        redg4=f14    
        redg5=f15    

     else if(Sstar>c00)then 


        redg1=f11+S1*(stat1(1)-rho1)    
        redg2=f12+S1*(stat1(2)-rhou1)    
        redg3=f13+S1*(stat1(3)-rhov1)    
        redg4=f14+S1*(stat1(4)-rhow1)    
        redg5=f15+S1*(stat1(5)-rhoe1)    

     else if(S2>c00)then

        redg1=f21+S2*(stat2(1)-rho2)    
        redg2=f22+S2*(stat2(2)-rhou2)    
        redg3=f23+S2*(stat2(3)-rhov2)    
        redg4=f24+S2*(stat2(4)-rhow2)    
        redg5=f25+S2*(stat2(5)-rhoe2)  

     else

        redg1=f21    
        redg2=f22    
        redg3=f23    
        redg4=f24    
        redg5=f25  

     endif
     !
     !     Correct the scaling and direction
     !
     redg1=-redg1*rnl
     redg2=-redg2*rnl
     redg3=-redg3*rnl
     redg4=-redg4*rnl
     redg5=-redg5*rnl
     !
     !     Send to points
     !

     !if(ip1==ipstrang )then

     !   write(*,*)S1,S2,Sstar
     !   write(*,*)flu(1,ip1),f12*rnl,-redg2,flu(2,ip1)

     !endif

     !if(ip2==ipstrang )then

     !   write(*,*)S1,S2,Sstar
     !   write(*,*)flu(1,ip2),f12*rnl,-redg2,flu(2,ip2)

     !endif
   

     flu(1,ip1)=flu(1,ip1)+redg1
     flu(2,ip1)=flu(2,ip1)+redg2
     flu(3,ip1)=flu(3,ip1)+redg3
     flu(4,ip1)=flu(4,ip1)+redg4
     flu(5,ip1)=flu(5,ip1)+redg5

     flu(1,ip2)=flu(1,ip2)-redg1
     flu(2,ip2)=flu(2,ip2)-redg2
     flu(3,ip2)=flu(3,ip2)-redg3
     flu(4,ip2)=flu(4,ip2)-redg4
     flu(5,ip2)=flu(5,ip2)-redg5

     !if(ip1==ipstrang )then

     !   write(*,*)iedge,rnl*diry*dpresh2,flu(3,ip2),redg3

     !endif

     !if(ip2==ipstrang )then

     !   write(*,*)iedge,rnl*diry*dpresh2,flu(3,ip2),redg3

     !endif

  enddo

end subroutine hllcflu




subroutine jstflu(nedge,ledge,npoin,nvar,nflu,edsha,gam,var,sens,coor,ndim,flu,grad)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nedge,npoin,nvar,nflu,ndim
  integer(ip), intent(in)    :: ledge(2,nedge)
  real(rp), intent(in)       :: edsha(5,nedge),gam,var(nvar,npoin)
  real(rp),intent(inout)     :: flu(nflu,npoin),grad(ndim,6,npoin) 
  real(rp), intent(in)       :: coor(ndim,npoin)
  real(rp), intent(inout)    :: sens(nedge)
  real(rp)     :: rnl,rnli,coef1,coef2,coef3,rho1,rhou1,rhov1,rhow1,rhoe1,rho2
  real(rp)     :: rhou2,rhov2,rhow2,rhoe2,pres1,pres2,rhoi1,rhoi2
  real(rp)     :: dirx,diry,dirz,rhoi
  real(rp)     :: c10,c00,c05,c20,c14,gam1,cm2
  real(rp)     :: redg1,redg2,redg3,redg4,redg5,vedg,pres12,pres22 
  real(rp)     :: redg1d,redg2d,redg3d,redg4d,redg5d 
  real(rp)     :: radv1,radv2,rl
  real(rp)     :: f11,f12,f13,f14,f15,f21,f22,f23,f24,f25 
  real(rp)     :: lx,ly,lz,beta,pres,u,v,w,c,lambda,radv 
  real(rp)     :: drho,drhou,drhov,drhow,drhoe,drho4,drhou4,drhov4,drhow4,drhoe4,dpres,dpres4 
  real(rp)     :: rho,rhou,rhov,rhow,rhoe,epsil,const,length 
  integer(ip)  :: iedge,ip1,ip2



  c00=0.0d+00
  c05=0.5d+00
  c10=1.0d+00
  c14=1.0d+00/4.0d+00
  c20=2.0d+00
  cm2=-2.0d+00
  epsil=1.0d-09
  !const=0.0d+00



  !
  !     First get the pressure sensor
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,coor,sens,c10,c05,epsil) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rnl,rnli,& 
!$omp&                       dirx,diry,dirz,lx,ly,lz,pres1,pres2,dpres,dpres4)

  do iedge=1,nedge
     !
     !     Points of the edge 
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Geometric coefficients
     !
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)
     rnl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3) 
     rnli=c10/rnl

     dirx=coef1*rnli
     diry=coef2*rnli
     dirz=coef3*rnli

     lx=coor(1,ip2)-coor(1,ip1)
     ly=coor(2,ip2)-coor(2,ip1)
     lz=coor(3,ip2)-coor(3,ip1)
     !
     !     Pressure
     !    
     pres1=var(6,ip1)
     pres2=var(6,ip2)

     dpres=pres2-pres1
     dpres4=c05*(grad(1,6,ip1)+grad(1,6,ip2)*lx+(grad(2,6,ip1)+grad(2,6,ip2))*ly+(grad(3,6,ip1)+grad(3,6,ip2))*lz)
     !
     !     Pressure sensor
     !
     sens(iedge)=c10-abs(dpres-dpres4)/(abs(dpres)+abs(dpres4)+epsil)

  enddo


  !
  !     Then compute the fluxes
  !

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,coor,sens,var,grad,flu,c10,c14,c05,gam,c00) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rnl,rnli,& 
!$omp&        dirx,diry,dirz,lx,ly,lz,beta,rho1,rhou1,rhov1,rhow1,rhoe1,rho2,rhou2,rhov2,rhow2,rhoe2,&
!$omp&        rhoi1,rhoi2,pres,pres1,pres2,radv,redg1,redg2,redg3,redg4,redg5,rho,rhoi,rhou,&
!$omp&        rhov,rhow,u,v,w,vedg,c,lambda,drho,drhou,drhov,drhow,drhoe,drho4,drhou4,&
!$omp&        drhov4,drhow4,drhoe4,redg1d,redg2d,redg3d,redg4d,redg5d,length )

  do iedge=1,nedge
     !
     !     Points of the edge 
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Geometric coefficients
     !
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)
     rnl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3) 
     rnli=c10/rnl

     dirx=coef1*rnli
     diry=coef2*rnli
     dirz=coef3*rnli

     lx=coor(1,ip2)-coor(1,ip1)
     ly=coor(2,ip2)-coor(2,ip1)
     lz=coor(3,ip2)-coor(3,ip1)
     length=sqrt(lx*lx+ly*ly+lz*lz)
     !
     !     Pressure sensor multiplied by c05 for fourth order terms
     !
     beta=sens(iedge)*c05
     !
     !     Get relevant quantities 
     !
     rho1=var(1,ip1)
     rhou1=var(2,ip1)
     rhov1=var(3,ip1)
     rhow1=var(4,ip1)
     rhoe1=var(5,ip1)
     pres1=var(6,ip1)
     rhoi1=var(7,ip1)

     rho2=var(1,ip2)
     rhou2=var(2,ip2)
     rhov2=var(3,ip2)
     rhow2=var(4,ip2)
     rhoe2=var(5,ip2)
     pres2=var(6,ip2)
     rhoi2=var(7,ip2)

     !
     !     Compute advection and pressure
     !
     pres=pres1+pres2
     radv=c14*(coef1*(rhou1+rhou2)+coef2*(rhov1+rhov2)+coef3*(rhow1+rhow2))*(rhoi1+rhoi2)

     !
     !     Euler Galerkin Fluxes
     !
     redg1= radv*(rho1+rho2) 
     redg2= radv*(rhou1+rhou2)+coef1*pres
     redg3= radv*(rhov1+rhov2)+coef2*pres 
     redg4= radv*(rhow1+rhow2)+coef3*pres 
     redg5= radv*(rhoe1+rhoe2+pres) 
     !
     !     Get maximal eigenvalue
     !
     rho=c05*(rho1+rho2)
     rhoi=c10/rho
     rhou=c05*(rhou1+rhou2)
     rhov=c05*(rhov1+rhov2)
     rhow=c05*(rhow1+rhow2)
     pres=c05*pres

     u=rhou*rhoi
     v=rhov*rhoi
     w=rhow*rhoi

     !
     !     Velocity in the edge direction
     !
     vedg=abs(dirx*u+diry*v+dirz*w)
     !
     !     Speed of sound
     !
     c=sqrt(gam*pres*rhoi)
     !
     !     Largest eigenvalue scaled
     !
     lambda=c05*(vedg+c)*rnl

     !
     !     Second order damping
     !
     drho=rho2-rho1 
     drhou=rhou2-rhou1 
     drhov=rhov2-rhov1 
     drhow=rhow2-rhow1 
     drhoe=rhoe2-rhoe1 

     !
     !     Fourth order damping
     !
     drho4=(grad(1,1,ip1)+grad(1,1,ip2))*lx+(grad(2,1,ip1)+grad(2,1,ip2))*ly+(grad(3,1,ip1)+grad(3,1,ip2))*lz       
     drhou4=(grad(1,2,ip1)+grad(1,2,ip2))*lx+(grad(2,2,ip1)+grad(2,2,ip2))*ly+(grad(3,2,ip1)+grad(3,2,ip2))*lz       
     drhov4=(grad(1,3,ip1)+grad(1,3,ip2))*lx+(grad(2,3,ip1)+grad(2,3,ip2))*ly+(grad(3,3,ip1)+grad(3,3,ip2))*lz       
     drhow4=(grad(1,4,ip1)+grad(1,4,ip2))*lx+(grad(2,4,ip1)+grad(2,4,ip2))*ly+(grad(3,4,ip1)+grad(3,4,ip2))*lz       
     drhoe4=(grad(1,5,ip1)+grad(1,5,ip2))*lx+(grad(2,5,ip1)+grad(2,5,ip2))*ly+(grad(3,5,ip1)+grad(3,5,ip2))*lz       

     !beta=c00
     !
     !     Blend with pressure sensor
     !
     redg1d=lambda*(drho-beta*drho4)
     redg2d=lambda*(drhou-beta*drhou4)
     redg3d=lambda*(drhov-beta*drhov4)
     redg4d=lambda*(drhow-beta*drhow4)
     redg5d=lambda*(drhoe-beta*drhoe4)
     
     !if(drho*redg1d<c00)write(*,*)redg1d,drho
     !if(drhou*redg2d<c00)write(*,*)redg2d,drhou
     !if(drhov*redg3d<c00)write(*,*)redg3d,drhov
     !if(drhow*redg4d<c00)write(*,*)redg4d,drhow
     !if(drhoe*redg5d<c00)write(*,*)redg5d,drhoe

     !redg1d=const*redg1d
     !redg2d=const*redg2d
     !redg3d=const*redg3d
     !redg4d=const*redg4d
     !redg5d=const*redg5d

     !
     !     Cut off the antidiffusion
     ! 
     if(drho*redg1d<c00)redg1d=c00
     if(drhou*redg2d<c00)redg2d=c00
     if(drhov*redg3d<c00)redg3d=c00
     if(drhow*redg4d<c00)redg4d=c00
     if(drhoe*redg5d<c00)redg5d=c00


     !
     !     Keep only first order diffusion
     ! 
     !if(drho*redg1d<c00)redg1d=lambda*drho
     !if(drhou*redg2d<c00)redg2d=lambda*drhou
     !if(drhov*redg3d<c00)redg3d=lambda*drhov
     !if(drhow*redg4d<c00)redg4d=lambda*drhow
     !if(drhoe*redg5d<c00)redg5d=lambda*drhoe


     !DBG
     !redg1d=c00
     !redg2d=c00
     !redg3d=c00
     !redg4d=c00
     !redg5d=c00
     !
     !     Sum up Galerkin + dissipation
     !
     redg1=redg1+redg1d  
     redg2=redg2+redg2d  
     redg3=redg3+redg3d  
     redg4=redg4+redg4d  
     redg5=redg5+redg5d  
     !
     !     Add to the flux points
     !
     flu(1,ip1)=flu(1,ip1)+redg1
     flu(2,ip1)=flu(2,ip1)+redg2
     flu(3,ip1)=flu(3,ip1)+redg3
     flu(4,ip1)=flu(4,ip1)+redg4
     flu(5,ip1)=flu(5,ip1)+redg5

     flu(1,ip2)=flu(1,ip2)-redg1
     flu(2,ip2)=flu(2,ip2)-redg2
     flu(3,ip2)=flu(3,ip2)-redg3
     flu(4,ip2)=flu(4,ip2)-redg4
     flu(5,ip2)=flu(5,ip2)-redg5

  enddo



end subroutine jstflu


subroutine invar(var,varn,dvar,nvar,npoin,gam,nflu,ipo,rdir,nboup,mboup,lboup,ndim,lbous,rbous,nsurf,varb)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: npoin,nvar,nflu,ipo,ndim,nboup,mboup,nsurf 
  integer(ip),intent(in)  :: lboup(6,mboup),lbous(2,nsurf) 
  real(rp), intent(in)    :: var(nvar,npoin),varn(nvar,npoin),gam,varb(nvar,nboup)
  real(rp), intent(in)    :: rdir(ndim,nboup),rbous(5,nsurf)
  real(rp),intent(inout)  :: dvar(nflu,npoin)   

  integer(ip)     :: ipoin,icond,isurf,i,kboup,inflow
  real(rp)        :: rho,rhou,rhov,rhow,rhoe,pres,rhoi,rhoinf,vinfx,vinfy,vinfz,pinf,minf,rhoeinf,cinf
  real(rp)        :: rhon,rhoun,rhovn,rhown,rhoen,presn,rhoin
  real(rp)        :: vx,vy,vz,dirx,diry,dirz,tx,ty,tz,vtx,vty,vtz,tl,vns,vninf,vtinf,vtinfx,vtinfy,vtinfz
  real(rp)        :: p,c00,c10,c05,c20,gam1i,gam1,gam2,epsil,c,cn,cbar,mach,rhocbar,vn,vt,pinft,rhoinft,adia,prho
  !
  !     This sub handles the characteristics boundary conditions
  !

  !     -1: inflow characteristic, everything imposed
  !     -2: inflow characteristic, total pressure imposed
  !     -3: outflow characteristic, everything imposed
  !     -4: outflow characteristic, pressure imposed
  !     -5: outflow characteristic, mach number imposed imposed
  !     -6: outflow characteristic, mass flux imposed

  c00   = 0.0d+00
  c05   = 0.5d+00
  c10   = 1.0d+00
  c20   = 2.0d+00
  gam1  = gam-c10 
  gam2  = c20*gam 
  gam1i = c10/(gam1)
  epsil = 1.0d-08
  !
  !     Which kind of data do we have?
  !
  isurf=lboup(3,ipo) 
  icond=-lbous(1,isurf)
  !
  !     Get normal
  !
  dirx=rdir(1,ipo)
  diry=rdir(2,ipo)
  dirz=rdir(3,ipo)
  !
  !     Orientation of normals outside, change for inside
  !
  dirx=-dirx
  diry=-diry
  dirz=-dirz
  !
  !     Inflow or outflow?
  !
  if(icond==1 .or. icond==2)then
     inflow=1_ip
       else
     inflow=0_ip
  endif
 
  if(lbous(2,isurf)==1)then 
     !
     !     States are given as input
     !
     if(icond==1 .or. icond==2 .or. icond==3)then

        rhoinf= rbous(1,isurf)
        vinfx = rbous(2,isurf)
        vinfy = rbous(3,isurf)
        vinfz = rbous(4,isurf)
        pinf  = rbous(5,isurf)
        !
        !     Compute vninf and vtinf
        !
        vninf=vinfx*dirx+vinfy*diry+vinfz*dirz
        vtinfx=vinfx-vninf*dirx
        vtinfy=vinfy-vninf*diry
        vtinfz=vinfz-vninf*dirz

     else if(icond==4 )then

        pinf  = rbous(1,isurf)

     else if(icond==5 )then

        minf  = rbous(1,isurf)

     else if(icond==6)then

        rhoinf= rbous(1,isurf)
        vinfx = rbous(2,isurf)
        vinfy = rbous(3,isurf)
        vinfz = rbous(4,isurf)
        !
        !     Compute vninf and vtinf
        !
        vninf=vinfx*dirx+vinfy*diry+vinfz*dirz
        vtinfx=vinfx-vninf*dirx
        vtinfy=vinfy-vninf*diry
        vtinfz=vinfz-vninf*dirz

     endif

  else
     !
     !     States are given as initial conditions
     !
     rhoinf=varb(1,ipo)
     vinfx=varb(2,ipo)/rhoinf
     vinfy=varb(3,ipo)/rhoinf
     vinfz=varb(4,ipo)/rhoinf
     rhoeinf=varb(5,ipo)
     pinf=gam1*(rhoeinf-rhoinf*c05*(vinfx*vinfx+vinfy+vinfy+vinfz*vinfz))
     cinf=sqrt(gam*pinf/rhoinf)
     minf=sqrt(vinfx*vinfx+vinfy*vinfy+vinfz*vinfz)/cinf 
     !
     !     Compute vninf and vtinf
     !
     vninf=vinfx*dirx+vinfy*diry+vinfz*dirz
     vtinfx=vinfx-vninf*dirx
     vtinfy=vinfy-vninf*diry
     vtinfz=vinfz-vninf*dirz

  endif
   
  !
  !     Get point number
  !
  ipoin=lboup(1,ipo)
  !
  !     Get the relevant quantities 
  !
  rho=var(1,ipoin)+dvar(1,ipoin)
  rhou=var(2,ipoin)+dvar(2,ipoin)
  rhov=var(3,ipoin)+dvar(3,ipoin)
  rhow=var(4,ipoin)+dvar(4,ipoin)
  rhoe=var(5,ipoin)+dvar(5,ipoin)
  rhoi=c10/rho

  rhon=varn(1,ipoin)
  rhoun=varn(2,ipoin)
  rhovn=varn(3,ipoin)
  rhown=varn(4,ipoin)
  rhoen=varn(5,ipoin)
  presn=varn(6,ipoin)
  rhoin=c10/rhon
  !
  !     Actual velocity
  !
  vx=rhou*rhoi
  vy=rhov*rhoi
  vz=rhow*rhoi
  pres=gam1*(rhoe-rho*c05*(vx*vx+vy*vy+vz*vz))
  !
  !     Compute speed of sound
  !
  c=sqrt(gam*pres*rhoi)
  cn=sqrt(gam*presn*rhoin)
  !
  !     Compute bar states
  ! 
  cbar=c05*(c+cn)
  rhocbar=c05*(rho*c+rhon*cn)
  !
  !     Compute normal and tangential velocity
  !
  vns=dirx*vx+diry*vy+dirz*vz
  vtx=vx-vns*dirx 
  vty=vy-vns*diry 
  vtz=vy-vns*dirz
  !
  !     Compute Mach number given normal at point
  !
  mach=vns/c   
  !
  !     Inflow or outflow ?
  !
  if(inflow)then
     !
     !     Inflow, take appropriate measures
     !
     if(mach>c10)then
        !
        !     Supersonic inflow, everything imposed
        !
        dvar(1,ipoin)=c00 
        dvar(2,ipoin)=c00 
        dvar(3,ipoin)=c00 
        dvar(4,ipoin)=c00 
        dvar(5,ipoin)=c00

     else

        !
        !  Subsonic inflow, what is imposed?
        ! 
        if(icond==1)then
           !
           !     Prescribed roinf, vtinf, vninf, pinf
           !
           p=c05*(pinf+pres+rhocbar*(vninf-vns))
           vn=vninf+(pinf-p)/rhocbar
           rho=rhoinf+(p-pinf)/(cbar*cbar)

        else if(icond==2)then   
           !
           !     Prescribed total pressure 
           !

           adia=pinf/(rhoinf**gam)
           prho=pinf/rhoinf+(gam1/gam2)*(vninf*vninf-vns*vns)
           rhoinft=(prho/adia)**gam1i
           pinft=(rho**gam)*adia

           p=c05*(pinft+pres)
           vn=vns
           rho=rhoinft+(p-pinft)/(cbar*cbar)

        else

           write(*,*)'Inflow characteristic bc unknown'
           stop

        endif
        !
        !     Apply on dvar
        !
        vx=vn*dirx+vtinfx
        vy=vn*diry+vtinfy
        vz=vn*dirz+vtinfz
        rhoe=p*gam1i+c05*rho*(vx*vx+vy*vy+vz*vz) 
        dvar(1,ipoin)=rho-rhon
        dvar(2,ipoin)=rho*vx-rhoun
        dvar(3,ipoin)=rho*vy-rhovn
        dvar(4,ipoin)=rho*vz-rhown
        dvar(5,ipoin)=rhoe-rhoen

     endif

  else 

     !
     !     We are dealing with an outflow
     !
     if(mach>c10)then
        !
        !     Supersonic outflow, nothing to do
        !

     else
        !
        !     Subsonic outflow, what is imposed?
        !
        if(icond==3)then
           !
           !     Prescribed vninf,pinf
           !
           p=c05*(pinf+pres+rhocbar*(vninf-vns))
           rho=rho+(p-pres)/(cbar*cbar)
           vn=vninf+(pinf-p)/rhocbar

        else if(icond==4)then   
           !
           !     Prescribed pinf
           !

           p=pinf
           rho=rho+(pinf-pres)/(cbar*cbar)
           vn=vns+(pinf-pres)/rhocbar

        else if(lboup(3,ipo)==5)then   
           !
           !     Prescribed mach
           !

           vn=-c*minf
           p=pres+rho*c*(vn-vns)
           rho=rho+(p-pres)/(cbar*cbar)

        else if(icond==6)then   
           !
           !     Prescribed mass flux
           !
           p=pres+c*(rhoinf*vninf-rho*vn)
           rho=rho+(p-pres)/(cbar*cbar)
           vn=rhoinf*vninf/rho
        
        else
        
           write(*,*)'Outflow characteristic bc unknown'
           stop

        endif

        vx=vn*dirx+vtx
        vy=vn*diry+vty
        vz=vn*dirz+vtz
        rhoe=p*gam1i+c05*rho*(vx*vx+vy*vy+vz*vz) 
        dvar(1,ipoin)=rho-rhon
        dvar(2,ipoin)=rho*vx-rhoun
        dvar(3,ipoin)=rho*vy-rhovn
        dvar(4,ipoin)=rho*vz-rhown
        dvar(5,ipoin)=rhoe-rhoen

     endif

  endif

end subroutine invar

  
subroutine hydreq(nedge,ledge,ndim,hydro1,hydro2,grad,edsha,npoin,ledgb,nedgb,edshab,edshap,nboup,lboup,rmass,grav,cpi,cp,R,gam,coor,ierr,mboup,cv)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_nasedg, only        : memor_edg
  implicit none
  real(rp),intent(in)       :: grav,cpi,cp,R,gam,cv 
  integer(ip),intent(in)  :: nedge,ndim,npoin,nedgb,nboup,mboup
  integer(ip),intent(in)  :: ledge(2,nedge),ledgb(2,nedgb),lboup(6,mboup)
  real(rp),intent(in)     :: edsha(5,nedge),edshab(3,nedgb),edshap(3,nboup),rmass(npoin),coor(ndim,npoin)
  real(rp),intent(inout)  :: grad(ndim,6,npoin),hydro1(2,nedge),hydro2(2,nedge)
  integer(ip),intent(inout) :: ierr
  real(rp)                :: rsum1,rsum2,rsum3,rsum4,rsum5,rtemp
  real(rp)                :: redg11,redg12,redg13,redg21,redg22,redg23
  real(rp)                :: redg31,redg32,redg33,redg41,redg42,redg43
  real(rp)                :: redg51,redg52,redg53,c00,coef1,coef2,coef3
  integer(ip)             :: ipoin,iedge,ip1,ip2,ipo
  real(rp)                :: temp,pres,temp0,pres0,rho,rhoi
  real(rp)                :: c20,k,dirx,diry,dirz,c10,c14 
  real(rp)                :: rho1,rhou1,rhov1,rhow1,rhoe1
  real(rp)                :: rho2,rhou2,rhov2,rhow2,rhoe2
  real(rp)                :: drho,drhou,drhov,drhow,drhoe
  real(rp)                :: bgra1,bgra2,bgra3,bgra4,bgra5 
  real(rp)                :: fgra1,fgra2,fgra3,fgra4,fgra5 
  real(rp)                :: s11,s12,s13,s14,s15,s21,s22,s23,s24,s25 
  real(rp)                :: epsil,z,rhoe,gam1,freq,freq2,presex
  real(rp)                :: c05,u
  real(rp), pointer       :: rtmp(:,:)
  integer(4)                 :: istat
  !
  !     Compute hydrostatic approximation for extrapolated density and pression
  !

  c10=1.0d+00
  c05=0.5d+00
  c00=0.0d+00
  gam1=gam-c10
  !temp0=300.0d+00
  !pres0=100000.0d+00
  temp0=280.0d+00
  pres0=100000.0d+00
  freq=0.01d+00
  freq2=freq*freq
  !
  !     Allocate rtemp
  !
  allocate(rtmp(2,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'RTMP','hydreq',rtmp)
  !
  !     Compute hydrostatic approximation for density and energy
  !

  do ipoin=1,npoin

     z=coor(3,ipoin)
     !
     !     Straka test case
     !
     !temp=temp0-grav*z*cpi
     !pres=pres0*(temp/temp0)**(cp/R)
     !rho=pres/(R*temp)
     !rhoe=pres/(gam-c10)
     !
     !     Schar mountain test case
     !
     !Potential temperature
     temp=temp0*exp(freq2*z/grav)
     !Exner pressure
     presex=1+((grav*grav)/(cp*temp0*freq2))*(exp(-freq2*z/grav)-c10)
     !Density
     rho=(pres0/(R*temp))*presex**(cv/R)
     rhoi=c10/rho
     !Pressure
     pres=pres0*(rho*R*temp/pres0)**(cp/cv)
     !Velocity
     u=10.0d+00
     !Energy
     rhoe=rho*cv*temp*presex+c05*rho*u*u


     rtmp(1,ipoin)=rho
     rtmp(2,ipoin)=rhoe

  enddo

 !
 !     Get the gradients of the density and energy
 !


  !
  !     Put grad to zero
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(grad,npoin,c00) &
!$omp& private(ipoin)
!
  do ipoin=1,npoin
     grad(1,1,ipoin)=c00
     grad(2,1,ipoin)=c00
     grad(3,1,ipoin)=c00
     grad(1,2,ipoin)=c00
     grad(2,2,ipoin)=c00
     grad(3,2,ipoin)=c00
  enddo
  !
  !     Compute the projection of the gradient of the unknowns
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,rtmp,grad) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rsum1,rsum2,rsum3,rsum4,rsum5,&  
!$omp&                       redg11,redg12,redg13,redg21,redg22,redg23,redg31,redg32,redg33,& 
!$omp&                       redg41,redg42,redg43,redg51,redg52,redg53 )
!
  do iedge=1,nedge

     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)

     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)

     rsum1=rtmp(1,ip1)+rtmp(1,ip2)
     rsum2=rtmp(2,ip1)+rtmp(2,ip2)
     
     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1

     redg21=coef1*rsum2
     redg22=coef2*rsum2
     redg23=coef3*rsum2
     
     !
     !     Obtain the final gradients
     !

     grad(1,1,ip1)=grad(1,1,ip1)-redg11
     grad(2,1,ip1)=grad(2,1,ip1)-redg12
     grad(3,1,ip1)=grad(3,1,ip1)-redg13
     grad(1,2,ip1)=grad(1,2,ip1)-redg21
     grad(2,2,ip1)=grad(2,2,ip1)-redg22
     grad(3,2,ip1)=grad(3,2,ip1)-redg23
     
     grad(1,1,ip2)=grad(1,1,ip2)+redg11
     grad(2,1,ip2)=grad(2,1,ip2)+redg12
     grad(3,1,ip2)=grad(3,1,ip2)+redg13
     grad(1,2,ip2)=grad(1,2,ip2)+redg21
     grad(2,2,ip2)=grad(2,2,ip2)+redg22
     grad(3,2,ip2)=grad(3,2,ip2)+redg23
    
  enddo

  !
  !     Boundary edge contribution 
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedgb,ledgb,edshab,grad,rtmp) &
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rsum1,rsum2,rsum3,rsum4,rsum5,&  
!$omp&                       redg11,redg12,redg13,redg21,redg22,redg23,redg31,redg32,redg33,& 
!$omp&                       redg41,redg42,redg43,redg51,redg52,redg53 )

  do iedge=1,nedgb

     ip1=ledgb(1,iedge)
     ip2=ledgb(2,iedge)

     coef1=edshab(1,iedge)
     coef2=edshab(2,iedge)
     coef3=edshab(3,iedge)

     rsum1=rtmp(1,ip1)+rtmp(1,ip2)
     rsum2=rtmp(2,ip1)+rtmp(2,ip2)
     
     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1

     redg21=coef1*rsum2
     redg22=coef2*rsum2
     redg23=coef3*rsum2
     
     !
     !     Obtain the final gradients
     !

     grad(1,1,ip1)=grad(1,1,ip1)+redg11
     grad(2,1,ip1)=grad(2,1,ip1)+redg12
     grad(3,1,ip1)=grad(3,1,ip1)+redg13
     grad(1,2,ip1)=grad(1,2,ip1)+redg21
     grad(2,2,ip1)=grad(2,2,ip1)+redg22
     grad(3,2,ip1)=grad(3,2,ip1)+redg23
     

     grad(1,1,ip2)=grad(1,1,ip2)+redg11
     grad(2,1,ip2)=grad(2,1,ip2)+redg12
     grad(3,1,ip2)=grad(3,1,ip2)+redg13
     grad(1,2,ip2)=grad(1,2,ip2)+redg21
     grad(2,2,ip2)=grad(2,2,ip2)+redg22
     grad(3,2,ip2)=grad(3,2,ip2)+redg23
     
  enddo

  !
  !     Boundary point contribution
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nboup,lboup,edshap,rtmp,grad) &
!$omp& private(ipo,ipoin,coef1,coef2,coef3,rsum1,rsum2,rsum3,rsum4,rsum5,&  
!$omp&                       redg11,redg12,redg13,redg21,redg22,redg23,redg31,redg32,redg33,& 
!$omp&                       redg41,redg42,redg43,redg51,redg52,redg53 )
 
  do ipo=1,nboup

     ipoin=lboup(1,ipo)

     coef1=edshap(1,ipo)
     coef2=edshap(2,ipo)
     coef3=edshap(3,ipo)


     rsum1=rtmp(1,ipoin)
     rsum2=rtmp(2,ipoin)


     redg11=coef1*rsum1
     redg12=coef2*rsum1
     redg13=coef3*rsum1

     redg21=coef1*rsum2
     redg22=coef2*rsum2
     redg23=coef3*rsum2
     
     !
     !     Obtain the final gradients
     !

     grad(1,1,ipoin)=grad(1,1,ipoin)-redg11
     grad(2,1,ipoin)=grad(2,1,ipoin)-redg12
     grad(3,1,ipoin)=grad(3,1,ipoin)-redg13

     grad(1,2,ipoin)=grad(1,2,ipoin)-redg21
     grad(2,2,ipoin)=grad(2,2,ipoin)-redg22
     grad(3,2,ipoin)=grad(3,2,ipoin)-redg23
     
  enddo

  !
  !     Multiply by the inverse of the lumped mass matrix
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,rmass,grad) &
!$omp& private(ipoin,rtemp)

  do ipoin=1,npoin
     rtemp=rmass(ipoin)
     grad(1,1,ipoin)=rtemp*grad(1,1,ipoin)
     grad(2,1,ipoin)=rtemp*grad(2,1,ipoin)
     grad(3,1,ipoin)=rtemp*grad(3,1,ipoin)
     grad(1,2,ipoin)=rtemp*grad(1,2,ipoin)
     grad(2,2,ipoin)=rtemp*grad(2,2,ipoin)
     grad(3,2,ipoin)=rtemp*grad(3,2,ipoin)
  enddo


 !
 !     Limit the values of density and energy  
 !

!
  !     Define extrapolation order
  !
  k=0.33333d+00
  c20=2.0d+00
  c00=0.0d+00
  c10=1.0d+00
  c14=1.0d+00/4.0d+00
  epsil=1.0d-18

  do iedge=1,nedge

     !
     !     Points of the edge
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Edge direction
     !
     dirx=coor(1,ip2)-coor(1,ip1)
     diry=coor(2,ip2)-coor(2,ip1)
     dirz=coor(3,ip2)-coor(3,ip1)
     !
     !     Get relevant quantities 
     !
     rho1=rtmp(1,ip1)
     rhoe1=rtmp(2,ip1)


     rho2=rtmp(1,ip2)
     rhoe2=rtmp(2,ip2)

     !
     !     Delta edge
     !
     drho=rho2-rho1
     drhoe=rhoe2-rhoe1


     !
     !     Backward gradient at ip1
     !

     bgra1=c20*(grad(1,1,ip1)*dirx+grad(2,1,ip1)*diry+grad(3,1,ip1)*dirz)-drho
     bgra5=c20*(grad(1,2,ip1)*dirx+grad(2,2,ip1)*diry+grad(3,2,ip1)*dirz)-drhoe


     !
     !     Forward gradient at ip2
     !

     fgra1=c20*(grad(1,1,ip2)*dirx+grad(2,1,ip2)*diry+grad(3,1,ip2)*dirz)-drho
     fgra5=c20*(grad(1,2,ip2)*dirx+grad(2,2,ip2)*diry+grad(3,2,ip2)*dirz)-drhoe

     !
     !     Get the Van Albada slope limiter 
     !
     s11=max(c00,(c20*bgra1*drho)/(bgra1*bgra1+drho*drho+epsil))
     s15=max(c00,(c20*bgra5*drhoe)/(bgra5*bgra5+drhoe*drhoe+epsil))

     s21=max(c00,(c20*fgra1*drho)/(fgra1*fgra1+drho*drho+epsil))
     s25=max(c00,(c20*fgra5*drhoe)/(fgra5*fgra5+drhoe*drhoe+epsil))

     !
     !     Finally compute the limited extrapolated values
     !

     hydro1(1,iedge)=rho1+c14*s11*((c10-k*s11)*bgra1+(c10+k*s11)*drho)
     hydro1(2,iedge)=rhoe1+c14*s15*((c10-k*s15)*bgra5+(c10+k*s15)*drhoe)

     hydro2(1,iedge)=rho2-c14*s21*((c10-k*s21)*fgra1+(c10+k*s21)*drho)
     hydro2(2,iedge)=rhoe2-c14*s25*((c10-k*s25)*fgra5+(c10+k*s25)*drhoe)



  enddo


 !
 !     Finally get the pressure
 !

  !
  !     For the moment, perfect gas
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(hydro1,hydro2,nedge,gam1,ierr,c00) &
!$omp& private(ipoin,rho,rhoe,pres)
!
  do iedge=1,nedge

     rho=hydro1(1,iedge)
     rhoe=hydro1(2,iedge)

     !
     !     Test everything is ok
     !
     if(rho<c00)then
        write(*,*)'negative density point:',ipoin
        ierr=1
     endif

     if(rhoe<c00)then
        write(*,*)'negative energy point:',ipoin
        ierr=1
     endif

     pres=gam1*rhoe
     hydro1(2,iedge)=pres

  enddo

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(hydro1,hydro2,nedge,gam1,ierr,c00) &
!$omp& private(ipoin,rho,rhoe,pres)
!
  do iedge=1,nedge

     rho=hydro2(1,iedge)
     rhoe=hydro2(2,iedge)

     !
     !     Test everything is ok
     !
     if(rho<c00)then
        write(*,*)'negative density point:',ipoin
        ierr=1
     endif

     if(rhoe<c00)then
        write(*,*)'negative energy point:',ipoin
        ierr=1
     endif

     pres=gam1*rhoe
     hydro2(2,iedge)=pres

  enddo

  call memchk(2_ip,istat,memor_edg,'RTMP','hydreq',rtmp)
  deallocate(rtmp,stat=istat)
  if(istat/=0) call memerr(2_ip,'RTMP','hydreq',0_ip)


end subroutine hydreq







