subroutine chkgrad(nedge,edsha,nedgb,edshab,nboup,edshap,npoin,mboup,ledge,lboup,ledgb)
  use def_kintyp, only       :  ip,rp,lg
  use def_nasedg, only          : memor_edg
  use mod_memchk
  implicit none
  integer(ip),intent(in)     :: nedge,nedgb,nboup
  integer(ip),intent(in)     :: npoin,mboup
  integer(ip),intent(in)     :: ledge(2,nedge),ledgb(2,nedgb)
  integer(ip),intent(in)     :: lboup(6,mboup)
  real(rp),intent(inout)     :: edsha(5,nedge)
  real(rp),intent(inout)     :: edshab(3,nedgb),edshap(3,nboup)
  real(rp),pointer           :: var(:,:)
  integer(ip)                :: ipoin,iedge,ip1,ip2
  real(rp)                   :: c00,rl 
  integer(4)                 :: istat
!
!     This sub checks the orientation of the normals
!     by computing the gradient of the unity function 
!     We expect the normal to be oriented outwards
!

 c00=0.0d+00
!
!     Allocate var
!
  allocate(var(3,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'VAR','chkgrad',var)

 do ipoin=1,npoin
     var(1,ipoin)=c00
     var(2,ipoin)=c00
     var(3,ipoin)=c00
  enddo


  do iedge=1,nedge
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)

     var(1,ip1)=var(1,ip1)-1.0*2.0d+00*edsha(2,iedge)
     var(2,ip1)=var(2,ip1)-1.0*2.0d+00*edsha(3,iedge)
     var(3,ip1)=var(3,ip1)-1.0*2.0d+00*edsha(4,iedge)
     var(1,ip2)=var(1,ip2)+1.0*2.0d+00*edsha(2,iedge)
     var(2,ip2)=var(2,ip2)+1.0*2.0d+00*edsha(3,iedge)
     var(3,ip2)=var(3,ip2)+1.0*2.0d+00*edsha(4,iedge)
  enddo

  do iedge=1,nedgb
     ip1=ledgb(1,iedge)
     ip2=ledgb(2,iedge)

     var(1,ip1)=var(1,ip1)+1.0*2.0d+00*edshab(1,iedge)
     var(2,ip1)=var(2,ip1)+1.0*2.0d+00*edshab(2,iedge)
     var(3,ip1)=var(3,ip1)+1.0*2.0d+00*edshab(3,iedge)
     var(1,ip2)=var(1,ip2)+1.0*2.0d+00*edshab(1,iedge)
     var(2,ip2)=var(2,ip2)+1.0*2.0d+00*edshab(2,iedge)
     var(3,ip2)=var(3,ip2)+1.0*2.0d+00*edshab(3,iedge)

  enddo
  
do ipoin=1,nboup

     ip1=lboup(1,ipoin)

     var(1,ip1)=var(1,ip1)-1.0*edshap(1,ipoin)
     var(2,ip1)=var(2,ip1)-1.0*edshap(2,ipoin)
     var(3,ip1)=var(3,ip1)-1.0*edshap(3,ipoin)

  enddo

  do ipoin=1,npoin

     rl=var(1,ipoin)*var(1,ipoin)+var(2,ipoin)*var(2,ipoin)+var(3,ipoin)*var(3,ipoin)
     rl=sqrt(rl)

     if(rl>1.0d-05)then 

        write(*,*)'Error in chkgrad, chk normal orientation'
        write(*,*)ipoin,var(1,ipoin),var(2,ipoin),var(3,ipoin)
        stop 

     endif
  enddo

  call memchk(2_ip,istat,memor_edg,'VAR','chkgrad',var)
  deallocate(var,stat=istat)
  if(istat/=0) call memerr(2_ip,'VAR','chkgrad',0_ip)

end subroutine chkgrad


subroutine chkini(npoin,nvar,var,gam)
use def_kintyp, only       :  ip,rp,lg
  use def_nasedg, only          : memor_edg
  use mod_memchk
  implicit none
  integer(ip),intent(in)     :: npoin,nvar
  real(rp),intent(in)        :: var(nvar,npoin),gam
  integer(ip)                :: ipoin
  real(rp)                   :: rho,rhou,rhov,rhow,rhoe,pres,rhoi,rtol,prest,c10
  real(rp)                   :: u,v,w,c05
  !
  !     This sub checks the validity of the initialization
  !
  rtol=1.0d-06
  c10=1.0d+00
  c05=0.5d+00

  do ipoin=1,npoin
    rho  = var(1,ipoin)
    rhou = var(2,ipoin)
    rhov = var(3,ipoin)
    rhow = var(4,ipoin)
    rhoe = var(5,ipoin)
    pres = var(6,ipoin) 
    rhoi = var(7,ipoin)
   
    if(abs((c10/rho)-rhoi)>rtol)then
       write(*,*)'Error in density initialization'
       stop
    endif
   
    u=rhou*rhoi
    v=rhov*rhoi
    w=rhow*rhoi


 
    prest=(gam-c10)*(rhoe-c05*rho*(u*u+v*v+w*w))
    if(abs(pres-prest)>rtol)then
       write(*,*)'Error in pressure initialization'
       stop
    endif 

  enddo

end subroutine chkini



