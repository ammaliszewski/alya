!
!          for the preconditioned euler equations
!                  ===============
!
!
!          for the Merkle preconditioning, Gamma is:
!
!
!          theta       0       0       0       rhot
!          theta u     rho     0       0       rhot u
!          theta v     0       rho     0       rhot v
!          theta w     0       0       rho     rhot w
!          theta H -(1-rho*H_p)  rho u   rho v   rho w   rhot H + rho H_t
!
!          with theta=1/Ur2-rhot/(rho*Cp)
!
!          Gamma^-1 is:
!
!          (B'-rhot q2)/C'   u*rhot/C'         v*rhot/C'       w*rhot/C'       -rhot/C'
!          -u/rho            1/rho             0               0               0
!          -v/rho            0                 1/rho           0               0
!          -w/rho            0                 0               1/rho           0
!          (q2*theta-A')/C'  -u*theta/C'       -v*theta/C'     -w*theta/C'     theta/C'
!
!
!          with:
!
!          A'=theta*H-(1-rho*Hp)
!          B'=rhot*H+rho*Ht
!          C'=rho*Ht*theta+rhot-rho*Hp*rhot
!
!
!         The eigenvalues are:
!
!         (v'-c',v,v,v,v'+c')
!
!         where
!
!         v'=v(1-alpha)
!         c'=sqrt(alpha^2 v^2+Vr^2 )
!         alpha=(1-beta Vr^2)/2
!         beta=rhop +rhot/(rho*Cp)
!
!         PG: Ht=Cp
!             H_p=0
!             beta=rho/p-1/(CpT)=rho/p -1/h
!                 =1/(gamma R T)
!                 =1/c2
!
!         In multiplicating the residual, the matrix is dQ_c/dQ_v Gamma^-1:
!
!
!           K*D+C              u*D               v*D             w*D             -D
!           u*J               u^2*D+C'          uv*D            uw*D           -uD
!  1/C'*    v*J               vu*D              v^2D+C'         vwD            -vD
!           w*J               wu*D              wv*D            w^2*D+C'       -wD
!           F                 u*G               v*G             w*G            -G+C'
!
!         with E=rhop-theta
!              D=rhot*E
!              F=H*J
!              G=H*D
!              I=rhot*K+rho*H_T
!              J=E*I
!              K=H-q^2
!
!
!
subroutine getdtpre(nedge,ledge,edsha,gam,var,nvar,npoin,dt,coor,ndim,cfl,niter,vn,viscp,rmass,ivisc,rdtloc,idtloc)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nedge,npoin,nvar,ndim,niter,ivisc,idtloc
  integer(ip), intent(in)    :: ledge(2,nedge)
  real(rp), intent(in)       :: edsha(5,nedge),gam,var(nvar,npoin),coor(ndim,npoin),cfl,vn,viscp(2,npoin),rmass(npoin)
  real(rp), intent(inout)    :: dt,rdtloc(npoin)
  real(rp)     :: rnl,coef1,coef2,coef3,rho1,rhou1,rhov1,rhow1,rhoe1,rho2
  real(rp)     :: rhou2,rhov2,rhow2,rhoe2,pres1,pres2,rhoi1,rhoi2,rl
  real(rp)     :: lambda,rho,rhou,rhov,rhow,rhoi,u,v,w,pres,c,vedg,lambdamax,vedgpre,cpre,cm13
  real(rp)     :: c05,c10,rx,ry,rz,length,lenm,c00,vedg2,beta,vinf,epsilpre,vr,vr2,alpha,alpha2,dtloc,mu,mu1,mu2,dtvisc,rmas1,rmas2,rmas
  integer(ip)  :: iedge,ip1,ip2,i,j,ipoin

  c05=0.5d+00
  cm13=-1.0d+00/3.0d+00
  c10=1.0d+00
  c00=0.0d+00
  epsilpre=1.0d-05

  dt=1.0d+12
  rdtloc=dt

  lambdamax=c00

!$omp parallel do &
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,coor,var,c10,c05,gam,epsilpre,ivisc,rmass,viscp,vn,cm13,cfl,rdtloc) &
!$omp private(iedge,ip1,ip2,coef1,coef2,coef3,rl,rx,ry,rz,length,&
!$omp&        rho1,rhou1,rhov1,rhow1,pres1,rho2,rhou2,rhov2,rhow2,pres2,&
!$omp&        rho,rhoi,rhou,rhov,rhow,pres,u,v,w,vedg,c,lambda,&
!$omp&        vedg2,beta,vr,vr2,alpha,alpha2,vedgpre,cpre,vinf,dtvisc,dtloc,&
!$omp&        mu1,mu2,mu,rmas1,rmas2,rmas)


  do iedge=1,nedge

     !
     !     Points of the edge
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Geometric coefficients
     !
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)
     rl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3)
     rl=c10/rl
     !
     !     Edge length
     !
     rx=coor(1,ip1)-coor(1,ip2)
     ry=coor(2,ip1)-coor(2,ip2)
     rz=coor(3,ip1)-coor(3,ip2)
     length=sqrt(rx*rx+ry*ry+rz*rz)
     !
     !     Get relevant quantities
     !
     rho1=var(1,ip1)
     rhou1=var(2,ip1)
     rhov1=var(3,ip1)
     rhow1=var(4,ip1)
     pres1=var(6,ip1)
     mu1=viscp(1,ip1)
     rmas1=rmass(ip1)**cm13

     rho2=var(1,ip2)
     rhou2=var(2,ip2)
     rhov2=var(3,ip2)
     rhow2=var(4,ip2)
     pres2=var(6,ip2)
     mu2=viscp(1,ip2)
     rmas2=rmass(ip2)**cm13

     rho=c05*(rho1+rho2)
     rhoi=c10/rho
     rhou=c05*(rhou1+rhou2)
     rhov=c05*(rhov1+rhov2)
     rhow=c05*(rhow1+rhow2)
     pres=c05*(pres1+pres2)
     mu=c05*(mu1+mu2)
     rmas=c05*(rmas1+rmas2)

     u=rhou*rhoi
     v=rhov*rhoi
     w=rhow*rhoi
     !
     !     Velocity in the edge direction
     !
     vedg=abs(coef1*u+coef2*v+coef3*w)*rl
     vedg2=vedg*vedg
     !
     !     Speed of sound
     !
     c=gam*pres*rhoi
     beta=c10/c                !Perfect Gas  assumption
     c=sqrt(c)   
     !
     !     -----set vinf
     !
     vinf=epsilpre*c
     !
     !     -----compute reference velocity for Euler
     !          if supersonic, vr=csoun
     !
     vr=min(c,max(vedg,vinf))
     !
     !     -----compute reference velocity for NS
     !
     if(ivisc==1)then
        vr=max(vr,mu/(rho*length))
     endif
     vr2=vr*vr
     !
     !     -----compute alpha
     !          if supersonic, alpha=0 and the preconditioning is unity
     !
     alpha=c05*(c10-beta*vr2)
     alpha2=alpha*alpha
     !
     !     -----compute pseudo speed of sound
     !
     cpre=sqrt(alpha2*vedg2+vr2)
     !
     !     -----compute pseudo velocity
     !
     vedgpre=vedg*(c10-alpha)
     !
     !     Largest eigenvalue 
     !
     lambda=vedgpre+cpre
     dtloc=cfl*length/lambda

     if(ivisc==1)then
        !
        !     Viscous dt
        !
        dtvisc=vn*rho*rmas/(abs(edsha(5,iedge))*mu)
        dtloc=min(dtloc,dtvisc)
    endif

    rdtloc(ip1)=min(dtloc,rdtloc(ipoin))
    rdtloc(ip2)=min(dtloc,rdtloc(ipoin))
     
  enddo

  !
  !     Compute time step
  !

  !dt=cfl*lenm/lambdamax

  if(niter<5)then
     do ipoin=1,npoin
        rdtloc(ipoin)=0.2d+00*rdtloc(ipoin)
     enddo
  endif

  !
  !     Do we want local or global time stepping
  !
  if(idtloc==0)then
     dt=rdtloc(1)
     do ipoin=2,npoin
        dt=min(dt,rdtloc(ipoin))
     enddo

     do ipoin=1,npoin
        rdtloc(ipoin)=dt
     enddo

  endif


end subroutine getdtpre

subroutine gtgradpre(nvar,ndim,var,grad,npoin,cvi)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: nvar,ndim,npoin
  real(rp),intent(in)     :: var(nvar,npoin),cvi
  real(rp),intent(inout)  :: grad(ndim,6,npoin)
  integer(ip)             :: ipoin
  real(rp)             :: rhoi,u,v,w,e,drhox,drhoy,drhoz  
  real(rp)             :: dux,duy,duz,dvx,dvy,dvz,dwx,dwy,dwz,dex,dey,dez
  real(rp)             :: dtx,dty,dtz

  !
  !     This subroutine computes the gradient of the viscous variables
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(npoin,var,grad,cvi) &
!$omp  private(ipoin,rhoi,u,v,w,e,drhox,drhoy,drhoz,&  
!$omp&                       dux,duy,duz,dvx,dvy,dvz,dwx,dwy,dwz,dex,dey,dez,& 
!$omp&                       dtx,dty,dtz)
  !
  do ipoin=1,npoin
     !
     !     Get velocities and energy
     !
     rhoi=var(7,ipoin)
     u=var(2,ipoin)*rhoi
     v=var(3,ipoin)*rhoi
     w=var(4,ipoin)*rhoi
     e=var(5,ipoin)*rhoi
     !  
     !     -----and form the gradient of the non-conserved quantities
     !          with the gradient of the conserved quantities

     drhox = grad(1,1,ipoin)
     drhoy = grad(2,1,ipoin)
     drhoz = grad(3,1,ipoin)

     dux  = rhoi*( grad(1,2,ipoin) - u*drhox )
     duy  = rhoi*( grad(2,2,ipoin) - u*drhoy )
     duz  = rhoi*( grad(3,2,ipoin) - u*drhoz )
     dvx  = rhoi*( grad(1,3,ipoin) - v*drhox )
     dvy  = rhoi*( grad(2,3,ipoin) - v*drhoy )
     dvz  = rhoi*( grad(3,3,ipoin) - v*drhoz )
     dwx  = rhoi*( grad(1,4,ipoin) - w*drhox )
     dwy  = rhoi*( grad(2,4,ipoin) - w*drhoy )
     dwz  = rhoi*( grad(3,4,ipoin) - w*drhoz )
     dex  = rhoi*( grad(1,5,ipoin) - e*drhox )
     dey  = rhoi*( grad(2,5,ipoin) - e*drhoy )
     dez  = rhoi*( grad(3,5,ipoin) - e*drhoz )
     dTx  = cvi*(dex - (u*dux + v*dvx + w*dwx))
     dTy  = cvi*(dey - (u*duy + v*dvy + w*dwy))
     dTz  = cvi*(dez - (u*duz + v*dvz + w*dwz))
     !
     !     -----then form the viscous gradients
     !
     grad(1,1,ipoin) = grad(1,6,ipoin) 
     grad(2,1,ipoin) = grad(2,6,ipoin) 
     grad(3,1,ipoin) = grad(3,6,ipoin) 
     grad(1,2,ipoin) = dux
     grad(2,2,ipoin) = duy
     grad(3,2,ipoin) = duz
     grad(1,3,ipoin) = dvx
     grad(2,3,ipoin) = dvy
     grad(3,3,ipoin) = dvz
     grad(1,4,ipoin) = dwx
     grad(2,4,ipoin) = dwy
     grad(3,4,ipoin) = dwz
     grad(1,5,ipoin) = dtx
     grad(2,5,ipoin) = dty
     grad(3,5,ipoin) = dtz

  enddo

end subroutine gtgradpre

subroutine jstflupre(nedge,ledge,npoin,nvar,nflu,edsha,gam,var,sens,coor,ndim,flu,grad,cvi,cp,ivisc,viscp)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nedge,npoin,nvar,nflu,ndim,ivisc
  integer(ip), intent(in)    :: ledge(2,nedge)
  real(rp), intent(in)       :: edsha(5,nedge),gam,var(nvar,npoin),cvi,cp
  real(rp),intent(inout)     :: flu(nflu,npoin),grad(ndim,6,npoin) 
  real(rp), intent(in)       :: coor(ndim,npoin),viscp(2,npoin)
  real(rp), intent(inout)    :: sens(nedge)
  real(rp)     :: rnl,rnli,coef1,coef2,coef3,rho1,rhou1,rhov1,rhow1,rhoe1,rho2
  real(rp)     :: rhou2,rhov2,rhow2,rhoe2,pres1,pres2,rhoi1,rhoi2
  real(rp)     :: dirx,diry,dirz,rhoi
  real(rp)     :: c10,c00,c05,c20,c14,gam1,cm2
  real(rp)     :: redg1,redg2,redg3,redg4,redg5,vedg,pres12,pres22 
  real(rp)     :: redg1n,redg2n,redg3n,redg4n,redg5n 
  real(rp)     :: redg1d,redg2d,redg3d,redg4d,redg5d 
  real(rp)     :: radv1,radv2,rl
  real(rp)     :: f11,f12,f13,f14,f15,f21,f22,f23,f24,f25 
  real(rp)     :: lx,ly,lz,beta,pres,u,v,w,c,lambda,radv 
  real(rp)     :: du,dv,dw,dT,dpres4,du4,dv4,dw4,dT4,dpres 
  real(rp)     :: rho,rhou,rhov,rhow,rhoe,epsil,cpre,vedgpre 
  real(rp)     :: u1,v1,w1,u2,v2,w2,temp1,temp2,e1,e2
  real(rp)     :: vedg2,betap,vinf,epsilpre,vr,vr2,alpha,alpha2
  real(rp)     :: temp,e,rhot,theta,enthal
  real(rp)     :: ra,rb,rc,rq2,mu
  real(rp)     :: ax1,ax2,ax3,ax4,ax5,rhop,length 
  integer(ip)  :: iedge,ip1,ip2



  c00=0.0d+00
  c05=0.5d+00
  c10=1.0d+00
  c14=1.0d+00/4.0d+00
  c20=2.0d+00
  cm2=-2.0d+00
  epsil=1.0d-09
  epsilpre=1.0d-05



  !
  !     First get the pressure sensor
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,coor,sens,c10,c05,epsil) &
!$omp private(iedge,ip1,ip2,coef1,coef2,coef3,rnl,rnli,& 
!$omp&        dirx,diry,dirz,lx,ly,lz,pres1,pres2,dpres,dpres4)

  do iedge=1,nedge
     !
     !     Points of the edge 
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Geometric coefficients
     !
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)
     rnl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3) 
     rnli=c10/rnl

     dirx=coef1*rnli
     diry=coef2*rnli
     dirz=coef3*rnli

     lx=coor(1,ip2)-coor(1,ip1)
     ly=coor(2,ip2)-coor(2,ip1)
     lz=coor(3,ip2)-coor(3,ip1)
     !
     !     Pressure
     !    
     pres1=var(6,ip1)
     pres2=var(6,ip2)

     dpres=pres2-pres1
     dpres4=c05*(grad(1,6,ip1)+grad(1,6,ip2))*lx+(grad(2,6,ip1)+grad(2,6,ip2))*ly+(grad(3,6,ip1)+grad(3,6,ip2))*lz
     !
     !     Pressure sensor
     !
     sens(iedge)=c10-abs(dpres-dpres4)/(abs(dpres)+abs(dpres4)+epsil)

  enddo


  !
  !     Then compute the fluxes
  !

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,coor,sens,var,grad,flu,c10,c14,c05,gam,c00,cvi,epsilpre,cp,ivisc,viscp) &
!$omp private(iedge,ip1,ip2,coef1,coef2,coef3,rnl,rnli,& 
!$omp&        dirx,diry,dirz,lx,ly,lz,beta,rho1,u1,v1,w1,e1,rho2,u2,v2,w2,e2,&
!$omp&        rhoi1,rhoi2,pres,pres1,pres2,radv,redg1,redg2,redg3,redg4,redg5,rho,rhoi,rhou,&
!$omp&        rhov,rhow,u,v,w,vedg,c,lambda,dpres,du,dv,dw,dT,dpres4,du4,&
!$omp&        dv4,dw4,dT4,rhou1,rhov1,rhow1,rhoe1,rhou2,rhov2,rhow2,rhoe2,&
!$omp&        vedg2,betap,vinf,vr,vr2,alpha,alpha2,cpre,vedgpre,&
!$omp&        temp1,temp2,redg1d,redg2d,redg3d,redg4d,redg5d,&
!$omp&        redg1n,redg2n,redg3n,redg4n,redg5n,e,temp,theta,enthal,&
!$omp&        rhot,rhop,&
!$omp&        ra,rb,rc,rq2,&
!$omp&        ax1,ax2,ax3,ax4,ax5,mu,length)


  do iedge=1,nedge
     !
     !     Points of the edge 
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Geometric coefficients
     !
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)
     rnl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3) 
     rnli=c10/rnl

     dirx=coef1*rnli
     diry=coef2*rnli
     dirz=coef3*rnli

     lx=coor(1,ip2)-coor(1,ip1)
     ly=coor(2,ip2)-coor(2,ip1)
     lz=coor(3,ip2)-coor(3,ip1)
     length=sqrt(lx*lx+ly*ly+lz*lz)
     !
     !     Pressure sensor multiplied by c05 for fourth order terms
     !
     beta=sens(iedge)*c05     
     !
     !     Get relevant quantities 
     !
     rho1=var(1,ip1)
     rhou1=var(2,ip1)
     rhov1=var(3,ip1)
     rhow1=var(4,ip1)
     rhoe1=var(5,ip1)
     pres1=var(6,ip1)
     rhoi1=var(7,ip1)

     rho2=var(1,ip2)
     rhou2=var(2,ip2)
     rhov2=var(3,ip2)
     rhow2=var(4,ip2)
     rhoe2=var(5,ip2)
     pres2=var(6,ip2)
     rhoi2=var(7,ip2)
     !
     !     Compute advection and pressure
     !
     pres=pres1+pres2
     radv=c14*(coef1*(rhou1+rhou2)+coef2*(rhov1+rhov2)+coef3*(rhow1+rhow2))*(rhoi1+rhoi2)

     !
     !     Euler Galerkin Fluxes
     !
     redg1= radv*(rho1+rho2) 
     redg2= radv*(rhou1+rhou2)+coef1*pres
     redg3= radv*(rhov1+rhov2)+coef2*pres 
     redg4= radv*(rhow1+rhow2)+coef3*pres 
     redg5= radv*(rhoe1+rhoe2+pres) 
     !
     !     Get maximal eigenvalue
     !
     rho=c05*(rho1+rho2)
     rhoi=c10/rho
     rhou=c05*(rhou1+rhou2)
     rhov=c05*(rhov1+rhov2)
     rhow=c05*(rhow1+rhow2)
     pres=c05*pres

     u=rhou*rhoi
     v=rhov*rhoi
     w=rhow*rhoi
     !
     !     Velocity in the edge direction
     !
     vedg=abs(dirx*u+diry*v+dirz*w)*rnl
     vedg2=vedg*vedg
     !
     !     Speed of sound
     !
     c=gam*pres*rhoi
     betap=c10/c   
     c=sqrt(c)   
     !
     !     set vinf
     !
     vinf=epsilpre*c
     !
     !     compute reference velocity for Euler
     !          if supersonic, vr=csoun
     !
     vr=min(c,max(vedg,vinf))
     !
     !     compute reference velocity for NS
     !
     if(ivisc==1)then
         mu=c05*(viscp(1,ip1)+viscp(1,ip2))
         vr=max(vr,mu/(rho*length))
     endif
     vr2=vr*vr
     !
     !     compute alpha
     !          if supersonic, alpha=0 and the preconditioning is unity
     !
     alpha=c05*(c10-betap*vr2)
     alpha2=alpha*alpha
     !
     !     compute pseudo speed of sound
     !
     cpre=sqrt(alpha2*vedg2+vr2)
     !
     !     compute pseudo velocity
     !
     vedgpre=vedg*(c10-alpha)
     !
     !     Largest eigenvalue scaled 
     !
     lambda=(vedgpre+cpre)*rnl
     !
     !     Switch to viscous variables
     !
     u1=rhou1*rhoi1
     v1=rhov1*rhoi1
     w1=rhow1*rhoi1
     e1=rhoe1*rhoi1
     temp1=cvi*(e1-c05*(u1*u1+v1*v1+w1*w1))

     u2=rhou2*rhoi2
     v2=rhov2*rhoi2
     w2=rhow2*rhoi2
     e2=rhoe2*rhoi2
     temp2=cvi*(e2-c05*(u2*u2+v2*v2+w2*w2))

     u=c05*(u1+u2)
     v=c05*(v1+v2)
     w=c05*(w1+w2)
     temp=c05*(temp1+temp2)
     e=c05*(e1+e2)
     !
     !     Second order damping
     !
     dpres=pres2-pres1 
     du=u2-u1 
     dv=v2-v1 
     dw=w2-w1 
     dT=temp2-temp1 
     !
     !     Fourth order damping
     !
     dpres4=(grad(1,1,ip1)+grad(1,1,ip2))*lx+(grad(2,1,ip1)+grad(2,1,ip2))*ly+(grad(3,1,ip1)+grad(3,1,ip2))*lz       
     du4=(grad(1,2,ip1)+grad(1,2,ip2))*lx+(grad(2,2,ip1)+grad(2,2,ip2))*ly+(grad(3,2,ip1)+grad(3,2,ip2))*lz       
     dv4=(grad(1,3,ip1)+grad(1,3,ip2))*lx+(grad(2,3,ip1)+grad(2,3,ip2))*ly+(grad(3,3,ip1)+grad(3,3,ip2))*lz       
     dw4=(grad(1,4,ip1)+grad(1,4,ip2))*lx+(grad(2,4,ip1)+grad(2,4,ip2))*ly+(grad(3,4,ip1)+grad(3,4,ip2))*lz       
     dT4=(grad(1,5,ip1)+grad(1,5,ip2))*lx+(grad(2,5,ip1)+grad(2,5,ip2))*ly+(grad(3,5,ip1)+grad(3,5,ip2))*lz       

     beta=c00
     !
     !     Blend with pressure sensor
     !
     ax1=lambda*(dpres-beta*dpres4)
     ax2=lambda*(du-beta*du4)
     ax3=lambda*(dv-beta*dv4)
     ax4=lambda*(dw-beta*dw4)
     ax5=lambda*(dT-beta*dT4)
     !
     !     Cut off the antidiffusion
     ! 
     if(dpres*ax1<c00)ax1=c00
     if(du*ax2<c00)ax2=c00
     if(dv*ax3<c00)ax3=c00
     if(dw*ax4<c00)ax4=c00
     if(dT*ax5<c00)ax5=c00
     !
     !     Keep only first order diffusion
     ! 
     !if(dpres*ax1<c00)ax1=lambda*dpres
     !if(du*ax2<c00)ax2=lambda*du
     !if(dv*ax3<c00)ax3=lambda*dv
     !if(dw*ax4<c00)ax4=lambda*dw
     !if(dT*ax5<c00)ax5=lambda*dT

     !DBG
     !ax1=c00
     !ax2=c00
     !ax3=c00
     !ax4=c00
     !ax5=c00

     !
     !     Multiply by Gamma^-1
     !     
     rhot=-rho/temp                   !Perfect Gas
     rhop=rho/pres                    !Perfect Gas 
     theta=c10/vr2-rhot/(rho*cp)      !Perfect Gas
     enthal=e+pres*rhoi

     ra=theta*enthal-c10
     rb=rhot*enthal+rho*cp
     rc=c10/(rho*cp*theta+rhot)
     rq2=u*u+v*v+w*w

     redg1d= ax1*(rb-rhot*rq2)*rc+ax2*u*rhot*rc+ax3*v*rhot*rc+ax4*w*rhot*rc-ax5*rhot*rc 
     redg2d=-ax1*u*rhoi+ax2*rhoi                      
     redg3d=-ax1*v*rhoi+       +ax3*rhoi              
     redg4d=-ax1*w*rhoi                 +ax4*rhoi     
     redg5d= ax1*(rq2*theta-ra)*rc-ax2*u*theta*rc+ax3*v*theta*rc+ax4*w*theta*rc+ax5*theta*rc
     !
     !     Sum up Galerkin + dissipation
     !
     redg1=redg1+redg1d  
     redg2=redg2+redg2d  
     redg3=redg3+redg3d  
     redg4=redg4+redg4d  
     redg5=redg5+redg5d  
     !
     !     -----multiply by the preconditioning matrix
     !
     redg1n=   redg1*theta           +redg5*rhot
     redg2n=u*(redg1*theta           +redg5*rhot)+redg2*rho
     redg3n=v*(redg1*theta           +redg5*rhot)+redg3*rho
     redg4n=w*(redg1*theta           +redg5*rhot)+redg4*rho
     redg5n=  redg1*(theta*enthal-c10)+redg2*rho*u +redg3*rho*v+redg4*rho*w+redg5*(rhot*enthal+rho*cp)
     !
     !     Add to the flux points
     !
     flu(1,ip1)=flu(1,ip1)+redg1n
     flu(2,ip1)=flu(2,ip1)+redg2n
     flu(3,ip1)=flu(3,ip1)+redg3n
     flu(4,ip1)=flu(4,ip1)+redg4n
     flu(5,ip1)=flu(5,ip1)+redg5n

     flu(1,ip2)=flu(1,ip2)-redg1n
     flu(2,ip2)=flu(2,ip2)-redg2n
     flu(3,ip2)=flu(3,ip2)-redg3n
     flu(4,ip2)=flu(4,ip2)-redg4n
     flu(5,ip2)=flu(5,ip2)-redg5n

  enddo

end subroutine jstflupre

subroutine preresid(flu,nflu,npoin,var,gam,cvi,nvar,cp,viscp,ivisc,rmass)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: npoin,nvar,nflu,ivisc
  real(rp), intent(in)       :: gam,var(nvar,npoin),cvi,cp,viscp(2,npoin),rmass(npoin)
  real(rp),intent(inout)     :: flu(nflu,npoin) 

  real(rp)                   :: epsilpre,rho,rhoi,u,v,w,rhoe,e,pres,temp   
  real(rp)                   :: vedg,vedg2,c,vinf,vr,vr2,rhot,rhop,theta,enthal  
  real(rp)                   :: re,rd,rc,rcprime,rcprimei,rk,ri,rj,rf,rg,uD,vD,wD
  real(rp)                   :: ax1,ax2,ax3,ax4,ax5 
  real(rp)                   :: c05,c10,cm13,length,mu
  real(rp)                   :: M(5,5),G(5,5),F(5,5),WG(5,5),c00,ra,rb,rq2
  integer(ip)                :: ipoin,i,j,k 

  epsilpre=1.0d-05
  c05=0.5d+00
  c10=1.0d+00
  c00=0.0d+00
  cm13=-1.0d+00/3.0d+00
  !
  !     -----loop over the points, forming the preconditioner
  !
!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(var,cvi,epsilpre,c05,c10,gam,npoin,flu,cp,cm13,rmass,viscp,ivisc) &
!$omp private(rho,rhoi,u,v,w,rhoe,e,pres,temp,vedg,vedg2,&
!$omp&        c,vinf,vr,vr2,rhot,rhop,theta,enthal,& 
!$omp&        re,rd,rc,rcprime,rcprimei,rk,ri,rj,rf,rg,uD,vD,wD,&
!$omp&        ax1,ax2,ax3,ax4,ax5,length,mu)

  do ipoin=1,npoin
     !
     !     -----build pressure, velocity and temperature
     !
     rho   = var(1,ipoin) 
     rhoi  = var(7,ipoin)
     u  = var(2,ipoin)*rhoi
     v  = var(3,ipoin)*rhoi
     w  = var(4,ipoin)*rhoi
     rhoe  = var(5,ipoin)  
     e  = rhoe*rhoi
     pres  = var( 6,ipoin)
     temp  = cvi*(e-(u*u+v*v+w*w)*c05)  
     length=rmass(ipoin)**cm13
     !
     !     -----velocity
     !
     vedg2=u*u+v*v+w*w
     vedg=sqrt(vedg2)
     !
     !     -----speed of sound^2
     !
     c=pres*rhoi*gam
     !
     !     -----set vinf
     ! 
     vinf=epsilpre*c 
     !
     !     -----compute reference velocity for Euler
     !
     vr=min(c,max(vedg,vinf))
     !
     !     -----compute reference velocity for NS
     !
     if(ivisc==1)then
         mu=viscp(1,ipoin)
         vr=max(vr,mu/(rho*length))
     endif

     vr2=vr*vr 
     !
     !     -----multiply by the preconditioning matrix
     !
     !
     !    density derivatives respect to temperature and pressure
     !
     rhot=-rho/temp     !Perfect Gas
     rhop=rho/pres     !Perfect Gas 
     !
     !     -----theta parameter
     !
     theta=c10/vr2-rhot/(rho*cp)
     !
     !     -----enthalpy
     !
     enthal=e+pres*rhoi
     !
     !     -----local variables
     !
     re=rhop-theta
     rd=rhot*re
     rcprime=rho*cp*theta+rhot
     rc=rho*cp*rhop+rhot
     rcprimei=c10/rcprime
     rk=enthal-vedg2
     ri=rhot*rk+rho*cp
     rj=re*ri
     rf=enthal*rj
     rg=enthal*rd
     uD=u*rd
     vD=v*rd
     wD=w*rd
     !
     !     -----get the residual 
     !
     ax1=flu(1,ipoin)
     ax2=flu(2,ipoin)
     ax3=flu(3,ipoin)
     ax4=flu(4,ipoin)
     ax5=flu(5,ipoin)
     !
     !     -----multiply by the inverse
     !
     ax1=rcprimei*ax1
     ax2=rcprimei*ax2
     ax3=rcprimei*ax3
     ax4=rcprimei*ax4
     ax5=rcprimei*ax5
     !
     !     -----multiply the residual 
     !
     flu(1,ipoin)=ax1*(rk*rd+rc)+ax2*uD+ax3*vD+ax4*wD-ax5*rd
     flu(2,ipoin)=ax1*u*rj+ax2*(u*uD+rcprime)+ax3*u*vD+ax4*u*wD-ax5*uD
     flu(3,ipoin)=ax1*v*rj+ax2*v*uD+ax3*(v*vD+rcprime)+ax4*v*wD-ax5*vD
     flu(4,ipoin)=ax1*w*rj+ax2*w*uD+ax3*w*vD+ax4*(w*wD+rcprime)-ax5*wD
     flu(5,ipoin)=ax1*rf +ax2*u*rg+ax3*v*rg+ax4*w*rg+ax5*(-rg+rcprime)

        !
!!$     !     DBG
!!$     ! 
!!$     WG(1,1)=rk*rd+rc2
!!$     WG(2,1)=uD
!!$     WG(3,1)=vD
!!$     WG(4,1)=wD
!!$     WG(5,1)=-rd
!!$
!!$     WG(1,2)=u*rj
!!$     WG(2,2)=u*uD+rctemp
!!$     WG(3,2)=u*vD
!!$     WG(4,2)=u*wD
!!$     WG(5,2)=-uD
!!$
!!$     WG(1,3)=v*rj
!!$     WG(2,3)=v*uD
!!$     WG(3,3)=v*vD+rctemp
!!$     WG(4,3)=v*wD
!!$     WG(5,3)=-vD
!!$
!!$     WG(1,4)=w*rj
!!$     WG(2,4)=w*uD
!!$     WG(3,4)=w*vD
!!$     WG(4,4)=w*wD+rctemp
!!$     WG(5,4)=-wD
!!$
!!$     WG(1,5)=rf
!!$     WG(2,5)=u*rg
!!$     WG(3,5)=v*rg
!!$     WG(4,5)=w*rg
!!$     WG(5,5)=-rg+rctemp
!!$
!!$     G(1,1)=theta
!!$     G(2,1)=c00
!!$     G(3,1)=c00
!!$     G(4,1)=c00
!!$     G(5,1)=rhot
!!$
!!$     G(1,2)=theta*u
!!$     G(2,2)=rho
!!$     G(3,2)=c00
!!$     G(4,2)=c00
!!$     G(5,2)=rhot*u
!!$     
!!$     G(1,3)=theta*v
!!$     G(2,3)=c00
!!$     G(3,3)=rho
!!$     G(4,3)=c00
!!$     G(5,3)=rhot*v
!!$
!!$     G(1,4)=theta*w
!!$     G(2,4)=c00
!!$     G(3,4)=c00
!!$     G(4,4)=rho
!!$     G(5,4)=rhot*w
!!$
!!$     G(1,5)=theta*enthal-c10
!!$     G(2,5)=rho*u
!!$     G(3,5)=rho*v
!!$     G(4,5)=rho*w
!!$     G(5,5)=rhot*enthal+rho*cp
!!$
!!$     M=c00
!!$
!!$     do i=1,5
!!$        do j=1,5
!!$             do k=1,5
!!$           M(j,i)=M(j,i)+rc*WG(k,i)*G(j,k)
!!$             enddo    
!!$        enddo
!!$     enddo
!!$
!!$     ra=rhop*enthal-c10
!!$     rb=rhot*enthal+rho*cp
!!$     rc=c10/(rho*cp*rhop+rhot)
!!$     rq2=u*u+v*v+w*w
!!$
!!$     G(1,1)=rb-rhot*rq2
!!$     G(2,1)=u*rhot*rc
!!$     G(3,1)=v*rhot*rc
!!$     G(4,1)=w*rhot*rc
!!$     G(5,1)=-rhot*rc
!!$
!!$     G(1,2)=-u*rhoi
!!$     G(2,2)=rhoi
!!$     G(3,2)=c00
!!$     G(4,2)=c00
!!$     G(5,2)=c00
!!$
!!$     G(1,3)=-v*rhoi
!!$     G(2,3)=c00
!!$     G(3,3)=rhoi
!!$     G(4,3)=c00 
!!$     G(5,3)=c00
!!$
!!$     G(1,4)=-w*rhoi
!!$     G(2,4)=c00
!!$     G(3,4)=c00
!!$     G(4,4)=rhoi
!!$     G(5,4)=c00
!!$ 
!!$     G(1,5)=(rq2*rhop-ra)*rc
!!$     G(2,5)=-u*rhop*rc
!!$     G(3,5)=-v*rhop*rc
!!$     G(4,5)=-w*rhop*rc
!!$     G(5,5)=rhop*rc
!!$     !
!!$
!!$     F=c00
!!$
!!$
!!$     do i=1,5
!!$        do j=1,5
!!$             do k=1,5
!!$           F(j,i)=F(j,i)+M(k,i)*G(j,k)
!!$             enddo    
!!$        enddo
!!$     enddo
!!$     do i=1,5
!!$        do j=1,5
!!$        write(*,*)i,j,F(j,i)
!!$        enddo
!!$     enddo

    
  enddo


end subroutine preresid



subroutine hllcflupre(nedge,ledge,npoin,nvar,nflu,edsha,gam,extr1,extr2,flu,coor,ndim,ivisc,viscp,rmass,hydro1,hydro2,grav)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nedge,npoin,nvar,nflu,ndim,ivisc
  integer(ip), intent(in)    :: ledge(2,nedge)
  real(rp), intent(in)       :: edsha(5,nedge),gam,extr1(nvar,nedge),extr2(nvar,nedge)
  real(rp), intent(in)       :: coor(ndim,npoin),viscp(2,npoin),rmass(npoin),hydro1(2,nedge),hydro2(2,nedge),grav
  real(rp),intent(inout)     :: flu(nflu,npoin) 
  real(rp)     :: srhor,srhol,det,deti,rhot,ut,vt,wt,Ht
  real(rp)     :: rnl,rnli,coef1,coef2,coef3,rho1,rhou1,rhov1,rhow1,rhoe1,rho2
  real(rp)     :: rhou2,rhov2,rhow2,rhoe2,pres1,pres2,rhoi1,rhoi2
  real(rp)     :: dirx,diry,dirz,rhor,vr,ur,wr,Hr,er,rhol,ul,vl,wl,Hl,el 
  real(rp)     :: at,c10,c00,c05,c20,c14,gam1,cm2
  real(rp)     :: redg1,redg2,redg3,redg4,redg5,vedg,pres12,pres22 
  real(rp)     :: u1,u2,vt2,upw1,upw2,upw3,upw4,upw5,radv1,radv2
  real(rp)     :: stat1(5),stat2(5),S1,S2,Sstar
  real(rp)     :: f11,f12,f13,f14,f15,f21,f22,f23,f24,f25 
  real(rp)     :: c1,c2,cons 
  real(rp)     :: g1,g2,g3,g4,g5,g6,g7,g8
  real(rp)     :: pmin,pmax,qmax,quser,um,pq,pt1,pt2,ge1,ge2,cup,ppv,pm 
  real(rp)     :: ux,lx,ly,lz,ll,dz,presh1,presh2,rhoh1,rhoh2,dpresh1,dpresh2,drhoh1,drhoh2
  real(rp)     :: u12,u22,betap1,betap2,vinf1,vinf2,epsilpre,vr1,vr2,vr21,vr22,alpha1,alpha21,alpha2,alpha22
  real(rp)     :: betapt,vinft,vrt,vr2t,alphat,alpha2t,length1,length2,cm13,length,mu
  integer(ip)  :: iedge,ip1,ip2,ipstrang,iopt

  epsilpre=1.0d-05
  c00=0.0d+00
  c05=0.5d+00
  c10=1.0d+00
  c14=1.0d+00/4.0d+00
  cm13=-1.0d+00/3.0d+00
  c20=2.0d+00
  cm2=-2.0d+00
  gam1=gam-c10
  g1=gam1/(c20*gam)
  g2=(gam+c10)/(c20*gam)
  g3=c20*gam/gam1
  g4=c20/gam1
  g5=c20/(gam+c10)
  g6=gam1/(gam+c10)
  g7=gam1/c20
  g8=gam1
  quser=c20
  iopt=1_ip
  ipstrang=-1_ip

!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(nedge,ledge,edsha,extr1,extr2,iopt,flu,c10,c14,c05,gam,c00,cm2, &
!$omp&       gam1,quser,g1,g2,g3,g4,g5,g6,g7,g8,ipstrang,epsilpre,ivisc,rmass,cm13,viscp,hydro1,hydro2)&
!$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rnl,rnli,& 
!$omp&        dirx,diry,dirz,rho1,rhou1,rhov1,rhow1,rhoe1,rho2,rhou2,rhov2,rhow2,rhoe2,&
!$omp&        rhoi1,u1,c1,rhoi2,u2,c2,pres1,pres2,redg1,redg2,redg3,redg4,redg5,&
!$omp&        vedg,ut,vt,wt,at,Ht,rhol,ul,vl,wl,el,Hl,rhor,ur,vr,wr,er,Hr,&
!$omp&        srhol,srhor,det,deti,vt2,S1,S2,Sstar, &
!$omp&        cup,ppv,pmin,pmax,qmax,um,pm,ge1,ge2,radv1,radv2,pt1,pt2,pq,cons,& 
!$omp&        f11,f12,f13,f14,f15,f21,f22,f23,f24,f25,stat1,stat2,&
!$omp&        u12,u22,betap1,betap2,vinf1,vinf2,vr1,vr2,vr21,vr22,alpha1,alpha21,alpha2,alpha22,&
!$omp&        betapt,vinft,vrt,vr2t,alphat,alpha2t,length,length1,length2,rhot,mu,presh1,dpresh1,rhoh1,presh2,dpresh2,rhoh2)


  do iedge=1,nedge

     !
     !     Edge points
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)

     !
     !    Edge coef *2 in the direction oposite to the edge direction
     !     
     ! 
     coef1=cm2*edsha(2,iedge)
     coef2=cm2*edsha(3,iedge)
     coef3=cm2*edsha(4,iedge)
     ! DBG 1D
     !coef1=edsha(2,iedge)
     !coef2=edsha(3,iedge)
     !coef3=edsha(4,iedge)

     rnl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3) 
     rnli=c10/rnl

     dirx=coef1*rnli
     diry=coef2*rnli
     dirz=coef3*rnli
     !
     !     Get relevant quantities 
     !
     rho1=extr1(1,iedge)
     rhou1=extr1(2,iedge)
     rhov1=extr1(3,iedge)
     rhow1=extr1(4,iedge)
     rhoe1=extr1(5,iedge)
     pres1=extr1(6,iedge)
     rhoi1=extr1(7,iedge)
     u1=(dirx*rhou1+diry*rhov1+dirz*rhow1)*rhoi1
     u12=u1*u1 
     c1=sqrt(gam*pres1*rhoi1)

     !if(abs(rho1*rhoi1-1)>1.0d-03)then
     !   write(*,*)'error rhoi1'
     !   stop
     !endif


     rho2=extr2(1,iedge)
     rhou2=extr2(2,iedge)
     rhov2=extr2(3,iedge)
     rhow2=extr2(4,iedge)
     rhoe2=extr2(5,iedge)
     pres2=extr2(6,iedge)
     rhoi2=extr2(7,iedge)
     u2=(dirx*rhou2+diry*rhov2+dirz*rhow2)*rhoi2
     u22=u2*u2
     c2=sqrt(gam*pres2*rhoi2)

     !if(abs(rho1*rhoi1-1)>1.0d-03)then
     !   write(*,*)'error rhoi1'
     !   stop
     !endif
     !
     !     Take into account gravity
     !
     rhoh1=hydro1(1,iedge)
     presh1=hydro1(2,iedge)
     !drhoh1=rho1-rhoh1
     dpresh1=pres1-presh1
     !dpresh1=pres1
     rhoh2=hydro2(1,iedge)
     presh2=hydro2(2,iedge)
     !drhoh2=rho2-rhoh2
     dpresh2=pres2-presh2
     !dpresh2=pres2

     !
     !     Compute left and right velocities and enthalpies
     !
     rhol=rho1
     ul=rhou1*rhoi1
     vl=rhov1*rhoi1
     wl=rhow1*rhoi1
     el=rhoe1*rhoi1
     Hl=(rhoe1+pres1)*rhoi1

     rhor=rho2
     ur=rhou2*rhoi2
     vr=rhov2*rhoi2
     wr=rhow2*rhoi2
     er=rhoe2*rhoi2
     Hr=(rhoe2+pres2)*rhoi2
     !
     !     Compute preconditioned velocities
     !
     betap1=c10/(c1*c1)   
     betap2=c10/(c2*c2)   
     !
     !     set vinf
     !
     vinf1=epsilpre*c1
     vinf2=epsilpre*c2
     !
     !     compute reference velocity for Euler
     !          if supersonic, vr=csoun
     !
     vr1=min(c1,max(u1,vinf1))
     vr2=min(c2,max(u2,vinf2))
     !
     !     compute reference velocity for NS
     !
     if(ivisc==1)then
         length1=rmass(ip1)**cm13
         length2=rmass(ip2)**cm13
         vr1=max(vr1,viscp(1,ip1)/(rho1*length1))
         vr2=max(vr2,viscp(1,ip2)/(rho2*length2))
     endif
     vr21=vr1*vr1
     vr22=vr2*vr2
     !
     !     compute alpha
     !          if supersonic, alpha=0 and the preconditioning is unity
     !
     alpha1=c05*(c10-betap1*vr21)
     alpha21=alpha1*alpha1
     alpha2=c05*(c10-betap2*vr22)
     alpha22=alpha2*alpha2
     !
     !     compute pseudo speed of sound
     !
     c1=sqrt(alpha21*u12+vr21)
     c2=sqrt(alpha22*u22+vr22)
     !
     !     compute pseudo velocity
     !
     u1=u1*(c10-alpha1)
     u2=u2*(c10-alpha2)

     if(iopt==1)then
        !
        !     Compute Roe average
        !
        srhol=sqrt(rhol)
        srhor=sqrt(rhor)
        det=srhol+srhor
        deti=c10/det

        rhot=srhol*srhor
        ut=(srhol*ul+srhor*ur)*deti
        vt=(srhol*vl+srhor*vr)*deti
        wt=(srhol*wl+srhor*wr)*deti
        Ht=(srhol*Hl+srhor*Hr)*deti
        vt2=ut*ut+vt*vt+wt*wt
        at=sqrt(gam1*(Ht-c05*vt2))
        !
        !     Velocity in the edge direction
        ! 
        vedg=dirx*ut+diry*vt+dirz*wt
        !
        !     Compute preconditioned Roe velocities
        !
        betapt=c10/(at*at)   
        !
        !     set vinf
        !
        vinft=epsilpre*at
        !
        !     compute reference velocity for Euler
        !          if supersonic, vr=csoun
        !
        vrt=min(at,max(vedg,vinft))
        !
        !     compute reference velocity for NS
        !
        if(ivisc==1)then
            mu=c05*(viscp(1,ip1)+viscp(1,ip2))
            length=c05*(length1+length2)
            vrt=max(vrt,mu/(rhot*length))
        endif
        vr2t=vrt*vrt
        !
        !     compute alpha
        !          if supersonic, alpha=0 and the preconditioning is unity
        !
        alphat=c05*(c10-betapt*vr2t)
        alpha2t=alphat*alphat
        !
        !     compute pseudo speed of sound
        !
        at=sqrt(alpha2t*vedg+vr2t)
        !
        !     compute pseudo velocity
        !
        vedg=vedg*(c10-alphat)
        !
        !     Wave speed estimate
        !
        S1=min(u1-c1,vedg-at)    
        S2=max(u2+c2,vedg+at)
        Sstar=(rho2*u2*(S2-u2)-rho1*u1*(S1-u1)+dpresh1-dpresh2)/(rho2*(S2-u2)-rho1*(S1-u1))

     else


        cup=c14*(rho1+rho2)*(c1+c2)
        ppv=c05*(pres1+pres2)+c05*(u1-u2)*cup
        ppv=max(c00,ppv)
        pmin=min(pres1,pres2)
        pmax=max(pres1,pres2)
        qmax=pmax/pmin

        if(qmax<=quser .and. (pmin<=ppv .and. ppv<=pmax))then

           !
           !     PRVS Riemann solver
           !
           pm=ppv
           um=c05*(u1+u2)+c05*(pres1-pres2)/cup  

        else if(ppv<pmin)then

           !
           !    Two rarefaction Riemann solver
           !
           pq=(pres1/pres2)**g1
           um=(pq*u1/c1+u2/c2+g4*(pq-c10))/(pq/c1+c10/c2)
           pt1=c10+g7*(u1-um)/c1
           pt2=c10+g7*(um-u2)/c2
           pm=c05*(pres1*pt1**g3+pres2*pt2**g3)

        else

           !
           !   Two Shock Riemann solver with PVRS estimate
           !
           ge1=sqrt((g5/rho1)/(g6*pres1+ppv))
           ge2=sqrt((g5/rho2)/(g6*pres2+ppv))
           pm=(ge1*pres1+ge2*pres2-(u2-u1))/(ge1+ge2) 
           um=c05*(u1+u2)+c05*(ge2*(pm-pres2)-ge1*(pm-pres1))

        endif
        !
        !     Find speed
        !

        if(pm<=pres1)then
           S1=u1-c1
        else
           S1=u1-c1*sqrt(c10+g2*(pm/pres1-c10))
        endif

        if(pm<=pres2)then
           S2=u2+c2
        else
           S2=u2+c2*sqrt(c10+g2*(pm/pres2-c10))
        endif

        Sstar=um

     endif
     !
     !     Compute states
     !
     cons=rho1*(S1-u1)/(S1-Sstar)
     stat1(1)=cons
     stat1(2)=cons*(ul+(Sstar-u1)*dirx)
     stat1(3)=cons*(vl+(Sstar-u1)*diry)
     stat1(4)=cons*(wl+(Sstar-u1)*dirz)
     stat1(5)=cons*(el+(Sstar-u1)*(Sstar+(pres1*rhoi1/(S1-u1))))

     cons=rho2*(S2-u2)/(S2-Sstar)
     stat2(1)=cons
     stat2(2)=cons*(ur+(Sstar-u2)*dirx)
     stat2(3)=cons*(vr+(Sstar-u2)*diry)
     stat2(4)=cons*(wr+(Sstar-u2)*dirz)
     stat2(5)=cons*(er+(Sstar-u2)*(Sstar+(pres2*rhoi2/(S2-u2))))

     radv1=u1
     radv2=u2

     !
     !     Euler Galerkin Fluxes
     !
     f11= radv1*rho1               
     f12= radv1*rhou1+dirx*dpresh1 
     f13= radv1*rhov1+diry*dpresh1 
     f14= radv1*rhow1+dirz*dpresh1  
     f15= radv1*(rhoe1+pres1)      

     f21= radv2*rho2               
     f22= radv2*rhou2+dirx*dpresh2 
     f23= radv2*rhov2+diry*dpresh2 
     f24= radv2*rhow2+dirz*dpresh2  
     f25= radv2*(rhoe2+pres2)

     !
     !     Get the HLLC fluxes
     !
     if(S1>c00)then

        redg1=f11    
        redg2=f12    
        redg3=f13    
        redg4=f14    
        redg5=f15    

     else if(Sstar>c00)then 


        redg1=f11+S1*(stat1(1)-rho1)    
        redg2=f12+S1*(stat1(2)-rhou1)    
        redg3=f13+S1*(stat1(3)-rhov1)    
        redg4=f14+S1*(stat1(4)-rhow1)    
        redg5=f15+S1*(stat1(5)-rhoe1)    

     else if(S2>c00)then

        redg1=f21+S2*(stat2(1)-rho2)    
        redg2=f22+S2*(stat2(2)-rhou2)    
        redg3=f23+S2*(stat2(3)-rhov2)    
        redg4=f24+S2*(stat2(4)-rhow2)    
        redg5=f25+S2*(stat2(5)-rhoe2)  

     else

        redg1=f21    
        redg2=f22    
        redg3=f23    
        redg4=f24    
        redg5=f25  

     endif
     !
     !     Correct the scaling and direction
     !
     redg1=-redg1*rnl
     redg2=-redg2*rnl
     redg3=-redg3*rnl
     redg4=-redg4*rnl
     redg5=-redg5*rnl
     !
     !     Send to points
     !

     !if(ip1==ipstrang )then

     !   write(*,*)S1,S2,Sstar
     !   write(*,*)flu(1,ip1),f12*rnl,-redg2,flu(2,ip1)

     !endif

     !if(ip2==ipstrang )then

     !   write(*,*)S1,S2,Sstar
     !   write(*,*)flu(1,ip2),f12*rnl,-redg2,flu(2,ip2)

     !endif

     flu(1,ip1)=flu(1,ip1)+redg1
     flu(2,ip1)=flu(2,ip1)+redg2
     flu(3,ip1)=flu(3,ip1)+redg3
     flu(4,ip1)=flu(4,ip1)+redg4
     flu(5,ip1)=flu(5,ip1)+redg5

     flu(1,ip2)=flu(1,ip2)-redg1
     flu(2,ip2)=flu(2,ip2)-redg2
     flu(3,ip2)=flu(3,ip2)-redg3
     flu(4,ip2)=flu(4,ip2)-redg4
     flu(5,ip2)=flu(5,ip2)-redg5

     if(ip1==ipstrang )then

        write(*,*)flu(1,ip1),flu(2,ip1)

     endif

     if(ip2==ipstrang )then

        write(*,*)flu(1,ip2),flu(2,ip2)

     endif

  enddo

end subroutine hllcflupre


subroutine preresdual(flu,nflu,nvar,npoin,var,varn,varnn,dtau,niter,dt,cvi,rmass,ivisc,gam,viscp,cp)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: npoin,nvar,nflu,niter,ivisc
  real(rp),intent(inout)     :: flu(nflu,npoin) 
  real(rp),intent(in)        :: var(nvar,npoin),varn(nvar,npoin),varnn(nvar,npoin),dt,dtau,cvi,rmass(npoin),gam,viscp(2,npoin),cp 
  real(rp)                   :: dtime,c10,c20,c30,c40,rcprimei,ctime
  real(rp)                   :: re,rd,rc,rk,ri,rj,rf,rg,uD,vD,wD,epsilpre,c05
  real(rp)                   :: c00,cm13,rho,rhoi,u,v,w,rhoe,e,pres,temp,length
  real(rp)                   :: vedg2,vedg,c,vinf,vr,vr2,rhot,mu,rhop,theta 
  real(rp)                   :: enthal,rl,ax1,ax2,ax3,ax4,ax5 
  integer(ip)                :: ipoin


!
!     This sub multiplies the residual by the preconditioner in a dual time
!     stepping context
!

  c10=1.0d+00
  c20=2.0d+00
  c30=3.0d+00
  c40=4.0d+00
  epsilpre=1.0d-05
  c05=0.5d+00
  c00=0.0d+00
  cm13=-1.0d+00/3.0d+00
 
  !
  !     First add previous time steps to the residual
  !

   if(niter>5)then

   dtime=c10/(c20*dt)

 do ipoin=1,npoin
     flu(1,ipoin)=flu(1,ipoin)+dtime*(c30*var(1,ipoin)-c40*varn(1,ipoin)+varnn(1,ipoin))
     flu(2,ipoin)=flu(2,ipoin)+dtime*(c30*var(2,ipoin)-c40*varn(2,ipoin)+varnn(2,ipoin))
     flu(3,ipoin)=flu(3,ipoin)+dtime*(c30*var(3,ipoin)-c40*varn(3,ipoin)+varnn(3,ipoin))
     flu(4,ipoin)=flu(4,ipoin)+dtime*(c30*var(4,ipoin)-c40*varn(4,ipoin)+varnn(4,ipoin))
     flu(5,ipoin)=flu(5,ipoin)+dtime*(c30*var(5,ipoin)-c40*varn(5,ipoin)+varnn(5,ipoin))
 enddo


    else

   dtime=c10/dt

 do ipoin=1,npoin
     flu(1,ipoin)=flu(1,ipoin)+dtime*(var(1,ipoin)-varn(1,ipoin))
     flu(2,ipoin)=flu(2,ipoin)+dtime*(var(2,ipoin)-varn(2,ipoin))
     flu(3,ipoin)=flu(3,ipoin)+dtime*(var(3,ipoin)-varn(3,ipoin))
     flu(4,ipoin)=flu(4,ipoin)+dtime*(var(4,ipoin)-varn(4,ipoin))
     flu(5,ipoin)=flu(5,ipoin)+dtime*(var(5,ipoin)-varn(5,ipoin))
 enddo

endif



 !
 !     Then multiply by the  inverse of Gamma+3/2*(dtau/dt)*dW/dQ 
 !

  ctime=(3.0d+00*dtau)/(2.0d+00*dt)


!$omp parallel do &         
!$omp& schedule(static) &
!$omp& default(none) &
!$omp& shared(var,cvi,epsilpre,c05,c10,gam,npoin,flu,cp,cm13,rmass,viscp,ivisc,ctime) &
!$omp private(rho,rhoi,u,v,w,rhoe,e,pres,temp,vedg,vedg2,&
!$omp&        c,vinf,vr,vr2,rhot,rhop,theta,enthal,& 
!$omp&        re,rd,rc,rcprimei,rk,ri,rj,rf,rg,uD,vD,wD,&
!$omp&        ax1,ax2,ax3,ax4,ax5,length,mu,rl)

  do ipoin=1,npoin
     !
     !     -----build pressure, velocity and temperature
     !
     rho   = var(1,ipoin) 
     rhoi  = var(7,ipoin)
     u  = var(2,ipoin)*rhoi
     v  = var(3,ipoin)*rhoi
     w  = var(4,ipoin)*rhoi
     rhoe  = var(5,ipoin)  
     e  = rhoe*rhoi
     pres  = var( 6,ipoin)
     temp  = cvi*(e-(u*u+v*v+w*w)*c05)  
     length=rmass(ipoin)**cm13
     !
     !     -----velocity
     !
     vedg2=u*u+v*v+w*w
     vedg=sqrt(vedg2)
     !
     !     -----speed of sound^2
     !
     c=pres*rhoi*gam
     !
     !     -----set vinf
     ! 
     vinf=epsilpre*c 
     !
     !     -----compute reference velocity for Euler
     !
     vr=min(c,max(vedg,vinf))
     !
     !     -----compute reference velocity for NS
     !
     if(ivisc==1)then
         mu=viscp(1,ipoin)
         vr=max(vr,mu/(rho*length))
     endif

     vr2=vr*vr 
     !
     !     -----multiply by the preconditioning matrix
     !
     !
     !    density derivatives respect to temperature and pressure
     !
     rhot=-rho/temp     !Perfect Gas
     rhop=rho/pres     !Perfect Gas 
     !
     !     -----theta parameter
     !
     theta=c10/vr2-rhot/(rho*cp)
     !
     !     -----enthalpy
     !
     enthal=e+pres*rhoi
     !
     !     -----local variables
     !
     re=rhop-theta
     rd=rhot*re
     rc=rho*cp*(theta+ctime*rhop)+rhot*(c10+ctime)
     rcprimei=c10/((c10+ctime)*(rho*cp*(theta+c*rhop)+rhot*(c10+ctime)))
     rk=enthal-vedg2
     ri=rhot*rk+rho*cp
     rj=re*ri
     rf=enthal*rj
     rg=enthal*rd
     rl=(rho*cp*rhop+rhot)*(c10+ctime)
     uD=u*rd
     vD=v*rd
     wD=w*rd
     !
     !     -----get the residual 
     !
     ax1=flu(1,ipoin)
     ax2=flu(2,ipoin)
     ax3=flu(3,ipoin)
     ax4=flu(4,ipoin)
     ax5=flu(5,ipoin)
     !
     !     -----multiply by the inverse
     !
     ax1=rcprimei*ax1
     ax2=rcprimei*ax2
     ax3=rcprimei*ax3
     ax4=rcprimei*ax4
     ax5=rcprimei*ax5
     !
     !     -----multiply the residual 
     !
     flu(1,ipoin)=ax1*(rk*rd+rl)+ax2*uD+ax3*vD+ax4*wD-ax5*rd
     flu(2,ipoin)=ax1*u*rj+ax2*(u*uD+rc)+ax3*u*vD+ax4*u*wD-ax5*uD
     flu(3,ipoin)=ax1*v*rj+ax2*v*uD+ax3*(v*vD+rc)+ax4*v*wD-ax5*vD
     flu(4,ipoin)=ax1*w*rj+ax2*w*uD+ax3*w*vD+ax4*(w*wD+rc)-ax5*wD
     flu(5,ipoin)=ax1*rf +ax2*u*rg+ax3*v*rg+ax4*w*rg+ax5*(-rg+rc)

   enddo

end subroutine preresdual








