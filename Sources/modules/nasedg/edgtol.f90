subroutine edgfil(elem,nelem,npoin,nnode,nedge,nsid,ndim,coor,rmass,nnofa,&
     nface,nedgb,nboup,lsurf,nsurf,lbous,lface,rnofa,irenum,mboup)
  use def_kintyp, only       :  ip,rp,lg
  use mod_memchk
  use mod_mshtol
  use def_nasedg, only          : memor_edg,ptoed2,ledglm,edsha,edshab,edshap,ledgfa,ptoed3,ledgb,lboup
  use def_meshin, only          : ptoel1,ptoel2,ledge,eltoel
  implicit none
  integer(ip),intent(in)     :: npoin,nsid,ndim,nelem,nnode
  integer(ip),intent(inout)  :: nface
  integer(ip),intent(in)     :: nnofa,nsurf
  integer(ip),intent(in)     :: elem(nnode,nelem),lface(nnofa,nface),lsurf(nface)
  integer(ip),intent(in)     :: irenum
  integer(ip),intent(inout)  :: nedge,nedgb,nboup,lbous(2,nsurf),mboup
  real(rp),intent(in)        :: coor(ndim,npoin),rnofa(ndim,nface)
  real(rp),intent(inout)     :: rmass(npoin)
  real(rp)                   :: rx,ry,rz,lenmax,c00 
  integer(ip)                :: nfac,ipoin,iedge,ip1,ip2
  integer(4)                 :: istat
  c00=0.0d+00

  !
  !    Allocate necessary help arrays 
  !
  allocate(ledglm(nsid,nelem),stat=istat)
  call memchk(zero,istat,memor_edg,'LEDGLM','edgfil',ledglm)
  allocate(ledgfa(nnofa,nface),stat=istat)
  call memchk(zero,istat,memor_edg,'LEDGFA','edgfil',ledgfa)
  !
  !     First get the elements surrounding the points
  !
  call ptoelm(elem,nelem,npoin,nnode,ptoel1,ptoel2)
  !
  !     Then get the elements surrounding elements
  !
  !call tetote(elem,nnode,nelem,ptoel1,ptoel2,npoin)
  !
  !     Get the boundary faces 
  ! 
  !call gtbfa(nface,nnofa,nelem,elem,nnode,npoin,ndim,eltoel)
  !
  !     Get all the edges  
  !
  call ptoedg(elem,nelem,npoin,nnode,ptoel1,ptoel2,nedge   )
  !
  !     Get the element to edge pointer
  !  
  call edglem(elem,nnode,nelem,ledglm,nsid,ptoed2,ledge,nedge)
  !
  !     Renumber the edges
  !
  if(irenum==1)then
     call renued(ledge,nedge,npoin,ledglm,nsid,nelem,elem,nnode)
  endif
  !
  !     Get the faces surrounding the points
  !
  call ptoelm(lface,nface,npoin,nnofa,ptoel1,ptoel2)
  !
  !     Get the boundary conditions
  !  
  call getbc(ptoel1,ptoel2,nface,lsurf,nboup,ndim,lbous,nsurf,npoin,rnofa,lface,nnofa,coor,mboup)
  !
  !     Get the boundary edges  
  !
  call ptoedg2(lface,nface,npoin,nnofa,ptoel1,ptoel2,nedgb)
  !
  !     Get the element to edge correspondance
  !  
  call edglem2(lface,nnofa,nface,ledgfa,nnofa,ptoed3,ledgb,nedgb)
  !
  !     Allocate edsha
  !  
  allocate(edsha(5,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'EDSHA','edgfil',edsha)
  !
  !     Allocate edshab
  !  
  allocate(edshab(3,nedgb),stat=istat)
  call memchk(zero,istat,memor_edg,'EDSHAB','edgfil',edshab)
  !
  !     Allocate edshap
  !  
  allocate(edshap(3,nboup),stat=istat)
  call memchk(zero,istat,memor_edg,'EDSHAP','edgfil',edshap)
  !
  !     Get the geometry coefficients
  !
  call edggeo(nedge,nelem,elem,edsha,nnode,coor,ndim,npoin,nsid,ledglm,rmass,lface,rnofa,nnofa,nface,edshab,ledgfa,nedgb,edshap,nboup,lboup,ledge,mboup)
  !
  !     Check normal orientation
  !
  call chkgrad(nedge,edsha,nedgb,edshab,nboup,edshap,npoin,mboup,ledge,lboup,ledgb)

  call memchk(2_ip,istat,memor_edg,'LEDGFA','edgfil',ledgfa)
  deallocate(ledgfa,stat=istat)
  if(istat/=0) call memerr(2_ip,'LEDGFA','edgfil',0_ip)
  call memchk(2_ip,istat,memor_edg,'LEDGLM','edgfil',ledglm)
  deallocate(ledglm,stat=istat)
  if(istat/=0) call memerr(2_ip,'LEDGLM','edgfil',0_ip)

end subroutine edgfil

subroutine getbc(ptoel1,ptoel2,nface,lsurf,nboup,ndim,lbous,nsurf,npoin,rnofa,lface,nnofa,coor,mboup)
  use def_kintyp, only       :  ip,rp,lg
  use def_nasedg, only          : memor_edg,lboup,rdir
  use mod_memchk
  use mod_mshtol
  implicit none
  integer(ip),intent(in)       :: npoin,nsurf,nface,nnofa,ndim
  integer(ip),intent(inout)    :: nboup,mboup
  integer(ip),intent(in)       :: lsurf(nface),lbous(2,nsurf),lface(nnofa,nface)
  real(rp), intent(in)         :: rnofa(ndim,nface),coor(ndim,npoin),ptoel1(*),ptoel2(*)
  integer(ip)                  :: lstack(100),nstack,nstack0,nstack1,ipoin,isurf,jsurf,iface,istack,jstack,ifa,nfac
  integer(ip)                  :: isurf1,isurf2,isurft,icond1,icond2,kboup,nsloc,i,idif,icond,iboup 
  real(rp)                     :: rnx,rny,rnz,rnl,rnx1,rny1,rnz1,rnx2,rny2,rnz2,c00,c10
  integer(4)                   :: istat
  integer(ip),pointer          :: lpnt(:)

  c00=0.0d+00
  c10=1.0d+00
  !
  !     Allocate lpnt
  !   
  allocate(lpnt(npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'GETBC','lpnt',lpnt)
  !
  !     Compute normalized normals to boundary faces 
  !
  call gtfnrl(lface,nface,nnofa,ndim,coor,npoin,rnofa)
  !
  !     Count boundary points
  !
  nboup=0_ip
  do ipoin=1,npoin
     nfac=ptoel2(ipoin+1)-ptoel2(ipoin) 
     if(nfac>0)then
        nboup=nboup+1
     endif
  enddo
  !
  !     Allocate lboup
  !  
  allocate(lboup(6,nboup),stat=istat)
  call memchk(zero,istat,memor_edg,'LBOUP','getbc',lboup)
  allocate(rdir(ndim,nboup),stat=istat)
  call memchk(zero,istat,memor_edg,'RDIR','getbc',rdir)
  !
  !     Remember nboup for after
  !
  mboup=nboup 
  !
  !     Then identify corners, ridge points, surface points
  !
  nboup=0_ip
  do ipoin=1,npoin
     nstack=0_ip

     do ifa=ptoel2(ipoin),ptoel2(ipoin+1)-1
        iface=ptoel1(ifa)
        isurf=lsurf(iface)

        do istack=1,nstack
           if(lstack(istack)==isurf)then
              exit
           endif
        enddo

        if(istack>nstack)then
           nstack=nstack+1
           lstack(nstack)=isurf
        endif

     enddo

     !
     !    Check if we have a surface with free condition
     ! 
     nstack0=nstack
     nstack=0_ip
     do istack=1,nstack0
        isurf=lstack(istack)
        if(lbous(1,isurf)/=0)then
           nstack=nstack+1
           lstack(nstack)=isurf 
        endif
     enddo
     !
     !    Check if we have a surface with characteristic condition
     ! 
     nstack1=nstack
     out_loop:do istack=1,nstack1
        isurf=lstack(istack)
        if(lbous(1,isurf)<0)then
           !
           !   Check if there is an inflow  
           !
           do jstack=1,nstack1
              jsurf=lstack(jstack)
              if(lbous(1,jsurf)<0 .and. lbous(1,jsurf)>-3)then
                 nstack=1_ip
                 lstack(1)=jsurf 
                 exit out_loop
              endif
           enddo

           nstack=1_ip
           lstack(1)=isurf 
           exit
        endif
     enddo out_loop

     !
     !     Analyse what we have
     !

     if(nstack==0)then
        !
        !     Do we have a free condition?
        !   
        if(nstack0==0)then
           !
           !     Not a boundary point
           !
           cycle

        else
           !
           !     A free boundary condition point
           ! 
           nboup=nboup+1
           lboup(1,nboup)=ipoin
           lboup(2,nboup)=0
           rdir(1,nboup)=c00
           rdir(2,nboup)=c00
           rdir(3,nboup)=c00

        endif

     else if(nstack==1)then
        !
        !     Boundary surface point
        !
        nboup=nboup+1          
        lboup(1,nboup)=ipoin
        lpnt(ipoin)=nboup
        isurft=lstack(1)
        lboup(2,nboup)=lbous(1,isurft)
        !
        !     Compute normal
        !   
        rnx=c00
        rny=c00
        rnz=c00

        do ifa=ptoel2(ipoin),ptoel2(ipoin+1)-1
           iface=ptoel1(ifa)
           isurf=lsurf(iface)
           if(isurf==isurft)then
              rnx=rnx+rnofa(1,iface)     
              rny=rny+rnofa(2,iface)     
              rnz=rnz+rnofa(3,iface)
           endif
        enddo

        rnl=sqrt(rnx*rnx+rny*rny+rnz*rnz)
        rnl=c10/rnl
        rnx=rnx*rnl
        rny=rny*rnl
        rnz=rnz*rnl

        rdir(1,nboup)=rnx
        rdir(2,nboup)=rny
        rdir(3,nboup)=rnz
        !
        !     Set surface attached to point and surface number
        !
        lboup(4,nboup)=isurft
        lboup(6,nboup)=1_ip

     else if(nstack==2)then 

        !
        !     Line point, compute tangent
        !

        nboup=nboup+1          
        lboup(1,nboup)=ipoin
        lpnt(ipoin)=nboup
        isurf1=lstack(1)
        icond1=lbous(1,isurf1)
        isurf2=lstack(2)
        icond2=lbous(1,isurf2)
        !
        !     Set surface attached to point and surface number
        !
        lboup(4,nboup)=isurf1
        lboup(5,nboup)=isurf2
        lboup(6,nboup)=2_ip

        if(icond1==1 .or. icond2==1 )then
           !
           !     Some surface is completely imposed
           ! 
           lboup(2,nboup)=1_ip
           rdir(1,nboup)=c00
           rdir(2,nboup)=c00
           rdir(3,nboup)=c00

        else if(icond1==2 .or. icond2==2)then 
           !
           !     Some surface is completely imposed for the velocity
           ! 
           lboup(2,nboup)=2_ip
           rdir(1,nboup)=c00
           rdir(2,nboup)=c00
           rdir(3,nboup)=c00


        else
           !
           !     Compute normal 1
           !   
           rnx=c00
           rny=c00
           rnz=c00

           do ifa=ptoel2(ipoin),ptoel2(ipoin+1)-1
              iface=ptoel1(ifa)
              isurf=lsurf(iface)
              if(isurf==isurf1)then
                 rnx=rnx+rnofa(1,iface)     
                 rny=rny+rnofa(2,iface)     
                 rnz=rnz+rnofa(3,iface) 
              endif
           enddo

           rnl=sqrt(rnx*rnx+rny*rny+rnz*rnz)
           rnl=c10/rnl
           rnx1=rnx*rnl
           rny1=rny*rnl
           rnz1=rnz*rnl
           !
           !     Compute normal 2
           !   

           rnx=c00
           rny=c00
           rnz=c00

           do ifa=ptoel2(ipoin),ptoel2(ipoin+1)-1
              iface=ptoel1(ifa)
              isurf=lsurf(iface)
              if(isurf==isurf2)then
                 rnx=rnx+rnofa(1,iface)     
                 rny=rny+rnofa(2,iface)     
                 rnz=rnz+rnofa(3,iface) 
              endif
           enddo

           rnl=sqrt(rnx*rnx+rny*rny+rnz*rnz)
           rnl=c10/rnl
           rnx2=rnx*rnl
           rny2=rny*rnl
           rnz2=rnz*rnl

           !
           !     Compute tangent
           !   

           rnx= rny1*rnz2-rnz1*rny2 
           rny=-rnx1*rnz2+rnz1*rnx2 
           rnz= rnx1*rny2-rny1*rnx2
           rnl=sqrt(rnx*rnx+rny*rny+rnz*rnz)
           rnl=c10/rnl
           rnx=rnx*rnl
           rny=rny*rnl
           rnz=rnz*rnl

           rdir(1,nboup)=rnx
           rdir(2,nboup)=rny
           rdir(3,nboup)=rnz
           lboup(2,nboup)=3_ip  

        endif

     else

        !
        !     Set surface attached to point and surface number
        !

        !
        !     Fill the first two surfaces
        !
        nboup=nboup+1          
        lboup(4,nboup)=lstack(1)
        lboup(5,nboup)=lstack(2)
        mboup=mboup+1
        lboup=>memrea(mboup,memor_edg,'LBOUP','getbc',lboup)
        lboup(6,nboup)=-mboup 
        kboup=mboup
        nsloc=2_ip

        do
           idif=nstack-nsloc
           if(idif<6)then

              do i=1,idif
                 lboup(i,kboup)=lstack(nsloc+i)
              enddo
              lboup(6,kboup)=idif
              exit

           else 

              do i=1,5
                 lboup(i,kboup)=lstack(nsloc+i)
              enddo
              nsloc=nsloc+5
              mboup=mboup+1
              lboup=>memrea(mboup,memor_edg,'LBOUP','getbc',lboup)
              lboup(6,kboup)=-mboup 
              kboup=mboup
           endif
        enddo

        !
        !     Corner point
        !

        do istack=1,nstack
           isurf1=lstack(istack)
           icond1=lbous(1,isurf1)
           if(icond1==1)exit
        enddo

        !
        !     Is everything to be imposed or only the velocity ?
        !

        if(istack>nstack)then

           lboup(2,nboup)=2_ip

        else

           lboup(2,nboup)=1_ip

        endif

        lboup(1,nboup)=ipoin
        lpnt(ipoin)=nboup

     endif

  enddo

  !
  !     Scatter characteristic boundary conditions
  ! 
  do iface=1,nface
     isurf=lsurf(iface)
     if(lbous(1,isurf)<0)then
        icond=-lbous(1,isurf)
        ipoin=lface(1,iface)
        iboup=lpnt(ipoin)
        lboup(3,iboup)=isurf
        ipoin=lface(2,iface)
        iboup=lpnt(ipoin) 
        lboup(3,iboup)=isurf
        ipoin=lface(3,iface)
        iboup=lpnt(ipoin) 
        lboup(3,iboup)=isurf
     endif
  enddo

  !
  !     Give priority to inflow characteristic bc
  ! 
  do iface=1,nface
     isurf=lsurf(iface)
     if(lbous(1,isurf)<0)then
        icond=-lbous(1,isurf)
        if(icond<3)then
           ipoin=lface(1,iface)
           iboup=lpnt(ipoin)
           lboup(3,iboup)=isurf
           ipoin=lface(2,iface)
           iboup=lpnt(ipoin) 
           lboup(3,iboup)=isurf
           ipoin=lface(3,iface)
           iboup=lpnt(ipoin) 
           lboup(3,iboup)=isurf
        endif
     endif
  enddo

  call memchk(2_ip,istat,memor_edg,'LPNT','getbc',lpnt)
  deallocate(lpnt,stat=istat)
  if(istat/=0) call memerr(2_ip,'LPNT','getbc',0_ip)

end subroutine getbc

subroutine edggeo(nedge,nelem,elem,edsha,nnode,coor,ndim,npoin,nsid,ledglm,rmass,lface,rnofa,nnofa,nface,edshab,ledgfa,nedgb,edshap,nboup,lboup,ledge,mboup)
  use def_kintyp, only       :  ip,rp,lg
  use def_nasedg, only          : memor_edg
  use mod_memchk
  use mod_mshtol
  implicit none
  integer(ip),intent(in)     :: npoin,nsid,nnode,ndim,nface,nnofa,nelem,nedge,nedgb,nboup
  integer(ip), intent(in)    :: lface(nnofa,nface),elem(nnode,nelem),ledgfa(nnofa,nface),ledge(2,nedge)
  integer(ip),intent(in)     :: ledglm(nsid,nelem),mboup,lboup(6,mboup)
  real(rp), intent(in)       :: coor(ndim,npoin)
  real(rp),intent(inout)     :: edsha(5,nedge),rmass(npoin),rnofa(nnofa,nface)
  real(rp),intent(inout)     :: edshab(3,nedgb),edshap(3,nboup)
  integer(4)                 :: istat
  integer(ip)                :: ielem,iedge,ip1,ip2,ip3,ip4,ipoin,iface,ipo
  integer(ip), pointer       :: lmark(:)
  real(rp)                   :: sh1x,sh1y,sh1z,sh2x,sh2y,sh2z,denom,c00,cwei4,cwei5,c10
  real(rp)                   :: sh3x,sh3y,sh3z,sh4x,sh4y,sh4z,cwei1,cwei2,cwei3,cwei6
  real(rp)                   :: x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4 
  real(rp)                   :: x21,y21,z21,x31,y31,z31,x41,y41,z41 
  real(rp)                   :: sign1,sign2,sign3,sign4,sign5,sign6,wei 
  real(rp)                   :: coef1,coef2,coef3,lx,ly,lz,ll,rnl,csca,denomi 

  cwei1=1.0d+00/(4.0d+00*6.0d+00*6.0d+00)      ! consistent mass
  cwei2=1.0d+00/(4.0d+00*2.0d+00*6.0d+00)      ! convective term
  cwei3=1.0d+00/6.0d+00                        ! Laplacian
  cwei4=1.0d+00/(4.0d+00*6.0d+00)                ! lumped mass
  cwei5=1.0d+00/48.0d+00               ! On faces
  cwei6=-1.0d+00/12.0d+00              ! On points

  c00=0.0d+00
  c10=1.0d+00

  !
  !     Clean up edsha and rmass
  !
  do iedge=1,nedge
     edsha(1,iedge)=c00
     edsha(2,iedge)=c00
     edsha(3,iedge)=c00
     edsha(4,iedge)=c00
     edsha(5,iedge)=c00
  enddo

  do iedge=1,nedgb
     edshab(1,iedge)=c00
     edshab(2,iedge)=c00
     edshab(3,iedge)=c00
  enddo

  do ipoin=1,nboup
     edshap(1,ipoin)=c00
     edshap(2,ipoin)=c00
     edshap(3,ipoin)=c00
  enddo

  do ipoin=1,npoin
     rmass(ipoin)=c00
  enddo
  !
  !     Compute non normalized normals to boundary faces 
  !
  call gtfnrl3(lface,nface,nnofa,ndim,coor,npoin,rnofa)

  !
  !     Fill volume edges
  !

  do ielem=1,nelem

     ip1=elem(1,ielem)
     ip2=elem(2,ielem)
     ip3=elem(3,ielem)
     ip4=elem(4,ielem)


     x1=coor(1,ip1)
     y1=coor(2,ip1)
     z1=coor(3,ip1)
     x2=coor(1,ip2)
     y2=coor(2,ip2)
     z2=coor(3,ip2)
     x3=coor(1,ip3)
     y3=coor(2,ip3)
     z3=coor(3,ip3)
     x4=coor(1,ip4)
     y4=coor(2,ip4)
     z4=coor(3,ip4)

     x21=x2-x1
     y21=y2-y1
     z21=z2-z1

     x31=x3-x1
     y31=y3-y1
     z31=z3-z1

     x41=x4-x1
     y41=y4-y1
     z41=z4-z1

     denom=x21*(y31*z41-z31*y41) + x31*(z21*y41-y21*z41) + x41*(y21*z31-z21*y31)


     if(denom<c00)then
        write(*,*)'Negative Jacobian, ielem=',ielem
        stop
     endif

     denomi=c10/denom

     sh2x= denomi*(y31*z41-z31*y41)
     sh3x= denomi*(z21*y41-y21*z41)
     sh4x= denomi*(y21*z31-z21*y31)
     sh1x=-sh2x-sh3x-sh4x

     sh2y= denomi*(z31*x41-x31*z41)
     sh3y= denomi*(x21*z41-z21*x41)
     sh4y= denomi*(z21*x31-x21*z31)
     sh1y=-sh2y-sh3y-sh4y

     sh2z= denomi*(x31*y41-y31*x41)
     sh3z= denomi*(y21*x41-x21*y41)
     sh4z= denomi*(x21*y31-y21*x31)
     sh1z=-sh2z-sh3z-sh4z

     wei=denom*cwei2

     sign1=float(ip2-ip1)
     sign2=float(ip3-ip1)
     sign3=float(ip4-ip1)
     sign4=float(ip3-ip2)
     sign5=float(ip4-ip2)
     sign6=float(ip4-ip3)

     sign1=sign(wei,sign1)
     sign2=sign(wei,sign2)
     sign3=sign(wei,sign3)
     sign4=sign(wei,sign4)
     sign5=sign(wei,sign5)
     sign6=sign(wei,sign6)

     iedge=ledglm(1,ielem)
     edsha(1,iedge)=edsha(1,iedge)+denom*cwei1
     edsha(2,iedge)=edsha(2,iedge)+(sh1x-sh2x)*sign1
     edsha(3,iedge)=edsha(3,iedge)+(sh1y-sh2y)*sign1
     edsha(4,iedge)=edsha(4,iedge)+(sh1z-sh2z)*sign1
     edsha(5,iedge)=edsha(5,iedge)+(sh1x*sh2x+sh1y*sh2y+sh1z*sh2z)*cwei3*denom

     iedge=ledglm(2,ielem)
     edsha(1,iedge)=edsha(1,iedge)+denom*cwei1
     edsha(2,iedge)=edsha(2,iedge)+(sh1x-sh3x)*sign2
     edsha(3,iedge)=edsha(3,iedge)+(sh1y-sh3y)*sign2
     edsha(4,iedge)=edsha(4,iedge)+(sh1z-sh3z)*sign2
     edsha(5,iedge)=edsha(5,iedge)+(sh1x*sh3x+sh1y*sh3y+sh1z*sh3z)*cwei3*denom

     iedge=ledglm(3,ielem)
     edsha(1,iedge)=edsha(1,iedge)+denom*cwei1
     edsha(2,iedge)=edsha(2,iedge)+(sh1x-sh4x)*sign3
     edsha(3,iedge)=edsha(3,iedge)+(sh1y-sh4y)*sign3
     edsha(4,iedge)=edsha(4,iedge)+(sh1z-sh4z)*sign3
     edsha(5,iedge)=edsha(5,iedge)+(sh1x*sh4x+sh1y*sh4y+sh1z*sh4z)*cwei3*denom 

     iedge=ledglm(4,ielem)
     edsha(1,iedge)=edsha(1,iedge)+denom*cwei1
     edsha(2,iedge)=edsha(2,iedge)+(sh2x-sh3x)*sign4
     edsha(3,iedge)=edsha(3,iedge)+(sh2y-sh3y)*sign4
     edsha(4,iedge)=edsha(4,iedge)+(sh2z-sh3z)*sign4
     edsha(5,iedge)=edsha(5,iedge)+(sh2x*sh3x+sh2y*sh3y+sh2z*sh3z)*cwei3*denom

     iedge=ledglm(5,ielem)
     edsha(1,iedge)=edsha(1,iedge)+denom*cwei1
     edsha(2,iedge)=edsha(2,iedge)+(sh2x-sh4x)*sign5
     edsha(3,iedge)=edsha(3,iedge)+(sh2y-sh4y)*sign5
     edsha(4,iedge)=edsha(4,iedge)+(sh2z-sh4z)*sign5
     edsha(5,iedge)=edsha(5,iedge)+(sh2x*sh4x+sh2y*sh4y+sh2z*sh4z)*cwei3*denom

     iedge=ledglm(6,ielem)
     edsha(1,iedge)=edsha(1,iedge)+denom*cwei1
     edsha(2,iedge)=edsha(2,iedge)+(sh3x-sh4x)*sign6
     edsha(3,iedge)=edsha(3,iedge)+(sh3y-sh4y)*sign6
     edsha(4,iedge)=edsha(4,iedge)+(sh3z-sh4z)*sign6
     edsha(5,iedge)=edsha(5,iedge)+(sh3x*sh4x+sh3y*sh4y+sh3z*sh4z)*cwei3*denom

     rmass(ip1)=rmass(ip1)+denom*cwei4
     rmass(ip2)=rmass(ip2)+denom*cwei4
     rmass(ip3)=rmass(ip3)+denom*cwei4
     rmass(ip4)=rmass(ip4)+denom*cwei4

  enddo

  !
  !     Check Laplacian
  !
  !do iedge=1,nedge
  !   if(edsha(5,iedge)>0.0d+00)then
  !      write(*,*)'Positive Laplacian:',iedge,ledge(1,iedge),ledge(2,iedge)
  !   endif
  !enddo 

  !   Check Alignement

  !do iedge=1,nedge
  !   ip1=ledge(1,iedge)
  !   ip2=ledge(2,iedge)
  ! 
  !    lx=coor(1,ip2)-coor(1,ip1)
  !    ly=coor(2,ip2)-coor(2,ip1)
  !    lz=coor(3,ip2)-coor(3,ip1)
  !    ll=sqrt(lx*lx+ly*ly+lz*lz)
  !    ll=c10/ll
  !    lx=lx*ll
  !    ly=ly*ll
  !    lz=lz*ll

  !    coef1=edsha(2,iedge)
  !    coef2=edsha(3,iedge)
  !    coef3=edsha(4,iedge)
  !    rnl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3)
  !    rnl=c10/rnl
  !    coef1=coef1*rnl
  !    coef2=coef2*rnl
  !    coef3=coef3*rnl

  !     csca=lx*coef1+ly*coef2+lz*coef3

  !  if(csca>c00)then
  !     write(*,*)'pb alignement'
  !  endif
  !  enddo 


  !
  !     Invert rmass
  ! 
  do ipoin=1,npoin
     rmass(ipoin)=c10/rmass(ipoin)
  enddo

  !
  !     Fill boundary edges
  !
  do iface=1,nface

     sh1x=rnofa(1,iface)*cwei5
     sh1y=rnofa(2,iface)*cwei5
     sh1z=rnofa(3,iface)*cwei5

     iedge=ledgfa(1,iface)
     edshab(1,iedge)=edshab(1,iedge)+sh1x
     edshab(2,iedge)=edshab(2,iedge)+sh1y
     edshab(3,iedge)=edshab(3,iedge)+sh1z

     iedge=ledgfa(2,iface)
     edshab(1,iedge)=edshab(1,iedge)+sh1x
     edshab(2,iedge)=edshab(2,iedge)+sh1y
     edshab(3,iedge)=edshab(3,iedge)+sh1z

     iedge=ledgfa(3,iface)
     edshab(1,iedge)=edshab(1,iedge)+sh1x
     edshab(2,iedge)=edshab(2,iedge)+sh1y
     edshab(3,iedge)=edshab(3,iedge)+sh1z

  enddo
  !
  !     Allocate lmark
  !

  allocate(lmark(npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'LMARK','edggeo',lmark)

  !
  !     Gather lboup
  !

  do ipo=1,nboup
     ipoin=lboup(1,ipo)
     lmark(ipoin)=ipo
  enddo

  !
  !     Fill boundary points
  !

  do  iface=1,nface
     ip1=lface(1,iface)
     ip2=lface(2,iface)
     ip3=lface(3,iface)

     ip1=lmark(ip1) 
     ip2=lmark(ip2) 
     ip3=lmark(ip3) 

     sh1x=cwei6*rnofa(1,iface)
     sh1y=cwei6*rnofa(2,iface)
     sh1z=cwei6*rnofa(3,iface)

     edshap(1,ip1)=edshap(1,ip1)+sh1x
     edshap(2,ip1)=edshap(2,ip1)+sh1y
     edshap(3,ip1)=edshap(3,ip1)+sh1z

     edshap(1,ip2)=edshap(1,ip2)+sh1x
     edshap(2,ip2)=edshap(2,ip2)+sh1y
     edshap(3,ip2)=edshap(3,ip2)+sh1z

     edshap(1,ip3)=edshap(1,ip3)+sh1x
     edshap(2,ip3)=edshap(2,ip3)+sh1y
     edshap(3,ip3)=edshap(3,ip3)+sh1z


  enddo

  call memchk(2_ip,istat,memor_edg,'LMARK','edggeo',lmark)
  deallocate(lmark,stat=istat)
  if(istat/=0) call memerr(2_ip,'LMARK','edggeo',0_ip)

end subroutine edggeo



subroutine ptoedg( &
     lface, nface , npoin, nnofa,ptoel1,ptoel2,nedge   )
  use def_kintyp, only       :  ip,rp,lg
  use mod_memchk
  use def_meshin, only          : memor_msh,ledge
  use def_nasedg, only          : ptoed2
  implicit none
  integer(ip), intent(in)            :: nface,npoin,nnofa,ptoel1(*)
  integer(ip), intent(in)            :: lface(nnofa,nface),ptoel2(*)
  integer(ip), intent(inout)         :: nedge
  integer(ip)                        :: iface,j,ipoin,ip1,iel
  integer(ip), pointer               :: lmark(:)
  integer(4)                 :: istat


  allocate(lmark(npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'LMARK','ptoedg',lmark)
  !
  !     First count the edges
  !
  !
  !     Loop on the points 
  !
  nedge=0_ip
  do ipoin=1,npoin

     !
     !     Loop on the elements surrounding the poin
     !
     do iel=ptoel2(ipoin),ptoel2(ipoin+1)-1
        iface=ptoel1(iel)
        do j=1,nnofa
           ip1=lface(j,iface)
           if(lmark(ip1)/=ipoin)then
              lmark(ip1)=ipoin 
              if(ipoin<ip1)then
                 nedge=nedge+1  
              endif
           endif
        enddo
     enddo
  enddo
  !
  !     Allocate the edges
  !
  if(.not.associated(ledge))then
     allocate(ledge(2,nedge),stat=istat)
     call memchk(zero,istat,memor_msh,'LEDGE','ptoedg',ledge)
  else
     ledge=>memrea(nedge,memor_msh,'LEDGE','ptoedg',ledge)
  endif
  if(.not.associated(ptoed2))then
     allocate(ptoed2(npoin+1),stat=istat)
     call memchk(zero,istat,memor_msh,'PTOED2','ptoedg',ptoed2)
  else
     ptoed2=>memrea(npoin+1,memor_msh,'PTOED2','ptoedg',ptoed2)
  endif
  !
  !     Reset lmark
  !
  lmark=0_ip
  !
  !     Fill the edges
  !
  !
  !     Loop on the points 
  !
  nedge=0_ip
  ptoed2(1)=1_ip
  do ipoin=1,npoin

     !
     !     Loop on the elements surrounding the point
     !
     do iel=ptoel2(ipoin),ptoel2(ipoin+1)-1
        iface=ptoel1(iel)
        do j=1,nnofa
           ip1=lface(j,iface)
           if(lmark(ip1)/=ipoin)then
              lmark(ip1)=ipoin  
              if(ipoin<ip1)then
                 nedge=nedge+1  
                 ledge(1,nedge)=ipoin
                 ledge(2,nedge)=ip1
              endif
           endif
        enddo
     enddo
     ptoed2(ipoin+1)=nedge+1
  enddo

  call memchk(2_ip,istat,memor_msh,'LMARK','ptoedg',lmark)
  deallocate(lmark,stat=istat)
  if(istat/=0) call memerr(2_ip,'LMARK','ptoedg',0_ip)

end subroutine ptoedg

subroutine ptoedg2( &
     lface, nface , npoin, nnofa,ptoel1,ptoel2,nedge   )
  use def_kintyp, only       :  ip,rp,lg
  use mod_memchk
  use def_nasedg, only          :memor_edg, ptoed3,ledgb
  implicit none
  integer(ip), intent(in)            :: nface,npoin,nnofa,ptoel1(*)
  integer(ip), intent(in)            :: lface(nnofa,nface),ptoel2(*)
  integer(ip), intent(inout)         :: nedge
  integer(ip)                        :: iface,j,ipoin,ip1,iel
  integer(ip), pointer               :: lmark(:)
  integer(4)                 :: istat


  allocate(lmark(npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'LMARK','ptoedg',lmark)
  !
  !     First count the edges
  !
  !
  !     Loop on the points 
  !
  nedge=0_ip
  do ipoin=1,npoin
     !
     !     Loop on the elements surrounding the point
     !
     do iel=ptoel2(ipoin),ptoel2(ipoin+1)-1
        iface=ptoel1(iel)
        do j=1,nnofa
           ip1=lface(j,iface)
           if(lmark(ip1)/=ipoin)then
              lmark(ip1)=ipoin  
              if(ipoin<ip1)then
                 nedge=nedge+1  
              endif
           endif
        enddo
     enddo
  enddo
  !
  !     Allocate ledgb 
  ! 
  allocate(ledgb(2,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'ledgb','ptoedg2',ledgb)

  allocate(ptoed3(npoin+1),stat=istat)
  call memchk(zero,istat,memor_edg,'PTOED3','ptoedg2',ptoed3)
  !
  !     Reset lmark
  !
  lmark=0_ip
  !
  !     Fill the edges
  !
  !
  !     Loop on the points 
  !
  nedge=0_ip
  ptoed3(1)=1_ip
  do ipoin=1,npoin

     !
     !     Loop on the elements surrounding the point
     !
     do iel=ptoel2(ipoin),ptoel2(ipoin+1)-1
        iface=ptoel1(iel)
        do j=1,nnofa
           ip1=lface(j,iface)
           if(lmark(ip1)/=ipoin)then
              lmark(ip1)=ipoin  
              if(ipoin<ip1)then
                 nedge=nedge+1  
                 ledgb(1,nedge)=ipoin
                 ledgb(2,nedge)=ip1
              endif
           endif
        enddo
     enddo
     ptoed3(ipoin+1)=nedge+1
  enddo

  call memchk(2_ip,istat,memor_edg,'LMARK','ptoedg2',lmark)
  deallocate(lmark,stat=istat)
  if(istat/=0) call memerr(2_ip,'LMARK','ptoedg2',0_ip)

end subroutine ptoedg2
