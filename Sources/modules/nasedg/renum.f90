subroutine renupo(npoin,ndim,coor,ptoel1,ptoel2,elem,nnode,nelem,lface,nface,nnofa)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_nasedg, only          : memor_edg  
  implicit none
  integer(ip),intent(in)   :: npoin,ndim,nnode,nelem,nnofa,nface
  integer(ip),intent(inout):: elem(nnode,nelem),lface(nnofa,nface)
  real(rp),intent(inout)   :: coor(ndim,npoin)
  integer(ip),intent(in)   :: ptoel1(*),ptoel2(*)
  integer(ip),pointer      :: lrenu(:),lstack(:)
  real(rp),pointer         :: coort(:,:)
  real(rp)                 :: xmax,ymax,zmax
  integer(ip)              :: ipmax,ipoin,istack,nstack,iter,iel,ielem,j,ip1,ipnew,iface 
  integer(4)               :: istat
  !
  !     This subroutine renumbers the points based on an advancing front like renumbering
  !


  !
  !     Allocate help arrays
  !

  allocate(lrenu(npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'LRENU','renupo',lrenu)
  allocate(lstack(npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'LSTACK','renupo',lstack)
  allocate(coort(ndim,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'LSTACK','renupo',lstack)

  !
  !     Find the max
  ! 
  xmax=coor(1,1)
  ymax=coor(2,1)
  zmax=coor(3,1)
  ipmax=1_ip

  do ipoin=2,npoin
     if(coor(1,ipoin)>xmax)then
        xmax=coor(1,ipoin)
        ymax=coor(2,ipoin)
        zmax=coor(3,ipoin)
        ipmax=ipoin
     endif
  enddo


  do ipoin=1,npoin
     if(coor(1,ipoin)==xmax)then
        if(coor(2,ipoin)>ymax)then
           xmax=coor(1,ipoin)
           ymax=coor(2,ipoin)
           zmax=coor(3,ipoin)
           ipmax=ipoin
        endif
     endif
  enddo

  do ipoin=1,npoin
     if(coor(1,ipoin)==xmax)then
        if(coor(2,ipoin)==ymax)then
           if(coor(3,ipoin)>zmax)then
              xmax=coor(1,ipoin)
              ymax=coor(2,ipoin)
              zmax=coor(3,ipoin)
              ipmax=ipoin
           endif
        endif
     endif
  enddo

  lstack(1)=ipmax
  lrenu(ipmax)=1_ip
  nstack=1_ip
  !
  !     Find a diameter
  !
  do iter=1,5

     istack=0_ip

     do 
        if(istack==nstack)exit
        istack=istack+1
        ipoin=lstack(istack)
        !
        !     Loop on elements surrounding points
        !
        do iel=ptoel2(ipoin),ptoel2(ipoin+1)-1
           ielem=ptoel1(iel)
           do j=1,nnode
              !
              !     Loop on nodes of elements
              !
              ip1=elem(j,ielem)
              if(lrenu(ip1)==0)then
                 !
                 !     Has this point been marked before?
                 !
                 nstack=nstack+1
                 lstack(nstack)=ip1
                 !lrenu(ip1)=lrenu(ipoin)+1_ip
                 lrenu(ip1)=nstack
              endif
           enddo
        enddo

     enddo

     !
     !     We have reached the end of the stack, take the last point and go back 
     !
     if(istack/=npoin)then
        write(*,*)'error renupo'
        stop
     endif
   
     !call outgidpnt(npoin,ndim,elem,nelem,nnode,coor,lrenu)

     ipmax=lstack(nstack)
     nstack=1_ip
     lstack(1)=ipmax
     lrenu=0_ip
     lrenu(ipmax)=1_ip

  enddo

  !
  !     Renumber the points
  !
  istack=0_ip

  do 
     if(istack==nstack)exit
     istack=istack+1
     ipoin=lstack(istack)
     !
     !     Loop on elements surrounding points
     !
     do iel=ptoel2(ipoin),ptoel2(ipoin+1)-1
        ielem=ptoel1(iel)
        do j=1,nnode
           !
           !     Loop on nodes of elements
           !
           ip1=elem(j,ielem)
           if(lrenu(ip1)==0)then
              !
              !     Has this point been marked before?
              !
              nstack=nstack+1
              !lrenu(ip1)=lrenu(ipoin)+1_ip
              lrenu(ip1)=nstack
              lstack(nstack)=ip1
           endif
        enddo
     enddo

  enddo

  if(istack/=npoin)then
     write(*,*)'error renupo'
     stop
  endif

  !
  !     Permute the points
  !
  do ipoin=1,npoin
     ipnew=lrenu(ipoin)
     coort(1,ipnew)=coor(1,ipoin)
     coort(2,ipnew)=coor(2,ipoin)
     coort(3,ipnew)=coor(3,ipoin)
  enddo
  !
  !     Copy in coor
  !
  do ipoin=1,npoin
     coor(1,ipoin)=coort(1,ipoin)
     coor(2,ipoin)=coort(2,ipoin)
     coor(3,ipoin)=coort(3,ipoin)
  enddo


  !
  !     Renumber elem
  !
  do ielem=1,nelem
     elem(1,ielem)=lrenu(elem(1,ielem))
     elem(2,ielem)=lrenu(elem(2,ielem))
     elem(3,ielem)=lrenu(elem(3,ielem))
     elem(4,ielem)=lrenu(elem(4,ielem))
  enddo

  !
  !     Renumber lface
  !
  do iface=1,nface
     lface(1,iface)=lrenu(lface(1,iface))
     lface(2,iface)=lrenu(lface(2,iface))
     lface(3,iface)=lrenu(lface(3,iface))
  enddo







  call memchk(2_ip,istat,memor_edg,'COORT','renupo',coort)
  deallocate(coort,stat=istat)
  if(istat/=0) call memerr(2_ip,'COORT','renupo',0_ip)
  call memchk(2_ip,istat,memor_edg,'LSTACK','renupo',lstack)
  deallocate(lstack,stat=istat)
  if(istat/=0) call memerr(2_ip,'LSTACK','renupo',0_ip)
  call memchk(2_ip,istat,memor_edg,'LRENU','renupo',lrenu)
  deallocate(lrenu,stat=istat)
  if(istat/=0) call memerr(2_ip,'LRENU','renupo',0_ip)

end subroutine renupo


subroutine renuinel(elem,nnode,nelem)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)     :: nnode,nelem
  integer(ip),intent(inout)  :: elem(nnode,nelem)
  integer(ip)          :: lnod2(4,4),lnod3(4,4),imin,imax,ielem
  integer(ip)          :: ip1,ip2,ip3,ip4


  lnod2(1,1)=0
  lnod2(2,1)=3
  lnod2(3,1)=4
  lnod2(4,1)=2
  lnod2(1,2)=4
  lnod2(2,2)=0
  lnod2(3,2)=1
  lnod2(4,2)=3
  lnod2(1,3)=2
  lnod2(2,3)=4
  lnod2(3,3)=0
  lnod2(4,3)=1
  lnod2(1,4)=3
  lnod2(2,4)=1
  lnod2(3,4)=2
  lnod2(4,4)=0

  lnod3(1,1)=0
  lnod3(2,1)=4
  lnod3(3,1)=2
  lnod3(4,1)=3
  lnod3(1,2)=3
  lnod3(2,2)=0
  lnod3(3,2)=4
  lnod3(4,2)=1
  lnod3(1,3)=4
  lnod3(2,3)=1
  lnod3(3,3)=0
  lnod3(4,3)=2
  lnod3(1,4)=2
  lnod3(2,4)=3
  lnod3(3,4)=1
  lnod3(4,4)=0

  !
  !     This subroutine renumbers the nodes contained inside an element
  !

  !
  !     Find the minimal point 
  !
  do ielem=1,nelem

     if(elem(1,ielem)<elem(2,ielem))then
        imin=1
     else
        imin=2
     endif
     if(elem(3,ielem)<elem(imin,ielem))imin=3
     if(elem(4,ielem)<elem(imin,ielem))imin=4

     if(elem(1,ielem)>elem(2,ielem))then
        imax=1
     else
        imax=2
     endif
     if(elem(3,ielem)>elem(imin,ielem))imax=3
     if(elem(4,ielem)>elem(imin,ielem))imax=4

     ip1=elem(imin,ielem) 
     ip2=elem(lnod2(imax,imin),ielem)
     ip3=elem(lnod3(imax,imin),ielem)
     ip4=elem(imax,ielem)


     elem(1,ielem)=ip1
     elem(2,ielem)=ip2
     elem(3,ielem)=ip3
     elem(4,ielem)=ip4


  enddo

end subroutine renuinel

subroutine renuel(elem,nnode,nelem,npoin)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_nasedg, only          : memor_edg  
  implicit none
  integer(ip),intent(in)     :: nnode,nelem,npoin
  integer(ip),intent(inout)  :: elem(nnode,nelem)
  integer(ip)                :: ielem,ipoin,ienew 
  integer(ip),pointer        :: elemt(:,:),lrenu(:),lpoin(:)
  integer(4)               :: istat
  ! 
  !     This subroutine renumber the elements
  !     It assumes that the first point in the element is the smallest
  !
  allocate(lpoin(npoin+1),stat=istat)
  call memchk(zero,istat,memor_edg,'LPOIN','renuel',lpoin)
  allocate(lrenu(nelem),stat=istat)
  call memchk(zero,istat,memor_edg,'LRENU','renuel',lrenu)
  allocate(elemt(nnode,nelem),stat=istat)
  call memchk(zero,istat,memor_edg,'ELEMT','renuel',elemt)

  !
  !     First reorder respect to the max
  !

  do ielem=1,nelem
     ipoin=elem(4,ielem)+1
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  lpoin(1)=1
  do ipoin=2,npoin+1
     lpoin(ipoin)=lpoin(ipoin)+lpoin(ipoin-1)
  enddo

  do ielem=1,nelem
     ipoin=elem(4,ielem)
     ienew=lpoin(ipoin)
     lrenu(ielem)=ienew
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  do ielem=1,nelem
     ienew=lrenu(ielem)
     elemt(1,ienew)=elem(1,ielem) 
     elemt(2,ienew)=elem(2,ielem) 
     elemt(3,ienew)=elem(3,ielem) 
     elemt(4,ienew)=elem(4,ielem) 
  enddo

  do ielem=1,nelem
     elem(1,ielem)=elemt(1,ielem) 
     elem(2,ielem)=elemt(2,ielem) 
     elem(3,ielem)=elemt(3,ielem) 
     elem(4,ielem)=elemt(4,ielem) 
  enddo

  !
  !     Then respect to the min
  !

  lpoin=0

  do ielem=1,nelem
     ipoin=elem(1,ielem)+1
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  lpoin(1)=1
  do ipoin=2,npoin+1
     lpoin(ipoin)=lpoin(ipoin)+lpoin(ipoin-1)
  enddo

  do ielem=1,nelem
     ipoin=elem(1,ielem)
     ienew=lpoin(ipoin)
     lrenu(ielem)=ienew
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  do ielem=1,nelem
     ienew=lrenu(ielem)
     elemt(1,ienew)=elem(1,ielem) 
     elemt(2,ienew)=elem(2,ielem) 
     elemt(3,ienew)=elem(3,ielem) 
     elemt(4,ienew)=elem(4,ielem) 
  enddo

  do ielem=1,nelem
     elem(1,ielem)=elemt(1,ielem) 
     elem(2,ielem)=elemt(2,ielem) 
     elem(3,ielem)=elemt(3,ielem) 
     elem(4,ielem)=elemt(4,ielem) 
  enddo

  call memchk(2_ip,istat,memor_edg,'ELEMT','renuel',elemt)
  deallocate(elemt,stat=istat)
  if(istat/=0) call memerr(2_ip,'ELEMT','renuel',0_ip)
  call memchk(2_ip,istat,memor_edg,'LRENU','renuel',lrenu)
  deallocate(lrenu,stat=istat)
  if(istat/=0) call memerr(2_ip,'LRENU','renuel',0_ip)
  call memchk(2_ip,istat,memor_edg,'LPOIN','renuel',lpoin)
  deallocate(lpoin,stat=istat)
  if(istat/=0) call memerr(2_ip,'LPOIN','renuel',0_ip)

end subroutine renuel

subroutine renuinfa(lface,nnofa,nface)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)     :: nnofa,nface
  integer(ip),intent(inout)  :: lface(nnofa,nface)
  integer(ip)          :: lnod2(2,3),imin,iface
  integer(ip)          :: ip1,ip2,ip3


  lnod2(1,1)=2
  lnod2(2,1)=3
  lnod2(1,2)=3
  lnod2(2,2)=1
  lnod2(1,3)=1
  lnod2(2,3)=2

  !
  !     This subroutine renumbers the nodes contained inside an element
  !

  !
  !     Find the minimal point 
  !
  do iface=1,nface

     if(lface(1,iface)<lface(2,iface))then
        imin=1
     else
        imin=2
     endif
     if(lface(3,iface)<lface(imin,iface))imin=3

     ip1=lface(imin,iface) 
     ip2=lface(lnod2(1,imin),iface)
     ip3=lface(lnod2(2,imin),iface)


     lface(1,iface)=ip1
     lface(2,iface)=ip2
     lface(3,iface)=ip3


  enddo

end subroutine renuinfa


subroutine renufa(lface,nnofa,nface,npoin,lsurf)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_nasedg, only          : memor_edg  
  implicit none
  integer(ip),intent(in)     :: nnofa,nface,npoin
  integer(ip),intent(inout)  :: lface(nnofa,nface),lsurf(nface)
  integer(ip)                :: iface,ipoin,ifnew 
  integer(ip),pointer        :: lfacet(:,:),lrenu(:),lpoin(:),lsurft(:)
  integer(4)               :: istat
  ! 
  !     This subroutine renumber the faces
  !     It assumes that the first point in the element is the smallest
  !
  allocate(lpoin(npoin+1),stat=istat)
  call memchk(zero,istat,memor_edg,'LPOIN','renufa',lpoin)
  allocate(lrenu(nface),stat=istat)
  call memchk(zero,istat,memor_edg,'LRENU','renufa',lrenu)
  allocate(lfacet(nnofa,nface),stat=istat)
  call memchk(zero,istat,memor_edg,'LFACET','renufa',lfacet)
  allocate(lsurft(nface),stat=istat)
  call memchk(zero,istat,memor_edg,'LSURFT','renufa',lsurft)

  !
  !     First reorder respect to the max
  !

  do iface=1,nface
     ipoin=lface(3,iface)+1
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  lpoin(1)=1
  do ipoin=2,npoin+1
     lpoin(ipoin)=lpoin(ipoin)+lpoin(ipoin-1)
  enddo

  do iface=1,nface
     ipoin=lface(3,iface)
     ifnew=lpoin(ipoin)
     lrenu(iface)=ifnew
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  do iface=1,nface
     ifnew=lrenu(iface)
     lsurft(ifnew)=lsurf(iface)
     lfacet(1,ifnew)=lface(1,iface) 
     lfacet(2,ifnew)=lface(2,iface) 
     lfacet(3,ifnew)=lface(3,iface) 
  enddo

  do iface=1,nface
     lsurf(iface)=lsurft(iface)
     lface(1,iface)=lfacet(1,iface) 
     lface(2,iface)=lfacet(2,iface) 
     lface(3,iface)=lfacet(3,iface) 
  enddo

  !
  !     Then respect to the min
  !

  lpoin=0

  do iface=1,nface
     ipoin=lface(1,iface)+1
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  lpoin(1)=1
  do ipoin=2,npoin+1
     lpoin(ipoin)=lpoin(ipoin)+lpoin(ipoin-1)
  enddo

  do iface=1,nface
     ipoin=lface(1,iface)
     ifnew=lpoin(ipoin)
     lrenu(iface)=ifnew
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  do iface=1,nface
     ifnew=lrenu(iface)
     lsurft(ifnew)=lsurf(iface)
     lfacet(1,ifnew)=lface(1,iface) 
     lfacet(2,ifnew)=lface(2,iface) 
     lfacet(3,ifnew)=lface(3,iface) 
  enddo

  do iface=1,nface
     lsurf(iface)=lsurft(iface)
     lface(1,iface)=lfacet(1,iface) 
     lface(2,iface)=lfacet(2,iface) 
     lface(3,iface)=lfacet(3,iface) 
  enddo

  call memchk(2_ip,istat,memor_edg,'LSURFT','renuel',lsurft)
  deallocate(lsurft,stat=istat)
  if(istat/=0) call memerr(2_ip,'LSURFT','renuel',0_ip)
  call memchk(2_ip,istat,memor_edg,'LFACET','renuel',lfacet)
  deallocate(lfacet,stat=istat)
  if(istat/=0) call memerr(2_ip,'LFACET','renuel',0_ip)
  call memchk(2_ip,istat,memor_edg,'LRENU','renuel',lrenu)
  deallocate(lrenu,stat=istat)
  if(istat/=0) call memerr(2_ip,'LRENU','renuel',0_ip)
  call memchk(2_ip,istat,memor_edg,'LPOIN','renuel',lpoin)
  deallocate(lpoin,stat=istat)
  if(istat/=0) call memerr(2_ip,'LPOIN','renuel',0_ip)

end subroutine renufa


subroutine renued(ledge,nedge,npoin,ledglm,nsid,nelem,elem,nnode)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_nasedg, only          : memor_edg  
  implicit none
  integer(ip),intent(in)     :: nedge,npoin,nelem,nnode,elem(nnode,nelem),nsid
  integer(ip),intent(inout)  :: ledge(2,nedge),ledglm(nsid,nelem)
  integer(ip)                :: iedge,ipoin,ienew 
  integer(ip),pointer        :: ledget(:,:),lrenu(:),lpoin(:)
  integer(ip)                :: ip1,ip2,ip3,ip4,ichk,ielem,ipa,ipb 
  integer(4)                 :: istat
  ! 
  !     This subroutine renumber the edges to minimize cache misses
  !     It assumes that the first point in the element is the smallest
  !
  allocate(lpoin(npoin+1),stat=istat)
  call memchk(zero,istat,memor_edg,'LPOIN','renued',lpoin)
  allocate(lrenu(nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'LRENU','renued',lrenu)
  allocate(ledget(2,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'LEDGET','renued',ledget)

  !
  !     First reorder respect to the max
  !

  do iedge=1,nedge
     ipoin=ledge(2,iedge)+1
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  lpoin(1)=1
  do ipoin=2,npoin+1
     lpoin(ipoin)=lpoin(ipoin)+lpoin(ipoin-1)
  enddo

  do iedge=1,nedge
     ipoin=ledge(2,iedge)
     ienew=lpoin(ipoin)
     lrenu(iedge)=ienew
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  do iedge=1,nedge
     ienew=lrenu(iedge)
     ledget(1,ienew)=ledge(1,iedge) 
     ledget(2,ienew)=ledge(2,iedge) 
  enddo

  do iedge=1,nedge
     ledge(1,iedge)=ledget(1,iedge) 
     ledge(2,iedge)=ledget(2,iedge) 
  enddo
  !
  !     Finally update ledglm
  !
  do ielem=1,nelem
     ledglm(1,ielem)=lrenu(ledglm(1,ielem))
     ledglm(2,ielem)=lrenu(ledglm(2,ielem))
     ledglm(3,ielem)=lrenu(ledglm(3,ielem))
     ledglm(4,ielem)=lrenu(ledglm(4,ielem))
     ledglm(5,ielem)=lrenu(ledglm(5,ielem))
     ledglm(6,ielem)=lrenu(ledglm(6,ielem))
  enddo

  !
  !     Then respect to the min
  !

  lpoin=0

  do iedge=1,nedge
     ipoin=ledge(1,iedge)+1
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  lpoin(1)=1
  do ipoin=2,npoin+1
     lpoin(ipoin)=lpoin(ipoin)+lpoin(ipoin-1)
  enddo

  do iedge=1,nedge
     ipoin=ledge(1,iedge)
     ienew=lpoin(ipoin)
     lrenu(iedge)=ienew
     lpoin(ipoin)=lpoin(ipoin)+1
  enddo

  do iedge=1,nedge
     ienew=lrenu(iedge)
     ledget(1,ienew)=ledge(1,iedge) 
     ledget(2,ienew)=ledge(2,iedge) 
  enddo

  do iedge=1,nedge
     ledge(1,iedge)=ledget(1,iedge) 
     ledge(2,iedge)=ledget(2,iedge) 
  enddo

  !
  !     Finally update ledglm
  !
  do ielem=1,nelem
     ledglm(1,ielem)=lrenu(ledglm(1,ielem))
     ledglm(2,ielem)=lrenu(ledglm(2,ielem))
     ledglm(3,ielem)=lrenu(ledglm(3,ielem))
     ledglm(4,ielem)=lrenu(ledglm(4,ielem))
     ledglm(5,ielem)=lrenu(ledglm(5,ielem))
     ledglm(6,ielem)=lrenu(ledglm(6,ielem))
  enddo


  !
  !     Debug
  !
  do ielem=1,nelem
     ip1=elem(1,ielem)
     ip2=elem(2,ielem)
     ip3=elem(3,ielem)
     ip4=elem(4,ielem)

     iedge=ledglm(1,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip1 .and. ipb==ip2) .or. (ipa==ip2 .and. ipb==ip1))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 1' 
        stop
     endif

     iedge=ledglm(2,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip1 .and. ipb==ip3) .or. (ipa==ip3 .and. ipb==ip1))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 2' 
        stop
     endif


     iedge=ledglm(3,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip1 .and. ipb==ip4) .or. (ipa==ip4 .and. ipb==ip1))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 3' 
        stop
     endif


     iedge=ledglm(4,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip2 .and. ipb==ip3) .or. (ipa==ip3 .and. ipb==ip2))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 4' 
        stop
     endif

     iedge=ledglm(5,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip2 .and. ipb==ip4) .or. (ipa==ip4 .and. ipb==ip2))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 5' 
        stop
     endif

     iedge=ledglm(6,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip3 .and. ipb==ip4) .or. (ipa==ip4 .and. ipb==ip3))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 6' 
        stop
     endif

  enddo

  call memchk(2_ip,istat,memor_edg,'LEDGET','renued',ledget)
  deallocate(ledget,stat=istat)
  if(istat/=0) call memerr(2_ip,'LEDGET','renued',0_ip)
  call memchk(2_ip,istat,memor_edg,'LRENU','renued',lrenu)
  deallocate(lrenu,stat=istat)
  if(istat/=0) call memerr(2_ip,'LRENU','renued',0_ip)

end subroutine renued


subroutine renumshared(ledge,nedge,npoin,ledglm,nsid,nelem,elem,nnode,nproc)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_nasedg, only          : memor_edg,edpar  
  implicit none
  integer(ip),intent(in)     :: nedge,npoin,nelem,nnode,elem(nnode,nelem),nsid
  integer(ip),intent(inout)  :: ledge(2,nedge),ledglm(nsid,nelem)
  integer(ip),intent(in)     :: nproc
  integer(ip)                :: iedge,ipoin,ienew 
  integer(ip),pointer        :: ledget(:,:),lrenu(:),lpoin(:)
  integer(ip)                :: nppg,ibegin,iend,nnpg,igrou,nedgnew
  integer(ip)                :: ip1,ip2,ip3,ip4,ichk,ielem,ipa,ipb 
  integer(ip)                :: ipmin,ipmax 
  integer(4)                 :: istat
  !
  !     This subroutine renumbers the edges for shared memory
  !     to avoid memory contention
  !     It assumes at the very least that the edges have been renumbered
  !     with respect to the first point
  !

  if(nproc<=1)return

  allocate(lpoin(npoin+1),stat=istat)
  call memchk(zero,istat,memor_edg,'LPOIN','renued',lpoin)
  allocate(lrenu(nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'LRENU','renued',lrenu)
  allocate(ledget(2,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'LEDGET','renued',ledget)
  allocate(edpar(nproc*10),stat=istat)
  call memchk(zero,istat,memor_edg,'EDPAR','renued',edpar)
  !
  !     Average range point per group
  ! 
  nppg=int(npoin/float(nproc))
  !
  !     First loop, agglomerate the edges
  !
  ibegin=1_ip
  iend=ibegin+nppg
  nedgnew=0_ip
  igrou=1_ip
  edpar(1)=1_ip

  do iedge=1,nedge

     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)

     !
     !     Check range
     !

     if(ip1>=ibegin)then
        if(ip2<=iend)then 
           !
           !     Renumber this edge
           !
           nedgnew=nedgnew+1 
           lrenu(iedge)=nedgnew
        endif

     else

        !
        !     Build a new group
        !
        igrou=igrou+1
        edpar=>memrea(igrou,memor_edg,'EDPAR','renumshare',edpar)
        edpar(igrou)=nedgnew
        nedgnew=nedgnew+1 
        lrenu(iedge)=nedgnew
        ibegin=iend+1
        iend=ibegin+nppg       

     endif

  enddo


  !
  !     Second part
  !

  do 

     !
     !     Determine new point range
     ! 
     ipmin=npoin+1
     ipmax=0_ip  
     ichk=0_ip

     do iedge=1,nedge
        if(lrenu(iedge)==0)then  
           ichk=1_ip
           ip1=ledge(1,iedge)    
           ip2=ledge(2,iedge)    
           if(ip1<ipmin)ipmin=ip1
           if(ip2>ipmax)ipmax=ip2
        endif
     enddo
     !
     !     Do we have still edges to renumber
     !
     if(ichk==0)exit

     nppg=int((ipmax-ipmin)/float(nproc)) 

     !
     !    New loop
     !
     ibegin=ipmin
     iend=ibegin+nppg

     do iedge=1,nedge

        ip1=ledge(1,iedge)
        ip2=ledge(2,iedge)

        !
        !     Check range
        !

        if(ip1>=ibegin)then
           if(ip2<=iend)then 
              !
              !     Renumber this edge
              !
              nedgnew=nedgnew+1 
              lrenu(iedge)=nedgnew
           endif

        else

           !
           !     Build a new group
           !
           igrou=igrou+1
           edpar=>memrea(igrou,memor_edg,'EDPAR','renumshare',edpar)
           edpar(igrou)=nedgnew
           nedgnew=nedgnew+1 
           lrenu(iedge)=nedgnew
           ibegin=iend+1
           iend=ibegin+nppg       

        endif

     enddo



  enddo

  !
  !     Transcribe the edges
  !


  do iedge=1,nedge
     ienew=lrenu(iedge)
     ledget(1,ienew)=ledge(1,iedge) 
     ledget(2,ienew)=ledge(2,iedge) 
  enddo

  do iedge=1,nedge
     ledge(1,iedge)=ledget(1,iedge) 
     ledge(2,iedge)=ledget(2,iedge) 
  enddo

  !
  !     Finally update ledglm
  !
  do ielem=1,nelem
     ledglm(1,ielem)=lrenu(ledglm(1,ielem))
     ledglm(2,ielem)=lrenu(ledglm(2,ielem))
     ledglm(3,ielem)=lrenu(ledglm(3,ielem))
     ledglm(4,ielem)=lrenu(ledglm(4,ielem))
     ledglm(5,ielem)=lrenu(ledglm(5,ielem))
     ledglm(6,ielem)=lrenu(ledglm(6,ielem))
  enddo


  !
  !     Debug
  !
  do ielem=1,nelem
     ip1=elem(1,ielem)
     ip2=elem(2,ielem)
     ip3=elem(3,ielem)
     ip4=elem(4,ielem)

     iedge=ledglm(1,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip1 .and. ipb==ip2) .or. (ipa==ip2 .and. ipb==ip1))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 1' 
        stop
     endif

     iedge=ledglm(2,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip1 .and. ipb==ip3) .or. (ipa==ip3 .and. ipb==ip1))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 2' 
        stop
     endif


     iedge=ledglm(3,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip1 .and. ipb==ip4) .or. (ipa==ip4 .and. ipb==ip1))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 3' 
        stop
     endif


     iedge=ledglm(4,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip2 .and. ipb==ip3) .or. (ipa==ip3 .and. ipb==ip2))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 4' 
        stop
     endif

     iedge=ledglm(5,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip2 .and. ipb==ip4) .or. (ipa==ip4 .and. ipb==ip2))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 5' 
        stop
     endif

     iedge=ledglm(6,ielem) 
     ipa=ledge(1,iedge)
     ipb=ledge(2,iedge)
     ichk=0
     if((ipa==ip3 .and. ipb==ip4) .or. (ipa==ip4 .and. ipb==ip3))then
        ichk=1
     endif
     if(ichk==0)then
        write(*,*)'error 6' 
        stop
     endif

  enddo

  call memchk(2_ip,istat,memor_edg,'LEDGET','renued',ledget)
  deallocate(ledget,stat=istat)
  if(istat/=0) call memerr(2_ip,'LEDGET','renued',0_ip)
  call memchk(2_ip,istat,memor_edg,'LRENU','renued',lrenu)
  deallocate(lrenu,stat=istat)
  if(istat/=0) call memerr(2_ip,'LRENU','renued',0_ip)
end subroutine renumshared


subroutine outgidpnt(npoin,ndimn,elem,nelem,nnode,coor,lrenu)
  implicit real*8 (a-h,o-z)
  integer npoin,ndimn,nnode
  integer lrenu(npoin),elem(nnode,nelem)
  real*8 coor(ndimn,npoin)
  open(unit=50,file='renupnt.msh',status='unknown')
  rewind 50
1 format('MESH dimension 3 ElemType Tetrahedra Nnode 4')
2 format('Coordinates')
3 format('#node number   coor_x   coor_y  coor_z')
100 format(i10,3e20.10)
200 format(5i10)
4 format('end coordinates')
5 format('Elements')
6 format('end elements')
  write(50,1)
  write(50,2)
  write(50,3)
  do 1000 ipoin=1,npoin
     rx=coor(1,ipoin)
     ry=coor(2,ipoin)
     rz=coor(3,ipoin)
     write(50,100)ipoin,rx,ry,rz
1000 continue
     write(50,4)
     write(50,5)
     do 2000 ielem=1,nelem
        write(50,200)ielem,elem(1,ielem),elem(2,ielem),elem(3,ielem),elem(4,ielem)
2000 continue
     write(50,6)
     close(50)

10 format('GID Post Results File 1.0')
12 format('ComponentNames "Vx", "Vy" , "Vz" ')
13 format('Values')
14 format('End Values')
15 format('   ')
16 format('Result "Renu" "Analysis/time" ','1',' Scalar OnNodes')
300 format(2i10)


 open(unit=60,file='renupnt.res',status='unknown')
 rewind 60
 write(60,10)
 write(60,16)
 write(60,13)
 do  ipoin=1,npoin
    write(60,300)ipoin,lrenu(ipoin)
 enddo
 write(60,14)

 close(60)








     return
end subroutine 

