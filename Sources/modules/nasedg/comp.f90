subroutine comp()
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_meshin, only          : coor,elem,ledge,lsurf,lface,rnofa
  use def_nasedg, only          : memor_edg,ptoed2,ledglm,flu,grad,rmass,varb
  use def_nasedg, only          : var,varn,varnn,varpn,dvar,edsha,extr1,extr2,sens 
  use def_nasedg, only          : edshab,edshap,ledgb,lboup,rdir,lbous,rbous,viscp 
  use def_nasedg, only          : hydro1,hydro2,rdtloc 

  implicit none
  !integer(ip), intent(in)    :: nrkord,ndim,nvar,nnode,nsid
  !integer(ip), intent(in)    :: nedge,npoin,nelem,nnofa,nface
  !integer(ip), intent(in)    :: nedgb,nboup
  !real(rp),intent(in)        :: gam
  integer(ip)                :: istep,i,igrav,ipre,idtloc
  real(rp)                   :: dt,dtau,gam,time,maxtime,ptime,tprint,fprint,dtaumax,cfl,vn
  real(rp)                   :: cvi,c10,c20,R,cv,cp,cpi,grav,temp0,scons,Pr,Pri,mu0,pres0 
  real(rp)                   :: maxvar,tolconv
  integer(4)                 :: istat
  integer(ip)      ::  nedge,nrkord,npoin,nvar,ndim,nelem,nelem0,nnode,nsid,nnofa,nface,nedgb,nboup,mboup,nflu,niter,iter,piter
  integer(ip)      :: nsurf,ipoin,ierr,irenum,ivisc,idual,ifirst,isolv
  integer(ip)      :: nlay
  integer(ip),pointer  :: lmark(:)

  c10=1.0d+00
  c20=2.0d+00
  !
  !     Mesh variables
  !
  ndim=3
  nnofa=3
  nnode=4
  nsid=6
  nvar=7
  nflu=5
  !
  !     Read parameters
  !
  call readparam(igrav,ipre,ivisc,idual,idtloc,isolv,ifirst,nrkord,irenum,cfl,vn,fprint,dtaumax,grav,R,gam)
  !
  !     Physical parameters
  !
  !gam=1.40027894d+00
  !R=8.134d+03
  !R=287.0d+00
  cv=R/(gam-c10)
  !cv=71428.57d+00
  cvi=c10/cv
  cp=gam*cv
  cpi=c10/cp
  !grav=9.81d+00
  !grav=0.0d+00
  scons=110.0d+00 
  temp0=1.0d+00
  Pr=0.7d+00
  Pri=c10/Pr
  mu0=2.5d-03
  !
  !     Do we have gravity?
  !
  !igrav=1_ip
  !igrav=0_ip
  !
  !     Do we want low Mach?
  !
  !ipre=1_ip 
  !ipre=0_ip
  !
  !     Do we want viscous fluxes?
  ! 
  !ivisc=1_ip
  !ivisc=0_ip
  !
  !    Do we want a dual time stepping?
  !
  !idual=1_ip
  !idual=0_ip
  !
  !    Do we want a local time stepping?
  !
  !idtloc=1_ip
  !idtloc=0_ip
  !
  !     Which solver do we want?
  ! 
  !
  !  iopt = 1   --> JST  flux
  !  iopt = 2   --> HLLC flux
  !  iopt = 3   --> ROE flux
  !
  !isolv=2_ip
  !
  !     For Riemann solvers, which order do we want?
  !
  !ifirst=0_ip
  !
  !     Numerical parameter
  !
  !cfl=0.5d+00
  !vn=0.6d+00
  !nrkord=4
  tolconv=1.0d-06

  !fprint=1.0d+00
  !dtaumax=1.0d+00
  dt=1.0d+00
  maxtime=1200.0d+00
  !
  !     Do we want to renumber?
  !  
  !irenum=0_ip 
  !irenum=1_ip
  !call readwrite(nvar,npoin)
  !call readwrite2()
  !
  !     Read the geometry
  !
  call readgeo(nelem,npoin,nnode,ndim,nface,nnofa,nsurf,irenum)
  !
  !     Allocate point database
  !
  allocate(var(nvar,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'VAR','comp',var)
  allocate(varn(nvar,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'VARN','comp',varn)
  allocate(varnn(nvar,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'VARNN','comp',varnn)
  allocate(varpn(nvar,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'VARPN','comp',varpn)
  allocate(dvar(nflu,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'DVAR','comp',dvar)
  allocate(rmass(npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'RMASS','comp',rmass)
  allocate(viscp(2,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'VISCP','comp',viscp)
  allocate(rdtloc(npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'RDTLOC','comp',rdtloc)
  !
  !     Allocate face database
  !
  allocate(rnofa(ndim,nface),stat=istat)
  call memchk(zero,istat,memor_edg,'RNOFA','comp',rnofa)
  !
  !     Get the edges, boundary conditions and the edge database
  !
  call edgfil(elem,nelem,npoin,nnode,nedge,nsid,ndim,coor,rmass,nnofa,nface,nedgb,nboup,lsurf,nsurf,lbous,lface,rnofa,irenum,mboup)

  !call writeAdrien(nelem,elem,nnode,npoin,coor,ndim,nface,lface,nnofa,lsurf)
  !stop
  !
  !     Allocate edge database
  !
  allocate(grad(ndim,6,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'GRAD','comp',grad)
  allocate(flu(nflu,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'FLU','comp',flu)
  allocate(extr1(nvar,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'EXTR1','comp',extr1)
  allocate(extr2(nvar,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'EXTR2','comp',extr2)
  allocate(sens(nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'SENS','comp',sens)
  allocate(hydro1(2,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'HYDRO1','comp',hydro1)
  allocate(hydro2(2,nedge),stat=istat)
  call memchk(zero,istat,memor_edg,'HYDRO2','comp',hydro2)
  allocate(varb(nvar,nboup),stat=istat)
  call memchk(zero,istat,memor_edg,'VARB','comp',varb)
  !
  !     If gravity, initialize hydrostatic equilibrium
  !
  if(igrav==1)then
     call hydreq(nedge,ledge,ndim,hydro1,hydro2,grad,edsha,npoin,ledgb,nedgb,edshab,edshap,nboup,lboup,rmass,grav,cpi,cp,R,gam,coor,ierr,mboup,cv)
  endif
  !
  !     Initialize the variables
  !
  call initvar(ndim,npoin,coor,var,nvar,gam,cpi,cvi,grav,R,cv,cp,lsurf,lface,nface,nnofa,lboup,nboup,mboup,rdir,varb,lbous,nsurf)
  !
  !     Check initialization (Experience teaches us...)
  !
  call chkini(npoin,nvar,var,gam)
  !
  !     Refine the mesh
  !  
  !  allocate(lmark(nelem),stat=istat)
  !  call memchk(zero,istat,memor_edg,'LMARK','comp',lmark)
  !  nlay=0
  !  pres0=100000.0d+00
  !  iter=0

  !  do 

  !  nelem0=nelem
  !  lmark=>memrea(nelem,memor_edg,'lmark','comp',lmark)
  !  call adapt(nnode,nnofa,nsid,nelem,npoin,nface,lmark,nlay,cpi,R,cvi,pres0,elem,ndim) 

  !  iter=iter+1
  !  if(iter==28)exit

  !  if(nelem==nelem0)exit

  !  enddo
  !
  !     Initialize viscosity and conductivity for NS
  !
  if(ivisc==1)then
     call suther(cvi,var,nvar,npoin,temp0,scons,viscp,cv,Pri,mu0)
  endif
  !
  !     Initialize time step, iteration and initial output
  !
  niter=1_ip
  time=0.0d+00 
  call outcomp(nnode,nelem,elem,ndim,npoin,coor,var,nvar,cvi,niter,time,R,cpi,rdir,lboup,nboup)
  !
  !     1D DBG
  !

  !call dbg1d(nedge,ledge,npoin,nedgb,nboup,gam,edsha,var,nvar,coor,ndim)!
  !   Copy to varn
  !

  !varn=var

  do ipoin=1,npoin
     varn(1,ipoin)=var(1,ipoin)
     varn(2,ipoin)=var(2,ipoin)
     varn(3,ipoin)=var(3,ipoin)
     varn(4,ipoin)=var(4,ipoin)
     varn(5,ipoin)=var(5,ipoin)
     varn(6,ipoin)=var(6,ipoin)
     varn(7,ipoin)=var(7,ipoin)
  enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  !     Loop on the physical time step
  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  time=0.0d+00 
  tprint=0.0d+00
  !  niter=1
  do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
     !
     !     Loop on the pseudo time step
     !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

     ptime=0.0d+00
     piter=0_ip 

     !  varpn=varn
     do ipoin=1,npoin
        varpn(1,ipoin)=varn(1,ipoin)
        varpn(2,ipoin)=varn(2,ipoin)
        varpn(3,ipoin)=varn(3,ipoin)
        varpn(4,ipoin)=varn(4,ipoin)
        varpn(5,ipoin)=varn(5,ipoin)
        varpn(6,ipoin)=varn(6,ipoin)
        varpn(7,ipoin)=varn(7,ipoin)
     enddo

     do 
        !
        !     Compute the pseudo time step
        !
        if(ipre==0)then 

           call getdt(nedge,ledge,edsha,gam,var,nvar,npoin,dtau,coor,ndim,cfl,niter,vn,viscp,rmass,ivisc,rdtloc,idtloc)

        else

           call getdtpre(nedge,ledge,edsha,gam,var,nvar,npoin,dtau,coor,ndim,cfl,niter,vn,viscp,rmass,ivisc,rdtloc,idtloc)

        endif
        !
        !     Do we have a too large time step due to zero velocities 
        !     at the begining
        !
        if(dtau>dtaumax)then
           write(*,*)'Correct a too large dt ',dtau,dtaumax
           dtau=dtaumax
        endif

        if(idual==1)then
           if(idtloc==0)then 
              write(*,*)'Pseudo Time=',ptime,'dtau=',dtau
           else
              write(*,*)'Running with pseudo local time step, piter=',piter
           endif
        else 
           if(idtloc==0)then
              write(*,*)'Time=',time,'dt=',dtau
           else
              write(*,*)'Running with local time step, niter=',niter
           endif
        endif
        !
        !     Advance in pseudo time with the Riemann solver
        !
        call rkcomp(nrkord,nedge,ledge,edsha,var,nvar,dvar,npoin,flu,gam,grad,dtau,ndim,rmass,varpn,extr1,extr2,coor,ledgb,nedgb,edshab,edshap,nboup,mboup,lboup,rdir,nflu,cvi,sens,lbous,rbous,nsurf,grav,igrav,ipre,cp,niter,ierr,viscp,ivisc,temp0,Pri,cv,scons,mu0,varnn,idual,dt,varn,ifirst,isolv,cpi,R,hydro1,hydro2,varb,rdtloc)
        !
        !     Some error
        !
        if(ierr==1)then
           !
           !     Divides the Courant by 2
           ! 
           write(*,*)'Error encountered, divide CFL by 2'
           cfl=cfl/c20
           !
           !     Go back
           !
           !var=varpn
           do ipoin=1,npoin
              var(1,ipoin)=varpn(1,ipoin)
              var(2,ipoin)=varpn(2,ipoin)
              var(3,ipoin)=varpn(3,ipoin)
              var(4,ipoin)=varpn(4,ipoin)
              var(5,ipoin)=varpn(5,ipoin)
              var(6,ipoin)=varpn(6,ipoin)
              var(7,ipoin)=varpn(7,ipoin)
           enddo
           cycle

        endif

        ptime = ptime+dtau
        piter = piter+1_ip
        !
        !     If we are marching without dual time stepping, go to the next physical iteration
        !

        if(idual==0)then
           dt=dtau
           exit
        else
           !
           !     Update the unknowns
           !
           do ipoin=1,npoin
              varpn(1,ipoin)=var(1,ipoin)
              varpn(2,ipoin)=var(2,ipoin)
              varpn(3,ipoin)=var(3,ipoin)
              varpn(4,ipoin)=var(4,ipoin)
              varpn(5,ipoin)=var(5,ipoin)
              varpn(6,ipoin)=var(6,ipoin)
              varpn(7,ipoin)=var(7,ipoin)
           enddo
           !varpn=var
           !
           !     Compute residual of the pseudo time stepping
           !
           call residedg(npoin,dvar,var,nvar,maxvar,nflu,time,niter)
           write(*,*)'After pseudo residual maxvar=',maxvar
           !
           !     Did we converged?
           !
           if(maxvar<tolconv)exit
        endif

     enddo
     !
     !     Transfer information from one time layer to the other
     !
     time  = time+dt
     !
     !     Compute residual of the physical time stepping
     !
     call residedg(npoin,dvar,var,nvar,maxvar,nflu,time,niter)
     write(*,*)'After residual maxvar=',maxvar
     niter = niter+1_ip
     !
     !     Do we want to output the solution?
     !
     if(time>tprint)then
        tprint=tprint+fprint
        call outcomp(nnode,nelem,elem,ndim,npoin,coor,var,nvar,cvi,niter,time,R,cpi,rdir,lboup,nboup)
        ! 1D DBG 
        !call outdbg1d(npoin,var,nvar,coor,ndim)
     endif
     !
     !     Did we converged?
     !
     if(maxvar<tolconv)exit
     !
     !     Did we reach the end? 
     !
     if(time>maxtime)exit
     !
     !    Update the unknowns
     !
     if(niter>5)then
        !varnn=varn
        do ipoin=1,npoin
           varnn(1,ipoin)=varn(1,ipoin)
           varnn(2,ipoin)=varn(2,ipoin)
           varnn(3,ipoin)=varn(3,ipoin)
           varnn(4,ipoin)=varn(4,ipoin)
           varnn(5,ipoin)=varn(5,ipoin)
           varnn(6,ipoin)=varn(6,ipoin)
           varnn(7,ipoin)=varn(7,ipoin)
        enddo
     endif
     ! varn=var
     do ipoin=1,npoin
        varn(1,ipoin)=var(1,ipoin)
        varn(2,ipoin)=var(2,ipoin)
        varn(3,ipoin)=var(3,ipoin)
        varn(4,ipoin)=var(4,ipoin)
        varn(5,ipoin)=var(5,ipoin)
        varn(6,ipoin)=var(6,ipoin)
        varn(7,ipoin)=var(7,ipoin)
     enddo

  enddo

end subroutine comp


subroutine initvar(ndim,npoin,coor,var,nvar,gam,cpi,cvi,grav,R,cv,cp,lsurf,lface,nface,nnofa,lboup,nboup,mboup,rdir,varb,lbous,nsurf)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)         :: npoin,ndim,nvar,nface,nnofa,nboup,mboup,nsurf
  integer(ip),intent(in)         :: lface(nnofa,nface),lsurf(nface),lboup(6,mboup),lbous(2,nsurf)
  real(rp), intent(in)           :: coor(ndim,npoin),gam,cpi,cvi,grav,R,cv,cp,rdir(ndim,nboup)
  real(rp), intent(inout)        :: var(nvar,npoin),varb(nvar,nboup)
  real(rp)                       :: z,temp,pres,rho,rhoi,rhoe,c10,c00
  real(rp)                       :: temp0,pres0,pi,xr,zr,L,dx,dz,presex
  real(rp)                       :: xc,zc,x,dT,rho0,c05,freq,freq2,u,v,w,u0,csca,rnx,rny,rnz
  integer(ip)                    :: ipoin,isurf,iface,ipo,i,iboup,kboup,ifound,nsurfl,icond

  c10=1.0d+00
  c05=0.5d+00
  c00=0.0d+00

  !
  !     Sod tube test case
  !

  goto 3000


  !$omp parallel do &
  !$omp& schedule(static) &
  !$omp& default(none) &
  !$omp& shared(npoin,coor,var,gam) &
  !$omp& private(ipoin)  

  do ipoin=1,npoin

     if(coor(1,ipoin)<5.0d+00)then

        var(1,ipoin)=1.0d+00
        var(2,ipoin)=0.0d+00
        var(3,ipoin)=0.0d+00
        var(4,ipoin)=0.0d+00
        var(5,ipoin)=1.0d+00/(gam-1.0d+00)
        var(6,ipoin)=1.0d+00
        var(7,ipoin)=1.0d+00

     else  

        var(1,ipoin)=1.00d-01
        var(2,ipoin)=0.0d+00
        var(3,ipoin)=0.0d+00
        var(4,ipoin)=0.0d+00
        var(5,ipoin)=1.0d-01/(gam-1.0d+00)
        var(6,ipoin)=1.0d-01
        var(7,ipoin)=1.0d-01/1.25d-01

     endif

  enddo

  return

1000 continue

  !
  !     Straka test case
  !

  temp0=300.0d+00
  pres0=100000.0d+00

  !$omp parallel do &
  !$omp& schedule(static) &
  !$omp& default(none) &
  !$omp& shared(npoin,coor,var,gam,temp0,pres0,cpi,grav,c10,R,cp) &
  !$omp& private(ipoin,z,temp,pres,rho,rhoi,rhoe)  
  do ipoin=1,npoin
     z=coor(3,ipoin)
     temp=temp0-grav*z*cpi
     pres=pres0*(temp/temp0)**(cp/R)
     rho=pres/(R*temp)
     rhoi=c10/rho
     rhoe=pres/(gam-c10)

     var(1,ipoin)=rho
     var(2,ipoin)=0.0d+00
     var(3,ipoin)=0.0d+00
     var(4,ipoin)=0.0d+00
     var(5,ipoin)=rhoe
     var(6,ipoin)=pres
     var(7,ipoin)=rhoi


  enddo

  !
  !     Perturbation
  !
  xc=0.0d+00
  xr=4000.0d+00
  zc=3000.0d+00
  zr=2000.0d+00
  pi=3.141592d+00

  !$omp parallel do &
  !$omp& schedule(static) &
  !$omp& default(none) &
  !$omp& shared(npoin,coor,var,xc,xr,zc,zr,pi,c10,cv,cvi,rhoe,rho,rhoi,pres,gam,R) &
  !$omp& private(ipoin,x,z,dx,dz,L,dT,temp)  
  do ipoin=1,npoin
     x=coor(1,ipoin)
     z=coor(3,ipoin)

     dx=(x-xc)/xr
     dz=(z-zc)/zr

     L=sqrt(dx*dx+dz*dz)

     if(L<c10)then

        dT=-7.5d+00*(cos(pi*L)+c10)
        temp=var(5,ipoin)/var(1,ipoin)*cvi
        temp=temp+dT
        !var(5,ipoin)=var(1,ipoin)*cv*temp

        !pres=pres0*(temp/temp0)**(cp/R)
        pres=var(6,ipoin)
        rho=pres/(R*temp)
        rhoi=c10/rho
        rhoe=pres/(gam-c10)
        var(1,ipoin)=rho
        var(5,ipoin)=rhoe
        !var(6,ipoin)=pres
        var(7,ipoin)=rhoi



     endif

  enddo

  return      


  !
  !     Cavity benchmarks
  !

2000 continue

  !
  !     Initialize density, velocity and temperature
  !  
  temp0=1.0d+00
  rho0=1.0d+00
  do ipoin=1,npoin
     var(1,ipoin)=rho0 
     var(2,ipoin)=c00
     var(3,ipoin)=c00
     var(4,ipoin)=c00
     var(5,ipoin)=cv*temp0*rho0
     var(6,ipoin)=(gam-c10)*var(5,ipoin)
     var(7,ipoin)=c10/rho0
  enddo

  do iface=1,nface
     isurf=lsurf(iface)
     if(isurf==5)then
        do ipo=1,nnofa
           ipoin=lface(ipo,iface) 
           var(2,ipoin)=c10*rho0 
           var(5,ipoin)=rho0*(cv*temp0+c05*c10*c10)      
           var(6,ipoin)=(gam-c10)*var(5,ipoin)
        enddo
     endif
  enddo

  !
  !     Schar mountain test case
  !
3000 continue
  !
  !     Brunt-Vaisala frequency
  !
  freq=0.01d+00 
  freq2=freq*freq 
  temp0=280.0d+00
  pres0=100000.0d+00

  !$omp parallel do &
  !$omp& schedule(static) &
  !$omp& default(none) &
  !$omp& shared(npoin,coor,var,gam,temp0,pres0,cpi,grav,c10,R,cp) &
  !$omp& private(ipoin,z,temp,pres,rho,rhoi,rhoe)  
  do ipoin=1,npoin
     z=coor(3,ipoin)
     !Potential temperature
     temp=temp0*exp(freq2*z/grav)
     !Exner pressure
     presex=1+((grav*grav)/(cp*temp0*freq2))*(exp(-freq2*z/grav)-c10)
     !Density
     rho=(pres0/(R*temp))*presex**(cv/R)
     rhoi=c10/rho
     !Pressure
     pres=pres0*(rho*R*temp/pres0)**(cp/cv)
     !Velocity 
     u=10.0d+00
     !Energy 
     rhoe=rho*cv*temp*presex+c05*rho*u*u

     var(1,ipoin)=rho
     var(2,ipoin)=rho*u
     var(3,ipoin)=0.0d+00
     var(4,ipoin)=0.0d+00
     var(5,ipoin)=rhoe
     var(6,ipoin)=pres
     var(7,ipoin)=rhoi


  enddo
  !
  !     Correct boundaries
  !

  do iboup=1,nboup
     isurf=lboup(3,iboup)
     icond=lboup(2,iboup) 
     ipoin=lboup(1,iboup)
     ifound=0_ip         
     if(icond==4 .and. isurf==0)then
        rnx=rdir(1,iboup) 
        rny=rdir(2,iboup) 
        rnz=rdir(3,iboup) 

        rho=var(1,ipoin)
        rhoi=c10/rho
        csca=rnx*10.0d+00

        u=10.0d+00-csca*rnx 
        v=-csca*rny 
        w=-csca*rnz
        pres=var(6,ipoin)
        rhoe=pres/(gam-c10)+c05*rho*(u*u+v*v+w*w)

        var(2,ipoin)=rho*u
        var(3,ipoin)=rho*v
        var(4,ipoin)=rho*w
        var(5,ipoin)=rhoe

     else if(icond==3 .and. isurf==0)then

        rnx=rdir(1,iboup) 
        rny=rdir(2,iboup) 
        rnz=rdir(3,iboup) 

        rho=var(1,ipoin)
        rhoi=c10/rho
        csca=rnx*10.0d+00

        u=csca*rnx 
        v=csca*rny 
        w=csca*rnz
        pres=var(6,ipoin)
        rhoe=pres/(gam-c10)+c05*rho*(u*u+v*v+w*w)

        var(2,ipoin)=rho*u
        var(3,ipoin)=rho*v
        var(4,ipoin)=rho*w
        var(5,ipoin)=rhoe

     endif

  enddo

  goto 5000


4000 continue
  !
  !     NACA 
  !
  do ipoin=1,npoin
     var(1,ipoin)=1.4d+00
     var(2,ipoin)=var(1,ipoin)*0.8d+00
     var(3,ipoin)=0.0d+00
     var(4,ipoin)=0.0d+00
     var(5,ipoin)=1.0d+00/(gam-c10)+c05*1.4d+00*0.8d+00*0.8d+00
     var(6,ipoin)=1.0d+00
     var(7,ipoin)=c10/var(1,ipoin)
  enddo

  !
  !     Correct boundaries
  !

  do iboup=1,nboup
     isurf=lboup(3,iboup)
     icond=lboup(2,iboup) 
     ipoin=lboup(1,iboup)
     ifound=0_ip         
     if(icond==4 .and. isurf==0)then
        rnx=rdir(1,iboup) 
        rny=rdir(2,iboup) 
        rnz=rdir(3,iboup) 

        rho=var(1,ipoin)
        rhoi=c10/rho
        csca=rnx*0.8d+00

        u=0.8d+00-csca*rnx 
        v=-csca*rny 
        w=-csca*rnz
        pres=1.0d+00
        rhoe=c10/(gam-c10)+c05*rho*(u*u+v*v+w*w)

        var(2,ipoin)=rho*u
        var(3,ipoin)=rho*v
        var(4,ipoin)=rho*w
        var(5,ipoin)=rhoe

     else if(icond==3 .and. isurf==0)then

        rnx=rdir(1,iboup) 
        rny=rdir(2,iboup) 
        rnz=rdir(3,iboup) 

        rho=var(1,ipoin)
        rhoi=c10/rho
        csca=rnx*0.8d+00

        u=csca*rnx 
        v=csca*rny 
        w=csca*rnz
        pres=1.0d+00
        rhoe=c10/(gam-c10)+c05*rho*(u*u+v*v+w*w)

        var(2,ipoin)=rho*u
        var(3,ipoin)=rho*v
        var(4,ipoin)=rho*w
        var(5,ipoin)=rhoe

     endif

  enddo

5000 continue


  !
  !     Copy the boundary states as characteristic states?
  !

     do iboup=1,nboup
        ipoin=lboup(1,iboup)
        varb(1,iboup)=var(1,ipoin)
        varb(2,iboup)=var(2,ipoin)
        varb(3,iboup)=var(3,ipoin)
        varb(4,iboup)=var(4,ipoin)
        varb(5,iboup)=var(5,ipoin)
     enddo








end subroutine initvar


subroutine getdt(nedge,ledge,edsha,gam,var,nvar,npoin,dt,coor,ndim,cfl,niter,vn,viscp,rmass,ivisc,rdtloc,idtloc)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nedge,npoin,nvar,ndim,niter,ivisc
  integer(ip), intent(in)    :: ledge(2,nedge)
  integer(ip), intent(in)    :: idtloc
  real(rp), intent(in)       :: edsha(5,nedge),gam,var(nvar,npoin),coor(ndim,npoin),cfl,vn,viscp(2,npoin),rmass(npoin)
  real(rp), intent(inout)    :: dt,rdtloc(npoin)
  real(rp)     :: rnl,coef1,coef2,coef3,rho1,rhou1,rhov1,rhow1,rhoe1,rho2
  real(rp)     :: rhou2,rhov2,rhow2,rhoe2,pres1,pres2,rhoi1,rhoi2,rl
  real(rp)     :: lambda,rho,rhou,rhov,rhow,rhoi,u,v,w,pres,c,vedg,lambdamax
  real(rp)     :: c05,c10,rx,ry,rz,length,lenm,c00,dtloc,mu,mu1,mu2,dtvisc,rmas1,rmas2,rmas,rmax
  integer(ip)  :: iedge,ip1,ip2,i,j,iedgmax,iedgemin,ipoin

  c05=0.5d+00
  c10=1.0d+00
  c00=0.0d+00
  dt=1.0d+12
  lambdamax=c00

  rdtloc=dt

  !$omp parallel do &
  !$omp& schedule(static) &
  !$omp& default(none) &
  !$omp& shared(nedge,ledge,edsha,coor,var,c10,c05,gam,ivisc,rmass,viscp,vn,cfl,rdtloc) &
  !$omp& private(iedge,ip1,ip2,coef1,coef2,coef3,rl,rx,ry,rz,length,&
  !$omp&         rho1,rhou1,rhov1,rhow1,pres1,rho2,rhou2,rhov2,rhow2,pres2,&
  !$omp&         rho,rhoi,rhou,rhov,rhow,pres,u,v,w,vedg,c,dtloc,lambda,dtvisc,&
  !$omp&         mu1,mu2,mu,rmas1,rmas2,rmas)

  do iedge=1,nedge

     !
     !     Points of the edge
     !
     ip1=ledge(1,iedge)
     ip2=ledge(2,iedge)
     !
     !     Geometric coefficients
     !
     coef1=edsha(2,iedge)
     coef2=edsha(3,iedge)
     coef3=edsha(4,iedge)
     rl=sqrt(coef1*coef1+coef2*coef2+coef3*coef3)
     rl=c10/rl
     !
     !     Edge length
     !
     rx=coor(1,ip1)-coor(1,ip2)
     ry=coor(2,ip1)-coor(2,ip2)
     rz=coor(3,ip1)-coor(3,ip2)
     length=sqrt(rx*rx+ry*ry+rz*rz)
     !
     !     Get relevant quantities
     !
     rho1=var(1,ip1)
     rhou1=var(2,ip1)
     rhov1=var(3,ip1)
     rhow1=var(4,ip1)
     pres1=var(6,ip1)
     mu1=viscp(1,ip1) 
     rmas1=rmass(ip1)

     rho2=var(1,ip2)
     rhou2=var(2,ip2)
     rhov2=var(3,ip2)
     rhow2=var(4,ip2)
     pres2=var(6,ip2)
     mu2=viscp(1,ip2) 
     rmas2=rmass(ip2)

     rho=c05*(rho1+rho2)
     rhoi=c10/rho
     rhou=c05*(rhou1+rhou2)
     rhov=c05*(rhov1+rhov2)
     rhow=c05*(rhow1+rhow2)
     pres=c05*(pres1+pres2)
     mu=c05*(mu1+mu2)  
     rmas=c05*(rmas1+rmas2)

     u=rhou*rhoi
     v=rhov*rhoi
     w=rhow*rhoi
     !
     !     Velocity in the edge direction
     !
     vedg=abs(coef1*u+coef2*v+coef3*w)*rl
     !
     !     Speed of sound
     ! 
     c=sqrt(gam*pres*rhoi)
     !
     !     Largest eigenvalue 
     !
     lambda=vedg+c
     dtloc=cfl*length/lambda

     if(ivisc==1)then
        !
        !     Viscous dt
        !
        dtvisc=vn*rho*rmas/(abs(edsha(5,iedge))*mu)
        dtloc=min(dtloc,dtvisc)
     endif

     rdtloc(ip1)=min(dtloc,rdtloc(ip1))
     rdtloc(ip2)=min(dtloc,rdtloc(ip2))

  enddo

  !
  !     Compute time step
  !

  !dt=cfl*lenm/lambdamax

  if(niter<5)then
     do ipoin=1,npoin
        rdtloc(ipoin)=0.2d+00*rdtloc(ipoin)
     enddo
  endif       
 
  !
  !     Take the min
  !
  dt=rdtloc(1)
  do ipoin=2,npoin
     dt=min(dt,rdtloc(ipoin))
  enddo
  !
  !     Do we want a local or a global time stepping?
  !
  if(idtloc==0)then
     do ipoin=1,npoin
        rdtloc(ipoin)=dt
     enddo
  else
     !
     !     Check the ratio dtmax/dtmin
     ! 
     rmax=c10
     do ipoin=1,npoin
        rmax=max(rmax,rdtloc(ipoin)/dt)
     enddo
     write(*,*)'Time ratio is:',rmax

  endif



end subroutine getdt


subroutine readgeo(nelem,npoin,nnode,ndim,nface,nnofa,nsurf,irenum)
  use def_kintyp, only       :  ip,rp,lg
  use mod_memchk
  use mod_mshtol
  use def_meshin, only       : memor_msh,elem,coor,lface,lsurf,ptoel1,ptoel2
  use def_nasedg, only       : memor_edg,lbous,rbous
  implicit none
  integer(ip), intent(inout)   :: nelem,npoin,nface,nsurf
  integer(ip), intent(in)      :: nnode,ndim,nnofa,irenum
  integer(ip)                  :: ielem,jelem,ipoin,i
  integer(ip)                  :: iface,jface,isurf,imat,icond,ichk
  integer(4)                   :: istat
  character*80                :: ntext

  ! open(unit=77,file='sod2.dat',status='old')
  ! open(unit=77,file='test.dat',status='old')
  !open(unit=77,file='sod3.dat',status='old')
  !open(unit=77,file='straka.dat',status='old')
  !open(unit=77,file='straka_fine.dat',status='old')
  !open(unit=77,file='unsort.bsc',status='old')
  !open(unit=77,file='cavity.dat',status='old')
  open(unit=77,file='schar.dat',status='old')
  !open(unit=77,file='naca.dat',status='old')

  !
  !     Read points
  !

  read(77,*)ntext,npoin
  allocate(coor(ndim,npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'COOR','readgeo',coor)

  do i=1,npoin
     read(77,*) ipoin,coor(1,i),coor(2,i),coor(3,i)
     !read(77,*) coor(1,i),coor(2,i),coor(3,i)
  end do

  !
  !     Read elements
  !

  read(77,*)ntext,nelem
  allocate(elem(nnode,nelem),stat=istat)
  call memchk(zero,istat,memor_msh,'ELEM','readgeo',elem) 
  do ielem=1,nelem
     read(77,*) jelem,elem(1,ielem),elem(2,ielem),elem(3,ielem),elem(4,ielem),imat
  enddo

  !
  !    Read faces
  !

  read(77,*)ntext,nface
  allocate(lface(nnofa,nface),stat=istat)
  call memchk(zero,istat,memor_msh,'LFACE','readgeo',lface)
  allocate(lsurf(nface),stat=istat)
  call memchk(zero,istat,memor_msh,'LSURF','readgeo',lsurf)
  do iface=1,nface
     read(77,*) jface,lface(1,iface),lface(2,iface),lface(3,iface),lsurf(iface)
     !read(77,*) jface,lface(1,iface),lface(2,iface),lface(3,iface)
     !lsurf(iface)=1
  enddo

  close(77)


  !
  !     Read Boundary condition types
  !
  !     0: everything free
  !     1: everything imposed
  !     2: velocity imposed
  !     3: tangent free        
  !     4: normal velocity imposed 
  !  
  !     -1: inflow characteristic, everything imposed
  !     -2: inflow characteristic, total pressure imposed
  !     -3: outflow characteristic, everything imposed
  !     -4: outflow characteristic, pressure imposed
  !     -5: outflow characteristic, mach number imposed imposed
  !     -6: outflow characteristic, mass flux imposed
  ! 
  !
  !open(unit=66,file='sod.cnd',status='old')
  !open(unit=66,file='cavity.cnd',status='old')
  open(unit=66,file='schar.cnd',status='old')
  !open(unit=66,file='naca.cnd',status='old')

  read(66,*)ntext,nsurf
  allocate(lbous(2,nsurf),stat=istat)
  call memchk(zero,istat,memor_edg,'LBOUS','readgeo',lbous)
  allocate(rbous(5,nsurf),stat=istat)
  call memchk(zero,istat,memor_edg,'RBOUS','readgeo',rbous)

  do isurf=1,nsurf
     read(66,*)lbous(1,isurf)
     !
     !     Do we have a characteristic surface
     ! 
     if(lbous(1,isurf)<0)then

        read(66,*)lbous(2,isurf)
        !
        !     Do we need to read the characteristic state?
        !
        if(lbous(2,isurf)==0)cycle
        !
        !     What do we want to impose?   
        !
        icond=-lbous(1,isurf)        
        !
        !     Everything imposed or total pressure imposed.
        !     Read everything
        !
        if(icond==1 .or. icond==2 .or. icond==3)then

           read(66,*)rbous(1,isurf)
           read(66,*)rbous(2,isurf)
           read(66,*)rbous(3,isurf)
           read(66,*)rbous(4,isurf)
           read(66,*)rbous(5,isurf)
           !
           !     Outflow, back pressure or Mach imposed
           !
        else if(icond==4 .or. icond==5)then   

           read(66,*)rbous(1,isurf)

        else if(icond==6)then
           !
           !     Outflow, mass flux imposed
           !
           read(66,*)rbous(1,isurf)
           read(66,*)rbous(2,isurf)
           read(66,*)rbous(3,isurf)
           read(66,*)rbous(4,isurf)

        endif

     endif

  enddo

  close(66)

  if(irenum==1)then
     !
     !     Get elements surrouding points
     !
     call ptoelm(elem,nelem,npoin,nnode,ptoel1,ptoel2)
     !
     !     Renumber points
     !
     call renupo(npoin,ndim,coor,ptoel1,ptoel2,elem,nnode,nelem,lface,nface,nnofa)
     !
     !     Renumber points in elements
     !
     call renuinel(elem,nnode,nelem)
     !
     !     Renumber the elements
     !
     !call renuel(elem,nnode,nelem,npoin)
     !
     !     Renumber points in faces
     !
     call renuinfa(lface,nnofa,nface)
     !
     !     Renumber the faces
     !
     call renufa(lface,nnofa,nface,npoin,lsurf)

  endif


end subroutine readgeo


subroutine outcomp(nnode,nelem,elem,ndim,npoin,coor,var,nvar,cvi,niter,time,R,cpi,rdir,lboup,nboup)
  use def_kintyp, only       :  ip,rp,lg
  implicit none
  integer(ip),intent(in)      :: nnode,nelem,npoin,ndim,nvar,niter,nboup
  integer(ip),intent(in)      :: elem(nnode,nelem)
  integer(ip),intent(in)      :: lboup(6,nboup) 
  real(rp), intent(in)        :: coor(ndim,npoin),var(nvar,npoin),cvi,time,R,cpi,rdir(ndim,nboup)
  integer(ip)                 :: ielem,ipoin,iboup       
  real(rp)                    :: rx,ry,rz,rhoi,c10,u1,v1,w1,e1,v2,temp,c05 
  real(rp)                    :: temppot,prespot,pres0 
  character*80                :: name
  c10=1.0d+00
  c05=0.5d+00

  !
  !     Print mesh
  !

  if(niter==1)then

     open(unit=50,file='outcomp.msh',status='unknown')
     rewind 50

1    format('MESH dimension 3 ElemType Tetrahedra Nnode 4')
2    format('Coordinates')
3    format('#node number   coord_x   coord_y  coord_z')
100  format(i10,3e20.10)
200  format(5i10)
4    format('end coordinates')
5    format('Elements')
6    format('end elements')
     write(50,1)
     write(50,2)
     write(50,3)
     do  ipoin=1,npoin
        rx=coor(1,ipoin)
        ry=coor(2,ipoin)
        rz=coor(3,ipoin)
        write(50,100)ipoin,rx,ry,rz
     enddo

     write(50,4)
     write(50,5)
     do  ielem=1,nelem
        write(50,200)ielem,elem(1,ielem),elem(2,ielem),elem(3,ielem),elem(4,ielem)
     enddo
     write(50,6)
     close(50)


  endif


  !    write(name,21)niter
  !21   format('outcomp',i4,'.res') 

  if(niter==1)then
     open(unit=60,file='outcomp.res',status='unknown')
     rewind 60
     write(60,10)
  endif

10 format('GID Post Results File 1.0')
11 format('Result "Velocity" "Analysis/time" ',1e20.10,' Vector OnNodes')
12 format('ComponentNames "Vx", "Vy" , "Vz" ')
13 format('Values')
14 format('End Values')
15 format('   ')
16 format('Result "Total energy" "Analysis/time" ',1e20.10,' Scalar OnNodes')
17 format('Result "Density" "Analysis/time" ',1e20.10,' Scalar OnNodes')
18 format('Result "Pressure" "Analysis/time" ',1e20.10,' Scalar OnNodes')
19 format('Result "Temperature" "Analysis/time" ',1e20.10,' Scalar OnNodes')
20 format('Result "Potential Temperature" "Analysis/time" ',1e20.10,' Scalar OnNodes')
21 format('Result "Normal" "Analysis/time" ',1e20.10,' Vector OnNodes')
300 format(i10,1e20.10)


  write(60,17)time
  write(60,13)
  do  ipoin=1,npoin
     write(60,300)ipoin,var(1,ipoin)
  enddo

  write(60,14)

  write(60,11)time
  write(60,12)
  write(60,13)
  do  ipoin=1,npoin
     rhoi=c10/var(1,ipoin)
     write(60,100)ipoin,var(2,ipoin)*rhoi,var(3,ipoin)*rhoi,var(4,ipoin)*rhoi
  enddo
  write(60,14)

  write(60,16)time
  write(60,13)
  do  ipoin=1,npoin
     write(60,300)ipoin,var(5,ipoin)
  enddo
  write(60,14)


  write(60,18)time
  write(60,13)
  do  ipoin=1,npoin
     write(60,300)ipoin,var(6,ipoin)
  enddo
  write(60,14)

  call conpri(npoin,var,nvar)

  write(60,19)time
  write(60,13)
  do  ipoin=1,npoin
     u1=var(2,ipoin)
     v1=var(3,ipoin)
     w1=var(4,ipoin)
     e1=var(5,ipoin)
     v2=c05*(u1*u1+v1*v1+w1*w1)
     temp=cvi*(e1-v2)

     write(60,300)ipoin,temp
  enddo
  write(60,14)

  pres0=100000.0d+00

  write(60,20)time
  write(60,13)
  do  ipoin=1,npoin
     u1=var(2,ipoin)
     v1=var(3,ipoin)
     w1=var(4,ipoin)
     e1=var(5,ipoin)
     v2=c05*(u1*u1+v1*v1+w1*w1)
     temp=cvi*(e1-v2)
     prespot=(var(6,ipoin)/pres0)**(R*cpi)
     temppot=temp/prespot
     write(60,300)ipoin,temppot
  enddo
  write(60,14)






  call pricon(npoin,var,nvar)



  write(60,15)

  !write(60,21)time
  !write(60,12)
  !write(60,13)
  !do  iboup=1,nboup
  !   ipoin=lboup(1,iboup)
  !   write(60,100)ipoin,rdir(1,iboup),rdir(2,iboup),rdir(3,iboup)
  !enddo
  !write(60,14)



  ! close(60)

  !Dekhna output

  !  110 format('nelem npoin')
  !  120  format(2i10)
  !  130  format(3e20.10)
  !  140  format(4i10)

  !       open(unit=70,file='straka.vol ',status='unknown')
  !       rewind 70
  !       write(70,110)
  !       write(70,120)nelem,npoin

  !     do  ipoin=1,npoin
  !        rx=coor(1,ipoin)
  !        ry=coor(2,ipoin)
  !        rz=coor(3,ipoin)
  !        write(70,130)rx,ry,rz
  !     enddo

  !     do  ielem=1,nelem
  !        write(70,140)elem(1,ielem),elem(2,ielem),elem(3,ielem),elem(4,ielem)
  !     enddo       

  !     close(70)

  !  210 format('npoin')
  !  220  format(i10)
  !  230  format('Density')
  !  240  format(e20.10)

  !       open(unit=80,file='straka.sres ',status='unknown')
  !       rewind 80
  !       write(80,210)
  !       write(80,220)npoin
  !       write(80,230)
  !       do  ipoin=1,npoin
  !          write(80,240)var(1,ipoin)
  !       enddo

  !       close(80)


end subroutine outcomp

subroutine dbg1d(nedge,ledge,npoin,nedgb,nboup,gam,edsha,var,nvar,coor,ndim)

  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: nvar,ndim
  integer(ip),intent(inout)  :: nedge,nedgb,nboup,npoin
  integer(ip),intent(inout)  :: ledge(2,nedge)
  real(rp),intent(in)        :: gam
  real(rp), intent(inout)    :: edsha(5,nedge),var(nvar,npoin),coor(ndim,npoin)
  real(rp)                   :: dx,dxi,c00,c10
  integer(ip)                :: i,iedge,ipa

  c00=0.0d+00
  c10=1.0d+00

  !DBG

  npoin=101
  dx=1.0d+00/float(npoin-1)
  dxi=float(npoin-1)
  coor(1,1)=c00
  coor(2,1)=c00
  coor(3,1)=c00
  do i=2,npoin
     coor(1,i)=coor(1,i-1)+dx
     coor(2,i)=c00
     coor(3,i)=c00
  enddo
  ipa=1

  do i=1,npoin

     if(coor(1,i)<0.5)then

        var(1,i)=1.0d+00
        var(2,i)=0.0d+00
        var(3,i)=0.0d+00
        var(4,i)=0.0d+00
        var(5,i)=1.0d+00/(gam-1.0d+00)
        var(6,i)=1.0d+00
        var(7,i)=1.0d+00

     else

        var(1,i)=1.00d-01
        var(2,i)=0.0d+00
        var(3,i)=0.0d+00
        var(4,i)=0.0d+00
        var(5,i)=1.0d-01/(gam-1.0d+00)
        var(6,i)=1.0d-01
        var(7,i)=1.0d-01/1.25d-01

     endif

  enddo


  do iedge=1,npoin-1
     ledge(1,iedge)=ipa
     ipa=ipa+1
     ledge(2,iedge)=ipa
     edsha(2,iedge)=c10/dx
     edsha(3,iedge)=c00
     edsha(4,iedge)=c00

  enddo

  nedge=npoin-1

  nedgb=0
  nboup=0




end subroutine dbg1d


subroutine outdbg1d(npoin,var,nvar,coor,ndim)

  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip), intent(in)    :: npoin,nvar,ndim
  real(rp), intent(inout)    :: var(nvar,npoin),coor(ndim,npoin)
  integer(ip)                :: ipoin

  open(unit=60,file='outdbg1d.dat',status='unknown')
  rewind 60


  do ipoin=1,npoin
     write(60,300)coor(1,ipoin),var(1,ipoin),var(2,ipoin)/var(1,ipoin),var(6,ipoin)/(var(1,ipoin)*0.4d+00),var(6,ipoin)
  enddo

300 format(5e20.10)


  close(60)

end subroutine outdbg1d


subroutine residedg(npoin,dvar,var,nvar,maxvar,nflu,time,niter)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)  :: npoin,nvar,nflu,niter
  real(rp),intent(in)     :: var(nvar,npoin),dvar(nflu,npoin),time
  real(rp),intent(inout)  :: maxvar 
  integer(ip)             :: ipoin
  real(rp)                :: mdrho,mdrhou,mdrhov,mdrhow,mdrhoe,ratio,mvar,epsil
  epsil=1.0d-12

  mdrho=abs(dvar(1,1)/var(1,1))
  mdrhou=abs(dvar(2,1)/max(epsil,var(2,1)))
  mdrhov=abs(dvar(3,1)/max(epsil,var(3,1)))
  mdrhow=abs(dvar(4,1)/max(epsil,var(4,1)))
  mdrhoe=abs(dvar(5,1)/var(5,1))

  do ipoin=1,npoin
     ratio=abs(dvar(1,ipoin)/var(1,ipoin))
     if(ratio>mdrho)mdrho=ratio   
     ratio=abs(dvar(2,ipoin)/max(epsil,abs(var(2,ipoin))))
     if(ratio>mdrhou)mdrhou=ratio   
     ratio=abs(dvar(3,ipoin)/max(epsil,abs(var(3,ipoin))))
     if(ratio>mdrhov)mdrhov=ratio   
     ratio=abs(dvar(4,ipoin)/max(epsil,abs(var(4,ipoin))))
     if(ratio>mdrhow)mdrhow=ratio   
     ratio=abs(dvar(5,ipoin)/var(5,ipoin))
     if(ratio>mdrhoe)mdrhoe=ratio   
  enddo

  maxvar=max(mdrho,mdrhou,mdrhov,mdrhow,mdrhoe)

  if(niter==1)then

     open(unit=66,file='resid.dat',status='unknown')
     rewind 66

  endif 

  write(66,100)niter,mdrho,mdrhou,mdrhov,mdrhow,mdrhoe    

100  format(i10,5e20.10)

end subroutine residedg

subroutine writeAdrien(nelem,elem,nnode,npoin,coor,ndim,nface,lface,nnofa,lsurf)
  use def_kintyp, only       :  ip,rp,lg,cell
  implicit none
  integer(ip),intent(in)    :: nelem,npoin,nface,nnode,nnofa,ndim
  real(rp),intent(in)       :: coor(ndim,npoin)   
  integer(ip),intent(in)    :: lface(nnofa,nface),elem(nnode,nelem),lsurf(nface)    
  integer(ip)               :: iface,ielem,ipoin
  real(rp)                  :: rx,ry,rz

  open(unit=50,file='adrien.msh',status='unknown')
  rewind 50
10 format('MeshVersionFormatted 2')
11 format('Dimension 3')
13 format('Vertices')
14 format('Tetrahedra')
15 format('Triangle')
16 format('End')
17 format('Solatvertices')
20 format(i10)
100 format(3e20.10,i10)
200 format(5i10)
300 format(4i10)

  write(50,10) 
  write(50,11) 
  write(50,13)
  write(50,20)npoin
  do ipoin=1,npoin
     rx=coor(1,ipoin)
     ry=coor(2,ipoin)
     rz=coor(3,ipoin)
     write(50,100)rx,ry,rz,0
  enddo
  write(50,14)
  write(50,20)nelem

  do  ielem=1,nelem
     write(50,200)elem(1,ielem),elem(2,ielem),elem(3,ielem),elem(4,ielem),0
  enddo

  write(50,15)
  write(50,20)nface
  do  iface=1,nface
     write(50,300)lface(1,iface),lface(2,iface),lface(3,iface),lsurf(iface)
  enddo


  write(50,15)

  close(50)





end subroutine writeAdrien


subroutine readwrite(nvar,npoin)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_nasedg, only          : memor_edg,var
  implicit none
  integer(ip),intent(inout)      :: nvar,npoin
  integer(ip)           :: ipoin,iter
  character*80                :: ntext
  real(rp)              :: time     
  integer(4)                 :: istat


  nvar=7
  npoin=158888


  allocate(var(nvar,npoin),stat=istat)
  call memchk(zero,istat,memor_edg,'VAR','comp',var)

  open(unit=70,file='outcomp.res',status='old')
  open(unit=60,file='outshort.res',status='unknown')
  !
  !     Header
  !    
  read(70,*)ntext,ntext,ntext,ntext,ntext

10 format('GID Post Results File 1.0')
11 format('Result "Velocity" "Analysis/time" ',1e20.10,' Vector OnNodes')
12 format('ComponentNames "Vx", "Vy" , "Vz" ')
13 format('Values')
14 format('End Values')
15 format('   ')
16 format('Result "Energy" "Analysis/time" ',1e20.10,' Scalar OnNodes')
17 format('Result "Density" "Analysis/time" ',1e20.10,' Scalar OnNodes')
18 format('Result "Pressure" "Analysis/time" ',1e20.10,' Scalar OnNodes')
19 format('Result "Temperature" "Analysis/time" ',1e20.10,' Scalar OnNodes')
20 format('Result "Potential Temperature" "Analysis/time" ',1e20.10,' Scalar OnNodes')
300 format(i10,1e20.10)
100 format(i10,3e20.10)

  rewind 60
  write(60,10)


  iter=0

  do
     if(iter==0)then
        iter=1
     else
        iter=0
     endif



     !
     !     Density
     !

     read(70,*)ntext,ntext,ntext,time,ntext,ntext
     read(70,*)ntext
     do  ipoin=1,npoin
        read(70,*)ntext,var(1,ipoin)
     enddo

     read(70,*)

     if(iter==1)then

        write(60,17)time
        write(60,13)
        do  ipoin=1,npoin
           write(60,300)ipoin,var(1,ipoin)
        enddo

        write(60,14)
     endif



     !
     !     Velocity
     !

     read(70,*)ntext,ntext,ntext,time,ntext,ntext
     read(70,*)ntext,ntext,ntext,ntext
     read(70,*)ntext
     do  ipoin=1,npoin
        read(70,*)ntext,var(2,ipoin),var(3,ipoin),var(4,ipoin)
     enddo

     read(70,*)

     if(iter==1)then

        write(60,11)time
        write(60,12)
        write(60,13)
        do  ipoin=1,npoin
           write(60,100)ipoin,var(2,ipoin),var(3,ipoin),var(4,ipoin)
        enddo
        write(60,14)
     endif


     !
     !       Energy   
     !

     read(70,*)ntext,ntext,ntext,time,ntext,ntext
     read(70,*)ntext
     do  ipoin=1,npoin
        read(70,*)ntext,var(5,ipoin)
     enddo

     read(70,*)

     if(iter==1)then


        write(60,16)time
        write(60,13)
        do  ipoin=1,npoin
           write(60,300)ipoin,var(5,ipoin)
        enddo
        write(60,14)
     endif

     !
     !     Pressure
     !

     read(70,*)ntext,ntext,ntext,time,ntext,ntext
     read(70,*)ntext
     do  ipoin=1,npoin
        read(70,*)ntext,var(6,ipoin)
     enddo

     read(70,*)

     if(iter==1)then

        write(60,18)time
        write(60,13)
        do  ipoin=1,npoin
           write(60,300)ipoin,var(6,ipoin)
        enddo
        write(60,14)
     endif

     !
     !     Temperature
     !

     read(70,*)ntext,ntext,ntext,time,ntext,ntext
     read(70,*)ntext
     do  ipoin=1,npoin
        read(70,*)ntext,var(7,ipoin)
     enddo

     read(70,*)

     if(iter==1)then

        write(60,19)time
        write(60,13)
        do  ipoin=1,npoin
           write(60,300)ipoin,var(7,ipoin)
        enddo
        write(60,14)
     endif

     !
     !     Potential temperature
     !

     read(70,*)ntext,ntext,ntext,time,ntext,ntext
     read(70,*)ntext
     do  ipoin=1,npoin
        read(70,*)ntext,var(7,ipoin)
     enddo

     read(70,*)

     if(iter==1)then

        write(60,20)time
        write(60,13)
        do  ipoin=1,npoin
           write(60,300)ipoin,var(7,ipoin)
        enddo
        write(60,14)

     endif

  enddo



  stop

end subroutine readwrite




subroutine readwrite2()
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_meshin, only          : memor_msh,lface,coor
  implicit none
  integer(ip)      :: npoin,nface
  integer(ip)      :: ipoin,iface,inum
  character*80     :: ntext
  integer(4)       :: istat
  real(rp)         ::rx,ry,rz 

  nface=5704
  npoin=2866


  allocate(lface(3,nface),stat=istat)
  call memchk(zero,istat,memor_msh,'LFACE','readwrite2',lface)
  allocate(coor(3,npoin),stat=istat)
  call memchk(zero,istat,memor_msh,'LFACE','readwrite2',coor)

  open(unit=70,file='747.dat',status='old')
  !
  !     Header
  !    
  read(70,*)ntext,ntext,ntext,ntext,ntext,ntext,ntext
  read(70,*)ntext
  read(70,*)ntext,ntext,ntext,ntext,ntext
  do ipoin=1,npoin
     read(70,*)coor(1,ipoin),coor(2,ipoin),coor(3,ipoin),inum 
  enddo
  read(70,*)ntext,ntext
  read(70,*)ntext
  do iface=1,nface
     read(70,*)lface(1,iface),lface(2,iface),lface(3,iface),inum
  enddo
  close(70)


   open(unit=50,file='747.msh',status='unknown')
     rewind 50

1    format('MESH dimension 3 ElemType Triangle Nnode 3')
2    format('Coordinates')
3    format('#node number   coord_x   coord_y  coord_z')
100  format(i10,3e20.10)
200  format(4i10)
4    format('end coordinates')
5    format('Elements')
6    format('end elements')
     write(50,1)
     write(50,2)
     write(50,3)
     do  ipoin=1,npoin
        rx=coor(1,ipoin)
        ry=coor(2,ipoin)
        rz=coor(3,ipoin)
        write(50,100)ipoin,rx,ry,rz
     enddo

     write(50,4)
     write(50,5)
     do  iface=1,nface
        write(50,200)iface,lface(1,iface),lface(2,iface),lface(3,iface)
     enddo
     write(50,6)
     close(50)

      stop
end subroutine readwrite2


subroutine readparam(igrav,ipre,ivisc,idual,idtloc,isolv,ifirst,nrkord,irenum,cfl,vn,fprint,dtaumax,grav,R,gam)
  use def_kintyp, only       :  ip,rp,lg,cell
  use mod_memchk
  use def_meshin, only          : memor_msh,lface,coor
  implicit none
  integer(ip),intent(inout) :: igrav,ipre,ivisc,idual,idtloc,isolv,ifirst
  integer(ip),intent(inout) :: nrkord,irenum
  real(rp),intent(inout)    :: cfl,vn,fprint,dtaumax,grav,R,gam
  character*80     :: ntext
  
  open(unit=50,file='param.dat',status='unknown')
  rewind 50
  read(50,*)ntext,ntext,ntext,ntext,ntext
  read(50,*)igrav,ipre,ivisc,idual,idtloc
  read(50,*)ntext,ntext,ntext
  read(50,*)isolv,ifirst,nrkord
  read(50,*)ntext,ntext
  read(50,*)cfl,vn
  read(50,*)ntext,ntext
  read(50,*)fprint,dtaumax
  read(50,*)ntext
  read(50,*)irenum
  read(50,*)ntext,ntext,ntext
  read(50,*)grav,R,gam
   close(50)

end subroutine readparam


