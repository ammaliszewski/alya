!------------------------------------------------------------------------
!> @addtogroup Partis
!! @{
!> @name    Partis memory
!! @file    pts_outvar.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!! @brief   Postprocess particle results on nodes
!! @details Postprocess particle results:
!!          - Number of particles per type
!!          - Desposited particles per type
!> @} 
!------------------------------------------------------------------------

subroutine pts_outvar(ivari)
  use def_parame
  use def_master
  use def_domain
  use def_partis
  use mod_memory
  use mod_gradie
  use mod_postpr
  use mod_projec, only :  projec_elements_to_nodes
  implicit none
  integer(ip), intent(in) :: ivari
  integer(ip)             :: ipoin,igaus,itype,idime
  integer(ip)             :: ilagr,ielem,pelty,pnode,pgaus,inode
  real(rp)                :: elcod(ndime,mnode)
  real(rp)                :: gpvol,gpdet
  real(rp)                :: xjacm(9) 
  real(rp),    pointer    :: gelem(:,:)

  if( ivari == 0 ) return

  select case ( ivari )  

  case( 1_ip )
     !
     ! Number of particles/node
     !
     if( INOTMASTER ) then
        nullify(gelem)
        call memgen(0_ip,ntyla_pts,npoin)
        call memory_alloca(mem_modul(1:2,modul),'GELEM','pts_outvar',gelem,ntyla_pts,nelem)
        !
        ! Count particles inside each element
        !
        do ilagr = 1,mlagr 
           if( lagrtyp(ilagr) % kfl_exist == -1 ) then 
              itype = lagrtyp(ilagr) % itype
              ielem = lagrtyp(ilagr) % ielem
              if( ielem > 0 .and. ielem <= nelem ) then
                 gelem(itype,ielem) = gelem(itype,ielem) + 1.0_rp
              end if
           end if
        end do
        call projec_elements_to_nodes(gelem,gevec) 
     else
        !
        ! This is necessary for outvar to get the right dimension
        !
        call memgen(0_ip,ntyla_pts,1_ip)
     end if

  case( 2_ip )
     !
     ! Deposition map
     !
     if( kfl_depos_pts == 0 ) return
     if( INOTMASTER ) then
        call projec_elements_to_nodes(depoe_pts,depos_pts) 
        gevec => depos_pts
     else
        call memgen(0_ip,ntyla_pts,1_ip)
     end if

  case( 3_ip )
     !
     ! Slip wall distance
     !     
     gesca => walld_slip_pts

  case( 4_ip )
     !
     ! Friction
     !     
     gesca => friction_pts

  case( 5_ip )
     !
     ! Residence
     !     
     if( kfl_resid_pts == 0 ) return
     if( INOTMASTER ) then
        call memgen(0_ip,ntyla_pts,npoin)
        call projec_elements_to_nodes(resid_pts,gevec) 
     else
        call memgen(0_ip,ntyla_pts,1_ip)
     end if

  end select

  call outvar(&
       ivari,&
       ittim,cutim,postp(1) % wopos(1,ivari))

end subroutine pts_outvar
