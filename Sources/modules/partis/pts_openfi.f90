subroutine pts_openfi(itask)
  !------------------------------------------------------------------------
  !****f* partis/pts_openfi
  ! NAME 
  !    pts_openfi
  ! DESCRIPTION
  !    This subroutine open files
  ! USES
  ! USED BY
  !    pts_turnon
  !***
  !------------------------------------------------------------------------
  use def_partis
  use def_master
  use def_kermod
  use def_domain
  use mod_iofile
  implicit none
  integer(ip), intent(in) :: itask 
  character(7)            :: statu
  character(11)           :: forma
  character(6)            :: posit
  character(200)          :: messa

  if( INOTSLAVE .or. itask == 10 ) then

     if( kfl_rstar == 2 ) then
        statu = 'old'
        forma = 'formatted'
        posit = 'append'
     else
        statu = 'unknown'
        forma = 'formatted'
        posit = 'asis'
     end if

     select case ( itask )

     case (   1_ip )
        !
        ! Open result file
        !
        if ( kfl_naked == 0 ) then
           call GETENV('FOR1725',fil_resul_pts)
           call GETENV('FOR1727',fil_oudep_pts)
        else if ( kfl_naked == 1 ) then
           fil_resul_pts = adjustl(trim(namda))//'.'           //exmod(modul)//'.res'
           fil_oudep_pts = adjustl(trim(namda))//'-deposition.'//exmod(modul)//'.csv' 
           !           
           !c etait mieux avant
           !
           !fil_oudep_pts = adjustl(trim(namda))//'-deposition.'//exmod(modul)//'.res' 
        end if

        if( kfl_rstar == 2 ) then 
           kfl_reawr = 1
           call iofile(4_ip,lun_resul_pts,fil_resul_pts,'LAGRANGIAN PARTICLES POSITION','old','unformatted')
           if( kfl_reawr == 1 ) then
              call iofile(zero,lun_resul_pts,fil_resul_pts,'LAGRANGIAN PARTICLES POSITION','old','formatted','append')
           else
              call iofile(zero,lun_resul_pts,fil_resul_pts,'LAGRANGIAN PARTICLES POSITION')
              igene = kfl_posla_pts
              call outfor(53_ip,lun_resul_pts,' ')
           end if
           kfl_reawr = 0
        else
           call iofile(zero,lun_resul_pts,fil_resul_pts,'LAGRANGIAN PARTICLES POSITION')
           igene = kfl_posla_pts
           call outfor(53_ip,lun_resul_pts,' ')
        end if
        !
        ! Convergence file
        !
        igene = kfl_posla_pts
        call outfor(54_ip,momod(modul) % lun_conve,' ')
        !
        ! Deposition file
        !
        if( kfl_oudep_pts /= 0 ) then
           if( kfl_rstar == 2 ) then 
              kfl_reawr = 1
              call iofile(4_ip,lun_oudep_pts,fil_oudep_pts,'LAGRANGIAN PARTICLES POSITION','old','unformatted')
              if( kfl_reawr == 1 ) then
                 call iofile(zero,lun_oudep_pts,fil_oudep_pts,'LAGRANGIAN PARTICLES POSITION','old','formatted','append')
              else
                 call iofile(zero,lun_oudep_pts,fil_oudep_pts,'LAGRANGIAN PARTICLES POSITION')
                 igene = kfl_oudep_pts
                 call outfor(57_ip,lun_oudep_pts,' ')
              end if
              kfl_reawr = 0
           else
              call iofile(zero,lun_oudep_pts,fil_oudep_pts,'LAGRANGIAN PARTICLES POSITION')
              igene = kfl_oudep_pts
              call outfor(57_ip,lun_oudep_pts,' ')
           end if
        end if

     end select

  end if

end subroutine pts_openfi

