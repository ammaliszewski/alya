!------------------------------------------------------------------------
!> @addtogroup Partis
!> @{
!> @file    pts_injec.f90
!> @author  Guillaume Houzeaux
!> @brief   Reallocate particle type
!> @details Reallocate particle type LAGRTYP
!> @} 
!------------------------------------------------------------------------

subroutine pts_reallocate(klagr)
  use def_master, only : ip
  use def_master, only : latyp
  use def_master, only : lagrtyp
  use def_master, only : mlagr
  use def_partis, only : lagrtyp_init
  implicit none  
  integer(ip), intent(in) :: klagr
  type(latyp), pointer    :: lagrtyp_tmp(:)

  if( size(lagrtyp) > klagr ) then
     !
     ! Required size is lower than actual
     !
     return

  else
     !
     ! Reallocate
     !
     allocate( lagrtyp_tmp(klagr) )
     lagrtyp_tmp(1:mlagr)       = lagrtyp(1:mlagr)
     lagrtyp_tmp(mlagr+1:klagr) = lagrtyp_init
     mlagr = klagr
     deallocate(lagrtyp)
     lagrtyp => lagrtyp_tmp

  end if

end subroutine pts_reallocate
