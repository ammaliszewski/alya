!-----------------------------------------------------------------------
!> @addtogroup Partis
!> @{
!> @file    pts_cross_wall.f90
!> @author  Guillaume Houzeaux
!> @date    05/11/2015
!> @brief   Check if a particle has crossed the wall
!> @details Check if a particle has crossed the wall
!>                                                     
!>                                                      |// wall
!>           lagrtyp(ilagr) % coord           coord_kp1 |//
!>                      o--------------------------->o  |//
!>                                 dista                |//
!>                                                      |//
!>           lagrtyp(ilagr) % coord                     |//  p2
!>                      o-------------------------------*---->o
!>                      <....................................>
!>                            dista + d/2 + hleng*toler |//
!>                                                      |//
!>                 p1                                   |//  p2
!>                  o---|-------------------------------*---->o
!>                  <...>
!>                d/2 + hleng*toler                     |//
!>                                                      |//
!>           * is the intersection point
!
!> @} 
!-----------------------------------------------------------------------

subroutine pts_cross_wall(ielem,ilagr,diame,hleng,iwall,coord_kp1,veloc_kp1,accel_kp1,xinte)

  use def_kintyp, only : ip,rp
  use def_master, only : lagrtyp,zeror 
  use def_domain, only : coord,lnodb,ndime
  use def_domain, only : lboel,mnodb,lnnob 
  use def_domain, only : ltypb
  use mod_elmgeo, only : elmgeo_segfac
  use mod_elmgeo, only : elmgeo_projection_on_a_face
  use def_partis, only : leleboun_pts
  use def_partis, only : kfl_fixbo_pts
  use def_partis, only : bouno_pts
  use def_partis, only : bvnat_pts  
  use def_partis, only : PTS_OUTFLOW_CONDITION
  use def_partis, only : PTS_WALL_CONDITION
  use def_partis, only : PTS_SLIP_CONDITION
  implicit none

  integer(ip), intent(inout)    :: ielem
  integer(ip), intent(in)       :: ilagr
  real(rp),    intent(in)       :: diame
  real(rp),    intent(in)       :: hleng
  integer(ip), intent(out)      :: iwall
  real(rp),    intent(inout)    :: coord_kp1(ndime)
  real(rp),    intent(inout)    :: veloc_kp1(ndime)
  real(rp),    intent(inout)    :: accel_kp1(ndime)
  real(rp),    intent(out)      :: xinte(ndime) 
  integer(ip)                   :: iboun,pnodb,kboun,iinte,pblty
  real(rp)                      :: facod(ndime,mnodb),p2(3),dista
  real(rp)                      :: s1,s2,toler,udotn,adotn,vect(3),p1(3)

  iwall         = 0
  iinte         = 0
  toler         = 0.01_rp  ! Tolerance 
  vect          = 0.0_rp
  vect(1:ndime) = coord_kp1(1:ndime)-lagrtyp(ilagr) % coord(1:ndime)
  dista         = sqrt(dot_product(vect(1:3),vect(1:3)))
  s1            = (         0.5_rp * diame + toler * hleng ) / ( dista + zeror )
  s2            = ( dista + 0.5_rp * diame + toler * hleng ) / ( dista + zeror )
  !s1            = (         0.5_rp * diame ) / ( dista + zeror )
  !s2            = ( dista + 0.5_rp * diame ) / ( dista + zeror )
  p1(1:ndime)   = lagrtyp(ilagr) % coord(1:ndime) - vect(1:ndime) * s1
  p2(1:ndime)   = lagrtyp(ilagr) % coord(1:ndime) + vect(1:ndime) * s2

  !-------------------------------------------------------------------
  !
  ! Loop over boundaries connected to IELEM to check intersection
  !
  !-------------------------------------------------------------------

  loop_kboun: do kboun = 1,size(leleboun_pts(ielem) % l)

     iboun                  = leleboun_pts(ielem) % l(kboun)
     pnodb                  = lnnob(iboun)
     pblty                  = ltypb(iboun)
     facod(1:ndime,1:pnodb) = coord(1:ndime,lnodb(1:pnodb,iboun))
     call elmgeo_segfac(ndime,pnodb,facod,p1,p2,iinte,xinte,toler)
     if( iinte /= 0 ) exit loop_kboun

  end do loop_kboun

  !-------------------------------------------------------------------
  !
  ! Trajectory crosses boundary IBOUN at coordinate XINTE
  !
  !-------------------------------------------------------------------

  if( iinte /= 0 ) then

     if(      kfl_fixbo_pts(iboun) == PTS_OUTFLOW_CONDITION ) then
        !
        ! Outflow
        !
        iwall = 1
        lagrtyp(ilagr) % kfl_exist = -4
        coord_kp1(1:ndime) = xinte(1:ndime)

     else if( kfl_fixbo_pts(iboun) == PTS_WALL_CONDITION ) then
        !
        ! Wall
        !
        if( bvnat_pts(1,iboun) > zeror ) then
           call runend('PTS_SOLITE: PARTICLE BOUNCING NOT CODED')
        else
           iwall = 1
           lagrtyp(ilagr) % kfl_exist = -2
           coord_kp1(1:ndime) = xinte(1:ndime)
        end if

     else if( kfl_fixbo_pts(iboun) == PTS_SLIP_CONDITION ) then   
        !
        ! Slip: project final point on the face and cancel normal velocity if it goes out 
        !
        call elmgeo_projection_on_a_face(ndime,pblty,facod,coord_kp1,xinte)
        udotn              = max(0.0_rp,dot_product(bouno_pts(1:ndime,iboun),veloc_kp1(1:ndime)))
        veloc_kp1(1:ndime) = veloc_kp1(1:ndime) - udotn * bouno_pts(1:ndime,iboun)
        coord_kp1(1:ndime) = xinte(1:ndime) 

     end if

  end if

end subroutine pts_cross_wall
