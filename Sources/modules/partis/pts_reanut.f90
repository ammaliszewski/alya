subroutine pts_reanut()
  !------------------------------------------------------------------------
  !****f* Partis/pts_reanut
  ! NAME 
  !    pts_reanut
  ! DESCRIPTION
  !    This routine reads data
  ! OUTPUT
  ! USES
  ! USED BY
  !    Reapro
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_master
  use def_kermod
  use def_inpout
  use def_domain
  use def_partis
  implicit none
  integer(ip) :: ipara,itype

  if( INOTSLAVE ) then

     kfl_adapt_pts =  0       
     kfl_usbin_pts =  0
     gamma_pts     =  0.5_rp             ! Newmark's gamma_pts constant
     beta_pts      =  1.0_rp / 4.0_rp    ! Newmark's beta constant
     chale_pts     = -1.0_rp             ! Characteristic length
     safet_pts     =  1.0_rp             ! Safety factor
     !gamma_pts   =  0.75_rp
     !beta_pts    =  0.390625_rp  

     !-------------------------------------------------------------
     !
     ! Numerical problem
     !
     !-------------------------------------------------------------

     call ecoute('ker_reanut')
     do while( words(1) /= 'NUMER' )
        call ecoute('ker_reanut')
     end do
     call ecoute('ker_reanut')

     do while(words(1) /= 'ENDNU' )

        if(      words(1) == 'NEWMA' ) then
           !
           ! Time integration
           !
           gamma_pts = getrea('GAMMA',0.5_rp, '#GAMMA FOR NEWMARK')
           beta_pts  = getrea('BETA ',0.25_rp,'#BETA FOR NEWMARK')

        else if( words(1) == 'ELEME' ) then
           !
           ! ELement search strategy
           !
           if( words(2) == 'BIN  ' ) then
              kfl_usbin_pts = 1
           else if( words(2) == 'NEIGH' ) then
              kfl_usbin_pts = 0
           end if

        else if( words(1) == 'ALGEB' ) then
           !
           ! Solver for slip wall
           !
           call reasol(1_ip)

        else if( words(1) == 'ADAPT' ) then
           !
           ! Time adaptation scheme
           !
           if( exists('CHARA') ) then
              if( exists('DIAME') ) then
                 chale_pts =  0.0_rp
              else if( exists('ELEME') ) then
                 chale_pts = -1.0_rp
              else if( exists('VISCO') ) then
                 chale_pts = -2.0_rp
              else if( exists('TAU  ') ) then
                 chale_pts = -3.0_rp
              else
                 chale_pts =  getrea('CHARA',1.0_rp,'#CHARACTERISTIC LENGTH')
              end if
           end if

           if( exists('SAFET') ) then
              safet_pts =  getrea('SAFET',1.0_rp,'#CHARACTERISTIC LENGTH')
           end if

           if( words(2) == 'ERROR' ) then
              kfl_adapt_pts = 1
           else if( words(2) == 'POSIT' ) then
              kfl_adapt_pts = 2
           else if( words(2) == 'FROMC' ) then
              kfl_adapt_pts = 3
           else if( words(2) == 'VELOC' ) then
              kfl_adapt_pts = 4
           end if

        end if

        call ecoute('ker_reanut')
     end do

  end if

end subroutine pts_reanut
