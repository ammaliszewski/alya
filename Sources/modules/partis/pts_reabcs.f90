!-----------------------------------------------------------------------
!> @addtogroup Partis
!> @{
!> @file    pts_inibcs.f90
!> @author  Guillaume Houzeaux
!> @date    16/11/1966   
!> @brief   Read boundary conditions 
!> @details The boundary codes are:
!>          KFL_FIXBOU(IBOUN) = 0 ... Outflow
!>                              1 ... Wall
!>                              3 ... Slip
!> @} 
!-----------------------------------------------------------------------

subroutine pts_reabcs()

  use def_kintyp
  use def_master
  use def_kermod
  use def_inpout
  use def_domain
  use def_partis
  use mod_opebcs
  implicit none
  character(len=8) :: fmt, num1, str2
  integer(ip) :: ipara, iinj
  !
  ! Allocate memory for boundary codes
  !
  call opbbcs(0_ip,1_ip,1_ip,tbcod_pts)      

  if( INOTSLAVE ) then

     kfl_injve_pts = 0
     ninj_pts      = 0
     tinla_pts     = 0.0_rp                   
     tpela_pts     = 0.0_rp
     kfl_injla_pts = 0
     parla_pts     = 0.0_rp
     parla2_pts    = 0.0_rp
     !
     ! Reach section
     !
     call ecoute('ker_reabcs')
     do while( words(1) /= 'BOUND' )
        call ecoute('ker_reabcs')
     end do
     call ecoute('ker_reabcs')
     !
     ! Read boundary conditions field
     !
     do while( words(1) /= 'ENDBO' )

        if( words(1) == 'CODES'.and.exists('BOUND') ) then
           ! 
           ! User-defined codes on boundaries
           !          
           kfl_fixbo => kfl_fixbo_pts
           bvnat     => bvnat_pts
           tbcod     => tbcod_pts(1:)
           call reacod(2_ip)

        else if( words(1)(1:4) == 'INJE') then
           !
           ! Inject particles with a saqure, sphere, semi-sphere or a circle
           !
           fmt = '(I1.1)'
           num1 = words(1)(5:5)
           read (num1, fmt) iinj
           if( iinj > 0 .and. iinj <= pts_minj) then
              if( words(2) == 'SQUAR' ) then
                 kfl_injla_pts(iinj) = 1
              else if( words(2) == 'SPHER' ) then
                 kfl_injla_pts(iinj) = 2
              else if( words(2) == 'SEMIS' ) then
                 kfl_injla_pts(iinj) = 3
              else if( words(2) == 'CIRCL' ) then
                 kfl_injla_pts(iinj) = 4
              else if( words(2) == 'RECTA' ) then
                 kfl_injla_pts(iinj) = 5
              else if( words(2) == 'POINT' ) then
                 kfl_injla_pts(iinj) = 6
             end if
              do ipara = 1,min(mpala,nnpar)
                 parla_pts(iinj,ipara) = param(ipara+2)
              end do
            else
                write(str2,fmt) pts_minj
                call runend("NUMBER OF INJECTIONS MUST BE BETWEEN 1 AND "//str2)
            end if
           
        else if( words(1) == 'INITI' ) then
           !
           ! Initial injection time
           !
           tinla_pts = getrea('INITI',1.0_rp,'#INITIAL INJECTION TIME LAGRANGIAN PARTICLE')
           
        else if( words(1) == 'PERIO' ) then
           !
           ! Adding a velocity field (unifrom or gaussian) in injected particles
           !
           tpela_pts = getrea('PERIO',1.0_rp,'#PERIOD INJECTION TIME LAGRANGIAN PARTICLE')
           
        else if( words(1) == 'VELOC' ) then
           !
           ! Injection velocity
           !
           if(     words(2) == 'ZERO ' ) then
              kfl_injve_pts = -1
           else if( words(2) == 'FLUID' ) then
              kfl_injve_pts =  0
           else if( words(2) == 'UNIFO' ) then
              kfl_injve_pts =  1
           else if( words(2) == 'GAUSS' ) then
              kfl_injve_pts =  2
           end if
           do ipara = 1,min(mpala,nnpar)
              parla2_pts(ipara) = param(ipara+2)
           end do

        end if
        
        call ecoute('ker_reabcs')
     end do

  end if

end subroutine pts_reabcs
