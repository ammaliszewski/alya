!------------------------------------------------------------------------
!> @addtogroup Partis
!! @{
!> @name    Partis inner iteration
!! @file    pts_doiter.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!! @brief   This routine ends a time step for particles
!! @details Update residence time
!> @} 
!------------------------------------------------------------------------

subroutine pts_endite()
  use def_kintyp
  use def_master
  use def_domain
  use def_partis
  use mod_communications, only : PAR_FROM_GHOST_ELEMENT_EXCHANGE
  implicit none
  integer(ip)   :: ielem
  !
  ! Residence: pass information from ghost element
  ! Pass information computed on my ghost element and send it to my neighbors
  !
  if( INOTMASTER .and. kfl_resid_pts /= 0 ) then
     call PAR_FROM_GHOST_ELEMENT_EXCHANGE(resid_pts,'SUM','IN MY CODE')
     do ielem = nelem+1,nelem_2
        resid_pts(1:ntyla_pts,ielem) = 0.0_rp
     end do 
  end if

end subroutine pts_endite
