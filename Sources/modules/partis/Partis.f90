subroutine Partis(order)
  !-----------------------------------------------------------------------
  !****f* partis/Partis
  ! NAME
  !   Partis
  ! DESCRIPTION
  !   This routine deals with the ADS equation. The task done
  !   corresponds to the order given by the master.
  ! USES
  !    pts_turnon
  !    pts_timste
  !    pts_begste
  !    pts_doiter
  !    pts_concon
  !    pts_conblk
  !    pts_newmsh
  !    pts_endste
  !    pts_turnof
  ! USED BY
  !    Reapro
  !    Turnon
  !    Timste
  !    Begste
  !    Doiter
  !    Concon
  !    Conblk
  !    Newmsh
  !    Endste
  !    Turnof
  !***
  !-----------------------------------------------------------------------
  use def_master
  implicit none
  integer(ip), intent(in) :: order
  select case (order)
 
  case(ITASK_TURNON)
     call pts_turnon()
  case(ITASK_INIUNK) 
     call pts_iniunk()
  case(ITASK_TIMSTE) 
     !call pts_timste()
  case(ITASK_BEGSTE) 
     call pts_begste()
  case(ITASK_DOITER)
     call pts_doiter()
  case(ITASK_CONCOU) 
     !call pts_concou()
  case(ITASK_CONBLK)
     !call pts_conblk()
  case(ITASK_NEWMSH)
     !call pts_newmsh()
  case(ITASK_ENDSTE)
     call pts_endste()
  case(ITASK_OUTPUT)
     call pts_output()
  case(ITASK_TURNOF)
     !call pts_turnof()
  end select
  !
  ! Coupling
  ! 
  if( order > 1000 ) call pts_plugin(order-1000_ip) 

end subroutine Partis
