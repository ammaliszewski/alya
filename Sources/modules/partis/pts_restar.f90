!------------------------------------------------------------------------
!> @addtogroup Partis
!! @{
!> @name    Partis restart
!! @file    pts_restar.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!! @brief   This routine writes or reads restart file
!! @details Restart file
!> @} 
!------------------------------------------------------------------------

subroutine pts_restar(itask)
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_partis
  use mod_memory
  use mod_postpr
  implicit none
  integer(ip), intent(in)    :: itask
  integer(ip)                :: kfl_gores,ipart,ilagr,ipars,isize,recvsize,sendsize
  integer(ip)                :: nlagr_0,nvar2,kfl_reawr_tmp
  integer(4)                 :: nvar2_4
  integer(ip), pointer       :: ksize(:)
  real(rp),    pointer       :: xarra(:)
  !
  ! Check if this is a preliminary or restart
  !
  call respre(itask,kfl_gores)
  if( kfl_gores == 0 ) return
  !
  ! 13 variables are required for restart
  !
  nvar2   = 31
  nvar2_4 = int(nvar2,4)
  nullify(xarra)
  nullify(ksize)

  select case ( itask )

  case ( READ_RESTART_FILE )

     !-------------------------------------------------------------------
     !
     ! Read continue restart file
     !
     !-------------------------------------------------------------------

     if( kfl_rstar == 2 ) then
        !
        ! Open file
        !
        kfl_reawr_tmp = kfl_reawr
        call parari('SUM',0_ip,1_ip,kfl_reawr_tmp) 
        !
        ! File does not exist: return in normal mode
        !
        if( kfl_reawr_tmp < 0 ) return

        if( IMASTER ) then
           !
           ! Read restart file
           ! IPARS .................. Total number of particles * NVAR2
           ! ksize(IPART) / NVAR2 ... Number of particles to read for subdomain IPART
           !           
           call memory_alloca(mem_modul(1:2,modul),'KSIZE','pts_restar',ksize,npart+1_ip,'DO_NOT_INITIALIZE')

           read(momod(modul) % lun_rstar) nlacc_pts
           read(momod(modul) % lun_rstar) cutla_pts
           read(momod(modul) % lun_rstar) ( ksize(ipart), ipart = 2,npart+1 )

           ipars = 0
           do ipart = 2,npart+1
              ipars = ipars + ksize(ipart)
           end do
           call livinf(-9_ip,'READ LAGRANGIAN PARTICLES= ',ipars / nvar2) 
           call memory_alloca(mem_modul(1:2,modul),'XARRA','pts_restar',xarra,ipars,'DO_NOT_INITIALIZE')

           ipars = 0
           do ipart = 2,npart+1
              do ilagr = 1,ksize(ipart) / nvar2
                 read(momod(modul) % lun_rstar) &
                      xarra(ipars +  1) , xarra(ipars +  2) , xarra(ipars +  3) , &
                      xarra(ipars +  4) , xarra(ipars +  5) , xarra(ipars +  6) , &
                      xarra(ipars +  7) , xarra(ipars +  8) , xarra(ipars +  9) , &
                      xarra(ipars + 10) , xarra(ipars + 11) , xarra(ipars + 12) , &
                      xarra(ipars + 13) , xarra(ipars + 14) , xarra(ipars + 15) , &
                      xarra(ipars + 16) , xarra(ipars + 17) , xarra(ipars + 18) , &
                      xarra(ipars + 19) , xarra(ipars + 20) , xarra(ipars + 21) , &
                      xarra(ipars + 22) , xarra(ipars + 23) , xarra(ipars + 24) , &
                      xarra(ipars + 25) , xarra(ipars + 26) , xarra(ipars + 27) , &
                      xarra(ipars + 28) , xarra(ipars + 29) , xarra(ipars + 30) , &
                      xarra(ipars + 31) 
                 ipars = ipars + nvar2
              end do
           end do

           ipars = 1
           do ipart = 2,npart+1
              kfl_desti_par = ipart - 1
              isize         = ksize(ipart)
              call parari('SND',0_ip,1_ip,nlacc_pts)
              call pararr('SND',0_ip,1_ip,cutla_pts)
              call parari('SND',0_ip,1_ip,isize)
              if( isize > 0 ) then
                 call pararr('SND',0_ip,isize,xarra(ipars))
              end if
              ipars = ipars + isize
           end do

           call memory_deallo(mem_modul(1:2,modul),'XARRA','pts_restar',xarra)
           call memory_deallo(mem_modul(1:2,modul),'KSIZE','pts_restar',ksize)           

        else if( ISEQUEN ) then
           !
           ! Sequen read file
           !        
           read(momod(modul) % lun_rstar) nlacc_pts
           read(momod(modul) % lun_rstar) cutla_pts
           read(momod(modul) % lun_rstar) isize 
           do ilagr = 1,isize
              lagrtyp(ilagr) % kfl_exist = -1
              read(momod(modul) % lun_rstar) &
                   lagrtyp(ilagr) % coord(1) , lagrtyp(ilagr) % coord(2) , lagrtyp(ilagr) % coord(3) , &
                   lagrtyp(ilagr) % veloc(1) , lagrtyp(ilagr) % veloc(2) , lagrtyp(ilagr) % veloc(3) , &
                   lagrtyp(ilagr) % accel(1) , lagrtyp(ilagr) % accel(2) , lagrtyp(ilagr) % accel(3) , &
                   lagrtyp(ilagr) % acced(1) , lagrtyp(ilagr) % acced(2) , lagrtyp(ilagr) % acced(3) , &
                   lagrtyp(ilagr) % stret    , lagrtyp(ilagr) % t        , &
                   lagrtyp(ilagr) % ilagr    , lagrtyp(ilagr) % itype    , lagrtyp(ilagr) % ielem    , lagrtyp(ilagr) % ittim , &
                   lagrtyp(ilagr) % dt_k     , lagrtyp(ilagr) % dt_km1   , lagrtyp(ilagr) % dt_km2   , lagrtyp(ilagr) % dtg   , &
                   lagrtyp(ilagr) % v_fluid_k(1)   , lagrtyp(ilagr) % v_fluid_k(2)   , lagrtyp(ilagr) % v_fluid_k(3)   , &
                   lagrtyp(ilagr) % v_fluid_km1(1) , lagrtyp(ilagr) % v_fluid_km1(2) , lagrtyp(ilagr) % v_fluid_km1(3) , &
                   lagrtyp(ilagr) % v_fluid_km2(1) , lagrtyp(ilagr) % v_fluid_km2(2) , lagrtyp(ilagr) % v_fluid_km2(3) 
           end do

        else if( ISLAVE ) then
           !
           ! Slave receive particle info from master
           !
           kfl_desti_par = 0 
           call parari('RCV',0_ip,1_ip,nlacc_pts)
           call pararr('RCV',0_ip,1_ip,cutla_pts)
           call parari('RCV',0_ip,1_ip,isize)

           nlagr_0 = isize / nvar2 
           
           if( nlagr_0 > mlagr ) call pts_reallocate(nlagr_0)

           if( isize > 0 ) then
              call memory_alloca(mem_modul(1:2,modul),'XARRA','pts_restar',xarra,isize,'DO_NOT_INITIALIZE')
              call pararr('RCV',0_ip,isize,xarra)
              ipars = 0
              do ilagr = 1,nlagr_0
                 lagrtyp(ilagr) % kfl_exist = -1
                 lagrtyp(ilagr) % coord(1)  =      xarra( ipars +  1 )
                 lagrtyp(ilagr) % coord(2)  =      xarra( ipars +  2 )
                 lagrtyp(ilagr) % coord(3)  =      xarra( ipars +  3 )
                 lagrtyp(ilagr) % veloc(1)  =      xarra( ipars +  4 )
                 lagrtyp(ilagr) % veloc(2)  =      xarra( ipars +  5 )
                 lagrtyp(ilagr) % veloc(3)  =      xarra( ipars +  6 )
                 lagrtyp(ilagr) % accel(1)  =      xarra( ipars +  7 )
                 lagrtyp(ilagr) % accel(2)  =      xarra( ipars +  8 )
                 lagrtyp(ilagr) % accel(3)  =      xarra( ipars +  9 )
                 lagrtyp(ilagr) % acced(1)  =      xarra( ipars + 10 )
                 lagrtyp(ilagr) % acced(2)  =      xarra( ipars + 11 )
                 lagrtyp(ilagr) % acced(3)  =      xarra( ipars + 12 )
                 lagrtyp(ilagr) % stret     =      xarra( ipars + 13 )
                 lagrtyp(ilagr) % t         =      xarra( ipars + 14 )
                 lagrtyp(ilagr) % ilagr     =  int(xarra( ipars + 15 ),ip)
                 lagrtyp(ilagr) % itype     =  int(xarra( ipars + 16 ),ip)
                 lagrtyp(ilagr) % ielem     =  int(xarra( ipars + 17 ),ip)
                 lagrtyp(ilagr) % ittim     =  int(xarra( ipars + 18 ),ip)
                 lagrtyp(ilagr) % dt_k      =      xarra( ipars + 19 )
                 lagrtyp(ilagr) % dt_km1    =      xarra( ipars + 20 )
                 lagrtyp(ilagr) % dt_km2    =      xarra( ipars + 21 )
                 lagrtyp(ilagr) % dtg       =      xarra( ipars + 22 )
                 lagrtyp(ilagr) % v_fluid_k(1) =   xarra( ipars + 23 )
                 lagrtyp(ilagr) % v_fluid_k(2) =   xarra( ipars + 24 )
                 lagrtyp(ilagr) % v_fluid_k(3) =   xarra( ipars + 25 )
                 lagrtyp(ilagr) % v_fluid_km1(1) = xarra( ipars + 26 )
                 lagrtyp(ilagr) % v_fluid_km1(2) = xarra( ipars + 27 )
                 lagrtyp(ilagr) % v_fluid_km1(3) = xarra( ipars + 28 )
                 lagrtyp(ilagr) % v_fluid_km2(1) = xarra( ipars + 29 )
                 lagrtyp(ilagr) % v_fluid_km2(2) = xarra( ipars + 30 )
                 lagrtyp(ilagr) % v_fluid_km2(3) = xarra( ipars + 31 )
                 ipars                      =  ipars + nvar2
              end do
              call memory_deallo(mem_modul(1:2,modul),'XARRA','pts_restar',xarra)
           end if

        end if
     end if

  case ( WRITE_RESTART_FILE )

     !-------------------------------------------------------------------
     !
     ! Write restart file
     !
     !-------------------------------------------------------------------

     nullify(sendbuf_ip)
     nullify(recvbuf_ip)
     nullify(sendbuf_rp)
     nullify(recvbuf_rp)
     nullify(displs)
     nullify(recvcounts)

     call memory_alloca(mem_modul(1:2,modul),'SENDBUF_IP','pts_restar',sendbuf_ip,1_ip)
     recvsize  = npart+1
     sendcount = 1
     recvcount = 1
     !
     ! SENDBUF_IP(IPART): Number of particles sent by slave IPART to master
     ! Just send existing particles with LAGRTYP(ILAGR) % KFL_EXIST = -1
     !
     if( INOTMASTER ) then
        sendbuf_ip(1) = 0
        do ilagr = 1,mlagr
           if( lagrtyp(ilagr) % kfl_exist == -1 ) then 
              sendbuf_ip(1) = sendbuf_ip(1) + 1
           end if
        end do
        call memory_alloca(mem_modul(1:2,modul),'RECVBUF_IP','pts_restar',recvbuf_ip,1_ip)
     else
        call memory_alloca(mem_modul(1:2,modul),'RECVBUF_IP','pts_restar',recvbuf_ip,recvsize)
     end if
     !
     !  MPI_Gather the number of particles
     !
     call Parall(709_ip)
     !
     ! The previous receive buffer is now a RECVCOUNTS
     !
     if( INOTMASTER ) then
        call memory_alloca(mem_modul(1:2,modul),'RECVCOUNTS','pts_restar',recvcounts,1_ip)
        sendcount = int(sendbuf_ip(1),4)
     else           
        call memory_alloca(mem_modul(1:2,modul),'RECVCOUNTS','pts_restar',recvcounts,recvsize)
     end if
     do ipart = 1,size(recvbuf_ip,1)
        recvcounts(ipart) = int(recvbuf_ip(ipart),4)
     end do
     call memory_deallo(mem_modul(1:2,modul),'RECVBUF_IP','pts_restar',recvbuf_ip)
     call memory_deallo(mem_modul(1:2,modul),'SENDBUF_IP','pts_restar',sendbuf_ip)

     if( IMASTER ) then

        call memory_alloca(mem_modul(1:2,modul),'DISPLS','pts_restar',displs,npart+1_ip,'DO_NOT_INITIALIZE')
        recvcounts(1) =  0
        recvcounts(2) =  recvcounts(2) * nvar2_4
        recvsize      =  recvcounts(2)
        sendcount     =  0
        displs(1)     =  0
        displs(2)     =  0
        do ipart = 3,npart+1
           recvcounts(ipart) = recvcounts(ipart) * int(nvar2_4,4)
           displs(ipart)     = displs(ipart-1)   + recvcounts(ipart-1)
           recvsize          = recvsize          + recvcounts(ipart)
        end do
        recvsize = max(recvsize,1_4)

        call memory_alloca(mem_modul(1:2,modul),'RECVBUF_RP','pts_restar',recvbuf_rp,recvsize,'DO_NOT_INITIALIZE')
        call memory_alloca(mem_modul(1:2,modul),'SENDBUF_RP','pts_restar',sendbuf_rp,    1_ip,'DO_NOT_INITIALIZE')

     else if( ISLAVE ) then

        sendcount = sendcount * nvar2_4
        sendsize  = max(sendcount,1_ip)
        call memory_alloca(mem_modul(1:2,modul),'DISPLS'    ,'pts_restar',displs,1_ip)
        call memory_alloca(mem_modul(1:2,modul),'RECVBUF_RP','pts_restar',recvbuf_rp,1_ip)
        call memory_alloca(mem_modul(1:2,modul),'SENDBUF_RP','pts_restar',sendbuf_rp,sendsize,'DO_NOT_INITIALIZE')

        ipars =  0
        do ilagr = 1,mlagr
           if( lagrtyp(ilagr) % kfl_exist == -1 ) then
              ipars             = ipars + 1                      ! 1
              sendbuf_rp(ipars) = lagrtyp(ilagr) % coord(1)
              ipars             = ipars + 1                      ! 2
              sendbuf_rp(ipars) = lagrtyp(ilagr) % coord(2)
              ipars             = ipars + 1                      ! 3
              sendbuf_rp(ipars) = lagrtyp(ilagr) % coord(3)
              ipars             = ipars + 1                      ! 4
              sendbuf_rp(ipars) = lagrtyp(ilagr) % veloc(1)
              ipars             = ipars + 1                      ! 5
              sendbuf_rp(ipars) = lagrtyp(ilagr) % veloc(2)
              ipars             = ipars + 1                      ! 6
              sendbuf_rp(ipars) = lagrtyp(ilagr) % veloc(3)
              ipars             = ipars + 1                      ! 7
              sendbuf_rp(ipars) = lagrtyp(ilagr) % accel(1)
              ipars             = ipars + 1                      ! 8
              sendbuf_rp(ipars) = lagrtyp(ilagr) % accel(2)
              ipars             = ipars + 1                      ! 9
              sendbuf_rp(ipars) = lagrtyp(ilagr) % accel(3)
              ipars             = ipars + 1                      ! 10: Useful for nastin coupling
              sendbuf_rp(ipars) = lagrtyp(ilagr) % acced(1)
              ipars             = ipars + 1                      ! 11
              sendbuf_rp(ipars) = lagrtyp(ilagr) % acced(2)
              ipars             = ipars + 1                      ! 12
              sendbuf_rp(ipars) = lagrtyp(ilagr) % acced(3)
              ipars             = ipars + 1                      ! 13
              sendbuf_rp(ipars) = lagrtyp(ilagr) % stret
              ipars             = ipars + 1                      ! 14
              sendbuf_rp(ipars) = lagrtyp(ilagr) % t
              ipars             = ipars + 1                      ! 15
              sendbuf_rp(ipars) = real(lagrtyp(ilagr) % ilagr,rp)
              ipars             = ipars + 1                      ! 16
              sendbuf_rp(ipars) = real(lagrtyp(ilagr) % itype,rp)
              ipars             = ipars + 1                      ! 17
              sendbuf_rp(ipars) = real(lagrtyp(ilagr) % ielem,rp)
              ipars             = ipars + 1                      ! 18
              sendbuf_rp(ipars) = real(lagrtyp(ilagr) % ittim,rp)
              ipars             = ipars + 1                      ! 19
              sendbuf_rp(ipars) = lagrtyp(ilagr) % dt_k
              ipars             = ipars + 1                      ! 20
              sendbuf_rp(ipars) = lagrtyp(ilagr) % dt_km1
              ipars             = ipars + 1                      ! 21
              sendbuf_rp(ipars) = lagrtyp(ilagr) % dt_km2
              ipars             = ipars + 1                      ! 22: Prediction time step
              sendbuf_rp(ipars) = lagrtyp(ilagr) % dtg    
              ipars             = ipars + 1                      ! 23
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_k(1)
              ipars             = ipars + 1                      ! 24
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_k(2)
              ipars             = ipars + 1                      ! 25
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_k(3)
              ipars             = ipars + 1                      ! 26
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_km1(1)
              ipars             = ipars + 1                      ! 27
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_km1(2)
              ipars             = ipars + 1                      ! 28
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_km1(3)
              ipars             = ipars + 1                      ! 29
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_km2(1)
              ipars             = ipars + 1                      ! 30
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_km2(2)
              ipars             = ipars + 1                      ! 31
              sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_km2(3)

            end if
        end do

     end if
     !
     ! MPI_Gatherv 
     !
     call Parall(710_ip)

     if( IMASTER ) then
        !
        ! Only write existing particles with kfl_exist = -1
        ! PARIG(IPART) ......... Length of data received by slave IPART
        ! PARIG(IPART)/NVAR2 ... Number of existing particles received by slave IPART
        !
        write(momod(modul) % lun_rstar) nlacc_pts
        write(momod(modul) % lun_rstar) cutla_pts
        write(momod(modul) % lun_rstar) ( int(recvcounts(ipart),ip) , ipart = 2,npart+1 )
        ipars = 0
        do ipart = 2,npart+1
           do ilagr = 1,recvcounts(ipart) / nvar2
              write(momod(modul) % lun_rstar)                                                 &
                   recvbuf_rp(ipars +  1) , recvbuf_rp(ipars +  2) , recvbuf_rp(ipars +  3) , &
                   recvbuf_rp(ipars +  4) , recvbuf_rp(ipars +  5) , recvbuf_rp(ipars +  6) , &
                   recvbuf_rp(ipars +  7) , recvbuf_rp(ipars +  8) , recvbuf_rp(ipars +  9) , &
                   recvbuf_rp(ipars + 10) , recvbuf_rp(ipars + 11) , recvbuf_rp(ipars + 12) , &
                   recvbuf_rp(ipars + 13) , recvbuf_rp(ipars + 14) , recvbuf_rp(ipars + 15) , &
                   recvbuf_rp(ipars + 16) , recvbuf_rp(ipars + 17) , recvbuf_rp(ipars + 18) , &
                   recvbuf_rp(ipars + 19) , recvbuf_rp(ipars + 20) , recvbuf_rp(ipars + 21) , &
                   recvbuf_rp(ipars + 22) , recvbuf_rp(ipars + 23) , recvbuf_rp(ipars + 24) , &
                   recvbuf_rp(ipars + 25) , recvbuf_rp(ipars + 26) , recvbuf_rp(ipars + 27) , &
                   recvbuf_rp(ipars + 28) , recvbuf_rp(ipars + 29) , recvbuf_rp(ipars + 30) , &
                   recvbuf_rp(ipars + 31) 
              ipars = ipars + nvar2
           end do
        end do

     else if( ISEQUEN ) then

        isize = 0
        do ilagr = 1,mlagr
           if( lagrtyp(ilagr) % kfl_exist == -1 ) isize = isize + 1
        end do
        write(momod(modul) % lun_rstar) nlacc_pts
        write(momod(modul) % lun_rstar) cutla_pts
        write(momod(modul) % lun_rstar) isize
        do ilagr = 1,mlagr
           if( lagrtyp(ilagr) % kfl_exist == -1 ) then
              write(momod(modul) % lun_rstar) &
                   lagrtyp(ilagr) % coord(1) , lagrtyp(ilagr) % coord(2) , lagrtyp(ilagr) % coord(3) , &
                   lagrtyp(ilagr) % veloc(1) , lagrtyp(ilagr) % veloc(2) , lagrtyp(ilagr) % veloc(3) , &
                   lagrtyp(ilagr) % accel(1) , lagrtyp(ilagr) % accel(2) , lagrtyp(ilagr) % accel(3) , &
                   lagrtyp(ilagr) % acced(1) , lagrtyp(ilagr) % acced(2) , lagrtyp(ilagr) % acced(3) , &
                   lagrtyp(ilagr) % stret    , lagrtyp(ilagr) % t        , &
                   lagrtyp(ilagr) % ilagr    , lagrtyp(ilagr) % itype    , lagrtyp(ilagr) % ielem    , lagrtyp(ilagr) % ittim , &
                   lagrtyp(ilagr) % dt_k     , lagrtyp(ilagr) % dt_km1   , lagrtyp(ilagr) % dt_km2   , lagrtyp(ilagr) % dtg   , &
                   lagrtyp(ilagr) % v_fluid_k(1)   , lagrtyp(ilagr) % v_fluid_k(2)   , lagrtyp(ilagr) % v_fluid_k(3)   , &
                   lagrtyp(ilagr) % v_fluid_km1(1) , lagrtyp(ilagr) % v_fluid_km1(2) , lagrtyp(ilagr) % v_fluid_km1(3) , &
                   lagrtyp(ilagr) % v_fluid_km2(1) , lagrtyp(ilagr) % v_fluid_km2(2) , lagrtyp(ilagr) % v_fluid_km2(3)
           end if
        end do

     end if
     !
     ! Deallocate memory
     !
     if( IPARALL ) then
        call memory_deallo(mem_modul(1:2,modul),'SENDBUF_RP','pts_restar',sendbuf_rp)
        call memory_deallo(mem_modul(1:2,modul),'RECVBUF_RP','pts_restar',recvbuf_rp)
        call memory_deallo(mem_modul(1:2,modul),'DISPLS'    ,'pts_restar',displs)
        call memory_deallo(mem_modul(1:2,modul),'RECVCOUNTS','pts_restar',recvcounts)
     end if

  end select

  !----------------------------------------------------------------------
  !
  ! Deposition map
  ! 
  !----------------------------------------------------------------------

  if( kfl_depos_pts /= 0 ) then
     gevec => depos_pts
     call postpr(gevec,postp(1) % wopos(1:3,2),ittim,cutim)
  end if
  !
  ! Finish
  !
  call respre(3_ip,kfl_gores)

end subroutine pts_restar
