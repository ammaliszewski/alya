subroutine pts_inivar(itask)
  !-----------------------------------------------------------------------
  !****f* Partis/pts_inivar
  ! NAME 
  !    pts_inivar
  ! DESCRIPTION
  !    Initial variables
  ! USES
  ! USED BY
  !    pts_turnon
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_partis
  use def_solver
  use mod_iofile
  use mod_communications, only : PAR_SUM
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: itype,iboun

  select case( itask )

  case( 0_ip )
     !
     ! Postprocess
     !
     postp(1) % wopos (1, 1) = 'PARTI'
     postp(1) % wopos (1, 2) = 'DEPOS'
     postp(1) % wopos (1, 3) = 'SLIPW'
     postp(1) % wopos (1, 4) = 'FRICT'
     postp(1) % wopos (1, 5) = 'RESID'

     postp(1) % wopos (2, 1) = 'MULTI'
     postp(1) % wopos (2, 2) = 'MULTI'
     postp(1) % wopos (2, 3) = 'SCALA'
     postp(1) % wopos (2, 4) = 'SCALA'
     postp(1) % wopos (2, 5) = 'MULTI'
     !
     ! Others
     !
     nlacc_pts      = 0     ! Number of accumulated particles
     kfl_depos_pts  = 0     ! Deposition map on mesh is not postprocess
     kfl_timei      = 1     ! Problem is always transient
     particles_sent = 0     ! # of particles received
     particles_recv = 0     ! # of particles sent
     comm_loops_pts = 0     ! # communication loops
     kfl_resid_pts  = 0     ! Residence time not required
     nullify(depos_pts)     ! Deposition map on mesh
     nullify(depoe_pts)     ! Deposition map over nodes
     nullify(defor_pts)     ! Deformation tensor
     nullify(hleng_pts)     ! Element characteristics length
     nullify(ledep_pts)  
     nullify(lboue_pts)
     nullify(kfl_fixbo_pts)     
     nullify(bvnat_pts)  
     nullify(tbcod_pts)
     nullify(leleboun_pts) 
     nullify(bouno_pts)  
     nullify(walld_slip_pts)
     nullify(kfl_fixno_walld_slip_pts)
     nullify(friction_pts)
     nullify(resid_pts)
     !
     ! Initialization particle
     !
     lagrtyp_init % ilagr          = 0
     lagrtyp_init % itype          = 1
     lagrtyp_init % kfl_exist      = 0
     lagrtyp_init % ielem          = 0
     lagrtyp_init % coord(1)       = 0.0_rp
     lagrtyp_init % coord(2)       = 0.0_rp
     lagrtyp_init % coord(3)       = 0.0_rp
     lagrtyp_init % veloc(1)       = 0.0_rp
     lagrtyp_init % veloc(2)       = 0.0_rp
     lagrtyp_init % veloc(3)       = 0.0_rp
     lagrtyp_init % accel(1)       = 0.0_rp
     lagrtyp_init % accel(2)       = 0.0_rp
     lagrtyp_init % accel(3)       = 0.0_rp
     lagrtyp_init % v_fluid_k(1)   = 0.0_rp
     lagrtyp_init % v_fluid_k(2)   = 0.0_rp
     lagrtyp_init % v_fluid_k(3)   = 0.0_rp
     lagrtyp_init % v_fluid_km1(1) = 0.0_rp
     lagrtyp_init % v_fluid_km1(2) = 0.0_rp
     lagrtyp_init % v_fluid_km1(3) = 0.0_rp
     lagrtyp_init % v_fluid_km2(1) = 0.0_rp
     lagrtyp_init % v_fluid_km2(2) = 0.0_rp
     lagrtyp_init % v_fluid_km2(3) = 0.0_rp
     lagrtyp_init % acced(1)       = 0.0_rp
     lagrtyp_init % acced(2)       = 0.0_rp
     lagrtyp_init % acced(3)       = 0.0_rp
     lagrtyp_init % accee(1)       = 0.0_rp
     lagrtyp_init % accee(2)       = 0.0_rp
     lagrtyp_init % accee(3)       = 0.0_rp
     lagrtyp_init % acceg(1)       = 0.0_rp 
     lagrtyp_init % acceg(2)       = 0.0_rp
     lagrtyp_init % acceg(3)       = 0.0_rp
     lagrtyp_init % stret          = 1.0_rp
     lagrtyp_init % t              = 0.0_rp
     lagrtyp_init % dt_k           = dtime
     lagrtyp_init % dt_km1         = dtime
     lagrtyp_init % dt_km2         = dtime
     lagrtyp_init % dtg            = dtime
     lagrtyp_init % Cd             = 0.0_rp
     lagrtyp_init % Re             = 0.0_rp
     lagrtyp_init % prope(1:mlapr) = parttyp(1) % prope(1:mlapr) 
     !
     ! Solver
     !
     call soldef(-1_ip)                   ! Allocate memory
     solve(1) % kfl_solve = 1             ! Slip wall solver
     solve(1) % wprob     = 'SPLIP_WALL'  ! Name
     solve(1) % kfl_iffix = 3             ! Fix Dirichlet with value given by initial solution

  case( 1_ip )
     !
     ! After reading the data
     !
     !
     ! Number of used types
     !
     ntyla_pts = 0
     number_types_pts = 0
     do itype = 1,mtyla
        if( parttyp(itype) % kfl_exist == 1 ) then
           ntyla_pts = max(ntyla_pts,itype)
           number_types_pts = number_types_pts + 1
        end if
     end do
     !
     ! Check if a slip condition has been imposed
     !
     kfl_slip_wall_pts = 0    
     if( INOTMASTER ) then
        do iboun = 1,nboun
           if( kfl_fixbo_pts(iboun) == PTS_SLIP_CONDITION ) then
              kfl_slip_wall_pts = kfl_slip_wall_pts + 1
           end if
        end do
     end if
     call PAR_SUM(kfl_slip_wall_pts,'IN MY CODE')

  end select

end subroutine pts_inivar
