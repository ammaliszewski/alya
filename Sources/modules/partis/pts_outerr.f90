subroutine pts_outerr()
  !------------------------------------------------------------------------
  !****f* master/pts_outerr
  ! NAME 
  !    pts_outerr
  ! DESCRIPTION
  !    This routine checks if there are errros and warnings
  ! USES
  ! USED BY
  !    Turnon
  !***
  !------------------------------------------------------------------------
  
  use def_master
  use def_kermod
  use def_domain
  use def_partis
  implicit none
  integer(ip)   :: ierro=0,iwarn=0,itype
  character(20) :: messa

  ierro = 0
  iwarn = 0
  !
  ! Particle injection
  !
  do itype = 1,mtyla
     if( parttyp(itype) % kfl_exist /= 0 ) then
        if(  parttyp(itype) % kfl_grafo == 1 .or. &
             parttyp(itype) % kfl_buofo == 1 ) then
           if(  grnor == 0.0_rp .and. gravi(1) == 0.0_rp .and. &
                gravi(2) == 0.0_rp .and. gravi(3) == 0.0_rp ) then
              ierro = ierro + 1
              call outfor(1_ip,lun_outpu,&
                   'GRAVITY IS MISSING FOR PARTICLE INJECTION')                    
           end if
        end if
        if(  parttyp(itype) % kfl_modla /= 1 .and. parttyp(itype) % diame <= 0.0_rp ) then 
           ierro = ierro + 1
           call outfor(1_ip,lun_outpu,&
                'ZERO OR NEGATIVE DIAMETER PARTICLE')  
        end if
     end if
     if( parttyp(itype) % kfl_brown /= 0 ) then
        if( kfl_prope == 0 .and. parttyp(itype) % diffu == 0.0_rp ) then
           ierro = ierro + 1
           call outfor(1_ip,lun_outpu,&
                'FOR BROWNIAN MOTION, VISCOSITY SHOULD BE DEFINED IN KERMOD')
        end if
     end if
  end do
  if( kfl_prope == 0 ) then
     ierro = ierro + 1
     call outfor(1_ip,lun_outpu,&
          'PTS_DOTER: PROPERTIES NOT DEFINED')
  end if
  !
  ! Check if there is a wall distance criterion but distance to the wall does not exist
  !
  !if( dimin_pts > 0.0_rp .and. kfl_walld == 0 ) then
  !   ierro = ierro + 1
  !   call outfor(1_ip,lun_outpu,&
  !        'DISTANCE TO THE WALL SHOULD BE COMPUTED BY KERMOD TO USE WALL-DISTANCE BASED DEPOSITION CRITERION')        
  !end if
  !
  ! Stop
  !
  messa = intost(ierro)
  if( ierro == 1 ) then
     call runend('PTS_OUTERR: '//trim(messa)//' ERROR HAS BEEN FOUND')
  else if( ierro >= 2 ) then
     call runend('PTS_OUTERR: '//trim(messa)//' ERRORS HAVE BEEN FOUND')
  end if

end subroutine pts_outerr
