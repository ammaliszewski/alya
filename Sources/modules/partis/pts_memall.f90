!------------------------------------------------------------------------
!> @addtogroup Partis
!! @{
!> @name    Partis memory
!! @file    pts_memall.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!! @brief   This routine allocate memory for particles
!! @details Allocate memory and initialize particle type
!> @} 
!------------------------------------------------------------------------

subroutine pts_memall()
  use def_parame
  use def_domain
  use def_master 
  use def_kermod
  use def_partis
  use def_solver
  use def_inpout
  use mod_memory
  implicit none  
  integer(ip) :: itype,ivari
  !
  ! Allocate memory: can only have MLAGR living at the same time
  !
  allocate( lagrtyp(mlagr) )
  lagrtyp(1:mlagr) = lagrtyp_init
  !
  ! Number of used types
  !
  ntyla_pts = 0
  do itype = 1,mtyla
     if( parttyp(itype) % kfl_exist == 1 ) ntyla_pts = max(ntyla_pts,itype)
  end do

  if( INOTMASTER ) then
     !
     ! Velocity deformation tensor
     !
     itype_loop: do itype = 1,ntyla_pts
        if( parttyp(itype) % kfl_exist /= 0 .and. parttyp(itype) % kfl_saffm /= 0 ) then
           call memory_alloca(mem_modul(1:2,modul),'DEFOR_PTS','pts_memall',defor_pts,ntens,npoin)                
           exit itype_loop
        end if
     end do itype_loop
     !
     ! Wall element
     !
     call memory_alloca(mem_modul(1:2,modul),'LBOUE_PTS','pts_memall',lboue_pts,nelem)              
     !
     ! Element natural length
     !
     call memory_alloca(mem_modul(1:2,modul),'HLENG_PTS','pts_memall',hleng_pts,nelem)  
     !
     ! Distance to slip boundaries and friction coefficient
     !         
     if( kfl_slip_wall_pts > 0 ) then
        call memory_alloca(mem_modul(1:2,modul),'WALLD_SLIP_PTS','pts_memall',walld_slip_pts,npoin)
        call memory_alloca(mem_modul(1:2,modul),'FRICTION_PTS'  ,'pts_memall',friction_pts,npoin)
     end if
  end if
  !
  ! Residence time
  !
  ivari = 5
  call posdef(25_ip,ivari)
  if( ivari > 0 ) then
     kfl_resid_pts = 1
     if( INOTMASTER ) then
        call memory_alloca(mem_modul(1:2,modul),'RESID_PTS','pts_memall',resid_pts,ntyla_pts,nelem_2) 
     end if
  end if
  !
  ! Deposition
  !
  ivari = 2
  call posdef(25_ip,ivari)
  if( ivari > 0 ) then
     kfl_depos_pts = 1
     if( ntyla_pts > 0 ) then
        if( INOTMASTER ) then
           call memory_alloca(mem_modul(1:2,modul),'DEPOS_PTS','pts_memall',depos_pts,ntyla_pts,npoin)
           call memory_alloca(mem_modul(1:2,modul),'DEPOE_PTS','pts_memall',depoe_pts,ntyla_pts,nelem)
           call memory_alloca(mem_modul(1:2,modul),'LEDEP_PTS','pts_memall',ledep_pts,nelem)
        else               
           call memory_alloca(mem_modul(1:2,modul),'DEPOS_PTS','pts_memall',depos_pts,ntyla_pts,1_ip)         
           call memory_alloca(mem_modul(1:2,modul),'DEPOE_PTS','pts_memall',depoe_pts,ntyla_pts,1_ip)         
           call memory_alloca(mem_modul(1:2,modul),'LEDEP_PTS','pts_memall',ledep_pts,1_ip)
        end if
     end if
  end if
  !
  ! Solver
  !
  solve_sol                => solve
  solve_sol(1) % kfl_fixno => kfl_fixno_walld_slip_pts  
  call soldef(4_ip)

end subroutine pts_memall
