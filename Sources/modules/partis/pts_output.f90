!------------------------------------------------------------------------
!> @addtogroup Partis
!! @{
!> @name    Starts an iteration
!! @file    pts_output.f90
!> @author  Guillaume Houzeaux
!> @date    28/01/2013
!! @brief   Output of particles
!! @details Postprocess on mesh (particle density) and output
!> @} 
!---------------------------F---------------------------------------------

subroutine pts_output()
  use def_parame
  use def_master
  use def_kermod
  use def_partis
  use mod_memory
#ifdef DBPARTICLES
  use, intrinsic :: iso_c_binding
#endif
  implicit none
  
#ifdef DBPARTICLES
!void sendparticles(int npart, int *recvcounts, int nvar2, int kfl_posla_pts, double cutim, double *recvbuf_rp)
  interface
    subroutine sendparticles (npart, recvcounts, nvar2, kfl_posla_pts, cutim, recvbuf_rp) bind ( c )
      use iso_c_binding
      integer ( c_int ), VALUE :: npart
      integer ( c_int ) :: recvcounts(*)
      integer ( c_int ), VALUE :: nvar2
      integer ( c_int ), VALUE :: kfl_posla_pts
      real ( c_double ), VALUE :: cutim      
      real ( c_double ) :: recvbuf_rp(*)
    end subroutine sendparticles
  end interface  
  integer ( c_int ), parameter :: ll = 3
#endif  
  
  integer(ip) :: ivari,ivarp,nvar2,ilagr,ipars,ipart,sendsize,jpart
  integer(ip) :: kfl_ifpos,kfl_ifdep,iprop,recvsize,ilimi
  integer(4)  :: nvar2_4
  
  
  !----------------------------------------------------------------------
  !
  ! Mesh dependent postprocess
  !
  !----------------------------------------------------------------------

  do ivarp = 1,nvarp
     ivari = ivarp
     call posdef(11_ip,ivari)
     call pts_outvar(ivari)
  end do

  !----------------------------------------------------------------------
  !
  ! Particles output
  !
  !----------------------------------------------------------------------
  !
  ! NVAR2: Number of variables for postprocess
  !
  if(      kfl_posla_pts == 0 ) then   
     nvar2 = 14
  else if( kfl_posla_pts == 1 ) then
     nvar2 = 16
  else if( kfl_posla_pts == 2 ) then
     nvar2 = 19
  else if( kfl_posla_pts == 3 ) then
     nvar2 = 28
  else if( kfl_posla_pts == 4 ) then 
     nvar2 = 28 + nlapr
  end if
  nvar2_4 = int(nvar2,4)
  !
  ! Check what kind of postprocess 
  ! KFL_ISPOS = 1: All particles info is required
  ! KFL_IFDEP = 1: Deposited particles info is required
  ! Initial solution if this is a restart is not written
  !
  kfl_ifpos = 0
  kfl_ifdep = 0
  if( mod(ittim, kfl_oufre_pts) == 0 )       kfl_ifpos = 1
  if( nlagr_3 > 0 .and. kfl_oudep_pts /= 0 ) kfl_ifdep = 1
  if( ittyp == ITASK_INITIA .and. kfl_rstar == 2 ) then
     kfl_ifpos = 0
     kfl_ifdep = 0  
  end if
  if( kfl_ifpos == 0 .and. kfl_ifdep == 1 ) then
     ilimi = -2    ! Only deposited and vanishing particles information is gathered
  else
     ilimi = -1    ! Existing, deposited and vanishing particles information is gathered
  end if

  if( kfl_ifpos == 1 .or. kfl_ifdep == 1 ) then
     !
     ! MPI_GATHER size of particles info
     !
     if( IPARALL ) then

        nullify(sendbuf_ip)
        nullify(recvbuf_ip)
        nullify(sendbuf_rp)
        nullify(recvbuf_rp)
        nullify(displs)
        nullify(recvcounts)

        call memory_alloca(mem_modul(1:2,modul),'SENDBUF_IP','pts_output',sendbuf_ip,1_ip)
        recvsize  = npart+1
        sendcount = 1
        recvcount = 1
        ! 
        ! SENDBUF_IP(IPART): Number of particles sent by slave IPART to master
        !
        if( INOTMASTER ) then
           sendbuf_ip(1) = 0
           do ilagr = 1,mlagr
              if( lagrtyp(ilagr) % kfl_exist <= ilimi ) then 
                 sendbuf_ip(1) = sendbuf_ip(1) + 1
              end if
           end do
           call memory_alloca(mem_modul(1:2,modul),'RECVBUF_IP','pts_output',recvbuf_ip,1_ip)
        else
           call memory_alloca(mem_modul(1:2,modul),'RECVBUF_IP','pts_output',recvbuf_ip,recvsize)
        end if
        !
        !  MPI_Gather the number of particles
        !
        call Parall(709_ip)
        !
        ! The previous receive buffer is now a RECVCOUNTS
        !
        if( INOTMASTER ) then
           call memory_alloca(mem_modul(1:2,modul),'RECVCOUNTS','pts_output',recvcounts,1_ip)
           sendcount = int(sendbuf_ip(1),4)
        else           
           call memory_alloca(mem_modul(1:2,modul),'RECVCOUNTS','pts_output',recvcounts,recvsize)
        end if
        do ipart = 1,size(recvbuf_ip,1)
           recvcounts(ipart) = int(recvbuf_ip(ipart),4)
        end do
        call memory_deallo(mem_modul(1:2,modul),'RECVBUF_IP','pts_output',recvbuf_ip)
        call memory_deallo(mem_modul(1:2,modul),'SENDBUF_IP','pts_output',sendbuf_ip)
        !
        ! MPI_GATHERV size of particles info
        ! RECVBUF_RP ... All particle info for master (significant only for master/root)
        ! DISPLS ....... Displacement (significant only for master/root) 
        !
        if( IMASTER ) then

           call memory_alloca(mem_modul(1:2,modul),'DISPLS','pts_output',displs,npart+1_ip,'DO_NOT_INITIALIZE')
           recvcounts(1) =  0
           recvcounts(2) =  recvcounts(2) * nvar2_4
           recvsize      =  recvcounts(2)
           sendcount     =  0
           displs(1)     =  0
           displs(2)     =  0
           do ipart = 3,npart+1
              recvcounts(ipart) = recvcounts(ipart) * int(nvar2_4,4)
              displs(ipart)     = displs(ipart-1)   + recvcounts(ipart-1)
              recvsize          = recvsize          + recvcounts(ipart)
           end do
           recvsize = max(recvsize,1_4)
           
           call memory_alloca(mem_modul(1:2,modul),'RECVBUF_RP','pts_output',recvbuf_rp,recvsize,'DO_NOT_INITIALIZE')
           call memory_alloca(mem_modul(1:2,modul),'SENDBUF_RP','pts_output',sendbuf_rp,    1_ip,'DO_NOT_INITIALIZE')

        else if( ISLAVE ) then

           sendcount = sendcount * nvar2_4
           sendsize  = max(sendcount,1_ip)
           call memory_alloca(mem_modul(1:2,modul),'DISPLS'    ,'pts_output',displs,1_ip)
           call memory_alloca(mem_modul(1:2,modul),'RECVBUF_RP','pts_output',recvbuf_rp,1_ip)
           call memory_alloca(mem_modul(1:2,modul),'SENDBUF_RP','pts_output',sendbuf_rp,sendsize,'DO_NOT_INITIALIZE')

           ipars = 0
           do ilagr = 1,mlagr
              if( lagrtyp(ilagr) % kfl_exist <= ilimi ) then 
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % coord(1)           !  1
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % coord(2)           !  2
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % coord(3)           !  3
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % veloc(1)           !  4
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % veloc(2)           !  5
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % veloc(3)           !  6
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % accel(1)           !  7
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % accel(2)           !  8
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % accel(3)           !  9
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = real(lagrtyp(ilagr) % ilagr,rp)     ! 10
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = real(lagrtyp(ilagr) % itype,rp)     ! 11
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = real(lagrtyp(ilagr) % ielem,rp)     ! 12
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = lagrtyp(ilagr) % dt_k               ! 13
                 ipars             = ipars + 1
                 sendbuf_rp(ipars) = real(lagrtyp(ilagr) % kfl_exist,rp) ! 14
                 if( kfl_posla_pts >= 1 ) then
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % Cd              ! 15
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % Re              ! 16
                 end if
                 if( kfl_posla_pts >= 2 ) then
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_k(1)        ! 17
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_k(2)        ! 18
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % v_fluid_k(3)        ! 19
                 end if
                 if( kfl_posla_pts >= 3 ) then
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % acced(1)        ! 20
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % acced(2)        ! 21
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % acced(3)        ! 22
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % accee(1)        ! 23
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % accee(2)        ! 24
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % accee(3)        ! 25
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % acceg(1)        ! 26
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % acceg(2)        ! 27
                    ipars             = ipars + 1
                    sendbuf_rp(ipars) = lagrtyp(ilagr) % acceg(3)        ! 28
                 end if
                 if( kfl_posla_pts >= 4 ) then
                    do iprop=1,nlapr
                       ipars             = ipars + 1
                       sendbuf_rp(ipars) = lagrtyp(ilagr) % prope(iprop) ! 28 + nlapr
                    end do
                 endif
              end if
           end do

        end if
        !
        ! MPI_GATHERV
        !
        call Parall(710_ip)

     end if

     !----------------------------------------------------------------------
     !
     ! Postprocess
     !
     !----------------------------------------------------------------------

#ifdef EVENT_POINTS
     call mpitrace_eventandcounters(500,4)
#endif

     if( kfl_ifpos == 1 .and. IMASTER )  then
     
#ifdef DBPARTICLES
  call sendparticles (npart, recvcounts, nvar2, kfl_posla_pts, cutim, recvbuf_rp)
#endif     
        ipars = 0
        do ipart = 2,npart+1
           jpart = ipart - 1
           do ilagr = 1,recvcounts(ipart) / nvar2
              if( kfl_posla_pts == 0 ) then
                 write(lun_resul_pts,100)                                             &
                      cutim,abs(int(recvbuf_rp(ipars+10))),                           &
                      recvbuf_rp(ipars+1),recvbuf_rp(ipars+2),recvbuf_rp(ipars+3),    &
                      recvbuf_rp(ipars+4),recvbuf_rp(ipars+5),recvbuf_rp(ipars+6),    &
                      int(recvbuf_rp(ipars+11)),jpart
              else if( kfl_posla_pts == 1 ) then
                 write(lun_resul_pts,100)                                             &
                      cutim,abs(int(recvbuf_rp(ipars+10))),                           &
                      recvbuf_rp(ipars+1),recvbuf_rp(ipars+2),recvbuf_rp(ipars+3),    &
                      recvbuf_rp(ipars+4),recvbuf_rp(ipars+5),recvbuf_rp(ipars+6),    &
                      int(recvbuf_rp(ipars+11)),jpart,                                &
                      recvbuf_rp(ipars+7),recvbuf_rp(ipars+8),recvbuf_rp(ipars+9),    &
                      recvbuf_rp(ipars+15),recvbuf_rp(ipars+16)
              else if( kfl_posla_pts == 2 ) then
                 write(lun_resul_pts,100)                                             &
                      cutim,abs(int(recvbuf_rp(ipars+10))),                           &
                      recvbuf_rp(ipars+1),recvbuf_rp(ipars+2),recvbuf_rp(ipars+3),    &
                      recvbuf_rp(ipars+4),recvbuf_rp(ipars+5),recvbuf_rp(ipars+6),    &
                      int(recvbuf_rp(ipars+11)),jpart,                                &
                      recvbuf_rp(ipars+7),recvbuf_rp(ipars+8),recvbuf_rp(ipars+9),    &
                      recvbuf_rp(ipars+15),recvbuf_rp(ipars+16),                      &
                      recvbuf_rp(ipars+17),recvbuf_rp(ipars+18),recvbuf_rp(ipars+19)
              else if( kfl_posla_pts == 3 ) then
                 write(lun_resul_pts,100)                                             &
                      cutim,abs(int(recvbuf_rp(ipars+10))),                           & 
                      recvbuf_rp(ipars+1),recvbuf_rp(ipars+2),recvbuf_rp(ipars+3),    &
                      recvbuf_rp(ipars+4),recvbuf_rp(ipars+5),recvbuf_rp(ipars+6),    &
                      int(recvbuf_rp(ipars+11)),jpart,                                &
                      recvbuf_rp(ipars+7),recvbuf_rp(ipars+8),recvbuf_rp(ipars+9),    &
                      recvbuf_rp(ipars+15),recvbuf_rp(ipars+16),                      &
                      recvbuf_rp(ipars+17),recvbuf_rp(ipars+18),recvbuf_rp(ipars+19), &
                      recvbuf_rp(ipars+20),recvbuf_rp(ipars+21),recvbuf_rp(ipars+22), &
                      recvbuf_rp(ipars+23),recvbuf_rp(ipars+24),recvbuf_rp(ipars+25), &
                      recvbuf_rp(ipars+26),recvbuf_rp(ipars+27),recvbuf_rp(ipars+28)
              else if( kfl_posla_pts == 4 ) then
                 write(lun_resul_pts,100)                                             &
                      cutim,abs(int(recvbuf_rp(ipars+10))),                           & 
                      recvbuf_rp(ipars+1),recvbuf_rp(ipars+2),recvbuf_rp(ipars+3),    &
                      recvbuf_rp(ipars+4),recvbuf_rp(ipars+5),recvbuf_rp(ipars+6),    &
                      int(recvbuf_rp(ipars+11)),jpart,                                &
                      recvbuf_rp(ipars+7),recvbuf_rp(ipars+8),recvbuf_rp(ipars+9),    &
                      recvbuf_rp(ipars+15),recvbuf_rp(ipars+16),                      &
                      recvbuf_rp(ipars+17),recvbuf_rp(ipars+18),recvbuf_rp(ipars+19), &
                      recvbuf_rp(ipars+20),recvbuf_rp(ipars+21),recvbuf_rp(ipars+22), &
                      recvbuf_rp(ipars+23),recvbuf_rp(ipars+24),recvbuf_rp(ipars+25), &
                      recvbuf_rp(ipars+26),recvbuf_rp(ipars+27),recvbuf_rp(ipars+28), &
                      (recvbuf_rp(ipars+28+iprop),iprop=1,nlapr)
              end if
              ipars = ipars + nvar2
           end do
        end do
        call flush(int(lun_resul_pts,4))

     else if( kfl_ifpos == 1 .and. ISEQUEN ) then 

        do ilagr = 1,mlagr
           if( lagrtyp(ilagr) % kfl_exist <= -1 ) then
              if( kfl_posla_pts == 0 ) then
                 write(lun_resul_pts,100)                                                           &
                      cutim,abs(lagrtyp(ilagr) % ilagr),                                            &
                      lagrtyp(ilagr) % coord(1),lagrtyp(ilagr) % coord(2),lagrtyp(ilagr) % coord(3),&
                      lagrtyp(ilagr) % veloc(1),lagrtyp(ilagr) % veloc(2),lagrtyp(ilagr) % veloc(3),&
                      lagrtyp(ilagr) % itype,1_ip
              else if( kfl_posla_pts == 1 ) then
                 write(lun_resul_pts,100)                                                           &
                      cutim,abs(lagrtyp(ilagr) % ilagr),                                            &
                      lagrtyp(ilagr) % coord(1),lagrtyp(ilagr) % coord(2),lagrtyp(ilagr) % coord(3),&
                      lagrtyp(ilagr) % veloc(1),lagrtyp(ilagr) % veloc(2),lagrtyp(ilagr) % veloc(3),&
                      lagrtyp(ilagr) % itype,1_ip,                                                  &
                      lagrtyp(ilagr) % accel(1),lagrtyp(ilagr) % accel(2),lagrtyp(ilagr) % accel(3),&
                      lagrtyp(ilagr) % Cd,lagrtyp(ilagr) % Re                 
              else if( kfl_posla_pts == 2 ) then
                 write(lun_resul_pts,100)                                                           &
                      cutim,abs(lagrtyp(ilagr) % ilagr),                                            &
                      lagrtyp(ilagr) % coord(1),lagrtyp(ilagr) % coord(2),lagrtyp(ilagr) % coord(3),&
                      lagrtyp(ilagr) % veloc(1),lagrtyp(ilagr) % veloc(2),lagrtyp(ilagr) % veloc(3),&
                      lagrtyp(ilagr) % itype,1_ip,                                                  &
                      lagrtyp(ilagr) % accel(1),lagrtyp(ilagr) % accel(2),lagrtyp(ilagr) % accel(3),&
                      lagrtyp(ilagr) % Cd,lagrtyp(ilagr) % Re,                                      &                 
                      lagrtyp(ilagr) % v_fluid_k(1),lagrtyp(ilagr) % v_fluid_k(2),lagrtyp(ilagr) % v_fluid_k(3)
              else if( kfl_posla_pts == 3 ) then
                 write(lun_resul_pts,100) &
                      cutim,abs(lagrtyp(ilagr) % ilagr),                                            &
                      lagrtyp(ilagr) % coord(1),lagrtyp(ilagr) % coord(2),lagrtyp(ilagr) % coord(3),&
                      lagrtyp(ilagr) % veloc(1),lagrtyp(ilagr) % veloc(2),lagrtyp(ilagr) % veloc(3),&
                      lagrtyp(ilagr) % itype,1_ip,                                                  &
                      lagrtyp(ilagr) % accel(1),lagrtyp(ilagr) % accel(2),lagrtyp(ilagr) % accel(3),&
                      lagrtyp(ilagr) % Cd,lagrtyp(ilagr) % Re,                                      &                 
                      lagrtyp(ilagr) % v_fluid_k(1),lagrtyp(ilagr) % v_fluid_k(2),lagrtyp(ilagr) % v_fluid_k(3),&
                      lagrtyp(ilagr) % acced(1),lagrtyp(ilagr) % acced(2),lagrtyp(ilagr) % acced(3),&
                      lagrtyp(ilagr) % accee(1),lagrtyp(ilagr) % accee(2),lagrtyp(ilagr) % accee(3),&
                      lagrtyp(ilagr) % acceg(1),lagrtyp(ilagr) % acceg(2),lagrtyp(ilagr) % acceg(3)
              else if( kfl_posla_pts == 3 ) then
                 write(lun_resul_pts,100) &
                      cutim,abs(lagrtyp(ilagr) % ilagr),                                            &
                      lagrtyp(ilagr) % coord(1),lagrtyp(ilagr) % coord(2),lagrtyp(ilagr) % coord(3),&
                      lagrtyp(ilagr) % veloc(1),lagrtyp(ilagr) % veloc(2),lagrtyp(ilagr) % veloc(3),&
                      lagrtyp(ilagr) % itype,1_ip,                                                  &
                      lagrtyp(ilagr) % accel(1),lagrtyp(ilagr) % accel(2),lagrtyp(ilagr) % accel(3),&
                      lagrtyp(ilagr) % Cd,lagrtyp(ilagr) % Re,                                      &                 
                      lagrtyp(ilagr) % v_fluid_k(1),lagrtyp(ilagr) % v_fluid_k(2),lagrtyp(ilagr) % v_fluid_k(3),&
                      lagrtyp(ilagr) % acced(1),lagrtyp(ilagr) % acced(2),lagrtyp(ilagr) % acced(3),&
                      lagrtyp(ilagr) % accee(1),lagrtyp(ilagr) % accee(2),lagrtyp(ilagr) % accee(3),&
                      lagrtyp(ilagr) % acceg(1),lagrtyp(ilagr) % acceg(2),lagrtyp(ilagr) % acceg(3),&
                      (lagrtyp(ilagr)%prope(iprop), iprop=1,nlapr)
              end if
           end if
        end do
        call flush(lun_resul_pts)
     end if
  end if

  !----------------------------------------------------------------------
  !
  ! Postprocess deposition
  !
  !----------------------------------------------------------------------

  if( kfl_ifdep == 1 ) then
     if( IMASTER ) then
        ipars = 0
        do ipart = 2,npart+1
           do ilagr = 1,recvcounts(ipart) / nvar2
              if( kfl_oudep_pts /= 0 .and. int(recvbuf_rp(ipars+10),ip) < 0 ) then
                 write(lun_oudep_pts,102)                                                   &
                      cutim,abs(int(recvbuf_rp(ipars+10),ip)),int(recvbuf_rp(ipars+11),ip), & 
                      recvbuf_rp(ipars+1),recvbuf_rp(ipars+2),recvbuf_rp(ipars+3),          &
                      int(recvbuf_rp(ipars+14),ip)              
              end if
              ipars = ipars + nvar2
           end do
        end do
        call flush(lun_oudep_pts)
     else if( ISEQUEN ) then 
        do ilagr = 1,mlagr
           if( lagrtyp(ilagr) % kfl_exist <= -2 ) then
              write(lun_oudep_pts,102)                                                            &
                   cutim,abs(lagrtyp(ilagr) % ilagr),lagrtyp(ilagr) % itype,                      &
                   lagrtyp(ilagr) % coord(1),lagrtyp(ilagr) % coord(2),lagrtyp(ilagr) % coord(3), &
                   lagrtyp(ilagr) % kfl_exist
           end if
        end do
        call flush(lun_oudep_pts)
     end if
  end if

#ifdef EVENT_POINTS
  call mpitrace_eventandcounters(500,0)
#endif
  !
  ! Deallocate memory
  !
  if( IPARALL .and. ( kfl_ifdep == 1 .or. kfl_ifpos == 1 ) ) then
     call memory_deallo(mem_modul(1:2,modul),'SENDBUF_RP','pts_output',sendbuf_rp)
     call memory_deallo(mem_modul(1:2,modul),'RECVBUF_RP','pts_output',recvbuf_rp)
     call memory_deallo(mem_modul(1:2,modul),'DISPLS'    ,'pts_output',displs)
     call memory_deallo(mem_modul(1:2,modul),'RECVCOUNTS','pts_output',recvcounts)
  end if
  !
  ! Formats
  !
100 format(e12.6,2x,i7,      6(2x,e12.6),2(2x,i7),40(2x,e12.6))
102 format(e12.6,',',i7,',',i7,3(',',e12.6),',',i2)

end subroutine pts_output
