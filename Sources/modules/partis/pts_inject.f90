!------------------------------------------------------------------------
!> @addtogroup Partis
!> @{
!> @file    pts_injec.f90
!> @author  Guillaume Houzeaux
!> @brief   Partis injection
!> @details Partis injection
!> @} 
!------------------------------------------------------------------------

subroutine pts_inject()

  use def_parame
  use def_master
  use def_kermod
  use def_partis
  use def_domain
  use mod_ker_proper 
  use mod_random
  use mod_elmgeo
  use mod_memory
  use mod_communications, only : PAR_SUM 
  use mod_communications, only : PAR_MAX
  use mod_communications, only : PAR_POINT_TO_POINT_ARRAY_OPERATION
  implicit none
  integer(ip)          :: ii,jj,kk,iinj
  integer(ip)          :: i1,i2,i3,i4,i5,i6,j1,j2,j3
  real(rp)             :: zfact,tfact,rfact
  real(rp)             :: xx(3),xmini,xmaxi,ymini,ymaxi,zmini,zmaxi
  real(rp)             :: x1,y1,z1,x2,y2,z2,x3,y3,z3,nn(3),p1(3),p2(3),p3(3) 
  real(rp)             :: ux,uy,uz,wx,wy,wz,d1,d2,d3
  real(rp)             :: t,z,r,radius,rad,xe,ye,ze
  integer(ip)          :: nside,nlagr_new,nlagr_pos,kfl_injec
  integer(ip)          :: number_injected_pts
  real(rp),    pointer :: particle_position(:,:)
  integer(ip), pointer :: particle_injector(:)
  integer(ip), pointer :: particle_place(:)

  kfl_injec = 0
  if( cutim >= tinla_pts ) then
     cutla_pts = cutla_pts + dtime
     if( cutla_pts >= tpela_pts-zeror ) then
        kfl_injec = 1
        cutla_pts = 0.0_rp
     end if
  end if
  if( kfl_injec == 0 ) return
  !
  ! Nullify pointers
  !
  nullify(particle_position)
  nullify(particle_injector)
  nullify(particle_place)
  !
  ! Number of injected particles when injection
  !
  number_injected_pts = 0

  do iinj = 1,size(kfl_injla_pts)
     if( kfl_injla_pts(iinj) == 1 ) then
        !
        ! Square
        !
        nside = int(parla_pts(iinj,7),ip)
        number_injected_pts = number_injected_pts + nside * nside

     else if( kfl_injla_pts(iinj) == 2 ) then
        !
        ! Sphere injector
        !
        nside = int(parla_pts(iinj,5),ip)
        number_injected_pts = number_injected_pts + nside ** ndime

     else if( kfl_injla_pts(iinj) == 3 ) then
        !
        ! Semi-Sphere injector
        !
        nside = int(parla_pts(iinj,8),ip)
        number_injected_pts = number_injected_pts + 2 * nside ** ndime 

     else if( kfl_injla_pts(iinj) == 4 ) then
        !
        ! Circle injector
        !
        nside = int(parla_pts(iinj,8),ip)
        number_injected_pts = number_injected_pts + nside ** ndime

     else if( kfl_injla_pts(iinj) == 5 ) then
        !
        ! Rectangle injector
        ! 
        if( ndime == 3) then
           nside = int(parla_pts(iinj,10),ip)
           number_injected_pts = number_injected_pts + nside * nside
        end if

     else if( kfl_injla_pts(iinj) == 6 ) then
        !
        ! Pointwise injector
        !
        number_injected_pts = number_injected_pts + 1

     end if

  end do

  if( INOTMASTER ) then

     call memory_alloca(mem_modul(1:2,modul),'PARTICLE_POSITION','pts_injec',particle_position,ndime,number_injected_pts)              
     call memory_alloca(mem_modul(1:2,modul),'PARTICLE_INJECTOR','pts_injec',particle_injector,number_injected_pts)              
     call memory_alloca(mem_modul(1:2,modul),'PARTICLE_INJECTOR','pts_injec',particle_place,number_injected_pts*number_types_pts)              

     nlagr_pos = 0

     do iinj = 1,size(kfl_injla_pts)

        if( kfl_injla_pts(iinj) == 1 ) then

           !----------------------------------------------------------
           !
           ! Square injector given the corners
           !
           !----------------------------------------------------------

           nside = int(parla_pts(iinj,7),ip)
           xmini = parla_pts(iinj,1)
           ymini = parla_pts(iinj,2)
           zmini = parla_pts(iinj,3)
           xmaxi = parla_pts(iinj,4)
           ymaxi = parla_pts(iinj,5)
           zmaxi = parla_pts(iinj,6)     

           if( zmini == zmaxi ) then
              i1 = 1
              i2 = 2
              i3 = 3
           else if( xmini == xmaxi ) then
              i1 = 3
              i2 = 2
              i3 = 1
           else if( ymini == ymaxi ) then
              i1 = 1
              i2 = 3
              i3 = 2
           else
              call runend('LAGRAN: WRONG BOX')
           end if
           i4 = i1 + 3_ip
           i5 = i2 + 3_ip
           i6 = i3 + 3_ip
           j1 = i1
           j2 = i2
           j3 = i3           

           if( xmini == xmaxi .or. ymini == ymaxi .or. zmini == zmaxi ) then


              xx(j3) = parla_pts(iinj,i3)

              do jj = 1,nside

                 if( nside == 1 ) then
                    xx(j2) = parla_pts(iinj,i2)
                 else
                    xx(j2) = real(jj-1,rp)/real(nside-1,rp)*(parla_pts(iinj,i5)-parla_pts(iinj,i2)) + parla_pts(iinj,i2)
                 end if

                 do ii = 1,nside

                    if( nside == 1 ) then
                       xx(j1) = parla_pts(iinj,i1)
                    else
                       xx(j1) = real(ii-1,rp)/real(nside-1,rp)*(parla_pts(iinj,i4)-parla_pts(iinj,i1)) + parla_pts(iinj,i1)
                    end if
                    nlagr_pos                            = nlagr_pos + 1
                    particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
                    particle_injector(nlagr_pos)         = iinj
                 end do
              end do

           end if

        else if( kfl_injla_pts(iinj) == 5 ) then

           !----------------------------------------------------------
           !
           ! Rectangle injector
           !
           !----------------------------------------------------------    

           nside = int(parla_pts(iinj,10),ip)

           if( ndime == 3 ) then

              x1 = parla_pts(iinj,1)
              y1 = parla_pts(iinj,2)
              z1 = parla_pts(iinj,3)
              x2 = parla_pts(iinj,4)
              y2 = parla_pts(iinj,5)
              z2 = parla_pts(iinj,6)
              x3 = parla_pts(iinj,7)
              y3 = parla_pts(iinj,8)
              z3 = parla_pts(iinj,9)

              d1 = sqrt( (x1-x2)**2_ip+(y1-y2)**2_ip+(z1-z2)**2_ip )
              d2 = sqrt( (x3-x2)**2_ip+(y3-y2)**2_ip+(z3-z2)**2_ip )
              d3 = sqrt( (x3-x1)**2_ip+(y3-y1)**2_ip+(z3-z1)**2_ip )

              if( d1-d2 >= 0 .and. d1-d3 >= 0 )then
                 p1(1) = x1
                 p1(2) = y1
                 p1(3) = z1
                 p2(1) = x3
                 p2(2) = y3
                 p2(3) = z3
                 p3(1) = x2
                 p3(2) = y2
                 p3(3) = z2
              else if( d2-d1 >=0 .and. d2-d3 >=0 )then
                 p1(1) = x2
                 p1(2) = y2
                 p1(3) = z2
                 p2(1) = x1
                 p2(2) = y1
                 p2(3) = z1
                 p3(1) = x3
                 p3(2) = y3
                 p3(3) = z3
              else if( d3-d1 >=0 .and. d3-d2 >=0 )then
                 p1(1) = x3
                 p1(2) = y3
                 p1(3) = z3
                 p2(1) = x2
                 p2(2) = y2
                 p2(3) = z2
                 p3(1) = x1
                 p3(2) = y1
                 p3(3) = z1
              end if

              ux = x3-x1
              uy = y3-y1
              uz = z3-z1
              wx = x2-x1
              wy = y2-y1
              wz = z2-z1
              nn(1) = uy*wz - uz*wy
              nn(2) = wx*uz - wz*ux
              nn(3) = ux*wy - wx*uy

              if( nn(1) == 0.0_rp)then
                 if( nn(3) == 0.0_rp )then
                    i1 = 1
                    i2 = 3
                    i3 = 2
                 else
                    i1 = 1
                    i2 = 2
                    i3 = 3
                    if( p1(i1) == p2(i1) .or. p2(i2) == p3(i2) )then
                       i1 = 2
                       i2 = 1
                       i3 = 3
                    end if
                 end if
              else 
                 i1 = 3
                 i2 = 2
                 i3 = 1
                 if( p1(i1) == p2(i1) .or. p2(i2) == p3(i2) )then
                    i1 = 2
                    i2 = 3
                    i3 = 1
                 end if

              end if

              do ii = 1,nside
                 if( nside == 1 ) then
                    xx(i1) = p2(i1)
                 else
                    if( (p2(i1)-p1(i1)) >= 0 )then
                       xx(i1) = real(ii-1,rp)/real(nside-1,rp)*(p2(i1)-p1(i1)) + p1(i1)
                    else
                       xx(i1) = real(ii-1,rp)/real(nside-1,rp)*(p1(i1)-p2(i1)) + p2(i1)
                    end if
                 end if
                 do jj =1,nside
                    if( nside == 1 ) then
                       xx(i2) = p2(i2)
                    else
                       if( (p3(i2)-p2(i2)) >= 0 )then
                          xx(i2) = real(jj-1,rp)/real(nside-1,rp)*(p3(i2)-p2(i2)) + p2(i2)
                       else
                          xx(i2) = real(jj-1,rp)/real(nside-1,rp)*(p2(i2)-p3(i2)) + p3(i2)
                       end if
                    end if
                    xx(i3) = (-(xx(i1)-p1(i1))*nn(i1) - (xx(i2)-p1(i2))*nn(i2))/nn(i3) + p1(i3)

                    nlagr_pos                            = nlagr_pos + 1
                    particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
                    particle_injector(nlagr_pos)         = iinj
                 end do
              end do
           end if

        else if( kfl_injla_pts(iinj) == 4 ) then

           !----------------------------------------------------------
           !
           ! Circle injector
           !
           !----------------------------------------------------------

           nside = int(parla_pts(iinj,8),ip)

           if( ndime == 3 ) then           

              xc    = parla_pts(iinj,1)
              yc    = parla_pts(iinj,2)
              zc    = parla_pts(iinj,3)
              rad   = parla_pts(iinj,4)
              nx    = parla_pts(iinj,5)
              ny    = parla_pts(iinj,6)
              nz    = parla_pts(iinj,7)
              zfact = 2.0_rp / real(nside+1,rp)
              tfact = 2.0_rp * pi / real(nside+1,rp)
              rfact = rad / real(nside,rp)
              do ii = 1,nside
                 z = -1.0_rp + real(ii,rp) * zfact
                 r = sqrt(1.0_rp-z*z)
                 do jj = 1,nside
                    t = real(jj,rp) * tfact
                    do kk = 1,nside
                       radius = real(kk,rp) * rfact
                       xe     = radius * r * cos(t)
                       ye     = radius * r * sin(t)
                       ze     = radius * z 
                       xx(1)  = ny*ze - nz*ye + xc
                       xx(2)  = nz*xe - nx*ze + yc
                       xx(3)  = nx*ye - ny*xe + zc

                       nlagr_pos                            = nlagr_pos + 1
                       particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
                       particle_injector(nlagr_pos)         = iinj
                    end do
                 end do
              end do

           else  

              xc    = parla_pts(iinj,1)
              yc    = parla_pts(iinj,2)
              zc    = parla_pts(iinj,3)
              rad   = parla_pts(iinj,4)
              nx    = parla_pts(iinj,5)
              ny    = parla_pts(iinj,6)
              nz    = parla_pts(iinj,7)
              zfact = 2.0_rp / real(nside+1,rp)
              tfact = 2.0_rp * pi / real(nside+1,rp)
              rfact = rad / real(nside,rp)

              do jj = 1,nside
                 t = real(jj,rp) * tfact
                 do kk = 1,nside
                    radius = real(kk,rp) * rfact
                    xx(1) = radius * cos(t) + xc
                    xx(2) = radius * sin(t) + yc

                    nlagr_pos                            = nlagr_pos + 1
                    particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
                    particle_injector(nlagr_pos)         = iinj
                 end do
              end do

           end if

        else if( kfl_injla_pts(iinj) == 2 ) then

           !----------------------------------------------------------
           !
           ! Sphere injector
           !
           !----------------------------------------------------------

           nside = int(parla_pts(iinj,5),ip)

           if( ndime == 3 ) then

              xc    = parla_pts(iinj,1)
              yc    = parla_pts(iinj,2)
              zc    = parla_pts(iinj,3)
              rad   = parla_pts(iinj,4)

              zfact = 2.0_rp / real(nside+1,rp)
              tfact = 2.0_rp * pi / real(nside+1,rp)
              rfact = rad / real(nside,rp)

              do ii = 1,nside

                 z = -1.0_rp + real(ii,rp) * zfact
                 r = sqrt(1.0_rp-z*z)

                 do jj = 1,nside

                    t = real(jj,rp) * tfact

                    do kk = 1,nside

                       radius = real(kk,rp) * rfact 

                       xx(1)  = radius * r * cos(t) + xc
                       xx(2)  = radius * r * sin(t) + yc
                       xx(3)  = radius * z          + zc

                       nlagr_pos                            = nlagr_pos + 1
                       particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
                       particle_injector(nlagr_pos)         = iinj

                    end do

                 end do
              end do

           else

              xc    = parla_pts(iinj,1)
              yc    = parla_pts(iinj,2)
              zc    = parla_pts(iinj,3)
              rad   = parla_pts(iinj,4)

              zfact = 2.0_rp      / real(nside+1,rp)
              tfact = 2.0_rp * pi / real(nside+1,rp)
              rfact = rad         / real(nside,rp)

              do jj = 1,nside

                 t = real(jj,rp) * tfact

                 do kk = 1,nside

                    radius = real(kk,rp) * rfact 
                    xx(1)  = radius * cos(t) + xc
                    xx(2)  = radius * sin(t) + yc

                    nlagr_pos                            = nlagr_pos + 1
                    particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
                    particle_injector(nlagr_pos)         = iinj

                 end do

              end do

           end if

        else if( kfl_injla_pts(iinj) == 3 ) then

           !----------------------------------------------------------
           !
           ! Semi-Sphere injector
           !
           !----------------------------------------------------------

           nside = int(parla_pts(iinj,8),ip)

           if( ndime == 3 ) then

              xc    = parla_pts(iinj,1)
              yc    = parla_pts(iinj,2)
              zc    = parla_pts(iinj,3)
              rad   = parla_pts(iinj,4)
              nx    = parla_pts(iinj,5)
              ny    = parla_pts(iinj,6)
              nz    = parla_pts(iinj,7)

              zfact = 2.0_rp      / real(nside+1,rp)
              tfact = 2.0_rp * pi / real(nside+1,rp)
              rfact = rad         / real(nside,rp) / 2.0_rp

              do ii = 1,nside

                 z = -1.0_rp + real(ii,rp) * zfact
                 r = sqrt(1.0_rp-z*z)

                 do jj = 1,nside

                    t = real(jj,rp) * tfact

                    do kk = 1,nside*2

                       radius = real(kk,rp) * rfact 

                       xx(1)  = radius * r * cos(t) + xc
                       xx(2)  = radius * r * sin(t) + yc
                       xx(3)  = radius * z          + zc

                       if( (xx(1)-xc)*nx + (xx(2)-yc)*ny + (xx(3)-zc)*nz >= 0.0_rp ) then
                          nlagr_pos                            = nlagr_pos + 1
                          particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
                          particle_injector(nlagr_pos)         = iinj
                       end if

                    end do

                 end do
              end do

           else

              xc    = parla_pts(iinj,1)
              yc    = parla_pts(iinj,2)
              zc    = parla_pts(iinj,3)
              rad   = parla_pts(iinj,4)
              nx    = parla_pts(iinj,5)
              ny    = parla_pts(iinj,6)
              nz    = parla_pts(iinj,7)

              zfact = 2.0_rp / real(nside+1,rp)
              tfact = 2.0_rp * pi / real(nside+1,rp)
              rfact = rad / real(nside,rp) / 2.0_rp

              do jj = 1,nside

                 t = real(jj,rp) * tfact

                 do kk = 1,nside*2

                    radius = real(kk,rp) * rfact 

                    xx(1)  = radius * cos(t) + xc
                    xx(2)  = radius * sin(t) + yc

                    if( (xx(1)-xc)*nx + (xx(2)-yc)*ny >= 0.0_rp ) then
                       nlagr_pos                            = nlagr_pos + 1
                       particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
                       particle_injector(nlagr_pos)         = iinj
                    end if

                 end do

              end do

           end if

        else if( kfl_injla_pts(iinj) == 6 ) then

           !----------------------------------------------------------
           !
           ! Pointwise injector
           !
           !----------------------------------------------------------

           xx(1:3)                              = parla_pts(iinj,1:3)
           nlagr_pos                            = nlagr_pos + 1
           particle_position(1:ndime,nlagr_pos) = xx(1:ndime)
           particle_injector(nlagr_pos)         = iinj
           

        end if
     end do
     !
     ! Loop over particles to find host elements
     ! NLAGR_POS = Number of different particles positions
     ! NUMBER_INJECTED_PTS = Total number of particles (NLAGR_POS*NTYPE)
     !
     !
     number_injected_pts = number_injected_pts * number_types_pts
     call pts_checkp(nlagr_pos,nlagr_new,particle_position,particle_injector,particle_place)

  end if
  !
  ! Check injection. In Parallel, decide who owns the particle
  !  
  if( IPARALL ) then
     call PAR_POINT_TO_POINT_ARRAY_OPERATION(particle_place,'IN MY ZONE','MIN RANK OR NEGATIVE')
     !npari =  number_injected_pts
     !pari1 => particle_place
     !call Parall(707_ip)
     if( INOTMASTER ) then 
        nlagr_new = 0
        do ii = 1,number_injected_pts
           if( particle_place(ii) < 0 ) then
              !
              ! One of my neighbors is in charge of this particle
              !
              jj = -particle_place(ii)
              lagrtyp(jj) % kfl_exist = 0
           else if( particle_place(ii) > 0 ) then
              !
              ! I take this particle
              !
              nlagr_new = nlagr_new + 1
           end if
        end do
     end if
     call PAR_MAX(number_injected_pts,'IN MY CODE')
     call PAR_SUM(nlagr_new,'IN MY CODE')
  end if

  coutp(1) = 'INJECT'
  ioutp(1) = nlagr_new
  coutp(2) = 'LAGRANGIAN PARTICLES OUT OF'
  ioutp(2) = number_injected_pts
  call livinf(-10_ip,' ',nlagr_pos)

  if( nlagr_new < number_injected_pts ) then
     coutp(1) = 'PARTICLES INJECTED OUT OF THE DOMAIN:'
     ioutp(1) = number_injected_pts-nlagr_new
     call livinf(-15_ip,' ',0_ip)
  end if
  !
  ! Actual total number of particles (over all partitions)
  !
  nlacc_pts = nlacc_pts + nlagr_new
  !
  ! Deallocate memory
  !
  call memory_deallo(mem_modul(1:2,modul),'PARTICLE_POSITION','pts_injec',particle_position)              
  call memory_deallo(mem_modul(1:2,modul),'PARTICLE_INJECTOR','pts_injec',particle_injector)              
  call memory_deallo(mem_modul(1:2,modul),'PARTICLE_INJECTOR','pts_injec',particle_place)              

end subroutine pts_inject
