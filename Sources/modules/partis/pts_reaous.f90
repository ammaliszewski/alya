subroutine pts_reaous()
  !------------------------------------------------------------------------
  !****f* Partis/pts_reaous
  ! NAME 
  !    pts_reaous
  ! DESCRIPTION
  !    This routine reads data
  ! OUTPUT
  ! USES
  ! USED BY
  !    Reapro
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_master
  use def_kermod
  use def_inpout
  use def_domain
  use def_partis
  implicit none
  integer(ip) :: dummi

  if( INOTSLAVE ) then

     kfl_posla_pts = 3                                    ! Lagrangian particles: postprocess
     kfl_oudep_pts = 0                                    ! Do not output deposition map
     kfl_oufre_pts = 1                                    ! Output frequency 
     kfl_exacs_pts = 0                                    ! Exact solution

     call ecoute('pts_reaous')
     do while( words(1) /= 'OUTPU' )
        call ecoute('pts_reaous')
     end do
     call ecoute('pts_reaous')

     do while(words(1) /= 'ENDOU' )

        call posdef(2_ip,dummi)

        if( words(1) == 'LEVEL' ) then
           kfl_posla_pts = getint('LEVEL',0_ip,'#POSTPROCESS LEVEL FOR LAGRANGIAN PARTICLE')
        end if
        if( words(1) == 'FREQU' ) then
           kfl_oufre_pts = getint('FREQU',1_ip,'#OUTPUT FREQUENCY FOR PARTICLES')
           kfl_oufre_pts = max(1_ip,kfl_oufre_pts)
        end if

        if( words(1) == 'OUTPU' ) then

           if( words(2) == 'DEPOS') then
              kfl_oudep_pts = 1

           else if(words(2)=='ERROR') then
              kfl_exacs_pts = getint('SOLUT',1_ip,'#Exact solution')

           end if
        end if

        call ecoute('pts_reaous')
     end do

  end if

end subroutine pts_reaous
