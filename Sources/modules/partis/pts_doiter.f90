!------------------------------------------------------------------------
!> @addtogroup Partis
!! @{
!> @name    Partis inner iteration
!! @file    pts_doiter.f90
!> @author  Guillaume Houzeaux
!> @date    28/06/2012
!! @brief   This routine solves a time step
!! @details This routine solves a time step
!> @} 
!------------------------------------------------------------------------

subroutine pts_doiter()
  use def_partis
  implicit none
  call pts_begite()
  call pts_solite()
  call pts_endite()

end subroutine pts_doiter
