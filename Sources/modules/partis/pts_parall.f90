subroutine pts_parall(order)
  !-----------------------------------------------------------------------
  !****f* Parall/pts_parall
  ! NAME
  !    pts_parall
  ! DESCRIPTION
  !    This routine exchange data 
  ! USES
  ! USED BY
  !    Reapro
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_inpout
  use def_partis
  use mod_memchk
  use mod_opebcs
  implicit none
  integer(ip), intent(in) :: order
  integer(ip)             :: ii,ji,dummi,imate,ipala,ifunc,ki,jj,iprop
  integer(4)              :: istat

  if( ISEQUEN ) return

  select case ( order )

  case( 1_ip )

     strre = 'pts_paral'
     strin = 'pts_paral'
     strch = 'pts_paral'

     !-------------------------------------------------------------------
     !
     ! Broadcast data read in *.pts.dat file
     !
     !-------------------------------------------------------------------

     if( IPARALL ) then

        do parii = 1,2 
           npari = 0 
           nparr = 0
           nparc = 0
           !
           ! Physical problem
           !           
           call iexcha(mlagr)
           call rexcha(dtmin_pts)
           call rexcha(dimin_pts)
           call rexcha(mean_free_path_pts)
           !
           ! Type description
           !
           do ii = 1,mtyla
              call iexcha(parttyp(ii) % kfl_exist)
              call iexcha(parttyp(ii) % kfl_modla)
              call iexcha(parttyp(ii) % kfl_grafo)
              call iexcha(parttyp(ii) % kfl_buofo)
              call iexcha(parttyp(ii) % kfl_drafo)
              call iexcha(parttyp(ii) % kfl_extfo)
              call iexcha(parttyp(ii) % kfl_brown)
              call iexcha(parttyp(ii) % kfl_saffm)
              call iexcha(parttyp(ii) % kfl_analy)
              call rexcha(parttyp(ii) % denpa)
              call rexcha(parttyp(ii) % spher)
              call rexcha(parttyp(ii) % diame)
              call rexcha(parttyp(ii) % calor)
              call rexcha(parttyp(ii) % emisi)
              call rexcha(parttyp(ii) % scatt)
              call rexcha(parttyp(ii) % diffu)
              do iprop = 1,mlapr
                 call rexcha(parttyp(ii) % prope(iprop))
              end do
              do iprop = 1,mlapr
                 call iexcha(parttyp(ii) % prova(iprop))
              end do
           end do
           !
           ! Numerical problem
           !                  
           call iexcha(kfl_adapt_pts)
           call iexcha(kfl_usbin_pts)
           call rexcha(gamma_pts)
           call rexcha(beta_pts)
           call rexcha(chale_pts)
           call rexcha(safet_pts)
           solve_sol => solve(1:1)
           call soldef(1_ip)
           !
           ! Output and postprocess
           !                  
           call posdef(1_ip,dummi)
           call iexcha(kfl_posla_pts)
           call iexcha(kfl_oudep_pts)
           call iexcha(kfl_oufre_pts)
           call iexcha(kfl_exacs_pts)
           !
           ! Boundary conditions
           !
           do ii = 1,pts_minj                  
              call iexcha(kfl_injla_pts(ii))
           end do
           call iexcha(kfl_injve_pts)
           call rexcha(tinla_pts)
           call rexcha(tpela_pts)
           do ii = 1,mpala
              call rexcha(parla2_pts(ii))
           end do
           do ii = 1,pts_minj
              do jj = 1,mpala
                 call rexcha(parla_pts(ii,jj))
              end do
           end do

           if( parii == 1 ) then
              allocate(parin(npari),stat=istat)
              call memchk(zero,istat,mem_modul(1:2,modul),'PARIN','pts_parall',parin)
              allocate(parre(nparr),stat=istat)
              call memchk(zero,istat,mem_modul(1:2,modul),'PARRE','pts_parall',parre)
              if(ISLAVE) call Parall(2_ip)
           end if
        end do
        if( IMASTER ) call Parall(2_ip)

        call memchk(two,istat,mem_modul(1:2,modul),'PARIN','pts_parall',parin)
        deallocate(parin,stat=istat)
        if(istat/=0) call memerr(two,'PARIN','pts_parall',0_ip)
        call memchk(two,istat,mem_modul(1:2,modul),'PARRE','pts_parall',parre)
        deallocate(parre,stat=istat)
        if(istat/=0) call memerr(two,'PARRE','ns_sendat',0_ip)

     end if

  end select

  npari = 0
  nparr = 0
  nparc = 0
  !
  ! Boundary conditions
  !
  call spbbcs(tbcod_pts)

end subroutine pts_parall

