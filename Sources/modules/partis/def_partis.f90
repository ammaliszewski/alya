module def_partis
  !------------------------------------------------------------------------
  !****f* Partis/def_partis
  ! NAME 
  !    def_partis
  ! DESCRIPTION
  !    Heading for the Partis routines
  ! USES
  ! USED BY
  !    Almost all
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_master, only : mlapr
  use def_master, only : latyp

  !------------------------------------------------------------------------
  ! Parameters
  !------------------------------------------------------------------------

  integer(ip), parameter       :: &
       PTS_OUTFLOW_CONDITION = 0, &
       PTS_WALL_CONDITION    = 1, &
       PTS_SLIP_CONDITION    = 3

  integer(ip), parameter   :: &
       mpala     = 20,        & ! Max # parameters for lagrangian particles
       pts_minj  = 5            ! Max # injections

  integer(ip), parameter   :: &
       lun_resul_pts = 1725,  & ! Result file unit
       lun_oudep_pts = 1727     ! Deposition map file unit

  character(150)           :: &
       fil_resul_pts,         & ! Result file 
       fil_oudep_pts            ! Deposition map file

  real(rp),    parameter   :: &
       zepts = epsilon(1.0_rp)   ! Zero

  !------------------------------------------------------------------------
  ! Physical problem: read in pts_reaphy
  !------------------------------------------------------------------------
!--BEGIN REA GROUP

  real(rp)                 :: &
       dtmin_pts,             &      ! Minimum time step 
       dimin_pts,             &      ! Minimum distance to wall under which particle is deposited
       mean_free_path_pts            ! Mean free path of medium

  !------------------------------------------------------------------------
  ! Numerical problem: read in pts_reanut
  !------------------------------------------------------------------------

  integer(ip)              :: &
       kfl_adapt_pts,         &      ! Adaptive time step
       kfl_usbin_pts                 ! Use element bin for element search
  real(rp)                 :: & 
       gamma_pts,             &      ! Lagrangian particles: Initial time of injection
       beta_pts,              &      ! Lagrangian particles: Time period of injection
       chale_pts,             &      ! Characteristic length for adaptive time step
       safet_pts                     ! Safety factor

  !------------------------------------------------------------------------
  ! Output and Postprocess: read in pts_reaous
  !------------------------------------------------------------------------

  integer(ip)              :: &     
       kfl_posla_pts,         &      ! Lagrangian particles: Postprocess 
       kfl_oudep_pts,         &      ! Output of deposition map
       kfl_oufre_pts,         &      ! Output frequency
       kfl_exacs_pts                 ! Exact solution

  !------------------------------------------------------------------------
  ! Physical problem: read in pts_reabcs
  !------------------------------------------------------------------------

  integer(ip)                   :: &  
       kfl_injla_pts(pts_minj),    &      ! Lagrangian particles: Injection model
       kfl_injve_pts                      ! Lagrangian particles: Velocity injection model
  real(rp)                      :: & 
       tinla_pts,                  &      ! Lagrangian particles: Initial time of injection
       tpela_pts,                  &      ! Lagrangian particles: Time period of injection
       parla_pts(pts_minj,mpala),  &      ! Parameters for the injection
       parla2_pts(mpala)
  integer(ip),   pointer  ::  &
       kfl_fixbo_pts(:)              ! Element boundary fixity     
  real(rp),      pointer  ::  &
       bvnat_pts(:,:)                ! Natural bc values
  type(bc_bound),pointer  :: &     
       tbcod_pts(:)                  ! Boundary code type

!--END REA GROUP

  !------------------------------------------------------------------------
  ! Others
  !------------------------------------------------------------------------

  integer(ip)              :: &
       nlacc_pts,             &      ! Number of accumulated particles
       kfl_depos_pts,         &      ! Postprocess deposition map
       kfl_resid_pts,         &      ! Postprocess residence time
       nlagr_1,               &      ! # total number of existing particles in all subdomains
       nlagr_2,               &      ! # particles going from one subdomain to another
       nlagr_3,               &      ! # particles going out of the computational domain
       nlagr_4,               &      ! # particles that disappear because of zero time step
       ntyla_pts,             &      ! # Number of effective particle types (max type used)
       number_types_pts,      &      ! # Number of particle types
       ninj_pts,              &      !
       kfl_slip_wall_pts             ! If there are slip walls
  real(rp)                 :: &   
       cutla_pts,             &      ! Current injection time
       xc,yc,zc,nx,ny,nz,     &      ! Center of the injected particles
       vv, sigma                     ! Velocity and sigma of injector
  logical(lg), pointer     :: &
       ledep_pts(:),          &      ! List of deposited elements
       nlagr_5(:)
  integer(ip), pointer     :: &
       lboue_pts(:),          &      ! List of element touching boundaries
       kfl_fixno_walld_slip_pts(:,:) ! Fixity wall distance to slip boundaries
  real(rp),    pointer     :: &
       depos_pts(:,:),        &      ! Deposition map on mesh over nodes
       resid_pts(:,:),        &      ! Residence time
       depoe_pts(:,:),        &      ! Deposition map over nodes
       defor_pts(:,:),        &      ! Velocity deformation tensor
       hleng_pts(:),          &      ! Element characteristic length
       bouno_pts(:,:),        &      ! Boundary outwards normal
       walld_slip_pts(:),     &      ! Wall distance to slip boundaries
       friction_pts(:)               ! Friction coefficient
  type(i1p),   pointer     :: &
       leleboun_pts(:)               ! List of element boundaries
  type(latyp)              :: &
       lagrtyp_init                  ! Initialization particle
  integer(ip)              :: &
       particles_sent,        &      ! Number of sent particles
       particles_recv,        &      ! Number of received particles
       comm_loops_pts                ! Number of communicaiton loops

end module def_partis
