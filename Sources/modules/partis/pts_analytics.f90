subroutine pts_analytics(ielem, dt, coord_p, coord_kp1)
  
    use def_domain
    use def_kermod
    use def_master
    use mod_elmgeo
    use def_kintyp, only : ip,rp
    implicit none
    !real(rp),    intent(in)  :: coord(*)
    !real(rp),    intent(in)  :: lnods(*)
    real(rp),    intent(in)  :: dt
    !integer(ip), intent(in)  :: ndime
    integer(ip), intent(in)  :: ielem 
    real(rp) :: idime
    real(rp) :: detm
    real(rp) :: node1(ndime), node2(ndime), node3(ndime), node4(ndime)
    real(rp) :: veloc1(ndime), veloc2(ndime), veloc3(ndime), veloc4(ndime)
    real(rp), intent(in) :: coord_p(ndime)
    real(rp), intent(out) :: coord_kp1(ndime)
    real(rp) :: a_s, b_s, c_s, a_t, b_t, c_t, A_x, A_y, B_x, B_y, C_x, C_y
    real(rp) :: landa_p, landa_m, sq
    !print *, 'x1,y1', coord(1:ndime, lnods(1, ielem))
    !print *, 'x2,y2', coord(1:ndime, lnods(2, ielem))
    !print *, 'x3,y3', coord(1:ndime, lnods(3, ielem))
    
    node1(1:ndime) = coord(1:ndime, lnods(1, ielem))
    node2(1:ndime) = coord(1:ndime, lnods(2, ielem)) 
    node3(1:ndime) = coord(1:ndime, lnods(3, ielem))
    if (ndime == 3) then
      node4(1:ndime) = coord(1:ndime, lnods(4, ielem))
    end if

    veloc1(1:ndime) = advec(1:ndime, lnods(1, ielem),1)
    veloc2(1:ndime) = advec(1:ndime, lnods(2, ielem),1)
    veloc3(1:ndime) = advec(1:ndime, lnods(3, ielem),1)
    if (ndime == 3) then
       veloc4(1:ndime) = advec(1:ndime, lnods(4, ielem),1)
    end if
    !print *, 'veloc1', veloc1(1:ndime)
    !print *, 'veloc2', veloc2(1:ndime)
    !print *, 'veloc3', veloc3(1:ndime)
    ! Calculate the main Matrix determinant
    if (ndime == 2) then
      detm = (node2(1) - node1(1)) * (node3(2) - node1(2)) &
           - (node3(1) - node1(1)) * (node2(2) - node1(2))
      print *, 'detm',  detm
  
    else if (ndime == 3) then
      detm = (node2(1) - node1(1)) * (node3(2) - node1(2)) * (node4(3) - node1(3)) &
           + (node3(1) - node1(1)) * (node4(2) - node1(2)) * (node2(3) - node1(3)) &
           + (node4(1) - node1(1)) * (node2(2) - node1(2)) * (node3(3) - node1(3)) &
           - (node4(1) - node1(1)) * (node3(2) - node1(2)) * (node2(3) - node1(3)) &
           - (node3(1) - node1(1)) * (node2(2) - node1(2)) * (node4(3) - node1(3)) &
           - (node2(1) - node1(1)) * (node4(2) - node1(2)) * (node3(3) - node1(3))
    end if
    ! s (= N_2) = a_s*x + b_s*y + c_s
    ! t (= N_3) = a_t*x + b_t*y + c_t
    a_s = (node3(2) - node1(2))/detm
    b_s = - (node3(1) - node1(1))/detm
    c_s = (node1(2) * (node3(1) - node1(1)) - node1(1) * (node3(2) - node1(2)))/detm
    a_t = - (node2(2) - node1(2))/detm
    b_t = (node2(1) - node1(1))/detm
    c_t = (node1(1) * (node1(2) - node1(2)) - node1(2) * (node2(1) - node1(1)))/detm
    print *, 'a_s, b_s, c_s, a_t, b_t, c_t', a_s, b_s, c_s, a_t, b_t, c_t
    A_x = - (a_s + a_t) * veloc1(1) + a_s * veloc2(1) + a_t * veloc3(1)
    A_y = - (a_s + a_t) * veloc1(2) + a_s * veloc2(2) + a_t * veloc3(2)
    B_x = - (b_s + b_t) * veloc1(1) + b_s * veloc2(1) + b_t * veloc3(1)
    B_y = - (b_s + b_t) * veloc1(2) + b_s * veloc2(2) + b_t * veloc3(2)
    C_x = (1.0_rp - c_s - c_t) * veloc1(1) + c_s * veloc2(1) + c_t * veloc3(1)
    C_y = (1.0_rp - c_s - c_t) * veloc1(2) + c_s * veloc2(2) + c_t * veloc3(2)
    print *, 'A_x, A_y, B_x, B_y, C_x, C_y', A_x, A_y, B_x, B_y, C_x, C_y
    ! Eigenvalues
    sq = 4.0_rp * A_y * B_x + (A_x - B_y)**2
    if (sq < 0) then
        print *, 'raiz negativa', sq
        sq = -1.0_rp*sq
        print *, 'nueva raiz', sq
    end if
    landa_p = 0.5_rp * (A_x + B_y + sqrt(sq))
    landa_m = 0.5_rp * (A_x + B_y - sqrt(sq))
    coord_kp1(1) = exp(dt * landa_m) * coord_p(1) + (C_x / landa_m) * (exp(dt * landa_m) - 1)
    coord_kp1(2) = exp(dt * landa_p) * coord_p(2) + (C_y / landa_p) * (exp(dt * landa_p) - 1)

end subroutine pts_analytics
