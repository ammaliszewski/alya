!-----------------------------------------------------------------------
!> @addtogroup Partis
!> @{
!> @file    pts_waldis.f90
!> @author  Guillaume Houzeaux
!> @date    16/11/1966   
!> @brief   Wall distance
!> @details Wall distance only for first nodes
!> @} 
!-----------------------------------------------------------------------
subroutine pts_waldis()
  use def_kintyp,        only : ip,rp,i1p
  use def_master
  use def_domain
  use def_kermod
  use def_partis
  use mod_kdtree
  use mod_memory
  use mod_maths,          only : maths_heap_sort
  use mod_graphs,         only : graphs_elepoi
  use mod_graphs,         only : graphs_dealep
  use mod_communications, only : PAR_INTERFACE_NODE_EXCHANGE
  use mod_communications, only : PAR_GHOST_BOUNDARY_EXCHANGE
  use mod_communications, only : PAR_SUM
  use mod_ADR,            only : ADR_assemble_laplacian
  use mod_solver,         only : solver_solve
  use mod_gradie,         only : gradie
  use mod_projec,         only : projec_boundaries_to_nodes
  implicit none
  integer(ip)             :: iboun,pblty,pnodb
  integer(ip)             :: ielem,inode,ipoin
  integer(ip)             :: jelem,iz,kboun,pnode
  integer(ip)             :: lelem_face(100)
  real(rp)                :: baloc(ndime,ndime),eucta
  real(rp)                :: bocod(ndime,mnodb),fact1
  real(rp)                :: elcod(ndime,mnode),fact2
  type(i1p),    pointer   :: lelem_boun_2(:)
  integer(ip),  pointer   :: nelem_boun_2(:)
  logical(lg),  pointer   :: boundary_mask(:)

  nullify(nelem_boun_2)
  nullify(lelem_boun_2)
  nullify(boundary_mask)

  if( INOTMASTER ) then
     !
     ! Mark node in wall elements
     !
     do ielem = 1,nelem
        do inode = 1,lnnod(ielem)
           ipoin = lnods(inode,ielem)
           if( lpoty(ipoin) /= 0 ) lboue_pts(ielem) = 1
        end do
     end do
     do iboun = 1,nboun
        if( kfl_fixbo_pts(iboun) == PTS_SLIP_CONDITION ) then
           ielem = lboel(lnnob(iboun)+1,iboun)
           lboue_pts(ielem) = PTS_SLIP_CONDITION
        end if
     end do     
     !
     ! List of boundaries connected to boundary elements (LBOUE_PTS)
     !    
     call memory_alloca(mem_modul(1:2,modul),'LELEBOUN_PTS','pts_waldis',leleboun_pts,nelem)     
     call memory_alloca(mem_modul(1:2,modul),'NELEM_BOUN_2','pts_waldis',nelem_boun_2,nelem_2)
     call memory_alloca(mem_modul(1:2,modul),'LELEM_BOUN_2','pts_waldis',lelem_boun_2,nelem_2)
     !
     ! Boundaries connected to neigboring elements using LBOEL
     !
     do iboun = 1,nboun_2
        pnodb = lnnob(iboun)
        ielem = lboel(pnodb+1,iboun)
        nelem_boun_2(ielem) = nelem_boun_2(ielem) + 1
     end do
     do ielem = 1,nelem_2
        call memory_alloca(mem_modul(1:2,modul),'LELEM_BOUN_2','pts_waldis',lelem_boun_2(ielem) % l,nelem_boun_2(ielem))
        nelem_boun_2(ielem) = 0
     end do
     do iboun = 1,nboun_2
        pnodb = lnnob(iboun)
        ielem = lboel(pnodb+1,iboun)
        nelem_boun_2(ielem) = nelem_boun_2(ielem) + 1
        lelem_boun_2(ielem) % l(nelem_boun_2(ielem)) = iboun 
     end do

     do ielem = 1,nelem
        if( lboue_pts(ielem) > 0 ) then
           iboun = 0
           iz    = pelel_2(ielem)-1
           do while( iz <= pelel_2(ielem+1)-1 )
              if( iz == pelel_2(ielem)-1 ) then
                 jelem = ielem
              else
                 jelem = lelel_2(iz)          
              end if
              do kboun = 1,nelem_boun_2(jelem)
                 iboun             = iboun + 1
                 lelem_face(iboun) = lelem_boun_2(jelem) % l(kboun)
              end do
              iz = iz + 1
           end do
           if( iboun > 0 ) then
              call maths_heap_sort(1_ip,iboun,lelem_face,'ELIMINATE DUPLICATES')
              call memory_alloca(mem_modul(1:2,modul),'LELEBOUN_PTS','pts_waldis',leleboun_pts(ielem) % l,iboun)
              do kboun = 1,iboun
                 leleboun_pts(ielem) % l(kboun) = lelem_face(kboun)
              end do
           end if
        end if
     end do

     call memory_deallo(mem_modul(1:2,modul),'NELEM_BOUN_2','pts_waldis',nelem_boun_2)
     call memory_deallo(mem_modul(1:2,modul),'LELEM_BOUN_2','pts_waldis',lelem_boun_2)
     !
     ! Boundary normals
     !
     call memory_alloca(mem_modul(1:2,modul),'BOUNO_PTS','pts_waldis',bouno_pts,ndime,nboun_2)               
     do iboun = 1,nboun
        pnodb = lnnob(iboun)
        pblty = abs(ltypb(iboun))
        ielem = lboel(pnodb+1,iboun)
        pnode = lnnod(ielem)
        bocod(1:ndime,1:pnodb) = coord(1:ndime,lnodb(1:pnodb,iboun))
        elcod(1:ndime,1:pnode) = coord(1:ndime,lnods(1:pnode,ielem))
        call bouder(&
             pnodb,ndime,ndimb,elmar(pblty) % dercg,&
             bocod,baloc,eucta)
        call chenor(pnode,baloc,bocod,elcod) 
        bouno_pts(1:ndime,iboun) = baloc(1:ndime,ndime)
     end do
     !
     ! Exchange boundary codes and normals
     !
     call PAR_GHOST_BOUNDARY_EXCHANGE(kfl_fixbo_pts,'SUBSTITUTE','IN MY CODE')
     call PAR_GHOST_BOUNDARY_EXCHANGE(bvnat_pts    ,'SUBSTITUTE','IN MY CODE')
     call PAR_GHOST_BOUNDARY_EXCHANGE(bouno_pts,    'SUBSTITUTE','IN MY CODE')
     
  end if

  !----------------------------------------------------------------------
  !
  ! Slip wall distance
  !
  !    Compute the generalized distance to the wall via a Poisson equation:
  !    1. Solve Lapl(f) = -1, with f = 0 on wall
  !    2. d = sqrt[ grad(f)^2 +2*f ] - sqrt[grad(f)^2]
  !    See the following references:
  !    P.G. Tucker, Differential equation-based wall distance computation for
  !         DES and RANS, J. Comp. Phys. 190 (2003) 229-248.
  !    P.G. Tucker, Int. J. Numer. Fluids 33 (2000) 869.
  !    P.G. Tucker, Appl. Math. Model. 22 (1998) 293.
  !
  !----------------------------------------------------------------------

  if( kfl_slip_wall_pts > 0 ) then
     do ipoin = 1,npoin
        walld_slip_pts(ipoin) =  1.0_rp
     end do 
     call ADR_assemble_laplacian(meshe(ndivi),elmar,amatr,walld_slip_pts,rhsid) 
     do ipoin = 1,npoin
        walld_slip_pts(ipoin) =  0.0_rp
        unkno(ipoin)          =  0.0_rp
     end do 
     call solver_solve(momod(modul) % solve,amatr,rhsid,unkno)    
     if( INOTMASTER ) then
        call memgen(0_ip,ndime,npoin)
        call gradie(unkno,gevec)
        do ipoin = 1,npoin
           fact1 = dot_product(gevec(1:ndime,ipoin),gevec(1:ndime,ipoin))
           fact2 = fact1 + 2.0_rp * max(unkno(ipoin),0.0_rp)
           walld_slip_pts(ipoin) = sqrt(fact2) - sqrt(fact1)  
        end do
        call memgen(2_ip,ndime,npoin)
     end if
  end if 

  !----------------------------------------------------------------------
  !
  ! Compute friction, use same solver as previous one
  !
  !----------------------------------------------------------------------

  if( kfl_slip_wall_pts > 0 ) then
     if( INOTMASTER ) then
        call memory_alloca(mem_modul(1:2,modul),'BOUNDARY_MASK','pts_waldis',boundary_mask,nboun)
        do iboun = 1,nboun
           if( kfl_fixbo_pts(iboun) == PTS_SLIP_CONDITION ) boundary_mask(iboun) = .true.        
        end do
        call projec_boundaries_to_nodes(bvnat_pts,meshe(ndivi),elmar,friction_pts,boundary_mask)   
        do ipoin = 1,npoin
           rhsid(ipoin) = 0.0_rp
        end do
        call ADR_assemble_laplacian(meshe(ndivi),elmar,amatr) 
     end if
     call solver_solve(momod(modul) % solve,amatr,rhsid,friction_pts)     
     call memory_deallo(mem_modul(1:2,modul),'BOUNDAY_MASK','pts_waldis',boundary_mask)
  end if

end subroutine pts_waldis
