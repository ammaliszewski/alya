subroutine chm_elmpre(&
     pnode,pgaus,elcon,elden,elvel,elDik,elmas,elmut,elkey,eleps,elrrt,elsen,gpsha,gpcar,gplap,&
     gpcon,gpvel,gpDik,gpgDk,gpmas,elmol,gpmol,gpgmo,gphmo,gpfar,&
     gpvol,gpdiv,gpdis,gpprd,gpspe,gpthi,sgsef,gpfac,gprrt,gpsen,gptur) 
  
  !-----------------------------------------------------------------------
  !****f* partis/chm_elmpre
  ! NAME 
  !    chm_elmpre
  ! DESCRIPTION
  !    Compute quantities at Gauss points
  ! USES
  ! USED BY
  !    chm_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp
  use def_domain, only      :  ndime,mnode,mgaus
  use def_chemic, only      :  nspec_chm,kfl_diffu_chm,kfl_tfles_chm,kfl_model_chm, &
                               diffu_chm,kfl_cotur_chm,kfl_tucfi_chm
  use def_master, only      :  speci,kfl_paral
  implicit none
  integer(ip),  intent(in)  :: pnode,pgaus
  real(rp),     intent(in)  :: elcon(pnode,nspec_chm)
  real(rp),     intent(in)  :: elden(pnode)
  real(rp),     intent(in)  :: elvel(ndime,pnode)
  real(rp),     intent(in)  :: elDik(pnode,nspec_chm)
  real(rp),     intent(in)  :: elmas(pnode,nspec_chm)
  real(rp),     intent(in)  :: elmut(pnode)
  real(rp),     intent(in)  :: elkey(pnode)
  real(rp),     intent(in)  :: eleps(pnode)
  real(rp),     intent(in)  :: elrrt(pnode)
  real(rp),     intent(in)  :: elsen(pnode)
  real(rp),     intent(in)  :: gpsha(pnode,pgaus)
  real(rp),     intent(in)  :: gpcar(ndime,mnode,pgaus)
  real(rp),     intent(in)  :: gplap(mgaus,mnode)                  
  real(rp),     intent(in)  :: gpvol(mgaus)
  real(rp),     intent(out) :: gpcon(pgaus,nspec_chm)
  real(rp),     intent(out) :: gpvel(ndime,pgaus)
  real(rp),     intent(out) :: gpDik(pgaus,nspec_chm)        ! Difussion coefficients
  real(rp),     intent(out) :: gpgDk(ndime,pgaus,nspec_chm)  ! Gradient of difussion coeffs
  real(rp),     intent(out) :: gpmas(pgaus,nspec_chm)        ! Mass source term
  real(rp),     intent(in)  :: elmol(pnode)
  real(rp),     intent(out) :: gpmol(pgaus)
  real(rp),     intent(out) :: gpgmo(ndime,pgaus)
  real(rp),     intent(out) :: gphmo(pgaus)
  real(rp),     intent(out) :: gpfar(mgaus)
  real(rp),     intent(out) :: gpdiv(pgaus)                   ! Velocity divergence
  real(rp),     intent(out) :: gpdis(mgaus)                   ! Dissipation rate for CFI model
  real(rp),     intent(out) :: gpprd(mgaus,nspec_chm)         ! Production term of the variance of c and f in the CFI model
  real(rp),     intent(out) :: gpspe(mgaus)                   ! Flame speed at gauss points
  real(rp),     intent(out) :: gpthi(mgaus)                   ! Flame thickness at gauss points
  real(rp),     intent(out) :: sgsef(mgaus)                   ! Efficiency function for the wrinkling of the flame front
  real(rp),     intent(out) :: gprrt(mgaus)                   ! {w_c * c} - {w_c}*{c}for CFI-LES model
  real(rp),     intent(out) :: gpfac(mgaus)                   ! Dynamic thickening factor F(c) for DTFLES 
  real(rp),     intent(out) :: gpsen(mgaus)                   ! Flame sensor for DTFLES
  real(rp),     intent(out) :: gptur(mgaus)                   ! Turbulence viscosity at gauss points
  real(rp)                  :: gpkey(mgaus)                   ! TKE from K-EPS or K-OMEGA models (RANS)
  real(rp)                  :: gpeps(mgaus)                   ! Dissipation from K-EPS or K-OMEGA models (RANS)
  real(rp)                  :: gpden(mgaus)                   ! Density at gauss points
  real(rp)                  :: aux,vol,vol2,rvol2,new
  integer(ip)               :: igaus,ispec,inode,idime,ifuel,ioxo2,ioxn2,icoun,ncoun

  !
  ! Various initializations
  !
  do ispec = 1,nspec_chm
     do igaus = 1,pgaus
        gpcon(igaus,ispec) = 0.0_rp
        gpDik(igaus,ispec) = 0.0_rp
        gpmas(igaus,ispec) = 0.0_rp
        do idime = 1,ndime
           gpgDk(idime,igaus,ispec)=0.0_rp
        enddo
     end do
  end do
  do igaus = 1,pgaus
     gpfar(igaus) = 0.0_rp
     gpspe(igaus) = 0.0_rp
     gpthi(igaus) = 0.0_rp
     gptur(igaus) = 0.0_rp
     sgsef(igaus) = 0.0_rp
     gpfac(igaus) = 0.0_rp
     gpsen(igaus) = 0.0_rp
     gpden(igaus) = 0.0_rp
     gprrt(igaus) = 0.0_rp
     do idime = 1,ndime
        gpvel(idime,igaus) = 0.0_rp
     enddo
  end do
  !
  ! Concentration
  !
  do ispec = 1,nspec_chm
     do igaus = 1,pgaus
        do inode = 1,pnode
           gpcon(igaus,ispec) = gpcon(igaus,ispec)&
                              + gpsha(inode,igaus) * elcon(inode,ispec)
        end do
     end do
  end do
  !
  ! Fluid velocity
  !
  do igaus = 1, pgaus
     do inode = 1,pnode
        do idime = 1,ndime
           gpvel(idime,igaus) = gpvel(idime,igaus) &
                +  gpsha(inode,igaus) * elvel(idime,inode) 
        end do
     enddo
  enddo
  !
  ! Fluid velocity divergence
  !
  do igaus = 1,pgaus
     do inode = 1,pnode
        do idime = 1,ndime
           gpdiv(igaus) = gpdiv(igaus) + gpcar(idime,inode,igaus) * elvel(idime,inode)
        end do
     end do
  end do
  !
  ! Mass source terms
  !
  do ispec = 1,nspec_chm
     do igaus = 1,pgaus
        do inode = 1,pnode
           gpmas(igaus,ispec) = gpmas(igaus,ispec) + gpsha(inode,igaus) * elmas(inode,ispec)
        enddo
     enddo
  enddo
  !
  ! turbulent viscosity
  !
  if (kfl_cotur_chm /= 0) then
     do igaus = 1,pgaus
        do inode = 1,pnode
           gptur(igaus) = gptur(igaus) + gpsha(inode,igaus) * elmut(inode)
        end do
     end do
  end if 
  !
  ! DTFLES combustion model
  !
  if (kfl_tfles_chm == 1_ip) then
     ifuel = 1  ! Fuel
     ioxo2 = 2  ! O2
     ioxn2 = 5  ! N2
     !
     ! Flame/turbulence interactions
     !
     do igaus = 1,pgaus
        gpfar(igaus) = gpcon(igaus,ifuel) / ( gpcon(igaus,ioxo2) + gpcon(igaus,ioxn2))
        do inode = 1,pnode
           gptur(igaus) = max(gptur(igaus),0.0_rp)
           gpsen(igaus) = gpsen(igaus) + gpsha(inode,igaus) * elsen(inode)
        end do
        call chm_flame_prop(gpfar(igaus),gpspe(igaus),gpthi(igaus))
        call chm_tfles_sgs(gpspe(igaus),gpthi(igaus),&
                           gptur(igaus),gpvol(igaus),sgsef(igaus),&
                           gpfac(igaus),gpsen(igaus))
     end do
     !
     ! Density
     !
     do igaus = 1,pgaus
        do inode = 1,pnode
           gpden(igaus) = gpden(igaus)  &
                        + gpsha(inode,igaus) * elden(inode)
        end do
     end do

  end if
  !
  ! CFI combustion model
  !

  if (kfl_model_chm == 5_ip ) then
    !
    ! coupled to turbulence model
    !
    if (kfl_cotur_chm > 0_ip .and. kfl_tucfi_chm == 1_ip) then !! CFI-RANS 
      !
      ! Transport of reaction rate fluctuations: {w_c * c} 
      !
      do igaus = 1,pgaus
         do inode = 1,pnode
            gprrt(igaus) = gprrt(igaus) + gpsha(inode,igaus) * elrrt(inode)
         end do
      end do

      do igaus = 1,pgaus
         gpkey(igaus) = 0.0_rp
         gpeps(igaus) = 0.0_rp
         do inode = 1,pnode
            gpkey(igaus) = gpkey(igaus) + gpsha(inode,igaus) * elkey(inode) 
            gpeps(igaus) = gpeps(igaus) + gpsha(inode,igaus) * eleps(inode) 
         end do
         if(gpkey(igaus) <= 0.0_rp) gpkey(igaus) = 1.0e-10_rp
         gpdis(igaus) = 2.0_rp * gpeps(igaus) / gpkey(igaus)     !! Reaction term (dissipation rate) of ADR quation for the CFI model
      end do

      ncoun = nspec_chm / 2_ip
      gpprd = 0.0_rp
      do icoun = 1,ncoun 
         ispec = 2_ip * icoun
         do igaus = 1,pgaus
            do inode = 1,pnode
               do idime = 1,ndime
                  gpprd(igaus,ispec) = gpprd(igaus,ispec) + gpcar(idime,inode,igaus) * elcon(inode,ispec-1_ip)
               end do
            end do
            aux = gpprd(igaus,ispec)
            gpprd(igaus,ispec) = 2.0_rp * aux * aux * gptur(igaus) / diffu_chm(1,1) 
         end do
      end do
      do igaus = 1,pgaus
        gprrt(igaus) = 2.0_rp * (gprrt(igaus) - gpmas(igaus,1_ip) * gpcon(igaus,1_ip))
      end do

    elseif (kfl_cotur_chm < 0_ip .and. kfl_tucfi_chm == 1_ip) then   !! CFI-LES model
      !
      ! Density
      !
      do igaus = 1,pgaus
         do inode = 1,pnode
            gpden(igaus) = gpden(igaus)  &
                         + gpsha(inode,igaus) * elden(inode)
         end do
      end do
      !
      ! Transport of reaction rate fluctuations: {w_c * c} 
      !
      do igaus = 1,pgaus
         do inode = 1,pnode
            gprrt(igaus) = gprrt(igaus) + gpsha(inode,igaus) * elrrt(inode)
         end do
      end do
      !
      ! Dissipation term: 2 nu_t / (Delta^2*Sc_t) 
      ! - Domingo et al. (2005) Combust. Flame
      !
      do igaus = 1,pgaus
         vol   = gpvol(igaus)**(0.33333_rp)
         vol2  = vol*vol
         rvol2 = 1.0_rp / vol2
         gpdis(igaus) = 2.0_rp * gptur(igaus) * rvol2 / diffu_chm(1,1) 
      end do
      !
      ! Production term: 2 rho nu_t/Sc_t * (grad c)^2  
      ! - Domingo et al. (2005) Combust. Flame
      !
      ! Transport of reaction rate fluctuations: {w_c * c} - {w_c}*{c} 
      ! - Domingo et al. (2005) Combust. Flame
      !
      ncoun = nspec_chm / 2_ip
      gpprd = 0.0_rp
      do icoun = 1,ncoun 
         ispec = 2_ip * icoun
         do igaus = 1,pgaus
            do inode = 1,pnode
               do idime = 1,ndime
                  gpprd(igaus,ispec) = gpprd(igaus,ispec) + gpcar(idime,inode,igaus) * elcon(inode,ispec-1_ip)
               end do
            end do
            aux = gpprd(igaus,ispec)
            gpprd(igaus,ispec) = 2.0_rp * gptur(igaus) * gpden(igaus) * aux * aux / diffu_chm(1,1) 
            gprrt(igaus)       = 2.0_rp * (gprrt(igaus) - gpmas(igaus,ispec-1_ip) * gpcon(igaus,ispec-1_ip))
         end do
      end do

    else
      !
      ! variance of reaction progress is not used
      !
      gpdis = 0.0_rp
      gpprd = 0.0_rp
    end if
  end if
  !
  ! Diffusion coefficients
  !
  if (kfl_tfles_chm >= 1) then
    do ispec = 1,nspec_chm          !!For DTFLES => D_eff = D_lam * E * F_dyn + D_tur * (1 - OMEGA); F_dyn = 1+(F_max-1)OMEGA
        do igaus = 1,pgaus      
           do inode = 1,pnode
              gpDik(igaus,ispec) = gpDik(igaus,ispec) & 
                                 + gpsha(inode,igaus) * elDik(inode,ispec) * sgsef(igaus) * gpfac(igaus)  &
                                 + gptur(igaus) / diffu_chm(1,1) * (1.0_rp - gpsen(igaus))   

              do idime = 1,ndime
                 gpgDk(idime,igaus,ispec) = gpgDk(idime,igaus,ispec)  &
                                          + gpcar(idime,inode,igaus) * elDik(inode,ispec) * sgsef(igaus) * gpfac(igaus) &
                                          + gpcar(idime,inode,igaus) * gptur(igaus) / diffu_chm(1,1)
              enddo
           end do
        end do
     enddo 
  else
     do ispec = 1,nspec_chm
        do igaus = 1,pgaus
           do inode = 1,pnode
              gpDik(igaus,ispec) = gpDik(igaus,ispec)&
                + gpsha(inode,igaus) * elDik(inode,ispec)
              do idime = 1,ndime
                 gpgDk(idime,igaus,ispec)= gpgDk(idime,igaus,ispec) + gpcar(idime,inode,igaus) * elDik(inode,ispec)
              enddo
           end do
        end do
     enddo
  endif
  !
  ! Molar mass
  !
  if (kfl_diffu_chm==2) then
     gpmol=0.0_rp
     gpgmo=0.0_rp
     gphmo=0.0_rp
     do igaus = 1,pgaus
        do inode = 1,pnode
           gpmol (igaus) = gpmol(igaus) + gpsha(inode,igaus) * elmol(inode)
           gphmo (igaus) = gphmo(igaus) + gplap(igaus,inode) * elmol(inode)
           do idime = 1,ndime
              gpgmo(idime,igaus)=gpgmo(idime,igaus) +  gpcar(idime,inode,igaus) * elmol(inode)
           enddo
        enddo
     enddo
  endif

end subroutine chm_elmpre
