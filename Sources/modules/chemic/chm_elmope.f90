subroutine chm_elmope()
  !------------------------------------------------------------------------
  !****f* partis/chm_elmadr
  ! NAME 
  !    chm_elmadr
  ! DESCRIPTION
  !    Elemental operations
  ! USES
  ! USED BY
  !    chm_matrix
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_chemic
  implicit none
  real(rp)    :: elmat(mnode,mnode) 
  real(rp)    :: elrhs(mnode)
  integer(ip) :: ielem,igaus,idime,iclas               ! Indices and dimensions
  integer(ip) :: izmat,izrhs,pelty,pnode
  integer(ip) :: pgaus,plapl,porde,ptopo

  real(rp)    :: elcon(mnode,nspec_chm,ncomp_chm)      ! <=> conce
  real(rp)    :: elco1(mnode,ncomp_chm)                ! <=> conce: work array
  real(rp)    :: elcod(ndime,mnode)                    ! <=> coord

  real(rp)    :: gpvol(mgaus)                          ! |J|*w 
  real(rp)    :: gpcon(mgaus,nspec_chm)                ! <=> conce
  real(rp)    :: gprea(mgaus)                          ! r
  real(rp)    :: gpvel(ndime,mgaus)                    ! u
  real(rp)    :: gpdif(mgaus)                          ! k
  real(rp)    :: gpgrd(ndime,mgaus)                    ! grad(k)
  real(rp)    :: gprhs(mgaus)                          ! f (all terms)
  real(rp)    :: gpden(mgaus)                          ! rho
  real(rp)    :: gppro(mgaus)                          ! Weighted residual L2-projection
  real(rp)    :: gpdiv(mgaus)                          ! Divergence of convection
  real(rp)    :: gpcar(ndime,mnode,mgaus)              ! dNk/dxj
  real(rp)    :: gphes(ntens,mnode,mgaus)              ! dNk/dxidxj
  real(rp)    :: gplap(mgaus,mnode)                    ! Laplacian
  real(rp)    :: gpfar(mgaus)                          ! Fuel / air ratio
  real(rp)    :: dummr(mgaus*ndime)
  real(rp)    :: gpsgs(mgaus)
  real(rp)    :: gptau(mgaus)
  real(rp)    :: chale(3),chave(3),hleng(3),tragl(9)
  real(rp)    :: dumms(mnode,nspec_chm)
  real(rp)    :: aaa(ndime,mnode,mgaus)
  real(rp)    :: xxx(mgaus,nspec_chm)
  real(rp)    :: ccc(ndime,mgaus,nspec_chm)


  !
  ! Initialization: unused variables
  !
  do igaus = 1,mgaus
     gpden(igaus) = 1.0_rp
     gptau(igaus) = 0.0_rp
     gpdiv(igaus) = 0.0_rp
     gprhs(igaus) = 0.0_rp
     gpdif(igaus) = 0.0_rp
     gprea(igaus) = 0.0_rp
     gpsgs(igaus) = 0.0_rp
     do idime = 1,ndime
        gpvel(idime,igaus) = 0.0_rp
        gpgrd(idime,igaus) = 0.0_rp
     end do
  end do
  !
  ! Loop over elements
  !
  elements: do ielem = 1,nelem
     !
     ! Element dimensions
     !
     pelty = ltype(ielem)
     pnode = nnode(pelty)
     pgaus = ngaus(pelty)
     plapl = llapl(pelty) 
     porde = lorde(pelty)
     ptopo = ltopo(pelty)
     !
     ! Gather operations
     !
     call chm_elmgat(&
          pnode,lnods(1,ielem),elcod,elcon)
     !
     ! CHALE, HLENG and TRAGL 
     !
     if( kfl_taust_chm /= 0 .or. kfl_shock_chm /= 0 ) then
        call elmlen(&
             ndime,pnode,elmar(pelty)%dercg,tragl,elcod,hnatu(pelty),&
             hleng)
        call elmchl(&
             tragl,hleng,elcod,dummr,chave,chale,pnode,&
             porde,hnatu(pelty),kfl_advec_chm,kfl_ellen_chm)
     else
        plapl = 0
     end if
     !
     ! Cartesian derivatives, Hessian matrix and volume: GPCAR, GPHES, PGVOL
     !
     call elmcar(&
          pnode,pgaus,plapl,elmar(pelty)%weigp,elmar(pelty)%shape,&
          elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,gpvol,gpcar,&
          gphes,ielem)
     !
     ! GPCON: Pre-computations
     !
     call chm_elmpre(&
          pnode,pgaus,elcon(:,:,1),dumms(:,1),aaa(:,:,1),dumms,dumms,dumms(:,1),dumms(:,1),dumms(:,1),dumms(:,1),& 
          dumms(:,1),elmar(pelty)%shape,gpcar,gplap,gpcon,gpvel,xxx,ccc,xxx,dumms(:,1),dummr,ccc(:,:,1),&
          dummr,gpfar,dummr,gpdiv,dummr,xxx,dummr,dummr,dummr,dummr,dummr,dummr,dummr)

     !
     ! Assemble matrix
     !  
     izmat = 1
     izrhs = 1
     do iclas = iclai_chm,iclaf_chm
        if( kfl_assem_chm == 1 ) then
           call chm_elmpro(&
                '111',iclas,pnode,pgaus,gpcon,elcod,elmar(pelty)%shape,&
                gpdif,gprea,gprhs)
        else
           call chm_elmpro(&
                '100',iclas,pnode,pgaus,gpcon,elcod,elmar(pelty)%shape,&
                gpdif,gprea,gprhs)
        end if
        call chm_elmwor(&
             pnode,iclas,elcon,elco1) ! elco1(:,:) <= elcon(:,iclas,:)

        call elmadr(& 
             1_ip,ielem,pnode,pgaus,ptopo,plapl,pelty,1_ip,1_ip,lnods(1,ielem),kfl_shock_chm,&
             kfl_taust_chm,kfl_stabi_chm,0_ip,0_ip,kfl_limit_chm,staco_chm,gpdif,&
             gprea,gpden,gpvel,gppro,gpvol,gpgrd,gprhs,elmar(pelty)%shape,gpcar,gphes,&
             elco1,elcod,chale,gptau,dtinv_chm,shock_chm,bemol_chm,gpdiv,gplap,gpsgs,&
             elmat,elrhs)
        if( kfl_model_chm == 2 ) then
           call chm_elmdit(&
                iclas,pnode,lnods(1,ielem),elmat,elrhs)
        else
          call chm_elmdir(&
                iclas,pnode,lnods(1,ielem),elmat,elrhs)           
        end if
        call assrhs(&
             solve(1)%ndofn,pnode,lnods(1,ielem),elrhs,rhsid(izrhs))
        call assmat(&
             solve(1)%ndofn,pnode,pnode,npoin,solve(1)%kfl_algso,&
             ielem,lnods(1,ielem),elmat,amatr(izmat))
        izrhs = izrhs + solve(1)%nzrhs
        izmat = izmat + solve(1)%nzmat
     end do

  end do elements

end subroutine chm_elmope
