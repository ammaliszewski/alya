subroutine chm_elmpro(&
     ctask,iclas,pnode,pgaus,gpcon,elcod,gpsha,gpdif,gprea,gprhs)   
  !-----------------------------------------------------------------------
  !****f* partis/chm_elmpro
  ! NAME 
  !    chm_elmpro
  ! DESCRIPTION
  !    Compute properties for class ICLAS:
  !    1. GPDIF: Diffusion
  !    2. GPREA: Reaction term
  !    3. GPSOU: Source term
  ! USES
  ! USED BY
  !    chm_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp
  use def_parame, only      :  pi
  use def_domain, only      :  ndime,mgaus,mnode
  use def_chemic, only      :  kfl_advec_chm,nclas_chm,kfl_timei_chm,&
       &                       kfl_sourc_chm,sourc_chm,kfl_tisch_chm,&
       &                       kfl_tiacc_chm,dtinv_chm,pabdf_chm,&
       &                       boltz_chm,diffu_chm,nspec_chm,&
       &                       nreac_chm,iarea_chm,jarea_chm,lreac_chm,&
       &                       radiu_chm,denma_chm,react_chm,socen_chm,&
       &                       sorad_chm,equil_chm
  implicit none
  character(1), intent(in)  :: ctask(3)
  integer(ip),  intent(in)  :: iclas,pnode,pgaus
  real(rp),     intent(in)  :: gpcon(pgaus,nspec_chm)
  real(rp),     intent(in)  :: elcod(ndime,pnode)
  real(rp),     intent(in)  :: gpsha(pnode,pgaus)
  real(rp),     intent(out) :: gpdif(pgaus)
  real(rp),     intent(out) :: gprea(pgaus)
  real(rp),     intent(out) :: gprhs(pgaus)
  integer(ip)               :: igaus,inode,idime
  integer(ip)               :: ireac,irea1,irea2,ipro1,iarea
  real(rp)                  :: k,kp,km,k1,k2,ovkT,C1,C2,C3,gpcod(3)
  real(rp)                  :: C0eq1,Eform1,Ceq1,C0eq2,Eform2,Ceq2,T
  !
  ! Constants
  !
  call chm_usrtem(T)
  ovkT = 1.0_rp/(boltz_chm*T) 

  !----------------------------------------------------------------------
  !
  ! RHS: Source term (do not move)
  !
  !----------------------------------------------------------------------

  if( ctask(3) == '1' ) then

     do igaus = 1,pgaus
        gprhs(igaus) = 0.0_rp
     end do
     if( kfl_sourc_chm == 1 ) then
        do igaus = 1,pgaus
           gprhs(igaus) = sourc_chm
        end do

     else if( kfl_sourc_chm == 2 ) then
        do igaus = 1,pgaus
           k = 0.0_rp
           do idime = 1,ndime
              gpcod(idime) = 0.0_rp
              do inode = 1,pnode
                 gpcod(idime) = gpcod(idime) &
                      + gpsha(inode,igaus)*elcod(idime,inode)
              end do
              k = k + ( gpcod(idime) - socen_chm(idime) ) ** 2
           end do
           if( sqrt(k) <= sorad_chm ) then
              gprhs(igaus) = sourc_chm
           end if
        end do
     end if

  end if

  !----------------------------------------------------------------------
  !
  ! GPDIF: Diffusion
  !    
  !----------------------------------------------------------------------

  if( ctask(1) == '1' ) then
     k = diffu_chm(1,iclas)*exp(-diffu_chm(2,iclas)*ovkT)
     do igaus = 1,pgaus
        gpdif(igaus) = k
     end do
  end if

  !----------------------------------------------------------------------
  !
  ! GPREA: Reaction
  !
  !----------------------------------------------------------------------  

  if( ctask(2) == '1' ) then
     do igaus = 1,pgaus 
        gprea(igaus) = 0.0_rp
     end do
     if( nreac_chm == 0 ) then
        continue
     else

        do iarea =  iarea_chm(iclas),iarea_chm(iclas+1)-1

           ireac =  jarea_chm(iarea)        ! Reaction ireac 
           irea1 =  lreac_chm(ireac)%l(1)   ! Reactant 1
           irea2 =  lreac_chm(ireac)%l(2)   ! Reactant 2
           ipro1 = -lreac_chm(ireac)%l(3)   ! Product 1

           do igaus = 1,pgaus

              C1 =  gpcon(igaus,irea1)
              C2 =  gpcon(igaus,irea2)
              if( ipro1 /= 0 ) C3 =  gpcon(igaus,ipro1)

              k1 =  0.0_rp
              k2 =  0.0_rp

              if( irea1 <= nclas_chm ) &                                                  ! k1 = k1'*exp(-E1/kT)
                   k1 = diffu_chm(1,irea1) * exp(-diffu_chm(2,irea1)*ovkT)
              if( irea2 <= nclas_chm ) &                                                  ! k2 = k2'*exp(-E2/kT)
                   k2 = diffu_chm(1,irea2) * exp(-diffu_chm(2,irea2)*ovkT)

              kp = 4.0_rp * pi * radiu_chm(ireac) * (k1+k2)                               ! k+ = 4 * pi * (rA+rB)*(kA+kB)
              km = denma_chm * kp * react_chm(1,ireac) * exp(-react_chm(2,ireac) * ovkT)  ! k- = rho * k+ * exp(-E/kT)

              if( ipro1 /= 0 ) then
                 !
                 ! Reaction has a product: R1 + R2 <=> P1
                 !
                 if( irea1 == iclas ) then                                  ! I am reactant 1
                    gprea(igaus) = gprea(igaus) + kp * C2
                    if( ctask(3) == '1' ) &
                         gprhs(igaus) = gprhs(igaus) + km * C3
                 end if
                 
                 if( irea2 == iclas ) then                                  ! I am reactant 2
                    gprea(igaus) = gprea(igaus) + kp * C1
                    if( ctask(3) == '1' ) &
                         gprhs(igaus) = gprhs(igaus) + km * C3
                 end if
                 
                 if( ipro1 == iclas ) then                                  ! I am product
                    gprea(igaus) = gprea(igaus) + km
                    if( ctask(3) == '1' ) &
                         gprhs(igaus) = gprhs(igaus) + kp * C1 * C2
                 end if

              else if( ipro1 == 0 ) then
                 !
                 ! Reaction does not have a product: R1 + R2 <=> 0
                 !
                 C0eq1  = equil_chm(1,irea1)                 ! C0eq
                 Eform1 = equil_chm(2,irea1)                 ! E_form: formation energy
                 Ceq1   = C0eq1 * exp(-Eform1*ovkT)          ! Ceq = C0eq * exp(-E_form/kT)

                 C0eq2  = equil_chm(1,irea2)                 ! C0eq
                 Eform2 = equil_chm(2,irea2)                 ! E_form: formation energy
                 Ceq2   = C0eq2 * exp(-Eform2*ovkT)          ! Ceq = C0eq * exp(-E_form/kT)

                 C3     = Ceq1 * Ceq2

                 if( irea1 == iclas ) then                                  ! I am reactant 1
                    gprea(igaus) = gprea(igaus) + kp * C2
                    if( ctask(3) == '1' ) &
                         gprhs(igaus) = gprhs(igaus) - kp * C3
                 end if
                 
                 if( irea2 == iclas ) then                                  ! I am reactant 2
                    gprea(igaus) = gprea(igaus) + kp * C1
                    if( ctask(3) == '1' ) &
                         gprhs(igaus) = gprhs(igaus) - kp * C3
                 end if
 
              end if

           end do
        end do

     end if
  end if

end subroutine chm_elmpro
