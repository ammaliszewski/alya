subroutine chm_elmprt(&
     pnode,pgaus,elcon,elpro,gpsha,gpcon,gppro)   
  !-----------------------------------------------------------------------
  !****f* partis/chm_elmprt
  ! NAME 
  !    chm_elmprt
  ! DESCRIPTION
  !    Compute concentrations at Gauss points
  ! USES
  ! USED BY
  !    chm_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp
  use def_parame, only      :  pi
  use def_domain, only      :  ndime,mgaus,mnode
  use def_chemic, only      :  nspec_chm,proje_chm,kfl_stabi_chm
  implicit none
  integer(ip),  intent(in)  :: pnode,pgaus
  real(rp),     intent(in)  :: elcon(pnode,nspec_chm,*)
  real(rp),     intent(in)  :: elpro(pnode)
  real(rp),     intent(in)  :: gpsha(pnode,pgaus)
  real(rp),     intent(out) :: gpcon(pgaus,nspec_chm)
  real(rp),     intent(out) :: gppro(pgaus)
  integer(ip)               :: igaus,ispec,inode
  
  do ispec = 1,nspec_chm
     do igaus = 1,pgaus
        gpcon(igaus,ispec) = 0.0_rp
     end do
  end do
  do ispec = 1,nspec_chm
     do igaus = 1,pgaus
        do inode = 1,pnode
           gpcon(igaus,ispec) = gpcon(igaus,ispec)&
                + gpsha(inode,igaus) * elcon(inode,ispec,1)
        end do
     end do
  end do
  !
  ! Projection
  !
  if( kfl_stabi_chm >= 1 ) then
     do igaus = 1,pgaus
        gppro(igaus) = 0.0_rp
        do inode = 1,pnode
           gppro(igaus) = gppro(igaus) &
                + gpsha(inode,igaus) * elpro(inode)
        end do
     end do     
  end if

end subroutine chm_elmprt
