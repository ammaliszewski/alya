subroutine chm_elmgat(pnode,lnods,elcod,elcon)
  !------------------------------------------------------------------------
  !****f* partis/chm_elmgat
  ! NAME 
  !    chm_elmgat
  ! DESCRIPTION
  !    This routine performs the gather operations
  ! USES
  ! USED BY
  !    chm_elmope
  !***
  !------------------------------------------------------------------------ 
  use def_kintyp, only     :  ip,rp 
  use def_domain, only     :  ndime,mnode,npoin,coord
  use def_master, only     :  conce
  use def_chemic, only     :  kfl_timei_chm,kfl_advec_chm,&
       &                      kfl_tiacc_chm,kfl_tisch_chm,&
       &                      nclas_chm,nspec_chm
  implicit none 
  integer(ip), intent(in)  :: pnode
  integer(ip), intent(in)  :: lnods(pnode)
  real(rp),    intent(out) :: elcod(ndime,pnode)
  real(rp),    intent(out) :: elcon(pnode,nspec_chm,*)
  integer(ip)              :: inode,ipoin,idime,itime,iclas
  !
  ! Current concentration and coordinates
  !
  do inode=1,pnode
     ipoin=lnods(inode)
     do iclas=1,nspec_chm
        elcon(inode,iclas,1)=conce(ipoin,iclas,1)
     end do
     do idime=1,ndime
        elcod(idime,inode)=coord(idime,ipoin)
     end do  
  end do
  !
  ! Time integration
  !
  if( kfl_timei_chm == 1 ) then
     do iclas = 1,nclas_chm
        do inode = 1,pnode
           ipoin = lnods(inode)
           elcon(inode,iclas,2) = conce(ipoin,iclas,3)
        end do
     end do
     if( kfl_tisch_chm == 2 ) then
        do iclas = 1,nclas_chm
           do itime = 3,1+kfl_tiacc_chm
              do inode = 1,pnode
                 ipoin = lnods(inode)
                 elcon(inode,iclas,itime) = conce(ipoin,iclas,itime+1)
              end do
           end do
        end do
     end if
  end if 

end subroutine chm_elmgat
