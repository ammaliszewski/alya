subroutine chm_elmmep(&
     ctask,iclas,pnode,pgaus,pelty,nddif,gpcon,elcod,elvel,&
     elvte,eltmr,elden,gpsha,gpcar,gpdif,gprea,gprhs,gpadv,&
     gpden)   
  !-----------------------------------------------------------------------
  !****f* partis/chm_elmmep
  ! NAME 
  !    chm_elmmep
  ! DESCRIPTION
  !    Compute properties for class ICLAS:
  !    1. GPDIF: Diffusion
  !    2. GPREA: Reaction term
  !    3. GPRHS: Source term
  !    4. GPADV: Advection term
  !    5. GPDEN: Density
  ! USES
  ! USED BY
  !    chm_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp
  use def_domain, only      :  ndime,mgaus,mnode
  use def_chemic, only      :  kfl_advec_chm,nclas_chm,kfl_sourc_chm,&
       &                       diffu_chm,nspec_chm,nreac_chm,iarea_chm,&
       &                       jarea_chm,lreac_chm,denma_chm,react_chm,&
       &                       socen_chm,sorad_chm,sourc_chm,lawdi_chm,&
       &                       lawde_chm,lawvt_chm,kfl_diffu_chm
  implicit none
  character(1), intent(in)  :: ctask(5)
  integer(ip),  intent(in)  :: iclas,pnode,pgaus,pelty,nddif
  real(rp),     intent(in)  :: gpcon(pgaus,nspec_chm)
  real(rp),     intent(in)  :: elcod(ndime,pnode)
  real(rp),     intent(in)  :: elvel(ndime,pnode)
  real(rp),     intent(in)  :: elvte(pnode)
  real(rp),     intent(in)  :: eltmr(pnode)
  real(rp),     intent(in)  :: elden(pnode)
  real(rp),     intent(in)  :: gpsha(pnode,pgaus)
  real(rp),     intent(in)  :: gpcar(ndime,mnode,pgaus)
  real(rp),     intent(out) :: gpdif(nddif,pgaus)
  real(rp),     intent(out) :: gprea(pgaus)
  real(rp),     intent(out) :: gprhs(pgaus)
  real(rp),     intent(out) :: gpadv(ndime,pgaus)
  real(rp),     intent(out) :: gpden(pgaus)
  integer(ip)               :: igaus,inode,idime
  integer(ip)               :: ireac,irea1,irea2,ipro1,iarea
  real(rp)                  :: gpcod(ndime,mgaus),k

  !----------------------------------------------------------------------
  !
  ! GPDEN: Density
  !
  !----------------------------------------------------------------------

  if( ctask(5) == '1' ) then

     if( lawde_chm /= 0 ) then
        !
        ! Density is in array ELDEN <= DENSI_CHM
        !
        do igaus = 1,pgaus
           gpden(igaus) = 0.0_rp
           do inode = 1,pnode
              gpden(igaus) = gpden(igaus) &
                   + gpsha(inode,igaus)*elden(inode)
           end do 
        end do
     else
        !
        ! Density is constant
        !
        do igaus = 1,pgaus
           gpden(igaus) = denma_chm
        end do
     end if

  end if
  
  !----------------------------------------------------------------------
  !
  ! RHS: Source term 
  !
  !----------------------------------------------------------------------

  if( ctask(3) == '1' ) then

     do igaus = 1,pgaus
        gprhs(igaus) = 0.0_rp
     end do

     if( kfl_sourc_chm == 1 ) then
        !
        ! Constant source
        !
        do igaus = 1,pgaus
           gprhs(igaus) = sourc_chm
        end do

     else if( kfl_sourc_chm == 2 ) then
        !
        ! Sphere source
        !
        do igaus = 1,pgaus
           k = 0.0_rp
           do idime = 1,ndime
              gpcod(idime,1) = 0.0_rp
              do inode = 1,pnode
                 gpcod(idime,1) = gpcod(idime,1) &
                      + gpsha(inode,igaus)*elcod(idime,inode)
              end do
              k = k + ( gpcod(idime,1) - socen_chm(idime) ) ** 2
           end do
           if( sqrt(k) <= sorad_chm ) then
              gprhs(igaus) = sourc_chm
           end if
        end do

     else if( kfl_sourc_chm < 0 ) then
        !
        ! Source is in array ELTMR <= TMRAT
        !
        do igaus = 1,pgaus
           gprhs(igaus) = 0.0_rp
           do inode = 1,pnode
              gprhs(igaus) = gprhs(igaus) + gpsha(inode,igaus)*eltmr(inode)
           end do
        end do
     end if

  end if

  !----------------------------------------------------------------------
  !
  ! GPDIF: Diffusion
  !    
  !----------------------------------------------------------------------

  if( ctask(1) == '1' ) then

     if( kfl_diffu_chm /= 0 ) then
!
!  arnau : this can be skiped if all comes from meteo
!
        call chm_hdiffu(iclas,pnode,pelty,pgaus,gpcar,elvel,elcod,gpden,gpdif)
        call chm_vdiffu(iclas,pnode,pelty,pgaus,gpcar,elvel,elcod,gpden,gpdif)
     end if

  end if

  !----------------------------------------------------------------------
  !
  ! GPREA: Reaction
  !
  !----------------------------------------------------------------------  

  if( ctask(2) == '1' ) then

     do igaus = 1,pgaus
        gprea(igaus) = 0.0_rp
     end do

  end if

  !----------------------------------------------------------------------
  !
  ! GPADV: Velocity
  !
  !----------------------------------------------------------------------  

  if( ctask(4) == '1' ) then

     if( kfl_advec_chm < 0 ) then
        !
        ! Velocity is in array ELVEL <= VELOC_CHM
        !
        do igaus = 1,pgaus
           do idime = 1,ndime
              gpadv(idime,igaus) = 0.0_rp
              do inode = 1,pnode
                 gpadv(idime,igaus) = gpadv(idime,igaus) &
                      + gpsha(inode,igaus) * elvel(idime,inode)
              end do
           end do
        end do

     else if( kfl_advec_chm > 0 ) then
        !
        ! Velocity is given by a function
        !
        do igaus = 1,pgaus
           do idime = 1,ndime
              gpcod(idime,igaus) = 0.0_rp
              do inode = 1,pnode
                 gpcod(idime,igaus) = gpcod(idime,igaus) &
                      + gpsha(inode,igaus) * elcod(idime,inode)
              end do
           end do
        end do
        call chm_velfun(pgaus,gpcod,gpadv)

     end if
     !
     ! Subtract terminal velocity which is in array ELVTE <= VTERM_CHM
     !
     if( lawvt_chm >= 1 ) then

        idime = ndime
        do igaus = 1,pgaus
           do inode = 1,pnode
              gpadv(idime,igaus) = gpadv(idime,igaus) &
                   - gpsha(inode,igaus) * elvte(inode)
           end do
        end do

     end if

  end if

end subroutine chm_elmmep
