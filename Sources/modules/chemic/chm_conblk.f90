subroutine chm_conblk()
  !-----------------------------------------------------------------------
  !****f* partis/chm_conblk
  ! NAME 
  !    chm_conblk
  ! DESCRIPTION
  !    This routine ends a block iteration
  ! USES
  ! USED BY
  !    Partis
  !***
  !-----------------------------------------------------------------------
  implicit none

end subroutine chm_conblk
