subroutine chm_updtss()
  !-----------------------------------------------------------------------
  !****f* Chemic/chm_updtss
  ! NAME 
  !    chm_updtss
  ! DESCRIPTION
  !    This routine computes the time step size
  ! USED BY
  !    chm_timste
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_chemic
  implicit none 
  real(rp) :: dtmin

  if( kfl_timei_chm /= 0 ) then

     dtmin = 1e6_rp
     if( kfl_dttyp_chm == 1 ) then
        !
        ! Time step based on critical time step
        !
        if (kfl_model_chm == 4 .or. kfl_model_chm == 5) then
           call chm_updtcc(dtmin)
        else        
           call chm_updtsc(dtmin)
        endif
        
     else if( kfl_dttyp_chm == 2 ) then
        !
        ! Adaptive time step
        !
        call chm_updtsf(dtmin)

     end if

     dtcri_chm = dtmin
     if( dtcri_chm /= 0.0_rp ) dtinv_chm = 1.0_rp/(dtcri_chm*safet_chm)
     if( kfl_timco == 1 )     then
        dtinv = max(dtinv,dtinv_chm)
     endif
  end if

end subroutine chm_updtss
