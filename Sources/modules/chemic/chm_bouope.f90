subroutine chm_bouope()
  !-------------------------------------------------------------------------
  !****f* emic/chm_bouope
  ! NAME 
  !    chm_bouope
  ! DESCRIPTION
  !    Boundary operations
  ! USES
  ! USED BY
  !    chm_matrix 
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_chemic
  implicit none
  real(rp)    :: elmat(mnode,mnode)
  real(rp)    :: elrhs(mnode)
  real(rp)    :: gbdif(mgaub) 
  real(rp)    :: baloc(9),bocod(ndime,mnodb)
  integer(ip) :: ielem,iboun,inodb,pblty,kfl_robin
  integer(ip) :: pnodb,pnode,pelty,pgaus,pgaub
  integer(ip) :: iclas,izmat,izrhs,idime,ipoin
  real(rp)    :: gbsur(mgaub)
  real(rp)    :: dummr(ndime*mnode)

  if( INOTMASTER .and. kfl_robin_chm /= 0 ) then
!!!     print *,'BOUOPEEEEEEEEEEEEEEEEEEE'
     kfl_robin = 0
     iboun     = 0
     do while( iboun < nboun )
        iboun = iboun + 1
        iclas = iclai_chm - 1
        do while( iclas < iclaf_chm )
           iclas = iclas + 1
           if( kfl_fixbo_chm(iclas,iboun) == 2 ) then
              kfl_robin = 1
              iclas     = iclaf_chm
              iboun     = nboun
           end if
        end do
     end do

     Robin: if( kfl_robin == 1 ) then

        boundaries: do iboun = 1,nboun

           pblty = ltypb(iboun)
           pnodb = nnode(pblty)
           ielem = lboel(pnodb+1,iboun)
           pelty = ltype(ielem)
           pnode = nnode(pelty)
           pgaus = ngaus(pelty)
           pgaub = ngaus(pblty)
           !
           ! Gather operations
           !
           do inodb = 1,pnodb
              ipoin = lnodb(inodb,iboun)
              do idime = 1,ndime
                 bocod(idime,inodb) = coord(idime,ipoin)
              end do
           end do
           !
           ! Jacobian
           !
           call boucar(& 
                pnodb,pgaub,elmar(pblty)%shape,elmar(pblty)%deriv,&
                elmar(pblty)%weigp,bocod,gbsur,baloc)
           !
           ! Assembly
           !
           izmat = 1
           izrhs = 1
           do iclas = iclai_chm,iclaf_chm
              if( kfl_fixbo_chm(iclas,iboun) == 2 ) then
                 call chm_elmpro(&
                      '100',iclas,pnodb,pgaub,dummr,dummr,elmar(pblty)%shape,&
                      gbdif,dummr,dummr)     
                 call chm_boumat(&
                      iclas,pnode,pnodb,pgaub,pgaus,lboel(1,iboun),elmar(pblty)%shape,&
                      gbsur,elmar(pelty)%shaga,gbdif,elmat,elrhs)
                 call chm_elmdir(& 
                      iclas,pnode,lnods(1,ielem),elmat,elrhs)
                 call assrhs(&
                      solve(1)%ndofn,pnode,lnods(1,ielem),elrhs,rhsid(izrhs))
                 call assmat(&
                      solve(1)%ndofn,pnode,pnode,npoin,solve(1)%kfl_algso,&
                      ielem,lnods(1,ielem),elmat,amatr(izmat))
              end if
              izrhs = izrhs + solve(1)%nzrhs
              izmat = izmat + solve(1)%nzmat
           end do

        end do boundaries

     end if Robin

  end if

end subroutine chm_bouope
