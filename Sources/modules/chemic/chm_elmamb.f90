subroutine chm_elmamb(itask)
  !------------------------------------------------------------------------
  !****f* partis/chm_elmadr
  ! NAME 
  !    chm_elmadr
  ! DESCRIPTION
  !    Elemental operations
  !
  !    d(rho*C)/dt + div(rho*C*u) - div [ k grad(C) ] + r*C = Q
  !
  !    <=> using d(rho)/dt + div(rho*u) = 0
  !
  !    rho dC/dt + rho*u.grad(C) - div [ k grad(C) ] + r*C = Q
  !
  !    C   = GPCON
  !    rho = GPDEN
  !    u   = GPADV
  !    k   = GPDIF
  !    r   = GPREA
  !    Q   = GPRHS
  !
  ! USES
  ! USED BY
  !    chm_matrix
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_chemic
  implicit none
  integer(ip), intent(in) :: itask
  real(rp)    :: elmat(mnode,mnode) 
  real(rp)    :: elrhs(mnode)
  integer(ip) :: ielem,igaus,idime,iclas               ! Indices and dimensions
  integer(ip) :: izmat,izrhs,pelty,pnode
  integer(ip) :: pgaus,plapl,porde,ptopo
  integer(ip) :: inode

  real(rp)    :: elcon(mnode,nspec_chm,ncomp_chm)      ! <=> conce
  real(rp)    :: elco1(mnode,ncomp_chm)                ! <=> conce: work array
  real(rp)    :: elcod(ndime,mnode)                    ! <=> coord
  real(rp)    :: elvel(ndime,mnode)                    ! <=> veloc
  real(rp)    :: elpro(mnode)                          ! <=> projection

  real(rp)    :: gpvol(mgaus)                          ! |J|*w 
  real(rp)    :: gpcon(mgaus,nspec_chm)                ! <=> conce
  real(rp)    :: gprea(mgaus)                          ! r
  real(rp)    :: gpadv(ndime,mgaus)                    ! u
  real(rp)    :: gpdif(ndime,mgaus)                    ! k
  real(rp)    :: gpgrd(ndime,mgaus)                    ! grad(k)
  real(rp)    :: gprhs(mgaus)                          ! f (all terms)
  real(rp)    :: gpden(mgaus)                          ! rho
  real(rp)    :: gppro(mgaus)                          ! Weighted residual L2-projection
  real(rp)    :: gpdiv(mgaus)                          ! Divergence of convection
  real(rp)    :: gpvte(mgaus)                          ! Terminal velocity ut
  real(rp)    :: gptmr(mgaus)                          ! Source term Q

  real(rp)    :: gpcar(ndime,mnode,mgaus)              ! dNk/dxj
  real(rp)    :: gphes(ntens,mnode,mgaus)              ! dNk/dxidxj
  real(rp)    :: gplap(mnode,mgaus)                    ! Laplacian
  real(rp)    :: dummr(mgaus*ndime)
  real(rp)    :: gptau(mgaus)
  real(rp)    :: chale(2),chave(3),hleng(3),tragl(9)
  real(rp), pointer :: gpsgs(:,:)
  real(rp), target  :: cosgs_chm(1,1)                ! Concentration subgrid scale  CHEMIC
  real(rp)    :: elrh2(mnode*ndime)                  ! temporary
  !
  ! Initialization: unused variables
  !
  do igaus = 1,mgaus
     gpden(igaus) = 1.0_rp
     gpdiv(igaus) = 0.0_rp
     gprhs(igaus) = 0.0_rp
     gprea(igaus) = 0.0_rp
     gptau(igaus) = 0.0_rp
     gppro(igaus) = 0.0_rp
     do idime = 1,ndime
        gpadv(idime,igaus) = 0.0_rp
        gpdif(idime,igaus) = 0.0_rp
        gpgrd(idime,igaus) = 0.0_rp
     end do
     do inode = 1,mnode
        do idime = 1,ntens
           gphes(idime,inode,igaus) = 0.0_rp
        end do
     end do
  end do
  gpsgs => cosgs_chm
  !
  ! Loop over elements
  !  
  elements: do ielem = 1,nelem
     !
     ! Element dimensions
     !
     pelty = ltype(ielem)
     pnode = nnode(pelty)
     pgaus = ngaus(pelty)
     plapl = llapl(pelty)
     porde = lorde(pelty)
     ptopo = ltopo(pelty)
     !
     ! Gather operations
     !
     call chm_elmgmb(&
          pnode,lnods(1,ielem),elcod,elcon,elpro,elvel)
     !
     ! CHALE, HLENG and TRAGL 
     !
     if( kfl_stabi_chm /= -2 .or. kfl_shock_chm /= 0 ) then
        call elmlen(&
             ndime,pnode,elmar(pelty)%dercg,tragl,elcod,hnatu(pelty),&
             hleng)
        call elmchl(&
             tragl,hleng,elcod,dummr,chave,chale,pnode,&
             porde,hnatu(pelty),kfl_advec_chm,kfl_ellen_chm)
     end if
     !
     ! Cartesian derivatives, Hessian matrix and volume: GPCAR, GPHES, PGVOL
     !
     call elmcar(&
          pnode,pgaus,plapl,elmar(pelty)%weigp,elmar(pelty)%shape,&
          elmar(pelty)%deriv,elmar(pelty)%heslo,elcod,gpvol,gpcar,&
          gphes,ielem)
     !
     ! GPCON: Pre-computations
     !
     call chm_elmprt(&
          pnode,pgaus,elcon,elpro,elmar(pelty)%shape,gpcon,gppro)  
     !
     ! Assemble matrix
     !  
     izmat = 1
     izrhs = 1
     do iclas = iclai_chm,iclaf_chm
        call chm_elmpmb(&
             itask,iclas,pnode,pgaus,elcon,elvel,elcod,elmar(pelty)%shape,&
             gpcar,gphes,gpdif,gprea,gprhs,gpadv,gpden,ielem)

        if( itask /= 10 ) then ! temporary

           if( kfl_sgsti_chm == 1 ) gpsgs => cosgs(ielem,iclas)%a
           call chm_elmwor(&
                pnode,iclas,elcon,elco1) ! elco1(:,:) <= elcon(:,iclas,:)

           call elmadr(& 
                itask,ielem,pnode,pgaus,ptopo,plapl,pelty,1_ip,ndime,lnods(1,ielem),kfl_shock_chm,&
                kfl_taust_chm,kfl_stabi_chm,0_ip,kfl_sgsti_chm,kfl_limit_chm,staco_chm,gpdif,gprea,&
                gpden,gpadv,gppro,gpvol,gpgrd,gprhs,elmar(pelty)%shape,gpcar,gphes,elco1,elcod,&
                chale,gptau,dtinv_chm,shock_chm,bemol_chm,gpdiv,gplap,gpsgs,elmat,elrhs) 

           if( itask == 1 ) then
              if( kfl_assem_chm < 2 ) then 
                 call chm_elmdit(&
                      iclas,pnode,lnods(1,ielem),elmat,elrhs)
              end if
              call assrhs(&
                   solve(1)%ndofn,pnode,lnods(1,ielem),elrhs,rhsid(izrhs))
              call assmat(&
                   solve(1)%ndofn,pnode,pnode,npoin,solve(1)%kfl_algso,&
                   ielem,lnods(1,ielem),elmat,amatr(izmat))
           end if
           izrhs = izrhs + solve(1)%nzrhs
           izmat = izmat + solve(1)%nzmat
        end if

     end do

  end do elements

end subroutine chm_elmamb
