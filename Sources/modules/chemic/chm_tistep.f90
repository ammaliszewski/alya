subroutine chm_tistep()
  !-----------------------------------------------------------------------
  !****f* partis/chm_tistep
  ! NAME 
  !    chm_tittim
  ! DESCRIPTION
  !    This routine sets the time step
  ! USES
  ! USED BY
  !    chm_begite
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_chemic
  implicit none

  if(kfl_timco/=2) then
     dtinv_chm=dtinv
     if(momod(modul) % kfl_stead==1) dtinv_chm = 0.0_rp
     if(kfl_timei_chm==0) dtinv_chm = 0.0_rp  

     if(kfl_tisch_chm==1) then
        !
        ! Trapezoidal rule: Euler iterations
        !
        if(ittim<=neule_chm) then
           kfl_tiacc_chm=1
        else
           kfl_tiacc_chm=kfl_tiaor_chm
        end if
        if(kfl_tiacc_chm==2) dtinv_chm = 2.0_rp*dtinv_chm
     else
        !
        ! BDF scheme: increase integration order at each time step
        !
        kfl_tiacc_chm=min(kfl_tiaor_chm,ittim)
        call parbdf(kfl_tiacc_chm,pabdf_chm)
     end if
  end if

  routp(1) = dtcri_chm
  routp(2) = 0.0_rp
  routp(3) = 0.0_rp
  ioutp(1) = kfl_timei_chm
  ioutp(2) = momod(modul) % kfl_stead
  call outfor(8_ip,lun_outpu,' ')

end subroutine chm_tistep
