subroutine chm_updtsc(dtmin)
  !-----------------------------------------------------------------------
  !****f* Chemic/chm_updtsc
  ! NAME 
  !    chm_updtsc
  ! DESCRIPTION
  !    This routine computes the critical time step size
  ! USED BY
  !    chm_updtss
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_chemic
  implicit none 
  real(rp),   intent(inout) :: dtmin
  integer(ip)               :: ielem,ipoin,iodes
  integer(ip)               :: pnode,pelty,porde
  real(rp)                  :: dtcri
  real(rp)                  :: elcod(ndime,mnode)
  real(rp)                  :: elcon(mnode,nspec_chm,ncomp_chm)  ! <=> conce
  real(rp)                  :: gpcon(nspec_chm)                  ! <=> conce
  real(rp)                  :: gprea(nspec_chm)                  ! r
  real(rp)                  :: gpdif(nspec_chm)                  ! k
  real(rp)                  :: gprhs(nspec_chm)                  ! f (all terms)
  real(rp)                  :: dummr,dif,adv,rea,chale(2)
  real(rp)                  :: chave(6),hleng(3),tragl(9)
  real(rp)                  :: dummv(ndime,mnode),dumms(mnode,nspec_chm)
  real(rp)                  :: aaa(ndime,mnode,mgaus)
  real(rp)                  :: dummw(mgaus,nspec_chm)
  real(rp)                  :: ccc(ndime,mgaus,nspec_chm)


  if( INOTMASTER ) then
        !
        ! PDE's
        !
        do ielem = 1,nelem
           pelty = ltype(ielem)
           pnode = nnode(pelty)
           porde = lorde(pelty) 
           call chm_elmgat(&
                pnode,lnods(1,ielem),elcod,elcon)                ! Gather operations
           call elmlen(&
                ndime,pnode,elmar(pelty)%dercg,tragl,elcod,&     ! CHALE, HLENG and TRAGL 
                hnatu(pelty),hleng)
           call elmchl(&
                tragl,hleng,elcod,dummr,chave,chale,pnode,&
                porde,hnatu(pelty),kfl_advec_chm,kfl_ellen_chm)

           call chm_elmpre(&
                pnode,1_ip,elcon(:,:,1),dumms,dummv,dumms,dumms,dumms(:,1),dumms(:,1),dumms(:,1),dumms(:,1),dumms(:,1),elmar(pelty)%shape,aaa,aaa(1,:,:), &
                gpcon,ccc(:,:,1),dummw,ccc,dummw,dummv(1,:),dummw(:,1),ccc(:,:,1),dummw(:,1),dummw(:,1),&
                dummw(:,1),dummw(:,1),dummw(:,1),dummw,dummw(:,1),dummw(:,1),dummw(:,1),dummw(:,1),dummw(:,1),dummw(:,1),dummw(:,1))

           do iclas_chm=1,nclas_chm
              call chm_elmpro(&                                  ! Diffusion and reaction
                   '110',iclas_chm,pnode,1_ip,gpcon,elcod,&
                   elmar(pelty)%shacg,gpdif,gprea,gprhs)
              adv = 0.0_rp
              dif = gpdif(1)
              rea = abs(gprea(1))
              call tauadr(&
                   1_ip,staco_chm,adv,dif,rea,&
                   chale(1),chale(2),dtcri)
              dtmin=min(dtmin,dtcri)
           end do
        end do
        !
        ! ODE's
        !
        do ipoin=1,npoin
           call chm_odepro(ipoin,0_ip,nodes_chm,gprea,gprhs)
           do iodes=1,nodes_chm
              if(gprea(iodes)/=0.0_rp) then
                 dtcri=1.0_rp/abs(gprea(iodes))
                 dtmin=min(dtmin,dtcri)
              end if
           end do
        end do

  end if
  !
  ! Look for minimum over subdomains
  !
  call pararr('MIN',0_ip,1_ip,dtmin)
  
end subroutine chm_updtsc
