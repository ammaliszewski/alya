subroutine chm_elmpmb(&
     itask,iclas,pnode,pgaus,elcon,elvel,elcod,gpsha,&
     gpcar,gphes,gpdif,gprea,gprhs,gpadv,gpden,ielem)   
  !-----------------------------------------------------------------------
  !****f* partis/chm_elmpmb
  ! NAME 
  !    chm_elmpmb
  ! DESCRIPTION
  !    Compute properties for class ICLAS for the equation:
  !    rho dui/dt + (a.grad) ui - div[ k grad(ui) ] + s ui = f
  !    GPDIF = k ..... Diffusion
  !    GPREA = s ..... Reaction term
  !    GPRHS = f ..... Source term
  !    GPADV = a ..... Advection term
  !    GPDEN = rho ... Density
  ! USES
  ! USED BY
  !    chm_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp
  use def_domain, only      :  ndime,mgaus,mnode,ntens,lnods
  use def_chemic, only      :  nspec_chm,kfl_timei_chm,dtinv_chm,&
       &                       wprob_chm,proad_chm

  implicit none
  integer(ip),  intent(in)  :: itask,iclas,pnode,pgaus,ielem

  real(rp),     intent(in)  :: elcon(pnode,nspec_chm,*)       ! Unknown
  real(rp),     intent(in)  :: elvel(ndime,pnode)             ! velocity
  real(rp),     intent(in)  :: elcod(ndime,pnode)             ! coordinates

  real(rp),     intent(in)  :: gpsha(pnode,pgaus)             ! N
  real(rp),     intent(in)  :: gpcar(ndime,mnode,pgaus)       ! dN/dxi
  real(rp),     intent(in)  :: gphes(ntens,mnode,pgaus)       ! d^2N/dxidxj

  real(rp),     intent(out) :: gpdif(pgaus)                   ! k
  real(rp),     intent(out) :: gprea(pgaus)                   ! s
  real(rp),     intent(out) :: gprhs(pgaus)                   ! f
  real(rp),     intent(out) :: gpadv(ndime,pgaus)             ! a
  real(rp),     intent(out) :: gpden(pgaus)                   ! rho

  real(rp)                  :: u(pgaus,nspec_chm)             ! u
  real(rp)                  :: gradu(ndime,pgaus,nspec_chm)   ! grad(u)
  real(rp)                  :: laplu(pgaus,nspec_chm)         ! Lapl(u)
  real(rp)                  :: dudt(pgaus,nspec_chm)          ! du/dt
  real(rp)                  :: x(ndime,pgaus)                 ! coordinates

  integer(ip)               :: igaus,inode,idime,ispec,ipoin
  real(rp)                  :: xfact

  real(rp)                  :: Dm      
  real(rp)                  :: Bm1     
  real(rp)                  :: Bm2     
  real(rp)                  :: alpham0 
  real(rp)                  :: alpham  
  real(rp)                  :: betam   
  real(rp)                  :: N       
  real(rp)                  :: alphamb 
  real(rp)                  :: betamb  
  real(rp)                  :: Am        
  real(rp)                  :: Ab         
  real(rp)                  :: Ds1     
  real(rp)                  :: alphac1  
  real(rp)                  :: betac1  
  real(rp)                  :: alphac2 
  real(rp)                  :: betac2  
  real(rp)                  :: p(pgaus)       
  real(rp)                  :: As1     
  real(rp)                  :: Ds2     
  real(rp)                  :: alpham2 
  real(rp)                  :: betam2  
  real(rp)                  :: alphab2 
  real(rp)                  :: betab2  
  real(rp)                  :: As2     
  real(rp)                  :: Dc      
  real(rp)                  :: Hc      
  real(rp)                  :: Ac      
  real(rp)                  :: gradp(ndime,pgaus)

  !----------------------------------------------------------------------
  !
  ! Values at Gauss g for specy i are:
  ! u(g,i)      = ui 
  ! grad(j,g,i) = dui/dxj
  ! laplu(g,i)  = div( grad(ui) )
  ! du/dt(g,i)  = (ui-ui^{n}) / dt
  !
  !----------------------------------------------------------------------

  do ispec = 1,nspec_chm
     do igaus = 1,pgaus

        u(igaus,ispec)     = 0.0_rp
        laplu(igaus,ispec) = 0.0_rp
        dudt(igaus,ispec)  = 0.0_rp
        do idime = 1,ndime
           gradu(idime,igaus,ispec) = 0.0_rp
           x(idime,igaus) = 0.0_rp
        end do

        do inode = 1,pnode
           u(igaus,ispec) = u(igaus,ispec) + gpsha(inode,igaus) * elcon(inode,ispec,1)
           do idime = 1,ndime
              xfact                    = gpcar(idime,inode,igaus) * elcon(inode,ispec,1)
              gradu(idime,igaus,ispec) = gradu(idime,igaus,ispec) + xfact
              laplu(igaus,ispec)       = laplu(igaus,ispec)       + gphes(idime,inode,igaus) * elcon(inode,ispec,1)
              x(idime,igaus)           = x(idime,igaus)           + gpsha(inode,igaus) * elcod(idime,inode)
           end do
        end do
        if( kfl_timei_chm == 1 ) then
           do inode = 1,pnode
              dudt(igaus,ispec) = dudt(igaus,ispec) - gpsha(inode,igaus) * elcon(inode,ispec,2)
           end do
           dudt(igaus,ispec) = dudt(igaus,ispec) * dtinv_chm
        end if

     end do
  end do

  if( wprob_chm == 'OSTE1' ) then

     !-------------------------------------------------------------------
     !
     ! Problem 1: OSTEOBLASTS 
     !
     ! PDE's: u1 = m, u2 = s1, u3 = s2, u4 = c
     ! ODE's: u5 = b, u6 = vf, u7 = vw, u8 = vl
     !
     !--------------------------------------------------------------------

     Dm      = 0.133_rp 
     Bm1     = 0.667e6_rp  
     Bm2     = 0.167e6_rp  
     alpham0 = 0.25_rp   
     alpham  = 0.25_rp  
     betam   = 10.0e-6_rp   
     N       = 1.0e3_rp   
     alphamb = 0.5_rp    
     betamb  = 10.0e-6_rp     
     Am      = 2.0e-3_rp      
     Ab      = 6.67e-3_rp    
     Ds1     = 0.3_rp 
     alphac1 = 6.67e-8_rp  
     betac1  = 0.1_rp  
     alphac2 = 1.0e-8_rp   
     betac2  = 10.0e-6_rp  
     As1     = 10.0_rp  
     Ds2     = 0.1_rp  
     alpham2 = 2.5e-6_rp  
     betam2  = 10.0e-6_rp  
     alphab2 = 2.5e-6_rp  
     betab2  = 10.0e-6_rp  
     As2     = 10.0_rp   
     Dc      = 1.365e-2_rp  
     Hc      = 0.333_rp  
     Ac      = 0.067_rp 

     do igaus = 1,pgaus  
        gpden(igaus) = 1.0_rp
        p(igaus) = 0.0_rp
     end do
     do igaus = 1,pgaus  
        gprea(igaus) = 0.0_rp
        gpdif(igaus) = 0.0_rp
        do idime = 1,ndime
           gpadv(idime,igaus) = 0.0_rp
           gradp(idime,igaus) = 0.0_rp
        end do
        gprhs(igaus) = 0.0_rp
     end do

     do inode = 1,pnode
        ipoin = lnods(inode,ielem)
        do igaus = 1,pgaus        
           p(igaus) = p(igaus) + proad_chm(ipoin) * gpsha(inode,igaus)
           do idime = 1,ndime
              gradp(idime,igaus) = gradp(idime,igaus) + proad_chm(ipoin) * gpcar(idime,inode,igaus)
           end do
        end do 
     end do

     if( iclas == 1 ) then
        !
        ! Osteogenic cells m
        !
        do igaus = 1,pgaus  

           xfact =   ( alpham0 + alpham*u(igaus,2)/(betam+u(igaus,2)) + alpham*u(igaus,3)/(betam+u(igaus,3)) )&
                &  * ( 1.0_rp - u(igaus,1) / N ) &
                &  - alphamb*u(igaus,2)/(betamb+u(igaus,2)) &
                &  - Am - ( Bm1 * laplu(igaus,2) + Bm2 * laplu(igaus,3) )

           if( xfact < 0.0_rp ) then
              gprea(igaus) = -xfact
              gprhs(igaus) =  0.0_rp
           else
              gprea(igaus) =  0.0_rp 
              gprhs(igaus) =  xfact*u(igaus,1)
           end if
           gpdif(igaus) = Dm
           do idime = 1,ndime
              gpadv(idime,igaus) = Bm1 * gradu(idime,igaus,2) + Bm2 * gradu(idime,igaus,3)
              !gprhs(igaus) = gprhs(igaus) - gradu(idime,igaus,1) * ( Bm1 * gradu(idime,igaus,2) + Bm2 * gradu(idime,igaus,3))
           end do

        end do

     else if( iclas == 2 ) then
        !
        ! Generic growth 1 s1
        !
        do igaus = 1,pgaus  
           gprea(igaus) = As1
           gpdif(igaus) = Ds1
           gprhs(igaus) = ( alphac1*p(igaus)/(betac1+p(igaus)) + alphac2*p(igaus)/(betac2+p(igaus)) ) * u(igaus,4) 
        end do

     else if( iclas == 3 ) then
        !
        ! Generic growth 2 s2
        !
        do igaus = 1,pgaus  
           gprea(igaus) = As2
           gpdif(igaus) = Ds2
           gprhs(igaus) =   alpham2 * u(igaus,3) / ( betam2 + u(igaus,3) ) * u(igaus,1) &
                &         + alphab2 * u(igaus,3) / ( betab2 + u(igaus,3) ) * u(igaus,5)
        end do

     else if( iclas == 4 ) then
        !
        ! Platelets c
        !
        do igaus = 1,pgaus             
           do idime = 1,ndime
              gpadv(idime,igaus) = Hc * gradp(idime,igaus)
           end do
           gprea(igaus) = Ac
           gpdif(igaus) = Dc
        end do

     end if

  end if

end subroutine chm_elmpmb
