subroutine chm_elmprc_cfi(&
  iclas,pgaus,gpcon,gpden,gpgde,gpDik,gpgDk,gpmas,gpvel,gpvec,gpgve,&
       gphco,gpsph,gptur,gpadv,gpdif,gpgrd,gprea,gprhs,gpmol,gpgmo,&
       gphmo,gpgac,gplac,gpdiv,gpdis,gpprd,gprrt)

  !-----------------------------------------------------------------------
  !****f* partis/chm_elmprc
  ! NAME 
  !    chm_elmprc
  ! DESCRIPTION
  !    Compute terms for each species ADR equation 
  ! USES
  ! USED BY
  !    chm_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only      :  ip,rp
  use def_domain, only      :  ndime,mgaus
  use def_chemic, only      :  nspec_chm,kfl_react_chm,kfl_diffu_chm,kfl_activ_chm,kfl_cotur_chm,diffu_chm
  use def_master, only      :  speci
  implicit none
  integer(ip),  intent(in)  :: iclas,pgaus
  real(rp),     intent(in)  :: gpden(pgaus),gpgde(ndime,pgaus)
  real(rp),     intent(in)  :: gpcon(pgaus,nspec_chm)
  real(rp),     intent(in)  :: gpDik(pgaus,nspec_chm)                ! Species diffusion coeff
  real(rp),     intent(in)  :: gpgDk(ndime,pgaus,nspec_chm)          ! Grad of diffusion coeff
  real(rp),     intent(in)  :: gpmas(pgaus,nspec_chm)                ! Species source term (reaction rate)
  real(rp),     intent(in)  :: gpvel(ndime,pgaus),gpvec(ndime,pgaus) ! Velocity and correction velocity
  real(rp),     intent(in)  :: gpgve(pgaus)                          ! Gradient of correction velocity
  real(rp),     intent(in)  :: gpdiv(pgaus)                          ! Divergence
  real(rp),     intent(in)  :: gpdis(pgaus)                          ! Dissipation rate term in the CFI model
  real(rp),     intent(in)  :: gpprd(pgaus,nspec_chm)                ! Production term in the CFI model
  real(rp),     intent(in)  :: gprrt(pgaus)                          ! Transport of reaction rate fluctuations for LES
  real(rp),     intent(in)  :: gphco(pgaus)                          ! heat conductivity 
  real(rp),     intent(in)  :: gpsph(pgaus)                          ! specific heat capacity
  real(rp),     intent(in)  :: gptur(pgaus)                          ! turbulent viscosity

  real(rp), intent(in)      :: gpmol(pgaus)
  real(rp), intent(in)      :: gpgmo(ndime,pgaus)
  real(rp), intent(in)      :: gphmo(pgaus)
  real(rp), intent(in)      :: gpgac(ndime,pgaus,nspec_chm)          ! Gradient(activity) / activity
  real(rp), intent(in)      :: gplac(pgaus,nspec_chm)                ! Laplacian(activity)/activity

  real(rp),     intent(out) :: gpadv(ndime,pgaus)
  real(rp),     intent(out) :: gpdif(pgaus)
  real(rp),     intent(out) :: gpgrd(ndime,pgaus)
  real(rp),     intent(out) :: gprea(pgaus)
  real(rp),     intent(out) :: gprhs(pgaus)
  integer(ip)               :: igaus,idime
  
  do igaus = 1,pgaus
     !
     ! Mass source term
     !
     gprhs(igaus) = gpmas(igaus,iclas) + gpprd(igaus,iclas)  
     !
     ! Diffusion coefficient
     !
     gpdif(igaus) = gphco(igaus) / gpsph(igaus)

     if (kfl_cotur_chm > 0_ip) then
       gpdif(igaus) = gpdif(igaus) + gptur(igaus) / diffu_chm(1,1)
     else if (kfl_cotur_chm < 0_ip) then
       gpdif(igaus) = gpdif(igaus) + ( gptur(igaus) * gpden(igaus) ) / diffu_chm(1,1)
     end if

     !
     ! Diffusion gradient
     !
     do idime = 1,ndime
       gpgrd(idime,igaus)  =  gpgDk(idime,igaus,iclas)
     enddo
     !
     ! Reaction
     !
     if (iclas == 2_ip .or. iclas == 4_ip ) then
        if (kfl_react_chm == 1) then
           gprea(igaus) = gpden(igaus) * gpdis(igaus)
        else ! We assemble reaction term on the rhs
           gprea(igaus) = 0.0_rp
           gprhs(igaus) = gprhs(igaus) - gpden(igaus) * gpdis(igaus) * gpcon(igaus,iclas) 
        endif
     endif
     if (iclas == 2_ip) gprhs(igaus) = gprhs(igaus) + gprrt(igaus)
     ! 
     ! Advection
     !
     do idime = 1,ndime
        gpadv(idime,igaus) = gpvel(idime,igaus) 
     enddo
  enddo

end subroutine chm_elmprc_cfi
