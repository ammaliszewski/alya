subroutine rad_timste()
  !-----------------------------------------------------------------------
  !****f* Radiat/rad_timste
  ! NAME 
  !    rad_begste
  ! DESCRIPTION
  !    This routine prepares for a new time step of the heat transfer
  !    equation --- NOT USED ANYMORE UNTIL WE PUT TIME DEPENDENCE AGAIN
  ! USES
  !   
  ! USED BY
  !    Radiat
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_radiat
  implicit none

end subroutine rad_timste

