subroutine rad_tistep
  !-----------------------------------------------------------------------
  !****f* Radiat/rad_tistep
  ! NAME 
  !    rad_tittim
  ! DESCRIPTION
  !    This routine sets the time step
  ! USES
  ! USED BY
  !    rad_begite
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_radiat
  implicit none

  dtinv_rad=0.0_rp

end subroutine rad_tistep
