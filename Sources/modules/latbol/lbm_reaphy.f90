subroutine lbm_reaphy
!-----------------------------------------------------------------------
!****f* Latbol/lbm_reaphy
! NAME 
!    lbm_reaphy
! DESCRIPTION
!    This routine reads the physical problem definition.
! USES
!    listen
!    memchk
!    runend
! USED BY
!    lbm_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      Mod_memchk

  use      def_latbol

  implicit none
  integer(ip) :: imate,istat,npara,ielem,iipar,idime,icomo,ibase
  real(rp)    :: xauxi,yauxi
!
! Read and write files
!
  lispa = 0
  lisda = lun_pdata_lbm                                       ! Reading file
  lisre = lun_outpu_lbm                                       ! Writing file
!
! Initializations (defaults)
!
  kfl_stead_lbm = 0                                    ! Steady-state

  kfl_genal_lbm = 1                                    ! Explicit algorithm

  kfl_cemod_lbm = 1                                    ! Monodomain

  nmate_lbm     = 1

!
! Unknowns point-wise dimensions
!
  nbase_lbm = 9
  if (ndime == 3)  nbase_lbm = 15
!
! Reach the section
!
  rewind(lisda)
  call listen('lbm_reaphy')
  do while(words(1)/='PHYSI')
     call listen('lbm_reaphy')
  end do
!
! Begin to read data
!
  do while(words(1)/='ENDPH')
     call listen('lbm_reaphy')
     if(words(1)=='PROBL') then
!
! Problem definition data
!
        call listen('lbm_reaphy')
        do while(words(1)/='ENDPR')
           if(words(1)=='TEMPO') then                    ! Temporal evolution
              if(exists('ON   ')) kfl_timei_lbm = 1 
              
           end if
           
           call listen('lbm_reaphy')
        end do
     else if(words(1)=='PROPE') then
!
! Properties
!               
        if(words(2)=='MATER') &
             nmate_lbm=getint('MATER',1,'#Number of materials')
        
        allocate(weieq_lbm(20,nmate_lbm),stat=istat)
        call memchk(zero,istat,memor_dom,'WEIEQ_LBM','lbm_reaphy',weieq_lbm)
        allocate(xpaeq_lbm(10,nmate_lbm),stat=istat)
        call memchk(zero,istat,memor_dom,'XPAEQ_LBM','lbm_reaphy',xpaeq_lbm)

        allocate(vbase_lbm(ndime,30),stat=istat)
        call memchk(zero,istat,memor_dom,'VBASE_LBM','lbm_reaphy',vbase_lbm)
        allocate(lbare_lbm(30,5),stat=istat)
        call memchk(zero,istat,memor_dom,'LBARE_LBM','lbm_reaphy',lbare_lbm)

        weieq_lbm = 0.0_rp
        xpaeq_lbm = 0.0_rp 
        vbase_lbm = 0.0_rp 
        
        if (ndime == 2) then

           weieq_lbm(1,1:nmate_lbm)= 4.0_rp / 9.0_rp
           weieq_lbm(2,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm(3,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm(4,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm(5,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm(6,1:nmate_lbm)= 1.0_rp / 36.0_rp
           weieq_lbm(7,1:nmate_lbm)= 1.0_rp / 36.0_rp
           weieq_lbm(8,1:nmate_lbm)= 1.0_rp / 36.0_rp
           weieq_lbm(9,1:nmate_lbm)= 1.0_rp / 36.0_rp

           vbase_lbm(1,1) =  0.0_rp
           vbase_lbm(2,1) =  0.0_rp
           
           vbase_lbm(1,2) =  1.0_rp
           vbase_lbm(2,2) =  0.0_rp
           
           vbase_lbm(1,3) = -1.0_rp
           vbase_lbm(2,3) =  0.0_rp
           
           vbase_lbm(1,4) =  0.0_rp
           vbase_lbm(2,4) =  1.0_rp
           
           vbase_lbm(1,5) =  0.0_rp
           vbase_lbm(2,5) = -1.0_rp
           
           vbase_lbm(1,6) = -1.0_rp
           vbase_lbm(2,6) = -1.0_rp
           
           vbase_lbm(1,7) =  1.0_rp
           vbase_lbm(2,7) =  1.0_rp
           
           vbase_lbm(1,8) =  1.0_rp
           vbase_lbm(2,8) = -1.0_rp
           
           vbase_lbm(1,9) = -1.0_rp
           vbase_lbm(2,9) =  1.0_rp
           
        else if (ndime == 3) then

           weieq_lbm( 1,1:nmate_lbm)= 2.0_rp / 9.0_rp
           weieq_lbm( 2,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm( 3,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm( 4,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm( 5,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm( 6,1:nmate_lbm)= 1.0_rp / 72.0_rp
           weieq_lbm( 7,1:nmate_lbm)= 1.0_rp / 72.0_rp
           weieq_lbm( 8,1:nmate_lbm)= 1.0_rp / 72.0_rp
           weieq_lbm( 9,1:nmate_lbm)= 1.0_rp / 72.0_rp
           weieq_lbm(10,1:nmate_lbm)= 1.0_rp / 72.0_rp
           weieq_lbm(11,1:nmate_lbm)= 1.0_rp / 72.0_rp
           weieq_lbm(12,1:nmate_lbm)= 1.0_rp / 72.0_rp
           weieq_lbm(13,1:nmate_lbm)= 1.0_rp / 72.0_rp
           weieq_lbm(14,1:nmate_lbm)= 1.0_rp / 9.0_rp
           weieq_lbm(15,1:nmate_lbm)= 1.0_rp / 9.0_rp           

           vbase_lbm(1,1) =  0.0_rp
           vbase_lbm(2,1) =  0.0_rp
           vbase_lbm(3,1) =  0.0_rp
           
           vbase_lbm(1,2) =  1.0_rp
           vbase_lbm(2,2) =  0.0_rp
           vbase_lbm(3,2) =  0.0_rp
           
           vbase_lbm(1,3) = -1.0_rp
           vbase_lbm(2,3) =  0.0_rp
           vbase_lbm(3,3) =  0.0_rp
           
           vbase_lbm(1,4) =  0.0_rp
           vbase_lbm(2,4) =  1.0_rp
           vbase_lbm(3,4) =  0.0_rp
           
           vbase_lbm(1,5) =  0.0_rp
           vbase_lbm(2,5) = -1.0_rp
           vbase_lbm(3,5) =  0.0_rp
           
           vbase_lbm(1,6) = -1.0_rp
           vbase_lbm(2,6) = -1.0_rp
           vbase_lbm(3,6) =  1.0_rp
           
           vbase_lbm(1,7) =  1.0_rp
           vbase_lbm(2,7) =  1.0_rp
           vbase_lbm(3,7) = -1.0_rp
           
           vbase_lbm(1,8) =  1.0_rp
           vbase_lbm(2,8) = -1.0_rp
           vbase_lbm(3,8) = -1.0_rp
           
           vbase_lbm(1,9) = -1.0_rp
           vbase_lbm(2,9) =  1.0_rp
           vbase_lbm(3,9) =  1.0_rp
           
           vbase_lbm(1,10) =  1.0_rp
           vbase_lbm(2,10) =  1.0_rp
           vbase_lbm(3,10) =  1.0_rp
           
           vbase_lbm(1,11) = -1.0_rp
           vbase_lbm(2,11) = -1.0_rp
           vbase_lbm(3,11) = -1.0_rp
           
           vbase_lbm(1,12) = -1.0_rp
           vbase_lbm(2,12) =  1.0_rp
           vbase_lbm(3,12) = -1.0_rp
           
           vbase_lbm(1,13) =  1.0_rp
           vbase_lbm(2,13) = -1.0_rp
           vbase_lbm(3,13) =  1.0_rp
           
           vbase_lbm(1,14) =  0.0_rp
           vbase_lbm(2,14) =  0.0_rp
           vbase_lbm(3,14) =  1.0_rp
           
           vbase_lbm(1,15) =  0.0_rp
           vbase_lbm(2,15) =  0.0_rp
           vbase_lbm(3,15) = -1.0_rp
           

           
        end if


! Normalize (momentarily)

        do ibase=2,nbase_lbm
           xauxi=0.0_rp
           do idime=1,ndime
              xauxi= xauxi + vbase_lbm(idime,ibase)*vbase_lbm(idime,ibase)
           end do
           xauxi = sqrt(xauxi)
           vbase_lbm(1:ndime,ibase) = vbase_lbm(1:ndime,ibase) / xauxi
        end do

        xpaeq_lbm( 1,1:nmate_lbm)= 1.0_rp 
        xpaeq_lbm( 2,1:nmate_lbm)= 3.0_rp 
        xpaeq_lbm( 3,1:nmate_lbm)= 9.0_rp / 2.0_rp
        xpaeq_lbm( 4,1:nmate_lbm)= 3.0_rp / 2.0_rp
        
        call listen('lbm_reaphy')
        do while(words(1)/='ENDPR')
           
           if(words(1)=='VISCO') then                    ! Viscosity, reference value
              viref_lbm = param(1)
           else if(words(1)=='DENSI') then               ! Density, reference value
              deref_lbm = param(1)

!!           else if(words(1)=='MATER') then               ! FALTA HACER ESTO
!!!
!!!       The following values are defined on a per-material basis
!!!
!!              call listen('lbm_reaphy')
!!              do while(words(1)/='ENDMA')
!!                 ielem=int(param(1))
!!                 weieq_lbm(ielem)=int(param(2))
!!                 call listen('lbm_reaphy')
!!              end do

           end if

           call listen('lbm_reaphy')

        end do
     end if
  end do
  where(weieq_lbm==0) weieq_lbm=1
  ndofn_lbm = ndime+2
  ndof2_lbm = ndofn_lbm*ndofn_lbm
!
! Initialization
!
  if(kfl_stead_lbm==0) dtinv_lbm=0.0_rp

      
end subroutine lbm_reaphy
