subroutine lbm_reabcs
!-----------------------------------------------------------------------
!****f* Latbol/lbm_reabcs
! NAME 
!    lbm_reabcs
! DESCRIPTION
!    This routine reads the boundary conditions... 
!
! USES
!    lbm_bcntoe
!    listen
!    memchk
!    runend
! USED BY
!    lbm_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      Mod_memchk

  use      def_latbol

  implicit none
  integer(ip) :: ipoin,pnodb,iboun,inodb,istat,icode,idofn
  real(rp)    :: rcode
!
! Initializations
!
  memor_lbm(1) = 0
  memor_lbm(2) = 0
!
! Allocate memory
!
  allocate(kfl_fixno_lbm(ndofn_lbm,npoin),stat=istat)
  call memchk(zero,istat,memor_lbm,'KFL_FIXNO_LBM','lbm_reabcs',kfl_fixno_lbm)
  allocate(kfl_fixbo_lbm(nboun),stat=istat)
  call memchk(zero,istat,memor_lbm,'KFL_FIXBO_LBM','lbm_reabcs',kfl_fixbo_lbm)
  allocate(bvess_lbm(ndofn_lbm,npoin),stat=istat)
  call memchk(zero,istat,memor_lbm,'BVESS_LBM',    'lbm_reabcs',bvess_lbm)
  allocate(bvnat_lbm(nboun),stat=istat)
  call memchk(zero,istat,memor_lbm,'BVNAT_LBM',    'lbm_reabcs',bvnat_lbm)
!
! Reach the nodal-wise section.
!
  rewind(lisda)
  call listen('lbm_reabcs')
  do while(words(1)/='BOUND')
     call listen('lbm_reabcs')
  end do
!
! Read data.
!
  call listen('lbm_reabcs')
  if(kfl_rstar_lbm==1) then
     if(words(1)/='THIS ') call runend('THIS IS NOT A RE-START FILE')
     call listen('lbm_reabcs')
  else
     if(words(1)=='THIS ') call runend('THIS IS A RE-START FILE')
  end if
  do while(words(1)/='ENDBO')
     if(words(1)=='ONNOD') then
        call listen('lbm_reabcs')
        do while(words(1)/='ENDON')
           ipoin                = int(param(1))
           kfl_fixno_lbm(1,ipoin) = int(param(2))
           call codfix(ndime,kfl_fixno_lbm(1,ipoin))
           bvess_lbm(1:ndofn_lbm,ipoin)  =     param(3:2+ndofn_lbm)
           call listen('lbm_reabcs')
        end do
     else if(words(1)=='ONBOU') then
        call listen('lbm_reabcs')
        do while(words(1)/='ENDON')
           iboun=int(param(1))
           kfl_fixbo_lbm(iboun)=int(param(2))
           pnodb=nnodb(ltypb(iboun))
           if(kfl_fixbo_lbm(iboun)==2) then
              allocate(bvnat_lbm(iboun)%a(1,pnodb),stat=istat)
              call memchk(zero,istat,memor_dom,'BVNAT_LBM','lbm_reabcs',bvnat_lbm(iboun)%a)
              do inodb=1,pnodb
                 bvnat_lbm(iboun)%a(1,inodb)=param(inodb+2)
              end do
           else if(kfl_fixbo_lbm(iboun)==3) then
              allocate(bvnat_lbm(iboun)%a(3,pnodb),stat=istat)
              call memchk(zero,istat,memor_dom,'BVNAT_LBM','lbm_reabcs',bvnat_lbm(iboun)%a)
              do inodb=1,pnodb
                 bvnat_lbm(iboun)%a(1,inodb)=param(1+(inodb-1)*3+2)
                 bvnat_lbm(iboun)%a(2,inodb)=param(1+(inodb-1)*3+3)
                 bvnat_lbm(iboun)%a(3,inodb)=param(1+(inodb-1)*3+4)
              end do
           end if
           call listen('lbm_reabcs')
        end do
     else
        ipoin                  = int(param(1))
        kfl_fixno_lbm(1,ipoin) = int(param(2))
        bvess_lbm(1:ndofn_lbm,ipoin)  =     param(3:2+ndofn_lbm)
     end if
     call listen('lbm_reabcs')
  end do
  
end subroutine lbm_reabcs
