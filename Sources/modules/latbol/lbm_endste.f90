subroutine lbm_endste

!-----------------------------------------------------------------------
!
! Compute RHS
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_latbol
  implicit none
!
! Compute convergence residual of the time evolution (that is,
! || u(n,*,*) - u(n-1,*,*)|| / ||u(n,*,*)||) and update unknowns
! u(n-1,*,*) <-- u(n,*,*) 
    call lbm_cvgunk(three)
    call lbm_updunk( five)
!
! Output results corresponding to the end of a time step
!
  call lbm_output(zero)
!
! Write restart file
!
  !call tem_restar(two)
!
! If not steady, go on
!
  if(kfl_stead_lbm==0) then
     if(kfl_timei_lbm==1) kfl_gotim = 1
  end if
  
end subroutine lbm_endste
