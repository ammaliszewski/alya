subroutine lbm_buidom
!-----------------------------------------------------------------------
!****f* Latbol/lbm_buidom
! NAME 
!    lbm_buidom
! DESCRIPTION
!    This routine builds some usual domain arrays der
! USED BY
!    lbm_iniunk
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_latbol
  use      Mod_memchk
  implicit none
  integer(ip) :: ipoin,jpoin,idofn,jcode,izdom,ibase,jbase,kbase
  integer(ip) :: ielem,ielty,inode,jnode,pnode,istat,idime
  real(rp)    :: xproj,cproj,xdimi,xdist,cdist,vdife(3),wbase(3)
!
! Allocate memory for the arrays of reordered neighbors indexes and build it
!
  allocate(lneba_lbm(nzdom),stat=istat)
  call memchk(zero,istat,memor_lbm,'LNEBA_LBM','lbm_iniunk',lneba_lbm)
  allocate(lcoba_lbm(nbase_lbm,npoin),stat=istat)
  call memchk(zero,istat,memor_lbm,'LCOBA_LBM','lbm_iniunk',lcoba_lbm)

  cproj= 0.95_rp
  vdife= 0.0_rp
  
  lneba_lbm = 0
  lcoba_lbm = 0
  do ipoin = 1,npoin
     do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
        jpoin          = c_dom(izdom)
        if(ipoin/=jpoin) then
           vdife(1:ndime) = coord(1:ndime,jpoin)-coord(1:ndime,ipoin)                   
           xproj= sqrt(dot_product(vdife(1:ndime),vdife(1:ndime)))
           vdife=vdife/xproj           
           do ibase=2,nbase_lbm
              xproj= dot_product(vdife(1:ndime),vbase_lbm(1:ndime,ibase))
              if (xproj > cproj) then
                 lneba_lbm(izdom)=ibase
                 lcoba_lbm(ibase,ipoin)=jpoin
              end if
           end do
        else
           lneba_lbm(izdom)=1
           lcoba_lbm(1,ipoin)=jpoin
        end if
     end do
  end do

!
! Compute the base relations: opposite and four closest ones
!
  
  lbare_lbm = 0
  lbare_lbm(1,1)=1
  do ibase= 2,nbase_lbm
     wbase(1:ndime) = - vbase_lbm(1:ndime,ibase)
     do jbase= 1,nbase_lbm
        if (jbase/=ibase) then
           xproj= dot_product(wbase(1:ndime),vbase_lbm(1:ndime,jbase))          
           if (xproj > cproj) then
              lbare_lbm(ibase,1)=jbase
           end if
        end if
     end do
  end do

! Unnormalize (correcting what done in lbm_reaphy)
  
  if (ndime == 2) then
     
     vbase_lbm(1,1) =  0.0_rp
     vbase_lbm(2,1) =  0.0_rp

     vbase_lbm(1,2) =  1.0_rp
     vbase_lbm(2,2) =  0.0_rp

     vbase_lbm(1,3) = -1.0_rp
     vbase_lbm(2,3) =  0.0_rp

     vbase_lbm(1,4) =  0.0_rp
     vbase_lbm(2,4) =  1.0_rp

     vbase_lbm(1,5) =  0.0_rp
     vbase_lbm(2,5) = -1.0_rp

     vbase_lbm(1,6) = -1.0_rp
     vbase_lbm(2,6) = -1.0_rp

     vbase_lbm(1,7) =  1.0_rp
     vbase_lbm(2,7) =  1.0_rp

     vbase_lbm(1,8) =  1.0_rp
     vbase_lbm(2,8) = -1.0_rp

     vbase_lbm(1,9) = -1.0_rp
     vbase_lbm(2,9) =  1.0_rp

     
  else if (ndime == 3) then
     

     vbase_lbm(1,1) =  0.0_rp
     vbase_lbm(2,1) =  0.0_rp
     vbase_lbm(3,1) =  0.0_rp

     vbase_lbm(1,2) =  1.0_rp
     vbase_lbm(2,2) =  0.0_rp
     vbase_lbm(3,2) =  0.0_rp

     vbase_lbm(1,3) = -1.0_rp
     vbase_lbm(2,3) =  0.0_rp
     vbase_lbm(3,3) =  0.0_rp

     vbase_lbm(1,4) =  0.0_rp
     vbase_lbm(2,4) =  1.0_rp
     vbase_lbm(3,4) =  0.0_rp

     vbase_lbm(1,5) =  0.0_rp
     vbase_lbm(2,5) = -1.0_rp
     vbase_lbm(3,5) =  0.0_rp

     vbase_lbm(1,6) = -1.0_rp
     vbase_lbm(2,6) = -1.0_rp
     vbase_lbm(3,6) =  1.0_rp

     vbase_lbm(1,7) =  1.0_rp
     vbase_lbm(2,7) =  1.0_rp
     vbase_lbm(3,7) = -1.0_rp

     vbase_lbm(1,8) =  1.0_rp
     vbase_lbm(2,8) = -1.0_rp
     vbase_lbm(3,8) = -1.0_rp

     vbase_lbm(1,9) = -1.0_rp
     vbase_lbm(2,9) =  1.0_rp
     vbase_lbm(3,9) =  1.0_rp

     vbase_lbm(1,10) =  1.0_rp
     vbase_lbm(2,10) =  1.0_rp
     vbase_lbm(3,10) =  1.0_rp

     vbase_lbm(1,11) = -1.0_rp
     vbase_lbm(2,11) = -1.0_rp
     vbase_lbm(3,11) = -1.0_rp

     vbase_lbm(1,12) = -1.0_rp
     vbase_lbm(2,12) =  1.0_rp
     vbase_lbm(3,12) = -1.0_rp

     vbase_lbm(1,13) =  1.0_rp
     vbase_lbm(2,13) = -1.0_rp
     vbase_lbm(3,13) =  1.0_rp

     vbase_lbm(1,14) =  0.0_rp
     vbase_lbm(2,14) =  0.0_rp
     vbase_lbm(3,14) =  1.0_rp

     vbase_lbm(1,15) =  0.0_rp
     vbase_lbm(2,15) =  0.0_rp
     vbase_lbm(3,15) = -1.0_rp

  end if

!
! Compute the internodal distance of the lattice (when needed) and its
! characteristic speed
!
  xlatt_lbm= 1e08
  
  if (kfl_latyp_lbm == 1) then
     !
     ! Take just the first element
     !
     elements: do ielem=1,nelem
        
        ielty=ltype(ielem)
        pnode=nnode(ielty)
        do inode=1,pnode
           ipoin = lnods(  1)%l(inode)
           do jnode=1,pnode
              jpoin = lnods(  1)%l(jnode)
              if(ipoin/=jpoin) then
                 xdist = 0.0_rp
                 do idime=1,ndime
                    cdist = coord(idime,ipoin) - coord(idime,jpoin)
                    xdist = xdist + cdist * cdist
                 end do
                 if (xdist < xlatt_lbm) xlatt_lbm = xdist
              end if
           end do
        end do

     end do elements

  end if
 
end subroutine lbm_buidom
