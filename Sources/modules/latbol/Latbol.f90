subroutine Latbol(order)
!-----------------------------------------------------------------------
!****f* latbol/Latbol
! NAME 
!    Latbol
! DESCRIPTION
!    This is the main subroutine for the Lattice-Boltzmann Model (LBM)
!    for fluid dynamics
! USES
!    lbm_turnon
!    lbm_begste
!    lbm_doiter
!    lbm_gencon
!    lbm_endste
!    lbm_turnof
! USED BY
!    Turnon
!    Begste
!    Doiter
!    Gencon
!    Endste
!    Turnof
!***
!
!    The task done corresponds to the order given by the master.
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  implicit none
  integer(ip) :: order
  select case (order)

  case(ITASK_TURNON)
     if(kfl_modul(modul)/=0) call lbm_turnon
  case(ITASK_BEGSTE) 
     if(kfl_modul(modul)/=0) call lbm_begste
  case(ITASK_DOITER)
     if(kfl_modul(modul)/=0) call lbm_doiter
  case(ITASK_CONCOU)
     if(kfl_modul(modul)/=0) call lbm_concou
  case(ITASK_CONBLK)
     !if(kfl_modul(modul)/=0) call lbm_conblk
  case(ITASK_NEWMSH)
     !if(kfl_modul(modul)/=0) call lbm_newmsh
  case(ITASK_ENDSTE)
     if(kfl_modul(modul)/=0) call lbm_endste
  case(ITASK_TURNOF)
     !if(kfl_modul(modul)/=0) call lbm_turnof
  end select

  
end subroutine Latbol
      
