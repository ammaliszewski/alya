subroutine lbm_turnon
!-----------------------------------------------------------------------
!****f* Latbol/lbm_turnon
! NAME 
!    lbm_turnon
! DESCRIPTION
!    This routine performs the following tasks:
!    - Gets file names and open them.
!    - Read some data for the run
!    - Write some info
!    - Allocate memory
! USES
!    lbm_openfi
!    lbm_reaphy
!    lbm_reabcs
!    lbm_reanut
!    lbm_reaous
!    lbm_outinf
!    lbm_memall
!    lbm_massma
! USED BY
!    Latbol
!***
!-----------------------------------------------------------------------

  use def_parame
  implicit none
  !
  ! Open most of the files
  !
  call lbm_openfi(one)
  !
  ! Read the physical problem
  !
  call lbm_reaphy
  !
  ! Read the numerical treatment
  !
  call lbm_reanut
  !
  ! Read the output strategy
  !
  call lbm_reaous
  !
  ! Service: Parall
  !
!!!!  call lbm_parall(one)  
  !
  !
  ! Read the boundary conditions
  !
  call lbm_reabcs
  !
  ! Service: Parall
  !
!!!!  call lbm_parall(two)  
  !
  ! Write info
  !
  call lbm_outinf
  !
  ! Allocate info
  !
  call lbm_memall
  !
  ! Build geometrical arrays
  !
  call lbm_buidom
  !
  ! Open report files when needed
  !
  call lbm_openfi(two)
  !
  ! Close input data file 
  !
  call lbm_openfi(three)
  
end subroutine lbm_turnon
