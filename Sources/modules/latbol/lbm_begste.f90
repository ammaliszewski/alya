subroutine lbm_begste
!-----------------------------------------------------------------------
!****f* Latbol/lbm_begste
! NAME 
!    lbm_begste
! DESCRIPTION
!    This routine prepares for a new time step 
! USES
!    lbm_iniunk
!    lbm_updtss
!    lbm_updbcs
!    lbm_updunk
! USED BY
!    Latbol
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_latbol
  implicit none

  if(istep>ndela_lbm) kfl_delay_lbm=0
!
! For the first time step, set up the initial condition and initialize
! STST_LBM.     
!
  
  if(istep==1) then
     kfl_stead_lbm = 0
     call lbm_iniunk
  end if
  if(kfl_stead_lbm==1) return
!
! Time step size.
!
  if(kfl_timei_lbm==1) call lbm_updtss     
!
! Load boundary conditions and compute force terms for the present time step.
!
  call lbm_updbcs
!
! Initial guess fo the unknowns: U(n,0,*) <-- U(n-1,*,*).
!
  call lbm_updunk(one)
!
! Write title in the log and solver files.
!
  write(lun_solve_lbm,100) istep
!
! Formats.
!
100 format(/,5x,'>>>  TIME STEP NUMBER: ',i5)
  


end subroutine lbm_begste
