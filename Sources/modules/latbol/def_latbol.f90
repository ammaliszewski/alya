module def_latbol

!-----------------------------------------------------------------------
!    
! Heading for the module subroutines
!
!-----------------------------------------------------------------------
  use def_kintyp
!
! Parameters
!
  integer(ip), parameter ::&
       lun_pdata_lbm = 801, lun_outpu_lbm = 802, lun_conve_lbm = 803,&
       lun_solve_lbm = 804, lun_rstar_lbm = 805
  character(150)         :: &
       fil_rstar_lbm
  real(rp), parameter :: &
       zelbm = epsilon(1.0_rp)
!------------------------------------------------------------------------
! Physical problem: read, modified or defined in lbm_reaphy
!------------------------------------------------------------------------
!
! General data
!
!--BEGIN REA GROUP
  integer(ip) ::         &
       kfl_rstar_lbn,    &       ! LBM is a re-start
       kfl_conve_lbm,    &       ! LBM convergence required 
       ncomp_lbm,        &       ! Number of components of the temperature
       ndofn_lbm,        &
       ndof2_lbm
!
! Problem data and flags
!
  integer(ip) ::&
       kfl_genal_lbm                ! General algorithm type

  integer(ip) ::&
       kfl_rstar_lbm,&              ! Restart
       kfl_latyp_lbm,&              ! Lattice type
       kfl_timei_lbm,&              ! Existence of dT/dt
       kfl_weigh_lbm,&              ! Weighting of dT/dt
       kfl_goite_lbm,&              ! Keep iterating
       kfl_stead_lbm,&              ! Steady-state has been reached 
       kfl_shock_lbm,&              ! Shock capturing type 
       kfl_tiacc_lbm,&              ! Temporal accuracy
       kfl_tiext_lbm,&              ! Externally fixed time step
       kfl_normc_lbm,&              ! Norm of convergence
       kfl_algso_lbm,&              ! Type of algebraic solver
       kfl_exacs_lbm,&              ! Exact solution for the propagation eq.
       kfl_splot_lbm,&              ! Output for 3D gnuplot flag
       kfl_repor_lbm,&              ! Report flag
       npp_inits_lbm,&              ! Postprocess initial step
       npp_stepi_lbm(5),&           ! Postprocess step interval
       npp_tracp_lbm(5),&           ! Points to be tracked for the LBM eqs.
       nou_inits_lbm,&              ! Output initial step 
       nou_stepi_lbm,&              ! Output step interval
       memor_lbm(2),&               ! Memory counter
       maxit_lbm,&                  ! Max # of iterations
       msoit_lbm,&                  ! Max # of solver iterations
       nkryd_lbm,&                  ! Krylov dimension
       nunkn_lbm,&                  ! # of unknonws ndofn*npoin  
       nevat_lbm,&                  ! Element matrix dim.=(ndime+1)*nnode
       nbase_lbm,&                  ! Number of base velocity vectors per node
       itera_lbm                    ! Internal iteration counter

  integer(ip) ::&                   ! Report file descriptors 
       lun_repor_lbm(40)

  real(rp) ::&
       xlatt_lbm    ,&              ! Internodal distance of the lattice
       bspee_lbm    ,&              ! Characteristic lattice speed
       tauin_lbm    ,&              ! 1/Relaxation parameter /1/tau)
       dtext_lbm    ,&              ! Externally fixed time step
       dtinv_lbm    ,&              ! 1/dt
       dtcri_lbm    ,&              ! Critical time step
       staco_lbm( 3),&              ! Stability constants
       shock_lbm    ,&              ! Shock capturing parameter
       safet_lbm    ,&              ! Safety factor for time step
       sstol_lbm    ,&              ! Steady state tolerance
       cotol_lbm    ,&              ! Convergence tolerance
       solco_lbm    ,&              ! Solver tolerance
       resid_lbm    ,&              ! Residual for outer iterations
       weigh_lbm,    &              ! Weight of dU/dt in the residual
       pos_tinit_lbm,&              ! Postprocess initial time
       pos_times_lbm(10,5),&        ! Postprocess times       
       out_tinit_lbm,&              ! Output initial time
       out_times_lbm(10),&          ! Output times 
       err01_lbm(2),&               ! L1 error T
       err02_lbm(2),&               ! L2 error T
       err0i_lbm(2),&               ! Linf error T
       err11_lbm(2),&               ! L1 error grad(T)
       err12_lbm(2),&               ! L2 error grad(T)
       err1i_lbm(2),&               ! Linf error grad(T)
       cpu_latbo(2)                 ! CPU for the LBM problem
!
! Physical properties used in the model
!
  real(rp),     pointer ::&
       weieq_lbm(:,:),&             ! Weight coefficients for the equilibrium distribution (material-wise) 
       xpaeq_lbm(:,:),&             ! Parameters of the equilibrium distribution (material-wise) 
       vbase_lbm(:,:),&             ! Velocity discretization base
       rhsau_lbm(:,:)               ! RHS auxiliar
  real(rp) ::&
       xmopa_lbm(20),&              ! Physical model parameters
       viref_lbm,&                  ! Viscosity (reference value)
       deref_lbm                    ! Density (reference value)
  integer(ip) ::&
       nmate_lbm,&                  ! # of materials
       kfl_cemod_lbm                ! Cell model
  integer(ip),  pointer ::&
       kfl_comod_lbm(:),&           !  Conductivity model (material-wise) 
       lbare_lbm(:,:),&             !  Base relations 
       lneba_lbm(:),&               !  Neighbors corresponding base index
       lcoba_lbm(:,:)               !  Neighbors connectivity
!
! Boundary conditions
! 
  integer(ip),  pointer ::&
       kfl_fixno_lbm(:,:),&         ! Nodal fixity 
       kfl_fixbo_lbm(:)             ! Element boundary fixity
    real(rp),     pointer ::&
       bvess_lbm(:,:)               ! Essential bc (or initial) values 
  type(r2p),     pointer ::&
       bvnat_lbm(:)                 ! Natural bc values
!--END REA GROUP
!
! Solver and miscelanea
! 
  integer(ip), pointer  ::& 
       lplbm(:)                     ! Skyline system matrix pointers
  integer(ip)  ::&
       idrep_lbm(20,5)              ! Report identification number
  character(5) ::  &
       worep_lbm(20,5),&            ! Report identification word
       tyrep_lbm(20)                ! Report type
end module def_latbol
