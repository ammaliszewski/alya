subroutine lbm_concou
!-----------------------------------------------------------------------
!****f* Latbol/lbm_concou
! NAME 
!    lbm_concou
! DESCRIPTION
!    This routine checks the convergence
! USED BY
!    Latbol
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_latbol
  implicit none

  if(kfl_conve_lbm==1) then
     if(resid_lbm>cotol_lbm) kfl_gocou = 1
  end if
  glres(modul) = resid_lbm
  write(lun_outpu,100) resid_lbm
100 format(  10x,'Residual for the dis. func:   ',3x,e16.6,' (%)')

end subroutine lbm_concou
