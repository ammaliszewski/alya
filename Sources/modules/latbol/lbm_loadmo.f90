subroutine lbm_loadmo
!-----------------------------------------------------------------------
!****f* Latbol/lbm_loadmo
! NAME 
!    lbm_loadmo
! DESCRIPTION
!    This routine "loads" the module by reading the corresponding
!    general problem data. It is the module's rproda subroutine.
!
! USED BY
!    Latbol
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain

  use      def_latbol

  implicit none
  integer(ip) :: kexit


  kfl_exist_lbm = 0                                      ! Module off
  kfl_conve_lbm = 0                                      ! 
  kfl_rstar_lbm = 0                                      ! This roblem is not a re-start
  kfl_delay_lbm = 0                                      ! Do not delay this problem
  ndela_lbm = 0                                          ! Steps of the problem delay

  mitgl = 1000000000                                     ! This is ALWAYS a transient problem
 
!
! Rewind the *.dat file (already read in rproda)
!
  lisda = lun_pdata                                      ! Reading file
  rewind (lisda)
!
! Reach the section.
!      
  call listen('LOADMO')
  do while(words(1)/='ENDRU')
     call listen('LOADMO')
  end do

  call listen('LOADMO')
  if(words(1)/='PROBL')&
       call runend('NSA_LOADMO: WRONG PROBLEM_DATA CARD')
!
! Read data
!      
  kexit = 0
  do while(words(1)/='ENDPR')
     call listen('RPRODA')
     if(words(1)=='EXCIT') then     
        if(exists('ON   ')) kfl_exist_lbm = 1
        do while(words(1)/='ENDEX' .and. kexit==0)
           call listen('RPRODA')
           if(words(1)=='RESTA') then
              if(exists('ON   ')) kfl_rstar_lbm = 1
           else if(words(1)=='DELAY') then
              kfl_delay_lbm  = 1
              ndela_lbm      = int(param(2))
           else if (words(1)=='ENDPR') then
              kexit = 1
           end if
        end do

     end if
  end do


end subroutine lbm_loadmo
