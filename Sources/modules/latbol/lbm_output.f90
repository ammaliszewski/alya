subroutine lbm_output(itask)
!-----------------------------------------------------------------------
!****f* Nstinc/lbm_output
! NAME 
!    lbm_output
! DESCRIPTION
!    End of a NSTINC time step 
!    itask = 0  When timemarching is true. There is output or post-process
!               of results if required.
!    itask = 1  When timemarching is false. Output and/or post-process of
!               results is forced if they have not been written previously.
! USES
!    output
!    postpr
! USED BY
!    lbm_endste (itask=1)
!    lbm_turnof (itask=2)
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_latbol
  use      Mod_postpr
  use      Mod_iofile
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: itime
  integer(ip), save       :: pos_alrea(11)
  select case(itask)
!
! End of a time step.
!
  case(zero)
     
     pos_alrea=0

!    At a given time step
     if(istep>=npp_inits_lbm.and.pos_alrea(1)==0.and.npp_stepi_lbm(1)>0) then     
        if(mod(istep,npp_stepi_lbm(1))==0) then
           call postpr(DISFU(1,1:npoin,1),'DIST1',istep,ctime)
           call postpr(DISFU(2,1:npoin,1),'DIST2',istep,ctime)
           call postpr(DISFU(3,1:npoin,1),'DIST3',istep,ctime)
           call postpr(DISFU(4,1:npoin,1),'DIST4',istep,ctime)
           call postpr(DISFU(5,1:npoin,1),'DIST5',istep,ctime)
           call postpr(DISFU(6,1:npoin,1),'DIST6',istep,ctime)
           call postpr(DISFU(7,1:npoin,1),'DIST7',istep,ctime)
           call postpr(DISFU(8,1:npoin,1),'DIST8',istep,ctime)
           call postpr(DISFU(9,1:npoin,1),'DIST9',istep,ctime)
           pos_alrea(1)=1
        end if
     end if
     if(istep>=npp_inits_lbm.and.pos_alrea(2)==0.and.npp_stepi_lbm(2)>0) then     
        if(mod(istep,npp_stepi_lbm(2))==0) then
           call postpr(veloc(:,:,1),'VELOC',istep,ctime)
           pos_alrea(2)=1
        end if
     end if
     if(istep>=npp_inits_lbm.and.pos_alrea(3)==0.and.npp_stepi_lbm(3)>0) then     
        if(mod(istep,npp_stepi_lbm(3))==0) then
           call postpr(press(:,1),'PRESS',istep,ctime)
           pos_alrea(3)=1
        end if
     end if
     if(istep>=npp_inits_lbm.and.pos_alrea(4)==0.and.npp_stepi_lbm(4)>0) then     
        if(mod(istep,npp_stepi_lbm(4))==0) then
           call postpr(densi(:,1),'DENSI',istep,ctime)
           pos_alrea(4)=1
        end if
     end if
     if(istep>=npp_inits_lbm.and.pos_alrea(5)==0.and.npp_stepi_lbm(5)>0) then     
        if(mod(istep,npp_stepi_lbm(5))==0) then
           call stream(veloc)
           pos_alrea(5)=1
        end if
     end if


!    At a given time
     if(ctime>=pos_tinit_lbm.and.pos_alrea(2)==0) then  
        do itime=1,10
           if(   abs(pos_times_lbm(itime,2)-ctime)<(0.5_rp*dtime).and.&
                &    pos_times_lbm(itime,2)>0.0_rp) then
              call postpr(veloc(:,:,1),'VELOC',istep,ctime)
              pos_alrea(2)=1
           end if
        end do
     end if
     if(ctime>=pos_tinit_lbm.and.pos_alrea(3)==0) then  
        do itime=1,10
           if(   abs(pos_times_lbm(itime,3)-ctime)<(0.5_rp*dtime).and.&
                &    pos_times_lbm(itime,3)>0.0_rp) then
              call postpr(press(:,1),'PRESS',istep,ctime)
              pos_alrea(3)=1
           end if
        end do
     end if
     if(ctime>=pos_tinit_lbm.and.pos_alrea(4)==0) then  
        do itime=1,10
           if(   abs(pos_times_lbm(itime,4)-ctime)<(0.5_rp*dtime).and.&
                &    pos_times_lbm(itime,4)>0.0_rp) then
              call postpr(densi(:,1),'DENSI',istep,ctime)
              pos_alrea(4)=1
           end if
        end do
     end if
     if(ctime>=pos_tinit_lbm.and.pos_alrea(5)==0) then  
        do itime=1,10
           if(   abs(pos_times_lbm(itime,5)-ctime)<(0.5_rp*dtime).and.&
                &    pos_times_lbm(itime,5)>0.0_rp) then
              call stream(veloc)
              pos_alrea(5)=1
           end if
        end do
     end if

!
! End of the run.
!
  case(one)

     if(npp_stepi_lbm( 2)/=0.and.pos_alrea( 2)==0) then
        call postpr(veloc(:,:,1),'VELOC',istep,ctime)
     end if
     if(npp_stepi_lbm( 3)/=0.and.pos_alrea( 3)==0) then
        call postpr(press(:,1),'PRESS',istep,ctime)
     end if
     if(npp_stepi_lbm( 4)/=0.and.pos_alrea( 4)==0) then
        call postpr(densi(:,1),'DENSI',istep,ctime)
     end if
     if(npp_stepi_lbm( 5)/=0.and.pos_alrea( 5)==0) then
        call stream(veloc)
     end if

  end select

end subroutine lbm_output
