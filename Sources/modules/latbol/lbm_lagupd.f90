subroutine lbm_lagupd

!-----------------------------------------------------------------------
!
! Compute RHS
!
!-----------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      def_latbol
  implicit none
  integer(ip) :: ielem,inode,ipoin,jnode,jpoin,kpoin,jzdom,jbase         ! Indices and dimensions
  integer(ip) :: idime,jdime,pnode,ibaop,kflbo
  integer(ip) :: ielty,imate,ibase,kbase,izdom,kacti(20)
  real(rp)    :: xvelo(ndime),xdens,xmome(ndime),bcter,dummr,xvaux
  real(rp)    :: xdist,cdist,xproj,vsqua,bspe2,bspe4
  real(rp)    :: xrelo,xreln,xfact,rtime,fucol(20)
!
! Initialization
!
  imate = 1 
  kacti = 0 
  bspe2 = bspee_lbm * bspee_lbm
  bspe4 = bspe2 * bspe2
  xreln = 1.0_rp
  xrelo = 0.0_rp

  do ipoin=1,npoin
     dummr=0.0_rp
     do ibase=1,nbase_lbm
        dummr=dummr+disfu(ibase,ipoin,3)
      end do
      continue
   end do
!
! Collision step
!
  do ipoin=1,npoin     
     xdens          = densi(ipoin,1)
     xvelo(1:ndime) = veloc(1:ndime,ipoin,1)
     vsqua          = dot_product(xvelo(1:ndime),xvelo(1:ndime))
     kflbo          = kfl_fixno_lbm(1,ipoin)
     bases: do ibase=1,nbase_lbm
        ! 
        ! 1. Equilibrium distribution function: fi^eq(xi,t)
        !            
        xproj        = dot_product(xvelo(1:ndime),vbase_lbm(1:ndime,ibase)) ! ei.u           
        fucol(ibase) =                                      &
             weieq_lbm(ibase,imate)*xdens*                  &               ! wi*rho*(
             ( xpaeq_lbm(1,imate)                           &               !  1
             + xpaeq_lbm(2,imate)*xproj/bspee_lbm           &               ! +3*(ei.u)/c^2
             + xpaeq_lbm(3,imate)*xproj*xproj/bspe2         &               ! +9/2*(ei.u)^2/c^4
             - xpaeq_lbm(4,imate)*vsqua/bspe2 )                             ! -3/2*u^2/c^2)           

        if (kflbo /= 0) then
           ! 
           ! 2 Collision update (local): fi(xi,t)-1/tau*[fi(xi,t)-fi^eq(xi,t)]
           !                 
           ibaop = lbare_lbm(ibase,1)
           if (lcoba_lbm(ibase,ipoin)==0 .and. lcoba_lbm(ibaop,ipoin)==0) then              
              ! 
              ! Correct collisions for buried links
              !           
              xvaux= 0.5_rp * (disfu(ibase,ipoin,3) + disfu(ibaop,ipoin,3))
              disfu(ibase,ipoin,2) = xvaux
              disfu(ibaop,ipoin,2) = xvaux
           else 
              ! 
              ! Otherwise, straight update
              !                      
              disfu(ibase,ipoin,2) = disfu(ibase,ipoin,3)         &      
                   + tauin_lbm*(fucol(ibase)-disfu(ibase,ipoin,3))                       
           end if
        else           
           ! 
           ! Straight update for no-boundary node
           !                      
           disfu(ibase,ipoin,2) = disfu(ibase,ipoin,3)         &      
                + tauin_lbm*(fucol(ibase)-disfu(ibase,ipoin,3))                       
        end if
     end do bases 

  end do 
!
! Streaming step
!
  disfu(:,:,1) = disfu(:,:,3)
  do ipoin=1,npoin
     do ibase=1,nbase_lbm
        jpoin = lcoba_lbm(ibase,ipoin)
        ! 
        ! 3. Streaming
        !           
        if(jpoin/=0) then
           disfu(ibase,jpoin,1) = disfu(ibase,ipoin,2)
        end if
     end do
  end do

!
! Extrapolation: takes into account density at inlets and outlets
!
  
!
! Bounce back step
!

  do ipoin=1,npoin
     kflbo          = kfl_fixno_lbm(1,ipoin)
     !     if(kfl_fixno_lbm(1,ipoin)==1) then
     !        do izdom = r_dom(ipoin),r_dom(ipoin+1)-1
     !           jpoin = c_dom(izdom)   
     !           ibase = lneba_lbm(izdom)
     !           if(ibase/=1) then
     !              inverse_base: do jzdom = r_dom(jpoin),r_dom(jpoin+1)-1           
     !                 kpoin = c_dom(jzdom)   
     !                 if(kpoin==ipoin) then
     !                    jbase = lneba_lbm(jzdom)
     !                    exit inverse_base
     !                 end if
     !              end do inverse_base
     !              disfu(ibase,ipoin,1) = disfu(jbase,jpoin,1)
     !           end if
     !        end do
     !     end if
     
     if (kflbo /= 0) then        
        xdens = 0.0_rp
        xmome = 0.0_rp
        bounce_back: do ibase=1,nbase_lbm           
           ibaop = lbare_lbm(ibase,1)
           if ((lcoba_lbm(ibase,ipoin)==0) .and. (lcoba_lbm(ibaop,ipoin)/=0)) then
              ! 
              ! Correct collisions for bounce back
              !           
              disfu(ibaop,ipoin,1) = disfu(ibase,ipoin,1)
           end if
           xdens          = xdens + disfu(ibase,ipoin,1)
           xmome(1:ndime) = xmome(1:ndime) &
                + bspee_lbm*disfu(ibase,ipoin,1)*vbase_lbm(1:ndime,ibase) 
        end do bounce_back
        xvelo(1:ndime) = xmome(1:ndime)/xdens
        vsqua          = dot_product(xvelo(1:ndime),vbase_lbm(1:ndime,ibase))
        ibaop = lbare_lbm(ibase,1)
        if ((lcoba_lbm(ibase,ipoin)/=0) .and. (lcoba_lbm(ibaop,ipoin)/=0)) then
           disfu(ibaop,ipoin,1) = disfu(ibaop,ipoin,1) - 0.25_rp * vsqua
           disfu(ibase,ipoin,1) = disfu(ibase,ipoin,1) - 0.25_rp * vsqua
        end if
     end if
     
  end do


!
! Body force
!


!
! Density and velocity boundary conditions
!


! 
! Update (final)
! 
  disfu(1:nbase_lbm,1:npoin,1)   = &
       xreln * disfu(1:nbase_lbm,1:npoin,1) + & 
       xrelo * disfu(1:nbase_lbm,1:npoin,3) 
  do ipoin=1,npoin
     dummr=0.0_rp
     do ibase=1,nbase_lbm
        dummr=dummr+disfu(ibase,ipoin,1)
      end do
      continue
   end do

end subroutine lbm_lagupd
