subroutine lbm_memall
!-----------------------------------------------------------------------
!****f* Latbol/lbm_memall
! NAME 
!    lbm_memall
! DESCRIPTION
!    This routine allocates memory for the arrays needed to solve the
!    equations
! USES
!    memchk
!    mediso
! USED BY
!    lbm_turnon
!***
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!
! This routine allocates memory for the arrays needed to solve the
! module's equation      
!

!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      def_solver

  use      def_latbol

  use      Mod_memchk


  implicit none
  integer(ip)    :: istat,knodb,iboun,lodof

!
! Vectors of unkowns and auxiliars
!
  nevat_lbm = mnode                                           ! <--- explicit

  ncomp_lbm = 3
  allocate(disfu(nbase_lbm,npoin,ncomp_lbm),stat=istat)
  call memchk(zero,istat,memor_lbm,'DISFU','lbm_memall',disfu)

  allocate(rhsau_lbm(nbase_lbm,npoin),stat=istat)
  call memchk(zero,istat,memor_lbm,'RHSAU_LBM','lbm_memall',rhsau_lbm)

  allocate(veloc(ndime,npoin,2),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'VELOC','memunk',veloc)
  allocate(press(npoin,1),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'PRESS','memunk',press)
  allocate(densi(npoin,1),stat=istat)
  call memchk(zero,istat,mem_modul(1:2,modul),'DENSI','memunk',densi)

!
! Derived dimensions according to the general algorithm type
!
  if(kfl_genal_lbm==1)  then
     lodof     =  1                             ! explicit lagrangian
     nunkn_lbm =  npoin*ndofn_lbm
  end if

  nusol     = max(nusol,1)
  mxdof     = max(mxdof,lodof)
  
end subroutine lbm_memall 
