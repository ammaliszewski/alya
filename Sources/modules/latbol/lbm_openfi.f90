subroutine lbm_openfi(iopfi)
!-----------------------------------------------------------------------
!****f* Latbol/lbm_openfi
! NAME 
!    lbm_openfi
! DESCRIPTION
!    This subroutine gets ALL the file names and open them to be used by 
!    the module in two possible ways:
! 
!    1. Recalling them from the environment, when Alya is launched
!       encapsulated in a shell script, or
! 
!    2. Composing the names out of the problem name which is given as
!       argument when the binary file Alya is launched "naked".
! USES
!    iofile
! USED BY
!    lbm_turnon
!***
!-----------------------------------------------------------------------
  use      def_latbol
  use      def_master
  use      mod_iofile

  implicit none
  integer(ip)    :: ilcha,iopfi,kdrep,jdrep,irepi
  character(150) :: fil_pdata_lbm,fil_conve_lbm
  character(150) :: fil_outpu_lbm,fil_solve_lbm

  lun_repor_lbm = 550

  if (iopfi == 1) then
     
     cpu_latbo = 0.0_rp                                  ! Total/Solver module CPU
     
     !
     ! Module name and extension
     !
     namod(modul)='LATBOL'
     exmod(modul)='lbm'
     
     !
     ! kfl_naked is set in the kernel subroutine getnam
     !
     if (kfl_naked==0) then
        !  encapsulated, then get names from the environment     
        call GETENV('FOR801',fil_pdata_lbm)
        call GETENV('FOR802',fil_outpu_lbm)
        call GETENV('FOR803',fil_conve_lbm)
        call GETENV('FOR804',fil_solve_lbm)   
        call GETENV('FOR805',fil_rstar_lbm)     
     else if (kfl_naked==1) then
        !  naked, then compose the names     
        fil_pdata_lbm = adjustl(trim(namda))//'.lbm.dat'
        fil_outpu_lbm = adjustl(trim(namda))//'.lbm.log'
        fil_conve_lbm = adjustl(trim(namda))//'.lbm.cvg'
        fil_solve_lbm = adjustl(trim(namda))//'.lbm.sol'    
        fil_rstar_lbm = adjustl(trim(namda))//'.lbm.rst'    
     end if
!
! Open files
!
  call iofile(zero,lun_pdata_lbm,fil_pdata_lbm,'LATBOL DATA','old')
  if(kfl_rstar==2) then
     call iofile(zero,lun_outpu_lbm,fil_outpu_lbm,'LATBOL OUTPUT',     'old','formatted','append')
     call iofile(zero,lun_conve_lbm,fil_conve_lbm,'LATBOL CONVERGENCE','old','formatted','append')
     call iofile(zero,lun_solve_lbm,fil_solve_lbm,'LATBOL SOLVER ',    'old','formatted','append')
  else
     call iofile(zero,lun_outpu_lbm,fil_outpu_lbm,'LATBOL OUTPUT')
     call iofile(zero,lun_conve_lbm,fil_conve_lbm,'LATBOL CONVERGENCE')
     call iofile(zero,lun_solve_lbm,fil_solve_lbm,'LATBOL SOLVER')
  end if

  else if (iopfi == 2) then

     if (kfl_repor_lbm == 0) return

     tyrep_lbm(1) = 'NODES'
     tyrep_lbm(2) = 'POXYZ'
     tyrep_lbm(3) = 'FACES'
     tyrep_lbm(4) = 'MATRL'
     tyrep_lbm(5) = 'DMAIN'

     
     if (kfl_naked==0) then
        !  encapsulated, then get names from the environment: this option is not allowed     
        
        return
        
     else if (kfl_naked==1) then
        !  naked, then compose the names     

        do kdrep=1,5
           if (idrep_lbm(1,kdrep) >= 1) then
              fil_outpu_lbm = adjustl(trim(namda))//'-'//tyrep_lbm(kdrep)//'.lbm.rep'
              lun_repor_lbm(kdrep) = 549 + kdrep

              call iofile(zero,lun_repor_lbm(kdrep),fil_outpu_lbm,'LBM_OPENFI')
 
              write (lun_repor_lbm(kdrep),'(a)') '#'
              write (lun_repor_lbm(kdrep),'(a)') '# LATBOL REPORT FILE'
              write (lun_repor_lbm(kdrep),'(a)') '# Columns are:'
              write (lun_repor_lbm(kdrep),'(a)') '#   1. Iteration  2. Time'
              write (lun_repor_lbm(kdrep),'(a)',advance='no') '#   '
              write (lun_repor_lbm(kdrep),'(a)',advance='no') tyrep_lbm(kdrep)              
              write (lun_repor_lbm(kdrep),'(a)',advance='no') '  -> '
              do irepi=1,idrep_lbm(1,kdrep)
                 write (lun_repor_lbm(kdrep),'(i12)',advance='no') &
                      idrep_lbm(irepi+2,kdrep)
              end do
              write (lun_repor_lbm(kdrep),'(a)') ' '

              write (lun_repor_lbm(kdrep),'(a)',advance='no') '# '
              write (lun_repor_lbm(kdrep),'(a)',advance='no') '  VALUES -> '
              do irepi=1,idrep_lbm(2,kdrep)
                 write (lun_repor_lbm(kdrep),'(a12)',advance='no') &
                      worep_lbm(irepi,kdrep)
              end do
              write (lun_repor_lbm(kdrep),'(a)') ' '
              write (lun_repor_lbm(kdrep),'(a)') '#'

           end if           
        end do

     end if



  end if

end subroutine lbm_openfi

