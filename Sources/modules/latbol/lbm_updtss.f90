subroutine lbm_updtss
!-----------------------------------------------------------------------
!****f* Latbol/lbm_updtss
! NAME 
!    lbm_updtss
! DESCRIPTION
!    This routine computes the proper time step size 
! USED BY
!    lbm_begste
!***
!-----------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      def_latbol
  implicit none 

  integer(ip) :: ielem,imate,idime,jdime,inode,pnode,ipoin,ielty,igaus
  real(rp)    :: velno,dtcri,dtaux,hlmin,cndmx,xfact,xpfhn,xmccc,xmalp,xcosa,xmchi
  real(rp)    :: cndex(ndime,ndime), cndin(ndime,ndime), elion(mnode,2),eliap(mnode,2) 


  
end subroutine lbm_updtss
