subroutine lbm_reaous
!-----------------------------------------------------------------------
!****f* Latbol/lbm_reaous
! NAME 
!    lbm_reaous
! DESCRIPTION
!    This routine reads the output strategy 
! USES
!    listen
! USED BY
!    lbm_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      Mod_memchk

  use      def_latbol

  implicit none
  integer(ip) jdrep,kdrep
!
! Initializations.
!
!
! Initializations.
!
  npp_inits_lbm  = 0
  npp_stepi_lbm  = 0
  pos_tinit_lbm  = 0.0_rp
  pos_times_lbm  = 0.0_rp
  kfl_exacs_lbm  = 0            ! Exact solution
!
! Reach the section.
!
  call listen('lbm_reaous')
  do while(words(1)/='OUTPU')
     call listen('lbm_reaous')
  end do
!
! Begin to read data.
!
  do while(words(1)/='ENDOU')
     call listen('lbm_reaous')
     
     ! Step or time for post-process          
     if(words(1)=='POSTP') then
        if(words(2)=='DISTR') then  
           if(words(3)=='STEPS') then
              npp_stepi_lbm(1) = getint('STEPS',1,'#Postprocess step interval for f')
              if(exists('ATTIM')) pos_times_lbm(1:10,1)=param(4:13)
           end if
        else if(words(2)=='VELOC') then  
           if(words(3)=='STEPS') then
              npp_stepi_lbm(2) = getint('STEPS',1,'#Postprocess step interval for u')
              if(exists('ATTIM')) pos_times_lbm(1:10,2)=param(4:13)
           end if
        else if(words(2)=='PRESS') then  
           if(words(3)=='STEPS') then
              npp_stepi_lbm(3) = getint('STEPS',1,'#Postprocess step interval for p')
              if(exists('ATTIM')) pos_times_lbm(1:10,3)=param(4:13)
           end if
        else if(words(2)=='DENSI') then  
           if(words(3)=='STEPS') then
              npp_stepi_lbm(4) = getint('STEPS',1,'#Postprocess step interval for rho')
              if(exists('ATTIM')) pos_times_lbm(1:10,4)=param(4:13)
           end if
        else if(words(2)=='STREA') then  
           if(words(3)=='STEPS') then
              npp_stepi_lbm(5) = getint('STEPS',1,'#Postprocess step interval for f')
              if(exists('ATTIM')) pos_times_lbm(1:10,5)=param(4:13)
           end if
        end if
     ! Starting time and step of post-process
     else if(words(2)=='START') then
        if(exists('STEP ')) then
           npp_inits_lbm = getint('STEP ',0,'#Initial step to start postprocess')  
        end if
        if(exists('ATTIM')) then
           pos_tinit_lbm = getrea('ATTIM',0.0_rp,'#Initial step to start postprocess')
        end if
     end if
  end do
  
end subroutine lbm_reaous
    
