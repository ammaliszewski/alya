subroutine lbm_endite(itask)
!-----------------------------------------------------------------------
!****f* Latbol/lbm_endite
! NAME 
!    lbm_endite
! DESCRIPTION
!    This routine checks convergence and updates unknowns at:
!    - itask=1 The end of an internal iteration
!    - itask=2 The end of the internal loop iteration
! USES
!    lbm_cvgunk
!    lbm_updunk
! USED BY
!    lbm_doiter
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  implicit none
  integer(ip) :: itask

  select case(itask)

  case(one)

     !  Compute convergence residual of the internal iteration (that is,
     !  || u(n,i,j) - u(n,i,j-1)|| / ||u(n,i,j)||) and update unknowns:
     !  u(n,i,j-1) <-- u(n,i,j)
     call lbm_cvgunk(  one)
     call lbm_updunk(three)


  case(two)

     !  Compute convergence residual of the external iteration (that is,
     !  || u(n,i,*) - u(n,i-1,*)|| / ||u(n,i,*)||) and update unknowns:
     !  u(n,i-1,*) <-- u(n,i,*)
     call lbm_cvgunk( two)
     call lbm_updunk(four)
     
  end select

end subroutine lbm_endite
