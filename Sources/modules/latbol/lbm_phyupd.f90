subroutine lbm_phyupd

!------------------------------------------------------------------------
!
! Update physical variables
!
!------------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      def_latbol
  implicit none
  real(rp)    :: xdens,xmome(3),souns,soun2
  integer(ip) :: idime,ipoin,ibase
!
! Initialization
!
  souns = bspee_lbm/sqrt(3.0_rp)                                        ! cs=c/sqrt(3)
  soun2 = souns*souns                                                   ! cs^2
!
! Loop over points
!   
  veloc(:,:,2)=veloc(:,:,1)
  point: do ipoin=1,npoin

     xdens = 0.0_rp
     xmome = 0.0_rp
     do ibase= 1,nbase_lbm
        xdens          = xdens + disfu(ibase,ipoin,1)
        xmome(1:ndime) = xmome(1:ndime) &
             + bspee_lbm*disfu(ibase,ipoin,1)*vbase_lbm(1:ndime,ibase) 
     end do
     
     densi(ipoin,1)         = xdens
     press(ipoin,1)         = xdens*soun2
     veloc(1:ndime,ipoin,1) = xmome(1:ndime)/xdens
     
     ! Prescribe physical boundary conditions
     do idime=1,ndime
        if(kfl_fixno_lbm(idime,ipoin)==1) then
           veloc(idime,ipoin,1) = bvess_lbm(idime,ipoin)
        end if
     end do
  
  end do point

end subroutine lbm_phyupd
