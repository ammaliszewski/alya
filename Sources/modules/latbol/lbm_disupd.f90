subroutine lbm_disupd

!-----------------------------------------------------------------------
!
! This routine updates the intracellular / transmembrane
! action potential by performing
! non-linear iterations
!
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_solver

  use      def_latbol

  implicit none
  integer(ip) :: inoli
  real(rp)    :: xnoli,xdeno,xnumi,xreln,xrelo,rtime
  real(rp)    :: xmeps,xmchi,xmalp,xmgam,xmccc,xmccm,xfact
  real(rp)    :: cpu_refe1,cpu_refe2


  !
  ! Initializations
  !

  rhsau_lbm     = 0.0_rp
  cpu_solve     = 0.0_rp
  call cputim(cpu_refe1)


  if (kfl_genal_lbm == 1) then             

     ! kfl_genal_lbm ==  1 :  
     ! 
     ! -->> Explicit langrangian algorithm 
     ! Explicit advance of the distribution function in a lagrangian system along the 
     ! material derivatives corresponding to each of the discrete set of particle velocities
     ! 
     ! 

     call lbm_lagupd
                
     !    Compute derived physical variables
  
     call lbm_phyupd
     
     
  else if (kfl_genal_lbm == 2) then
     
     ! kfl_genal_lbm ==  2 :  
     ! 
     ! -->> 
     ! 
     ! 
     
  end if

  
  call cputim(cpu_refe2)
  cpu_latbo(1) = cpu_latbo(1) + cpu_refe2 - cpu_refe1 
  cpu_latbo(2) = cpu_latbo(2) + cpu_solve

end subroutine lbm_disupd
