subroutine lbm_iniunk
!-----------------------------------------------------------------------
!****f* Latbol/lbm_iniunk
! NAME 
!    lbm_iniunk
! DESCRIPTION
!    This routine sets up the initial condition for the module
! USED BY
!    lbm_begste
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain

  use      def_latbol

  implicit none
  integer(ip) :: ipoin,jpoin,idofn,jcode,izdom,ibase,istat,idime,inode
  integer(ip) :: jnode,ielty,pnode,ielem,imate,kbase,kacti(20)
  integer(ip) :: jzdom,kpoin,jbase
  real(rp)    :: xproj,cproj,xdimi,vdife(3),xdist,cdist,tau
!
! Initial density and velocity
!
  imate=1
  densi=deref_lbm
  do ipoin=1,npoin 
     do ibase=1,nbase_lbm
        disfu(ibase,ipoin,3) = densi(ipoin,1)*weieq_lbm(ibase,imate)
     end do
     do idime=1,ndime
        if(  kfl_fixno_lbm(idime,ipoin)==0.or.&
             kfl_fixno_lbm(idime,ipoin)==1) then
           veloc(idime,ipoin,1) = bvess_lbm(idime,ipoin)
        end if
     end do
  end do

  do ipoin=1,npoin
     if(kfl_fixno_lbm(1,ipoin)==1) then
        do izdom = r_dom(ipoin),r_dom(ipoin+1)-1           
           jpoin = c_dom(izdom)   
           ibase = lneba_lbm(izdom)
           if(ibase/=1) then
              inverse_base: do jzdom = r_dom(jpoin),r_dom(jpoin+1)-1           
                 kpoin = c_dom(jzdom)   
                 if(kpoin==ipoin) then
                    jbase = lneba_lbm(izdom)
                    exit inverse_base
                 end if
              end do inverse_base
              disfu(ibase,ipoin,3) = disfu(jbase,jpoin,3)
           end if
        end do
     end if
  end do
!
! tau fixed
!
  tau       = 0.51_rp
  xlatt_lbm = sqrt(xlatt_lbm)
  tauin_lbm = 1.0_rp/tau
  dtime     = (tau-0.5_rp)*xlatt_lbm*xlatt_lbm/(3.0_rp*viref_lbm)
  bspee_lbm = xlatt_lbm/dtime
!
! dt fixed
!
  xlatt_lbm = sqrt(xlatt_lbm)
  dtime     = xlatt_lbm
  tauin_lbm = 1.0_rp/(3.0_rp*viref_lbm*dtime/(xlatt_lbm*xlatt_lbm)+0.50_rp)
  bspee_lbm = xlatt_lbm/dtime

end subroutine lbm_iniunk
