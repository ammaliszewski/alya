subroutine lbm_doiter
!-----------------------------------------------------------------------
!****f* Latbol/lbm_doiter
! NAME 
!    lbm_doiter
! DESCRIPTION
!    This routine solves an iteration of the linearized incompressible NS
!    equations.
!    Fractional step is to be implemented
! USES
!    lbm_comapp
!    lbm_iapupd
!    lbm_eapupd
!    lbm_rcpupd
! USED BY
!    Latbol
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_domain
  use      def_master
  use      def_solver
  use      def_latbol 
  implicit none
  real(rp)    :: cpu_refe1,cpu_refe2

  call cputim(cpu_refe1)

  call lbm_disupd
  call lbm_endite(two)

  call cputim(cpu_refe2)
  cpu_latbo(1) = cpu_latbo(1) + cpu_refe2 - cpu_refe1 
  cpu_latbo(2) = cpu_latbo(2) + cpu_solve
  
  
end subroutine lbm_doiter
