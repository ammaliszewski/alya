subroutine lbm_cvgunk(itask)
!-----------------------------------------------------------------------
!****f* Latbol/lbm_cvgunk
! NAME 
!    lbm_cvgunk
! DESCRIPTION
!    This routine performs several convergence checks for the 
!    incompressible NS equations
! USES
!    lbm_endite (itask=1,2) (TO BE UNPROGRAMMED)
!    lbm_endste (itask=3)
! USED BY
!    Latbol
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_latbol
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip), save       :: ipass=0
  integer(ip)             :: ipoin,ixlbm,idime
  real(rp)                :: va,vo,numer,denom,rilbm,pnume,pdeno,prlbm
  real(rp)                :: rivel
!
! Initializations
!
  numer = 0.0_rp
  denom = 0.0_rp

  select case(itask)
!
! Check convergence of the inner iterations, always in the L2 norm:
! || u(n,i,j) - u(n,i,j-1)|| / ||u(n,i,j)||
!
  case(1)
!
! Check convergence of the outer iterations in the norm selected by the user:
! || u(n,i,*) - u(n,i-1,*)|| / ||u(n,i,*)||
!
  case(2)
     call vecres(two,npoin*nbase_lbm,disfu(1,1,1),disfu(1,1,3),resid_lbm,zelbm)
     call vecres(two,ndime*npoin,veloc(1,1,1),veloc(1,1,2),rivel,zelbm)
     if(ipass==0.and.kfl_rstar/=2) then
        ipass=1
        write(lun_conve_lbm,100)
     end if
    write(lun_conve_lbm,101) istep,iiter,one,ctime,resid_lbm,rivel
    ! Write in the log file.
    call flush(lun_conve_lbm)
!
! Check residual of the time evolution, always in the L2 norm:
! || u(n,*,*) - u(n-1,*,*)|| / ||u(n,*,*)||
!     
  case(3)       
     call vecres(two,npoin,disfu(1,1,1),disfu(1,1,3),rilbm,zelbm)

  end select
!
! Formats
!
100 format('$ ','       Time','     Global','      Inner',&
         &      '       Current','   Distr. func','      Velocity',/,&
         & '$ ','       step','  iteration','  iteration',&
         &      '          time','      residual','      residual')
101 format(4x,i9,2x,i9,11(2x,e12.6))
102 format('$ >>>  INTERNAL ACTION POTENTIAL BECOMES STATIONARY AT TIME STEP ',i5)
        
end subroutine lbm_cvgunk


