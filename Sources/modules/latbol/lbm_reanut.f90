subroutine lbm_reanut
!-----------------------------------------------------------------------
!****f* Latbol/lbm_reanut
! NAME 
!    lbm_reanut
! DESCRIPTION
!    This routine reads the numerical treatment 
! USES
!    listen
! USED BY
!    lbm_turnon
!***
!-----------------------------------------------------------------------
  use      def_parame
  use      def_inpout
  use      def_master
  use      def_domain
  use      Mod_memchk

  use      def_latbol

  implicit none
  integer(ip) :: istab,ipart,rpalo

!
!  Initializations (defaults)
!
  kfl_shock_lbm = 0                                ! Shock capturing off
  kfl_weigh_lbm = 1                                ! dT/dt is in the residual
  kfl_tiacc_lbm = 1                                ! First order time integ.
  kfl_tiext_lbm = 1                                ! Externally fixed time step
  kfl_normc_lbm = 2                                ! L2 norm for convergence
  kfl_algso_lbm = 8                                ! Algebraic solver is GMRES
  kfl_latyp_lbm = 1                                ! Lattice is uniform


  maxit_lbm     = 1                                ! One internal iteration
  staco_lbm(1)  = 4.0_rp                           ! Diffusive term
  staco_lbm(2)  = 2.0_rp                           ! Convective term
  staco_lbm(3)  = 1.0_rp                           ! Reactive term
  shock_lbm     = 0.0_rp                           ! SC parameter
  sstol_lbm     = 1.0d-5 !F90                      ! Steady-satate tolerance
  safet_lbm     = 1.0                              ! Safety factor
  cotol_lbm     = 1.0_rp                           ! Internal tolerance

  dtext_lbm     = 0.0_rp                           ! Externally fixed time step

!
! Reach the section
!
  rewind(lisda)
  call listen('lbm_reanut')
  do while(words(1)/='NUMER')
     call listen('lbm_reanut')
  end do
!
! Begin to read data
!
  do while(words(1)/='ENDNU')
     call listen('lbm_reanut')
     if(     words(1)=='STABI') then
     else if(words(1).eq.'GENER') then
        if(exists('LAGRA')) kfl_genal_lbm = 1
     else if(words(1).eq.'LATTI') then
        if(exists('UNIFO')) kfl_latyp_lbm = 1
     else if(words(1)=='SHOCK') then
        if(exists('ISOTR').or.exists('ON   ')) then
           kfl_shock_lbm = 1
           shock_lbm     = getrea('FACTO',0.0_rp,'#Shock capturing parameter')
        else if(exists('ANISO')) then
           kfl_shock_lbm = 2
           shock_lbm     = getrea('FACTO',0.0_rp,'#Shock capturing parameter')
        end if
     else if(words(1)=='TEMPO') then
        if(exists('GALER')) kfl_weigh_lbm = 0
     else if(words(1)=='TIMEA') then
        kfl_tiacc_lbm = int(param(1))
        if(kfl_timei_lbm==0) kfl_tiacc_lbm = 1
     else if(words(1)=='SAFET') then
        safet_lbm = param(1)
     else if(words(1)=='STEAD') then
        sstol_lbm = param(1)
     else if(words(1)=='NORMO') then
        if(exists('L1   ')) then
           kfl_normc_lbm = 1
        else if(exists('L-inf')) then
           kfl_normc_lbm = 0
        end if
     else if(words(1)=='MAXIM') then
        maxit_lbm = int(param(1))
     else if(words(1)=='CONVE') then
        cotol_lbm = param(1)
     else if(words(1)=='ALGEB') then
        if(exists('DIREC')) kfl_algso_lbm =  0
        if(exists('CG   ')) kfl_algso_lbm =  1
        if(exists('CGNOR')) kfl_algso_lbm =  2
        if(exists('BiCG ')) kfl_algso_lbm =  3
        if(exists('BiCGW')) kfl_algso_lbm =  4
        if(exists('BiCGS')) kfl_algso_lbm =  5
        if(exists('TRANS')) kfl_algso_lbm =  6
        if(exists('FULLO')) kfl_algso_lbm =  7
        if(exists('GMRES')) kfl_algso_lbm =  8
        if(exists('FLEXI')) kfl_algso_lbm =  9
        if(exists('QUASI')) kfl_algso_lbm = 10
     else if(words(1)=='SOLVE') then
        msoit_lbm = int(param(1))
     else if(words(1)=='TOLER') then
        solco_lbm = param(1)
     else if(words(1)=='KRYLO') then
        nkryd_lbm = int(param(1))
     end if
  end do


  if (dtime >= 0._rp ) dtext_lbm    = dtime

  
end subroutine lbm_reanut
