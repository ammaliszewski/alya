subroutine lbm_rproda

!-----------------------------------------------------------------------
! 
! This routine reads the problem data.
!
!-----------------------------------------------------------------------
  use      def_kintyp
  use      def_master
  use      def_inpout
  use      def_latbol
  implicit none

  kfl_exist_lbm = 0                                      ! LBM formulation off
  kfl_conve_lbm = 0                                      ! LBM conv. not needed
  kfl_delay_lbm = 0                                      ! Do not delay LBM problem
  ndela_lbm     = 0                                      ! Steps of LBM delay
!
! Reach the section.
!      
  rewind(lisda)
  do while(words(1)/='RUNDA')
     call listen('RPRODA')
  end do
  do while(words(1)/='ENDRU')
     call listen('RPRODA')
  end do
  do while(words(1)/='PROBL')
     call listen('RPRODA')
  end do   
!
! Read data.
!      
  do while(words(1)/='ENDPR')
     call listen('RPRODA')
     if(words(1)=='LATBO') then            
        if(exists('ON   ')) kfl_exist_lbm = 1
        do while(words(1)/='ENDLA')
           call listen('RPRODA')
           if(words(1)=='DELAY') then
              kfl_delay_lbm  = 1
              ndela_lbm      = int(param(2))
           else if(words(1)=='CONVE') then
              if(exists('YES  ')) kfl_conve_lbm = 1
           end if
        end do
     end if
  end do
!
! Equivalence master-module existence
!
  if(kfl_exist_lbm/=0) kfl_exist(modul)=1

end subroutine lbm_rproda
