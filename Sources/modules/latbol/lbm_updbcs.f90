subroutine lbm_updbcs
!-----------------------------------------------------------------------
!****f* Latbol/lbm_updbcs
! NAME 
!    lbm_updbcs
! DESCRIPTION
!    This routine updates the velocity boundary conditions
! USED BY
!    lbm_begste
!***
!-----------------------------------------------------------------------
  use      def_master
  use      def_domain
  use      def_latbol
  implicit none
  integer(ip) :: ipoin
!
! Update of boundary values and prescription of Dirichlet conditions
!      


end subroutine lbm_updbcs
