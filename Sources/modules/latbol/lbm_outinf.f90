subroutine lbm_outinf
!-----------------------------------------------------------------------
!****f* Latbol/lbm_outinf
! NAME 
!    lbm_outinf
! DESCRIPTION
!    This routine writes on the module output files
! USED BY
!    lbm_turnon
!***
!-----------------------------------------------------------------------
  use      def_master

  use      def_latbol

  implicit none
  character(60) :: equat
  integer(ip)   :: ierhs,imate
!
! Headings
!
  write(lun_outpu_lbm,100) title
  write(lun_solve_lbm,102) title
!
! Write information in Result file
!


!!!!!!!! acaaaaaaaaaaaaaa ver los outputs


!  equat=''
!  if(kfl_timei_lbm==1) equat=trim(equat)//'rho*Cp*dT/dt '
!  if(kfl_advec_lbm>=1) equat=trim(equat)//'+rho*Cp*u.grad(T) '
!                       equat=trim(equat)//'-div[k*grad(T)] '
!  if(react_lbm>zeexm)  equat=trim(equat)//'+s*T'
!                       equat=trim(equat)//'= '
!  ierhs=0
!  if(kfl_sourc_lbm==1) then
!     equat=trim(equat)//'Q '
!     ierhs=1
!  end if
!  if(ierhs==0) equat=trim(equat)//'0 '

!  write(lun_resul_lbm,110) trim(equat),nmate_lbm
!  write(lun_resul_lbm,111) 'rho [ M / L^3 ]     = ',(densi_lbm(imate),imate=1,nmate_lbm)
!  write(lun_resul_lbm,111) 'Cp  [ L^2 / T^3 K ] = ',(sphea_lbm(imate),imate=1,nmate_lbm)
!  write(lun_resul_lbm,111) 'k   [ M L / T^3 K ] = ',(tcond_lbm(imate),imate=1,nmate_lbm)
!  write(lun_resul_lbm,*) 
!  if(react_lbm>zeexm) then
!     write(lun_resul_lbm,111) 's   [ M / L T^3 K ] = ',react_lbm
!     write(lun_resul_lbm,*) 
!  end if

!
! Formats
!
100 format(///,&
         5x,'****************************** ',/,&
         5x,'RESULTS FOR MODEL: ',a10,/,&
         5x,'****************************** ',/)
101 format(///,&
         5x,'******************************************* ',&
         /,&
         5x,'EVOLUTION FOR MODEL: ',&
         a10,/,&
         5x,'******************************************* ')
102 format(///,&
         5x,'***************************************** ',&
         /,&
         5x,'SOLVER INFORMATION FOR MODEL: ',&
         a10,/,&
         5x,'***************************************** ',/)
110 format(/,&
       5x, '>>>  DIFFERENTIAL EQUATION:',///,&
       10x,a,//,&
       10x,'Physical properties: ',i2,' material(s)',/,&
       10x,'------------------------------------------------------------ ')
111 format(&
       10x,a,10e12.6)

end subroutine lbm_outinf
      
