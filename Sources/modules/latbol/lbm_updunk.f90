subroutine lbm_updunk(itask)
!-----------------------------------------------------------------------
!****f* Latbol/lbm_updunk
! NAME 
!    lbm_updunk
! DESCRIPTION
!    This routine performs several types of updates 
! USED BY
!    lbm_begste (itask=1)
!    lbm_begite (itask=2)
!    lbm_endite (itask=3, inner loop) 
!    lbm_endite (itask=4, outer loop) 
!    lbm_endste (itask=5)
!***
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!
! This routine performs several types of updates for the unknowns
!
! NO ESTA BIEN ESTA SUBRUTINA.. REVISAR ANTES DE USAR!!!!!!!!!
!
!-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain

  use      def_latbol

  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin
  
  select case (itask)
!
! Assign U(n,0,*) <-- U(n-1,*,*), initial guess for outer iterations
!
  case(1)
!
! Assign U(n,i,0) <-- U(n,i-1,*), initial guess for inner iterations
!
  case(2)
!
! Assign U(n,i,j-1) <-- U(n,i,j), update of the temperature
!
  case(3)
!
! Assign U(n,i-1,*) <-- U(n,i,*)
!        
  case(4)
!
! Obtain U(n,*,*) for the Crank-Nicolson method and assign
! U(n-1,*,*) <-- U(n,*,*)
!        
  case(5)
     disfu(1:nbase_lbm,1:npoin,3)   = disfu(1:nbase_lbm,1:npoin,1)
     
  end select
  
end subroutine lbm_updunk

