subroutine lev_reabcs()
  !-----------------------------------------------------------------------
  !****f* wavequ/lev_reabcs
  ! NAME 
  !    lev_reabcs
  ! DESCRIPTION
  !    This routine reads the boundary conditions
  ! USES
  !    lev_membc
  ! USED BY
  !    lev_turnon
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use def_levels
  use mod_opebcs
  implicit none
  integer(ip) :: dummi

   if( kfl_icodn > 0 ) then
     call opnbcs(1_ip,1_ip,dummi,dummi,tncod_lev) ! Memory for structure
     call opnbcs(2_ip,1_ip, 1_ip, 0_ip,tncod_lev) ! Memory for variable
  end if
  if( kfl_geome > 0 ) then
     call opnbcs(0_ip,1_ip,1_ip,0_ip,tgcod_lev)
  end if

  if( INOTSLAVE ) then
     !
     ! Initialization
     !
     kfl_inlev_lev = 0_ip
     height_lev    = 0.0_rp
     !
     ! Reach the nodal-wise section
     !
     call ecoute('lev_reabcs')
     do while(words(1)/='BOUND')
        call ecoute('lev_reabcs')
     end do
     !
     ! Loop over nodes and or boundaries
     !
     do while(words(1)/='ENDBO')
        call ecoute('lev_reabcs')

        if( words(1) == 'PARAM' ) then

           !-------------------------------------------------------------
           !
           ! Parameters
           !
           !-------------------------------------------------------------

           call ecoute('nsi_reabcs')
           do while(words(1)/='ENDPA') 
              if(words(1)=='INITI') then                 
                 !
                 ! Initialization of level set  
                 !
                 if(exists('HEIGH')) then                 
                    kfl_inlev_lev = -1
                    if( exists('ONX  ')) kfl_inlev_lev = -3
                    if( exists('ONY  ')) kfl_inlev_lev = -2
                    if( exists('ONZ  ')) kfl_inlev_lev = -1
                    if( exists('ONXIN')) kfl_inlev_lev = -6
                    if( exists('ONYIN')) kfl_inlev_lev = -5
                    if( exists('ONZIN')) kfl_inlev_lev = -4
                    height_lev    = getrea('HEIGH',0.0_rp,  '#Height of the level set')
                 else if(exists('FUNCT')) then  
                    kfl_inlev_lev = getint('FUNCT',1_ip,  '#Initialization function')
                 end if
              end if
              call ecoute('lev_reabcs')
           end do

        else if( words(1) == 'CODES' .and. exists('NODES') ) then 

           !-------------------------------------------------------------
           !
           ! User-defined codes on nodes
           !           
           !-------------------------------------------------------------

           if(exists('GEOME')) then
              !
              ! Geometrical b.c.
              !              
              tgcod => tgcod_lev
              call reacod(4_ip) 

           else
              !
              ! Node boundary condition
              !
              tncod => tncod_lev
              call reacod(1_ip)

           end if

        else if(words(1)=='ONNOD') then

           call runend('LEV_REABCS: THIS OPTION DOES NOT EXIST ANY MORE')

        end if

     end do

  end if

end subroutine lev_reabcs
