subroutine lev_updbcs()
  !-----------------------------------------------------------------------
  !****f* levels/lev_updbcs
  ! NAME 
  !    lev_updbcs
  ! DESCRIPTION
  !    This routine deals with adaptive boundary conditions
  ! USED BY
  !    lev_begste
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_levels
  implicit none
  integer(ip)   :: ipoin,ibopo,idime
  real(rp)      :: udotn

  if( INOTMASTER ) then
     do ipoin = 1,npoin
        ibopo = lpoty(ipoin)
        if( ibopo /= 0 ) then
           if( abs(kfl_fixno_lev(1,ipoin)) == 8 ) then
              udotn = 0.0_rp
              do idime = 1,ndime
                 udotn = udotn + veloc(idime,ipoin,1)*exnor(idime,1,ibopo)                    
              end do
              if( udotn >= 0.0_rp ) then
                 kfl_fixno_lev(1,ipoin) = -8              ! Outflow
              else
                 kfl_fixno_lev(1,ipoin) =  8              ! Inflow
              end if
           end if
        end if
     end do
  end if

end subroutine lev_updbcs

