subroutine lev_outset
  !-----------------------------------------------------------------------
  !****f* Levels/lev_outset
  ! NAME 
  !    lev_outset
  ! DESCRIPTION
  !    Calculation on sets
  ! USED BY
  !    lev_output
  !***
  !-----------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_domain
  use      def_levels
  implicit none

end subroutine lev_outset
