subroutine infmak() 
  !------------------------------------------------------------------------
  !****f* info/infmak 
  ! NAME  
  !    infmak 
  ! DESCRIPTION 
  !    Output makefile info 
  ! USES 
  !    outfor
  ! USED BY 
  !*** 
  !------------------------------------------------------------------------ 
  use def_master 
  implicit none 
  
  coutp(1)  = 'COMPILED BY:      ammaliszewski'   
  coutp(2)  = 'DATE:             Mon Nov 18 21:51:14 -03 2019'   
  coutp(3)  = 'HOSTNAME:         draco2'   
  coutp(4)  = 'ARCHITECTURE:     x86_64'   
  coutp(5)  = 'OPERATING SYSTEM: Linux 4.15.0-66-generic'   
  coutp(6)  = 'END'                           
  coutp(10) = 'F90:             mpif90'      
  coutp(11) = 'FPPFLAGS:        -x f95-cpp-input'    
  coutp(12) = 'FCFLAGS:         -c -J$O -I$O -ffree-line-length-none -fimplicit-none -fno-automatic' 
  coutp(13) = 'FOPT:            -O1' 
  coutp(14) = 'CSALYA:          -DMETIS' 
  coutp(15) =  'END'                          
  
end subroutine infmak 
