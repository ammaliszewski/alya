subroutine Solmum
!-----------------------------------------------------------------------
!
! This routine solves linear systems using MUMPS sparse direct method.     
!
!-----------------------------------------------------------------------
  implicit none

  call runend('SOLMUM: CANNOT USE THIS SOLVER: COMPILE THE SERVICE')

end subroutine Solmum
