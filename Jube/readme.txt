- Started from what Metin sent me for allinea

- things that are specific to Marenostrum are in the MN folder. Similar folders should be added for other systems. 

- now  case_dir and problemname have to be set by hand in the .xml.

- in Marenostrum run with: 
   - export JUBE_INCLUDE_PATH=/gpfs/projects/bsc21/WORK-HERBERT/svnmn3/Alya/Jube/MN
   - jube -v run AlyaJube.xml &

- svnlog and svndiff for the whole Alya written in 000000/000001_app/work/dir/bin
So that we have a better knowledge of which sources we are using. 

- Jube help is in http://apps.fz-juelich.de/jsc/jube/jube2/docu/index.html
