#!/bin/bash
# @ output = par_merge.out
# @ error =  par_merge.err
# @ total_tasks = 32
# @ cpus_per_task = 2
# @ tasks_per_node = 2
# @ wall_clock_limit = 24:00:00
# @ scratch = 15000

ALYA_BIN="DONDE ESTA EL BINARIO"
OUTNAME="NOMBRE DE LA TRAZA"
MPITRACE_HOME=/home/bsc41/bsc41273/pamare64/pqt

date
time srun ${MPITRACE_HOME}/bin/mpimpi2prv -syn -f TRACE.mpits -o ${OUTNAME}.prv -e ${ALYA_BIN} -maxmem 1536 date
