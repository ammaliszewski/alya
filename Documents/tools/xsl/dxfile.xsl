<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="html"/>  
  <xsl:template match="/module">
    
    <xsl:for-each select="group">
      <xsl:if test="groupHelp"><xsl:value-of select="groupHelp"></xsl:value-of><p/></xsl:if>
      <xsl:choose>
        <xsl:when test="count(subGroup) > 0">
          <xsl:for-each select="subGroup">
            <xsl:if test="subGroupHelp"><xsl:value-of select="subGroupHelp"></xsl:value-of><p/></xsl:if>
            <xsl:for-each select="inputLine">
              <xsl:value-of select="inputLineName"/>:<xsl:value-of select="inputLineHelp" disable-output-escaping="yes"/>
              <p/>
            </xsl:for-each>
          </xsl:for-each>          
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="//inputLineHelp">
            <xsl:value-of select="." disable-output-escaping="yes"/>
            <p/>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>       
    </xsl:for-each>        
  </xsl:template>
</xsl:stylesheet>