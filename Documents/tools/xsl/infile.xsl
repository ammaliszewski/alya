<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="text"/>
  <xsl:template match="/module/group">
<xsl:value-of select="groupName"></xsl:value-of><xsl:text>
</xsl:text>
    <!--group types-->
    <xsl:if test="groupType">
      <xsl:text>    </xsl:text><xsl:value-of select=" replace(normalize-space(replace(groupTypeDefinition,'&#xa;','%')),'%','&#xa;')  "></xsl:value-of>
    </xsl:if>
  <xsl:for-each select="subGroup">
    <xsl:if test="subGroupName"><xsl:text>    </xsl:text><xsl:value-of select="subGroupName"></xsl:value-of><xsl:text>&#xa;</xsl:text></xsl:if>
    
    <xsl:variable name="spacing"><xsl:choose>
      <xsl:when test="subGroupName"><xsl:text>           </xsl:text></xsl:when>
      <xsl:otherwise><xsl:text>    </xsl:text></xsl:otherwise>
    </xsl:choose></xsl:variable>
    
    <!-- subGroupTypes -->
    <xsl:if test="subGroupType">
      <xsl:value-of select="$spacing"></xsl:value-of><xsl:value-of select="subGroupType"></xsl:value-of><xsl:text>&#xa;</xsl:text>
      <xsl:value-of select="$spacing"></xsl:value-of>END_<xsl:value-of select="subGroupType"></xsl:value-of><xsl:text>&#xa;</xsl:text>
    </xsl:if>
    <!-- inputLine of the subgroup -->
    <xsl:for-each select="inputLine">
      
      <xsl:variable name="numElements"><xsl:value-of select="count(inputElement)"></xsl:value-of></xsl:variable>           
      
      <xsl:variable name="linea">
        <xsl:call-template name="procInputElement"><xsl:with-param name="inputElementList" select="inputElement"></xsl:with-param><xsl:with-param name="spacing" select="$spacing"></xsl:with-param></xsl:call-template>       
      </xsl:variable>
      
      <xsl:variable name="finElement">
        <xsl:if test="count(inputElement[inputElementType = 'list'])"><xsl:text>&#xa;</xsl:text><xsl:value-of select="$spacing"></xsl:value-of>END_<xsl:value-of select="inputLineName"></xsl:value-of></xsl:if>       
      </xsl:variable>
      
      <xsl:value-of select="$spacing"></xsl:value-of><xsl:value-of select="inputLineName"></xsl:value-of>: <xsl:if test="$numElements = 0">ON | OFF</xsl:if><xsl:value-of select="$linea"></xsl:value-of><xsl:value-of select="$finElement"></xsl:value-of><xsl:text>&#xa;</xsl:text>       
    </xsl:for-each>
    <xsl:if test="subGroupName"><xsl:text>    </xsl:text>END_<xsl:value-of select="subGroupName"></xsl:value-of><xsl:if test="position()!=last()"><xsl:text>&#xa;</xsl:text></xsl:if></xsl:if>
  </xsl:for-each>
END_<xsl:value-of select="groupName"></xsl:value-of>    
  </xsl:template>
  
  <xsl:template name="procInputElement">
    <xsl:param name="inputElementList"></xsl:param>
    <xsl:param name="group">-1</xsl:param>
    <xsl:param name="spacing"><xsl:text>           </xsl:text></xsl:param>
    <xsl:for-each select="$inputElementList">
      <xsl:variable name="tratar">
        <xsl:choose>
          <xsl:when test="$group = -1">
            <xsl:choose>
              <xsl:when test="count(inputElementGroup) = 0">1</xsl:when>
              <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
            <xsl:when test="inputElementGroup = $group">2</xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:if test="$tratar > 0">
        <xsl:choose>
          <xsl:when test="inputElementType = 'list'"></xsl:when>
          <xsl:when test="$tratar = 2"> , </xsl:when>
          <xsl:when test="position()!=1"> , </xsl:when>
        </xsl:choose>        
        <xsl:choose>
          <xsl:when test="inputElementType = 'edit'"><xsl:value-of select="inputLineEditName"></xsl:value-of>=real</xsl:when>
          <xsl:when test="inputElementType = 'edit2'">real<xsl:value-of select="position()"></xsl:value-of></xsl:when>
          <xsl:when test="inputElementType = 'combo'"><xsl:if test="inputElementName"><xsl:value-of select="inputElementName"></xsl:value-of> = </xsl:if><xsl:for-each select="item[itemName != 'EMPTY']"><xsl:value-of select="itemName"></xsl:value-of>
            <xsl:if test="itemDependence"> [ <xsl:call-template name="procInputElement"><xsl:with-param name="inputElementList" select="$inputElementList[position() > 1]"></xsl:with-param><xsl:with-param name="group" select="itemDependence"></xsl:with-param><xsl:with-param name="spacing" select="$spacing"></xsl:with-param></xsl:call-template> ] </xsl:if>
            <xsl:if test="position()!=last()"> | </xsl:if></xsl:for-each></xsl:when>
          <xsl:when test="inputElementType = 'list'"><xsl:text>&#xa;</xsl:text><xsl:value-of select="$spacing"></xsl:value-of><xsl:text>    </xsl:text><xsl:for-each select="item"><xsl:value-of select="itemName"></xsl:value-of>=value<xsl:if test="position()!=last()"> , </xsl:if></xsl:for-each><xsl:text>&#xa;</xsl:text><xsl:value-of select="$spacing"></xsl:value-of><xsl:text>    ...&#xa;    </xsl:text><xsl:value-of select="$spacing"></xsl:value-of>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>    
          
  </xsl:template>
  
</xsl:stylesheet>