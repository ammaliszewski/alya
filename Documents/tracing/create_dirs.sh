#!/bin/bash
#
# Create subdirectories for each (4-node) host
#
NUM_SERVERS=41
NUM_CENTERS=5
NUM_BLADES=14

TRACE_DIR=/gpfs/scratch/bsc21/bsc21943/TRACE_ALYA

for ((server=1; server <= NUM_SERVERS ; server++)) 
do
	for ((center=1; center <= NUM_CENTERS; center++)) 
	do
		for ((blade=1; blade <= NUM_BLADES; blade++)) 
		do
			#echo "$TRACE_DIR/`printf s%02dc%02db%02d $server $center $blade`"
			mkdir -p $TRACE_DIR/`printf s%02dc%02db%02d $server $center $blade`
		done	
	done
done
