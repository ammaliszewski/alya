#!/bin/bash

TRACE_DIR=/gpfs/scratch/bsc21/bsc21943/TRACE_ALYA

# MPItrace configuration
export MPITRACE_ON=1
export MPTRACE_ON=1
export MPITRACE_HOME=/gpfs/apps/CEPBATOOLS/64.hwc
export MPTRACE_DIR=/scratch/tmp
export MPTRACE_FINAL_DIR="$TRACE_DIR/`hostname`/$SLURM_JOBID"
export MPTRACE_BUFFER_SIZE=250000
export MPTRACE_FILE_SIZE=10
export MPITRACE_MPI_CALLER=2
export MPTRACE_COUNTERS=0x80000000,0x80000032,0x8000003b
export MPTRACE_COUNTERS_DOMAIN=all
export MPITRACE_MPI_COUNTERS_ON=1
export MPITRACE_NETWORK_COUNTERS=1
export MPITRACE_RUSAGE=1

mkdir -p $MPTRACE_FINAL_DIR/routes

#################
# Routes before #
#################
cp /var/run/gm_mapper/map.0 $MPTRACE_FINAL_DIR/routes/local_map.`hostname`.$$.map
/usr/local/gm_board_info > $MPTRACE_FINAL_DIR/routes/discover.check_before.`hostname`.$$.out
env > $MPTRACE_FINAL_DIR/routes/env.`hostname`.$$.out
squeue > $MPTRACE_FINAL_DIR/routes/machinelist.`hostname`.$$.out

#################
#    Tracing    #
#################
export LD_LIBRARY_PATH=/usr/local/papi/64/lib:$LD_LIBRARY_PATH

# Old version
#export LD_PRELOAD=/gpfs/apps/CEPBATOOLS/lib/64.hwc/libmpitrace.so
# New version
export LD_PRELOAD=/gpfs/apps/CEPBATOOLS/64.hwc/lib/libmpitrace.so
srun ...
export LD_PRELOAD=

#################
# Routes after  #
#################
/usr/local/gm_board_info > $MPTRACE_FINAL_DIR/routes/discover.check_after.`hostname`.$$.out

